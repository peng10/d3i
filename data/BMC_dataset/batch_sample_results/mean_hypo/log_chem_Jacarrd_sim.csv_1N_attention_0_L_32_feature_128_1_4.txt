start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 101.39168500900269
time_elpased: 1.617
batch start
#iterations: 1
currently lose_sum: 100.45674878358841
time_elpased: 1.574
batch start
#iterations: 2
currently lose_sum: 100.41584467887878
time_elpased: 1.599
batch start
#iterations: 3
currently lose_sum: 100.29390454292297
time_elpased: 1.582
batch start
#iterations: 4
currently lose_sum: 100.33276641368866
time_elpased: 1.583
batch start
#iterations: 5
currently lose_sum: 100.2615077495575
time_elpased: 1.583
batch start
#iterations: 6
currently lose_sum: 100.26135402917862
time_elpased: 1.573
batch start
#iterations: 7
currently lose_sum: 100.15078091621399
time_elpased: 1.607
batch start
#iterations: 8
currently lose_sum: 100.16726541519165
time_elpased: 1.598
batch start
#iterations: 9
currently lose_sum: 100.15135490894318
time_elpased: 1.625
batch start
#iterations: 10
currently lose_sum: 100.08285093307495
time_elpased: 1.586
batch start
#iterations: 11
currently lose_sum: 100.08312606811523
time_elpased: 1.615
batch start
#iterations: 12
currently lose_sum: 100.06387311220169
time_elpased: 1.574
batch start
#iterations: 13
currently lose_sum: 100.11328780651093
time_elpased: 1.62
batch start
#iterations: 14
currently lose_sum: 99.95384323596954
time_elpased: 1.577
batch start
#iterations: 15
currently lose_sum: 99.9834173321724
time_elpased: 1.608
batch start
#iterations: 16
currently lose_sum: 99.95408356189728
time_elpased: 1.578
batch start
#iterations: 17
currently lose_sum: 99.84889793395996
time_elpased: 1.58
batch start
#iterations: 18
currently lose_sum: 99.83108949661255
time_elpased: 1.605
batch start
#iterations: 19
currently lose_sum: 99.79093843698502
time_elpased: 1.59
start validation test
0.60412371134
0.590686492743
0.682721004425
0.633377888104
0.603985721669
65.693
batch start
#iterations: 20
currently lose_sum: 99.97110503911972
time_elpased: 1.592
batch start
#iterations: 21
currently lose_sum: 99.61411815881729
time_elpased: 1.591
batch start
#iterations: 22
currently lose_sum: 99.52297365665436
time_elpased: 1.577
batch start
#iterations: 23
currently lose_sum: 99.76428091526031
time_elpased: 1.583
batch start
#iterations: 24
currently lose_sum: 99.5621275305748
time_elpased: 1.578
batch start
#iterations: 25
currently lose_sum: 99.6129652261734
time_elpased: 1.57
batch start
#iterations: 26
currently lose_sum: 99.6937472820282
time_elpased: 1.585
batch start
#iterations: 27
currently lose_sum: 99.50287467241287
time_elpased: 1.623
batch start
#iterations: 28
currently lose_sum: 99.56600058078766
time_elpased: 1.6
batch start
#iterations: 29
currently lose_sum: 99.43956345319748
time_elpased: 1.621
batch start
#iterations: 30
currently lose_sum: 99.44487178325653
time_elpased: 1.589
batch start
#iterations: 31
currently lose_sum: 99.48910129070282
time_elpased: 1.611
batch start
#iterations: 32
currently lose_sum: 99.50552928447723
time_elpased: 1.655
batch start
#iterations: 33
currently lose_sum: 99.39395314455032
time_elpased: 1.61
batch start
#iterations: 34
currently lose_sum: 99.37342667579651
time_elpased: 1.601
batch start
#iterations: 35
currently lose_sum: 99.19642627239227
time_elpased: 1.596
batch start
#iterations: 36
currently lose_sum: 99.23933762311935
time_elpased: 1.588
batch start
#iterations: 37
currently lose_sum: 99.15833282470703
time_elpased: 1.579
batch start
#iterations: 38
currently lose_sum: 99.30651032924652
time_elpased: 1.605
batch start
#iterations: 39
currently lose_sum: 99.0213161110878
time_elpased: 1.592
start validation test
0.602731958763
0.658868163136
0.428938972934
0.519603565418
0.603037079155
64.538
batch start
#iterations: 40
currently lose_sum: 99.36606824398041
time_elpased: 1.597
batch start
#iterations: 41
currently lose_sum: 99.28402590751648
time_elpased: 1.579
batch start
#iterations: 42
currently lose_sum: 99.2581358551979
time_elpased: 1.579
batch start
#iterations: 43
currently lose_sum: 99.22388160228729
time_elpased: 1.592
batch start
#iterations: 44
currently lose_sum: 99.08349305391312
time_elpased: 1.591
batch start
#iterations: 45
currently lose_sum: 99.1675243973732
time_elpased: 1.616
batch start
#iterations: 46
currently lose_sum: 99.0672299861908
time_elpased: 1.605
batch start
#iterations: 47
currently lose_sum: 99.2631448507309
time_elpased: 1.65
batch start
#iterations: 48
currently lose_sum: 99.12647593021393
time_elpased: 1.58
batch start
#iterations: 49
currently lose_sum: 99.10653561353683
time_elpased: 1.6
batch start
#iterations: 50
currently lose_sum: 99.15323960781097
time_elpased: 1.606
batch start
#iterations: 51
currently lose_sum: 98.98256975412369
time_elpased: 1.604
batch start
#iterations: 52
currently lose_sum: 99.0887166261673
time_elpased: 1.592
batch start
#iterations: 53
currently lose_sum: 99.17813140153885
time_elpased: 1.579
batch start
#iterations: 54
currently lose_sum: 99.12133359909058
time_elpased: 1.581
batch start
#iterations: 55
currently lose_sum: 98.90559512376785
time_elpased: 1.631
batch start
#iterations: 56
currently lose_sum: 99.10560637712479
time_elpased: 1.62
batch start
#iterations: 57
currently lose_sum: 99.41181230545044
time_elpased: 1.586
batch start
#iterations: 58
currently lose_sum: 99.05108517408371
time_elpased: 1.598
batch start
#iterations: 59
currently lose_sum: 99.21028977632523
time_elpased: 1.616
start validation test
0.636701030928
0.600678989061
0.819388700216
0.693191711649
0.636380294547
64.450
batch start
#iterations: 60
currently lose_sum: 99.08983016014099
time_elpased: 1.612
batch start
#iterations: 61
currently lose_sum: 99.25884246826172
time_elpased: 1.592
batch start
#iterations: 62
currently lose_sum: 98.98003935813904
time_elpased: 1.589
batch start
#iterations: 63
currently lose_sum: 98.86516004800797
time_elpased: 1.578
batch start
#iterations: 64
currently lose_sum: 99.09404736757278
time_elpased: 1.623
batch start
#iterations: 65
currently lose_sum: 98.99103784561157
time_elpased: 1.579
batch start
#iterations: 66
currently lose_sum: 99.08550053834915
time_elpased: 1.594
batch start
#iterations: 67
currently lose_sum: 99.029845058918
time_elpased: 1.6
batch start
#iterations: 68
currently lose_sum: 98.94469213485718
time_elpased: 1.582
batch start
#iterations: 69
currently lose_sum: 99.00471061468124
time_elpased: 1.59
batch start
#iterations: 70
currently lose_sum: 99.1734670996666
time_elpased: 1.6
batch start
#iterations: 71
currently lose_sum: 99.08126264810562
time_elpased: 1.603
batch start
#iterations: 72
currently lose_sum: 98.89445054531097
time_elpased: 1.612
batch start
#iterations: 73
currently lose_sum: 98.99599009752274
time_elpased: 1.592
batch start
#iterations: 74
currently lose_sum: 98.88099825382233
time_elpased: 1.595
batch start
#iterations: 75
currently lose_sum: 99.00567030906677
time_elpased: 1.619
batch start
#iterations: 76
currently lose_sum: 99.13433039188385
time_elpased: 1.587
batch start
#iterations: 77
currently lose_sum: 99.15193051099777
time_elpased: 1.593
batch start
#iterations: 78
currently lose_sum: 98.96534603834152
time_elpased: 1.603
batch start
#iterations: 79
currently lose_sum: 98.92737150192261
time_elpased: 1.622
start validation test
0.618195876289
0.671568627451
0.465267057734
0.549699069852
0.618464366417
63.947
batch start
#iterations: 80
currently lose_sum: 98.835380256176
time_elpased: 1.588
batch start
#iterations: 81
currently lose_sum: 98.71455466747284
time_elpased: 1.576
batch start
#iterations: 82
currently lose_sum: 99.13718944787979
time_elpased: 1.619
batch start
#iterations: 83
currently lose_sum: 98.9453592300415
time_elpased: 1.626
batch start
#iterations: 84
currently lose_sum: 98.776995241642
time_elpased: 1.597
batch start
#iterations: 85
currently lose_sum: 98.96306675672531
time_elpased: 1.603
batch start
#iterations: 86
currently lose_sum: 99.215440928936
time_elpased: 1.612
batch start
#iterations: 87
currently lose_sum: 98.8354440331459
time_elpased: 1.586
batch start
#iterations: 88
currently lose_sum: 99.03456318378448
time_elpased: 1.613
batch start
#iterations: 89
currently lose_sum: 98.88023459911346
time_elpased: 1.584
batch start
#iterations: 90
currently lose_sum: 98.84579879045486
time_elpased: 1.592
batch start
#iterations: 91
currently lose_sum: 99.23505181074142
time_elpased: 1.613
batch start
#iterations: 92
currently lose_sum: 99.05469864606857
time_elpased: 1.599
batch start
#iterations: 93
currently lose_sum: 98.8809825181961
time_elpased: 1.63
batch start
#iterations: 94
currently lose_sum: 98.83117371797562
time_elpased: 1.607
batch start
#iterations: 95
currently lose_sum: 98.9915908575058
time_elpased: 1.575
batch start
#iterations: 96
currently lose_sum: 99.02945065498352
time_elpased: 1.613
batch start
#iterations: 97
currently lose_sum: 99.12628477811813
time_elpased: 1.602
batch start
#iterations: 98
currently lose_sum: 98.90835958719254
time_elpased: 1.627
batch start
#iterations: 99
currently lose_sum: 99.00495892763138
time_elpased: 1.576
start validation test
0.627371134021
0.651744327885
0.549861068231
0.596483393804
0.627507214896
64.063
batch start
#iterations: 100
currently lose_sum: 98.87610876560211
time_elpased: 1.581
batch start
#iterations: 101
currently lose_sum: 99.07591688632965
time_elpased: 1.613
batch start
#iterations: 102
currently lose_sum: 98.93216276168823
time_elpased: 1.584
batch start
#iterations: 103
currently lose_sum: 98.91929286718369
time_elpased: 1.593
batch start
#iterations: 104
currently lose_sum: 98.94587242603302
time_elpased: 1.612
batch start
#iterations: 105
currently lose_sum: 98.79984402656555
time_elpased: 1.625
batch start
#iterations: 106
currently lose_sum: 98.93666702508926
time_elpased: 1.615
batch start
#iterations: 107
currently lose_sum: 98.8417050242424
time_elpased: 1.605
batch start
#iterations: 108
currently lose_sum: 98.87199378013611
time_elpased: 1.619
batch start
#iterations: 109
currently lose_sum: 99.03324788808823
time_elpased: 1.585
batch start
#iterations: 110
currently lose_sum: 98.85125875473022
time_elpased: 1.616
batch start
#iterations: 111
currently lose_sum: 98.92659151554108
time_elpased: 1.617
batch start
#iterations: 112
currently lose_sum: 98.85795670747757
time_elpased: 1.598
batch start
#iterations: 113
currently lose_sum: 98.63323664665222
time_elpased: 1.603
batch start
#iterations: 114
currently lose_sum: 98.85051500797272
time_elpased: 1.594
batch start
#iterations: 115
currently lose_sum: 98.66113257408142
time_elpased: 1.575
batch start
#iterations: 116
currently lose_sum: 98.83984315395355
time_elpased: 1.59
batch start
#iterations: 117
currently lose_sum: 98.87619507312775
time_elpased: 1.595
batch start
#iterations: 118
currently lose_sum: 98.82340884208679
time_elpased: 1.593
batch start
#iterations: 119
currently lose_sum: 98.67220258712769
time_elpased: 1.648
start validation test
0.650979381443
0.62817612252
0.74292477102
0.680748738743
0.65081795713
63.666
batch start
#iterations: 120
currently lose_sum: 98.82632803916931
time_elpased: 1.591
batch start
#iterations: 121
currently lose_sum: 98.9487276673317
time_elpased: 1.595
batch start
#iterations: 122
currently lose_sum: 98.90678292512894
time_elpased: 1.591
batch start
#iterations: 123
currently lose_sum: 98.9930391907692
time_elpased: 1.593
batch start
#iterations: 124
currently lose_sum: 99.11795526742935
time_elpased: 1.625
batch start
#iterations: 125
currently lose_sum: 98.65683144330978
time_elpased: 1.587
batch start
#iterations: 126
currently lose_sum: 98.8044638633728
time_elpased: 1.595
batch start
#iterations: 127
currently lose_sum: 98.91317933797836
time_elpased: 1.594
batch start
#iterations: 128
currently lose_sum: 98.86297798156738
time_elpased: 1.606
batch start
#iterations: 129
currently lose_sum: 98.45192271471024
time_elpased: 1.594
batch start
#iterations: 130
currently lose_sum: 98.85839569568634
time_elpased: 1.611
batch start
#iterations: 131
currently lose_sum: 98.87292444705963
time_elpased: 1.598
batch start
#iterations: 132
currently lose_sum: 99.0487647652626
time_elpased: 1.59
batch start
#iterations: 133
currently lose_sum: 98.76149892807007
time_elpased: 1.603
batch start
#iterations: 134
currently lose_sum: 98.94223779439926
time_elpased: 1.584
batch start
#iterations: 135
currently lose_sum: 98.90038919448853
time_elpased: 1.569
batch start
#iterations: 136
currently lose_sum: 98.60421544313431
time_elpased: 1.593
batch start
#iterations: 137
currently lose_sum: 98.72840791940689
time_elpased: 1.609
batch start
#iterations: 138
currently lose_sum: 98.84314405918121
time_elpased: 1.58
batch start
#iterations: 139
currently lose_sum: 98.93872237205505
time_elpased: 1.581
start validation test
0.654793814433
0.6613592648
0.636924976845
0.648912188729
0.654825185933
63.582
batch start
#iterations: 140
currently lose_sum: 98.83787536621094
time_elpased: 1.602
batch start
#iterations: 141
currently lose_sum: 98.91999632120132
time_elpased: 1.6
batch start
#iterations: 142
currently lose_sum: 98.99092590808868
time_elpased: 1.61
batch start
#iterations: 143
currently lose_sum: 98.84703642129898
time_elpased: 1.598
batch start
#iterations: 144
currently lose_sum: 98.6103196144104
time_elpased: 1.6
batch start
#iterations: 145
currently lose_sum: 98.89803338050842
time_elpased: 1.6
batch start
#iterations: 146
currently lose_sum: 98.63122165203094
time_elpased: 1.586
batch start
#iterations: 147
currently lose_sum: 98.94770961999893
time_elpased: 1.608
batch start
#iterations: 148
currently lose_sum: 98.89571624994278
time_elpased: 1.587
batch start
#iterations: 149
currently lose_sum: 98.73996841907501
time_elpased: 1.599
batch start
#iterations: 150
currently lose_sum: 98.77207618951797
time_elpased: 1.585
batch start
#iterations: 151
currently lose_sum: 98.60221141576767
time_elpased: 1.616
batch start
#iterations: 152
currently lose_sum: 98.70010024309158
time_elpased: 1.584
batch start
#iterations: 153
currently lose_sum: 98.93235152959824
time_elpased: 1.589
batch start
#iterations: 154
currently lose_sum: 98.51100999116898
time_elpased: 1.619
batch start
#iterations: 155
currently lose_sum: 98.80969899892807
time_elpased: 1.605
batch start
#iterations: 156
currently lose_sum: 99.00271552801132
time_elpased: 1.571
batch start
#iterations: 157
currently lose_sum: 98.7757311463356
time_elpased: 1.606
batch start
#iterations: 158
currently lose_sum: 98.85042947530746
time_elpased: 1.599
batch start
#iterations: 159
currently lose_sum: 98.89075827598572
time_elpased: 1.603
start validation test
0.599690721649
0.679155188246
0.380570134815
0.487798443477
0.600075421637
63.919
batch start
#iterations: 160
currently lose_sum: 98.8020646572113
time_elpased: 1.592
batch start
#iterations: 161
currently lose_sum: 98.75483852624893
time_elpased: 1.599
batch start
#iterations: 162
currently lose_sum: 98.75824189186096
time_elpased: 1.62
batch start
#iterations: 163
currently lose_sum: 98.66850483417511
time_elpased: 1.591
batch start
#iterations: 164
currently lose_sum: 98.77671152353287
time_elpased: 1.608
batch start
#iterations: 165
currently lose_sum: 98.9843037724495
time_elpased: 1.614
batch start
#iterations: 166
currently lose_sum: 98.70277231931686
time_elpased: 1.607
batch start
#iterations: 167
currently lose_sum: 98.58081936836243
time_elpased: 1.588
batch start
#iterations: 168
currently lose_sum: 98.99459528923035
time_elpased: 1.607
batch start
#iterations: 169
currently lose_sum: 98.8759053349495
time_elpased: 1.592
batch start
#iterations: 170
currently lose_sum: 98.55810141563416
time_elpased: 1.598
batch start
#iterations: 171
currently lose_sum: 98.64013266563416
time_elpased: 1.619
batch start
#iterations: 172
currently lose_sum: 98.88727217912674
time_elpased: 1.615
batch start
#iterations: 173
currently lose_sum: 98.71240246295929
time_elpased: 1.602
batch start
#iterations: 174
currently lose_sum: 98.75151228904724
time_elpased: 1.594
batch start
#iterations: 175
currently lose_sum: 98.70304214954376
time_elpased: 1.625
batch start
#iterations: 176
currently lose_sum: 98.82078099250793
time_elpased: 1.59
batch start
#iterations: 177
currently lose_sum: 98.61730992794037
time_elpased: 1.612
batch start
#iterations: 178
currently lose_sum: 98.69958597421646
time_elpased: 1.615
batch start
#iterations: 179
currently lose_sum: 98.9218322634697
time_elpased: 1.616
start validation test
0.64175257732
0.670151272906
0.560769784913
0.610600627521
0.641894755102
63.543
batch start
#iterations: 180
currently lose_sum: 98.8282173871994
time_elpased: 1.631
batch start
#iterations: 181
currently lose_sum: 98.71847659349442
time_elpased: 1.605
batch start
#iterations: 182
currently lose_sum: 98.64219510555267
time_elpased: 1.595
batch start
#iterations: 183
currently lose_sum: 98.61588943004608
time_elpased: 1.59
batch start
#iterations: 184
currently lose_sum: 98.62622171640396
time_elpased: 1.603
batch start
#iterations: 185
currently lose_sum: 98.59944128990173
time_elpased: 1.606
batch start
#iterations: 186
currently lose_sum: 98.80844235420227
time_elpased: 1.603
batch start
#iterations: 187
currently lose_sum: 98.90104061365128
time_elpased: 1.589
batch start
#iterations: 188
currently lose_sum: 98.43582105636597
time_elpased: 1.625
batch start
#iterations: 189
currently lose_sum: 98.6891342997551
time_elpased: 1.635
batch start
#iterations: 190
currently lose_sum: 98.80585271120071
time_elpased: 1.588
batch start
#iterations: 191
currently lose_sum: 98.68952995538712
time_elpased: 1.598
batch start
#iterations: 192
currently lose_sum: 98.7222512960434
time_elpased: 1.598
batch start
#iterations: 193
currently lose_sum: 98.82910561561584
time_elpased: 1.588
batch start
#iterations: 194
currently lose_sum: 98.65217232704163
time_elpased: 1.623
batch start
#iterations: 195
currently lose_sum: 98.67222064733505
time_elpased: 1.61
batch start
#iterations: 196
currently lose_sum: 98.4638751745224
time_elpased: 1.604
batch start
#iterations: 197
currently lose_sum: 98.60587626695633
time_elpased: 1.612
batch start
#iterations: 198
currently lose_sum: 98.62997615337372
time_elpased: 1.592
batch start
#iterations: 199
currently lose_sum: 98.49983191490173
time_elpased: 1.616
start validation test
0.652886597938
0.631351827389
0.737779149943
0.680429005315
0.652737555969
63.474
batch start
#iterations: 200
currently lose_sum: 98.74412167072296
time_elpased: 1.622
batch start
#iterations: 201
currently lose_sum: 98.64275068044662
time_elpased: 1.596
batch start
#iterations: 202
currently lose_sum: 98.7390940785408
time_elpased: 1.597
batch start
#iterations: 203
currently lose_sum: 98.73473989963531
time_elpased: 1.611
batch start
#iterations: 204
currently lose_sum: 98.52828633785248
time_elpased: 1.615
batch start
#iterations: 205
currently lose_sum: 98.77517908811569
time_elpased: 1.593
batch start
#iterations: 206
currently lose_sum: 98.59764796495438
time_elpased: 1.615
batch start
#iterations: 207
currently lose_sum: 98.72969061136246
time_elpased: 1.598
batch start
#iterations: 208
currently lose_sum: 98.45790594816208
time_elpased: 1.637
batch start
#iterations: 209
currently lose_sum: 98.5861821770668
time_elpased: 1.616
batch start
#iterations: 210
currently lose_sum: 98.35463172197342
time_elpased: 1.632
batch start
#iterations: 211
currently lose_sum: 98.44008773565292
time_elpased: 1.649
batch start
#iterations: 212
currently lose_sum: 98.56791758537292
time_elpased: 1.601
batch start
#iterations: 213
currently lose_sum: 98.59868276119232
time_elpased: 1.622
batch start
#iterations: 214
currently lose_sum: 98.97087198495865
time_elpased: 1.607
batch start
#iterations: 215
currently lose_sum: 98.58408451080322
time_elpased: 1.609
batch start
#iterations: 216
currently lose_sum: 98.68986505270004
time_elpased: 1.637
batch start
#iterations: 217
currently lose_sum: 98.62390953302383
time_elpased: 1.601
batch start
#iterations: 218
currently lose_sum: 98.52304524183273
time_elpased: 1.614
batch start
#iterations: 219
currently lose_sum: 98.64567589759827
time_elpased: 1.606
start validation test
0.658350515464
0.654589130217
0.673047236801
0.663689872133
0.658324713103
63.193
batch start
#iterations: 220
currently lose_sum: 98.86227142810822
time_elpased: 1.596
batch start
#iterations: 221
currently lose_sum: 98.845667719841
time_elpased: 1.619
batch start
#iterations: 222
currently lose_sum: 98.68493050336838
time_elpased: 1.59
batch start
#iterations: 223
currently lose_sum: 98.75618833303452
time_elpased: 1.614
batch start
#iterations: 224
currently lose_sum: 98.49190938472748
time_elpased: 1.593
batch start
#iterations: 225
currently lose_sum: 98.514289021492
time_elpased: 1.636
batch start
#iterations: 226
currently lose_sum: 98.44878286123276
time_elpased: 1.631
batch start
#iterations: 227
currently lose_sum: 98.59297853708267
time_elpased: 1.598
batch start
#iterations: 228
currently lose_sum: 98.98312026262283
time_elpased: 1.616
batch start
#iterations: 229
currently lose_sum: 98.83786553144455
time_elpased: 1.59
batch start
#iterations: 230
currently lose_sum: 98.5106571316719
time_elpased: 1.584
batch start
#iterations: 231
currently lose_sum: 98.53011351823807
time_elpased: 1.576
batch start
#iterations: 232
currently lose_sum: 98.84748828411102
time_elpased: 1.6
batch start
#iterations: 233
currently lose_sum: 98.62172561883926
time_elpased: 1.602
batch start
#iterations: 234
currently lose_sum: 98.71706026792526
time_elpased: 1.613
batch start
#iterations: 235
currently lose_sum: 98.756687104702
time_elpased: 1.589
batch start
#iterations: 236
currently lose_sum: 98.50278216600418
time_elpased: 1.59
batch start
#iterations: 237
currently lose_sum: 98.6812332868576
time_elpased: 1.59
batch start
#iterations: 238
currently lose_sum: 98.55429893732071
time_elpased: 1.585
batch start
#iterations: 239
currently lose_sum: 98.57964086532593
time_elpased: 1.599
start validation test
0.65
0.668936857901
0.596377482762
0.630576713819
0.6500941426
63.388
batch start
#iterations: 240
currently lose_sum: 98.92858248949051
time_elpased: 1.611
batch start
#iterations: 241
currently lose_sum: 98.65808182954788
time_elpased: 1.603
batch start
#iterations: 242
currently lose_sum: 98.64786463975906
time_elpased: 1.601
batch start
#iterations: 243
currently lose_sum: 98.66675281524658
time_elpased: 1.6
batch start
#iterations: 244
currently lose_sum: 98.77472430467606
time_elpased: 1.613
batch start
#iterations: 245
currently lose_sum: 98.75181567668915
time_elpased: 1.59
batch start
#iterations: 246
currently lose_sum: 98.64889132976532
time_elpased: 1.596
batch start
#iterations: 247
currently lose_sum: 98.55787044763565
time_elpased: 1.585
batch start
#iterations: 248
currently lose_sum: 98.88981574773788
time_elpased: 1.609
batch start
#iterations: 249
currently lose_sum: 98.6386456489563
time_elpased: 1.602
batch start
#iterations: 250
currently lose_sum: 98.6685693860054
time_elpased: 1.602
batch start
#iterations: 251
currently lose_sum: 98.51757735013962
time_elpased: 1.629
batch start
#iterations: 252
currently lose_sum: 98.69932466745377
time_elpased: 1.602
batch start
#iterations: 253
currently lose_sum: 98.34519672393799
time_elpased: 1.598
batch start
#iterations: 254
currently lose_sum: 98.52169162034988
time_elpased: 1.623
batch start
#iterations: 255
currently lose_sum: 98.77232652902603
time_elpased: 1.59
batch start
#iterations: 256
currently lose_sum: 98.63185131549835
time_elpased: 1.608
batch start
#iterations: 257
currently lose_sum: 98.86948996782303
time_elpased: 1.595
batch start
#iterations: 258
currently lose_sum: 98.23736375570297
time_elpased: 1.593
batch start
#iterations: 259
currently lose_sum: 98.62552034854889
time_elpased: 1.6
start validation test
0.643556701031
0.603196817914
0.842749819903
0.703129695617
0.643206986787
63.552
batch start
#iterations: 260
currently lose_sum: 98.74704509973526
time_elpased: 1.589
batch start
#iterations: 261
currently lose_sum: 98.47292101383209
time_elpased: 1.624
batch start
#iterations: 262
currently lose_sum: 98.52956712245941
time_elpased: 1.612
batch start
#iterations: 263
currently lose_sum: 98.74216908216476
time_elpased: 1.626
batch start
#iterations: 264
currently lose_sum: 98.88277941942215
time_elpased: 1.625
batch start
#iterations: 265
currently lose_sum: 98.5059186220169
time_elpased: 1.611
batch start
#iterations: 266
currently lose_sum: 98.69120115041733
time_elpased: 1.601
batch start
#iterations: 267
currently lose_sum: 98.77833759784698
time_elpased: 1.572
batch start
#iterations: 268
currently lose_sum: 98.77652996778488
time_elpased: 1.64
batch start
#iterations: 269
currently lose_sum: 98.72554183006287
time_elpased: 1.606
batch start
#iterations: 270
currently lose_sum: 98.43764358758926
time_elpased: 1.586
batch start
#iterations: 271
currently lose_sum: 98.41941475868225
time_elpased: 1.59
batch start
#iterations: 272
currently lose_sum: 98.90779954195023
time_elpased: 1.608
batch start
#iterations: 273
currently lose_sum: 98.49949926137924
time_elpased: 1.592
batch start
#iterations: 274
currently lose_sum: 99.01323890686035
time_elpased: 1.605
batch start
#iterations: 275
currently lose_sum: 98.77206754684448
time_elpased: 1.594
batch start
#iterations: 276
currently lose_sum: 98.50554156303406
time_elpased: 1.584
batch start
#iterations: 277
currently lose_sum: 98.76959455013275
time_elpased: 1.581
batch start
#iterations: 278
currently lose_sum: 98.72706943750381
time_elpased: 1.587
batch start
#iterations: 279
currently lose_sum: 98.44372487068176
time_elpased: 1.572
start validation test
0.633659793814
0.683958274598
0.49933106926
0.577241092142
0.633895628609
63.309
batch start
#iterations: 280
currently lose_sum: 98.69858938455582
time_elpased: 1.608
batch start
#iterations: 281
currently lose_sum: 98.55342280864716
time_elpased: 1.635
batch start
#iterations: 282
currently lose_sum: 98.5704642534256
time_elpased: 1.612
batch start
#iterations: 283
currently lose_sum: 98.62164211273193
time_elpased: 1.639
batch start
#iterations: 284
currently lose_sum: 98.52816408872604
time_elpased: 1.607
batch start
#iterations: 285
currently lose_sum: 98.53380650281906
time_elpased: 1.611
batch start
#iterations: 286
currently lose_sum: 98.62942117452621
time_elpased: 1.592
batch start
#iterations: 287
currently lose_sum: 98.56346517801285
time_elpased: 1.604
batch start
#iterations: 288
currently lose_sum: 98.52501052618027
time_elpased: 1.593
batch start
#iterations: 289
currently lose_sum: 98.56479501724243
time_elpased: 1.58
batch start
#iterations: 290
currently lose_sum: 98.82330125570297
time_elpased: 1.6
batch start
#iterations: 291
currently lose_sum: 98.09035921096802
time_elpased: 1.593
batch start
#iterations: 292
currently lose_sum: 98.64484250545502
time_elpased: 1.599
batch start
#iterations: 293
currently lose_sum: 98.58123618364334
time_elpased: 1.586
batch start
#iterations: 294
currently lose_sum: 98.48771131038666
time_elpased: 1.581
batch start
#iterations: 295
currently lose_sum: 98.59407091140747
time_elpased: 1.574
batch start
#iterations: 296
currently lose_sum: 98.65078032016754
time_elpased: 1.582
batch start
#iterations: 297
currently lose_sum: 98.96706354618073
time_elpased: 1.595
batch start
#iterations: 298
currently lose_sum: 98.49998557567596
time_elpased: 1.594
batch start
#iterations: 299
currently lose_sum: 98.76583349704742
time_elpased: 1.585
start validation test
0.634896907216
0.677254374159
0.517855305135
0.5869248265
0.635102391801
63.268
batch start
#iterations: 300
currently lose_sum: 98.58676648139954
time_elpased: 1.576
batch start
#iterations: 301
currently lose_sum: 98.5122377872467
time_elpased: 1.608
batch start
#iterations: 302
currently lose_sum: 98.28589981794357
time_elpased: 1.608
batch start
#iterations: 303
currently lose_sum: 98.48950320482254
time_elpased: 1.59
batch start
#iterations: 304
currently lose_sum: 98.58824837207794
time_elpased: 1.638
batch start
#iterations: 305
currently lose_sum: 98.53835564851761
time_elpased: 1.59
batch start
#iterations: 306
currently lose_sum: 98.6211878657341
time_elpased: 1.611
batch start
#iterations: 307
currently lose_sum: 98.52549493312836
time_elpased: 1.592
batch start
#iterations: 308
currently lose_sum: 98.72154307365417
time_elpased: 1.592
batch start
#iterations: 309
currently lose_sum: 98.64446091651917
time_elpased: 1.608
batch start
#iterations: 310
currently lose_sum: 98.83538740873337
time_elpased: 1.607
batch start
#iterations: 311
currently lose_sum: 98.65748429298401
time_elpased: 1.581
batch start
#iterations: 312
currently lose_sum: 98.32678472995758
time_elpased: 1.607
batch start
#iterations: 313
currently lose_sum: 98.53919714689255
time_elpased: 1.576
batch start
#iterations: 314
currently lose_sum: 98.57400077581406
time_elpased: 1.578
batch start
#iterations: 315
currently lose_sum: 98.62511193752289
time_elpased: 1.576
batch start
#iterations: 316
currently lose_sum: 98.64179509878159
time_elpased: 1.622
batch start
#iterations: 317
currently lose_sum: 98.58722674846649
time_elpased: 1.597
batch start
#iterations: 318
currently lose_sum: 98.42786526679993
time_elpased: 1.615
batch start
#iterations: 319
currently lose_sum: 98.47857350111008
time_elpased: 1.58
start validation test
0.599278350515
0.683474976393
0.372440053514
0.482147615241
0.599676600133
63.580
batch start
#iterations: 320
currently lose_sum: 98.60420745611191
time_elpased: 1.621
batch start
#iterations: 321
currently lose_sum: 98.43107986450195
time_elpased: 1.579
batch start
#iterations: 322
currently lose_sum: 98.79114037752151
time_elpased: 1.607
batch start
#iterations: 323
currently lose_sum: 98.86864978075027
time_elpased: 1.578
batch start
#iterations: 324
currently lose_sum: 98.63574862480164
time_elpased: 1.618
batch start
#iterations: 325
currently lose_sum: 98.55804246664047
time_elpased: 1.601
batch start
#iterations: 326
currently lose_sum: 98.41669768095016
time_elpased: 1.586
batch start
#iterations: 327
currently lose_sum: 98.5047105550766
time_elpased: 1.6
batch start
#iterations: 328
currently lose_sum: 98.5773446559906
time_elpased: 1.583
batch start
#iterations: 329
currently lose_sum: 98.61297100782394
time_elpased: 1.595
batch start
#iterations: 330
currently lose_sum: 98.41833865642548
time_elpased: 1.595
batch start
#iterations: 331
currently lose_sum: 98.66208016872406
time_elpased: 1.6
batch start
#iterations: 332
currently lose_sum: 98.71281152963638
time_elpased: 1.604
batch start
#iterations: 333
currently lose_sum: 98.55410653352737
time_elpased: 1.579
batch start
#iterations: 334
currently lose_sum: 98.93238574266434
time_elpased: 1.611
batch start
#iterations: 335
currently lose_sum: 98.38881295919418
time_elpased: 1.587
batch start
#iterations: 336
currently lose_sum: 98.61646169424057
time_elpased: 1.599
batch start
#iterations: 337
currently lose_sum: 98.35008108615875
time_elpased: 1.594
batch start
#iterations: 338
currently lose_sum: 98.77581775188446
time_elpased: 1.61
batch start
#iterations: 339
currently lose_sum: 98.62784492969513
time_elpased: 1.6
start validation test
0.649329896907
0.674616490892
0.579294020788
0.623332041415
0.64945285569
63.127
batch start
#iterations: 340
currently lose_sum: 98.70699501037598
time_elpased: 1.596
batch start
#iterations: 341
currently lose_sum: 98.76613116264343
time_elpased: 1.582
batch start
#iterations: 342
currently lose_sum: 98.48474180698395
time_elpased: 1.622
batch start
#iterations: 343
currently lose_sum: 98.32345253229141
time_elpased: 1.6
batch start
#iterations: 344
currently lose_sum: 98.53519660234451
time_elpased: 1.601
batch start
#iterations: 345
currently lose_sum: 98.7192999124527
time_elpased: 1.59
batch start
#iterations: 346
currently lose_sum: 98.64627522230148
time_elpased: 1.629
batch start
#iterations: 347
currently lose_sum: 98.33712404966354
time_elpased: 1.574
batch start
#iterations: 348
currently lose_sum: 98.80473899841309
time_elpased: 1.603
batch start
#iterations: 349
currently lose_sum: 98.2793157696724
time_elpased: 1.6
batch start
#iterations: 350
currently lose_sum: 98.54131180047989
time_elpased: 1.61
batch start
#iterations: 351
currently lose_sum: 98.44114446640015
time_elpased: 1.625
batch start
#iterations: 352
currently lose_sum: 98.57147884368896
time_elpased: 1.619
batch start
#iterations: 353
currently lose_sum: 98.68416404724121
time_elpased: 1.603
batch start
#iterations: 354
currently lose_sum: 98.48364996910095
time_elpased: 1.613
batch start
#iterations: 355
currently lose_sum: 98.41010475158691
time_elpased: 1.598
batch start
#iterations: 356
currently lose_sum: 98.415458381176
time_elpased: 1.612
batch start
#iterations: 357
currently lose_sum: 98.48032885789871
time_elpased: 1.606
batch start
#iterations: 358
currently lose_sum: 98.26306629180908
time_elpased: 1.604
batch start
#iterations: 359
currently lose_sum: 98.5299266576767
time_elpased: 1.616
start validation test
0.626340206186
0.677553956835
0.484614592981
0.565068698626
0.626589027359
63.327
batch start
#iterations: 360
currently lose_sum: 98.51855850219727
time_elpased: 1.589
batch start
#iterations: 361
currently lose_sum: 98.80332332849503
time_elpased: 1.571
batch start
#iterations: 362
currently lose_sum: 98.4793284535408
time_elpased: 1.628
batch start
#iterations: 363
currently lose_sum: 98.49375993013382
time_elpased: 1.621
batch start
#iterations: 364
currently lose_sum: 98.625885784626
time_elpased: 1.64
batch start
#iterations: 365
currently lose_sum: 98.50797939300537
time_elpased: 1.579
batch start
#iterations: 366
currently lose_sum: 98.63470649719238
time_elpased: 1.594
batch start
#iterations: 367
currently lose_sum: 98.73004102706909
time_elpased: 1.584
batch start
#iterations: 368
currently lose_sum: 98.7813515663147
time_elpased: 1.626
batch start
#iterations: 369
currently lose_sum: 98.75709563493729
time_elpased: 1.573
batch start
#iterations: 370
currently lose_sum: 98.4674237370491
time_elpased: 1.627
batch start
#iterations: 371
currently lose_sum: 98.4437221288681
time_elpased: 1.604
batch start
#iterations: 372
currently lose_sum: 98.50084537267685
time_elpased: 1.603
batch start
#iterations: 373
currently lose_sum: 98.71126371622086
time_elpased: 1.6
batch start
#iterations: 374
currently lose_sum: 98.38102865219116
time_elpased: 1.583
batch start
#iterations: 375
currently lose_sum: 98.56944626569748
time_elpased: 1.582
batch start
#iterations: 376
currently lose_sum: 98.60225826501846
time_elpased: 1.602
batch start
#iterations: 377
currently lose_sum: 98.68233132362366
time_elpased: 1.595
batch start
#iterations: 378
currently lose_sum: 98.48291552066803
time_elpased: 1.591
batch start
#iterations: 379
currently lose_sum: 98.40587145090103
time_elpased: 1.578
start validation test
0.657268041237
0.669128996692
0.624575486261
0.646085058817
0.65732543806
63.103
batch start
#iterations: 380
currently lose_sum: 98.40954619646072
time_elpased: 1.602
batch start
#iterations: 381
currently lose_sum: 98.62771195173264
time_elpased: 1.601
batch start
#iterations: 382
currently lose_sum: 98.6972451210022
time_elpased: 1.592
batch start
#iterations: 383
currently lose_sum: 98.42931258678436
time_elpased: 1.595
batch start
#iterations: 384
currently lose_sum: 98.55643421411514
time_elpased: 1.61
batch start
#iterations: 385
currently lose_sum: 98.96514642238617
time_elpased: 1.606
batch start
#iterations: 386
currently lose_sum: 98.45355224609375
time_elpased: 1.59
batch start
#iterations: 387
currently lose_sum: 98.33076399564743
time_elpased: 1.605
batch start
#iterations: 388
currently lose_sum: 98.61264669895172
time_elpased: 1.589
batch start
#iterations: 389
currently lose_sum: 98.7926869392395
time_elpased: 1.588
batch start
#iterations: 390
currently lose_sum: 98.63808351755142
time_elpased: 1.576
batch start
#iterations: 391
currently lose_sum: 98.23034811019897
time_elpased: 1.6
batch start
#iterations: 392
currently lose_sum: 98.28024798631668
time_elpased: 1.595
batch start
#iterations: 393
currently lose_sum: 98.36643344163895
time_elpased: 1.592
batch start
#iterations: 394
currently lose_sum: 98.62710213661194
time_elpased: 1.594
batch start
#iterations: 395
currently lose_sum: 98.6098957657814
time_elpased: 1.612
batch start
#iterations: 396
currently lose_sum: 98.31442618370056
time_elpased: 1.612
batch start
#iterations: 397
currently lose_sum: 98.50966912508011
time_elpased: 1.619
batch start
#iterations: 398
currently lose_sum: 98.5508941411972
time_elpased: 1.589
batch start
#iterations: 399
currently lose_sum: 98.43356901407242
time_elpased: 1.6
start validation test
0.626649484536
0.687765634487
0.466296181949
0.555780435449
0.626931009492
63.175
acc: 0.653
pre: 0.664
rec: 0.622
F1: 0.642
auc: 0.701
