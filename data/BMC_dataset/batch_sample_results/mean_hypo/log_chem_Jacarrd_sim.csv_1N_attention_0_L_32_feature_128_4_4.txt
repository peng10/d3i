start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.51334118843079
time_elpased: 1.968
batch start
#iterations: 1
currently lose_sum: 100.4405774474144
time_elpased: 1.977
batch start
#iterations: 2
currently lose_sum: 100.46077692508698
time_elpased: 1.987
batch start
#iterations: 3
currently lose_sum: 100.368683218956
time_elpased: 1.969
batch start
#iterations: 4
currently lose_sum: 100.41630053520203
time_elpased: 1.913
batch start
#iterations: 5
currently lose_sum: 100.36103010177612
time_elpased: 1.949
batch start
#iterations: 6
currently lose_sum: 100.3338828086853
time_elpased: 1.968
batch start
#iterations: 7
currently lose_sum: 100.24521458148956
time_elpased: 1.984
batch start
#iterations: 8
currently lose_sum: 100.43511575460434
time_elpased: 1.964
batch start
#iterations: 9
currently lose_sum: 100.30450385808945
time_elpased: 1.963
batch start
#iterations: 10
currently lose_sum: 100.22176039218903
time_elpased: 1.935
batch start
#iterations: 11
currently lose_sum: 100.24631559848785
time_elpased: 1.956
batch start
#iterations: 12
currently lose_sum: 100.18594694137573
time_elpased: 1.943
batch start
#iterations: 13
currently lose_sum: 100.16067188978195
time_elpased: 2.0
batch start
#iterations: 14
currently lose_sum: 100.07385802268982
time_elpased: 1.964
batch start
#iterations: 15
currently lose_sum: 100.0004660487175
time_elpased: 1.986
batch start
#iterations: 16
currently lose_sum: 99.97395747900009
time_elpased: 1.932
batch start
#iterations: 17
currently lose_sum: 100.00371438264847
time_elpased: 1.974
batch start
#iterations: 18
currently lose_sum: 99.92084640264511
time_elpased: 1.973
batch start
#iterations: 19
currently lose_sum: 100.0838805437088
time_elpased: 1.949
start validation test
0.591649484536
0.568007880579
0.771431511783
0.654272497163
0.591333849458
65.857
batch start
#iterations: 20
currently lose_sum: 100.09332770109177
time_elpased: 1.988
batch start
#iterations: 21
currently lose_sum: 99.77209335565567
time_elpased: 1.982
batch start
#iterations: 22
currently lose_sum: 99.53317242860794
time_elpased: 1.977
batch start
#iterations: 23
currently lose_sum: 99.94457602500916
time_elpased: 1.953
batch start
#iterations: 24
currently lose_sum: 100.33223283290863
time_elpased: 1.918
batch start
#iterations: 25
currently lose_sum: 99.7150526046753
time_elpased: 1.96
batch start
#iterations: 26
currently lose_sum: 99.88547343015671
time_elpased: 1.982
batch start
#iterations: 27
currently lose_sum: 100.22660565376282
time_elpased: 1.943
batch start
#iterations: 28
currently lose_sum: 99.8377537727356
time_elpased: 2.001
batch start
#iterations: 29
currently lose_sum: 100.01374530792236
time_elpased: 1.96
batch start
#iterations: 30
currently lose_sum: 99.46254819631577
time_elpased: 1.957
batch start
#iterations: 31
currently lose_sum: 100.29612863063812
time_elpased: 1.974
batch start
#iterations: 32
currently lose_sum: 99.74856913089752
time_elpased: 1.986
batch start
#iterations: 33
currently lose_sum: 99.50438356399536
time_elpased: 1.951
batch start
#iterations: 34
currently lose_sum: 99.85354048013687
time_elpased: 2.008
batch start
#iterations: 35
currently lose_sum: 100.03103756904602
time_elpased: 1.968
batch start
#iterations: 36
currently lose_sum: 100.09733664989471
time_elpased: 1.968
batch start
#iterations: 37
currently lose_sum: 100.34617775678635
time_elpased: 1.995
batch start
#iterations: 38
currently lose_sum: 99.53992754220963
time_elpased: 2.034
batch start
#iterations: 39
currently lose_sum: 99.6321993470192
time_elpased: 2.014
start validation test
0.600670103093
0.638693325824
0.466810744057
0.539389975623
0.600905113844
64.727
batch start
#iterations: 40
currently lose_sum: 100.02216523885727
time_elpased: 1.975
batch start
#iterations: 41
currently lose_sum: 100.49400496482849
time_elpased: 1.966
batch start
#iterations: 42
currently lose_sum: 100.20934921503067
time_elpased: 1.954
batch start
#iterations: 43
currently lose_sum: 99.61048740148544
time_elpased: 1.945
batch start
#iterations: 44
currently lose_sum: 100.11275988817215
time_elpased: 2.043
batch start
#iterations: 45
currently lose_sum: 99.83272391557693
time_elpased: 2.039
batch start
#iterations: 46
currently lose_sum: 100.51734858751297
time_elpased: 1.962
batch start
#iterations: 47
currently lose_sum: 99.72247296571732
time_elpased: 1.99
batch start
#iterations: 48
currently lose_sum: 99.49322897195816
time_elpased: 1.935
batch start
#iterations: 49
currently lose_sum: 100.16924780607224
time_elpased: 1.998
batch start
#iterations: 50
currently lose_sum: 100.49149185419083
time_elpased: 2.026
batch start
#iterations: 51
currently lose_sum: 100.2378243803978
time_elpased: 2.003
batch start
#iterations: 52
currently lose_sum: 99.76405721902847
time_elpased: 1.973
batch start
#iterations: 53
currently lose_sum: 99.74621397256851
time_elpased: 1.963
batch start
#iterations: 54
currently lose_sum: 99.76810216903687
time_elpased: 1.962
batch start
#iterations: 55
currently lose_sum: 99.69290882349014
time_elpased: 2.016
batch start
#iterations: 56
currently lose_sum: 99.89717668294907
time_elpased: 1.963
batch start
#iterations: 57
currently lose_sum: 99.83235746622086
time_elpased: 1.974
batch start
#iterations: 58
currently lose_sum: 99.52886259555817
time_elpased: 1.96
batch start
#iterations: 59
currently lose_sum: 99.8860936164856
time_elpased: 1.954
start validation test
0.49912371134
0.0
0.0
nan
0.5
67.264
acc: 0.593
pre: 0.629
rec: 0.459
F1: 0.531
auc: 0.594
