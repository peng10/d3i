start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 101.4107535481453
time_elpased: 1.345
batch start
#iterations: 1
currently lose_sum: 100.43655532598495
time_elpased: 1.328
batch start
#iterations: 2
currently lose_sum: 100.34719812870026
time_elpased: 1.328
batch start
#iterations: 3
currently lose_sum: 100.40854001045227
time_elpased: 1.334
batch start
#iterations: 4
currently lose_sum: 100.34379076957703
time_elpased: 1.339
batch start
#iterations: 5
currently lose_sum: 100.30252265930176
time_elpased: 1.359
batch start
#iterations: 6
currently lose_sum: 100.29448962211609
time_elpased: 1.334
batch start
#iterations: 7
currently lose_sum: 100.26749020814896
time_elpased: 1.346
batch start
#iterations: 8
currently lose_sum: 100.22514659166336
time_elpased: 1.326
batch start
#iterations: 9
currently lose_sum: 100.19864577054977
time_elpased: 1.318
batch start
#iterations: 10
currently lose_sum: 100.17423564195633
time_elpased: 1.331
batch start
#iterations: 11
currently lose_sum: 100.10534924268723
time_elpased: 1.344
batch start
#iterations: 12
currently lose_sum: 100.16705679893494
time_elpased: 1.317
batch start
#iterations: 13
currently lose_sum: 99.99681746959686
time_elpased: 1.319
batch start
#iterations: 14
currently lose_sum: 99.98467230796814
time_elpased: 1.332
batch start
#iterations: 15
currently lose_sum: 99.9209018945694
time_elpased: 1.325
batch start
#iterations: 16
currently lose_sum: 100.02884221076965
time_elpased: 1.339
batch start
#iterations: 17
currently lose_sum: 99.9867068529129
time_elpased: 1.324
batch start
#iterations: 18
currently lose_sum: 99.78369909524918
time_elpased: 1.315
batch start
#iterations: 19
currently lose_sum: 99.93107658624649
time_elpased: 1.346
start validation test
0.584072164948
0.602157203075
0.499845631368
0.546252038464
0.584220037619
65.973
batch start
#iterations: 20
currently lose_sum: 99.89230078458786
time_elpased: 1.316
batch start
#iterations: 21
currently lose_sum: 99.76498991250992
time_elpased: 1.315
batch start
#iterations: 22
currently lose_sum: 99.8110419511795
time_elpased: 1.346
batch start
#iterations: 23
currently lose_sum: 99.73561716079712
time_elpased: 1.316
batch start
#iterations: 24
currently lose_sum: 99.60952216386795
time_elpased: 1.328
batch start
#iterations: 25
currently lose_sum: 99.4183601140976
time_elpased: 1.332
batch start
#iterations: 26
currently lose_sum: 99.59234029054642
time_elpased: 1.325
batch start
#iterations: 27
currently lose_sum: 99.57420426607132
time_elpased: 1.339
batch start
#iterations: 28
currently lose_sum: 99.47733724117279
time_elpased: 1.318
batch start
#iterations: 29
currently lose_sum: 99.52420800924301
time_elpased: 1.333
batch start
#iterations: 30
currently lose_sum: 99.39368611574173
time_elpased: 1.332
batch start
#iterations: 31
currently lose_sum: 99.4812496304512
time_elpased: 1.319
batch start
#iterations: 32
currently lose_sum: 99.23826628923416
time_elpased: 1.348
batch start
#iterations: 33
currently lose_sum: 99.35627841949463
time_elpased: 1.324
batch start
#iterations: 34
currently lose_sum: 99.47379899024963
time_elpased: 1.33
batch start
#iterations: 35
currently lose_sum: 99.27386647462845
time_elpased: 1.318
batch start
#iterations: 36
currently lose_sum: 99.40100514888763
time_elpased: 1.321
batch start
#iterations: 37
currently lose_sum: 99.45453470945358
time_elpased: 1.363
batch start
#iterations: 38
currently lose_sum: 99.45362138748169
time_elpased: 1.334
batch start
#iterations: 39
currently lose_sum: 99.19059067964554
time_elpased: 1.319
start validation test
0.616855670103
0.652185501066
0.503653390964
0.568375820219
0.617054414164
64.786
batch start
#iterations: 40
currently lose_sum: 99.24181759357452
time_elpased: 1.386
batch start
#iterations: 41
currently lose_sum: 99.15494054555893
time_elpased: 1.327
batch start
#iterations: 42
currently lose_sum: 99.30725109577179
time_elpased: 1.361
batch start
#iterations: 43
currently lose_sum: 99.06089335680008
time_elpased: 1.315
batch start
#iterations: 44
currently lose_sum: 99.3873723745346
time_elpased: 1.326
batch start
#iterations: 45
currently lose_sum: 99.10232949256897
time_elpased: 1.364
batch start
#iterations: 46
currently lose_sum: 99.10775762796402
time_elpased: 1.323
batch start
#iterations: 47
currently lose_sum: 99.03552407026291
time_elpased: 1.328
batch start
#iterations: 48
currently lose_sum: 99.20988094806671
time_elpased: 1.318
batch start
#iterations: 49
currently lose_sum: 99.02330499887466
time_elpased: 1.335
batch start
#iterations: 50
currently lose_sum: 99.35881274938583
time_elpased: 1.343
batch start
#iterations: 51
currently lose_sum: 99.18127006292343
time_elpased: 1.364
batch start
#iterations: 52
currently lose_sum: 99.12304532527924
time_elpased: 1.341
batch start
#iterations: 53
currently lose_sum: 99.19757330417633
time_elpased: 1.355
batch start
#iterations: 54
currently lose_sum: 99.20103824138641
time_elpased: 1.303
batch start
#iterations: 55
currently lose_sum: 98.96446484327316
time_elpased: 1.335
batch start
#iterations: 56
currently lose_sum: 99.18625700473785
time_elpased: 1.335
batch start
#iterations: 57
currently lose_sum: 98.86902159452438
time_elpased: 1.314
batch start
#iterations: 58
currently lose_sum: 99.18347132205963
time_elpased: 1.329
batch start
#iterations: 59
currently lose_sum: 98.91649055480957
time_elpased: 1.345
start validation test
0.609329896907
0.666562792147
0.440259339302
0.530275797955
0.609626726348
64.410
batch start
#iterations: 60
currently lose_sum: 99.0619764328003
time_elpased: 1.316
batch start
#iterations: 61
currently lose_sum: 99.04900455474854
time_elpased: 1.332
batch start
#iterations: 62
currently lose_sum: 99.05960428714752
time_elpased: 1.354
batch start
#iterations: 63
currently lose_sum: 99.16814738512039
time_elpased: 1.352
batch start
#iterations: 64
currently lose_sum: 99.2662136554718
time_elpased: 1.319
batch start
#iterations: 65
currently lose_sum: 98.91035532951355
time_elpased: 1.313
batch start
#iterations: 66
currently lose_sum: 98.81280618906021
time_elpased: 1.334
batch start
#iterations: 67
currently lose_sum: 98.9881973862648
time_elpased: 1.316
batch start
#iterations: 68
currently lose_sum: 99.00144320726395
time_elpased: 1.334
batch start
#iterations: 69
currently lose_sum: 99.13043355941772
time_elpased: 1.37
batch start
#iterations: 70
currently lose_sum: 98.85376358032227
time_elpased: 1.327
batch start
#iterations: 71
currently lose_sum: 98.7745852470398
time_elpased: 1.335
batch start
#iterations: 72
currently lose_sum: 98.73431998491287
time_elpased: 1.351
batch start
#iterations: 73
currently lose_sum: 98.65644609928131
time_elpased: 1.317
batch start
#iterations: 74
currently lose_sum: 98.73801481723785
time_elpased: 1.35
batch start
#iterations: 75
currently lose_sum: 98.67977327108383
time_elpased: 1.333
batch start
#iterations: 76
currently lose_sum: 98.69750809669495
time_elpased: 1.327
batch start
#iterations: 77
currently lose_sum: 98.82997715473175
time_elpased: 1.344
batch start
#iterations: 78
currently lose_sum: 98.81853145360947
time_elpased: 1.339
batch start
#iterations: 79
currently lose_sum: 99.03873836994171
time_elpased: 1.318
start validation test
0.614793814433
0.661068044789
0.473911701142
0.552058982197
0.615041154712
64.308
batch start
#iterations: 80
currently lose_sum: 98.85469716787338
time_elpased: 1.375
batch start
#iterations: 81
currently lose_sum: 99.08048266172409
time_elpased: 1.327
batch start
#iterations: 82
currently lose_sum: 98.90780276060104
time_elpased: 1.332
batch start
#iterations: 83
currently lose_sum: 99.07482725381851
time_elpased: 1.333
batch start
#iterations: 84
currently lose_sum: 98.88964867591858
time_elpased: 1.305
batch start
#iterations: 85
currently lose_sum: 98.93567264080048
time_elpased: 1.314
batch start
#iterations: 86
currently lose_sum: 98.91057634353638
time_elpased: 1.32
batch start
#iterations: 87
currently lose_sum: 98.68593668937683
time_elpased: 1.343
batch start
#iterations: 88
currently lose_sum: 98.84065836668015
time_elpased: 1.326
batch start
#iterations: 89
currently lose_sum: 98.82449579238892
time_elpased: 1.353
batch start
#iterations: 90
currently lose_sum: 98.8357081413269
time_elpased: 1.342
batch start
#iterations: 91
currently lose_sum: 98.75298744440079
time_elpased: 1.351
batch start
#iterations: 92
currently lose_sum: 98.83459883928299
time_elpased: 1.315
batch start
#iterations: 93
currently lose_sum: 98.65962100028992
time_elpased: 1.337
batch start
#iterations: 94
currently lose_sum: 98.83043104410172
time_elpased: 1.329
batch start
#iterations: 95
currently lose_sum: 98.84784680604935
time_elpased: 1.361
batch start
#iterations: 96
currently lose_sum: 98.87406033277512
time_elpased: 1.337
batch start
#iterations: 97
currently lose_sum: 98.86427593231201
time_elpased: 1.373
batch start
#iterations: 98
currently lose_sum: 98.80081331729889
time_elpased: 1.336
batch start
#iterations: 99
currently lose_sum: 98.92981773614883
time_elpased: 1.341
start validation test
0.637680412371
0.641682479443
0.626427909849
0.633963443212
0.637700167875
63.993
batch start
#iterations: 100
currently lose_sum: 98.61501741409302
time_elpased: 1.327
batch start
#iterations: 101
currently lose_sum: 98.9880348443985
time_elpased: 1.345
batch start
#iterations: 102
currently lose_sum: 98.59244674444199
time_elpased: 1.34
batch start
#iterations: 103
currently lose_sum: 98.81838434934616
time_elpased: 1.334
batch start
#iterations: 104
currently lose_sum: 98.66017377376556
time_elpased: 1.337
batch start
#iterations: 105
currently lose_sum: 98.59169870615005
time_elpased: 1.328
batch start
#iterations: 106
currently lose_sum: 98.93996798992157
time_elpased: 1.334
batch start
#iterations: 107
currently lose_sum: 98.69607818126678
time_elpased: 1.341
batch start
#iterations: 108
currently lose_sum: 98.76952695846558
time_elpased: 1.351
batch start
#iterations: 109
currently lose_sum: 98.70794814825058
time_elpased: 1.401
batch start
#iterations: 110
currently lose_sum: 98.78051495552063
time_elpased: 1.336
batch start
#iterations: 111
currently lose_sum: 98.60597270727158
time_elpased: 1.347
batch start
#iterations: 112
currently lose_sum: 98.74184423685074
time_elpased: 1.353
batch start
#iterations: 113
currently lose_sum: 98.56076544523239
time_elpased: 1.362
batch start
#iterations: 114
currently lose_sum: 98.93038636445999
time_elpased: 1.311
batch start
#iterations: 115
currently lose_sum: 98.8314842581749
time_elpased: 1.324
batch start
#iterations: 116
currently lose_sum: 98.51446151733398
time_elpased: 1.314
batch start
#iterations: 117
currently lose_sum: 99.00015586614609
time_elpased: 1.358
batch start
#iterations: 118
currently lose_sum: 98.66890901327133
time_elpased: 1.326
batch start
#iterations: 119
currently lose_sum: 98.71622663736343
time_elpased: 1.358
start validation test
0.635618556701
0.626917177914
0.673047236801
0.64916373021
0.63555284488
63.784
batch start
#iterations: 120
currently lose_sum: 98.89674043655396
time_elpased: 1.328
batch start
#iterations: 121
currently lose_sum: 98.8170200586319
time_elpased: 1.35
batch start
#iterations: 122
currently lose_sum: 98.61654549837112
time_elpased: 1.349
batch start
#iterations: 123
currently lose_sum: 98.67516249418259
time_elpased: 1.346
batch start
#iterations: 124
currently lose_sum: 98.798368871212
time_elpased: 1.303
batch start
#iterations: 125
currently lose_sum: 98.83608543872833
time_elpased: 1.354
batch start
#iterations: 126
currently lose_sum: 98.73474699258804
time_elpased: 1.325
batch start
#iterations: 127
currently lose_sum: 98.75227218866348
time_elpased: 1.338
batch start
#iterations: 128
currently lose_sum: 98.85926032066345
time_elpased: 1.317
batch start
#iterations: 129
currently lose_sum: 98.75713729858398
time_elpased: 1.381
batch start
#iterations: 130
currently lose_sum: 98.7945711016655
time_elpased: 1.346
batch start
#iterations: 131
currently lose_sum: 98.63649702072144
time_elpased: 1.327
batch start
#iterations: 132
currently lose_sum: 98.63879030942917
time_elpased: 1.317
batch start
#iterations: 133
currently lose_sum: 98.7377946972847
time_elpased: 1.37
batch start
#iterations: 134
currently lose_sum: 98.66509747505188
time_elpased: 1.34
batch start
#iterations: 135
currently lose_sum: 98.85932183265686
time_elpased: 1.318
batch start
#iterations: 136
currently lose_sum: 99.02628147602081
time_elpased: 1.334
batch start
#iterations: 137
currently lose_sum: 98.60136926174164
time_elpased: 1.342
batch start
#iterations: 138
currently lose_sum: 98.60035771131516
time_elpased: 1.359
batch start
#iterations: 139
currently lose_sum: 98.78038680553436
time_elpased: 1.318
start validation test
0.657577319588
0.627403846154
0.778944118555
0.695009411873
0.657364241453
63.730
batch start
#iterations: 140
currently lose_sum: 98.63302195072174
time_elpased: 1.333
batch start
#iterations: 141
currently lose_sum: 98.82159614562988
time_elpased: 1.336
batch start
#iterations: 142
currently lose_sum: 98.56253492832184
time_elpased: 1.326
batch start
#iterations: 143
currently lose_sum: 98.74223625659943
time_elpased: 1.321
batch start
#iterations: 144
currently lose_sum: 98.7367342710495
time_elpased: 1.319
batch start
#iterations: 145
currently lose_sum: 98.60058891773224
time_elpased: 1.369
batch start
#iterations: 146
currently lose_sum: 98.76172667741776
time_elpased: 1.316
batch start
#iterations: 147
currently lose_sum: 98.880899310112
time_elpased: 1.331
batch start
#iterations: 148
currently lose_sum: 98.64381527900696
time_elpased: 1.318
batch start
#iterations: 149
currently lose_sum: 98.74841475486755
time_elpased: 1.326
batch start
#iterations: 150
currently lose_sum: 98.7343817949295
time_elpased: 1.314
batch start
#iterations: 151
currently lose_sum: 98.5738867521286
time_elpased: 1.356
batch start
#iterations: 152
currently lose_sum: 98.6551057100296
time_elpased: 1.341
batch start
#iterations: 153
currently lose_sum: 98.63737952709198
time_elpased: 1.37
batch start
#iterations: 154
currently lose_sum: 98.71048927307129
time_elpased: 1.33
batch start
#iterations: 155
currently lose_sum: 98.70051592588425
time_elpased: 1.356
batch start
#iterations: 156
currently lose_sum: 98.5556498169899
time_elpased: 1.325
batch start
#iterations: 157
currently lose_sum: 98.63734447956085
time_elpased: 1.346
batch start
#iterations: 158
currently lose_sum: 98.73727399110794
time_elpased: 1.359
batch start
#iterations: 159
currently lose_sum: 98.70436877012253
time_elpased: 1.33
start validation test
0.655463917526
0.631515046397
0.749408253576
0.685429216867
0.655298983754
63.726
batch start
#iterations: 160
currently lose_sum: 98.57287782430649
time_elpased: 1.349
batch start
#iterations: 161
currently lose_sum: 98.78852486610413
time_elpased: 1.318
batch start
#iterations: 162
currently lose_sum: 98.57718044519424
time_elpased: 1.334
batch start
#iterations: 163
currently lose_sum: 98.71259391307831
time_elpased: 1.332
batch start
#iterations: 164
currently lose_sum: 98.96714073419571
time_elpased: 1.356
batch start
#iterations: 165
currently lose_sum: 98.55732196569443
time_elpased: 1.343
batch start
#iterations: 166
currently lose_sum: 98.64895159006119
time_elpased: 1.325
batch start
#iterations: 167
currently lose_sum: 98.6866791844368
time_elpased: 1.373
batch start
#iterations: 168
currently lose_sum: 98.64559704065323
time_elpased: 1.318
batch start
#iterations: 169
currently lose_sum: 98.51681613922119
time_elpased: 1.363
batch start
#iterations: 170
currently lose_sum: 98.8028182387352
time_elpased: 1.313
batch start
#iterations: 171
currently lose_sum: 98.829006254673
time_elpased: 1.342
batch start
#iterations: 172
currently lose_sum: 98.55221170186996
time_elpased: 1.36
batch start
#iterations: 173
currently lose_sum: 98.78855413198471
time_elpased: 1.336
batch start
#iterations: 174
currently lose_sum: 98.36381459236145
time_elpased: 1.369
batch start
#iterations: 175
currently lose_sum: 98.74385917186737
time_elpased: 1.355
batch start
#iterations: 176
currently lose_sum: 98.68402290344238
time_elpased: 1.34
batch start
#iterations: 177
currently lose_sum: 98.79750782251358
time_elpased: 1.338
batch start
#iterations: 178
currently lose_sum: 98.75831055641174
time_elpased: 1.36
batch start
#iterations: 179
currently lose_sum: 98.39124763011932
time_elpased: 1.364
start validation test
0.650979381443
0.656802214179
0.634969640836
0.645701428497
0.651007489012
63.479
batch start
#iterations: 180
currently lose_sum: 98.75801020860672
time_elpased: 1.331
batch start
#iterations: 181
currently lose_sum: 98.51321691274643
time_elpased: 1.331
batch start
#iterations: 182
currently lose_sum: 98.59921908378601
time_elpased: 1.383
batch start
#iterations: 183
currently lose_sum: 98.39676821231842
time_elpased: 1.337
batch start
#iterations: 184
currently lose_sum: 98.36945015192032
time_elpased: 1.341
batch start
#iterations: 185
currently lose_sum: 98.64277935028076
time_elpased: 1.348
batch start
#iterations: 186
currently lose_sum: 98.43430268764496
time_elpased: 1.321
batch start
#iterations: 187
currently lose_sum: 98.61259388923645
time_elpased: 1.366
batch start
#iterations: 188
currently lose_sum: 98.40272855758667
time_elpased: 1.325
batch start
#iterations: 189
currently lose_sum: 98.62411916255951
time_elpased: 1.334
batch start
#iterations: 190
currently lose_sum: 98.51986593008041
time_elpased: 1.327
batch start
#iterations: 191
currently lose_sum: 98.69503831863403
time_elpased: 1.316
batch start
#iterations: 192
currently lose_sum: 98.64202237129211
time_elpased: 1.342
batch start
#iterations: 193
currently lose_sum: 98.55424457788467
time_elpased: 1.348
batch start
#iterations: 194
currently lose_sum: 98.51667708158493
time_elpased: 1.346
batch start
#iterations: 195
currently lose_sum: 98.6960175037384
time_elpased: 1.351
batch start
#iterations: 196
currently lose_sum: 98.54332613945007
time_elpased: 1.327
batch start
#iterations: 197
currently lose_sum: 98.80603712797165
time_elpased: 1.33
batch start
#iterations: 198
currently lose_sum: 98.45968014001846
time_elpased: 1.339
batch start
#iterations: 199
currently lose_sum: 98.94755548238754
time_elpased: 1.326
start validation test
0.594948453608
0.67197039778
0.373777914994
0.480359740775
0.595336752602
63.929
batch start
#iterations: 200
currently lose_sum: 98.80615067481995
time_elpased: 1.36
batch start
#iterations: 201
currently lose_sum: 98.60679507255554
time_elpased: 1.351
batch start
#iterations: 202
currently lose_sum: 98.6688266992569
time_elpased: 1.325
batch start
#iterations: 203
currently lose_sum: 98.66049635410309
time_elpased: 1.324
batch start
#iterations: 204
currently lose_sum: 98.71265864372253
time_elpased: 1.344
batch start
#iterations: 205
currently lose_sum: 98.53747171163559
time_elpased: 1.336
batch start
#iterations: 206
currently lose_sum: 98.6223995089531
time_elpased: 1.337
batch start
#iterations: 207
currently lose_sum: 98.53829908370972
time_elpased: 1.329
batch start
#iterations: 208
currently lose_sum: 98.4699359536171
time_elpased: 1.407
batch start
#iterations: 209
currently lose_sum: 98.58958441019058
time_elpased: 1.346
batch start
#iterations: 210
currently lose_sum: 98.60127711296082
time_elpased: 1.314
batch start
#iterations: 211
currently lose_sum: 98.42829900979996
time_elpased: 1.356
batch start
#iterations: 212
currently lose_sum: 98.34739869832993
time_elpased: 1.328
batch start
#iterations: 213
currently lose_sum: 98.32844316959381
time_elpased: 1.332
batch start
#iterations: 214
currently lose_sum: 98.43874341249466
time_elpased: 1.332
batch start
#iterations: 215
currently lose_sum: 98.50532776117325
time_elpased: 1.333
batch start
#iterations: 216
currently lose_sum: 98.54103231430054
time_elpased: 1.385
batch start
#iterations: 217
currently lose_sum: 98.60871428251266
time_elpased: 1.33
batch start
#iterations: 218
currently lose_sum: 98.53593075275421
time_elpased: 1.338
batch start
#iterations: 219
currently lose_sum: 98.78576612472534
time_elpased: 1.359
start validation test
0.640309278351
0.671595038216
0.551610579397
0.605718160244
0.640465002597
63.445
batch start
#iterations: 220
currently lose_sum: 98.86535120010376
time_elpased: 1.343
batch start
#iterations: 221
currently lose_sum: 98.71164882183075
time_elpased: 1.345
batch start
#iterations: 222
currently lose_sum: 98.57981377840042
time_elpased: 1.339
batch start
#iterations: 223
currently lose_sum: 98.63867145776749
time_elpased: 1.338
batch start
#iterations: 224
currently lose_sum: 98.52028596401215
time_elpased: 1.349
batch start
#iterations: 225
currently lose_sum: 98.36672788858414
time_elpased: 1.355
batch start
#iterations: 226
currently lose_sum: 98.73866349458694
time_elpased: 1.317
batch start
#iterations: 227
currently lose_sum: 98.88249951601028
time_elpased: 1.374
batch start
#iterations: 228
currently lose_sum: 98.45309263467789
time_elpased: 1.33
batch start
#iterations: 229
currently lose_sum: 98.50315344333649
time_elpased: 1.322
batch start
#iterations: 230
currently lose_sum: 98.70701789855957
time_elpased: 1.365
batch start
#iterations: 231
currently lose_sum: 98.4083394408226
time_elpased: 1.372
batch start
#iterations: 232
currently lose_sum: 98.52792936563492
time_elpased: 1.366
batch start
#iterations: 233
currently lose_sum: 98.66008186340332
time_elpased: 1.38
batch start
#iterations: 234
currently lose_sum: 98.75461840629578
time_elpased: 1.383
batch start
#iterations: 235
currently lose_sum: 98.38651770353317
time_elpased: 1.39
batch start
#iterations: 236
currently lose_sum: 98.42697042226791
time_elpased: 1.404
batch start
#iterations: 237
currently lose_sum: 98.70762026309967
time_elpased: 1.394
batch start
#iterations: 238
currently lose_sum: 98.68434524536133
time_elpased: 1.43
batch start
#iterations: 239
currently lose_sum: 98.47387933731079
time_elpased: 1.382
start validation test
0.608711340206
0.672845528455
0.425851600288
0.521585680973
0.609032378684
63.655
batch start
#iterations: 240
currently lose_sum: 98.51143580675125
time_elpased: 1.324
batch start
#iterations: 241
currently lose_sum: 98.65248960256577
time_elpased: 1.377
batch start
#iterations: 242
currently lose_sum: 98.57286810874939
time_elpased: 1.335
batch start
#iterations: 243
currently lose_sum: 98.36597156524658
time_elpased: 1.348
batch start
#iterations: 244
currently lose_sum: 98.64951312541962
time_elpased: 1.335
batch start
#iterations: 245
currently lose_sum: 98.68756854534149
time_elpased: 1.332
batch start
#iterations: 246
currently lose_sum: 98.6623398065567
time_elpased: 1.326
batch start
#iterations: 247
currently lose_sum: 98.58596080541611
time_elpased: 1.326
batch start
#iterations: 248
currently lose_sum: 98.58967047929764
time_elpased: 1.336
batch start
#iterations: 249
currently lose_sum: 98.64754778146744
time_elpased: 1.419
batch start
#iterations: 250
currently lose_sum: 98.75884908437729
time_elpased: 1.347
batch start
#iterations: 251
currently lose_sum: 98.54218590259552
time_elpased: 1.353
batch start
#iterations: 252
currently lose_sum: 98.57943320274353
time_elpased: 1.359
batch start
#iterations: 253
currently lose_sum: 98.63211268186569
time_elpased: 1.362
batch start
#iterations: 254
currently lose_sum: 98.81248015165329
time_elpased: 1.41
batch start
#iterations: 255
currently lose_sum: 98.28358173370361
time_elpased: 1.359
batch start
#iterations: 256
currently lose_sum: 98.46826189756393
time_elpased: 1.346
batch start
#iterations: 257
currently lose_sum: 98.51381182670593
time_elpased: 1.333
batch start
#iterations: 258
currently lose_sum: 98.63563793897629
time_elpased: 1.341
batch start
#iterations: 259
currently lose_sum: 98.63186651468277
time_elpased: 1.385
start validation test
0.639381443299
0.66978659678
0.552330966348
0.605414551607
0.639534273838
63.444
batch start
#iterations: 260
currently lose_sum: 98.34678941965103
time_elpased: 1.359
batch start
#iterations: 261
currently lose_sum: 98.84909081459045
time_elpased: 1.349
batch start
#iterations: 262
currently lose_sum: 98.54645758867264
time_elpased: 1.336
batch start
#iterations: 263
currently lose_sum: 98.60847789049149
time_elpased: 1.334
batch start
#iterations: 264
currently lose_sum: 98.75235217809677
time_elpased: 1.378
batch start
#iterations: 265
currently lose_sum: 98.45343625545502
time_elpased: 1.35
batch start
#iterations: 266
currently lose_sum: 98.61158394813538
time_elpased: 1.386
batch start
#iterations: 267
currently lose_sum: 98.48074060678482
time_elpased: 1.335
batch start
#iterations: 268
currently lose_sum: 98.45971596240997
time_elpased: 1.345
batch start
#iterations: 269
currently lose_sum: 98.66779720783234
time_elpased: 1.35
batch start
#iterations: 270
currently lose_sum: 98.71709191799164
time_elpased: 1.347
batch start
#iterations: 271
currently lose_sum: 98.48353886604309
time_elpased: 1.39
batch start
#iterations: 272
currently lose_sum: 98.55051082372665
time_elpased: 1.383
batch start
#iterations: 273
currently lose_sum: 98.61014568805695
time_elpased: 1.409
batch start
#iterations: 274
currently lose_sum: 98.22570711374283
time_elpased: 1.358
batch start
#iterations: 275
currently lose_sum: 98.65811997652054
time_elpased: 1.377
batch start
#iterations: 276
currently lose_sum: 98.77970188856125
time_elpased: 1.374
batch start
#iterations: 277
currently lose_sum: 98.70925450325012
time_elpased: 1.379
batch start
#iterations: 278
currently lose_sum: 98.6002944111824
time_elpased: 1.337
batch start
#iterations: 279
currently lose_sum: 98.56292581558228
time_elpased: 1.364
start validation test
0.657783505155
0.633524206143
0.751466502007
0.687473520689
0.657619030204
63.513
batch start
#iterations: 280
currently lose_sum: 98.7458365559578
time_elpased: 1.34
batch start
#iterations: 281
currently lose_sum: 98.58341652154922
time_elpased: 1.334
batch start
#iterations: 282
currently lose_sum: 98.82980954647064
time_elpased: 1.345
batch start
#iterations: 283
currently lose_sum: 98.74888664484024
time_elpased: 1.351
batch start
#iterations: 284
currently lose_sum: 98.34421074390411
time_elpased: 1.328
batch start
#iterations: 285
currently lose_sum: 98.55646622180939
time_elpased: 1.344
batch start
#iterations: 286
currently lose_sum: 98.7157352566719
time_elpased: 1.338
batch start
#iterations: 287
currently lose_sum: 98.5368173122406
time_elpased: 1.372
batch start
#iterations: 288
currently lose_sum: 98.27921098470688
time_elpased: 1.336
batch start
#iterations: 289
currently lose_sum: 98.5869500041008
time_elpased: 1.343
batch start
#iterations: 290
currently lose_sum: 98.61596500873566
time_elpased: 1.38
batch start
#iterations: 291
currently lose_sum: 98.44882643222809
time_elpased: 1.333
batch start
#iterations: 292
currently lose_sum: 98.12554973363876
time_elpased: 1.344
batch start
#iterations: 293
currently lose_sum: 98.54431581497192
time_elpased: 1.357
batch start
#iterations: 294
currently lose_sum: 98.47314709424973
time_elpased: 1.322
batch start
#iterations: 295
currently lose_sum: 98.45321244001389
time_elpased: 1.364
batch start
#iterations: 296
currently lose_sum: 98.3551657795906
time_elpased: 1.397
batch start
#iterations: 297
currently lose_sum: 98.38804876804352
time_elpased: 1.337
batch start
#iterations: 298
currently lose_sum: 98.66707319021225
time_elpased: 1.336
batch start
#iterations: 299
currently lose_sum: 98.51298177242279
time_elpased: 1.349
start validation test
0.650412371134
0.671858531444
0.590408562313
0.628505696757
0.650517717075
63.161
batch start
#iterations: 300
currently lose_sum: 98.77906119823456
time_elpased: 1.338
batch start
#iterations: 301
currently lose_sum: 98.44854283332825
time_elpased: 1.329
batch start
#iterations: 302
currently lose_sum: 98.68763780593872
time_elpased: 1.338
batch start
#iterations: 303
currently lose_sum: 98.54297602176666
time_elpased: 1.335
batch start
#iterations: 304
currently lose_sum: 98.32518821954727
time_elpased: 1.418
batch start
#iterations: 305
currently lose_sum: 98.63390696048737
time_elpased: 1.375
batch start
#iterations: 306
currently lose_sum: 98.70069229602814
time_elpased: 1.352
batch start
#iterations: 307
currently lose_sum: 98.45914721488953
time_elpased: 1.347
batch start
#iterations: 308
currently lose_sum: 98.4626629948616
time_elpased: 1.407
batch start
#iterations: 309
currently lose_sum: 98.58653622865677
time_elpased: 1.388
batch start
#iterations: 310
currently lose_sum: 98.42599362134933
time_elpased: 1.337
batch start
#iterations: 311
currently lose_sum: 98.48428559303284
time_elpased: 1.36
batch start
#iterations: 312
currently lose_sum: 98.72825556993484
time_elpased: 1.364
batch start
#iterations: 313
currently lose_sum: 98.21129131317139
time_elpased: 1.345
batch start
#iterations: 314
currently lose_sum: 98.61027973890305
time_elpased: 1.323
batch start
#iterations: 315
currently lose_sum: 98.53684222698212
time_elpased: 1.352
batch start
#iterations: 316
currently lose_sum: 98.71076238155365
time_elpased: 1.353
batch start
#iterations: 317
currently lose_sum: 98.59889686107635
time_elpased: 1.334
batch start
#iterations: 318
currently lose_sum: 98.50929743051529
time_elpased: 1.335
batch start
#iterations: 319
currently lose_sum: 98.49540758132935
time_elpased: 1.365
start validation test
0.621134020619
0.681323732189
0.457651538541
0.547525240089
0.621421039331
63.501
batch start
#iterations: 320
currently lose_sum: 98.42516613006592
time_elpased: 1.348
batch start
#iterations: 321
currently lose_sum: 98.37124317884445
time_elpased: 1.352
batch start
#iterations: 322
currently lose_sum: 98.75633066892624
time_elpased: 1.406
batch start
#iterations: 323
currently lose_sum: 98.53415912389755
time_elpased: 1.345
batch start
#iterations: 324
currently lose_sum: 98.76831465959549
time_elpased: 1.347
batch start
#iterations: 325
currently lose_sum: 98.71225643157959
time_elpased: 1.348
batch start
#iterations: 326
currently lose_sum: 98.47570276260376
time_elpased: 1.376
batch start
#iterations: 327
currently lose_sum: 98.48426467180252
time_elpased: 1.333
batch start
#iterations: 328
currently lose_sum: 98.4900683760643
time_elpased: 1.361
batch start
#iterations: 329
currently lose_sum: 98.63595521450043
time_elpased: 1.354
batch start
#iterations: 330
currently lose_sum: 98.54964751005173
time_elpased: 1.36
batch start
#iterations: 331
currently lose_sum: 98.44681286811829
time_elpased: 1.351
batch start
#iterations: 332
currently lose_sum: 98.38181805610657
time_elpased: 1.403
batch start
#iterations: 333
currently lose_sum: 98.62068420648575
time_elpased: 1.356
batch start
#iterations: 334
currently lose_sum: 98.56899243593216
time_elpased: 1.35
batch start
#iterations: 335
currently lose_sum: 98.42280846834183
time_elpased: 1.344
batch start
#iterations: 336
currently lose_sum: 98.43641698360443
time_elpased: 1.366
batch start
#iterations: 337
currently lose_sum: 98.64357006549835
time_elpased: 1.332
batch start
#iterations: 338
currently lose_sum: 98.24711585044861
time_elpased: 1.357
batch start
#iterations: 339
currently lose_sum: 98.42705804109573
time_elpased: 1.371
start validation test
0.635360824742
0.67894380501
0.516002881548
0.586364167934
0.635570376021
63.226
batch start
#iterations: 340
currently lose_sum: 98.38234031200409
time_elpased: 1.362
batch start
#iterations: 341
currently lose_sum: 98.54269808530807
time_elpased: 1.351
batch start
#iterations: 342
currently lose_sum: 98.5835971236229
time_elpased: 1.349
batch start
#iterations: 343
currently lose_sum: 98.35729956626892
time_elpased: 1.384
batch start
#iterations: 344
currently lose_sum: 98.31184875965118
time_elpased: 1.333
batch start
#iterations: 345
currently lose_sum: 98.6322233080864
time_elpased: 1.373
batch start
#iterations: 346
currently lose_sum: 98.45180839300156
time_elpased: 1.378
batch start
#iterations: 347
currently lose_sum: 98.4843590259552
time_elpased: 1.341
batch start
#iterations: 348
currently lose_sum: 98.45410597324371
time_elpased: 1.371
batch start
#iterations: 349
currently lose_sum: 98.63639998435974
time_elpased: 1.37
batch start
#iterations: 350
currently lose_sum: 98.49644964933395
time_elpased: 1.331
batch start
#iterations: 351
currently lose_sum: 98.41170692443848
time_elpased: 1.348
batch start
#iterations: 352
currently lose_sum: 98.40177565813065
time_elpased: 1.369
batch start
#iterations: 353
currently lose_sum: 98.6400277018547
time_elpased: 1.356
batch start
#iterations: 354
currently lose_sum: 98.61946135759354
time_elpased: 1.352
batch start
#iterations: 355
currently lose_sum: 98.65707015991211
time_elpased: 1.374
batch start
#iterations: 356
currently lose_sum: 98.54561454057693
time_elpased: 1.356
batch start
#iterations: 357
currently lose_sum: 98.35585856437683
time_elpased: 1.389
batch start
#iterations: 358
currently lose_sum: 98.51227360963821
time_elpased: 1.335
batch start
#iterations: 359
currently lose_sum: 98.36660069227219
time_elpased: 1.343
start validation test
0.640463917526
0.657985710993
0.587629926932
0.620820875238
0.640556675745
63.536
batch start
#iterations: 360
currently lose_sum: 98.40914136171341
time_elpased: 1.366
batch start
#iterations: 361
currently lose_sum: 98.66551798582077
time_elpased: 1.365
batch start
#iterations: 362
currently lose_sum: 98.36926060914993
time_elpased: 1.413
batch start
#iterations: 363
currently lose_sum: 98.48325490951538
time_elpased: 1.353
batch start
#iterations: 364
currently lose_sum: 98.71396428346634
time_elpased: 1.396
batch start
#iterations: 365
currently lose_sum: 98.5943363904953
time_elpased: 1.368
batch start
#iterations: 366
currently lose_sum: 98.62774765491486
time_elpased: 1.316
batch start
#iterations: 367
currently lose_sum: 98.73651593923569
time_elpased: 1.335
batch start
#iterations: 368
currently lose_sum: 98.58445209264755
time_elpased: 1.315
batch start
#iterations: 369
currently lose_sum: 98.61365711688995
time_elpased: 1.333
batch start
#iterations: 370
currently lose_sum: 98.36217761039734
time_elpased: 1.328
batch start
#iterations: 371
currently lose_sum: 98.39225113391876
time_elpased: 1.365
batch start
#iterations: 372
currently lose_sum: 98.47767955064774
time_elpased: 1.352
batch start
#iterations: 373
currently lose_sum: 98.57483804225922
time_elpased: 1.331
batch start
#iterations: 374
currently lose_sum: 98.30962973833084
time_elpased: 1.389
batch start
#iterations: 375
currently lose_sum: 98.56753259897232
time_elpased: 1.35
batch start
#iterations: 376
currently lose_sum: 98.42683273553848
time_elpased: 1.381
batch start
#iterations: 377
currently lose_sum: 98.39422005414963
time_elpased: 1.363
batch start
#iterations: 378
currently lose_sum: 98.61643654108047
time_elpased: 1.351
batch start
#iterations: 379
currently lose_sum: 98.47329521179199
time_elpased: 1.356
start validation test
0.644896907216
0.670608108108
0.57198723886
0.617384059983
0.645024911385
63.307
batch start
#iterations: 380
currently lose_sum: 98.61157768964767
time_elpased: 1.354
batch start
#iterations: 381
currently lose_sum: 98.52380710840225
time_elpased: 1.355
batch start
#iterations: 382
currently lose_sum: 98.495290517807
time_elpased: 1.357
batch start
#iterations: 383
currently lose_sum: 98.27400839328766
time_elpased: 1.334
batch start
#iterations: 384
currently lose_sum: 98.60454481840134
time_elpased: 1.343
batch start
#iterations: 385
currently lose_sum: 98.54901921749115
time_elpased: 1.329
batch start
#iterations: 386
currently lose_sum: 98.5163311958313
time_elpased: 1.337
batch start
#iterations: 387
currently lose_sum: 98.51850092411041
time_elpased: 1.337
batch start
#iterations: 388
currently lose_sum: 98.35706448554993
time_elpased: 1.347
batch start
#iterations: 389
currently lose_sum: 98.595583319664
time_elpased: 1.341
batch start
#iterations: 390
currently lose_sum: 98.60668790340424
time_elpased: 1.358
batch start
#iterations: 391
currently lose_sum: 98.60707151889801
time_elpased: 1.33
batch start
#iterations: 392
currently lose_sum: 98.5548187494278
time_elpased: 1.349
batch start
#iterations: 393
currently lose_sum: 98.49094778299332
time_elpased: 1.345
batch start
#iterations: 394
currently lose_sum: 98.42725133895874
time_elpased: 1.349
batch start
#iterations: 395
currently lose_sum: 98.17903411388397
time_elpased: 1.321
batch start
#iterations: 396
currently lose_sum: 98.51970100402832
time_elpased: 1.357
batch start
#iterations: 397
currently lose_sum: 98.26548999547958
time_elpased: 1.333
batch start
#iterations: 398
currently lose_sum: 98.20291304588318
time_elpased: 1.331
batch start
#iterations: 399
currently lose_sum: 98.57783943414688
time_elpased: 1.331
start validation test
0.621391752577
0.677971188475
0.464958320469
0.551614675539
0.621666395596
63.352
acc: 0.650
pre: 0.670
rec: 0.593
F1: 0.629
auc: 0.706
