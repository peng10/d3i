start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 101.46146959066391
time_elpased: 1.329
batch start
#iterations: 1
currently lose_sum: 100.42368990182877
time_elpased: 1.289
batch start
#iterations: 2
currently lose_sum: 100.3562114238739
time_elpased: 1.299
batch start
#iterations: 3
currently lose_sum: 100.32411885261536
time_elpased: 1.297
batch start
#iterations: 4
currently lose_sum: 100.36108583211899
time_elpased: 1.301
batch start
#iterations: 5
currently lose_sum: 100.34105974435806
time_elpased: 1.31
batch start
#iterations: 6
currently lose_sum: 100.27208179235458
time_elpased: 1.305
batch start
#iterations: 7
currently lose_sum: 100.25637429952621
time_elpased: 1.29
batch start
#iterations: 8
currently lose_sum: 100.25993430614471
time_elpased: 1.322
batch start
#iterations: 9
currently lose_sum: 100.23967111110687
time_elpased: 1.294
batch start
#iterations: 10
currently lose_sum: 100.23489612340927
time_elpased: 1.326
batch start
#iterations: 11
currently lose_sum: 100.18883001804352
time_elpased: 1.369
batch start
#iterations: 12
currently lose_sum: 100.162233710289
time_elpased: 1.305
batch start
#iterations: 13
currently lose_sum: 100.10132712125778
time_elpased: 1.299
batch start
#iterations: 14
currently lose_sum: 100.05122941732407
time_elpased: 1.296
batch start
#iterations: 15
currently lose_sum: 100.01197850704193
time_elpased: 1.311
batch start
#iterations: 16
currently lose_sum: 100.0525034070015
time_elpased: 1.286
batch start
#iterations: 17
currently lose_sum: 99.98140120506287
time_elpased: 1.312
batch start
#iterations: 18
currently lose_sum: 99.83638972043991
time_elpased: 1.31
batch start
#iterations: 19
currently lose_sum: 99.99605917930603
time_elpased: 1.295
start validation test
0.595567010309
0.609402409075
0.536276628589
0.570505802496
0.595671103719
65.965
batch start
#iterations: 20
currently lose_sum: 99.91025602817535
time_elpased: 1.294
batch start
#iterations: 21
currently lose_sum: 99.68763655424118
time_elpased: 1.338
batch start
#iterations: 22
currently lose_sum: 99.68721061944962
time_elpased: 1.302
batch start
#iterations: 23
currently lose_sum: 99.81231641769409
time_elpased: 1.292
batch start
#iterations: 24
currently lose_sum: 99.74175000190735
time_elpased: 1.325
batch start
#iterations: 25
currently lose_sum: 99.57228457927704
time_elpased: 1.305
batch start
#iterations: 26
currently lose_sum: 99.72595119476318
time_elpased: 1.293
batch start
#iterations: 27
currently lose_sum: 99.44097942113876
time_elpased: 1.295
batch start
#iterations: 28
currently lose_sum: 99.59637022018433
time_elpased: 1.285
batch start
#iterations: 29
currently lose_sum: 99.5258966088295
time_elpased: 1.341
batch start
#iterations: 30
currently lose_sum: 99.48367285728455
time_elpased: 1.303
batch start
#iterations: 31
currently lose_sum: 99.32793164253235
time_elpased: 1.294
batch start
#iterations: 32
currently lose_sum: 99.43465173244476
time_elpased: 1.294
batch start
#iterations: 33
currently lose_sum: 99.41452515125275
time_elpased: 1.338
batch start
#iterations: 34
currently lose_sum: 99.47718489170074
time_elpased: 1.314
batch start
#iterations: 35
currently lose_sum: 99.27971994876862
time_elpased: 1.314
batch start
#iterations: 36
currently lose_sum: 99.32352352142334
time_elpased: 1.337
batch start
#iterations: 37
currently lose_sum: 99.33455657958984
time_elpased: 1.294
batch start
#iterations: 38
currently lose_sum: 99.16853553056717
time_elpased: 1.301
batch start
#iterations: 39
currently lose_sum: 98.9397434592247
time_elpased: 1.31
start validation test
0.617371134021
0.583369675825
0.825975095194
0.683791267306
0.617004897592
64.730
batch start
#iterations: 40
currently lose_sum: 99.2864660024643
time_elpased: 1.329
batch start
#iterations: 41
currently lose_sum: 99.1903760433197
time_elpased: 1.317
batch start
#iterations: 42
currently lose_sum: 99.09964543581009
time_elpased: 1.314
batch start
#iterations: 43
currently lose_sum: 99.32720464468002
time_elpased: 1.299
batch start
#iterations: 44
currently lose_sum: 99.14702796936035
time_elpased: 1.302
batch start
#iterations: 45
currently lose_sum: 99.252938747406
time_elpased: 1.311
batch start
#iterations: 46
currently lose_sum: 99.14186936616898
time_elpased: 1.303
batch start
#iterations: 47
currently lose_sum: 99.084696829319
time_elpased: 1.329
batch start
#iterations: 48
currently lose_sum: 99.06845593452454
time_elpased: 1.316
batch start
#iterations: 49
currently lose_sum: 99.18161982297897
time_elpased: 1.296
batch start
#iterations: 50
currently lose_sum: 99.04041302204132
time_elpased: 1.316
batch start
#iterations: 51
currently lose_sum: 99.04143130779266
time_elpased: 1.331
batch start
#iterations: 52
currently lose_sum: 99.15944194793701
time_elpased: 1.3
batch start
#iterations: 53
currently lose_sum: 98.97780376672745
time_elpased: 1.308
batch start
#iterations: 54
currently lose_sum: 99.07099014520645
time_elpased: 1.313
batch start
#iterations: 55
currently lose_sum: 99.06293398141861
time_elpased: 1.305
batch start
#iterations: 56
currently lose_sum: 98.99926686286926
time_elpased: 1.306
batch start
#iterations: 57
currently lose_sum: 99.01704460382462
time_elpased: 1.306
batch start
#iterations: 58
currently lose_sum: 98.789803981781
time_elpased: 1.297
batch start
#iterations: 59
currently lose_sum: 99.06259047985077
time_elpased: 1.304
start validation test
0.584639175258
0.664877757901
0.344242050015
0.453620829943
0.58506122949
64.800
batch start
#iterations: 60
currently lose_sum: 99.32672023773193
time_elpased: 1.314
batch start
#iterations: 61
currently lose_sum: 99.1363776922226
time_elpased: 1.294
batch start
#iterations: 62
currently lose_sum: 99.02717214822769
time_elpased: 1.318
batch start
#iterations: 63
currently lose_sum: 99.06581580638885
time_elpased: 1.314
batch start
#iterations: 64
currently lose_sum: 99.16585397720337
time_elpased: 1.283
batch start
#iterations: 65
currently lose_sum: 99.01340997219086
time_elpased: 1.292
batch start
#iterations: 66
currently lose_sum: 98.81886917352676
time_elpased: 1.314
batch start
#iterations: 67
currently lose_sum: 98.85164308547974
time_elpased: 1.281
batch start
#iterations: 68
currently lose_sum: 98.91402035951614
time_elpased: 1.283
batch start
#iterations: 69
currently lose_sum: 99.18220180273056
time_elpased: 1.334
batch start
#iterations: 70
currently lose_sum: 99.11850100755692
time_elpased: 1.314
batch start
#iterations: 71
currently lose_sum: 99.07946568727493
time_elpased: 1.298
batch start
#iterations: 72
currently lose_sum: 99.08200627565384
time_elpased: 1.303
batch start
#iterations: 73
currently lose_sum: 98.92198902368546
time_elpased: 1.308
batch start
#iterations: 74
currently lose_sum: 98.74342238903046
time_elpased: 1.303
batch start
#iterations: 75
currently lose_sum: 98.93024927377701
time_elpased: 1.307
batch start
#iterations: 76
currently lose_sum: 98.85862278938293
time_elpased: 1.293
batch start
#iterations: 77
currently lose_sum: 99.03524434566498
time_elpased: 1.289
batch start
#iterations: 78
currently lose_sum: 98.94840860366821
time_elpased: 1.308
batch start
#iterations: 79
currently lose_sum: 99.06990319490433
time_elpased: 1.305
start validation test
0.650567010309
0.651412079984
0.650406504065
0.650908903651
0.650567292103
63.773
batch start
#iterations: 80
currently lose_sum: 98.88514518737793
time_elpased: 1.311
batch start
#iterations: 81
currently lose_sum: 98.8625220656395
time_elpased: 1.318
batch start
#iterations: 82
currently lose_sum: 98.84417426586151
time_elpased: 1.285
batch start
#iterations: 83
currently lose_sum: 98.84927594661713
time_elpased: 1.291
batch start
#iterations: 84
currently lose_sum: 98.83988434076309
time_elpased: 1.323
batch start
#iterations: 85
currently lose_sum: 98.96076393127441
time_elpased: 1.311
batch start
#iterations: 86
currently lose_sum: 99.01099109649658
time_elpased: 1.339
batch start
#iterations: 87
currently lose_sum: 98.76222902536392
time_elpased: 1.3
batch start
#iterations: 88
currently lose_sum: 98.87855875492096
time_elpased: 1.347
batch start
#iterations: 89
currently lose_sum: 99.02597641944885
time_elpased: 1.298
batch start
#iterations: 90
currently lose_sum: 98.76926946640015
time_elpased: 1.318
batch start
#iterations: 91
currently lose_sum: 99.07778543233871
time_elpased: 1.305
batch start
#iterations: 92
currently lose_sum: 98.81157237291336
time_elpased: 1.299
batch start
#iterations: 93
currently lose_sum: 98.71452617645264
time_elpased: 1.306
batch start
#iterations: 94
currently lose_sum: 99.00972014665604
time_elpased: 1.288
batch start
#iterations: 95
currently lose_sum: 99.00715500116348
time_elpased: 1.282
batch start
#iterations: 96
currently lose_sum: 99.04155820608139
time_elpased: 1.288
batch start
#iterations: 97
currently lose_sum: 98.94535201787949
time_elpased: 1.293
batch start
#iterations: 98
currently lose_sum: 99.16063112020493
time_elpased: 1.289
batch start
#iterations: 99
currently lose_sum: 98.9314957857132
time_elpased: 1.326
start validation test
0.567783505155
0.674528301887
0.264896573016
0.380403458213
0.568315269881
64.697
batch start
#iterations: 100
currently lose_sum: 98.93873858451843
time_elpased: 1.296
batch start
#iterations: 101
currently lose_sum: 98.86058247089386
time_elpased: 1.299
batch start
#iterations: 102
currently lose_sum: 98.88166338205338
time_elpased: 1.308
batch start
#iterations: 103
currently lose_sum: 98.82115244865417
time_elpased: 1.334
batch start
#iterations: 104
currently lose_sum: 98.81515806913376
time_elpased: 1.297
batch start
#iterations: 105
currently lose_sum: 98.79313611984253
time_elpased: 1.287
batch start
#iterations: 106
currently lose_sum: 98.82947111129761
time_elpased: 1.284
batch start
#iterations: 107
currently lose_sum: 98.79423624277115
time_elpased: 1.294
batch start
#iterations: 108
currently lose_sum: 98.84044080972672
time_elpased: 1.296
batch start
#iterations: 109
currently lose_sum: 98.88372433185577
time_elpased: 1.297
batch start
#iterations: 110
currently lose_sum: 98.83598375320435
time_elpased: 1.312
batch start
#iterations: 111
currently lose_sum: 98.94846135377884
time_elpased: 1.338
batch start
#iterations: 112
currently lose_sum: 98.81668537855148
time_elpased: 1.318
batch start
#iterations: 113
currently lose_sum: 98.54703032970428
time_elpased: 1.293
batch start
#iterations: 114
currently lose_sum: 98.86886578798294
time_elpased: 1.294
batch start
#iterations: 115
currently lose_sum: 98.86704856157303
time_elpased: 1.31
batch start
#iterations: 116
currently lose_sum: 98.9245742559433
time_elpased: 1.308
batch start
#iterations: 117
currently lose_sum: 98.68710738420486
time_elpased: 1.312
batch start
#iterations: 118
currently lose_sum: 98.65961301326752
time_elpased: 1.297
batch start
#iterations: 119
currently lose_sum: 98.77201926708221
time_elpased: 1.31
start validation test
0.618865979381
0.649812975622
0.518472779665
0.576760160275
0.619042235128
63.919
batch start
#iterations: 120
currently lose_sum: 98.74919694662094
time_elpased: 1.298
batch start
#iterations: 121
currently lose_sum: 98.93830215930939
time_elpased: 1.307
batch start
#iterations: 122
currently lose_sum: 98.73132336139679
time_elpased: 1.298
batch start
#iterations: 123
currently lose_sum: 98.7299639582634
time_elpased: 1.302
batch start
#iterations: 124
currently lose_sum: 98.8170103430748
time_elpased: 1.297
batch start
#iterations: 125
currently lose_sum: 98.83851301670074
time_elpased: 1.314
batch start
#iterations: 126
currently lose_sum: 98.84216439723969
time_elpased: 1.345
batch start
#iterations: 127
currently lose_sum: 99.09000730514526
time_elpased: 1.319
batch start
#iterations: 128
currently lose_sum: 98.56715697050095
time_elpased: 1.303
batch start
#iterations: 129
currently lose_sum: 98.55406069755554
time_elpased: 1.325
batch start
#iterations: 130
currently lose_sum: 98.85536164045334
time_elpased: 1.289
batch start
#iterations: 131
currently lose_sum: 98.93720322847366
time_elpased: 1.3
batch start
#iterations: 132
currently lose_sum: 98.79057943820953
time_elpased: 1.33
batch start
#iterations: 133
currently lose_sum: 98.84376156330109
time_elpased: 1.299
batch start
#iterations: 134
currently lose_sum: 98.69592469930649
time_elpased: 1.323
batch start
#iterations: 135
currently lose_sum: 98.94439774751663
time_elpased: 1.302
batch start
#iterations: 136
currently lose_sum: 98.7366179227829
time_elpased: 1.318
batch start
#iterations: 137
currently lose_sum: 98.83320838212967
time_elpased: 1.299
batch start
#iterations: 138
currently lose_sum: 98.95758384466171
time_elpased: 1.294
batch start
#iterations: 139
currently lose_sum: 98.71216148138046
time_elpased: 1.299
start validation test
0.659381443299
0.644591200819
0.713183081198
0.677154582763
0.659286986225
63.418
batch start
#iterations: 140
currently lose_sum: 98.85672730207443
time_elpased: 1.286
batch start
#iterations: 141
currently lose_sum: 98.71310329437256
time_elpased: 1.282
batch start
#iterations: 142
currently lose_sum: 98.97438383102417
time_elpased: 1.297
batch start
#iterations: 143
currently lose_sum: 98.71568822860718
time_elpased: 1.308
batch start
#iterations: 144
currently lose_sum: 98.81218993663788
time_elpased: 1.298
batch start
#iterations: 145
currently lose_sum: 99.05546748638153
time_elpased: 1.306
batch start
#iterations: 146
currently lose_sum: 98.51097202301025
time_elpased: 1.309
batch start
#iterations: 147
currently lose_sum: 98.75096482038498
time_elpased: 1.305
batch start
#iterations: 148
currently lose_sum: 98.86986511945724
time_elpased: 1.351
batch start
#iterations: 149
currently lose_sum: 98.78450852632523
time_elpased: 1.323
batch start
#iterations: 150
currently lose_sum: 98.57426232099533
time_elpased: 1.288
batch start
#iterations: 151
currently lose_sum: 98.71815025806427
time_elpased: 1.316
batch start
#iterations: 152
currently lose_sum: 98.62848174571991
time_elpased: 1.292
batch start
#iterations: 153
currently lose_sum: 98.87405526638031
time_elpased: 1.334
batch start
#iterations: 154
currently lose_sum: 98.65853255987167
time_elpased: 1.299
batch start
#iterations: 155
currently lose_sum: 98.76558667421341
time_elpased: 1.302
batch start
#iterations: 156
currently lose_sum: 98.91118854284286
time_elpased: 1.308
batch start
#iterations: 157
currently lose_sum: 98.59002166986465
time_elpased: 1.31
batch start
#iterations: 158
currently lose_sum: 98.95666074752808
time_elpased: 1.328
batch start
#iterations: 159
currently lose_sum: 98.74688476324081
time_elpased: 1.31
start validation test
0.595721649485
0.679777436685
0.364618709478
0.474646660861
0.596127386341
63.857
batch start
#iterations: 160
currently lose_sum: 98.82892763614655
time_elpased: 1.296
batch start
#iterations: 161
currently lose_sum: 98.65086221694946
time_elpased: 1.322
batch start
#iterations: 162
currently lose_sum: 98.76576286554337
time_elpased: 1.304
batch start
#iterations: 163
currently lose_sum: 98.65548944473267
time_elpased: 1.286
batch start
#iterations: 164
currently lose_sum: 98.82244008779526
time_elpased: 1.311
batch start
#iterations: 165
currently lose_sum: 98.6730785369873
time_elpased: 1.3
batch start
#iterations: 166
currently lose_sum: 98.75326812267303
time_elpased: 1.293
batch start
#iterations: 167
currently lose_sum: 98.96280920505524
time_elpased: 1.315
batch start
#iterations: 168
currently lose_sum: 98.87504875659943
time_elpased: 1.288
batch start
#iterations: 169
currently lose_sum: 98.85426020622253
time_elpased: 1.317
batch start
#iterations: 170
currently lose_sum: 98.3783295750618
time_elpased: 1.295
batch start
#iterations: 171
currently lose_sum: 98.8833801150322
time_elpased: 1.314
batch start
#iterations: 172
currently lose_sum: 98.69214278459549
time_elpased: 1.29
batch start
#iterations: 173
currently lose_sum: 98.71459627151489
time_elpased: 1.305
batch start
#iterations: 174
currently lose_sum: 98.50278383493423
time_elpased: 1.327
batch start
#iterations: 175
currently lose_sum: 98.73409873247147
time_elpased: 1.305
batch start
#iterations: 176
currently lose_sum: 98.79756915569305
time_elpased: 1.284
batch start
#iterations: 177
currently lose_sum: 98.72178637981415
time_elpased: 1.3
batch start
#iterations: 178
currently lose_sum: 98.63885229825974
time_elpased: 1.323
batch start
#iterations: 179
currently lose_sum: 98.6324508190155
time_elpased: 1.304
start validation test
0.637886597938
0.674307174307
0.535864978903
0.597167268765
0.638065712626
63.376
batch start
#iterations: 180
currently lose_sum: 98.72878301143646
time_elpased: 1.31
batch start
#iterations: 181
currently lose_sum: 98.84452188014984
time_elpased: 1.318
batch start
#iterations: 182
currently lose_sum: 98.7621294260025
time_elpased: 1.316
batch start
#iterations: 183
currently lose_sum: 98.72358649969101
time_elpased: 1.313
batch start
#iterations: 184
currently lose_sum: 98.57091236114502
time_elpased: 1.301
batch start
#iterations: 185
currently lose_sum: 98.57452255487442
time_elpased: 1.306
batch start
#iterations: 186
currently lose_sum: 98.5610664486885
time_elpased: 1.324
batch start
#iterations: 187
currently lose_sum: 98.70721185207367
time_elpased: 1.312
batch start
#iterations: 188
currently lose_sum: 98.73146855831146
time_elpased: 1.306
batch start
#iterations: 189
currently lose_sum: 98.81651717424393
time_elpased: 1.293
batch start
#iterations: 190
currently lose_sum: 98.63991242647171
time_elpased: 1.281
batch start
#iterations: 191
currently lose_sum: 98.50456911325455
time_elpased: 1.331
batch start
#iterations: 192
currently lose_sum: 98.69175028800964
time_elpased: 1.305
batch start
#iterations: 193
currently lose_sum: 98.67061573266983
time_elpased: 1.297
batch start
#iterations: 194
currently lose_sum: 98.58512657880783
time_elpased: 1.311
batch start
#iterations: 195
currently lose_sum: 98.70613700151443
time_elpased: 1.3
batch start
#iterations: 196
currently lose_sum: 98.61728751659393
time_elpased: 1.29
batch start
#iterations: 197
currently lose_sum: 98.85900098085403
time_elpased: 1.312
batch start
#iterations: 198
currently lose_sum: 98.54380631446838
time_elpased: 1.285
batch start
#iterations: 199
currently lose_sum: 98.47805452346802
time_elpased: 1.323
start validation test
0.626237113402
0.67283431455
0.493979623341
0.569699127648
0.626469311825
63.543
batch start
#iterations: 200
currently lose_sum: 98.56079602241516
time_elpased: 1.293
batch start
#iterations: 201
currently lose_sum: 98.60680812597275
time_elpased: 1.301
batch start
#iterations: 202
currently lose_sum: 98.5862307548523
time_elpased: 1.332
batch start
#iterations: 203
currently lose_sum: 98.71203875541687
time_elpased: 1.318
batch start
#iterations: 204
currently lose_sum: 98.78210097551346
time_elpased: 1.291
batch start
#iterations: 205
currently lose_sum: 98.76399785280228
time_elpased: 1.308
batch start
#iterations: 206
currently lose_sum: 98.59106475114822
time_elpased: 1.288
batch start
#iterations: 207
currently lose_sum: 98.5033266544342
time_elpased: 1.306
batch start
#iterations: 208
currently lose_sum: 98.52503788471222
time_elpased: 1.297
batch start
#iterations: 209
currently lose_sum: 98.56954276561737
time_elpased: 1.311
batch start
#iterations: 210
currently lose_sum: 98.49360865354538
time_elpased: 1.288
batch start
#iterations: 211
currently lose_sum: 98.57360994815826
time_elpased: 1.299
batch start
#iterations: 212
currently lose_sum: 98.48891872167587
time_elpased: 1.299
batch start
#iterations: 213
currently lose_sum: 98.63134491443634
time_elpased: 1.323
batch start
#iterations: 214
currently lose_sum: 98.8368392586708
time_elpased: 1.302
batch start
#iterations: 215
currently lose_sum: 98.26432937383652
time_elpased: 1.298
batch start
#iterations: 216
currently lose_sum: 98.62763780355453
time_elpased: 1.31
batch start
#iterations: 217
currently lose_sum: 98.66553211212158
time_elpased: 1.31
batch start
#iterations: 218
currently lose_sum: 98.61417543888092
time_elpased: 1.349
batch start
#iterations: 219
currently lose_sum: 98.65261334180832
time_elpased: 1.331
start validation test
0.640618556701
0.670942832233
0.554389214778
0.607122731883
0.640769945611
63.347
batch start
#iterations: 220
currently lose_sum: 98.72507095336914
time_elpased: 1.347
batch start
#iterations: 221
currently lose_sum: 98.70079380273819
time_elpased: 1.313
batch start
#iterations: 222
currently lose_sum: 98.56579726934433
time_elpased: 1.32
batch start
#iterations: 223
currently lose_sum: 98.79771810770035
time_elpased: 1.372
batch start
#iterations: 224
currently lose_sum: 98.65901350975037
time_elpased: 1.282
batch start
#iterations: 225
currently lose_sum: 98.56561177968979
time_elpased: 1.328
batch start
#iterations: 226
currently lose_sum: 98.43096786737442
time_elpased: 1.308
batch start
#iterations: 227
currently lose_sum: 98.57159852981567
time_elpased: 1.3
batch start
#iterations: 228
currently lose_sum: 98.86321693658829
time_elpased: 1.304
batch start
#iterations: 229
currently lose_sum: 98.6612839102745
time_elpased: 1.302
batch start
#iterations: 230
currently lose_sum: 98.55541110038757
time_elpased: 1.301
batch start
#iterations: 231
currently lose_sum: 98.78188562393188
time_elpased: 1.303
batch start
#iterations: 232
currently lose_sum: 98.70592480897903
time_elpased: 1.297
batch start
#iterations: 233
currently lose_sum: 98.65454089641571
time_elpased: 1.313
batch start
#iterations: 234
currently lose_sum: 98.61640322208405
time_elpased: 1.294
batch start
#iterations: 235
currently lose_sum: 98.69241446256638
time_elpased: 1.295
batch start
#iterations: 236
currently lose_sum: 98.38868224620819
time_elpased: 1.319
batch start
#iterations: 237
currently lose_sum: 98.7525115609169
time_elpased: 1.297
batch start
#iterations: 238
currently lose_sum: 98.5193783044815
time_elpased: 1.307
batch start
#iterations: 239
currently lose_sum: 98.58538907766342
time_elpased: 1.295
start validation test
0.626288659794
0.679261735213
0.481012658228
0.563200385589
0.626543714222
63.555
batch start
#iterations: 240
currently lose_sum: 98.75129890441895
time_elpased: 1.294
batch start
#iterations: 241
currently lose_sum: 98.79287928342819
time_elpased: 1.302
batch start
#iterations: 242
currently lose_sum: 98.47631126642227
time_elpased: 1.286
batch start
#iterations: 243
currently lose_sum: 98.72544610500336
time_elpased: 1.302
batch start
#iterations: 244
currently lose_sum: 98.65944421291351
time_elpased: 1.317
batch start
#iterations: 245
currently lose_sum: 98.5929611325264
time_elpased: 1.324
batch start
#iterations: 246
currently lose_sum: 98.81478530168533
time_elpased: 1.283
batch start
#iterations: 247
currently lose_sum: 98.80001449584961
time_elpased: 1.338
batch start
#iterations: 248
currently lose_sum: 98.63640177249908
time_elpased: 1.295
batch start
#iterations: 249
currently lose_sum: 98.57127058506012
time_elpased: 1.32
batch start
#iterations: 250
currently lose_sum: 98.67582434415817
time_elpased: 1.289
batch start
#iterations: 251
currently lose_sum: 98.65628236532211
time_elpased: 1.316
batch start
#iterations: 252
currently lose_sum: 98.58892065286636
time_elpased: 1.32
batch start
#iterations: 253
currently lose_sum: 98.61232304573059
time_elpased: 1.304
batch start
#iterations: 254
currently lose_sum: 98.71761870384216
time_elpased: 1.317
batch start
#iterations: 255
currently lose_sum: 98.63565629720688
time_elpased: 1.302
batch start
#iterations: 256
currently lose_sum: 98.24524420499802
time_elpased: 1.29
batch start
#iterations: 257
currently lose_sum: 98.69712507724762
time_elpased: 1.34
batch start
#iterations: 258
currently lose_sum: 98.40401446819305
time_elpased: 1.299
batch start
#iterations: 259
currently lose_sum: 98.50429850816727
time_elpased: 1.304
start validation test
0.645360824742
0.679035718793
0.553668827828
0.609977324263
0.645521804185
63.268
batch start
#iterations: 260
currently lose_sum: 98.63516926765442
time_elpased: 1.309
batch start
#iterations: 261
currently lose_sum: 98.49727034568787
time_elpased: 1.336
batch start
#iterations: 262
currently lose_sum: 98.53901928663254
time_elpased: 1.293
batch start
#iterations: 263
currently lose_sum: 98.74590867757797
time_elpased: 1.303
batch start
#iterations: 264
currently lose_sum: 98.66048282384872
time_elpased: 1.3
batch start
#iterations: 265
currently lose_sum: 98.6012020111084
time_elpased: 1.291
batch start
#iterations: 266
currently lose_sum: 98.67535322904587
time_elpased: 1.304
batch start
#iterations: 267
currently lose_sum: 98.66826891899109
time_elpased: 1.31
batch start
#iterations: 268
currently lose_sum: 98.73211658000946
time_elpased: 1.307
batch start
#iterations: 269
currently lose_sum: 98.66687381267548
time_elpased: 1.3
batch start
#iterations: 270
currently lose_sum: 98.62142193317413
time_elpased: 1.304
batch start
#iterations: 271
currently lose_sum: 98.437156021595
time_elpased: 1.309
batch start
#iterations: 272
currently lose_sum: 98.81135320663452
time_elpased: 1.294
batch start
#iterations: 273
currently lose_sum: 98.49302661418915
time_elpased: 1.295
batch start
#iterations: 274
currently lose_sum: 98.77629429101944
time_elpased: 1.303
batch start
#iterations: 275
currently lose_sum: 98.59840106964111
time_elpased: 1.307
batch start
#iterations: 276
currently lose_sum: 98.60026097297668
time_elpased: 1.321
batch start
#iterations: 277
currently lose_sum: 98.83300638198853
time_elpased: 1.323
batch start
#iterations: 278
currently lose_sum: 98.73271977901459
time_elpased: 1.298
batch start
#iterations: 279
currently lose_sum: 98.4184136390686
time_elpased: 1.326
start validation test
0.616340206186
0.681571382945
0.439230215087
0.534201138995
0.616651150092
63.517
batch start
#iterations: 280
currently lose_sum: 98.68586331605911
time_elpased: 1.302
batch start
#iterations: 281
currently lose_sum: 98.4831935763359
time_elpased: 1.311
batch start
#iterations: 282
currently lose_sum: 98.64415830373764
time_elpased: 1.287
batch start
#iterations: 283
currently lose_sum: 98.64313685894012
time_elpased: 1.31
batch start
#iterations: 284
currently lose_sum: 98.35909152030945
time_elpased: 1.323
batch start
#iterations: 285
currently lose_sum: 98.52095001935959
time_elpased: 1.324
batch start
#iterations: 286
currently lose_sum: 98.51073390245438
time_elpased: 1.289
batch start
#iterations: 287
currently lose_sum: 98.53626620769501
time_elpased: 1.285
batch start
#iterations: 288
currently lose_sum: 98.550352871418
time_elpased: 1.299
batch start
#iterations: 289
currently lose_sum: 98.54720175266266
time_elpased: 1.323
batch start
#iterations: 290
currently lose_sum: 98.57590740919113
time_elpased: 1.293
batch start
#iterations: 291
currently lose_sum: 98.49972361326218
time_elpased: 1.325
batch start
#iterations: 292
currently lose_sum: 98.62680160999298
time_elpased: 1.301
batch start
#iterations: 293
currently lose_sum: 98.44313633441925
time_elpased: 1.318
batch start
#iterations: 294
currently lose_sum: 98.45421648025513
time_elpased: 1.302
batch start
#iterations: 295
currently lose_sum: 98.58116352558136
time_elpased: 1.295
batch start
#iterations: 296
currently lose_sum: 98.47963309288025
time_elpased: 1.349
batch start
#iterations: 297
currently lose_sum: 98.85981512069702
time_elpased: 1.296
batch start
#iterations: 298
currently lose_sum: 98.56335812807083
time_elpased: 1.302
batch start
#iterations: 299
currently lose_sum: 98.65038168430328
time_elpased: 1.281
start validation test
0.651082474227
0.654377880184
0.642996809715
0.648637425383
0.651096669858
63.224
batch start
#iterations: 300
currently lose_sum: 98.5523008108139
time_elpased: 1.302
batch start
#iterations: 301
currently lose_sum: 98.60136717557907
time_elpased: 1.333
batch start
#iterations: 302
currently lose_sum: 98.59225219488144
time_elpased: 1.305
batch start
#iterations: 303
currently lose_sum: 98.57373464107513
time_elpased: 1.301
batch start
#iterations: 304
currently lose_sum: 98.68578815460205
time_elpased: 1.314
batch start
#iterations: 305
currently lose_sum: 98.3390041589737
time_elpased: 1.29
batch start
#iterations: 306
currently lose_sum: 98.50321400165558
time_elpased: 1.323
batch start
#iterations: 307
currently lose_sum: 98.47897231578827
time_elpased: 1.315
batch start
#iterations: 308
currently lose_sum: 98.79547846317291
time_elpased: 1.314
batch start
#iterations: 309
currently lose_sum: 98.66469937562943
time_elpased: 1.316
batch start
#iterations: 310
currently lose_sum: 98.73606342077255
time_elpased: 1.306
batch start
#iterations: 311
currently lose_sum: 98.63567054271698
time_elpased: 1.309
batch start
#iterations: 312
currently lose_sum: 98.6680297255516
time_elpased: 1.305
batch start
#iterations: 313
currently lose_sum: 98.60815685987473
time_elpased: 1.322
batch start
#iterations: 314
currently lose_sum: 98.42557018995285
time_elpased: 1.295
batch start
#iterations: 315
currently lose_sum: 98.50421893596649
time_elpased: 1.294
batch start
#iterations: 316
currently lose_sum: 98.3982412815094
time_elpased: 1.303
batch start
#iterations: 317
currently lose_sum: 98.72082090377808
time_elpased: 1.32
batch start
#iterations: 318
currently lose_sum: 98.5820484161377
time_elpased: 1.304
batch start
#iterations: 319
currently lose_sum: 98.40854668617249
time_elpased: 1.306
start validation test
0.631546391753
0.677736266777
0.50406504065
0.578139754485
0.631770204927
63.265
batch start
#iterations: 320
currently lose_sum: 98.61253219842911
time_elpased: 1.318
batch start
#iterations: 321
currently lose_sum: 98.30740869045258
time_elpased: 1.33
batch start
#iterations: 322
currently lose_sum: 98.6570548415184
time_elpased: 1.313
batch start
#iterations: 323
currently lose_sum: 98.5590535402298
time_elpased: 1.334
batch start
#iterations: 324
currently lose_sum: 98.48884445428848
time_elpased: 1.293
batch start
#iterations: 325
currently lose_sum: 98.63987356424332
time_elpased: 1.307
batch start
#iterations: 326
currently lose_sum: 98.78551256656647
time_elpased: 1.31
batch start
#iterations: 327
currently lose_sum: 98.50804138183594
time_elpased: 1.325
batch start
#iterations: 328
currently lose_sum: 98.50951063632965
time_elpased: 1.305
batch start
#iterations: 329
currently lose_sum: 98.53035908937454
time_elpased: 1.304
batch start
#iterations: 330
currently lose_sum: 98.50308591127396
time_elpased: 1.311
batch start
#iterations: 331
currently lose_sum: 98.72110253572464
time_elpased: 1.324
batch start
#iterations: 332
currently lose_sum: 98.54519015550613
time_elpased: 1.317
batch start
#iterations: 333
currently lose_sum: 98.68454122543335
time_elpased: 1.31
batch start
#iterations: 334
currently lose_sum: 98.82310938835144
time_elpased: 1.336
batch start
#iterations: 335
currently lose_sum: 98.50678408145905
time_elpased: 1.335
batch start
#iterations: 336
currently lose_sum: 98.59630125761032
time_elpased: 1.315
batch start
#iterations: 337
currently lose_sum: 98.54644876718521
time_elpased: 1.296
batch start
#iterations: 338
currently lose_sum: 98.3712785243988
time_elpased: 1.324
batch start
#iterations: 339
currently lose_sum: 98.49701875448227
time_elpased: 1.335
start validation test
0.628969072165
0.682245695268
0.485232067511
0.567115708444
0.629221424647
63.278
batch start
#iterations: 340
currently lose_sum: 98.65467220544815
time_elpased: 1.299
batch start
#iterations: 341
currently lose_sum: 98.7221354842186
time_elpased: 1.332
batch start
#iterations: 342
currently lose_sum: 98.5784969329834
time_elpased: 1.327
batch start
#iterations: 343
currently lose_sum: 98.54714214801788
time_elpased: 1.293
batch start
#iterations: 344
currently lose_sum: 98.55720084905624
time_elpased: 1.321
batch start
#iterations: 345
currently lose_sum: 98.46080565452576
time_elpased: 1.328
batch start
#iterations: 346
currently lose_sum: 98.55172753334045
time_elpased: 1.318
batch start
#iterations: 347
currently lose_sum: 98.30533140897751
time_elpased: 1.307
batch start
#iterations: 348
currently lose_sum: 98.73321813344955
time_elpased: 1.301
batch start
#iterations: 349
currently lose_sum: 98.46870285272598
time_elpased: 1.313
batch start
#iterations: 350
currently lose_sum: 98.5640172958374
time_elpased: 1.314
batch start
#iterations: 351
currently lose_sum: 98.5261994600296
time_elpased: 1.293
batch start
#iterations: 352
currently lose_sum: 98.26090204715729
time_elpased: 1.346
batch start
#iterations: 353
currently lose_sum: 98.4311358332634
time_elpased: 1.317
batch start
#iterations: 354
currently lose_sum: 98.52134996652603
time_elpased: 1.334
batch start
#iterations: 355
currently lose_sum: 98.50241738557816
time_elpased: 1.352
batch start
#iterations: 356
currently lose_sum: 98.57613182067871
time_elpased: 1.316
batch start
#iterations: 357
currently lose_sum: 98.60255986452103
time_elpased: 1.312
batch start
#iterations: 358
currently lose_sum: 98.37022912502289
time_elpased: 1.309
batch start
#iterations: 359
currently lose_sum: 98.28977036476135
time_elpased: 1.312
start validation test
0.645979381443
0.66756852135
0.584027992179
0.623010209683
0.646088146662
63.132
batch start
#iterations: 360
currently lose_sum: 98.45164197683334
time_elpased: 1.342
batch start
#iterations: 361
currently lose_sum: 98.52806597948074
time_elpased: 1.301
batch start
#iterations: 362
currently lose_sum: 98.62926173210144
time_elpased: 1.314
batch start
#iterations: 363
currently lose_sum: 98.5107529759407
time_elpased: 1.302
batch start
#iterations: 364
currently lose_sum: 98.6519455909729
time_elpased: 1.314
batch start
#iterations: 365
currently lose_sum: 98.58357828855515
time_elpased: 1.336
batch start
#iterations: 366
currently lose_sum: 98.54150623083115
time_elpased: 1.318
batch start
#iterations: 367
currently lose_sum: 98.47210139036179
time_elpased: 1.307
batch start
#iterations: 368
currently lose_sum: 98.50255799293518
time_elpased: 1.31
batch start
#iterations: 369
currently lose_sum: 98.722727060318
time_elpased: 1.288
batch start
#iterations: 370
currently lose_sum: 98.57233130931854
time_elpased: 1.337
batch start
#iterations: 371
currently lose_sum: 98.49411404132843
time_elpased: 1.304
batch start
#iterations: 372
currently lose_sum: 98.51118052005768
time_elpased: 1.344
batch start
#iterations: 373
currently lose_sum: 98.78492480516434
time_elpased: 1.312
batch start
#iterations: 374
currently lose_sum: 98.5167191028595
time_elpased: 1.315
batch start
#iterations: 375
currently lose_sum: 98.38513821363449
time_elpased: 1.301
batch start
#iterations: 376
currently lose_sum: 98.53202039003372
time_elpased: 1.313
batch start
#iterations: 377
currently lose_sum: 98.56510508060455
time_elpased: 1.306
batch start
#iterations: 378
currently lose_sum: 98.56426507234573
time_elpased: 1.316
batch start
#iterations: 379
currently lose_sum: 98.56566888093948
time_elpased: 1.339
start validation test
0.635567010309
0.672577911071
0.530822270248
0.593350972046
0.635750905856
63.373
batch start
#iterations: 380
currently lose_sum: 98.72803717851639
time_elpased: 1.286
batch start
#iterations: 381
currently lose_sum: 98.54464811086655
time_elpased: 1.312
batch start
#iterations: 382
currently lose_sum: 98.40679579973221
time_elpased: 1.311
batch start
#iterations: 383
currently lose_sum: 98.18387573957443
time_elpased: 1.325
batch start
#iterations: 384
currently lose_sum: 98.40040147304535
time_elpased: 1.293
batch start
#iterations: 385
currently lose_sum: 98.76957380771637
time_elpased: 1.299
batch start
#iterations: 386
currently lose_sum: 98.56633293628693
time_elpased: 1.327
batch start
#iterations: 387
currently lose_sum: 98.52142679691315
time_elpased: 1.317
batch start
#iterations: 388
currently lose_sum: 98.92247098684311
time_elpased: 1.315
batch start
#iterations: 389
currently lose_sum: 98.44598466157913
time_elpased: 1.345
batch start
#iterations: 390
currently lose_sum: 98.41402852535248
time_elpased: 1.337
batch start
#iterations: 391
currently lose_sum: 98.46684789657593
time_elpased: 1.304
batch start
#iterations: 392
currently lose_sum: 98.60544675588608
time_elpased: 1.305
batch start
#iterations: 393
currently lose_sum: 98.54178595542908
time_elpased: 1.305
batch start
#iterations: 394
currently lose_sum: 98.27030742168427
time_elpased: 1.292
batch start
#iterations: 395
currently lose_sum: 98.50836032629013
time_elpased: 1.302
batch start
#iterations: 396
currently lose_sum: 98.19988268613815
time_elpased: 1.314
batch start
#iterations: 397
currently lose_sum: 98.70780658721924
time_elpased: 1.347
batch start
#iterations: 398
currently lose_sum: 98.3576927781105
time_elpased: 1.307
batch start
#iterations: 399
currently lose_sum: 98.40011638402939
time_elpased: 1.294
start validation test
0.66087628866
0.663403457613
0.655552125142
0.659454423107
0.66088563605
63.046
acc: 0.654
pre: 0.657
rec: 0.646
F1: 0.651
auc: 0.699
