start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.500050842762
time_elpased: 1.529
batch start
#iterations: 1
currently lose_sum: 100.4427900314331
time_elpased: 1.548
batch start
#iterations: 2
currently lose_sum: 100.40645974874496
time_elpased: 1.56
batch start
#iterations: 3
currently lose_sum: 100.3369368314743
time_elpased: 1.527
batch start
#iterations: 4
currently lose_sum: 100.39040851593018
time_elpased: 1.517
batch start
#iterations: 5
currently lose_sum: 100.37993377447128
time_elpased: 1.515
batch start
#iterations: 6
currently lose_sum: 100.2966765165329
time_elpased: 1.519
batch start
#iterations: 7
currently lose_sum: 100.22613215446472
time_elpased: 1.492
batch start
#iterations: 8
currently lose_sum: 100.27596002817154
time_elpased: 1.56
batch start
#iterations: 9
currently lose_sum: 100.20106250047684
time_elpased: 1.54
batch start
#iterations: 10
currently lose_sum: 100.14225852489471
time_elpased: 1.578
batch start
#iterations: 11
currently lose_sum: 100.1185410618782
time_elpased: 1.534
batch start
#iterations: 12
currently lose_sum: 100.10802680253983
time_elpased: 1.522
batch start
#iterations: 13
currently lose_sum: 100.04610562324524
time_elpased: 1.554
batch start
#iterations: 14
currently lose_sum: 99.89521604776382
time_elpased: 1.522
batch start
#iterations: 15
currently lose_sum: 99.86447089910507
time_elpased: 1.506
batch start
#iterations: 16
currently lose_sum: 99.94216680526733
time_elpased: 1.533
batch start
#iterations: 17
currently lose_sum: 99.88532584905624
time_elpased: 1.532
batch start
#iterations: 18
currently lose_sum: 99.78546631336212
time_elpased: 1.511
batch start
#iterations: 19
currently lose_sum: 99.7445108294487
time_elpased: 1.542
start validation test
0.603608247423
0.578268592169
0.770608212411
0.66072531545
0.603315053226
64.994
batch start
#iterations: 20
currently lose_sum: 99.70434123277664
time_elpased: 1.516
batch start
#iterations: 21
currently lose_sum: 99.5952097773552
time_elpased: 1.592
batch start
#iterations: 22
currently lose_sum: 99.51884329319
time_elpased: 1.528
batch start
#iterations: 23
currently lose_sum: 99.94312298297882
time_elpased: 1.542
batch start
#iterations: 24
currently lose_sum: 99.77676981687546
time_elpased: 1.497
batch start
#iterations: 25
currently lose_sum: 99.54219037294388
time_elpased: 1.518
batch start
#iterations: 26
currently lose_sum: 99.5878256559372
time_elpased: 1.512
batch start
#iterations: 27
currently lose_sum: 100.23094910383224
time_elpased: 1.525
batch start
#iterations: 28
currently lose_sum: 99.74630641937256
time_elpased: 1.513
batch start
#iterations: 29
currently lose_sum: 99.39906966686249
time_elpased: 1.511
batch start
#iterations: 30
currently lose_sum: 99.70206254720688
time_elpased: 1.517
batch start
#iterations: 31
currently lose_sum: 99.81764030456543
time_elpased: 1.535
batch start
#iterations: 32
currently lose_sum: 99.41360980272293
time_elpased: 1.511
batch start
#iterations: 33
currently lose_sum: 99.58916866779327
time_elpased: 1.53
batch start
#iterations: 34
currently lose_sum: 99.759885430336
time_elpased: 1.549
batch start
#iterations: 35
currently lose_sum: 99.63195866346359
time_elpased: 1.523
batch start
#iterations: 36
currently lose_sum: 100.13693857192993
time_elpased: 1.512
batch start
#iterations: 37
currently lose_sum: 99.26490014791489
time_elpased: 1.537
batch start
#iterations: 38
currently lose_sum: 99.95190107822418
time_elpased: 1.513
batch start
#iterations: 39
currently lose_sum: 100.40025770664215
time_elpased: 1.501
start validation test
0.602010309278
0.601567270507
0.60831532366
0.604922478637
0.601999239853
66.413
batch start
#iterations: 40
currently lose_sum: 99.46398252248764
time_elpased: 1.521
batch start
#iterations: 41
currently lose_sum: 99.76685565710068
time_elpased: 1.545
batch start
#iterations: 42
currently lose_sum: 99.88899493217468
time_elpased: 1.489
batch start
#iterations: 43
currently lose_sum: 99.30918490886688
time_elpased: 1.542
batch start
#iterations: 44
currently lose_sum: 99.4848153591156
time_elpased: 1.521
batch start
#iterations: 45
currently lose_sum: 100.19838356971741
time_elpased: 1.512
batch start
#iterations: 46
currently lose_sum: 100.49277567863464
time_elpased: 1.5
batch start
#iterations: 47
currently lose_sum: 100.36559808254242
time_elpased: 1.503
batch start
#iterations: 48
currently lose_sum: 99.49445134401321
time_elpased: 1.507
batch start
#iterations: 49
currently lose_sum: 99.62968236207962
time_elpased: 1.494
batch start
#iterations: 50
currently lose_sum: 99.75497686862946
time_elpased: 1.53
batch start
#iterations: 51
currently lose_sum: 99.3746435046196
time_elpased: 1.531
batch start
#iterations: 52
currently lose_sum: 99.31512999534607
time_elpased: 1.57
batch start
#iterations: 53
currently lose_sum: 100.34118503332138
time_elpased: 1.507
batch start
#iterations: 54
currently lose_sum: 99.17860406637192
time_elpased: 1.54
batch start
#iterations: 55
currently lose_sum: 99.52623456716537
time_elpased: 1.499
batch start
#iterations: 56
currently lose_sum: 99.79009371995926
time_elpased: 1.508
batch start
#iterations: 57
currently lose_sum: 99.66497832536697
time_elpased: 1.529
batch start
#iterations: 58
currently lose_sum: 98.74958395957947
time_elpased: 1.504
batch start
#iterations: 59
currently lose_sum: 99.99169659614563
time_elpased: 1.556
start validation test
0.517319587629
0.509431945706
0.981064114439
0.670629616602
0.516505412584
67.215
batch start
#iterations: 60
currently lose_sum: 100.0380996465683
time_elpased: 1.501
batch start
#iterations: 61
currently lose_sum: 100.18295806646347
time_elpased: 1.493
batch start
#iterations: 62
currently lose_sum: 100.49682140350342
time_elpased: 1.52
batch start
#iterations: 63
currently lose_sum: 100.46912580728531
time_elpased: 1.519
batch start
#iterations: 64
currently lose_sum: 99.5274606347084
time_elpased: 1.504
batch start
#iterations: 65
currently lose_sum: 100.29721957445145
time_elpased: 1.509
batch start
#iterations: 66
currently lose_sum: 100.47778856754303
time_elpased: 1.503
batch start
#iterations: 67
currently lose_sum: 100.23638468980789
time_elpased: 1.526
batch start
#iterations: 68
currently lose_sum: 100.41579622030258
time_elpased: 1.503
batch start
#iterations: 69
currently lose_sum: 99.50396746397018
time_elpased: 1.515
batch start
#iterations: 70
currently lose_sum: 99.7095599770546
time_elpased: 1.507
batch start
#iterations: 71
currently lose_sum: 100.48989528417587
time_elpased: 1.519
batch start
#iterations: 72
currently lose_sum: 100.03498077392578
time_elpased: 1.508
batch start
#iterations: 73
currently lose_sum: 99.69724631309509
time_elpased: 1.501
batch start
#iterations: 74
currently lose_sum: 100.50468266010284
time_elpased: 1.562
batch start
#iterations: 75
currently lose_sum: 100.50402146577835
time_elpased: 1.513
batch start
#iterations: 76
currently lose_sum: 100.5030387043953
time_elpased: 1.519
batch start
#iterations: 77
currently lose_sum: 100.50164771080017
time_elpased: 1.515
batch start
#iterations: 78
currently lose_sum: 100.4981300830841
time_elpased: 1.511
batch start
#iterations: 79
currently lose_sum: 100.47552144527435
time_elpased: 1.55
start validation test
0.601597938144
0.597757671125
0.625501698055
0.611315061604
0.601555971407
67.104
batch start
#iterations: 80
currently lose_sum: 100.44229257106781
time_elpased: 1.524
batch start
#iterations: 81
currently lose_sum: 99.81551909446716
time_elpased: 1.518
batch start
#iterations: 82
currently lose_sum: 100.15961623191833
time_elpased: 1.531
batch start
#iterations: 83
currently lose_sum: 100.20308816432953
time_elpased: 1.532
batch start
#iterations: 84
currently lose_sum: 100.50351852178574
time_elpased: 1.523
batch start
#iterations: 85
currently lose_sum: 100.50181812047958
time_elpased: 1.547
batch start
#iterations: 86
currently lose_sum: 100.49830508232117
time_elpased: 1.522
batch start
#iterations: 87
currently lose_sum: 100.34093755483627
time_elpased: 1.523
batch start
#iterations: 88
currently lose_sum: 100.05251157283783
time_elpased: 1.553
batch start
#iterations: 89
currently lose_sum: 100.50962328910828
time_elpased: 1.505
batch start
#iterations: 90
currently lose_sum: 100.50491219758987
time_elpased: 1.507
batch start
#iterations: 91
currently lose_sum: 100.50510340929031
time_elpased: 1.488
batch start
#iterations: 92
currently lose_sum: 100.50447863340378
time_elpased: 1.551
batch start
#iterations: 93
currently lose_sum: 100.50350266695023
time_elpased: 1.524
batch start
#iterations: 94
currently lose_sum: 100.50277405977249
time_elpased: 1.493
batch start
#iterations: 95
currently lose_sum: 100.50169062614441
time_elpased: 1.5
batch start
#iterations: 96
currently lose_sum: 100.4963207244873
time_elpased: 1.504
batch start
#iterations: 97
currently lose_sum: 100.29563051462173
time_elpased: 1.529
batch start
#iterations: 98
currently lose_sum: 100.21433448791504
time_elpased: 1.505
batch start
#iterations: 99
currently lose_sum: 100.51829481124878
time_elpased: 1.534
start validation test
0.522216494845
0.513088699311
0.903673973449
0.654541388692
0.521546787406
67.235
batch start
#iterations: 100
currently lose_sum: 100.50582206249237
time_elpased: 1.519
batch start
#iterations: 101
currently lose_sum: 100.505615234375
time_elpased: 1.499
batch start
#iterations: 102
currently lose_sum: 100.50562316179276
time_elpased: 1.487
batch start
#iterations: 103
currently lose_sum: 100.50523942708969
time_elpased: 1.518
batch start
#iterations: 104
currently lose_sum: 100.50523161888123
time_elpased: 1.497
batch start
#iterations: 105
currently lose_sum: 100.50510483980179
time_elpased: 1.558
batch start
#iterations: 106
currently lose_sum: 100.50417011976242
time_elpased: 1.51
batch start
#iterations: 107
currently lose_sum: 100.503915309906
time_elpased: 1.529
batch start
#iterations: 108
currently lose_sum: 100.50305831432343
time_elpased: 1.533
batch start
#iterations: 109
currently lose_sum: 100.49859595298767
time_elpased: 1.553
batch start
#iterations: 110
currently lose_sum: 100.57288670539856
time_elpased: 1.545
batch start
#iterations: 111
currently lose_sum: 100.50807797908783
time_elpased: 1.523
batch start
#iterations: 112
currently lose_sum: 100.50557380914688
time_elpased: 1.509
batch start
#iterations: 113
currently lose_sum: 100.50494730472565
time_elpased: 1.515
batch start
#iterations: 114
currently lose_sum: 100.50493425130844
time_elpased: 1.507
batch start
#iterations: 115
currently lose_sum: 100.50414764881134
time_elpased: 1.541
batch start
#iterations: 116
currently lose_sum: 100.50382167100906
time_elpased: 1.539
batch start
#iterations: 117
currently lose_sum: 100.50190305709839
time_elpased: 1.577
batch start
#iterations: 118
currently lose_sum: 100.49292540550232
time_elpased: 1.505
batch start
#iterations: 119
currently lose_sum: 100.31955617666245
time_elpased: 1.518
start validation test
0.547164948454
0.527685361217
0.914068128023
0.669102414404
0.546520793331
66.365
batch start
#iterations: 120
currently lose_sum: 100.13898950815201
time_elpased: 1.519
batch start
#iterations: 121
currently lose_sum: 100.12813699245453
time_elpased: 1.512
batch start
#iterations: 122
currently lose_sum: 99.96686762571335
time_elpased: 1.511
batch start
#iterations: 123
currently lose_sum: 100.50917410850525
time_elpased: 1.517
batch start
#iterations: 124
currently lose_sum: 100.06901150941849
time_elpased: 1.52
batch start
#iterations: 125
currently lose_sum: 100.5412038564682
time_elpased: 1.52
batch start
#iterations: 126
currently lose_sum: 100.50291913747787
time_elpased: 1.551
batch start
#iterations: 127
currently lose_sum: 100.50157833099365
time_elpased: 1.52
batch start
#iterations: 128
currently lose_sum: 100.49662417173386
time_elpased: 1.518
batch start
#iterations: 129
currently lose_sum: 100.4029728770256
time_elpased: 1.543
batch start
#iterations: 130
currently lose_sum: 100.32635128498077
time_elpased: 1.521
batch start
#iterations: 131
currently lose_sum: 100.50566923618317
time_elpased: 1.529
batch start
#iterations: 132
currently lose_sum: 100.50559973716736
time_elpased: 1.502
batch start
#iterations: 133
currently lose_sum: 100.50541150569916
time_elpased: 1.504
batch start
#iterations: 134
currently lose_sum: 100.5053322315216
time_elpased: 1.547
batch start
#iterations: 135
currently lose_sum: 100.50515156984329
time_elpased: 1.541
batch start
#iterations: 136
currently lose_sum: 100.50515830516815
time_elpased: 1.508
batch start
#iterations: 137
currently lose_sum: 100.50473648309708
time_elpased: 1.488
batch start
#iterations: 138
currently lose_sum: 100.50415503978729
time_elpased: 1.529
batch start
#iterations: 139
currently lose_sum: 100.50369399785995
time_elpased: 1.499
start validation test
0.538711340206
0.542809364548
0.501080580426
0.521110932734
0.538777406809
67.231
batch start
#iterations: 140
currently lose_sum: 100.50306552648544
time_elpased: 1.504
batch start
#iterations: 141
currently lose_sum: 100.50036364793777
time_elpased: 1.524
batch start
#iterations: 142
currently lose_sum: 100.4947235584259
time_elpased: 1.52
batch start
#iterations: 143
currently lose_sum: 100.28671276569366
time_elpased: 1.559
batch start
#iterations: 144
currently lose_sum: 100.05933725833893
time_elpased: 1.533
batch start
#iterations: 145
currently lose_sum: 100.07742077112198
time_elpased: 1.518
batch start
#iterations: 146
currently lose_sum: 100.50594955682755
time_elpased: 1.491
batch start
#iterations: 147
currently lose_sum: 100.50569921731949
time_elpased: 1.538
batch start
#iterations: 148
currently lose_sum: 100.50578081607819
time_elpased: 1.528
batch start
#iterations: 149
currently lose_sum: 100.50564527511597
time_elpased: 1.506
batch start
#iterations: 150
currently lose_sum: 100.50533378124237
time_elpased: 1.519
batch start
#iterations: 151
currently lose_sum: 100.50541055202484
time_elpased: 1.511
batch start
#iterations: 152
currently lose_sum: 100.50527882575989
time_elpased: 1.523
batch start
#iterations: 153
currently lose_sum: 100.5053101181984
time_elpased: 1.545
batch start
#iterations: 154
currently lose_sum: 100.50461429357529
time_elpased: 1.526
batch start
#iterations: 155
currently lose_sum: 100.50454902648926
time_elpased: 1.535
batch start
#iterations: 156
currently lose_sum: 100.50430196523666
time_elpased: 1.509
batch start
#iterations: 157
currently lose_sum: 100.50333309173584
time_elpased: 1.62
batch start
#iterations: 158
currently lose_sum: 100.50055009126663
time_elpased: 1.486
batch start
#iterations: 159
currently lose_sum: 100.32349467277527
time_elpased: 1.514
start validation test
0.606494845361
0.645035510375
0.476690336524
0.54823055983
0.606722737197
64.767
batch start
#iterations: 160
currently lose_sum: 100.30291646718979
time_elpased: 1.496
batch start
#iterations: 161
currently lose_sum: 99.34392434358597
time_elpased: 1.527
batch start
#iterations: 162
currently lose_sum: 99.74475908279419
time_elpased: 1.492
batch start
#iterations: 163
currently lose_sum: 99.7735561132431
time_elpased: 1.506
batch start
#iterations: 164
currently lose_sum: 100.50448840856552
time_elpased: 1.503
batch start
#iterations: 165
currently lose_sum: 100.50304812192917
time_elpased: 1.508
batch start
#iterations: 166
currently lose_sum: 100.5009537935257
time_elpased: 1.535
batch start
#iterations: 167
currently lose_sum: 100.49656295776367
time_elpased: 1.516
batch start
#iterations: 168
currently lose_sum: 100.42304140329361
time_elpased: 1.53
batch start
#iterations: 169
currently lose_sum: 99.40609729290009
time_elpased: 1.511
batch start
#iterations: 170
currently lose_sum: 100.39825963973999
time_elpased: 1.545
batch start
#iterations: 171
currently lose_sum: 99.38234168291092
time_elpased: 1.5
batch start
#iterations: 172
currently lose_sum: 99.82560616731644
time_elpased: 1.502
batch start
#iterations: 173
currently lose_sum: 100.50546282529831
time_elpased: 1.531
batch start
#iterations: 174
currently lose_sum: 100.50485664606094
time_elpased: 1.507
batch start
#iterations: 175
currently lose_sum: 100.50444203615189
time_elpased: 1.539
batch start
#iterations: 176
currently lose_sum: 100.50348860025406
time_elpased: 1.492
batch start
#iterations: 177
currently lose_sum: 100.50162279605865
time_elpased: 1.535
batch start
#iterations: 178
currently lose_sum: 100.49728518724442
time_elpased: 1.585
batch start
#iterations: 179
currently lose_sum: 100.29897314310074
time_elpased: 1.506
start validation test
0.630618556701
0.602507433899
0.771534424205
0.676624548736
0.630371157161
64.042
batch start
#iterations: 180
currently lose_sum: 99.15773969888687
time_elpased: 1.538
batch start
#iterations: 181
currently lose_sum: 100.32227939367294
time_elpased: 1.507
batch start
#iterations: 182
currently lose_sum: 100.50593304634094
time_elpased: 1.546
batch start
#iterations: 183
currently lose_sum: 100.5057435631752
time_elpased: 1.525
batch start
#iterations: 184
currently lose_sum: 100.50571298599243
time_elpased: 1.493
batch start
#iterations: 185
currently lose_sum: 100.50565195083618
time_elpased: 1.545
batch start
#iterations: 186
currently lose_sum: 100.50553953647614
time_elpased: 1.493
batch start
#iterations: 187
currently lose_sum: 100.50552517175674
time_elpased: 1.529
batch start
#iterations: 188
currently lose_sum: 100.50536453723907
time_elpased: 1.543
batch start
#iterations: 189
currently lose_sum: 100.50530236959457
time_elpased: 1.481
batch start
#iterations: 190
currently lose_sum: 100.50505501031876
time_elpased: 1.55
batch start
#iterations: 191
currently lose_sum: 100.5048616528511
time_elpased: 1.541
batch start
#iterations: 192
currently lose_sum: 100.50440502166748
time_elpased: 1.499
batch start
#iterations: 193
currently lose_sum: 100.50387579202652
time_elpased: 1.485
batch start
#iterations: 194
currently lose_sum: 100.50260269641876
time_elpased: 1.525
batch start
#iterations: 195
currently lose_sum: 100.50103998184204
time_elpased: 1.542
batch start
#iterations: 196
currently lose_sum: 100.49198520183563
time_elpased: 1.528
batch start
#iterations: 197
currently lose_sum: 99.8878344297409
time_elpased: 1.522
batch start
#iterations: 198
currently lose_sum: 99.75662940740585
time_elpased: 1.502
batch start
#iterations: 199
currently lose_sum: 99.55048525333405
time_elpased: 1.53
start validation test
0.49912371134
0.0
0.0
nan
0.5
67.242
acc: 0.632
pre: 0.603
rec: 0.775
F1: 0.678
auc: 0.631
