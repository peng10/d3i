start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 101.30451327562332
time_elpased: 1.491
batch start
#iterations: 1
currently lose_sum: 100.4876292347908
time_elpased: 1.416
batch start
#iterations: 2
currently lose_sum: 100.37480676174164
time_elpased: 1.419
batch start
#iterations: 3
currently lose_sum: 100.32675790786743
time_elpased: 1.425
batch start
#iterations: 4
currently lose_sum: 100.22749412059784
time_elpased: 1.438
batch start
#iterations: 5
currently lose_sum: 100.1840290427208
time_elpased: 1.434
batch start
#iterations: 6
currently lose_sum: 100.14905846118927
time_elpased: 1.411
batch start
#iterations: 7
currently lose_sum: 100.1080692410469
time_elpased: 1.435
batch start
#iterations: 8
currently lose_sum: 99.95998704433441
time_elpased: 1.414
batch start
#iterations: 9
currently lose_sum: 100.05119806528091
time_elpased: 1.409
batch start
#iterations: 10
currently lose_sum: 99.96443527936935
time_elpased: 1.419
batch start
#iterations: 11
currently lose_sum: 99.94859552383423
time_elpased: 1.422
batch start
#iterations: 12
currently lose_sum: 100.00211304426193
time_elpased: 1.44
batch start
#iterations: 13
currently lose_sum: 99.76901704072952
time_elpased: 1.449
batch start
#iterations: 14
currently lose_sum: 99.83492803573608
time_elpased: 1.429
batch start
#iterations: 15
currently lose_sum: 99.8429684638977
time_elpased: 1.446
batch start
#iterations: 16
currently lose_sum: 99.70626211166382
time_elpased: 1.422
batch start
#iterations: 17
currently lose_sum: 99.60536134243011
time_elpased: 1.421
batch start
#iterations: 18
currently lose_sum: 99.69145745038986
time_elpased: 1.441
batch start
#iterations: 19
currently lose_sum: 99.57545286417007
time_elpased: 1.435
start validation test
0.590309278351
0.60652776105
0.518266954821
0.558934517203
0.590435759761
65.578
batch start
#iterations: 20
currently lose_sum: 99.57512199878693
time_elpased: 1.43
batch start
#iterations: 21
currently lose_sum: 99.46267402172089
time_elpased: 1.421
batch start
#iterations: 22
currently lose_sum: 99.46800875663757
time_elpased: 1.437
batch start
#iterations: 23
currently lose_sum: 99.45670771598816
time_elpased: 1.415
batch start
#iterations: 24
currently lose_sum: 99.46654433012009
time_elpased: 1.458
batch start
#iterations: 25
currently lose_sum: 99.52890676259995
time_elpased: 1.451
batch start
#iterations: 26
currently lose_sum: 99.29008078575134
time_elpased: 1.448
batch start
#iterations: 27
currently lose_sum: 99.50246739387512
time_elpased: 1.433
batch start
#iterations: 28
currently lose_sum: 99.4727241396904
time_elpased: 1.433
batch start
#iterations: 29
currently lose_sum: 99.51913625001907
time_elpased: 1.394
batch start
#iterations: 30
currently lose_sum: 99.32300305366516
time_elpased: 1.445
batch start
#iterations: 31
currently lose_sum: 99.25043427944183
time_elpased: 1.407
batch start
#iterations: 32
currently lose_sum: 99.19005632400513
time_elpased: 1.4
batch start
#iterations: 33
currently lose_sum: 99.3941712975502
time_elpased: 1.693
batch start
#iterations: 34
currently lose_sum: 99.34781247377396
time_elpased: 1.426
batch start
#iterations: 35
currently lose_sum: 99.30668145418167
time_elpased: 1.462
batch start
#iterations: 36
currently lose_sum: 99.10887706279755
time_elpased: 1.422
batch start
#iterations: 37
currently lose_sum: 99.1819396018982
time_elpased: 1.457
batch start
#iterations: 38
currently lose_sum: 98.99089753627777
time_elpased: 1.411
batch start
#iterations: 39
currently lose_sum: 99.27827090024948
time_elpased: 1.41
start validation test
0.634690721649
0.607786885246
0.76309560564
0.676643701236
0.634465287071
64.684
batch start
#iterations: 40
currently lose_sum: 99.20332324504852
time_elpased: 1.416
batch start
#iterations: 41
currently lose_sum: 99.14961260557175
time_elpased: 1.409
batch start
#iterations: 42
currently lose_sum: 99.1136280298233
time_elpased: 1.445
batch start
#iterations: 43
currently lose_sum: 98.91534692049026
time_elpased: 1.419
batch start
#iterations: 44
currently lose_sum: 99.11814433336258
time_elpased: 1.424
batch start
#iterations: 45
currently lose_sum: 99.1670218706131
time_elpased: 1.42
batch start
#iterations: 46
currently lose_sum: 99.1555905342102
time_elpased: 1.42
batch start
#iterations: 47
currently lose_sum: 99.0763099193573
time_elpased: 1.436
batch start
#iterations: 48
currently lose_sum: 99.09569042921066
time_elpased: 1.451
batch start
#iterations: 49
currently lose_sum: 98.91754400730133
time_elpased: 1.474
batch start
#iterations: 50
currently lose_sum: 98.9302989244461
time_elpased: 1.419
batch start
#iterations: 51
currently lose_sum: 98.9612666964531
time_elpased: 1.428
batch start
#iterations: 52
currently lose_sum: 99.10790199041367
time_elpased: 1.417
batch start
#iterations: 53
currently lose_sum: 98.9402237534523
time_elpased: 1.46
batch start
#iterations: 54
currently lose_sum: 99.04876828193665
time_elpased: 1.458
batch start
#iterations: 55
currently lose_sum: 99.02391105890274
time_elpased: 1.412
batch start
#iterations: 56
currently lose_sum: 99.01353365182877
time_elpased: 1.452
batch start
#iterations: 57
currently lose_sum: 98.83253234624863
time_elpased: 1.444
batch start
#iterations: 58
currently lose_sum: 98.86288547515869
time_elpased: 1.423
batch start
#iterations: 59
currently lose_sum: 98.8680989742279
time_elpased: 1.417
start validation test
0.603969072165
0.666666666667
0.418647730781
0.514318224919
0.604294432364
64.378
batch start
#iterations: 60
currently lose_sum: 98.94650393724442
time_elpased: 1.478
batch start
#iterations: 61
currently lose_sum: 98.7717170715332
time_elpased: 1.434
batch start
#iterations: 62
currently lose_sum: 99.17175930738449
time_elpased: 1.426
batch start
#iterations: 63
currently lose_sum: 98.95674461126328
time_elpased: 1.504
batch start
#iterations: 64
currently lose_sum: 98.96713125705719
time_elpased: 1.489
batch start
#iterations: 65
currently lose_sum: 98.876280605793
time_elpased: 1.436
batch start
#iterations: 66
currently lose_sum: 98.84303909540176
time_elpased: 1.463
batch start
#iterations: 67
currently lose_sum: 98.95928472280502
time_elpased: 1.45
batch start
#iterations: 68
currently lose_sum: 98.96188116073608
time_elpased: 1.452
batch start
#iterations: 69
currently lose_sum: 98.77122074365616
time_elpased: 1.526
batch start
#iterations: 70
currently lose_sum: 98.68661630153656
time_elpased: 1.428
batch start
#iterations: 71
currently lose_sum: 98.90069097280502
time_elpased: 1.422
batch start
#iterations: 72
currently lose_sum: 98.6592007279396
time_elpased: 1.478
batch start
#iterations: 73
currently lose_sum: 98.85816037654877
time_elpased: 1.438
batch start
#iterations: 74
currently lose_sum: 98.87846982479095
time_elpased: 1.452
batch start
#iterations: 75
currently lose_sum: 98.99863266944885
time_elpased: 1.446
batch start
#iterations: 76
currently lose_sum: 98.831618309021
time_elpased: 1.438
batch start
#iterations: 77
currently lose_sum: 98.81296688318253
time_elpased: 1.415
batch start
#iterations: 78
currently lose_sum: 98.88548523187637
time_elpased: 1.419
batch start
#iterations: 79
currently lose_sum: 99.06393557786942
time_elpased: 1.455
start validation test
0.648402061856
0.653977031051
0.632911392405
0.643271795408
0.648429258115
63.964
batch start
#iterations: 80
currently lose_sum: 99.01438337564468
time_elpased: 1.414
batch start
#iterations: 81
currently lose_sum: 98.90658617019653
time_elpased: 1.452
batch start
#iterations: 82
currently lose_sum: 98.92855024337769
time_elpased: 1.478
batch start
#iterations: 83
currently lose_sum: 98.65455394983292
time_elpased: 1.425
batch start
#iterations: 84
currently lose_sum: 98.81877928972244
time_elpased: 1.421
batch start
#iterations: 85
currently lose_sum: 98.77296167612076
time_elpased: 1.438
batch start
#iterations: 86
currently lose_sum: 99.00210303068161
time_elpased: 1.412
batch start
#iterations: 87
currently lose_sum: 98.66750645637512
time_elpased: 1.435
batch start
#iterations: 88
currently lose_sum: 98.77044177055359
time_elpased: 1.447
batch start
#iterations: 89
currently lose_sum: 98.81313985586166
time_elpased: 1.438
batch start
#iterations: 90
currently lose_sum: 98.90362358093262
time_elpased: 1.433
batch start
#iterations: 91
currently lose_sum: 98.86615061759949
time_elpased: 1.441
batch start
#iterations: 92
currently lose_sum: 98.73009419441223
time_elpased: 1.472
batch start
#iterations: 93
currently lose_sum: 98.850421667099
time_elpased: 1.399
batch start
#iterations: 94
currently lose_sum: 98.96751129627228
time_elpased: 1.459
batch start
#iterations: 95
currently lose_sum: 98.64827108383179
time_elpased: 1.464
batch start
#iterations: 96
currently lose_sum: 98.88890552520752
time_elpased: 1.425
batch start
#iterations: 97
currently lose_sum: 98.69067239761353
time_elpased: 1.485
batch start
#iterations: 98
currently lose_sum: 98.74666744470596
time_elpased: 1.433
batch start
#iterations: 99
currently lose_sum: 98.65134507417679
time_elpased: 1.435
start validation test
0.643608247423
0.650877381849
0.622208500566
0.636220141008
0.643645817979
63.737
batch start
#iterations: 100
currently lose_sum: 98.94220745563507
time_elpased: 1.423
batch start
#iterations: 101
currently lose_sum: 98.78356683254242
time_elpased: 1.412
batch start
#iterations: 102
currently lose_sum: 98.96336513757706
time_elpased: 1.415
batch start
#iterations: 103
currently lose_sum: 98.78367120027542
time_elpased: 1.407
batch start
#iterations: 104
currently lose_sum: 98.646164894104
time_elpased: 1.443
batch start
#iterations: 105
currently lose_sum: 98.62307715415955
time_elpased: 1.427
batch start
#iterations: 106
currently lose_sum: 98.7451901435852
time_elpased: 1.403
batch start
#iterations: 107
currently lose_sum: 98.92198872566223
time_elpased: 1.429
batch start
#iterations: 108
currently lose_sum: 98.87406355142593
time_elpased: 1.478
batch start
#iterations: 109
currently lose_sum: 98.78173542022705
time_elpased: 1.425
batch start
#iterations: 110
currently lose_sum: 98.76548141241074
time_elpased: 1.468
batch start
#iterations: 111
currently lose_sum: 98.69152456521988
time_elpased: 1.416
batch start
#iterations: 112
currently lose_sum: 98.77866995334625
time_elpased: 1.421
batch start
#iterations: 113
currently lose_sum: 98.6670446395874
time_elpased: 1.432
batch start
#iterations: 114
currently lose_sum: 98.62624132633209
time_elpased: 1.443
batch start
#iterations: 115
currently lose_sum: 98.75687253475189
time_elpased: 1.425
batch start
#iterations: 116
currently lose_sum: 98.57750767469406
time_elpased: 1.436
batch start
#iterations: 117
currently lose_sum: 98.71180528402328
time_elpased: 1.434
batch start
#iterations: 118
currently lose_sum: 98.5681711435318
time_elpased: 1.43
batch start
#iterations: 119
currently lose_sum: 98.80403363704681
time_elpased: 1.43
start validation test
0.653092783505
0.646435925091
0.678501595143
0.662080739104
0.653048174417
63.527
batch start
#iterations: 120
currently lose_sum: 98.53737533092499
time_elpased: 1.436
batch start
#iterations: 121
currently lose_sum: 98.58259457349777
time_elpased: 1.453
batch start
#iterations: 122
currently lose_sum: 98.81381690502167
time_elpased: 1.428
batch start
#iterations: 123
currently lose_sum: 98.73744469881058
time_elpased: 1.446
batch start
#iterations: 124
currently lose_sum: 98.83070200681686
time_elpased: 1.432
batch start
#iterations: 125
currently lose_sum: 98.6474871635437
time_elpased: 1.426
batch start
#iterations: 126
currently lose_sum: 99.00012969970703
time_elpased: 1.45
batch start
#iterations: 127
currently lose_sum: 98.84038227796555
time_elpased: 1.441
batch start
#iterations: 128
currently lose_sum: 98.72109693288803
time_elpased: 1.42
batch start
#iterations: 129
currently lose_sum: 98.99893456697464
time_elpased: 1.46
batch start
#iterations: 130
currently lose_sum: 98.68950670957565
time_elpased: 1.409
batch start
#iterations: 131
currently lose_sum: 98.82438069581985
time_elpased: 1.412
batch start
#iterations: 132
currently lose_sum: 98.75136339664459
time_elpased: 1.429
batch start
#iterations: 133
currently lose_sum: 98.61273604631424
time_elpased: 1.463
batch start
#iterations: 134
currently lose_sum: 98.7607604265213
time_elpased: 1.46
batch start
#iterations: 135
currently lose_sum: 98.78695833683014
time_elpased: 1.487
batch start
#iterations: 136
currently lose_sum: 98.73707640171051
time_elpased: 1.429
batch start
#iterations: 137
currently lose_sum: 98.40952092409134
time_elpased: 1.415
batch start
#iterations: 138
currently lose_sum: 98.7329221367836
time_elpased: 1.474
batch start
#iterations: 139
currently lose_sum: 98.44759964942932
time_elpased: 1.477
start validation test
0.637680412371
0.668084042021
0.549758155809
0.60317281093
0.637834773454
63.535
batch start
#iterations: 140
currently lose_sum: 98.7167518734932
time_elpased: 1.44
batch start
#iterations: 141
currently lose_sum: 98.77767270803452
time_elpased: 1.432
batch start
#iterations: 142
currently lose_sum: 98.66947340965271
time_elpased: 1.427
batch start
#iterations: 143
currently lose_sum: 98.63648283481598
time_elpased: 1.409
batch start
#iterations: 144
currently lose_sum: 98.89350461959839
time_elpased: 1.434
batch start
#iterations: 145
currently lose_sum: 98.5549368262291
time_elpased: 1.431
batch start
#iterations: 146
currently lose_sum: 98.62884658575058
time_elpased: 1.456
batch start
#iterations: 147
currently lose_sum: 98.4173972606659
time_elpased: 1.446
batch start
#iterations: 148
currently lose_sum: 98.63333344459534
time_elpased: 1.443
batch start
#iterations: 149
currently lose_sum: 98.68401300907135
time_elpased: 1.448
batch start
#iterations: 150
currently lose_sum: 98.70385038852692
time_elpased: 1.475
batch start
#iterations: 151
currently lose_sum: 98.58579778671265
time_elpased: 1.441
batch start
#iterations: 152
currently lose_sum: 98.66352313756943
time_elpased: 1.45
batch start
#iterations: 153
currently lose_sum: 98.88099962472916
time_elpased: 1.437
batch start
#iterations: 154
currently lose_sum: 98.75129282474518
time_elpased: 1.442
batch start
#iterations: 155
currently lose_sum: 98.76641941070557
time_elpased: 1.4
batch start
#iterations: 156
currently lose_sum: 98.71539944410324
time_elpased: 1.479
batch start
#iterations: 157
currently lose_sum: 98.53818440437317
time_elpased: 1.412
batch start
#iterations: 158
currently lose_sum: 98.52521657943726
time_elpased: 1.437
batch start
#iterations: 159
currently lose_sum: 98.64952266216278
time_elpased: 1.414
start validation test
0.639742268041
0.668686618847
0.556447463209
0.607425714767
0.639888504918
63.656
batch start
#iterations: 160
currently lose_sum: 98.46045005321503
time_elpased: 1.426
batch start
#iterations: 161
currently lose_sum: 98.84464651346207
time_elpased: 1.407
batch start
#iterations: 162
currently lose_sum: 98.81950944662094
time_elpased: 1.431
batch start
#iterations: 163
currently lose_sum: 98.79699277877808
time_elpased: 1.403
batch start
#iterations: 164
currently lose_sum: 98.28085911273956
time_elpased: 1.446
batch start
#iterations: 165
currently lose_sum: 98.66972309350967
time_elpased: 1.427
batch start
#iterations: 166
currently lose_sum: 98.6582453250885
time_elpased: 1.461
batch start
#iterations: 167
currently lose_sum: 98.69279259443283
time_elpased: 1.421
batch start
#iterations: 168
currently lose_sum: 98.35429972410202
time_elpased: 1.448
batch start
#iterations: 169
currently lose_sum: 98.70291531085968
time_elpased: 1.456
batch start
#iterations: 170
currently lose_sum: 98.80398511886597
time_elpased: 1.438
batch start
#iterations: 171
currently lose_sum: 98.65484917163849
time_elpased: 1.433
batch start
#iterations: 172
currently lose_sum: 98.89564347267151
time_elpased: 1.429
batch start
#iterations: 173
currently lose_sum: 98.75739026069641
time_elpased: 1.429
batch start
#iterations: 174
currently lose_sum: 98.32279056310654
time_elpased: 1.422
batch start
#iterations: 175
currently lose_sum: 98.50107669830322
time_elpased: 1.465
batch start
#iterations: 176
currently lose_sum: 98.63794749975204
time_elpased: 1.451
batch start
#iterations: 177
currently lose_sum: 98.56629425287247
time_elpased: 1.386
batch start
#iterations: 178
currently lose_sum: 98.75191330909729
time_elpased: 1.411
batch start
#iterations: 179
currently lose_sum: 98.54287749528885
time_elpased: 1.45
start validation test
0.664793814433
0.641511095456
0.749716990841
0.691406064632
0.664644718698
63.407
batch start
#iterations: 180
currently lose_sum: 98.67932909727097
time_elpased: 1.422
batch start
#iterations: 181
currently lose_sum: 98.54700928926468
time_elpased: 1.472
batch start
#iterations: 182
currently lose_sum: 98.55806851387024
time_elpased: 1.441
batch start
#iterations: 183
currently lose_sum: 98.50639522075653
time_elpased: 1.426
batch start
#iterations: 184
currently lose_sum: 98.7431373000145
time_elpased: 1.435
batch start
#iterations: 185
currently lose_sum: 98.5861913561821
time_elpased: 1.441
batch start
#iterations: 186
currently lose_sum: 98.64649546146393
time_elpased: 1.473
batch start
#iterations: 187
currently lose_sum: 98.72749191522598
time_elpased: 1.454
batch start
#iterations: 188
currently lose_sum: 98.55197262763977
time_elpased: 1.41
batch start
#iterations: 189
currently lose_sum: 98.7434378862381
time_elpased: 1.411
batch start
#iterations: 190
currently lose_sum: 98.62695944309235
time_elpased: 1.439
batch start
#iterations: 191
currently lose_sum: 98.47830939292908
time_elpased: 1.421
batch start
#iterations: 192
currently lose_sum: 98.63148444890976
time_elpased: 1.404
batch start
#iterations: 193
currently lose_sum: 98.51535719633102
time_elpased: 1.425
batch start
#iterations: 194
currently lose_sum: 98.4844183921814
time_elpased: 1.421
batch start
#iterations: 195
currently lose_sum: 98.7323487997055
time_elpased: 1.436
batch start
#iterations: 196
currently lose_sum: 98.55804997682571
time_elpased: 1.42
batch start
#iterations: 197
currently lose_sum: 98.65578836202621
time_elpased: 1.447
batch start
#iterations: 198
currently lose_sum: 98.51378148794174
time_elpased: 1.459
batch start
#iterations: 199
currently lose_sum: 98.55289554595947
time_elpased: 1.415
start validation test
0.648762886598
0.652260568551
0.639909437069
0.646025974026
0.648778430194
63.393
batch start
#iterations: 200
currently lose_sum: 98.56657058000565
time_elpased: 1.522
batch start
#iterations: 201
currently lose_sum: 98.50973296165466
time_elpased: 1.452
batch start
#iterations: 202
currently lose_sum: 98.61548590660095
time_elpased: 1.492
batch start
#iterations: 203
currently lose_sum: 98.57623624801636
time_elpased: 1.427
batch start
#iterations: 204
currently lose_sum: 98.52524596452713
time_elpased: 1.429
batch start
#iterations: 205
currently lose_sum: 98.47150325775146
time_elpased: 1.437
batch start
#iterations: 206
currently lose_sum: 98.35561621189117
time_elpased: 1.426
batch start
#iterations: 207
currently lose_sum: 98.64281165599823
time_elpased: 1.49
batch start
#iterations: 208
currently lose_sum: 98.4591755270958
time_elpased: 1.452
batch start
#iterations: 209
currently lose_sum: 98.40079319477081
time_elpased: 1.434
batch start
#iterations: 210
currently lose_sum: 98.40726393461227
time_elpased: 1.417
batch start
#iterations: 211
currently lose_sum: 98.78243213891983
time_elpased: 1.431
batch start
#iterations: 212
currently lose_sum: 98.61798524856567
time_elpased: 1.437
batch start
#iterations: 213
currently lose_sum: 98.62107825279236
time_elpased: 1.442
batch start
#iterations: 214
currently lose_sum: 98.49330043792725
time_elpased: 1.427
batch start
#iterations: 215
currently lose_sum: 98.48866647481918
time_elpased: 1.443
batch start
#iterations: 216
currently lose_sum: 98.45698660612106
time_elpased: 1.427
batch start
#iterations: 217
currently lose_sum: 98.5844054222107
time_elpased: 1.433
batch start
#iterations: 218
currently lose_sum: 98.3094511628151
time_elpased: 1.487
batch start
#iterations: 219
currently lose_sum: 98.61552882194519
time_elpased: 1.403
start validation test
0.633402061856
0.671720500989
0.524338787692
0.588949254421
0.633593539255
63.443
batch start
#iterations: 220
currently lose_sum: 98.61270248889923
time_elpased: 1.543
batch start
#iterations: 221
currently lose_sum: 98.81061762571335
time_elpased: 1.487
batch start
#iterations: 222
currently lose_sum: 98.36793446540833
time_elpased: 1.435
batch start
#iterations: 223
currently lose_sum: 98.4965792298317
time_elpased: 1.447
batch start
#iterations: 224
currently lose_sum: 98.34889847040176
time_elpased: 1.417
batch start
#iterations: 225
currently lose_sum: 98.7416165471077
time_elpased: 1.421
batch start
#iterations: 226
currently lose_sum: 98.41494899988174
time_elpased: 1.425
batch start
#iterations: 227
currently lose_sum: 98.6290014386177
time_elpased: 1.396
batch start
#iterations: 228
currently lose_sum: 98.7441024184227
time_elpased: 1.428
batch start
#iterations: 229
currently lose_sum: 98.40232920646667
time_elpased: 1.426
batch start
#iterations: 230
currently lose_sum: 98.55260360240936
time_elpased: 1.453
batch start
#iterations: 231
currently lose_sum: 98.30665099620819
time_elpased: 1.43
batch start
#iterations: 232
currently lose_sum: 98.61116409301758
time_elpased: 1.433
batch start
#iterations: 233
currently lose_sum: 98.60948532819748
time_elpased: 1.44
batch start
#iterations: 234
currently lose_sum: 98.61601340770721
time_elpased: 1.442
batch start
#iterations: 235
currently lose_sum: 98.6590376496315
time_elpased: 1.39
batch start
#iterations: 236
currently lose_sum: 98.60734462738037
time_elpased: 1.433
batch start
#iterations: 237
currently lose_sum: 98.73038804531097
time_elpased: 1.477
batch start
#iterations: 238
currently lose_sum: 98.61724489927292
time_elpased: 1.467
batch start
#iterations: 239
currently lose_sum: 98.62229514122009
time_elpased: 1.417
start validation test
0.662371134021
0.64347195796
0.730884017701
0.684398188301
0.662250849086
63.437
batch start
#iterations: 240
currently lose_sum: 98.49039268493652
time_elpased: 1.429
batch start
#iterations: 241
currently lose_sum: 98.71209210157394
time_elpased: 1.446
batch start
#iterations: 242
currently lose_sum: 98.45222342014313
time_elpased: 1.444
batch start
#iterations: 243
currently lose_sum: 98.52380472421646
time_elpased: 1.426
batch start
#iterations: 244
currently lose_sum: 98.58537435531616
time_elpased: 1.402
batch start
#iterations: 245
currently lose_sum: 98.5701829791069
time_elpased: 1.433
batch start
#iterations: 246
currently lose_sum: 98.62630879878998
time_elpased: 1.431
batch start
#iterations: 247
currently lose_sum: 98.33156090974808
time_elpased: 1.449
batch start
#iterations: 248
currently lose_sum: 98.40308904647827
time_elpased: 1.416
batch start
#iterations: 249
currently lose_sum: 98.59864908456802
time_elpased: 1.424
batch start
#iterations: 250
currently lose_sum: 98.49484574794769
time_elpased: 1.417
batch start
#iterations: 251
currently lose_sum: 98.55001801252365
time_elpased: 1.442
batch start
#iterations: 252
currently lose_sum: 98.53274011611938
time_elpased: 1.423
batch start
#iterations: 253
currently lose_sum: 98.34400862455368
time_elpased: 1.448
batch start
#iterations: 254
currently lose_sum: 98.49126476049423
time_elpased: 1.437
batch start
#iterations: 255
currently lose_sum: 98.5515638589859
time_elpased: 1.403
batch start
#iterations: 256
currently lose_sum: 98.34228897094727
time_elpased: 1.42
batch start
#iterations: 257
currently lose_sum: 98.63021737337112
time_elpased: 1.412
batch start
#iterations: 258
currently lose_sum: 98.58853483200073
time_elpased: 1.471
batch start
#iterations: 259
currently lose_sum: 98.36305892467499
time_elpased: 1.442
start validation test
0.664948453608
0.64138173508
0.750951939899
0.691855503935
0.664797461223
63.239
batch start
#iterations: 260
currently lose_sum: 98.52556204795837
time_elpased: 1.424
batch start
#iterations: 261
currently lose_sum: 98.24333798885345
time_elpased: 1.441
batch start
#iterations: 262
currently lose_sum: 98.44848543405533
time_elpased: 1.413
batch start
#iterations: 263
currently lose_sum: 98.52790653705597
time_elpased: 1.406
batch start
#iterations: 264
currently lose_sum: 98.4361075758934
time_elpased: 1.469
batch start
#iterations: 265
currently lose_sum: 98.54879957437515
time_elpased: 1.39
batch start
#iterations: 266
currently lose_sum: 98.3744198679924
time_elpased: 1.496
batch start
#iterations: 267
currently lose_sum: 98.243219435215
time_elpased: 1.442
batch start
#iterations: 268
currently lose_sum: 98.52255237102509
time_elpased: 1.414
batch start
#iterations: 269
currently lose_sum: 98.36943936347961
time_elpased: 1.408
batch start
#iterations: 270
currently lose_sum: 98.4951366186142
time_elpased: 1.427
batch start
#iterations: 271
currently lose_sum: 98.67080450057983
time_elpased: 1.457
batch start
#iterations: 272
currently lose_sum: 98.22446709871292
time_elpased: 1.406
batch start
#iterations: 273
currently lose_sum: 98.43969666957855
time_elpased: 1.423
batch start
#iterations: 274
currently lose_sum: 98.64058899879456
time_elpased: 1.425
batch start
#iterations: 275
currently lose_sum: 98.58569556474686
time_elpased: 1.428
batch start
#iterations: 276
currently lose_sum: 98.69503664970398
time_elpased: 1.44
batch start
#iterations: 277
currently lose_sum: 98.38837707042694
time_elpased: 1.431
batch start
#iterations: 278
currently lose_sum: 98.71094983816147
time_elpased: 1.424
batch start
#iterations: 279
currently lose_sum: 98.4809564948082
time_elpased: 1.446
start validation test
0.653402061856
0.668089408065
0.612123083256
0.63888292159
0.653474533469
63.284
batch start
#iterations: 280
currently lose_sum: 98.42647051811218
time_elpased: 1.446
batch start
#iterations: 281
currently lose_sum: 98.28158515691757
time_elpased: 1.45
batch start
#iterations: 282
currently lose_sum: 98.46173679828644
time_elpased: 1.436
batch start
#iterations: 283
currently lose_sum: 98.5726049542427
time_elpased: 1.425
batch start
#iterations: 284
currently lose_sum: 98.57179123163223
time_elpased: 1.419
batch start
#iterations: 285
currently lose_sum: 98.48150783777237
time_elpased: 1.439
batch start
#iterations: 286
currently lose_sum: 98.15809386968613
time_elpased: 1.436
batch start
#iterations: 287
currently lose_sum: 98.33379715681076
time_elpased: 1.453
batch start
#iterations: 288
currently lose_sum: 98.43569320440292
time_elpased: 1.416
batch start
#iterations: 289
currently lose_sum: 98.30257159471512
time_elpased: 1.427
batch start
#iterations: 290
currently lose_sum: 98.25286519527435
time_elpased: 1.411
batch start
#iterations: 291
currently lose_sum: 98.51704144477844
time_elpased: 1.423
batch start
#iterations: 292
currently lose_sum: 98.4071935415268
time_elpased: 1.447
batch start
#iterations: 293
currently lose_sum: 98.66970008611679
time_elpased: 1.439
batch start
#iterations: 294
currently lose_sum: 98.70111906528473
time_elpased: 1.427
batch start
#iterations: 295
currently lose_sum: 98.48923826217651
time_elpased: 1.432
batch start
#iterations: 296
currently lose_sum: 98.31475526094437
time_elpased: 1.415
batch start
#iterations: 297
currently lose_sum: 98.31982606649399
time_elpased: 1.426
batch start
#iterations: 298
currently lose_sum: 98.42452210187912
time_elpased: 1.411
batch start
#iterations: 299
currently lose_sum: 98.36972069740295
time_elpased: 1.404
start validation test
0.666907216495
0.644422752684
0.747350005146
0.692080434575
0.666765986772
63.105
batch start
#iterations: 300
currently lose_sum: 98.24707609415054
time_elpased: 1.446
batch start
#iterations: 301
currently lose_sum: 98.74928849935532
time_elpased: 1.432
batch start
#iterations: 302
currently lose_sum: 98.49272608757019
time_elpased: 1.408
batch start
#iterations: 303
currently lose_sum: 98.54996085166931
time_elpased: 1.425
batch start
#iterations: 304
currently lose_sum: 98.52829074859619
time_elpased: 1.431
batch start
#iterations: 305
currently lose_sum: 98.34493219852448
time_elpased: 1.439
batch start
#iterations: 306
currently lose_sum: 98.50917875766754
time_elpased: 1.458
batch start
#iterations: 307
currently lose_sum: 98.62879127264023
time_elpased: 1.412
batch start
#iterations: 308
currently lose_sum: 98.33286172151566
time_elpased: 1.433
batch start
#iterations: 309
currently lose_sum: 98.1996397972107
time_elpased: 1.408
batch start
#iterations: 310
currently lose_sum: 98.7345524430275
time_elpased: 1.419
batch start
#iterations: 311
currently lose_sum: 98.38286572694778
time_elpased: 1.446
batch start
#iterations: 312
currently lose_sum: 98.29594373703003
time_elpased: 1.431
batch start
#iterations: 313
currently lose_sum: 98.51240277290344
time_elpased: 1.415
batch start
#iterations: 314
currently lose_sum: 98.28014874458313
time_elpased: 1.464
batch start
#iterations: 315
currently lose_sum: 98.36490160226822
time_elpased: 1.471
batch start
#iterations: 316
currently lose_sum: 98.33276093006134
time_elpased: 1.412
batch start
#iterations: 317
currently lose_sum: 98.32351464033127
time_elpased: 1.424
batch start
#iterations: 318
currently lose_sum: 98.59657275676727
time_elpased: 1.421
batch start
#iterations: 319
currently lose_sum: 98.45693117380142
time_elpased: 1.475
start validation test
0.670360824742
0.652553269655
0.731192754966
0.689638437273
0.670254024906
63.156
batch start
#iterations: 320
currently lose_sum: 98.03735238313675
time_elpased: 1.423
batch start
#iterations: 321
currently lose_sum: 98.44803708791733
time_elpased: 1.429
batch start
#iterations: 322
currently lose_sum: 98.42761194705963
time_elpased: 1.424
batch start
#iterations: 323
currently lose_sum: 98.56220138072968
time_elpased: 1.435
batch start
#iterations: 324
currently lose_sum: 98.33625131845474
time_elpased: 1.434
batch start
#iterations: 325
currently lose_sum: 98.57754671573639
time_elpased: 1.428
batch start
#iterations: 326
currently lose_sum: 98.28388386964798
time_elpased: 1.396
batch start
#iterations: 327
currently lose_sum: 98.66696190834045
time_elpased: 1.452
batch start
#iterations: 328
currently lose_sum: 98.54922425746918
time_elpased: 1.472
batch start
#iterations: 329
currently lose_sum: 98.16560578346252
time_elpased: 1.402
batch start
#iterations: 330
currently lose_sum: 98.39369016885757
time_elpased: 1.408
batch start
#iterations: 331
currently lose_sum: 98.43595886230469
time_elpased: 1.438
batch start
#iterations: 332
currently lose_sum: 98.47630071640015
time_elpased: 1.425
batch start
#iterations: 333
currently lose_sum: 98.33563333749771
time_elpased: 1.417
batch start
#iterations: 334
currently lose_sum: 98.11292177438736
time_elpased: 1.437
batch start
#iterations: 335
currently lose_sum: 98.43047088384628
time_elpased: 1.417
batch start
#iterations: 336
currently lose_sum: 98.25041776895523
time_elpased: 1.435
batch start
#iterations: 337
currently lose_sum: 98.6284436583519
time_elpased: 1.451
batch start
#iterations: 338
currently lose_sum: 98.2670578956604
time_elpased: 1.426
batch start
#iterations: 339
currently lose_sum: 98.58522093296051
time_elpased: 1.407
start validation test
0.66206185567
0.646627702013
0.717299578059
0.680132708821
0.661964877329
63.158
batch start
#iterations: 340
currently lose_sum: 98.43014031648636
time_elpased: 1.503
batch start
#iterations: 341
currently lose_sum: 98.46691656112671
time_elpased: 1.423
batch start
#iterations: 342
currently lose_sum: 98.4564146399498
time_elpased: 1.481
batch start
#iterations: 343
currently lose_sum: 98.24862533807755
time_elpased: 1.463
batch start
#iterations: 344
currently lose_sum: 98.29792535305023
time_elpased: 1.428
batch start
#iterations: 345
currently lose_sum: 98.40652972459793
time_elpased: 1.442
batch start
#iterations: 346
currently lose_sum: 98.28827404975891
time_elpased: 1.44
batch start
#iterations: 347
currently lose_sum: 98.40289640426636
time_elpased: 1.467
batch start
#iterations: 348
currently lose_sum: 98.36940485239029
time_elpased: 1.426
batch start
#iterations: 349
currently lose_sum: 98.60071408748627
time_elpased: 1.405
batch start
#iterations: 350
currently lose_sum: 98.25752449035645
time_elpased: 1.435
batch start
#iterations: 351
currently lose_sum: 98.35566824674606
time_elpased: 1.42
batch start
#iterations: 352
currently lose_sum: 98.43805861473083
time_elpased: 1.464
batch start
#iterations: 353
currently lose_sum: 98.28459733724594
time_elpased: 1.467
batch start
#iterations: 354
currently lose_sum: 98.67335271835327
time_elpased: 1.471
batch start
#iterations: 355
currently lose_sum: 98.16412341594696
time_elpased: 1.439
batch start
#iterations: 356
currently lose_sum: 98.46068221330643
time_elpased: 1.418
batch start
#iterations: 357
currently lose_sum: 98.46719592809677
time_elpased: 1.444
batch start
#iterations: 358
currently lose_sum: 98.49497473239899
time_elpased: 1.401
batch start
#iterations: 359
currently lose_sum: 98.46548026800156
time_elpased: 1.404
start validation test
0.619484536082
0.675484743725
0.462488422353
0.549053145999
0.619760166975
63.447
batch start
#iterations: 360
currently lose_sum: 98.68602526187897
time_elpased: 1.455
batch start
#iterations: 361
currently lose_sum: 98.49069559574127
time_elpased: 1.446
batch start
#iterations: 362
currently lose_sum: 98.74711984395981
time_elpased: 1.421
batch start
#iterations: 363
currently lose_sum: 98.36920720338821
time_elpased: 1.457
batch start
#iterations: 364
currently lose_sum: 98.27548623085022
time_elpased: 1.41
batch start
#iterations: 365
currently lose_sum: 98.64514589309692
time_elpased: 1.428
batch start
#iterations: 366
currently lose_sum: 98.33404779434204
time_elpased: 1.45
batch start
#iterations: 367
currently lose_sum: 98.46483945846558
time_elpased: 1.451
batch start
#iterations: 368
currently lose_sum: 98.55718332529068
time_elpased: 1.433
batch start
#iterations: 369
currently lose_sum: 98.68468230962753
time_elpased: 1.439
batch start
#iterations: 370
currently lose_sum: 98.38121366500854
time_elpased: 1.429
batch start
#iterations: 371
currently lose_sum: 98.38438177108765
time_elpased: 1.446
batch start
#iterations: 372
currently lose_sum: 98.33629620075226
time_elpased: 1.495
batch start
#iterations: 373
currently lose_sum: 98.30212730169296
time_elpased: 1.407
batch start
#iterations: 374
currently lose_sum: 98.55831217765808
time_elpased: 1.409
batch start
#iterations: 375
currently lose_sum: 98.11722707748413
time_elpased: 1.418
batch start
#iterations: 376
currently lose_sum: 98.50118607282639
time_elpased: 1.46
batch start
#iterations: 377
currently lose_sum: 98.48181629180908
time_elpased: 1.446
batch start
#iterations: 378
currently lose_sum: 98.43682825565338
time_elpased: 1.453
batch start
#iterations: 379
currently lose_sum: 98.1621036529541
time_elpased: 1.428
start validation test
0.655103092784
0.66762685575
0.620150252135
0.643013391666
0.655164457886
63.123
batch start
#iterations: 380
currently lose_sum: 98.73244106769562
time_elpased: 1.443
batch start
#iterations: 381
currently lose_sum: 98.18275529146194
time_elpased: 1.433
batch start
#iterations: 382
currently lose_sum: 98.50370174646378
time_elpased: 1.453
batch start
#iterations: 383
currently lose_sum: 98.47018432617188
time_elpased: 1.45
batch start
#iterations: 384
currently lose_sum: 98.6952828168869
time_elpased: 1.439
batch start
#iterations: 385
currently lose_sum: 98.63304352760315
time_elpased: 1.416
batch start
#iterations: 386
currently lose_sum: 98.33496969938278
time_elpased: 1.448
batch start
#iterations: 387
currently lose_sum: 98.76734739542007
time_elpased: 1.417
batch start
#iterations: 388
currently lose_sum: 98.3782650232315
time_elpased: 1.431
batch start
#iterations: 389
currently lose_sum: 98.43152517080307
time_elpased: 1.425
batch start
#iterations: 390
currently lose_sum: 98.3950337767601
time_elpased: 1.464
batch start
#iterations: 391
currently lose_sum: 98.450244307518
time_elpased: 1.404
batch start
#iterations: 392
currently lose_sum: 98.34051042795181
time_elpased: 1.407
batch start
#iterations: 393
currently lose_sum: 98.3979304432869
time_elpased: 1.438
batch start
#iterations: 394
currently lose_sum: 98.51475781202316
time_elpased: 1.413
batch start
#iterations: 395
currently lose_sum: 98.26030498743057
time_elpased: 1.439
batch start
#iterations: 396
currently lose_sum: 98.39663082361221
time_elpased: 1.43
batch start
#iterations: 397
currently lose_sum: 98.37982445955276
time_elpased: 1.475
batch start
#iterations: 398
currently lose_sum: 98.26004272699356
time_elpased: 1.431
batch start
#iterations: 399
currently lose_sum: 98.41138410568237
time_elpased: 1.411
start validation test
0.648195876289
0.672224868985
0.580837707111
0.623198807486
0.648314133944
63.108
acc: 0.667
pre: 0.645
rec: 0.745
F1: 0.691
auc: 0.704
