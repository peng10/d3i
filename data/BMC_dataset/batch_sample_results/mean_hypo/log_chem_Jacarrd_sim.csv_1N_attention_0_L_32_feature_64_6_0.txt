start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.50148636102676
time_elpased: 1.883
batch start
#iterations: 1
currently lose_sum: 100.40993249416351
time_elpased: 1.851
batch start
#iterations: 2
currently lose_sum: 100.37700855731964
time_elpased: 1.835
batch start
#iterations: 3
currently lose_sum: 100.42149317264557
time_elpased: 1.872
batch start
#iterations: 4
currently lose_sum: 100.37345975637436
time_elpased: 1.875
batch start
#iterations: 5
currently lose_sum: 100.37882906198502
time_elpased: 1.861
batch start
#iterations: 6
currently lose_sum: 100.27577179670334
time_elpased: 1.908
batch start
#iterations: 7
currently lose_sum: 100.34405678510666
time_elpased: 1.9
batch start
#iterations: 8
currently lose_sum: 100.2952509522438
time_elpased: 1.838
batch start
#iterations: 9
currently lose_sum: 100.1430795788765
time_elpased: 1.891
batch start
#iterations: 10
currently lose_sum: 100.09501749277115
time_elpased: 1.862
batch start
#iterations: 11
currently lose_sum: 100.0919821858406
time_elpased: 1.874
batch start
#iterations: 12
currently lose_sum: 100.11803358793259
time_elpased: 1.832
batch start
#iterations: 13
currently lose_sum: 100.08552426099777
time_elpased: 1.896
batch start
#iterations: 14
currently lose_sum: 100.03047269582748
time_elpased: 1.862
batch start
#iterations: 15
currently lose_sum: 100.13098311424255
time_elpased: 1.885
batch start
#iterations: 16
currently lose_sum: 99.95116555690765
time_elpased: 1.914
batch start
#iterations: 17
currently lose_sum: 100.42279547452927
time_elpased: 1.828
batch start
#iterations: 18
currently lose_sum: 100.20947551727295
time_elpased: 1.838
batch start
#iterations: 19
currently lose_sum: 100.00223016738892
time_elpased: 1.914
start validation test
0.584278350515
0.573684210526
0.661829782855
0.614612701295
0.584142197015
66.301
batch start
#iterations: 20
currently lose_sum: 100.0877416729927
time_elpased: 1.853
batch start
#iterations: 21
currently lose_sum: 100.1004987359047
time_elpased: 1.915
batch start
#iterations: 22
currently lose_sum: 99.8268233537674
time_elpased: 1.853
batch start
#iterations: 23
currently lose_sum: 100.16191893815994
time_elpased: 1.878
batch start
#iterations: 24
currently lose_sum: 99.63338482379913
time_elpased: 1.858
batch start
#iterations: 25
currently lose_sum: 99.9378719329834
time_elpased: 1.907
batch start
#iterations: 26
currently lose_sum: 100.47984862327576
time_elpased: 1.84
batch start
#iterations: 27
currently lose_sum: 99.96055430173874
time_elpased: 1.89
batch start
#iterations: 28
currently lose_sum: 99.67645919322968
time_elpased: 1.838
batch start
#iterations: 29
currently lose_sum: 100.00183486938477
time_elpased: 1.881
batch start
#iterations: 30
currently lose_sum: 100.50396925210953
time_elpased: 1.852
batch start
#iterations: 31
currently lose_sum: 100.5004774928093
time_elpased: 1.872
batch start
#iterations: 32
currently lose_sum: 100.49210274219513
time_elpased: 1.846
batch start
#iterations: 33
currently lose_sum: 100.38707882165909
time_elpased: 1.933
batch start
#iterations: 34
currently lose_sum: 99.85959005355835
time_elpased: 1.873
batch start
#iterations: 35
currently lose_sum: 99.77932721376419
time_elpased: 1.868
batch start
#iterations: 36
currently lose_sum: 100.50708222389221
time_elpased: 1.887
batch start
#iterations: 37
currently lose_sum: 100.5065153837204
time_elpased: 1.903
batch start
#iterations: 38
currently lose_sum: 100.50591492652893
time_elpased: 1.812
batch start
#iterations: 39
currently lose_sum: 100.50489735603333
time_elpased: 1.86
start validation test
0.528298969072
0.526850094877
0.571472676752
0.548254924224
0.528223170969
67.233
batch start
#iterations: 40
currently lose_sum: 100.50300425291061
time_elpased: 1.887
batch start
#iterations: 41
currently lose_sum: 100.50074708461761
time_elpased: 1.836
batch start
#iterations: 42
currently lose_sum: 100.48308843374252
time_elpased: 1.827
batch start
#iterations: 43
currently lose_sum: 100.01051491498947
time_elpased: 1.932
batch start
#iterations: 44
currently lose_sum: 99.89658397436142
time_elpased: 1.867
batch start
#iterations: 45
currently lose_sum: 100.23411250114441
time_elpased: 1.965
batch start
#iterations: 46
currently lose_sum: 100.50563025474548
time_elpased: 1.85
batch start
#iterations: 47
currently lose_sum: 100.5055143237114
time_elpased: 1.84
batch start
#iterations: 48
currently lose_sum: 100.50534999370575
time_elpased: 1.826
batch start
#iterations: 49
currently lose_sum: 100.50513631105423
time_elpased: 1.87
batch start
#iterations: 50
currently lose_sum: 100.50474739074707
time_elpased: 1.906
batch start
#iterations: 51
currently lose_sum: 100.50453299283981
time_elpased: 1.851
batch start
#iterations: 52
currently lose_sum: 100.50385874509811
time_elpased: 1.857
batch start
#iterations: 53
currently lose_sum: 100.50240671634674
time_elpased: 1.884
batch start
#iterations: 54
currently lose_sum: 100.49699002504349
time_elpased: 1.945
batch start
#iterations: 55
currently lose_sum: 100.31676203012466
time_elpased: 1.873
batch start
#iterations: 56
currently lose_sum: 100.22735607624054
time_elpased: 1.867
batch start
#iterations: 57
currently lose_sum: 100.4980064034462
time_elpased: 1.869
batch start
#iterations: 58
currently lose_sum: 100.37609803676605
time_elpased: 1.923
batch start
#iterations: 59
currently lose_sum: 100.01035571098328
time_elpased: 1.889
start validation test
0.49912371134
0.0
0.0
nan
0.5
67.326
acc: 0.586
pre: 0.575
rec: 0.668
F1: 0.618
auc: 0.586
