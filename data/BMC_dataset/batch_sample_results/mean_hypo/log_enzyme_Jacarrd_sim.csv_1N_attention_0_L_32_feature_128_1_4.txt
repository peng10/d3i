start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.81095016002655
time_elpased: 1.599
batch start
#iterations: 1
currently lose_sum: 100.39400923252106
time_elpased: 1.59
batch start
#iterations: 2
currently lose_sum: 100.37418645620346
time_elpased: 1.584
batch start
#iterations: 3
currently lose_sum: 100.07240527868271
time_elpased: 1.594
batch start
#iterations: 4
currently lose_sum: 100.11005532741547
time_elpased: 1.566
batch start
#iterations: 5
currently lose_sum: 99.9699074625969
time_elpased: 1.59
batch start
#iterations: 6
currently lose_sum: 99.92828726768494
time_elpased: 1.59
batch start
#iterations: 7
currently lose_sum: 99.94758552312851
time_elpased: 1.635
batch start
#iterations: 8
currently lose_sum: 99.99106240272522
time_elpased: 1.6
batch start
#iterations: 9
currently lose_sum: 99.88691526651382
time_elpased: 1.578
batch start
#iterations: 10
currently lose_sum: 99.8171523809433
time_elpased: 1.566
batch start
#iterations: 11
currently lose_sum: 99.99337816238403
time_elpased: 1.581
batch start
#iterations: 12
currently lose_sum: 99.75776171684265
time_elpased: 1.566
batch start
#iterations: 13
currently lose_sum: 99.92599296569824
time_elpased: 1.604
batch start
#iterations: 14
currently lose_sum: 99.63602435588837
time_elpased: 1.598
batch start
#iterations: 15
currently lose_sum: 99.71913248300552
time_elpased: 1.589
batch start
#iterations: 16
currently lose_sum: 99.7029281258583
time_elpased: 1.597
batch start
#iterations: 17
currently lose_sum: 99.75478202104568
time_elpased: 1.581
batch start
#iterations: 18
currently lose_sum: 99.5969123840332
time_elpased: 1.581
batch start
#iterations: 19
currently lose_sum: 99.79088479280472
time_elpased: 1.569
start validation test
0.597577319588
0.616038882139
0.521765977153
0.564996935421
0.597710418092
65.271
batch start
#iterations: 20
currently lose_sum: 99.77171605825424
time_elpased: 1.576
batch start
#iterations: 21
currently lose_sum: 99.5466319322586
time_elpased: 1.567
batch start
#iterations: 22
currently lose_sum: 99.56193786859512
time_elpased: 1.588
batch start
#iterations: 23
currently lose_sum: 99.78798770904541
time_elpased: 1.572
batch start
#iterations: 24
currently lose_sum: 99.56479376554489
time_elpased: 1.573
batch start
#iterations: 25
currently lose_sum: 99.56456422805786
time_elpased: 1.569
batch start
#iterations: 26
currently lose_sum: 99.70237302780151
time_elpased: 1.602
batch start
#iterations: 27
currently lose_sum: 99.45142716169357
time_elpased: 1.588
batch start
#iterations: 28
currently lose_sum: 99.39836585521698
time_elpased: 1.59
batch start
#iterations: 29
currently lose_sum: 99.39962387084961
time_elpased: 1.587
batch start
#iterations: 30
currently lose_sum: 99.6150734424591
time_elpased: 1.567
batch start
#iterations: 31
currently lose_sum: 99.38496482372284
time_elpased: 1.593
batch start
#iterations: 32
currently lose_sum: 99.53838217258453
time_elpased: 1.611
batch start
#iterations: 33
currently lose_sum: 99.49357903003693
time_elpased: 1.594
batch start
#iterations: 34
currently lose_sum: 99.54162234067917
time_elpased: 1.576
batch start
#iterations: 35
currently lose_sum: 99.46868234872818
time_elpased: 1.596
batch start
#iterations: 36
currently lose_sum: 99.36111479997635
time_elpased: 1.592
batch start
#iterations: 37
currently lose_sum: 99.52765452861786
time_elpased: 1.567
batch start
#iterations: 38
currently lose_sum: 99.52758640050888
time_elpased: 1.575
batch start
#iterations: 39
currently lose_sum: 99.35629951953888
time_elpased: 1.594
start validation test
0.603556701031
0.644466628637
0.465061232891
0.540259429733
0.603799851187
65.032
batch start
#iterations: 40
currently lose_sum: 99.50969588756561
time_elpased: 1.571
batch start
#iterations: 41
currently lose_sum: 99.42510092258453
time_elpased: 1.566
batch start
#iterations: 42
currently lose_sum: 99.54213804006577
time_elpased: 1.572
batch start
#iterations: 43
currently lose_sum: 99.53881901502609
time_elpased: 1.575
batch start
#iterations: 44
currently lose_sum: 99.3029870390892
time_elpased: 1.587
batch start
#iterations: 45
currently lose_sum: 99.36066889762878
time_elpased: 1.593
batch start
#iterations: 46
currently lose_sum: 99.37777501344681
time_elpased: 1.594
batch start
#iterations: 47
currently lose_sum: 99.69005209207535
time_elpased: 1.604
batch start
#iterations: 48
currently lose_sum: 99.48659890890121
time_elpased: 1.586
batch start
#iterations: 49
currently lose_sum: 99.41726619005203
time_elpased: 1.595
batch start
#iterations: 50
currently lose_sum: 99.39484196901321
time_elpased: 1.578
batch start
#iterations: 51
currently lose_sum: 99.41166031360626
time_elpased: 1.602
batch start
#iterations: 52
currently lose_sum: 99.5605046749115
time_elpased: 1.577
batch start
#iterations: 53
currently lose_sum: 99.18667662143707
time_elpased: 1.578
batch start
#iterations: 54
currently lose_sum: 99.40776932239532
time_elpased: 1.572
batch start
#iterations: 55
currently lose_sum: 99.260791182518
time_elpased: 1.591
batch start
#iterations: 56
currently lose_sum: 99.40411275625229
time_elpased: 1.587
batch start
#iterations: 57
currently lose_sum: 99.52191919088364
time_elpased: 1.574
batch start
#iterations: 58
currently lose_sum: 99.41572040319443
time_elpased: 1.57
batch start
#iterations: 59
currently lose_sum: 99.57543641328812
time_elpased: 1.574
start validation test
0.595567010309
0.647998734377
0.421529278584
0.510786881157
0.595872560391
65.018
batch start
#iterations: 60
currently lose_sum: 99.44471257925034
time_elpased: 1.591
batch start
#iterations: 61
currently lose_sum: 99.47170585393906
time_elpased: 1.593
batch start
#iterations: 62
currently lose_sum: 99.34569942951202
time_elpased: 1.573
batch start
#iterations: 63
currently lose_sum: 99.1601710319519
time_elpased: 1.608
batch start
#iterations: 64
currently lose_sum: 99.59434574842453
time_elpased: 1.577
batch start
#iterations: 65
currently lose_sum: 99.30233007669449
time_elpased: 1.571
batch start
#iterations: 66
currently lose_sum: 99.37213039398193
time_elpased: 1.572
batch start
#iterations: 67
currently lose_sum: 99.21630388498306
time_elpased: 1.584
batch start
#iterations: 68
currently lose_sum: 99.25139278173447
time_elpased: 1.604
batch start
#iterations: 69
currently lose_sum: 99.516850233078
time_elpased: 1.582
batch start
#iterations: 70
currently lose_sum: 99.3668584227562
time_elpased: 1.588
batch start
#iterations: 71
currently lose_sum: 99.46336007118225
time_elpased: 1.566
batch start
#iterations: 72
currently lose_sum: 99.17075943946838
time_elpased: 1.569
batch start
#iterations: 73
currently lose_sum: 99.43290209770203
time_elpased: 1.568
batch start
#iterations: 74
currently lose_sum: 99.2995754480362
time_elpased: 1.578
batch start
#iterations: 75
currently lose_sum: 99.49658530950546
time_elpased: 1.574
batch start
#iterations: 76
currently lose_sum: 99.29800373315811
time_elpased: 1.577
batch start
#iterations: 77
currently lose_sum: 99.39662355184555
time_elpased: 1.587
batch start
#iterations: 78
currently lose_sum: 99.45998829603195
time_elpased: 1.582
batch start
#iterations: 79
currently lose_sum: 99.2058921456337
time_elpased: 1.569
start validation test
0.607989690722
0.642433234421
0.490171863744
0.556067946997
0.608196538089
64.725
batch start
#iterations: 80
currently lose_sum: 99.43328660726547
time_elpased: 1.575
batch start
#iterations: 81
currently lose_sum: 99.26394420862198
time_elpased: 1.618
batch start
#iterations: 82
currently lose_sum: 99.47351449728012
time_elpased: 1.583
batch start
#iterations: 83
currently lose_sum: 99.24178260564804
time_elpased: 1.592
batch start
#iterations: 84
currently lose_sum: 99.26911467313766
time_elpased: 1.587
batch start
#iterations: 85
currently lose_sum: 99.38186430931091
time_elpased: 1.607
batch start
#iterations: 86
currently lose_sum: 99.48360735177994
time_elpased: 1.583
batch start
#iterations: 87
currently lose_sum: 99.26892817020416
time_elpased: 1.605
batch start
#iterations: 88
currently lose_sum: 99.32235729694366
time_elpased: 1.64
batch start
#iterations: 89
currently lose_sum: 99.35815340280533
time_elpased: 1.586
batch start
#iterations: 90
currently lose_sum: 99.28996694087982
time_elpased: 1.577
batch start
#iterations: 91
currently lose_sum: 99.55058860778809
time_elpased: 1.586
batch start
#iterations: 92
currently lose_sum: 99.27079457044601
time_elpased: 1.59
batch start
#iterations: 93
currently lose_sum: 99.20335471630096
time_elpased: 1.58
batch start
#iterations: 94
currently lose_sum: 99.22681784629822
time_elpased: 1.573
batch start
#iterations: 95
currently lose_sum: 99.42135679721832
time_elpased: 1.574
batch start
#iterations: 96
currently lose_sum: 99.48131531476974
time_elpased: 1.585
batch start
#iterations: 97
currently lose_sum: 99.44145607948303
time_elpased: 1.595
batch start
#iterations: 98
currently lose_sum: 99.32279217243195
time_elpased: 1.582
batch start
#iterations: 99
currently lose_sum: 99.33587831258774
time_elpased: 1.594
start validation test
0.615360824742
0.64095511939
0.527631985181
0.57879882592
0.615514846251
64.597
batch start
#iterations: 100
currently lose_sum: 99.28319215774536
time_elpased: 1.575
batch start
#iterations: 101
currently lose_sum: 99.20684295892715
time_elpased: 1.58
batch start
#iterations: 102
currently lose_sum: 99.31114143133163
time_elpased: 1.583
batch start
#iterations: 103
currently lose_sum: 99.30876529216766
time_elpased: 1.607
batch start
#iterations: 104
currently lose_sum: 99.28625959157944
time_elpased: 1.597
batch start
#iterations: 105
currently lose_sum: 99.39480024576187
time_elpased: 1.575
batch start
#iterations: 106
currently lose_sum: 99.22610187530518
time_elpased: 1.584
batch start
#iterations: 107
currently lose_sum: 99.36377668380737
time_elpased: 1.64
batch start
#iterations: 108
currently lose_sum: 99.2427464723587
time_elpased: 1.577
batch start
#iterations: 109
currently lose_sum: 99.43825203180313
time_elpased: 1.597
batch start
#iterations: 110
currently lose_sum: 99.25991296768188
time_elpased: 1.629
batch start
#iterations: 111
currently lose_sum: 99.41287177801132
time_elpased: 1.574
batch start
#iterations: 112
currently lose_sum: 99.36601936817169
time_elpased: 1.601
batch start
#iterations: 113
currently lose_sum: 99.2150330543518
time_elpased: 1.591
batch start
#iterations: 114
currently lose_sum: 99.26021319627762
time_elpased: 1.595
batch start
#iterations: 115
currently lose_sum: 99.16636085510254
time_elpased: 1.577
batch start
#iterations: 116
currently lose_sum: 99.33647632598877
time_elpased: 1.609
batch start
#iterations: 117
currently lose_sum: 99.28843402862549
time_elpased: 1.6
batch start
#iterations: 118
currently lose_sum: 99.24872523546219
time_elpased: 1.612
batch start
#iterations: 119
currently lose_sum: 99.24644708633423
time_elpased: 1.582
start validation test
0.600206185567
0.651381812568
0.434187506432
0.521057181672
0.600497656965
64.734
batch start
#iterations: 120
currently lose_sum: 99.20576614141464
time_elpased: 1.596
batch start
#iterations: 121
currently lose_sum: 99.33019161224365
time_elpased: 1.605
batch start
#iterations: 122
currently lose_sum: 99.4004180431366
time_elpased: 1.633
batch start
#iterations: 123
currently lose_sum: 99.35555821657181
time_elpased: 1.61
batch start
#iterations: 124
currently lose_sum: 99.36060559749603
time_elpased: 1.588
batch start
#iterations: 125
currently lose_sum: 99.04669165611267
time_elpased: 1.626
batch start
#iterations: 126
currently lose_sum: 99.22764080762863
time_elpased: 1.592
batch start
#iterations: 127
currently lose_sum: 99.33077973127365
time_elpased: 1.588
batch start
#iterations: 128
currently lose_sum: 99.2072371840477
time_elpased: 1.586
batch start
#iterations: 129
currently lose_sum: 99.0808966755867
time_elpased: 1.61
batch start
#iterations: 130
currently lose_sum: 99.30884450674057
time_elpased: 1.59
batch start
#iterations: 131
currently lose_sum: 99.31077671051025
time_elpased: 1.582
batch start
#iterations: 132
currently lose_sum: 99.26888173818588
time_elpased: 1.59
batch start
#iterations: 133
currently lose_sum: 99.3173041343689
time_elpased: 1.583
batch start
#iterations: 134
currently lose_sum: 99.24673718214035
time_elpased: 1.611
batch start
#iterations: 135
currently lose_sum: 99.39279448986053
time_elpased: 1.587
batch start
#iterations: 136
currently lose_sum: 99.14957350492477
time_elpased: 1.589
batch start
#iterations: 137
currently lose_sum: 99.21646416187286
time_elpased: 1.614
batch start
#iterations: 138
currently lose_sum: 99.2830821275711
time_elpased: 1.566
batch start
#iterations: 139
currently lose_sum: 99.32922768592834
time_elpased: 1.592
start validation test
0.614690721649
0.651527439849
0.496037871771
0.563248612328
0.614899035028
64.530
batch start
#iterations: 140
currently lose_sum: 99.29656565189362
time_elpased: 1.588
batch start
#iterations: 141
currently lose_sum: 99.48920327425003
time_elpased: 1.578
batch start
#iterations: 142
currently lose_sum: 99.42227977514267
time_elpased: 1.606
batch start
#iterations: 143
currently lose_sum: 99.33382433652878
time_elpased: 1.588
batch start
#iterations: 144
currently lose_sum: 99.09917265176773
time_elpased: 1.598
batch start
#iterations: 145
currently lose_sum: 99.33789217472076
time_elpased: 1.605
batch start
#iterations: 146
currently lose_sum: 99.05359750986099
time_elpased: 1.598
batch start
#iterations: 147
currently lose_sum: 99.35488891601562
time_elpased: 1.605
batch start
#iterations: 148
currently lose_sum: 99.36037081480026
time_elpased: 1.592
batch start
#iterations: 149
currently lose_sum: 99.19658344984055
time_elpased: 1.604
batch start
#iterations: 150
currently lose_sum: 99.06463551521301
time_elpased: 1.597
batch start
#iterations: 151
currently lose_sum: 99.16121137142181
time_elpased: 1.587
batch start
#iterations: 152
currently lose_sum: 99.2011046409607
time_elpased: 1.582
batch start
#iterations: 153
currently lose_sum: 99.4348760843277
time_elpased: 1.589
batch start
#iterations: 154
currently lose_sum: 99.03880286216736
time_elpased: 1.596
batch start
#iterations: 155
currently lose_sum: 99.13932079076767
time_elpased: 1.62
batch start
#iterations: 156
currently lose_sum: 99.4940675497055
time_elpased: 1.577
batch start
#iterations: 157
currently lose_sum: 99.27666449546814
time_elpased: 1.588
batch start
#iterations: 158
currently lose_sum: 99.28104919195175
time_elpased: 1.581
batch start
#iterations: 159
currently lose_sum: 99.25757050514221
time_elpased: 1.569
start validation test
0.620927835052
0.635913953756
0.568899866214
0.600543183053
0.621019178176
64.472
batch start
#iterations: 160
currently lose_sum: 99.31086540222168
time_elpased: 1.606
batch start
#iterations: 161
currently lose_sum: 99.3040526509285
time_elpased: 1.591
batch start
#iterations: 162
currently lose_sum: 99.26250958442688
time_elpased: 1.583
batch start
#iterations: 163
currently lose_sum: 99.16787338256836
time_elpased: 1.619
batch start
#iterations: 164
currently lose_sum: 99.15374100208282
time_elpased: 1.581
batch start
#iterations: 165
currently lose_sum: 99.24157160520554
time_elpased: 1.573
batch start
#iterations: 166
currently lose_sum: 99.01120710372925
time_elpased: 1.575
batch start
#iterations: 167
currently lose_sum: 99.20837622880936
time_elpased: 1.582
batch start
#iterations: 168
currently lose_sum: 99.40482419729233
time_elpased: 1.583
batch start
#iterations: 169
currently lose_sum: 99.31059694290161
time_elpased: 1.579
batch start
#iterations: 170
currently lose_sum: 99.09852641820908
time_elpased: 1.603
batch start
#iterations: 171
currently lose_sum: 99.12223082780838
time_elpased: 1.603
batch start
#iterations: 172
currently lose_sum: 99.27560526132584
time_elpased: 1.609
batch start
#iterations: 173
currently lose_sum: 99.23338162899017
time_elpased: 1.598
batch start
#iterations: 174
currently lose_sum: 99.31180620193481
time_elpased: 1.582
batch start
#iterations: 175
currently lose_sum: 99.23801231384277
time_elpased: 1.596
batch start
#iterations: 176
currently lose_sum: 99.22920650243759
time_elpased: 1.596
batch start
#iterations: 177
currently lose_sum: 99.15905046463013
time_elpased: 1.601
batch start
#iterations: 178
currently lose_sum: 99.27415215969086
time_elpased: 1.603
batch start
#iterations: 179
currently lose_sum: 99.40024971961975
time_elpased: 1.611
start validation test
0.622113402062
0.643251681076
0.551301842132
0.593737877528
0.622237722677
64.517
batch start
#iterations: 180
currently lose_sum: 99.33225303888321
time_elpased: 1.578
batch start
#iterations: 181
currently lose_sum: 99.23011976480484
time_elpased: 1.597
batch start
#iterations: 182
currently lose_sum: 99.04543572664261
time_elpased: 1.579
batch start
#iterations: 183
currently lose_sum: 99.189508497715
time_elpased: 1.581
batch start
#iterations: 184
currently lose_sum: 99.11027479171753
time_elpased: 1.603
batch start
#iterations: 185
currently lose_sum: 99.02659267187119
time_elpased: 1.59
batch start
#iterations: 186
currently lose_sum: 99.34806495904922
time_elpased: 1.581
batch start
#iterations: 187
currently lose_sum: 99.3811987042427
time_elpased: 1.578
batch start
#iterations: 188
currently lose_sum: 98.99843502044678
time_elpased: 1.573
batch start
#iterations: 189
currently lose_sum: 99.2267718911171
time_elpased: 1.588
batch start
#iterations: 190
currently lose_sum: 99.27809828519821
time_elpased: 1.605
batch start
#iterations: 191
currently lose_sum: 99.25535821914673
time_elpased: 1.593
batch start
#iterations: 192
currently lose_sum: 99.31042695045471
time_elpased: 1.589
batch start
#iterations: 193
currently lose_sum: 99.17724812030792
time_elpased: 1.575
batch start
#iterations: 194
currently lose_sum: 99.04798048734665
time_elpased: 1.6
batch start
#iterations: 195
currently lose_sum: 99.13548529148102
time_elpased: 1.602
batch start
#iterations: 196
currently lose_sum: 98.90768647193909
time_elpased: 1.576
batch start
#iterations: 197
currently lose_sum: 99.1779453754425
time_elpased: 1.603
batch start
#iterations: 198
currently lose_sum: 99.0437000989914
time_elpased: 1.601
batch start
#iterations: 199
currently lose_sum: 99.06110912561417
time_elpased: 1.607
start validation test
0.609381443299
0.648727576137
0.480086446434
0.55180979418
0.609608440608
64.440
batch start
#iterations: 200
currently lose_sum: 99.2419804930687
time_elpased: 1.611
batch start
#iterations: 201
currently lose_sum: 99.16687953472137
time_elpased: 1.574
batch start
#iterations: 202
currently lose_sum: 99.23885494470596
time_elpased: 1.597
batch start
#iterations: 203
currently lose_sum: 99.23614835739136
time_elpased: 1.586
batch start
#iterations: 204
currently lose_sum: 99.11070734262466
time_elpased: 1.597
batch start
#iterations: 205
currently lose_sum: 99.19770550727844
time_elpased: 1.614
batch start
#iterations: 206
currently lose_sum: 99.01007974147797
time_elpased: 1.629
batch start
#iterations: 207
currently lose_sum: 99.17964673042297
time_elpased: 1.598
batch start
#iterations: 208
currently lose_sum: 99.07341796159744
time_elpased: 1.589
batch start
#iterations: 209
currently lose_sum: 99.06064015626907
time_elpased: 1.606
batch start
#iterations: 210
currently lose_sum: 98.95152443647385
time_elpased: 1.617
batch start
#iterations: 211
currently lose_sum: 99.22009640932083
time_elpased: 1.601
batch start
#iterations: 212
currently lose_sum: 99.09212440252304
time_elpased: 1.577
batch start
#iterations: 213
currently lose_sum: 99.01276844739914
time_elpased: 1.602
batch start
#iterations: 214
currently lose_sum: 99.43339383602142
time_elpased: 1.586
batch start
#iterations: 215
currently lose_sum: 98.93990975618362
time_elpased: 1.6
batch start
#iterations: 216
currently lose_sum: 99.07371789216995
time_elpased: 1.609
batch start
#iterations: 217
currently lose_sum: 99.11527144908905
time_elpased: 1.614
batch start
#iterations: 218
currently lose_sum: 99.21789485216141
time_elpased: 1.589
batch start
#iterations: 219
currently lose_sum: 99.21042078733444
time_elpased: 1.585
start validation test
0.602628865979
0.655997513984
0.434496243697
0.522751191729
0.60292404873
64.586
batch start
#iterations: 220
currently lose_sum: 99.3326513171196
time_elpased: 1.683
batch start
#iterations: 221
currently lose_sum: 99.12382388114929
time_elpased: 1.61
batch start
#iterations: 222
currently lose_sum: 99.32942986488342
time_elpased: 1.597
batch start
#iterations: 223
currently lose_sum: 99.20536214113235
time_elpased: 1.617
batch start
#iterations: 224
currently lose_sum: 99.1811175942421
time_elpased: 1.594
batch start
#iterations: 225
currently lose_sum: 99.03753435611725
time_elpased: 1.611
batch start
#iterations: 226
currently lose_sum: 98.92143177986145
time_elpased: 1.583
batch start
#iterations: 227
currently lose_sum: 98.99927514791489
time_elpased: 1.596
batch start
#iterations: 228
currently lose_sum: 99.505686044693
time_elpased: 1.597
batch start
#iterations: 229
currently lose_sum: 99.34759432077408
time_elpased: 1.58
batch start
#iterations: 230
currently lose_sum: 99.07241761684418
time_elpased: 1.571
batch start
#iterations: 231
currently lose_sum: 99.12538915872574
time_elpased: 1.604
batch start
#iterations: 232
currently lose_sum: 99.42778879404068
time_elpased: 1.581
batch start
#iterations: 233
currently lose_sum: 99.05480545759201
time_elpased: 1.595
batch start
#iterations: 234
currently lose_sum: 99.19111049175262
time_elpased: 1.592
batch start
#iterations: 235
currently lose_sum: 99.1709411740303
time_elpased: 1.599
batch start
#iterations: 236
currently lose_sum: 98.9804477095604
time_elpased: 1.583
batch start
#iterations: 237
currently lose_sum: 99.26969522237778
time_elpased: 1.606
batch start
#iterations: 238
currently lose_sum: 99.157395362854
time_elpased: 1.601
batch start
#iterations: 239
currently lose_sum: 99.19649028778076
time_elpased: 1.6
start validation test
0.617319587629
0.645476462378
0.523515488319
0.57813387885
0.617484275193
64.352
batch start
#iterations: 240
currently lose_sum: 99.36548322439194
time_elpased: 1.6
batch start
#iterations: 241
currently lose_sum: 99.21017807722092
time_elpased: 1.583
batch start
#iterations: 242
currently lose_sum: 99.32202780246735
time_elpased: 1.576
batch start
#iterations: 243
currently lose_sum: 99.26980984210968
time_elpased: 1.598
batch start
#iterations: 244
currently lose_sum: 99.37020671367645
time_elpased: 1.6
batch start
#iterations: 245
currently lose_sum: 99.1514276266098
time_elpased: 1.592
batch start
#iterations: 246
currently lose_sum: 99.13467127084732
time_elpased: 1.611
batch start
#iterations: 247
currently lose_sum: 98.94099575281143
time_elpased: 1.623
batch start
#iterations: 248
currently lose_sum: 99.30916625261307
time_elpased: 1.597
batch start
#iterations: 249
currently lose_sum: 99.16188305616379
time_elpased: 1.607
batch start
#iterations: 250
currently lose_sum: 99.26178693771362
time_elpased: 1.599
batch start
#iterations: 251
currently lose_sum: 99.0062125325203
time_elpased: 1.636
batch start
#iterations: 252
currently lose_sum: 99.26317656040192
time_elpased: 1.598
batch start
#iterations: 253
currently lose_sum: 98.90316462516785
time_elpased: 1.591
batch start
#iterations: 254
currently lose_sum: 99.11144638061523
time_elpased: 1.581
batch start
#iterations: 255
currently lose_sum: 99.17499369382858
time_elpased: 1.579
batch start
#iterations: 256
currently lose_sum: 99.09046882390976
time_elpased: 1.591
batch start
#iterations: 257
currently lose_sum: 99.37965697050095
time_elpased: 1.599
batch start
#iterations: 258
currently lose_sum: 98.95867258310318
time_elpased: 1.6
batch start
#iterations: 259
currently lose_sum: 98.94978141784668
time_elpased: 1.608
start validation test
0.619742268041
0.643593519882
0.5396727385
0.587069689337
0.61988284245
64.279
batch start
#iterations: 260
currently lose_sum: 99.19869619607925
time_elpased: 1.58
batch start
#iterations: 261
currently lose_sum: 99.09750491380692
time_elpased: 1.6
batch start
#iterations: 262
currently lose_sum: 99.13886600732803
time_elpased: 1.618
batch start
#iterations: 263
currently lose_sum: 99.20673656463623
time_elpased: 1.584
batch start
#iterations: 264
currently lose_sum: 99.31518989801407
time_elpased: 1.612
batch start
#iterations: 265
currently lose_sum: 99.15337538719177
time_elpased: 1.591
batch start
#iterations: 266
currently lose_sum: 99.29242783784866
time_elpased: 1.578
batch start
#iterations: 267
currently lose_sum: 99.33980464935303
time_elpased: 1.592
batch start
#iterations: 268
currently lose_sum: 99.27352064847946
time_elpased: 1.602
batch start
#iterations: 269
currently lose_sum: 99.33726036548615
time_elpased: 1.591
batch start
#iterations: 270
currently lose_sum: 99.06654971837997
time_elpased: 1.605
batch start
#iterations: 271
currently lose_sum: 99.01541638374329
time_elpased: 1.587
batch start
#iterations: 272
currently lose_sum: 99.39195311069489
time_elpased: 1.616
batch start
#iterations: 273
currently lose_sum: 99.13493829965591
time_elpased: 1.61
batch start
#iterations: 274
currently lose_sum: 99.49657219648361
time_elpased: 1.602
batch start
#iterations: 275
currently lose_sum: 99.23475760221481
time_elpased: 1.634
batch start
#iterations: 276
currently lose_sum: 99.01073080301285
time_elpased: 1.582
batch start
#iterations: 277
currently lose_sum: 99.24822199344635
time_elpased: 1.6
batch start
#iterations: 278
currently lose_sum: 99.1152058839798
time_elpased: 1.617
batch start
#iterations: 279
currently lose_sum: 99.03299105167389
time_elpased: 1.606
start validation test
0.618092783505
0.656623235613
0.497890295359
0.566344746854
0.618303817513
64.352
batch start
#iterations: 280
currently lose_sum: 99.27269971370697
time_elpased: 1.578
batch start
#iterations: 281
currently lose_sum: 98.99878460168839
time_elpased: 1.591
batch start
#iterations: 282
currently lose_sum: 99.06493675708771
time_elpased: 1.625
batch start
#iterations: 283
currently lose_sum: 99.33452689647675
time_elpased: 1.614
batch start
#iterations: 284
currently lose_sum: 99.08381569385529
time_elpased: 1.609
batch start
#iterations: 285
currently lose_sum: 99.09814637899399
time_elpased: 1.608
batch start
#iterations: 286
currently lose_sum: 99.26604789495468
time_elpased: 1.625
batch start
#iterations: 287
currently lose_sum: 99.10954886674881
time_elpased: 1.587
batch start
#iterations: 288
currently lose_sum: 99.13100844621658
time_elpased: 1.604
batch start
#iterations: 289
currently lose_sum: 99.013487637043
time_elpased: 1.615
batch start
#iterations: 290
currently lose_sum: 99.33984810113907
time_elpased: 1.622
batch start
#iterations: 291
currently lose_sum: 98.83886170387268
time_elpased: 1.631
batch start
#iterations: 292
currently lose_sum: 99.14933633804321
time_elpased: 1.584
batch start
#iterations: 293
currently lose_sum: 99.236328125
time_elpased: 1.592
batch start
#iterations: 294
currently lose_sum: 99.11049211025238
time_elpased: 1.596
batch start
#iterations: 295
currently lose_sum: 99.18289595842361
time_elpased: 1.611
batch start
#iterations: 296
currently lose_sum: 99.13036197423935
time_elpased: 1.588
batch start
#iterations: 297
currently lose_sum: 99.3830269575119
time_elpased: 1.613
batch start
#iterations: 298
currently lose_sum: 99.19202536344528
time_elpased: 1.591
batch start
#iterations: 299
currently lose_sum: 99.25652730464935
time_elpased: 1.601
start validation test
0.62087628866
0.654419456067
0.515076669754
0.5764468759
0.621062036209
64.347
batch start
#iterations: 300
currently lose_sum: 99.09740263223648
time_elpased: 1.572
batch start
#iterations: 301
currently lose_sum: 99.05254757404327
time_elpased: 1.587
batch start
#iterations: 302
currently lose_sum: 98.95855122804642
time_elpased: 1.581
batch start
#iterations: 303
currently lose_sum: 99.03970408439636
time_elpased: 1.598
batch start
#iterations: 304
currently lose_sum: 99.23143899440765
time_elpased: 1.618
batch start
#iterations: 305
currently lose_sum: 99.25695741176605
time_elpased: 1.609
batch start
#iterations: 306
currently lose_sum: 99.12299931049347
time_elpased: 1.603
batch start
#iterations: 307
currently lose_sum: 98.96209436655045
time_elpased: 1.585
batch start
#iterations: 308
currently lose_sum: 99.30440366268158
time_elpased: 1.608
batch start
#iterations: 309
currently lose_sum: 99.1315336227417
time_elpased: 1.587
batch start
#iterations: 310
currently lose_sum: 99.3692062497139
time_elpased: 1.612
batch start
#iterations: 311
currently lose_sum: 99.08406263589859
time_elpased: 1.602
batch start
#iterations: 312
currently lose_sum: 99.08908653259277
time_elpased: 1.609
batch start
#iterations: 313
currently lose_sum: 99.07712465524673
time_elpased: 1.578
batch start
#iterations: 314
currently lose_sum: 99.19719648361206
time_elpased: 1.599
batch start
#iterations: 315
currently lose_sum: 99.20798146724701
time_elpased: 1.582
batch start
#iterations: 316
currently lose_sum: 99.16617614030838
time_elpased: 1.609
batch start
#iterations: 317
currently lose_sum: 99.1467695236206
time_elpased: 1.57
batch start
#iterations: 318
currently lose_sum: 99.04901313781738
time_elpased: 1.629
batch start
#iterations: 319
currently lose_sum: 98.98024970293045
time_elpased: 1.571
start validation test
0.616494845361
0.654582484725
0.496140784193
0.564453811029
0.616706145479
64.352
batch start
#iterations: 320
currently lose_sum: 99.10317671298981
time_elpased: 1.607
batch start
#iterations: 321
currently lose_sum: 98.9212104678154
time_elpased: 1.573
batch start
#iterations: 322
currently lose_sum: 99.37386184930801
time_elpased: 1.592
batch start
#iterations: 323
currently lose_sum: 99.24891597032547
time_elpased: 1.579
batch start
#iterations: 324
currently lose_sum: 99.22500735521317
time_elpased: 1.593
batch start
#iterations: 325
currently lose_sum: 99.20749580860138
time_elpased: 1.597
batch start
#iterations: 326
currently lose_sum: 99.09491527080536
time_elpased: 1.591
batch start
#iterations: 327
currently lose_sum: 99.02952915430069
time_elpased: 1.599
batch start
#iterations: 328
currently lose_sum: 99.0672961473465
time_elpased: 1.595
batch start
#iterations: 329
currently lose_sum: 99.1457559466362
time_elpased: 1.596
batch start
#iterations: 330
currently lose_sum: 99.09734815359116
time_elpased: 1.617
batch start
#iterations: 331
currently lose_sum: 99.21480387449265
time_elpased: 1.573
batch start
#iterations: 332
currently lose_sum: 99.28448510169983
time_elpased: 1.614
batch start
#iterations: 333
currently lose_sum: 99.12073564529419
time_elpased: 1.584
batch start
#iterations: 334
currently lose_sum: 99.36469292640686
time_elpased: 1.594
batch start
#iterations: 335
currently lose_sum: 99.14697700738907
time_elpased: 1.585
batch start
#iterations: 336
currently lose_sum: 99.10339033603668
time_elpased: 1.596
batch start
#iterations: 337
currently lose_sum: 98.97184854745865
time_elpased: 1.572
batch start
#iterations: 338
currently lose_sum: 99.23696810007095
time_elpased: 1.616
batch start
#iterations: 339
currently lose_sum: 99.10834181308746
time_elpased: 1.584
start validation test
0.626288659794
0.64978749241
0.550684367603
0.596145276292
0.62642139479
64.347
batch start
#iterations: 340
currently lose_sum: 99.19170892238617
time_elpased: 1.596
batch start
#iterations: 341
currently lose_sum: 99.45872300863266
time_elpased: 1.575
batch start
#iterations: 342
currently lose_sum: 99.18303626775742
time_elpased: 1.619
batch start
#iterations: 343
currently lose_sum: 98.98699897527695
time_elpased: 1.562
batch start
#iterations: 344
currently lose_sum: 99.02112644910812
time_elpased: 1.573
batch start
#iterations: 345
currently lose_sum: 99.3556712269783
time_elpased: 1.613
batch start
#iterations: 346
currently lose_sum: 99.2658805847168
time_elpased: 1.633
batch start
#iterations: 347
currently lose_sum: 98.95355850458145
time_elpased: 1.579
batch start
#iterations: 348
currently lose_sum: 99.21964502334595
time_elpased: 1.592
batch start
#iterations: 349
currently lose_sum: 98.89310699701309
time_elpased: 1.573
batch start
#iterations: 350
currently lose_sum: 99.18959164619446
time_elpased: 1.61
batch start
#iterations: 351
currently lose_sum: 99.15325337648392
time_elpased: 1.592
batch start
#iterations: 352
currently lose_sum: 99.11196166276932
time_elpased: 1.598
batch start
#iterations: 353
currently lose_sum: 99.26967698335648
time_elpased: 1.592
batch start
#iterations: 354
currently lose_sum: 99.12953299283981
time_elpased: 1.589
batch start
#iterations: 355
currently lose_sum: 98.9505346417427
time_elpased: 1.587
batch start
#iterations: 356
currently lose_sum: 99.05056309700012
time_elpased: 1.589
batch start
#iterations: 357
currently lose_sum: 99.29326343536377
time_elpased: 1.604
batch start
#iterations: 358
currently lose_sum: 99.00408899784088
time_elpased: 1.618
batch start
#iterations: 359
currently lose_sum: 99.24959152936935
time_elpased: 1.587
start validation test
0.620463917526
0.642218463026
0.546979520428
0.590785305397
0.620592930719
64.393
batch start
#iterations: 360
currently lose_sum: 99.12172245979309
time_elpased: 1.608
batch start
#iterations: 361
currently lose_sum: 99.20270889997482
time_elpased: 1.575
batch start
#iterations: 362
currently lose_sum: 99.01873916387558
time_elpased: 1.601
batch start
#iterations: 363
currently lose_sum: 99.02065414190292
time_elpased: 1.586
batch start
#iterations: 364
currently lose_sum: 99.09914469718933
time_elpased: 1.595
batch start
#iterations: 365
currently lose_sum: 99.12534773349762
time_elpased: 1.605
batch start
#iterations: 366
currently lose_sum: 99.19199168682098
time_elpased: 1.604
batch start
#iterations: 367
currently lose_sum: 99.12756633758545
time_elpased: 1.576
batch start
#iterations: 368
currently lose_sum: 99.40621852874756
time_elpased: 1.611
batch start
#iterations: 369
currently lose_sum: 99.20259684324265
time_elpased: 1.579
batch start
#iterations: 370
currently lose_sum: 99.12782925367355
time_elpased: 1.601
batch start
#iterations: 371
currently lose_sum: 98.94032680988312
time_elpased: 1.597
batch start
#iterations: 372
currently lose_sum: 99.06146758794785
time_elpased: 1.613
batch start
#iterations: 373
currently lose_sum: 99.1137130856514
time_elpased: 1.576
batch start
#iterations: 374
currently lose_sum: 99.04041266441345
time_elpased: 1.624
batch start
#iterations: 375
currently lose_sum: 99.0672407746315
time_elpased: 1.589
batch start
#iterations: 376
currently lose_sum: 99.1988520026207
time_elpased: 1.599
batch start
#iterations: 377
currently lose_sum: 99.27322000265121
time_elpased: 1.613
batch start
#iterations: 378
currently lose_sum: 99.0530617237091
time_elpased: 1.615
batch start
#iterations: 379
currently lose_sum: 99.17199128866196
time_elpased: 1.556
start validation test
0.618505154639
0.6544
0.505094164866
0.570134169716
0.618704265124
64.376
batch start
#iterations: 380
currently lose_sum: 98.99585390090942
time_elpased: 1.64
batch start
#iterations: 381
currently lose_sum: 99.10142678022385
time_elpased: 1.609
batch start
#iterations: 382
currently lose_sum: 99.24973285198212
time_elpased: 1.599
batch start
#iterations: 383
currently lose_sum: 99.06859624385834
time_elpased: 1.577
batch start
#iterations: 384
currently lose_sum: 99.16906440258026
time_elpased: 1.585
batch start
#iterations: 385
currently lose_sum: 99.54087626934052
time_elpased: 1.589
batch start
#iterations: 386
currently lose_sum: 99.0797489285469
time_elpased: 1.62
batch start
#iterations: 387
currently lose_sum: 99.00201922655106
time_elpased: 1.591
batch start
#iterations: 388
currently lose_sum: 99.24859023094177
time_elpased: 1.641
batch start
#iterations: 389
currently lose_sum: 99.25838762521744
time_elpased: 1.597
batch start
#iterations: 390
currently lose_sum: 99.21828228235245
time_elpased: 1.622
batch start
#iterations: 391
currently lose_sum: 98.90417402982712
time_elpased: 1.574
batch start
#iterations: 392
currently lose_sum: 98.94391298294067
time_elpased: 1.596
batch start
#iterations: 393
currently lose_sum: 99.00396525859833
time_elpased: 1.605
batch start
#iterations: 394
currently lose_sum: 99.33679074048996
time_elpased: 1.606
batch start
#iterations: 395
currently lose_sum: 99.16543024778366
time_elpased: 1.592
batch start
#iterations: 396
currently lose_sum: 99.01426786184311
time_elpased: 1.605
batch start
#iterations: 397
currently lose_sum: 99.08596414327621
time_elpased: 1.584
batch start
#iterations: 398
currently lose_sum: 99.14348012208939
time_elpased: 1.609
batch start
#iterations: 399
currently lose_sum: 98.98651415109634
time_elpased: 1.591
start validation test
0.621443298969
0.650475586557
0.527837810024
0.582774684695
0.621607637843
64.339
acc: 0.624
pre: 0.647
rec: 0.546
F1: 0.592
auc: 0.670
