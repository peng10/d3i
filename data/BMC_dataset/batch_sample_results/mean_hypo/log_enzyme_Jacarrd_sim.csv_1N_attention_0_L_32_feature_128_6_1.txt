start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.42271727323532
time_elpased: 2.25
batch start
#iterations: 1
currently lose_sum: 100.33884251117706
time_elpased: 2.244
batch start
#iterations: 2
currently lose_sum: 100.2494570016861
time_elpased: 2.199
batch start
#iterations: 3
currently lose_sum: 100.16053146123886
time_elpased: 2.238
batch start
#iterations: 4
currently lose_sum: 99.98544710874557
time_elpased: 2.228
batch start
#iterations: 5
currently lose_sum: 99.94629335403442
time_elpased: 2.227
batch start
#iterations: 6
currently lose_sum: 99.91195797920227
time_elpased: 2.262
batch start
#iterations: 7
currently lose_sum: 99.85358434915543
time_elpased: 2.235
batch start
#iterations: 8
currently lose_sum: 99.71253311634064
time_elpased: 2.215
batch start
#iterations: 9
currently lose_sum: 99.66139423847198
time_elpased: 2.257
batch start
#iterations: 10
currently lose_sum: 99.58528143167496
time_elpased: 2.204
batch start
#iterations: 11
currently lose_sum: 99.56990504264832
time_elpased: 2.245
batch start
#iterations: 12
currently lose_sum: 99.51258593797684
time_elpased: 2.297
batch start
#iterations: 13
currently lose_sum: 99.25125229358673
time_elpased: 2.254
batch start
#iterations: 14
currently lose_sum: 99.24873334169388
time_elpased: 2.23
batch start
#iterations: 15
currently lose_sum: 99.24947506189346
time_elpased: 2.229
batch start
#iterations: 16
currently lose_sum: 99.09044998884201
time_elpased: 2.228
batch start
#iterations: 17
currently lose_sum: 99.30469816923141
time_elpased: 2.234
batch start
#iterations: 18
currently lose_sum: 98.81980186700821
time_elpased: 2.246
batch start
#iterations: 19
currently lose_sum: 99.02695316076279
time_elpased: 2.216
start validation test
0.604278350515
0.609961190168
0.582278481013
0.595798452061
0.60431697468
65.216
batch start
#iterations: 20
currently lose_sum: 98.98053985834122
time_elpased: 2.232
batch start
#iterations: 21
currently lose_sum: 98.69725435972214
time_elpased: 2.22
batch start
#iterations: 22
currently lose_sum: 99.07511919736862
time_elpased: 2.219
batch start
#iterations: 23
currently lose_sum: 98.68414223194122
time_elpased: 2.255
batch start
#iterations: 24
currently lose_sum: 98.74290233850479
time_elpased: 2.225
batch start
#iterations: 25
currently lose_sum: 98.77928441762924
time_elpased: 2.182
batch start
#iterations: 26
currently lose_sum: 98.85702669620514
time_elpased: 2.238
batch start
#iterations: 27
currently lose_sum: 98.71673399209976
time_elpased: 2.272
batch start
#iterations: 28
currently lose_sum: 98.50010079145432
time_elpased: 2.22
batch start
#iterations: 29
currently lose_sum: 98.63944667577744
time_elpased: 2.217
batch start
#iterations: 30
currently lose_sum: 98.36933380365372
time_elpased: 2.236
batch start
#iterations: 31
currently lose_sum: 98.52802896499634
time_elpased: 2.222
batch start
#iterations: 32
currently lose_sum: 98.54153823852539
time_elpased: 2.286
batch start
#iterations: 33
currently lose_sum: 98.43221700191498
time_elpased: 2.213
batch start
#iterations: 34
currently lose_sum: 98.36786848306656
time_elpased: 2.234
batch start
#iterations: 35
currently lose_sum: 98.4407451748848
time_elpased: 2.233
batch start
#iterations: 36
currently lose_sum: 97.9240962266922
time_elpased: 2.24
batch start
#iterations: 37
currently lose_sum: 98.40982180833817
time_elpased: 2.197
batch start
#iterations: 38
currently lose_sum: 98.47235089540482
time_elpased: 2.221
batch start
#iterations: 39
currently lose_sum: 98.0435763001442
time_elpased: 2.25
start validation test
0.611649484536
0.618680004349
0.585571678502
0.601670720102
0.611695268147
64.398
batch start
#iterations: 40
currently lose_sum: 97.96135753393173
time_elpased: 2.223
batch start
#iterations: 41
currently lose_sum: 98.11425876617432
time_elpased: 2.221
batch start
#iterations: 42
currently lose_sum: 98.11691468954086
time_elpased: 2.228
batch start
#iterations: 43
currently lose_sum: 98.17937642335892
time_elpased: 2.213
batch start
#iterations: 44
currently lose_sum: 98.08220613002777
time_elpased: 2.247
batch start
#iterations: 45
currently lose_sum: 97.99516361951828
time_elpased: 2.239
batch start
#iterations: 46
currently lose_sum: 97.96974462270737
time_elpased: 2.272
batch start
#iterations: 47
currently lose_sum: 97.74826544523239
time_elpased: 2.241
batch start
#iterations: 48
currently lose_sum: 98.0629112124443
time_elpased: 2.212
batch start
#iterations: 49
currently lose_sum: 97.9906365275383
time_elpased: 2.232
batch start
#iterations: 50
currently lose_sum: 98.02013367414474
time_elpased: 2.287
batch start
#iterations: 51
currently lose_sum: 97.85230803489685
time_elpased: 2.232
batch start
#iterations: 52
currently lose_sum: 98.29159396886826
time_elpased: 2.239
batch start
#iterations: 53
currently lose_sum: 97.64533704519272
time_elpased: 2.266
batch start
#iterations: 54
currently lose_sum: 97.93895876407623
time_elpased: 2.204
batch start
#iterations: 55
currently lose_sum: 97.90845537185669
time_elpased: 2.234
batch start
#iterations: 56
currently lose_sum: 97.82503890991211
time_elpased: 2.229
batch start
#iterations: 57
currently lose_sum: 97.92602288722992
time_elpased: 2.209
batch start
#iterations: 58
currently lose_sum: 97.83105099201202
time_elpased: 2.259
batch start
#iterations: 59
currently lose_sum: 98.17165398597717
time_elpased: 2.254
start validation test
0.617731958763
0.616600790514
0.626119172584
0.621323529412
0.617717233715
63.735
batch start
#iterations: 60
currently lose_sum: 97.72431713342667
time_elpased: 2.238
batch start
#iterations: 61
currently lose_sum: 97.85095697641373
time_elpased: 2.229
batch start
#iterations: 62
currently lose_sum: 97.83789396286011
time_elpased: 2.25
batch start
#iterations: 63
currently lose_sum: 98.02375042438507
time_elpased: 2.252
batch start
#iterations: 64
currently lose_sum: 97.6172690987587
time_elpased: 2.194
batch start
#iterations: 65
currently lose_sum: 97.7303677201271
time_elpased: 2.241
batch start
#iterations: 66
currently lose_sum: 97.655786216259
time_elpased: 2.28
batch start
#iterations: 67
currently lose_sum: 97.7531173825264
time_elpased: 2.24
batch start
#iterations: 68
currently lose_sum: 97.72649455070496
time_elpased: 2.273
batch start
#iterations: 69
currently lose_sum: 97.40386801958084
time_elpased: 2.314
batch start
#iterations: 70
currently lose_sum: 97.554623067379
time_elpased: 2.217
batch start
#iterations: 71
currently lose_sum: 97.52833646535873
time_elpased: 2.211
batch start
#iterations: 72
currently lose_sum: 97.46711927652359
time_elpased: 2.217
batch start
#iterations: 73
currently lose_sum: 97.07046282291412
time_elpased: 2.21
batch start
#iterations: 74
currently lose_sum: 97.54625010490417
time_elpased: 2.176
batch start
#iterations: 75
currently lose_sum: 97.6899539232254
time_elpased: 2.276
batch start
#iterations: 76
currently lose_sum: 97.6733084321022
time_elpased: 2.252
batch start
#iterations: 77
currently lose_sum: 97.76223450899124
time_elpased: 2.202
batch start
#iterations: 78
currently lose_sum: 97.541290640831
time_elpased: 2.26
batch start
#iterations: 79
currently lose_sum: 97.49703913927078
time_elpased: 2.254
start validation test
0.616701030928
0.637293848561
0.544818359576
0.587438970262
0.616827232045
63.499
batch start
#iterations: 80
currently lose_sum: 97.28341835737228
time_elpased: 2.204
batch start
#iterations: 81
currently lose_sum: 97.39441764354706
time_elpased: 2.251
batch start
#iterations: 82
currently lose_sum: 97.49735659360886
time_elpased: 2.202
batch start
#iterations: 83
currently lose_sum: 97.26139026880264
time_elpased: 2.294
batch start
#iterations: 84
currently lose_sum: 97.16865479946136
time_elpased: 2.187
batch start
#iterations: 85
currently lose_sum: 97.24612349271774
time_elpased: 2.211
batch start
#iterations: 86
currently lose_sum: 97.46343392133713
time_elpased: 2.238
batch start
#iterations: 87
currently lose_sum: 97.45618283748627
time_elpased: 2.225
batch start
#iterations: 88
currently lose_sum: 97.48793983459473
time_elpased: 2.276
batch start
#iterations: 89
currently lose_sum: 96.93839538097382
time_elpased: 2.27
batch start
#iterations: 90
currently lose_sum: 97.09762209653854
time_elpased: 2.248
batch start
#iterations: 91
currently lose_sum: 97.49008792638779
time_elpased: 2.226
batch start
#iterations: 92
currently lose_sum: 97.10630065202713
time_elpased: 2.227
batch start
#iterations: 93
currently lose_sum: 97.45521491765976
time_elpased: 2.263
batch start
#iterations: 94
currently lose_sum: 97.26120662689209
time_elpased: 2.249
batch start
#iterations: 95
currently lose_sum: 97.30671048164368
time_elpased: 2.243
batch start
#iterations: 96
currently lose_sum: 97.2431371808052
time_elpased: 2.206
batch start
#iterations: 97
currently lose_sum: 96.88107508420944
time_elpased: 2.244
batch start
#iterations: 98
currently lose_sum: 97.00384205579758
time_elpased: 2.206
batch start
#iterations: 99
currently lose_sum: 97.1317173242569
time_elpased: 2.207
start validation test
0.599639175258
0.624457492979
0.503447566121
0.557461113327
0.599808054464
64.585
batch start
#iterations: 100
currently lose_sum: 97.32149600982666
time_elpased: 2.213
batch start
#iterations: 101
currently lose_sum: 97.30316460132599
time_elpased: 2.198
batch start
#iterations: 102
currently lose_sum: 96.7754909992218
time_elpased: 2.242
batch start
#iterations: 103
currently lose_sum: 97.48264372348785
time_elpased: 2.249
batch start
#iterations: 104
currently lose_sum: 97.02488392591476
time_elpased: 2.2
batch start
#iterations: 105
currently lose_sum: 97.08571273088455
time_elpased: 2.224
batch start
#iterations: 106
currently lose_sum: 97.16911154985428
time_elpased: 2.189
batch start
#iterations: 107
currently lose_sum: 97.01470559835434
time_elpased: 2.233
batch start
#iterations: 108
currently lose_sum: 96.77302199602127
time_elpased: 2.194
batch start
#iterations: 109
currently lose_sum: 97.01498031616211
time_elpased: 2.293
batch start
#iterations: 110
currently lose_sum: 96.84174782037735
time_elpased: 2.225
batch start
#iterations: 111
currently lose_sum: 97.00710326433182
time_elpased: 2.261
batch start
#iterations: 112
currently lose_sum: 96.81670492887497
time_elpased: 2.231
batch start
#iterations: 113
currently lose_sum: 96.99474918842316
time_elpased: 2.248
batch start
#iterations: 114
currently lose_sum: 96.83806085586548
time_elpased: 2.275
batch start
#iterations: 115
currently lose_sum: 96.94962060451508
time_elpased: 2.245
batch start
#iterations: 116
currently lose_sum: 97.13891172409058
time_elpased: 2.185
batch start
#iterations: 117
currently lose_sum: 97.04657274484634
time_elpased: 2.25
batch start
#iterations: 118
currently lose_sum: 96.98456573486328
time_elpased: 2.251
batch start
#iterations: 119
currently lose_sum: 96.79815226793289
time_elpased: 2.219
start validation test
0.624896907216
0.635435168739
0.589070700834
0.611375166889
0.624959805648
63.232
batch start
#iterations: 120
currently lose_sum: 96.85848611593246
time_elpased: 2.231
batch start
#iterations: 121
currently lose_sum: 96.91806602478027
time_elpased: 2.225
batch start
#iterations: 122
currently lose_sum: 96.95727527141571
time_elpased: 2.235
batch start
#iterations: 123
currently lose_sum: 96.84713065624237
time_elpased: 2.197
batch start
#iterations: 124
currently lose_sum: 96.7319787144661
time_elpased: 2.297
batch start
#iterations: 125
currently lose_sum: 97.02204775810242
time_elpased: 2.247
batch start
#iterations: 126
currently lose_sum: 97.02407836914062
time_elpased: 2.227
batch start
#iterations: 127
currently lose_sum: 96.7576864361763
time_elpased: 2.277
batch start
#iterations: 128
currently lose_sum: 96.80928170681
time_elpased: 2.233
batch start
#iterations: 129
currently lose_sum: 96.70548439025879
time_elpased: 2.27
batch start
#iterations: 130
currently lose_sum: 96.96941661834717
time_elpased: 2.294
batch start
#iterations: 131
currently lose_sum: 96.6859130859375
time_elpased: 2.231
batch start
#iterations: 132
currently lose_sum: 96.61809593439102
time_elpased: 2.212
batch start
#iterations: 133
currently lose_sum: 96.58892101049423
time_elpased: 2.191
batch start
#iterations: 134
currently lose_sum: 96.70440363883972
time_elpased: 2.253
batch start
#iterations: 135
currently lose_sum: 96.90073090791702
time_elpased: 2.213
batch start
#iterations: 136
currently lose_sum: 96.75751143693924
time_elpased: 2.235
batch start
#iterations: 137
currently lose_sum: 96.92840772867203
time_elpased: 2.271
batch start
#iterations: 138
currently lose_sum: 96.69343209266663
time_elpased: 2.225
batch start
#iterations: 139
currently lose_sum: 96.62025570869446
time_elpased: 2.203
start validation test
0.602680412371
0.623539540032
0.521765977153
0.568130883012
0.602822470142
64.521
batch start
#iterations: 140
currently lose_sum: 96.72463417053223
time_elpased: 2.233
batch start
#iterations: 141
currently lose_sum: 96.38331365585327
time_elpased: 2.195
batch start
#iterations: 142
currently lose_sum: 97.12757927179337
time_elpased: 2.222
batch start
#iterations: 143
currently lose_sum: 96.51200670003891
time_elpased: 2.252
batch start
#iterations: 144
currently lose_sum: 96.55162090063095
time_elpased: 2.262
batch start
#iterations: 145
currently lose_sum: 96.60821598768234
time_elpased: 2.188
batch start
#iterations: 146
currently lose_sum: 96.58928573131561
time_elpased: 2.264
batch start
#iterations: 147
currently lose_sum: 96.46153247356415
time_elpased: 2.243
batch start
#iterations: 148
currently lose_sum: 96.41293036937714
time_elpased: 2.233
batch start
#iterations: 149
currently lose_sum: 96.70795857906342
time_elpased: 2.209
batch start
#iterations: 150
currently lose_sum: 96.25050109624863
time_elpased: 2.297
batch start
#iterations: 151
currently lose_sum: 96.83634448051453
time_elpased: 2.197
batch start
#iterations: 152
currently lose_sum: 96.5761050581932
time_elpased: 2.261
batch start
#iterations: 153
currently lose_sum: 96.24523764848709
time_elpased: 2.226
batch start
#iterations: 154
currently lose_sum: 96.51318019628525
time_elpased: 2.185
batch start
#iterations: 155
currently lose_sum: 96.40134626626968
time_elpased: 2.234
batch start
#iterations: 156
currently lose_sum: 96.42008566856384
time_elpased: 2.195
batch start
#iterations: 157
currently lose_sum: 96.65190637111664
time_elpased: 2.193
batch start
#iterations: 158
currently lose_sum: 96.76470285654068
time_elpased: 2.239
batch start
#iterations: 159
currently lose_sum: 96.35162097215652
time_elpased: 2.242
start validation test
0.606907216495
0.643988431346
0.481218483071
0.550830486512
0.607127882453
63.908
batch start
#iterations: 160
currently lose_sum: 96.57487124204636
time_elpased: 2.273
batch start
#iterations: 161
currently lose_sum: 95.91492980718613
time_elpased: 2.229
batch start
#iterations: 162
currently lose_sum: 96.1855901479721
time_elpased: 2.251
batch start
#iterations: 163
currently lose_sum: 96.07558989524841
time_elpased: 2.273
batch start
#iterations: 164
currently lose_sum: 96.26645761728287
time_elpased: 2.309
batch start
#iterations: 165
currently lose_sum: 96.194509267807
time_elpased: 2.292
batch start
#iterations: 166
currently lose_sum: 96.14817404747009
time_elpased: 2.281
batch start
#iterations: 167
currently lose_sum: 96.41767865419388
time_elpased: 2.195
batch start
#iterations: 168
currently lose_sum: 95.95569109916687
time_elpased: 2.256
batch start
#iterations: 169
currently lose_sum: 95.98003840446472
time_elpased: 2.258
batch start
#iterations: 170
currently lose_sum: 95.91676044464111
time_elpased: 2.25
batch start
#iterations: 171
currently lose_sum: 96.10516256093979
time_elpased: 2.227
batch start
#iterations: 172
currently lose_sum: 95.9201095700264
time_elpased: 2.275
batch start
#iterations: 173
currently lose_sum: 96.10797530412674
time_elpased: 2.264
batch start
#iterations: 174
currently lose_sum: 96.09829837083817
time_elpased: 2.223
batch start
#iterations: 175
currently lose_sum: 95.46763724088669
time_elpased: 2.267
batch start
#iterations: 176
currently lose_sum: 96.10938638448715
time_elpased: 2.194
batch start
#iterations: 177
currently lose_sum: 96.09245496988297
time_elpased: 2.255
batch start
#iterations: 178
currently lose_sum: 95.91318219900131
time_elpased: 2.211
batch start
#iterations: 179
currently lose_sum: 95.90712183713913
time_elpased: 2.226
start validation test
0.614793814433
0.627906976744
0.566841617783
0.595813727081
0.61487800191
63.586
batch start
#iterations: 180
currently lose_sum: 95.75292217731476
time_elpased: 2.301
batch start
#iterations: 181
currently lose_sum: 95.51254814863205
time_elpased: 2.213
batch start
#iterations: 182
currently lose_sum: 95.93760579824448
time_elpased: 2.203
batch start
#iterations: 183
currently lose_sum: 95.33591741323471
time_elpased: 2.225
batch start
#iterations: 184
currently lose_sum: 95.66242879629135
time_elpased: 2.226
batch start
#iterations: 185
currently lose_sum: 95.90984606742859
time_elpased: 2.212
batch start
#iterations: 186
currently lose_sum: 95.48690235614777
time_elpased: 2.234
batch start
#iterations: 187
currently lose_sum: 95.44867515563965
time_elpased: 2.207
batch start
#iterations: 188
currently lose_sum: 95.50159066915512
time_elpased: 2.22
batch start
#iterations: 189
currently lose_sum: 95.48775428533554
time_elpased: 2.259
batch start
#iterations: 190
currently lose_sum: 95.651964366436
time_elpased: 2.225
batch start
#iterations: 191
currently lose_sum: 95.45439010858536
time_elpased: 2.227
batch start
#iterations: 192
currently lose_sum: 95.39141136407852
time_elpased: 2.254
batch start
#iterations: 193
currently lose_sum: 95.6185804605484
time_elpased: 2.236
batch start
#iterations: 194
currently lose_sum: 95.46086072921753
time_elpased: 2.287
batch start
#iterations: 195
currently lose_sum: 95.28059422969818
time_elpased: 2.238
batch start
#iterations: 196
currently lose_sum: 95.45292562246323
time_elpased: 2.206
batch start
#iterations: 197
currently lose_sum: 95.323068857193
time_elpased: 2.244
batch start
#iterations: 198
currently lose_sum: 95.38055694103241
time_elpased: 2.228
batch start
#iterations: 199
currently lose_sum: 95.18411070108414
time_elpased: 2.201
start validation test
0.62175257732
0.641019561352
0.556447463209
0.595747025121
0.62186723052
63.497
batch start
#iterations: 200
currently lose_sum: 95.60550105571747
time_elpased: 2.191
batch start
#iterations: 201
currently lose_sum: 95.25094413757324
time_elpased: 2.225
batch start
#iterations: 202
currently lose_sum: 94.82632821798325
time_elpased: 2.216
batch start
#iterations: 203
currently lose_sum: 95.11128371953964
time_elpased: 2.253
batch start
#iterations: 204
currently lose_sum: 95.26885771751404
time_elpased: 2.258
batch start
#iterations: 205
currently lose_sum: 95.47476172447205
time_elpased: 2.208
batch start
#iterations: 206
currently lose_sum: 94.96318435668945
time_elpased: 2.271
batch start
#iterations: 207
currently lose_sum: 94.91692227125168
time_elpased: 2.213
batch start
#iterations: 208
currently lose_sum: 95.10949021577835
time_elpased: 2.221
batch start
#iterations: 209
currently lose_sum: 94.78973472118378
time_elpased: 2.267
batch start
#iterations: 210
currently lose_sum: 94.8368524312973
time_elpased: 2.229
batch start
#iterations: 211
currently lose_sum: 94.67905616760254
time_elpased: 2.204
batch start
#iterations: 212
currently lose_sum: 94.87612253427505
time_elpased: 2.245
batch start
#iterations: 213
currently lose_sum: 95.01194405555725
time_elpased: 2.292
batch start
#iterations: 214
currently lose_sum: 94.74694675207138
time_elpased: 2.234
batch start
#iterations: 215
currently lose_sum: 94.5748827457428
time_elpased: 2.237
batch start
#iterations: 216
currently lose_sum: 94.62070143222809
time_elpased: 2.266
batch start
#iterations: 217
currently lose_sum: 94.93184864521027
time_elpased: 2.251
batch start
#iterations: 218
currently lose_sum: 94.37413400411606
time_elpased: 2.208
batch start
#iterations: 219
currently lose_sum: 94.68661254644394
time_elpased: 2.203
start validation test
0.608041237113
0.616908266017
0.573736750026
0.594539831503
0.608101463932
64.316
batch start
#iterations: 220
currently lose_sum: 94.38044840097427
time_elpased: 2.245
batch start
#iterations: 221
currently lose_sum: 94.3102056980133
time_elpased: 2.241
batch start
#iterations: 222
currently lose_sum: 94.40416812896729
time_elpased: 2.248
batch start
#iterations: 223
currently lose_sum: 94.51019072532654
time_elpased: 2.233
batch start
#iterations: 224
currently lose_sum: 94.05650740861893
time_elpased: 2.192
batch start
#iterations: 225
currently lose_sum: 94.00568073987961
time_elpased: 2.225
batch start
#iterations: 226
currently lose_sum: 94.01248490810394
time_elpased: 2.231
batch start
#iterations: 227
currently lose_sum: 94.65944856405258
time_elpased: 2.207
batch start
#iterations: 228
currently lose_sum: 94.12140965461731
time_elpased: 2.22
batch start
#iterations: 229
currently lose_sum: 94.16931641101837
time_elpased: 2.281
batch start
#iterations: 230
currently lose_sum: 94.08144265413284
time_elpased: 2.214
batch start
#iterations: 231
currently lose_sum: 94.21979182958603
time_elpased: 2.21
batch start
#iterations: 232
currently lose_sum: 94.08919197320938
time_elpased: 2.229
batch start
#iterations: 233
currently lose_sum: 93.61381942033768
time_elpased: 2.282
batch start
#iterations: 234
currently lose_sum: 93.56298494338989
time_elpased: 2.219
batch start
#iterations: 235
currently lose_sum: 93.74539995193481
time_elpased: 2.258
batch start
#iterations: 236
currently lose_sum: 94.23735618591309
time_elpased: 2.203
batch start
#iterations: 237
currently lose_sum: 93.87345308065414
time_elpased: 2.271
batch start
#iterations: 238
currently lose_sum: 93.52257001399994
time_elpased: 2.232
batch start
#iterations: 239
currently lose_sum: 93.9230968952179
time_elpased: 2.219
start validation test
0.611597938144
0.635662770455
0.526088298858
0.575708091672
0.611748063505
64.774
batch start
#iterations: 240
currently lose_sum: 93.69697117805481
time_elpased: 2.272
batch start
#iterations: 241
currently lose_sum: 93.27492040395737
time_elpased: 2.259
batch start
#iterations: 242
currently lose_sum: 93.4058296084404
time_elpased: 2.253
batch start
#iterations: 243
currently lose_sum: 93.5435761809349
time_elpased: 2.263
batch start
#iterations: 244
currently lose_sum: 93.46684056520462
time_elpased: 2.25
batch start
#iterations: 245
currently lose_sum: 93.26976734399796
time_elpased: 2.24
batch start
#iterations: 246
currently lose_sum: 93.06924325227737
time_elpased: 2.274
batch start
#iterations: 247
currently lose_sum: 93.25033807754517
time_elpased: 2.24
batch start
#iterations: 248
currently lose_sum: 93.20696103572845
time_elpased: 2.287
batch start
#iterations: 249
currently lose_sum: 93.30393677949905
time_elpased: 2.232
batch start
#iterations: 250
currently lose_sum: 93.00178229808807
time_elpased: 2.211
batch start
#iterations: 251
currently lose_sum: 92.6634430885315
time_elpased: 2.222
batch start
#iterations: 252
currently lose_sum: 92.42712706327438
time_elpased: 2.199
batch start
#iterations: 253
currently lose_sum: 92.4144195318222
time_elpased: 2.212
batch start
#iterations: 254
currently lose_sum: 92.34917414188385
time_elpased: 2.206
batch start
#iterations: 255
currently lose_sum: 92.50161910057068
time_elpased: 2.239
batch start
#iterations: 256
currently lose_sum: 92.37092173099518
time_elpased: 2.259
batch start
#iterations: 257
currently lose_sum: 92.37553852796555
time_elpased: 2.313
batch start
#iterations: 258
currently lose_sum: 92.1337947845459
time_elpased: 2.271
batch start
#iterations: 259
currently lose_sum: 92.12652575969696
time_elpased: 2.279
start validation test
0.605515463918
0.637783711615
0.491612637645
0.555239146859
0.605715437897
65.610
batch start
#iterations: 260
currently lose_sum: 91.88905161619186
time_elpased: 2.249
batch start
#iterations: 261
currently lose_sum: 91.5215260386467
time_elpased: 2.244
batch start
#iterations: 262
currently lose_sum: 92.30618792772293
time_elpased: 2.208
batch start
#iterations: 263
currently lose_sum: 91.97209364175797
time_elpased: 2.261
batch start
#iterations: 264
currently lose_sum: 91.88387936353683
time_elpased: 2.23
batch start
#iterations: 265
currently lose_sum: 91.9718359708786
time_elpased: 2.224
batch start
#iterations: 266
currently lose_sum: 91.58675754070282
time_elpased: 2.254
batch start
#iterations: 267
currently lose_sum: 92.01631563901901
time_elpased: 2.248
batch start
#iterations: 268
currently lose_sum: 91.32265400886536
time_elpased: 2.199
batch start
#iterations: 269
currently lose_sum: 91.45822328329086
time_elpased: 2.242
batch start
#iterations: 270
currently lose_sum: 91.3336746096611
time_elpased: 2.221
batch start
#iterations: 271
currently lose_sum: 90.5832867026329
time_elpased: 2.214
batch start
#iterations: 272
currently lose_sum: 90.7636889219284
time_elpased: 2.203
batch start
#iterations: 273
currently lose_sum: 90.98696893453598
time_elpased: 2.184
batch start
#iterations: 274
currently lose_sum: 90.72666907310486
time_elpased: 2.194
batch start
#iterations: 275
currently lose_sum: 91.21048480272293
time_elpased: 2.221
batch start
#iterations: 276
currently lose_sum: 90.19764214754105
time_elpased: 2.195
batch start
#iterations: 277
currently lose_sum: 90.58573967218399
time_elpased: 2.245
batch start
#iterations: 278
currently lose_sum: 90.11338299512863
time_elpased: 2.24
batch start
#iterations: 279
currently lose_sum: 90.4733179807663
time_elpased: 2.265
start validation test
0.595309278351
0.617922143579
0.503138828857
0.554654262862
0.595471097791
67.215
batch start
#iterations: 280
currently lose_sum: 90.28509575128555
time_elpased: 2.187
batch start
#iterations: 281
currently lose_sum: 90.02852720022202
time_elpased: 2.207
batch start
#iterations: 282
currently lose_sum: 90.04585772752762
time_elpased: 2.217
batch start
#iterations: 283
currently lose_sum: 90.3028170466423
time_elpased: 2.2
batch start
#iterations: 284
currently lose_sum: 89.69422435760498
time_elpased: 2.221
batch start
#iterations: 285
currently lose_sum: 88.94025355577469
time_elpased: 2.234
batch start
#iterations: 286
currently lose_sum: 89.12658846378326
time_elpased: 2.233
batch start
#iterations: 287
currently lose_sum: 89.59469699859619
time_elpased: 2.209
batch start
#iterations: 288
currently lose_sum: 89.00018739700317
time_elpased: 2.282
batch start
#iterations: 289
currently lose_sum: 89.50208973884583
time_elpased: 2.251
batch start
#iterations: 290
currently lose_sum: 89.04423320293427
time_elpased: 2.229
batch start
#iterations: 291
currently lose_sum: 88.46955341100693
time_elpased: 2.201
batch start
#iterations: 292
currently lose_sum: 88.44043058156967
time_elpased: 2.218
batch start
#iterations: 293
currently lose_sum: 88.27425521612167
time_elpased: 2.212
batch start
#iterations: 294
currently lose_sum: 88.36714160442352
time_elpased: 2.222
batch start
#iterations: 295
currently lose_sum: 88.54713851213455
time_elpased: 2.207
batch start
#iterations: 296
currently lose_sum: 88.69726496934891
time_elpased: 2.247
batch start
#iterations: 297
currently lose_sum: 88.09142071008682
time_elpased: 2.314
batch start
#iterations: 298
currently lose_sum: 87.05902445316315
time_elpased: 2.217
batch start
#iterations: 299
currently lose_sum: 87.1547087430954
time_elpased: 2.182
start validation test
0.59
0.642062852538
0.410003087373
0.500439643261
0.590316012343
70.872
batch start
#iterations: 300
currently lose_sum: 87.62974286079407
time_elpased: 2.199
batch start
#iterations: 301
currently lose_sum: 87.51003801822662
time_elpased: 2.267
batch start
#iterations: 302
currently lose_sum: 87.26861315965652
time_elpased: 2.225
batch start
#iterations: 303
currently lose_sum: 87.42837077379227
time_elpased: 2.223
batch start
#iterations: 304
currently lose_sum: 86.94869929552078
time_elpased: 2.232
batch start
#iterations: 305
currently lose_sum: 86.1511760354042
time_elpased: 2.191
batch start
#iterations: 306
currently lose_sum: 87.00385564565659
time_elpased: 2.254
batch start
#iterations: 307
currently lose_sum: 86.27079701423645
time_elpased: 2.195
batch start
#iterations: 308
currently lose_sum: 86.04824143648148
time_elpased: 2.233
batch start
#iterations: 309
currently lose_sum: 85.61538577079773
time_elpased: 2.247
batch start
#iterations: 310
currently lose_sum: 85.87990963459015
time_elpased: 2.203
batch start
#iterations: 311
currently lose_sum: 85.43568426370621
time_elpased: 2.215
batch start
#iterations: 312
currently lose_sum: 84.82437998056412
time_elpased: 2.215
batch start
#iterations: 313
currently lose_sum: 84.90190708637238
time_elpased: 2.161
batch start
#iterations: 314
currently lose_sum: 84.60313338041306
time_elpased: 2.21
batch start
#iterations: 315
currently lose_sum: 84.7176718711853
time_elpased: 2.232
batch start
#iterations: 316
currently lose_sum: 84.18457180261612
time_elpased: 2.216
batch start
#iterations: 317
currently lose_sum: 84.57569479942322
time_elpased: 2.17
batch start
#iterations: 318
currently lose_sum: 84.16825121641159
time_elpased: 2.203
batch start
#iterations: 319
currently lose_sum: 84.31366938352585
time_elpased: 2.222
start validation test
0.58293814433
0.625618046972
0.416692394772
0.500216196183
0.583230014385
75.966
batch start
#iterations: 320
currently lose_sum: 83.4713129401207
time_elpased: 2.209
batch start
#iterations: 321
currently lose_sum: 83.17046651244164
time_elpased: 2.205
batch start
#iterations: 322
currently lose_sum: 83.60687667131424
time_elpased: 2.222
batch start
#iterations: 323
currently lose_sum: 83.96197819709778
time_elpased: 2.243
batch start
#iterations: 324
currently lose_sum: 82.70272463560104
time_elpased: 2.214
batch start
#iterations: 325
currently lose_sum: 82.28488487005234
time_elpased: 2.188
batch start
#iterations: 326
currently lose_sum: 82.12293991446495
time_elpased: 2.218
batch start
#iterations: 327
currently lose_sum: 81.76126843690872
time_elpased: 2.227
batch start
#iterations: 328
currently lose_sum: 82.719461530447
time_elpased: 2.266
batch start
#iterations: 329
currently lose_sum: 81.72028428316116
time_elpased: 2.233
batch start
#iterations: 330
currently lose_sum: 81.46498101949692
time_elpased: 2.285
batch start
#iterations: 331
currently lose_sum: 81.19805878400803
time_elpased: 2.173
batch start
#iterations: 332
currently lose_sum: 81.0083369910717
time_elpased: 2.232
batch start
#iterations: 333
currently lose_sum: 81.41295510530472
time_elpased: 2.164
batch start
#iterations: 334
currently lose_sum: 80.27568688988686
time_elpased: 2.249
batch start
#iterations: 335
currently lose_sum: 80.56156262755394
time_elpased: 2.205
batch start
#iterations: 336
currently lose_sum: 80.39821764826775
time_elpased: 2.202
batch start
#iterations: 337
currently lose_sum: 79.69156423211098
time_elpased: 2.259
batch start
#iterations: 338
currently lose_sum: 80.08714243769646
time_elpased: 2.254
batch start
#iterations: 339
currently lose_sum: 79.38142302632332
time_elpased: 2.27
start validation test
0.57618556701
0.625440510153
0.38355459504
0.475503955091
0.576523760393
84.173
batch start
#iterations: 340
currently lose_sum: 79.44840928912163
time_elpased: 2.21
batch start
#iterations: 341
currently lose_sum: 79.04768991470337
time_elpased: 2.218
batch start
#iterations: 342
currently lose_sum: 79.06417307257652
time_elpased: 2.203
batch start
#iterations: 343
currently lose_sum: 79.20653676986694
time_elpased: 2.195
batch start
#iterations: 344
currently lose_sum: 78.61896148324013
time_elpased: 2.228
batch start
#iterations: 345
currently lose_sum: 78.36041101813316
time_elpased: 2.288
batch start
#iterations: 346
currently lose_sum: 78.09580087661743
time_elpased: 2.193
batch start
#iterations: 347
currently lose_sum: 77.4141643345356
time_elpased: 2.295
batch start
#iterations: 348
currently lose_sum: 76.89871519804001
time_elpased: 2.188
batch start
#iterations: 349
currently lose_sum: 77.4817804992199
time_elpased: 2.197
batch start
#iterations: 350
currently lose_sum: 77.33983713388443
time_elpased: 2.281
batch start
#iterations: 351
currently lose_sum: 76.94802409410477
time_elpased: 2.215
batch start
#iterations: 352
currently lose_sum: 76.67263811826706
time_elpased: 2.18
batch start
#iterations: 353
currently lose_sum: 76.60647210478783
time_elpased: 2.242
batch start
#iterations: 354
currently lose_sum: 76.21664881706238
time_elpased: 2.225
batch start
#iterations: 355
currently lose_sum: 75.84656590223312
time_elpased: 2.186
batch start
#iterations: 356
currently lose_sum: 76.36090904474258
time_elpased: 2.26
batch start
#iterations: 357
currently lose_sum: 75.37941035628319
time_elpased: 2.205
batch start
#iterations: 358
currently lose_sum: 75.5791946053505
time_elpased: 2.202
batch start
#iterations: 359
currently lose_sum: 75.44040343165398
time_elpased: 2.196
start validation test
0.565618556701
0.638412017167
0.30616445405
0.413855463588
0.566074068396
98.391
batch start
#iterations: 360
currently lose_sum: 75.3344646692276
time_elpased: 2.187
batch start
#iterations: 361
currently lose_sum: 74.70173585414886
time_elpased: 2.261
batch start
#iterations: 362
currently lose_sum: 74.60115975141525
time_elpased: 2.264
batch start
#iterations: 363
currently lose_sum: 74.55424338579178
time_elpased: 2.194
batch start
#iterations: 364
currently lose_sum: 74.89056259393692
time_elpased: 2.274
batch start
#iterations: 365
currently lose_sum: 73.65761479735374
time_elpased: 2.21
batch start
#iterations: 366
currently lose_sum: 74.17561709880829
time_elpased: 2.226
batch start
#iterations: 367
currently lose_sum: 73.40073552727699
time_elpased: 2.23
batch start
#iterations: 368
currently lose_sum: 74.42362692952156
time_elpased: 2.233
batch start
#iterations: 369
currently lose_sum: 73.26124745607376
time_elpased: 2.234
batch start
#iterations: 370
currently lose_sum: 73.57322111725807
time_elpased: 2.188
batch start
#iterations: 371
currently lose_sum: 73.95357319712639
time_elpased: 2.288
batch start
#iterations: 372
currently lose_sum: 72.95116236805916
time_elpased: 2.184
batch start
#iterations: 373
currently lose_sum: 72.18564003705978
time_elpased: 2.219
batch start
#iterations: 374
currently lose_sum: 71.75089997053146
time_elpased: 2.2
batch start
#iterations: 375
currently lose_sum: 72.60885253548622
time_elpased: 2.249
batch start
#iterations: 376
currently lose_sum: 71.60610508918762
time_elpased: 2.258
batch start
#iterations: 377
currently lose_sum: 73.013906031847
time_elpased: 2.19
batch start
#iterations: 378
currently lose_sum: 72.1588094830513
time_elpased: 2.275
batch start
#iterations: 379
currently lose_sum: 71.58696511387825
time_elpased: 2.24
start validation test
0.55675257732
0.602832965416
0.337244005351
0.432521612882
0.557137958475
114.464
batch start
#iterations: 380
currently lose_sum: 71.31474655866623
time_elpased: 2.212
batch start
#iterations: 381
currently lose_sum: 70.87329402565956
time_elpased: 2.219
batch start
#iterations: 382
currently lose_sum: 71.77375510334969
time_elpased: 2.212
batch start
#iterations: 383
currently lose_sum: 71.62690162658691
time_elpased: 2.227
batch start
#iterations: 384
currently lose_sum: 70.35716935992241
time_elpased: 2.217
batch start
#iterations: 385
currently lose_sum: 70.68606269359589
time_elpased: 2.206
batch start
#iterations: 386
currently lose_sum: 71.07869991660118
time_elpased: 2.187
batch start
#iterations: 387
currently lose_sum: 70.3972797691822
time_elpased: 2.223
batch start
#iterations: 388
currently lose_sum: 70.25216871500015
time_elpased: 2.197
batch start
#iterations: 389
currently lose_sum: 70.0675939321518
time_elpased: 2.204
batch start
#iterations: 390
currently lose_sum: 69.55746951699257
time_elpased: 2.268
batch start
#iterations: 391
currently lose_sum: 69.88770064711571
time_elpased: 2.174
batch start
#iterations: 392
currently lose_sum: 70.13735842704773
time_elpased: 2.203
batch start
#iterations: 393
currently lose_sum: 68.58907747268677
time_elpased: 2.24
batch start
#iterations: 394
currently lose_sum: 69.04877284169197
time_elpased: 2.198
batch start
#iterations: 395
currently lose_sum: 69.22636964917183
time_elpased: 2.216
batch start
#iterations: 396
currently lose_sum: 69.03818279504776
time_elpased: 2.222
batch start
#iterations: 397
currently lose_sum: 69.1663349866867
time_elpased: 2.197
batch start
#iterations: 398
currently lose_sum: 68.68384590744972
time_elpased: 2.217
batch start
#iterations: 399
currently lose_sum: 68.76368474960327
time_elpased: 2.185
start validation test
0.555463917526
0.613264248705
0.304517855305
0.406959152799
0.555904492044
128.849
acc: 0.627
pre: 0.637
rec: 0.593
F1: 0.614
auc: 0.671
