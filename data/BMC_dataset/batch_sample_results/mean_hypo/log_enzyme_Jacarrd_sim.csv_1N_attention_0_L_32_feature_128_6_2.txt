start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.44694364070892
time_elpased: 2.279
batch start
#iterations: 1
currently lose_sum: 100.26866853237152
time_elpased: 2.233
batch start
#iterations: 2
currently lose_sum: 100.23016220331192
time_elpased: 2.237
batch start
#iterations: 3
currently lose_sum: 100.08043938875198
time_elpased: 2.198
batch start
#iterations: 4
currently lose_sum: 100.03770220279694
time_elpased: 2.233
batch start
#iterations: 5
currently lose_sum: 99.96462136507034
time_elpased: 2.241
batch start
#iterations: 6
currently lose_sum: 99.75698786973953
time_elpased: 2.195
batch start
#iterations: 7
currently lose_sum: 99.88518953323364
time_elpased: 2.241
batch start
#iterations: 8
currently lose_sum: 99.76175993680954
time_elpased: 2.176
batch start
#iterations: 9
currently lose_sum: 99.59544372558594
time_elpased: 2.23
batch start
#iterations: 10
currently lose_sum: 99.60518491268158
time_elpased: 2.224
batch start
#iterations: 11
currently lose_sum: 99.56393527984619
time_elpased: 2.246
batch start
#iterations: 12
currently lose_sum: 99.53831267356873
time_elpased: 2.213
batch start
#iterations: 13
currently lose_sum: 99.57523846626282
time_elpased: 2.254
batch start
#iterations: 14
currently lose_sum: 99.37070041894913
time_elpased: 2.221
batch start
#iterations: 15
currently lose_sum: 99.26636028289795
time_elpased: 2.218
batch start
#iterations: 16
currently lose_sum: 99.1291578412056
time_elpased: 2.252
batch start
#iterations: 17
currently lose_sum: 99.1943524479866
time_elpased: 2.23
batch start
#iterations: 18
currently lose_sum: 99.07933872938156
time_elpased: 2.236
batch start
#iterations: 19
currently lose_sum: 98.90322959423065
time_elpased: 2.229
start validation test
0.602268041237
0.596063370139
0.638880312854
0.616729584741
0.602203762747
65.084
batch start
#iterations: 20
currently lose_sum: 98.98213654756546
time_elpased: 2.201
batch start
#iterations: 21
currently lose_sum: 98.86201775074005
time_elpased: 2.231
batch start
#iterations: 22
currently lose_sum: 98.87016767263412
time_elpased: 2.178
batch start
#iterations: 23
currently lose_sum: 98.98038876056671
time_elpased: 2.203
batch start
#iterations: 24
currently lose_sum: 98.73821544647217
time_elpased: 2.234
batch start
#iterations: 25
currently lose_sum: 98.69659608602524
time_elpased: 2.202
batch start
#iterations: 26
currently lose_sum: 98.6429648399353
time_elpased: 2.185
batch start
#iterations: 27
currently lose_sum: 98.75091189146042
time_elpased: 2.243
batch start
#iterations: 28
currently lose_sum: 98.79550272226334
time_elpased: 2.219
batch start
#iterations: 29
currently lose_sum: 98.64432573318481
time_elpased: 2.219
batch start
#iterations: 30
currently lose_sum: 98.61385899782181
time_elpased: 2.177
batch start
#iterations: 31
currently lose_sum: 98.54853355884552
time_elpased: 2.2
batch start
#iterations: 32
currently lose_sum: 98.64082908630371
time_elpased: 2.245
batch start
#iterations: 33
currently lose_sum: 98.76209563016891
time_elpased: 2.262
batch start
#iterations: 34
currently lose_sum: 98.16847336292267
time_elpased: 2.196
batch start
#iterations: 35
currently lose_sum: 98.47321557998657
time_elpased: 2.196
batch start
#iterations: 36
currently lose_sum: 98.42039042711258
time_elpased: 2.192
batch start
#iterations: 37
currently lose_sum: 98.35036194324493
time_elpased: 2.282
batch start
#iterations: 38
currently lose_sum: 98.10553538799286
time_elpased: 2.241
batch start
#iterations: 39
currently lose_sum: 98.19444006681442
time_elpased: 2.148
start validation test
0.617216494845
0.607106124357
0.66821035299
0.636194395454
0.617126967262
64.107
batch start
#iterations: 40
currently lose_sum: 97.902228474617
time_elpased: 2.212
batch start
#iterations: 41
currently lose_sum: 98.21272492408752
time_elpased: 2.25
batch start
#iterations: 42
currently lose_sum: 98.13313460350037
time_elpased: 2.221
batch start
#iterations: 43
currently lose_sum: 98.14359319210052
time_elpased: 2.234
batch start
#iterations: 44
currently lose_sum: 98.16337639093399
time_elpased: 2.184
batch start
#iterations: 45
currently lose_sum: 98.224937915802
time_elpased: 2.192
batch start
#iterations: 46
currently lose_sum: 98.09135413169861
time_elpased: 2.232
batch start
#iterations: 47
currently lose_sum: 98.10463666915894
time_elpased: 2.252
batch start
#iterations: 48
currently lose_sum: 98.12206572294235
time_elpased: 2.24
batch start
#iterations: 49
currently lose_sum: 98.18317031860352
time_elpased: 2.186
batch start
#iterations: 50
currently lose_sum: 98.06432545185089
time_elpased: 2.21
batch start
#iterations: 51
currently lose_sum: 98.04079604148865
time_elpased: 2.184
batch start
#iterations: 52
currently lose_sum: 98.00999313592911
time_elpased: 2.203
batch start
#iterations: 53
currently lose_sum: 97.6466451883316
time_elpased: 2.246
batch start
#iterations: 54
currently lose_sum: 97.78011310100555
time_elpased: 2.232
batch start
#iterations: 55
currently lose_sum: 97.91813498735428
time_elpased: 2.208
batch start
#iterations: 56
currently lose_sum: 97.79725742340088
time_elpased: 2.249
batch start
#iterations: 57
currently lose_sum: 97.90584760904312
time_elpased: 2.243
batch start
#iterations: 58
currently lose_sum: 97.86339497566223
time_elpased: 2.198
batch start
#iterations: 59
currently lose_sum: 98.00918400287628
time_elpased: 2.265
start validation test
0.622989690722
0.640839291994
0.562622208501
0.599188952214
0.623095675148
64.098
batch start
#iterations: 60
currently lose_sum: 97.83734601736069
time_elpased: 2.216
batch start
#iterations: 61
currently lose_sum: 98.0625342130661
time_elpased: 2.222
batch start
#iterations: 62
currently lose_sum: 98.05531007051468
time_elpased: 2.183
batch start
#iterations: 63
currently lose_sum: 97.57203644514084
time_elpased: 2.214
batch start
#iterations: 64
currently lose_sum: 97.52863681316376
time_elpased: 2.233
batch start
#iterations: 65
currently lose_sum: 97.7457605600357
time_elpased: 2.231
batch start
#iterations: 66
currently lose_sum: 97.87809753417969
time_elpased: 2.219
batch start
#iterations: 67
currently lose_sum: 97.64296704530716
time_elpased: 2.226
batch start
#iterations: 68
currently lose_sum: 97.68712371587753
time_elpased: 2.258
batch start
#iterations: 69
currently lose_sum: 97.7305366396904
time_elpased: 2.231
batch start
#iterations: 70
currently lose_sum: 97.63243240118027
time_elpased: 2.207
batch start
#iterations: 71
currently lose_sum: 97.47063481807709
time_elpased: 2.239
batch start
#iterations: 72
currently lose_sum: 97.52254378795624
time_elpased: 2.224
batch start
#iterations: 73
currently lose_sum: 97.5621046423912
time_elpased: 2.19
batch start
#iterations: 74
currently lose_sum: 97.95580422878265
time_elpased: 2.243
batch start
#iterations: 75
currently lose_sum: 97.5353307723999
time_elpased: 2.227
batch start
#iterations: 76
currently lose_sum: 97.59788936376572
time_elpased: 2.216
batch start
#iterations: 77
currently lose_sum: 97.64456969499588
time_elpased: 2.21
batch start
#iterations: 78
currently lose_sum: 97.71062135696411
time_elpased: 2.201
batch start
#iterations: 79
currently lose_sum: 97.5852033495903
time_elpased: 2.235
start validation test
0.62412371134
0.628211906524
0.611402696305
0.619693334724
0.624146045044
63.288
batch start
#iterations: 80
currently lose_sum: 97.62653160095215
time_elpased: 2.24
batch start
#iterations: 81
currently lose_sum: 97.27255749702454
time_elpased: 2.243
batch start
#iterations: 82
currently lose_sum: 97.49358016252518
time_elpased: 2.166
batch start
#iterations: 83
currently lose_sum: 97.35598963499069
time_elpased: 2.202
batch start
#iterations: 84
currently lose_sum: 97.22267234325409
time_elpased: 2.204
batch start
#iterations: 85
currently lose_sum: 97.58335107564926
time_elpased: 2.31
batch start
#iterations: 86
currently lose_sum: 97.56169748306274
time_elpased: 2.174
batch start
#iterations: 87
currently lose_sum: 97.68417119979858
time_elpased: 2.221
batch start
#iterations: 88
currently lose_sum: 97.38483214378357
time_elpased: 2.241
batch start
#iterations: 89
currently lose_sum: 97.60794448852539
time_elpased: 2.247
batch start
#iterations: 90
currently lose_sum: 97.50399041175842
time_elpased: 2.221
batch start
#iterations: 91
currently lose_sum: 97.77442634105682
time_elpased: 2.23
batch start
#iterations: 92
currently lose_sum: 97.30024874210358
time_elpased: 2.242
batch start
#iterations: 93
currently lose_sum: 97.46163761615753
time_elpased: 2.236
batch start
#iterations: 94
currently lose_sum: 97.45836490392685
time_elpased: 2.174
batch start
#iterations: 95
currently lose_sum: 97.35984754562378
time_elpased: 2.2
batch start
#iterations: 96
currently lose_sum: 97.45737731456757
time_elpased: 2.239
batch start
#iterations: 97
currently lose_sum: 97.42611688375473
time_elpased: 2.276
batch start
#iterations: 98
currently lose_sum: 97.65757405757904
time_elpased: 2.267
batch start
#iterations: 99
currently lose_sum: 97.26333516836166
time_elpased: 2.241
start validation test
0.625927835052
0.639455782313
0.580426057425
0.608512704321
0.62600772044
63.064
batch start
#iterations: 100
currently lose_sum: 97.29030776023865
time_elpased: 2.237
batch start
#iterations: 101
currently lose_sum: 97.36666911840439
time_elpased: 2.218
batch start
#iterations: 102
currently lose_sum: 97.38508200645447
time_elpased: 2.19
batch start
#iterations: 103
currently lose_sum: 97.40970307588577
time_elpased: 2.23
batch start
#iterations: 104
currently lose_sum: 97.34668856859207
time_elpased: 2.225
batch start
#iterations: 105
currently lose_sum: 97.16100734472275
time_elpased: 2.281
batch start
#iterations: 106
currently lose_sum: 97.1453492641449
time_elpased: 2.192
batch start
#iterations: 107
currently lose_sum: 97.30422973632812
time_elpased: 2.235
batch start
#iterations: 108
currently lose_sum: 97.20470303297043
time_elpased: 2.278
batch start
#iterations: 109
currently lose_sum: 97.06748294830322
time_elpased: 2.231
batch start
#iterations: 110
currently lose_sum: 97.1272052526474
time_elpased: 2.171
batch start
#iterations: 111
currently lose_sum: 96.95575076341629
time_elpased: 2.257
batch start
#iterations: 112
currently lose_sum: 97.34117829799652
time_elpased: 2.226
batch start
#iterations: 113
currently lose_sum: 97.0320200920105
time_elpased: 2.235
batch start
#iterations: 114
currently lose_sum: 96.96350014209747
time_elpased: 2.212
batch start
#iterations: 115
currently lose_sum: 97.1380490064621
time_elpased: 2.223
batch start
#iterations: 116
currently lose_sum: 97.16972726583481
time_elpased: 2.215
batch start
#iterations: 117
currently lose_sum: 97.04948234558105
time_elpased: 2.253
batch start
#iterations: 118
currently lose_sum: 97.27886873483658
time_elpased: 2.213
batch start
#iterations: 119
currently lose_sum: 97.00031048059464
time_elpased: 2.206
start validation test
0.630154639175
0.637049816692
0.608006586395
0.622189458164
0.630193523498
62.936
batch start
#iterations: 120
currently lose_sum: 96.8034000992775
time_elpased: 2.226
batch start
#iterations: 121
currently lose_sum: 96.85612118244171
time_elpased: 2.253
batch start
#iterations: 122
currently lose_sum: 97.27817171812057
time_elpased: 2.203
batch start
#iterations: 123
currently lose_sum: 96.84196507930756
time_elpased: 2.218
batch start
#iterations: 124
currently lose_sum: 97.2527517080307
time_elpased: 2.243
batch start
#iterations: 125
currently lose_sum: 97.17946630716324
time_elpased: 2.237
batch start
#iterations: 126
currently lose_sum: 97.3217180967331
time_elpased: 2.2
batch start
#iterations: 127
currently lose_sum: 96.95467257499695
time_elpased: 2.264
batch start
#iterations: 128
currently lose_sum: 96.94629967212677
time_elpased: 2.205
batch start
#iterations: 129
currently lose_sum: 97.23947262763977
time_elpased: 2.198
batch start
#iterations: 130
currently lose_sum: 96.7396115064621
time_elpased: 2.27
batch start
#iterations: 131
currently lose_sum: 97.14503490924835
time_elpased: 2.228
batch start
#iterations: 132
currently lose_sum: 96.98187285661697
time_elpased: 2.193
batch start
#iterations: 133
currently lose_sum: 97.17598766088486
time_elpased: 2.249
batch start
#iterations: 134
currently lose_sum: 96.83911973237991
time_elpased: 2.28
batch start
#iterations: 135
currently lose_sum: 96.958182990551
time_elpased: 2.207
batch start
#iterations: 136
currently lose_sum: 96.63214683532715
time_elpased: 2.214
batch start
#iterations: 137
currently lose_sum: 96.72914671897888
time_elpased: 2.236
batch start
#iterations: 138
currently lose_sum: 96.94684380292892
time_elpased: 2.184
batch start
#iterations: 139
currently lose_sum: 96.70524996519089
time_elpased: 2.237
start validation test
0.613092783505
0.615048392132
0.608212411238
0.611611300838
0.613101351751
64.048
batch start
#iterations: 140
currently lose_sum: 96.74435418844223
time_elpased: 2.173
batch start
#iterations: 141
currently lose_sum: 96.77833217382431
time_elpased: 2.204
batch start
#iterations: 142
currently lose_sum: 97.26629149913788
time_elpased: 2.218
batch start
#iterations: 143
currently lose_sum: 96.82592314481735
time_elpased: 2.22
batch start
#iterations: 144
currently lose_sum: 96.99879086017609
time_elpased: 2.227
batch start
#iterations: 145
currently lose_sum: 96.8631751537323
time_elpased: 2.223
batch start
#iterations: 146
currently lose_sum: 96.75244069099426
time_elpased: 2.203
batch start
#iterations: 147
currently lose_sum: 96.46954894065857
time_elpased: 2.23
batch start
#iterations: 148
currently lose_sum: 96.61608529090881
time_elpased: 2.286
batch start
#iterations: 149
currently lose_sum: 96.55789029598236
time_elpased: 2.279
batch start
#iterations: 150
currently lose_sum: 96.8027275800705
time_elpased: 2.267
batch start
#iterations: 151
currently lose_sum: 97.04932302236557
time_elpased: 2.238
batch start
#iterations: 152
currently lose_sum: 96.6055703163147
time_elpased: 2.252
batch start
#iterations: 153
currently lose_sum: 96.70937496423721
time_elpased: 2.201
batch start
#iterations: 154
currently lose_sum: 96.7439894080162
time_elpased: 2.235
batch start
#iterations: 155
currently lose_sum: 96.5677160024643
time_elpased: 2.213
batch start
#iterations: 156
currently lose_sum: 96.74667942523956
time_elpased: 2.193
batch start
#iterations: 157
currently lose_sum: 95.99568182229996
time_elpased: 2.234
batch start
#iterations: 158
currently lose_sum: 96.73966425657272
time_elpased: 2.23
batch start
#iterations: 159
currently lose_sum: 96.43688088655472
time_elpased: 2.212
start validation test
0.602268041237
0.620731265838
0.529381496347
0.571428571429
0.602396004809
64.141
batch start
#iterations: 160
currently lose_sum: 96.30352210998535
time_elpased: 2.204
batch start
#iterations: 161
currently lose_sum: 96.56894528865814
time_elpased: 2.226
batch start
#iterations: 162
currently lose_sum: 96.39504420757294
time_elpased: 2.253
batch start
#iterations: 163
currently lose_sum: 96.6895159482956
time_elpased: 2.264
batch start
#iterations: 164
currently lose_sum: 96.24195444583893
time_elpased: 2.218
batch start
#iterations: 165
currently lose_sum: 96.52149212360382
time_elpased: 2.228
batch start
#iterations: 166
currently lose_sum: 96.11301702260971
time_elpased: 2.247
batch start
#iterations: 167
currently lose_sum: 96.47570645809174
time_elpased: 2.26
batch start
#iterations: 168
currently lose_sum: 96.39453458786011
time_elpased: 2.208
batch start
#iterations: 169
currently lose_sum: 96.36577677726746
time_elpased: 2.21
batch start
#iterations: 170
currently lose_sum: 96.38188606500626
time_elpased: 2.183
batch start
#iterations: 171
currently lose_sum: 96.52836436033249
time_elpased: 2.21
batch start
#iterations: 172
currently lose_sum: 96.19510287046432
time_elpased: 2.214
batch start
#iterations: 173
currently lose_sum: 96.28314018249512
time_elpased: 2.178
batch start
#iterations: 174
currently lose_sum: 96.22522228956223
time_elpased: 2.221
batch start
#iterations: 175
currently lose_sum: 95.85393363237381
time_elpased: 2.194
batch start
#iterations: 176
currently lose_sum: 96.03952413797379
time_elpased: 2.253
batch start
#iterations: 177
currently lose_sum: 96.1134335398674
time_elpased: 2.2
batch start
#iterations: 178
currently lose_sum: 96.4134134054184
time_elpased: 2.206
batch start
#iterations: 179
currently lose_sum: 96.31105947494507
time_elpased: 2.206
start validation test
0.618917525773
0.641053653799
0.543480498096
0.588248398775
0.619049967111
63.207
batch start
#iterations: 180
currently lose_sum: 96.06052047014236
time_elpased: 2.259
batch start
#iterations: 181
currently lose_sum: 96.2449603676796
time_elpased: 2.206
batch start
#iterations: 182
currently lose_sum: 95.88675993680954
time_elpased: 2.301
batch start
#iterations: 183
currently lose_sum: 95.78607594966888
time_elpased: 2.23
batch start
#iterations: 184
currently lose_sum: 96.10376840829849
time_elpased: 2.208
batch start
#iterations: 185
currently lose_sum: 96.19990122318268
time_elpased: 2.171
batch start
#iterations: 186
currently lose_sum: 96.07797986268997
time_elpased: 2.219
batch start
#iterations: 187
currently lose_sum: 95.91111087799072
time_elpased: 2.226
batch start
#iterations: 188
currently lose_sum: 96.2081087231636
time_elpased: 2.267
batch start
#iterations: 189
currently lose_sum: 95.76189595460892
time_elpased: 2.218
batch start
#iterations: 190
currently lose_sum: 95.65610581636429
time_elpased: 2.184
batch start
#iterations: 191
currently lose_sum: 95.79966658353806
time_elpased: 2.198
batch start
#iterations: 192
currently lose_sum: 95.69576680660248
time_elpased: 2.242
batch start
#iterations: 193
currently lose_sum: 95.81150108575821
time_elpased: 2.25
batch start
#iterations: 194
currently lose_sum: 95.68164724111557
time_elpased: 2.255
batch start
#iterations: 195
currently lose_sum: 95.76039558649063
time_elpased: 2.22
batch start
#iterations: 196
currently lose_sum: 95.66324543952942
time_elpased: 2.224
batch start
#iterations: 197
currently lose_sum: 95.54625910520554
time_elpased: 2.246
batch start
#iterations: 198
currently lose_sum: 95.8346865773201
time_elpased: 2.243
batch start
#iterations: 199
currently lose_sum: 95.7108798623085
time_elpased: 2.197
start validation test
0.620051546392
0.632572332731
0.576000823299
0.602962563964
0.62012888423
63.424
batch start
#iterations: 200
currently lose_sum: 95.52494031190872
time_elpased: 2.228
batch start
#iterations: 201
currently lose_sum: 95.45819592475891
time_elpased: 2.257
batch start
#iterations: 202
currently lose_sum: 95.50878155231476
time_elpased: 2.252
batch start
#iterations: 203
currently lose_sum: 95.26704490184784
time_elpased: 2.267
batch start
#iterations: 204
currently lose_sum: 95.26530009508133
time_elpased: 2.199
batch start
#iterations: 205
currently lose_sum: 95.44987964630127
time_elpased: 2.215
batch start
#iterations: 206
currently lose_sum: 95.36926084756851
time_elpased: 2.231
batch start
#iterations: 207
currently lose_sum: 95.05464440584183
time_elpased: 2.237
batch start
#iterations: 208
currently lose_sum: 95.30497169494629
time_elpased: 2.19
batch start
#iterations: 209
currently lose_sum: 95.14623945951462
time_elpased: 2.208
batch start
#iterations: 210
currently lose_sum: 95.45365637540817
time_elpased: 2.229
batch start
#iterations: 211
currently lose_sum: 95.3098818063736
time_elpased: 2.226
batch start
#iterations: 212
currently lose_sum: 95.32229036092758
time_elpased: 2.197
batch start
#iterations: 213
currently lose_sum: 95.08889132738113
time_elpased: 2.245
batch start
#iterations: 214
currently lose_sum: 95.05343705415726
time_elpased: 2.246
batch start
#iterations: 215
currently lose_sum: 94.94173669815063
time_elpased: 2.223
batch start
#iterations: 216
currently lose_sum: 94.80078136920929
time_elpased: 2.232
batch start
#iterations: 217
currently lose_sum: 94.65301167964935
time_elpased: 2.249
batch start
#iterations: 218
currently lose_sum: 94.71733039617538
time_elpased: 2.2
batch start
#iterations: 219
currently lose_sum: 94.73091846704483
time_elpased: 2.186
start validation test
0.611030927835
0.628934552797
0.544921271998
0.583921482135
0.611146993533
64.160
batch start
#iterations: 220
currently lose_sum: 94.88762438297272
time_elpased: 2.264
batch start
#iterations: 221
currently lose_sum: 94.99653232097626
time_elpased: 2.206
batch start
#iterations: 222
currently lose_sum: 94.25649863481522
time_elpased: 2.231
batch start
#iterations: 223
currently lose_sum: 94.62556207180023
time_elpased: 2.214
batch start
#iterations: 224
currently lose_sum: 94.58072221279144
time_elpased: 2.196
batch start
#iterations: 225
currently lose_sum: 94.76208883523941
time_elpased: 2.201
batch start
#iterations: 226
currently lose_sum: 94.60334849357605
time_elpased: 2.207
batch start
#iterations: 227
currently lose_sum: 94.17679357528687
time_elpased: 2.229
batch start
#iterations: 228
currently lose_sum: 94.29610180854797
time_elpased: 2.273
batch start
#iterations: 229
currently lose_sum: 94.39858692884445
time_elpased: 2.242
batch start
#iterations: 230
currently lose_sum: 93.84411519765854
time_elpased: 2.234
batch start
#iterations: 231
currently lose_sum: 94.15727132558823
time_elpased: 2.287
batch start
#iterations: 232
currently lose_sum: 94.3027696609497
time_elpased: 2.233
batch start
#iterations: 233
currently lose_sum: 94.42815101146698
time_elpased: 2.235
batch start
#iterations: 234
currently lose_sum: 94.12237805128098
time_elpased: 2.238
batch start
#iterations: 235
currently lose_sum: 94.05443263053894
time_elpased: 2.297
batch start
#iterations: 236
currently lose_sum: 93.84588384628296
time_elpased: 2.214
batch start
#iterations: 237
currently lose_sum: 94.1548523902893
time_elpased: 2.208
batch start
#iterations: 238
currently lose_sum: 93.88803839683533
time_elpased: 2.202
batch start
#iterations: 239
currently lose_sum: 94.08224910497665
time_elpased: 2.227
start validation test
0.587268041237
0.631985180611
0.421323453741
0.505588144489
0.587559382556
65.082
batch start
#iterations: 240
currently lose_sum: 93.81447005271912
time_elpased: 2.182
batch start
#iterations: 241
currently lose_sum: 93.83517098426819
time_elpased: 2.284
batch start
#iterations: 242
currently lose_sum: 93.99902081489563
time_elpased: 2.208
batch start
#iterations: 243
currently lose_sum: 93.52307343482971
time_elpased: 2.27
batch start
#iterations: 244
currently lose_sum: 93.26278477907181
time_elpased: 2.211
batch start
#iterations: 245
currently lose_sum: 93.11561077833176
time_elpased: 2.215
batch start
#iterations: 246
currently lose_sum: 93.43148791790009
time_elpased: 2.222
batch start
#iterations: 247
currently lose_sum: 93.2191134095192
time_elpased: 2.274
batch start
#iterations: 248
currently lose_sum: 92.75505179166794
time_elpased: 2.177
batch start
#iterations: 249
currently lose_sum: 93.69004863500595
time_elpased: 2.232
batch start
#iterations: 250
currently lose_sum: 93.09382039308548
time_elpased: 2.24
batch start
#iterations: 251
currently lose_sum: 92.30213725566864
time_elpased: 2.209
batch start
#iterations: 252
currently lose_sum: 93.08235448598862
time_elpased: 2.242
batch start
#iterations: 253
currently lose_sum: 93.1731584072113
time_elpased: 2.248
batch start
#iterations: 254
currently lose_sum: 92.98958092927933
time_elpased: 2.27
batch start
#iterations: 255
currently lose_sum: 92.95886939764023
time_elpased: 2.235
batch start
#iterations: 256
currently lose_sum: 92.85226279497147
time_elpased: 2.232
batch start
#iterations: 257
currently lose_sum: 92.52805149555206
time_elpased: 2.231
batch start
#iterations: 258
currently lose_sum: 91.99115586280823
time_elpased: 2.204
batch start
#iterations: 259
currently lose_sum: 91.88488775491714
time_elpased: 2.231
start validation test
0.599690721649
0.615320959924
0.53565915406
0.572733274648
0.599803138943
65.797
batch start
#iterations: 260
currently lose_sum: 92.13534891605377
time_elpased: 2.21
batch start
#iterations: 261
currently lose_sum: 92.15876358747482
time_elpased: 2.193
batch start
#iterations: 262
currently lose_sum: 92.1262823343277
time_elpased: 2.232
batch start
#iterations: 263
currently lose_sum: 91.89323711395264
time_elpased: 2.206
batch start
#iterations: 264
currently lose_sum: 91.62685644626617
time_elpased: 2.294
batch start
#iterations: 265
currently lose_sum: 91.98925817012787
time_elpased: 2.228
batch start
#iterations: 266
currently lose_sum: 91.22237348556519
time_elpased: 2.224
batch start
#iterations: 267
currently lose_sum: 91.2830661535263
time_elpased: 2.214
batch start
#iterations: 268
currently lose_sum: 90.85442584753036
time_elpased: 2.256
batch start
#iterations: 269
currently lose_sum: 91.65989148616791
time_elpased: 2.277
batch start
#iterations: 270
currently lose_sum: 91.29387652873993
time_elpased: 2.241
batch start
#iterations: 271
currently lose_sum: 91.01755917072296
time_elpased: 2.204
batch start
#iterations: 272
currently lose_sum: 90.92810183763504
time_elpased: 2.237
batch start
#iterations: 273
currently lose_sum: 90.42547106742859
time_elpased: 2.235
batch start
#iterations: 274
currently lose_sum: 90.9117084145546
time_elpased: 2.198
batch start
#iterations: 275
currently lose_sum: 90.87793689966202
time_elpased: 2.239
batch start
#iterations: 276
currently lose_sum: 90.57710748910904
time_elpased: 2.213
batch start
#iterations: 277
currently lose_sum: 90.2923623919487
time_elpased: 2.23
batch start
#iterations: 278
currently lose_sum: 90.06762313842773
time_elpased: 2.214
batch start
#iterations: 279
currently lose_sum: 90.14969331026077
time_elpased: 2.233
start validation test
0.590051546392
0.625676831006
0.451888442935
0.524768449358
0.59029411303
68.034
batch start
#iterations: 280
currently lose_sum: 90.26716589927673
time_elpased: 2.201
batch start
#iterations: 281
currently lose_sum: 89.11705470085144
time_elpased: 2.221
batch start
#iterations: 282
currently lose_sum: 89.68169128894806
time_elpased: 2.259
batch start
#iterations: 283
currently lose_sum: 89.59554296731949
time_elpased: 2.267
batch start
#iterations: 284
currently lose_sum: 89.80888545513153
time_elpased: 2.184
batch start
#iterations: 285
currently lose_sum: 89.21599739789963
time_elpased: 2.217
batch start
#iterations: 286
currently lose_sum: 89.37177300453186
time_elpased: 2.245
batch start
#iterations: 287
currently lose_sum: 88.85270774364471
time_elpased: 2.228
batch start
#iterations: 288
currently lose_sum: 89.2366019487381
time_elpased: 2.252
batch start
#iterations: 289
currently lose_sum: 88.66003686189651
time_elpased: 2.209
batch start
#iterations: 290
currently lose_sum: 87.95960056781769
time_elpased: 2.202
batch start
#iterations: 291
currently lose_sum: 88.70308125019073
time_elpased: 2.175
batch start
#iterations: 292
currently lose_sum: 88.50782990455627
time_elpased: 2.228
batch start
#iterations: 293
currently lose_sum: 88.20129835605621
time_elpased: 2.225
batch start
#iterations: 294
currently lose_sum: 87.52533155679703
time_elpased: 2.165
batch start
#iterations: 295
currently lose_sum: 87.998654961586
time_elpased: 2.227
batch start
#iterations: 296
currently lose_sum: 87.5795037150383
time_elpased: 2.232
batch start
#iterations: 297
currently lose_sum: 87.53707998991013
time_elpased: 2.233
batch start
#iterations: 298
currently lose_sum: 87.52824127674103
time_elpased: 2.199
batch start
#iterations: 299
currently lose_sum: 87.2395641207695
time_elpased: 2.217
start validation test
0.588865979381
0.611559656542
0.491098075538
0.544748858447
0.589037626016
71.437
batch start
#iterations: 300
currently lose_sum: 86.85515832901001
time_elpased: 2.289
batch start
#iterations: 301
currently lose_sum: 86.86155569553375
time_elpased: 2.253
batch start
#iterations: 302
currently lose_sum: 86.77579218149185
time_elpased: 2.234
batch start
#iterations: 303
currently lose_sum: 86.69312399625778
time_elpased: 2.213
batch start
#iterations: 304
currently lose_sum: 86.59855663776398
time_elpased: 2.235
batch start
#iterations: 305
currently lose_sum: 86.38738960027695
time_elpased: 2.223
batch start
#iterations: 306
currently lose_sum: 85.63205426931381
time_elpased: 2.226
batch start
#iterations: 307
currently lose_sum: 85.97660720348358
time_elpased: 2.207
batch start
#iterations: 308
currently lose_sum: 85.36610502004623
time_elpased: 2.229
batch start
#iterations: 309
currently lose_sum: 84.9666556417942
time_elpased: 2.228
batch start
#iterations: 310
currently lose_sum: 85.49045127630234
time_elpased: 2.176
batch start
#iterations: 311
currently lose_sum: 85.36699914932251
time_elpased: 2.254
batch start
#iterations: 312
currently lose_sum: 84.85678601264954
time_elpased: 2.205
batch start
#iterations: 313
currently lose_sum: 84.12265187501907
time_elpased: 2.25
batch start
#iterations: 314
currently lose_sum: 84.18359243869781
time_elpased: 2.258
batch start
#iterations: 315
currently lose_sum: 83.64005744457245
time_elpased: 2.273
batch start
#iterations: 316
currently lose_sum: 84.50683218240738
time_elpased: 2.214
batch start
#iterations: 317
currently lose_sum: 83.71820557117462
time_elpased: 2.241
batch start
#iterations: 318
currently lose_sum: 83.42456322908401
time_elpased: 2.229
batch start
#iterations: 319
currently lose_sum: 83.38448470830917
time_elpased: 2.255
start validation test
0.577113402062
0.617268640521
0.40979726253
0.492577931717
0.577407151352
78.844
batch start
#iterations: 320
currently lose_sum: 83.31712204217911
time_elpased: 2.233
batch start
#iterations: 321
currently lose_sum: 82.96693122386932
time_elpased: 2.218
batch start
#iterations: 322
currently lose_sum: 82.6649381518364
time_elpased: 2.235
batch start
#iterations: 323
currently lose_sum: 82.32478702068329
time_elpased: 2.252
batch start
#iterations: 324
currently lose_sum: 82.34707167744637
time_elpased: 2.242
batch start
#iterations: 325
currently lose_sum: 81.64666298031807
time_elpased: 2.215
batch start
#iterations: 326
currently lose_sum: 81.44117051362991
time_elpased: 2.242
batch start
#iterations: 327
currently lose_sum: 81.4989144206047
time_elpased: 2.186
batch start
#iterations: 328
currently lose_sum: 81.7627888917923
time_elpased: 2.219
batch start
#iterations: 329
currently lose_sum: 81.94606256484985
time_elpased: 2.275
batch start
#iterations: 330
currently lose_sum: 81.12618708610535
time_elpased: 2.261
batch start
#iterations: 331
currently lose_sum: 81.23769107460976
time_elpased: 2.231
batch start
#iterations: 332
currently lose_sum: 80.68728095293045
time_elpased: 2.265
batch start
#iterations: 333
currently lose_sum: 79.55109989643097
time_elpased: 2.206
batch start
#iterations: 334
currently lose_sum: 80.46283483505249
time_elpased: 2.252
batch start
#iterations: 335
currently lose_sum: 80.11835664510727
time_elpased: 2.204
batch start
#iterations: 336
currently lose_sum: 79.62070971727371
time_elpased: 2.255
batch start
#iterations: 337
currently lose_sum: 79.64759677648544
time_elpased: 2.248
batch start
#iterations: 338
currently lose_sum: 79.17990958690643
time_elpased: 2.249
batch start
#iterations: 339
currently lose_sum: 79.77905204892159
time_elpased: 2.276
start validation test
0.56618556701
0.624688518306
0.335391581764
0.436453729744
0.566590761449
89.775
batch start
#iterations: 340
currently lose_sum: 79.30799782276154
time_elpased: 2.241
batch start
#iterations: 341
currently lose_sum: 79.01678878068924
time_elpased: 2.242
batch start
#iterations: 342
currently lose_sum: 78.45178803801537
time_elpased: 2.221
batch start
#iterations: 343
currently lose_sum: 79.06908759474754
time_elpased: 2.213
batch start
#iterations: 344
currently lose_sum: 78.24121767282486
time_elpased: 2.235
batch start
#iterations: 345
currently lose_sum: 77.54715803265572
time_elpased: 2.233
batch start
#iterations: 346
currently lose_sum: 78.31567144393921
time_elpased: 2.274
batch start
#iterations: 347
currently lose_sum: 77.39841943979263
time_elpased: 2.237
batch start
#iterations: 348
currently lose_sum: 77.18032237887383
time_elpased: 2.272
batch start
#iterations: 349
currently lose_sum: 77.44358864426613
time_elpased: 2.244
batch start
#iterations: 350
currently lose_sum: 76.8678662776947
time_elpased: 2.273
batch start
#iterations: 351
currently lose_sum: 75.94004127383232
time_elpased: 2.258
batch start
#iterations: 352
currently lose_sum: 76.06640121340752
time_elpased: 2.242
batch start
#iterations: 353
currently lose_sum: 76.46104422211647
time_elpased: 2.245
batch start
#iterations: 354
currently lose_sum: 76.45049116015434
time_elpased: 2.24
batch start
#iterations: 355
currently lose_sum: 75.82256352901459
time_elpased: 2.182
batch start
#iterations: 356
currently lose_sum: 76.49858665466309
time_elpased: 2.271
batch start
#iterations: 357
currently lose_sum: 76.35250481963158
time_elpased: 2.253
batch start
#iterations: 358
currently lose_sum: 76.19436848163605
time_elpased: 2.271
batch start
#iterations: 359
currently lose_sum: 75.19846832752228
time_elpased: 2.222
start validation test
0.56412371134
0.59816285225
0.395389523515
0.476084262701
0.564419950232
99.356
batch start
#iterations: 360
currently lose_sum: 75.09303906559944
time_elpased: 2.239
batch start
#iterations: 361
currently lose_sum: 75.30970615148544
time_elpased: 2.224
batch start
#iterations: 362
currently lose_sum: 75.26563623547554
time_elpased: 2.214
batch start
#iterations: 363
currently lose_sum: 75.0799001455307
time_elpased: 2.189
batch start
#iterations: 364
currently lose_sum: 74.48328995704651
time_elpased: 2.188
batch start
#iterations: 365
currently lose_sum: 74.42651745676994
time_elpased: 2.192
batch start
#iterations: 366
currently lose_sum: 74.87109538912773
time_elpased: 2.248
batch start
#iterations: 367
currently lose_sum: 74.11122906208038
time_elpased: 2.221
batch start
#iterations: 368
currently lose_sum: 74.31062677502632
time_elpased: 2.224
batch start
#iterations: 369
currently lose_sum: 73.35695594549179
time_elpased: 2.224
batch start
#iterations: 370
currently lose_sum: 72.99428924918175
time_elpased: 2.213
batch start
#iterations: 371
currently lose_sum: 73.62103399634361
time_elpased: 2.255
batch start
#iterations: 372
currently lose_sum: 73.15386971831322
time_elpased: 2.214
batch start
#iterations: 373
currently lose_sum: 73.33458495140076
time_elpased: 2.207
batch start
#iterations: 374
currently lose_sum: 73.12156024575233
time_elpased: 2.237
batch start
#iterations: 375
currently lose_sum: 73.26112240552902
time_elpased: 2.224
batch start
#iterations: 376
currently lose_sum: 72.56714105606079
time_elpased: 2.253
batch start
#iterations: 377
currently lose_sum: 72.65150648355484
time_elpased: 2.227
batch start
#iterations: 378
currently lose_sum: 72.36740148067474
time_elpased: 2.233
batch start
#iterations: 379
currently lose_sum: 71.93612450361252
time_elpased: 2.276
start validation test
0.557731958763
0.595626576955
0.364412884635
0.452177244286
0.558071360215
116.122
batch start
#iterations: 380
currently lose_sum: 72.20336201786995
time_elpased: 2.187
batch start
#iterations: 381
currently lose_sum: 72.21435269713402
time_elpased: 2.237
batch start
#iterations: 382
currently lose_sum: 71.12522971630096
time_elpased: 2.249
batch start
#iterations: 383
currently lose_sum: 72.42570862174034
time_elpased: 2.235
batch start
#iterations: 384
currently lose_sum: 71.13215085864067
time_elpased: 2.218
batch start
#iterations: 385
currently lose_sum: 71.25335368514061
time_elpased: 2.243
batch start
#iterations: 386
currently lose_sum: 71.62452909350395
time_elpased: 2.256
batch start
#iterations: 387
currently lose_sum: 70.58816474676132
time_elpased: 2.216
batch start
#iterations: 388
currently lose_sum: 69.94975104928017
time_elpased: 2.226
batch start
#iterations: 389
currently lose_sum: 71.65563136339188
time_elpased: 2.192
batch start
#iterations: 390
currently lose_sum: 70.89148080348969
time_elpased: 2.235
batch start
#iterations: 391
currently lose_sum: 70.42254957556725
time_elpased: 2.212
batch start
#iterations: 392
currently lose_sum: 70.33929565548897
time_elpased: 2.222
batch start
#iterations: 393
currently lose_sum: 70.58653816580772
time_elpased: 2.208
batch start
#iterations: 394
currently lose_sum: 70.51728019118309
time_elpased: 2.202
batch start
#iterations: 395
currently lose_sum: 69.3915698826313
time_elpased: 2.225
batch start
#iterations: 396
currently lose_sum: 69.5843356847763
time_elpased: 2.252
batch start
#iterations: 397
currently lose_sum: 69.79085585474968
time_elpased: 2.205
batch start
#iterations: 398
currently lose_sum: 69.44365975260735
time_elpased: 2.213
batch start
#iterations: 399
currently lose_sum: 69.2621677517891
time_elpased: 2.229
start validation test
0.551649484536
0.605246849824
0.301533395081
0.402527819755
0.552088601909
128.216
acc: 0.626
pre: 0.635
rec: 0.595
F1: 0.614
auc: 0.667
