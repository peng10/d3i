start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.73893386125565
time_elpased: 1.34
batch start
#iterations: 1
currently lose_sum: 100.289421916008
time_elpased: 1.327
batch start
#iterations: 2
currently lose_sum: 100.0000512599945
time_elpased: 1.31
batch start
#iterations: 3
currently lose_sum: 100.06597065925598
time_elpased: 1.337
batch start
#iterations: 4
currently lose_sum: 99.97644972801208
time_elpased: 1.34
batch start
#iterations: 5
currently lose_sum: 100.0025964975357
time_elpased: 1.325
batch start
#iterations: 6
currently lose_sum: 99.94300937652588
time_elpased: 1.315
batch start
#iterations: 7
currently lose_sum: 99.76812064647675
time_elpased: 1.321
batch start
#iterations: 8
currently lose_sum: 99.75880026817322
time_elpased: 1.325
batch start
#iterations: 9
currently lose_sum: 99.78841596841812
time_elpased: 1.326
batch start
#iterations: 10
currently lose_sum: 99.71260613203049
time_elpased: 1.323
batch start
#iterations: 11
currently lose_sum: 99.73812222480774
time_elpased: 1.335
batch start
#iterations: 12
currently lose_sum: 99.90324759483337
time_elpased: 1.338
batch start
#iterations: 13
currently lose_sum: 99.6454706788063
time_elpased: 1.331
batch start
#iterations: 14
currently lose_sum: 99.70598924160004
time_elpased: 1.338
batch start
#iterations: 15
currently lose_sum: 99.45209956169128
time_elpased: 1.328
batch start
#iterations: 16
currently lose_sum: 99.64736193418503
time_elpased: 1.364
batch start
#iterations: 17
currently lose_sum: 99.73685562610626
time_elpased: 1.341
batch start
#iterations: 18
currently lose_sum: 99.5954162478447
time_elpased: 1.333
batch start
#iterations: 19
currently lose_sum: 99.7592601776123
time_elpased: 1.337
start validation test
0.591288659794
0.629904097646
0.446125347329
0.522320621724
0.591543516379
65.443
batch start
#iterations: 20
currently lose_sum: 99.59729546308517
time_elpased: 1.319
batch start
#iterations: 21
currently lose_sum: 99.53965538740158
time_elpased: 1.332
batch start
#iterations: 22
currently lose_sum: 99.59105372428894
time_elpased: 1.339
batch start
#iterations: 23
currently lose_sum: 99.43608558177948
time_elpased: 1.373
batch start
#iterations: 24
currently lose_sum: 99.56012171506882
time_elpased: 1.325
batch start
#iterations: 25
currently lose_sum: 99.35204803943634
time_elpased: 1.323
batch start
#iterations: 26
currently lose_sum: 99.52997994422913
time_elpased: 1.327
batch start
#iterations: 27
currently lose_sum: 99.39201772212982
time_elpased: 1.327
batch start
#iterations: 28
currently lose_sum: 99.45523470640182
time_elpased: 1.344
batch start
#iterations: 29
currently lose_sum: 99.50665491819382
time_elpased: 1.351
batch start
#iterations: 30
currently lose_sum: 99.38621723651886
time_elpased: 1.358
batch start
#iterations: 31
currently lose_sum: 99.42322009801865
time_elpased: 1.323
batch start
#iterations: 32
currently lose_sum: 99.17149263620377
time_elpased: 1.346
batch start
#iterations: 33
currently lose_sum: 99.32835513353348
time_elpased: 1.328
batch start
#iterations: 34
currently lose_sum: 99.34344094991684
time_elpased: 1.339
batch start
#iterations: 35
currently lose_sum: 99.293492436409
time_elpased: 1.332
batch start
#iterations: 36
currently lose_sum: 99.66880881786346
time_elpased: 1.326
batch start
#iterations: 37
currently lose_sum: 99.43574118614197
time_elpased: 1.331
batch start
#iterations: 38
currently lose_sum: 99.32516205310822
time_elpased: 1.319
batch start
#iterations: 39
currently lose_sum: 99.49008238315582
time_elpased: 1.368
start validation test
0.614845360825
0.639736088634
0.528866934239
0.579042253521
0.614996309214
64.917
batch start
#iterations: 40
currently lose_sum: 99.44978547096252
time_elpased: 1.341
batch start
#iterations: 41
currently lose_sum: 99.2834062576294
time_elpased: 1.39
batch start
#iterations: 42
currently lose_sum: 99.4553673863411
time_elpased: 1.315
batch start
#iterations: 43
currently lose_sum: 99.3594297170639
time_elpased: 1.338
batch start
#iterations: 44
currently lose_sum: 99.41439831256866
time_elpased: 1.354
batch start
#iterations: 45
currently lose_sum: 99.26821458339691
time_elpased: 1.325
batch start
#iterations: 46
currently lose_sum: 99.39217132329941
time_elpased: 1.349
batch start
#iterations: 47
currently lose_sum: 99.37099277973175
time_elpased: 1.367
batch start
#iterations: 48
currently lose_sum: 99.4351664185524
time_elpased: 1.355
batch start
#iterations: 49
currently lose_sum: 99.38549971580505
time_elpased: 1.317
batch start
#iterations: 50
currently lose_sum: 99.62784367799759
time_elpased: 1.348
batch start
#iterations: 51
currently lose_sum: 99.3326325416565
time_elpased: 1.359
batch start
#iterations: 52
currently lose_sum: 99.24931704998016
time_elpased: 1.349
batch start
#iterations: 53
currently lose_sum: 99.54901200532913
time_elpased: 1.339
batch start
#iterations: 54
currently lose_sum: 99.41976761817932
time_elpased: 1.32
batch start
#iterations: 55
currently lose_sum: 99.24458599090576
time_elpased: 1.344
batch start
#iterations: 56
currently lose_sum: 99.44393986463547
time_elpased: 1.353
batch start
#iterations: 57
currently lose_sum: 99.22653079032898
time_elpased: 1.356
batch start
#iterations: 58
currently lose_sum: 99.33355242013931
time_elpased: 1.339
batch start
#iterations: 59
currently lose_sum: 99.31896936893463
time_elpased: 1.348
start validation test
0.618711340206
0.630807397384
0.575692086035
0.601990852838
0.618786867142
64.747
batch start
#iterations: 60
currently lose_sum: 99.46736007928848
time_elpased: 1.317
batch start
#iterations: 61
currently lose_sum: 99.37203168869019
time_elpased: 1.39
batch start
#iterations: 62
currently lose_sum: 99.23604434728622
time_elpased: 1.318
batch start
#iterations: 63
currently lose_sum: 99.5892025232315
time_elpased: 1.334
batch start
#iterations: 64
currently lose_sum: 99.52371072769165
time_elpased: 1.333
batch start
#iterations: 65
currently lose_sum: 99.28732025623322
time_elpased: 1.356
batch start
#iterations: 66
currently lose_sum: 99.09822410345078
time_elpased: 1.335
batch start
#iterations: 67
currently lose_sum: 99.31378155946732
time_elpased: 1.342
batch start
#iterations: 68
currently lose_sum: 99.29574590921402
time_elpased: 1.317
batch start
#iterations: 69
currently lose_sum: 99.35810983181
time_elpased: 1.34
batch start
#iterations: 70
currently lose_sum: 99.15898686647415
time_elpased: 1.327
batch start
#iterations: 71
currently lose_sum: 99.11478996276855
time_elpased: 1.306
batch start
#iterations: 72
currently lose_sum: 99.08091187477112
time_elpased: 1.32
batch start
#iterations: 73
currently lose_sum: 99.22725397348404
time_elpased: 1.327
batch start
#iterations: 74
currently lose_sum: 99.19421255588531
time_elpased: 1.4
batch start
#iterations: 75
currently lose_sum: 98.92211890220642
time_elpased: 1.358
batch start
#iterations: 76
currently lose_sum: 99.11375057697296
time_elpased: 1.343
batch start
#iterations: 77
currently lose_sum: 99.05558609962463
time_elpased: 1.317
batch start
#iterations: 78
currently lose_sum: 99.19119727611542
time_elpased: 1.344
batch start
#iterations: 79
currently lose_sum: 99.3948125243187
time_elpased: 1.339
start validation test
0.621030927835
0.638482257876
0.561078522178
0.597283085013
0.62113618353
64.632
batch start
#iterations: 80
currently lose_sum: 99.26706659793854
time_elpased: 1.386
batch start
#iterations: 81
currently lose_sum: 99.43813556432724
time_elpased: 1.339
batch start
#iterations: 82
currently lose_sum: 99.27939051389694
time_elpased: 1.34
batch start
#iterations: 83
currently lose_sum: 99.34657418727875
time_elpased: 1.329
batch start
#iterations: 84
currently lose_sum: 99.24233812093735
time_elpased: 1.35
batch start
#iterations: 85
currently lose_sum: 99.34733480215073
time_elpased: 1.324
batch start
#iterations: 86
currently lose_sum: 99.1428234577179
time_elpased: 1.331
batch start
#iterations: 87
currently lose_sum: 99.11040526628494
time_elpased: 1.339
batch start
#iterations: 88
currently lose_sum: 99.14904576539993
time_elpased: 1.371
batch start
#iterations: 89
currently lose_sum: 99.18012201786041
time_elpased: 1.348
batch start
#iterations: 90
currently lose_sum: 99.21621435880661
time_elpased: 1.335
batch start
#iterations: 91
currently lose_sum: 99.1097804903984
time_elpased: 1.361
batch start
#iterations: 92
currently lose_sum: 99.30131059885025
time_elpased: 1.34
batch start
#iterations: 93
currently lose_sum: 99.17914146184921
time_elpased: 1.354
batch start
#iterations: 94
currently lose_sum: 99.11159318685532
time_elpased: 1.334
batch start
#iterations: 95
currently lose_sum: 99.24466162919998
time_elpased: 1.344
batch start
#iterations: 96
currently lose_sum: 99.29281878471375
time_elpased: 1.335
batch start
#iterations: 97
currently lose_sum: 99.28503102064133
time_elpased: 1.321
batch start
#iterations: 98
currently lose_sum: 99.35251981019974
time_elpased: 1.334
batch start
#iterations: 99
currently lose_sum: 99.1336373090744
time_elpased: 1.34
start validation test
0.610927835052
0.651107705169
0.480909745806
0.553214158873
0.611156101861
64.599
batch start
#iterations: 100
currently lose_sum: 99.11607068777084
time_elpased: 1.333
batch start
#iterations: 101
currently lose_sum: 99.39438688755035
time_elpased: 1.342
batch start
#iterations: 102
currently lose_sum: 99.10329675674438
time_elpased: 1.332
batch start
#iterations: 103
currently lose_sum: 99.24633967876434
time_elpased: 1.315
batch start
#iterations: 104
currently lose_sum: 99.21814554929733
time_elpased: 1.353
batch start
#iterations: 105
currently lose_sum: 99.0760589838028
time_elpased: 1.344
batch start
#iterations: 106
currently lose_sum: 99.42031729221344
time_elpased: 1.341
batch start
#iterations: 107
currently lose_sum: 99.09482723474503
time_elpased: 1.389
batch start
#iterations: 108
currently lose_sum: 99.13361954689026
time_elpased: 1.334
batch start
#iterations: 109
currently lose_sum: 99.09231770038605
time_elpased: 1.386
batch start
#iterations: 110
currently lose_sum: 99.2896277308464
time_elpased: 1.349
batch start
#iterations: 111
currently lose_sum: 99.16791552305222
time_elpased: 1.351
batch start
#iterations: 112
currently lose_sum: 99.23728096485138
time_elpased: 1.327
batch start
#iterations: 113
currently lose_sum: 99.0962889790535
time_elpased: 1.359
batch start
#iterations: 114
currently lose_sum: 99.1091845035553
time_elpased: 1.358
batch start
#iterations: 115
currently lose_sum: 99.07182550430298
time_elpased: 1.337
batch start
#iterations: 116
currently lose_sum: 99.00525188446045
time_elpased: 1.335
batch start
#iterations: 117
currently lose_sum: 99.26110208034515
time_elpased: 1.333
batch start
#iterations: 118
currently lose_sum: 99.14349526166916
time_elpased: 1.358
batch start
#iterations: 119
currently lose_sum: 99.17625170946121
time_elpased: 1.36
start validation test
0.620515463918
0.649599796722
0.526191211279
0.58141914942
0.620681064692
64.605
batch start
#iterations: 120
currently lose_sum: 99.24500650167465
time_elpased: 1.347
batch start
#iterations: 121
currently lose_sum: 99.26049906015396
time_elpased: 1.368
batch start
#iterations: 122
currently lose_sum: 99.161816239357
time_elpased: 1.339
batch start
#iterations: 123
currently lose_sum: 99.29011797904968
time_elpased: 1.342
batch start
#iterations: 124
currently lose_sum: 99.24958783388138
time_elpased: 1.33
batch start
#iterations: 125
currently lose_sum: 99.22337460517883
time_elpased: 1.368
batch start
#iterations: 126
currently lose_sum: 99.12083804607391
time_elpased: 1.332
batch start
#iterations: 127
currently lose_sum: 99.15977358818054
time_elpased: 1.342
batch start
#iterations: 128
currently lose_sum: 99.17896574735641
time_elpased: 1.357
batch start
#iterations: 129
currently lose_sum: 99.18982344865799
time_elpased: 1.36
batch start
#iterations: 130
currently lose_sum: 99.21043795347214
time_elpased: 1.339
batch start
#iterations: 131
currently lose_sum: 99.38679945468903
time_elpased: 1.332
batch start
#iterations: 132
currently lose_sum: 99.18503451347351
time_elpased: 1.375
batch start
#iterations: 133
currently lose_sum: 99.15946584939957
time_elpased: 1.332
batch start
#iterations: 134
currently lose_sum: 99.07653146982193
time_elpased: 1.358
batch start
#iterations: 135
currently lose_sum: 99.28208547830582
time_elpased: 1.333
batch start
#iterations: 136
currently lose_sum: 99.21085584163666
time_elpased: 1.331
batch start
#iterations: 137
currently lose_sum: 99.07113111019135
time_elpased: 1.324
batch start
#iterations: 138
currently lose_sum: 99.01755148172379
time_elpased: 1.357
batch start
#iterations: 139
currently lose_sum: 99.35216170549393
time_elpased: 1.33
start validation test
0.616082474227
0.651407980782
0.502315529484
0.567228355607
0.616282209646
64.644
batch start
#iterations: 140
currently lose_sum: 99.03183245658875
time_elpased: 1.348
batch start
#iterations: 141
currently lose_sum: 99.18125832080841
time_elpased: 1.354
batch start
#iterations: 142
currently lose_sum: 99.04429930448532
time_elpased: 1.364
batch start
#iterations: 143
currently lose_sum: 99.38086199760437
time_elpased: 1.353
batch start
#iterations: 144
currently lose_sum: 99.2586989402771
time_elpased: 1.346
batch start
#iterations: 145
currently lose_sum: 99.12667292356491
time_elpased: 1.352
batch start
#iterations: 146
currently lose_sum: 99.26007109880447
time_elpased: 1.338
batch start
#iterations: 147
currently lose_sum: 99.50858199596405
time_elpased: 1.348
batch start
#iterations: 148
currently lose_sum: 99.1701854467392
time_elpased: 1.363
batch start
#iterations: 149
currently lose_sum: 99.2717564702034
time_elpased: 1.325
batch start
#iterations: 150
currently lose_sum: 99.23657888174057
time_elpased: 1.339
batch start
#iterations: 151
currently lose_sum: 99.06625711917877
time_elpased: 1.333
batch start
#iterations: 152
currently lose_sum: 99.16049635410309
time_elpased: 1.374
batch start
#iterations: 153
currently lose_sum: 99.16188311576843
time_elpased: 1.309
batch start
#iterations: 154
currently lose_sum: 99.1958521604538
time_elpased: 1.318
batch start
#iterations: 155
currently lose_sum: 99.25187611579895
time_elpased: 1.324
batch start
#iterations: 156
currently lose_sum: 99.03444147109985
time_elpased: 1.345
batch start
#iterations: 157
currently lose_sum: 99.13978779315948
time_elpased: 1.355
batch start
#iterations: 158
currently lose_sum: 99.21130686998367
time_elpased: 1.322
batch start
#iterations: 159
currently lose_sum: 99.2199696302414
time_elpased: 1.318
start validation test
0.625412371134
0.632260850788
0.602655140475
0.617103113968
0.625452324963
64.478
batch start
#iterations: 160
currently lose_sum: 99.09630954265594
time_elpased: 1.341
batch start
#iterations: 161
currently lose_sum: 99.26477086544037
time_elpased: 1.329
batch start
#iterations: 162
currently lose_sum: 99.11937683820724
time_elpased: 1.36
batch start
#iterations: 163
currently lose_sum: 99.18538588285446
time_elpased: 1.377
batch start
#iterations: 164
currently lose_sum: 99.48550009727478
time_elpased: 1.354
batch start
#iterations: 165
currently lose_sum: 99.20424574613571
time_elpased: 1.362
batch start
#iterations: 166
currently lose_sum: 99.09665888547897
time_elpased: 1.362
batch start
#iterations: 167
currently lose_sum: 99.14164519309998
time_elpased: 1.349
batch start
#iterations: 168
currently lose_sum: 99.12156200408936
time_elpased: 1.353
batch start
#iterations: 169
currently lose_sum: 99.13123524188995
time_elpased: 1.361
batch start
#iterations: 170
currently lose_sum: 99.23350042104721
time_elpased: 1.321
batch start
#iterations: 171
currently lose_sum: 99.17129462957382
time_elpased: 1.36
batch start
#iterations: 172
currently lose_sum: 99.11101424694061
time_elpased: 1.346
batch start
#iterations: 173
currently lose_sum: 99.29454797506332
time_elpased: 1.367
batch start
#iterations: 174
currently lose_sum: 99.0101990699768
time_elpased: 1.379
batch start
#iterations: 175
currently lose_sum: 99.25567412376404
time_elpased: 1.334
batch start
#iterations: 176
currently lose_sum: 99.05344212055206
time_elpased: 1.377
batch start
#iterations: 177
currently lose_sum: 99.13650041818619
time_elpased: 1.368
batch start
#iterations: 178
currently lose_sum: 99.22885018587112
time_elpased: 1.339
batch start
#iterations: 179
currently lose_sum: 98.96781587600708
time_elpased: 1.351
start validation test
0.619948453608
0.642961697975
0.542451373881
0.588445436785
0.620084511685
64.430
batch start
#iterations: 180
currently lose_sum: 99.22736769914627
time_elpased: 1.354
batch start
#iterations: 181
currently lose_sum: 99.17021411657333
time_elpased: 1.354
batch start
#iterations: 182
currently lose_sum: 99.31714951992035
time_elpased: 1.342
batch start
#iterations: 183
currently lose_sum: 99.01789110898972
time_elpased: 1.349
batch start
#iterations: 184
currently lose_sum: 98.85597211122513
time_elpased: 1.362
batch start
#iterations: 185
currently lose_sum: 99.20653522014618
time_elpased: 1.363
batch start
#iterations: 186
currently lose_sum: 99.04589402675629
time_elpased: 1.384
batch start
#iterations: 187
currently lose_sum: 98.93644005060196
time_elpased: 1.361
batch start
#iterations: 188
currently lose_sum: 99.02231043577194
time_elpased: 1.339
batch start
#iterations: 189
currently lose_sum: 99.2397375702858
time_elpased: 1.357
batch start
#iterations: 190
currently lose_sum: 99.09215641021729
time_elpased: 1.332
batch start
#iterations: 191
currently lose_sum: 99.09536916017532
time_elpased: 1.333
batch start
#iterations: 192
currently lose_sum: 98.95380473136902
time_elpased: 1.351
batch start
#iterations: 193
currently lose_sum: 99.00249671936035
time_elpased: 1.345
batch start
#iterations: 194
currently lose_sum: 98.9527645111084
time_elpased: 1.333
batch start
#iterations: 195
currently lose_sum: 99.2177169919014
time_elpased: 1.341
batch start
#iterations: 196
currently lose_sum: 99.03863221406937
time_elpased: 1.378
batch start
#iterations: 197
currently lose_sum: 99.27147763967514
time_elpased: 1.34
batch start
#iterations: 198
currently lose_sum: 99.00468647480011
time_elpased: 1.365
batch start
#iterations: 199
currently lose_sum: 99.15262073278427
time_elpased: 1.313
start validation test
0.622680412371
0.640619500176
0.56190182155
0.598684210526
0.622787118562
64.395
batch start
#iterations: 200
currently lose_sum: 99.16921877861023
time_elpased: 1.355
batch start
#iterations: 201
currently lose_sum: 99.0092915892601
time_elpased: 1.338
batch start
#iterations: 202
currently lose_sum: 99.13385617733002
time_elpased: 1.384
batch start
#iterations: 203
currently lose_sum: 99.12534689903259
time_elpased: 1.329
batch start
#iterations: 204
currently lose_sum: 99.31961107254028
time_elpased: 1.362
batch start
#iterations: 205
currently lose_sum: 99.08229941129684
time_elpased: 1.336
batch start
#iterations: 206
currently lose_sum: 99.08648186922073
time_elpased: 1.336
batch start
#iterations: 207
currently lose_sum: 99.04012966156006
time_elpased: 1.323
batch start
#iterations: 208
currently lose_sum: 99.00700110197067
time_elpased: 1.338
batch start
#iterations: 209
currently lose_sum: 98.91494911909103
time_elpased: 1.343
batch start
#iterations: 210
currently lose_sum: 99.00792163610458
time_elpased: 1.355
batch start
#iterations: 211
currently lose_sum: 99.10603100061417
time_elpased: 1.361
batch start
#iterations: 212
currently lose_sum: 98.94311481714249
time_elpased: 1.349
batch start
#iterations: 213
currently lose_sum: 99.03373563289642
time_elpased: 1.334
batch start
#iterations: 214
currently lose_sum: 98.94113904237747
time_elpased: 1.343
batch start
#iterations: 215
currently lose_sum: 99.01754665374756
time_elpased: 1.356
batch start
#iterations: 216
currently lose_sum: 99.15918380022049
time_elpased: 1.359
batch start
#iterations: 217
currently lose_sum: 98.92788672447205
time_elpased: 1.324
batch start
#iterations: 218
currently lose_sum: 99.14496862888336
time_elpased: 1.352
batch start
#iterations: 219
currently lose_sum: 99.25189298391342
time_elpased: 1.324
start validation test
0.621443298969
0.64520866479
0.542554286302
0.589445438283
0.621581800799
64.311
batch start
#iterations: 220
currently lose_sum: 99.2591552734375
time_elpased: 1.326
batch start
#iterations: 221
currently lose_sum: 99.26207536458969
time_elpased: 1.323
batch start
#iterations: 222
currently lose_sum: 98.99872130155563
time_elpased: 1.344
batch start
#iterations: 223
currently lose_sum: 98.96751880645752
time_elpased: 1.371
batch start
#iterations: 224
currently lose_sum: 99.0582874417305
time_elpased: 1.347
batch start
#iterations: 225
currently lose_sum: 99.04103475809097
time_elpased: 1.322
batch start
#iterations: 226
currently lose_sum: 99.26707285642624
time_elpased: 1.317
batch start
#iterations: 227
currently lose_sum: 99.05123949050903
time_elpased: 1.329
batch start
#iterations: 228
currently lose_sum: 98.94569754600525
time_elpased: 1.355
batch start
#iterations: 229
currently lose_sum: 99.06919610500336
time_elpased: 1.32
batch start
#iterations: 230
currently lose_sum: 99.25697183609009
time_elpased: 1.328
batch start
#iterations: 231
currently lose_sum: 99.00686705112457
time_elpased: 1.318
batch start
#iterations: 232
currently lose_sum: 98.9905406832695
time_elpased: 1.352
batch start
#iterations: 233
currently lose_sum: 99.09913516044617
time_elpased: 1.346
batch start
#iterations: 234
currently lose_sum: 99.22367876768112
time_elpased: 1.366
batch start
#iterations: 235
currently lose_sum: 98.86216694116592
time_elpased: 1.358
batch start
#iterations: 236
currently lose_sum: 99.00193583965302
time_elpased: 1.354
batch start
#iterations: 237
currently lose_sum: 99.06213575601578
time_elpased: 1.372
batch start
#iterations: 238
currently lose_sum: 99.21908521652222
time_elpased: 1.333
batch start
#iterations: 239
currently lose_sum: 99.02373272180557
time_elpased: 1.317
start validation test
0.624432989691
0.643354169124
0.561387259442
0.599582325786
0.624543676194
64.319
batch start
#iterations: 240
currently lose_sum: 99.07443583011627
time_elpased: 1.337
batch start
#iterations: 241
currently lose_sum: 99.1217343211174
time_elpased: 1.374
batch start
#iterations: 242
currently lose_sum: 99.12965887784958
time_elpased: 1.346
batch start
#iterations: 243
currently lose_sum: 99.00401532649994
time_elpased: 1.346
batch start
#iterations: 244
currently lose_sum: 99.1322420835495
time_elpased: 1.324
batch start
#iterations: 245
currently lose_sum: 99.1546835899353
time_elpased: 1.338
batch start
#iterations: 246
currently lose_sum: 99.22523564100266
time_elpased: 1.403
batch start
#iterations: 247
currently lose_sum: 99.06019920110703
time_elpased: 1.322
batch start
#iterations: 248
currently lose_sum: 99.09263181686401
time_elpased: 1.332
batch start
#iterations: 249
currently lose_sum: 99.10130000114441
time_elpased: 1.321
batch start
#iterations: 250
currently lose_sum: 99.3086290359497
time_elpased: 1.347
batch start
#iterations: 251
currently lose_sum: 99.03572827577591
time_elpased: 1.34
batch start
#iterations: 252
currently lose_sum: 99.1690793633461
time_elpased: 1.352
batch start
#iterations: 253
currently lose_sum: 99.08019483089447
time_elpased: 1.344
batch start
#iterations: 254
currently lose_sum: 99.28240418434143
time_elpased: 1.334
batch start
#iterations: 255
currently lose_sum: 98.83078837394714
time_elpased: 1.331
batch start
#iterations: 256
currently lose_sum: 99.10803735256195
time_elpased: 1.352
batch start
#iterations: 257
currently lose_sum: 99.11287033557892
time_elpased: 1.314
batch start
#iterations: 258
currently lose_sum: 99.14276987314224
time_elpased: 1.354
batch start
#iterations: 259
currently lose_sum: 99.16816806793213
time_elpased: 1.37
start validation test
0.622989690722
0.653350350989
0.526808685808
0.583295350957
0.623158551311
64.384
batch start
#iterations: 260
currently lose_sum: 98.7584924697876
time_elpased: 1.357
batch start
#iterations: 261
currently lose_sum: 99.31660455465317
time_elpased: 1.35
batch start
#iterations: 262
currently lose_sum: 99.02945017814636
time_elpased: 1.341
batch start
#iterations: 263
currently lose_sum: 99.157624065876
time_elpased: 1.349
batch start
#iterations: 264
currently lose_sum: 99.29368644952774
time_elpased: 1.372
batch start
#iterations: 265
currently lose_sum: 99.0680131316185
time_elpased: 1.33
batch start
#iterations: 266
currently lose_sum: 99.10983180999756
time_elpased: 1.346
batch start
#iterations: 267
currently lose_sum: 99.13097333908081
time_elpased: 1.347
batch start
#iterations: 268
currently lose_sum: 98.95534038543701
time_elpased: 1.37
batch start
#iterations: 269
currently lose_sum: 99.0054144859314
time_elpased: 1.342
batch start
#iterations: 270
currently lose_sum: 99.30299097299576
time_elpased: 1.348
batch start
#iterations: 271
currently lose_sum: 99.21117895841599
time_elpased: 1.332
batch start
#iterations: 272
currently lose_sum: 99.17416620254517
time_elpased: 1.348
batch start
#iterations: 273
currently lose_sum: 99.06988954544067
time_elpased: 1.34
batch start
#iterations: 274
currently lose_sum: 98.91395592689514
time_elpased: 1.329
batch start
#iterations: 275
currently lose_sum: 99.15656954050064
time_elpased: 1.362
batch start
#iterations: 276
currently lose_sum: 99.37651437520981
time_elpased: 1.344
batch start
#iterations: 277
currently lose_sum: 99.20138674974442
time_elpased: 1.341
batch start
#iterations: 278
currently lose_sum: 99.14432406425476
time_elpased: 1.337
batch start
#iterations: 279
currently lose_sum: 98.98421710729599
time_elpased: 1.348
start validation test
0.622783505155
0.650672026127
0.533086343522
0.5860391447
0.622940982357
64.458
batch start
#iterations: 280
currently lose_sum: 99.16858929395676
time_elpased: 1.34
batch start
#iterations: 281
currently lose_sum: 99.06704717874527
time_elpased: 1.326
batch start
#iterations: 282
currently lose_sum: 99.2935380935669
time_elpased: 1.368
batch start
#iterations: 283
currently lose_sum: 99.3919569849968
time_elpased: 1.326
batch start
#iterations: 284
currently lose_sum: 99.0282415151596
time_elpased: 1.352
batch start
#iterations: 285
currently lose_sum: 99.0971627831459
time_elpased: 1.328
batch start
#iterations: 286
currently lose_sum: 99.32109010219574
time_elpased: 1.331
batch start
#iterations: 287
currently lose_sum: 99.1020575761795
time_elpased: 1.369
batch start
#iterations: 288
currently lose_sum: 98.90567690134048
time_elpased: 1.341
batch start
#iterations: 289
currently lose_sum: 99.0328506231308
time_elpased: 1.326
batch start
#iterations: 290
currently lose_sum: 99.16909021139145
time_elpased: 1.342
batch start
#iterations: 291
currently lose_sum: 98.87336701154709
time_elpased: 1.332
batch start
#iterations: 292
currently lose_sum: 98.9108276963234
time_elpased: 1.341
batch start
#iterations: 293
currently lose_sum: 98.92622315883636
time_elpased: 1.34
batch start
#iterations: 294
currently lose_sum: 99.02381813526154
time_elpased: 1.344
batch start
#iterations: 295
currently lose_sum: 99.10080361366272
time_elpased: 1.317
batch start
#iterations: 296
currently lose_sum: 99.0058741569519
time_elpased: 1.369
batch start
#iterations: 297
currently lose_sum: 99.06059807538986
time_elpased: 1.353
batch start
#iterations: 298
currently lose_sum: 99.2069291472435
time_elpased: 1.353
batch start
#iterations: 299
currently lose_sum: 98.921246945858
time_elpased: 1.34
start validation test
0.622164948454
0.63850528026
0.566224143254
0.600196356496
0.622263161165
64.292
batch start
#iterations: 300
currently lose_sum: 99.30132395029068
time_elpased: 1.345
batch start
#iterations: 301
currently lose_sum: 99.08545088768005
time_elpased: 1.331
batch start
#iterations: 302
currently lose_sum: 99.17579054832458
time_elpased: 1.338
batch start
#iterations: 303
currently lose_sum: 99.05184608697891
time_elpased: 1.324
batch start
#iterations: 304
currently lose_sum: 99.15338408946991
time_elpased: 1.349
batch start
#iterations: 305
currently lose_sum: 98.98540496826172
time_elpased: 1.331
batch start
#iterations: 306
currently lose_sum: 99.21856552362442
time_elpased: 1.347
batch start
#iterations: 307
currently lose_sum: 99.22426801919937
time_elpased: 1.346
batch start
#iterations: 308
currently lose_sum: 99.01946192979813
time_elpased: 1.324
batch start
#iterations: 309
currently lose_sum: 99.20671433210373
time_elpased: 1.345
batch start
#iterations: 310
currently lose_sum: 99.07768458127975
time_elpased: 1.322
batch start
#iterations: 311
currently lose_sum: 99.11547738313675
time_elpased: 1.352
batch start
#iterations: 312
currently lose_sum: 99.1785501241684
time_elpased: 1.34
batch start
#iterations: 313
currently lose_sum: 98.93188846111298
time_elpased: 1.337
batch start
#iterations: 314
currently lose_sum: 98.96846324205399
time_elpased: 1.323
batch start
#iterations: 315
currently lose_sum: 98.97539955377579
time_elpased: 1.335
batch start
#iterations: 316
currently lose_sum: 99.24046325683594
time_elpased: 1.333
batch start
#iterations: 317
currently lose_sum: 99.04251271486282
time_elpased: 1.341
batch start
#iterations: 318
currently lose_sum: 99.12115955352783
time_elpased: 1.325
batch start
#iterations: 319
currently lose_sum: 99.0579439997673
time_elpased: 1.338
start validation test
0.620463917526
0.650511508951
0.523515488319
0.580144836631
0.620634125447
64.365
batch start
#iterations: 320
currently lose_sum: 98.95881980657578
time_elpased: 1.364
batch start
#iterations: 321
currently lose_sum: 98.85581457614899
time_elpased: 1.349
batch start
#iterations: 322
currently lose_sum: 99.31637036800385
time_elpased: 1.331
batch start
#iterations: 323
currently lose_sum: 99.09147894382477
time_elpased: 1.343
batch start
#iterations: 324
currently lose_sum: 99.25504338741302
time_elpased: 1.334
batch start
#iterations: 325
currently lose_sum: 99.31036454439163
time_elpased: 1.342
batch start
#iterations: 326
currently lose_sum: 99.04567039012909
time_elpased: 1.359
batch start
#iterations: 327
currently lose_sum: 99.08044564723969
time_elpased: 1.34
batch start
#iterations: 328
currently lose_sum: 99.06723421812057
time_elpased: 1.337
batch start
#iterations: 329
currently lose_sum: 99.1270500421524
time_elpased: 1.345
batch start
#iterations: 330
currently lose_sum: 98.91846334934235
time_elpased: 1.334
batch start
#iterations: 331
currently lose_sum: 99.090256690979
time_elpased: 1.351
batch start
#iterations: 332
currently lose_sum: 99.00681239366531
time_elpased: 1.344
batch start
#iterations: 333
currently lose_sum: 99.19516623020172
time_elpased: 1.382
batch start
#iterations: 334
currently lose_sum: 99.11055809259415
time_elpased: 1.35
batch start
#iterations: 335
currently lose_sum: 99.20230543613434
time_elpased: 1.34
batch start
#iterations: 336
currently lose_sum: 99.01227897405624
time_elpased: 1.346
batch start
#iterations: 337
currently lose_sum: 99.28995060920715
time_elpased: 1.347
batch start
#iterations: 338
currently lose_sum: 98.84150511026382
time_elpased: 1.343
batch start
#iterations: 339
currently lose_sum: 99.0693548321724
time_elpased: 1.332
start validation test
0.618298969072
0.647486603725
0.522280539261
0.578182853888
0.618467544236
64.324
batch start
#iterations: 340
currently lose_sum: 98.97305953502655
time_elpased: 1.342
batch start
#iterations: 341
currently lose_sum: 99.02601903676987
time_elpased: 1.369
batch start
#iterations: 342
currently lose_sum: 99.03320801258087
time_elpased: 1.331
batch start
#iterations: 343
currently lose_sum: 99.11726766824722
time_elpased: 1.356
batch start
#iterations: 344
currently lose_sum: 98.81166529655457
time_elpased: 1.325
batch start
#iterations: 345
currently lose_sum: 98.9954679608345
time_elpased: 1.352
batch start
#iterations: 346
currently lose_sum: 99.16127473115921
time_elpased: 1.327
batch start
#iterations: 347
currently lose_sum: 98.99584883451462
time_elpased: 1.327
batch start
#iterations: 348
currently lose_sum: 99.03455126285553
time_elpased: 1.343
batch start
#iterations: 349
currently lose_sum: 99.24408131837845
time_elpased: 1.361
batch start
#iterations: 350
currently lose_sum: 99.0736038684845
time_elpased: 1.342
batch start
#iterations: 351
currently lose_sum: 99.16035330295563
time_elpased: 1.344
batch start
#iterations: 352
currently lose_sum: 98.83649778366089
time_elpased: 1.327
batch start
#iterations: 353
currently lose_sum: 99.17024201154709
time_elpased: 1.367
batch start
#iterations: 354
currently lose_sum: 99.18934291601181
time_elpased: 1.337
batch start
#iterations: 355
currently lose_sum: 99.33183175325394
time_elpased: 1.339
batch start
#iterations: 356
currently lose_sum: 99.03362327814102
time_elpased: 1.362
batch start
#iterations: 357
currently lose_sum: 98.96965706348419
time_elpased: 1.373
batch start
#iterations: 358
currently lose_sum: 99.16157579421997
time_elpased: 1.337
batch start
#iterations: 359
currently lose_sum: 98.94419854879379
time_elpased: 1.309
start validation test
0.620051546392
0.652298104388
0.517032005763
0.576841380102
0.620232413085
64.379
batch start
#iterations: 360
currently lose_sum: 99.18338912725449
time_elpased: 1.345
batch start
#iterations: 361
currently lose_sum: 99.17138481140137
time_elpased: 1.356
batch start
#iterations: 362
currently lose_sum: 98.9274570941925
time_elpased: 1.387
batch start
#iterations: 363
currently lose_sum: 98.90242779254913
time_elpased: 1.343
batch start
#iterations: 364
currently lose_sum: 99.32273948192596
time_elpased: 1.35
batch start
#iterations: 365
currently lose_sum: 99.28890937566757
time_elpased: 1.353
batch start
#iterations: 366
currently lose_sum: 99.13514226675034
time_elpased: 1.332
batch start
#iterations: 367
currently lose_sum: 99.30415403842926
time_elpased: 1.344
batch start
#iterations: 368
currently lose_sum: 99.12334561347961
time_elpased: 1.319
batch start
#iterations: 369
currently lose_sum: 99.10269409418106
time_elpased: 1.337
batch start
#iterations: 370
currently lose_sum: 99.08912974596024
time_elpased: 1.333
batch start
#iterations: 371
currently lose_sum: 98.8128536939621
time_elpased: 1.326
batch start
#iterations: 372
currently lose_sum: 99.09996318817139
time_elpased: 1.387
batch start
#iterations: 373
currently lose_sum: 98.93427348136902
time_elpased: 1.329
batch start
#iterations: 374
currently lose_sum: 98.94131636619568
time_elpased: 1.362
batch start
#iterations: 375
currently lose_sum: 99.176992893219
time_elpased: 1.364
batch start
#iterations: 376
currently lose_sum: 98.99546903371811
time_elpased: 1.328
batch start
#iterations: 377
currently lose_sum: 98.95208317041397
time_elpased: 1.34
batch start
#iterations: 378
currently lose_sum: 99.16038227081299
time_elpased: 1.364
batch start
#iterations: 379
currently lose_sum: 99.01726180315018
time_elpased: 1.335
start validation test
0.617680412371
0.649389451806
0.514459195225
0.574102784955
0.617861633139
64.420
batch start
#iterations: 380
currently lose_sum: 99.15989154577255
time_elpased: 1.319
batch start
#iterations: 381
currently lose_sum: 98.95250231027603
time_elpased: 1.36
batch start
#iterations: 382
currently lose_sum: 99.05105447769165
time_elpased: 1.347
batch start
#iterations: 383
currently lose_sum: 98.89571976661682
time_elpased: 1.337
batch start
#iterations: 384
currently lose_sum: 99.22528392076492
time_elpased: 1.331
batch start
#iterations: 385
currently lose_sum: 99.08577632904053
time_elpased: 1.368
batch start
#iterations: 386
currently lose_sum: 99.07292640209198
time_elpased: 1.356
batch start
#iterations: 387
currently lose_sum: 99.17679244279861
time_elpased: 1.33
batch start
#iterations: 388
currently lose_sum: 99.10968136787415
time_elpased: 1.357
batch start
#iterations: 389
currently lose_sum: 99.11210834980011
time_elpased: 1.328
batch start
#iterations: 390
currently lose_sum: 99.1431714296341
time_elpased: 1.358
batch start
#iterations: 391
currently lose_sum: 99.1492171883583
time_elpased: 1.347
batch start
#iterations: 392
currently lose_sum: 99.01992177963257
time_elpased: 1.32
batch start
#iterations: 393
currently lose_sum: 99.02666366100311
time_elpased: 1.33
batch start
#iterations: 394
currently lose_sum: 99.20226567983627
time_elpased: 1.349
batch start
#iterations: 395
currently lose_sum: 98.95744329690933
time_elpased: 1.352
batch start
#iterations: 396
currently lose_sum: 99.12219780683517
time_elpased: 1.341
batch start
#iterations: 397
currently lose_sum: 98.98477065563202
time_elpased: 1.326
batch start
#iterations: 398
currently lose_sum: 98.79794269800186
time_elpased: 1.36
batch start
#iterations: 399
currently lose_sum: 99.08557611703873
time_elpased: 1.376
start validation test
0.625360824742
0.642168814583
0.569208603478
0.603491543917
0.625459408628
64.280
acc: 0.623
pre: 0.638
rec: 0.574
F1: 0.604
auc: 0.670
