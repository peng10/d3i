start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.44166618585587
time_elpased: 1.777
batch start
#iterations: 1
currently lose_sum: 100.28550165891647
time_elpased: 1.745
batch start
#iterations: 2
currently lose_sum: 100.27037626504898
time_elpased: 1.759
batch start
#iterations: 3
currently lose_sum: 100.12904727458954
time_elpased: 1.841
batch start
#iterations: 4
currently lose_sum: 100.14754527807236
time_elpased: 1.799
batch start
#iterations: 5
currently lose_sum: 99.98783755302429
time_elpased: 1.822
batch start
#iterations: 6
currently lose_sum: 99.85999524593353
time_elpased: 1.775
batch start
#iterations: 7
currently lose_sum: 99.9052078127861
time_elpased: 1.774
batch start
#iterations: 8
currently lose_sum: 99.75617468357086
time_elpased: 1.775
batch start
#iterations: 9
currently lose_sum: 99.8002102971077
time_elpased: 1.821
batch start
#iterations: 10
currently lose_sum: 99.63324499130249
time_elpased: 1.79
batch start
#iterations: 11
currently lose_sum: 99.2527174949646
time_elpased: 1.81
batch start
#iterations: 12
currently lose_sum: 99.39921295642853
time_elpased: 1.764
batch start
#iterations: 13
currently lose_sum: 99.50587499141693
time_elpased: 1.759
batch start
#iterations: 14
currently lose_sum: 99.28681606054306
time_elpased: 1.802
batch start
#iterations: 15
currently lose_sum: 99.37609201669693
time_elpased: 1.733
batch start
#iterations: 16
currently lose_sum: 99.38226050138474
time_elpased: 1.771
batch start
#iterations: 17
currently lose_sum: 99.40948528051376
time_elpased: 1.864
batch start
#iterations: 18
currently lose_sum: 99.19833862781525
time_elpased: 1.74
batch start
#iterations: 19
currently lose_sum: 99.0059182047844
time_elpased: 1.772
start validation test
0.615
0.605147402901
0.665500205846
0.633890495564
0.614916563063
64.564
batch start
#iterations: 20
currently lose_sum: 98.96794509887695
time_elpased: 1.783
batch start
#iterations: 21
currently lose_sum: 98.83395093679428
time_elpased: 1.752
batch start
#iterations: 22
currently lose_sum: 98.73600512742996
time_elpased: 1.751
batch start
#iterations: 23
currently lose_sum: 98.7816453576088
time_elpased: 1.798
batch start
#iterations: 24
currently lose_sum: 98.78491187095642
time_elpased: 1.76
batch start
#iterations: 25
currently lose_sum: 98.788035094738
time_elpased: 1.852
batch start
#iterations: 26
currently lose_sum: 98.57489287853241
time_elpased: 1.76
batch start
#iterations: 27
currently lose_sum: 98.59142100811005
time_elpased: 1.796
batch start
#iterations: 28
currently lose_sum: 98.58425033092499
time_elpased: 1.768
batch start
#iterations: 29
currently lose_sum: 98.81402313709259
time_elpased: 1.832
batch start
#iterations: 30
currently lose_sum: 98.530737221241
time_elpased: 1.751
batch start
#iterations: 31
currently lose_sum: 98.42184448242188
time_elpased: 1.715
batch start
#iterations: 32
currently lose_sum: 98.38549429178238
time_elpased: 1.84
batch start
#iterations: 33
currently lose_sum: 98.51363641023636
time_elpased: 1.775
batch start
#iterations: 34
currently lose_sum: 98.51731932163239
time_elpased: 1.744
batch start
#iterations: 35
currently lose_sum: 98.75461286306381
time_elpased: 1.752
batch start
#iterations: 36
currently lose_sum: 98.48579865694046
time_elpased: 1.737
batch start
#iterations: 37
currently lose_sum: 98.44759511947632
time_elpased: 1.775
batch start
#iterations: 38
currently lose_sum: 98.18025344610214
time_elpased: 1.79
batch start
#iterations: 39
currently lose_sum: 98.57983720302582
time_elpased: 1.795
start validation test
0.625618556701
0.628496595076
0.617435158501
0.62291677483
0.625632077392
63.895
batch start
#iterations: 40
currently lose_sum: 98.3390520811081
time_elpased: 1.767
batch start
#iterations: 41
currently lose_sum: 98.16519773006439
time_elpased: 1.751
batch start
#iterations: 42
currently lose_sum: 98.2551948428154
time_elpased: 1.788
batch start
#iterations: 43
currently lose_sum: 98.14053499698639
time_elpased: 1.761
batch start
#iterations: 44
currently lose_sum: 98.34192299842834
time_elpased: 1.758
batch start
#iterations: 45
currently lose_sum: 98.2591934800148
time_elpased: 1.734
batch start
#iterations: 46
currently lose_sum: 98.11845803260803
time_elpased: 1.786
batch start
#iterations: 47
currently lose_sum: 98.1129909157753
time_elpased: 1.833
batch start
#iterations: 48
currently lose_sum: 98.0100890994072
time_elpased: 1.761
batch start
#iterations: 49
currently lose_sum: 98.16414576768875
time_elpased: 1.795
batch start
#iterations: 50
currently lose_sum: 98.29094529151917
time_elpased: 1.827
batch start
#iterations: 51
currently lose_sum: 98.00863820314407
time_elpased: 1.752
batch start
#iterations: 52
currently lose_sum: 98.00024300813675
time_elpased: 1.781
batch start
#iterations: 53
currently lose_sum: 97.92883914709091
time_elpased: 1.732
batch start
#iterations: 54
currently lose_sum: 98.17479687929153
time_elpased: 1.742
batch start
#iterations: 55
currently lose_sum: 97.93862074613571
time_elpased: 1.793
batch start
#iterations: 56
currently lose_sum: 97.86093878746033
time_elpased: 1.763
batch start
#iterations: 57
currently lose_sum: 97.89611285924911
time_elpased: 1.805
batch start
#iterations: 58
currently lose_sum: 97.92395466566086
time_elpased: 1.731
batch start
#iterations: 59
currently lose_sum: 97.90598845481873
time_elpased: 1.749
start validation test
0.62206185567
0.625685364825
0.610745162618
0.618125
0.622080553222
64.259
batch start
#iterations: 60
currently lose_sum: 97.77676057815552
time_elpased: 1.79
batch start
#iterations: 61
currently lose_sum: 98.12519472837448
time_elpased: 1.814
batch start
#iterations: 62
currently lose_sum: 98.01008993387222
time_elpased: 1.731
batch start
#iterations: 63
currently lose_sum: 97.75608617067337
time_elpased: 1.758
batch start
#iterations: 64
currently lose_sum: 98.08548188209534
time_elpased: 1.732
batch start
#iterations: 65
currently lose_sum: 97.86938309669495
time_elpased: 1.773
batch start
#iterations: 66
currently lose_sum: 97.72638738155365
time_elpased: 1.759
batch start
#iterations: 67
currently lose_sum: 97.86027258634567
time_elpased: 1.767
batch start
#iterations: 68
currently lose_sum: 97.80645990371704
time_elpased: 1.772
batch start
#iterations: 69
currently lose_sum: 97.94670879840851
time_elpased: 1.745
batch start
#iterations: 70
currently lose_sum: 97.7588900923729
time_elpased: 1.807
batch start
#iterations: 71
currently lose_sum: 97.80523365736008
time_elpased: 1.78
batch start
#iterations: 72
currently lose_sum: 97.57992750406265
time_elpased: 1.731
batch start
#iterations: 73
currently lose_sum: 97.79646039009094
time_elpased: 1.778
batch start
#iterations: 74
currently lose_sum: 97.53895807266235
time_elpased: 1.807
batch start
#iterations: 75
currently lose_sum: 97.89639586210251
time_elpased: 1.822
batch start
#iterations: 76
currently lose_sum: 97.57612240314484
time_elpased: 1.741
batch start
#iterations: 77
currently lose_sum: 97.70608049631119
time_elpased: 1.754
batch start
#iterations: 78
currently lose_sum: 97.65029901266098
time_elpased: 1.731
batch start
#iterations: 79
currently lose_sum: 97.62416231632233
time_elpased: 1.781
start validation test
0.627319587629
0.638820638821
0.588719637711
0.612747723621
0.627383362846
62.955
batch start
#iterations: 80
currently lose_sum: 97.83411967754364
time_elpased: 1.749
batch start
#iterations: 81
currently lose_sum: 97.57300841808319
time_elpased: 1.845
batch start
#iterations: 82
currently lose_sum: 97.67564159631729
time_elpased: 1.741
batch start
#iterations: 83
currently lose_sum: 97.65816807746887
time_elpased: 1.777
batch start
#iterations: 84
currently lose_sum: 97.4908909201622
time_elpased: 1.748
batch start
#iterations: 85
currently lose_sum: 97.645194709301
time_elpased: 1.739
batch start
#iterations: 86
currently lose_sum: 97.2821935415268
time_elpased: 1.708
batch start
#iterations: 87
currently lose_sum: 97.47759091854095
time_elpased: 1.783
batch start
#iterations: 88
currently lose_sum: 97.3601723909378
time_elpased: 1.756
batch start
#iterations: 89
currently lose_sum: 97.65831881761551
time_elpased: 1.808
batch start
#iterations: 90
currently lose_sum: 97.55967730283737
time_elpased: 1.754
batch start
#iterations: 91
currently lose_sum: 97.5126736164093
time_elpased: 1.811
batch start
#iterations: 92
currently lose_sum: 97.45547205209732
time_elpased: 1.724
batch start
#iterations: 93
currently lose_sum: 97.52480471134186
time_elpased: 1.79
batch start
#iterations: 94
currently lose_sum: 96.99382025003433
time_elpased: 1.736
batch start
#iterations: 95
currently lose_sum: 97.63196563720703
time_elpased: 1.745
batch start
#iterations: 96
currently lose_sum: 97.63303452730179
time_elpased: 1.798
batch start
#iterations: 97
currently lose_sum: 97.1770538687706
time_elpased: 1.817
batch start
#iterations: 98
currently lose_sum: 97.51747101545334
time_elpased: 1.797
batch start
#iterations: 99
currently lose_sum: 97.26016002893448
time_elpased: 1.781
start validation test
0.608711340206
0.609728389962
0.607657472211
0.608691169648
0.608713081417
63.887
batch start
#iterations: 100
currently lose_sum: 97.39888775348663
time_elpased: 1.77
batch start
#iterations: 101
currently lose_sum: 97.38336306810379
time_elpased: 1.746
batch start
#iterations: 102
currently lose_sum: 97.54264122247696
time_elpased: 1.87
batch start
#iterations: 103
currently lose_sum: 97.30561298131943
time_elpased: 1.749
batch start
#iterations: 104
currently lose_sum: 97.2186376452446
time_elpased: 1.832
batch start
#iterations: 105
currently lose_sum: 97.6520407795906
time_elpased: 1.845
batch start
#iterations: 106
currently lose_sum: 97.16222870349884
time_elpased: 1.788
batch start
#iterations: 107
currently lose_sum: 97.40061366558075
time_elpased: 1.774
batch start
#iterations: 108
currently lose_sum: 97.30508208274841
time_elpased: 1.72
batch start
#iterations: 109
currently lose_sum: 97.2774943113327
time_elpased: 1.778
batch start
#iterations: 110
currently lose_sum: 97.33425164222717
time_elpased: 1.74
batch start
#iterations: 111
currently lose_sum: 97.35651737451553
time_elpased: 1.795
batch start
#iterations: 112
currently lose_sum: 97.03890591859818
time_elpased: 1.776
batch start
#iterations: 113
currently lose_sum: 97.14568305015564
time_elpased: 1.757
batch start
#iterations: 114
currently lose_sum: 97.32793420553207
time_elpased: 1.754
batch start
#iterations: 115
currently lose_sum: 97.13504058122635
time_elpased: 1.774
batch start
#iterations: 116
currently lose_sum: 97.24129569530487
time_elpased: 1.803
batch start
#iterations: 117
currently lose_sum: 96.85568118095398
time_elpased: 1.767
batch start
#iterations: 118
currently lose_sum: 97.37543696165085
time_elpased: 1.789
batch start
#iterations: 119
currently lose_sum: 97.1537275314331
time_elpased: 1.772
start validation test
0.627474226804
0.640320216484
0.584499794154
0.611138014528
0.627545229584
63.209
batch start
#iterations: 120
currently lose_sum: 96.96792513132095
time_elpased: 1.794
batch start
#iterations: 121
currently lose_sum: 97.14176124334335
time_elpased: 1.762
batch start
#iterations: 122
currently lose_sum: 96.81937503814697
time_elpased: 1.801
batch start
#iterations: 123
currently lose_sum: 97.35053378343582
time_elpased: 1.758
batch start
#iterations: 124
currently lose_sum: 97.14300018548965
time_elpased: 1.774
batch start
#iterations: 125
currently lose_sum: 96.90762478113174
time_elpased: 1.75
batch start
#iterations: 126
currently lose_sum: 96.97218954563141
time_elpased: 1.753
batch start
#iterations: 127
currently lose_sum: 96.95919704437256
time_elpased: 1.838
batch start
#iterations: 128
currently lose_sum: 96.82632315158844
time_elpased: 1.788
batch start
#iterations: 129
currently lose_sum: 97.10787951946259
time_elpased: 1.801
batch start
#iterations: 130
currently lose_sum: 96.74204510450363
time_elpased: 1.818
batch start
#iterations: 131
currently lose_sum: 96.94835698604584
time_elpased: 1.771
batch start
#iterations: 132
currently lose_sum: 96.8700743317604
time_elpased: 1.779
batch start
#iterations: 133
currently lose_sum: 96.99351888895035
time_elpased: 1.788
batch start
#iterations: 134
currently lose_sum: 96.98431205749512
time_elpased: 1.782
batch start
#iterations: 135
currently lose_sum: 97.05653589963913
time_elpased: 1.731
batch start
#iterations: 136
currently lose_sum: 97.07388764619827
time_elpased: 1.762
batch start
#iterations: 137
currently lose_sum: 96.94749534130096
time_elpased: 1.817
batch start
#iterations: 138
currently lose_sum: 96.8797305226326
time_elpased: 1.797
batch start
#iterations: 139
currently lose_sum: 96.59179526567459
time_elpased: 1.831
start validation test
0.613144329897
0.62459145723
0.570399341293
0.596266609285
0.613214953587
63.558
batch start
#iterations: 140
currently lose_sum: 96.49269789457321
time_elpased: 1.788
batch start
#iterations: 141
currently lose_sum: 96.65833222866058
time_elpased: 1.829
batch start
#iterations: 142
currently lose_sum: 96.94753694534302
time_elpased: 1.753
batch start
#iterations: 143
currently lose_sum: 96.4043989777565
time_elpased: 1.809
batch start
#iterations: 144
currently lose_sum: 96.77137964963913
time_elpased: 1.811
batch start
#iterations: 145
currently lose_sum: 96.5616102218628
time_elpased: 1.74
batch start
#iterations: 146
currently lose_sum: 96.64254277944565
time_elpased: 1.825
batch start
#iterations: 147
currently lose_sum: 96.73915636539459
time_elpased: 1.72
batch start
#iterations: 148
currently lose_sum: 96.69854778051376
time_elpased: 1.795
batch start
#iterations: 149
currently lose_sum: 96.79073685407639
time_elpased: 1.726
batch start
#iterations: 150
currently lose_sum: 96.82666832208633
time_elpased: 1.788
batch start
#iterations: 151
currently lose_sum: 96.80666643381119
time_elpased: 1.777
batch start
#iterations: 152
currently lose_sum: 96.56312221288681
time_elpased: 1.8
batch start
#iterations: 153
currently lose_sum: 96.87076771259308
time_elpased: 1.769
batch start
#iterations: 154
currently lose_sum: 96.50863260030746
time_elpased: 1.798
batch start
#iterations: 155
currently lose_sum: 96.49937981367111
time_elpased: 1.767
batch start
#iterations: 156
currently lose_sum: 96.69285583496094
time_elpased: 1.753
batch start
#iterations: 157
currently lose_sum: 96.26794922351837
time_elpased: 1.778
batch start
#iterations: 158
currently lose_sum: 96.58217370510101
time_elpased: 1.736
batch start
#iterations: 159
currently lose_sum: 96.62466567754745
time_elpased: 1.771
start validation test
0.621907216495
0.628828048912
0.598085631947
0.613071688558
0.621946574751
63.483
batch start
#iterations: 160
currently lose_sum: 96.38444846868515
time_elpased: 1.797
batch start
#iterations: 161
currently lose_sum: 96.40680587291718
time_elpased: 1.764
batch start
#iterations: 162
currently lose_sum: 96.42995595932007
time_elpased: 1.784
batch start
#iterations: 163
currently lose_sum: 96.52096652984619
time_elpased: 1.796
batch start
#iterations: 164
currently lose_sum: 96.51528787612915
time_elpased: 1.783
batch start
#iterations: 165
currently lose_sum: 96.05524027347565
time_elpased: 1.788
batch start
#iterations: 166
currently lose_sum: 96.08516848087311
time_elpased: 1.748
batch start
#iterations: 167
currently lose_sum: 96.12430763244629
time_elpased: 1.771
batch start
#iterations: 168
currently lose_sum: 96.10414123535156
time_elpased: 1.811
batch start
#iterations: 169
currently lose_sum: 96.39725667238235
time_elpased: 1.808
batch start
#iterations: 170
currently lose_sum: 95.96207404136658
time_elpased: 1.737
batch start
#iterations: 171
currently lose_sum: 95.91072475910187
time_elpased: 1.729
batch start
#iterations: 172
currently lose_sum: 96.18871450424194
time_elpased: 1.778
batch start
#iterations: 173
currently lose_sum: 95.84208154678345
time_elpased: 1.758
batch start
#iterations: 174
currently lose_sum: 96.04572612047195
time_elpased: 1.798
batch start
#iterations: 175
currently lose_sum: 95.70570528507233
time_elpased: 1.732
batch start
#iterations: 176
currently lose_sum: 96.60699439048767
time_elpased: 1.827
batch start
#iterations: 177
currently lose_sum: 96.01801085472107
time_elpased: 1.793
batch start
#iterations: 178
currently lose_sum: 95.85315316915512
time_elpased: 1.728
batch start
#iterations: 179
currently lose_sum: 95.74541664123535
time_elpased: 1.758
start validation test
0.603453608247
0.611879216901
0.569370111157
0.58985978568
0.603509921336
64.286
batch start
#iterations: 180
currently lose_sum: 95.94118893146515
time_elpased: 1.729
batch start
#iterations: 181
currently lose_sum: 96.39080226421356
time_elpased: 1.788
batch start
#iterations: 182
currently lose_sum: 95.88569217920303
time_elpased: 1.77
batch start
#iterations: 183
currently lose_sum: 95.42056733369827
time_elpased: 1.771
batch start
#iterations: 184
currently lose_sum: 95.94143050909042
time_elpased: 1.773
batch start
#iterations: 185
currently lose_sum: 95.76383692026138
time_elpased: 1.738
batch start
#iterations: 186
currently lose_sum: 95.90124273300171
time_elpased: 1.783
batch start
#iterations: 187
currently lose_sum: 95.57240390777588
time_elpased: 1.781
batch start
#iterations: 188
currently lose_sum: 95.94355207681656
time_elpased: 1.765
batch start
#iterations: 189
currently lose_sum: 96.045807659626
time_elpased: 1.708
batch start
#iterations: 190
currently lose_sum: 95.84477937221527
time_elpased: 1.777
batch start
#iterations: 191
currently lose_sum: 95.5819177031517
time_elpased: 1.756
batch start
#iterations: 192
currently lose_sum: 95.79997009038925
time_elpased: 1.766
batch start
#iterations: 193
currently lose_sum: 95.36379474401474
time_elpased: 1.774
batch start
#iterations: 194
currently lose_sum: 95.68054908514023
time_elpased: 1.784
batch start
#iterations: 195
currently lose_sum: 95.378633081913
time_elpased: 1.808
batch start
#iterations: 196
currently lose_sum: 95.58958607912064
time_elpased: 1.787
batch start
#iterations: 197
currently lose_sum: 94.98150807619095
time_elpased: 1.737
batch start
#iterations: 198
currently lose_sum: 95.48006564378738
time_elpased: 1.756
batch start
#iterations: 199
currently lose_sum: 95.16537266969681
time_elpased: 1.755
start validation test
0.599536082474
0.608395501615
0.562371346233
0.584478793389
0.599597486417
64.452
batch start
#iterations: 200
currently lose_sum: 95.24107879400253
time_elpased: 1.754
batch start
#iterations: 201
currently lose_sum: 94.84410011768341
time_elpased: 1.741
batch start
#iterations: 202
currently lose_sum: 95.28423207998276
time_elpased: 1.816
batch start
#iterations: 203
currently lose_sum: 95.49180054664612
time_elpased: 1.729
batch start
#iterations: 204
currently lose_sum: 95.28655534982681
time_elpased: 1.751
batch start
#iterations: 205
currently lose_sum: 95.13514709472656
time_elpased: 1.759
batch start
#iterations: 206
currently lose_sum: 95.02631205320358
time_elpased: 1.814
batch start
#iterations: 207
currently lose_sum: 95.17336690425873
time_elpased: 1.757
batch start
#iterations: 208
currently lose_sum: 94.9250020980835
time_elpased: 1.791
batch start
#iterations: 209
currently lose_sum: 94.8211520910263
time_elpased: 1.754
batch start
#iterations: 210
currently lose_sum: 94.77335143089294
time_elpased: 1.806
batch start
#iterations: 211
currently lose_sum: 94.6666539311409
time_elpased: 1.739
batch start
#iterations: 212
currently lose_sum: 95.42525815963745
time_elpased: 1.77
batch start
#iterations: 213
currently lose_sum: 95.00133860111237
time_elpased: 1.729
batch start
#iterations: 214
currently lose_sum: 94.79011762142181
time_elpased: 1.775
batch start
#iterations: 215
currently lose_sum: 94.83687192201614
time_elpased: 1.741
batch start
#iterations: 216
currently lose_sum: 94.80754625797272
time_elpased: 1.728
batch start
#iterations: 217
currently lose_sum: 94.71399283409119
time_elpased: 1.754
batch start
#iterations: 218
currently lose_sum: 94.68025428056717
time_elpased: 1.717
batch start
#iterations: 219
currently lose_sum: 94.35757917165756
time_elpased: 1.724
start validation test
0.610309278351
0.627092666824
0.547447509263
0.584569732938
0.610413139183
64.255
batch start
#iterations: 220
currently lose_sum: 94.27451878786087
time_elpased: 1.765
batch start
#iterations: 221
currently lose_sum: 94.3970200419426
time_elpased: 1.751
batch start
#iterations: 222
currently lose_sum: 94.15822088718414
time_elpased: 1.806
batch start
#iterations: 223
currently lose_sum: 94.27792829275131
time_elpased: 1.738
batch start
#iterations: 224
currently lose_sum: 93.87697643041611
time_elpased: 1.774
batch start
#iterations: 225
currently lose_sum: 94.29648369550705
time_elpased: 1.77
batch start
#iterations: 226
currently lose_sum: 93.82437759637833
time_elpased: 1.756
batch start
#iterations: 227
currently lose_sum: 94.16646593809128
time_elpased: 1.827
batch start
#iterations: 228
currently lose_sum: 94.46159225702286
time_elpased: 1.751
batch start
#iterations: 229
currently lose_sum: 93.9252455830574
time_elpased: 1.778
batch start
#iterations: 230
currently lose_sum: 94.10943603515625
time_elpased: 1.769
batch start
#iterations: 231
currently lose_sum: 94.49364596605301
time_elpased: 1.749
batch start
#iterations: 232
currently lose_sum: 94.3466237783432
time_elpased: 1.828
batch start
#iterations: 233
currently lose_sum: 93.8894756436348
time_elpased: 1.764
batch start
#iterations: 234
currently lose_sum: 94.06664198637009
time_elpased: 1.801
batch start
#iterations: 235
currently lose_sum: 94.24257630109787
time_elpased: 1.791
batch start
#iterations: 236
currently lose_sum: 93.72443181276321
time_elpased: 1.848
batch start
#iterations: 237
currently lose_sum: 93.75973981618881
time_elpased: 1.776
batch start
#iterations: 238
currently lose_sum: 94.08573347330093
time_elpased: 1.733
batch start
#iterations: 239
currently lose_sum: 93.19987273216248
time_elpased: 1.787
start validation test
0.594896907216
0.62532055608
0.476842321943
0.541080291971
0.595091958163
65.310
batch start
#iterations: 240
currently lose_sum: 93.45819836854935
time_elpased: 1.844
batch start
#iterations: 241
currently lose_sum: 93.63413894176483
time_elpased: 1.794
batch start
#iterations: 242
currently lose_sum: 93.71703952550888
time_elpased: 1.75
batch start
#iterations: 243
currently lose_sum: 93.72128999233246
time_elpased: 1.763
batch start
#iterations: 244
currently lose_sum: 94.16460233926773
time_elpased: 1.787
batch start
#iterations: 245
currently lose_sum: 93.3858015537262
time_elpased: 1.803
batch start
#iterations: 246
currently lose_sum: 93.44089591503143
time_elpased: 1.759
batch start
#iterations: 247
currently lose_sum: 93.60579425096512
time_elpased: 1.751
batch start
#iterations: 248
currently lose_sum: 93.35849261283875
time_elpased: 1.795
batch start
#iterations: 249
currently lose_sum: 93.34732377529144
time_elpased: 1.778
batch start
#iterations: 250
currently lose_sum: 93.04225796461105
time_elpased: 1.777
batch start
#iterations: 251
currently lose_sum: 93.4127648472786
time_elpased: 1.785
batch start
#iterations: 252
currently lose_sum: 93.28291440010071
time_elpased: 1.84
batch start
#iterations: 253
currently lose_sum: 93.0324958562851
time_elpased: 1.887
batch start
#iterations: 254
currently lose_sum: 92.80965662002563
time_elpased: 1.745
batch start
#iterations: 255
currently lose_sum: 93.25465834140778
time_elpased: 1.805
batch start
#iterations: 256
currently lose_sum: 92.23826694488525
time_elpased: 1.781
batch start
#iterations: 257
currently lose_sum: 92.62687587738037
time_elpased: 1.759
batch start
#iterations: 258
currently lose_sum: 93.05490362644196
time_elpased: 1.737
batch start
#iterations: 259
currently lose_sum: 92.91605007648468
time_elpased: 1.733
start validation test
0.601907216495
0.630517354289
0.495471387402
0.554895971414
0.602083070818
66.331
batch start
#iterations: 260
currently lose_sum: 92.33553206920624
time_elpased: 1.799
batch start
#iterations: 261
currently lose_sum: 92.45042389631271
time_elpased: 1.765
batch start
#iterations: 262
currently lose_sum: 92.00174754858017
time_elpased: 1.764
batch start
#iterations: 263
currently lose_sum: 92.84682857990265
time_elpased: 1.779
batch start
#iterations: 264
currently lose_sum: 92.29020643234253
time_elpased: 1.763
batch start
#iterations: 265
currently lose_sum: 92.41571938991547
time_elpased: 1.817
batch start
#iterations: 266
currently lose_sum: 92.16156613826752
time_elpased: 1.808
batch start
#iterations: 267
currently lose_sum: 92.0475441813469
time_elpased: 1.772
batch start
#iterations: 268
currently lose_sum: 92.30928081274033
time_elpased: 1.76
batch start
#iterations: 269
currently lose_sum: 92.3769280910492
time_elpased: 1.757
batch start
#iterations: 270
currently lose_sum: 91.82096540927887
time_elpased: 1.82
batch start
#iterations: 271
currently lose_sum: 91.99481683969498
time_elpased: 1.772
batch start
#iterations: 272
currently lose_sum: 91.97457766532898
time_elpased: 1.777
batch start
#iterations: 273
currently lose_sum: 91.99495112895966
time_elpased: 1.776
batch start
#iterations: 274
currently lose_sum: 91.2970814704895
time_elpased: 1.772
batch start
#iterations: 275
currently lose_sum: 92.10740929841995
time_elpased: 1.786
batch start
#iterations: 276
currently lose_sum: 91.67792654037476
time_elpased: 1.813
batch start
#iterations: 277
currently lose_sum: 91.32708770036697
time_elpased: 1.781
batch start
#iterations: 278
currently lose_sum: 91.48499649763107
time_elpased: 1.791
batch start
#iterations: 279
currently lose_sum: 91.15551555156708
time_elpased: 1.777
start validation test
0.590051546392
0.623545900491
0.457904487443
0.528039878939
0.590269881062
67.397
batch start
#iterations: 280
currently lose_sum: 91.44934421777725
time_elpased: 1.739
batch start
#iterations: 281
currently lose_sum: 91.13391923904419
time_elpased: 1.781
batch start
#iterations: 282
currently lose_sum: 90.71650940179825
time_elpased: 1.771
batch start
#iterations: 283
currently lose_sum: 91.198022544384
time_elpased: 1.814
batch start
#iterations: 284
currently lose_sum: 90.68948018550873
time_elpased: 1.815
batch start
#iterations: 285
currently lose_sum: 90.47414261102676
time_elpased: 1.759
batch start
#iterations: 286
currently lose_sum: 90.43225508928299
time_elpased: 1.787
batch start
#iterations: 287
currently lose_sum: 90.51048743724823
time_elpased: 1.773
batch start
#iterations: 288
currently lose_sum: 90.76906323432922
time_elpased: 1.836
batch start
#iterations: 289
currently lose_sum: 90.43239140510559
time_elpased: 1.755
batch start
#iterations: 290
currently lose_sum: 90.71821630001068
time_elpased: 1.807
batch start
#iterations: 291
currently lose_sum: 90.49344903230667
time_elpased: 1.851
batch start
#iterations: 292
currently lose_sum: 89.89209449291229
time_elpased: 1.801
batch start
#iterations: 293
currently lose_sum: 90.36084455251694
time_elpased: 1.79
batch start
#iterations: 294
currently lose_sum: 90.3042818903923
time_elpased: 1.791
batch start
#iterations: 295
currently lose_sum: 90.30639082193375
time_elpased: 1.769
batch start
#iterations: 296
currently lose_sum: 89.89533740282059
time_elpased: 1.868
batch start
#iterations: 297
currently lose_sum: 90.14148670434952
time_elpased: 1.751
batch start
#iterations: 298
currently lose_sum: 89.62514156103134
time_elpased: 1.737
batch start
#iterations: 299
currently lose_sum: 89.87777531147003
time_elpased: 1.795
start validation test
0.587319587629
0.623305451399
0.444833264718
0.519159159159
0.587555004932
69.409
batch start
#iterations: 300
currently lose_sum: 90.09564709663391
time_elpased: 1.749
batch start
#iterations: 301
currently lose_sum: 89.66132694482803
time_elpased: 1.782
batch start
#iterations: 302
currently lose_sum: 89.53985917568207
time_elpased: 1.778
batch start
#iterations: 303
currently lose_sum: 89.4640953540802
time_elpased: 1.777
batch start
#iterations: 304
currently lose_sum: 88.67774617671967
time_elpased: 1.776
batch start
#iterations: 305
currently lose_sum: 89.24581253528595
time_elpased: 1.806
batch start
#iterations: 306
currently lose_sum: 90.08296060562134
time_elpased: 1.746
batch start
#iterations: 307
currently lose_sum: 89.42375463247299
time_elpased: 1.772
batch start
#iterations: 308
currently lose_sum: 89.14482688903809
time_elpased: 1.875
batch start
#iterations: 309
currently lose_sum: 88.65197223424911
time_elpased: 1.771
batch start
#iterations: 310
currently lose_sum: 88.74312734603882
time_elpased: 1.808
batch start
#iterations: 311
currently lose_sum: 88.7414118051529
time_elpased: 1.787
batch start
#iterations: 312
currently lose_sum: 88.98957806825638
time_elpased: 1.773
batch start
#iterations: 313
currently lose_sum: 87.95674359798431
time_elpased: 1.807
batch start
#iterations: 314
currently lose_sum: 88.74413710832596
time_elpased: 1.79
batch start
#iterations: 315
currently lose_sum: 88.46326118707657
time_elpased: 1.79
batch start
#iterations: 316
currently lose_sum: 88.79968947172165
time_elpased: 1.748
batch start
#iterations: 317
currently lose_sum: 88.13021099567413
time_elpased: 1.773
batch start
#iterations: 318
currently lose_sum: 88.40278750658035
time_elpased: 1.776
batch start
#iterations: 319
currently lose_sum: 88.04485660791397
time_elpased: 1.741
start validation test
0.580773195876
0.599297453268
0.4916632359
0.540170746876
0.580920424228
71.463
batch start
#iterations: 320
currently lose_sum: 88.7101543545723
time_elpased: 1.734
batch start
#iterations: 321
currently lose_sum: 88.28476989269257
time_elpased: 1.752
batch start
#iterations: 322
currently lose_sum: 88.03553223609924
time_elpased: 1.76
batch start
#iterations: 323
currently lose_sum: 87.88421589136124
time_elpased: 1.769
batch start
#iterations: 324
currently lose_sum: 87.43080031871796
time_elpased: 1.763
batch start
#iterations: 325
currently lose_sum: 87.82025527954102
time_elpased: 1.789
batch start
#iterations: 326
currently lose_sum: 87.28815460205078
time_elpased: 1.798
batch start
#iterations: 327
currently lose_sum: 87.18282955884933
time_elpased: 1.796
batch start
#iterations: 328
currently lose_sum: 87.25884515047073
time_elpased: 1.768
batch start
#iterations: 329
currently lose_sum: 87.07452011108398
time_elpased: 1.737
batch start
#iterations: 330
currently lose_sum: 87.45802557468414
time_elpased: 1.77
batch start
#iterations: 331
currently lose_sum: 87.20262503623962
time_elpased: 1.743
batch start
#iterations: 332
currently lose_sum: 87.24985241889954
time_elpased: 1.809
batch start
#iterations: 333
currently lose_sum: 86.50887447595596
time_elpased: 1.742
batch start
#iterations: 334
currently lose_sum: 86.88776886463165
time_elpased: 1.74
batch start
#iterations: 335
currently lose_sum: 86.4982174038887
time_elpased: 1.833
batch start
#iterations: 336
currently lose_sum: 86.90462172031403
time_elpased: 1.807
batch start
#iterations: 337
currently lose_sum: 86.6548462510109
time_elpased: 1.821
batch start
#iterations: 338
currently lose_sum: 86.48158657550812
time_elpased: 1.78
batch start
#iterations: 339
currently lose_sum: 86.24164938926697
time_elpased: 1.763
start validation test
0.578556701031
0.610315186246
0.438452037876
0.510301868711
0.578788183333
76.451
batch start
#iterations: 340
currently lose_sum: 86.66427898406982
time_elpased: 1.779
batch start
#iterations: 341
currently lose_sum: 85.86960023641586
time_elpased: 1.796
batch start
#iterations: 342
currently lose_sum: 86.35688245296478
time_elpased: 1.774
batch start
#iterations: 343
currently lose_sum: 86.20068150758743
time_elpased: 1.791
batch start
#iterations: 344
currently lose_sum: 85.69577014446259
time_elpased: 1.741
batch start
#iterations: 345
currently lose_sum: 85.50400525331497
time_elpased: 1.806
batch start
#iterations: 346
currently lose_sum: 85.80660158395767
time_elpased: 1.773
batch start
#iterations: 347
currently lose_sum: 85.73529034852982
time_elpased: 1.809
batch start
#iterations: 348
currently lose_sum: 85.1988434791565
time_elpased: 1.797
batch start
#iterations: 349
currently lose_sum: 85.01989930868149
time_elpased: 1.832
batch start
#iterations: 350
currently lose_sum: 85.28608441352844
time_elpased: 1.816
batch start
#iterations: 351
currently lose_sum: 85.35471510887146
time_elpased: 1.767
batch start
#iterations: 352
currently lose_sum: 85.22012060880661
time_elpased: 1.819
batch start
#iterations: 353
currently lose_sum: 84.79028797149658
time_elpased: 1.803
batch start
#iterations: 354
currently lose_sum: 84.4644945859909
time_elpased: 1.802
batch start
#iterations: 355
currently lose_sum: 84.9470003247261
time_elpased: 1.764
batch start
#iterations: 356
currently lose_sum: 84.42611119151115
time_elpased: 1.735
batch start
#iterations: 357
currently lose_sum: 84.7278082370758
time_elpased: 1.746
batch start
#iterations: 358
currently lose_sum: 85.44525331258774
time_elpased: 1.739
batch start
#iterations: 359
currently lose_sum: 84.80747050046921
time_elpased: 1.78
start validation test
0.57675257732
0.619882109288
0.400473445862
0.486587882198
0.577043827434
76.349
batch start
#iterations: 360
currently lose_sum: 84.62530553340912
time_elpased: 1.783
batch start
#iterations: 361
currently lose_sum: 83.94006419181824
time_elpased: 1.781
batch start
#iterations: 362
currently lose_sum: 83.91105651855469
time_elpased: 1.759
batch start
#iterations: 363
currently lose_sum: 83.92591309547424
time_elpased: 1.793
batch start
#iterations: 364
currently lose_sum: 83.85890752077103
time_elpased: 1.814
batch start
#iterations: 365
currently lose_sum: 83.53105407953262
time_elpased: 1.761
batch start
#iterations: 366
currently lose_sum: 83.71082782745361
time_elpased: 1.799
batch start
#iterations: 367
currently lose_sum: 83.94990402460098
time_elpased: 1.785
batch start
#iterations: 368
currently lose_sum: 83.73803016543388
time_elpased: 1.796
batch start
#iterations: 369
currently lose_sum: 83.45840483903885
time_elpased: 1.796
batch start
#iterations: 370
currently lose_sum: 83.01186031103134
time_elpased: 1.801
batch start
#iterations: 371
currently lose_sum: 82.89112311601639
time_elpased: 1.715
batch start
#iterations: 372
currently lose_sum: 83.09321147203445
time_elpased: 1.815
batch start
#iterations: 373
currently lose_sum: 83.33907842636108
time_elpased: 1.788
batch start
#iterations: 374
currently lose_sum: 83.43075251579285
time_elpased: 1.782
batch start
#iterations: 375
currently lose_sum: 81.91641634702682
time_elpased: 1.748
batch start
#iterations: 376
currently lose_sum: 82.44341152906418
time_elpased: 1.74
batch start
#iterations: 377
currently lose_sum: 83.03479832410812
time_elpased: 1.794
batch start
#iterations: 378
currently lose_sum: 82.8413354754448
time_elpased: 1.738
batch start
#iterations: 379
currently lose_sum: 82.48230612277985
time_elpased: 1.752
start validation test
0.571804123711
0.587853847113
0.485179086044
0.53160417254
0.57194724645
81.720
batch start
#iterations: 380
currently lose_sum: 81.85944706201553
time_elpased: 1.735
batch start
#iterations: 381
currently lose_sum: 81.8103997707367
time_elpased: 1.734
batch start
#iterations: 382
currently lose_sum: 82.3605677485466
time_elpased: 1.756
batch start
#iterations: 383
currently lose_sum: 82.26576489210129
time_elpased: 1.797
batch start
#iterations: 384
currently lose_sum: 82.0276083946228
time_elpased: 1.829
batch start
#iterations: 385
currently lose_sum: 81.95543551445007
time_elpased: 1.788
batch start
#iterations: 386
currently lose_sum: 80.96945658326149
time_elpased: 1.749
batch start
#iterations: 387
currently lose_sum: 81.54193487763405
time_elpased: 1.742
batch start
#iterations: 388
currently lose_sum: 81.99636420607567
time_elpased: 1.766
batch start
#iterations: 389
currently lose_sum: 81.61646136641502
time_elpased: 1.744
batch start
#iterations: 390
currently lose_sum: 81.24599826335907
time_elpased: 1.781
batch start
#iterations: 391
currently lose_sum: 81.4863655269146
time_elpased: 1.753
batch start
#iterations: 392
currently lose_sum: 80.77286931872368
time_elpased: 1.749
batch start
#iterations: 393
currently lose_sum: 80.99630078673363
time_elpased: 1.761
batch start
#iterations: 394
currently lose_sum: 81.18106693029404
time_elpased: 1.737
batch start
#iterations: 395
currently lose_sum: 80.80471459031105
time_elpased: 1.848
batch start
#iterations: 396
currently lose_sum: 80.29955765604973
time_elpased: 1.777
batch start
#iterations: 397
currently lose_sum: 81.11655792593956
time_elpased: 1.794
batch start
#iterations: 398
currently lose_sum: 80.47748857736588
time_elpased: 1.789
batch start
#iterations: 399
currently lose_sum: 80.95592701435089
time_elpased: 1.789
start validation test
0.563041237113
0.607757870934
0.359613009469
0.451859036534
0.563377343231
87.705
acc: 0.627
pre: 0.638
rec: 0.591
F1: 0.614
auc: 0.674
