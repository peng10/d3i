start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.08425706624985
time_elpased: 2.218
batch start
#iterations: 1
currently lose_sum: 99.65659093856812
time_elpased: 2.212
batch start
#iterations: 2
currently lose_sum: 99.27138739824295
time_elpased: 2.171
batch start
#iterations: 3
currently lose_sum: 98.47213286161423
time_elpased: 2.21
batch start
#iterations: 4
currently lose_sum: 98.2956514954567
time_elpased: 2.178
batch start
#iterations: 5
currently lose_sum: 97.95900589227676
time_elpased: 2.253
batch start
#iterations: 6
currently lose_sum: 97.75963294506073
time_elpased: 2.298
batch start
#iterations: 7
currently lose_sum: 97.69474524259567
time_elpased: 2.222
batch start
#iterations: 8
currently lose_sum: 97.36714887619019
time_elpased: 2.21
batch start
#iterations: 9
currently lose_sum: 97.11635273694992
time_elpased: 2.289
batch start
#iterations: 10
currently lose_sum: 97.26090949773788
time_elpased: 2.263
batch start
#iterations: 11
currently lose_sum: 97.08923119306564
time_elpased: 2.215
batch start
#iterations: 12
currently lose_sum: 96.6357952952385
time_elpased: 2.189
batch start
#iterations: 13
currently lose_sum: 96.92223691940308
time_elpased: 2.238
batch start
#iterations: 14
currently lose_sum: 96.61926525831223
time_elpased: 2.218
batch start
#iterations: 15
currently lose_sum: 96.22893220186234
time_elpased: 2.187
batch start
#iterations: 16
currently lose_sum: 96.03752660751343
time_elpased: 2.23
batch start
#iterations: 17
currently lose_sum: 95.62167054414749
time_elpased: 2.255
batch start
#iterations: 18
currently lose_sum: 95.89734333753586
time_elpased: 2.205
batch start
#iterations: 19
currently lose_sum: 95.36279273033142
time_elpased: 2.232
start validation test
0.641288659794
0.611393939394
0.77871552079
0.684984835453
0.641061601783
61.871
batch start
#iterations: 20
currently lose_sum: 95.23924469947815
time_elpased: 2.192
batch start
#iterations: 21
currently lose_sum: 95.0605901479721
time_elpased: 2.262
batch start
#iterations: 22
currently lose_sum: 95.19984066486359
time_elpased: 2.169
batch start
#iterations: 23
currently lose_sum: 94.79562324285507
time_elpased: 2.243
batch start
#iterations: 24
currently lose_sum: 94.82815647125244
time_elpased: 2.262
batch start
#iterations: 25
currently lose_sum: 94.34575629234314
time_elpased: 2.248
batch start
#iterations: 26
currently lose_sum: 94.36515629291534
time_elpased: 2.248
batch start
#iterations: 27
currently lose_sum: 94.72200584411621
time_elpased: 2.219
batch start
#iterations: 28
currently lose_sum: 94.69928103685379
time_elpased: 2.219
batch start
#iterations: 29
currently lose_sum: 94.55763560533524
time_elpased: 2.147
batch start
#iterations: 30
currently lose_sum: 94.25803256034851
time_elpased: 2.295
batch start
#iterations: 31
currently lose_sum: 94.37535011768341
time_elpased: 2.17
batch start
#iterations: 32
currently lose_sum: 93.81391805410385
time_elpased: 2.249
batch start
#iterations: 33
currently lose_sum: 94.10896444320679
time_elpased: 2.257
batch start
#iterations: 34
currently lose_sum: 93.6806269288063
time_elpased: 2.201
batch start
#iterations: 35
currently lose_sum: 94.20701593160629
time_elpased: 2.177
batch start
#iterations: 36
currently lose_sum: 93.46657806634903
time_elpased: 2.211
batch start
#iterations: 37
currently lose_sum: 93.94527810811996
time_elpased: 2.233
batch start
#iterations: 38
currently lose_sum: 93.61788427829742
time_elpased: 2.196
batch start
#iterations: 39
currently lose_sum: 93.78561407327652
time_elpased: 2.194
start validation test
0.66618556701
0.641955835962
0.75401399753
0.693487315411
0.666040456014
59.909
batch start
#iterations: 40
currently lose_sum: 93.1482544541359
time_elpased: 2.214
batch start
#iterations: 41
currently lose_sum: 93.86775732040405
time_elpased: 2.201
batch start
#iterations: 42
currently lose_sum: 93.37562221288681
time_elpased: 2.277
batch start
#iterations: 43
currently lose_sum: 93.08211815357208
time_elpased: 2.206
batch start
#iterations: 44
currently lose_sum: 93.30282390117645
time_elpased: 2.22
batch start
#iterations: 45
currently lose_sum: 93.09744840860367
time_elpased: 2.201
batch start
#iterations: 46
currently lose_sum: 93.41797107458115
time_elpased: 2.259
batch start
#iterations: 47
currently lose_sum: 92.97509384155273
time_elpased: 2.204
batch start
#iterations: 48
currently lose_sum: 92.7770471572876
time_elpased: 2.213
batch start
#iterations: 49
currently lose_sum: 92.70549613237381
time_elpased: 2.191
batch start
#iterations: 50
currently lose_sum: 92.817473590374
time_elpased: 2.212
batch start
#iterations: 51
currently lose_sum: 92.2922745347023
time_elpased: 2.217
batch start
#iterations: 52
currently lose_sum: 92.70837169885635
time_elpased: 2.22
batch start
#iterations: 53
currently lose_sum: 92.68722075223923
time_elpased: 2.176
batch start
#iterations: 54
currently lose_sum: 92.50113594532013
time_elpased: 2.225
batch start
#iterations: 55
currently lose_sum: 92.38124412298203
time_elpased: 2.211
batch start
#iterations: 56
currently lose_sum: 92.37204849720001
time_elpased: 2.32
batch start
#iterations: 57
currently lose_sum: 92.91719949245453
time_elpased: 2.237
batch start
#iterations: 58
currently lose_sum: 92.44373041391373
time_elpased: 2.218
batch start
#iterations: 59
currently lose_sum: 92.23739051818848
time_elpased: 2.211
start validation test
0.651443298969
0.636481241915
0.708933717579
0.670756646217
0.651348312734
60.141
batch start
#iterations: 60
currently lose_sum: 92.22466415166855
time_elpased: 2.499
batch start
#iterations: 61
currently lose_sum: 92.72526913881302
time_elpased: 2.605
batch start
#iterations: 62
currently lose_sum: 91.82322758436203
time_elpased: 2.618
batch start
#iterations: 63
currently lose_sum: 92.28110814094543
time_elpased: 2.573
batch start
#iterations: 64
currently lose_sum: 92.15727186203003
time_elpased: 2.587
batch start
#iterations: 65
currently lose_sum: 92.07754158973694
time_elpased: 2.502
batch start
#iterations: 66
currently lose_sum: 91.85816031694412
time_elpased: 2.53
batch start
#iterations: 67
currently lose_sum: 91.9481640458107
time_elpased: 2.537
batch start
#iterations: 68
currently lose_sum: 91.79760932922363
time_elpased: 2.482
batch start
#iterations: 69
currently lose_sum: 91.72807914018631
time_elpased: 2.453
batch start
#iterations: 70
currently lose_sum: 91.46635615825653
time_elpased: 2.573
batch start
#iterations: 71
currently lose_sum: 91.42734664678574
time_elpased: 2.491
batch start
#iterations: 72
currently lose_sum: 91.3569728732109
time_elpased: 2.509
batch start
#iterations: 73
currently lose_sum: 91.30910009145737
time_elpased: 2.482
batch start
#iterations: 74
currently lose_sum: 91.49522262811661
time_elpased: 2.485
batch start
#iterations: 75
currently lose_sum: 91.65789294242859
time_elpased: 2.474
batch start
#iterations: 76
currently lose_sum: 91.44018626213074
time_elpased: 2.586
batch start
#iterations: 77
currently lose_sum: 91.51520383358002
time_elpased: 2.496
batch start
#iterations: 78
currently lose_sum: 91.38333624601364
time_elpased: 2.479
batch start
#iterations: 79
currently lose_sum: 90.99504381418228
time_elpased: 2.48
start validation test
0.663969072165
0.656638902499
0.689687114039
0.672757391697
0.663926580563
59.995
batch start
#iterations: 80
currently lose_sum: 91.45385974645615
time_elpased: 2.497
batch start
#iterations: 81
currently lose_sum: 91.01290088891983
time_elpased: 2.444
batch start
#iterations: 82
currently lose_sum: 90.6232323050499
time_elpased: 2.507
batch start
#iterations: 83
currently lose_sum: 91.03863430023193
time_elpased: 2.575
batch start
#iterations: 84
currently lose_sum: 90.44051003456116
time_elpased: 2.592
batch start
#iterations: 85
currently lose_sum: 90.40297424793243
time_elpased: 2.629
batch start
#iterations: 86
currently lose_sum: 90.24752455949783
time_elpased: 2.669
batch start
#iterations: 87
currently lose_sum: 90.68972700834274
time_elpased: 2.528
batch start
#iterations: 88
currently lose_sum: 90.61978667974472
time_elpased: 2.536
batch start
#iterations: 89
currently lose_sum: 90.03054547309875
time_elpased: 2.515
batch start
#iterations: 90
currently lose_sum: 90.21018433570862
time_elpased: 2.478
batch start
#iterations: 91
currently lose_sum: 90.16884791851044
time_elpased: 2.547
batch start
#iterations: 92
currently lose_sum: 90.38319772481918
time_elpased: 2.466
batch start
#iterations: 93
currently lose_sum: 89.84722590446472
time_elpased: 2.526
batch start
#iterations: 94
currently lose_sum: 89.67002028226852
time_elpased: 2.461
batch start
#iterations: 95
currently lose_sum: 90.30012547969818
time_elpased: 2.508
batch start
#iterations: 96
currently lose_sum: 89.76357138156891
time_elpased: 2.537
batch start
#iterations: 97
currently lose_sum: 89.1526346206665
time_elpased: 2.525
batch start
#iterations: 98
currently lose_sum: 89.59252315759659
time_elpased: 2.509
batch start
#iterations: 99
currently lose_sum: 89.00726276636124
time_elpased: 2.6
start validation test
0.651443298969
0.646150801504
0.672087278716
0.658863888609
0.651409190783
59.956
batch start
#iterations: 100
currently lose_sum: 89.63549667596817
time_elpased: 2.197
batch start
#iterations: 101
currently lose_sum: 89.2476914525032
time_elpased: 2.221
batch start
#iterations: 102
currently lose_sum: 89.66222888231277
time_elpased: 2.195
batch start
#iterations: 103
currently lose_sum: 88.91310548782349
time_elpased: 2.28
batch start
#iterations: 104
currently lose_sum: 89.18509119749069
time_elpased: 2.264
batch start
#iterations: 105
currently lose_sum: 88.71042460203171
time_elpased: 2.3
batch start
#iterations: 106
currently lose_sum: 88.76434510946274
time_elpased: 2.251
batch start
#iterations: 107
currently lose_sum: 89.38903450965881
time_elpased: 2.227
batch start
#iterations: 108
currently lose_sum: 88.90912008285522
time_elpased: 2.21
batch start
#iterations: 109
currently lose_sum: 88.58309650421143
time_elpased: 2.215
batch start
#iterations: 110
currently lose_sum: 88.3974216580391
time_elpased: 2.197
batch start
#iterations: 111
currently lose_sum: 88.0608698129654
time_elpased: 2.193
batch start
#iterations: 112
currently lose_sum: 88.03392857313156
time_elpased: 2.168
batch start
#iterations: 113
currently lose_sum: 88.20917403697968
time_elpased: 2.244
batch start
#iterations: 114
currently lose_sum: 88.20071798563004
time_elpased: 2.217
batch start
#iterations: 115
currently lose_sum: 88.01736325025558
time_elpased: 2.191
batch start
#iterations: 116
currently lose_sum: 87.88290083408356
time_elpased: 2.251
batch start
#iterations: 117
currently lose_sum: 87.77741611003876
time_elpased: 2.176
batch start
#iterations: 118
currently lose_sum: 87.85590422153473
time_elpased: 2.155
batch start
#iterations: 119
currently lose_sum: 87.83487129211426
time_elpased: 2.242
start validation test
0.646701030928
0.65390406539
0.625771922602
0.639528768276
0.646735610206
60.335
batch start
#iterations: 120
currently lose_sum: 87.18971520662308
time_elpased: 2.186
batch start
#iterations: 121
currently lose_sum: 87.18104976415634
time_elpased: 2.18
batch start
#iterations: 122
currently lose_sum: 87.20845586061478
time_elpased: 2.231
batch start
#iterations: 123
currently lose_sum: 87.55108749866486
time_elpased: 2.192
batch start
#iterations: 124
currently lose_sum: 87.0945617556572
time_elpased: 2.251
batch start
#iterations: 125
currently lose_sum: 86.98570650815964
time_elpased: 2.196
batch start
#iterations: 126
currently lose_sum: 86.80649793148041
time_elpased: 2.174
batch start
#iterations: 127
currently lose_sum: 86.87128406763077
time_elpased: 2.242
batch start
#iterations: 128
currently lose_sum: 86.79479545354843
time_elpased: 2.196
batch start
#iterations: 129
currently lose_sum: 86.9127259850502
time_elpased: 2.192
batch start
#iterations: 130
currently lose_sum: 86.3908856511116
time_elpased: 2.173
batch start
#iterations: 131
currently lose_sum: 86.16817808151245
time_elpased: 2.155
batch start
#iterations: 132
currently lose_sum: 86.57753521203995
time_elpased: 2.189
batch start
#iterations: 133
currently lose_sum: 86.59578746557236
time_elpased: 2.172
batch start
#iterations: 134
currently lose_sum: 86.36609119176865
time_elpased: 2.208
batch start
#iterations: 135
currently lose_sum: 85.88552004098892
time_elpased: 2.187
batch start
#iterations: 136
currently lose_sum: 85.59006649255753
time_elpased: 2.159
batch start
#iterations: 137
currently lose_sum: 85.43531203269958
time_elpased: 2.23
batch start
#iterations: 138
currently lose_sum: 85.77370023727417
time_elpased: 2.254
batch start
#iterations: 139
currently lose_sum: 85.39575618505478
time_elpased: 2.171
start validation test
0.638092783505
0.640116460435
0.633594071634
0.636838566182
0.638100216321
61.218
batch start
#iterations: 140
currently lose_sum: 84.95719921588898
time_elpased: 2.25
batch start
#iterations: 141
currently lose_sum: 85.51661986112595
time_elpased: 2.175
batch start
#iterations: 142
currently lose_sum: 84.88980388641357
time_elpased: 2.247
batch start
#iterations: 143
currently lose_sum: 85.22012883424759
time_elpased: 2.255
batch start
#iterations: 144
currently lose_sum: 84.86670136451721
time_elpased: 2.179
batch start
#iterations: 145
currently lose_sum: 84.8331670165062
time_elpased: 2.164
batch start
#iterations: 146
currently lose_sum: 84.63050746917725
time_elpased: 2.246
batch start
#iterations: 147
currently lose_sum: 85.12322002649307
time_elpased: 2.27
batch start
#iterations: 148
currently lose_sum: 84.56409472227097
time_elpased: 2.138
batch start
#iterations: 149
currently lose_sum: 84.43607896566391
time_elpased: 2.21
batch start
#iterations: 150
currently lose_sum: 84.0214661359787
time_elpased: 2.174
batch start
#iterations: 151
currently lose_sum: 84.68510615825653
time_elpased: 2.217
batch start
#iterations: 152
currently lose_sum: 83.99008792638779
time_elpased: 2.186
batch start
#iterations: 153
currently lose_sum: 84.09630692005157
time_elpased: 2.216
batch start
#iterations: 154
currently lose_sum: 83.46061676740646
time_elpased: 2.157
batch start
#iterations: 155
currently lose_sum: 83.29412966966629
time_elpased: 2.141
batch start
#iterations: 156
currently lose_sum: 83.3320392370224
time_elpased: 2.16
batch start
#iterations: 157
currently lose_sum: 82.84740728139877
time_elpased: 2.183
batch start
#iterations: 158
currently lose_sum: 83.70654666423798
time_elpased: 2.212
batch start
#iterations: 159
currently lose_sum: 83.65906542539597
time_elpased: 2.194
start validation test
0.627835051546
0.638114209827
0.593454096336
0.61497440273
0.627891856099
62.987
batch start
#iterations: 160
currently lose_sum: 82.31363329291344
time_elpased: 2.162
batch start
#iterations: 161
currently lose_sum: 82.20118513703346
time_elpased: 2.155
batch start
#iterations: 162
currently lose_sum: 81.89303225278854
time_elpased: 2.188
batch start
#iterations: 163
currently lose_sum: 82.40660136938095
time_elpased: 2.149
batch start
#iterations: 164
currently lose_sum: 81.8546102643013
time_elpased: 2.204
batch start
#iterations: 165
currently lose_sum: 82.23290795087814
time_elpased: 2.19
batch start
#iterations: 166
currently lose_sum: 81.14963561296463
time_elpased: 2.205
batch start
#iterations: 167
currently lose_sum: 82.13556271791458
time_elpased: 2.194
batch start
#iterations: 168
currently lose_sum: 81.42821949720383
time_elpased: 2.203
batch start
#iterations: 169
currently lose_sum: 80.64866226911545
time_elpased: 2.186
batch start
#iterations: 170
currently lose_sum: 80.73133507370949
time_elpased: 2.186
batch start
#iterations: 171
currently lose_sum: 81.17851909995079
time_elpased: 2.195
batch start
#iterations: 172
currently lose_sum: 80.33247131109238
time_elpased: 2.24
batch start
#iterations: 173
currently lose_sum: 79.76763236522675
time_elpased: 2.22
batch start
#iterations: 174
currently lose_sum: 80.51439419388771
time_elpased: 2.208
batch start
#iterations: 175
currently lose_sum: 79.95627424120903
time_elpased: 2.204
batch start
#iterations: 176
currently lose_sum: 79.01623582839966
time_elpased: 2.187
batch start
#iterations: 177
currently lose_sum: 79.06438955664635
time_elpased: 2.194
batch start
#iterations: 178
currently lose_sum: 79.12738546729088
time_elpased: 2.16
batch start
#iterations: 179
currently lose_sum: 78.93369710445404
time_elpased: 2.18
start validation test
0.620309278351
0.649036022324
0.526657060519
0.581477272727
0.620464011465
67.325
batch start
#iterations: 180
currently lose_sum: 78.68539875745773
time_elpased: 2.242
batch start
#iterations: 181
currently lose_sum: 78.23772817850113
time_elpased: 2.223
batch start
#iterations: 182
currently lose_sum: 78.59916123747826
time_elpased: 2.175
batch start
#iterations: 183
currently lose_sum: 76.88556531071663
time_elpased: 2.802
batch start
#iterations: 184
currently lose_sum: 77.5320049226284
time_elpased: 4.2
batch start
#iterations: 185
currently lose_sum: 76.98020133376122
time_elpased: 4.649
batch start
#iterations: 186
currently lose_sum: 77.22655567526817
time_elpased: 4.723
batch start
#iterations: 187
currently lose_sum: 76.25330865383148
time_elpased: 4.695
batch start
#iterations: 188
currently lose_sum: 76.8511138856411
time_elpased: 4.457
batch start
#iterations: 189
currently lose_sum: 76.85195270180702
time_elpased: 4.231
batch start
#iterations: 190
currently lose_sum: 75.93481710553169
time_elpased: 4.454
batch start
#iterations: 191
currently lose_sum: 75.83932784199715
time_elpased: 4.573
batch start
#iterations: 192
currently lose_sum: 75.16112497448921
time_elpased: 4.102
batch start
#iterations: 193
currently lose_sum: 74.64675039052963
time_elpased: 4.335
batch start
#iterations: 194
currently lose_sum: 75.19906216859818
time_elpased: 3.829
batch start
#iterations: 195
currently lose_sum: 73.45653599500656
time_elpased: 4.058
batch start
#iterations: 196
currently lose_sum: 73.29612991213799
time_elpased: 4.333
batch start
#iterations: 197
currently lose_sum: 73.51627084612846
time_elpased: 4.327
batch start
#iterations: 198
currently lose_sum: 73.81656709313393
time_elpased: 3.07
batch start
#iterations: 199
currently lose_sum: 73.1230941414833
time_elpased: 2.229
start validation test
0.605154639175
0.641910546659
0.478592013174
0.548349056604
0.60536374719
75.023
batch start
#iterations: 200
currently lose_sum: 72.9493959248066
time_elpased: 2.157
batch start
#iterations: 201
currently lose_sum: 72.51974600553513
time_elpased: 2.17
batch start
#iterations: 202
currently lose_sum: 72.61901319026947
time_elpased: 2.168
batch start
#iterations: 203
currently lose_sum: 73.16660004854202
time_elpased: 2.178
batch start
#iterations: 204
currently lose_sum: 71.27558407187462
time_elpased: 2.203
batch start
#iterations: 205
currently lose_sum: 71.81692627072334
time_elpased: 2.187
batch start
#iterations: 206
currently lose_sum: 71.21013808250427
time_elpased: 2.209
batch start
#iterations: 207
currently lose_sum: 70.37369671463966
time_elpased: 2.132
batch start
#iterations: 208
currently lose_sum: 69.71501779556274
time_elpased: 2.211
batch start
#iterations: 209
currently lose_sum: 69.54001960158348
time_elpased: 2.182
batch start
#iterations: 210
currently lose_sum: 68.66323709487915
time_elpased: 2.169
batch start
#iterations: 211
currently lose_sum: 69.13296300172806
time_elpased: 2.243
batch start
#iterations: 212
currently lose_sum: 68.10892495512962
time_elpased: 2.229
batch start
#iterations: 213
currently lose_sum: 67.55938863754272
time_elpased: 2.202
batch start
#iterations: 214
currently lose_sum: 68.13325223326683
time_elpased: 2.207
batch start
#iterations: 215
currently lose_sum: 67.38052052259445
time_elpased: 2.216
batch start
#iterations: 216
currently lose_sum: 66.56561464071274
time_elpased: 2.151
batch start
#iterations: 217
currently lose_sum: 66.46280738711357
time_elpased: 2.229
batch start
#iterations: 218
currently lose_sum: 66.82052779197693
time_elpased: 2.209
batch start
#iterations: 219
currently lose_sum: 65.48624005913734
time_elpased: 2.189
start validation test
0.572164948454
0.622279792746
0.37083161795
0.464723332903
0.572497593362
88.717
batch start
#iterations: 220
currently lose_sum: 66.12673425674438
time_elpased: 2.229
batch start
#iterations: 221
currently lose_sum: 64.7236402630806
time_elpased: 2.232
batch start
#iterations: 222
currently lose_sum: 64.5003507733345
time_elpased: 2.207
batch start
#iterations: 223
currently lose_sum: 63.67701169848442
time_elpased: 2.207
batch start
#iterations: 224
currently lose_sum: 62.71647799015045
time_elpased: 2.208
batch start
#iterations: 225
currently lose_sum: 62.55249562859535
time_elpased: 2.174
batch start
#iterations: 226
currently lose_sum: 62.42798337340355
time_elpased: 2.73
batch start
#iterations: 227
currently lose_sum: 62.48477903008461
time_elpased: 4.163
batch start
#iterations: 228
currently lose_sum: 61.67415100336075
time_elpased: 5.324
batch start
#iterations: 229
currently lose_sum: 61.49719077348709
time_elpased: 4.496
batch start
#iterations: 230
currently lose_sum: 61.32058674097061
time_elpased: 4.757
batch start
#iterations: 231
currently lose_sum: 60.869081646203995
time_elpased: 4.603
batch start
#iterations: 232
currently lose_sum: 60.78483992815018
time_elpased: 4.329
batch start
#iterations: 233
currently lose_sum: 60.74116379022598
time_elpased: 4.34
batch start
#iterations: 234
currently lose_sum: 59.83427983522415
time_elpased: 4.389
batch start
#iterations: 235
currently lose_sum: 59.199521243572235
time_elpased: 4.201
batch start
#iterations: 236
currently lose_sum: 59.45873898267746
time_elpased: 4.363
batch start
#iterations: 237
currently lose_sum: 57.2971992790699
time_elpased: 4.264
batch start
#iterations: 238
currently lose_sum: 56.970903396606445
time_elpased: 4.568
batch start
#iterations: 239
currently lose_sum: 56.61922749876976
time_elpased: 4.249
start validation test
0.566030927835
0.632428017153
0.318752573075
0.423869157599
0.566439483564
105.030
batch start
#iterations: 240
currently lose_sum: 56.814476042985916
time_elpased: 4.141
batch start
#iterations: 241
currently lose_sum: 56.16445177793503
time_elpased: 4.404
batch start
#iterations: 242
currently lose_sum: 55.7510099709034
time_elpased: 4.357
batch start
#iterations: 243
currently lose_sum: 55.02018213272095
time_elpased: 4.311
batch start
#iterations: 244
currently lose_sum: 55.32704338431358
time_elpased: 4.416
batch start
#iterations: 245
currently lose_sum: 53.16982915997505
time_elpased: 4.228
batch start
#iterations: 246
currently lose_sum: 54.516120970249176
time_elpased: 4.307
batch start
#iterations: 247
currently lose_sum: 53.948619306087494
time_elpased: 4.318
batch start
#iterations: 248
currently lose_sum: 52.970239341259
time_elpased: 4.567
batch start
#iterations: 249
currently lose_sum: 52.79308542609215
time_elpased: 4.135
batch start
#iterations: 250
currently lose_sum: 52.40971240401268
time_elpased: 4.475
batch start
#iterations: 251
currently lose_sum: 51.58404844999313
time_elpased: 4.487
batch start
#iterations: 252
currently lose_sum: 50.951448768377304
time_elpased: 4.128
batch start
#iterations: 253
currently lose_sum: 50.44310715794563
time_elpased: 4.09
batch start
#iterations: 254
currently lose_sum: 51.69808742403984
time_elpased: 4.191
batch start
#iterations: 255
currently lose_sum: 49.177073910832405
time_elpased: 4.457
batch start
#iterations: 256
currently lose_sum: 49.008207723498344
time_elpased: 4.156
batch start
#iterations: 257
currently lose_sum: 48.399321258068085
time_elpased: 4.523
batch start
#iterations: 258
currently lose_sum: 48.589521020650864
time_elpased: 4.556
batch start
#iterations: 259
currently lose_sum: 47.36443565785885
time_elpased: 4.299
start validation test
0.553092783505
0.629455445545
0.261733223549
0.36972957255
0.553574170634
130.758
batch start
#iterations: 260
currently lose_sum: 47.614165753126144
time_elpased: 4.322
batch start
#iterations: 261
currently lose_sum: 47.65650926530361
time_elpased: 4.0
batch start
#iterations: 262
currently lose_sum: 47.43079710006714
time_elpased: 4.17
batch start
#iterations: 263
currently lose_sum: 46.09178480505943
time_elpased: 4.083
batch start
#iterations: 264
currently lose_sum: 45.26790875196457
time_elpased: 3.705
batch start
#iterations: 265
currently lose_sum: 45.966225400567055
time_elpased: 2.207
batch start
#iterations: 266
currently lose_sum: 44.75321674346924
time_elpased: 2.229
batch start
#iterations: 267
currently lose_sum: 44.93364441394806
time_elpased: 2.176
batch start
#iterations: 268
currently lose_sum: 43.67977266013622
time_elpased: 2.187
batch start
#iterations: 269
currently lose_sum: 44.19583633542061
time_elpased: 2.239
batch start
#iterations: 270
currently lose_sum: 43.97483175992966
time_elpased: 2.207
batch start
#iterations: 271
currently lose_sum: 42.78577619791031
time_elpased: 2.204
batch start
#iterations: 272
currently lose_sum: 43.23825569450855
time_elpased: 2.176
batch start
#iterations: 273
currently lose_sum: 42.138311460614204
time_elpased: 2.157
batch start
#iterations: 274
currently lose_sum: 41.959892719984055
time_elpased: 2.23
batch start
#iterations: 275
currently lose_sum: 41.24841848015785
time_elpased: 2.171
batch start
#iterations: 276
currently lose_sum: 40.43399518728256
time_elpased: 2.177
batch start
#iterations: 277
currently lose_sum: 40.97258234024048
time_elpased: 2.188
batch start
#iterations: 278
currently lose_sum: 39.80394773185253
time_elpased: 2.157
batch start
#iterations: 279
currently lose_sum: 38.29496040940285
time_elpased: 2.204
start validation test
0.548195876289
0.637229437229
0.227254013998
0.335027691374
0.548726139589
155.077
batch start
#iterations: 280
currently lose_sum: 38.824157416820526
time_elpased: 2.196
batch start
#iterations: 281
currently lose_sum: 39.107551500201225
time_elpased: 2.176
batch start
#iterations: 282
currently lose_sum: 38.13114292919636
time_elpased: 2.151
batch start
#iterations: 283
currently lose_sum: 38.11443592607975
time_elpased: 2.208
batch start
#iterations: 284
currently lose_sum: 37.787082090973854
time_elpased: 2.185
batch start
#iterations: 285
currently lose_sum: 38.07695618271828
time_elpased: 2.18
batch start
#iterations: 286
currently lose_sum: 37.06404650211334
time_elpased: 2.211
batch start
#iterations: 287
currently lose_sum: 37.544706135988235
time_elpased: 2.142
batch start
#iterations: 288
currently lose_sum: 35.69813595712185
time_elpased: 2.162
batch start
#iterations: 289
currently lose_sum: 35.90863500535488
time_elpased: 2.146
batch start
#iterations: 290
currently lose_sum: 35.67403742671013
time_elpased: 2.161
batch start
#iterations: 291
currently lose_sum: 34.444687113165855
time_elpased: 2.171
batch start
#iterations: 292
currently lose_sum: 33.947611793875694
time_elpased: 2.204
batch start
#iterations: 293
currently lose_sum: 34.15784913301468
time_elpased: 2.203
batch start
#iterations: 294
currently lose_sum: 34.78621779382229
time_elpased: 2.171
batch start
#iterations: 295
currently lose_sum: 33.64716613292694
time_elpased: 2.163
batch start
#iterations: 296
currently lose_sum: 32.76540046930313
time_elpased: 2.151
batch start
#iterations: 297
currently lose_sum: 32.98142535984516
time_elpased: 2.153
batch start
#iterations: 298
currently lose_sum: 32.9545441865921
time_elpased: 2.181
batch start
#iterations: 299
currently lose_sum: 32.5017476528883
time_elpased: 2.2
start validation test
0.535103092784
0.648234793705
0.156854672705
0.252589707467
0.535728038542
192.866
batch start
#iterations: 300
currently lose_sum: 32.92071512341499
time_elpased: 2.159
batch start
#iterations: 301
currently lose_sum: 31.780357852578163
time_elpased: 2.191
batch start
#iterations: 302
currently lose_sum: 32.44778490066528
time_elpased: 2.226
batch start
#iterations: 303
currently lose_sum: 32.45928677916527
time_elpased: 2.185
batch start
#iterations: 304
currently lose_sum: 30.81073349714279
time_elpased: 2.198
batch start
#iterations: 305
currently lose_sum: 31.199529349803925
time_elpased: 2.163
batch start
#iterations: 306
currently lose_sum: nan
time_elpased: 2.154
train finish final lose is: nan
acc: 0.673
pre: 0.647
rec: 0.765
F1: 0.701
auc: 0.673
