start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.19565254449844
time_elpased: 2.262
batch start
#iterations: 1
currently lose_sum: 99.55202442407608
time_elpased: 2.199
batch start
#iterations: 2
currently lose_sum: 99.48575067520142
time_elpased: 2.2
batch start
#iterations: 3
currently lose_sum: 98.79724645614624
time_elpased: 2.263
batch start
#iterations: 4
currently lose_sum: 98.60943508148193
time_elpased: 2.189
batch start
#iterations: 5
currently lose_sum: 98.1898507475853
time_elpased: 2.204
batch start
#iterations: 6
currently lose_sum: 97.85020214319229
time_elpased: 2.213
batch start
#iterations: 7
currently lose_sum: 97.86025196313858
time_elpased: 2.213
batch start
#iterations: 8
currently lose_sum: 97.82139080762863
time_elpased: 2.217
batch start
#iterations: 9
currently lose_sum: 97.1820787191391
time_elpased: 2.237
batch start
#iterations: 10
currently lose_sum: 97.19970417022705
time_elpased: 2.211
batch start
#iterations: 11
currently lose_sum: 97.21236395835876
time_elpased: 2.202
batch start
#iterations: 12
currently lose_sum: 96.92266356945038
time_elpased: 2.247
batch start
#iterations: 13
currently lose_sum: 96.84116923809052
time_elpased: 2.232
batch start
#iterations: 14
currently lose_sum: 96.28979086875916
time_elpased: 2.206
batch start
#iterations: 15
currently lose_sum: 96.32035499811172
time_elpased: 2.233
batch start
#iterations: 16
currently lose_sum: 96.38876241445541
time_elpased: 2.182
batch start
#iterations: 17
currently lose_sum: 95.73545825481415
time_elpased: 2.215
batch start
#iterations: 18
currently lose_sum: 95.48921430110931
time_elpased: 2.18
batch start
#iterations: 19
currently lose_sum: 95.40309244394302
time_elpased: 2.211
start validation test
0.66
0.640017945267
0.734074302768
0.683827053974
0.659869951136
60.633
batch start
#iterations: 20
currently lose_sum: 95.47378444671631
time_elpased: 2.19
batch start
#iterations: 21
currently lose_sum: 94.8225217461586
time_elpased: 2.212
batch start
#iterations: 22
currently lose_sum: 95.12097418308258
time_elpased: 2.223
batch start
#iterations: 23
currently lose_sum: 94.94517731666565
time_elpased: 2.18
batch start
#iterations: 24
currently lose_sum: 94.80508428812027
time_elpased: 2.231
batch start
#iterations: 25
currently lose_sum: 94.61051833629608
time_elpased: 2.247
batch start
#iterations: 26
currently lose_sum: 94.57999557256699
time_elpased: 2.213
batch start
#iterations: 27
currently lose_sum: 93.85758018493652
time_elpased: 2.175
batch start
#iterations: 28
currently lose_sum: 94.05760782957077
time_elpased: 2.22
batch start
#iterations: 29
currently lose_sum: 93.82202833890915
time_elpased: 2.231
batch start
#iterations: 30
currently lose_sum: 93.93402463197708
time_elpased: 2.203
batch start
#iterations: 31
currently lose_sum: 94.17059445381165
time_elpased: 2.197
batch start
#iterations: 32
currently lose_sum: 93.65850126743317
time_elpased: 2.226
batch start
#iterations: 33
currently lose_sum: 93.20816743373871
time_elpased: 2.18
batch start
#iterations: 34
currently lose_sum: 93.4660291671753
time_elpased: 2.197
batch start
#iterations: 35
currently lose_sum: 93.32218551635742
time_elpased: 2.224
batch start
#iterations: 36
currently lose_sum: 93.689138174057
time_elpased: 2.249
batch start
#iterations: 37
currently lose_sum: 92.79202872514725
time_elpased: 2.194
batch start
#iterations: 38
currently lose_sum: 92.90525740385056
time_elpased: 2.263
batch start
#iterations: 39
currently lose_sum: 93.00512117147446
time_elpased: 2.214
start validation test
0.662319587629
0.634425951087
0.768858701245
0.695203089378
0.662132541782
59.367
batch start
#iterations: 40
currently lose_sum: 93.07844603061676
time_elpased: 2.217
batch start
#iterations: 41
currently lose_sum: 92.40010923147202
time_elpased: 2.185
batch start
#iterations: 42
currently lose_sum: 92.26791328191757
time_elpased: 2.176
batch start
#iterations: 43
currently lose_sum: 92.60915839672089
time_elpased: 2.234
batch start
#iterations: 44
currently lose_sum: 92.46805369853973
time_elpased: 2.236
batch start
#iterations: 45
currently lose_sum: 92.44126254320145
time_elpased: 2.192
batch start
#iterations: 46
currently lose_sum: 92.23940402269363
time_elpased: 2.178
batch start
#iterations: 47
currently lose_sum: 92.66756230592728
time_elpased: 2.221
batch start
#iterations: 48
currently lose_sum: 92.52889227867126
time_elpased: 2.203
batch start
#iterations: 49
currently lose_sum: 92.23191624879837
time_elpased: 2.218
batch start
#iterations: 50
currently lose_sum: 91.77679824829102
time_elpased: 2.209
batch start
#iterations: 51
currently lose_sum: 91.8006157875061
time_elpased: 2.23
batch start
#iterations: 52
currently lose_sum: 92.057626247406
time_elpased: 2.217
batch start
#iterations: 53
currently lose_sum: 91.35183781385422
time_elpased: 2.178
batch start
#iterations: 54
currently lose_sum: 91.61905634403229
time_elpased: 2.205
batch start
#iterations: 55
currently lose_sum: 91.9614343047142
time_elpased: 2.232
batch start
#iterations: 56
currently lose_sum: 91.28786706924438
time_elpased: 2.173
batch start
#iterations: 57
currently lose_sum: 91.34650760889053
time_elpased: 2.185
batch start
#iterations: 58
currently lose_sum: 91.26609921455383
time_elpased: 2.19
batch start
#iterations: 59
currently lose_sum: 91.88401687145233
time_elpased: 2.22
start validation test
0.656134020619
0.652239104358
0.671503550479
0.661731149536
0.656107037038
59.006
batch start
#iterations: 60
currently lose_sum: 91.70269179344177
time_elpased: 2.185
batch start
#iterations: 61
currently lose_sum: 91.21462118625641
time_elpased: 2.208
batch start
#iterations: 62
currently lose_sum: 91.09830272197723
time_elpased: 2.208
batch start
#iterations: 63
currently lose_sum: 90.90366953611374
time_elpased: 2.217
batch start
#iterations: 64
currently lose_sum: 91.15959846973419
time_elpased: 2.193
batch start
#iterations: 65
currently lose_sum: 90.7836241722107
time_elpased: 2.156
batch start
#iterations: 66
currently lose_sum: 90.78336614370346
time_elpased: 2.238
batch start
#iterations: 67
currently lose_sum: 90.81735610961914
time_elpased: 2.24
batch start
#iterations: 68
currently lose_sum: 90.57929879426956
time_elpased: 2.2
batch start
#iterations: 69
currently lose_sum: 90.83657151460648
time_elpased: 2.196
batch start
#iterations: 70
currently lose_sum: 90.68179506063461
time_elpased: 2.186
batch start
#iterations: 71
currently lose_sum: 90.30758947134018
time_elpased: 2.153
batch start
#iterations: 72
currently lose_sum: 90.20649123191833
time_elpased: 2.189
batch start
#iterations: 73
currently lose_sum: 89.91821873188019
time_elpased: 2.195
batch start
#iterations: 74
currently lose_sum: 89.8655361533165
time_elpased: 2.174
batch start
#iterations: 75
currently lose_sum: 90.80550426244736
time_elpased: 2.188
batch start
#iterations: 76
currently lose_sum: 90.0160540342331
time_elpased: 2.206
batch start
#iterations: 77
currently lose_sum: 90.21775555610657
time_elpased: 2.221
batch start
#iterations: 78
currently lose_sum: 90.29561561346054
time_elpased: 2.192
batch start
#iterations: 79
currently lose_sum: 89.9769777059555
time_elpased: 2.177
start validation test
0.630721649485
0.62435460302
0.659565709581
0.641477329597
0.630671009288
60.825
batch start
#iterations: 80
currently lose_sum: 90.07494616508484
time_elpased: 2.198
batch start
#iterations: 81
currently lose_sum: 89.83101999759674
time_elpased: 2.197
batch start
#iterations: 82
currently lose_sum: 90.456467628479
time_elpased: 2.205
batch start
#iterations: 83
currently lose_sum: 89.46223431825638
time_elpased: 2.209
batch start
#iterations: 84
currently lose_sum: 90.07379245758057
time_elpased: 2.219
batch start
#iterations: 85
currently lose_sum: 89.77171248197556
time_elpased: 2.216
batch start
#iterations: 86
currently lose_sum: 89.88108545541763
time_elpased: 2.211
batch start
#iterations: 87
currently lose_sum: 89.05489027500153
time_elpased: 2.188
batch start
#iterations: 88
currently lose_sum: 89.00618255138397
time_elpased: 2.273
batch start
#iterations: 89
currently lose_sum: 89.59859001636505
time_elpased: 2.188
batch start
#iterations: 90
currently lose_sum: 88.82945013046265
time_elpased: 2.221
batch start
#iterations: 91
currently lose_sum: 89.02107661962509
time_elpased: 2.156
batch start
#iterations: 92
currently lose_sum: 88.88806337118149
time_elpased: 2.178
batch start
#iterations: 93
currently lose_sum: 88.75979292392731
time_elpased: 2.185
batch start
#iterations: 94
currently lose_sum: 88.95163369178772
time_elpased: 2.206
batch start
#iterations: 95
currently lose_sum: 88.46210706233978
time_elpased: 2.184
batch start
#iterations: 96
currently lose_sum: 88.39035052061081
time_elpased: 2.212
batch start
#iterations: 97
currently lose_sum: 88.46260166168213
time_elpased: 2.216
batch start
#iterations: 98
currently lose_sum: 88.37922406196594
time_elpased: 2.218
batch start
#iterations: 99
currently lose_sum: 88.5556515455246
time_elpased: 2.204
start validation test
0.658762886598
0.63904103439
0.732427704024
0.682554905534
0.658633556649
58.950
batch start
#iterations: 100
currently lose_sum: 88.28679150342941
time_elpased: 2.207
batch start
#iterations: 101
currently lose_sum: 87.93603086471558
time_elpased: 2.217
batch start
#iterations: 102
currently lose_sum: 87.8444139957428
time_elpased: 2.197
batch start
#iterations: 103
currently lose_sum: 87.75976067781448
time_elpased: 2.268
batch start
#iterations: 104
currently lose_sum: 87.85571855306625
time_elpased: 2.213
batch start
#iterations: 105
currently lose_sum: 87.68881750106812
time_elpased: 2.229
batch start
#iterations: 106
currently lose_sum: 87.78685390949249
time_elpased: 2.274
batch start
#iterations: 107
currently lose_sum: 87.51441299915314
time_elpased: 2.2
batch start
#iterations: 108
currently lose_sum: 88.00794142484665
time_elpased: 2.198
batch start
#iterations: 109
currently lose_sum: 87.55916231870651
time_elpased: 2.189
batch start
#iterations: 110
currently lose_sum: 86.76611465215683
time_elpased: 2.204
batch start
#iterations: 111
currently lose_sum: 86.94273418188095
time_elpased: 2.21
batch start
#iterations: 112
currently lose_sum: 86.87770634889603
time_elpased: 2.193
batch start
#iterations: 113
currently lose_sum: 86.69251149892807
time_elpased: 2.225
batch start
#iterations: 114
currently lose_sum: 86.7276423573494
time_elpased: 2.209
batch start
#iterations: 115
currently lose_sum: 87.09182471036911
time_elpased: 2.157
batch start
#iterations: 116
currently lose_sum: 86.93901163339615
time_elpased: 2.219
batch start
#iterations: 117
currently lose_sum: 86.8952386379242
time_elpased: 2.164
batch start
#iterations: 118
currently lose_sum: 87.13798242807388
time_elpased: 2.162
batch start
#iterations: 119
currently lose_sum: 85.76676106452942
time_elpased: 2.189
start validation test
0.651958762887
0.653834180762
0.648451168056
0.651131549034
0.65196492101
59.777
batch start
#iterations: 120
currently lose_sum: 86.38230621814728
time_elpased: 2.244
batch start
#iterations: 121
currently lose_sum: 86.62479281425476
time_elpased: 2.201
batch start
#iterations: 122
currently lose_sum: 86.1563892364502
time_elpased: 2.17
batch start
#iterations: 123
currently lose_sum: 86.32222139835358
time_elpased: 2.172
batch start
#iterations: 124
currently lose_sum: 86.37122750282288
time_elpased: 2.193
batch start
#iterations: 125
currently lose_sum: 85.20274102687836
time_elpased: 2.239
batch start
#iterations: 126
currently lose_sum: 85.47980111837387
time_elpased: 2.175
batch start
#iterations: 127
currently lose_sum: 85.29904752969742
time_elpased: 2.186
batch start
#iterations: 128
currently lose_sum: 85.1789202094078
time_elpased: 2.22
batch start
#iterations: 129
currently lose_sum: 85.58547693490982
time_elpased: 2.244
batch start
#iterations: 130
currently lose_sum: 85.24943625926971
time_elpased: 2.225
batch start
#iterations: 131
currently lose_sum: 84.8718690276146
time_elpased: 2.212
batch start
#iterations: 132
currently lose_sum: 85.33914202451706
time_elpased: 2.2
batch start
#iterations: 133
currently lose_sum: 84.84440392255783
time_elpased: 2.211
batch start
#iterations: 134
currently lose_sum: 85.10633826255798
time_elpased: 2.179
batch start
#iterations: 135
currently lose_sum: 85.3617132306099
time_elpased: 2.176
batch start
#iterations: 136
currently lose_sum: 84.8895161151886
time_elpased: 2.2
batch start
#iterations: 137
currently lose_sum: 84.0680850148201
time_elpased: 2.213
batch start
#iterations: 138
currently lose_sum: 83.76654633879662
time_elpased: 2.204
batch start
#iterations: 139
currently lose_sum: 83.77225163578987
time_elpased: 2.188
start validation test
0.648969072165
0.651643192488
0.642790984872
0.647186820019
0.64897991875
60.913
batch start
#iterations: 140
currently lose_sum: 84.094362616539
time_elpased: 2.162
batch start
#iterations: 141
currently lose_sum: 83.9235343337059
time_elpased: 2.198
batch start
#iterations: 142
currently lose_sum: 83.90611523389816
time_elpased: 2.201
batch start
#iterations: 143
currently lose_sum: 83.21430939435959
time_elpased: 2.19
batch start
#iterations: 144
currently lose_sum: 83.99273389577866
time_elpased: 2.189
batch start
#iterations: 145
currently lose_sum: 83.42838224768639
time_elpased: 2.252
batch start
#iterations: 146
currently lose_sum: 83.68479496240616
time_elpased: 2.192
batch start
#iterations: 147
currently lose_sum: 82.95842570066452
time_elpased: 2.226
batch start
#iterations: 148
currently lose_sum: 82.86378467082977
time_elpased: 2.227
batch start
#iterations: 149
currently lose_sum: 83.01919230818748
time_elpased: 2.244
batch start
#iterations: 150
currently lose_sum: 82.43478292226791
time_elpased: 2.227
batch start
#iterations: 151
currently lose_sum: 82.81821775436401
time_elpased: 2.22
batch start
#iterations: 152
currently lose_sum: 81.61009562015533
time_elpased: 2.215
batch start
#iterations: 153
currently lose_sum: 82.4209852218628
time_elpased: 2.177
batch start
#iterations: 154
currently lose_sum: 81.49091130495071
time_elpased: 2.203
batch start
#iterations: 155
currently lose_sum: 81.90312185883522
time_elpased: 2.222
batch start
#iterations: 156
currently lose_sum: 82.02373132109642
time_elpased: 2.227
batch start
#iterations: 157
currently lose_sum: 81.23977160453796
time_elpased: 2.229
batch start
#iterations: 158
currently lose_sum: 82.28813052177429
time_elpased: 2.188
batch start
#iterations: 159
currently lose_sum: 81.44244742393494
time_elpased: 2.212
start validation test
0.622680412371
0.652383979657
0.528043634867
0.583665112046
0.622846561831
65.118
batch start
#iterations: 160
currently lose_sum: 80.49961477518082
time_elpased: 2.183
batch start
#iterations: 161
currently lose_sum: 80.64965608716011
time_elpased: 2.199
batch start
#iterations: 162
currently lose_sum: 80.33534687757492
time_elpased: 2.248
batch start
#iterations: 163
currently lose_sum: 80.12052339315414
time_elpased: 2.23
batch start
#iterations: 164
currently lose_sum: 80.05985078215599
time_elpased: 2.166
batch start
#iterations: 165
currently lose_sum: 79.76601773500443
time_elpased: 2.208
batch start
#iterations: 166
currently lose_sum: 78.85201650857925
time_elpased: 2.241
batch start
#iterations: 167
currently lose_sum: 79.27135762572289
time_elpased: 2.191
batch start
#iterations: 168
currently lose_sum: 79.34532803297043
time_elpased: 2.227
batch start
#iterations: 169
currently lose_sum: 78.72031751275063
time_elpased: 2.17
batch start
#iterations: 170
currently lose_sum: 78.25179994106293
time_elpased: 2.219
batch start
#iterations: 171
currently lose_sum: 78.13212931156158
time_elpased: 2.225
batch start
#iterations: 172
currently lose_sum: 78.81552249193192
time_elpased: 2.221
batch start
#iterations: 173
currently lose_sum: 77.77397584915161
time_elpased: 2.223
batch start
#iterations: 174
currently lose_sum: 77.98671689629555
time_elpased: 2.229
batch start
#iterations: 175
currently lose_sum: 77.04696017503738
time_elpased: 2.182
batch start
#iterations: 176
currently lose_sum: 77.15203130245209
time_elpased: 2.203
batch start
#iterations: 177
currently lose_sum: 76.90544047951698
time_elpased: 2.198
batch start
#iterations: 178
currently lose_sum: 76.90004125237465
time_elpased: 2.219
batch start
#iterations: 179
currently lose_sum: 76.25122624635696
time_elpased: 2.199
start validation test
0.616340206186
0.645210727969
0.519913553566
0.575824927338
0.616509498047
69.630
batch start
#iterations: 180
currently lose_sum: 76.46463778614998
time_elpased: 2.179
batch start
#iterations: 181
currently lose_sum: 75.95787778496742
time_elpased: 2.199
batch start
#iterations: 182
currently lose_sum: 75.94138863682747
time_elpased: 2.207
batch start
#iterations: 183
currently lose_sum: 76.03693413734436
time_elpased: 2.234
batch start
#iterations: 184
currently lose_sum: 74.57004281878471
time_elpased: 2.187
batch start
#iterations: 185
currently lose_sum: 74.96878579258919
time_elpased: 2.21
batch start
#iterations: 186
currently lose_sum: 74.09670230746269
time_elpased: 2.189
batch start
#iterations: 187
currently lose_sum: 73.756317704916
time_elpased: 2.173
batch start
#iterations: 188
currently lose_sum: 73.63456839323044
time_elpased: 2.209
batch start
#iterations: 189
currently lose_sum: 73.6047811806202
time_elpased: 2.252
batch start
#iterations: 190
currently lose_sum: 73.79717367887497
time_elpased: 2.221
batch start
#iterations: 191
currently lose_sum: 73.21355098485947
time_elpased: 2.258
batch start
#iterations: 192
currently lose_sum: 72.63089990615845
time_elpased: 2.248
batch start
#iterations: 193
currently lose_sum: 71.97845461964607
time_elpased: 2.208
batch start
#iterations: 194
currently lose_sum: 72.12008705735207
time_elpased: 2.245
batch start
#iterations: 195
currently lose_sum: 71.68253511190414
time_elpased: 2.203
batch start
#iterations: 196
currently lose_sum: 70.89223474264145
time_elpased: 2.228
batch start
#iterations: 197
currently lose_sum: 71.03134453296661
time_elpased: 2.194
batch start
#iterations: 198
currently lose_sum: 71.04329019784927
time_elpased: 2.172
batch start
#iterations: 199
currently lose_sum: 71.06466767191887
time_elpased: 2.218
start validation test
0.623969072165
0.653524340771
0.530513532983
0.585629082647
0.624133147779
76.410
batch start
#iterations: 200
currently lose_sum: 70.2886500954628
time_elpased: 2.192
batch start
#iterations: 201
currently lose_sum: 69.24097710847855
time_elpased: 2.191
batch start
#iterations: 202
currently lose_sum: 68.98684763908386
time_elpased: 2.276
batch start
#iterations: 203
currently lose_sum: 69.09054094552994
time_elpased: 2.202
batch start
#iterations: 204
currently lose_sum: 68.48487138748169
time_elpased: 2.242
batch start
#iterations: 205
currently lose_sum: 66.96551075577736
time_elpased: 2.216
batch start
#iterations: 206
currently lose_sum: 68.4231863617897
time_elpased: 2.27
batch start
#iterations: 207
currently lose_sum: 67.45544031262398
time_elpased: 2.22
batch start
#iterations: 208
currently lose_sum: 66.71662068367004
time_elpased: 2.183
batch start
#iterations: 209
currently lose_sum: 66.28968378901482
time_elpased: 2.232
batch start
#iterations: 210
currently lose_sum: 65.7976505458355
time_elpased: 2.239
batch start
#iterations: 211
currently lose_sum: 66.70443841814995
time_elpased: 2.24
batch start
#iterations: 212
currently lose_sum: 65.69498723745346
time_elpased: 2.221
batch start
#iterations: 213
currently lose_sum: 64.5429807305336
time_elpased: 2.21
batch start
#iterations: 214
currently lose_sum: 64.32148736715317
time_elpased: 2.188
batch start
#iterations: 215
currently lose_sum: 63.63777756690979
time_elpased: 2.197
batch start
#iterations: 216
currently lose_sum: 63.827965408563614
time_elpased: 2.231
batch start
#iterations: 217
currently lose_sum: 63.73193025588989
time_elpased: 2.257
batch start
#iterations: 218
currently lose_sum: 63.049513787031174
time_elpased: 2.232
batch start
#iterations: 219
currently lose_sum: 62.11534312367439
time_elpased: 2.257
start validation test
0.587216494845
0.648582855156
0.383863332304
0.482286009827
0.587573512687
91.693
batch start
#iterations: 220
currently lose_sum: 61.877171248197556
time_elpased: 2.234
batch start
#iterations: 221
currently lose_sum: 61.761031061410904
time_elpased: 2.309
batch start
#iterations: 222
currently lose_sum: 60.91131857037544
time_elpased: 2.212
batch start
#iterations: 223
currently lose_sum: 60.28983989357948
time_elpased: 2.206
batch start
#iterations: 224
currently lose_sum: 60.23818078637123
time_elpased: 2.228
batch start
#iterations: 225
currently lose_sum: 60.61260998249054
time_elpased: 2.207
batch start
#iterations: 226
currently lose_sum: 59.48639518022537
time_elpased: 2.186
batch start
#iterations: 227
currently lose_sum: 59.75848090648651
time_elpased: 2.19
batch start
#iterations: 228
currently lose_sum: 59.466859102249146
time_elpased: 2.225
batch start
#iterations: 229
currently lose_sum: 57.681764632463455
time_elpased: 2.2
batch start
#iterations: 230
currently lose_sum: 57.77247294783592
time_elpased: 2.189
batch start
#iterations: 231
currently lose_sum: 57.82846775650978
time_elpased: 2.231
batch start
#iterations: 232
currently lose_sum: 56.79179549217224
time_elpased: 2.213
batch start
#iterations: 233
currently lose_sum: 56.32536739110947
time_elpased: 2.202
batch start
#iterations: 234
currently lose_sum: 55.26153966784477
time_elpased: 2.209
batch start
#iterations: 235
currently lose_sum: 55.57535961270332
time_elpased: 2.267
batch start
#iterations: 236
currently lose_sum: 55.64128455519676
time_elpased: 2.193
batch start
#iterations: 237
currently lose_sum: 54.35687446594238
time_elpased: 2.264
batch start
#iterations: 238
currently lose_sum: 54.80487710237503
time_elpased: 2.246
batch start
#iterations: 239
currently lose_sum: 54.56392738223076
time_elpased: 2.196
start validation test
0.57793814433
0.646765214053
0.346711948132
0.451427040064
0.578344097582
107.427
batch start
#iterations: 240
currently lose_sum: 53.50200468301773
time_elpased: 2.202
batch start
#iterations: 241
currently lose_sum: 53.41711139678955
time_elpased: 2.185
batch start
#iterations: 242
currently lose_sum: 52.94838306307793
time_elpased: 2.234
batch start
#iterations: 243
currently lose_sum: 52.01537963747978
time_elpased: 2.191
batch start
#iterations: 244
currently lose_sum: 51.58873608708382
time_elpased: 2.198
batch start
#iterations: 245
currently lose_sum: 50.60218659043312
time_elpased: 2.198
batch start
#iterations: 246
currently lose_sum: 50.3951113820076
time_elpased: 2.213
batch start
#iterations: 247
currently lose_sum: 50.06479063630104
time_elpased: 2.288
batch start
#iterations: 248
currently lose_sum: 49.94667564332485
time_elpased: 2.187
batch start
#iterations: 249
currently lose_sum: 49.077557384967804
time_elpased: 2.244
batch start
#iterations: 250
currently lose_sum: 48.7714853733778
time_elpased: 2.217
batch start
#iterations: 251
currently lose_sum: 48.77882939577103
time_elpased: 2.196
batch start
#iterations: 252
currently lose_sum: 47.55608932673931
time_elpased: 2.212
batch start
#iterations: 253
currently lose_sum: 47.5838987082243
time_elpased: 2.213
batch start
#iterations: 254
currently lose_sum: 47.98981982469559
time_elpased: 2.183
batch start
#iterations: 255
currently lose_sum: 46.49145798385143
time_elpased: 2.257
batch start
#iterations: 256
currently lose_sum: 46.601673632860184
time_elpased: 2.217
batch start
#iterations: 257
currently lose_sum: 45.726532101631165
time_elpased: 2.246
batch start
#iterations: 258
currently lose_sum: 45.3301247805357
time_elpased: 2.221
batch start
#iterations: 259
currently lose_sum: 44.59688474237919
time_elpased: 2.194
start validation test
0.562113402062
0.659031754295
0.260574251312
0.373478870123
0.56264280055
137.148
batch start
#iterations: 260
currently lose_sum: 46.06334066390991
time_elpased: 2.217
batch start
#iterations: 261
currently lose_sum: 44.50648374855518
time_elpased: 2.213
batch start
#iterations: 262
currently lose_sum: 43.516108959913254
time_elpased: 2.287
batch start
#iterations: 263
currently lose_sum: 44.05455107986927
time_elpased: 2.221
batch start
#iterations: 264
currently lose_sum: 42.999301970005035
time_elpased: 2.263
batch start
#iterations: 265
currently lose_sum: 42.78479404747486
time_elpased: 2.237
batch start
#iterations: 266
currently lose_sum: 42.25347928702831
time_elpased: 2.196
batch start
#iterations: 267
currently lose_sum: 42.615721717476845
time_elpased: 2.223
batch start
#iterations: 268
currently lose_sum: 43.15189932286739
time_elpased: 2.244
batch start
#iterations: 269
currently lose_sum: 41.472780138254166
time_elpased: 2.191
batch start
#iterations: 270
currently lose_sum: 41.719439297914505
time_elpased: 2.181
batch start
#iterations: 271
currently lose_sum: 41.89766673743725
time_elpased: 2.206
batch start
#iterations: 272
currently lose_sum: 41.25580871105194
time_elpased: 2.221
batch start
#iterations: 273
currently lose_sum: 39.43253310024738
time_elpased: 2.244
batch start
#iterations: 274
currently lose_sum: 38.44132047891617
time_elpased: 2.22
batch start
#iterations: 275
currently lose_sum: 38.60361260175705
time_elpased: 2.221
batch start
#iterations: 276
currently lose_sum: 39.13484935462475
time_elpased: 2.233
batch start
#iterations: 277
currently lose_sum: 38.12994010746479
time_elpased: 2.198
batch start
#iterations: 278
currently lose_sum: 39.05228208005428
time_elpased: 2.224
batch start
#iterations: 279
currently lose_sum: 37.2004184871912
time_elpased: 2.194
start validation test
0.544020618557
0.645603477098
0.198723885973
0.303903053195
0.544626840229
164.448
batch start
#iterations: 280
currently lose_sum: 36.296128273010254
time_elpased: 2.175
batch start
#iterations: 281
currently lose_sum: 37.51000899076462
time_elpased: 2.201
batch start
#iterations: 282
currently lose_sum: 36.21115440130234
time_elpased: 2.211
batch start
#iterations: 283
currently lose_sum: 36.12438400089741
time_elpased: 2.268
batch start
#iterations: 284
currently lose_sum: 36.076409578323364
time_elpased: 2.232
batch start
#iterations: 285
currently lose_sum: 35.7200445830822
time_elpased: 2.213
batch start
#iterations: 286
currently lose_sum: 35.55120728909969
time_elpased: 2.223
batch start
#iterations: 287
currently lose_sum: 34.98994034528732
time_elpased: 2.206
batch start
#iterations: 288
currently lose_sum: 34.23219498991966
time_elpased: 2.206
batch start
#iterations: 289
currently lose_sum: 34.441621482372284
time_elpased: 2.209
batch start
#iterations: 290
currently lose_sum: 33.91356611251831
time_elpased: 2.241
batch start
#iterations: 291
currently lose_sum: 33.90925379097462
time_elpased: 2.2
batch start
#iterations: 292
currently lose_sum: nan
time_elpased: 2.251
train finish final lose is: nan
acc: 0.651
pre: 0.631
rec: 0.731
F1: 0.677
auc: 0.651
