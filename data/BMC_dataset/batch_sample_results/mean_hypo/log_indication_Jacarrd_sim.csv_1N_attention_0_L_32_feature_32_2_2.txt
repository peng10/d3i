start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 99.95886492729187
time_elpased: 1.41
batch start
#iterations: 1
currently lose_sum: 99.67246371507645
time_elpased: 1.353
batch start
#iterations: 2
currently lose_sum: 99.66662752628326
time_elpased: 1.365
batch start
#iterations: 3
currently lose_sum: 99.1416557431221
time_elpased: 1.38
batch start
#iterations: 4
currently lose_sum: 99.0900827050209
time_elpased: 1.398
batch start
#iterations: 5
currently lose_sum: 99.09250605106354
time_elpased: 1.389
batch start
#iterations: 6
currently lose_sum: 98.85507082939148
time_elpased: 1.358
batch start
#iterations: 7
currently lose_sum: 98.70717877149582
time_elpased: 1.396
batch start
#iterations: 8
currently lose_sum: 98.54188078641891
time_elpased: 1.399
batch start
#iterations: 9
currently lose_sum: 98.43669563531876
time_elpased: 1.376
batch start
#iterations: 10
currently lose_sum: 97.90156310796738
time_elpased: 1.369
batch start
#iterations: 11
currently lose_sum: 97.74233704805374
time_elpased: 1.364
batch start
#iterations: 12
currently lose_sum: 97.41961246728897
time_elpased: 1.353
batch start
#iterations: 13
currently lose_sum: 97.69949549436569
time_elpased: 1.365
batch start
#iterations: 14
currently lose_sum: 96.68202102184296
time_elpased: 1.451
batch start
#iterations: 15
currently lose_sum: 96.76064395904541
time_elpased: 1.381
batch start
#iterations: 16
currently lose_sum: 96.39511227607727
time_elpased: 1.386
batch start
#iterations: 17
currently lose_sum: 96.5879043340683
time_elpased: 1.42
batch start
#iterations: 18
currently lose_sum: 96.32227313518524
time_elpased: 1.367
batch start
#iterations: 19
currently lose_sum: 96.03542357683182
time_elpased: 1.346
start validation test
0.652680412371
0.623970037453
0.771534424205
0.6899503037
0.652471745821
61.632
batch start
#iterations: 20
currently lose_sum: 96.07442343235016
time_elpased: 1.386
batch start
#iterations: 21
currently lose_sum: 95.73816454410553
time_elpased: 1.346
batch start
#iterations: 22
currently lose_sum: 95.76594680547714
time_elpased: 1.369
batch start
#iterations: 23
currently lose_sum: 95.00519663095474
time_elpased: 1.378
batch start
#iterations: 24
currently lose_sum: 95.19478672742844
time_elpased: 1.362
batch start
#iterations: 25
currently lose_sum: 95.19899863004684
time_elpased: 1.382
batch start
#iterations: 26
currently lose_sum: 95.48247462511063
time_elpased: 1.355
batch start
#iterations: 27
currently lose_sum: 95.14686518907547
time_elpased: 1.339
batch start
#iterations: 28
currently lose_sum: 94.99148082733154
time_elpased: 1.36
batch start
#iterations: 29
currently lose_sum: 95.07954847812653
time_elpased: 1.39
batch start
#iterations: 30
currently lose_sum: 95.39798998832703
time_elpased: 1.38
batch start
#iterations: 31
currently lose_sum: 94.35409688949585
time_elpased: 1.355
batch start
#iterations: 32
currently lose_sum: 94.3614501953125
time_elpased: 1.38
batch start
#iterations: 33
currently lose_sum: 94.33781605958939
time_elpased: 1.365
batch start
#iterations: 34
currently lose_sum: 94.65995806455612
time_elpased: 1.372
batch start
#iterations: 35
currently lose_sum: 94.09030777215958
time_elpased: 1.345
batch start
#iterations: 36
currently lose_sum: 94.43235456943512
time_elpased: 1.373
batch start
#iterations: 37
currently lose_sum: 93.94370996952057
time_elpased: 1.358
batch start
#iterations: 38
currently lose_sum: 94.13328266143799
time_elpased: 1.375
batch start
#iterations: 39
currently lose_sum: 93.81751602888107
time_elpased: 1.355
start validation test
0.666649484536
0.652125070212
0.716887928373
0.68297465562
0.666561283199
59.662
batch start
#iterations: 40
currently lose_sum: 94.24208748340607
time_elpased: 1.38
batch start
#iterations: 41
currently lose_sum: 93.94724804162979
time_elpased: 1.346
batch start
#iterations: 42
currently lose_sum: 93.43294113874435
time_elpased: 1.36
batch start
#iterations: 43
currently lose_sum: 93.57224118709564
time_elpased: 1.343
batch start
#iterations: 44
currently lose_sum: 93.82602608203888
time_elpased: 1.399
batch start
#iterations: 45
currently lose_sum: 93.40834712982178
time_elpased: 1.363
batch start
#iterations: 46
currently lose_sum: 93.21577543020248
time_elpased: 1.393
batch start
#iterations: 47
currently lose_sum: 92.97947061061859
time_elpased: 1.368
batch start
#iterations: 48
currently lose_sum: 93.59033751487732
time_elpased: 1.357
batch start
#iterations: 49
currently lose_sum: 93.35591977834702
time_elpased: 1.366
batch start
#iterations: 50
currently lose_sum: 93.33909183740616
time_elpased: 1.409
batch start
#iterations: 51
currently lose_sum: 93.14251720905304
time_elpased: 1.344
batch start
#iterations: 52
currently lose_sum: 92.99724864959717
time_elpased: 1.355
batch start
#iterations: 53
currently lose_sum: 92.86229920387268
time_elpased: 1.361
batch start
#iterations: 54
currently lose_sum: 93.20092594623566
time_elpased: 1.364
batch start
#iterations: 55
currently lose_sum: 92.85462880134583
time_elpased: 1.406
batch start
#iterations: 56
currently lose_sum: 93.19210159778595
time_elpased: 1.366
batch start
#iterations: 57
currently lose_sum: 93.03875988721848
time_elpased: 1.353
batch start
#iterations: 58
currently lose_sum: 92.76698452234268
time_elpased: 1.363
batch start
#iterations: 59
currently lose_sum: 92.60557651519775
time_elpased: 1.373
start validation test
0.66324742268
0.644753591562
0.729751981064
0.684624668115
0.663130663671
59.491
batch start
#iterations: 60
currently lose_sum: 92.48152959346771
time_elpased: 1.37
batch start
#iterations: 61
currently lose_sum: 92.9387099146843
time_elpased: 1.377
batch start
#iterations: 62
currently lose_sum: 92.57818412780762
time_elpased: 1.363
batch start
#iterations: 63
currently lose_sum: 92.57375746965408
time_elpased: 1.407
batch start
#iterations: 64
currently lose_sum: 92.7500792145729
time_elpased: 1.359
batch start
#iterations: 65
currently lose_sum: 92.56005543470383
time_elpased: 1.358
batch start
#iterations: 66
currently lose_sum: 92.10742563009262
time_elpased: 1.387
batch start
#iterations: 67
currently lose_sum: 91.98153865337372
time_elpased: 1.356
batch start
#iterations: 68
currently lose_sum: 92.15876388549805
time_elpased: 1.386
batch start
#iterations: 69
currently lose_sum: 91.66186648607254
time_elpased: 1.373
batch start
#iterations: 70
currently lose_sum: 91.85653781890869
time_elpased: 1.369
batch start
#iterations: 71
currently lose_sum: 91.88314044475555
time_elpased: 1.358
batch start
#iterations: 72
currently lose_sum: 92.03692823648453
time_elpased: 1.353
batch start
#iterations: 73
currently lose_sum: 92.0096263885498
time_elpased: 1.366
batch start
#iterations: 74
currently lose_sum: 91.71054494380951
time_elpased: 1.416
batch start
#iterations: 75
currently lose_sum: 91.71438008546829
time_elpased: 1.378
batch start
#iterations: 76
currently lose_sum: 91.90394753217697
time_elpased: 1.381
batch start
#iterations: 77
currently lose_sum: 91.91799688339233
time_elpased: 1.371
batch start
#iterations: 78
currently lose_sum: 91.54544430971146
time_elpased: 1.359
batch start
#iterations: 79
currently lose_sum: 92.44417196512222
time_elpased: 1.408
start validation test
0.672783505155
0.653233876103
0.739014099002
0.693481409947
0.672667227132
58.657
batch start
#iterations: 80
currently lose_sum: 92.09248131513596
time_elpased: 1.38
batch start
#iterations: 81
currently lose_sum: 91.61033600568771
time_elpased: 1.375
batch start
#iterations: 82
currently lose_sum: 91.87753701210022
time_elpased: 1.375
batch start
#iterations: 83
currently lose_sum: 91.60629695653915
time_elpased: 1.383
batch start
#iterations: 84
currently lose_sum: 91.77613967657089
time_elpased: 1.421
batch start
#iterations: 85
currently lose_sum: 91.55987000465393
time_elpased: 1.371
batch start
#iterations: 86
currently lose_sum: 91.30156326293945
time_elpased: 1.348
batch start
#iterations: 87
currently lose_sum: 91.26710969209671
time_elpased: 1.382
batch start
#iterations: 88
currently lose_sum: 91.42393618822098
time_elpased: 1.338
batch start
#iterations: 89
currently lose_sum: 91.55855429172516
time_elpased: 1.356
batch start
#iterations: 90
currently lose_sum: 91.76879942417145
time_elpased: 1.396
batch start
#iterations: 91
currently lose_sum: 91.30743908882141
time_elpased: 1.393
batch start
#iterations: 92
currently lose_sum: 91.28183913230896
time_elpased: 1.38
batch start
#iterations: 93
currently lose_sum: 90.86554354429245
time_elpased: 1.367
batch start
#iterations: 94
currently lose_sum: 91.19825035333633
time_elpased: 1.41
batch start
#iterations: 95
currently lose_sum: 90.9009400010109
time_elpased: 1.377
batch start
#iterations: 96
currently lose_sum: 90.8560249209404
time_elpased: 1.379
batch start
#iterations: 97
currently lose_sum: 90.51084607839584
time_elpased: 1.363
batch start
#iterations: 98
currently lose_sum: 91.23354381322861
time_elpased: 1.361
batch start
#iterations: 99
currently lose_sum: 90.64493137598038
time_elpased: 1.378
start validation test
0.674742268041
0.673667040473
0.680045281465
0.676841134897
0.674732957783
58.018
batch start
#iterations: 100
currently lose_sum: 90.7696967124939
time_elpased: 1.375
batch start
#iterations: 101
currently lose_sum: 90.6610118150711
time_elpased: 1.354
batch start
#iterations: 102
currently lose_sum: 91.04742711782455
time_elpased: 1.399
batch start
#iterations: 103
currently lose_sum: 90.55650794506073
time_elpased: 1.37
batch start
#iterations: 104
currently lose_sum: 90.69776928424835
time_elpased: 1.35
batch start
#iterations: 105
currently lose_sum: 90.79072493314743
time_elpased: 1.367
batch start
#iterations: 106
currently lose_sum: 90.1262276172638
time_elpased: 1.366
batch start
#iterations: 107
currently lose_sum: 90.73101109266281
time_elpased: 1.355
batch start
#iterations: 108
currently lose_sum: 90.22895616292953
time_elpased: 1.376
batch start
#iterations: 109
currently lose_sum: 90.37827581167221
time_elpased: 1.368
batch start
#iterations: 110
currently lose_sum: 90.8172327876091
time_elpased: 1.397
batch start
#iterations: 111
currently lose_sum: 90.2556540966034
time_elpased: 1.357
batch start
#iterations: 112
currently lose_sum: 90.6095312833786
time_elpased: 1.389
batch start
#iterations: 113
currently lose_sum: 89.95749896764755
time_elpased: 1.364
batch start
#iterations: 114
currently lose_sum: 90.29463851451874
time_elpased: 1.386
batch start
#iterations: 115
currently lose_sum: 90.45852768421173
time_elpased: 1.381
batch start
#iterations: 116
currently lose_sum: 90.26463502645493
time_elpased: 1.395
batch start
#iterations: 117
currently lose_sum: 89.67131239175797
time_elpased: 1.396
batch start
#iterations: 118
currently lose_sum: 90.41564530134201
time_elpased: 1.373
batch start
#iterations: 119
currently lose_sum: 89.65971517562866
time_elpased: 1.402
start validation test
0.680979381443
0.675208581645
0.699598641556
0.687187263078
0.68094669246
57.586
batch start
#iterations: 120
currently lose_sum: 90.38672679662704
time_elpased: 1.35
batch start
#iterations: 121
currently lose_sum: 89.5573239326477
time_elpased: 1.393
batch start
#iterations: 122
currently lose_sum: 89.93640071153641
time_elpased: 1.384
batch start
#iterations: 123
currently lose_sum: 89.605120241642
time_elpased: 1.393
batch start
#iterations: 124
currently lose_sum: 90.10292446613312
time_elpased: 1.373
batch start
#iterations: 125
currently lose_sum: 90.11992305517197
time_elpased: 1.36
batch start
#iterations: 126
currently lose_sum: 90.21247225999832
time_elpased: 1.374
batch start
#iterations: 127
currently lose_sum: 89.99214798212051
time_elpased: 1.375
batch start
#iterations: 128
currently lose_sum: 90.06154406070709
time_elpased: 1.38
batch start
#iterations: 129
currently lose_sum: 89.95986324548721
time_elpased: 1.366
batch start
#iterations: 130
currently lose_sum: 90.01750761270523
time_elpased: 1.368
batch start
#iterations: 131
currently lose_sum: 90.28339993953705
time_elpased: 1.382
batch start
#iterations: 132
currently lose_sum: 89.85753041505814
time_elpased: 1.372
batch start
#iterations: 133
currently lose_sum: 89.46898555755615
time_elpased: 1.385
batch start
#iterations: 134
currently lose_sum: 89.55093777179718
time_elpased: 1.386
batch start
#iterations: 135
currently lose_sum: 89.9614075422287
time_elpased: 1.424
batch start
#iterations: 136
currently lose_sum: 89.51617139577866
time_elpased: 1.404
batch start
#iterations: 137
currently lose_sum: 89.66006201505661
time_elpased: 1.363
batch start
#iterations: 138
currently lose_sum: 89.38834273815155
time_elpased: 1.37
batch start
#iterations: 139
currently lose_sum: 89.05508762598038
time_elpased: 1.391
start validation test
0.679278350515
0.657078651685
0.752289801379
0.701468189233
0.679150167652
57.856
batch start
#iterations: 140
currently lose_sum: 89.24082428216934
time_elpased: 1.384
batch start
#iterations: 141
currently lose_sum: 89.81946569681168
time_elpased: 1.401
batch start
#iterations: 142
currently lose_sum: 89.43777352571487
time_elpased: 1.427
batch start
#iterations: 143
currently lose_sum: 89.32160753011703
time_elpased: 1.387
batch start
#iterations: 144
currently lose_sum: 89.18089365959167
time_elpased: 1.369
batch start
#iterations: 145
currently lose_sum: 89.4471788406372
time_elpased: 1.419
batch start
#iterations: 146
currently lose_sum: 89.41202610731125
time_elpased: 1.358
batch start
#iterations: 147
currently lose_sum: 89.0863066315651
time_elpased: 1.405
batch start
#iterations: 148
currently lose_sum: 89.33026367425919
time_elpased: 1.348
batch start
#iterations: 149
currently lose_sum: 89.37516242265701
time_elpased: 1.366
batch start
#iterations: 150
currently lose_sum: 89.34886407852173
time_elpased: 1.4
batch start
#iterations: 151
currently lose_sum: 89.2993032336235
time_elpased: 1.375
batch start
#iterations: 152
currently lose_sum: 88.96265745162964
time_elpased: 1.407
batch start
#iterations: 153
currently lose_sum: 89.27586227655411
time_elpased: 1.396
batch start
#iterations: 154
currently lose_sum: 89.19387298822403
time_elpased: 1.409
batch start
#iterations: 155
currently lose_sum: 88.61537444591522
time_elpased: 1.396
batch start
#iterations: 156
currently lose_sum: 89.01171481609344
time_elpased: 1.354
batch start
#iterations: 157
currently lose_sum: 88.67152327299118
time_elpased: 1.366
batch start
#iterations: 158
currently lose_sum: 88.9665960073471
time_elpased: 1.364
batch start
#iterations: 159
currently lose_sum: 88.49822241067886
time_elpased: 1.36
start validation test
0.670360824742
0.680151843818
0.64536379541
0.662301314886
0.670404710883
58.037
batch start
#iterations: 160
currently lose_sum: 88.52047598361969
time_elpased: 1.441
batch start
#iterations: 161
currently lose_sum: 88.87171423435211
time_elpased: 1.369
batch start
#iterations: 162
currently lose_sum: 89.14562165737152
time_elpased: 1.389
batch start
#iterations: 163
currently lose_sum: 89.04215866327286
time_elpased: 1.372
batch start
#iterations: 164
currently lose_sum: 88.70342177152634
time_elpased: 1.414
batch start
#iterations: 165
currently lose_sum: 88.14303958415985
time_elpased: 1.374
batch start
#iterations: 166
currently lose_sum: 88.72113513946533
time_elpased: 1.379
batch start
#iterations: 167
currently lose_sum: 88.85132730007172
time_elpased: 1.383
batch start
#iterations: 168
currently lose_sum: 88.69896161556244
time_elpased: 1.369
batch start
#iterations: 169
currently lose_sum: 88.63886207342148
time_elpased: 1.395
batch start
#iterations: 170
currently lose_sum: 88.9539560675621
time_elpased: 1.375
batch start
#iterations: 171
currently lose_sum: 89.03513073921204
time_elpased: 1.373
batch start
#iterations: 172
currently lose_sum: 88.74603015184402
time_elpased: 1.355
batch start
#iterations: 173
currently lose_sum: 88.86765080690384
time_elpased: 1.375
batch start
#iterations: 174
currently lose_sum: 88.652128636837
time_elpased: 1.359
batch start
#iterations: 175
currently lose_sum: 88.01529175043106
time_elpased: 1.366
batch start
#iterations: 176
currently lose_sum: 88.22851037979126
time_elpased: 1.359
batch start
#iterations: 177
currently lose_sum: 88.0823604464531
time_elpased: 1.373
batch start
#iterations: 178
currently lose_sum: 88.36553424596786
time_elpased: 1.378
batch start
#iterations: 179
currently lose_sum: 87.74163389205933
time_elpased: 1.42
start validation test
0.683144329897
0.674180327869
0.711021920346
0.692111194591
0.683095386487
57.218
batch start
#iterations: 180
currently lose_sum: 88.24567002058029
time_elpased: 1.361
batch start
#iterations: 181
currently lose_sum: 88.72690731287003
time_elpased: 1.372
batch start
#iterations: 182
currently lose_sum: 88.37027561664581
time_elpased: 1.364
batch start
#iterations: 183
currently lose_sum: 88.34449648857117
time_elpased: 1.392
batch start
#iterations: 184
currently lose_sum: 88.50137633085251
time_elpased: 1.336
batch start
#iterations: 185
currently lose_sum: 88.33377283811569
time_elpased: 1.376
batch start
#iterations: 186
currently lose_sum: 87.88328635692596
time_elpased: 1.385
batch start
#iterations: 187
currently lose_sum: 88.2267906665802
time_elpased: 1.42
batch start
#iterations: 188
currently lose_sum: 88.31853359937668
time_elpased: 1.392
batch start
#iterations: 189
currently lose_sum: 88.17115992307663
time_elpased: 1.364
batch start
#iterations: 190
currently lose_sum: 88.13375228643417
time_elpased: 1.375
batch start
#iterations: 191
currently lose_sum: 87.90696996450424
time_elpased: 1.373
batch start
#iterations: 192
currently lose_sum: 88.1990498304367
time_elpased: 1.359
batch start
#iterations: 193
currently lose_sum: 88.34820091724396
time_elpased: 1.374
batch start
#iterations: 194
currently lose_sum: 88.34300744533539
time_elpased: 1.367
batch start
#iterations: 195
currently lose_sum: 88.04076904058456
time_elpased: 1.392
batch start
#iterations: 196
currently lose_sum: 88.12704610824585
time_elpased: 1.375
batch start
#iterations: 197
currently lose_sum: 88.0994423031807
time_elpased: 1.394
batch start
#iterations: 198
currently lose_sum: 87.68480306863785
time_elpased: 1.364
batch start
#iterations: 199
currently lose_sum: 88.09983468055725
time_elpased: 1.441
start validation test
0.671494845361
0.679053330478
0.652567664917
0.665547100499
0.671528074945
57.892
batch start
#iterations: 200
currently lose_sum: 87.53622299432755
time_elpased: 1.364
batch start
#iterations: 201
currently lose_sum: 88.04131484031677
time_elpased: 1.415
batch start
#iterations: 202
currently lose_sum: 87.69028919935226
time_elpased: 1.36
batch start
#iterations: 203
currently lose_sum: 87.40185433626175
time_elpased: 1.384
batch start
#iterations: 204
currently lose_sum: 87.64483290910721
time_elpased: 1.37
batch start
#iterations: 205
currently lose_sum: 87.69593054056168
time_elpased: 1.421
batch start
#iterations: 206
currently lose_sum: 87.71592628955841
time_elpased: 1.356
batch start
#iterations: 207
currently lose_sum: 87.88164412975311
time_elpased: 1.41
batch start
#iterations: 208
currently lose_sum: 88.28390276432037
time_elpased: 1.393
batch start
#iterations: 209
currently lose_sum: 87.82233655452728
time_elpased: 1.37
batch start
#iterations: 210
currently lose_sum: 87.86564552783966
time_elpased: 1.404
batch start
#iterations: 211
currently lose_sum: 87.768181681633
time_elpased: 1.36
batch start
#iterations: 212
currently lose_sum: 87.68475317955017
time_elpased: 1.355
batch start
#iterations: 213
currently lose_sum: 87.12517142295837
time_elpased: 1.393
batch start
#iterations: 214
currently lose_sum: 87.53283298015594
time_elpased: 1.376
batch start
#iterations: 215
currently lose_sum: 87.14765226840973
time_elpased: 1.374
batch start
#iterations: 216
currently lose_sum: 87.11052000522614
time_elpased: 1.363
batch start
#iterations: 217
currently lose_sum: 87.64417827129364
time_elpased: 1.396
batch start
#iterations: 218
currently lose_sum: 87.47288370132446
time_elpased: 1.36
batch start
#iterations: 219
currently lose_sum: 86.87953525781631
time_elpased: 1.392
start validation test
0.666340206186
0.676419403959
0.640012349491
0.657712442494
0.666386428799
57.957
batch start
#iterations: 220
currently lose_sum: 87.67049789428711
time_elpased: 1.426
batch start
#iterations: 221
currently lose_sum: 87.0576524734497
time_elpased: 1.407
batch start
#iterations: 222
currently lose_sum: 86.80760282278061
time_elpased: 1.378
batch start
#iterations: 223
currently lose_sum: 87.10716241598129
time_elpased: 1.377
batch start
#iterations: 224
currently lose_sum: 87.00482732057571
time_elpased: 1.354
batch start
#iterations: 225
currently lose_sum: 87.5433229804039
time_elpased: 1.381
batch start
#iterations: 226
currently lose_sum: 87.03011429309845
time_elpased: 1.382
batch start
#iterations: 227
currently lose_sum: 86.98423671722412
time_elpased: 1.389
batch start
#iterations: 228
currently lose_sum: 87.43271452188492
time_elpased: 1.343
batch start
#iterations: 229
currently lose_sum: 87.47061961889267
time_elpased: 1.412
batch start
#iterations: 230
currently lose_sum: 87.04327774047852
time_elpased: 1.368
batch start
#iterations: 231
currently lose_sum: 87.0403722524643
time_elpased: 1.382
batch start
#iterations: 232
currently lose_sum: 87.03638750314713
time_elpased: 1.4
batch start
#iterations: 233
currently lose_sum: 87.45153045654297
time_elpased: 1.384
batch start
#iterations: 234
currently lose_sum: 87.19247937202454
time_elpased: 1.365
batch start
#iterations: 235
currently lose_sum: 86.85773265361786
time_elpased: 1.361
batch start
#iterations: 236
currently lose_sum: 86.82709121704102
time_elpased: 1.394
batch start
#iterations: 237
currently lose_sum: 87.62611645460129
time_elpased: 1.383
batch start
#iterations: 238
currently lose_sum: 86.85793280601501
time_elpased: 1.373
batch start
#iterations: 239
currently lose_sum: 87.40909099578857
time_elpased: 1.369
start validation test
0.663711340206
0.670293333333
0.64670165689
0.65828619317
0.663741203329
57.944
batch start
#iterations: 240
currently lose_sum: 86.19941192865372
time_elpased: 1.348
batch start
#iterations: 241
currently lose_sum: 86.98967295885086
time_elpased: 1.365
batch start
#iterations: 242
currently lose_sum: 86.92183405160904
time_elpased: 1.412
batch start
#iterations: 243
currently lose_sum: 86.71988201141357
time_elpased: 1.382
batch start
#iterations: 244
currently lose_sum: 87.00825971364975
time_elpased: 1.363
batch start
#iterations: 245
currently lose_sum: 86.83711296319962
time_elpased: 1.377
batch start
#iterations: 246
currently lose_sum: 86.84857833385468
time_elpased: 1.42
batch start
#iterations: 247
currently lose_sum: 86.35666739940643
time_elpased: 1.375
batch start
#iterations: 248
currently lose_sum: 86.78837496042252
time_elpased: 1.347
batch start
#iterations: 249
currently lose_sum: 86.48362439870834
time_elpased: 1.394
batch start
#iterations: 250
currently lose_sum: 86.18609702587128
time_elpased: 1.355
batch start
#iterations: 251
currently lose_sum: 86.30990010499954
time_elpased: 1.405
batch start
#iterations: 252
currently lose_sum: 86.81311547756195
time_elpased: 1.406
batch start
#iterations: 253
currently lose_sum: 86.10988938808441
time_elpased: 1.422
batch start
#iterations: 254
currently lose_sum: 86.33643537759781
time_elpased: 1.376
batch start
#iterations: 255
currently lose_sum: 86.45822155475616
time_elpased: 1.42
batch start
#iterations: 256
currently lose_sum: 87.00382196903229
time_elpased: 1.385
batch start
#iterations: 257
currently lose_sum: 86.02088069915771
time_elpased: 1.356
batch start
#iterations: 258
currently lose_sum: 86.49647772312164
time_elpased: 1.371
batch start
#iterations: 259
currently lose_sum: 87.01308876276016
time_elpased: 1.391
start validation test
0.665721649485
0.685832566697
0.613769682001
0.647803182534
0.665812859176
58.194
batch start
#iterations: 260
currently lose_sum: 86.02460104227066
time_elpased: 1.38
batch start
#iterations: 261
currently lose_sum: 86.31866580247879
time_elpased: 1.378
batch start
#iterations: 262
currently lose_sum: 85.86408996582031
time_elpased: 1.408
batch start
#iterations: 263
currently lose_sum: 86.82927334308624
time_elpased: 1.406
batch start
#iterations: 264
currently lose_sum: 86.49510908126831
time_elpased: 1.386
batch start
#iterations: 265
currently lose_sum: 85.93426513671875
time_elpased: 1.361
batch start
#iterations: 266
currently lose_sum: 86.21232694387436
time_elpased: 1.397
batch start
#iterations: 267
currently lose_sum: 85.87844491004944
time_elpased: 1.437
batch start
#iterations: 268
currently lose_sum: 85.9481491446495
time_elpased: 1.356
batch start
#iterations: 269
currently lose_sum: 86.2422548532486
time_elpased: 1.36
batch start
#iterations: 270
currently lose_sum: 86.042436003685
time_elpased: 1.348
batch start
#iterations: 271
currently lose_sum: 86.3484069108963
time_elpased: 1.388
batch start
#iterations: 272
currently lose_sum: 85.89415407180786
time_elpased: 1.376
batch start
#iterations: 273
currently lose_sum: 85.96357047557831
time_elpased: 1.371
batch start
#iterations: 274
currently lose_sum: 86.26062381267548
time_elpased: 1.366
batch start
#iterations: 275
currently lose_sum: 86.35655879974365
time_elpased: 1.378
batch start
#iterations: 276
currently lose_sum: 85.38392674922943
time_elpased: 1.375
batch start
#iterations: 277
currently lose_sum: 85.9624525308609
time_elpased: 1.395
batch start
#iterations: 278
currently lose_sum: 85.64442652463913
time_elpased: 1.417
batch start
#iterations: 279
currently lose_sum: 85.8785006403923
time_elpased: 1.379
start validation test
0.654020618557
0.675587238518
0.594936708861
0.632702199847
0.654124349473
58.707
batch start
#iterations: 280
currently lose_sum: 86.15253466367722
time_elpased: 1.382
batch start
#iterations: 281
currently lose_sum: 85.66900652647018
time_elpased: 1.384
batch start
#iterations: 282
currently lose_sum: 85.84415888786316
time_elpased: 1.375
batch start
#iterations: 283
currently lose_sum: 86.18892279267311
time_elpased: 1.376
batch start
#iterations: 284
currently lose_sum: 85.31803768873215
time_elpased: 1.369
batch start
#iterations: 285
currently lose_sum: 85.68376916646957
time_elpased: 1.399
batch start
#iterations: 286
currently lose_sum: 85.27822935581207
time_elpased: 1.349
batch start
#iterations: 287
currently lose_sum: 85.97410720586777
time_elpased: 1.417
batch start
#iterations: 288
currently lose_sum: 86.30986845493317
time_elpased: 1.365
batch start
#iterations: 289
currently lose_sum: 85.7538526058197
time_elpased: 1.379
batch start
#iterations: 290
currently lose_sum: 85.60787463188171
time_elpased: 1.363
batch start
#iterations: 291
currently lose_sum: 86.25426876544952
time_elpased: 1.419
batch start
#iterations: 292
currently lose_sum: 85.82403510808945
time_elpased: 1.344
batch start
#iterations: 293
currently lose_sum: 84.8263675570488
time_elpased: 1.375
batch start
#iterations: 294
currently lose_sum: 86.09920877218246
time_elpased: 1.391
batch start
#iterations: 295
currently lose_sum: 85.87579780817032
time_elpased: 1.368
batch start
#iterations: 296
currently lose_sum: 86.11336195468903
time_elpased: 1.387
batch start
#iterations: 297
currently lose_sum: 85.30614125728607
time_elpased: 1.372
batch start
#iterations: 298
currently lose_sum: 85.57811504602432
time_elpased: 1.395
batch start
#iterations: 299
currently lose_sum: 85.27555173635483
time_elpased: 1.422
start validation test
0.669690721649
0.675023801968
0.656684161778
0.665727699531
0.669713556671
57.949
batch start
#iterations: 300
currently lose_sum: 85.44745177030563
time_elpased: 1.345
batch start
#iterations: 301
currently lose_sum: 85.85697329044342
time_elpased: 1.39
batch start
#iterations: 302
currently lose_sum: 85.1088285446167
time_elpased: 1.424
batch start
#iterations: 303
currently lose_sum: 85.17941105365753
time_elpased: 1.373
batch start
#iterations: 304
currently lose_sum: 85.39144897460938
time_elpased: 1.368
batch start
#iterations: 305
currently lose_sum: 85.4519254565239
time_elpased: 1.399
batch start
#iterations: 306
currently lose_sum: 84.80603969097137
time_elpased: 1.372
batch start
#iterations: 307
currently lose_sum: 85.24424546957016
time_elpased: 1.365
batch start
#iterations: 308
currently lose_sum: 85.1332117319107
time_elpased: 1.384
batch start
#iterations: 309
currently lose_sum: 84.26584362983704
time_elpased: 1.352
batch start
#iterations: 310
currently lose_sum: 85.2294362783432
time_elpased: 1.363
batch start
#iterations: 311
currently lose_sum: 85.49517154693604
time_elpased: 1.364
batch start
#iterations: 312
currently lose_sum: 84.76052725315094
time_elpased: 1.37
batch start
#iterations: 313
currently lose_sum: 85.34916979074478
time_elpased: 1.366
batch start
#iterations: 314
currently lose_sum: 85.72113192081451
time_elpased: 1.367
batch start
#iterations: 315
currently lose_sum: 85.07343977689743
time_elpased: 1.373
batch start
#iterations: 316
currently lose_sum: 84.63501900434494
time_elpased: 1.364
batch start
#iterations: 317
currently lose_sum: 85.45082467794418
time_elpased: 1.359
batch start
#iterations: 318
currently lose_sum: 84.5939337015152
time_elpased: 1.377
batch start
#iterations: 319
currently lose_sum: 84.95340752601624
time_elpased: 1.369
start validation test
0.657268041237
0.672825597116
0.614592981373
0.642392298177
0.657342963887
58.603
batch start
#iterations: 320
currently lose_sum: 84.97800874710083
time_elpased: 1.372
batch start
#iterations: 321
currently lose_sum: 85.02640461921692
time_elpased: 1.409
batch start
#iterations: 322
currently lose_sum: 84.97155565023422
time_elpased: 1.362
batch start
#iterations: 323
currently lose_sum: 84.99126845598221
time_elpased: 1.362
batch start
#iterations: 324
currently lose_sum: 85.16427630186081
time_elpased: 1.39
batch start
#iterations: 325
currently lose_sum: 84.9262306690216
time_elpased: 1.37
batch start
#iterations: 326
currently lose_sum: 84.75673067569733
time_elpased: 1.378
batch start
#iterations: 327
currently lose_sum: 84.70609456300735
time_elpased: 1.362
batch start
#iterations: 328
currently lose_sum: 84.8491005897522
time_elpased: 1.359
batch start
#iterations: 329
currently lose_sum: 84.48415380716324
time_elpased: 1.385
batch start
#iterations: 330
currently lose_sum: 84.88707447052002
time_elpased: 1.356
batch start
#iterations: 331
currently lose_sum: 85.22572839260101
time_elpased: 1.363
batch start
#iterations: 332
currently lose_sum: 84.48425841331482
time_elpased: 1.364
batch start
#iterations: 333
currently lose_sum: 84.28156369924545
time_elpased: 1.394
batch start
#iterations: 334
currently lose_sum: 84.72829675674438
time_elpased: 1.367
batch start
#iterations: 335
currently lose_sum: 83.71747899055481
time_elpased: 1.363
batch start
#iterations: 336
currently lose_sum: 84.5753208398819
time_elpased: 1.379
batch start
#iterations: 337
currently lose_sum: 84.80439889431
time_elpased: 1.41
batch start
#iterations: 338
currently lose_sum: 84.50480377674103
time_elpased: 1.408
batch start
#iterations: 339
currently lose_sum: 84.35040724277496
time_elpased: 1.361
start validation test
0.654484536082
0.668267083519
0.615930842853
0.641032506828
0.654552223037
58.766
batch start
#iterations: 340
currently lose_sum: 84.77395033836365
time_elpased: 1.379
batch start
#iterations: 341
currently lose_sum: 84.85630685091019
time_elpased: 1.41
batch start
#iterations: 342
currently lose_sum: 85.04107987880707
time_elpased: 1.395
batch start
#iterations: 343
currently lose_sum: 84.72391179203987
time_elpased: 1.368
batch start
#iterations: 344
currently lose_sum: 84.04420930147171
time_elpased: 1.366
batch start
#iterations: 345
currently lose_sum: 84.7477999329567
time_elpased: 1.376
batch start
#iterations: 346
currently lose_sum: 84.70333272218704
time_elpased: 1.35
batch start
#iterations: 347
currently lose_sum: 84.61130198836327
time_elpased: 1.361
batch start
#iterations: 348
currently lose_sum: 84.43834084272385
time_elpased: 1.373
batch start
#iterations: 349
currently lose_sum: 84.18929779529572
time_elpased: 1.378
batch start
#iterations: 350
currently lose_sum: 84.73110592365265
time_elpased: 1.348
batch start
#iterations: 351
currently lose_sum: 84.53233647346497
time_elpased: 1.374
batch start
#iterations: 352
currently lose_sum: 84.27986109256744
time_elpased: 1.39
batch start
#iterations: 353
currently lose_sum: 84.14747619628906
time_elpased: 1.458
batch start
#iterations: 354
currently lose_sum: 84.32957577705383
time_elpased: 1.427
batch start
#iterations: 355
currently lose_sum: 83.99630150198936
time_elpased: 1.373
batch start
#iterations: 356
currently lose_sum: 84.93623751401901
time_elpased: 1.371
batch start
#iterations: 357
currently lose_sum: 85.00767379999161
time_elpased: 1.36
batch start
#iterations: 358
currently lose_sum: 84.03313535451889
time_elpased: 1.381
batch start
#iterations: 359
currently lose_sum: 83.96175929903984
time_elpased: 1.391
start validation test
0.655670103093
0.678039629499
0.595142533704
0.633892359969
0.655776368577
59.094
batch start
#iterations: 360
currently lose_sum: 84.05008524656296
time_elpased: 1.365
batch start
#iterations: 361
currently lose_sum: 85.30250680446625
time_elpased: 1.393
batch start
#iterations: 362
currently lose_sum: 84.01324886083603
time_elpased: 1.38
batch start
#iterations: 363
currently lose_sum: 84.06680518388748
time_elpased: 1.354
batch start
#iterations: 364
currently lose_sum: 84.19090867042542
time_elpased: 1.376
batch start
#iterations: 365
currently lose_sum: 84.6350793838501
time_elpased: 1.355
batch start
#iterations: 366
currently lose_sum: 83.93641918897629
time_elpased: 1.422
batch start
#iterations: 367
currently lose_sum: 84.14583176374435
time_elpased: 1.359
batch start
#iterations: 368
currently lose_sum: 84.58601623773575
time_elpased: 1.378
batch start
#iterations: 369
currently lose_sum: 84.33239489793777
time_elpased: 1.364
batch start
#iterations: 370
currently lose_sum: 84.18022710084915
time_elpased: 1.366
batch start
#iterations: 371
currently lose_sum: 84.20644867420197
time_elpased: 1.359
batch start
#iterations: 372
currently lose_sum: 84.41668093204498
time_elpased: 1.38
batch start
#iterations: 373
currently lose_sum: 83.77008157968521
time_elpased: 1.358
batch start
#iterations: 374
currently lose_sum: 84.13144963979721
time_elpased: 1.361
batch start
#iterations: 375
currently lose_sum: 84.18843924999237
time_elpased: 1.387
batch start
#iterations: 376
currently lose_sum: 83.61147195100784
time_elpased: 1.365
batch start
#iterations: 377
currently lose_sum: 83.96196067333221
time_elpased: 1.374
batch start
#iterations: 378
currently lose_sum: 83.57984402775764
time_elpased: 1.368
batch start
#iterations: 379
currently lose_sum: 83.59346407651901
time_elpased: 1.364
start validation test
0.650670103093
0.67466730038
0.584336729443
0.62626151216
0.650786561561
59.225
batch start
#iterations: 380
currently lose_sum: 83.80054265260696
time_elpased: 1.375
batch start
#iterations: 381
currently lose_sum: 83.72851878404617
time_elpased: 1.404
batch start
#iterations: 382
currently lose_sum: 83.7311495244503
time_elpased: 1.367
batch start
#iterations: 383
currently lose_sum: 83.8483943939209
time_elpased: 1.378
batch start
#iterations: 384
currently lose_sum: 83.47430819272995
time_elpased: 1.418
batch start
#iterations: 385
currently lose_sum: 83.96147155761719
time_elpased: 1.363
batch start
#iterations: 386
currently lose_sum: 83.67463916540146
time_elpased: 1.388
batch start
#iterations: 387
currently lose_sum: 83.304882645607
time_elpased: 1.463
batch start
#iterations: 388
currently lose_sum: 84.02430486679077
time_elpased: 1.353
batch start
#iterations: 389
currently lose_sum: 83.77235823869705
time_elpased: 1.356
batch start
#iterations: 390
currently lose_sum: 84.12106442451477
time_elpased: 1.382
batch start
#iterations: 391
currently lose_sum: 83.77287971973419
time_elpased: 1.375
batch start
#iterations: 392
currently lose_sum: 83.87292796373367
time_elpased: 1.355
batch start
#iterations: 393
currently lose_sum: 83.64434707164764
time_elpased: 1.38
batch start
#iterations: 394
currently lose_sum: 83.32575845718384
time_elpased: 1.387
batch start
#iterations: 395
currently lose_sum: 83.25781354308128
time_elpased: 1.393
batch start
#iterations: 396
currently lose_sum: 83.80519902706146
time_elpased: 1.389
batch start
#iterations: 397
currently lose_sum: 83.18393594026566
time_elpased: 1.366
batch start
#iterations: 398
currently lose_sum: 83.44668537378311
time_elpased: 1.356
batch start
#iterations: 399
currently lose_sum: 83.2926954627037
time_elpased: 1.366
start validation test
0.65175257732
0.673380957958
0.591746423793
0.629929886065
0.651857927377
59.256
acc: 0.676
pre: 0.667
rec: 0.705
F1: 0.685
auc: 0.735
