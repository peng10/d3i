start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.09733611345291
time_elpased: 1.494
batch start
#iterations: 1
currently lose_sum: 99.78686213493347
time_elpased: 1.483
batch start
#iterations: 2
currently lose_sum: 99.41609334945679
time_elpased: 1.52
batch start
#iterations: 3
currently lose_sum: 99.40101379156113
time_elpased: 1.537
batch start
#iterations: 4
currently lose_sum: 99.39179074764252
time_elpased: 1.461
batch start
#iterations: 5
currently lose_sum: 99.35046464204788
time_elpased: 1.514
batch start
#iterations: 6
currently lose_sum: 99.16762685775757
time_elpased: 1.524
batch start
#iterations: 7
currently lose_sum: 99.14094114303589
time_elpased: 1.509
batch start
#iterations: 8
currently lose_sum: 98.95363479852676
time_elpased: 1.486
batch start
#iterations: 9
currently lose_sum: 98.66307109594345
time_elpased: 1.534
batch start
#iterations: 10
currently lose_sum: 98.40254646539688
time_elpased: 1.515
batch start
#iterations: 11
currently lose_sum: 98.09200268983841
time_elpased: 1.525
batch start
#iterations: 12
currently lose_sum: 98.15098428726196
time_elpased: 1.517
batch start
#iterations: 13
currently lose_sum: 97.73647332191467
time_elpased: 1.481
batch start
#iterations: 14
currently lose_sum: 97.37054872512817
time_elpased: 1.516
batch start
#iterations: 15
currently lose_sum: 97.30180901288986
time_elpased: 1.516
batch start
#iterations: 16
currently lose_sum: 97.28435558080673
time_elpased: 1.491
batch start
#iterations: 17
currently lose_sum: 96.83600014448166
time_elpased: 1.48
batch start
#iterations: 18
currently lose_sum: 96.8021143078804
time_elpased: 1.504
batch start
#iterations: 19
currently lose_sum: 96.5526829957962
time_elpased: 1.495
start validation test
0.648608247423
0.628924664236
0.727768629065
0.674745932535
0.648477457862
61.744
batch start
#iterations: 20
currently lose_sum: 96.41797512769699
time_elpased: 1.536
batch start
#iterations: 21
currently lose_sum: 96.43141835927963
time_elpased: 1.472
batch start
#iterations: 22
currently lose_sum: 95.97806870937347
time_elpased: 1.5
batch start
#iterations: 23
currently lose_sum: 95.63897228240967
time_elpased: 1.535
batch start
#iterations: 24
currently lose_sum: 95.85802066326141
time_elpased: 1.48
batch start
#iterations: 25
currently lose_sum: 95.69056951999664
time_elpased: 1.516
batch start
#iterations: 26
currently lose_sum: 95.31180733442307
time_elpased: 1.519
batch start
#iterations: 27
currently lose_sum: 95.53205597400665
time_elpased: 1.507
batch start
#iterations: 28
currently lose_sum: 95.00479799509048
time_elpased: 1.524
batch start
#iterations: 29
currently lose_sum: 95.6339425444603
time_elpased: 1.522
batch start
#iterations: 30
currently lose_sum: 94.74720078706741
time_elpased: 1.503
batch start
#iterations: 31
currently lose_sum: 94.42313653230667
time_elpased: 1.489
batch start
#iterations: 32
currently lose_sum: 94.25718063116074
time_elpased: 1.527
batch start
#iterations: 33
currently lose_sum: 94.65826678276062
time_elpased: 1.526
batch start
#iterations: 34
currently lose_sum: 94.15270435810089
time_elpased: 1.517
batch start
#iterations: 35
currently lose_sum: 94.63312071561813
time_elpased: 1.491
batch start
#iterations: 36
currently lose_sum: 93.98466950654984
time_elpased: 1.524
batch start
#iterations: 37
currently lose_sum: 93.91927546262741
time_elpased: 1.493
batch start
#iterations: 38
currently lose_sum: 93.58457726240158
time_elpased: 1.502
batch start
#iterations: 39
currently lose_sum: 93.99495297670364
time_elpased: 1.491
start validation test
0.650979381443
0.64316966456
0.68083573487
0.661466926654
0.650930052483
60.757
batch start
#iterations: 40
currently lose_sum: 93.56657046079636
time_elpased: 1.543
batch start
#iterations: 41
currently lose_sum: 93.78672307729721
time_elpased: 1.532
batch start
#iterations: 42
currently lose_sum: 93.52551054954529
time_elpased: 1.521
batch start
#iterations: 43
currently lose_sum: 93.33024710416794
time_elpased: 1.489
batch start
#iterations: 44
currently lose_sum: 93.33994668722153
time_elpased: 1.523
batch start
#iterations: 45
currently lose_sum: 93.27517813444138
time_elpased: 1.523
batch start
#iterations: 46
currently lose_sum: 93.07577842473984
time_elpased: 1.486
batch start
#iterations: 47
currently lose_sum: 93.10712569952011
time_elpased: 1.504
batch start
#iterations: 48
currently lose_sum: 92.92310482263565
time_elpased: 1.481
batch start
#iterations: 49
currently lose_sum: 93.14888036251068
time_elpased: 1.533
batch start
#iterations: 50
currently lose_sum: 92.766696870327
time_elpased: 1.478
batch start
#iterations: 51
currently lose_sum: 92.34479129314423
time_elpased: 1.536
batch start
#iterations: 52
currently lose_sum: 92.6999300122261
time_elpased: 1.537
batch start
#iterations: 53
currently lose_sum: 92.6965189576149
time_elpased: 1.53
batch start
#iterations: 54
currently lose_sum: 92.75017255544662
time_elpased: 1.509
batch start
#iterations: 55
currently lose_sum: 92.46574437618256
time_elpased: 1.515
batch start
#iterations: 56
currently lose_sum: 91.92174053192139
time_elpased: 1.488
batch start
#iterations: 57
currently lose_sum: 92.39474320411682
time_elpased: 1.53
batch start
#iterations: 58
currently lose_sum: 92.51205468177795
time_elpased: 1.5
batch start
#iterations: 59
currently lose_sum: 92.11062353849411
time_elpased: 1.537
start validation test
0.651030927835
0.651605599012
0.651605599012
0.651605599012
0.651029978358
59.989
batch start
#iterations: 60
currently lose_sum: 91.72247225046158
time_elpased: 1.487
batch start
#iterations: 61
currently lose_sum: 92.77064335346222
time_elpased: 1.494
batch start
#iterations: 62
currently lose_sum: 91.88961410522461
time_elpased: 1.524
batch start
#iterations: 63
currently lose_sum: 91.83299434185028
time_elpased: 1.505
batch start
#iterations: 64
currently lose_sum: 92.01665824651718
time_elpased: 1.526
batch start
#iterations: 65
currently lose_sum: 92.17849177122116
time_elpased: 1.524
batch start
#iterations: 66
currently lose_sum: 91.70456522703171
time_elpased: 1.498
batch start
#iterations: 67
currently lose_sum: 92.14918041229248
time_elpased: 1.488
batch start
#iterations: 68
currently lose_sum: 91.27544069290161
time_elpased: 1.516
batch start
#iterations: 69
currently lose_sum: 91.72004401683807
time_elpased: 1.557
batch start
#iterations: 70
currently lose_sum: 91.26412349939346
time_elpased: 1.549
batch start
#iterations: 71
currently lose_sum: 91.25083547830582
time_elpased: 1.484
batch start
#iterations: 72
currently lose_sum: 91.19198822975159
time_elpased: 1.51
batch start
#iterations: 73
currently lose_sum: 91.35681247711182
time_elpased: 1.483
batch start
#iterations: 74
currently lose_sum: 91.207654774189
time_elpased: 1.557
batch start
#iterations: 75
currently lose_sum: 91.35602992773056
time_elpased: 1.468
batch start
#iterations: 76
currently lose_sum: 91.12737965583801
time_elpased: 1.588
batch start
#iterations: 77
currently lose_sum: 91.3648025393486
time_elpased: 1.483
batch start
#iterations: 78
currently lose_sum: 91.39413303136826
time_elpased: 1.506
batch start
#iterations: 79
currently lose_sum: 91.3020516037941
time_elpased: 1.513
start validation test
0.664329896907
0.665290961618
0.663647591601
0.664468260511
0.664331024219
58.971
batch start
#iterations: 80
currently lose_sum: 91.00229489803314
time_elpased: 1.498
batch start
#iterations: 81
currently lose_sum: 90.5414908528328
time_elpased: 1.527
batch start
#iterations: 82
currently lose_sum: 91.41684871912003
time_elpased: 1.472
batch start
#iterations: 83
currently lose_sum: 91.06089371442795
time_elpased: 1.472
batch start
#iterations: 84
currently lose_sum: 90.3499665260315
time_elpased: 1.502
batch start
#iterations: 85
currently lose_sum: 90.83989536762238
time_elpased: 1.504
batch start
#iterations: 86
currently lose_sum: 90.20405167341232
time_elpased: 1.47
batch start
#iterations: 87
currently lose_sum: 90.71134185791016
time_elpased: 1.499
batch start
#iterations: 88
currently lose_sum: 90.38522934913635
time_elpased: 1.503
batch start
#iterations: 89
currently lose_sum: 90.80405133962631
time_elpased: 1.487
batch start
#iterations: 90
currently lose_sum: 90.75222587585449
time_elpased: 1.524
batch start
#iterations: 91
currently lose_sum: 90.35043823719025
time_elpased: 1.518
batch start
#iterations: 92
currently lose_sum: 90.37054359912872
time_elpased: 1.531
batch start
#iterations: 93
currently lose_sum: 90.40430074930191
time_elpased: 1.532
batch start
#iterations: 94
currently lose_sum: 90.39499861001968
time_elpased: 1.485
batch start
#iterations: 95
currently lose_sum: 89.8032655119896
time_elpased: 1.484
batch start
#iterations: 96
currently lose_sum: 90.08406490087509
time_elpased: 1.51
batch start
#iterations: 97
currently lose_sum: 90.0332800745964
time_elpased: 1.495
batch start
#iterations: 98
currently lose_sum: 89.80743968486786
time_elpased: 1.476
batch start
#iterations: 99
currently lose_sum: 89.78290510177612
time_elpased: 1.501
start validation test
0.670618556701
0.657391633542
0.714903252367
0.684942313381
0.670545389092
58.690
batch start
#iterations: 100
currently lose_sum: 90.20046818256378
time_elpased: 1.502
batch start
#iterations: 101
currently lose_sum: 90.0319032073021
time_elpased: 1.501
batch start
#iterations: 102
currently lose_sum: 90.09775102138519
time_elpased: 1.467
batch start
#iterations: 103
currently lose_sum: 90.19832146167755
time_elpased: 1.5
batch start
#iterations: 104
currently lose_sum: 89.54784518480301
time_elpased: 1.512
batch start
#iterations: 105
currently lose_sum: 89.83902215957642
time_elpased: 1.537
batch start
#iterations: 106
currently lose_sum: 89.69003599882126
time_elpased: 1.488
batch start
#iterations: 107
currently lose_sum: 89.58819383382797
time_elpased: 1.512
batch start
#iterations: 108
currently lose_sum: 89.94368010759354
time_elpased: 1.487
batch start
#iterations: 109
currently lose_sum: 90.13322019577026
time_elpased: 1.501
batch start
#iterations: 110
currently lose_sum: 89.63524723052979
time_elpased: 1.485
batch start
#iterations: 111
currently lose_sum: 89.86561924219131
time_elpased: 1.508
batch start
#iterations: 112
currently lose_sum: 88.82693612575531
time_elpased: 1.489
batch start
#iterations: 113
currently lose_sum: 89.32985371351242
time_elpased: 1.5
batch start
#iterations: 114
currently lose_sum: 89.76062256097794
time_elpased: 1.486
batch start
#iterations: 115
currently lose_sum: 89.44137221574783
time_elpased: 1.508
batch start
#iterations: 116
currently lose_sum: 89.25286972522736
time_elpased: 1.52
batch start
#iterations: 117
currently lose_sum: 89.3056069612503
time_elpased: 1.5
batch start
#iterations: 118
currently lose_sum: 89.19065874814987
time_elpased: 1.494
batch start
#iterations: 119
currently lose_sum: 89.50237721204758
time_elpased: 1.523
start validation test
0.665463917526
0.662469782434
0.676924660354
0.669619222154
0.665444981974
58.661
batch start
#iterations: 120
currently lose_sum: 88.84573918581009
time_elpased: 1.53
batch start
#iterations: 121
currently lose_sum: 88.82697236537933
time_elpased: 1.481
batch start
#iterations: 122
currently lose_sum: 88.8838871717453
time_elpased: 1.523
batch start
#iterations: 123
currently lose_sum: 89.61686408519745
time_elpased: 1.515
batch start
#iterations: 124
currently lose_sum: 89.35617887973785
time_elpased: 1.469
batch start
#iterations: 125
currently lose_sum: 88.34829223155975
time_elpased: 1.466
batch start
#iterations: 126
currently lose_sum: 88.39362996816635
time_elpased: 1.487
batch start
#iterations: 127
currently lose_sum: 88.74445486068726
time_elpased: 1.488
batch start
#iterations: 128
currently lose_sum: 88.93173772096634
time_elpased: 1.481
batch start
#iterations: 129
currently lose_sum: 88.7161175608635
time_elpased: 1.514
batch start
#iterations: 130
currently lose_sum: 88.77512818574905
time_elpased: 1.474
batch start
#iterations: 131
currently lose_sum: 88.9419795870781
time_elpased: 1.486
batch start
#iterations: 132
currently lose_sum: 88.15306532382965
time_elpased: 1.505
batch start
#iterations: 133
currently lose_sum: 88.78640133142471
time_elpased: 1.519
batch start
#iterations: 134
currently lose_sum: 89.12997061014175
time_elpased: 1.568
batch start
#iterations: 135
currently lose_sum: 88.47764903306961
time_elpased: 1.476
batch start
#iterations: 136
currently lose_sum: 88.48313820362091
time_elpased: 1.484
batch start
#iterations: 137
currently lose_sum: 88.40369749069214
time_elpased: 1.476
batch start
#iterations: 138
currently lose_sum: 88.4390355348587
time_elpased: 1.477
batch start
#iterations: 139
currently lose_sum: 88.09993350505829
time_elpased: 1.478
start validation test
0.669793814433
0.665334665335
0.685467270482
0.675250937849
0.669767918595
58.634
batch start
#iterations: 140
currently lose_sum: 88.50402516126633
time_elpased: 1.498
batch start
#iterations: 141
currently lose_sum: 87.97646099328995
time_elpased: 1.518
batch start
#iterations: 142
currently lose_sum: 88.29341340065002
time_elpased: 1.48
batch start
#iterations: 143
currently lose_sum: 88.23961073160172
time_elpased: 1.514
batch start
#iterations: 144
currently lose_sum: 87.79976427555084
time_elpased: 1.541
batch start
#iterations: 145
currently lose_sum: 87.96150922775269
time_elpased: 1.52
batch start
#iterations: 146
currently lose_sum: 87.81685274839401
time_elpased: 1.514
batch start
#iterations: 147
currently lose_sum: 88.12394857406616
time_elpased: 1.517
batch start
#iterations: 148
currently lose_sum: 88.05477702617645
time_elpased: 1.545
batch start
#iterations: 149
currently lose_sum: 88.04687064886093
time_elpased: 1.467
batch start
#iterations: 150
currently lose_sum: 88.64222192764282
time_elpased: 1.499
batch start
#iterations: 151
currently lose_sum: 88.24199283123016
time_elpased: 1.543
batch start
#iterations: 152
currently lose_sum: 87.64927464723587
time_elpased: 1.521
batch start
#iterations: 153
currently lose_sum: 87.97633796930313
time_elpased: 1.486
batch start
#iterations: 154
currently lose_sum: 87.8535828590393
time_elpased: 1.485
batch start
#iterations: 155
currently lose_sum: 87.80052244663239
time_elpased: 1.492
batch start
#iterations: 156
currently lose_sum: 87.42967402935028
time_elpased: 1.536
batch start
#iterations: 157
currently lose_sum: 87.2846674323082
time_elpased: 1.496
batch start
#iterations: 158
currently lose_sum: 87.9074478149414
time_elpased: 1.49
batch start
#iterations: 159
currently lose_sum: 87.96390569210052
time_elpased: 1.489
start validation test
0.645927835052
0.646224961479
0.647488678469
0.646856202766
0.645925256211
59.373
batch start
#iterations: 160
currently lose_sum: 87.6791399717331
time_elpased: 1.608
batch start
#iterations: 161
currently lose_sum: 88.00426530838013
time_elpased: 1.486
batch start
#iterations: 162
currently lose_sum: 87.78841990232468
time_elpased: 1.508
batch start
#iterations: 163
currently lose_sum: 87.55785316228867
time_elpased: 1.565
batch start
#iterations: 164
currently lose_sum: 87.82835906744003
time_elpased: 1.509
batch start
#iterations: 165
currently lose_sum: 87.71725279092789
time_elpased: 1.496
batch start
#iterations: 166
currently lose_sum: 87.26063233613968
time_elpased: 1.498
batch start
#iterations: 167
currently lose_sum: 87.18795478343964
time_elpased: 1.519
batch start
#iterations: 168
currently lose_sum: 87.79183411598206
time_elpased: 1.517
batch start
#iterations: 169
currently lose_sum: 87.35516583919525
time_elpased: 1.466
batch start
#iterations: 170
currently lose_sum: 87.13743513822556
time_elpased: 1.507
batch start
#iterations: 171
currently lose_sum: 87.57722175121307
time_elpased: 1.517
batch start
#iterations: 172
currently lose_sum: 87.16082310676575
time_elpased: 1.48
batch start
#iterations: 173
currently lose_sum: 87.08073139190674
time_elpased: 1.538
batch start
#iterations: 174
currently lose_sum: 86.78254580497742
time_elpased: 1.498
batch start
#iterations: 175
currently lose_sum: 87.17212009429932
time_elpased: 1.519
batch start
#iterations: 176
currently lose_sum: 87.05486607551575
time_elpased: 1.483
batch start
#iterations: 177
currently lose_sum: 87.0436378121376
time_elpased: 1.504
batch start
#iterations: 178
currently lose_sum: 86.6276923418045
time_elpased: 1.478
batch start
#iterations: 179
currently lose_sum: 86.44355702400208
time_elpased: 1.506
start validation test
0.645618556701
0.671952548118
0.571325648415
0.617566891027
0.645741304174
59.649
batch start
#iterations: 180
currently lose_sum: 87.42106658220291
time_elpased: 1.472
batch start
#iterations: 181
currently lose_sum: 86.83984500169754
time_elpased: 1.491
batch start
#iterations: 182
currently lose_sum: 87.574638068676
time_elpased: 1.51
batch start
#iterations: 183
currently lose_sum: 86.56611606478691
time_elpased: 1.526
batch start
#iterations: 184
currently lose_sum: 87.1681712269783
time_elpased: 1.494
batch start
#iterations: 185
currently lose_sum: 86.29474192857742
time_elpased: 1.558
batch start
#iterations: 186
currently lose_sum: 87.05542266368866
time_elpased: 1.492
batch start
#iterations: 187
currently lose_sum: 85.90391850471497
time_elpased: 1.526
batch start
#iterations: 188
currently lose_sum: 86.69796776771545
time_elpased: 1.561
batch start
#iterations: 189
currently lose_sum: 86.43259996175766
time_elpased: 1.526
batch start
#iterations: 190
currently lose_sum: 86.28575015068054
time_elpased: 1.478
batch start
#iterations: 191
currently lose_sum: 86.44267094135284
time_elpased: 1.468
batch start
#iterations: 192
currently lose_sum: 86.56140577793121
time_elpased: 1.514
batch start
#iterations: 193
currently lose_sum: 86.93364226818085
time_elpased: 1.509
batch start
#iterations: 194
currently lose_sum: 86.4428203701973
time_elpased: 1.504
batch start
#iterations: 195
currently lose_sum: 86.15839564800262
time_elpased: 1.481
batch start
#iterations: 196
currently lose_sum: 86.01180166006088
time_elpased: 1.52
batch start
#iterations: 197
currently lose_sum: 86.4533240199089
time_elpased: 1.465
batch start
#iterations: 198
currently lose_sum: 85.6830335855484
time_elpased: 1.481
batch start
#iterations: 199
currently lose_sum: 85.5376688838005
time_elpased: 1.514
start validation test
0.642371134021
0.654470640569
0.605701934953
0.629142612786
0.642431719232
59.861
batch start
#iterations: 200
currently lose_sum: 85.90614527463913
time_elpased: 1.547
batch start
#iterations: 201
currently lose_sum: 85.73571527004242
time_elpased: 1.492
batch start
#iterations: 202
currently lose_sum: 85.8327311873436
time_elpased: 1.481
batch start
#iterations: 203
currently lose_sum: 86.4632847905159
time_elpased: 1.483
batch start
#iterations: 204
currently lose_sum: 86.63171303272247
time_elpased: 1.545
batch start
#iterations: 205
currently lose_sum: 85.80228400230408
time_elpased: 1.494
batch start
#iterations: 206
currently lose_sum: 85.7181043624878
time_elpased: 1.469
batch start
#iterations: 207
currently lose_sum: 85.80774825811386
time_elpased: 1.495
batch start
#iterations: 208
currently lose_sum: 85.81817936897278
time_elpased: 1.552
batch start
#iterations: 209
currently lose_sum: 85.57205384969711
time_elpased: 1.478
batch start
#iterations: 210
currently lose_sum: 85.2052885890007
time_elpased: 1.545
batch start
#iterations: 211
currently lose_sum: 85.3639081120491
time_elpased: 1.534
batch start
#iterations: 212
currently lose_sum: 86.23335349559784
time_elpased: 1.49
batch start
#iterations: 213
currently lose_sum: 85.21843576431274
time_elpased: 1.486
batch start
#iterations: 214
currently lose_sum: 85.38026601076126
time_elpased: 1.529
batch start
#iterations: 215
currently lose_sum: 85.38613855838776
time_elpased: 1.493
batch start
#iterations: 216
currently lose_sum: 85.87133574485779
time_elpased: 1.497
batch start
#iterations: 217
currently lose_sum: 85.61912608146667
time_elpased: 1.473
batch start
#iterations: 218
currently lose_sum: 85.73465698957443
time_elpased: 1.522
batch start
#iterations: 219
currently lose_sum: 85.26330375671387
time_elpased: 1.53
start validation test
0.653350515464
0.660616475137
0.633079456566
0.646554895675
0.653384007507
59.249
batch start
#iterations: 220
currently lose_sum: 85.4071426987648
time_elpased: 1.538
batch start
#iterations: 221
currently lose_sum: 85.22870516777039
time_elpased: 1.484
batch start
#iterations: 222
currently lose_sum: 84.8363818526268
time_elpased: 1.507
batch start
#iterations: 223
currently lose_sum: 84.83637583255768
time_elpased: 1.529
batch start
#iterations: 224
currently lose_sum: 85.24136453866959
time_elpased: 1.517
batch start
#iterations: 225
currently lose_sum: 85.09891903400421
time_elpased: 1.483
batch start
#iterations: 226
currently lose_sum: 84.37980934977531
time_elpased: 1.49
batch start
#iterations: 227
currently lose_sum: 84.87942606210709
time_elpased: 1.526
batch start
#iterations: 228
currently lose_sum: 84.52161306142807
time_elpased: 1.518
batch start
#iterations: 229
currently lose_sum: 84.35200646519661
time_elpased: 1.503
batch start
#iterations: 230
currently lose_sum: 85.38679444789886
time_elpased: 1.501
batch start
#iterations: 231
currently lose_sum: 85.50873738527298
time_elpased: 1.528
batch start
#iterations: 232
currently lose_sum: 85.03364527225494
time_elpased: 1.562
batch start
#iterations: 233
currently lose_sum: 85.5512563586235
time_elpased: 1.469
batch start
#iterations: 234
currently lose_sum: 85.22095489501953
time_elpased: 1.515
batch start
#iterations: 235
currently lose_sum: 85.40801858901978
time_elpased: 1.529
batch start
#iterations: 236
currently lose_sum: 84.88920325040817
time_elpased: 1.527
batch start
#iterations: 237
currently lose_sum: 84.553271651268
time_elpased: 1.498
batch start
#iterations: 238
currently lose_sum: 85.29373395442963
time_elpased: 1.53
batch start
#iterations: 239
currently lose_sum: 84.38322085142136
time_elpased: 1.529
start validation test
0.637371134021
0.656108070339
0.579868258543
0.615636780856
0.637466140837
60.358
batch start
#iterations: 240
currently lose_sum: 84.40028315782547
time_elpased: 1.518
batch start
#iterations: 241
currently lose_sum: 84.40652424097061
time_elpased: 1.549
batch start
#iterations: 242
currently lose_sum: 84.47018253803253
time_elpased: 1.521
batch start
#iterations: 243
currently lose_sum: 84.76939713954926
time_elpased: 1.512
batch start
#iterations: 244
currently lose_sum: 84.5723717212677
time_elpased: 1.516
batch start
#iterations: 245
currently lose_sum: 84.87640708684921
time_elpased: 1.497
batch start
#iterations: 246
currently lose_sum: 83.91582697629929
time_elpased: 1.526
batch start
#iterations: 247
currently lose_sum: 84.97518545389175
time_elpased: 1.532
batch start
#iterations: 248
currently lose_sum: 84.26878452301025
time_elpased: 1.479
batch start
#iterations: 249
currently lose_sum: 84.51341480016708
time_elpased: 1.479
batch start
#iterations: 250
currently lose_sum: 84.20745235681534
time_elpased: 1.531
batch start
#iterations: 251
currently lose_sum: 84.72062289714813
time_elpased: 1.543
batch start
#iterations: 252
currently lose_sum: 84.1172114610672
time_elpased: 1.531
batch start
#iterations: 253
currently lose_sum: 84.0857789516449
time_elpased: 1.496
batch start
#iterations: 254
currently lose_sum: 83.19157814979553
time_elpased: 1.509
batch start
#iterations: 255
currently lose_sum: 84.29706686735153
time_elpased: 1.505
batch start
#iterations: 256
currently lose_sum: 84.12896159291267
time_elpased: 1.519
batch start
#iterations: 257
currently lose_sum: 83.9186547100544
time_elpased: 1.534
batch start
#iterations: 258
currently lose_sum: 83.87874698638916
time_elpased: 1.494
batch start
#iterations: 259
currently lose_sum: 84.2233652472496
time_elpased: 1.504
start validation test
0.649587628866
0.664561245206
0.606422396048
0.634162092347
0.649658946888
59.900
batch start
#iterations: 260
currently lose_sum: 84.38837587833405
time_elpased: 1.565
batch start
#iterations: 261
currently lose_sum: 83.96602994203568
time_elpased: 1.467
batch start
#iterations: 262
currently lose_sum: 82.80726438760757
time_elpased: 1.48
batch start
#iterations: 263
currently lose_sum: 83.83921247720718
time_elpased: 1.523
batch start
#iterations: 264
currently lose_sum: 83.94702118635178
time_elpased: 1.521
batch start
#iterations: 265
currently lose_sum: 83.6641013622284
time_elpased: 1.495
batch start
#iterations: 266
currently lose_sum: 83.12092179059982
time_elpased: 1.5
batch start
#iterations: 267
currently lose_sum: 84.60381317138672
time_elpased: 1.54
batch start
#iterations: 268
currently lose_sum: 83.15119218826294
time_elpased: 1.508
batch start
#iterations: 269
currently lose_sum: 83.56887209415436
time_elpased: 1.478
batch start
#iterations: 270
currently lose_sum: 83.63325798511505
time_elpased: 1.54
batch start
#iterations: 271
currently lose_sum: 83.75654110312462
time_elpased: 1.511
batch start
#iterations: 272
currently lose_sum: 83.60870057344437
time_elpased: 1.52
batch start
#iterations: 273
currently lose_sum: 83.11285716295242
time_elpased: 1.514
batch start
#iterations: 274
currently lose_sum: 82.71600291132927
time_elpased: 1.492
batch start
#iterations: 275
currently lose_sum: 83.6067259311676
time_elpased: 1.489
batch start
#iterations: 276
currently lose_sum: 83.38607439398766
time_elpased: 1.506
batch start
#iterations: 277
currently lose_sum: 82.71390110254288
time_elpased: 1.548
batch start
#iterations: 278
currently lose_sum: 83.1087928712368
time_elpased: 1.509
batch start
#iterations: 279
currently lose_sum: 83.508524030447
time_elpased: 1.519
start validation test
0.635824742268
0.664842681259
0.55022643063
0.602128737962
0.63596616864
60.994
batch start
#iterations: 280
currently lose_sum: 82.77593657374382
time_elpased: 1.529
batch start
#iterations: 281
currently lose_sum: 82.99238231778145
time_elpased: 1.547
batch start
#iterations: 282
currently lose_sum: 83.0821682214737
time_elpased: 1.579
batch start
#iterations: 283
currently lose_sum: 83.31663173437119
time_elpased: 1.474
batch start
#iterations: 284
currently lose_sum: 83.18340197205544
time_elpased: 1.496
batch start
#iterations: 285
currently lose_sum: 82.77727568149567
time_elpased: 1.497
batch start
#iterations: 286
currently lose_sum: 83.10579377412796
time_elpased: 1.528
batch start
#iterations: 287
currently lose_sum: 82.6719998717308
time_elpased: 1.524
batch start
#iterations: 288
currently lose_sum: 83.44955110549927
time_elpased: 1.489
batch start
#iterations: 289
currently lose_sum: 82.76341289281845
time_elpased: 1.483
batch start
#iterations: 290
currently lose_sum: 82.86206492781639
time_elpased: 1.552
batch start
#iterations: 291
currently lose_sum: 82.98950731754303
time_elpased: 1.468
batch start
#iterations: 292
currently lose_sum: 82.35193437337875
time_elpased: 1.499
batch start
#iterations: 293
currently lose_sum: 82.15909352898598
time_elpased: 1.477
batch start
#iterations: 294
currently lose_sum: 82.00400087237358
time_elpased: 1.51
batch start
#iterations: 295
currently lose_sum: 82.68676763772964
time_elpased: 1.497
batch start
#iterations: 296
currently lose_sum: 82.90055292844772
time_elpased: 1.557
batch start
#iterations: 297
currently lose_sum: 82.37015950679779
time_elpased: 1.504
batch start
#iterations: 298
currently lose_sum: 82.52536129951477
time_elpased: 1.519
batch start
#iterations: 299
currently lose_sum: 83.04884466528893
time_elpased: 1.519
start validation test
0.63118556701
0.667232597623
0.52583367641
0.588154032119
0.63135963044
61.113
batch start
#iterations: 300
currently lose_sum: 82.25262171030045
time_elpased: 1.489
batch start
#iterations: 301
currently lose_sum: 82.2349014878273
time_elpased: 1.5
batch start
#iterations: 302
currently lose_sum: 81.60397186875343
time_elpased: 1.544
batch start
#iterations: 303
currently lose_sum: 82.4957637488842
time_elpased: 1.482
batch start
#iterations: 304
currently lose_sum: 81.18041098117828
time_elpased: 1.509
batch start
#iterations: 305
currently lose_sum: 82.31310868263245
time_elpased: 1.478
batch start
#iterations: 306
currently lose_sum: 82.52944776415825
time_elpased: 1.481
batch start
#iterations: 307
currently lose_sum: 82.32725018262863
time_elpased: 1.465
batch start
#iterations: 308
currently lose_sum: 82.05705481767654
time_elpased: 1.495
batch start
#iterations: 309
currently lose_sum: 81.76095315814018
time_elpased: 1.49
batch start
#iterations: 310
currently lose_sum: 82.16663956642151
time_elpased: 1.517
batch start
#iterations: 311
currently lose_sum: 82.0920435488224
time_elpased: 1.492
batch start
#iterations: 312
currently lose_sum: 81.93182861804962
time_elpased: 1.554
batch start
#iterations: 313
currently lose_sum: 81.94440904259682
time_elpased: 1.492
batch start
#iterations: 314
currently lose_sum: 81.68341985344887
time_elpased: 1.491
batch start
#iterations: 315
currently lose_sum: 81.69017565250397
time_elpased: 1.538
batch start
#iterations: 316
currently lose_sum: 81.4212626516819
time_elpased: 1.56
batch start
#iterations: 317
currently lose_sum: 81.4764261841774
time_elpased: 1.49
batch start
#iterations: 318
currently lose_sum: 81.58881404995918
time_elpased: 1.513
batch start
#iterations: 319
currently lose_sum: 81.86674073338509
time_elpased: 1.509
start validation test
0.632371134021
0.668404588113
0.527789213668
0.589832068093
0.632543925298
61.517
batch start
#iterations: 320
currently lose_sum: 81.8285459280014
time_elpased: 1.545
batch start
#iterations: 321
currently lose_sum: 81.97056701779366
time_elpased: 1.467
batch start
#iterations: 322
currently lose_sum: 81.61170822381973
time_elpased: 1.489
batch start
#iterations: 323
currently lose_sum: 80.94909048080444
time_elpased: 1.516
batch start
#iterations: 324
currently lose_sum: 81.466826826334
time_elpased: 1.507
batch start
#iterations: 325
currently lose_sum: 81.29729384183884
time_elpased: 1.522
batch start
#iterations: 326
currently lose_sum: 81.19563978910446
time_elpased: 1.502
batch start
#iterations: 327
currently lose_sum: 81.12309500575066
time_elpased: 1.507
batch start
#iterations: 328
currently lose_sum: 81.5554806292057
time_elpased: 1.517
batch start
#iterations: 329
currently lose_sum: 80.92997422814369
time_elpased: 1.551
batch start
#iterations: 330
currently lose_sum: 81.0857762992382
time_elpased: 1.483
batch start
#iterations: 331
currently lose_sum: 81.03408083319664
time_elpased: 1.531
batch start
#iterations: 332
currently lose_sum: 81.217968583107
time_elpased: 1.556
batch start
#iterations: 333
currently lose_sum: 80.85349053144455
time_elpased: 1.5
batch start
#iterations: 334
currently lose_sum: 80.95677861571312
time_elpased: 1.494
batch start
#iterations: 335
currently lose_sum: 80.6260857284069
time_elpased: 1.501
batch start
#iterations: 336
currently lose_sum: 81.24856698513031
time_elpased: 1.499
batch start
#iterations: 337
currently lose_sum: 81.38485491275787
time_elpased: 1.512
batch start
#iterations: 338
currently lose_sum: 81.31289020180702
time_elpased: 1.508
batch start
#iterations: 339
currently lose_sum: 81.0962604880333
time_elpased: 1.494
start validation test
0.629175257732
0.66492283547
0.52326060107
0.585646814883
0.629350250969
62.217
batch start
#iterations: 340
currently lose_sum: 80.80561941862106
time_elpased: 1.517
batch start
#iterations: 341
currently lose_sum: 81.30911898612976
time_elpased: 1.47
batch start
#iterations: 342
currently lose_sum: 80.73537263274193
time_elpased: 1.533
batch start
#iterations: 343
currently lose_sum: 80.98334163427353
time_elpased: 1.475
batch start
#iterations: 344
currently lose_sum: 80.37373530864716
time_elpased: 1.499
batch start
#iterations: 345
currently lose_sum: 81.10508701205254
time_elpased: 1.48
batch start
#iterations: 346
currently lose_sum: 80.7333819270134
time_elpased: 1.503
batch start
#iterations: 347
currently lose_sum: 81.54756718873978
time_elpased: 1.513
batch start
#iterations: 348
currently lose_sum: 81.23515418171883
time_elpased: 1.505
batch start
#iterations: 349
currently lose_sum: 80.16020593047142
time_elpased: 1.5
batch start
#iterations: 350
currently lose_sum: 81.27956026792526
time_elpased: 1.511
batch start
#iterations: 351
currently lose_sum: 80.45706421136856
time_elpased: 1.501
batch start
#iterations: 352
currently lose_sum: 80.86899045109749
time_elpased: 1.474
batch start
#iterations: 353
currently lose_sum: 80.54613521695137
time_elpased: 1.507
batch start
#iterations: 354
currently lose_sum: 79.96236774325371
time_elpased: 1.56
batch start
#iterations: 355
currently lose_sum: 80.39581117033958
time_elpased: 1.486
batch start
#iterations: 356
currently lose_sum: 81.05812728404999
time_elpased: 1.524
batch start
#iterations: 357
currently lose_sum: 80.14477384090424
time_elpased: 1.531
batch start
#iterations: 358
currently lose_sum: 80.297597438097
time_elpased: 1.491
batch start
#iterations: 359
currently lose_sum: 79.9976232945919
time_elpased: 1.462
start validation test
0.628350515464
0.665960264901
0.51749691231
0.58241630951
0.628533668877
62.577
batch start
#iterations: 360
currently lose_sum: 80.24296766519547
time_elpased: 1.5
batch start
#iterations: 361
currently lose_sum: 80.2269643843174
time_elpased: 1.526
batch start
#iterations: 362
currently lose_sum: 80.20954138040543
time_elpased: 1.478
batch start
#iterations: 363
currently lose_sum: 80.39946031570435
time_elpased: 1.487
batch start
#iterations: 364
currently lose_sum: 80.34427165985107
time_elpased: 1.569
batch start
#iterations: 365
currently lose_sum: 79.85444775223732
time_elpased: 1.512
batch start
#iterations: 366
currently lose_sum: 80.2206172645092
time_elpased: 1.531
batch start
#iterations: 367
currently lose_sum: 79.64495941996574
time_elpased: 1.479
batch start
#iterations: 368
currently lose_sum: 80.1563093662262
time_elpased: 1.479
batch start
#iterations: 369
currently lose_sum: 79.84383842349052
time_elpased: 1.537
batch start
#iterations: 370
currently lose_sum: 79.34927400946617
time_elpased: 1.479
batch start
#iterations: 371
currently lose_sum: 79.79970428347588
time_elpased: 1.55
batch start
#iterations: 372
currently lose_sum: 80.24757805466652
time_elpased: 1.501
batch start
#iterations: 373
currently lose_sum: 80.22436141967773
time_elpased: 1.465
batch start
#iterations: 374
currently lose_sum: 79.4500002861023
time_elpased: 1.543
batch start
#iterations: 375
currently lose_sum: 79.5700232386589
time_elpased: 1.572
batch start
#iterations: 376
currently lose_sum: 79.98382192850113
time_elpased: 1.475
batch start
#iterations: 377
currently lose_sum: 79.16521075367928
time_elpased: 1.498
batch start
#iterations: 378
currently lose_sum: 79.19419947266579
time_elpased: 1.503
batch start
#iterations: 379
currently lose_sum: 79.69299226999283
time_elpased: 1.544
start validation test
0.620257731959
0.659600489197
0.499588307946
0.568550512445
0.620457103167
63.495
batch start
#iterations: 380
currently lose_sum: 79.3765818476677
time_elpased: 1.501
batch start
#iterations: 381
currently lose_sum: 79.53949630260468
time_elpased: 1.513
batch start
#iterations: 382
currently lose_sum: 79.53705072402954
time_elpased: 1.508
batch start
#iterations: 383
currently lose_sum: 79.3467099070549
time_elpased: 1.539
batch start
#iterations: 384
currently lose_sum: 79.26020267605782
time_elpased: 1.491
batch start
#iterations: 385
currently lose_sum: 79.11590740084648
time_elpased: 1.528
batch start
#iterations: 386
currently lose_sum: 79.22771915793419
time_elpased: 1.516
batch start
#iterations: 387
currently lose_sum: 78.88888582587242
time_elpased: 1.494
batch start
#iterations: 388
currently lose_sum: 79.69917023181915
time_elpased: 1.51
batch start
#iterations: 389
currently lose_sum: 78.87738654017448
time_elpased: 1.485
batch start
#iterations: 390
currently lose_sum: 78.97464853525162
time_elpased: 1.543
batch start
#iterations: 391
currently lose_sum: 79.3803428709507
time_elpased: 1.488
batch start
#iterations: 392
currently lose_sum: 78.49094912409782
time_elpased: 1.527
batch start
#iterations: 393
currently lose_sum: 79.57509610056877
time_elpased: 1.476
batch start
#iterations: 394
currently lose_sum: 78.29008892178535
time_elpased: 1.51
batch start
#iterations: 395
currently lose_sum: 78.42974925041199
time_elpased: 1.504
batch start
#iterations: 396
currently lose_sum: 79.07310798764229
time_elpased: 1.48
batch start
#iterations: 397
currently lose_sum: 78.73270961642265
time_elpased: 1.521
batch start
#iterations: 398
currently lose_sum: 78.60524877905846
time_elpased: 1.53
batch start
#iterations: 399
currently lose_sum: 79.022626131773
time_elpased: 1.518
start validation test
0.627216494845
0.662778505898
0.520481679704
0.583073907529
0.627392843156
63.328
acc: 0.679
pre: 0.674
rec: 0.693
F1: 0.683
auc: 0.744
