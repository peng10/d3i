start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.11465948820114
time_elpased: 1.745
batch start
#iterations: 1
currently lose_sum: 99.79473125934601
time_elpased: 1.822
batch start
#iterations: 2
currently lose_sum: 99.29366785287857
time_elpased: 1.773
batch start
#iterations: 3
currently lose_sum: 98.89198637008667
time_elpased: 1.809
batch start
#iterations: 4
currently lose_sum: 98.58211749792099
time_elpased: 1.769
batch start
#iterations: 5
currently lose_sum: 97.94838958978653
time_elpased: 1.757
batch start
#iterations: 6
currently lose_sum: 97.80360162258148
time_elpased: 1.79
batch start
#iterations: 7
currently lose_sum: 97.55271089076996
time_elpased: 1.831
batch start
#iterations: 8
currently lose_sum: 97.56378918886185
time_elpased: 1.767
batch start
#iterations: 9
currently lose_sum: 97.09687322378159
time_elpased: 1.803
batch start
#iterations: 10
currently lose_sum: 97.12387639284134
time_elpased: 1.778
batch start
#iterations: 11
currently lose_sum: 97.02623736858368
time_elpased: 1.778
batch start
#iterations: 12
currently lose_sum: 97.39290243387222
time_elpased: 1.752
batch start
#iterations: 13
currently lose_sum: 96.95070040225983
time_elpased: 1.77
batch start
#iterations: 14
currently lose_sum: 96.81967180967331
time_elpased: 1.836
batch start
#iterations: 15
currently lose_sum: 96.69841998815536
time_elpased: 1.744
batch start
#iterations: 16
currently lose_sum: 96.74318337440491
time_elpased: 1.728
batch start
#iterations: 17
currently lose_sum: 96.35020476579666
time_elpased: 1.818
batch start
#iterations: 18
currently lose_sum: 96.46836793422699
time_elpased: 1.801
batch start
#iterations: 19
currently lose_sum: 96.13545799255371
time_elpased: 1.725
start validation test
0.633453608247
0.615870474157
0.712536023055
0.660686166913
0.633322947504
62.079
batch start
#iterations: 20
currently lose_sum: 96.05368512868881
time_elpased: 1.791
batch start
#iterations: 21
currently lose_sum: 95.67658507823944
time_elpased: 1.819
batch start
#iterations: 22
currently lose_sum: 95.63631135225296
time_elpased: 1.772
batch start
#iterations: 23
currently lose_sum: 95.12095671892166
time_elpased: 1.781
batch start
#iterations: 24
currently lose_sum: 95.59384393692017
time_elpased: 1.731
batch start
#iterations: 25
currently lose_sum: 94.98247128725052
time_elpased: 1.79
batch start
#iterations: 26
currently lose_sum: 94.84693467617035
time_elpased: 1.784
batch start
#iterations: 27
currently lose_sum: 95.14887499809265
time_elpased: 1.759
batch start
#iterations: 28
currently lose_sum: 94.6894541978836
time_elpased: 1.758
batch start
#iterations: 29
currently lose_sum: 95.31094819307327
time_elpased: 1.736
batch start
#iterations: 30
currently lose_sum: 94.53662151098251
time_elpased: 1.747
batch start
#iterations: 31
currently lose_sum: 94.24801069498062
time_elpased: 1.701
batch start
#iterations: 32
currently lose_sum: 94.05554503202438
time_elpased: 1.755
batch start
#iterations: 33
currently lose_sum: 94.4978985786438
time_elpased: 1.767
batch start
#iterations: 34
currently lose_sum: 93.96587890386581
time_elpased: 1.774
batch start
#iterations: 35
currently lose_sum: 94.35321247577667
time_elpased: 1.727
batch start
#iterations: 36
currently lose_sum: 93.96210408210754
time_elpased: 1.756
batch start
#iterations: 37
currently lose_sum: 93.91846090555191
time_elpased: 1.721
batch start
#iterations: 38
currently lose_sum: 93.61289268732071
time_elpased: 1.765
batch start
#iterations: 39
currently lose_sum: 94.24169939756393
time_elpased: 1.76
start validation test
0.657989690722
0.630739200543
0.76492383697
0.691380994465
0.657813013074
60.080
batch start
#iterations: 40
currently lose_sum: 93.6338039636612
time_elpased: 1.733
batch start
#iterations: 41
currently lose_sum: 93.99080175161362
time_elpased: 1.773
batch start
#iterations: 42
currently lose_sum: 93.4226016998291
time_elpased: 1.799
batch start
#iterations: 43
currently lose_sum: 93.43847686052322
time_elpased: 1.724
batch start
#iterations: 44
currently lose_sum: 93.51346081495285
time_elpased: 1.757
batch start
#iterations: 45
currently lose_sum: 93.39355027675629
time_elpased: 1.716
batch start
#iterations: 46
currently lose_sum: 93.16789072751999
time_elpased: 1.723
batch start
#iterations: 47
currently lose_sum: 93.22276300191879
time_elpased: 1.88
batch start
#iterations: 48
currently lose_sum: 92.93644428253174
time_elpased: 1.743
batch start
#iterations: 49
currently lose_sum: 93.39719551801682
time_elpased: 1.724
batch start
#iterations: 50
currently lose_sum: 92.8671390414238
time_elpased: 1.779
batch start
#iterations: 51
currently lose_sum: 92.63429307937622
time_elpased: 1.761
batch start
#iterations: 52
currently lose_sum: 92.84229671955109
time_elpased: 1.772
batch start
#iterations: 53
currently lose_sum: 92.66693019866943
time_elpased: 1.837
batch start
#iterations: 54
currently lose_sum: 92.77586209774017
time_elpased: 1.75
batch start
#iterations: 55
currently lose_sum: 92.7319084405899
time_elpased: 1.755
batch start
#iterations: 56
currently lose_sum: 92.10565096139908
time_elpased: 1.815
batch start
#iterations: 57
currently lose_sum: 92.72317683696747
time_elpased: 1.697
batch start
#iterations: 58
currently lose_sum: 92.57326030731201
time_elpased: 1.735
batch start
#iterations: 59
currently lose_sum: 92.1433635354042
time_elpased: 1.759
start validation test
0.659072164948
0.643957675886
0.714079868259
0.677208394339
0.65898128068
59.841
batch start
#iterations: 60
currently lose_sum: 91.95584166049957
time_elpased: 1.705
batch start
#iterations: 61
currently lose_sum: 92.75149911642075
time_elpased: 1.729
batch start
#iterations: 62
currently lose_sum: 91.74283427000046
time_elpased: 1.791
batch start
#iterations: 63
currently lose_sum: 91.86465471982956
time_elpased: 1.793
batch start
#iterations: 64
currently lose_sum: 91.9177798628807
time_elpased: 1.744
batch start
#iterations: 65
currently lose_sum: 92.12539494037628
time_elpased: 1.772
batch start
#iterations: 66
currently lose_sum: 91.50820910930634
time_elpased: 1.745
batch start
#iterations: 67
currently lose_sum: 92.20293128490448
time_elpased: 1.791
batch start
#iterations: 68
currently lose_sum: 91.5139821767807
time_elpased: 1.738
batch start
#iterations: 69
currently lose_sum: 91.39968490600586
time_elpased: 1.754
batch start
#iterations: 70
currently lose_sum: 91.1696086525917
time_elpased: 1.735
batch start
#iterations: 71
currently lose_sum: 91.26333028078079
time_elpased: 1.821
batch start
#iterations: 72
currently lose_sum: 91.1166181564331
time_elpased: 1.769
batch start
#iterations: 73
currently lose_sum: 91.24965327978134
time_elpased: 1.81
batch start
#iterations: 74
currently lose_sum: 90.85292983055115
time_elpased: 1.752
batch start
#iterations: 75
currently lose_sum: 91.24177956581116
time_elpased: 1.755
batch start
#iterations: 76
currently lose_sum: 90.86608880758286
time_elpased: 1.762
batch start
#iterations: 77
currently lose_sum: 91.04084664583206
time_elpased: 1.769
batch start
#iterations: 78
currently lose_sum: 90.91822719573975
time_elpased: 1.756
batch start
#iterations: 79
currently lose_sum: 90.91030091047287
time_elpased: 1.752
start validation test
0.665412371134
0.657271042622
0.693598188555
0.674946166558
0.665365802249
59.553
batch start
#iterations: 80
currently lose_sum: 90.62016242742538
time_elpased: 1.744
batch start
#iterations: 81
currently lose_sum: 90.42189806699753
time_elpased: 1.742
batch start
#iterations: 82
currently lose_sum: 91.15817993879318
time_elpased: 1.72
batch start
#iterations: 83
currently lose_sum: 90.60145425796509
time_elpased: 1.753
batch start
#iterations: 84
currently lose_sum: 90.1461523771286
time_elpased: 1.768
batch start
#iterations: 85
currently lose_sum: 90.55984836816788
time_elpased: 1.727
batch start
#iterations: 86
currently lose_sum: 89.79815554618835
time_elpased: 1.817
batch start
#iterations: 87
currently lose_sum: 90.025430560112
time_elpased: 1.742
batch start
#iterations: 88
currently lose_sum: 89.70664721727371
time_elpased: 1.74
batch start
#iterations: 89
currently lose_sum: 90.24915289878845
time_elpased: 1.779
batch start
#iterations: 90
currently lose_sum: 89.85284966230392
time_elpased: 1.762
batch start
#iterations: 91
currently lose_sum: 89.8736321926117
time_elpased: 1.789
batch start
#iterations: 92
currently lose_sum: 89.73053634166718
time_elpased: 1.825
batch start
#iterations: 93
currently lose_sum: 90.07225120067596
time_elpased: 1.812
batch start
#iterations: 94
currently lose_sum: 90.16622567176819
time_elpased: 1.738
batch start
#iterations: 95
currently lose_sum: 89.22273248434067
time_elpased: 1.722
batch start
#iterations: 96
currently lose_sum: 89.58852982521057
time_elpased: 1.721
batch start
#iterations: 97
currently lose_sum: 89.21895390748978
time_elpased: 1.74
batch start
#iterations: 98
currently lose_sum: 89.27388459444046
time_elpased: 1.748
batch start
#iterations: 99
currently lose_sum: 89.26820141077042
time_elpased: 1.72
start validation test
0.660824742268
0.641900452489
0.730032935364
0.683135895213
0.660710395811
58.831
batch start
#iterations: 100
currently lose_sum: 89.62809377908707
time_elpased: 1.722
batch start
#iterations: 101
currently lose_sum: 89.3355518579483
time_elpased: 1.73
batch start
#iterations: 102
currently lose_sum: 89.37923502922058
time_elpased: 1.753
batch start
#iterations: 103
currently lose_sum: 89.22256052494049
time_elpased: 1.796
batch start
#iterations: 104
currently lose_sum: 88.82216638326645
time_elpased: 1.739
batch start
#iterations: 105
currently lose_sum: 89.28860986232758
time_elpased: 1.766
batch start
#iterations: 106
currently lose_sum: 88.55583852529526
time_elpased: 1.742
batch start
#iterations: 107
currently lose_sum: 88.75031280517578
time_elpased: 1.73
batch start
#iterations: 108
currently lose_sum: 89.03958731889725
time_elpased: 1.734
batch start
#iterations: 109
currently lose_sum: 89.4510987997055
time_elpased: 1.77
batch start
#iterations: 110
currently lose_sum: 88.99597436189651
time_elpased: 1.773
batch start
#iterations: 111
currently lose_sum: 88.95158421993256
time_elpased: 1.802
batch start
#iterations: 112
currently lose_sum: 88.10240960121155
time_elpased: 1.745
batch start
#iterations: 113
currently lose_sum: 88.4214271903038
time_elpased: 1.695
batch start
#iterations: 114
currently lose_sum: 88.79664593935013
time_elpased: 1.745
batch start
#iterations: 115
currently lose_sum: 88.61092042922974
time_elpased: 1.783
batch start
#iterations: 116
currently lose_sum: 88.36611580848694
time_elpased: 1.749
batch start
#iterations: 117
currently lose_sum: 88.20587366819382
time_elpased: 1.787
batch start
#iterations: 118
currently lose_sum: 88.26892030239105
time_elpased: 1.761
batch start
#iterations: 119
currently lose_sum: 88.69104069471359
time_elpased: 1.748
start validation test
0.670257731959
0.649114924971
0.743515850144
0.693115855121
0.670136694176
58.805
batch start
#iterations: 120
currently lose_sum: 88.31561464071274
time_elpased: 1.724
batch start
#iterations: 121
currently lose_sum: 87.80274051427841
time_elpased: 1.794
batch start
#iterations: 122
currently lose_sum: 87.71770393848419
time_elpased: 1.751
batch start
#iterations: 123
currently lose_sum: 88.4447511434555
time_elpased: 1.773
batch start
#iterations: 124
currently lose_sum: 88.39022123813629
time_elpased: 1.75
batch start
#iterations: 125
currently lose_sum: 86.99073523283005
time_elpased: 1.813
batch start
#iterations: 126
currently lose_sum: 87.46221625804901
time_elpased: 1.699
batch start
#iterations: 127
currently lose_sum: 87.54468774795532
time_elpased: 1.792
batch start
#iterations: 128
currently lose_sum: 87.54540807008743
time_elpased: 1.762
batch start
#iterations: 129
currently lose_sum: 87.4998209476471
time_elpased: 1.767
batch start
#iterations: 130
currently lose_sum: 87.72971707582474
time_elpased: 1.738
batch start
#iterations: 131
currently lose_sum: 87.73049318790436
time_elpased: 1.722
batch start
#iterations: 132
currently lose_sum: 86.96969538927078
time_elpased: 1.772
batch start
#iterations: 133
currently lose_sum: 87.66081118583679
time_elpased: 1.785
batch start
#iterations: 134
currently lose_sum: 87.82189291715622
time_elpased: 1.721
batch start
#iterations: 135
currently lose_sum: 87.1706223487854
time_elpased: 1.765
batch start
#iterations: 136
currently lose_sum: 87.2663437128067
time_elpased: 1.72
batch start
#iterations: 137
currently lose_sum: 87.29327577352524
time_elpased: 1.721
batch start
#iterations: 138
currently lose_sum: 87.14169931411743
time_elpased: 1.793
batch start
#iterations: 139
currently lose_sum: 86.57371246814728
time_elpased: 1.769
start validation test
0.66881443299
0.654666791992
0.716858789625
0.684352738885
0.668735053631
59.516
batch start
#iterations: 140
currently lose_sum: 86.9818280339241
time_elpased: 1.8
batch start
#iterations: 141
currently lose_sum: 86.41400611400604
time_elpased: 1.734
batch start
#iterations: 142
currently lose_sum: 86.79304206371307
time_elpased: 1.71
batch start
#iterations: 143
currently lose_sum: 86.86396217346191
time_elpased: 1.777
batch start
#iterations: 144
currently lose_sum: 85.9781157374382
time_elpased: 1.742
batch start
#iterations: 145
currently lose_sum: 86.57989585399628
time_elpased: 1.78
batch start
#iterations: 146
currently lose_sum: 86.27393621206284
time_elpased: 1.797
batch start
#iterations: 147
currently lose_sum: 86.39585107564926
time_elpased: 1.762
batch start
#iterations: 148
currently lose_sum: 86.22925156354904
time_elpased: 1.823
batch start
#iterations: 149
currently lose_sum: 86.42376416921616
time_elpased: 1.777
batch start
#iterations: 150
currently lose_sum: 86.60023021697998
time_elpased: 1.725
batch start
#iterations: 151
currently lose_sum: 86.20212799310684
time_elpased: 1.765
batch start
#iterations: 152
currently lose_sum: 86.14368852972984
time_elpased: 1.691
batch start
#iterations: 153
currently lose_sum: 86.40051436424255
time_elpased: 1.801
batch start
#iterations: 154
currently lose_sum: 86.19418853521347
time_elpased: 1.717
batch start
#iterations: 155
currently lose_sum: 86.45292341709137
time_elpased: 1.702
batch start
#iterations: 156
currently lose_sum: 85.77584582567215
time_elpased: 1.748
batch start
#iterations: 157
currently lose_sum: 85.37780827283859
time_elpased: 1.801
batch start
#iterations: 158
currently lose_sum: 85.66783332824707
time_elpased: 1.767
batch start
#iterations: 159
currently lose_sum: 85.5127803683281
time_elpased: 1.804
start validation test
0.646237113402
0.640944570695
0.667661589131
0.654030347331
0.646201715673
60.356
batch start
#iterations: 160
currently lose_sum: 85.72084426879883
time_elpased: 1.743
batch start
#iterations: 161
currently lose_sum: 86.00523936748505
time_elpased: 1.763
batch start
#iterations: 162
currently lose_sum: 85.43497031927109
time_elpased: 1.736
batch start
#iterations: 163
currently lose_sum: 84.9614799618721
time_elpased: 1.787
batch start
#iterations: 164
currently lose_sum: 85.08728271722794
time_elpased: 1.731
batch start
#iterations: 165
currently lose_sum: 85.38814932107925
time_elpased: 1.767
batch start
#iterations: 166
currently lose_sum: 84.54884606599808
time_elpased: 1.739
batch start
#iterations: 167
currently lose_sum: 85.1899881362915
time_elpased: 1.759
batch start
#iterations: 168
currently lose_sum: 85.23230057954788
time_elpased: 1.735
batch start
#iterations: 169
currently lose_sum: 84.66884869337082
time_elpased: 1.792
batch start
#iterations: 170
currently lose_sum: 84.80515313148499
time_elpased: 1.732
batch start
#iterations: 171
currently lose_sum: 84.68942940235138
time_elpased: 1.74
batch start
#iterations: 172
currently lose_sum: 84.60260224342346
time_elpased: 1.734
batch start
#iterations: 173
currently lose_sum: 84.41280037164688
time_elpased: 1.735
batch start
#iterations: 174
currently lose_sum: 84.0489020049572
time_elpased: 1.758
batch start
#iterations: 175
currently lose_sum: 84.30462890863419
time_elpased: 1.782
batch start
#iterations: 176
currently lose_sum: 84.43296027183533
time_elpased: 1.792
batch start
#iterations: 177
currently lose_sum: 84.29137599468231
time_elpased: 1.739
batch start
#iterations: 178
currently lose_sum: 83.61132502555847
time_elpased: 1.776
batch start
#iterations: 179
currently lose_sum: 83.51476496458054
time_elpased: 1.786
start validation test
0.64381443299
0.66264780895
0.588307945657
0.623269000109
0.643906141354
62.365
batch start
#iterations: 180
currently lose_sum: 84.29360407590866
time_elpased: 1.71
batch start
#iterations: 181
currently lose_sum: 83.7466470003128
time_elpased: 1.781
batch start
#iterations: 182
currently lose_sum: 84.47198873758316
time_elpased: 1.711
batch start
#iterations: 183
currently lose_sum: 83.6500752568245
time_elpased: 1.738
batch start
#iterations: 184
currently lose_sum: 84.06808096170425
time_elpased: 1.74
batch start
#iterations: 185
currently lose_sum: 83.38812917470932
time_elpased: 1.736
batch start
#iterations: 186
currently lose_sum: 83.18993729352951
time_elpased: 1.775
batch start
#iterations: 187
currently lose_sum: 82.80911359190941
time_elpased: 1.782
batch start
#iterations: 188
currently lose_sum: 83.84327399730682
time_elpased: 1.756
batch start
#iterations: 189
currently lose_sum: 82.93610551953316
time_elpased: 1.741
batch start
#iterations: 190
currently lose_sum: 82.35997286438942
time_elpased: 1.752
batch start
#iterations: 191
currently lose_sum: 82.63506129384041
time_elpased: 1.752
batch start
#iterations: 192
currently lose_sum: 83.242251932621
time_elpased: 1.709
batch start
#iterations: 193
currently lose_sum: 83.04704278707504
time_elpased: 1.756
batch start
#iterations: 194
currently lose_sum: 82.47447603940964
time_elpased: 1.774
batch start
#iterations: 195
currently lose_sum: 82.74938571453094
time_elpased: 1.741
batch start
#iterations: 196
currently lose_sum: 81.98608127236366
time_elpased: 1.768
batch start
#iterations: 197
currently lose_sum: 82.37105840444565
time_elpased: 1.709
batch start
#iterations: 198
currently lose_sum: 81.37132427096367
time_elpased: 1.759
batch start
#iterations: 199
currently lose_sum: 81.07640171051025
time_elpased: 1.764
start validation test
0.634536082474
0.649680802554
0.586558254426
0.616508005193
0.634615351913
64.583
batch start
#iterations: 200
currently lose_sum: 82.20990061759949
time_elpased: 1.721
batch start
#iterations: 201
currently lose_sum: 81.17010205984116
time_elpased: 1.724
batch start
#iterations: 202
currently lose_sum: 81.87304040789604
time_elpased: 1.763
batch start
#iterations: 203
currently lose_sum: 81.97009721398354
time_elpased: 1.745
batch start
#iterations: 204
currently lose_sum: 82.00716206431389
time_elpased: 1.74
batch start
#iterations: 205
currently lose_sum: 81.93704605102539
time_elpased: 1.76
batch start
#iterations: 206
currently lose_sum: 81.07563653588295
time_elpased: 1.771
batch start
#iterations: 207
currently lose_sum: 81.76892113685608
time_elpased: 1.831
batch start
#iterations: 208
currently lose_sum: 81.03502443432808
time_elpased: 1.729
batch start
#iterations: 209
currently lose_sum: 80.29876157641411
time_elpased: 1.789
batch start
#iterations: 210
currently lose_sum: 80.32421216368675
time_elpased: 1.783
batch start
#iterations: 211
currently lose_sum: 80.2906908094883
time_elpased: 1.731
batch start
#iterations: 212
currently lose_sum: 81.37965926527977
time_elpased: 1.726
batch start
#iterations: 213
currently lose_sum: 80.54955896735191
time_elpased: 1.776
batch start
#iterations: 214
currently lose_sum: 80.47524645924568
time_elpased: 1.751
batch start
#iterations: 215
currently lose_sum: 79.70026206970215
time_elpased: 1.702
batch start
#iterations: 216
currently lose_sum: 80.41096976399422
time_elpased: 1.727
batch start
#iterations: 217
currently lose_sum: 79.98955291509628
time_elpased: 1.745
batch start
#iterations: 218
currently lose_sum: 80.35619878768921
time_elpased: 1.747
batch start
#iterations: 219
currently lose_sum: 79.77099230885506
time_elpased: 1.841
start validation test
0.643195876289
0.651617104406
0.617949773569
0.634337031167
0.643237588148
66.097
batch start
#iterations: 220
currently lose_sum: 79.4838342666626
time_elpased: 1.824
batch start
#iterations: 221
currently lose_sum: 79.30741420388222
time_elpased: 1.825
batch start
#iterations: 222
currently lose_sum: 78.72954007983208
time_elpased: 1.774
batch start
#iterations: 223
currently lose_sum: 78.93093386292458
time_elpased: 1.792
batch start
#iterations: 224
currently lose_sum: 79.78678050637245
time_elpased: 1.73
batch start
#iterations: 225
currently lose_sum: 79.13785928487778
time_elpased: 1.743
batch start
#iterations: 226
currently lose_sum: 78.73026838898659
time_elpased: 1.806
batch start
#iterations: 227
currently lose_sum: 78.48135402798653
time_elpased: 1.742
batch start
#iterations: 228
currently lose_sum: 77.64511665701866
time_elpased: 1.727
batch start
#iterations: 229
currently lose_sum: 78.26731407642365
time_elpased: 1.75
batch start
#iterations: 230
currently lose_sum: 78.8408074080944
time_elpased: 1.769
batch start
#iterations: 231
currently lose_sum: 78.67720985412598
time_elpased: 1.755
batch start
#iterations: 232
currently lose_sum: 78.36936807632446
time_elpased: 1.76
batch start
#iterations: 233
currently lose_sum: 78.69690638780594
time_elpased: 1.753
batch start
#iterations: 234
currently lose_sum: 78.3729567527771
time_elpased: 1.812
batch start
#iterations: 235
currently lose_sum: 77.90337067842484
time_elpased: 1.75
batch start
#iterations: 236
currently lose_sum: 77.8885969221592
time_elpased: 1.728
batch start
#iterations: 237
currently lose_sum: 78.38955852389336
time_elpased: 1.782
batch start
#iterations: 238
currently lose_sum: 78.18651196360588
time_elpased: 1.737
batch start
#iterations: 239
currently lose_sum: 77.61801508069038
time_elpased: 1.724
start validation test
0.627010309278
0.649397590361
0.554755043228
0.598357015986
0.627129690139
68.748
batch start
#iterations: 240
currently lose_sum: 77.16987127065659
time_elpased: 1.782
batch start
#iterations: 241
currently lose_sum: 77.37960186600685
time_elpased: 1.73
batch start
#iterations: 242
currently lose_sum: 76.66159850358963
time_elpased: 1.755
batch start
#iterations: 243
currently lose_sum: 76.19707670807838
time_elpased: 1.751
batch start
#iterations: 244
currently lose_sum: 77.05534663796425
time_elpased: 1.735
batch start
#iterations: 245
currently lose_sum: 76.44207856059074
time_elpased: 1.707
batch start
#iterations: 246
currently lose_sum: 75.3994075357914
time_elpased: 1.777
batch start
#iterations: 247
currently lose_sum: 76.50850197672844
time_elpased: 1.736
batch start
#iterations: 248
currently lose_sum: 76.35040166974068
time_elpased: 1.725
batch start
#iterations: 249
currently lose_sum: 76.33392161130905
time_elpased: 1.761
batch start
#iterations: 250
currently lose_sum: 75.66572171449661
time_elpased: 1.753
batch start
#iterations: 251
currently lose_sum: 75.39159691333771
time_elpased: 1.751
batch start
#iterations: 252
currently lose_sum: 75.56757733225822
time_elpased: 1.766
batch start
#iterations: 253
currently lose_sum: 75.22732675075531
time_elpased: 1.709
batch start
#iterations: 254
currently lose_sum: 75.42015114426613
time_elpased: 1.764
batch start
#iterations: 255
currently lose_sum: 75.36949065327644
time_elpased: 1.742
batch start
#iterations: 256
currently lose_sum: 75.06649833917618
time_elpased: 1.748
batch start
#iterations: 257
currently lose_sum: 74.53737035393715
time_elpased: 1.783
batch start
#iterations: 258
currently lose_sum: 74.65418717265129
time_elpased: 1.722
batch start
#iterations: 259
currently lose_sum: 75.04217115044594
time_elpased: 1.724
start validation test
0.608969072165
0.641059602649
0.498147385755
0.560639406927
0.609152172845
73.442
batch start
#iterations: 260
currently lose_sum: 75.25478139519691
time_elpased: 1.738
batch start
#iterations: 261
currently lose_sum: 74.71808889508247
time_elpased: 1.733
batch start
#iterations: 262
currently lose_sum: 73.15132156014442
time_elpased: 1.751
batch start
#iterations: 263
currently lose_sum: 73.98562854528427
time_elpased: 1.79
batch start
#iterations: 264
currently lose_sum: 74.09943109750748
time_elpased: 1.759
batch start
#iterations: 265
currently lose_sum: 73.44923982024193
time_elpased: 1.739
batch start
#iterations: 266
currently lose_sum: 73.10633760690689
time_elpased: 1.751
batch start
#iterations: 267
currently lose_sum: 73.93962368369102
time_elpased: 1.762
batch start
#iterations: 268
currently lose_sum: 72.7190363407135
time_elpased: 1.772
batch start
#iterations: 269
currently lose_sum: 72.37732592225075
time_elpased: 1.751
batch start
#iterations: 270
currently lose_sum: 72.92280197143555
time_elpased: 1.727
batch start
#iterations: 271
currently lose_sum: 72.48719123005867
time_elpased: 1.74
batch start
#iterations: 272
currently lose_sum: 73.0847062766552
time_elpased: 1.774
batch start
#iterations: 273
currently lose_sum: 72.30489975214005
time_elpased: 1.774
batch start
#iterations: 274
currently lose_sum: 72.75186216831207
time_elpased: 1.751
batch start
#iterations: 275
currently lose_sum: 72.64699518680573
time_elpased: 1.733
batch start
#iterations: 276
currently lose_sum: 72.32656872272491
time_elpased: 1.748
batch start
#iterations: 277
currently lose_sum: 71.829306691885
time_elpased: 1.78
batch start
#iterations: 278
currently lose_sum: 72.34751060605049
time_elpased: 1.731
batch start
#iterations: 279
currently lose_sum: 71.78828775882721
time_elpased: 1.785
start validation test
0.607680412371
0.639348603204
0.497015232606
0.55926805258
0.607863254469
76.551
batch start
#iterations: 280
currently lose_sum: 71.18072363734245
time_elpased: 1.738
batch start
#iterations: 281
currently lose_sum: 71.1277442574501
time_elpased: 1.738
batch start
#iterations: 282
currently lose_sum: 71.19201412796974
time_elpased: 1.763
batch start
#iterations: 283
currently lose_sum: 71.34084510803223
time_elpased: 1.772
batch start
#iterations: 284
currently lose_sum: 70.75369265675545
time_elpased: 1.761
batch start
#iterations: 285
currently lose_sum: 70.48178890347481
time_elpased: 1.75
batch start
#iterations: 286
currently lose_sum: 70.6176028251648
time_elpased: 1.718
batch start
#iterations: 287
currently lose_sum: 69.81869968771935
time_elpased: 1.772
batch start
#iterations: 288
currently lose_sum: 71.03935304284096
time_elpased: 1.723
batch start
#iterations: 289
currently lose_sum: 69.7370413839817
time_elpased: 1.734
batch start
#iterations: 290
currently lose_sum: 70.37713289260864
time_elpased: 1.744
batch start
#iterations: 291
currently lose_sum: 69.4969912469387
time_elpased: 1.717
batch start
#iterations: 292
currently lose_sum: 70.22756633162498
time_elpased: 1.762
batch start
#iterations: 293
currently lose_sum: 68.5715401172638
time_elpased: 1.79
batch start
#iterations: 294
currently lose_sum: 68.88315126299858
time_elpased: 1.757
batch start
#iterations: 295
currently lose_sum: 69.0050103366375
time_elpased: 1.746
batch start
#iterations: 296
currently lose_sum: 69.49305152893066
time_elpased: 1.772
batch start
#iterations: 297
currently lose_sum: 68.42123410105705
time_elpased: 1.697
batch start
#iterations: 298
currently lose_sum: 69.25441998243332
time_elpased: 1.756
batch start
#iterations: 299
currently lose_sum: 69.471778601408
time_elpased: 1.734
start validation test
0.599793814433
0.649007633588
0.437525730753
0.522685355957
0.600061915356
83.558
batch start
#iterations: 300
currently lose_sum: 69.06030598282814
time_elpased: 1.732
batch start
#iterations: 301
currently lose_sum: 68.10067555308342
time_elpased: 1.774
batch start
#iterations: 302
currently lose_sum: 67.98093581199646
time_elpased: 1.727
batch start
#iterations: 303
currently lose_sum: 68.28759598731995
time_elpased: 1.785
batch start
#iterations: 304
currently lose_sum: 67.59294655919075
time_elpased: 1.722
batch start
#iterations: 305
currently lose_sum: 67.49198454618454
time_elpased: 1.759
batch start
#iterations: 306
currently lose_sum: 68.1643598973751
time_elpased: 1.753
batch start
#iterations: 307
currently lose_sum: 67.62766328454018
time_elpased: 1.743
batch start
#iterations: 308
currently lose_sum: 67.39511919021606
time_elpased: 1.702
batch start
#iterations: 309
currently lose_sum: 66.30663606524467
time_elpased: 1.736
batch start
#iterations: 310
currently lose_sum: 67.65964895486832
time_elpased: 1.761
batch start
#iterations: 311
currently lose_sum: 66.58173924684525
time_elpased: 1.759
batch start
#iterations: 312
currently lose_sum: 66.3163635134697
time_elpased: 1.759
batch start
#iterations: 313
currently lose_sum: 66.26838463544846
time_elpased: 1.721
batch start
#iterations: 314
currently lose_sum: 66.27675697207451
time_elpased: 1.749
batch start
#iterations: 315
currently lose_sum: 66.18131795525551
time_elpased: 1.71
batch start
#iterations: 316
currently lose_sum: 65.49631148576736
time_elpased: 1.756
batch start
#iterations: 317
currently lose_sum: 65.70461720228195
time_elpased: 1.72
batch start
#iterations: 318
currently lose_sum: 65.97162503004074
time_elpased: 1.791
batch start
#iterations: 319
currently lose_sum: 66.2803190946579
time_elpased: 1.711
start validation test
0.590773195876
0.634234778667
0.432070811033
0.513988368534
0.591035405517
86.930
batch start
#iterations: 320
currently lose_sum: 65.86654895544052
time_elpased: 1.77
batch start
#iterations: 321
currently lose_sum: 65.22323259711266
time_elpased: 1.756
batch start
#iterations: 322
currently lose_sum: 64.68000549077988
time_elpased: 1.785
batch start
#iterations: 323
currently lose_sum: 64.4939895272255
time_elpased: 1.794
batch start
#iterations: 324
currently lose_sum: 64.51697194576263
time_elpased: 1.798
batch start
#iterations: 325
currently lose_sum: 65.1677570939064
time_elpased: 1.718
batch start
#iterations: 326
currently lose_sum: 64.12340247631073
time_elpased: 1.749
batch start
#iterations: 327
currently lose_sum: 63.541074335575104
time_elpased: 1.779
batch start
#iterations: 328
currently lose_sum: 63.79788354039192
time_elpased: 1.73
batch start
#iterations: 329
currently lose_sum: 64.48553609848022
time_elpased: 1.745
batch start
#iterations: 330
currently lose_sum: 64.31934654712677
time_elpased: 1.797
batch start
#iterations: 331
currently lose_sum: 63.08420506119728
time_elpased: 1.729
batch start
#iterations: 332
currently lose_sum: 64.28834143280983
time_elpased: 1.814
batch start
#iterations: 333
currently lose_sum: 63.11731439828873
time_elpased: 1.839
batch start
#iterations: 334
currently lose_sum: 63.14573088288307
time_elpased: 1.763
batch start
#iterations: 335
currently lose_sum: 62.63366371393204
time_elpased: 1.73
batch start
#iterations: 336
currently lose_sum: 63.114581882953644
time_elpased: 1.764
batch start
#iterations: 337
currently lose_sum: 62.2488195002079
time_elpased: 1.779
batch start
#iterations: 338
currently lose_sum: 62.58795124292374
time_elpased: 1.718
batch start
#iterations: 339
currently lose_sum: 62.630488097667694
time_elpased: 1.739
start validation test
0.57793814433
0.639670932358
0.360127624537
0.460819175556
0.578298013012
98.159
batch start
#iterations: 340
currently lose_sum: 61.702396273612976
time_elpased: 1.741
batch start
#iterations: 341
currently lose_sum: 62.13444373011589
time_elpased: 1.747
batch start
#iterations: 342
currently lose_sum: 61.8132247030735
time_elpased: 1.769
batch start
#iterations: 343
currently lose_sum: 61.7902390062809
time_elpased: 1.745
batch start
#iterations: 344
currently lose_sum: 61.999479323625565
time_elpased: 1.795
batch start
#iterations: 345
currently lose_sum: 61.726458132267
time_elpased: 1.765
batch start
#iterations: 346
currently lose_sum: 61.20797887444496
time_elpased: 1.75
batch start
#iterations: 347
currently lose_sum: 60.903107315301895
time_elpased: 1.756
batch start
#iterations: 348
currently lose_sum: 61.62740361690521
time_elpased: 1.743
batch start
#iterations: 349
currently lose_sum: 60.622781842947006
time_elpased: 1.766
batch start
#iterations: 350
currently lose_sum: 61.07741221785545
time_elpased: 1.771
batch start
#iterations: 351
currently lose_sum: 60.5577487051487
time_elpased: 1.707
batch start
#iterations: 352
currently lose_sum: 60.34859073162079
time_elpased: 1.723
batch start
#iterations: 353
currently lose_sum: 59.48592686653137
time_elpased: 1.813
batch start
#iterations: 354
currently lose_sum: 60.53117561340332
time_elpased: 1.752
batch start
#iterations: 355
currently lose_sum: 61.09517738223076
time_elpased: 1.754
batch start
#iterations: 356
currently lose_sum: 59.61965569853783
time_elpased: 1.735
batch start
#iterations: 357
currently lose_sum: 58.98716872930527
time_elpased: 1.841
batch start
#iterations: 358
currently lose_sum: 59.80488249659538
time_elpased: 1.77
batch start
#iterations: 359
currently lose_sum: 59.54090994596481
time_elpased: 1.739
start validation test
0.571649484536
0.626849512811
0.357554549197
0.455367675973
0.572003214293
99.413
batch start
#iterations: 360
currently lose_sum: 59.172416657209396
time_elpased: 1.748
batch start
#iterations: 361
currently lose_sum: 58.62614765763283
time_elpased: 1.756
batch start
#iterations: 362
currently lose_sum: 58.73436287045479
time_elpased: 1.744
batch start
#iterations: 363
currently lose_sum: 58.988788932561874
time_elpased: 1.825
batch start
#iterations: 364
currently lose_sum: 59.106981456279755
time_elpased: 1.772
batch start
#iterations: 365
currently lose_sum: 57.58157780766487
time_elpased: 1.793
batch start
#iterations: 366
currently lose_sum: 58.79902493953705
time_elpased: 1.777
batch start
#iterations: 367
currently lose_sum: 59.074038565158844
time_elpased: 1.732
batch start
#iterations: 368
currently lose_sum: 58.23118457198143
time_elpased: 1.778
batch start
#iterations: 369
currently lose_sum: 57.516536593437195
time_elpased: 1.751
batch start
#iterations: 370
currently lose_sum: 58.12618041038513
time_elpased: 1.742
batch start
#iterations: 371
currently lose_sum: 57.95763656497002
time_elpased: 1.769
batch start
#iterations: 372
currently lose_sum: 58.90101882815361
time_elpased: 1.814
batch start
#iterations: 373
currently lose_sum: 56.82716080546379
time_elpased: 1.742
batch start
#iterations: 374
currently lose_sum: 57.56281542778015
time_elpased: 1.77
batch start
#iterations: 375
currently lose_sum: 57.28750783205032
time_elpased: 1.791
batch start
#iterations: 376
currently lose_sum: 57.26278439164162
time_elpased: 1.78
batch start
#iterations: 377
currently lose_sum: 56.567164838314056
time_elpased: 1.776
batch start
#iterations: 378
currently lose_sum: 56.50875121355057
time_elpased: 1.726
batch start
#iterations: 379
currently lose_sum: 56.18351498246193
time_elpased: 1.733
start validation test
0.560927835052
0.620136381869
0.318237958007
0.420623044484
0.561328809652
109.693
batch start
#iterations: 380
currently lose_sum: 56.45179983973503
time_elpased: 1.801
batch start
#iterations: 381
currently lose_sum: 56.24469551444054
time_elpased: 1.742
batch start
#iterations: 382
currently lose_sum: 55.73063197731972
time_elpased: 1.731
batch start
#iterations: 383
currently lose_sum: 55.02462089061737
time_elpased: 1.745
batch start
#iterations: 384
currently lose_sum: 54.91067451238632
time_elpased: 1.768
batch start
#iterations: 385
currently lose_sum: 55.24399891495705
time_elpased: 1.753
batch start
#iterations: 386
currently lose_sum: 55.9016695022583
time_elpased: 1.723
batch start
#iterations: 387
currently lose_sum: 55.15662184357643
time_elpased: 1.721
batch start
#iterations: 388
currently lose_sum: 54.260035425424576
time_elpased: 1.774
batch start
#iterations: 389
currently lose_sum: 54.2678444981575
time_elpased: 1.767
batch start
#iterations: 390
currently lose_sum: 54.63815161585808
time_elpased: 1.783
batch start
#iterations: 391
currently lose_sum: 55.73615399003029
time_elpased: 1.747
batch start
#iterations: 392
currently lose_sum: 54.42089834809303
time_elpased: 1.767
batch start
#iterations: 393
currently lose_sum: 54.15783470869064
time_elpased: 1.716
batch start
#iterations: 394
currently lose_sum: 53.97850567102432
time_elpased: 1.747
batch start
#iterations: 395
currently lose_sum: 52.92442661523819
time_elpased: 1.763
batch start
#iterations: 396
currently lose_sum: 53.71300330758095
time_elpased: 1.732
batch start
#iterations: 397
currently lose_sum: 53.082702964544296
time_elpased: 1.751
batch start
#iterations: 398
currently lose_sum: 52.949114948511124
time_elpased: 1.721
batch start
#iterations: 399
currently lose_sum: 53.3619627058506
time_elpased: 1.765
start validation test
0.559175257732
0.628250330542
0.293433511733
0.400028062298
0.559614318857
116.295
acc: 0.683
pre: 0.659
rec: 0.761
F1: 0.706
auc: 0.738
