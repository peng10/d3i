start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.16491776704788
time_elpased: 3.047
batch start
#iterations: 1
currently lose_sum: 99.8106340765953
time_elpased: 2.9
batch start
#iterations: 2
currently lose_sum: 99.8935472369194
time_elpased: 2.918
batch start
#iterations: 3
currently lose_sum: 99.81051176786423
time_elpased: 2.756
batch start
#iterations: 4
currently lose_sum: 99.61460542678833
time_elpased: 2.771
batch start
#iterations: 5
currently lose_sum: 99.63260650634766
time_elpased: 3.14
batch start
#iterations: 6
currently lose_sum: 99.65649253129959
time_elpased: 4.016
batch start
#iterations: 7
currently lose_sum: 99.33593988418579
time_elpased: 3.928
batch start
#iterations: 8
currently lose_sum: 99.43979120254517
time_elpased: 3.549
batch start
#iterations: 9
currently lose_sum: 99.39034456014633
time_elpased: 3.741
batch start
#iterations: 10
currently lose_sum: 99.3847696185112
time_elpased: 3.105
batch start
#iterations: 11
currently lose_sum: 99.35811227560043
time_elpased: 3.139
batch start
#iterations: 12
currently lose_sum: 99.1550931930542
time_elpased: 3.636
batch start
#iterations: 13
currently lose_sum: 99.04862123727798
time_elpased: 3.189
batch start
#iterations: 14
currently lose_sum: 99.15518206357956
time_elpased: 3.594
batch start
#iterations: 15
currently lose_sum: 99.12549954652786
time_elpased: 2.813
batch start
#iterations: 16
currently lose_sum: 98.97647178173065
time_elpased: 3.436
batch start
#iterations: 17
currently lose_sum: 99.00261294841766
time_elpased: 3.014
batch start
#iterations: 18
currently lose_sum: 98.85881119966507
time_elpased: 2.958
batch start
#iterations: 19
currently lose_sum: 98.8171374797821
time_elpased: 3.11
start validation test
0.612474226804
0.634924530617
0.532468868992
0.579200716445
0.61261468855
64.969
batch start
#iterations: 20
currently lose_sum: 98.982397377491
time_elpased: 3.178
batch start
#iterations: 21
currently lose_sum: 98.61845022439957
time_elpased: 3.12
batch start
#iterations: 22
currently lose_sum: 98.8240550160408
time_elpased: 3.23
batch start
#iterations: 23
currently lose_sum: 98.41895508766174
time_elpased: 3.255
batch start
#iterations: 24
currently lose_sum: 98.27530437707901
time_elpased: 2.971
batch start
#iterations: 25
currently lose_sum: 98.16850107908249
time_elpased: 2.871
batch start
#iterations: 26
currently lose_sum: 98.00460070371628
time_elpased: 2.593
batch start
#iterations: 27
currently lose_sum: 98.50348991155624
time_elpased: 2.444
batch start
#iterations: 28
currently lose_sum: 98.18668031692505
time_elpased: 3.005
batch start
#iterations: 29
currently lose_sum: 97.97135108709335
time_elpased: 3.157
batch start
#iterations: 30
currently lose_sum: 97.74185800552368
time_elpased: 3.115
batch start
#iterations: 31
currently lose_sum: 98.03709131479263
time_elpased: 3.029
batch start
#iterations: 32
currently lose_sum: 97.63021099567413
time_elpased: 2.758
batch start
#iterations: 33
currently lose_sum: 97.69658470153809
time_elpased: 2.87
batch start
#iterations: 34
currently lose_sum: 97.56737118959427
time_elpased: 2.858
batch start
#iterations: 35
currently lose_sum: 97.63655310869217
time_elpased: 3.061
batch start
#iterations: 36
currently lose_sum: 97.03648167848587
time_elpased: 3.024
batch start
#iterations: 37
currently lose_sum: 97.09275603294373
time_elpased: 3.016
batch start
#iterations: 38
currently lose_sum: 96.8384495973587
time_elpased: 2.839
batch start
#iterations: 39
currently lose_sum: 97.092804312706
time_elpased: 3.057
start validation test
0.649639175258
0.664897221595
0.605845425543
0.633999246136
0.64971606194
61.514
batch start
#iterations: 40
currently lose_sum: 97.44009125232697
time_elpased: 2.911
batch start
#iterations: 41
currently lose_sum: 97.50954538583755
time_elpased: 2.95
batch start
#iterations: 42
currently lose_sum: 96.71783775091171
time_elpased: 2.887
batch start
#iterations: 43
currently lose_sum: 96.42807823419571
time_elpased: 2.612
batch start
#iterations: 44
currently lose_sum: 96.66994017362595
time_elpased: 3.188
batch start
#iterations: 45
currently lose_sum: 96.71614253520966
time_elpased: 2.86
batch start
#iterations: 46
currently lose_sum: 97.62161076068878
time_elpased: 2.975
batch start
#iterations: 47
currently lose_sum: 96.88790363073349
time_elpased: 2.903
batch start
#iterations: 48
currently lose_sum: 97.00554984807968
time_elpased: 3.035
batch start
#iterations: 49
currently lose_sum: 96.55513525009155
time_elpased: 2.948
batch start
#iterations: 50
currently lose_sum: 96.736945271492
time_elpased: 2.975
batch start
#iterations: 51
currently lose_sum: 96.74959164857864
time_elpased: 2.922
batch start
#iterations: 52
currently lose_sum: 96.47520518302917
time_elpased: 2.795
batch start
#iterations: 53
currently lose_sum: 96.0180771946907
time_elpased: 2.999
batch start
#iterations: 54
currently lose_sum: 97.43593138456345
time_elpased: 2.685
batch start
#iterations: 55
currently lose_sum: 96.17054122686386
time_elpased: 2.641
batch start
#iterations: 56
currently lose_sum: 96.08572560548782
time_elpased: 2.807
batch start
#iterations: 57
currently lose_sum: 96.08166551589966
time_elpased: 2.923
batch start
#iterations: 58
currently lose_sum: 95.86714535951614
time_elpased: 2.869
batch start
#iterations: 59
currently lose_sum: 96.72570896148682
time_elpased: 2.853
start validation test
0.661443298969
0.636355763402
0.756200473397
0.691121143717
0.661276938134
61.214
batch start
#iterations: 60
currently lose_sum: 95.82560431957245
time_elpased: 3.185
batch start
#iterations: 61
currently lose_sum: 96.27074098587036
time_elpased: 3.271
batch start
#iterations: 62
currently lose_sum: 95.77006989717484
time_elpased: 3.615
batch start
#iterations: 63
currently lose_sum: 96.30985748767853
time_elpased: 3.285
batch start
#iterations: 64
currently lose_sum: 95.8934879899025
time_elpased: 3.734
batch start
#iterations: 65
currently lose_sum: 95.96849292516708
time_elpased: 3.401
batch start
#iterations: 66
currently lose_sum: 96.1735251545906
time_elpased: 2.849
batch start
#iterations: 67
currently lose_sum: 95.84859877824783
time_elpased: 3.085
batch start
#iterations: 68
currently lose_sum: 96.13816338777542
time_elpased: 2.998
batch start
#iterations: 69
currently lose_sum: 95.1605293750763
time_elpased: 2.851
batch start
#iterations: 70
currently lose_sum: 95.2713994383812
time_elpased: 2.822
batch start
#iterations: 71
currently lose_sum: 95.69975471496582
time_elpased: 3.044
batch start
#iterations: 72
currently lose_sum: 96.16265588998795
time_elpased: 3.267
batch start
#iterations: 73
currently lose_sum: 95.36843645572662
time_elpased: 3.052
batch start
#iterations: 74
currently lose_sum: 95.81990188360214
time_elpased: 3.168
batch start
#iterations: 75
currently lose_sum: 95.57845735549927
time_elpased: 3.143
batch start
#iterations: 76
currently lose_sum: 96.2468056678772
time_elpased: 3.298
batch start
#iterations: 77
currently lose_sum: 95.77996063232422
time_elpased: 3.436
batch start
#iterations: 78
currently lose_sum: 95.5893424153328
time_elpased: 3.341
batch start
#iterations: 79
currently lose_sum: 95.69066148996353
time_elpased: 2.988
start validation test
0.659587628866
0.626678603402
0.792425645775
0.699872750409
0.659354411238
60.763
batch start
#iterations: 80
currently lose_sum: 94.89039331674576
time_elpased: 2.502
batch start
#iterations: 81
currently lose_sum: 95.08366596698761
time_elpased: 2.994
batch start
#iterations: 82
currently lose_sum: 95.3840320110321
time_elpased: 3.066
batch start
#iterations: 83
currently lose_sum: 95.1878634095192
time_elpased: 3.063
batch start
#iterations: 84
currently lose_sum: 94.54720467329025
time_elpased: 3.042
batch start
#iterations: 85
currently lose_sum: 94.99496233463287
time_elpased: 3.022
batch start
#iterations: 86
currently lose_sum: 94.97049683332443
time_elpased: 3.145
batch start
#iterations: 87
currently lose_sum: 94.75464272499084
time_elpased: 2.85
batch start
#iterations: 88
currently lose_sum: 95.05175846815109
time_elpased: 2.675
batch start
#iterations: 89
currently lose_sum: 95.24115085601807
time_elpased: 3.025
batch start
#iterations: 90
currently lose_sum: 94.38900429010391
time_elpased: 2.881
batch start
#iterations: 91
currently lose_sum: 95.19575589895248
time_elpased: 2.945
batch start
#iterations: 92
currently lose_sum: 94.58464008569717
time_elpased: 2.884
batch start
#iterations: 93
currently lose_sum: 95.32595831155777
time_elpased: 3.134
batch start
#iterations: 94
currently lose_sum: 94.82529187202454
time_elpased: 2.831
batch start
#iterations: 95
currently lose_sum: 95.33067786693573
time_elpased: 3.012
batch start
#iterations: 96
currently lose_sum: 95.20276069641113
time_elpased: 3.027
batch start
#iterations: 97
currently lose_sum: 94.28937631845474
time_elpased: 3.038
batch start
#iterations: 98
currently lose_sum: 94.5434975028038
time_elpased: 3.127
batch start
#iterations: 99
currently lose_sum: 94.04605495929718
time_elpased: 2.89
start validation test
0.666855670103
0.661057216393
0.687249150973
0.673898783995
0.666819866202
59.922
batch start
#iterations: 100
currently lose_sum: 94.51925414800644
time_elpased: 2.918
batch start
#iterations: 101
currently lose_sum: 94.52076441049576
time_elpased: 3.007
batch start
#iterations: 102
currently lose_sum: 94.59265893697739
time_elpased: 2.888
batch start
#iterations: 103
currently lose_sum: 94.19798493385315
time_elpased: 2.903
batch start
#iterations: 104
currently lose_sum: 94.54313731193542
time_elpased: 3.092
batch start
#iterations: 105
currently lose_sum: 94.61583650112152
time_elpased: 2.949
batch start
#iterations: 106
currently lose_sum: 94.74438959360123
time_elpased: 2.895
batch start
#iterations: 107
currently lose_sum: 94.451828956604
time_elpased: 2.877
batch start
#iterations: 108
currently lose_sum: 94.59531366825104
time_elpased: 2.863
batch start
#iterations: 109
currently lose_sum: 94.71095395088196
time_elpased: 2.749
batch start
#iterations: 110
currently lose_sum: 94.73034596443176
time_elpased: 3.064
batch start
#iterations: 111
currently lose_sum: 94.22286927700043
time_elpased: 2.981
batch start
#iterations: 112
currently lose_sum: 94.70905661582947
time_elpased: 2.756
batch start
#iterations: 113
currently lose_sum: 94.2158676981926
time_elpased: 2.48
batch start
#iterations: 114
currently lose_sum: 93.80735969543457
time_elpased: 2.298
batch start
#iterations: 115
currently lose_sum: 93.76930946111679
time_elpased: 2.353
batch start
#iterations: 116
currently lose_sum: 94.86467534303665
time_elpased: 2.701
batch start
#iterations: 117
currently lose_sum: 94.33482015132904
time_elpased: 2.615
batch start
#iterations: 118
currently lose_sum: 94.79471445083618
time_elpased: 2.859
batch start
#iterations: 119
currently lose_sum: 93.92842924594879
time_elpased: 3.364
start validation test
0.659639175258
0.655451277955
0.67562004734
0.665382861197
0.659611118372
59.906
batch start
#iterations: 120
currently lose_sum: 94.09368187189102
time_elpased: 3.571
batch start
#iterations: 121
currently lose_sum: 93.52760428190231
time_elpased: 3.317
batch start
#iterations: 122
currently lose_sum: 94.29121619462967
time_elpased: 3.3
batch start
#iterations: 123
currently lose_sum: 94.63868391513824
time_elpased: 3.333
batch start
#iterations: 124
currently lose_sum: 94.23159003257751
time_elpased: 3.161
batch start
#iterations: 125
currently lose_sum: 94.37727683782578
time_elpased: 3.453
batch start
#iterations: 126
currently lose_sum: 93.9564328789711
time_elpased: 3.37
batch start
#iterations: 127
currently lose_sum: 93.60096216201782
time_elpased: 3.196
batch start
#iterations: 128
currently lose_sum: 93.51289993524551
time_elpased: 3.171
batch start
#iterations: 129
currently lose_sum: 93.75297522544861
time_elpased: 3.66
batch start
#iterations: 130
currently lose_sum: 94.06723219156265
time_elpased: 3.62
batch start
#iterations: 131
currently lose_sum: 93.79118776321411
time_elpased: 3.204
batch start
#iterations: 132
currently lose_sum: 93.93949687480927
time_elpased: 3.316
batch start
#iterations: 133
currently lose_sum: 93.3911794424057
time_elpased: 3.443
batch start
#iterations: 134
currently lose_sum: 93.81903910636902
time_elpased: 3.121
batch start
#iterations: 135
currently lose_sum: 93.95309257507324
time_elpased: 2.9
batch start
#iterations: 136
currently lose_sum: 93.36849039793015
time_elpased: 2.832
batch start
#iterations: 137
currently lose_sum: 93.16137093305588
time_elpased: 3.528
batch start
#iterations: 138
currently lose_sum: 93.7220288515091
time_elpased: 3.202
batch start
#iterations: 139
currently lose_sum: 93.70698714256287
time_elpased: 3.243
start validation test
0.645979381443
0.676277688405
0.562416383658
0.614113945387
0.646126089175
60.743
batch start
#iterations: 140
currently lose_sum: 93.85500764846802
time_elpased: 3.039
batch start
#iterations: 141
currently lose_sum: 93.2161335349083
time_elpased: 3.384
batch start
#iterations: 142
currently lose_sum: 94.27368026971817
time_elpased: 3.086
batch start
#iterations: 143
currently lose_sum: 94.15856093168259
time_elpased: 3.068
batch start
#iterations: 144
currently lose_sum: 93.63424211740494
time_elpased: 3.032
batch start
#iterations: 145
currently lose_sum: 93.9920043349266
time_elpased: 2.844
batch start
#iterations: 146
currently lose_sum: 93.87027269601822
time_elpased: 2.805
batch start
#iterations: 147
currently lose_sum: 93.81776684522629
time_elpased: 2.795
batch start
#iterations: 148
currently lose_sum: 93.82909137010574
time_elpased: 3.132
batch start
#iterations: 149
currently lose_sum: 93.69723278284073
time_elpased: 3.209
batch start
#iterations: 150
currently lose_sum: 93.26708805561066
time_elpased: 3.121
batch start
#iterations: 151
currently lose_sum: 93.48567926883698
time_elpased: 3.303
batch start
#iterations: 152
currently lose_sum: 93.48578399419785
time_elpased: 3.149
batch start
#iterations: 153
currently lose_sum: 93.46756726503372
time_elpased: 2.988
batch start
#iterations: 154
currently lose_sum: 93.84049367904663
time_elpased: 2.933
batch start
#iterations: 155
currently lose_sum: 93.65221846103668
time_elpased: 3.026
batch start
#iterations: 156
currently lose_sum: 93.95541828870773
time_elpased: 2.823
batch start
#iterations: 157
currently lose_sum: 93.69998633861542
time_elpased: 2.798
batch start
#iterations: 158
currently lose_sum: 94.09517419338226
time_elpased: 2.864
batch start
#iterations: 159
currently lose_sum: 93.57537287473679
time_elpased: 3.067
start validation test
0.654896907216
0.6375386856
0.720798600391
0.676616915423
0.654781206629
60.390
batch start
#iterations: 160
currently lose_sum: 93.58940762281418
time_elpased: 3.243
batch start
#iterations: 161
currently lose_sum: 92.94022637605667
time_elpased: 2.927
batch start
#iterations: 162
currently lose_sum: 93.40757405757904
time_elpased: 3.065
batch start
#iterations: 163
currently lose_sum: 93.10923755168915
time_elpased: 2.951
batch start
#iterations: 164
currently lose_sum: 93.02467435598373
time_elpased: 2.547
batch start
#iterations: 165
currently lose_sum: 93.1737465262413
time_elpased: 2.683
batch start
#iterations: 166
currently lose_sum: 93.13573122024536
time_elpased: 3.043
batch start
#iterations: 167
currently lose_sum: 93.1078839302063
time_elpased: 2.722
batch start
#iterations: 168
currently lose_sum: 92.76002115011215
time_elpased: 2.924
batch start
#iterations: 169
currently lose_sum: 93.28485453128815
time_elpased: 3.083
batch start
#iterations: 170
currently lose_sum: 92.3157651424408
time_elpased: 2.973
batch start
#iterations: 171
currently lose_sum: 93.02403825521469
time_elpased: 3.058
batch start
#iterations: 172
currently lose_sum: 93.65659481287003
time_elpased: 2.943
batch start
#iterations: 173
currently lose_sum: 93.14165967702866
time_elpased: 3.009
batch start
#iterations: 174
currently lose_sum: 93.82682663202286
time_elpased: 2.935
batch start
#iterations: 175
currently lose_sum: 93.34373551607132
time_elpased: 3.107
batch start
#iterations: 176
currently lose_sum: 93.03311389684677
time_elpased: 3.033
batch start
#iterations: 177
currently lose_sum: 92.99038070440292
time_elpased: 2.947
batch start
#iterations: 178
currently lose_sum: 92.8189348578453
time_elpased: 2.72
batch start
#iterations: 179
currently lose_sum: 93.2434241771698
time_elpased: 2.712
start validation test
0.663556701031
0.683122847302
0.612328908099
0.645791501601
0.663646639323
59.423
batch start
#iterations: 180
currently lose_sum: 93.44467902183533
time_elpased: 3.006
batch start
#iterations: 181
currently lose_sum: 92.83541107177734
time_elpased: 3.0
batch start
#iterations: 182
currently lose_sum: 92.83438342809677
time_elpased: 2.973
batch start
#iterations: 183
currently lose_sum: 92.05424076318741
time_elpased: 3.61
batch start
#iterations: 184
currently lose_sum: 92.99917602539062
time_elpased: 3.546
batch start
#iterations: 185
currently lose_sum: 92.52528369426727
time_elpased: 3.314
batch start
#iterations: 186
currently lose_sum: 92.38259905576706
time_elpased: 3.274
batch start
#iterations: 187
currently lose_sum: 93.03206956386566
time_elpased: 3.778
batch start
#iterations: 188
currently lose_sum: 92.68067848682404
time_elpased: 3.154
batch start
#iterations: 189
currently lose_sum: 92.8595781326294
time_elpased: 3.032
batch start
#iterations: 190
currently lose_sum: 92.9548898935318
time_elpased: 3.533
batch start
#iterations: 191
currently lose_sum: 93.43661803007126
time_elpased: 3.612
batch start
#iterations: 192
currently lose_sum: 92.39867222309113
time_elpased: 3.432
batch start
#iterations: 193
currently lose_sum: 92.56182998418808
time_elpased: 3.183
batch start
#iterations: 194
currently lose_sum: 92.80109822750092
time_elpased: 3.215
batch start
#iterations: 195
currently lose_sum: 92.7804359793663
time_elpased: 2.633
batch start
#iterations: 196
currently lose_sum: 92.30425518751144
time_elpased: 2.651
batch start
#iterations: 197
currently lose_sum: 92.2100800871849
time_elpased: 2.75
batch start
#iterations: 198
currently lose_sum: 92.20102393627167
time_elpased: 2.702
batch start
#iterations: 199
currently lose_sum: 93.13242328166962
time_elpased: 2.544
start validation test
0.677886597938
0.65205191161
0.765256766492
0.704133327021
0.677733206131
58.087
batch start
#iterations: 200
currently lose_sum: 92.79334074258804
time_elpased: 3.368
batch start
#iterations: 201
currently lose_sum: 92.19176006317139
time_elpased: 3.716
batch start
#iterations: 202
currently lose_sum: 92.25673282146454
time_elpased: 3.465
batch start
#iterations: 203
currently lose_sum: 92.35996472835541
time_elpased: 3.172
batch start
#iterations: 204
currently lose_sum: 92.57505464553833
time_elpased: 3.21
batch start
#iterations: 205
currently lose_sum: 92.95826059579849
time_elpased: 2.749
batch start
#iterations: 206
currently lose_sum: 92.12072902917862
time_elpased: 2.715
batch start
#iterations: 207
currently lose_sum: 92.2383262515068
time_elpased: 3.484
batch start
#iterations: 208
currently lose_sum: 92.32423812150955
time_elpased: 3.263
batch start
#iterations: 209
currently lose_sum: 91.85032051801682
time_elpased: 2.892
batch start
#iterations: 210
currently lose_sum: 92.65075129270554
time_elpased: 3.256
batch start
#iterations: 211
currently lose_sum: 92.14563888311386
time_elpased: 3.172
batch start
#iterations: 212
currently lose_sum: 92.5030928850174
time_elpased: 2.941
batch start
#iterations: 213
currently lose_sum: 92.3753337264061
time_elpased: 3.179
batch start
#iterations: 214
currently lose_sum: 92.34832781553268
time_elpased: 2.925
batch start
#iterations: 215
currently lose_sum: 92.48429292440414
time_elpased: 3.136
batch start
#iterations: 216
currently lose_sum: 92.12971299886703
time_elpased: 3.253
batch start
#iterations: 217
currently lose_sum: 93.05044955015182
time_elpased: 2.811
batch start
#iterations: 218
currently lose_sum: 92.68692660331726
time_elpased: 2.858
batch start
#iterations: 219
currently lose_sum: 92.78100323677063
time_elpased: 2.919
start validation test
0.651391752577
0.63043094313
0.734691777298
0.678579915403
0.651245506536
60.107
batch start
#iterations: 220
currently lose_sum: 92.26408284902573
time_elpased: 3.037
batch start
#iterations: 221
currently lose_sum: 92.12922602891922
time_elpased: 2.912
batch start
#iterations: 222
currently lose_sum: 92.16866493225098
time_elpased: 2.819
batch start
#iterations: 223
currently lose_sum: 91.50788098573685
time_elpased: 3.153
batch start
#iterations: 224
currently lose_sum: 91.93645912408829
time_elpased: 2.764
batch start
#iterations: 225
currently lose_sum: 91.82737195491791
time_elpased: 3.03
batch start
#iterations: 226
currently lose_sum: 92.4508318901062
time_elpased: 2.911
batch start
#iterations: 227
currently lose_sum: 92.26056504249573
time_elpased: 2.906
batch start
#iterations: 228
currently lose_sum: 91.90128409862518
time_elpased: 2.759
batch start
#iterations: 229
currently lose_sum: 91.609255194664
time_elpased: 2.935
batch start
#iterations: 230
currently lose_sum: 92.06479150056839
time_elpased: 3.023
batch start
#iterations: 231
currently lose_sum: 92.18562036752701
time_elpased: 2.901
batch start
#iterations: 232
currently lose_sum: 92.12710303068161
time_elpased: 2.705
batch start
#iterations: 233
currently lose_sum: 91.48809152841568
time_elpased: 3.011
batch start
#iterations: 234
currently lose_sum: 92.03611838817596
time_elpased: 2.926
batch start
#iterations: 235
currently lose_sum: 91.65307122468948
time_elpased: 3.008
batch start
#iterations: 236
currently lose_sum: 92.07720077037811
time_elpased: 2.7
batch start
#iterations: 237
currently lose_sum: 92.18589454889297
time_elpased: 2.904
batch start
#iterations: 238
currently lose_sum: 92.11781430244446
time_elpased: 2.878
batch start
#iterations: 239
currently lose_sum: 93.26392459869385
time_elpased: 2.84
start validation test
0.641855670103
0.612661730002
0.774827621694
0.684267926929
0.641622217333
60.648
batch start
#iterations: 240
currently lose_sum: 91.51231980323792
time_elpased: 3.093
batch start
#iterations: 241
currently lose_sum: 92.16387647390366
time_elpased: 2.952
batch start
#iterations: 242
currently lose_sum: 92.22090089321136
time_elpased: 2.911
batch start
#iterations: 243
currently lose_sum: 92.12049424648285
time_elpased: 2.709
batch start
#iterations: 244
currently lose_sum: 91.79807060956955
time_elpased: 2.734
batch start
#iterations: 245
currently lose_sum: 91.58300572633743
time_elpased: 3.238
batch start
#iterations: 246
currently lose_sum: 91.67133951187134
time_elpased: 3.442
batch start
#iterations: 247
currently lose_sum: 91.61801946163177
time_elpased: 3.554
batch start
#iterations: 248
currently lose_sum: 92.00294041633606
time_elpased: 3.101
batch start
#iterations: 249
currently lose_sum: 92.17563426494598
time_elpased: 3.067
batch start
#iterations: 250
currently lose_sum: 91.55880051851273
time_elpased: 3.16
batch start
#iterations: 251
currently lose_sum: 91.4656091928482
time_elpased: 3.032
batch start
#iterations: 252
currently lose_sum: 91.9899086356163
time_elpased: 3.332
batch start
#iterations: 253
currently lose_sum: 91.39857459068298
time_elpased: 2.925
batch start
#iterations: 254
currently lose_sum: 90.88516491651535
time_elpased: 2.998
batch start
#iterations: 255
currently lose_sum: 91.37873911857605
time_elpased: 2.697
batch start
#iterations: 256
currently lose_sum: 90.9487881064415
time_elpased: 3.097
batch start
#iterations: 257
currently lose_sum: 91.42315274477005
time_elpased: 3.306
batch start
#iterations: 258
currently lose_sum: 91.47842067480087
time_elpased: 3.281
batch start
#iterations: 259
currently lose_sum: 91.81644994020462
time_elpased: 3.277
start validation test
0.687010309278
0.673786592925
0.727179170526
0.699465452386
0.686939786647
57.558
batch start
#iterations: 260
currently lose_sum: 91.23720228672028
time_elpased: 3.127
batch start
#iterations: 261
currently lose_sum: 91.02880370616913
time_elpased: 2.881
batch start
#iterations: 262
currently lose_sum: 92.04257225990295
time_elpased: 2.926
batch start
#iterations: 263
currently lose_sum: 90.91500782966614
time_elpased: 3.021
batch start
#iterations: 264
currently lose_sum: 92.30024534463882
time_elpased: 3.447
batch start
#iterations: 265
currently lose_sum: 91.08810955286026
time_elpased: 3.49
batch start
#iterations: 266
currently lose_sum: 91.11080992221832
time_elpased: 3.35
batch start
#iterations: 267
currently lose_sum: 91.29881167411804
time_elpased: 3.653
batch start
#iterations: 268
currently lose_sum: 91.33008170127869
time_elpased: 3.41
batch start
#iterations: 269
currently lose_sum: 91.64120572805405
time_elpased: 3.531
batch start
#iterations: 270
currently lose_sum: 92.25529086589813
time_elpased: 3.101
batch start
#iterations: 271
currently lose_sum: 91.12572830915451
time_elpased: 2.822
batch start
#iterations: 272
currently lose_sum: 91.63346922397614
time_elpased: 2.887
batch start
#iterations: 273
currently lose_sum: 91.63488334417343
time_elpased: 2.883
batch start
#iterations: 274
currently lose_sum: 91.24499189853668
time_elpased: 2.946
batch start
#iterations: 275
currently lose_sum: 91.73484796285629
time_elpased: 3.219
batch start
#iterations: 276
currently lose_sum: 91.23631417751312
time_elpased: 2.955
batch start
#iterations: 277
currently lose_sum: 91.26714140176773
time_elpased: 3.381
batch start
#iterations: 278
currently lose_sum: 90.586659014225
time_elpased: 3.069
batch start
#iterations: 279
currently lose_sum: 90.96255391836166
time_elpased: 3.005
start validation test
0.686082474227
0.678934385792
0.708140372543
0.693229901269
0.686043748184
57.417
batch start
#iterations: 280
currently lose_sum: 90.80166101455688
time_elpased: 2.933
batch start
#iterations: 281
currently lose_sum: 91.13634729385376
time_elpased: 2.966
batch start
#iterations: 282
currently lose_sum: 91.29460734128952
time_elpased: 3.113
batch start
#iterations: 283
currently lose_sum: 90.87235474586487
time_elpased: 3.112
batch start
#iterations: 284
currently lose_sum: 91.21783459186554
time_elpased: 3.179
batch start
#iterations: 285
currently lose_sum: 91.11802303791046
time_elpased: 3.268
batch start
#iterations: 286
currently lose_sum: 90.71585088968277
time_elpased: 3.337
batch start
#iterations: 287
currently lose_sum: 90.93930178880692
time_elpased: 3.095
batch start
#iterations: 288
currently lose_sum: 90.80707859992981
time_elpased: 2.992
batch start
#iterations: 289
currently lose_sum: 91.20503550767899
time_elpased: 3.082
batch start
#iterations: 290
currently lose_sum: 91.22969245910645
time_elpased: 3.035
batch start
#iterations: 291
currently lose_sum: 90.22306084632874
time_elpased: 3.141
batch start
#iterations: 292
currently lose_sum: 90.84444087743759
time_elpased: 3.13
batch start
#iterations: 293
currently lose_sum: 91.1704033613205
time_elpased: 3.096
batch start
#iterations: 294
currently lose_sum: 91.22528523206711
time_elpased: 3.283
batch start
#iterations: 295
currently lose_sum: 91.2360890507698
time_elpased: 3.031
batch start
#iterations: 296
currently lose_sum: 91.0637509226799
time_elpased: 3.038
batch start
#iterations: 297
currently lose_sum: 90.4492775797844
time_elpased: 2.472
batch start
#iterations: 298
currently lose_sum: 91.21520751714706
time_elpased: 2.905
batch start
#iterations: 299
currently lose_sum: 91.09310978651047
time_elpased: 2.688
start validation test
0.691134020619
0.676023060202
0.736132551199
0.704798502315
0.691055018758
57.193
batch start
#iterations: 300
currently lose_sum: 91.05029600858688
time_elpased: 3.036
batch start
#iterations: 301
currently lose_sum: 91.42256194353104
time_elpased: 3.135
batch start
#iterations: 302
currently lose_sum: 90.18191367387772
time_elpased: 3.295
batch start
#iterations: 303
currently lose_sum: 91.2993488907814
time_elpased: 3.169
batch start
#iterations: 304
currently lose_sum: 90.68054223060608
time_elpased: 3.039
batch start
#iterations: 305
currently lose_sum: 91.23871648311615
time_elpased: 3.091
batch start
#iterations: 306
currently lose_sum: 90.68197923898697
time_elpased: 2.717
batch start
#iterations: 307
currently lose_sum: 91.39979839324951
time_elpased: 3.113
batch start
#iterations: 308
currently lose_sum: 90.38395458459854
time_elpased: 3.012
batch start
#iterations: 309
currently lose_sum: 90.80438154935837
time_elpased: 3.22
batch start
#iterations: 310
currently lose_sum: 91.55650877952576
time_elpased: 3.064
batch start
#iterations: 311
currently lose_sum: 90.49951028823853
time_elpased: 2.81
batch start
#iterations: 312
currently lose_sum: 90.73235732316971
time_elpased: 2.94
batch start
#iterations: 313
currently lose_sum: 91.05304539203644
time_elpased: 2.989
batch start
#iterations: 314
currently lose_sum: 90.56085413694382
time_elpased: 3.099
batch start
#iterations: 315
currently lose_sum: 90.89059150218964
time_elpased: 2.631
batch start
#iterations: 316
currently lose_sum: 90.42295116186142
time_elpased: 2.623
batch start
#iterations: 317
currently lose_sum: 90.81262534856796
time_elpased: 2.788
batch start
#iterations: 318
currently lose_sum: 90.88792681694031
time_elpased: 3.008
batch start
#iterations: 319
currently lose_sum: 91.72465193271637
time_elpased: 3.155
start validation test
0.674381443299
0.691743740131
0.631161881239
0.660065651402
0.674457321906
58.187
batch start
#iterations: 320
currently lose_sum: 89.95204550027847
time_elpased: 3.232
batch start
#iterations: 321
currently lose_sum: 91.74291306734085
time_elpased: 3.204
batch start
#iterations: 322
currently lose_sum: 90.78741192817688
time_elpased: 3.014
batch start
#iterations: 323
currently lose_sum: 90.77723217010498
time_elpased: 3.22
batch start
#iterations: 324
currently lose_sum: 90.10579591989517
time_elpased: 2.962
batch start
#iterations: 325
currently lose_sum: 90.36142480373383
time_elpased: 2.658
batch start
#iterations: 326
currently lose_sum: 90.22361242771149
time_elpased: 3.249
batch start
#iterations: 327
currently lose_sum: 90.00739341974258
time_elpased: 3.134
batch start
#iterations: 328
currently lose_sum: 90.24820333719254
time_elpased: 3.221
batch start
#iterations: 329
currently lose_sum: 90.33207327127457
time_elpased: 2.493
batch start
#iterations: 330
currently lose_sum: 89.9419464468956
time_elpased: 2.827
batch start
#iterations: 331
currently lose_sum: 90.59676665067673
time_elpased: 3.075
batch start
#iterations: 332
currently lose_sum: 90.53762984275818
time_elpased: 3.1
batch start
#iterations: 333
currently lose_sum: 90.33823722600937
time_elpased: 3.356
batch start
#iterations: 334
currently lose_sum: 90.63746803998947
time_elpased: 2.359
batch start
#iterations: 335
currently lose_sum: 90.57713174819946
time_elpased: 2.962
batch start
#iterations: 336
currently lose_sum: 90.1307709813118
time_elpased: 3.182
batch start
#iterations: 337
currently lose_sum: 90.53474789857864
time_elpased: 3.334
batch start
#iterations: 338
currently lose_sum: 90.31148207187653
time_elpased: 3.31
batch start
#iterations: 339
currently lose_sum: 90.80471485853195
time_elpased: 3.018
start validation test
0.669845360825
0.655756207675
0.717505402902
0.68524251806
0.66976168627
58.334
batch start
#iterations: 340
currently lose_sum: 90.37007623910904
time_elpased: 3.392
batch start
#iterations: 341
currently lose_sum: 90.67102199792862
time_elpased: 2.896
batch start
#iterations: 342
currently lose_sum: 89.94546449184418
time_elpased: 3.061
batch start
#iterations: 343
currently lose_sum: 89.85953050851822
time_elpased: 3.025
batch start
#iterations: 344
currently lose_sum: 90.40326261520386
time_elpased: 3.276
batch start
#iterations: 345
currently lose_sum: 89.96352875232697
time_elpased: 3.1
batch start
#iterations: 346
currently lose_sum: 90.21805065870285
time_elpased: 2.911
batch start
#iterations: 347
currently lose_sum: 90.93549877405167
time_elpased: 3.401
batch start
#iterations: 348
currently lose_sum: 90.06114000082016
time_elpased: 2.744
batch start
#iterations: 349
currently lose_sum: 90.48638492822647
time_elpased: 3.148
batch start
#iterations: 350
currently lose_sum: 90.06526839733124
time_elpased: 2.831
batch start
#iterations: 351
currently lose_sum: 90.55269294977188
time_elpased: 2.731
batch start
#iterations: 352
currently lose_sum: 90.58264857530594
time_elpased: 3.008
batch start
#iterations: 353
currently lose_sum: 91.3735466003418
time_elpased: 3.274
batch start
#iterations: 354
currently lose_sum: 90.07470220327377
time_elpased: 3.45
batch start
#iterations: 355
currently lose_sum: 91.23238551616669
time_elpased: 3.073
batch start
#iterations: 356
currently lose_sum: 90.74213296175003
time_elpased: 3.032
batch start
#iterations: 357
currently lose_sum: 90.10597097873688
time_elpased: 3.204
batch start
#iterations: 358
currently lose_sum: 90.38222467899323
time_elpased: 3.112
batch start
#iterations: 359
currently lose_sum: 90.77818548679352
time_elpased: 3.242
start validation test
0.650206185567
0.69084516213
0.545950396213
0.609910324212
0.650389222686
58.695
batch start
#iterations: 360
currently lose_sum: 89.78949254751205
time_elpased: 3.243
batch start
#iterations: 361
currently lose_sum: 90.4151708483696
time_elpased: 3.233
batch start
#iterations: 362
currently lose_sum: 89.80403971672058
time_elpased: 3.224
batch start
#iterations: 363
currently lose_sum: 90.29236882925034
time_elpased: 3.074
batch start
#iterations: 364
currently lose_sum: 91.04061198234558
time_elpased: 3.142
batch start
#iterations: 365
currently lose_sum: 89.72080379724503
time_elpased: 3.099
batch start
#iterations: 366
currently lose_sum: 89.31696051359177
time_elpased: 3.411
batch start
#iterations: 367
currently lose_sum: 89.33230316638947
time_elpased: 3.207
batch start
#iterations: 368
currently lose_sum: 89.7773824930191
time_elpased: 2.783
batch start
#iterations: 369
currently lose_sum: 90.60284304618835
time_elpased: 3.02
batch start
#iterations: 370
currently lose_sum: 89.772674202919
time_elpased: 2.924
batch start
#iterations: 371
currently lose_sum: 90.54063111543655
time_elpased: 3.428
batch start
#iterations: 372
currently lose_sum: 89.65930712223053
time_elpased: 3.084
batch start
#iterations: 373
currently lose_sum: 89.65083175897598
time_elpased: 2.812
batch start
#iterations: 374
currently lose_sum: 89.6964984536171
time_elpased: 3.097
batch start
#iterations: 375
currently lose_sum: 90.49629300832748
time_elpased: 3.465
batch start
#iterations: 376
currently lose_sum: 89.51893097162247
time_elpased: 3.593
batch start
#iterations: 377
currently lose_sum: 89.57046627998352
time_elpased: 3.509
batch start
#iterations: 378
currently lose_sum: 89.73505425453186
time_elpased: 3.672
batch start
#iterations: 379
currently lose_sum: 90.04773110151291
time_elpased: 3.382
start validation test
0.693195876289
0.682183296235
0.72542965936
0.703142144638
0.693139284911
56.770
batch start
#iterations: 380
currently lose_sum: 90.57017242908478
time_elpased: 3.33
batch start
#iterations: 381
currently lose_sum: 89.28765839338303
time_elpased: 3.092
batch start
#iterations: 382
currently lose_sum: 89.65167284011841
time_elpased: 2.914
batch start
#iterations: 383
currently lose_sum: 90.32048046588898
time_elpased: 3.585
batch start
#iterations: 384
currently lose_sum: 90.30693638324738
time_elpased: 2.782
batch start
#iterations: 385
currently lose_sum: 89.28643798828125
time_elpased: 3.299
batch start
#iterations: 386
currently lose_sum: 89.83916330337524
time_elpased: 2.885
batch start
#iterations: 387
currently lose_sum: 90.47726500034332
time_elpased: 3.224
batch start
#iterations: 388
currently lose_sum: 89.662646651268
time_elpased: 2.917
batch start
#iterations: 389
currently lose_sum: 89.21421903371811
time_elpased: 2.877
batch start
#iterations: 390
currently lose_sum: 89.71904933452606
time_elpased: 2.927
batch start
#iterations: 391
currently lose_sum: 89.41416317224503
time_elpased: 2.829
batch start
#iterations: 392
currently lose_sum: 89.0929102897644
time_elpased: 3.175
batch start
#iterations: 393
currently lose_sum: 89.98096162080765
time_elpased: 3.189
batch start
#iterations: 394
currently lose_sum: 89.47549736499786
time_elpased: 3.089
batch start
#iterations: 395
currently lose_sum: 89.49407404661179
time_elpased: 2.905
batch start
#iterations: 396
currently lose_sum: 89.32737451791763
time_elpased: 3.089
batch start
#iterations: 397
currently lose_sum: 89.61417078971863
time_elpased: 3.196
batch start
#iterations: 398
currently lose_sum: 89.73448103666306
time_elpased: 3.082
batch start
#iterations: 399
currently lose_sum: 89.8202611207962
time_elpased: 3.172
start validation test
0.695360824742
0.692331009397
0.705155912319
0.698684613032
0.695343627955
56.719
acc: 0.690
pre: 0.690
rec: 0.693
F1: 0.691
auc: 0.751
