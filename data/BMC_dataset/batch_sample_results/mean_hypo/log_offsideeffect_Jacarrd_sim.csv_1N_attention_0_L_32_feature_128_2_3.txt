start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.10565567016602
time_elpased: 1.718
batch start
#iterations: 1
currently lose_sum: 100.0295974612236
time_elpased: 1.736
batch start
#iterations: 2
currently lose_sum: 99.72881668806076
time_elpased: 1.729
batch start
#iterations: 3
currently lose_sum: 99.63143026828766
time_elpased: 1.733
batch start
#iterations: 4
currently lose_sum: 99.53903079032898
time_elpased: 1.71
batch start
#iterations: 5
currently lose_sum: 99.54674309492111
time_elpased: 1.735
batch start
#iterations: 6
currently lose_sum: 99.47047513723373
time_elpased: 1.699
batch start
#iterations: 7
currently lose_sum: 99.33921706676483
time_elpased: 1.712
batch start
#iterations: 8
currently lose_sum: 99.40515583753586
time_elpased: 1.714
batch start
#iterations: 9
currently lose_sum: 99.35619080066681
time_elpased: 1.77
batch start
#iterations: 10
currently lose_sum: 99.38276046514511
time_elpased: 1.748
batch start
#iterations: 11
currently lose_sum: 99.27072155475616
time_elpased: 1.752
batch start
#iterations: 12
currently lose_sum: 99.08115059137344
time_elpased: 1.774
batch start
#iterations: 13
currently lose_sum: 99.30288487672806
time_elpased: 1.728
batch start
#iterations: 14
currently lose_sum: 99.13151633739471
time_elpased: 1.731
batch start
#iterations: 15
currently lose_sum: 99.02484267950058
time_elpased: 1.747
batch start
#iterations: 16
currently lose_sum: 99.1612041592598
time_elpased: 1.745
batch start
#iterations: 17
currently lose_sum: 99.0061149597168
time_elpased: 1.74
batch start
#iterations: 18
currently lose_sum: 98.9273008108139
time_elpased: 1.732
batch start
#iterations: 19
currently lose_sum: 98.71341043710709
time_elpased: 1.754
start validation test
0.632113402062
0.614408659391
0.712741869082
0.659932339067
0.631980186916
64.055
batch start
#iterations: 20
currently lose_sum: 98.83186054229736
time_elpased: 1.702
batch start
#iterations: 21
currently lose_sum: 98.61425590515137
time_elpased: 1.711
batch start
#iterations: 22
currently lose_sum: 98.68041878938675
time_elpased: 1.7
batch start
#iterations: 23
currently lose_sum: 98.54743129014969
time_elpased: 1.745
batch start
#iterations: 24
currently lose_sum: 98.56014317274094
time_elpased: 1.688
batch start
#iterations: 25
currently lose_sum: 98.21912223100662
time_elpased: 1.731
batch start
#iterations: 26
currently lose_sum: 98.398641705513
time_elpased: 1.732
batch start
#iterations: 27
currently lose_sum: 98.43877518177032
time_elpased: 1.742
batch start
#iterations: 28
currently lose_sum: 98.15761870145798
time_elpased: 1.734
batch start
#iterations: 29
currently lose_sum: 97.9678316116333
time_elpased: 1.722
batch start
#iterations: 30
currently lose_sum: 97.98942470550537
time_elpased: 1.797
batch start
#iterations: 31
currently lose_sum: 97.9246376156807
time_elpased: 1.728
batch start
#iterations: 32
currently lose_sum: 97.69040882587433
time_elpased: 1.736
batch start
#iterations: 33
currently lose_sum: 97.63641285896301
time_elpased: 1.762
batch start
#iterations: 34
currently lose_sum: 97.43014043569565
time_elpased: 1.726
batch start
#iterations: 35
currently lose_sum: 97.56724894046783
time_elpased: 1.727
batch start
#iterations: 36
currently lose_sum: 97.25031352043152
time_elpased: 1.712
batch start
#iterations: 37
currently lose_sum: 97.26135587692261
time_elpased: 1.75
batch start
#iterations: 38
currently lose_sum: 97.19555097818375
time_elpased: 1.735
batch start
#iterations: 39
currently lose_sum: 97.41094934940338
time_elpased: 1.754
start validation test
0.637371134021
0.60111639134
0.820193495266
0.693770948505
0.637069073118
63.552
batch start
#iterations: 40
currently lose_sum: 97.29621177911758
time_elpased: 1.745
batch start
#iterations: 41
currently lose_sum: 96.90034675598145
time_elpased: 1.728
batch start
#iterations: 42
currently lose_sum: 96.9140442609787
time_elpased: 1.754
batch start
#iterations: 43
currently lose_sum: 97.01735097169876
time_elpased: 1.723
batch start
#iterations: 44
currently lose_sum: 96.77401900291443
time_elpased: 1.712
batch start
#iterations: 45
currently lose_sum: 96.79818499088287
time_elpased: 1.754
batch start
#iterations: 46
currently lose_sum: 96.80514413118362
time_elpased: 1.737
batch start
#iterations: 47
currently lose_sum: 97.07576805353165
time_elpased: 1.728
batch start
#iterations: 48
currently lose_sum: 96.5356216430664
time_elpased: 1.723
batch start
#iterations: 49
currently lose_sum: 96.62923485040665
time_elpased: 1.716
batch start
#iterations: 50
currently lose_sum: 96.86739045381546
time_elpased: 1.728
batch start
#iterations: 51
currently lose_sum: 97.08052563667297
time_elpased: 1.764
batch start
#iterations: 52
currently lose_sum: 96.8670563697815
time_elpased: 1.709
batch start
#iterations: 53
currently lose_sum: 96.06350296735764
time_elpased: 1.759
batch start
#iterations: 54
currently lose_sum: 97.02554750442505
time_elpased: 1.715
batch start
#iterations: 55
currently lose_sum: 96.60973912477493
time_elpased: 1.744
batch start
#iterations: 56
currently lose_sum: 96.47021263837814
time_elpased: 1.729
batch start
#iterations: 57
currently lose_sum: 96.62546437978745
time_elpased: 1.765
batch start
#iterations: 58
currently lose_sum: 96.31040924787521
time_elpased: 1.743
batch start
#iterations: 59
currently lose_sum: 96.62873578071594
time_elpased: 1.787
start validation test
0.641082474227
0.60282363487
0.830588719638
0.698610570056
0.640769370145
62.369
batch start
#iterations: 60
currently lose_sum: 96.0550747513771
time_elpased: 1.718
batch start
#iterations: 61
currently lose_sum: 96.28816932439804
time_elpased: 1.743
batch start
#iterations: 62
currently lose_sum: 96.27391004562378
time_elpased: 1.732
batch start
#iterations: 63
currently lose_sum: 96.19179660081863
time_elpased: 1.757
batch start
#iterations: 64
currently lose_sum: 96.3854809999466
time_elpased: 1.772
batch start
#iterations: 65
currently lose_sum: 95.83513087034225
time_elpased: 1.779
batch start
#iterations: 66
currently lose_sum: 96.07372426986694
time_elpased: 1.711
batch start
#iterations: 67
currently lose_sum: 95.7954353094101
time_elpased: 1.75
batch start
#iterations: 68
currently lose_sum: 95.66511285305023
time_elpased: 1.701
batch start
#iterations: 69
currently lose_sum: 95.64542245864868
time_elpased: 1.71
batch start
#iterations: 70
currently lose_sum: 95.94560241699219
time_elpased: 1.738
batch start
#iterations: 71
currently lose_sum: 95.4988648891449
time_elpased: 1.73
batch start
#iterations: 72
currently lose_sum: 95.69376814365387
time_elpased: 1.756
batch start
#iterations: 73
currently lose_sum: 95.99267131090164
time_elpased: 1.724
batch start
#iterations: 74
currently lose_sum: 95.2647939324379
time_elpased: 1.79
batch start
#iterations: 75
currently lose_sum: 96.43856644630432
time_elpased: 1.733
batch start
#iterations: 76
currently lose_sum: 95.83843660354614
time_elpased: 1.757
batch start
#iterations: 77
currently lose_sum: 95.87214434146881
time_elpased: 1.735
batch start
#iterations: 78
currently lose_sum: 95.67052394151688
time_elpased: 1.753
batch start
#iterations: 79
currently lose_sum: 95.5378058552742
time_elpased: 1.755
start validation test
0.656958762887
0.618066805523
0.82461918485
0.706556726487
0.656681752689
60.276
batch start
#iterations: 80
currently lose_sum: 95.43675297498703
time_elpased: 1.711
batch start
#iterations: 81
currently lose_sum: 96.39829432964325
time_elpased: 1.775
batch start
#iterations: 82
currently lose_sum: 95.14497345685959
time_elpased: 1.711
batch start
#iterations: 83
currently lose_sum: 95.06299555301666
time_elpased: 1.729
batch start
#iterations: 84
currently lose_sum: 95.2128279209137
time_elpased: 1.742
batch start
#iterations: 85
currently lose_sum: 95.41393780708313
time_elpased: 1.698
batch start
#iterations: 86
currently lose_sum: 95.24998474121094
time_elpased: 1.776
batch start
#iterations: 87
currently lose_sum: 95.2780448794365
time_elpased: 1.722
batch start
#iterations: 88
currently lose_sum: 95.45019418001175
time_elpased: 1.745
batch start
#iterations: 89
currently lose_sum: 94.9008971452713
time_elpased: 1.737
batch start
#iterations: 90
currently lose_sum: 95.34559708833694
time_elpased: 1.704
batch start
#iterations: 91
currently lose_sum: 95.36360907554626
time_elpased: 1.758
batch start
#iterations: 92
currently lose_sum: 95.59814089536667
time_elpased: 1.731
batch start
#iterations: 93
currently lose_sum: 94.61189889907837
time_elpased: 1.701
batch start
#iterations: 94
currently lose_sum: 95.71131008863449
time_elpased: 1.735
batch start
#iterations: 95
currently lose_sum: 95.38539636135101
time_elpased: 1.758
batch start
#iterations: 96
currently lose_sum: 95.1395024061203
time_elpased: 1.723
batch start
#iterations: 97
currently lose_sum: 94.72324025630951
time_elpased: 1.814
batch start
#iterations: 98
currently lose_sum: 95.20102822780609
time_elpased: 1.709
batch start
#iterations: 99
currently lose_sum: 95.44971585273743
time_elpased: 1.75
start validation test
0.639432989691
0.599794616005
0.841601482091
0.700415435351
0.63909896492
61.993
batch start
#iterations: 100
currently lose_sum: 95.64201378822327
time_elpased: 1.766
batch start
#iterations: 101
currently lose_sum: 95.44527697563171
time_elpased: 1.741
batch start
#iterations: 102
currently lose_sum: 94.82726585865021
time_elpased: 1.736
batch start
#iterations: 103
currently lose_sum: 95.00241392850876
time_elpased: 1.754
batch start
#iterations: 104
currently lose_sum: 94.34229874610901
time_elpased: 1.723
batch start
#iterations: 105
currently lose_sum: 95.2991401553154
time_elpased: 1.788
batch start
#iterations: 106
currently lose_sum: 94.31266462802887
time_elpased: 1.739
batch start
#iterations: 107
currently lose_sum: 95.3391261100769
time_elpased: 1.744
batch start
#iterations: 108
currently lose_sum: 94.62543880939484
time_elpased: 1.751
batch start
#iterations: 109
currently lose_sum: 95.39475947618484
time_elpased: 1.77
batch start
#iterations: 110
currently lose_sum: 95.03489995002747
time_elpased: 1.715
batch start
#iterations: 111
currently lose_sum: 94.55749952793121
time_elpased: 1.755
batch start
#iterations: 112
currently lose_sum: 94.32995969057083
time_elpased: 1.784
batch start
#iterations: 113
currently lose_sum: 94.99105817079544
time_elpased: 1.772
batch start
#iterations: 114
currently lose_sum: 94.67773658037186
time_elpased: 1.742
batch start
#iterations: 115
currently lose_sum: 94.79802948236465
time_elpased: 1.754
batch start
#iterations: 116
currently lose_sum: 94.77970767021179
time_elpased: 1.752
batch start
#iterations: 117
currently lose_sum: 94.31185269355774
time_elpased: 1.722
batch start
#iterations: 118
currently lose_sum: 95.20460575819016
time_elpased: 1.714
batch start
#iterations: 119
currently lose_sum: 94.51447421312332
time_elpased: 1.763
start validation test
0.624639175258
0.596235963941
0.776039522437
0.674358286379
0.624389030116
62.551
batch start
#iterations: 120
currently lose_sum: 94.08146804571152
time_elpased: 1.735
batch start
#iterations: 121
currently lose_sum: 94.24061965942383
time_elpased: 1.763
batch start
#iterations: 122
currently lose_sum: 94.52135241031647
time_elpased: 1.787
batch start
#iterations: 123
currently lose_sum: 94.53596484661102
time_elpased: 1.79
batch start
#iterations: 124
currently lose_sum: 94.6132807135582
time_elpased: 1.711
batch start
#iterations: 125
currently lose_sum: 94.10088264942169
time_elpased: 1.773
batch start
#iterations: 126
currently lose_sum: 94.0510351061821
time_elpased: 1.722
batch start
#iterations: 127
currently lose_sum: 94.41819667816162
time_elpased: 1.734
batch start
#iterations: 128
currently lose_sum: 94.70819526910782
time_elpased: 1.738
batch start
#iterations: 129
currently lose_sum: 94.69362366199493
time_elpased: 1.76
batch start
#iterations: 130
currently lose_sum: 94.61564886569977
time_elpased: 1.738
batch start
#iterations: 131
currently lose_sum: 93.99455958604813
time_elpased: 1.713
batch start
#iterations: 132
currently lose_sum: 93.89729738235474
time_elpased: 1.72
batch start
#iterations: 133
currently lose_sum: 94.26649379730225
time_elpased: 1.743
batch start
#iterations: 134
currently lose_sum: 94.44895100593567
time_elpased: 1.724
batch start
#iterations: 135
currently lose_sum: 94.24183911085129
time_elpased: 1.754
batch start
#iterations: 136
currently lose_sum: 93.90097868442535
time_elpased: 1.725
batch start
#iterations: 137
currently lose_sum: 93.83989840745926
time_elpased: 1.781
batch start
#iterations: 138
currently lose_sum: 94.06199938058853
time_elpased: 1.749
batch start
#iterations: 139
currently lose_sum: 93.77800804376602
time_elpased: 1.734
start validation test
0.664845360825
0.655868089234
0.695965417867
0.675322081294
0.664793943961
59.554
batch start
#iterations: 140
currently lose_sum: 93.25948768854141
time_elpased: 1.737
batch start
#iterations: 141
currently lose_sum: 94.32677054405212
time_elpased: 1.772
batch start
#iterations: 142
currently lose_sum: 93.56835460662842
time_elpased: 1.759
batch start
#iterations: 143
currently lose_sum: 94.62699383497238
time_elpased: 1.746
batch start
#iterations: 144
currently lose_sum: 93.50049549341202
time_elpased: 1.716
batch start
#iterations: 145
currently lose_sum: 93.58013963699341
time_elpased: 1.771
batch start
#iterations: 146
currently lose_sum: 93.55037879943848
time_elpased: 1.735
batch start
#iterations: 147
currently lose_sum: 93.58858215808868
time_elpased: 1.766
batch start
#iterations: 148
currently lose_sum: 94.11098283529282
time_elpased: 1.754
batch start
#iterations: 149
currently lose_sum: 94.1739290356636
time_elpased: 1.703
batch start
#iterations: 150
currently lose_sum: 93.94953447580338
time_elpased: 1.754
batch start
#iterations: 151
currently lose_sum: 94.29540574550629
time_elpased: 1.721
batch start
#iterations: 152
currently lose_sum: 93.95693999528885
time_elpased: 1.732
batch start
#iterations: 153
currently lose_sum: 94.05783772468567
time_elpased: 1.749
batch start
#iterations: 154
currently lose_sum: 93.83157408237457
time_elpased: 1.726
batch start
#iterations: 155
currently lose_sum: 93.6405782699585
time_elpased: 1.712
batch start
#iterations: 156
currently lose_sum: 93.3756417632103
time_elpased: 1.732
batch start
#iterations: 157
currently lose_sum: 93.6586657166481
time_elpased: 1.748
batch start
#iterations: 158
currently lose_sum: 94.11576861143112
time_elpased: 1.758
batch start
#iterations: 159
currently lose_sum: 93.52970576286316
time_elpased: 1.73
start validation test
0.621443298969
0.590811638591
0.794153972828
0.677555321391
0.621157944696
62.239
batch start
#iterations: 160
currently lose_sum: 93.4068210721016
time_elpased: 1.809
batch start
#iterations: 161
currently lose_sum: 93.20722508430481
time_elpased: 1.758
batch start
#iterations: 162
currently lose_sum: 93.62905478477478
time_elpased: 1.714
batch start
#iterations: 163
currently lose_sum: 93.29058879613876
time_elpased: 1.759
batch start
#iterations: 164
currently lose_sum: 93.45321303606033
time_elpased: 1.793
batch start
#iterations: 165
currently lose_sum: 93.64626896381378
time_elpased: 1.708
batch start
#iterations: 166
currently lose_sum: 93.45257866382599
time_elpased: 1.775
batch start
#iterations: 167
currently lose_sum: 93.23684281110764
time_elpased: 1.75
batch start
#iterations: 168
currently lose_sum: 93.20324087142944
time_elpased: 1.72
batch start
#iterations: 169
currently lose_sum: 93.12220126390457
time_elpased: 1.734
batch start
#iterations: 170
currently lose_sum: 93.48080062866211
time_elpased: 1.698
batch start
#iterations: 171
currently lose_sum: 93.21949815750122
time_elpased: 1.742
batch start
#iterations: 172
currently lose_sum: 93.10167235136032
time_elpased: 1.719
batch start
#iterations: 173
currently lose_sum: 92.7629719376564
time_elpased: 1.797
batch start
#iterations: 174
currently lose_sum: 93.76474469900131
time_elpased: 1.741
batch start
#iterations: 175
currently lose_sum: 93.73774498701096
time_elpased: 1.752
batch start
#iterations: 176
currently lose_sum: 93.05141931772232
time_elpased: 1.699
batch start
#iterations: 177
currently lose_sum: 92.91730618476868
time_elpased: 1.747
batch start
#iterations: 178
currently lose_sum: 93.04710185527802
time_elpased: 1.734
batch start
#iterations: 179
currently lose_sum: 93.7043645977974
time_elpased: 1.774
start validation test
0.677525773196
0.655101308947
0.752058460272
0.700239578342
0.677402629558
58.760
batch start
#iterations: 180
currently lose_sum: 93.32602733373642
time_elpased: 1.749
batch start
#iterations: 181
currently lose_sum: 93.29848676919937
time_elpased: 1.701
batch start
#iterations: 182
currently lose_sum: 93.81977915763855
time_elpased: 1.77
batch start
#iterations: 183
currently lose_sum: 92.62665438652039
time_elpased: 1.726
batch start
#iterations: 184
currently lose_sum: 93.34733641147614
time_elpased: 1.724
batch start
#iterations: 185
currently lose_sum: 92.91696745157242
time_elpased: 1.76
batch start
#iterations: 186
currently lose_sum: 93.82931524515152
time_elpased: 1.749
batch start
#iterations: 187
currently lose_sum: 92.70074707269669
time_elpased: 1.732
batch start
#iterations: 188
currently lose_sum: 92.97532922029495
time_elpased: 1.733
batch start
#iterations: 189
currently lose_sum: 92.46119147539139
time_elpased: 1.73
batch start
#iterations: 190
currently lose_sum: 93.19086062908173
time_elpased: 1.678
batch start
#iterations: 191
currently lose_sum: 93.63294440507889
time_elpased: 1.68
batch start
#iterations: 192
currently lose_sum: 92.63199496269226
time_elpased: 1.711
batch start
#iterations: 193
currently lose_sum: 92.36713927984238
time_elpased: 1.784
batch start
#iterations: 194
currently lose_sum: 92.51185250282288
time_elpased: 1.769
batch start
#iterations: 195
currently lose_sum: 92.89610177278519
time_elpased: 1.718
batch start
#iterations: 196
currently lose_sum: 92.66366231441498
time_elpased: 1.713
batch start
#iterations: 197
currently lose_sum: 93.18671929836273
time_elpased: 1.735
batch start
#iterations: 198
currently lose_sum: 92.19436401128769
time_elpased: 1.723
batch start
#iterations: 199
currently lose_sum: 92.49006003141403
time_elpased: 1.762
start validation test
0.657164948454
0.624038850668
0.793536434747
0.698654342803
0.656939634143
59.731
batch start
#iterations: 200
currently lose_sum: 92.83495008945465
time_elpased: 1.708
batch start
#iterations: 201
currently lose_sum: 92.26688599586487
time_elpased: 1.704
batch start
#iterations: 202
currently lose_sum: 92.86973863840103
time_elpased: 1.696
batch start
#iterations: 203
currently lose_sum: 92.76381725072861
time_elpased: 1.761
batch start
#iterations: 204
currently lose_sum: 92.64108514785767
time_elpased: 1.749
batch start
#iterations: 205
currently lose_sum: 92.58080494403839
time_elpased: 1.72
batch start
#iterations: 206
currently lose_sum: 93.1627094745636
time_elpased: 1.753
batch start
#iterations: 207
currently lose_sum: 92.80098670721054
time_elpased: 1.728
batch start
#iterations: 208
currently lose_sum: 92.0983414053917
time_elpased: 1.725
batch start
#iterations: 209
currently lose_sum: 92.93183028697968
time_elpased: 1.763
batch start
#iterations: 210
currently lose_sum: 92.7300381064415
time_elpased: 1.753
batch start
#iterations: 211
currently lose_sum: 92.2143366932869
time_elpased: 1.738
batch start
#iterations: 212
currently lose_sum: 93.32708245515823
time_elpased: 1.773
batch start
#iterations: 213
currently lose_sum: 92.49670213460922
time_elpased: 1.795
batch start
#iterations: 214
currently lose_sum: 92.1143861413002
time_elpased: 1.742
batch start
#iterations: 215
currently lose_sum: 92.28610336780548
time_elpased: 1.758
batch start
#iterations: 216
currently lose_sum: 91.72117972373962
time_elpased: 1.724
batch start
#iterations: 217
currently lose_sum: 92.14328342676163
time_elpased: 1.785
batch start
#iterations: 218
currently lose_sum: 92.80753380060196
time_elpased: 1.701
batch start
#iterations: 219
currently lose_sum: 92.26041603088379
time_elpased: 1.718
start validation test
0.645670103093
0.636293880683
0.682791272128
0.65872306623
0.645608771132
60.292
batch start
#iterations: 220
currently lose_sum: 92.26227778196335
time_elpased: 1.726
batch start
#iterations: 221
currently lose_sum: 91.50131875276566
time_elpased: 1.704
batch start
#iterations: 222
currently lose_sum: 91.88327139616013
time_elpased: 1.729
batch start
#iterations: 223
currently lose_sum: 91.36583924293518
time_elpased: 1.75
batch start
#iterations: 224
currently lose_sum: 92.84920704364777
time_elpased: 1.714
batch start
#iterations: 225
currently lose_sum: 92.20920670032501
time_elpased: 1.742
batch start
#iterations: 226
currently lose_sum: 91.81156414747238
time_elpased: 1.773
batch start
#iterations: 227
currently lose_sum: 91.9425008893013
time_elpased: 1.743
batch start
#iterations: 228
currently lose_sum: 92.27307790517807
time_elpased: 1.687
batch start
#iterations: 229
currently lose_sum: 93.1118723154068
time_elpased: 1.747
batch start
#iterations: 230
currently lose_sum: 92.27277278900146
time_elpased: 1.714
batch start
#iterations: 231
currently lose_sum: 92.54514420032501
time_elpased: 1.744
batch start
#iterations: 232
currently lose_sum: 92.33427655696869
time_elpased: 1.718
batch start
#iterations: 233
currently lose_sum: 92.23077696561813
time_elpased: 1.774
batch start
#iterations: 234
currently lose_sum: 91.95734202861786
time_elpased: 1.747
batch start
#iterations: 235
currently lose_sum: 92.5899589061737
time_elpased: 1.772
batch start
#iterations: 236
currently lose_sum: 92.35321122407913
time_elpased: 1.722
batch start
#iterations: 237
currently lose_sum: 91.8405641913414
time_elpased: 1.767
batch start
#iterations: 238
currently lose_sum: 91.15081548690796
time_elpased: 1.729
batch start
#iterations: 239
currently lose_sum: 92.36175960302353
time_elpased: 1.722
start validation test
0.661134020619
0.670390455531
0.636167146974
0.652830587241
0.661175271133
58.904
batch start
#iterations: 240
currently lose_sum: 93.26823276281357
time_elpased: 1.721
batch start
#iterations: 241
currently lose_sum: 91.70462030172348
time_elpased: 1.734
batch start
#iterations: 242
currently lose_sum: 92.14547270536423
time_elpased: 1.723
batch start
#iterations: 243
currently lose_sum: 92.00086599588394
time_elpased: 1.738
batch start
#iterations: 244
currently lose_sum: 91.4110626578331
time_elpased: 1.741
batch start
#iterations: 245
currently lose_sum: 92.71702653169632
time_elpased: 1.722
batch start
#iterations: 246
currently lose_sum: 91.9950379729271
time_elpased: 1.784
batch start
#iterations: 247
currently lose_sum: 91.6185097694397
time_elpased: 1.748
batch start
#iterations: 248
currently lose_sum: 92.27267360687256
time_elpased: 1.708
batch start
#iterations: 249
currently lose_sum: 92.28349286317825
time_elpased: 1.726
batch start
#iterations: 250
currently lose_sum: 91.08504039049149
time_elpased: 1.743
batch start
#iterations: 251
currently lose_sum: 92.09724736213684
time_elpased: 1.754
batch start
#iterations: 252
currently lose_sum: 92.08314371109009
time_elpased: 1.707
batch start
#iterations: 253
currently lose_sum: 91.84511941671371
time_elpased: 1.729
batch start
#iterations: 254
currently lose_sum: 91.60886430740356
time_elpased: 1.727
batch start
#iterations: 255
currently lose_sum: 91.52045953273773
time_elpased: 1.784
batch start
#iterations: 256
currently lose_sum: 91.79755556583405
time_elpased: 1.741
batch start
#iterations: 257
currently lose_sum: 91.4336234331131
time_elpased: 1.737
batch start
#iterations: 258
currently lose_sum: 92.07061952352524
time_elpased: 1.676
batch start
#iterations: 259
currently lose_sum: 91.56351208686829
time_elpased: 1.76
start validation test
0.65881443299
0.643685626798
0.713976945245
0.677011662519
0.658723292945
59.037
batch start
#iterations: 260
currently lose_sum: 91.23623931407928
time_elpased: 1.763
batch start
#iterations: 261
currently lose_sum: 91.57218050956726
time_elpased: 1.724
batch start
#iterations: 262
currently lose_sum: 91.60996371507645
time_elpased: 1.762
batch start
#iterations: 263
currently lose_sum: 91.8327311873436
time_elpased: 1.744
batch start
#iterations: 264
currently lose_sum: 91.72502851486206
time_elpased: 1.727
batch start
#iterations: 265
currently lose_sum: 91.67204535007477
time_elpased: 1.773
batch start
#iterations: 266
currently lose_sum: 91.40600168704987
time_elpased: 1.726
batch start
#iterations: 267
currently lose_sum: 91.59784716367722
time_elpased: 1.762
batch start
#iterations: 268
currently lose_sum: 91.4247579574585
time_elpased: 1.739
batch start
#iterations: 269
currently lose_sum: 91.85392951965332
time_elpased: 1.739
batch start
#iterations: 270
currently lose_sum: 91.80818223953247
time_elpased: 1.713
batch start
#iterations: 271
currently lose_sum: 90.86743903160095
time_elpased: 1.746
batch start
#iterations: 272
currently lose_sum: 91.70386928319931
time_elpased: 1.732
batch start
#iterations: 273
currently lose_sum: 91.36314195394516
time_elpased: 1.696
batch start
#iterations: 274
currently lose_sum: 91.15578818321228
time_elpased: 1.712
batch start
#iterations: 275
currently lose_sum: 91.60550493001938
time_elpased: 1.713
batch start
#iterations: 276
currently lose_sum: 91.63842660188675
time_elpased: 1.728
batch start
#iterations: 277
currently lose_sum: 91.66058272123337
time_elpased: 1.745
batch start
#iterations: 278
currently lose_sum: 91.63258594274521
time_elpased: 1.729
batch start
#iterations: 279
currently lose_sum: 91.51813971996307
time_elpased: 1.711
start validation test
0.676340206186
0.659076182542
0.732811856731
0.693990935231
0.676246903169
57.877
batch start
#iterations: 280
currently lose_sum: 91.26912665367126
time_elpased: 1.696
batch start
#iterations: 281
currently lose_sum: 90.98098343610764
time_elpased: 1.748
batch start
#iterations: 282
currently lose_sum: 90.56260222196579
time_elpased: 1.726
batch start
#iterations: 283
currently lose_sum: 90.8749789595604
time_elpased: 1.714
batch start
#iterations: 284
currently lose_sum: 91.29957693815231
time_elpased: 1.728
batch start
#iterations: 285
currently lose_sum: 91.37473803758621
time_elpased: 1.721
batch start
#iterations: 286
currently lose_sum: 90.90087670087814
time_elpased: 1.716
batch start
#iterations: 287
currently lose_sum: 91.31561774015427
time_elpased: 1.753
batch start
#iterations: 288
currently lose_sum: 91.08043992519379
time_elpased: 1.723
batch start
#iterations: 289
currently lose_sum: 91.43507605791092
time_elpased: 1.732
batch start
#iterations: 290
currently lose_sum: 90.707264482975
time_elpased: 1.745
batch start
#iterations: 291
currently lose_sum: 91.71568876504898
time_elpased: 1.714
batch start
#iterations: 292
currently lose_sum: 90.69355553388596
time_elpased: 1.78
batch start
#iterations: 293
currently lose_sum: 91.04877054691315
time_elpased: 1.775
batch start
#iterations: 294
currently lose_sum: 90.97428125143051
time_elpased: 1.707
batch start
#iterations: 295
currently lose_sum: 90.76456421613693
time_elpased: 1.745
batch start
#iterations: 296
currently lose_sum: 91.00468695163727
time_elpased: 1.728
batch start
#iterations: 297
currently lose_sum: 91.88419950008392
time_elpased: 1.762
batch start
#iterations: 298
currently lose_sum: 91.60570150613785
time_elpased: 1.687
batch start
#iterations: 299
currently lose_sum: 91.05622094869614
time_elpased: 1.721
start validation test
0.662164948454
0.671215074724
0.637916838205
0.654142480211
0.66220501142
58.249
batch start
#iterations: 300
currently lose_sum: 91.20105803012848
time_elpased: 1.746
batch start
#iterations: 301
currently lose_sum: 91.42896276712418
time_elpased: 1.757
batch start
#iterations: 302
currently lose_sum: 91.02534359693527
time_elpased: 1.702
batch start
#iterations: 303
currently lose_sum: 90.3039380311966
time_elpased: 1.762
batch start
#iterations: 304
currently lose_sum: 90.70818763971329
time_elpased: 1.727
batch start
#iterations: 305
currently lose_sum: 90.94792926311493
time_elpased: 1.742
batch start
#iterations: 306
currently lose_sum: 90.75042682886124
time_elpased: 1.728
batch start
#iterations: 307
currently lose_sum: 91.24302458763123
time_elpased: 1.767
batch start
#iterations: 308
currently lose_sum: 90.74661207199097
time_elpased: 1.76
batch start
#iterations: 309
currently lose_sum: 91.00373911857605
time_elpased: 1.748
batch start
#iterations: 310
currently lose_sum: 91.66024947166443
time_elpased: 1.723
batch start
#iterations: 311
currently lose_sum: 91.42290794849396
time_elpased: 1.695
batch start
#iterations: 312
currently lose_sum: 90.53244137763977
time_elpased: 1.696
batch start
#iterations: 313
currently lose_sum: 91.16159588098526
time_elpased: 1.801
batch start
#iterations: 314
currently lose_sum: 90.37313717603683
time_elpased: 1.739
batch start
#iterations: 315
currently lose_sum: 91.21458679437637
time_elpased: 1.732
batch start
#iterations: 316
currently lose_sum: 90.87360870838165
time_elpased: 1.734
batch start
#iterations: 317
currently lose_sum: 91.32306081056595
time_elpased: 1.749
batch start
#iterations: 318
currently lose_sum: 91.4697214961052
time_elpased: 1.745
batch start
#iterations: 319
currently lose_sum: 90.8366289138794
time_elpased: 1.71
start validation test
0.679845360825
0.676432095037
0.691539728283
0.683902488676
0.679826039276
57.587
batch start
#iterations: 320
currently lose_sum: 90.8936916589737
time_elpased: 1.73
batch start
#iterations: 321
currently lose_sum: 91.48491084575653
time_elpased: 1.715
batch start
#iterations: 322
currently lose_sum: 90.38819259405136
time_elpased: 1.71
batch start
#iterations: 323
currently lose_sum: 90.55168569087982
time_elpased: 1.789
batch start
#iterations: 324
currently lose_sum: 91.20981955528259
time_elpased: 1.758
batch start
#iterations: 325
currently lose_sum: 90.61107993125916
time_elpased: 1.746
batch start
#iterations: 326
currently lose_sum: 90.60936892032623
time_elpased: 1.698
batch start
#iterations: 327
currently lose_sum: 91.00763124227524
time_elpased: 1.734
batch start
#iterations: 328
currently lose_sum: 90.03627341985703
time_elpased: 1.724
batch start
#iterations: 329
currently lose_sum: 90.05472582578659
time_elpased: 1.701
batch start
#iterations: 330
currently lose_sum: 90.78812670707703
time_elpased: 1.696
batch start
#iterations: 331
currently lose_sum: 90.5023603439331
time_elpased: 1.742
batch start
#iterations: 332
currently lose_sum: 91.06074869632721
time_elpased: 1.708
batch start
#iterations: 333
currently lose_sum: 90.50130724906921
time_elpased: 1.695
batch start
#iterations: 334
currently lose_sum: 90.40200346708298
time_elpased: 1.696
batch start
#iterations: 335
currently lose_sum: 90.58765423297882
time_elpased: 1.76
batch start
#iterations: 336
currently lose_sum: 90.36233854293823
time_elpased: 1.757
batch start
#iterations: 337
currently lose_sum: 90.45726358890533
time_elpased: 1.74
batch start
#iterations: 338
currently lose_sum: 90.36398303508759
time_elpased: 1.712
batch start
#iterations: 339
currently lose_sum: 90.695985019207
time_elpased: 1.754
start validation test
0.683453608247
0.650754828371
0.794153972828
0.715338617717
0.683270708017
57.453
batch start
#iterations: 340
currently lose_sum: 91.2575803399086
time_elpased: 1.736
batch start
#iterations: 341
currently lose_sum: 91.2039692401886
time_elpased: 1.738
batch start
#iterations: 342
currently lose_sum: 90.38588827848434
time_elpased: 1.728
batch start
#iterations: 343
currently lose_sum: 90.72571021318436
time_elpased: 1.714
batch start
#iterations: 344
currently lose_sum: 90.57728791236877
time_elpased: 1.752
batch start
#iterations: 345
currently lose_sum: 90.43770945072174
time_elpased: 1.737
batch start
#iterations: 346
currently lose_sum: 90.65172505378723
time_elpased: 1.734
batch start
#iterations: 347
currently lose_sum: 90.18827718496323
time_elpased: 1.747
batch start
#iterations: 348
currently lose_sum: 90.62646836042404
time_elpased: 1.76
batch start
#iterations: 349
currently lose_sum: 90.49996215105057
time_elpased: 1.743
batch start
#iterations: 350
currently lose_sum: 90.68065023422241
time_elpased: 1.787
batch start
#iterations: 351
currently lose_sum: 90.40966999530792
time_elpased: 1.755
batch start
#iterations: 352
currently lose_sum: 89.87768507003784
time_elpased: 1.726
batch start
#iterations: 353
currently lose_sum: 90.64576947689056
time_elpased: 1.741
batch start
#iterations: 354
currently lose_sum: 90.750945687294
time_elpased: 1.741
batch start
#iterations: 355
currently lose_sum: 90.43444329500198
time_elpased: 1.721
batch start
#iterations: 356
currently lose_sum: 89.45365595817566
time_elpased: 1.735
batch start
#iterations: 357
currently lose_sum: 90.77734768390656
time_elpased: 1.784
batch start
#iterations: 358
currently lose_sum: 90.30247408151627
time_elpased: 1.756
batch start
#iterations: 359
currently lose_sum: 89.66140931844711
time_elpased: 1.724
start validation test
0.675309278351
0.670322001794
0.692054343351
0.681014837697
0.67528161199
57.669
batch start
#iterations: 360
currently lose_sum: 90.1835207939148
time_elpased: 1.737
batch start
#iterations: 361
currently lose_sum: 90.6253268122673
time_elpased: 1.767
batch start
#iterations: 362
currently lose_sum: 90.59739077091217
time_elpased: 1.773
batch start
#iterations: 363
currently lose_sum: 90.2470498085022
time_elpased: 1.789
batch start
#iterations: 364
currently lose_sum: 90.20790600776672
time_elpased: 1.728
batch start
#iterations: 365
currently lose_sum: 90.5472321510315
time_elpased: 1.71
batch start
#iterations: 366
currently lose_sum: 90.34266656637192
time_elpased: 1.734
batch start
#iterations: 367
currently lose_sum: 89.53439456224442
time_elpased: 1.72
batch start
#iterations: 368
currently lose_sum: 90.06685018539429
time_elpased: 1.715
batch start
#iterations: 369
currently lose_sum: 90.33994948863983
time_elpased: 1.768
batch start
#iterations: 370
currently lose_sum: 89.95040905475616
time_elpased: 1.734
batch start
#iterations: 371
currently lose_sum: 90.04001694917679
time_elpased: 1.68
batch start
#iterations: 372
currently lose_sum: 90.00636422634125
time_elpased: 1.727
batch start
#iterations: 373
currently lose_sum: 90.0604020357132
time_elpased: 1.695
batch start
#iterations: 374
currently lose_sum: 90.23703956604004
time_elpased: 1.743
batch start
#iterations: 375
currently lose_sum: 90.34163349866867
time_elpased: 1.739
batch start
#iterations: 376
currently lose_sum: 89.79356735944748
time_elpased: 1.774
batch start
#iterations: 377
currently lose_sum: 90.05762749910355
time_elpased: 1.7
batch start
#iterations: 378
currently lose_sum: 90.76171690225601
time_elpased: 1.715
batch start
#iterations: 379
currently lose_sum: 90.83040428161621
time_elpased: 1.71
start validation test
0.662989690722
0.672230652504
0.638328530259
0.654841093865
0.663030436133
57.992
batch start
#iterations: 380
currently lose_sum: 89.51039791107178
time_elpased: 1.741
batch start
#iterations: 381
currently lose_sum: 89.31739264726639
time_elpased: 1.743
batch start
#iterations: 382
currently lose_sum: 89.7531509399414
time_elpased: 1.777
batch start
#iterations: 383
currently lose_sum: 89.96864432096481
time_elpased: 1.766
batch start
#iterations: 384
currently lose_sum: 90.14953422546387
time_elpased: 1.761
batch start
#iterations: 385
currently lose_sum: 89.56686049699783
time_elpased: 1.702
batch start
#iterations: 386
currently lose_sum: 90.3275556564331
time_elpased: 1.761
batch start
#iterations: 387
currently lose_sum: 89.73315268754959
time_elpased: 1.694
batch start
#iterations: 388
currently lose_sum: 90.05737262964249
time_elpased: 1.738
batch start
#iterations: 389
currently lose_sum: 89.84280490875244
time_elpased: 1.757
batch start
#iterations: 390
currently lose_sum: 90.08493965864182
time_elpased: 1.748
batch start
#iterations: 391
currently lose_sum: 89.96949344873428
time_elpased: 1.726
batch start
#iterations: 392
currently lose_sum: 89.96573853492737
time_elpased: 1.717
batch start
#iterations: 393
currently lose_sum: 89.57140624523163
time_elpased: 1.723
batch start
#iterations: 394
currently lose_sum: 90.07631778717041
time_elpased: 1.719
batch start
#iterations: 395
currently lose_sum: 89.8505682349205
time_elpased: 1.744
batch start
#iterations: 396
currently lose_sum: 89.96661472320557
time_elpased: 1.781
batch start
#iterations: 397
currently lose_sum: 90.35216701030731
time_elpased: 1.784
batch start
#iterations: 398
currently lose_sum: 89.52246832847595
time_elpased: 1.765
batch start
#iterations: 399
currently lose_sum: 89.67574125528336
time_elpased: 1.708
start validation test
0.650309278351
0.64604502889
0.667455743104
0.656575883365
0.650280948793
59.062
acc: 0.698
pre: 0.664
rec: 0.804
F1: 0.728
auc: 0.754
