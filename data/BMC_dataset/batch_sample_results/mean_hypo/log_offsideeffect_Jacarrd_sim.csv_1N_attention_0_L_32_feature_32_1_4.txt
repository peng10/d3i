start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.80992871522903
time_elpased: 1.333
batch start
#iterations: 1
currently lose_sum: 100.10865247249603
time_elpased: 1.309
batch start
#iterations: 2
currently lose_sum: 99.77609795331955
time_elpased: 1.334
batch start
#iterations: 3
currently lose_sum: 99.80213803052902
time_elpased: 1.322
batch start
#iterations: 4
currently lose_sum: 99.83216047286987
time_elpased: 1.311
batch start
#iterations: 5
currently lose_sum: 99.69668465852737
time_elpased: 1.328
batch start
#iterations: 6
currently lose_sum: 99.51154708862305
time_elpased: 1.31
batch start
#iterations: 7
currently lose_sum: 99.5876624584198
time_elpased: 1.345
batch start
#iterations: 8
currently lose_sum: 99.72002136707306
time_elpased: 1.332
batch start
#iterations: 9
currently lose_sum: 99.49301499128342
time_elpased: 1.33
batch start
#iterations: 10
currently lose_sum: 99.4687893986702
time_elpased: 1.345
batch start
#iterations: 11
currently lose_sum: 99.41554266214371
time_elpased: 1.338
batch start
#iterations: 12
currently lose_sum: 99.43063253164291
time_elpased: 1.328
batch start
#iterations: 13
currently lose_sum: 99.50832659006119
time_elpased: 1.32
batch start
#iterations: 14
currently lose_sum: 99.19960224628448
time_elpased: 1.366
batch start
#iterations: 15
currently lose_sum: 99.38080096244812
time_elpased: 1.34
batch start
#iterations: 16
currently lose_sum: 99.3878139257431
time_elpased: 1.308
batch start
#iterations: 17
currently lose_sum: 99.3346853852272
time_elpased: 1.309
batch start
#iterations: 18
currently lose_sum: 99.04330688714981
time_elpased: 1.332
batch start
#iterations: 19
currently lose_sum: 99.41239011287689
time_elpased: 1.337
start validation test
0.631804123711
0.634623430962
0.624369661418
0.629454790683
0.631817176057
64.527
batch start
#iterations: 20
currently lose_sum: 99.398257791996
time_elpased: 1.312
batch start
#iterations: 21
currently lose_sum: 99.02330774068832
time_elpased: 1.367
batch start
#iterations: 22
currently lose_sum: 99.15332520008087
time_elpased: 1.333
batch start
#iterations: 23
currently lose_sum: 99.3127338886261
time_elpased: 1.315
batch start
#iterations: 24
currently lose_sum: 99.21356868743896
time_elpased: 1.325
batch start
#iterations: 25
currently lose_sum: 99.16036260128021
time_elpased: 1.343
batch start
#iterations: 26
currently lose_sum: 99.2892804145813
time_elpased: 1.299
batch start
#iterations: 27
currently lose_sum: 99.11939007043839
time_elpased: 1.324
batch start
#iterations: 28
currently lose_sum: 99.11670881509781
time_elpased: 1.306
batch start
#iterations: 29
currently lose_sum: 99.06815320253372
time_elpased: 1.329
batch start
#iterations: 30
currently lose_sum: 98.9793426990509
time_elpased: 1.325
batch start
#iterations: 31
currently lose_sum: 98.97627937793732
time_elpased: 1.321
batch start
#iterations: 32
currently lose_sum: 98.99732261896133
time_elpased: 1.304
batch start
#iterations: 33
currently lose_sum: 98.95228534936905
time_elpased: 1.342
batch start
#iterations: 34
currently lose_sum: 99.12730848789215
time_elpased: 1.32
batch start
#iterations: 35
currently lose_sum: 98.83684402704239
time_elpased: 1.315
batch start
#iterations: 36
currently lose_sum: 99.14664494991302
time_elpased: 1.341
batch start
#iterations: 37
currently lose_sum: 99.04629635810852
time_elpased: 1.337
batch start
#iterations: 38
currently lose_sum: 98.668182015419
time_elpased: 1.311
batch start
#iterations: 39
currently lose_sum: 98.71913868188858
time_elpased: 1.311
start validation test
0.626288659794
0.67120055517
0.497684470516
0.571563644959
0.626514444284
63.851
batch start
#iterations: 40
currently lose_sum: 99.06735342741013
time_elpased: 1.322
batch start
#iterations: 41
currently lose_sum: 98.92776536941528
time_elpased: 1.348
batch start
#iterations: 42
currently lose_sum: 98.83644962310791
time_elpased: 1.352
batch start
#iterations: 43
currently lose_sum: 98.94195252656937
time_elpased: 1.323
batch start
#iterations: 44
currently lose_sum: 98.90334522724152
time_elpased: 1.343
batch start
#iterations: 45
currently lose_sum: 98.99215459823608
time_elpased: 1.326
batch start
#iterations: 46
currently lose_sum: 98.84725004434586
time_elpased: 1.361
batch start
#iterations: 47
currently lose_sum: 98.96649527549744
time_elpased: 1.32
batch start
#iterations: 48
currently lose_sum: 98.88221192359924
time_elpased: 1.366
batch start
#iterations: 49
currently lose_sum: 98.97369754314423
time_elpased: 1.314
batch start
#iterations: 50
currently lose_sum: 98.73999059200287
time_elpased: 1.332
batch start
#iterations: 51
currently lose_sum: 98.75018990039825
time_elpased: 1.318
batch start
#iterations: 52
currently lose_sum: 98.96127539873123
time_elpased: 1.343
batch start
#iterations: 53
currently lose_sum: 98.64892894029617
time_elpased: 1.318
batch start
#iterations: 54
currently lose_sum: 98.84733521938324
time_elpased: 1.32
batch start
#iterations: 55
currently lose_sum: 98.72250491380692
time_elpased: 1.369
batch start
#iterations: 56
currently lose_sum: 98.70866352319717
time_elpased: 1.31
batch start
#iterations: 57
currently lose_sum: 98.7881509065628
time_elpased: 1.306
batch start
#iterations: 58
currently lose_sum: 98.63030081987381
time_elpased: 1.333
batch start
#iterations: 59
currently lose_sum: 98.75353753566742
time_elpased: 1.327
start validation test
0.644329896907
0.638373121132
0.668724915097
0.653196622437
0.64428706769
63.549
batch start
#iterations: 60
currently lose_sum: 98.97725957632065
time_elpased: 1.325
batch start
#iterations: 61
currently lose_sum: 98.90933728218079
time_elpased: 1.325
batch start
#iterations: 62
currently lose_sum: 98.85422652959824
time_elpased: 1.337
batch start
#iterations: 63
currently lose_sum: 98.90102171897888
time_elpased: 1.322
batch start
#iterations: 64
currently lose_sum: 99.04080480337143
time_elpased: 1.324
batch start
#iterations: 65
currently lose_sum: 98.86859333515167
time_elpased: 1.333
batch start
#iterations: 66
currently lose_sum: 98.59366130828857
time_elpased: 1.326
batch start
#iterations: 67
currently lose_sum: 98.7996506690979
time_elpased: 1.324
batch start
#iterations: 68
currently lose_sum: 98.69618660211563
time_elpased: 1.313
batch start
#iterations: 69
currently lose_sum: 98.98502027988434
time_elpased: 1.319
batch start
#iterations: 70
currently lose_sum: 98.76640754938126
time_elpased: 1.374
batch start
#iterations: 71
currently lose_sum: 98.74141758680344
time_elpased: 1.326
batch start
#iterations: 72
currently lose_sum: 98.82913208007812
time_elpased: 1.318
batch start
#iterations: 73
currently lose_sum: 98.79700779914856
time_elpased: 1.345
batch start
#iterations: 74
currently lose_sum: 98.5478795170784
time_elpased: 1.329
batch start
#iterations: 75
currently lose_sum: 98.79415339231491
time_elpased: 1.299
batch start
#iterations: 76
currently lose_sum: 98.7059223651886
time_elpased: 1.317
batch start
#iterations: 77
currently lose_sum: 98.77266782522202
time_elpased: 1.318
batch start
#iterations: 78
currently lose_sum: 98.85071206092834
time_elpased: 1.336
batch start
#iterations: 79
currently lose_sum: 98.82248139381409
time_elpased: 1.316
start validation test
0.660360824742
0.657724889068
0.671194813214
0.664391585596
0.660341804004
63.388
batch start
#iterations: 80
currently lose_sum: 98.6777366399765
time_elpased: 1.325
batch start
#iterations: 81
currently lose_sum: 98.78183209896088
time_elpased: 1.349
batch start
#iterations: 82
currently lose_sum: 98.67235779762268
time_elpased: 1.312
batch start
#iterations: 83
currently lose_sum: 98.68350392580032
time_elpased: 1.317
batch start
#iterations: 84
currently lose_sum: 98.82757610082626
time_elpased: 1.33
batch start
#iterations: 85
currently lose_sum: 98.75401699542999
time_elpased: 1.333
batch start
#iterations: 86
currently lose_sum: 98.752685546875
time_elpased: 1.315
batch start
#iterations: 87
currently lose_sum: 98.53660297393799
time_elpased: 1.351
batch start
#iterations: 88
currently lose_sum: 98.73359167575836
time_elpased: 1.319
batch start
#iterations: 89
currently lose_sum: 98.72662329673767
time_elpased: 1.324
batch start
#iterations: 90
currently lose_sum: 98.6034694314003
time_elpased: 1.317
batch start
#iterations: 91
currently lose_sum: 98.83954524993896
time_elpased: 1.316
batch start
#iterations: 92
currently lose_sum: 98.58006793260574
time_elpased: 1.324
batch start
#iterations: 93
currently lose_sum: 98.44260776042938
time_elpased: 1.327
batch start
#iterations: 94
currently lose_sum: 98.79892575740814
time_elpased: 1.314
batch start
#iterations: 95
currently lose_sum: 98.8163286447525
time_elpased: 1.315
batch start
#iterations: 96
currently lose_sum: 98.7625281214714
time_elpased: 1.345
batch start
#iterations: 97
currently lose_sum: 98.65596038103104
time_elpased: 1.329
batch start
#iterations: 98
currently lose_sum: 98.86959594488144
time_elpased: 1.353
batch start
#iterations: 99
currently lose_sum: 98.91983270645142
time_elpased: 1.318
start validation test
0.615463917526
0.677353449631
0.443552536791
0.536069651741
0.61576573447
63.732
batch start
#iterations: 100
currently lose_sum: 98.61867386102676
time_elpased: 1.323
batch start
#iterations: 101
currently lose_sum: 98.60867273807526
time_elpased: 1.34
batch start
#iterations: 102
currently lose_sum: 98.60004252195358
time_elpased: 1.311
batch start
#iterations: 103
currently lose_sum: 98.63117533922195
time_elpased: 1.327
batch start
#iterations: 104
currently lose_sum: 98.65592169761658
time_elpased: 1.336
batch start
#iterations: 105
currently lose_sum: 98.64977407455444
time_elpased: 1.322
batch start
#iterations: 106
currently lose_sum: 98.611208319664
time_elpased: 1.34
batch start
#iterations: 107
currently lose_sum: 98.78118759393692
time_elpased: 1.305
batch start
#iterations: 108
currently lose_sum: 98.67200809717178
time_elpased: 1.338
batch start
#iterations: 109
currently lose_sum: 98.66657209396362
time_elpased: 1.338
batch start
#iterations: 110
currently lose_sum: 98.69963788986206
time_elpased: 1.33
batch start
#iterations: 111
currently lose_sum: 98.84175544977188
time_elpased: 1.33
batch start
#iterations: 112
currently lose_sum: 98.6056158542633
time_elpased: 1.313
batch start
#iterations: 113
currently lose_sum: 98.29340887069702
time_elpased: 1.344
batch start
#iterations: 114
currently lose_sum: 98.76593309640884
time_elpased: 1.32
batch start
#iterations: 115
currently lose_sum: 98.76406538486481
time_elpased: 1.317
batch start
#iterations: 116
currently lose_sum: 98.51468169689178
time_elpased: 1.315
batch start
#iterations: 117
currently lose_sum: 98.47337675094604
time_elpased: 1.307
batch start
#iterations: 118
currently lose_sum: 98.58388602733612
time_elpased: 1.329
batch start
#iterations: 119
currently lose_sum: 98.65697306394577
time_elpased: 1.317
start validation test
0.653144329897
0.661758336942
0.629000720387
0.644963857964
0.653186717727
63.127
batch start
#iterations: 120
currently lose_sum: 98.38568526506424
time_elpased: 1.325
batch start
#iterations: 121
currently lose_sum: 98.67336863279343
time_elpased: 1.336
batch start
#iterations: 122
currently lose_sum: 98.66395169496536
time_elpased: 1.349
batch start
#iterations: 123
currently lose_sum: 98.59434694051743
time_elpased: 1.336
batch start
#iterations: 124
currently lose_sum: 98.61905336380005
time_elpased: 1.358
batch start
#iterations: 125
currently lose_sum: 98.5508422255516
time_elpased: 1.325
batch start
#iterations: 126
currently lose_sum: 98.8621506690979
time_elpased: 1.334
batch start
#iterations: 127
currently lose_sum: 98.78295421600342
time_elpased: 1.361
batch start
#iterations: 128
currently lose_sum: 98.48108023405075
time_elpased: 1.335
batch start
#iterations: 129
currently lose_sum: 98.45012074708939
time_elpased: 1.313
batch start
#iterations: 130
currently lose_sum: 98.81471240520477
time_elpased: 1.33
batch start
#iterations: 131
currently lose_sum: 98.82070797681808
time_elpased: 1.309
batch start
#iterations: 132
currently lose_sum: 98.51567804813385
time_elpased: 1.34
batch start
#iterations: 133
currently lose_sum: 98.64738911390305
time_elpased: 1.337
batch start
#iterations: 134
currently lose_sum: 98.386938393116
time_elpased: 1.356
batch start
#iterations: 135
currently lose_sum: 98.70181882381439
time_elpased: 1.335
batch start
#iterations: 136
currently lose_sum: 98.49691528081894
time_elpased: 1.335
batch start
#iterations: 137
currently lose_sum: 98.59340417385101
time_elpased: 1.316
batch start
#iterations: 138
currently lose_sum: 98.86241829395294
time_elpased: 1.319
batch start
#iterations: 139
currently lose_sum: 98.48909032344818
time_elpased: 1.332
start validation test
0.617474226804
0.682686187142
0.441494288361
0.53621648647
0.617783186729
63.398
batch start
#iterations: 140
currently lose_sum: 98.55919682979584
time_elpased: 1.311
batch start
#iterations: 141
currently lose_sum: 98.64999806880951
time_elpased: 1.306
batch start
#iterations: 142
currently lose_sum: 98.81941175460815
time_elpased: 1.346
batch start
#iterations: 143
currently lose_sum: 98.5833809375763
time_elpased: 1.335
batch start
#iterations: 144
currently lose_sum: 98.65457659959793
time_elpased: 1.329
batch start
#iterations: 145
currently lose_sum: 98.962919652462
time_elpased: 1.334
batch start
#iterations: 146
currently lose_sum: 98.5122988820076
time_elpased: 1.317
batch start
#iterations: 147
currently lose_sum: 98.57229423522949
time_elpased: 1.33
batch start
#iterations: 148
currently lose_sum: 98.60994303226471
time_elpased: 1.339
batch start
#iterations: 149
currently lose_sum: 98.72678852081299
time_elpased: 1.331
batch start
#iterations: 150
currently lose_sum: 98.34177631139755
time_elpased: 1.325
batch start
#iterations: 151
currently lose_sum: 98.66119229793549
time_elpased: 1.339
batch start
#iterations: 152
currently lose_sum: 98.398670732975
time_elpased: 1.34
batch start
#iterations: 153
currently lose_sum: 98.73247915506363
time_elpased: 1.348
batch start
#iterations: 154
currently lose_sum: 98.48708522319794
time_elpased: 1.361
batch start
#iterations: 155
currently lose_sum: 98.69103175401688
time_elpased: 1.315
batch start
#iterations: 156
currently lose_sum: 98.77789252996445
time_elpased: 1.327
batch start
#iterations: 157
currently lose_sum: 98.43832790851593
time_elpased: 1.351
batch start
#iterations: 158
currently lose_sum: 98.73291313648224
time_elpased: 1.326
batch start
#iterations: 159
currently lose_sum: 98.60410153865814
time_elpased: 1.313
start validation test
0.642164948454
0.684679888194
0.529381496347
0.597098084736
0.642362957199
63.195
batch start
#iterations: 160
currently lose_sum: 98.75913941860199
time_elpased: 1.341
batch start
#iterations: 161
currently lose_sum: 98.56456422805786
time_elpased: 1.32
batch start
#iterations: 162
currently lose_sum: 98.53939801454544
time_elpased: 1.342
batch start
#iterations: 163
currently lose_sum: 98.50198048353195
time_elpased: 1.316
batch start
#iterations: 164
currently lose_sum: 98.54393339157104
time_elpased: 1.335
batch start
#iterations: 165
currently lose_sum: 98.54151636362076
time_elpased: 1.338
batch start
#iterations: 166
currently lose_sum: 98.45617115497589
time_elpased: 1.333
batch start
#iterations: 167
currently lose_sum: 98.8972424864769
time_elpased: 1.336
batch start
#iterations: 168
currently lose_sum: 98.77435100078583
time_elpased: 1.332
batch start
#iterations: 169
currently lose_sum: 98.79538840055466
time_elpased: 1.318
batch start
#iterations: 170
currently lose_sum: 98.27549767494202
time_elpased: 1.347
batch start
#iterations: 171
currently lose_sum: 98.77828085422516
time_elpased: 1.336
batch start
#iterations: 172
currently lose_sum: 98.56535971164703
time_elpased: 1.323
batch start
#iterations: 173
currently lose_sum: 98.45874798297882
time_elpased: 1.35
batch start
#iterations: 174
currently lose_sum: 98.38259345293045
time_elpased: 1.356
batch start
#iterations: 175
currently lose_sum: 98.58947265148163
time_elpased: 1.34
batch start
#iterations: 176
currently lose_sum: 98.63270342350006
time_elpased: 1.318
batch start
#iterations: 177
currently lose_sum: 98.57331073284149
time_elpased: 1.314
batch start
#iterations: 178
currently lose_sum: 98.50481361150742
time_elpased: 1.336
batch start
#iterations: 179
currently lose_sum: 98.52800965309143
time_elpased: 1.335
start validation test
0.662164948454
0.66414115205
0.658536585366
0.661326994626
0.662171318605
63.012
batch start
#iterations: 180
currently lose_sum: 98.60820692777634
time_elpased: 1.352
batch start
#iterations: 181
currently lose_sum: 98.64152079820633
time_elpased: 1.339
batch start
#iterations: 182
currently lose_sum: 98.6071949005127
time_elpased: 1.343
batch start
#iterations: 183
currently lose_sum: 98.49803805351257
time_elpased: 1.316
batch start
#iterations: 184
currently lose_sum: 98.48651015758514
time_elpased: 1.332
batch start
#iterations: 185
currently lose_sum: 98.42651969194412
time_elpased: 1.349
batch start
#iterations: 186
currently lose_sum: 98.49867063760757
time_elpased: 1.326
batch start
#iterations: 187
currently lose_sum: 98.60412192344666
time_elpased: 1.322
batch start
#iterations: 188
currently lose_sum: 98.65302407741547
time_elpased: 1.329
batch start
#iterations: 189
currently lose_sum: 98.55868232250214
time_elpased: 1.381
batch start
#iterations: 190
currently lose_sum: 98.41839629411697
time_elpased: 1.317
batch start
#iterations: 191
currently lose_sum: 98.2578946352005
time_elpased: 1.349
batch start
#iterations: 192
currently lose_sum: 98.646943628788
time_elpased: 1.337
batch start
#iterations: 193
currently lose_sum: 98.50646829605103
time_elpased: 1.32
batch start
#iterations: 194
currently lose_sum: 98.54923683404922
time_elpased: 1.341
batch start
#iterations: 195
currently lose_sum: 98.50321263074875
time_elpased: 1.318
batch start
#iterations: 196
currently lose_sum: 98.48996186256409
time_elpased: 1.321
batch start
#iterations: 197
currently lose_sum: 98.47003310918808
time_elpased: 1.368
batch start
#iterations: 198
currently lose_sum: 98.46827119588852
time_elpased: 1.331
batch start
#iterations: 199
currently lose_sum: 98.45951122045517
time_elpased: 1.321
start validation test
0.66706185567
0.653418722923
0.71400638057
0.682370297517
0.666979437316
63.145
batch start
#iterations: 200
currently lose_sum: 98.47797185182571
time_elpased: 1.332
batch start
#iterations: 201
currently lose_sum: 98.25316643714905
time_elpased: 1.32
batch start
#iterations: 202
currently lose_sum: 98.53539150953293
time_elpased: 1.337
batch start
#iterations: 203
currently lose_sum: 98.67610841989517
time_elpased: 1.316
batch start
#iterations: 204
currently lose_sum: 98.52873957157135
time_elpased: 1.343
batch start
#iterations: 205
currently lose_sum: 98.53085696697235
time_elpased: 1.332
batch start
#iterations: 206
currently lose_sum: 98.54118436574936
time_elpased: 1.334
batch start
#iterations: 207
currently lose_sum: 98.43167734146118
time_elpased: 1.622
batch start
#iterations: 208
currently lose_sum: 98.41777020692825
time_elpased: 2.144
batch start
#iterations: 209
currently lose_sum: 98.4165335893631
time_elpased: 1.777
batch start
#iterations: 210
currently lose_sum: 98.46029680967331
time_elpased: 2.155
batch start
#iterations: 211
currently lose_sum: 98.49969547986984
time_elpased: 2.383
batch start
#iterations: 212
currently lose_sum: 98.40216898918152
time_elpased: 2.238
batch start
#iterations: 213
currently lose_sum: 98.40999120473862
time_elpased: 2.283
batch start
#iterations: 214
currently lose_sum: 98.635089635849
time_elpased: 2.086
batch start
#iterations: 215
currently lose_sum: 98.2323003411293
time_elpased: 2.138
batch start
#iterations: 216
currently lose_sum: 98.45163303613663
time_elpased: 1.997
batch start
#iterations: 217
currently lose_sum: 98.58755499124527
time_elpased: 2.45
batch start
#iterations: 218
currently lose_sum: 98.40815705060959
time_elpased: 2.021
batch start
#iterations: 219
currently lose_sum: 98.48383235931396
time_elpased: 2.278
start validation test
0.62793814433
0.687191011236
0.472059277555
0.559663250366
0.628211813723
63.218
batch start
#iterations: 220
currently lose_sum: 98.66566801071167
time_elpased: 2.18
batch start
#iterations: 221
currently lose_sum: 98.46313112974167
time_elpased: 2.191
batch start
#iterations: 222
currently lose_sum: 98.48821538686752
time_elpased: 2.534
batch start
#iterations: 223
currently lose_sum: 98.67095273733139
time_elpased: 2.05
batch start
#iterations: 224
currently lose_sum: 98.48125046491623
time_elpased: 2.109
batch start
#iterations: 225
currently lose_sum: 98.50707513093948
time_elpased: 2.152
batch start
#iterations: 226
currently lose_sum: 98.30828529596329
time_elpased: 2.192
batch start
#iterations: 227
currently lose_sum: 98.3391962647438
time_elpased: 2.116
batch start
#iterations: 228
currently lose_sum: 98.69773977994919
time_elpased: 2.303
batch start
#iterations: 229
currently lose_sum: 98.53445041179657
time_elpased: 1.853
batch start
#iterations: 230
currently lose_sum: 98.46390187740326
time_elpased: 2.095
batch start
#iterations: 231
currently lose_sum: 98.69939798116684
time_elpased: 2.199
batch start
#iterations: 232
currently lose_sum: 98.55069863796234
time_elpased: 2.186
batch start
#iterations: 233
currently lose_sum: 98.52949243783951
time_elpased: 2.094
batch start
#iterations: 234
currently lose_sum: 98.4791909456253
time_elpased: 2.257
batch start
#iterations: 235
currently lose_sum: 98.56537264585495
time_elpased: 2.169
batch start
#iterations: 236
currently lose_sum: 98.28485298156738
time_elpased: 2.149
batch start
#iterations: 237
currently lose_sum: 98.79149115085602
time_elpased: 2.249
batch start
#iterations: 238
currently lose_sum: 98.34725952148438
time_elpased: 2.156
batch start
#iterations: 239
currently lose_sum: 98.49375140666962
time_elpased: 2.027
start validation test
0.645
0.679568527919
0.551096017289
0.608626470421
0.645164862925
63.174
batch start
#iterations: 240
currently lose_sum: 98.74752604961395
time_elpased: 2.158
batch start
#iterations: 241
currently lose_sum: 98.64126461744308
time_elpased: 2.166
batch start
#iterations: 242
currently lose_sum: 98.31415969133377
time_elpased: 2.167
batch start
#iterations: 243
currently lose_sum: 98.56180441379547
time_elpased: 2.039
batch start
#iterations: 244
currently lose_sum: 98.53711187839508
time_elpased: 1.855
batch start
#iterations: 245
currently lose_sum: 98.4078426361084
time_elpased: 2.321
batch start
#iterations: 246
currently lose_sum: 98.72463637590408
time_elpased: 2.134
batch start
#iterations: 247
currently lose_sum: 98.56358903646469
time_elpased: 2.184
batch start
#iterations: 248
currently lose_sum: 98.57700699567795
time_elpased: 2.302
batch start
#iterations: 249
currently lose_sum: 98.4279505610466
time_elpased: 2.288
batch start
#iterations: 250
currently lose_sum: 98.56570875644684
time_elpased: 2.214
batch start
#iterations: 251
currently lose_sum: 98.53847968578339
time_elpased: 1.912
batch start
#iterations: 252
currently lose_sum: 98.46172577142715
time_elpased: 2.097
batch start
#iterations: 253
currently lose_sum: 98.46176171302795
time_elpased: 2.108
batch start
#iterations: 254
currently lose_sum: 98.54783272743225
time_elpased: 2.153
batch start
#iterations: 255
currently lose_sum: 98.55140143632889
time_elpased: 2.128
batch start
#iterations: 256
currently lose_sum: 98.1590706706047
time_elpased: 2.213
batch start
#iterations: 257
currently lose_sum: 98.59268462657928
time_elpased: 2.126
batch start
#iterations: 258
currently lose_sum: 98.12143647670746
time_elpased: 2.249
batch start
#iterations: 259
currently lose_sum: 98.47677320241928
time_elpased: 2.136
start validation test
0.606855670103
0.686874105866
0.395286611094
0.501796328957
0.607227112218
63.440
batch start
#iterations: 260
currently lose_sum: 98.5398536324501
time_elpased: 2.158
batch start
#iterations: 261
currently lose_sum: 98.52062207460403
time_elpased: 1.994
batch start
#iterations: 262
currently lose_sum: 98.39330071210861
time_elpased: 2.43
batch start
#iterations: 263
currently lose_sum: 98.65532320737839
time_elpased: 2.27
batch start
#iterations: 264
currently lose_sum: 98.42022293806076
time_elpased: 2.353
batch start
#iterations: 265
currently lose_sum: 98.53590893745422
time_elpased: 1.88
batch start
#iterations: 266
currently lose_sum: 98.48387664556503
time_elpased: 2.206
batch start
#iterations: 267
currently lose_sum: 98.59169089794159
time_elpased: 2.502
batch start
#iterations: 268
currently lose_sum: 98.40253961086273
time_elpased: 2.56
batch start
#iterations: 269
currently lose_sum: 98.66727322340012
time_elpased: 3.014
batch start
#iterations: 270
currently lose_sum: 98.49650478363037
time_elpased: 3.174
batch start
#iterations: 271
currently lose_sum: 98.35260319709778
time_elpased: 3.512
batch start
#iterations: 272
currently lose_sum: 98.61988294124603
time_elpased: 3.366
batch start
#iterations: 273
currently lose_sum: 98.35392397642136
time_elpased: 3.266
batch start
#iterations: 274
currently lose_sum: 98.7455507516861
time_elpased: 3.068
batch start
#iterations: 275
currently lose_sum: 98.4681413769722
time_elpased: 3.302
batch start
#iterations: 276
currently lose_sum: 98.49827861785889
time_elpased: 3.286
batch start
#iterations: 277
currently lose_sum: 98.66900187730789
time_elpased: 3.392
batch start
#iterations: 278
currently lose_sum: 98.61878949403763
time_elpased: 3.426
batch start
#iterations: 279
currently lose_sum: 98.36043173074722
time_elpased: 3.297
start validation test
0.640154639175
0.684565569347
0.52217762684
0.592445560161
0.640361766017
63.118
batch start
#iterations: 280
currently lose_sum: 98.4272570014
time_elpased: 3.327
batch start
#iterations: 281
currently lose_sum: 98.32268434762955
time_elpased: 3.168
batch start
#iterations: 282
currently lose_sum: 98.53947114944458
time_elpased: 3.394
batch start
#iterations: 283
currently lose_sum: 98.40271782875061
time_elpased: 3.025
batch start
#iterations: 284
currently lose_sum: 98.313456594944
time_elpased: 3.291
batch start
#iterations: 285
currently lose_sum: 98.45564836263657
time_elpased: 3.207
batch start
#iterations: 286
currently lose_sum: 98.40797710418701
time_elpased: 3.31
batch start
#iterations: 287
currently lose_sum: 98.43990790843964
time_elpased: 3.094
batch start
#iterations: 288
currently lose_sum: 98.44986015558243
time_elpased: 3.321
batch start
#iterations: 289
currently lose_sum: 98.46241575479507
time_elpased: 2.912
batch start
#iterations: 290
currently lose_sum: 98.44469094276428
time_elpased: 3.167
batch start
#iterations: 291
currently lose_sum: 98.39229077100754
time_elpased: 3.41
batch start
#iterations: 292
currently lose_sum: 98.43109053373337
time_elpased: 3.077
batch start
#iterations: 293
currently lose_sum: 98.35209810733795
time_elpased: 3.21
batch start
#iterations: 294
currently lose_sum: 98.32474076747894
time_elpased: 3.401
batch start
#iterations: 295
currently lose_sum: 98.41976833343506
time_elpased: 3.052
batch start
#iterations: 296
currently lose_sum: 98.22966527938843
time_elpased: 2.956
batch start
#iterations: 297
currently lose_sum: 98.78956353664398
time_elpased: 3.243
batch start
#iterations: 298
currently lose_sum: 98.4419754743576
time_elpased: 3.242
batch start
#iterations: 299
currently lose_sum: 98.50927352905273
time_elpased: 3.267
start validation test
0.648402061856
0.68559343758
0.55047854276
0.610651292882
0.648573981697
62.919
batch start
#iterations: 300
currently lose_sum: 98.44701898097992
time_elpased: 3.4
batch start
#iterations: 301
currently lose_sum: 98.42884773015976
time_elpased: 3.024
batch start
#iterations: 302
currently lose_sum: 98.51210260391235
time_elpased: 3.179
batch start
#iterations: 303
currently lose_sum: 98.43528658151627
time_elpased: 3.129
batch start
#iterations: 304
currently lose_sum: 98.46046662330627
time_elpased: 3.148
batch start
#iterations: 305
currently lose_sum: 98.18398731946945
time_elpased: 1.314
batch start
#iterations: 306
currently lose_sum: 98.43927717208862
time_elpased: 1.298
batch start
#iterations: 307
currently lose_sum: 98.35318928956985
time_elpased: 1.351
batch start
#iterations: 308
currently lose_sum: 98.6032492518425
time_elpased: 1.34
batch start
#iterations: 309
currently lose_sum: 98.60308307409286
time_elpased: 1.325
batch start
#iterations: 310
currently lose_sum: 98.53328710794449
time_elpased: 1.334
batch start
#iterations: 311
currently lose_sum: 98.38241654634476
time_elpased: 1.334
batch start
#iterations: 312
currently lose_sum: 98.57948523759842
time_elpased: 1.317
batch start
#iterations: 313
currently lose_sum: 98.4308916926384
time_elpased: 1.35
batch start
#iterations: 314
currently lose_sum: 98.2853941321373
time_elpased: 1.311
batch start
#iterations: 315
currently lose_sum: 98.44199579954147
time_elpased: 1.3
batch start
#iterations: 316
currently lose_sum: 98.29712551832199
time_elpased: 1.311
batch start
#iterations: 317
currently lose_sum: 98.60968035459518
time_elpased: 1.32
batch start
#iterations: 318
currently lose_sum: 98.48443973064423
time_elpased: 1.319
batch start
#iterations: 319
currently lose_sum: 98.28488820791245
time_elpased: 1.314
start validation test
0.630412371134
0.685776805252
0.483791293609
0.567342505431
0.63066978705
63.056
batch start
#iterations: 320
currently lose_sum: 98.46519714593887
time_elpased: 1.322
batch start
#iterations: 321
currently lose_sum: 98.11510902643204
time_elpased: 1.316
batch start
#iterations: 322
currently lose_sum: 98.55350822210312
time_elpased: 1.317
batch start
#iterations: 323
currently lose_sum: 98.50706994533539
time_elpased: 1.331
batch start
#iterations: 324
currently lose_sum: 98.5146614909172
time_elpased: 1.329
batch start
#iterations: 325
currently lose_sum: 98.56252956390381
time_elpased: 1.322
batch start
#iterations: 326
currently lose_sum: 98.65854579210281
time_elpased: 1.317
batch start
#iterations: 327
currently lose_sum: 98.3443654179573
time_elpased: 1.324
batch start
#iterations: 328
currently lose_sum: 98.3773780465126
time_elpased: 1.344
batch start
#iterations: 329
currently lose_sum: 98.36062788963318
time_elpased: 1.319
batch start
#iterations: 330
currently lose_sum: 98.36117213964462
time_elpased: 1.336
batch start
#iterations: 331
currently lose_sum: 98.56939554214478
time_elpased: 1.327
batch start
#iterations: 332
currently lose_sum: 98.4321318268776
time_elpased: 1.313
batch start
#iterations: 333
currently lose_sum: 98.54197776317596
time_elpased: 1.321
batch start
#iterations: 334
currently lose_sum: 98.73597693443298
time_elpased: 1.373
batch start
#iterations: 335
currently lose_sum: 98.47616118192673
time_elpased: 1.336
batch start
#iterations: 336
currently lose_sum: 98.41300857067108
time_elpased: 1.319
batch start
#iterations: 337
currently lose_sum: 98.48684507608414
time_elpased: 1.326
batch start
#iterations: 338
currently lose_sum: 98.30421620607376
time_elpased: 1.325
batch start
#iterations: 339
currently lose_sum: 98.29807615280151
time_elpased: 1.347
start validation test
0.659175257732
0.677733257012
0.609241535453
0.641664860178
0.659262924083
62.898
batch start
#iterations: 340
currently lose_sum: 98.32602739334106
time_elpased: 1.335
batch start
#iterations: 341
currently lose_sum: 98.57302260398865
time_elpased: 1.31
batch start
#iterations: 342
currently lose_sum: 98.52067977190018
time_elpased: 1.32
batch start
#iterations: 343
currently lose_sum: 98.43745106458664
time_elpased: 1.322
batch start
#iterations: 344
currently lose_sum: 98.46406751871109
time_elpased: 1.32
batch start
#iterations: 345
currently lose_sum: 98.35282975435257
time_elpased: 1.323
batch start
#iterations: 346
currently lose_sum: 98.36636346578598
time_elpased: 1.306
batch start
#iterations: 347
currently lose_sum: 98.17022740840912
time_elpased: 1.359
batch start
#iterations: 348
currently lose_sum: 98.68172216415405
time_elpased: 1.308
batch start
#iterations: 349
currently lose_sum: 98.30716252326965
time_elpased: 1.332
batch start
#iterations: 350
currently lose_sum: 98.39013516902924
time_elpased: 1.316
batch start
#iterations: 351
currently lose_sum: 98.47743302583694
time_elpased: 1.309
batch start
#iterations: 352
currently lose_sum: 98.19629508256912
time_elpased: 1.327
batch start
#iterations: 353
currently lose_sum: 98.36842530965805
time_elpased: 1.32
batch start
#iterations: 354
currently lose_sum: 98.41912686824799
time_elpased: 1.327
batch start
#iterations: 355
currently lose_sum: 98.2803008556366
time_elpased: 1.334
batch start
#iterations: 356
currently lose_sum: 98.42043745517731
time_elpased: 1.317
batch start
#iterations: 357
currently lose_sum: 98.39449417591095
time_elpased: 1.323
batch start
#iterations: 358
currently lose_sum: 98.2627221941948
time_elpased: 1.337
batch start
#iterations: 359
currently lose_sum: 98.24452883005142
time_elpased: 1.324
start validation test
0.649175257732
0.677132773518
0.572604713389
0.62049737928
0.649309689133
63.019
batch start
#iterations: 360
currently lose_sum: 98.35509705543518
time_elpased: 1.37
batch start
#iterations: 361
currently lose_sum: 98.48351776599884
time_elpased: 1.322
batch start
#iterations: 362
currently lose_sum: 98.55986845493317
time_elpased: 1.321
batch start
#iterations: 363
currently lose_sum: 98.38429284095764
time_elpased: 1.328
batch start
#iterations: 364
currently lose_sum: 98.54305809736252
time_elpased: 1.316
batch start
#iterations: 365
currently lose_sum: 98.41808658838272
time_elpased: 1.33
batch start
#iterations: 366
currently lose_sum: 98.3756816983223
time_elpased: 1.315
batch start
#iterations: 367
currently lose_sum: 98.3544824719429
time_elpased: 1.308
batch start
#iterations: 368
currently lose_sum: 98.51921164989471
time_elpased: 1.357
batch start
#iterations: 369
currently lose_sum: 98.5262701511383
time_elpased: 1.326
batch start
#iterations: 370
currently lose_sum: 98.50052064657211
time_elpased: 1.319
batch start
#iterations: 371
currently lose_sum: 98.34620583057404
time_elpased: 1.333
batch start
#iterations: 372
currently lose_sum: 98.4285758137703
time_elpased: 1.315
batch start
#iterations: 373
currently lose_sum: 98.58554589748383
time_elpased: 1.31
batch start
#iterations: 374
currently lose_sum: 98.54477852582932
time_elpased: 1.32
batch start
#iterations: 375
currently lose_sum: 98.35396599769592
time_elpased: 1.315
batch start
#iterations: 376
currently lose_sum: 98.3809552192688
time_elpased: 1.309
batch start
#iterations: 377
currently lose_sum: 98.43177682161331
time_elpased: 1.315
batch start
#iterations: 378
currently lose_sum: 98.41873377561569
time_elpased: 1.338
batch start
#iterations: 379
currently lose_sum: 98.40699678659439
time_elpased: 1.331
start validation test
0.65175257732
0.68070303918
0.573942574869
0.622780569514
0.65188918478
63.048
batch start
#iterations: 380
currently lose_sum: 98.54238229990005
time_elpased: 1.325
batch start
#iterations: 381
currently lose_sum: 98.41699069738388
time_elpased: 1.309
batch start
#iterations: 382
currently lose_sum: 98.26782256364822
time_elpased: 1.3
batch start
#iterations: 383
currently lose_sum: 98.09157383441925
time_elpased: 1.316
batch start
#iterations: 384
currently lose_sum: 98.30752718448639
time_elpased: 1.355
batch start
#iterations: 385
currently lose_sum: 98.72128403186798
time_elpased: 1.306
batch start
#iterations: 386
currently lose_sum: 98.5356787443161
time_elpased: 1.311
batch start
#iterations: 387
currently lose_sum: 98.41609019041061
time_elpased: 1.329
batch start
#iterations: 388
currently lose_sum: 98.80591470003128
time_elpased: 1.332
batch start
#iterations: 389
currently lose_sum: 98.34061658382416
time_elpased: 1.323
batch start
#iterations: 390
currently lose_sum: 98.34885829687119
time_elpased: 1.317
batch start
#iterations: 391
currently lose_sum: 98.34039705991745
time_elpased: 1.334
batch start
#iterations: 392
currently lose_sum: 98.48353534936905
time_elpased: 1.353
batch start
#iterations: 393
currently lose_sum: 98.30631136894226
time_elpased: 1.309
batch start
#iterations: 394
currently lose_sum: 98.17326325178146
time_elpased: 1.321
batch start
#iterations: 395
currently lose_sum: 98.32854914665222
time_elpased: 1.322
batch start
#iterations: 396
currently lose_sum: 98.14974504709244
time_elpased: 1.341
batch start
#iterations: 397
currently lose_sum: 98.5039974451065
time_elpased: 1.318
batch start
#iterations: 398
currently lose_sum: 98.2188413143158
time_elpased: 1.329
batch start
#iterations: 399
currently lose_sum: 98.18346786499023
time_elpased: 1.332
start validation test
0.615979381443
0.6876344976
0.427498199033
0.527224267039
0.61631028923
63.117
acc: 0.657
pre: 0.673
rec: 0.610
F1: 0.640
auc: 0.706
