start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.2760677933693
time_elpased: 2.343
batch start
#iterations: 1
currently lose_sum: 99.95471805334091
time_elpased: 2.498
batch start
#iterations: 2
currently lose_sum: 99.65195006132126
time_elpased: 2.266
batch start
#iterations: 3
currently lose_sum: 99.49165314435959
time_elpased: 2.447
batch start
#iterations: 4
currently lose_sum: 99.50181204080582
time_elpased: 2.438
batch start
#iterations: 5
currently lose_sum: 99.40002632141113
time_elpased: 2.496
batch start
#iterations: 6
currently lose_sum: 99.35358101129532
time_elpased: 2.459
batch start
#iterations: 7
currently lose_sum: 99.2021079659462
time_elpased: 2.394
batch start
#iterations: 8
currently lose_sum: 99.39789444208145
time_elpased: 1.898
batch start
#iterations: 9
currently lose_sum: 99.00314491987228
time_elpased: 1.947
batch start
#iterations: 10
currently lose_sum: 99.17220479249954
time_elpased: 1.861
batch start
#iterations: 11
currently lose_sum: 98.89609551429749
time_elpased: 1.861
batch start
#iterations: 12
currently lose_sum: 98.9607954621315
time_elpased: 2.157
batch start
#iterations: 13
currently lose_sum: 99.02811515331268
time_elpased: 2.028
batch start
#iterations: 14
currently lose_sum: 98.94314163923264
time_elpased: 1.781
batch start
#iterations: 15
currently lose_sum: 98.71214872598648
time_elpased: 2.017
batch start
#iterations: 16
currently lose_sum: 99.11032861471176
time_elpased: 1.955
batch start
#iterations: 17
currently lose_sum: 98.7686031460762
time_elpased: 2.365
batch start
#iterations: 18
currently lose_sum: 98.50818717479706
time_elpased: 2.737
batch start
#iterations: 19
currently lose_sum: 98.49104934930801
time_elpased: 2.503
start validation test
0.634432989691
0.630029732408
0.654281597365
0.641926688882
0.634400195626
63.279
batch start
#iterations: 20
currently lose_sum: 98.75545436143875
time_elpased: 2.266
batch start
#iterations: 21
currently lose_sum: 98.37712520360947
time_elpased: 2.648
batch start
#iterations: 22
currently lose_sum: 99.67367249727249
time_elpased: 2.521
batch start
#iterations: 23
currently lose_sum: 99.13183355331421
time_elpased: 2.485
batch start
#iterations: 24
currently lose_sum: 98.74070584774017
time_elpased: 2.562
batch start
#iterations: 25
currently lose_sum: 98.28444463014603
time_elpased: 2.326
batch start
#iterations: 26
currently lose_sum: 98.31903678178787
time_elpased: 2.25
batch start
#iterations: 27
currently lose_sum: 98.2485044002533
time_elpased: 2.054
batch start
#iterations: 28
currently lose_sum: 98.61141955852509
time_elpased: 2.479
batch start
#iterations: 29
currently lose_sum: 98.30100476741791
time_elpased: 2.559
batch start
#iterations: 30
currently lose_sum: 98.76891130208969
time_elpased: 2.448
batch start
#iterations: 31
currently lose_sum: 98.21074151992798
time_elpased: 2.395
batch start
#iterations: 32
currently lose_sum: 98.4231504201889
time_elpased: 2.453
batch start
#iterations: 33
currently lose_sum: 98.31057929992676
time_elpased: 2.122
batch start
#iterations: 34
currently lose_sum: 98.02774888277054
time_elpased: 2.173
batch start
#iterations: 35
currently lose_sum: 97.87050133943558
time_elpased: 2.424
batch start
#iterations: 36
currently lose_sum: 98.33345782756805
time_elpased: 2.508
batch start
#iterations: 37
currently lose_sum: 98.06671953201294
time_elpased: 2.621
batch start
#iterations: 38
currently lose_sum: 97.87197029590607
time_elpased: 2.179
batch start
#iterations: 39
currently lose_sum: 98.10060316324234
time_elpased: 2.518
start validation test
0.634432989691
0.63626921479
0.630506381227
0.633374689826
0.634439477272
63.357
batch start
#iterations: 40
currently lose_sum: 97.95671427249908
time_elpased: 2.423
batch start
#iterations: 41
currently lose_sum: 97.74231964349747
time_elpased: 2.277
batch start
#iterations: 42
currently lose_sum: 97.53119629621506
time_elpased: 2.489
batch start
#iterations: 43
currently lose_sum: 97.74693661928177
time_elpased: 2.494
batch start
#iterations: 44
currently lose_sum: 96.94145959615707
time_elpased: 2.511
batch start
#iterations: 45
currently lose_sum: 97.50813937187195
time_elpased: 2.424
batch start
#iterations: 46
currently lose_sum: 96.82153052091599
time_elpased: 2.388
batch start
#iterations: 47
currently lose_sum: 97.35645908117294
time_elpased: 2.488
batch start
#iterations: 48
currently lose_sum: 97.68528133630753
time_elpased: 2.466
batch start
#iterations: 49
currently lose_sum: 96.5338146686554
time_elpased: 2.486
batch start
#iterations: 50
currently lose_sum: 97.01705133914948
time_elpased: 1.991
batch start
#iterations: 51
currently lose_sum: 96.95908272266388
time_elpased: 2.474
batch start
#iterations: 52
currently lose_sum: 97.02013522386551
time_elpased: 1.911
batch start
#iterations: 53
currently lose_sum: 96.22633790969849
time_elpased: 2.074
batch start
#iterations: 54
currently lose_sum: 96.77318602800369
time_elpased: 2.476
batch start
#iterations: 55
currently lose_sum: 96.30823540687561
time_elpased: 2.442
batch start
#iterations: 56
currently lose_sum: 96.80742144584656
time_elpased: 2.409
batch start
#iterations: 57
currently lose_sum: 95.8496453166008
time_elpased: 2.474
batch start
#iterations: 58
currently lose_sum: 96.31151413917542
time_elpased: 2.238
batch start
#iterations: 59
currently lose_sum: 96.62754410505295
time_elpased: 2.414
start validation test
0.616958762887
0.58675677728
0.795286125978
0.675289490933
0.616664128664
63.278
batch start
#iterations: 60
currently lose_sum: 95.61007809638977
time_elpased: 2.33
batch start
#iterations: 61
currently lose_sum: 96.2328108549118
time_elpased: 2.421
batch start
#iterations: 62
currently lose_sum: 96.17308163642883
time_elpased: 2.427
batch start
#iterations: 63
currently lose_sum: 96.41276335716248
time_elpased: 2.464
batch start
#iterations: 64
currently lose_sum: 96.48836427927017
time_elpased: 2.497
batch start
#iterations: 65
currently lose_sum: 96.61063706874847
time_elpased: 2.474
batch start
#iterations: 66
currently lose_sum: 96.12349039316177
time_elpased: 2.27
batch start
#iterations: 67
currently lose_sum: 95.6526734828949
time_elpased: 2.389
batch start
#iterations: 68
currently lose_sum: 95.9188284277916
time_elpased: 1.955
batch start
#iterations: 69
currently lose_sum: 95.47572833299637
time_elpased: 1.865
batch start
#iterations: 70
currently lose_sum: 96.02092880010605
time_elpased: 1.69
batch start
#iterations: 71
currently lose_sum: 95.76280844211578
time_elpased: 1.556
batch start
#iterations: 72
currently lose_sum: 95.93629437685013
time_elpased: 1.574
batch start
#iterations: 73
currently lose_sum: 96.42488139867783
time_elpased: 1.573
batch start
#iterations: 74
currently lose_sum: 95.248650431633
time_elpased: 1.698
batch start
#iterations: 75
currently lose_sum: 95.51422482728958
time_elpased: 1.59
batch start
#iterations: 76
currently lose_sum: 95.5370060801506
time_elpased: 1.503
batch start
#iterations: 77
currently lose_sum: 95.4165186882019
time_elpased: 1.477
batch start
#iterations: 78
currently lose_sum: 95.960245013237
time_elpased: 1.509
batch start
#iterations: 79
currently lose_sum: 95.96762186288834
time_elpased: 1.443
start validation test
0.644278350515
0.605914666265
0.828736105393
0.700021734406
0.6439735876
61.613
batch start
#iterations: 80
currently lose_sum: 95.20042437314987
time_elpased: 1.476
batch start
#iterations: 81
currently lose_sum: 95.15408307313919
time_elpased: 1.625
batch start
#iterations: 82
currently lose_sum: 95.20890462398529
time_elpased: 1.491
batch start
#iterations: 83
currently lose_sum: 95.17254853248596
time_elpased: 1.635
batch start
#iterations: 84
currently lose_sum: 95.37906628847122
time_elpased: 1.693
batch start
#iterations: 85
currently lose_sum: 95.76301670074463
time_elpased: 1.605
batch start
#iterations: 86
currently lose_sum: 95.34467250108719
time_elpased: 1.808
batch start
#iterations: 87
currently lose_sum: 95.1061469912529
time_elpased: 2.272
batch start
#iterations: 88
currently lose_sum: 94.85612338781357
time_elpased: 1.7
batch start
#iterations: 89
currently lose_sum: 95.14620387554169
time_elpased: 2.185
batch start
#iterations: 90
currently lose_sum: 95.748659491539
time_elpased: 2.356
batch start
#iterations: 91
currently lose_sum: 95.34578430652618
time_elpased: 2.298
batch start
#iterations: 92
currently lose_sum: 95.44541120529175
time_elpased: 2.696
batch start
#iterations: 93
currently lose_sum: 94.74281841516495
time_elpased: 2.574
batch start
#iterations: 94
currently lose_sum: 94.73218238353729
time_elpased: 2.371
batch start
#iterations: 95
currently lose_sum: 95.20382362604141
time_elpased: 2.282
batch start
#iterations: 96
currently lose_sum: 94.68103814125061
time_elpased: 2.722
batch start
#iterations: 97
currently lose_sum: 94.86618775129318
time_elpased: 2.566
batch start
#iterations: 98
currently lose_sum: 94.78050827980042
time_elpased: 2.497
batch start
#iterations: 99
currently lose_sum: 94.8721051812172
time_elpased: 2.433
start validation test
0.63618556701
0.599744821375
0.822457801564
0.693663194444
0.635877806193
61.936
batch start
#iterations: 100
currently lose_sum: 95.0965376496315
time_elpased: 2.298
batch start
#iterations: 101
currently lose_sum: 94.84461170434952
time_elpased: 2.219
batch start
#iterations: 102
currently lose_sum: 95.07348173856735
time_elpased: 2.145
batch start
#iterations: 103
currently lose_sum: 95.17719960212708
time_elpased: 2.311
batch start
#iterations: 104
currently lose_sum: 94.82746970653534
time_elpased: 2.433
batch start
#iterations: 105
currently lose_sum: 95.19194376468658
time_elpased: 2.42
batch start
#iterations: 106
currently lose_sum: 94.68298780918121
time_elpased: 2.129
batch start
#iterations: 107
currently lose_sum: 94.7934867143631
time_elpased: 2.45
batch start
#iterations: 108
currently lose_sum: 94.40073895454407
time_elpased: 2.126
batch start
#iterations: 109
currently lose_sum: 95.57416760921478
time_elpased: 1.98
batch start
#iterations: 110
currently lose_sum: 94.70647913217545
time_elpased: 2.348
batch start
#iterations: 111
currently lose_sum: 94.71041816473007
time_elpased: 2.528
batch start
#iterations: 112
currently lose_sum: 95.11078017950058
time_elpased: 2.344
batch start
#iterations: 113
currently lose_sum: 94.40736258029938
time_elpased: 2.321
batch start
#iterations: 114
currently lose_sum: 94.85518884658813
time_elpased: 2.429
batch start
#iterations: 115
currently lose_sum: 94.16495823860168
time_elpased: 2.458
batch start
#iterations: 116
currently lose_sum: 95.17738646268845
time_elpased: 2.214
batch start
#iterations: 117
currently lose_sum: 94.29083448648453
time_elpased: 2.401
batch start
#iterations: 118
currently lose_sum: 95.64233034849167
time_elpased: 2.475
batch start
#iterations: 119
currently lose_sum: 95.05087339878082
time_elpased: 2.445
start validation test
0.65618556701
0.619845766446
0.810724578016
0.702550838387
0.655930236137
60.667
batch start
#iterations: 120
currently lose_sum: 94.39470303058624
time_elpased: 2.385
batch start
#iterations: 121
currently lose_sum: 94.55729973316193
time_elpased: 2.34
batch start
#iterations: 122
currently lose_sum: 94.4772824048996
time_elpased: 2.476
batch start
#iterations: 123
currently lose_sum: 94.22363942861557
time_elpased: 2.552
batch start
#iterations: 124
currently lose_sum: 95.57109844684601
time_elpased: 1.884
batch start
#iterations: 125
currently lose_sum: 94.63173717260361
time_elpased: 2.429
batch start
#iterations: 126
currently lose_sum: 94.11817419528961
time_elpased: 2.416
batch start
#iterations: 127
currently lose_sum: 94.89767903089523
time_elpased: 2.297
batch start
#iterations: 128
currently lose_sum: 94.1801638007164
time_elpased: 1.963
batch start
#iterations: 129
currently lose_sum: 93.80152559280396
time_elpased: 2.477
batch start
#iterations: 130
currently lose_sum: 94.5325374007225
time_elpased: 2.441
batch start
#iterations: 131
currently lose_sum: 94.43328112363815
time_elpased: 2.196
batch start
#iterations: 132
currently lose_sum: 94.26971405744553
time_elpased: 2.409
batch start
#iterations: 133
currently lose_sum: 93.80669140815735
time_elpased: 2.375
batch start
#iterations: 134
currently lose_sum: 94.71881067752838
time_elpased: 2.455
batch start
#iterations: 135
currently lose_sum: 94.66252267360687
time_elpased: 2.209
batch start
#iterations: 136
currently lose_sum: 94.06035047769547
time_elpased: 2.522
batch start
#iterations: 137
currently lose_sum: 94.63972073793411
time_elpased: 2.419
batch start
#iterations: 138
currently lose_sum: 94.4539960026741
time_elpased: 2.3
batch start
#iterations: 139
currently lose_sum: 93.64200532436371
time_elpased: 2.386
start validation test
0.658608247423
0.620678891924
0.818649650062
0.70604944299
0.658343825444
59.879
batch start
#iterations: 140
currently lose_sum: 94.020672082901
time_elpased: 2.284
batch start
#iterations: 141
currently lose_sum: 93.61698627471924
time_elpased: 2.47
batch start
#iterations: 142
currently lose_sum: 94.54142826795578
time_elpased: 2.251
batch start
#iterations: 143
currently lose_sum: 94.22984510660172
time_elpased: 1.941
batch start
#iterations: 144
currently lose_sum: 93.67436438798904
time_elpased: 1.997
batch start
#iterations: 145
currently lose_sum: 93.81669789552689
time_elpased: 1.734
batch start
#iterations: 146
currently lose_sum: 94.82035881280899
time_elpased: 2.095
batch start
#iterations: 147
currently lose_sum: 93.82396924495697
time_elpased: 2.404
batch start
#iterations: 148
currently lose_sum: 94.25424987077713
time_elpased: 2.479
batch start
#iterations: 149
currently lose_sum: 94.93811070919037
time_elpased: 2.06
batch start
#iterations: 150
currently lose_sum: 94.18748211860657
time_elpased: 2.268
batch start
#iterations: 151
currently lose_sum: 94.31553381681442
time_elpased: 2.419
batch start
#iterations: 152
currently lose_sum: 93.50289475917816
time_elpased: 2.681
batch start
#iterations: 153
currently lose_sum: 93.81171572208405
time_elpased: 2.201
batch start
#iterations: 154
currently lose_sum: 94.22349065542221
time_elpased: 2.367
batch start
#iterations: 155
currently lose_sum: 93.57861185073853
time_elpased: 2.146
batch start
#iterations: 156
currently lose_sum: 94.11034709215164
time_elpased: 2.681
batch start
#iterations: 157
currently lose_sum: 93.68671452999115
time_elpased: 2.388
batch start
#iterations: 158
currently lose_sum: 93.835533618927
time_elpased: 2.396
batch start
#iterations: 159
currently lose_sum: 93.66396772861481
time_elpased: 2.487
start validation test
0.598453608247
0.579639431029
0.721387402223
0.642791636097
0.598250495824
63.126
batch start
#iterations: 160
currently lose_sum: 94.42362141609192
time_elpased: 2.691
batch start
#iterations: 161
currently lose_sum: 93.93798381090164
time_elpased: 2.582
batch start
#iterations: 162
currently lose_sum: 94.70470994710922
time_elpased: 2.45
batch start
#iterations: 163
currently lose_sum: 93.63102424144745
time_elpased: 2.6
batch start
#iterations: 164
currently lose_sum: 93.77021634578705
time_elpased: 2.413
batch start
#iterations: 165
currently lose_sum: 94.23207038640976
time_elpased: 2.196
batch start
#iterations: 166
currently lose_sum: 93.46569913625717
time_elpased: 2.452
batch start
#iterations: 167
currently lose_sum: 93.637013733387
time_elpased: 2.321
batch start
#iterations: 168
currently lose_sum: 94.17697113752365
time_elpased: 2.399
batch start
#iterations: 169
currently lose_sum: 93.63440752029419
time_elpased: 2.399
batch start
#iterations: 170
currently lose_sum: 94.03582990169525
time_elpased: 2.513
batch start
#iterations: 171
currently lose_sum: 93.2495334148407
time_elpased: 2.376
batch start
#iterations: 172
currently lose_sum: 93.13762456178665
time_elpased: 2.447
batch start
#iterations: 173
currently lose_sum: 93.37156164646149
time_elpased: 2.411
batch start
#iterations: 174
currently lose_sum: 93.43986409902573
time_elpased: 2.485
batch start
#iterations: 175
currently lose_sum: 93.4685869216919
time_elpased: 2.427
batch start
#iterations: 176
currently lose_sum: 93.396224796772
time_elpased: 2.389
batch start
#iterations: 177
currently lose_sum: 93.86770135164261
time_elpased: 2.379
batch start
#iterations: 178
currently lose_sum: 93.50182473659515
time_elpased: 2.499
batch start
#iterations: 179
currently lose_sum: 93.79138869047165
time_elpased: 2.113
start validation test
0.667216494845
0.641101108033
0.762247838617
0.696445363927
0.667059483125
59.332
batch start
#iterations: 180
currently lose_sum: 92.96158301830292
time_elpased: 2.038
batch start
#iterations: 181
currently lose_sum: 93.45060354471207
time_elpased: 2.391
batch start
#iterations: 182
currently lose_sum: 93.30931276082993
time_elpased: 2.474
batch start
#iterations: 183
currently lose_sum: 93.52932327985764
time_elpased: 2.538
batch start
#iterations: 184
currently lose_sum: 93.34745109081268
time_elpased: 2.427
batch start
#iterations: 185
currently lose_sum: 93.27449214458466
time_elpased: 2.387
batch start
#iterations: 186
currently lose_sum: 93.08574151992798
time_elpased: 2.095
batch start
#iterations: 187
currently lose_sum: 93.34656798839569
time_elpased: 2.336
batch start
#iterations: 188
currently lose_sum: 93.28465020656586
time_elpased: 2.415
batch start
#iterations: 189
currently lose_sum: 92.87646532058716
time_elpased: 2.41
batch start
#iterations: 190
currently lose_sum: 93.16888153553009
time_elpased: 2.432
batch start
#iterations: 191
currently lose_sum: 92.88773220777512
time_elpased: 2.249
batch start
#iterations: 192
currently lose_sum: 93.10248064994812
time_elpased: 2.373
batch start
#iterations: 193
currently lose_sum: 93.08130437135696
time_elpased: 2.371
batch start
#iterations: 194
currently lose_sum: 93.02004551887512
time_elpased: 2.442
batch start
#iterations: 195
currently lose_sum: 93.55469238758087
time_elpased: 2.425
batch start
#iterations: 196
currently lose_sum: 92.99752885103226
time_elpased: 2.079
batch start
#iterations: 197
currently lose_sum: 92.9302864074707
time_elpased: 2.434
batch start
#iterations: 198
currently lose_sum: 92.50174111127853
time_elpased: 2.44
batch start
#iterations: 199
currently lose_sum: 92.35051339864731
time_elpased: 2.514
start validation test
0.652628865979
0.636097650178
0.716035405517
0.673703578173
0.652524105071
59.527
batch start
#iterations: 200
currently lose_sum: 93.12086153030396
time_elpased: 2.235
batch start
#iterations: 201
currently lose_sum: 92.60941171646118
time_elpased: 1.868
batch start
#iterations: 202
currently lose_sum: 92.60881745815277
time_elpased: 2.313
batch start
#iterations: 203
currently lose_sum: 92.60935413837433
time_elpased: 2.013
batch start
#iterations: 204
currently lose_sum: 92.95279747247696
time_elpased: 1.648
batch start
#iterations: 205
currently lose_sum: 93.24357229471207
time_elpased: 1.612
batch start
#iterations: 206
currently lose_sum: 92.69451475143433
time_elpased: 1.922
batch start
#iterations: 207
currently lose_sum: 92.95948100090027
time_elpased: 1.927
batch start
#iterations: 208
currently lose_sum: 92.56938809156418
time_elpased: 2.138
batch start
#iterations: 209
currently lose_sum: 92.95493030548096
time_elpased: 2.417
batch start
#iterations: 210
currently lose_sum: 92.63835453987122
time_elpased: 2.527
batch start
#iterations: 211
currently lose_sum: 92.50701451301575
time_elpased: 2.239
batch start
#iterations: 212
currently lose_sum: 92.80820453166962
time_elpased: 2.311
batch start
#iterations: 213
currently lose_sum: 93.10711508989334
time_elpased: 2.665
batch start
#iterations: 214
currently lose_sum: 92.8319496512413
time_elpased: 2.384
batch start
#iterations: 215
currently lose_sum: 93.26512807607651
time_elpased: 2.53
batch start
#iterations: 216
currently lose_sum: 92.3031035065651
time_elpased: 2.257
batch start
#iterations: 217
currently lose_sum: 92.35071414709091
time_elpased: 2.106
batch start
#iterations: 218
currently lose_sum: 92.28102886676788
time_elpased: 2.657
batch start
#iterations: 219
currently lose_sum: 92.66457635164261
time_elpased: 2.577
start validation test
0.637525773196
0.619119474525
0.717888019761
0.664855590506
0.637392997902
60.580
batch start
#iterations: 220
currently lose_sum: 92.38422775268555
time_elpased: 2.562
batch start
#iterations: 221
currently lose_sum: 92.56001389026642
time_elpased: 2.449
batch start
#iterations: 222
currently lose_sum: 92.50470870733261
time_elpased: 2.474
batch start
#iterations: 223
currently lose_sum: 92.05443221330643
time_elpased: 2.308
batch start
#iterations: 224
currently lose_sum: 92.90717577934265
time_elpased: 2.459
batch start
#iterations: 225
currently lose_sum: 92.18776935338974
time_elpased: 2.387
batch start
#iterations: 226
currently lose_sum: 91.97378844022751
time_elpased: 2.178
batch start
#iterations: 227
currently lose_sum: 92.5116668343544
time_elpased: 2.131
batch start
#iterations: 228
currently lose_sum: 92.79325991868973
time_elpased: 2.289
batch start
#iterations: 229
currently lose_sum: 92.04689985513687
time_elpased: 2.259
batch start
#iterations: 230
currently lose_sum: 92.35591423511505
time_elpased: 1.926
batch start
#iterations: 231
currently lose_sum: 92.32737863063812
time_elpased: 1.799
batch start
#iterations: 232
currently lose_sum: 92.82756471633911
time_elpased: 2.381
batch start
#iterations: 233
currently lose_sum: 92.78976571559906
time_elpased: 2.483
batch start
#iterations: 234
currently lose_sum: 92.21075171232224
time_elpased: 2.516
batch start
#iterations: 235
currently lose_sum: 92.6835590004921
time_elpased: 2.221
batch start
#iterations: 236
currently lose_sum: 92.00307929515839
time_elpased: 2.366
batch start
#iterations: 237
currently lose_sum: 92.43432486057281
time_elpased: 1.844
batch start
#iterations: 238
currently lose_sum: 92.1953296661377
time_elpased: 2.332
batch start
#iterations: 239
currently lose_sum: 92.30201822519302
time_elpased: 2.418
start validation test
0.662577319588
0.633148521505
0.775730753396
0.697224791859
0.662390366372
59.049
batch start
#iterations: 240
currently lose_sum: 92.7324965596199
time_elpased: 2.47
batch start
#iterations: 241
currently lose_sum: 91.78848105669022
time_elpased: 2.341
batch start
#iterations: 242
currently lose_sum: 92.87791121006012
time_elpased: 2.4
batch start
#iterations: 243
currently lose_sum: 92.37366324663162
time_elpased: 2.301
batch start
#iterations: 244
currently lose_sum: 91.98507279157639
time_elpased: 2.396
batch start
#iterations: 245
currently lose_sum: 92.54488897323608
time_elpased: 2.264
batch start
#iterations: 246
currently lose_sum: 92.70736056566238
time_elpased: 2.496
batch start
#iterations: 247
currently lose_sum: 91.76465851068497
time_elpased: 2.353
batch start
#iterations: 248
currently lose_sum: 92.47658658027649
time_elpased: 2.398
batch start
#iterations: 249
currently lose_sum: 92.74337548017502
time_elpased: 2.287
batch start
#iterations: 250
currently lose_sum: 91.7388334274292
time_elpased: 2.389
batch start
#iterations: 251
currently lose_sum: 92.16766214370728
time_elpased: 2.442
batch start
#iterations: 252
currently lose_sum: 92.1352887749672
time_elpased: 2.522
batch start
#iterations: 253
currently lose_sum: 92.24466270208359
time_elpased: 2.462
batch start
#iterations: 254
currently lose_sum: 92.52046692371368
time_elpased: 2.414
batch start
#iterations: 255
currently lose_sum: 92.2708153128624
time_elpased: 2.414
batch start
#iterations: 256
currently lose_sum: 92.7600422501564
time_elpased: 2.433
batch start
#iterations: 257
currently lose_sum: 91.58579576015472
time_elpased: 2.332
batch start
#iterations: 258
currently lose_sum: 91.98161900043488
time_elpased: 2.028
batch start
#iterations: 259
currently lose_sum: 91.7712482213974
time_elpased: 2.384
start validation test
0.656030927835
0.62334819619
0.791375051462
0.697383338624
0.655807310943
59.246
batch start
#iterations: 260
currently lose_sum: 92.51777738332748
time_elpased: 2.475
batch start
#iterations: 261
currently lose_sum: 91.91780889034271
time_elpased: 2.046
batch start
#iterations: 262
currently lose_sum: 91.82985717058182
time_elpased: 2.32
batch start
#iterations: 263
currently lose_sum: 91.57421082258224
time_elpased: 2.422
batch start
#iterations: 264
currently lose_sum: 91.9811435341835
time_elpased: 2.31
batch start
#iterations: 265
currently lose_sum: 91.90892374515533
time_elpased: 2.25
batch start
#iterations: 266
currently lose_sum: 91.43861192464828
time_elpased: 1.962
batch start
#iterations: 267
currently lose_sum: 91.88845896720886
time_elpased: 2.137
batch start
#iterations: 268
currently lose_sum: 92.01683557033539
time_elpased: 2.169
batch start
#iterations: 269
currently lose_sum: 92.26671266555786
time_elpased: 2.356
batch start
#iterations: 270
currently lose_sum: 91.7326728105545
time_elpased: 2.403
batch start
#iterations: 271
currently lose_sum: 91.83559519052505
time_elpased: 2.307
batch start
#iterations: 272
currently lose_sum: 91.87481188774109
time_elpased: 2.574
batch start
#iterations: 273
currently lose_sum: 91.46421402692795
time_elpased: 2.695
batch start
#iterations: 274
currently lose_sum: 91.85100144147873
time_elpased: 2.506
batch start
#iterations: 275
currently lose_sum: 91.61036258935928
time_elpased: 2.448
batch start
#iterations: 276
currently lose_sum: 91.58358836174011
time_elpased: 2.483
batch start
#iterations: 277
currently lose_sum: 92.27795851230621
time_elpased: 2.464
batch start
#iterations: 278
currently lose_sum: 92.2011405825615
time_elpased: 2.584
batch start
#iterations: 279
currently lose_sum: 91.63119703531265
time_elpased: 2.372
start validation test
0.673659793814
0.672054488157
0.680424042816
0.676213368793
0.673648617856
58.086
batch start
#iterations: 280
currently lose_sum: 91.85054588317871
time_elpased: 2.059
batch start
#iterations: 281
currently lose_sum: 91.59631788730621
time_elpased: 2.624
batch start
#iterations: 282
currently lose_sum: 91.41727256774902
time_elpased: 2.541
batch start
#iterations: 283
currently lose_sum: 91.97832691669464
time_elpased: 2.187
batch start
#iterations: 284
currently lose_sum: 91.48817282915115
time_elpased: 2.472
batch start
#iterations: 285
currently lose_sum: 91.62102937698364
time_elpased: 2.522
batch start
#iterations: 286
currently lose_sum: 91.91268002986908
time_elpased: 2.367
batch start
#iterations: 287
currently lose_sum: 91.84181308746338
time_elpased: 2.465
batch start
#iterations: 288
currently lose_sum: 91.73453271389008
time_elpased: 2.132
batch start
#iterations: 289
currently lose_sum: 91.42416697740555
time_elpased: 2.178
batch start
#iterations: 290
currently lose_sum: 91.6772186756134
time_elpased: 2.054
batch start
#iterations: 291
currently lose_sum: 91.60369777679443
time_elpased: 1.903
batch start
#iterations: 292
currently lose_sum: 91.79389727115631
time_elpased: 2.487
batch start
#iterations: 293
currently lose_sum: 91.35727441310883
time_elpased: 2.416
batch start
#iterations: 294
currently lose_sum: 91.86257630586624
time_elpased: 1.933
batch start
#iterations: 295
currently lose_sum: 91.63298416137695
time_elpased: 2.412
batch start
#iterations: 296
currently lose_sum: 91.36817413568497
time_elpased: 2.484
batch start
#iterations: 297
currently lose_sum: 92.14054030179977
time_elpased: 2.244
batch start
#iterations: 298
currently lose_sum: 91.08578276634216
time_elpased: 2.284
batch start
#iterations: 299
currently lose_sum: 91.33056825399399
time_elpased: 2.495
start validation test
0.632216494845
0.610289718827
0.734973240016
0.666853434188
0.632046719141
60.647
batch start
#iterations: 300
currently lose_sum: 91.80384939908981
time_elpased: 2.49
batch start
#iterations: 301
currently lose_sum: 93.02662575244904
time_elpased: 2.444
batch start
#iterations: 302
currently lose_sum: 90.6964961886406
time_elpased: 2.432
batch start
#iterations: 303
currently lose_sum: 91.24983209371567
time_elpased: 2.484
batch start
#iterations: 304
currently lose_sum: 91.28274005651474
time_elpased: 2.476
batch start
#iterations: 305
currently lose_sum: 91.17963796854019
time_elpased: 2.406
batch start
#iterations: 306
currently lose_sum: 91.47387856245041
time_elpased: 2.394
batch start
#iterations: 307
currently lose_sum: 91.61295014619827
time_elpased: 2.415
batch start
#iterations: 308
currently lose_sum: 91.02408027648926
time_elpased: 2.527
batch start
#iterations: 309
currently lose_sum: 90.93034392595291
time_elpased: 2.494
batch start
#iterations: 310
currently lose_sum: 91.27987748384476
time_elpased: 2.216
batch start
#iterations: 311
currently lose_sum: 91.38873940706253
time_elpased: 2.427
batch start
#iterations: 312
currently lose_sum: 91.57007879018784
time_elpased: 2.228
batch start
#iterations: 313
currently lose_sum: 91.35827046632767
time_elpased: 2.024
batch start
#iterations: 314
currently lose_sum: 91.4135582447052
time_elpased: 2.471
batch start
#iterations: 315
currently lose_sum: 91.19866746664047
time_elpased: 2.405
batch start
#iterations: 316
currently lose_sum: 91.5096806883812
time_elpased: 2.434
batch start
#iterations: 317
currently lose_sum: 90.71623456478119
time_elpased: 2.498
batch start
#iterations: 318
currently lose_sum: 91.44712430238724
time_elpased: 2.596
batch start
#iterations: 319
currently lose_sum: 91.50877040624619
time_elpased: 2.494
start validation test
0.676443298969
0.663063063063
0.719637710992
0.69019298159
0.676371932737
57.887
batch start
#iterations: 320
currently lose_sum: 92.12784820795059
time_elpased: 2.436
batch start
#iterations: 321
currently lose_sum: 91.53772085905075
time_elpased: 2.491
batch start
#iterations: 322
currently lose_sum: 91.03690081834793
time_elpased: 2.379
batch start
#iterations: 323
currently lose_sum: 91.1775603890419
time_elpased: 2.424
batch start
#iterations: 324
currently lose_sum: 91.72205829620361
time_elpased: 2.49
batch start
#iterations: 325
currently lose_sum: 91.29559355974197
time_elpased: 2.273
batch start
#iterations: 326
currently lose_sum: 90.8231348991394
time_elpased: 1.881
batch start
#iterations: 327
currently lose_sum: 91.0178993344307
time_elpased: 2.241
batch start
#iterations: 328
currently lose_sum: 91.27586597204208
time_elpased: 2.333
batch start
#iterations: 329
currently lose_sum: 90.93754458427429
time_elpased: 2.552
batch start
#iterations: 330
currently lose_sum: 91.00559079647064
time_elpased: 2.517
batch start
#iterations: 331
currently lose_sum: 90.95055544376373
time_elpased: 2.257
batch start
#iterations: 332
currently lose_sum: 91.8420370221138
time_elpased: 2.53
batch start
#iterations: 333
currently lose_sum: 91.27504223585129
time_elpased: 2.442
batch start
#iterations: 334
currently lose_sum: 91.02569305896759
time_elpased: 2.609
batch start
#iterations: 335
currently lose_sum: 90.81961417198181
time_elpased: 2.494
batch start
#iterations: 336
currently lose_sum: 90.90425807237625
time_elpased: 2.439
batch start
#iterations: 337
currently lose_sum: 91.41480422019958
time_elpased: 2.409
batch start
#iterations: 338
currently lose_sum: 91.13514095544815
time_elpased: 2.225
batch start
#iterations: 339
currently lose_sum: 91.0745278596878
time_elpased: 2.506
start validation test
0.637164948454
0.61496177961
0.736928777275
0.670443372817
0.637000117675
60.044
batch start
#iterations: 340
currently lose_sum: 91.54668682813644
time_elpased: 2.435
batch start
#iterations: 341
currently lose_sum: 90.58170562982559
time_elpased: 2.144
batch start
#iterations: 342
currently lose_sum: 91.21443819999695
time_elpased: 2.337
batch start
#iterations: 343
currently lose_sum: 91.50724399089813
time_elpased: 2.405
batch start
#iterations: 344
currently lose_sum: 91.2786563038826
time_elpased: 2.405
batch start
#iterations: 345
currently lose_sum: 91.0460855960846
time_elpased: 2.548
batch start
#iterations: 346
currently lose_sum: 90.84467977285385
time_elpased: 2.444
batch start
#iterations: 347
currently lose_sum: 90.70400989055634
time_elpased: 2.449
batch start
#iterations: 348
currently lose_sum: 90.93320709466934
time_elpased: 2.431
batch start
#iterations: 349
currently lose_sum: 91.09051024913788
time_elpased: 2.36
batch start
#iterations: 350
currently lose_sum: 91.05951577425003
time_elpased: 2.554
batch start
#iterations: 351
currently lose_sum: 91.32718223333359
time_elpased: 2.477
batch start
#iterations: 352
currently lose_sum: 91.00520521402359
time_elpased: 2.42
batch start
#iterations: 353
currently lose_sum: 91.39423459768295
time_elpased: 2.558
batch start
#iterations: 354
currently lose_sum: 90.72216993570328
time_elpased: 2.541
batch start
#iterations: 355
currently lose_sum: 91.10043144226074
time_elpased: 2.414
batch start
#iterations: 356
currently lose_sum: 90.13576465845108
time_elpased: 2.404
batch start
#iterations: 357
currently lose_sum: 91.62166970968246
time_elpased: 2.388
batch start
#iterations: 358
currently lose_sum: 90.4739481806755
time_elpased: 2.423
batch start
#iterations: 359
currently lose_sum: 90.31856334209442
time_elpased: 2.449
start validation test
0.681855670103
0.677840224809
0.695142033759
0.686382113821
0.681833718242
57.789
batch start
#iterations: 360
currently lose_sum: 90.83138906955719
time_elpased: 2.214
batch start
#iterations: 361
currently lose_sum: 90.35609567165375
time_elpased: 2.226
batch start
#iterations: 362
currently lose_sum: 90.56907850503922
time_elpased: 2.354
batch start
#iterations: 363
currently lose_sum: 91.24150848388672
time_elpased: 2.451
batch start
#iterations: 364
currently lose_sum: 90.5686776638031
time_elpased: 2.477
batch start
#iterations: 365
currently lose_sum: 91.07698899507523
time_elpased: 2.392
batch start
#iterations: 366
currently lose_sum: 91.50147569179535
time_elpased: 2.392
batch start
#iterations: 367
currently lose_sum: 90.70821106433868
time_elpased: 2.528
batch start
#iterations: 368
currently lose_sum: 91.15487778186798
time_elpased: 2.377
batch start
#iterations: 369
currently lose_sum: 91.21070343255997
time_elpased: 2.535
batch start
#iterations: 370
currently lose_sum: 90.79919695854187
time_elpased: 2.39
batch start
#iterations: 371
currently lose_sum: 90.45160150527954
time_elpased: 2.251
batch start
#iterations: 372
currently lose_sum: 90.79958862066269
time_elpased: 2.542
batch start
#iterations: 373
currently lose_sum: 90.60548835992813
time_elpased: 2.408
batch start
#iterations: 374
currently lose_sum: 91.02972650527954
time_elpased: 2.482
batch start
#iterations: 375
currently lose_sum: 90.91633760929108
time_elpased: 2.428
batch start
#iterations: 376
currently lose_sum: 90.95906776189804
time_elpased: 2.034
batch start
#iterations: 377
currently lose_sum: 90.40284711122513
time_elpased: 2.016
batch start
#iterations: 378
currently lose_sum: 90.3954057097435
time_elpased: 2.442
batch start
#iterations: 379
currently lose_sum: 90.8792052268982
time_elpased: 2.04
start validation test
0.604278350515
0.602596357049
0.616303005352
0.609372614868
0.604258483263
61.655
batch start
#iterations: 380
currently lose_sum: 90.92725610733032
time_elpased: 2.379
batch start
#iterations: 381
currently lose_sum: 90.54493135213852
time_elpased: 2.441
batch start
#iterations: 382
currently lose_sum: 90.62593245506287
time_elpased: 2.426
batch start
#iterations: 383
currently lose_sum: 90.58360826969147
time_elpased: 2.34
batch start
#iterations: 384
currently lose_sum: 90.70597410202026
time_elpased: 2.427
batch start
#iterations: 385
currently lose_sum: 90.32397973537445
time_elpased: 1.936
batch start
#iterations: 386
currently lose_sum: 90.51612079143524
time_elpased: 2.279
batch start
#iterations: 387
currently lose_sum: 90.09536987543106
time_elpased: 1.975
batch start
#iterations: 388
currently lose_sum: 90.15311938524246
time_elpased: 2.494
batch start
#iterations: 389
currently lose_sum: 90.00351768732071
time_elpased: 2.474
batch start
#iterations: 390
currently lose_sum: 90.39942163228989
time_elpased: 2.327
batch start
#iterations: 391
currently lose_sum: 90.60041958093643
time_elpased: 2.629
batch start
#iterations: 392
currently lose_sum: 91.12942135334015
time_elpased: 2.491
batch start
#iterations: 393
currently lose_sum: 90.46009719371796
time_elpased: 2.274
batch start
#iterations: 394
currently lose_sum: 90.9408068060875
time_elpased: 2.569
batch start
#iterations: 395
currently lose_sum: 90.57075035572052
time_elpased: 2.602
batch start
#iterations: 396
currently lose_sum: 90.75888067483902
time_elpased: 1.945
batch start
#iterations: 397
currently lose_sum: 90.96156913042068
time_elpased: 2.421
batch start
#iterations: 398
currently lose_sum: 90.32077103853226
time_elpased: 2.285
batch start
#iterations: 399
currently lose_sum: 90.95351165533066
time_elpased: 1.952
start validation test
0.681237113402
0.677630255482
0.693392342528
0.685420693865
0.681217030413
57.706
acc: 0.689
pre: 0.686
rec: 0.697
F1: 0.692
auc: 0.754
