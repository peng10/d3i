start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.27804106473923
time_elpased: 2.21
batch start
#iterations: 1
currently lose_sum: 99.97646647691727
time_elpased: 2.5
batch start
#iterations: 2
currently lose_sum: 99.63925993442535
time_elpased: 2.496
batch start
#iterations: 3
currently lose_sum: 99.4828822016716
time_elpased: 2.515
batch start
#iterations: 4
currently lose_sum: 99.47824800014496
time_elpased: 2.722
batch start
#iterations: 5
currently lose_sum: 99.34812462329865
time_elpased: 2.67
batch start
#iterations: 6
currently lose_sum: 99.27645349502563
time_elpased: 2.447
batch start
#iterations: 7
currently lose_sum: 99.15005522966385
time_elpased: 2.545
batch start
#iterations: 8
currently lose_sum: 99.35313701629639
time_elpased: 2.464
batch start
#iterations: 9
currently lose_sum: 99.05209308862686
time_elpased: 2.75
batch start
#iterations: 10
currently lose_sum: 99.15628284215927
time_elpased: 2.447
batch start
#iterations: 11
currently lose_sum: 98.95019495487213
time_elpased: 2.537
batch start
#iterations: 12
currently lose_sum: 98.80520761013031
time_elpased: 2.538
batch start
#iterations: 13
currently lose_sum: 99.27100265026093
time_elpased: 2.598
batch start
#iterations: 14
currently lose_sum: 98.66208136081696
time_elpased: 2.48
batch start
#iterations: 15
currently lose_sum: 99.05714869499207
time_elpased: 2.545
batch start
#iterations: 16
currently lose_sum: 99.67467856407166
time_elpased: 2.588
batch start
#iterations: 17
currently lose_sum: 98.84395265579224
time_elpased: 2.484
batch start
#iterations: 18
currently lose_sum: 98.63916540145874
time_elpased: 2.563
batch start
#iterations: 19
currently lose_sum: 98.8786091208458
time_elpased: 2.321
start validation test
0.623711340206
0.64983874969
0.539213668176
0.589380132748
0.623850948091
63.739
batch start
#iterations: 20
currently lose_sum: 99.02870970964432
time_elpased: 2.662
batch start
#iterations: 21
currently lose_sum: 98.56672191619873
time_elpased: 2.612
batch start
#iterations: 22
currently lose_sum: 98.9004373550415
time_elpased: 2.501
batch start
#iterations: 23
currently lose_sum: 98.49511551856995
time_elpased: 2.393
batch start
#iterations: 24
currently lose_sum: 98.58661168813705
time_elpased: 1.811
batch start
#iterations: 25
currently lose_sum: 98.17088741064072
time_elpased: 2.381
batch start
#iterations: 26
currently lose_sum: 98.16519618034363
time_elpased: 2.558
batch start
#iterations: 27
currently lose_sum: 98.08798456192017
time_elpased: 2.571
batch start
#iterations: 28
currently lose_sum: 98.4784665107727
time_elpased: 2.316
batch start
#iterations: 29
currently lose_sum: 98.0665272474289
time_elpased: 2.362
batch start
#iterations: 30
currently lose_sum: 98.98290544748306
time_elpased: 2.539
batch start
#iterations: 31
currently lose_sum: 98.3620577454567
time_elpased: 2.415
batch start
#iterations: 32
currently lose_sum: 98.37514805793762
time_elpased: 2.344
batch start
#iterations: 33
currently lose_sum: 98.08720088005066
time_elpased: 2.018
batch start
#iterations: 34
currently lose_sum: 97.95530688762665
time_elpased: 2.359
batch start
#iterations: 35
currently lose_sum: 97.63934910297394
time_elpased: 2.54
batch start
#iterations: 36
currently lose_sum: 98.24298006296158
time_elpased: 2.119
batch start
#iterations: 37
currently lose_sum: 97.40700715780258
time_elpased: 2.007
batch start
#iterations: 38
currently lose_sum: 97.61015623807907
time_elpased: 2.161
batch start
#iterations: 39
currently lose_sum: 97.56456935405731
time_elpased: 2.34
start validation test
0.649226804124
0.63784449285
0.693186496501
0.664364981504
0.649154173488
62.634
batch start
#iterations: 40
currently lose_sum: 97.80651921033859
time_elpased: 2.867
batch start
#iterations: 41
currently lose_sum: 97.30320262908936
time_elpased: 2.617
batch start
#iterations: 42
currently lose_sum: 97.05331939458847
time_elpased: 2.362
batch start
#iterations: 43
currently lose_sum: 97.68374276161194
time_elpased: 2.7
batch start
#iterations: 44
currently lose_sum: 96.90886372327805
time_elpased: 2.607
batch start
#iterations: 45
currently lose_sum: 97.57302671670914
time_elpased: 2.606
batch start
#iterations: 46
currently lose_sum: 96.99595314264297
time_elpased: 2.683
batch start
#iterations: 47
currently lose_sum: 96.9655093550682
time_elpased: 2.529
batch start
#iterations: 48
currently lose_sum: 96.93937522172928
time_elpased: 2.658
batch start
#iterations: 49
currently lose_sum: 96.76323574781418
time_elpased: 2.653
batch start
#iterations: 50
currently lose_sum: 96.96565997600555
time_elpased: 2.439
batch start
#iterations: 51
currently lose_sum: 96.34068632125854
time_elpased: 2.515
batch start
#iterations: 52
currently lose_sum: 97.36969751119614
time_elpased: 2.43
batch start
#iterations: 53
currently lose_sum: 96.30684030056
time_elpased: 2.664
batch start
#iterations: 54
currently lose_sum: 97.23911374807358
time_elpased: 2.501
batch start
#iterations: 55
currently lose_sum: 96.58204501867294
time_elpased: 2.571
batch start
#iterations: 56
currently lose_sum: 97.0249691605568
time_elpased: 2.667
batch start
#iterations: 57
currently lose_sum: 95.8008685708046
time_elpased: 2.481
batch start
#iterations: 58
currently lose_sum: 97.82647615671158
time_elpased: 2.517
batch start
#iterations: 59
currently lose_sum: 96.59690243005753
time_elpased: 2.454
start validation test
0.648505154639
0.606421276908
0.849526554138
0.70767779826
0.648173025107
62.102
batch start
#iterations: 60
currently lose_sum: 96.58793783187866
time_elpased: 2.507
batch start
#iterations: 61
currently lose_sum: 96.19555252790451
time_elpased: 2.554
batch start
#iterations: 62
currently lose_sum: 96.47587585449219
time_elpased: 2.625
batch start
#iterations: 63
currently lose_sum: 96.46238911151886
time_elpased: 2.493
batch start
#iterations: 64
currently lose_sum: 96.58947086334229
time_elpased: 2.304
batch start
#iterations: 65
currently lose_sum: 96.54736113548279
time_elpased: 2.614
batch start
#iterations: 66
currently lose_sum: 96.42787832021713
time_elpased: 2.556
batch start
#iterations: 67
currently lose_sum: 96.11438858509064
time_elpased: 2.491
batch start
#iterations: 68
currently lose_sum: 96.43429797887802
time_elpased: 2.48
batch start
#iterations: 69
currently lose_sum: 95.60507065057755
time_elpased: 2.617
batch start
#iterations: 70
currently lose_sum: 96.2096539735794
time_elpased: 2.428
batch start
#iterations: 71
currently lose_sum: 99.00632673501968
time_elpased: 2.526
batch start
#iterations: 72
currently lose_sum: 97.36096447706223
time_elpased: 2.559
batch start
#iterations: 73
currently lose_sum: 96.4674825668335
time_elpased: 2.497
batch start
#iterations: 74
currently lose_sum: 95.77148669958115
time_elpased: 2.501
batch start
#iterations: 75
currently lose_sum: 95.93171459436417
time_elpased: 2.494
batch start
#iterations: 76
currently lose_sum: 95.73173099756241
time_elpased: 2.511
batch start
#iterations: 77
currently lose_sum: 96.2148215174675
time_elpased: 2.65
batch start
#iterations: 78
currently lose_sum: 95.98026192188263
time_elpased: 2.464
batch start
#iterations: 79
currently lose_sum: 95.94221317768097
time_elpased: 2.672
start validation test
0.643092783505
0.597608726052
0.879682997118
0.71171621284
0.642701886828
61.934
batch start
#iterations: 80
currently lose_sum: 95.89298093318939
time_elpased: 2.53
batch start
#iterations: 81
currently lose_sum: 95.48098474740982
time_elpased: 2.558
batch start
#iterations: 82
currently lose_sum: 95.59105563163757
time_elpased: 2.516
batch start
#iterations: 83
currently lose_sum: 95.49273759126663
time_elpased: 2.568
batch start
#iterations: 84
currently lose_sum: 95.17866295576096
time_elpased: 2.592
batch start
#iterations: 85
currently lose_sum: 95.55986034870148
time_elpased: 2.361
batch start
#iterations: 86
currently lose_sum: 95.48681575059891
time_elpased: 2.353
batch start
#iterations: 87
currently lose_sum: 95.15086764097214
time_elpased: 2.519
batch start
#iterations: 88
currently lose_sum: 95.10613489151001
time_elpased: 2.566
batch start
#iterations: 89
currently lose_sum: 95.47567701339722
time_elpased: 2.588
batch start
#iterations: 90
currently lose_sum: 95.83111149072647
time_elpased: 2.537
batch start
#iterations: 91
currently lose_sum: 95.28342795372009
time_elpased: 2.495
batch start
#iterations: 92
currently lose_sum: 95.7079935669899
time_elpased: 2.15
batch start
#iterations: 93
currently lose_sum: 95.09542739391327
time_elpased: 1.882
batch start
#iterations: 94
currently lose_sum: 94.9443131685257
time_elpased: 2.264
batch start
#iterations: 95
currently lose_sum: 95.28898346424103
time_elpased: 2.538
batch start
#iterations: 96
currently lose_sum: 94.8483099937439
time_elpased: 2.628
batch start
#iterations: 97
currently lose_sum: 94.88057613372803
time_elpased: 2.689
batch start
#iterations: 98
currently lose_sum: 94.9349936246872
time_elpased: 2.371
batch start
#iterations: 99
currently lose_sum: 95.13849115371704
time_elpased: 2.357
start validation test
0.659639175258
0.629245204683
0.779950596953
0.696539363022
0.659440395544
60.081
batch start
#iterations: 100
currently lose_sum: 96.2614077925682
time_elpased: 2.739
batch start
#iterations: 101
currently lose_sum: 94.81780242919922
time_elpased: 2.542
batch start
#iterations: 102
currently lose_sum: 95.23413491249084
time_elpased: 2.665
batch start
#iterations: 103
currently lose_sum: 95.50262409448624
time_elpased: 2.5
batch start
#iterations: 104
currently lose_sum: 94.85248869657516
time_elpased: 2.542
batch start
#iterations: 105
currently lose_sum: 95.23857939243317
time_elpased: 2.632
batch start
#iterations: 106
currently lose_sum: 94.78622931241989
time_elpased: 2.582
batch start
#iterations: 107
currently lose_sum: 94.93798798322678
time_elpased: 2.46
batch start
#iterations: 108
currently lose_sum: 94.59235680103302
time_elpased: 2.434
batch start
#iterations: 109
currently lose_sum: 95.1266719698906
time_elpased: 2.375
batch start
#iterations: 110
currently lose_sum: 94.88631814718246
time_elpased: 2.633
batch start
#iterations: 111
currently lose_sum: 94.77878224849701
time_elpased: 2.624
batch start
#iterations: 112
currently lose_sum: 94.8754711151123
time_elpased: 2.093
batch start
#iterations: 113
currently lose_sum: 94.59590166807175
time_elpased: 2.494
batch start
#iterations: 114
currently lose_sum: 95.28643590211868
time_elpased: 2.527
batch start
#iterations: 115
currently lose_sum: 94.36879396438599
time_elpased: 2.512
batch start
#iterations: 116
currently lose_sum: 95.19178110361099
time_elpased: 2.548
batch start
#iterations: 117
currently lose_sum: 94.64452290534973
time_elpased: 2.596
batch start
#iterations: 118
currently lose_sum: 95.32981896400452
time_elpased: 2.741
batch start
#iterations: 119
currently lose_sum: 95.39545077085495
time_elpased: 2.629
start validation test
0.664020618557
0.649077009137
0.716550020585
0.68114665884
0.663933828962
60.257
batch start
#iterations: 120
currently lose_sum: 94.79071485996246
time_elpased: 2.422
batch start
#iterations: 121
currently lose_sum: 94.82334113121033
time_elpased: 2.636
batch start
#iterations: 122
currently lose_sum: 94.89600044488907
time_elpased: 2.625
batch start
#iterations: 123
currently lose_sum: 94.44213271141052
time_elpased: 2.526
batch start
#iterations: 124
currently lose_sum: 95.02331459522247
time_elpased: 2.231
batch start
#iterations: 125
currently lose_sum: 94.97030597925186
time_elpased: 2.364
batch start
#iterations: 126
currently lose_sum: 94.2358986735344
time_elpased: 2.416
batch start
#iterations: 127
currently lose_sum: 94.62564635276794
time_elpased: 2.436
batch start
#iterations: 128
currently lose_sum: 94.38381499052048
time_elpased: 2.393
batch start
#iterations: 129
currently lose_sum: 93.9278599023819
time_elpased: 2.45
batch start
#iterations: 130
currently lose_sum: 94.59552335739136
time_elpased: 2.476
batch start
#iterations: 131
currently lose_sum: 94.87369817495346
time_elpased: 2.462
batch start
#iterations: 132
currently lose_sum: 94.03302150964737
time_elpased: 2.582
batch start
#iterations: 133
currently lose_sum: 93.92928391695023
time_elpased: 2.555
batch start
#iterations: 134
currently lose_sum: 94.44746547937393
time_elpased: 2.164
batch start
#iterations: 135
currently lose_sum: 95.19595974683762
time_elpased: 2.662
batch start
#iterations: 136
currently lose_sum: 94.21040445566177
time_elpased: 2.427
batch start
#iterations: 137
currently lose_sum: 95.02061182260513
time_elpased: 2.579
batch start
#iterations: 138
currently lose_sum: 94.69489979743958
time_elpased: 2.566
batch start
#iterations: 139
currently lose_sum: 93.7565136551857
time_elpased: 2.605
start validation test
0.660773195876
0.622184114116
0.821531494442
0.708094921269
0.660507589435
59.832
batch start
#iterations: 140
currently lose_sum: 94.27990686893463
time_elpased: 2.61
batch start
#iterations: 141
currently lose_sum: 94.0303829908371
time_elpased: 2.68
batch start
#iterations: 142
currently lose_sum: 94.84156012535095
time_elpased: 2.285
batch start
#iterations: 143
currently lose_sum: 94.69748383760452
time_elpased: 2.357
batch start
#iterations: 144
currently lose_sum: 93.8373162150383
time_elpased: 2.543
batch start
#iterations: 145
currently lose_sum: 94.23536914587021
time_elpased: 2.514
batch start
#iterations: 146
currently lose_sum: 94.93112379312515
time_elpased: 2.578
batch start
#iterations: 147
currently lose_sum: 94.08188581466675
time_elpased: 2.497
batch start
#iterations: 148
currently lose_sum: 94.8064831495285
time_elpased: 2.502
batch start
#iterations: 149
currently lose_sum: 94.9864330291748
time_elpased: 2.185
batch start
#iterations: 150
currently lose_sum: 94.28522217273712
time_elpased: 2.307
batch start
#iterations: 151
currently lose_sum: 94.58521020412445
time_elpased: 2.468
batch start
#iterations: 152
currently lose_sum: 93.88854819536209
time_elpased: 2.953
batch start
#iterations: 153
currently lose_sum: 94.30440306663513
time_elpased: 2.786
batch start
#iterations: 154
currently lose_sum: 93.99538832902908
time_elpased: 2.422
batch start
#iterations: 155
currently lose_sum: 93.67404228448868
time_elpased: 2.716
batch start
#iterations: 156
currently lose_sum: 94.46074378490448
time_elpased: 2.463
batch start
#iterations: 157
currently lose_sum: 93.7984247803688
time_elpased: 2.433
batch start
#iterations: 158
currently lose_sum: 94.07019877433777
time_elpased: 2.604
batch start
#iterations: 159
currently lose_sum: 94.02183073759079
time_elpased: 2.541
start validation test
0.647268041237
0.620907331033
0.759263071223
0.683150437561
0.647083001948
60.663
batch start
#iterations: 160
currently lose_sum: 94.33250558376312
time_elpased: 2.753
batch start
#iterations: 161
currently lose_sum: 94.02567100524902
time_elpased: 2.642
batch start
#iterations: 162
currently lose_sum: 94.97292280197144
time_elpased: 2.507
batch start
#iterations: 163
currently lose_sum: 94.11539041996002
time_elpased: 2.753
batch start
#iterations: 164
currently lose_sum: 94.06370741128922
time_elpased: 2.655
batch start
#iterations: 165
currently lose_sum: 94.51914978027344
time_elpased: 2.448
batch start
#iterations: 166
currently lose_sum: 93.56250709295273
time_elpased: 2.652
batch start
#iterations: 167
currently lose_sum: 93.88048875331879
time_elpased: 2.453
batch start
#iterations: 168
currently lose_sum: 94.2875691652298
time_elpased: 2.154
batch start
#iterations: 169
currently lose_sum: 93.77953237295151
time_elpased: 2.507
batch start
#iterations: 170
currently lose_sum: 94.1082369685173
time_elpased: 2.503
batch start
#iterations: 171
currently lose_sum: 93.36755323410034
time_elpased: 2.558
batch start
#iterations: 172
currently lose_sum: 93.49463367462158
time_elpased: 2.479
batch start
#iterations: 173
currently lose_sum: 93.54222822189331
time_elpased: 2.571
batch start
#iterations: 174
currently lose_sum: 93.57798701524734
time_elpased: 2.474
batch start
#iterations: 175
currently lose_sum: 93.69210523366928
time_elpased: 2.685
batch start
#iterations: 176
currently lose_sum: 93.81437534093857
time_elpased: 2.494
batch start
#iterations: 177
currently lose_sum: 94.02875649929047
time_elpased: 2.544
batch start
#iterations: 178
currently lose_sum: 93.53758108615875
time_elpased: 2.524
batch start
#iterations: 179
currently lose_sum: 93.53218132257462
time_elpased: 2.572
start validation test
0.66675257732
0.646375506529
0.738781391519
0.68949618174
0.666633570605
59.016
batch start
#iterations: 180
currently lose_sum: 93.06547969579697
time_elpased: 2.566
batch start
#iterations: 181
currently lose_sum: 93.79667729139328
time_elpased: 2.708
batch start
#iterations: 182
currently lose_sum: 93.66678202152252
time_elpased: 2.808
batch start
#iterations: 183
currently lose_sum: 93.59001559019089
time_elpased: 2.637
batch start
#iterations: 184
currently lose_sum: 93.83000987768173
time_elpased: 2.527
batch start
#iterations: 185
currently lose_sum: 93.50093215703964
time_elpased: 2.551
batch start
#iterations: 186
currently lose_sum: 93.2671023607254
time_elpased: 2.513
batch start
#iterations: 187
currently lose_sum: 93.31608176231384
time_elpased: 2.678
batch start
#iterations: 188
currently lose_sum: 93.38126140832901
time_elpased: 2.33
batch start
#iterations: 189
currently lose_sum: 92.89282077550888
time_elpased: 2.58
batch start
#iterations: 190
currently lose_sum: 93.45493292808533
time_elpased: 2.078
batch start
#iterations: 191
currently lose_sum: 93.49975097179413
time_elpased: 2.395
batch start
#iterations: 192
currently lose_sum: 92.93010860681534
time_elpased: 2.486
batch start
#iterations: 193
currently lose_sum: 93.1982649564743
time_elpased: 2.538
batch start
#iterations: 194
currently lose_sum: 92.9674146771431
time_elpased: 2.511
batch start
#iterations: 195
currently lose_sum: 93.7860631942749
time_elpased: 2.508
batch start
#iterations: 196
currently lose_sum: 92.57649558782578
time_elpased: 2.603
batch start
#iterations: 197
currently lose_sum: 93.08132642507553
time_elpased: 2.371
batch start
#iterations: 198
currently lose_sum: 92.92731589078903
time_elpased: 2.429
batch start
#iterations: 199
currently lose_sum: 92.75074452161789
time_elpased: 2.282
start validation test
0.659587628866
0.652698724239
0.684540963359
0.668240731438
0.659546400721
58.935
batch start
#iterations: 200
currently lose_sum: 93.32941669225693
time_elpased: 2.631
batch start
#iterations: 201
currently lose_sum: 92.62739396095276
time_elpased: 2.499
batch start
#iterations: 202
currently lose_sum: 92.71453070640564
time_elpased: 2.604
batch start
#iterations: 203
currently lose_sum: 92.95787924528122
time_elpased: 2.545
batch start
#iterations: 204
currently lose_sum: 93.05416667461395
time_elpased: 2.395
batch start
#iterations: 205
currently lose_sum: 93.69716680049896
time_elpased: 1.935
batch start
#iterations: 206
currently lose_sum: 92.93008786439896
time_elpased: 2.271
batch start
#iterations: 207
currently lose_sum: 93.67265278100967
time_elpased: 2.353
batch start
#iterations: 208
currently lose_sum: 92.96868526935577
time_elpased: 2.898
batch start
#iterations: 209
currently lose_sum: 93.43334412574768
time_elpased: 2.767
batch start
#iterations: 210
currently lose_sum: 92.74856299161911
time_elpased: 2.322
batch start
#iterations: 211
currently lose_sum: 92.53653508424759
time_elpased: 2.51
batch start
#iterations: 212
currently lose_sum: 92.81077617406845
time_elpased: 2.736
batch start
#iterations: 213
currently lose_sum: 93.22623717784882
time_elpased: 2.299
batch start
#iterations: 214
currently lose_sum: 93.00913268327713
time_elpased: 2.511
batch start
#iterations: 215
currently lose_sum: 93.38977217674255
time_elpased: 2.543
batch start
#iterations: 216
currently lose_sum: 92.38758790493011
time_elpased: 2.325
batch start
#iterations: 217
currently lose_sum: 92.44066905975342
time_elpased: 2.739
batch start
#iterations: 218
currently lose_sum: 92.32840061187744
time_elpased: 2.575
batch start
#iterations: 219
currently lose_sum: 92.87050729990005
time_elpased: 2.175
start validation test
0.66118556701
0.629522789088
0.786125977769
0.699162433063
0.660979139235
59.714
batch start
#iterations: 220
currently lose_sum: 92.50747013092041
time_elpased: 2.535
batch start
#iterations: 221
currently lose_sum: 93.36932706832886
time_elpased: 2.623
batch start
#iterations: 222
currently lose_sum: 92.32751393318176
time_elpased: 2.482
batch start
#iterations: 223
currently lose_sum: 92.2514038681984
time_elpased: 2.346
batch start
#iterations: 224
currently lose_sum: 92.9139631986618
time_elpased: 2.615
batch start
#iterations: 225
currently lose_sum: 92.50651985406876
time_elpased: 2.19
batch start
#iterations: 226
currently lose_sum: 91.78571504354477
time_elpased: 2.415
batch start
#iterations: 227
currently lose_sum: 92.85198324918747
time_elpased: 2.459
batch start
#iterations: 228
currently lose_sum: 92.40573650598526
time_elpased: 2.557
batch start
#iterations: 229
currently lose_sum: 91.998694896698
time_elpased: 2.62
batch start
#iterations: 230
currently lose_sum: 92.56310671567917
time_elpased: 2.587
batch start
#iterations: 231
currently lose_sum: 92.66540849208832
time_elpased: 2.6
batch start
#iterations: 232
currently lose_sum: 92.78150475025177
time_elpased: 2.245
batch start
#iterations: 233
currently lose_sum: 93.0225800871849
time_elpased: 2.563
batch start
#iterations: 234
currently lose_sum: 92.33120858669281
time_elpased: 2.499
batch start
#iterations: 235
currently lose_sum: 92.64903700351715
time_elpased: 2.528
batch start
#iterations: 236
currently lose_sum: 92.25423330068588
time_elpased: 2.526
batch start
#iterations: 237
currently lose_sum: 92.94504833221436
time_elpased: 2.602
batch start
#iterations: 238
currently lose_sum: 92.67769360542297
time_elpased: 2.35
batch start
#iterations: 239
currently lose_sum: 92.4612368941307
time_elpased: 2.486
start validation test
0.676134020619
0.655297204379
0.745471387402
0.697481823872
0.67601946074
57.911
batch start
#iterations: 240
currently lose_sum: 92.77314502000809
time_elpased: 2.175
batch start
#iterations: 241
currently lose_sum: 91.77365255355835
time_elpased: 2.517
batch start
#iterations: 242
currently lose_sum: 93.40862691402435
time_elpased: 2.538
batch start
#iterations: 243
currently lose_sum: 92.34018933773041
time_elpased: 2.501
batch start
#iterations: 244
currently lose_sum: 92.08455765247345
time_elpased: 2.595
batch start
#iterations: 245
currently lose_sum: 92.44161647558212
time_elpased: 2.647
batch start
#iterations: 246
currently lose_sum: 92.66055065393448
time_elpased: 2.673
batch start
#iterations: 247
currently lose_sum: 92.67465537786484
time_elpased: 2.499
batch start
#iterations: 248
currently lose_sum: 92.42736899852753
time_elpased: 2.371
batch start
#iterations: 249
currently lose_sum: 92.91561514139175
time_elpased: 2.461
batch start
#iterations: 250
currently lose_sum: 91.9133358001709
time_elpased: 2.569
batch start
#iterations: 251
currently lose_sum: 92.41014206409454
time_elpased: 2.51
batch start
#iterations: 252
currently lose_sum: 92.20700490474701
time_elpased: 2.487
batch start
#iterations: 253
currently lose_sum: 91.90119123458862
time_elpased: 2.473
batch start
#iterations: 254
currently lose_sum: 92.32152581214905
time_elpased: 2.834
batch start
#iterations: 255
currently lose_sum: 91.63725090026855
time_elpased: 2.607
batch start
#iterations: 256
currently lose_sum: 92.66660839319229
time_elpased: 2.508
batch start
#iterations: 257
currently lose_sum: 91.79884243011475
time_elpased: 2.498
batch start
#iterations: 258
currently lose_sum: 92.3881443142891
time_elpased: 2.444
batch start
#iterations: 259
currently lose_sum: 91.7127777338028
time_elpased: 2.532
start validation test
0.640927835052
0.616564937267
0.74855907781
0.676180736333
0.640750005654
60.014
batch start
#iterations: 260
currently lose_sum: 92.70702427625656
time_elpased: 2.182
batch start
#iterations: 261
currently lose_sum: 92.13510406017303
time_elpased: 2.382
batch start
#iterations: 262
currently lose_sum: 91.76347929239273
time_elpased: 2.106
batch start
#iterations: 263
currently lose_sum: 91.34062486886978
time_elpased: 2.026
batch start
#iterations: 264
currently lose_sum: 92.43580096960068
time_elpased: 2.546
batch start
#iterations: 265
currently lose_sum: 91.9077576994896
time_elpased: 2.873
batch start
#iterations: 266
currently lose_sum: 91.58877581357956
time_elpased: 2.803
batch start
#iterations: 267
currently lose_sum: 92.01932483911514
time_elpased: 2.489
batch start
#iterations: 268
currently lose_sum: 92.19172215461731
time_elpased: 2.565
batch start
#iterations: 269
currently lose_sum: 92.24809688329697
time_elpased: 2.589
batch start
#iterations: 270
currently lose_sum: 92.13173359632492
time_elpased: 2.643
batch start
#iterations: 271
currently lose_sum: 91.59729492664337
time_elpased: 2.814
batch start
#iterations: 272
currently lose_sum: 91.76413643360138
time_elpased: 2.741
batch start
#iterations: 273
currently lose_sum: 91.47024714946747
time_elpased: 2.293
batch start
#iterations: 274
currently lose_sum: 92.02662301063538
time_elpased: 2.721
batch start
#iterations: 275
currently lose_sum: 91.71967285871506
time_elpased: 2.307
batch start
#iterations: 276
currently lose_sum: 92.18084752559662
time_elpased: 2.44
batch start
#iterations: 277
currently lose_sum: 92.64398008584976
time_elpased: 2.637
batch start
#iterations: 278
currently lose_sum: 91.94995504617691
time_elpased: 2.448
batch start
#iterations: 279
currently lose_sum: 91.93226927518845
time_elpased: 2.478
start validation test
0.672835051546
0.673606101206
0.672704816797
0.67315515732
0.672835266722
58.162
batch start
#iterations: 280
currently lose_sum: 91.92823833227158
time_elpased: 2.598
batch start
#iterations: 281
currently lose_sum: 91.65960192680359
time_elpased: 2.62
batch start
#iterations: 282
currently lose_sum: 91.64161849021912
time_elpased: 2.475
batch start
#iterations: 283
currently lose_sum: 92.15030652284622
time_elpased: 2.576
batch start
#iterations: 284
currently lose_sum: 91.5237478017807
time_elpased: 2.596
batch start
#iterations: 285
currently lose_sum: 91.53373777866364
time_elpased: 2.524
batch start
#iterations: 286
currently lose_sum: 91.6175599694252
time_elpased: 2.624
batch start
#iterations: 287
currently lose_sum: 91.66403025388718
time_elpased: 2.823
batch start
#iterations: 288
currently lose_sum: 92.04145789146423
time_elpased: 2.085
batch start
#iterations: 289
currently lose_sum: 91.52697789669037
time_elpased: 2.49
batch start
#iterations: 290
currently lose_sum: 91.95418512821198
time_elpased: 2.707
batch start
#iterations: 291
currently lose_sum: 91.89507883787155
time_elpased: 2.489
batch start
#iterations: 292
currently lose_sum: 91.91811680793762
time_elpased: 2.459
batch start
#iterations: 293
currently lose_sum: 91.66255623102188
time_elpased: 2.568
batch start
#iterations: 294
currently lose_sum: 91.90383511781693
time_elpased: 2.626
batch start
#iterations: 295
currently lose_sum: 91.56267541646957
time_elpased: 2.752
batch start
#iterations: 296
currently lose_sum: 91.76829451322556
time_elpased: 2.303
batch start
#iterations: 297
currently lose_sum: 92.04365241527557
time_elpased: 2.511
batch start
#iterations: 298
currently lose_sum: 90.85250687599182
time_elpased: 2.479
batch start
#iterations: 299
currently lose_sum: 91.48058897256851
time_elpased: 2.508
start validation test
0.630567010309
0.602262697585
0.772540139975
0.676856485865
0.630332440909
60.920
batch start
#iterations: 300
currently lose_sum: 91.61311197280884
time_elpased: 2.631
batch start
#iterations: 301
currently lose_sum: 92.72542345523834
time_elpased: 2.451
batch start
#iterations: 302
currently lose_sum: 90.9436417222023
time_elpased: 2.612
batch start
#iterations: 303
currently lose_sum: 91.40060752630234
time_elpased: 2.617
batch start
#iterations: 304
currently lose_sum: 91.25832915306091
time_elpased: 1.948
batch start
#iterations: 305
currently lose_sum: 91.27956646680832
time_elpased: 2.499
batch start
#iterations: 306
currently lose_sum: 91.63620591163635
time_elpased: 2.614
batch start
#iterations: 307
currently lose_sum: 91.40723580121994
time_elpased: 2.524
batch start
#iterations: 308
currently lose_sum: 91.12780743837357
time_elpased: 2.449
batch start
#iterations: 309
currently lose_sum: 91.16288959980011
time_elpased: 2.548
batch start
#iterations: 310
currently lose_sum: 91.79779237508774
time_elpased: 2.511
batch start
#iterations: 311
currently lose_sum: 91.22665774822235
time_elpased: 2.348
batch start
#iterations: 312
currently lose_sum: 91.59837079048157
time_elpased: 2.226
batch start
#iterations: 313
currently lose_sum: 92.23314863443375
time_elpased: 2.454
batch start
#iterations: 314
currently lose_sum: 91.44755321741104
time_elpased: 2.484
batch start
#iterations: 315
currently lose_sum: 90.92830854654312
time_elpased: 2.499
batch start
#iterations: 316
currently lose_sum: 91.73469769954681
time_elpased: 2.482
batch start
#iterations: 317
currently lose_sum: 90.60460275411606
time_elpased: 2.328
batch start
#iterations: 318
currently lose_sum: 91.32311338186264
time_elpased: 1.823
batch start
#iterations: 319
currently lose_sum: 91.33367919921875
time_elpased: 1.61
start validation test
0.68175257732
0.682389289392
0.68196788802
0.682178523628
0.681752221581
57.796
batch start
#iterations: 320
currently lose_sum: 92.3059349656105
time_elpased: 1.522
batch start
#iterations: 321
currently lose_sum: 91.61646366119385
time_elpased: 1.549
batch start
#iterations: 322
currently lose_sum: 91.2749097943306
time_elpased: 1.503
batch start
#iterations: 323
currently lose_sum: 91.41022449731827
time_elpased: 1.541
batch start
#iterations: 324
currently lose_sum: 91.55202043056488
time_elpased: 1.673
batch start
#iterations: 325
currently lose_sum: 91.33529251813889
time_elpased: 1.619
batch start
#iterations: 326
currently lose_sum: 90.92423802614212
time_elpased: 1.568
batch start
#iterations: 327
currently lose_sum: 91.21927118301392
time_elpased: 1.631
batch start
#iterations: 328
currently lose_sum: 90.60123944282532
time_elpased: 1.714
batch start
#iterations: 329
currently lose_sum: 91.11361789703369
time_elpased: 1.776
batch start
#iterations: 330
currently lose_sum: 91.064606487751
time_elpased: 1.763
batch start
#iterations: 331
currently lose_sum: 91.07733082771301
time_elpased: 1.945
batch start
#iterations: 332
currently lose_sum: 91.43152594566345
time_elpased: 2.231
batch start
#iterations: 333
currently lose_sum: 91.46321511268616
time_elpased: 2.556
batch start
#iterations: 334
currently lose_sum: 91.33463788032532
time_elpased: 2.433
batch start
#iterations: 335
currently lose_sum: 91.08918571472168
time_elpased: 2.783
batch start
#iterations: 336
currently lose_sum: 91.14809662103653
time_elpased: 2.496
batch start
#iterations: 337
currently lose_sum: 91.29252463579178
time_elpased: 2.14
batch start
#iterations: 338
currently lose_sum: 91.13916486501694
time_elpased: 2.731
batch start
#iterations: 339
currently lose_sum: 90.95937925577164
time_elpased: 2.413
start validation test
0.646288659794
0.62661934339
0.726842321943
0.673020108644
0.646155568241
59.622
batch start
#iterations: 340
currently lose_sum: 91.38889956474304
time_elpased: 2.408
batch start
#iterations: 341
currently lose_sum: 90.86443084478378
time_elpased: 2.774
batch start
#iterations: 342
currently lose_sum: 91.06965601444244
time_elpased: 2.293
batch start
#iterations: 343
currently lose_sum: 91.59284800291061
time_elpased: 2.338
batch start
#iterations: 344
currently lose_sum: 91.13444113731384
time_elpased: 2.509
batch start
#iterations: 345
currently lose_sum: 90.97605329751968
time_elpased: 2.374
batch start
#iterations: 346
currently lose_sum: 91.0614607334137
time_elpased: 2.381
batch start
#iterations: 347
currently lose_sum: 90.54620683193207
time_elpased: 2.625
batch start
#iterations: 348
currently lose_sum: 91.20079159736633
time_elpased: 2.618
batch start
#iterations: 349
currently lose_sum: 91.12561213970184
time_elpased: 2.466
batch start
#iterations: 350
currently lose_sum: 91.03917992115021
time_elpased: 2.076
batch start
#iterations: 351
currently lose_sum: 91.2430237531662
time_elpased: 2.497
batch start
#iterations: 352
currently lose_sum: 91.02964955568314
time_elpased: 2.548
batch start
#iterations: 353
currently lose_sum: 91.43269747495651
time_elpased: 2.535
batch start
#iterations: 354
currently lose_sum: 90.70396310091019
time_elpased: 2.401
batch start
#iterations: 355
currently lose_sum: 91.16699278354645
time_elpased: 2.533
batch start
#iterations: 356
currently lose_sum: 90.28121888637543
time_elpased: 2.58
batch start
#iterations: 357
currently lose_sum: 91.93304777145386
time_elpased: 2.229
batch start
#iterations: 358
currently lose_sum: 90.55410438776016
time_elpased: 2.556
batch start
#iterations: 359
currently lose_sum: 90.24908304214478
time_elpased: 2.502
start validation test
0.68587628866
0.672541920732
0.726533552902
0.698495943004
0.685809114328
57.263
batch start
#iterations: 360
currently lose_sum: 90.9933460354805
time_elpased: 2.308
batch start
#iterations: 361
currently lose_sum: 90.43567055463791
time_elpased: 2.572
batch start
#iterations: 362
currently lose_sum: 90.83537477254868
time_elpased: 2.086
batch start
#iterations: 363
currently lose_sum: 91.38482594490051
time_elpased: 2.47
batch start
#iterations: 364
currently lose_sum: 90.65801918506622
time_elpased: 2.721
batch start
#iterations: 365
currently lose_sum: 91.44243609905243
time_elpased: 2.487
batch start
#iterations: 366
currently lose_sum: 90.920026242733
time_elpased: 2.554
batch start
#iterations: 367
currently lose_sum: 90.89686131477356
time_elpased: 2.565
batch start
#iterations: 368
currently lose_sum: 91.12426996231079
time_elpased: 2.404
batch start
#iterations: 369
currently lose_sum: 91.3137657046318
time_elpased: 2.549
batch start
#iterations: 370
currently lose_sum: 90.84949016571045
time_elpased: 1.946
batch start
#iterations: 371
currently lose_sum: 90.68294978141785
time_elpased: 2.553
batch start
#iterations: 372
currently lose_sum: 90.8430165052414
time_elpased: 2.636
batch start
#iterations: 373
currently lose_sum: 90.9925726056099
time_elpased: 2.575
batch start
#iterations: 374
currently lose_sum: 91.17705774307251
time_elpased: 2.235
batch start
#iterations: 375
currently lose_sum: 91.2326592206955
time_elpased: 2.463
batch start
#iterations: 376
currently lose_sum: 90.77765500545502
time_elpased: 2.513
batch start
#iterations: 377
currently lose_sum: 90.50707358121872
time_elpased: 2.535
batch start
#iterations: 378
currently lose_sum: 90.46504127979279
time_elpased: 2.372
batch start
#iterations: 379
currently lose_sum: 90.80651104450226
time_elpased: 2.543
start validation test
0.623865979381
0.60629229282
0.710065870729
0.654088646599
0.623723559074
60.831
batch start
#iterations: 380
currently lose_sum: 91.33750170469284
time_elpased: 2.498
batch start
#iterations: 381
currently lose_sum: 90.59698593616486
time_elpased: 2.611
batch start
#iterations: 382
currently lose_sum: 90.82154524326324
time_elpased: 2.542
batch start
#iterations: 383
currently lose_sum: 90.60455840826035
time_elpased: 2.456
batch start
#iterations: 384
currently lose_sum: 90.49205535650253
time_elpased: 2.508
batch start
#iterations: 385
currently lose_sum: 90.74070477485657
time_elpased: 2.365
batch start
#iterations: 386
currently lose_sum: 90.60456025600433
time_elpased: 2.493
batch start
#iterations: 387
currently lose_sum: 90.26261299848557
time_elpased: 2.537
batch start
#iterations: 388
currently lose_sum: 90.61875385046005
time_elpased: 2.208
batch start
#iterations: 389
currently lose_sum: 90.12726736068726
time_elpased: 2.337
batch start
#iterations: 390
currently lose_sum: 90.42990863323212
time_elpased: 2.718
batch start
#iterations: 391
currently lose_sum: 90.34479874372482
time_elpased: 2.732
batch start
#iterations: 392
currently lose_sum: 91.1032201051712
time_elpased: 2.706
batch start
#iterations: 393
currently lose_sum: 90.50039505958557
time_elpased: 2.384
batch start
#iterations: 394
currently lose_sum: 90.68685102462769
time_elpased: 2.552
batch start
#iterations: 395
currently lose_sum: 90.54874563217163
time_elpased: 2.791
batch start
#iterations: 396
currently lose_sum: 90.862149477005
time_elpased: 2.733
batch start
#iterations: 397
currently lose_sum: 90.375756919384
time_elpased: 2.647
batch start
#iterations: 398
currently lose_sum: 90.28907668590546
time_elpased: 2.808
batch start
#iterations: 399
currently lose_sum: 90.21575075387955
time_elpased: 2.765
start validation test
0.66381443299
0.642309748708
0.741869081927
0.68850893113
0.663685470331
58.579
acc: 0.696
pre: 0.683
rec: 0.734
F1: 0.708
auc: 0.757
