start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.39715373516083
time_elpased: 2.108
batch start
#iterations: 1
currently lose_sum: 99.9105115532875
time_elpased: 2.736
batch start
#iterations: 2
currently lose_sum: 99.79398334026337
time_elpased: 2.735
batch start
#iterations: 3
currently lose_sum: 99.79581713676453
time_elpased: 2.449
batch start
#iterations: 4
currently lose_sum: 99.76303672790527
time_elpased: 2.088
batch start
#iterations: 5
currently lose_sum: 99.5923941731453
time_elpased: 2.286
batch start
#iterations: 6
currently lose_sum: 99.51221150159836
time_elpased: 2.337
batch start
#iterations: 7
currently lose_sum: 99.62532860040665
time_elpased: 2.241
batch start
#iterations: 8
currently lose_sum: 99.68141043186188
time_elpased: 2.194
batch start
#iterations: 9
currently lose_sum: 99.34329605102539
time_elpased: 2.335
batch start
#iterations: 10
currently lose_sum: 99.4712443947792
time_elpased: 2.33
batch start
#iterations: 11
currently lose_sum: 99.45797836780548
time_elpased: 2.304
batch start
#iterations: 12
currently lose_sum: 99.43453693389893
time_elpased: 2.282
batch start
#iterations: 13
currently lose_sum: 99.45913398265839
time_elpased: 2.442
batch start
#iterations: 14
currently lose_sum: 99.13461208343506
time_elpased: 2.664
batch start
#iterations: 15
currently lose_sum: 99.24936789274216
time_elpased: 2.413
batch start
#iterations: 16
currently lose_sum: 99.4781801700592
time_elpased: 2.218
batch start
#iterations: 17
currently lose_sum: 99.1133177280426
time_elpased: 2.258
batch start
#iterations: 18
currently lose_sum: 99.34043604135513
time_elpased: 2.254
batch start
#iterations: 19
currently lose_sum: 99.32324624061584
time_elpased: 2.257
start validation test
0.627422680412
0.625668989195
0.637645363795
0.631600407747
0.627404732915
64.685
batch start
#iterations: 20
currently lose_sum: 99.32860881090164
time_elpased: 2.221
batch start
#iterations: 21
currently lose_sum: 99.13171255588531
time_elpased: 2.279
batch start
#iterations: 22
currently lose_sum: 99.27436798810959
time_elpased: 2.518
batch start
#iterations: 23
currently lose_sum: 99.11248683929443
time_elpased: 2.534
batch start
#iterations: 24
currently lose_sum: 99.21499490737915
time_elpased: 2.486
batch start
#iterations: 25
currently lose_sum: 99.29843401908875
time_elpased: 2.226
batch start
#iterations: 26
currently lose_sum: 99.09193629026413
time_elpased: 2.254
batch start
#iterations: 27
currently lose_sum: 99.16961598396301
time_elpased: 2.228
batch start
#iterations: 28
currently lose_sum: 99.24976575374603
time_elpased: 2.249
batch start
#iterations: 29
currently lose_sum: 98.85464364290237
time_elpased: 2.253
batch start
#iterations: 30
currently lose_sum: 99.04838836193085
time_elpased: 2.257
batch start
#iterations: 31
currently lose_sum: 98.98041331768036
time_elpased: 2.594
batch start
#iterations: 32
currently lose_sum: 98.96795785427094
time_elpased: 2.531
batch start
#iterations: 33
currently lose_sum: 99.02448534965515
time_elpased: 2.378
batch start
#iterations: 34
currently lose_sum: 99.0341289639473
time_elpased: 2.275
batch start
#iterations: 35
currently lose_sum: 98.9756526350975
time_elpased: 2.232
batch start
#iterations: 36
currently lose_sum: 99.10476863384247
time_elpased: 2.305
batch start
#iterations: 37
currently lose_sum: 98.80543279647827
time_elpased: 2.296
batch start
#iterations: 38
currently lose_sum: 98.77367132902145
time_elpased: 2.222
batch start
#iterations: 39
currently lose_sum: 98.9094769358635
time_elpased: 2.246
start validation test
0.64881443299
0.646076458753
0.660903571061
0.65340591138
0.648793208643
63.902
batch start
#iterations: 40
currently lose_sum: 98.8974027633667
time_elpased: 2.564
batch start
#iterations: 41
currently lose_sum: 98.97420263290405
time_elpased: 2.434
batch start
#iterations: 42
currently lose_sum: 98.88835090398788
time_elpased: 2.344
batch start
#iterations: 43
currently lose_sum: 98.89138334989548
time_elpased: 2.251
batch start
#iterations: 44
currently lose_sum: 98.99972414970398
time_elpased: 2.249
batch start
#iterations: 45
currently lose_sum: 98.88487136363983
time_elpased: 2.284
batch start
#iterations: 46
currently lose_sum: 98.78298598527908
time_elpased: 2.25
batch start
#iterations: 47
currently lose_sum: 99.08223581314087
time_elpased: 2.237
batch start
#iterations: 48
currently lose_sum: 98.9080393910408
time_elpased: 2.247
batch start
#iterations: 49
currently lose_sum: 98.8133442401886
time_elpased: 1.883
batch start
#iterations: 50
currently lose_sum: 98.80378651618958
time_elpased: 1.598
batch start
#iterations: 51
currently lose_sum: 98.89060038328171
time_elpased: 1.499
batch start
#iterations: 52
currently lose_sum: 98.84124290943146
time_elpased: 1.843
batch start
#iterations: 53
currently lose_sum: 98.63967460393906
time_elpased: 1.803
batch start
#iterations: 54
currently lose_sum: 98.98048061132431
time_elpased: 2.025
batch start
#iterations: 55
currently lose_sum: 98.76955807209015
time_elpased: 2.612
batch start
#iterations: 56
currently lose_sum: 98.67941761016846
time_elpased: 2.496
batch start
#iterations: 57
currently lose_sum: 98.76137405633926
time_elpased: 2.306
batch start
#iterations: 58
currently lose_sum: 98.69502055644989
time_elpased: 2.484
batch start
#iterations: 59
currently lose_sum: 98.97183668613434
time_elpased: 2.499
start validation test
0.624948453608
0.677115077637
0.480189358856
0.561897880539
0.625202600527
63.986
batch start
#iterations: 60
currently lose_sum: 98.81544631719589
time_elpased: 2.333
batch start
#iterations: 61
currently lose_sum: 99.05833685398102
time_elpased: 2.486
batch start
#iterations: 62
currently lose_sum: 98.78422921895981
time_elpased: 2.675
batch start
#iterations: 63
currently lose_sum: 98.81183725595474
time_elpased: 2.478
batch start
#iterations: 64
currently lose_sum: 99.0684505701065
time_elpased: 2.372
batch start
#iterations: 65
currently lose_sum: 98.67112666368484
time_elpased: 2.27
batch start
#iterations: 66
currently lose_sum: 98.83736699819565
time_elpased: 2.481
batch start
#iterations: 67
currently lose_sum: 98.99029886722565
time_elpased: 2.29
batch start
#iterations: 68
currently lose_sum: 98.847516477108
time_elpased: 2.506
batch start
#iterations: 69
currently lose_sum: 98.80726265907288
time_elpased: 2.287
batch start
#iterations: 70
currently lose_sum: 98.75185984373093
time_elpased: 2.202
batch start
#iterations: 71
currently lose_sum: 98.85068202018738
time_elpased: 2.339
batch start
#iterations: 72
currently lose_sum: 98.78020942211151
time_elpased: 2.421
batch start
#iterations: 73
currently lose_sum: 98.7762708067894
time_elpased: 2.514
batch start
#iterations: 74
currently lose_sum: 98.65517359972
time_elpased: 2.26
batch start
#iterations: 75
currently lose_sum: 98.81053549051285
time_elpased: 2.288
batch start
#iterations: 76
currently lose_sum: 98.79192584753036
time_elpased: 2.135
batch start
#iterations: 77
currently lose_sum: 98.86200499534607
time_elpased: 2.29
batch start
#iterations: 78
currently lose_sum: 98.7442678809166
time_elpased: 2.24
batch start
#iterations: 79
currently lose_sum: 98.76808840036392
time_elpased: 2.055
start validation test
0.620618556701
0.683367045278
0.451991355357
0.544103072349
0.620914607762
63.626
batch start
#iterations: 80
currently lose_sum: 98.78559213876724
time_elpased: 2.604
batch start
#iterations: 81
currently lose_sum: 98.49777263402939
time_elpased: 2.145
batch start
#iterations: 82
currently lose_sum: 98.8466426730156
time_elpased: 2.228
batch start
#iterations: 83
currently lose_sum: 98.58885073661804
time_elpased: 2.256
batch start
#iterations: 84
currently lose_sum: 98.9482609629631
time_elpased: 2.247
batch start
#iterations: 85
currently lose_sum: 98.8023008108139
time_elpased: 2.252
batch start
#iterations: 86
currently lose_sum: 98.64342761039734
time_elpased: 2.263
batch start
#iterations: 87
currently lose_sum: 98.64962351322174
time_elpased: 2.338
batch start
#iterations: 88
currently lose_sum: 98.8293667435646
time_elpased: 2.184
batch start
#iterations: 89
currently lose_sum: 98.67302507162094
time_elpased: 2.337
batch start
#iterations: 90
currently lose_sum: 98.72658896446228
time_elpased: 2.282
batch start
#iterations: 91
currently lose_sum: 98.74285924434662
time_elpased: 2.319
batch start
#iterations: 92
currently lose_sum: 98.47675877809525
time_elpased: 2.223
batch start
#iterations: 93
currently lose_sum: 98.7836766242981
time_elpased: 2.253
batch start
#iterations: 94
currently lose_sum: 98.79482513666153
time_elpased: 2.206
batch start
#iterations: 95
currently lose_sum: 98.74580019712448
time_elpased: 2.224
batch start
#iterations: 96
currently lose_sum: 98.73503905534744
time_elpased: 2.313
batch start
#iterations: 97
currently lose_sum: 98.85041511058807
time_elpased: 2.351
batch start
#iterations: 98
currently lose_sum: 98.93633913993835
time_elpased: 2.533
batch start
#iterations: 99
currently lose_sum: 98.83491003513336
time_elpased: 2.512
start validation test
0.614896907216
0.680953915566
0.434907893383
0.53080449664
0.615212905692
63.689
batch start
#iterations: 100
currently lose_sum: 98.68618828058243
time_elpased: 2.293
batch start
#iterations: 101
currently lose_sum: 98.5572110414505
time_elpased: 2.251
batch start
#iterations: 102
currently lose_sum: 98.6582265496254
time_elpased: 2.24
batch start
#iterations: 103
currently lose_sum: 98.669318318367
time_elpased: 2.25
batch start
#iterations: 104
currently lose_sum: 98.71650719642639
time_elpased: 2.285
batch start
#iterations: 105
currently lose_sum: 98.63893300294876
time_elpased: 2.33
batch start
#iterations: 106
currently lose_sum: 98.76293575763702
time_elpased: 2.326
batch start
#iterations: 107
currently lose_sum: 98.67939788103104
time_elpased: 2.281
batch start
#iterations: 108
currently lose_sum: 98.678526699543
time_elpased: 2.173
batch start
#iterations: 109
currently lose_sum: 98.85623931884766
time_elpased: 1.867
batch start
#iterations: 110
currently lose_sum: 98.60011464357376
time_elpased: 1.791
batch start
#iterations: 111
currently lose_sum: 98.85569006204605
time_elpased: 2.279
batch start
#iterations: 112
currently lose_sum: 98.49970948696136
time_elpased: 2.238
batch start
#iterations: 113
currently lose_sum: 98.2944648861885
time_elpased: 2.248
batch start
#iterations: 114
currently lose_sum: 98.96109282970428
time_elpased: 2.235
batch start
#iterations: 115
currently lose_sum: 98.60021644830704
time_elpased: 2.31
batch start
#iterations: 116
currently lose_sum: 98.50870162248611
time_elpased: 2.312
batch start
#iterations: 117
currently lose_sum: 98.48285764455795
time_elpased: 2.009
batch start
#iterations: 118
currently lose_sum: 98.60108643770218
time_elpased: 1.946
batch start
#iterations: 119
currently lose_sum: 98.67775493860245
time_elpased: 2.028
start validation test
0.650309278351
0.668195894025
0.59956776783
0.632024300282
0.650398362899
63.220
batch start
#iterations: 120
currently lose_sum: 98.3831844329834
time_elpased: 2.508
batch start
#iterations: 121
currently lose_sum: 98.60498350858688
time_elpased: 2.614
batch start
#iterations: 122
currently lose_sum: 98.8125946521759
time_elpased: 2.172
batch start
#iterations: 123
currently lose_sum: 98.51838773488998
time_elpased: 2.448
batch start
#iterations: 124
currently lose_sum: 98.66699260473251
time_elpased: 2.426
batch start
#iterations: 125
currently lose_sum: 98.7730502486229
time_elpased: 2.18
batch start
#iterations: 126
currently lose_sum: 98.7741881608963
time_elpased: 2.409
batch start
#iterations: 127
currently lose_sum: 98.63423359394073
time_elpased: 2.19
batch start
#iterations: 128
currently lose_sum: 98.50380021333694
time_elpased: 2.615
batch start
#iterations: 129
currently lose_sum: 98.55295020341873
time_elpased: 2.645
batch start
#iterations: 130
currently lose_sum: 98.73249733448029
time_elpased: 2.332
batch start
#iterations: 131
currently lose_sum: 98.68184494972229
time_elpased: 2.623
batch start
#iterations: 132
currently lose_sum: 98.83032876253128
time_elpased: 2.508
batch start
#iterations: 133
currently lose_sum: 98.37615060806274
time_elpased: 2.403
batch start
#iterations: 134
currently lose_sum: 98.76875030994415
time_elpased: 2.498
batch start
#iterations: 135
currently lose_sum: 98.62317591905594
time_elpased: 2.374
batch start
#iterations: 136
currently lose_sum: 98.57849055528641
time_elpased: 2.169
batch start
#iterations: 137
currently lose_sum: 98.70919036865234
time_elpased: 2.41
batch start
#iterations: 138
currently lose_sum: 98.750523686409
time_elpased: 2.468
batch start
#iterations: 139
currently lose_sum: 98.55575889348984
time_elpased: 2.384
start validation test
0.649381443299
0.666021186923
0.601728928682
0.632244809689
0.649465104638
63.176
batch start
#iterations: 140
currently lose_sum: 98.60887289047241
time_elpased: 2.257
batch start
#iterations: 141
currently lose_sum: 98.92985111474991
time_elpased: 2.341
batch start
#iterations: 142
currently lose_sum: 98.58694875240326
time_elpased: 2.369
batch start
#iterations: 143
currently lose_sum: 98.89407390356064
time_elpased: 2.245
batch start
#iterations: 144
currently lose_sum: 98.63576698303223
time_elpased: 2.318
batch start
#iterations: 145
currently lose_sum: 98.73004364967346
time_elpased: 2.428
batch start
#iterations: 146
currently lose_sum: 98.50047439336777
time_elpased: 2.496
batch start
#iterations: 147
currently lose_sum: 98.66032499074936
time_elpased: 2.388
batch start
#iterations: 148
currently lose_sum: 98.64215922355652
time_elpased: 2.247
batch start
#iterations: 149
currently lose_sum: 98.63907486200333
time_elpased: 2.254
batch start
#iterations: 150
currently lose_sum: 98.60813367366791
time_elpased: 2.25
batch start
#iterations: 151
currently lose_sum: 98.44467860460281
time_elpased: 2.219
batch start
#iterations: 152
currently lose_sum: 98.52585518360138
time_elpased: 2.355
batch start
#iterations: 153
currently lose_sum: 98.64348703622818
time_elpased: 2.343
batch start
#iterations: 154
currently lose_sum: 98.77643489837646
time_elpased: 2.513
batch start
#iterations: 155
currently lose_sum: 98.58668357133865
time_elpased: 2.4
batch start
#iterations: 156
currently lose_sum: 98.62644571065903
time_elpased: 2.379
batch start
#iterations: 157
currently lose_sum: 98.65374147891998
time_elpased: 2.258
batch start
#iterations: 158
currently lose_sum: 98.65170574188232
time_elpased: 2.084
batch start
#iterations: 159
currently lose_sum: 98.56849151849747
time_elpased: 1.919
start validation test
0.627731958763
0.685336502748
0.474735000515
0.5609192607
0.628000568521
63.356
batch start
#iterations: 160
currently lose_sum: 98.73835629224777
time_elpased: 1.838
batch start
#iterations: 161
currently lose_sum: 98.60499030351639
time_elpased: 1.999
batch start
#iterations: 162
currently lose_sum: 98.35223662853241
time_elpased: 2.394
batch start
#iterations: 163
currently lose_sum: 98.64646261930466
time_elpased: 2.177
batch start
#iterations: 164
currently lose_sum: 98.62694108486176
time_elpased: 2.344
batch start
#iterations: 165
currently lose_sum: 98.43231046199799
time_elpased: 2.181
batch start
#iterations: 166
currently lose_sum: 98.66301566362381
time_elpased: 2.329
batch start
#iterations: 167
currently lose_sum: 98.704396545887
time_elpased: 2.267
batch start
#iterations: 168
currently lose_sum: 98.79179257154465
time_elpased: 2.245
batch start
#iterations: 169
currently lose_sum: 98.59496825933456
time_elpased: 2.263
batch start
#iterations: 170
currently lose_sum: 98.55070477724075
time_elpased: 2.256
batch start
#iterations: 171
currently lose_sum: 98.66861981153488
time_elpased: 2.2
batch start
#iterations: 172
currently lose_sum: 98.55086606740952
time_elpased: 2.341
batch start
#iterations: 173
currently lose_sum: 98.46202486753464
time_elpased: 2.455
batch start
#iterations: 174
currently lose_sum: 98.5254635810852
time_elpased: 2.176
batch start
#iterations: 175
currently lose_sum: 98.55186432600021
time_elpased: 2.329
batch start
#iterations: 176
currently lose_sum: 98.7777658700943
time_elpased: 2.31
batch start
#iterations: 177
currently lose_sum: 98.44687110185623
time_elpased: 2.249
batch start
#iterations: 178
currently lose_sum: 98.46163934469223
time_elpased: 2.235
batch start
#iterations: 179
currently lose_sum: 98.7510878443718
time_elpased: 2.278
start validation test
0.619381443299
0.683210303125
0.447669033652
0.540910221338
0.619682910919
63.510
batch start
#iterations: 180
currently lose_sum: 98.6265938282013
time_elpased: 2.263
batch start
#iterations: 181
currently lose_sum: 98.6602965593338
time_elpased: 1.896
batch start
#iterations: 182
currently lose_sum: 98.61556673049927
time_elpased: 2.092
batch start
#iterations: 183
currently lose_sum: 98.28226709365845
time_elpased: 1.973
batch start
#iterations: 184
currently lose_sum: 98.50540894269943
time_elpased: 1.904
batch start
#iterations: 185
currently lose_sum: 98.38579505681992
time_elpased: 1.964
batch start
#iterations: 186
currently lose_sum: 98.52397656440735
time_elpased: 2.364
batch start
#iterations: 187
currently lose_sum: 98.79310047626495
time_elpased: 2.742
batch start
#iterations: 188
currently lose_sum: 98.6536796092987
time_elpased: 2.289
batch start
#iterations: 189
currently lose_sum: 98.41985303163528
time_elpased: 2.527
batch start
#iterations: 190
currently lose_sum: 98.4482626914978
time_elpased: 2.61
batch start
#iterations: 191
currently lose_sum: 98.36477547883987
time_elpased: 2.157
batch start
#iterations: 192
currently lose_sum: 98.68459296226501
time_elpased: 2.296
batch start
#iterations: 193
currently lose_sum: 98.50207769870758
time_elpased: 2.558
batch start
#iterations: 194
currently lose_sum: 98.52806186676025
time_elpased: 2.602
batch start
#iterations: 195
currently lose_sum: 98.53226393461227
time_elpased: 2.538
batch start
#iterations: 196
currently lose_sum: 98.51168936491013
time_elpased: 2.364
batch start
#iterations: 197
currently lose_sum: 98.40444988012314
time_elpased: 2.536
batch start
#iterations: 198
currently lose_sum: 98.35178858041763
time_elpased: 2.029
batch start
#iterations: 199
currently lose_sum: 98.69881391525269
time_elpased: 2.495
start validation test
0.658969072165
0.656949083915
0.667901615725
0.662380077567
0.658953389707
63.282
batch start
#iterations: 200
currently lose_sum: 98.34320294857025
time_elpased: 2.175
batch start
#iterations: 201
currently lose_sum: 98.49764442443848
time_elpased: 2.529
batch start
#iterations: 202
currently lose_sum: 98.54432100057602
time_elpased: 2.376
batch start
#iterations: 203
currently lose_sum: 98.63338285684586
time_elpased: 2.613
batch start
#iterations: 204
currently lose_sum: 98.4878939986229
time_elpased: 2.511
batch start
#iterations: 205
currently lose_sum: 98.6956257224083
time_elpased: 2.294
batch start
#iterations: 206
currently lose_sum: 98.39776229858398
time_elpased: 2.353
batch start
#iterations: 207
currently lose_sum: 98.47970867156982
time_elpased: 2.451
batch start
#iterations: 208
currently lose_sum: 98.58947509527206
time_elpased: 2.264
batch start
#iterations: 209
currently lose_sum: 98.39566332101822
time_elpased: 2.256
batch start
#iterations: 210
currently lose_sum: 98.52532452344894
time_elpased: 1.898
batch start
#iterations: 211
currently lose_sum: 98.38838785886765
time_elpased: 2.605
batch start
#iterations: 212
currently lose_sum: 98.36129051446915
time_elpased: 2.448
batch start
#iterations: 213
currently lose_sum: 98.54676699638367
time_elpased: 2.304
batch start
#iterations: 214
currently lose_sum: 98.54845017194748
time_elpased: 2.244
batch start
#iterations: 215
currently lose_sum: 98.40397781133652
time_elpased: 2.26
batch start
#iterations: 216
currently lose_sum: 98.57371252775192
time_elpased: 2.263
batch start
#iterations: 217
currently lose_sum: 98.59873235225677
time_elpased: 2.275
batch start
#iterations: 218
currently lose_sum: 98.2771053314209
time_elpased: 2.433
batch start
#iterations: 219
currently lose_sum: 98.54240095615387
time_elpased: 2.163
start validation test
0.651134020619
0.675723989989
0.583513430071
0.626242544732
0.651252738995
63.206
batch start
#iterations: 220
currently lose_sum: 98.66377979516983
time_elpased: 2.441
batch start
#iterations: 221
currently lose_sum: 98.49643987417221
time_elpased: 2.206
batch start
#iterations: 222
currently lose_sum: 98.54697811603546
time_elpased: 2.33
batch start
#iterations: 223
currently lose_sum: 98.54547917842865
time_elpased: 2.249
batch start
#iterations: 224
currently lose_sum: 98.65175741910934
time_elpased: 2.264
batch start
#iterations: 225
currently lose_sum: 98.35366129875183
time_elpased: 2.314
batch start
#iterations: 226
currently lose_sum: 98.35196715593338
time_elpased: 2.272
batch start
#iterations: 227
currently lose_sum: 98.55558013916016
time_elpased: 2.256
batch start
#iterations: 228
currently lose_sum: 98.5132582783699
time_elpased: 2.277
batch start
#iterations: 229
currently lose_sum: 98.6806720495224
time_elpased: 2.569
batch start
#iterations: 230
currently lose_sum: 98.61699944734573
time_elpased: 2.3
batch start
#iterations: 231
currently lose_sum: 98.5692647099495
time_elpased: 2.282
batch start
#iterations: 232
currently lose_sum: 98.49147856235504
time_elpased: 2.263
batch start
#iterations: 233
currently lose_sum: 98.579414665699
time_elpased: 2.255
batch start
#iterations: 234
currently lose_sum: 98.5835092663765
time_elpased: 2.261
batch start
#iterations: 235
currently lose_sum: 98.30655533075333
time_elpased: 2.322
batch start
#iterations: 236
currently lose_sum: 98.58902883529663
time_elpased: 2.405
batch start
#iterations: 237
currently lose_sum: 98.53152704238892
time_elpased: 2.415
batch start
#iterations: 238
currently lose_sum: 98.43269693851471
time_elpased: 2.133
batch start
#iterations: 239
currently lose_sum: 98.62321710586548
time_elpased: 2.135
start validation test
0.598711340206
0.68896713615
0.362457548626
0.475015172972
0.599126120177
63.722
batch start
#iterations: 240
currently lose_sum: 98.71045207977295
time_elpased: 2.29
batch start
#iterations: 241
currently lose_sum: 98.59919673204422
time_elpased: 2.249
batch start
#iterations: 242
currently lose_sum: 98.42116689682007
time_elpased: 2.241
batch start
#iterations: 243
currently lose_sum: 98.50854587554932
time_elpased: 2.246
batch start
#iterations: 244
currently lose_sum: 98.58921736478806
time_elpased: 2.255
batch start
#iterations: 245
currently lose_sum: 98.49705773591995
time_elpased: 2.306
batch start
#iterations: 246
currently lose_sum: 98.81248986721039
time_elpased: 2.358
batch start
#iterations: 247
currently lose_sum: 98.45915585756302
time_elpased: 1.969
batch start
#iterations: 248
currently lose_sum: 98.42569333314896
time_elpased: 1.751
batch start
#iterations: 249
currently lose_sum: 98.47231495380402
time_elpased: 2.321
batch start
#iterations: 250
currently lose_sum: 98.73817205429077
time_elpased: 2.161
batch start
#iterations: 251
currently lose_sum: 98.37866562604904
time_elpased: 2.761
batch start
#iterations: 252
currently lose_sum: 98.67956948280334
time_elpased: 2.473
batch start
#iterations: 253
currently lose_sum: 98.43049943447113
time_elpased: 2.381
batch start
#iterations: 254
currently lose_sum: 98.44907128810883
time_elpased: 2.558
batch start
#iterations: 255
currently lose_sum: 98.39509838819504
time_elpased: 2.597
batch start
#iterations: 256
currently lose_sum: 98.29467403888702
time_elpased: 2.193
batch start
#iterations: 257
currently lose_sum: 98.60602313280106
time_elpased: 2.363
batch start
#iterations: 258
currently lose_sum: 98.22838270664215
time_elpased: 2.667
batch start
#iterations: 259
currently lose_sum: 98.58831417560577
time_elpased: 2.292
start validation test
0.641546391753
0.680328938781
0.536379541011
0.599838876741
0.64173102838
63.063
batch start
#iterations: 260
currently lose_sum: 98.45645916461945
time_elpased: 2.554
batch start
#iterations: 261
currently lose_sum: 98.50008291006088
time_elpased: 2.653
batch start
#iterations: 262
currently lose_sum: 98.5307902097702
time_elpased: 2.231
batch start
#iterations: 263
currently lose_sum: 98.48702174425125
time_elpased: 2.515
batch start
#iterations: 264
currently lose_sum: 98.56063312292099
time_elpased: 2.318
batch start
#iterations: 265
currently lose_sum: 98.50038057565689
time_elpased: 2.436
batch start
#iterations: 266
currently lose_sum: 98.67421263456345
time_elpased: 2.36
batch start
#iterations: 267
currently lose_sum: 98.38044261932373
time_elpased: 2.482
batch start
#iterations: 268
currently lose_sum: 98.63488000631332
time_elpased: 2.156
batch start
#iterations: 269
currently lose_sum: 98.62249737977982
time_elpased: 1.967
batch start
#iterations: 270
currently lose_sum: 98.53420633077621
time_elpased: 2.107
batch start
#iterations: 271
currently lose_sum: 98.29108250141144
time_elpased: 2.102
batch start
#iterations: 272
currently lose_sum: 98.54929637908936
time_elpased: 2.28
batch start
#iterations: 273
currently lose_sum: 98.56912779808044
time_elpased: 2.098
batch start
#iterations: 274
currently lose_sum: 98.61268281936646
time_elpased: 2.254
batch start
#iterations: 275
currently lose_sum: 98.57240122556686
time_elpased: 2.388
batch start
#iterations: 276
currently lose_sum: 98.58359342813492
time_elpased: 2.252
batch start
#iterations: 277
currently lose_sum: 98.60661053657532
time_elpased: 2.504
batch start
#iterations: 278
currently lose_sum: 98.53377014398575
time_elpased: 2.124
batch start
#iterations: 279
currently lose_sum: 98.37665122747421
time_elpased: 2.303
start validation test
0.663195876289
0.657965260546
0.682206442318
0.669866612773
0.663162500308
62.997
batch start
#iterations: 280
currently lose_sum: 98.54763519763947
time_elpased: 2.256
batch start
#iterations: 281
currently lose_sum: 98.27923250198364
time_elpased: 2.276
batch start
#iterations: 282
currently lose_sum: 98.62150502204895
time_elpased: 2.315
batch start
#iterations: 283
currently lose_sum: 98.25049871206284
time_elpased: 2.424
batch start
#iterations: 284
currently lose_sum: 98.41899198293686
time_elpased: 2.407
batch start
#iterations: 285
currently lose_sum: 98.46389043331146
time_elpased: 2.359
batch start
#iterations: 286
currently lose_sum: 98.40658259391785
time_elpased: 2.5
batch start
#iterations: 287
currently lose_sum: 98.53829956054688
time_elpased: 2.203
batch start
#iterations: 288
currently lose_sum: 98.46572232246399
time_elpased: 2.249
batch start
#iterations: 289
currently lose_sum: 98.37941384315491
time_elpased: 2.074
batch start
#iterations: 290
currently lose_sum: 98.55059921741486
time_elpased: 1.84
batch start
#iterations: 291
currently lose_sum: 98.31993305683136
time_elpased: 2.245
batch start
#iterations: 292
currently lose_sum: 98.42276936769485
time_elpased: 2.175
batch start
#iterations: 293
currently lose_sum: 98.24799168109894
time_elpased: 1.963
batch start
#iterations: 294
currently lose_sum: 98.45409542322159
time_elpased: 2.623
batch start
#iterations: 295
currently lose_sum: 98.48024874925613
time_elpased: 2.536
batch start
#iterations: 296
currently lose_sum: 98.33527529239655
time_elpased: 1.929
batch start
#iterations: 297
currently lose_sum: 98.8282008767128
time_elpased: 2.243
batch start
#iterations: 298
currently lose_sum: 98.35930252075195
time_elpased: 2.228
batch start
#iterations: 299
currently lose_sum: 98.6315729022026
time_elpased: 2.249
start validation test
0.648041237113
0.687135639332
0.545847483791
0.608396421198
0.64822065401
62.986
batch start
#iterations: 300
currently lose_sum: 98.56435418128967
time_elpased: 2.244
batch start
#iterations: 301
currently lose_sum: 98.37130725383759
time_elpased: 2.314
batch start
#iterations: 302
currently lose_sum: 98.48491448163986
time_elpased: 2.347
batch start
#iterations: 303
currently lose_sum: 98.30643165111542
time_elpased: 2.546
batch start
#iterations: 304
currently lose_sum: 98.3288682103157
time_elpased: 2.519
batch start
#iterations: 305
currently lose_sum: 98.40226799249649
time_elpased: 2.159
batch start
#iterations: 306
currently lose_sum: 98.4799445271492
time_elpased: 2.267
batch start
#iterations: 307
currently lose_sum: 98.35053718090057
time_elpased: 2.25
batch start
#iterations: 308
currently lose_sum: 98.70976084470749
time_elpased: 2.264
batch start
#iterations: 309
currently lose_sum: 98.47974270582199
time_elpased: 2.275
batch start
#iterations: 310
currently lose_sum: 98.44847041368484
time_elpased: 2.285
batch start
#iterations: 311
currently lose_sum: 98.63900434970856
time_elpased: 2.305
batch start
#iterations: 312
currently lose_sum: 98.47928303480148
time_elpased: 2.279
batch start
#iterations: 313
currently lose_sum: 98.46249175071716
time_elpased: 1.814
batch start
#iterations: 314
currently lose_sum: 98.23419314622879
time_elpased: 1.929
batch start
#iterations: 315
currently lose_sum: 98.25327008962631
time_elpased: 1.829
batch start
#iterations: 316
currently lose_sum: 98.73442405462265
time_elpased: 2.07
batch start
#iterations: 317
currently lose_sum: 98.55757886171341
time_elpased: 2.328
batch start
#iterations: 318
currently lose_sum: 98.28317755460739
time_elpased: 2.15
batch start
#iterations: 319
currently lose_sum: 98.39713352918625
time_elpased: 1.816
start validation test
0.635206185567
0.689003436426
0.495214572399
0.576252918987
0.635451962436
63.026
batch start
#iterations: 320
currently lose_sum: 98.36411279439926
time_elpased: 2.243
batch start
#iterations: 321
currently lose_sum: 98.26391249895096
time_elpased: 2.172
batch start
#iterations: 322
currently lose_sum: 98.5709518790245
time_elpased: 2.467
batch start
#iterations: 323
currently lose_sum: 98.5372326374054
time_elpased: 2.56
batch start
#iterations: 324
currently lose_sum: 98.60203295946121
time_elpased: 2.666
batch start
#iterations: 325
currently lose_sum: 98.75337028503418
time_elpased: 2.33
batch start
#iterations: 326
currently lose_sum: 98.40049356222153
time_elpased: 2.529
batch start
#iterations: 327
currently lose_sum: 98.40111947059631
time_elpased: 2.262
batch start
#iterations: 328
currently lose_sum: 98.19152897596359
time_elpased: 2.182
batch start
#iterations: 329
currently lose_sum: 98.3895708322525
time_elpased: 2.542
batch start
#iterations: 330
currently lose_sum: 98.5814214348793
time_elpased: 2.534
batch start
#iterations: 331
currently lose_sum: 98.47552675008774
time_elpased: 2.719
batch start
#iterations: 332
currently lose_sum: 98.37815660238266
time_elpased: 1.898
batch start
#iterations: 333
currently lose_sum: 98.63967770338058
time_elpased: 2.528
batch start
#iterations: 334
currently lose_sum: 98.77013808488846
time_elpased: 2.529
batch start
#iterations: 335
currently lose_sum: 98.15962982177734
time_elpased: 2.561
batch start
#iterations: 336
currently lose_sum: 98.7473840713501
time_elpased: 2.428
batch start
#iterations: 337
currently lose_sum: 98.17990380525589
time_elpased: 2.455
batch start
#iterations: 338
currently lose_sum: 98.33834946155548
time_elpased: 2.41
batch start
#iterations: 339
currently lose_sum: 98.4020209312439
time_elpased: 2.4
start validation test
0.642886597938
0.672139242069
0.560358135227
0.611179705915
0.643031489384
63.110
batch start
#iterations: 340
currently lose_sum: 98.35563188791275
time_elpased: 2.368
batch start
#iterations: 341
currently lose_sum: 98.61394339799881
time_elpased: 2.404
batch start
#iterations: 342
currently lose_sum: 98.4654210805893
time_elpased: 2.449
batch start
#iterations: 343
currently lose_sum: 98.53058397769928
time_elpased: 2.333
batch start
#iterations: 344
currently lose_sum: 98.18896853923798
time_elpased: 2.293
batch start
#iterations: 345
currently lose_sum: 98.43412858247757
time_elpased: 2.281
batch start
#iterations: 346
currently lose_sum: 98.52399414777756
time_elpased: 2.331
batch start
#iterations: 347
currently lose_sum: 98.39150846004486
time_elpased: 1.903
batch start
#iterations: 348
currently lose_sum: 98.56938058137894
time_elpased: 2.361
batch start
#iterations: 349
currently lose_sum: 98.27649563550949
time_elpased: 2.104
batch start
#iterations: 350
currently lose_sum: 98.46551865339279
time_elpased: 2.172
batch start
#iterations: 351
currently lose_sum: 98.2473816871643
time_elpased: 2.382
batch start
#iterations: 352
currently lose_sum: 98.27641659975052
time_elpased: 2.314
batch start
#iterations: 353
currently lose_sum: 98.56613367795944
time_elpased: 2.319
batch start
#iterations: 354
currently lose_sum: 98.29034072160721
time_elpased: 2.251
batch start
#iterations: 355
currently lose_sum: 98.37469428777695
time_elpased: 2.366
batch start
#iterations: 356
currently lose_sum: 98.35001862049103
time_elpased: 2.427
batch start
#iterations: 357
currently lose_sum: 98.42082077264786
time_elpased: 2.356
batch start
#iterations: 358
currently lose_sum: 98.1101786494255
time_elpased: 2.318
batch start
#iterations: 359
currently lose_sum: 98.35253077745438
time_elpased: 2.333
start validation test
0.664381443299
0.673748103187
0.639703612226
0.656284643404
0.664424769038
62.746
batch start
#iterations: 360
currently lose_sum: 98.4528940320015
time_elpased: 2.4
batch start
#iterations: 361
currently lose_sum: 98.51712244749069
time_elpased: 2.344
batch start
#iterations: 362
currently lose_sum: 98.44483917951584
time_elpased: 2.276
batch start
#iterations: 363
currently lose_sum: 98.50159734487534
time_elpased: 2.285
batch start
#iterations: 364
currently lose_sum: 98.35842901468277
time_elpased: 2.393
batch start
#iterations: 365
currently lose_sum: 98.59560489654541
time_elpased: 2.267
batch start
#iterations: 366
currently lose_sum: 98.34636461734772
time_elpased: 2.095
batch start
#iterations: 367
currently lose_sum: 98.39934104681015
time_elpased: 2.249
batch start
#iterations: 368
currently lose_sum: 98.46892058849335
time_elpased: 2.264
batch start
#iterations: 369
currently lose_sum: 98.67036271095276
time_elpased: 2.297
batch start
#iterations: 370
currently lose_sum: 98.56702953577042
time_elpased: 2.411
batch start
#iterations: 371
currently lose_sum: 98.27453076839447
time_elpased: 2.117
batch start
#iterations: 372
currently lose_sum: 98.50228947401047
time_elpased: 2.255
batch start
#iterations: 373
currently lose_sum: 98.60714197158813
time_elpased: 2.321
batch start
#iterations: 374
currently lose_sum: 98.43027973175049
time_elpased: 2.344
batch start
#iterations: 375
currently lose_sum: 98.37505859136581
time_elpased: 2.068
batch start
#iterations: 376
currently lose_sum: 98.37075197696686
time_elpased: 1.852
batch start
#iterations: 377
currently lose_sum: 98.52844029664993
time_elpased: 2.268
batch start
#iterations: 378
currently lose_sum: 98.43468362092972
time_elpased: 2.235
batch start
#iterations: 379
currently lose_sum: 98.52959901094437
time_elpased: 2.058
start validation test
0.660927835052
0.670024916044
0.636513327159
0.652839349799
0.660970698486
62.948
batch start
#iterations: 380
currently lose_sum: 98.29184919595718
time_elpased: 1.885
batch start
#iterations: 381
currently lose_sum: 98.44347095489502
time_elpased: 2.109
batch start
#iterations: 382
currently lose_sum: 98.19508683681488
time_elpased: 2.317
batch start
#iterations: 383
currently lose_sum: 98.22762036323547
time_elpased: 2.407
batch start
#iterations: 384
currently lose_sum: 98.52153146266937
time_elpased: 2.305
batch start
#iterations: 385
currently lose_sum: 98.66304218769073
time_elpased: 2.15
batch start
#iterations: 386
currently lose_sum: 98.58673697710037
time_elpased: 2.527
batch start
#iterations: 387
currently lose_sum: 98.61850661039352
time_elpased: 2.898
batch start
#iterations: 388
currently lose_sum: 98.36590087413788
time_elpased: 2.362
batch start
#iterations: 389
currently lose_sum: 98.4173544049263
time_elpased: 2.577
batch start
#iterations: 390
currently lose_sum: 98.49907720088959
time_elpased: 2.673
batch start
#iterations: 391
currently lose_sum: 98.49260801076889
time_elpased: 2.409
batch start
#iterations: 392
currently lose_sum: 98.33588653802872
time_elpased: 2.601
batch start
#iterations: 393
currently lose_sum: 98.10744744539261
time_elpased: 2.427
batch start
#iterations: 394
currently lose_sum: 98.28334212303162
time_elpased: 2.084
batch start
#iterations: 395
currently lose_sum: 98.32270985841751
time_elpased: 1.903
batch start
#iterations: 396
currently lose_sum: 98.52269357442856
time_elpased: 2.601
batch start
#iterations: 397
currently lose_sum: 98.1122470498085
time_elpased: 2.366
batch start
#iterations: 398
currently lose_sum: 98.37927252054214
time_elpased: 2.382
batch start
#iterations: 399
currently lose_sum: 98.22335708141327
time_elpased: 2.195
start validation test
0.67
0.669669362268
0.673253061645
0.671456430258
0.669994288749
62.718
acc: 0.667
pre: 0.665
rec: 0.674
F1: 0.670
auc: 0.707
