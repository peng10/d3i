start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.56555968523026
time_elpased: 3.496
batch start
#iterations: 1
currently lose_sum: 100.37063628435135
time_elpased: 3.505
batch start
#iterations: 2
currently lose_sum: 100.32297170162201
time_elpased: 3.752
batch start
#iterations: 3
currently lose_sum: 100.16891396045685
time_elpased: 3.587
batch start
#iterations: 4
currently lose_sum: 100.13851720094681
time_elpased: 3.531
batch start
#iterations: 5
currently lose_sum: 100.09944480657578
time_elpased: 3.728
batch start
#iterations: 6
currently lose_sum: 99.93436348438263
time_elpased: 3.872
batch start
#iterations: 7
currently lose_sum: 99.60685563087463
time_elpased: 3.644
batch start
#iterations: 8
currently lose_sum: 99.45080006122589
time_elpased: 3.867
batch start
#iterations: 9
currently lose_sum: 99.53269785642624
time_elpased: 3.685
batch start
#iterations: 10
currently lose_sum: 99.47132807970047
time_elpased: 3.609
batch start
#iterations: 11
currently lose_sum: 99.24496293067932
time_elpased: 3.879
batch start
#iterations: 12
currently lose_sum: 99.02599811553955
time_elpased: 3.894
batch start
#iterations: 13
currently lose_sum: 98.91785591840744
time_elpased: 3.976
batch start
#iterations: 14
currently lose_sum: 98.91276597976685
time_elpased: 3.764
batch start
#iterations: 15
currently lose_sum: 98.601802110672
time_elpased: 3.829
batch start
#iterations: 16
currently lose_sum: 98.67690765857697
time_elpased: 3.447
batch start
#iterations: 17
currently lose_sum: 98.55001664161682
time_elpased: 3.836
batch start
#iterations: 18
currently lose_sum: 98.3908611536026
time_elpased: 3.783
batch start
#iterations: 19
currently lose_sum: 98.14337408542633
time_elpased: 3.413
start validation test
0.518505154639
0.533849477854
0.305135329834
0.38831772641
0.518879758277
66.481
batch start
#iterations: 20
currently lose_sum: 98.36078333854675
time_elpased: 3.442
batch start
#iterations: 21
currently lose_sum: 98.16372847557068
time_elpased: 3.947
batch start
#iterations: 22
currently lose_sum: 97.89175552129745
time_elpased: 3.675
batch start
#iterations: 23
currently lose_sum: 97.96662473678589
time_elpased: 3.612
batch start
#iterations: 24
currently lose_sum: 97.5327297449112
time_elpased: 3.88
batch start
#iterations: 25
currently lose_sum: 97.43019187450409
time_elpased: 3.582
batch start
#iterations: 26
currently lose_sum: 97.75265753269196
time_elpased: 3.625
batch start
#iterations: 27
currently lose_sum: 97.42916065454483
time_elpased: 3.928
batch start
#iterations: 28
currently lose_sum: 97.23389875888824
time_elpased: 3.437
batch start
#iterations: 29
currently lose_sum: 97.21681779623032
time_elpased: 3.616
batch start
#iterations: 30
currently lose_sum: 97.07412624359131
time_elpased: 3.622
batch start
#iterations: 31
currently lose_sum: 97.02392721176147
time_elpased: 3.751
batch start
#iterations: 32
currently lose_sum: 96.90483433008194
time_elpased: 3.464
batch start
#iterations: 33
currently lose_sum: 97.01720935106277
time_elpased: 3.544
batch start
#iterations: 34
currently lose_sum: 96.74993515014648
time_elpased: 3.565
batch start
#iterations: 35
currently lose_sum: 96.68722069263458
time_elpased: 3.605
batch start
#iterations: 36
currently lose_sum: 96.76266646385193
time_elpased: 3.489
batch start
#iterations: 37
currently lose_sum: 96.54695242643356
time_elpased: 3.787
batch start
#iterations: 38
currently lose_sum: 96.76560401916504
time_elpased: 3.122
batch start
#iterations: 39
currently lose_sum: 96.71355056762695
time_elpased: 3.497
start validation test
0.615103092784
0.58925737861
0.764330554698
0.665471977062
0.614841100957
63.415
batch start
#iterations: 40
currently lose_sum: 97.19034576416016
time_elpased: 3.775
batch start
#iterations: 41
currently lose_sum: 97.03232932090759
time_elpased: 3.703
batch start
#iterations: 42
currently lose_sum: 96.26699763536453
time_elpased: 3.845
batch start
#iterations: 43
currently lose_sum: 96.10026276111603
time_elpased: 3.464
batch start
#iterations: 44
currently lose_sum: 96.37599486112595
time_elpased: 3.527
batch start
#iterations: 45
currently lose_sum: 96.12929391860962
time_elpased: 3.636
batch start
#iterations: 46
currently lose_sum: 96.25133347511292
time_elpased: 3.447
batch start
#iterations: 47
currently lose_sum: 96.4334864616394
time_elpased: 3.616
batch start
#iterations: 48
currently lose_sum: 96.43511581420898
time_elpased: 3.718
batch start
#iterations: 49
currently lose_sum: 96.35396522283554
time_elpased: 3.741
batch start
#iterations: 50
currently lose_sum: 96.33408445119858
time_elpased: 3.711
batch start
#iterations: 51
currently lose_sum: 95.95052999258041
time_elpased: 3.791
batch start
#iterations: 52
currently lose_sum: 96.2314640879631
time_elpased: 3.518
batch start
#iterations: 53
currently lose_sum: 95.71960616111755
time_elpased: 3.406
batch start
#iterations: 54
currently lose_sum: 95.89813631772995
time_elpased: 3.614
batch start
#iterations: 55
currently lose_sum: 96.40149295330048
time_elpased: 3.78
batch start
#iterations: 56
currently lose_sum: 95.95409834384918
time_elpased: 3.772
batch start
#iterations: 57
currently lose_sum: 95.89582377672195
time_elpased: 3.872
batch start
#iterations: 58
currently lose_sum: 96.09264004230499
time_elpased: 3.709
batch start
#iterations: 59
currently lose_sum: 96.17004591226578
time_elpased: 3.579
start validation test
0.627783505155
0.607401032702
0.726355871154
0.661573792004
0.627610446162
62.394
batch start
#iterations: 60
currently lose_sum: 95.84996140003204
time_elpased: 3.702
batch start
#iterations: 61
currently lose_sum: 96.12391948699951
time_elpased: 3.608
batch start
#iterations: 62
currently lose_sum: 95.6445654630661
time_elpased: 3.783
batch start
#iterations: 63
currently lose_sum: 95.68485856056213
time_elpased: 4.035
batch start
#iterations: 64
currently lose_sum: 95.8311642408371
time_elpased: 3.791
batch start
#iterations: 65
currently lose_sum: 95.29079914093018
time_elpased: 3.555
batch start
#iterations: 66
currently lose_sum: 95.60129714012146
time_elpased: 3.665
batch start
#iterations: 67
currently lose_sum: 95.60720181465149
time_elpased: 3.275
batch start
#iterations: 68
currently lose_sum: 95.40945732593536
time_elpased: 3.467
batch start
#iterations: 69
currently lose_sum: 95.35945373773575
time_elpased: 3.212
batch start
#iterations: 70
currently lose_sum: 94.65534764528275
time_elpased: 3.624
batch start
#iterations: 71
currently lose_sum: 94.98338049650192
time_elpased: 3.846
batch start
#iterations: 72
currently lose_sum: 95.42992055416107
time_elpased: 3.674
batch start
#iterations: 73
currently lose_sum: 94.86960226297379
time_elpased: 3.507
batch start
#iterations: 74
currently lose_sum: 94.81375694274902
time_elpased: 3.837
batch start
#iterations: 75
currently lose_sum: 95.00077676773071
time_elpased: 3.812
batch start
#iterations: 76
currently lose_sum: 95.22405803203583
time_elpased: 3.76
batch start
#iterations: 77
currently lose_sum: 95.42115789651871
time_elpased: 3.666
batch start
#iterations: 78
currently lose_sum: 95.19204658269882
time_elpased: 3.838
batch start
#iterations: 79
currently lose_sum: 94.67778438329697
time_elpased: 4.195
start validation test
0.611391752577
0.610850977199
0.617577441597
0.61419579346
0.611380892646
62.970
batch start
#iterations: 80
currently lose_sum: 94.81070059537888
time_elpased: 3.603
batch start
#iterations: 81
currently lose_sum: 95.61507970094681
time_elpased: 3.949
batch start
#iterations: 82
currently lose_sum: 95.34498703479767
time_elpased: 3.85
batch start
#iterations: 83
currently lose_sum: 94.94143307209015
time_elpased: 3.816
batch start
#iterations: 84
currently lose_sum: 94.94475811719894
time_elpased: 3.53
batch start
#iterations: 85
currently lose_sum: 94.99722647666931
time_elpased: 3.758
batch start
#iterations: 86
currently lose_sum: 95.08733403682709
time_elpased: 3.949
batch start
#iterations: 87
currently lose_sum: 94.64715671539307
time_elpased: 3.496
batch start
#iterations: 88
currently lose_sum: 94.80075281858444
time_elpased: 3.661
batch start
#iterations: 89
currently lose_sum: 95.0946934223175
time_elpased: 3.295
batch start
#iterations: 90
currently lose_sum: 94.8213581442833
time_elpased: 3.624
batch start
#iterations: 91
currently lose_sum: 94.5625250339508
time_elpased: 3.539
batch start
#iterations: 92
currently lose_sum: 94.43832504749298
time_elpased: 3.914
batch start
#iterations: 93
currently lose_sum: 94.29049307107925
time_elpased: 3.653
batch start
#iterations: 94
currently lose_sum: 94.58373957872391
time_elpased: 3.269
batch start
#iterations: 95
currently lose_sum: 95.22210001945496
time_elpased: 3.657
batch start
#iterations: 96
currently lose_sum: 94.60186409950256
time_elpased: 3.739
batch start
#iterations: 97
currently lose_sum: 94.63766169548035
time_elpased: 3.71
batch start
#iterations: 98
currently lose_sum: 94.63911122083664
time_elpased: 3.381
batch start
#iterations: 99
currently lose_sum: 94.31934088468552
time_elpased: 3.791
start validation test
0.632577319588
0.617885438485
0.698260780076
0.655618900377
0.632462002142
61.985
batch start
#iterations: 100
currently lose_sum: 94.40906262397766
time_elpased: 3.665
batch start
#iterations: 101
currently lose_sum: 94.08157658576965
time_elpased: 3.685
batch start
#iterations: 102
currently lose_sum: 94.26392823457718
time_elpased: 3.744
batch start
#iterations: 103
currently lose_sum: 94.53044199943542
time_elpased: 3.861
batch start
#iterations: 104
currently lose_sum: 93.75020354986191
time_elpased: 3.506
batch start
#iterations: 105
currently lose_sum: 93.75208908319473
time_elpased: 3.299
batch start
#iterations: 106
currently lose_sum: 93.99954736232758
time_elpased: 3.646
batch start
#iterations: 107
currently lose_sum: 93.99719321727753
time_elpased: 3.583
batch start
#iterations: 108
currently lose_sum: 94.16723275184631
time_elpased: 3.664
batch start
#iterations: 109
currently lose_sum: 94.04238492250443
time_elpased: 3.542
batch start
#iterations: 110
currently lose_sum: 93.86812394857407
time_elpased: 3.626
batch start
#iterations: 111
currently lose_sum: 94.23272007703781
time_elpased: 3.712
batch start
#iterations: 112
currently lose_sum: 94.13134717941284
time_elpased: 3.473
batch start
#iterations: 113
currently lose_sum: 94.09985774755478
time_elpased: 3.836
batch start
#iterations: 114
currently lose_sum: 93.80247926712036
time_elpased: 3.732
batch start
#iterations: 115
currently lose_sum: 93.82559704780579
time_elpased: 3.748
batch start
#iterations: 116
currently lose_sum: 93.57896357774734
time_elpased: 3.641
batch start
#iterations: 117
currently lose_sum: 93.91127479076385
time_elpased: 3.564
batch start
#iterations: 118
currently lose_sum: 93.81583595275879
time_elpased: 3.603
batch start
#iterations: 119
currently lose_sum: 93.50110900402069
time_elpased: 3.954
start validation test
0.635927835052
0.648967220476
0.594936708861
0.62077852349
0.635999801296
61.525
batch start
#iterations: 120
currently lose_sum: 94.03022265434265
time_elpased: 3.832
batch start
#iterations: 121
currently lose_sum: 94.03524440526962
time_elpased: 3.847
batch start
#iterations: 122
currently lose_sum: 93.83560782670975
time_elpased: 3.679
batch start
#iterations: 123
currently lose_sum: 94.02527445554733
time_elpased: 3.443
batch start
#iterations: 124
currently lose_sum: 94.24657040834427
time_elpased: 3.896
batch start
#iterations: 125
currently lose_sum: 93.60615330934525
time_elpased: 3.217
batch start
#iterations: 126
currently lose_sum: 93.91231995820999
time_elpased: 3.516
batch start
#iterations: 127
currently lose_sum: 93.63833647966385
time_elpased: 3.244
batch start
#iterations: 128
currently lose_sum: 93.55521857738495
time_elpased: 3.706
batch start
#iterations: 129
currently lose_sum: 93.42054343223572
time_elpased: 3.771
batch start
#iterations: 130
currently lose_sum: 93.28183597326279
time_elpased: 3.491
batch start
#iterations: 131
currently lose_sum: 93.14469176530838
time_elpased: 3.64
batch start
#iterations: 132
currently lose_sum: 93.10215896368027
time_elpased: 3.651
batch start
#iterations: 133
currently lose_sum: 93.8249626159668
time_elpased: 3.732
batch start
#iterations: 134
currently lose_sum: 93.55063587427139
time_elpased: 3.538
batch start
#iterations: 135
currently lose_sum: 93.32147860527039
time_elpased: 3.59
batch start
#iterations: 136
currently lose_sum: 93.04746413230896
time_elpased: 3.51
batch start
#iterations: 137
currently lose_sum: 93.25328820943832
time_elpased: 3.468
batch start
#iterations: 138
currently lose_sum: 92.5965285897255
time_elpased: 3.587
batch start
#iterations: 139
currently lose_sum: 93.22605764865875
time_elpased: 3.962
start validation test
0.633298969072
0.626371492378
0.663888031285
0.644584332534
0.633245265255
61.844
batch start
#iterations: 140
currently lose_sum: 93.27839356660843
time_elpased: 3.606
batch start
#iterations: 141
currently lose_sum: 93.27960115671158
time_elpased: 3.791
batch start
#iterations: 142
currently lose_sum: 93.03031826019287
time_elpased: 3.763
batch start
#iterations: 143
currently lose_sum: 93.31827408075333
time_elpased: 3.716
batch start
#iterations: 144
currently lose_sum: 92.64943259954453
time_elpased: 3.68
batch start
#iterations: 145
currently lose_sum: 92.91020476818085
time_elpased: 3.949
batch start
#iterations: 146
currently lose_sum: 93.08435142040253
time_elpased: 3.6
batch start
#iterations: 147
currently lose_sum: 92.99429363012314
time_elpased: 3.779
batch start
#iterations: 148
currently lose_sum: 92.868743121624
time_elpased: 3.798
batch start
#iterations: 149
currently lose_sum: 93.07897156476974
time_elpased: 3.911
batch start
#iterations: 150
currently lose_sum: 92.46305453777313
time_elpased: 3.549
batch start
#iterations: 151
currently lose_sum: 92.7683818936348
time_elpased: 3.475
batch start
#iterations: 152
currently lose_sum: 93.23794603347778
time_elpased: 3.536
batch start
#iterations: 153
currently lose_sum: 92.4616791009903
time_elpased: 4.015
batch start
#iterations: 154
currently lose_sum: 92.92479193210602
time_elpased: 3.353
batch start
#iterations: 155
currently lose_sum: 93.0263221859932
time_elpased: 3.769
batch start
#iterations: 156
currently lose_sum: 92.0187639594078
time_elpased: 3.537
batch start
#iterations: 157
currently lose_sum: 92.84622120857239
time_elpased: 3.531
batch start
#iterations: 158
currently lose_sum: 92.96151238679886
time_elpased: 3.817
batch start
#iterations: 159
currently lose_sum: 92.39384055137634
time_elpased: 3.883
start validation test
0.636443298969
0.633013780707
0.652361840074
0.642542192489
0.636415351515
61.435
batch start
#iterations: 160
currently lose_sum: 92.59236824512482
time_elpased: 3.872
batch start
#iterations: 161
currently lose_sum: 92.30760109424591
time_elpased: 3.662
batch start
#iterations: 162
currently lose_sum: 92.65049183368683
time_elpased: 3.511
batch start
#iterations: 163
currently lose_sum: 91.98680067062378
time_elpased: 3.681
batch start
#iterations: 164
currently lose_sum: 92.37183851003647
time_elpased: 3.717
batch start
#iterations: 165
currently lose_sum: 92.1018762588501
time_elpased: 3.556
batch start
#iterations: 166
currently lose_sum: 92.23934787511826
time_elpased: 3.504
batch start
#iterations: 167
currently lose_sum: 92.34054177999496
time_elpased: 3.454
batch start
#iterations: 168
currently lose_sum: 91.84404021501541
time_elpased: 3.558
batch start
#iterations: 169
currently lose_sum: 91.59045368432999
time_elpased: 3.67
batch start
#iterations: 170
currently lose_sum: 92.00129526853561
time_elpased: 3.689
batch start
#iterations: 171
currently lose_sum: 91.83994817733765
time_elpased: 3.489
batch start
#iterations: 172
currently lose_sum: 91.60410499572754
time_elpased: 3.616
batch start
#iterations: 173
currently lose_sum: 91.33095896244049
time_elpased: 3.402
batch start
#iterations: 174
currently lose_sum: 92.16518574953079
time_elpased: 3.658
batch start
#iterations: 175
currently lose_sum: 92.26951766014099
time_elpased: 3.806
batch start
#iterations: 176
currently lose_sum: 92.06993389129639
time_elpased: 3.489
batch start
#iterations: 177
currently lose_sum: 91.22539764642715
time_elpased: 3.654
batch start
#iterations: 178
currently lose_sum: 91.6066260933876
time_elpased: 3.777
batch start
#iterations: 179
currently lose_sum: 91.53908795118332
time_elpased: 3.82
start validation test
0.624072164948
0.625154894672
0.623031799938
0.624091541673
0.62407399147
62.652
batch start
#iterations: 180
currently lose_sum: 91.76756536960602
time_elpased: 3.807
batch start
#iterations: 181
currently lose_sum: 90.87727665901184
time_elpased: 3.679
batch start
#iterations: 182
currently lose_sum: 91.60951465368271
time_elpased: 3.648
batch start
#iterations: 183
currently lose_sum: 90.97307962179184
time_elpased: 3.689
batch start
#iterations: 184
currently lose_sum: 91.32525193691254
time_elpased: 3.694
batch start
#iterations: 185
currently lose_sum: 90.94411671161652
time_elpased: 3.39
batch start
#iterations: 186
currently lose_sum: 91.12073051929474
time_elpased: 3.582
batch start
#iterations: 187
currently lose_sum: 91.08717370033264
time_elpased: 3.531
batch start
#iterations: 188
currently lose_sum: 91.06173640489578
time_elpased: 3.496
batch start
#iterations: 189
currently lose_sum: 91.18542850017548
time_elpased: 3.589
batch start
#iterations: 190
currently lose_sum: 90.72668051719666
time_elpased: 3.557
batch start
#iterations: 191
currently lose_sum: 90.11061263084412
time_elpased: 3.599
batch start
#iterations: 192
currently lose_sum: 90.35179167985916
time_elpased: 3.622
batch start
#iterations: 193
currently lose_sum: 90.75289636850357
time_elpased: 3.547
batch start
#iterations: 194
currently lose_sum: 90.53822910785675
time_elpased: 3.673
batch start
#iterations: 195
currently lose_sum: 90.86668235063553
time_elpased: 3.692
batch start
#iterations: 196
currently lose_sum: 90.57614326477051
time_elpased: 3.649
batch start
#iterations: 197
currently lose_sum: 91.2003121972084
time_elpased: 3.766
batch start
#iterations: 198
currently lose_sum: 90.60542941093445
time_elpased: 3.655
batch start
#iterations: 199
currently lose_sum: 90.75006049871445
time_elpased: 3.477
start validation test
0.615721649485
0.628028073353
0.570958114644
0.598134871436
0.615800238774
63.492
batch start
#iterations: 200
currently lose_sum: 89.91459912061691
time_elpased: 3.608
batch start
#iterations: 201
currently lose_sum: 90.20433992147446
time_elpased: 3.349
batch start
#iterations: 202
currently lose_sum: 89.81947833299637
time_elpased: 3.482
batch start
#iterations: 203
currently lose_sum: 90.354840695858
time_elpased: 3.616
batch start
#iterations: 204
currently lose_sum: 90.44064599275589
time_elpased: 3.503
batch start
#iterations: 205
currently lose_sum: 90.1712681055069
time_elpased: 3.782
batch start
#iterations: 206
currently lose_sum: 89.51302075386047
time_elpased: 3.557
batch start
#iterations: 207
currently lose_sum: 89.59697896242142
time_elpased: 3.523
batch start
#iterations: 208
currently lose_sum: 89.77101618051529
time_elpased: 4.021
batch start
#iterations: 209
currently lose_sum: 89.76118141412735
time_elpased: 3.963
batch start
#iterations: 210
currently lose_sum: 89.42290711402893
time_elpased: 3.969
batch start
#iterations: 211
currently lose_sum: 88.91682249307632
time_elpased: 3.985
batch start
#iterations: 212
currently lose_sum: 89.8937800526619
time_elpased: 4.0
batch start
#iterations: 213
currently lose_sum: 89.66665637493134
time_elpased: 3.552
batch start
#iterations: 214
currently lose_sum: 88.85928785800934
time_elpased: 3.374
batch start
#iterations: 215
currently lose_sum: 89.81065893173218
time_elpased: 3.789
batch start
#iterations: 216
currently lose_sum: 89.56650978326797
time_elpased: 3.727
batch start
#iterations: 217
currently lose_sum: 88.57633984088898
time_elpased: 3.714
batch start
#iterations: 218
currently lose_sum: 89.55875569581985
time_elpased: 3.698
batch start
#iterations: 219
currently lose_sum: 88.57456296682358
time_elpased: 3.477
start validation test
0.613298969072
0.622741881857
0.578264896573
0.599679829242
0.61336047679
63.806
batch start
#iterations: 220
currently lose_sum: 89.0689103603363
time_elpased: 3.397
batch start
#iterations: 221
currently lose_sum: 88.8875042796135
time_elpased: 3.793
batch start
#iterations: 222
currently lose_sum: 88.2084613442421
time_elpased: 3.31
batch start
#iterations: 223
currently lose_sum: 88.55796986818314
time_elpased: 3.552
batch start
#iterations: 224
currently lose_sum: 88.5689976811409
time_elpased: 3.686
batch start
#iterations: 225
currently lose_sum: 89.04389292001724
time_elpased: 3.594
batch start
#iterations: 226
currently lose_sum: 88.21947038173676
time_elpased: 3.469
batch start
#iterations: 227
currently lose_sum: 88.3996210694313
time_elpased: 3.514
batch start
#iterations: 228
currently lose_sum: 88.10720551013947
time_elpased: 3.692
batch start
#iterations: 229
currently lose_sum: 87.51627397537231
time_elpased: 3.431
batch start
#iterations: 230
currently lose_sum: 87.3920294046402
time_elpased: 3.915
batch start
#iterations: 231
currently lose_sum: 88.09517675638199
time_elpased: 3.723
batch start
#iterations: 232
currently lose_sum: 87.47293490171432
time_elpased: 3.614
batch start
#iterations: 233
currently lose_sum: 87.7699403166771
time_elpased: 3.695
batch start
#iterations: 234
currently lose_sum: 87.59111315011978
time_elpased: 3.475
batch start
#iterations: 235
currently lose_sum: 86.62377750873566
time_elpased: 3.841
batch start
#iterations: 236
currently lose_sum: 87.6994856595993
time_elpased: 3.711
batch start
#iterations: 237
currently lose_sum: 87.01773834228516
time_elpased: 3.71
batch start
#iterations: 238
currently lose_sum: 87.85245394706726
time_elpased: 3.675
batch start
#iterations: 239
currently lose_sum: 86.92913007736206
time_elpased: 3.766
start validation test
0.609329896907
0.626449018216
0.545024184419
0.582906829564
0.609442795504
64.715
batch start
#iterations: 240
currently lose_sum: 87.12664288282394
time_elpased: 3.774
batch start
#iterations: 241
currently lose_sum: 86.90373587608337
time_elpased: 3.809
batch start
#iterations: 242
currently lose_sum: 86.31794583797455
time_elpased: 3.703
batch start
#iterations: 243
currently lose_sum: 86.9229605793953
time_elpased: 3.616
batch start
#iterations: 244
currently lose_sum: 86.38126534223557
time_elpased: 3.776
batch start
#iterations: 245
currently lose_sum: 86.4543052315712
time_elpased: 3.886
batch start
#iterations: 246
currently lose_sum: 86.47524493932724
time_elpased: 3.707
batch start
#iterations: 247
currently lose_sum: 86.57305270433426
time_elpased: 3.61
batch start
#iterations: 248
currently lose_sum: 86.71823996305466
time_elpased: 3.651
batch start
#iterations: 249
currently lose_sum: 85.43718892335892
time_elpased: 3.633
batch start
#iterations: 250
currently lose_sum: 86.0282968878746
time_elpased: 3.621
batch start
#iterations: 251
currently lose_sum: 85.22516191005707
time_elpased: 3.832
batch start
#iterations: 252
currently lose_sum: 85.57674962282181
time_elpased: 3.752
batch start
#iterations: 253
currently lose_sum: 85.35923248529434
time_elpased: 3.581
batch start
#iterations: 254
currently lose_sum: 85.38531368970871
time_elpased: 3.223
batch start
#iterations: 255
currently lose_sum: 85.46184027194977
time_elpased: 3.529
batch start
#iterations: 256
currently lose_sum: 85.08091151714325
time_elpased: 3.743
batch start
#iterations: 257
currently lose_sum: 85.52870118618011
time_elpased: 3.682
batch start
#iterations: 258
currently lose_sum: 84.70394176244736
time_elpased: 3.662
batch start
#iterations: 259
currently lose_sum: 84.68136936426163
time_elpased: 3.586
start validation test
0.595979381443
0.615573871325
0.514973757333
0.560797937913
0.596121599311
67.571
batch start
#iterations: 260
currently lose_sum: 84.79503858089447
time_elpased: 3.415
batch start
#iterations: 261
currently lose_sum: 83.99088853597641
time_elpased: 3.53
batch start
#iterations: 262
currently lose_sum: 83.99056875705719
time_elpased: 3.793
batch start
#iterations: 263
currently lose_sum: 84.42072743177414
time_elpased: 3.601
batch start
#iterations: 264
currently lose_sum: 84.54712706804276
time_elpased: 3.572
batch start
#iterations: 265
currently lose_sum: 83.3383828997612
time_elpased: 3.815
batch start
#iterations: 266
currently lose_sum: 84.01967513561249
time_elpased: 3.602
batch start
#iterations: 267
currently lose_sum: 84.61207699775696
time_elpased: 3.465
batch start
#iterations: 268
currently lose_sum: 83.40537542104721
time_elpased: 3.855
batch start
#iterations: 269
currently lose_sum: 82.93572998046875
time_elpased: 3.703
batch start
#iterations: 270
currently lose_sum: 83.21842694282532
time_elpased: 3.739
batch start
#iterations: 271
currently lose_sum: 83.3283699452877
time_elpased: 3.415
batch start
#iterations: 272
currently lose_sum: 82.83003932237625
time_elpased: 3.46
batch start
#iterations: 273
currently lose_sum: 82.82508119940758
time_elpased: 3.765
batch start
#iterations: 274
currently lose_sum: 82.96454128623009
time_elpased: 3.469
batch start
#iterations: 275
currently lose_sum: 82.22154068946838
time_elpased: 3.519
batch start
#iterations: 276
currently lose_sum: 82.3357282280922
time_elpased: 3.485
batch start
#iterations: 277
currently lose_sum: 82.19335347414017
time_elpased: 4.111
batch start
#iterations: 278
currently lose_sum: 81.99771988391876
time_elpased: 3.742
batch start
#iterations: 279
currently lose_sum: 81.91808694601059
time_elpased: 3.8
start validation test
0.587113402062
0.608106396453
0.494082535762
0.545196456961
0.587276732097
71.068
batch start
#iterations: 280
currently lose_sum: 80.73380249738693
time_elpased: 3.714
batch start
#iterations: 281
currently lose_sum: 81.7581499516964
time_elpased: 3.882
batch start
#iterations: 282
currently lose_sum: 81.57252508401871
time_elpased: 3.552
batch start
#iterations: 283
currently lose_sum: 81.14809069037437
time_elpased: 3.563
batch start
#iterations: 284
currently lose_sum: 81.05990147590637
time_elpased: 3.539
batch start
#iterations: 285
currently lose_sum: 81.38269928097725
time_elpased: 3.469
batch start
#iterations: 286
currently lose_sum: 80.80721321702003
time_elpased: 3.832
batch start
#iterations: 287
currently lose_sum: 80.50376036763191
time_elpased: 3.685
batch start
#iterations: 288
currently lose_sum: 80.08853653073311
time_elpased: 3.772
batch start
#iterations: 289
currently lose_sum: 79.3845106959343
time_elpased: 3.658
batch start
#iterations: 290
currently lose_sum: 80.19247734546661
time_elpased: 3.838
batch start
#iterations: 291
currently lose_sum: 79.21005353331566
time_elpased: 3.623
batch start
#iterations: 292
currently lose_sum: 79.48187020421028
time_elpased: 3.744
batch start
#iterations: 293
currently lose_sum: 79.16525310277939
time_elpased: 3.794
batch start
#iterations: 294
currently lose_sum: 79.28315410017967
time_elpased: 3.738
batch start
#iterations: 295
currently lose_sum: 79.47202098369598
time_elpased: 3.513
batch start
#iterations: 296
currently lose_sum: 79.28732207417488
time_elpased: 3.435
batch start
#iterations: 297
currently lose_sum: 78.10899862647057
time_elpased: 3.862
batch start
#iterations: 298
currently lose_sum: 78.56342333555222
time_elpased: 3.428
batch start
#iterations: 299
currently lose_sum: 79.09987345337868
time_elpased: 3.378
start validation test
0.586958762887
0.6144853534
0.470618503653
0.533014744449
0.587163016156
75.314
batch start
#iterations: 300
currently lose_sum: 78.40646308660507
time_elpased: 3.46
batch start
#iterations: 301
currently lose_sum: 78.2360218167305
time_elpased: 3.459
batch start
#iterations: 302
currently lose_sum: 77.7809086740017
time_elpased: 3.721
batch start
#iterations: 303
currently lose_sum: 78.23177641630173
time_elpased: 3.654
batch start
#iterations: 304
currently lose_sum: 78.23568767309189
time_elpased: 3.802
batch start
#iterations: 305
currently lose_sum: 77.43626582622528
time_elpased: 3.705
batch start
#iterations: 306
currently lose_sum: 77.55806344747543
time_elpased: 3.612
batch start
#iterations: 307
currently lose_sum: 77.5636594593525
time_elpased: 3.813
batch start
#iterations: 308
currently lose_sum: 76.63382223248482
time_elpased: 3.419
batch start
#iterations: 309
currently lose_sum: 77.29535600543022
time_elpased: 3.397
batch start
#iterations: 310
currently lose_sum: 76.70012983679771
time_elpased: 3.493
batch start
#iterations: 311
currently lose_sum: 75.92774590849876
time_elpased: 3.437
batch start
#iterations: 312
currently lose_sum: 76.00794556736946
time_elpased: 3.562
batch start
#iterations: 313
currently lose_sum: 75.70950889587402
time_elpased: 3.736
batch start
#iterations: 314
currently lose_sum: 76.28734016418457
time_elpased: 3.448
batch start
#iterations: 315
currently lose_sum: 75.8576867878437
time_elpased: 3.467
batch start
#iterations: 316
currently lose_sum: 75.37965422868729
time_elpased: 3.607
batch start
#iterations: 317
currently lose_sum: 75.29553472995758
time_elpased: 3.34
batch start
#iterations: 318
currently lose_sum: 74.95065009593964
time_elpased: 3.844
batch start
#iterations: 319
currently lose_sum: 74.48535931110382
time_elpased: 3.733
start validation test
0.578608247423
0.607142857143
0.449624369661
0.516644001656
0.578834698514
76.768
batch start
#iterations: 320
currently lose_sum: 75.51392069458961
time_elpased: 3.341
batch start
#iterations: 321
currently lose_sum: 74.52984994649887
time_elpased: 3.523
batch start
#iterations: 322
currently lose_sum: 74.36337128281593
time_elpased: 3.451
batch start
#iterations: 323
currently lose_sum: 74.06539490818977
time_elpased: 3.671
batch start
#iterations: 324
currently lose_sum: 73.81902316212654
time_elpased: 3.754
batch start
#iterations: 325
currently lose_sum: 73.40909379720688
time_elpased: 3.568
batch start
#iterations: 326
currently lose_sum: 73.31568309664726
time_elpased: 3.565
batch start
#iterations: 327
currently lose_sum: 74.03888335824013
time_elpased: 3.544
batch start
#iterations: 328
currently lose_sum: 73.08335739374161
time_elpased: 3.52
batch start
#iterations: 329
currently lose_sum: 72.6581406891346
time_elpased: 3.609
batch start
#iterations: 330
currently lose_sum: 72.54002457857132
time_elpased: 3.387
batch start
#iterations: 331
currently lose_sum: 72.51328748464584
time_elpased: 3.494
batch start
#iterations: 332
currently lose_sum: 72.11717388033867
time_elpased: 3.389
batch start
#iterations: 333
currently lose_sum: 72.67939072847366
time_elpased: 3.445
batch start
#iterations: 334
currently lose_sum: 72.11588788032532
time_elpased: 3.653
batch start
#iterations: 335
currently lose_sum: 72.39897593855858
time_elpased: 3.611
batch start
#iterations: 336
currently lose_sum: 71.40924835205078
time_elpased: 3.76
batch start
#iterations: 337
currently lose_sum: 71.93747851252556
time_elpased: 3.631
batch start
#iterations: 338
currently lose_sum: 71.50966197252274
time_elpased: 3.885
batch start
#iterations: 339
currently lose_sum: 71.2564534842968
time_elpased: 3.657
start validation test
0.563711340206
0.5979059228
0.393742924771
0.474807644577
0.564009745975
84.078
batch start
#iterations: 340
currently lose_sum: 71.48935851454735
time_elpased: 3.795
batch start
#iterations: 341
currently lose_sum: 71.29874929785728
time_elpased: 3.478
batch start
#iterations: 342
currently lose_sum: 70.888552993536
time_elpased: 3.768
batch start
#iterations: 343
currently lose_sum: 69.99226772785187
time_elpased: 3.951
batch start
#iterations: 344
currently lose_sum: 69.59291881322861
time_elpased: 3.653
batch start
#iterations: 345
currently lose_sum: 70.56492355465889
time_elpased: 3.908
batch start
#iterations: 346
currently lose_sum: 69.1367794573307
time_elpased: 3.752
batch start
#iterations: 347
currently lose_sum: 69.09280648827553
time_elpased: 3.573
batch start
#iterations: 348
currently lose_sum: 69.32149574160576
time_elpased: 3.526
batch start
#iterations: 349
currently lose_sum: 69.23228114843369
time_elpased: 3.563
batch start
#iterations: 350
currently lose_sum: 68.13670155405998
time_elpased: 3.886
batch start
#iterations: 351
currently lose_sum: 69.36436858773232
time_elpased: 3.707
batch start
#iterations: 352
currently lose_sum: 68.55165955424309
time_elpased: 3.706
batch start
#iterations: 353
currently lose_sum: 69.41387239098549
time_elpased: 3.689
batch start
#iterations: 354
currently lose_sum: 67.79595828056335
time_elpased: 3.702
batch start
#iterations: 355
currently lose_sum: 67.66330298781395
time_elpased: 3.689
batch start
#iterations: 356
currently lose_sum: 67.92490330338478
time_elpased: 3.794
batch start
#iterations: 357
currently lose_sum: 67.49572631716728
time_elpased: 3.402
batch start
#iterations: 358
currently lose_sum: 67.70233407616615
time_elpased: 3.561
batch start
#iterations: 359
currently lose_sum: 67.36085671186447
time_elpased: 3.584
start validation test
0.561391752577
0.610339788089
0.343830400329
0.43986570996
0.561773715088
92.695
batch start
#iterations: 360
currently lose_sum: 67.41315934062004
time_elpased: 3.749
batch start
#iterations: 361
currently lose_sum: 67.46741107106209
time_elpased: 3.686
batch start
#iterations: 362
currently lose_sum: 67.27974891662598
time_elpased: 3.599
batch start
#iterations: 363
currently lose_sum: 66.23357963562012
time_elpased: 3.493
batch start
#iterations: 364
currently lose_sum: 65.7928039431572
time_elpased: 3.671
batch start
#iterations: 365
currently lose_sum: 65.58523461222649
time_elpased: 3.477
batch start
#iterations: 366
currently lose_sum: 65.73573273420334
time_elpased: 3.658
batch start
#iterations: 367
currently lose_sum: 65.88358110189438
time_elpased: 3.502
batch start
#iterations: 368
currently lose_sum: 65.26490858197212
time_elpased: 3.787
batch start
#iterations: 369
currently lose_sum: 65.06733137369156
time_elpased: 3.466
batch start
#iterations: 370
currently lose_sum: 65.42702168226242
time_elpased: 3.669
batch start
#iterations: 371
currently lose_sum: 65.67025113105774
time_elpased: 3.636
batch start
#iterations: 372
currently lose_sum: 64.9406188428402
time_elpased: 3.555
batch start
#iterations: 373
currently lose_sum: 64.3030863404274
time_elpased: 3.708
batch start
#iterations: 374
currently lose_sum: 64.14062720537186
time_elpased: 3.676
batch start
#iterations: 375
currently lose_sum: 64.44687330722809
time_elpased: 3.752
batch start
#iterations: 376
currently lose_sum: 64.62743729352951
time_elpased: 3.385
batch start
#iterations: 377
currently lose_sum: 63.98374444246292
time_elpased: 3.444
batch start
#iterations: 378
currently lose_sum: 64.33911752700806
time_elpased: 3.694
batch start
#iterations: 379
currently lose_sum: 63.84267094731331
time_elpased: 3.581
start validation test
0.558608247423
0.601405975395
0.352166306473
0.444213669111
0.558970688091
97.702
batch start
#iterations: 380
currently lose_sum: 62.4766808450222
time_elpased: 3.537
batch start
#iterations: 381
currently lose_sum: 63.39548963308334
time_elpased: 3.56
batch start
#iterations: 382
currently lose_sum: 62.04100784659386
time_elpased: 3.669
batch start
#iterations: 383
currently lose_sum: 62.189639538526535
time_elpased: 3.445
batch start
#iterations: 384
currently lose_sum: 63.241758823394775
time_elpased: 3.827
batch start
#iterations: 385
currently lose_sum: 62.11355149745941
time_elpased: 3.791
batch start
#iterations: 386
currently lose_sum: 61.72936126589775
time_elpased: 3.841
batch start
#iterations: 387
currently lose_sum: 62.044576823711395
time_elpased: 3.663
batch start
#iterations: 388
currently lose_sum: 62.11947920918465
time_elpased: 3.861
batch start
#iterations: 389
currently lose_sum: 61.10190525650978
time_elpased: 3.613
batch start
#iterations: 390
currently lose_sum: 61.602419912815094
time_elpased: 3.939
batch start
#iterations: 391
currently lose_sum: 61.300101190805435
time_elpased: 3.575
batch start
#iterations: 392
currently lose_sum: 61.2692469060421
time_elpased: 3.677
batch start
#iterations: 393
currently lose_sum: 60.687375009059906
time_elpased: 3.543
batch start
#iterations: 394
currently lose_sum: 60.790611773729324
time_elpased: 3.681
batch start
#iterations: 395
currently lose_sum: 60.16116946935654
time_elpased: 3.593
batch start
#iterations: 396
currently lose_sum: 60.32615610957146
time_elpased: 3.637
batch start
#iterations: 397
currently lose_sum: 60.29884859919548
time_elpased: 3.789
batch start
#iterations: 398
currently lose_sum: 60.188647985458374
time_elpased: 3.6
batch start
#iterations: 399
currently lose_sum: 60.08561038970947
time_elpased: 3.414
start validation test
0.543092783505
0.586493611843
0.297622723063
0.394866193337
0.543523744057
106.697
acc: 0.636
pre: 0.634
rec: 0.650
F1: 0.641
auc: 0.691
