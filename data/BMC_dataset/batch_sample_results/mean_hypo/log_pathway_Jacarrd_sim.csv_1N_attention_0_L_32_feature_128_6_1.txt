start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.53173434734344
time_elpased: 3.889
batch start
#iterations: 1
currently lose_sum: 100.43239384889603
time_elpased: 4.551
batch start
#iterations: 2
currently lose_sum: 100.32966470718384
time_elpased: 4.46
batch start
#iterations: 3
currently lose_sum: 100.35014641284943
time_elpased: 4.331
batch start
#iterations: 4
currently lose_sum: 100.10780119895935
time_elpased: 4.236
batch start
#iterations: 5
currently lose_sum: 100.18064326047897
time_elpased: 4.362
batch start
#iterations: 6
currently lose_sum: 99.96536272764206
time_elpased: 4.318
batch start
#iterations: 7
currently lose_sum: 99.80302917957306
time_elpased: 4.721
batch start
#iterations: 8
currently lose_sum: 99.60363572835922
time_elpased: 4.46
batch start
#iterations: 9
currently lose_sum: 99.32233184576035
time_elpased: 4.404
batch start
#iterations: 10
currently lose_sum: 99.43403768539429
time_elpased: 4.255
batch start
#iterations: 11
currently lose_sum: 99.2625025510788
time_elpased: 3.657
batch start
#iterations: 12
currently lose_sum: 99.27231580018997
time_elpased: 4.248
batch start
#iterations: 13
currently lose_sum: 99.04009062051773
time_elpased: 4.359
batch start
#iterations: 14
currently lose_sum: 99.0304925441742
time_elpased: 4.025
batch start
#iterations: 15
currently lose_sum: 99.09633177518845
time_elpased: 4.343
batch start
#iterations: 16
currently lose_sum: 98.75161826610565
time_elpased: 4.342
batch start
#iterations: 17
currently lose_sum: 98.8607349395752
time_elpased: 3.737
batch start
#iterations: 18
currently lose_sum: 98.59828191995621
time_elpased: 3.692
batch start
#iterations: 19
currently lose_sum: 98.69636577367783
time_elpased: 4.091
start validation test
0.615567010309
0.589267367423
0.767315014922
0.666607063031
0.615300593282
64.088
batch start
#iterations: 20
currently lose_sum: 98.46276867389679
time_elpased: 4.561
batch start
#iterations: 21
currently lose_sum: 98.43564945459366
time_elpased: 4.572
batch start
#iterations: 22
currently lose_sum: 98.16298460960388
time_elpased: 4.057
batch start
#iterations: 23
currently lose_sum: 98.20961630344391
time_elpased: 4.448
batch start
#iterations: 24
currently lose_sum: 98.01701438426971
time_elpased: 3.946
batch start
#iterations: 25
currently lose_sum: 98.26995968818665
time_elpased: 4.14
batch start
#iterations: 26
currently lose_sum: 98.1877030134201
time_elpased: 4.488
batch start
#iterations: 27
currently lose_sum: 98.24567133188248
time_elpased: 3.904
batch start
#iterations: 28
currently lose_sum: 97.84653979539871
time_elpased: 4.265
batch start
#iterations: 29
currently lose_sum: 97.97879803180695
time_elpased: 4.096
batch start
#iterations: 30
currently lose_sum: 97.79525607824326
time_elpased: 4.041
batch start
#iterations: 31
currently lose_sum: 97.93006438016891
time_elpased: 4.507
batch start
#iterations: 32
currently lose_sum: 97.65308767557144
time_elpased: 4.363
batch start
#iterations: 33
currently lose_sum: 97.53512793779373
time_elpased: 4.291
batch start
#iterations: 34
currently lose_sum: 97.66062796115875
time_elpased: 4.287
batch start
#iterations: 35
currently lose_sum: 97.50649011135101
time_elpased: 4.12
batch start
#iterations: 36
currently lose_sum: 97.13940769433975
time_elpased: 3.964
batch start
#iterations: 37
currently lose_sum: 97.31845170259476
time_elpased: 4.57
batch start
#iterations: 38
currently lose_sum: 97.34595108032227
time_elpased: 4.342
batch start
#iterations: 39
currently lose_sum: 97.20977592468262
time_elpased: 4.634
start validation test
0.614278350515
0.576141785958
0.869815786765
0.693156189773
0.613829715132
63.887
batch start
#iterations: 40
currently lose_sum: 97.24292695522308
time_elpased: 3.886
batch start
#iterations: 41
currently lose_sum: 97.20468562841415
time_elpased: 4.314
batch start
#iterations: 42
currently lose_sum: 96.77017217874527
time_elpased: 4.663
batch start
#iterations: 43
currently lose_sum: 97.05472177267075
time_elpased: 4.144
batch start
#iterations: 44
currently lose_sum: 96.84863585233688
time_elpased: 3.984
batch start
#iterations: 45
currently lose_sum: 96.603708922863
time_elpased: 4.243
batch start
#iterations: 46
currently lose_sum: 96.95446944236755
time_elpased: 4.674
batch start
#iterations: 47
currently lose_sum: 97.07042562961578
time_elpased: 3.781
batch start
#iterations: 48
currently lose_sum: 97.09252333641052
time_elpased: 3.909
batch start
#iterations: 49
currently lose_sum: 96.75742465257645
time_elpased: 3.928
batch start
#iterations: 50
currently lose_sum: 96.74459254741669
time_elpased: 3.92
batch start
#iterations: 51
currently lose_sum: 97.02929317951202
time_elpased: 5.058
batch start
#iterations: 52
currently lose_sum: 96.90371596813202
time_elpased: 3.762
batch start
#iterations: 53
currently lose_sum: 96.66956186294556
time_elpased: 4.229
batch start
#iterations: 54
currently lose_sum: 96.51783883571625
time_elpased: 4.184
batch start
#iterations: 55
currently lose_sum: 96.5159295797348
time_elpased: 4.137
batch start
#iterations: 56
currently lose_sum: 96.27019155025482
time_elpased: 4.348
batch start
#iterations: 57
currently lose_sum: 96.5795629620552
time_elpased: 4.228
batch start
#iterations: 58
currently lose_sum: 96.06640219688416
time_elpased: 4.166
batch start
#iterations: 59
currently lose_sum: 96.77114844322205
time_elpased: 4.124
start validation test
0.644639175258
0.659979598776
0.599259030565
0.628155339806
0.644718847101
62.201
batch start
#iterations: 60
currently lose_sum: 96.32227128744125
time_elpased: 3.976
batch start
#iterations: 61
currently lose_sum: 96.58493822813034
time_elpased: 4.216
batch start
#iterations: 62
currently lose_sum: 96.2402720451355
time_elpased: 4.093
batch start
#iterations: 63
currently lose_sum: 96.0640172958374
time_elpased: 4.165
batch start
#iterations: 64
currently lose_sum: 96.28231221437454
time_elpased: 4.404
batch start
#iterations: 65
currently lose_sum: 96.03529858589172
time_elpased: 4.097
batch start
#iterations: 66
currently lose_sum: 96.60855036973953
time_elpased: 4.128
batch start
#iterations: 67
currently lose_sum: 96.0192061662674
time_elpased: 4.622
batch start
#iterations: 68
currently lose_sum: 95.86273884773254
time_elpased: 4.34
batch start
#iterations: 69
currently lose_sum: 95.8622236251831
time_elpased: 4.28
batch start
#iterations: 70
currently lose_sum: 95.68271142244339
time_elpased: 4.188
batch start
#iterations: 71
currently lose_sum: 95.71349388360977
time_elpased: 4.226
batch start
#iterations: 72
currently lose_sum: 95.82424980401993
time_elpased: 4.371
batch start
#iterations: 73
currently lose_sum: 95.6600650548935
time_elpased: 4.716
batch start
#iterations: 74
currently lose_sum: 96.09399151802063
time_elpased: 4.349
batch start
#iterations: 75
currently lose_sum: 96.13051718473434
time_elpased: 4.231
batch start
#iterations: 76
currently lose_sum: 96.21247506141663
time_elpased: 4.243
batch start
#iterations: 77
currently lose_sum: 95.68409699201584
time_elpased: 4.558
batch start
#iterations: 78
currently lose_sum: 95.98979234695435
time_elpased: 4.549
batch start
#iterations: 79
currently lose_sum: 95.89722210168839
time_elpased: 4.559
start validation test
0.647216494845
0.645409454398
0.656169599671
0.65074505001
0.647200776289
61.363
batch start
#iterations: 80
currently lose_sum: 95.98463428020477
time_elpased: 3.838
batch start
#iterations: 81
currently lose_sum: 95.85147082805634
time_elpased: 4.015
batch start
#iterations: 82
currently lose_sum: 96.03296309709549
time_elpased: 3.919
batch start
#iterations: 83
currently lose_sum: 95.65457618236542
time_elpased: 4.188
batch start
#iterations: 84
currently lose_sum: 95.74238312244415
time_elpased: 3.839
batch start
#iterations: 85
currently lose_sum: 95.80906796455383
time_elpased: 4.344
batch start
#iterations: 86
currently lose_sum: 95.85243916511536
time_elpased: 4.204
batch start
#iterations: 87
currently lose_sum: 95.50845807790756
time_elpased: 4.056
batch start
#iterations: 88
currently lose_sum: 95.71526062488556
time_elpased: 4.089
batch start
#iterations: 89
currently lose_sum: 95.67476511001587
time_elpased: 4.109
batch start
#iterations: 90
currently lose_sum: 94.80018728971481
time_elpased: 3.731
batch start
#iterations: 91
currently lose_sum: 95.39964818954468
time_elpased: 4.385
batch start
#iterations: 92
currently lose_sum: 95.26921105384827
time_elpased: 4.514
batch start
#iterations: 93
currently lose_sum: 95.54628133773804
time_elpased: 3.616
batch start
#iterations: 94
currently lose_sum: 95.4061861038208
time_elpased: 4.244
batch start
#iterations: 95
currently lose_sum: 95.45359355211258
time_elpased: 4.182
batch start
#iterations: 96
currently lose_sum: 95.43304735422134
time_elpased: 4.214
batch start
#iterations: 97
currently lose_sum: 95.14838492870331
time_elpased: 4.4
batch start
#iterations: 98
currently lose_sum: 94.70555508136749
time_elpased: 4.487
batch start
#iterations: 99
currently lose_sum: 95.07460308074951
time_elpased: 4.394
start validation test
0.641030927835
0.656544978961
0.594113409489
0.62377093463
0.641113298775
61.611
batch start
#iterations: 100
currently lose_sum: 95.20631563663483
time_elpased: 4.302
batch start
#iterations: 101
currently lose_sum: 95.41207259893417
time_elpased: 4.111
batch start
#iterations: 102
currently lose_sum: 94.927350461483
time_elpased: 4.524
batch start
#iterations: 103
currently lose_sum: 95.33527839183807
time_elpased: 4.317
batch start
#iterations: 104
currently lose_sum: 95.16603416204453
time_elpased: 4.197
batch start
#iterations: 105
currently lose_sum: 95.19106143712997
time_elpased: 4.175
batch start
#iterations: 106
currently lose_sum: 95.00358355045319
time_elpased: 4.187
batch start
#iterations: 107
currently lose_sum: 95.12632429599762
time_elpased: 4.137
batch start
#iterations: 108
currently lose_sum: 94.44549310207367
time_elpased: 4.443
batch start
#iterations: 109
currently lose_sum: 94.65126085281372
time_elpased: 4.16
batch start
#iterations: 110
currently lose_sum: 94.50709581375122
time_elpased: 4.25
batch start
#iterations: 111
currently lose_sum: 95.13964587450027
time_elpased: 4.334
batch start
#iterations: 112
currently lose_sum: 94.96780860424042
time_elpased: 4.276
batch start
#iterations: 113
currently lose_sum: 94.73187124729156
time_elpased: 4.244
batch start
#iterations: 114
currently lose_sum: 94.74187082052231
time_elpased: 4.076
batch start
#iterations: 115
currently lose_sum: 94.206159055233
time_elpased: 4.099
batch start
#iterations: 116
currently lose_sum: 94.83634757995605
time_elpased: 3.851
batch start
#iterations: 117
currently lose_sum: 94.68019771575928
time_elpased: 4.183
batch start
#iterations: 118
currently lose_sum: 94.6446151137352
time_elpased: 4.495
batch start
#iterations: 119
currently lose_sum: 94.3346027135849
time_elpased: 4.45
start validation test
0.613865979381
0.616325250836
0.606874549758
0.611563391237
0.613878253915
62.643
batch start
#iterations: 120
currently lose_sum: 94.65356642007828
time_elpased: 4.128
batch start
#iterations: 121
currently lose_sum: 94.3907972574234
time_elpased: 3.934
batch start
#iterations: 122
currently lose_sum: 94.54816007614136
time_elpased: 4.168
batch start
#iterations: 123
currently lose_sum: 94.42994403839111
time_elpased: 3.566
batch start
#iterations: 124
currently lose_sum: 94.72746866941452
time_elpased: 4.106
batch start
#iterations: 125
currently lose_sum: 94.70855170488358
time_elpased: 4.062
batch start
#iterations: 126
currently lose_sum: 94.43223160505295
time_elpased: 4.151
batch start
#iterations: 127
currently lose_sum: 94.20155948400497
time_elpased: 4.671
batch start
#iterations: 128
currently lose_sum: 94.6477724313736
time_elpased: 4.112
batch start
#iterations: 129
currently lose_sum: 93.95935207605362
time_elpased: 3.925
batch start
#iterations: 130
currently lose_sum: 94.54704576730728
time_elpased: 3.878
batch start
#iterations: 131
currently lose_sum: 93.94504499435425
time_elpased: 4.42
batch start
#iterations: 132
currently lose_sum: 94.47445595264435
time_elpased: 4.459
batch start
#iterations: 133
currently lose_sum: 93.91622132062912
time_elpased: 4.618
batch start
#iterations: 134
currently lose_sum: 94.47565913200378
time_elpased: 4.198
batch start
#iterations: 135
currently lose_sum: 93.89935034513474
time_elpased: 3.712
batch start
#iterations: 136
currently lose_sum: 94.1208997964859
time_elpased: 3.367
batch start
#iterations: 137
currently lose_sum: 93.79825299978256
time_elpased: 4.221
batch start
#iterations: 138
currently lose_sum: 94.3791743516922
time_elpased: 4.279
batch start
#iterations: 139
currently lose_sum: 93.74816358089447
time_elpased: 4.517
start validation test
0.60412371134
0.602868397132
0.614284244108
0.608522785197
0.604105872958
63.615
batch start
#iterations: 140
currently lose_sum: 94.29706054925919
time_elpased: 4.25
batch start
#iterations: 141
currently lose_sum: 93.91378837823868
time_elpased: 4.15
batch start
#iterations: 142
currently lose_sum: 94.2494524717331
time_elpased: 4.068
batch start
#iterations: 143
currently lose_sum: 94.12652468681335
time_elpased: 4.244
batch start
#iterations: 144
currently lose_sum: 93.53634738922119
time_elpased: 4.077
batch start
#iterations: 145
currently lose_sum: 94.24497181177139
time_elpased: 4.087
batch start
#iterations: 146
currently lose_sum: 94.30539351701736
time_elpased: 4.112
batch start
#iterations: 147
currently lose_sum: 93.6394853591919
time_elpased: 4.257
batch start
#iterations: 148
currently lose_sum: 94.06538170576096
time_elpased: 4.696
batch start
#iterations: 149
currently lose_sum: 93.96952933073044
time_elpased: 4.348
batch start
#iterations: 150
currently lose_sum: 93.51710993051529
time_elpased: 3.891
batch start
#iterations: 151
currently lose_sum: 93.95004856586456
time_elpased: 3.615
batch start
#iterations: 152
currently lose_sum: 93.95956867933273
time_elpased: 4.179
batch start
#iterations: 153
currently lose_sum: 93.23361450433731
time_elpased: 4.068
batch start
#iterations: 154
currently lose_sum: 94.01991713047028
time_elpased: 4.7
batch start
#iterations: 155
currently lose_sum: 93.94831562042236
time_elpased: 4.113
batch start
#iterations: 156
currently lose_sum: 93.7975686788559
time_elpased: 4.569
batch start
#iterations: 157
currently lose_sum: 93.91161775588989
time_elpased: 4.117
batch start
#iterations: 158
currently lose_sum: 93.57479536533356
time_elpased: 4.526
batch start
#iterations: 159
currently lose_sum: 93.62183982133865
time_elpased: 4.271
start validation test
0.636082474227
0.6317564217
0.655552125142
0.643434343434
0.636048292252
61.700
batch start
#iterations: 160
currently lose_sum: 93.72524589300156
time_elpased: 4.103
batch start
#iterations: 161
currently lose_sum: 93.11836510896683
time_elpased: 4.152
batch start
#iterations: 162
currently lose_sum: 93.57304936647415
time_elpased: 4.159
batch start
#iterations: 163
currently lose_sum: 93.2671669125557
time_elpased: 4.282
batch start
#iterations: 164
currently lose_sum: 93.66119873523712
time_elpased: 4.429
batch start
#iterations: 165
currently lose_sum: 93.32617181539536
time_elpased: 4.134
batch start
#iterations: 166
currently lose_sum: 93.19687694311142
time_elpased: 4.103
batch start
#iterations: 167
currently lose_sum: 93.32358211278915
time_elpased: 4.226
batch start
#iterations: 168
currently lose_sum: 93.09946244955063
time_elpased: 4.138
batch start
#iterations: 169
currently lose_sum: 93.35760343074799
time_elpased: 4.072
batch start
#iterations: 170
currently lose_sum: 92.99244809150696
time_elpased: 4.263
batch start
#iterations: 171
currently lose_sum: 93.29298627376556
time_elpased: 4.099
batch start
#iterations: 172
currently lose_sum: 93.52799308300018
time_elpased: 4.141
batch start
#iterations: 173
currently lose_sum: 93.6645427942276
time_elpased: 4.218
batch start
#iterations: 174
currently lose_sum: 92.62618100643158
time_elpased: 4.021
batch start
#iterations: 175
currently lose_sum: 92.55906224250793
time_elpased: 4.341
batch start
#iterations: 176
currently lose_sum: 92.82491534948349
time_elpased: 4.246
batch start
#iterations: 177
currently lose_sum: 93.24465262889862
time_elpased: 4.197
batch start
#iterations: 178
currently lose_sum: 92.59125345945358
time_elpased: 4.281
batch start
#iterations: 179
currently lose_sum: 93.00881636142731
time_elpased: 3.778
start validation test
0.641134020619
0.641093926047
0.644128846352
0.642607802875
0.64112876274
61.431
batch start
#iterations: 180
currently lose_sum: 92.27052968740463
time_elpased: 4.552
batch start
#iterations: 181
currently lose_sum: 92.60395312309265
time_elpased: 4.217
batch start
#iterations: 182
currently lose_sum: 93.0149462223053
time_elpased: 3.868
batch start
#iterations: 183
currently lose_sum: 92.31271398067474
time_elpased: 3.738
batch start
#iterations: 184
currently lose_sum: 93.00487715005875
time_elpased: 4.291
batch start
#iterations: 185
currently lose_sum: 92.40865182876587
time_elpased: 4.359
batch start
#iterations: 186
currently lose_sum: 92.51380461454391
time_elpased: 4.416
batch start
#iterations: 187
currently lose_sum: 92.82608431577682
time_elpased: 4.519
batch start
#iterations: 188
currently lose_sum: 92.48865902423859
time_elpased: 4.063
batch start
#iterations: 189
currently lose_sum: 92.11986339092255
time_elpased: 4.074
batch start
#iterations: 190
currently lose_sum: 92.65174454450607
time_elpased: 4.48
batch start
#iterations: 191
currently lose_sum: 92.7037747502327
time_elpased: 4.746
batch start
#iterations: 192
currently lose_sum: 92.59684211015701
time_elpased: 4.342
batch start
#iterations: 193
currently lose_sum: 92.62168508768082
time_elpased: 4.485
batch start
#iterations: 194
currently lose_sum: 92.66676723957062
time_elpased: 4.558
batch start
#iterations: 195
currently lose_sum: 92.0921242237091
time_elpased: 4.181
batch start
#iterations: 196
currently lose_sum: 92.41038608551025
time_elpased: 4.103
batch start
#iterations: 197
currently lose_sum: 92.39884048700333
time_elpased: 4.426
batch start
#iterations: 198
currently lose_sum: 92.10441952943802
time_elpased: 4.068
batch start
#iterations: 199
currently lose_sum: 92.32611989974976
time_elpased: 4.446
start validation test
0.645463917526
0.63711001642
0.678810332407
0.657299451918
0.645405372751
61.252
batch start
#iterations: 200
currently lose_sum: 92.29780495166779
time_elpased: 3.789
batch start
#iterations: 201
currently lose_sum: 91.79871463775635
time_elpased: 4.351
batch start
#iterations: 202
currently lose_sum: 91.74640363454819
time_elpased: 3.716
batch start
#iterations: 203
currently lose_sum: 92.12894660234451
time_elpased: 4.249
batch start
#iterations: 204
currently lose_sum: 92.11577069759369
time_elpased: 4.213
batch start
#iterations: 205
currently lose_sum: 92.51206409931183
time_elpased: 4.349
batch start
#iterations: 206
currently lose_sum: 91.63739478588104
time_elpased: 4.362
batch start
#iterations: 207
currently lose_sum: 92.08907353878021
time_elpased: 4.347
batch start
#iterations: 208
currently lose_sum: 91.84833359718323
time_elpased: 4.277
batch start
#iterations: 209
currently lose_sum: 91.51228541135788
time_elpased: 4.191
batch start
#iterations: 210
currently lose_sum: 91.45138883590698
time_elpased: 4.482
batch start
#iterations: 211
currently lose_sum: 91.23811382055283
time_elpased: 4.42
batch start
#iterations: 212
currently lose_sum: 91.85414201021194
time_elpased: 4.132
batch start
#iterations: 213
currently lose_sum: 91.77573293447495
time_elpased: 4.115
batch start
#iterations: 214
currently lose_sum: 91.8270269036293
time_elpased: 4.282
batch start
#iterations: 215
currently lose_sum: 91.4026945233345
time_elpased: 4.558
batch start
#iterations: 216
currently lose_sum: 91.3291563987732
time_elpased: 4.372
batch start
#iterations: 217
currently lose_sum: 91.75618541240692
time_elpased: 4.267
batch start
#iterations: 218
currently lose_sum: 91.41237366199493
time_elpased: 3.8
batch start
#iterations: 219
currently lose_sum: 91.30642801523209
time_elpased: 4.245
start validation test
0.621649484536
0.624776902887
0.612431820521
0.618542771022
0.621665667567
62.725
batch start
#iterations: 220
currently lose_sum: 91.40871441364288
time_elpased: 4.104
batch start
#iterations: 221
currently lose_sum: 91.59923887252808
time_elpased: 4.457
batch start
#iterations: 222
currently lose_sum: 91.37202221155167
time_elpased: 4.378
batch start
#iterations: 223
currently lose_sum: 90.94340169429779
time_elpased: 4.556
batch start
#iterations: 224
currently lose_sum: 90.73929011821747
time_elpased: 4.004
batch start
#iterations: 225
currently lose_sum: 90.78278172016144
time_elpased: 4.618
batch start
#iterations: 226
currently lose_sum: 90.53343653678894
time_elpased: 3.985
batch start
#iterations: 227
currently lose_sum: 91.20704311132431
time_elpased: 3.59
batch start
#iterations: 228
currently lose_sum: 90.24466210603714
time_elpased: 4.324
batch start
#iterations: 229
currently lose_sum: 90.77661490440369
time_elpased: 4.088
batch start
#iterations: 230
currently lose_sum: 90.95477342605591
time_elpased: 4.539
batch start
#iterations: 231
currently lose_sum: 90.90220898389816
time_elpased: 4.192
batch start
#iterations: 232
currently lose_sum: 91.0562572479248
time_elpased: 4.084
batch start
#iterations: 233
currently lose_sum: 90.1649871468544
time_elpased: 4.136
batch start
#iterations: 234
currently lose_sum: 90.25473862886429
time_elpased: 4.309
batch start
#iterations: 235
currently lose_sum: 90.63194072246552
time_elpased: 4.71
batch start
#iterations: 236
currently lose_sum: 90.96040093898773
time_elpased: 3.688
batch start
#iterations: 237
currently lose_sum: 90.41340982913971
time_elpased: 3.528
batch start
#iterations: 238
currently lose_sum: 90.80945473909378
time_elpased: 4.204
batch start
#iterations: 239
currently lose_sum: 90.96071755886078
time_elpased: 4.06
start validation test
0.614175257732
0.61113324039
0.631573530925
0.621185282656
0.61414471238
63.267
batch start
#iterations: 240
currently lose_sum: 90.31129425764084
time_elpased: 4.409
batch start
#iterations: 241
currently lose_sum: 90.3091384768486
time_elpased: 4.194
batch start
#iterations: 242
currently lose_sum: 90.2849633693695
time_elpased: 4.238
batch start
#iterations: 243
currently lose_sum: 90.2301897406578
time_elpased: 4.252
batch start
#iterations: 244
currently lose_sum: 90.50586903095245
time_elpased: 4.312
batch start
#iterations: 245
currently lose_sum: 89.87260192632675
time_elpased: 4.646
batch start
#iterations: 246
currently lose_sum: 90.12836575508118
time_elpased: 4.343
batch start
#iterations: 247
currently lose_sum: 89.95600485801697
time_elpased: 4.154
batch start
#iterations: 248
currently lose_sum: 89.71447134017944
time_elpased: 4.302
batch start
#iterations: 249
currently lose_sum: 90.04863208532333
time_elpased: 3.823
batch start
#iterations: 250
currently lose_sum: 89.92766857147217
time_elpased: 3.752
batch start
#iterations: 251
currently lose_sum: 89.61788821220398
time_elpased: 4.386
batch start
#iterations: 252
currently lose_sum: 89.47252708673477
time_elpased: 3.907
batch start
#iterations: 253
currently lose_sum: 89.78488385677338
time_elpased: 4.338
batch start
#iterations: 254
currently lose_sum: 89.36094206571579
time_elpased: 4.441
batch start
#iterations: 255
currently lose_sum: 89.41994208097458
time_elpased: 4.042
batch start
#iterations: 256
currently lose_sum: 88.95282542705536
time_elpased: 4.228
batch start
#iterations: 257
currently lose_sum: 89.52041602134705
time_elpased: 4.393
batch start
#iterations: 258
currently lose_sum: 89.0047909617424
time_elpased: 4.033
batch start
#iterations: 259
currently lose_sum: 89.01305538415909
time_elpased: 4.201
start validation test
0.627216494845
0.625517729064
0.637233714109
0.63132137031
0.627198908072
63.907
batch start
#iterations: 260
currently lose_sum: 88.92436212301254
time_elpased: 4.495
batch start
#iterations: 261
currently lose_sum: 88.59010773897171
time_elpased: 4.09
batch start
#iterations: 262
currently lose_sum: 88.98729836940765
time_elpased: 4.076
batch start
#iterations: 263
currently lose_sum: 88.7609920501709
time_elpased: 4.302
batch start
#iterations: 264
currently lose_sum: 88.72679054737091
time_elpased: 4.31
batch start
#iterations: 265
currently lose_sum: 88.73088252544403
time_elpased: 4.506
batch start
#iterations: 266
currently lose_sum: 88.69158971309662
time_elpased: 4.39
batch start
#iterations: 267
currently lose_sum: 88.90552914142609
time_elpased: 4.22
batch start
#iterations: 268
currently lose_sum: 88.80062848329544
time_elpased: 4.165
batch start
#iterations: 269
currently lose_sum: 88.9173281788826
time_elpased: 4.372
batch start
#iterations: 270
currently lose_sum: 88.4572240114212
time_elpased: 4.321
batch start
#iterations: 271
currently lose_sum: 87.85633093118668
time_elpased: 3.592
batch start
#iterations: 272
currently lose_sum: 88.44703561067581
time_elpased: 3.888
batch start
#iterations: 273
currently lose_sum: 87.50196480751038
time_elpased: 3.731
batch start
#iterations: 274
currently lose_sum: 87.94546860456467
time_elpased: 3.983
batch start
#iterations: 275
currently lose_sum: 87.92533177137375
time_elpased: 4.43
batch start
#iterations: 276
currently lose_sum: 87.14162176847458
time_elpased: 4.374
batch start
#iterations: 277
currently lose_sum: 87.68359780311584
time_elpased: 4.144
batch start
#iterations: 278
currently lose_sum: 87.1920211315155
time_elpased: 4.192
batch start
#iterations: 279
currently lose_sum: 87.82711893320084
time_elpased: 4.24
start validation test
0.621958762887
0.625592916623
0.610785221776
0.618100395751
0.621978379761
64.834
batch start
#iterations: 280
currently lose_sum: 86.83357852697372
time_elpased: 4.217
batch start
#iterations: 281
currently lose_sum: 87.34100598096848
time_elpased: 4.434
batch start
#iterations: 282
currently lose_sum: 87.20072090625763
time_elpased: 4.151
batch start
#iterations: 283
currently lose_sum: 87.19992989301682
time_elpased: 4.278
batch start
#iterations: 284
currently lose_sum: 87.16723918914795
time_elpased: 4.203
batch start
#iterations: 285
currently lose_sum: 86.65779489278793
time_elpased: 4.252
batch start
#iterations: 286
currently lose_sum: 86.19238638877869
time_elpased: 4.091
batch start
#iterations: 287
currently lose_sum: 86.60959708690643
time_elpased: 4.268
batch start
#iterations: 288
currently lose_sum: 86.05849868059158
time_elpased: 4.187
batch start
#iterations: 289
currently lose_sum: 86.13649153709412
time_elpased: 4.388
batch start
#iterations: 290
currently lose_sum: 86.40852838754654
time_elpased: 4.279
batch start
#iterations: 291
currently lose_sum: 86.22630190849304
time_elpased: 4.295
batch start
#iterations: 292
currently lose_sum: 86.36008435487747
time_elpased: 4.413
batch start
#iterations: 293
currently lose_sum: 86.30752515792847
time_elpased: 4.774
batch start
#iterations: 294
currently lose_sum: 86.53555515408516
time_elpased: 3.608
batch start
#iterations: 295
currently lose_sum: 85.95313847064972
time_elpased: 4.436
batch start
#iterations: 296
currently lose_sum: 85.65817874670029
time_elpased: 4.089
batch start
#iterations: 297
currently lose_sum: 85.7378597855568
time_elpased: 3.888
batch start
#iterations: 298
currently lose_sum: 84.77602458000183
time_elpased: 4.384
batch start
#iterations: 299
currently lose_sum: 85.45159941911697
time_elpased: 4.356
start validation test
0.607113402062
0.620888632429
0.553668827828
0.585355238821
0.607207232255
68.088
batch start
#iterations: 300
currently lose_sum: 84.95678728818893
time_elpased: 4.278
batch start
#iterations: 301
currently lose_sum: 85.1112756729126
time_elpased: 3.808
batch start
#iterations: 302
currently lose_sum: 84.83736854791641
time_elpased: 4.135
batch start
#iterations: 303
currently lose_sum: 84.95435786247253
time_elpased: 4.29
batch start
#iterations: 304
currently lose_sum: 85.05156314373016
time_elpased: 4.383
batch start
#iterations: 305
currently lose_sum: 85.24499487876892
time_elpased: 3.864
batch start
#iterations: 306
currently lose_sum: 84.74512553215027
time_elpased: 4.147
batch start
#iterations: 307
currently lose_sum: 84.17622047662735
time_elpased: 3.948
batch start
#iterations: 308
currently lose_sum: 84.20691806077957
time_elpased: 4.224
batch start
#iterations: 309
currently lose_sum: 83.97901022434235
time_elpased: 4.305
batch start
#iterations: 310
currently lose_sum: 84.45323044061661
time_elpased: 3.309
batch start
#iterations: 311
currently lose_sum: 83.38559424877167
time_elpased: 4.25
batch start
#iterations: 312
currently lose_sum: 83.84749096632004
time_elpased: 4.284
batch start
#iterations: 313
currently lose_sum: 83.64984321594238
time_elpased: 4.349
batch start
#iterations: 314
currently lose_sum: 82.88467705249786
time_elpased: 3.961
batch start
#iterations: 315
currently lose_sum: 83.51903581619263
time_elpased: 4.231
batch start
#iterations: 316
currently lose_sum: 82.63974279165268
time_elpased: 4.139
batch start
#iterations: 317
currently lose_sum: 83.36979103088379
time_elpased: 4.217
batch start
#iterations: 318
currently lose_sum: 82.61727738380432
time_elpased: 4.298
batch start
#iterations: 319
currently lose_sum: 82.81860160827637
time_elpased: 4.194
start validation test
0.59587628866
0.623178894868
0.488628177421
0.547761882787
0.596064579261
73.258
batch start
#iterations: 320
currently lose_sum: 82.57202318310738
time_elpased: 4.037
batch start
#iterations: 321
currently lose_sum: 82.4145565032959
time_elpased: 4.118
batch start
#iterations: 322
currently lose_sum: 83.06900358200073
time_elpased: 4.144
batch start
#iterations: 323
currently lose_sum: 82.58866900205612
time_elpased: 4.093
batch start
#iterations: 324
currently lose_sum: 81.71204829216003
time_elpased: 3.999
batch start
#iterations: 325
currently lose_sum: 82.13992166519165
time_elpased: 4.114
batch start
#iterations: 326
currently lose_sum: 81.2760791182518
time_elpased: 4.402
batch start
#iterations: 327
currently lose_sum: 80.96174108982086
time_elpased: 4.438
batch start
#iterations: 328
currently lose_sum: 80.95660516619682
time_elpased: 3.735
batch start
#iterations: 329
currently lose_sum: 80.45291712880135
time_elpased: 4.323
batch start
#iterations: 330
currently lose_sum: 80.47941410541534
time_elpased: 3.912
batch start
#iterations: 331
currently lose_sum: 80.50026461482048
time_elpased: 4.656
batch start
#iterations: 332
currently lose_sum: 80.31152078509331
time_elpased: 4.203
batch start
#iterations: 333
currently lose_sum: 79.38994494080544
time_elpased: 4.55
batch start
#iterations: 334
currently lose_sum: 80.05377444624901
time_elpased: 4.135
batch start
#iterations: 335
currently lose_sum: 80.20002713799477
time_elpased: 4.351
batch start
#iterations: 336
currently lose_sum: 79.59129676222801
time_elpased: 4.246
batch start
#iterations: 337
currently lose_sum: 80.03843182325363
time_elpased: 4.216
batch start
#iterations: 338
currently lose_sum: 79.755195915699
time_elpased: 4.231
batch start
#iterations: 339
currently lose_sum: 79.43962898850441
time_elpased: 3.582
start validation test
0.59706185567
0.617778328788
0.51281259648
0.56042287578
0.597209768239
76.972
batch start
#iterations: 340
currently lose_sum: 78.87985023856163
time_elpased: 4.105
batch start
#iterations: 341
currently lose_sum: 78.97080057859421
time_elpased: 4.188
batch start
#iterations: 342
currently lose_sum: 78.64644140005112
time_elpased: 4.463
batch start
#iterations: 343
currently lose_sum: 78.2633750140667
time_elpased: 4.284
batch start
#iterations: 344
currently lose_sum: 78.56643995642662
time_elpased: 4.45
batch start
#iterations: 345
currently lose_sum: 77.52231025695801
time_elpased: 4.279
batch start
#iterations: 346
currently lose_sum: 77.89725315570831
time_elpased: 4.22
batch start
#iterations: 347
currently lose_sum: 77.09588214755058
time_elpased: 4.124
batch start
#iterations: 348
currently lose_sum: 76.99264425039291
time_elpased: 4.17
batch start
#iterations: 349
currently lose_sum: 76.26528146862984
time_elpased: 3.728
batch start
#iterations: 350
currently lose_sum: 77.04388481378555
time_elpased: 4.19
batch start
#iterations: 351
currently lose_sum: 76.9980328977108
time_elpased: 3.851
batch start
#iterations: 352
currently lose_sum: 76.75941264629364
time_elpased: 4.072
batch start
#iterations: 353
currently lose_sum: 76.7943842113018
time_elpased: 3.832
batch start
#iterations: 354
currently lose_sum: 75.83220285177231
time_elpased: 4.115
batch start
#iterations: 355
currently lose_sum: 75.73405110836029
time_elpased: 3.9
batch start
#iterations: 356
currently lose_sum: 75.35694718360901
time_elpased: 4.224
batch start
#iterations: 357
currently lose_sum: 75.93205925822258
time_elpased: 4.278
batch start
#iterations: 358
currently lose_sum: 74.85905495285988
time_elpased: 4.174
batch start
#iterations: 359
currently lose_sum: 74.37230229377747
time_elpased: 4.367
start validation test
0.573865979381
0.615593112245
0.397344859525
0.48295703296
0.574175889434
86.091
batch start
#iterations: 360
currently lose_sum: 74.41366946697235
time_elpased: 4.414
batch start
#iterations: 361
currently lose_sum: 75.57557082176208
time_elpased: 3.796
batch start
#iterations: 362
currently lose_sum: 73.91126838326454
time_elpased: 4.376
batch start
#iterations: 363
currently lose_sum: 73.54816648364067
time_elpased: 4.48
batch start
#iterations: 364
currently lose_sum: 74.19607192277908
time_elpased: 4.687
batch start
#iterations: 365
currently lose_sum: 73.23729705810547
time_elpased: 4.757
batch start
#iterations: 366
currently lose_sum: 74.15587320923805
time_elpased: 4.23
batch start
#iterations: 367
currently lose_sum: 73.42377650737762
time_elpased: 4.542
batch start
#iterations: 368
currently lose_sum: 72.69623327255249
time_elpased: 3.747
batch start
#iterations: 369
currently lose_sum: 72.44022235274315
time_elpased: 4.575
batch start
#iterations: 370
currently lose_sum: 72.7523395717144
time_elpased: 4.437
batch start
#iterations: 371
currently lose_sum: 71.94060754776001
time_elpased: 4.355
batch start
#iterations: 372
currently lose_sum: 72.69821551442146
time_elpased: 4.488
batch start
#iterations: 373
currently lose_sum: 71.28295710682869
time_elpased: 4.213
batch start
#iterations: 374
currently lose_sum: 71.6391890347004
time_elpased: 4.709
batch start
#iterations: 375
currently lose_sum: 71.58832746744156
time_elpased: 4.356
batch start
#iterations: 376
currently lose_sum: 70.87244454026222
time_elpased: 4.289
batch start
#iterations: 377
currently lose_sum: 71.4348846077919
time_elpased: 4.091
batch start
#iterations: 378
currently lose_sum: 70.90961802005768
time_elpased: 4.333
batch start
#iterations: 379
currently lose_sum: 70.49775740504265
time_elpased: 4.116
start validation test
0.576134020619
0.613976197742
0.414119584234
0.494622334214
0.576418461951
95.626
batch start
#iterations: 380
currently lose_sum: 70.44747403264046
time_elpased: 4.262
batch start
#iterations: 381
currently lose_sum: 69.76013731956482
time_elpased: 4.062
batch start
#iterations: 382
currently lose_sum: 69.29699465632439
time_elpased: 4.082
batch start
#iterations: 383
currently lose_sum: 69.9464080631733
time_elpased: 4.136
batch start
#iterations: 384
currently lose_sum: 68.70152816176414
time_elpased: 4.526
batch start
#iterations: 385
currently lose_sum: 68.4168236553669
time_elpased: 4.145
batch start
#iterations: 386
currently lose_sum: 68.10132998228073
time_elpased: 4.144
batch start
#iterations: 387
currently lose_sum: 68.44179865717888
time_elpased: 3.992
batch start
#iterations: 388
currently lose_sum: 68.52618372440338
time_elpased: 4.351
batch start
#iterations: 389
currently lose_sum: 68.21557015180588
time_elpased: 4.533
batch start
#iterations: 390
currently lose_sum: 68.50058200955391
time_elpased: 4.426
batch start
#iterations: 391
currently lose_sum: 68.39481717348099
time_elpased: 4.308
batch start
#iterations: 392
currently lose_sum: 67.54295617341995
time_elpased: 4.139
batch start
#iterations: 393
currently lose_sum: 67.96710073947906
time_elpased: 4.186
batch start
#iterations: 394
currently lose_sum: 67.999566167593
time_elpased: 4.177
batch start
#iterations: 395
currently lose_sum: 65.76650866866112
time_elpased: 4.268
batch start
#iterations: 396
currently lose_sum: 66.28709930181503
time_elpased: 4.33
batch start
#iterations: 397
currently lose_sum: 66.62478464841843
time_elpased: 4.211
batch start
#iterations: 398
currently lose_sum: 65.89026936888695
time_elpased: 4.26
batch start
#iterations: 399
currently lose_sum: 65.92153605818748
time_elpased: 4.612
start validation test
0.564381443299
0.598230912477
0.396727384995
0.477074438463
0.564675785857
107.127
acc: 0.640
pre: 0.633
rec: 0.670
F1: 0.651
auc: 0.689
