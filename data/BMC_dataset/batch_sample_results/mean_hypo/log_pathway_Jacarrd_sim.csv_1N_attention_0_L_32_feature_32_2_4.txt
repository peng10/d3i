start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.59923189878464
time_elpased: 1.364
batch start
#iterations: 1
currently lose_sum: 100.18501943349838
time_elpased: 1.341
batch start
#iterations: 2
currently lose_sum: 100.12841892242432
time_elpased: 1.35
batch start
#iterations: 3
currently lose_sum: 99.9978266954422
time_elpased: 1.351
batch start
#iterations: 4
currently lose_sum: 99.92839270830154
time_elpased: 1.369
batch start
#iterations: 5
currently lose_sum: 99.77006274461746
time_elpased: 1.374
batch start
#iterations: 6
currently lose_sum: 99.7240543961525
time_elpased: 1.41
batch start
#iterations: 7
currently lose_sum: 99.6166318655014
time_elpased: 1.365
batch start
#iterations: 8
currently lose_sum: 99.59928691387177
time_elpased: 1.353
batch start
#iterations: 9
currently lose_sum: 99.61715859174728
time_elpased: 1.383
batch start
#iterations: 10
currently lose_sum: 99.44035232067108
time_elpased: 1.365
batch start
#iterations: 11
currently lose_sum: 99.33001762628555
time_elpased: 1.361
batch start
#iterations: 12
currently lose_sum: 99.21629732847214
time_elpased: 1.362
batch start
#iterations: 13
currently lose_sum: 99.4346472620964
time_elpased: 1.379
batch start
#iterations: 14
currently lose_sum: 99.25545883178711
time_elpased: 1.379
batch start
#iterations: 15
currently lose_sum: 99.19843238592148
time_elpased: 1.362
batch start
#iterations: 16
currently lose_sum: 99.06900507211685
time_elpased: 1.344
batch start
#iterations: 17
currently lose_sum: 98.71965718269348
time_elpased: 1.361
batch start
#iterations: 18
currently lose_sum: 98.54426348209381
time_elpased: 1.36
batch start
#iterations: 19
currently lose_sum: 98.81912577152252
time_elpased: 1.356
start validation test
0.609793814433
0.600618614678
0.65946279716
0.628666732071
0.609706612873
64.222
batch start
#iterations: 20
currently lose_sum: 98.57766002416611
time_elpased: 1.367
batch start
#iterations: 21
currently lose_sum: 98.57900899648666
time_elpased: 1.359
batch start
#iterations: 22
currently lose_sum: 98.36641317605972
time_elpased: 1.357
batch start
#iterations: 23
currently lose_sum: 98.5734235048294
time_elpased: 1.367
batch start
#iterations: 24
currently lose_sum: 98.32701855897903
time_elpased: 1.385
batch start
#iterations: 25
currently lose_sum: 98.01682329177856
time_elpased: 1.363
batch start
#iterations: 26
currently lose_sum: 98.31988477706909
time_elpased: 1.383
batch start
#iterations: 27
currently lose_sum: 97.82926720380783
time_elpased: 1.357
batch start
#iterations: 28
currently lose_sum: 97.85471975803375
time_elpased: 1.372
batch start
#iterations: 29
currently lose_sum: 97.6969284415245
time_elpased: 1.386
batch start
#iterations: 30
currently lose_sum: 97.37903434038162
time_elpased: 1.367
batch start
#iterations: 31
currently lose_sum: 97.42211955785751
time_elpased: 1.361
batch start
#iterations: 32
currently lose_sum: 97.49681693315506
time_elpased: 1.346
batch start
#iterations: 33
currently lose_sum: 97.07610726356506
time_elpased: 1.361
batch start
#iterations: 34
currently lose_sum: 97.34034794569016
time_elpased: 1.369
batch start
#iterations: 35
currently lose_sum: 96.90985262393951
time_elpased: 1.402
batch start
#iterations: 36
currently lose_sum: 97.17384922504425
time_elpased: 1.382
batch start
#iterations: 37
currently lose_sum: 96.86527508497238
time_elpased: 1.37
batch start
#iterations: 38
currently lose_sum: 96.61645817756653
time_elpased: 1.355
batch start
#iterations: 39
currently lose_sum: 96.61632168292999
time_elpased: 1.353
start validation test
0.636958762887
0.625563486101
0.685499639807
0.654161551682
0.63687354189
62.463
batch start
#iterations: 40
currently lose_sum: 96.6837420463562
time_elpased: 1.373
batch start
#iterations: 41
currently lose_sum: 96.6536111831665
time_elpased: 1.374
batch start
#iterations: 42
currently lose_sum: 96.5533287525177
time_elpased: 1.354
batch start
#iterations: 43
currently lose_sum: 97.01847183704376
time_elpased: 1.393
batch start
#iterations: 44
currently lose_sum: 96.34969198703766
time_elpased: 1.363
batch start
#iterations: 45
currently lose_sum: 96.68831253051758
time_elpased: 1.355
batch start
#iterations: 46
currently lose_sum: 96.05236828327179
time_elpased: 1.404
batch start
#iterations: 47
currently lose_sum: 96.43282628059387
time_elpased: 1.368
batch start
#iterations: 48
currently lose_sum: 96.37388688325882
time_elpased: 1.357
batch start
#iterations: 49
currently lose_sum: 95.99276965856552
time_elpased: 1.363
batch start
#iterations: 50
currently lose_sum: 96.05688959360123
time_elpased: 1.376
batch start
#iterations: 51
currently lose_sum: 95.70620882511139
time_elpased: 1.361
batch start
#iterations: 52
currently lose_sum: 95.54420953989029
time_elpased: 1.35
batch start
#iterations: 53
currently lose_sum: 95.71249091625214
time_elpased: 1.362
batch start
#iterations: 54
currently lose_sum: 95.5191752910614
time_elpased: 1.368
batch start
#iterations: 55
currently lose_sum: 95.42075723409653
time_elpased: 1.398
batch start
#iterations: 56
currently lose_sum: 95.6082261800766
time_elpased: 1.359
batch start
#iterations: 57
currently lose_sum: 95.43247658014297
time_elpased: 1.403
batch start
#iterations: 58
currently lose_sum: 95.65638995170593
time_elpased: 1.384
batch start
#iterations: 59
currently lose_sum: 95.61679518222809
time_elpased: 1.372
start validation test
0.633556701031
0.628397006696
0.6567870742
0.642278468274
0.633515916528
61.860
batch start
#iterations: 60
currently lose_sum: 95.9139958024025
time_elpased: 1.372
batch start
#iterations: 61
currently lose_sum: 95.62178474664688
time_elpased: 1.366
batch start
#iterations: 62
currently lose_sum: 95.46048438549042
time_elpased: 1.406
batch start
#iterations: 63
currently lose_sum: 95.55665504932404
time_elpased: 1.375
batch start
#iterations: 64
currently lose_sum: 95.45906811952591
time_elpased: 1.358
batch start
#iterations: 65
currently lose_sum: 95.47882676124573
time_elpased: 1.363
batch start
#iterations: 66
currently lose_sum: 95.22032636404037
time_elpased: 1.35
batch start
#iterations: 67
currently lose_sum: 94.68427330255508
time_elpased: 1.37
batch start
#iterations: 68
currently lose_sum: 94.97128766775131
time_elpased: 1.355
batch start
#iterations: 69
currently lose_sum: 95.14213043451309
time_elpased: 1.373
batch start
#iterations: 70
currently lose_sum: 95.5843865275383
time_elpased: 1.363
batch start
#iterations: 71
currently lose_sum: 95.0651461482048
time_elpased: 1.356
batch start
#iterations: 72
currently lose_sum: 94.88626170158386
time_elpased: 1.374
batch start
#iterations: 73
currently lose_sum: 94.93487566709518
time_elpased: 1.36
batch start
#iterations: 74
currently lose_sum: 94.73324763774872
time_elpased: 1.404
batch start
#iterations: 75
currently lose_sum: 95.17495280504227
time_elpased: 1.349
batch start
#iterations: 76
currently lose_sum: 94.92748355865479
time_elpased: 1.347
batch start
#iterations: 77
currently lose_sum: 94.56390923261642
time_elpased: 1.42
batch start
#iterations: 78
currently lose_sum: 94.88637268543243
time_elpased: 1.365
batch start
#iterations: 79
currently lose_sum: 94.82817679643631
time_elpased: 1.361
start validation test
0.621855670103
0.595614810055
0.763198518061
0.66907253699
0.621607520933
62.753
batch start
#iterations: 80
currently lose_sum: 94.34104853868484
time_elpased: 1.364
batch start
#iterations: 81
currently lose_sum: 95.172771692276
time_elpased: 1.389
batch start
#iterations: 82
currently lose_sum: 94.98585188388824
time_elpased: 1.367
batch start
#iterations: 83
currently lose_sum: 94.62035208940506
time_elpased: 1.345
batch start
#iterations: 84
currently lose_sum: 95.25348722934723
time_elpased: 1.366
batch start
#iterations: 85
currently lose_sum: 95.25421053171158
time_elpased: 1.37
batch start
#iterations: 86
currently lose_sum: 94.8250937461853
time_elpased: 1.371
batch start
#iterations: 87
currently lose_sum: 94.66927742958069
time_elpased: 1.387
batch start
#iterations: 88
currently lose_sum: 94.70930010080338
time_elpased: 1.358
batch start
#iterations: 89
currently lose_sum: 95.15785640478134
time_elpased: 1.419
batch start
#iterations: 90
currently lose_sum: 94.65185713768005
time_elpased: 1.346
batch start
#iterations: 91
currently lose_sum: 94.71622347831726
time_elpased: 1.355
batch start
#iterations: 92
currently lose_sum: 94.36529499292374
time_elpased: 1.357
batch start
#iterations: 93
currently lose_sum: 94.2414271235466
time_elpased: 1.375
batch start
#iterations: 94
currently lose_sum: 94.71359485387802
time_elpased: 1.366
batch start
#iterations: 95
currently lose_sum: 94.7699483036995
time_elpased: 1.367
batch start
#iterations: 96
currently lose_sum: 94.59691566228867
time_elpased: 1.375
batch start
#iterations: 97
currently lose_sum: 94.8849669098854
time_elpased: 1.356
batch start
#iterations: 98
currently lose_sum: 94.3212383389473
time_elpased: 1.354
batch start
#iterations: 99
currently lose_sum: 94.59195601940155
time_elpased: 1.366
start validation test
0.634587628866
0.619000181127
0.703406401153
0.658509562118
0.634466806897
61.771
batch start
#iterations: 100
currently lose_sum: 94.67105734348297
time_elpased: 1.369
batch start
#iterations: 101
currently lose_sum: 94.41341888904572
time_elpased: 1.376
batch start
#iterations: 102
currently lose_sum: 94.63219380378723
time_elpased: 1.37
batch start
#iterations: 103
currently lose_sum: 94.67408949136734
time_elpased: 1.349
batch start
#iterations: 104
currently lose_sum: 94.03602558374405
time_elpased: 1.354
batch start
#iterations: 105
currently lose_sum: 94.451491355896
time_elpased: 1.354
batch start
#iterations: 106
currently lose_sum: 94.47126644849777
time_elpased: 1.392
batch start
#iterations: 107
currently lose_sum: 94.29525530338287
time_elpased: 1.363
batch start
#iterations: 108
currently lose_sum: 94.51878213882446
time_elpased: 1.353
batch start
#iterations: 109
currently lose_sum: 94.4458681344986
time_elpased: 1.364
batch start
#iterations: 110
currently lose_sum: 94.24874824285507
time_elpased: 1.364
batch start
#iterations: 111
currently lose_sum: 94.91263657808304
time_elpased: 1.375
batch start
#iterations: 112
currently lose_sum: 93.96276140213013
time_elpased: 1.374
batch start
#iterations: 113
currently lose_sum: 93.78387969732285
time_elpased: 1.361
batch start
#iterations: 114
currently lose_sum: 93.94222110509872
time_elpased: 1.366
batch start
#iterations: 115
currently lose_sum: 94.05088996887207
time_elpased: 1.362
batch start
#iterations: 116
currently lose_sum: 94.55903655290604
time_elpased: 1.36
batch start
#iterations: 117
currently lose_sum: 94.03529685735703
time_elpased: 1.359
batch start
#iterations: 118
currently lose_sum: 94.19417506456375
time_elpased: 1.408
batch start
#iterations: 119
currently lose_sum: 94.18512463569641
time_elpased: 1.361
start validation test
0.610412371134
0.597630460342
0.680045281465
0.636179840185
0.61029011982
62.966
batch start
#iterations: 120
currently lose_sum: 93.87895458936691
time_elpased: 1.365
batch start
#iterations: 121
currently lose_sum: 94.50467544794083
time_elpased: 1.368
batch start
#iterations: 122
currently lose_sum: 94.21559065580368
time_elpased: 1.351
batch start
#iterations: 123
currently lose_sum: 93.84353643655777
time_elpased: 1.39
batch start
#iterations: 124
currently lose_sum: 93.87152510881424
time_elpased: 1.37
batch start
#iterations: 125
currently lose_sum: 93.90344107151031
time_elpased: 1.37
batch start
#iterations: 126
currently lose_sum: 94.22619587182999
time_elpased: 1.374
batch start
#iterations: 127
currently lose_sum: 94.13562726974487
time_elpased: 1.419
batch start
#iterations: 128
currently lose_sum: 94.04074603319168
time_elpased: 1.365
batch start
#iterations: 129
currently lose_sum: 93.94643324613571
time_elpased: 1.387
batch start
#iterations: 130
currently lose_sum: 94.26321393251419
time_elpased: 1.345
batch start
#iterations: 131
currently lose_sum: 93.69462275505066
time_elpased: 1.383
batch start
#iterations: 132
currently lose_sum: 94.30072075128555
time_elpased: 1.354
batch start
#iterations: 133
currently lose_sum: 93.73219496011734
time_elpased: 1.388
batch start
#iterations: 134
currently lose_sum: 93.81228333711624
time_elpased: 1.381
batch start
#iterations: 135
currently lose_sum: 94.06079506874084
time_elpased: 1.397
batch start
#iterations: 136
currently lose_sum: 94.06109631061554
time_elpased: 1.385
batch start
#iterations: 137
currently lose_sum: 93.79186475276947
time_elpased: 1.374
batch start
#iterations: 138
currently lose_sum: 94.01695305109024
time_elpased: 1.359
batch start
#iterations: 139
currently lose_sum: 94.08533751964569
time_elpased: 1.417
start validation test
0.651494845361
0.647328548644
0.668313265411
0.657653552079
0.65146531803
60.526
batch start
#iterations: 140
currently lose_sum: 93.93315505981445
time_elpased: 1.355
batch start
#iterations: 141
currently lose_sum: 93.65049338340759
time_elpased: 1.356
batch start
#iterations: 142
currently lose_sum: 94.39828819036484
time_elpased: 1.364
batch start
#iterations: 143
currently lose_sum: 93.53700870275497
time_elpased: 1.388
batch start
#iterations: 144
currently lose_sum: 94.06765222549438
time_elpased: 1.383
batch start
#iterations: 145
currently lose_sum: 93.95620214939117
time_elpased: 1.374
batch start
#iterations: 146
currently lose_sum: 93.70238214731216
time_elpased: 1.37
batch start
#iterations: 147
currently lose_sum: 93.59919929504395
time_elpased: 1.429
batch start
#iterations: 148
currently lose_sum: 93.81834697723389
time_elpased: 1.384
batch start
#iterations: 149
currently lose_sum: 94.15285843610764
time_elpased: 1.392
batch start
#iterations: 150
currently lose_sum: 93.74924999475479
time_elpased: 1.361
batch start
#iterations: 151
currently lose_sum: 93.90816181898117
time_elpased: 1.386
batch start
#iterations: 152
currently lose_sum: 93.23441290855408
time_elpased: 1.339
batch start
#iterations: 153
currently lose_sum: 93.6901690363884
time_elpased: 1.384
batch start
#iterations: 154
currently lose_sum: 93.50750666856766
time_elpased: 1.369
batch start
#iterations: 155
currently lose_sum: 94.10255390405655
time_elpased: 1.375
batch start
#iterations: 156
currently lose_sum: 93.73356127738953
time_elpased: 1.376
batch start
#iterations: 157
currently lose_sum: 93.73276191949844
time_elpased: 1.42
batch start
#iterations: 158
currently lose_sum: 93.93513703346252
time_elpased: 1.375
batch start
#iterations: 159
currently lose_sum: 94.16424322128296
time_elpased: 1.387
start validation test
0.646855670103
0.63432695913
0.696408356489
0.663919548688
0.646768672719
60.961
batch start
#iterations: 160
currently lose_sum: 93.83410584926605
time_elpased: 1.42
batch start
#iterations: 161
currently lose_sum: 93.86588442325592
time_elpased: 1.385
batch start
#iterations: 162
currently lose_sum: 93.29782122373581
time_elpased: 1.373
batch start
#iterations: 163
currently lose_sum: 93.44489371776581
time_elpased: 1.353
batch start
#iterations: 164
currently lose_sum: 93.60055029392242
time_elpased: 1.402
batch start
#iterations: 165
currently lose_sum: 93.43522709608078
time_elpased: 1.396
batch start
#iterations: 166
currently lose_sum: 93.43128031492233
time_elpased: 1.353
batch start
#iterations: 167
currently lose_sum: 93.81828445196152
time_elpased: 1.362
batch start
#iterations: 168
currently lose_sum: 93.4483163356781
time_elpased: 1.38
batch start
#iterations: 169
currently lose_sum: 93.23694652318954
time_elpased: 1.37
batch start
#iterations: 170
currently lose_sum: 93.26331323385239
time_elpased: 1.376
batch start
#iterations: 171
currently lose_sum: 93.51691782474518
time_elpased: 1.397
batch start
#iterations: 172
currently lose_sum: 93.72799760103226
time_elpased: 1.362
batch start
#iterations: 173
currently lose_sum: 93.27368342876434
time_elpased: 1.382
batch start
#iterations: 174
currently lose_sum: 93.43493813276291
time_elpased: 1.369
batch start
#iterations: 175
currently lose_sum: 93.51431065797806
time_elpased: 1.391
batch start
#iterations: 176
currently lose_sum: 93.85752254724503
time_elpased: 1.364
batch start
#iterations: 177
currently lose_sum: 93.86424326896667
time_elpased: 1.36
batch start
#iterations: 178
currently lose_sum: 92.98240089416504
time_elpased: 1.386
batch start
#iterations: 179
currently lose_sum: 93.49547171592712
time_elpased: 1.364
start validation test
0.642422680412
0.64554973822
0.634455078728
0.63995432605
0.642436668766
61.007
batch start
#iterations: 180
currently lose_sum: 93.24988925457001
time_elpased: 1.381
batch start
#iterations: 181
currently lose_sum: 93.10819971561432
time_elpased: 1.382
batch start
#iterations: 182
currently lose_sum: 92.96976178884506
time_elpased: 1.376
batch start
#iterations: 183
currently lose_sum: 93.52415060997009
time_elpased: 1.383
batch start
#iterations: 184
currently lose_sum: 93.13279318809509
time_elpased: 1.381
batch start
#iterations: 185
currently lose_sum: 93.07811051607132
time_elpased: 1.432
batch start
#iterations: 186
currently lose_sum: 93.10358935594559
time_elpased: 1.365
batch start
#iterations: 187
currently lose_sum: 93.41583436727524
time_elpased: 1.379
batch start
#iterations: 188
currently lose_sum: 93.13064122200012
time_elpased: 1.374
batch start
#iterations: 189
currently lose_sum: 93.6942680478096
time_elpased: 1.415
batch start
#iterations: 190
currently lose_sum: 93.043337225914
time_elpased: 1.344
batch start
#iterations: 191
currently lose_sum: 93.27298367023468
time_elpased: 1.39
batch start
#iterations: 192
currently lose_sum: 93.20758521556854
time_elpased: 1.386
batch start
#iterations: 193
currently lose_sum: 93.09649521112442
time_elpased: 1.367
batch start
#iterations: 194
currently lose_sum: 93.14066326618195
time_elpased: 1.365
batch start
#iterations: 195
currently lose_sum: 93.27611148357391
time_elpased: 1.392
batch start
#iterations: 196
currently lose_sum: 92.93151831626892
time_elpased: 1.4
batch start
#iterations: 197
currently lose_sum: 93.8659657239914
time_elpased: 1.385
batch start
#iterations: 198
currently lose_sum: 92.83500891923904
time_elpased: 1.384
batch start
#iterations: 199
currently lose_sum: 92.79549008607864
time_elpased: 1.37
start validation test
0.625206185567
0.620682849813
0.647319131419
0.633721223112
0.62516736288
61.972
batch start
#iterations: 200
currently lose_sum: 93.3890289068222
time_elpased: 1.348
batch start
#iterations: 201
currently lose_sum: 93.35591685771942
time_elpased: 1.39
batch start
#iterations: 202
currently lose_sum: 93.21029525995255
time_elpased: 1.373
batch start
#iterations: 203
currently lose_sum: 93.22243970632553
time_elpased: 1.381
batch start
#iterations: 204
currently lose_sum: 93.05584001541138
time_elpased: 1.398
batch start
#iterations: 205
currently lose_sum: 93.04417490959167
time_elpased: 1.376
batch start
#iterations: 206
currently lose_sum: 92.78906780481339
time_elpased: 1.357
batch start
#iterations: 207
currently lose_sum: 93.05493289232254
time_elpased: 1.392
batch start
#iterations: 208
currently lose_sum: 92.57434105873108
time_elpased: 1.37
batch start
#iterations: 209
currently lose_sum: 92.80665582418442
time_elpased: 1.361
batch start
#iterations: 210
currently lose_sum: 92.53815060853958
time_elpased: 1.356
batch start
#iterations: 211
currently lose_sum: 93.17470544576645
time_elpased: 1.354
batch start
#iterations: 212
currently lose_sum: 92.66085833311081
time_elpased: 1.418
batch start
#iterations: 213
currently lose_sum: 92.82866716384888
time_elpased: 1.399
batch start
#iterations: 214
currently lose_sum: 93.04689902067184
time_elpased: 1.36
batch start
#iterations: 215
currently lose_sum: 92.91617596149445
time_elpased: 1.367
batch start
#iterations: 216
currently lose_sum: 92.72146278619766
time_elpased: 1.368
batch start
#iterations: 217
currently lose_sum: 93.10373079776764
time_elpased: 1.359
batch start
#iterations: 218
currently lose_sum: 92.66622388362885
time_elpased: 1.384
batch start
#iterations: 219
currently lose_sum: 92.80597186088562
time_elpased: 1.386
start validation test
0.641237113402
0.642657559764
0.639086137697
0.640866873065
0.641240889772
60.897
batch start
#iterations: 220
currently lose_sum: 92.78632658720016
time_elpased: 1.392
batch start
#iterations: 221
currently lose_sum: 92.89947193861008
time_elpased: 1.4
batch start
#iterations: 222
currently lose_sum: 92.44063198566437
time_elpased: 1.353
batch start
#iterations: 223
currently lose_sum: 92.69201362133026
time_elpased: 1.368
batch start
#iterations: 224
currently lose_sum: 92.86120533943176
time_elpased: 1.38
batch start
#iterations: 225
currently lose_sum: 93.01233470439911
time_elpased: 1.398
batch start
#iterations: 226
currently lose_sum: 92.77772146463394
time_elpased: 1.376
batch start
#iterations: 227
currently lose_sum: 92.5197429060936
time_elpased: 1.408
batch start
#iterations: 228
currently lose_sum: 92.58241951465607
time_elpased: 1.38
batch start
#iterations: 229
currently lose_sum: 92.44550108909607
time_elpased: 1.372
batch start
#iterations: 230
currently lose_sum: 92.8543148636818
time_elpased: 1.383
batch start
#iterations: 231
currently lose_sum: 92.40699374675751
time_elpased: 1.395
batch start
#iterations: 232
currently lose_sum: 92.95229893922806
time_elpased: 1.356
batch start
#iterations: 233
currently lose_sum: 92.76171791553497
time_elpased: 1.355
batch start
#iterations: 234
currently lose_sum: 92.39582359790802
time_elpased: 1.37
batch start
#iterations: 235
currently lose_sum: 92.89167201519012
time_elpased: 1.364
batch start
#iterations: 236
currently lose_sum: 92.90327423810959
time_elpased: 1.399
batch start
#iterations: 237
currently lose_sum: 92.7408127784729
time_elpased: 1.393
batch start
#iterations: 238
currently lose_sum: 92.96985304355621
time_elpased: 1.396
batch start
#iterations: 239
currently lose_sum: 92.29804348945618
time_elpased: 1.36
start validation test
0.643608247423
0.635476075399
0.676546259133
0.65536835809
0.643550419663
60.815
batch start
#iterations: 240
currently lose_sum: 92.57126474380493
time_elpased: 1.355
batch start
#iterations: 241
currently lose_sum: 92.93306106328964
time_elpased: 1.368
batch start
#iterations: 242
currently lose_sum: 92.60635846853256
time_elpased: 1.369
batch start
#iterations: 243
currently lose_sum: 92.73564267158508
time_elpased: 1.372
batch start
#iterations: 244
currently lose_sum: 92.99943870306015
time_elpased: 1.387
batch start
#iterations: 245
currently lose_sum: 92.60316860675812
time_elpased: 1.401
batch start
#iterations: 246
currently lose_sum: 92.70320010185242
time_elpased: 1.432
batch start
#iterations: 247
currently lose_sum: 92.52857410907745
time_elpased: 1.364
batch start
#iterations: 248
currently lose_sum: 92.84495013952255
time_elpased: 1.378
batch start
#iterations: 249
currently lose_sum: 92.50209534168243
time_elpased: 1.367
batch start
#iterations: 250
currently lose_sum: 92.3869069814682
time_elpased: 1.388
batch start
#iterations: 251
currently lose_sum: 92.48917466402054
time_elpased: 1.429
batch start
#iterations: 252
currently lose_sum: 92.33711063861847
time_elpased: 1.372
batch start
#iterations: 253
currently lose_sum: 92.42866522073746
time_elpased: 1.371
batch start
#iterations: 254
currently lose_sum: 92.5572555065155
time_elpased: 1.37
batch start
#iterations: 255
currently lose_sum: 92.7416273355484
time_elpased: 1.414
batch start
#iterations: 256
currently lose_sum: 92.35234206914902
time_elpased: 1.367
batch start
#iterations: 257
currently lose_sum: 92.89112257957458
time_elpased: 1.401
batch start
#iterations: 258
currently lose_sum: 92.01275789737701
time_elpased: 1.376
batch start
#iterations: 259
currently lose_sum: 92.41345554590225
time_elpased: 1.405
start validation test
0.613865979381
0.622983425414
0.580220232582
0.600841903341
0.613925049679
62.261
batch start
#iterations: 260
currently lose_sum: 92.32750117778778
time_elpased: 1.41
batch start
#iterations: 261
currently lose_sum: 92.28756618499756
time_elpased: 1.37
batch start
#iterations: 262
currently lose_sum: 92.30228370428085
time_elpased: 1.373
batch start
#iterations: 263
currently lose_sum: 92.22991210222244
time_elpased: 1.372
batch start
#iterations: 264
currently lose_sum: 92.70883703231812
time_elpased: 1.387
batch start
#iterations: 265
currently lose_sum: 92.57735919952393
time_elpased: 1.41
batch start
#iterations: 266
currently lose_sum: 92.23774141073227
time_elpased: 1.356
batch start
#iterations: 267
currently lose_sum: 92.80662226676941
time_elpased: 1.392
batch start
#iterations: 268
currently lose_sum: 92.46996766328812
time_elpased: 1.377
batch start
#iterations: 269
currently lose_sum: 92.8997493982315
time_elpased: 1.361
batch start
#iterations: 270
currently lose_sum: 92.23274159431458
time_elpased: 1.366
batch start
#iterations: 271
currently lose_sum: 92.34088772535324
time_elpased: 1.378
batch start
#iterations: 272
currently lose_sum: 92.3201414346695
time_elpased: 1.378
batch start
#iterations: 273
currently lose_sum: 91.85799008607864
time_elpased: 1.386
batch start
#iterations: 274
currently lose_sum: 92.70452481508255
time_elpased: 1.37
batch start
#iterations: 275
currently lose_sum: 92.26177597045898
time_elpased: 1.374
batch start
#iterations: 276
currently lose_sum: 92.39972138404846
time_elpased: 1.396
batch start
#iterations: 277
currently lose_sum: 92.39316248893738
time_elpased: 1.419
batch start
#iterations: 278
currently lose_sum: 91.92579448223114
time_elpased: 1.405
batch start
#iterations: 279
currently lose_sum: 92.39994430541992
time_elpased: 1.381
start validation test
0.641546391753
0.652063841497
0.609653185139
0.630145729178
0.641602385196
60.631
batch start
#iterations: 280
currently lose_sum: 92.26826441287994
time_elpased: 1.39
batch start
#iterations: 281
currently lose_sum: 92.53142100572586
time_elpased: 1.348
batch start
#iterations: 282
currently lose_sum: 92.12299489974976
time_elpased: 1.357
batch start
#iterations: 283
currently lose_sum: 91.6149353981018
time_elpased: 1.368
batch start
#iterations: 284
currently lose_sum: 92.10319513082504
time_elpased: 1.393
batch start
#iterations: 285
currently lose_sum: 91.99243628978729
time_elpased: 1.389
batch start
#iterations: 286
currently lose_sum: 92.18913638591766
time_elpased: 1.373
batch start
#iterations: 287
currently lose_sum: 91.84675812721252
time_elpased: 1.408
batch start
#iterations: 288
currently lose_sum: 92.20251375436783
time_elpased: 1.393
batch start
#iterations: 289
currently lose_sum: 91.881707072258
time_elpased: 1.363
batch start
#iterations: 290
currently lose_sum: 91.98744678497314
time_elpased: 1.372
batch start
#iterations: 291
currently lose_sum: 91.75827515125275
time_elpased: 1.363
batch start
#iterations: 292
currently lose_sum: 92.43749475479126
time_elpased: 1.363
batch start
#iterations: 293
currently lose_sum: 91.57488596439362
time_elpased: 1.381
batch start
#iterations: 294
currently lose_sum: 91.80540180206299
time_elpased: 1.37
batch start
#iterations: 295
currently lose_sum: 91.9731171131134
time_elpased: 1.368
batch start
#iterations: 296
currently lose_sum: 91.78541588783264
time_elpased: 1.377
batch start
#iterations: 297
currently lose_sum: 92.3576608300209
time_elpased: 1.354
batch start
#iterations: 298
currently lose_sum: 91.64914762973785
time_elpased: 1.361
batch start
#iterations: 299
currently lose_sum: 91.6376296877861
time_elpased: 1.369
start validation test
0.632886597938
0.641047939993
0.606874549758
0.623493338972
0.632932266101
61.185
batch start
#iterations: 300
currently lose_sum: 91.41215491294861
time_elpased: 1.347
batch start
#iterations: 301
currently lose_sum: 91.62900668382645
time_elpased: 1.391
batch start
#iterations: 302
currently lose_sum: 92.12181413173676
time_elpased: 1.342
batch start
#iterations: 303
currently lose_sum: 91.95220649242401
time_elpased: 1.356
batch start
#iterations: 304
currently lose_sum: 91.92857319116592
time_elpased: 1.361
batch start
#iterations: 305
currently lose_sum: 91.80111461877823
time_elpased: 1.367
batch start
#iterations: 306
currently lose_sum: 91.93115597963333
time_elpased: 1.373
batch start
#iterations: 307
currently lose_sum: 91.70001155138016
time_elpased: 1.39
batch start
#iterations: 308
currently lose_sum: 92.00623309612274
time_elpased: 1.368
batch start
#iterations: 309
currently lose_sum: 91.47907418012619
time_elpased: 1.378
batch start
#iterations: 310
currently lose_sum: 91.70755904912949
time_elpased: 1.352
batch start
#iterations: 311
currently lose_sum: 91.44739490747452
time_elpased: 1.361
batch start
#iterations: 312
currently lose_sum: 91.441386282444
time_elpased: 1.364
batch start
#iterations: 313
currently lose_sum: 91.61763525009155
time_elpased: 1.395
batch start
#iterations: 314
currently lose_sum: 91.99712663888931
time_elpased: 1.348
batch start
#iterations: 315
currently lose_sum: 91.23659420013428
time_elpased: 1.368
batch start
#iterations: 316
currently lose_sum: 91.8924942612648
time_elpased: 1.361
batch start
#iterations: 317
currently lose_sum: 91.96719688177109
time_elpased: 1.382
batch start
#iterations: 318
currently lose_sum: 92.10252529382706
time_elpased: 1.353
batch start
#iterations: 319
currently lose_sum: 91.60755383968353
time_elpased: 1.372
start validation test
0.633041237113
0.626880250049
0.660491921375
0.643247306439
0.632993043203
61.326
batch start
#iterations: 320
currently lose_sum: 91.63308769464493
time_elpased: 1.377
batch start
#iterations: 321
currently lose_sum: 91.96550929546356
time_elpased: 1.377
batch start
#iterations: 322
currently lose_sum: 91.77796286344528
time_elpased: 1.377
batch start
#iterations: 323
currently lose_sum: 91.90815359354019
time_elpased: 1.406
batch start
#iterations: 324
currently lose_sum: 91.79772078990936
time_elpased: 1.355
batch start
#iterations: 325
currently lose_sum: 92.14402401447296
time_elpased: 1.387
batch start
#iterations: 326
currently lose_sum: 91.6553202867508
time_elpased: 1.367
batch start
#iterations: 327
currently lose_sum: 91.66572612524033
time_elpased: 1.39
batch start
#iterations: 328
currently lose_sum: 91.62395876646042
time_elpased: 1.391
batch start
#iterations: 329
currently lose_sum: 91.53691387176514
time_elpased: 1.371
batch start
#iterations: 330
currently lose_sum: 91.42735493183136
time_elpased: 1.356
batch start
#iterations: 331
currently lose_sum: 91.72609001398087
time_elpased: 1.379
batch start
#iterations: 332
currently lose_sum: 91.40099388360977
time_elpased: 1.356
batch start
#iterations: 333
currently lose_sum: 91.42066329717636
time_elpased: 1.377
batch start
#iterations: 334
currently lose_sum: 91.50225222110748
time_elpased: 1.395
batch start
#iterations: 335
currently lose_sum: 91.41947025060654
time_elpased: 1.377
batch start
#iterations: 336
currently lose_sum: 91.86052316427231
time_elpased: 1.366
batch start
#iterations: 337
currently lose_sum: 91.3040582537651
time_elpased: 1.365
batch start
#iterations: 338
currently lose_sum: 91.34174400568008
time_elpased: 1.364
batch start
#iterations: 339
currently lose_sum: 91.56847107410431
time_elpased: 1.368
start validation test
0.635154639175
0.649858035207
0.588864875991
0.61785984235
0.635235907994
61.012
batch start
#iterations: 340
currently lose_sum: 91.35690242052078
time_elpased: 1.343
batch start
#iterations: 341
currently lose_sum: 92.34679543972015
time_elpased: 1.381
batch start
#iterations: 342
currently lose_sum: 91.66171997785568
time_elpased: 1.373
batch start
#iterations: 343
currently lose_sum: 91.46978902816772
time_elpased: 1.366
batch start
#iterations: 344
currently lose_sum: 91.4995049238205
time_elpased: 1.353
batch start
#iterations: 345
currently lose_sum: 91.6658383011818
time_elpased: 1.394
batch start
#iterations: 346
currently lose_sum: 91.22476202249527
time_elpased: 1.352
batch start
#iterations: 347
currently lose_sum: 91.52413427829742
time_elpased: 1.413
batch start
#iterations: 348
currently lose_sum: 91.58991861343384
time_elpased: 1.348
batch start
#iterations: 349
currently lose_sum: 91.3012193441391
time_elpased: 1.38
batch start
#iterations: 350
currently lose_sum: 91.69398498535156
time_elpased: 1.362
batch start
#iterations: 351
currently lose_sum: 91.57990312576294
time_elpased: 1.395
batch start
#iterations: 352
currently lose_sum: 91.0767770409584
time_elpased: 1.345
batch start
#iterations: 353
currently lose_sum: 91.18055421113968
time_elpased: 1.386
batch start
#iterations: 354
currently lose_sum: 91.35227078199387
time_elpased: 1.375
batch start
#iterations: 355
currently lose_sum: 91.52993607521057
time_elpased: 1.397
batch start
#iterations: 356
currently lose_sum: 91.38555765151978
time_elpased: 1.364
batch start
#iterations: 357
currently lose_sum: 91.48056423664093
time_elpased: 1.366
batch start
#iterations: 358
currently lose_sum: 91.08177787065506
time_elpased: 1.356
batch start
#iterations: 359
currently lose_sum: 91.4557580947876
time_elpased: 1.374
start validation test
0.62293814433
0.632356182499
0.590511474735
0.610717896866
0.62299507435
61.378
batch start
#iterations: 360
currently lose_sum: 91.76101964712143
time_elpased: 1.37
batch start
#iterations: 361
currently lose_sum: 91.40019035339355
time_elpased: 1.371
batch start
#iterations: 362
currently lose_sum: 91.36043465137482
time_elpased: 1.367
batch start
#iterations: 363
currently lose_sum: 91.4331915974617
time_elpased: 1.395
batch start
#iterations: 364
currently lose_sum: 91.02094751596451
time_elpased: 1.362
batch start
#iterations: 365
currently lose_sum: 91.44757801294327
time_elpased: 1.38
batch start
#iterations: 366
currently lose_sum: 91.6512400507927
time_elpased: 1.379
batch start
#iterations: 367
currently lose_sum: 91.38170236349106
time_elpased: 1.366
batch start
#iterations: 368
currently lose_sum: 90.86488234996796
time_elpased: 1.389
batch start
#iterations: 369
currently lose_sum: 91.59906494617462
time_elpased: 1.383
batch start
#iterations: 370
currently lose_sum: 91.17123675346375
time_elpased: 1.39
batch start
#iterations: 371
currently lose_sum: 90.67362636327744
time_elpased: 1.383
batch start
#iterations: 372
currently lose_sum: 91.20869916677475
time_elpased: 1.386
batch start
#iterations: 373
currently lose_sum: 91.27816009521484
time_elpased: 1.376
batch start
#iterations: 374
currently lose_sum: 91.12969022989273
time_elpased: 1.413
batch start
#iterations: 375
currently lose_sum: 90.88619482517242
time_elpased: 1.386
batch start
#iterations: 376
currently lose_sum: 91.08817487955093
time_elpased: 1.35
batch start
#iterations: 377
currently lose_sum: 91.47642642259598
time_elpased: 1.37
batch start
#iterations: 378
currently lose_sum: 91.22521793842316
time_elpased: 1.357
batch start
#iterations: 379
currently lose_sum: 90.88940906524658
time_elpased: 1.386
start validation test
0.63587628866
0.640118305693
0.623649274467
0.6317764804
0.635897755069
60.970
batch start
#iterations: 380
currently lose_sum: 91.2835938334465
time_elpased: 1.362
batch start
#iterations: 381
currently lose_sum: 91.18781214952469
time_elpased: 1.386
batch start
#iterations: 382
currently lose_sum: 91.2665786743164
time_elpased: 1.383
batch start
#iterations: 383
currently lose_sum: 90.65122675895691
time_elpased: 1.367
batch start
#iterations: 384
currently lose_sum: 90.63570982217789
time_elpased: 1.371
batch start
#iterations: 385
currently lose_sum: 91.27063113451004
time_elpased: 1.478
batch start
#iterations: 386
currently lose_sum: 90.97372353076935
time_elpased: 1.377
batch start
#iterations: 387
currently lose_sum: 91.35837656259537
time_elpased: 1.412
batch start
#iterations: 388
currently lose_sum: 91.43463158607483
time_elpased: 1.372
batch start
#iterations: 389
currently lose_sum: 91.1401988863945
time_elpased: 1.372
batch start
#iterations: 390
currently lose_sum: 91.03595161437988
time_elpased: 1.348
batch start
#iterations: 391
currently lose_sum: 91.16939240694046
time_elpased: 1.397
batch start
#iterations: 392
currently lose_sum: 91.4783039689064
time_elpased: 1.386
batch start
#iterations: 393
currently lose_sum: 90.9920437335968
time_elpased: 1.369
batch start
#iterations: 394
currently lose_sum: 91.12314975261688
time_elpased: 1.383
batch start
#iterations: 395
currently lose_sum: 91.00053882598877
time_elpased: 1.388
batch start
#iterations: 396
currently lose_sum: 91.23075747489929
time_elpased: 1.428
batch start
#iterations: 397
currently lose_sum: 91.2003909945488
time_elpased: 1.387
batch start
#iterations: 398
currently lose_sum: 91.13334947824478
time_elpased: 1.368
batch start
#iterations: 399
currently lose_sum: 90.9436075091362
time_elpased: 1.396
start validation test
0.631340206186
0.635527845292
0.618915303077
0.627111574557
0.631362020019
61.304
acc: 0.643
pre: 0.639
rec: 0.659
F1: 0.649
auc: 0.697
