start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.51841980218887
time_elpased: 2.465
batch start
#iterations: 1
currently lose_sum: 100.32462030649185
time_elpased: 2.361
batch start
#iterations: 2
currently lose_sum: 100.24687021970749
time_elpased: 2.371
batch start
#iterations: 3
currently lose_sum: 100.06437969207764
time_elpased: 2.482
batch start
#iterations: 4
currently lose_sum: 99.93844485282898
time_elpased: 1.917
batch start
#iterations: 5
currently lose_sum: 99.81292402744293
time_elpased: 2.286
batch start
#iterations: 6
currently lose_sum: 99.63443148136139
time_elpased: 2.393
batch start
#iterations: 7
currently lose_sum: 99.46382141113281
time_elpased: 2.13
batch start
#iterations: 8
currently lose_sum: 99.43163347244263
time_elpased: 2.461
batch start
#iterations: 9
currently lose_sum: 99.4155707359314
time_elpased: 2.281
batch start
#iterations: 10
currently lose_sum: 99.21513640880585
time_elpased: 2.361
batch start
#iterations: 11
currently lose_sum: 98.93627148866653
time_elpased: 2.565
batch start
#iterations: 12
currently lose_sum: 98.9996001124382
time_elpased: 2.257
batch start
#iterations: 13
currently lose_sum: 98.98802703619003
time_elpased: 2.248
batch start
#iterations: 14
currently lose_sum: 98.57206535339355
time_elpased: 2.326
batch start
#iterations: 15
currently lose_sum: 98.21902722120285
time_elpased: 2.244
batch start
#iterations: 16
currently lose_sum: 98.30163204669952
time_elpased: 2.417
batch start
#iterations: 17
currently lose_sum: 98.22212982177734
time_elpased: 2.517
batch start
#iterations: 18
currently lose_sum: 97.884142100811
time_elpased: 2.281
batch start
#iterations: 19
currently lose_sum: 97.62302082777023
time_elpased: 2.175
start validation test
0.592525773196
0.619841269841
0.482247607286
0.542455287376
0.592719383525
64.692
batch start
#iterations: 20
currently lose_sum: 97.58763486146927
time_elpased: 2.264
batch start
#iterations: 21
currently lose_sum: 97.3189886212349
time_elpased: 2.343
batch start
#iterations: 22
currently lose_sum: 97.54684174060822
time_elpased: 2.177
batch start
#iterations: 23
currently lose_sum: 97.78340357542038
time_elpased: 2.42
batch start
#iterations: 24
currently lose_sum: 97.2095155119896
time_elpased: 2.337
batch start
#iterations: 25
currently lose_sum: 97.03952032327652
time_elpased: 2.383
batch start
#iterations: 26
currently lose_sum: 97.15018928050995
time_elpased: 2.424
batch start
#iterations: 27
currently lose_sum: 96.73003470897675
time_elpased: 2.687
batch start
#iterations: 28
currently lose_sum: 96.73584538698196
time_elpased: 2.362
batch start
#iterations: 29
currently lose_sum: 97.07917886972427
time_elpased: 2.492
batch start
#iterations: 30
currently lose_sum: 96.2506954073906
time_elpased: 2.402
batch start
#iterations: 31
currently lose_sum: 96.35222864151001
time_elpased: 2.528
batch start
#iterations: 32
currently lose_sum: 96.43169432878494
time_elpased: 2.431
batch start
#iterations: 33
currently lose_sum: 96.45080029964447
time_elpased: 1.97
batch start
#iterations: 34
currently lose_sum: 96.25673419237137
time_elpased: 2.201
batch start
#iterations: 35
currently lose_sum: 96.17653834819794
time_elpased: 2.265
batch start
#iterations: 36
currently lose_sum: 96.47836941480637
time_elpased: 2.441
batch start
#iterations: 37
currently lose_sum: 95.8218206167221
time_elpased: 2.472
batch start
#iterations: 38
currently lose_sum: 96.05891090631485
time_elpased: 2.398
batch start
#iterations: 39
currently lose_sum: 95.98997581005096
time_elpased: 2.295
start validation test
0.633092783505
0.616893046685
0.705773386848
0.658346932898
0.632965181496
62.301
batch start
#iterations: 40
currently lose_sum: 95.82393503189087
time_elpased: 2.428
batch start
#iterations: 41
currently lose_sum: 96.27110970020294
time_elpased: 2.25
batch start
#iterations: 42
currently lose_sum: 95.99710607528687
time_elpased: 2.53
batch start
#iterations: 43
currently lose_sum: 95.87688779830933
time_elpased: 2.433
batch start
#iterations: 44
currently lose_sum: 95.60030007362366
time_elpased: 2.163
batch start
#iterations: 45
currently lose_sum: 95.337373316288
time_elpased: 2.439
batch start
#iterations: 46
currently lose_sum: 95.47260063886642
time_elpased: 2.458
batch start
#iterations: 47
currently lose_sum: 95.62820345163345
time_elpased: 2.447
batch start
#iterations: 48
currently lose_sum: 95.67224550247192
time_elpased: 2.353
batch start
#iterations: 49
currently lose_sum: 95.66101282835007
time_elpased: 2.469
batch start
#iterations: 50
currently lose_sum: 96.08188873529434
time_elpased: 2.404
batch start
#iterations: 51
currently lose_sum: 95.17042833566666
time_elpased: 2.397
batch start
#iterations: 52
currently lose_sum: 95.1821009516716
time_elpased: 2.406
batch start
#iterations: 53
currently lose_sum: 95.17072439193726
time_elpased: 2.217
batch start
#iterations: 54
currently lose_sum: 95.84789645671844
time_elpased: 2.306
batch start
#iterations: 55
currently lose_sum: 95.24662661552429
time_elpased: 2.215
batch start
#iterations: 56
currently lose_sum: 95.24374467134476
time_elpased: 2.423
batch start
#iterations: 57
currently lose_sum: 94.8141313791275
time_elpased: 2.387
batch start
#iterations: 58
currently lose_sum: 95.50184082984924
time_elpased: 2.438
batch start
#iterations: 59
currently lose_sum: 95.19084823131561
time_elpased: 2.337
start validation test
0.641701030928
0.624035874439
0.716064629001
0.666890305267
0.641570474162
61.496
batch start
#iterations: 60
currently lose_sum: 95.28442472219467
time_elpased: 2.493
batch start
#iterations: 61
currently lose_sum: 95.23329085111618
time_elpased: 2.157
batch start
#iterations: 62
currently lose_sum: 95.28597754240036
time_elpased: 2.104
batch start
#iterations: 63
currently lose_sum: 95.7856497168541
time_elpased: 2.484
batch start
#iterations: 64
currently lose_sum: 95.37134110927582
time_elpased: 2.488
batch start
#iterations: 65
currently lose_sum: 94.73556470870972
time_elpased: 2.424
batch start
#iterations: 66
currently lose_sum: 94.61716437339783
time_elpased: 2.441
batch start
#iterations: 67
currently lose_sum: 94.66222214698792
time_elpased: 2.589
batch start
#iterations: 68
currently lose_sum: 94.84795904159546
time_elpased: 2.297
batch start
#iterations: 69
currently lose_sum: 95.18423265218735
time_elpased: 2.298
batch start
#iterations: 70
currently lose_sum: 94.69529461860657
time_elpased: 2.259
batch start
#iterations: 71
currently lose_sum: 94.60204994678497
time_elpased: 2.346
batch start
#iterations: 72
currently lose_sum: 94.55674868822098
time_elpased: 2.324
batch start
#iterations: 73
currently lose_sum: 94.11587065458298
time_elpased: 2.648
batch start
#iterations: 74
currently lose_sum: 94.51388478279114
time_elpased: 2.439
batch start
#iterations: 75
currently lose_sum: 94.60420787334442
time_elpased: 2.339
batch start
#iterations: 76
currently lose_sum: 94.3562758564949
time_elpased: 2.412
batch start
#iterations: 77
currently lose_sum: 95.0030809044838
time_elpased: 2.432
batch start
#iterations: 78
currently lose_sum: 94.68274408578873
time_elpased: 2.397
batch start
#iterations: 79
currently lose_sum: 94.59864419698715
time_elpased: 2.288
start validation test
0.628402061856
0.622917075083
0.654008438819
0.638084241177
0.628357105911
61.815
batch start
#iterations: 80
currently lose_sum: 94.32312846183777
time_elpased: 2.305
batch start
#iterations: 81
currently lose_sum: 94.95722186565399
time_elpased: 2.321
batch start
#iterations: 82
currently lose_sum: 94.13329702615738
time_elpased: 2.344
batch start
#iterations: 83
currently lose_sum: 94.90636283159256
time_elpased: 2.473
batch start
#iterations: 84
currently lose_sum: 94.6749017238617
time_elpased: 2.457
batch start
#iterations: 85
currently lose_sum: 95.04984813928604
time_elpased: 2.296
batch start
#iterations: 86
currently lose_sum: 94.27733755111694
time_elpased: 2.281
batch start
#iterations: 87
currently lose_sum: 94.52773463726044
time_elpased: 2.295
batch start
#iterations: 88
currently lose_sum: 94.573351085186
time_elpased: 2.509
batch start
#iterations: 89
currently lose_sum: 94.30071419477463
time_elpased: 2.349
batch start
#iterations: 90
currently lose_sum: 94.55946958065033
time_elpased: 2.351
batch start
#iterations: 91
currently lose_sum: 94.13712465763092
time_elpased: 2.371
batch start
#iterations: 92
currently lose_sum: 94.22145104408264
time_elpased: 2.132
batch start
#iterations: 93
currently lose_sum: 94.05853074789047
time_elpased: 2.482
batch start
#iterations: 94
currently lose_sum: 93.97657644748688
time_elpased: 2.531
batch start
#iterations: 95
currently lose_sum: 94.51651710271835
time_elpased: 2.38
batch start
#iterations: 96
currently lose_sum: 94.12655633687973
time_elpased: 2.298
batch start
#iterations: 97
currently lose_sum: 94.38785082101822
time_elpased: 2.369
batch start
#iterations: 98
currently lose_sum: 94.21321588754654
time_elpased: 2.361
batch start
#iterations: 99
currently lose_sum: 93.96616578102112
time_elpased: 2.587
start validation test
0.638865979381
0.63049966304
0.673973448595
0.651512136888
0.638804342804
61.409
batch start
#iterations: 100
currently lose_sum: 93.91014695167542
time_elpased: 2.438
batch start
#iterations: 101
currently lose_sum: 94.12855768203735
time_elpased: 2.507
batch start
#iterations: 102
currently lose_sum: 93.9564957022667
time_elpased: 2.373
batch start
#iterations: 103
currently lose_sum: 94.03814548254013
time_elpased: 2.185
batch start
#iterations: 104
currently lose_sum: 93.92207336425781
time_elpased: 2.443
batch start
#iterations: 105
currently lose_sum: 93.5668021440506
time_elpased: 2.487
batch start
#iterations: 106
currently lose_sum: 93.55755525827408
time_elpased: 2.472
batch start
#iterations: 107
currently lose_sum: 93.96319514513016
time_elpased: 2.552
batch start
#iterations: 108
currently lose_sum: 93.45820754766464
time_elpased: 2.425
batch start
#iterations: 109
currently lose_sum: 93.74554324150085
time_elpased: 2.321
batch start
#iterations: 110
currently lose_sum: 94.12450575828552
time_elpased: 2.319
batch start
#iterations: 111
currently lose_sum: 93.93700867891312
time_elpased: 2.514
batch start
#iterations: 112
currently lose_sum: 93.82238811254501
time_elpased: 2.356
batch start
#iterations: 113
currently lose_sum: 93.50130605697632
time_elpased: 2.225
batch start
#iterations: 114
currently lose_sum: 94.15480411052704
time_elpased: 2.431
batch start
#iterations: 115
currently lose_sum: 93.40006130933762
time_elpased: 2.455
batch start
#iterations: 116
currently lose_sum: 93.72658663988113
time_elpased: 2.497
batch start
#iterations: 117
currently lose_sum: 93.52859216928482
time_elpased: 2.397
batch start
#iterations: 118
currently lose_sum: 93.5449270606041
time_elpased: 2.339
batch start
#iterations: 119
currently lose_sum: 93.58486294746399
time_elpased: 2.519
start validation test
0.618453608247
0.606027296876
0.680868580838
0.641271687506
0.618344029136
62.413
batch start
#iterations: 120
currently lose_sum: 93.72248893976212
time_elpased: 2.525
batch start
#iterations: 121
currently lose_sum: 93.62328088283539
time_elpased: 2.335
batch start
#iterations: 122
currently lose_sum: 93.46153140068054
time_elpased: 2.221
batch start
#iterations: 123
currently lose_sum: 93.3334773182869
time_elpased: 2.42
batch start
#iterations: 124
currently lose_sum: 94.00996148586273
time_elpased: 2.363
batch start
#iterations: 125
currently lose_sum: 93.81573116779327
time_elpased: 2.366
batch start
#iterations: 126
currently lose_sum: 93.91357886791229
time_elpased: 2.391
batch start
#iterations: 127
currently lose_sum: 93.89697396755219
time_elpased: 2.38
batch start
#iterations: 128
currently lose_sum: 93.80071437358856
time_elpased: 2.449
batch start
#iterations: 129
currently lose_sum: 93.459501683712
time_elpased: 2.579
batch start
#iterations: 130
currently lose_sum: 93.22150522470474
time_elpased: 2.375
batch start
#iterations: 131
currently lose_sum: 93.56917369365692
time_elpased: 2.189
batch start
#iterations: 132
currently lose_sum: 93.29206842184067
time_elpased: 2.239
batch start
#iterations: 133
currently lose_sum: 93.48204863071442
time_elpased: 2.361
batch start
#iterations: 134
currently lose_sum: 93.00030720233917
time_elpased: 2.362
batch start
#iterations: 135
currently lose_sum: 93.85525494813919
time_elpased: 2.352
batch start
#iterations: 136
currently lose_sum: 93.48028510808945
time_elpased: 2.466
batch start
#iterations: 137
currently lose_sum: 92.98867231607437
time_elpased: 2.342
batch start
#iterations: 138
currently lose_sum: 93.12517899274826
time_elpased: 2.271
batch start
#iterations: 139
currently lose_sum: 93.38809895515442
time_elpased: 2.132
start validation test
0.620927835052
0.613376835237
0.657816198415
0.634819743768
0.62086307184
62.151
batch start
#iterations: 140
currently lose_sum: 93.70866298675537
time_elpased: 2.129
batch start
#iterations: 141
currently lose_sum: 93.44104814529419
time_elpased: 2.327
batch start
#iterations: 142
currently lose_sum: 92.90664720535278
time_elpased: 2.211
batch start
#iterations: 143
currently lose_sum: 93.65705454349518
time_elpased: 2.441
batch start
#iterations: 144
currently lose_sum: 92.73943537473679
time_elpased: 2.345
batch start
#iterations: 145
currently lose_sum: 93.18400317430496
time_elpased: 2.47
batch start
#iterations: 146
currently lose_sum: 93.23929184675217
time_elpased: 2.426
batch start
#iterations: 147
currently lose_sum: 93.10962241888046
time_elpased: 2.537
batch start
#iterations: 148
currently lose_sum: 93.118885576725
time_elpased: 2.465
batch start
#iterations: 149
currently lose_sum: 93.46357309818268
time_elpased: 2.423
batch start
#iterations: 150
currently lose_sum: 93.00905442237854
time_elpased: 2.046
batch start
#iterations: 151
currently lose_sum: 93.46287298202515
time_elpased: 2.352
batch start
#iterations: 152
currently lose_sum: 92.92607694864273
time_elpased: 2.375
batch start
#iterations: 153
currently lose_sum: 93.38586497306824
time_elpased: 2.166
batch start
#iterations: 154
currently lose_sum: 93.0808715224266
time_elpased: 1.894
batch start
#iterations: 155
currently lose_sum: 93.26343190670013
time_elpased: 2.327
batch start
#iterations: 156
currently lose_sum: 92.47558689117432
time_elpased: 2.207
batch start
#iterations: 157
currently lose_sum: 92.84092915058136
time_elpased: 2.454
batch start
#iterations: 158
currently lose_sum: 93.32618016004562
time_elpased: 2.359
batch start
#iterations: 159
currently lose_sum: 93.33562606573105
time_elpased: 2.45
start validation test
0.637422680412
0.627072084873
0.681280230524
0.65305317155
0.637345681719
61.142
batch start
#iterations: 160
currently lose_sum: 93.31749159097672
time_elpased: 2.247
batch start
#iterations: 161
currently lose_sum: 92.956269800663
time_elpased: 2.462
batch start
#iterations: 162
currently lose_sum: 92.55140709877014
time_elpased: 2.519
batch start
#iterations: 163
currently lose_sum: 92.76922297477722
time_elpased: 2.367
batch start
#iterations: 164
currently lose_sum: 93.25149899721146
time_elpased: 2.416
batch start
#iterations: 165
currently lose_sum: 92.88004571199417
time_elpased: 2.449
batch start
#iterations: 166
currently lose_sum: 93.00896167755127
time_elpased: 2.357
batch start
#iterations: 167
currently lose_sum: 93.07611638307571
time_elpased: 2.597
batch start
#iterations: 168
currently lose_sum: 92.4691190123558
time_elpased: 2.523
batch start
#iterations: 169
currently lose_sum: 92.79685986042023
time_elpased: 2.553
batch start
#iterations: 170
currently lose_sum: 92.75480484962463
time_elpased: 2.502
batch start
#iterations: 171
currently lose_sum: 92.63654404878616
time_elpased: 2.382
batch start
#iterations: 172
currently lose_sum: 92.35009378194809
time_elpased: 2.466
batch start
#iterations: 173
currently lose_sum: 92.90514832735062
time_elpased: 2.399
batch start
#iterations: 174
currently lose_sum: 92.26860338449478
time_elpased: 2.43
batch start
#iterations: 175
currently lose_sum: 92.83080726861954
time_elpased: 2.372
batch start
#iterations: 176
currently lose_sum: 92.86586630344391
time_elpased: 2.363
batch start
#iterations: 177
currently lose_sum: 92.70578688383102
time_elpased: 2.392
batch start
#iterations: 178
currently lose_sum: 92.72305071353912
time_elpased: 2.419
batch start
#iterations: 179
currently lose_sum: 92.2956513762474
time_elpased: 2.311
start validation test
0.636134020619
0.63351416516
0.648965730164
0.641146865945
0.636111492573
61.432
batch start
#iterations: 180
currently lose_sum: 92.38123857975006
time_elpased: 2.136
batch start
#iterations: 181
currently lose_sum: 92.07925570011139
time_elpased: 2.416
batch start
#iterations: 182
currently lose_sum: 92.92868494987488
time_elpased: 2.429
batch start
#iterations: 183
currently lose_sum: 92.33268213272095
time_elpased: 2.292
batch start
#iterations: 184
currently lose_sum: 92.50769776105881
time_elpased: 2.276
batch start
#iterations: 185
currently lose_sum: 92.16743630170822
time_elpased: 2.445
batch start
#iterations: 186
currently lose_sum: 92.09767073392868
time_elpased: 2.187
batch start
#iterations: 187
currently lose_sum: 92.48718184232712
time_elpased: 2.311
batch start
#iterations: 188
currently lose_sum: 92.15588593482971
time_elpased: 2.245
batch start
#iterations: 189
currently lose_sum: 92.05730414390564
time_elpased: 2.51
batch start
#iterations: 190
currently lose_sum: 92.2315314412117
time_elpased: 2.348
batch start
#iterations: 191
currently lose_sum: 91.90346109867096
time_elpased: 2.383
batch start
#iterations: 192
currently lose_sum: 92.18128317594528
time_elpased: 2.242
batch start
#iterations: 193
currently lose_sum: 92.19403916597366
time_elpased: 2.385
batch start
#iterations: 194
currently lose_sum: 92.41519570350647
time_elpased: 2.481
batch start
#iterations: 195
currently lose_sum: 92.22955930233002
time_elpased: 2.215
batch start
#iterations: 196
currently lose_sum: 92.08797955513
time_elpased: 2.468
batch start
#iterations: 197
currently lose_sum: 92.08207565546036
time_elpased: 2.241
batch start
#iterations: 198
currently lose_sum: 92.65870273113251
time_elpased: 2.494
batch start
#iterations: 199
currently lose_sum: 92.6866158246994
time_elpased: 2.346
start validation test
0.633402061856
0.634681005067
0.631676443347
0.633175159893
0.633405091445
61.478
batch start
#iterations: 200
currently lose_sum: 92.54981392621994
time_elpased: 2.511
batch start
#iterations: 201
currently lose_sum: 91.90179997682571
time_elpased: 2.379
batch start
#iterations: 202
currently lose_sum: 92.11501705646515
time_elpased: 2.466
batch start
#iterations: 203
currently lose_sum: 92.28201079368591
time_elpased: 2.272
batch start
#iterations: 204
currently lose_sum: 92.11401933431625
time_elpased: 2.189
batch start
#iterations: 205
currently lose_sum: 92.34608924388885
time_elpased: 2.305
batch start
#iterations: 206
currently lose_sum: 92.24991571903229
time_elpased: 2.551
batch start
#iterations: 207
currently lose_sum: 92.07134002447128
time_elpased: 2.375
batch start
#iterations: 208
currently lose_sum: 91.87175261974335
time_elpased: 2.448
batch start
#iterations: 209
currently lose_sum: 91.86160236597061
time_elpased: 2.291
batch start
#iterations: 210
currently lose_sum: 92.29259473085403
time_elpased: 2.325
batch start
#iterations: 211
currently lose_sum: 92.40585964918137
time_elpased: 2.414
batch start
#iterations: 212
currently lose_sum: 91.99485701322556
time_elpased: 2.287
batch start
#iterations: 213
currently lose_sum: 91.43190693855286
time_elpased: 2.426
batch start
#iterations: 214
currently lose_sum: 91.3626606464386
time_elpased: 2.581
batch start
#iterations: 215
currently lose_sum: 91.80084997415543
time_elpased: 2.377
batch start
#iterations: 216
currently lose_sum: 92.20891839265823
time_elpased: 2.458
batch start
#iterations: 217
currently lose_sum: 91.77383989095688
time_elpased: 2.333
batch start
#iterations: 218
currently lose_sum: 91.92880660295486
time_elpased: 2.479
batch start
#iterations: 219
currently lose_sum: 92.07221060991287
time_elpased: 2.545
start validation test
0.635927835052
0.637001858352
0.634969640836
0.635984126166
0.635929517309
61.516
batch start
#iterations: 220
currently lose_sum: 92.09416097402573
time_elpased: 2.187
batch start
#iterations: 221
currently lose_sum: 91.8128582239151
time_elpased: 2.43
batch start
#iterations: 222
currently lose_sum: 91.74364197254181
time_elpased: 2.29
batch start
#iterations: 223
currently lose_sum: 91.59907042980194
time_elpased: 2.124
batch start
#iterations: 224
currently lose_sum: 92.13249433040619
time_elpased: 2.333
batch start
#iterations: 225
currently lose_sum: 91.9811874628067
time_elpased: 2.455
batch start
#iterations: 226
currently lose_sum: 91.77444213628769
time_elpased: 2.42
batch start
#iterations: 227
currently lose_sum: 91.45406323671341
time_elpased: 2.305
batch start
#iterations: 228
currently lose_sum: 91.63864350318909
time_elpased: 2.198
batch start
#iterations: 229
currently lose_sum: 91.75195729732513
time_elpased: 2.382
batch start
#iterations: 230
currently lose_sum: 91.3295236825943
time_elpased: 2.548
batch start
#iterations: 231
currently lose_sum: 91.34901237487793
time_elpased: 2.335
batch start
#iterations: 232
currently lose_sum: 91.3734483718872
time_elpased: 2.304
batch start
#iterations: 233
currently lose_sum: 91.59096378087997
time_elpased: 2.427
batch start
#iterations: 234
currently lose_sum: 91.68062961101532
time_elpased: 2.473
batch start
#iterations: 235
currently lose_sum: 91.21483594179153
time_elpased: 2.404
batch start
#iterations: 236
currently lose_sum: 91.83988815546036
time_elpased: 2.469
batch start
#iterations: 237
currently lose_sum: 91.89373505115509
time_elpased: 2.675
batch start
#iterations: 238
currently lose_sum: 91.40057599544525
time_elpased: 2.215
batch start
#iterations: 239
currently lose_sum: 90.99098753929138
time_elpased: 2.308
start validation test
0.634690721649
0.631289936102
0.65071524133
0.640855419855
0.634662588134
61.532
batch start
#iterations: 240
currently lose_sum: 91.61783754825592
time_elpased: 2.195
batch start
#iterations: 241
currently lose_sum: 91.46700537204742
time_elpased: 2.329
batch start
#iterations: 242
currently lose_sum: 91.57187789678574
time_elpased: 2.332
batch start
#iterations: 243
currently lose_sum: 91.65918689966202
time_elpased: 2.358
batch start
#iterations: 244
currently lose_sum: 91.63949757814407
time_elpased: 2.459
batch start
#iterations: 245
currently lose_sum: 91.38856947422028
time_elpased: 2.027
batch start
#iterations: 246
currently lose_sum: 91.6191748380661
time_elpased: 2.259
batch start
#iterations: 247
currently lose_sum: 91.70828855037689
time_elpased: 2.398
batch start
#iterations: 248
currently lose_sum: 91.50188970565796
time_elpased: 2.415
batch start
#iterations: 249
currently lose_sum: 91.38011085987091
time_elpased: 2.394
batch start
#iterations: 250
currently lose_sum: 91.52393412590027
time_elpased: 2.416
batch start
#iterations: 251
currently lose_sum: 90.8484520316124
time_elpased: 2.335
batch start
#iterations: 252
currently lose_sum: 91.02251517772675
time_elpased: 2.437
batch start
#iterations: 253
currently lose_sum: 91.65978717803955
time_elpased: 2.323
batch start
#iterations: 254
currently lose_sum: 91.07476204633713
time_elpased: 2.489
batch start
#iterations: 255
currently lose_sum: 91.38985764980316
time_elpased: 2.223
batch start
#iterations: 256
currently lose_sum: 91.18433916568756
time_elpased: 2.589
batch start
#iterations: 257
currently lose_sum: 91.73290526866913
time_elpased: 2.163
batch start
#iterations: 258
currently lose_sum: 91.14546006917953
time_elpased: 2.205
batch start
#iterations: 259
currently lose_sum: 91.03308427333832
time_elpased: 2.415
start validation test
0.634742268041
0.633134298148
0.643820109087
0.638432493112
0.634726330491
61.671
batch start
#iterations: 260
currently lose_sum: 91.0523629784584
time_elpased: 2.537
batch start
#iterations: 261
currently lose_sum: 91.31451588869095
time_elpased: 2.401
batch start
#iterations: 262
currently lose_sum: 91.74566042423248
time_elpased: 2.587
batch start
#iterations: 263
currently lose_sum: 91.07604163885117
time_elpased: 2.499
batch start
#iterations: 264
currently lose_sum: 91.37795823812485
time_elpased: 2.218
batch start
#iterations: 265
currently lose_sum: 90.69743919372559
time_elpased: 2.245
batch start
#iterations: 266
currently lose_sum: 91.23689472675323
time_elpased: 2.359
batch start
#iterations: 267
currently lose_sum: 91.3917191028595
time_elpased: 2.422
batch start
#iterations: 268
currently lose_sum: 90.72067248821259
time_elpased: 2.461
batch start
#iterations: 269
currently lose_sum: 90.81552648544312
time_elpased: 2.449
batch start
#iterations: 270
currently lose_sum: 91.64450240135193
time_elpased: 2.042
batch start
#iterations: 271
currently lose_sum: 91.25264412164688
time_elpased: 2.483
batch start
#iterations: 272
currently lose_sum: 90.55492603778839
time_elpased: 2.391
batch start
#iterations: 273
currently lose_sum: 91.25291407108307
time_elpased: 2.42
batch start
#iterations: 274
currently lose_sum: 91.17467188835144
time_elpased: 2.376
batch start
#iterations: 275
currently lose_sum: 90.6193972826004
time_elpased: 2.364
batch start
#iterations: 276
currently lose_sum: 90.90475589036942
time_elpased: 2.486
batch start
#iterations: 277
currently lose_sum: 90.71715950965881
time_elpased: 2.458
batch start
#iterations: 278
currently lose_sum: 91.13999354839325
time_elpased: 2.425
batch start
#iterations: 279
currently lose_sum: 90.84518271684647
time_elpased: 2.43
start validation test
0.633659793814
0.629876592357
0.651332715859
0.640424993676
0.633628766274
61.717
batch start
#iterations: 280
currently lose_sum: 90.91905498504639
time_elpased: 2.489
batch start
#iterations: 281
currently lose_sum: 91.11458504199982
time_elpased: 2.353
batch start
#iterations: 282
currently lose_sum: 90.7483788728714
time_elpased: 2.624
batch start
#iterations: 283
currently lose_sum: 90.72208893299103
time_elpased: 2.266
batch start
#iterations: 284
currently lose_sum: 90.56616508960724
time_elpased: 2.293
batch start
#iterations: 285
currently lose_sum: 90.6755051612854
time_elpased: 2.322
batch start
#iterations: 286
currently lose_sum: 90.63428908586502
time_elpased: 2.459
batch start
#iterations: 287
currently lose_sum: 91.04654449224472
time_elpased: 2.587
batch start
#iterations: 288
currently lose_sum: 90.75103449821472
time_elpased: 2.51
batch start
#iterations: 289
currently lose_sum: 90.38991528749466
time_elpased: 2.406
batch start
#iterations: 290
currently lose_sum: 90.4849624633789
time_elpased: 2.508
batch start
#iterations: 291
currently lose_sum: 90.7509064078331
time_elpased: 2.389
batch start
#iterations: 292
currently lose_sum: 91.12245780229568
time_elpased: 2.269
batch start
#iterations: 293
currently lose_sum: 90.39573973417282
time_elpased: 2.27
batch start
#iterations: 294
currently lose_sum: 90.2799898982048
time_elpased: 2.099
batch start
#iterations: 295
currently lose_sum: 90.2142054438591
time_elpased: 2.381
batch start
#iterations: 296
currently lose_sum: 90.34820520877838
time_elpased: 2.4
batch start
#iterations: 297
currently lose_sum: 90.43534874916077
time_elpased: 2.391
batch start
#iterations: 298
currently lose_sum: 90.96337813138962
time_elpased: 2.393
batch start
#iterations: 299
currently lose_sum: 90.38811564445496
time_elpased: 1.952
start validation test
0.629020618557
0.641636690647
0.587424102089
0.613334766024
0.629093647657
62.331
batch start
#iterations: 300
currently lose_sum: 90.47369229793549
time_elpased: 2.431
batch start
#iterations: 301
currently lose_sum: 90.76051890850067
time_elpased: 2.362
batch start
#iterations: 302
currently lose_sum: 90.85052716732025
time_elpased: 2.489
batch start
#iterations: 303
currently lose_sum: 90.62212228775024
time_elpased: 2.541
batch start
#iterations: 304
currently lose_sum: 90.62498033046722
time_elpased: 2.627
batch start
#iterations: 305
currently lose_sum: 90.65373355150223
time_elpased: 2.403
batch start
#iterations: 306
currently lose_sum: 90.64574217796326
time_elpased: 2.464
batch start
#iterations: 307
currently lose_sum: 90.43539452552795
time_elpased: 2.429
batch start
#iterations: 308
currently lose_sum: 90.65719532966614
time_elpased: 2.25
batch start
#iterations: 309
currently lose_sum: 90.39526432752609
time_elpased: 2.325
batch start
#iterations: 310
currently lose_sum: 90.51423913240433
time_elpased: 2.513
batch start
#iterations: 311
currently lose_sum: 90.36355745792389
time_elpased: 2.215
batch start
#iterations: 312
currently lose_sum: 90.22447580099106
time_elpased: 2.177
batch start
#iterations: 313
currently lose_sum: 89.59146630764008
time_elpased: 2.331
batch start
#iterations: 314
currently lose_sum: 90.32154774665833
time_elpased: 2.298
batch start
#iterations: 315
currently lose_sum: 90.36824315786362
time_elpased: 2.035
batch start
#iterations: 316
currently lose_sum: 90.34711235761642
time_elpased: 2.508
batch start
#iterations: 317
currently lose_sum: 90.43341773748398
time_elpased: 2.476
batch start
#iterations: 318
currently lose_sum: 90.42204767465591
time_elpased: 2.477
batch start
#iterations: 319
currently lose_sum: 89.7117629647255
time_elpased: 2.421
start validation test
0.631443298969
0.630849220104
0.636822064423
0.633821571238
0.631433855717
62.041
batch start
#iterations: 320
currently lose_sum: 90.36314558982849
time_elpased: 2.333
batch start
#iterations: 321
currently lose_sum: 90.19161891937256
time_elpased: 2.4
batch start
#iterations: 322
currently lose_sum: 90.14767611026764
time_elpased: 2.378
batch start
#iterations: 323
currently lose_sum: 90.18029934167862
time_elpased: 2.463
batch start
#iterations: 324
currently lose_sum: 90.91873896121979
time_elpased: 2.468
batch start
#iterations: 325
currently lose_sum: 90.19772577285767
time_elpased: 2.382
batch start
#iterations: 326
currently lose_sum: 90.04876786470413
time_elpased: 2.391
batch start
#iterations: 327
currently lose_sum: 90.17314130067825
time_elpased: 2.268
batch start
#iterations: 328
currently lose_sum: 89.74321937561035
time_elpased: 2.241
batch start
#iterations: 329
currently lose_sum: 89.43250262737274
time_elpased: 2.485
batch start
#iterations: 330
currently lose_sum: 89.46775579452515
time_elpased: 2.446
batch start
#iterations: 331
currently lose_sum: 89.93593323230743
time_elpased: 2.376
batch start
#iterations: 332
currently lose_sum: 89.64093816280365
time_elpased: 2.048
batch start
#iterations: 333
currently lose_sum: 90.0148674249649
time_elpased: 2.386
batch start
#iterations: 334
currently lose_sum: 90.08678460121155
time_elpased: 2.547
batch start
#iterations: 335
currently lose_sum: 90.23080241680145
time_elpased: 2.313
batch start
#iterations: 336
currently lose_sum: 89.70181429386139
time_elpased: 2.271
batch start
#iterations: 337
currently lose_sum: 90.13596594333649
time_elpased: 2.406
batch start
#iterations: 338
currently lose_sum: 90.0594374537468
time_elpased: 2.481
batch start
#iterations: 339
currently lose_sum: 89.85727018117905
time_elpased: 2.339
start validation test
0.628608247423
0.628767685052
0.631161881239
0.629962508346
0.628603764125
62.003
batch start
#iterations: 340
currently lose_sum: 90.09179204702377
time_elpased: 2.424
batch start
#iterations: 341
currently lose_sum: 89.85507488250732
time_elpased: 2.488
batch start
#iterations: 342
currently lose_sum: 89.81865018606186
time_elpased: 2.403
batch start
#iterations: 343
currently lose_sum: 90.26014965772629
time_elpased: 2.231
batch start
#iterations: 344
currently lose_sum: 89.57460731267929
time_elpased: 2.447
batch start
#iterations: 345
currently lose_sum: 89.74295777082443
time_elpased: 2.325
batch start
#iterations: 346
currently lose_sum: 89.6126366853714
time_elpased: 2.283
batch start
#iterations: 347
currently lose_sum: 89.81012374162674
time_elpased: 2.382
batch start
#iterations: 348
currently lose_sum: 90.05302441120148
time_elpased: 2.481
batch start
#iterations: 349
currently lose_sum: 89.88200706243515
time_elpased: 2.466
batch start
#iterations: 350
currently lose_sum: 89.09731709957123
time_elpased: 2.456
batch start
#iterations: 351
currently lose_sum: 89.13594657182693
time_elpased: 2.184
batch start
#iterations: 352
currently lose_sum: 89.62235856056213
time_elpased: 2.419
batch start
#iterations: 353
currently lose_sum: 90.00723737478256
time_elpased: 2.452
batch start
#iterations: 354
currently lose_sum: 90.25668263435364
time_elpased: 2.276
batch start
#iterations: 355
currently lose_sum: 90.03828310966492
time_elpased: 2.432
batch start
#iterations: 356
currently lose_sum: 89.55203461647034
time_elpased: 2.366
batch start
#iterations: 357
currently lose_sum: 89.78985732793808
time_elpased: 2.077
batch start
#iterations: 358
currently lose_sum: 89.80997204780579
time_elpased: 2.379
batch start
#iterations: 359
currently lose_sum: 89.81894046068192
time_elpased: 2.283
start validation test
0.627319587629
0.642685025818
0.576412472985
0.607747395833
0.627408962921
62.956
batch start
#iterations: 360
currently lose_sum: 89.53011918067932
time_elpased: 2.296
batch start
#iterations: 361
currently lose_sum: 90.01512461900711
time_elpased: 2.443
batch start
#iterations: 362
currently lose_sum: 89.35290014743805
time_elpased: 2.513
batch start
#iterations: 363
currently lose_sum: 89.44377261400223
time_elpased: 2.187
batch start
#iterations: 364
currently lose_sum: 89.99344897270203
time_elpased: 2.279
batch start
#iterations: 365
currently lose_sum: 89.80499458312988
time_elpased: 2.374
batch start
#iterations: 366
currently lose_sum: 89.1925500035286
time_elpased: 2.431
batch start
#iterations: 367
currently lose_sum: 89.6790434718132
time_elpased: 2.344
batch start
#iterations: 368
currently lose_sum: 89.10767954587936
time_elpased: 2.547
batch start
#iterations: 369
currently lose_sum: 89.45481163263321
time_elpased: 2.208
batch start
#iterations: 370
currently lose_sum: 89.25180172920227
time_elpased: 2.334
batch start
#iterations: 371
currently lose_sum: 88.6338403224945
time_elpased: 2.584
batch start
#iterations: 372
currently lose_sum: 89.30456274747849
time_elpased: 2.568
batch start
#iterations: 373
currently lose_sum: 89.38616412878036
time_elpased: 2.135
batch start
#iterations: 374
currently lose_sum: 88.95651787519455
time_elpased: 2.294
batch start
#iterations: 375
currently lose_sum: 89.25182449817657
time_elpased: 2.329
batch start
#iterations: 376
currently lose_sum: 89.39739841222763
time_elpased: 2.431
batch start
#iterations: 377
currently lose_sum: 89.48628425598145
time_elpased: 2.204
batch start
#iterations: 378
currently lose_sum: 89.25129771232605
time_elpased: 2.185
batch start
#iterations: 379
currently lose_sum: 89.49510037899017
time_elpased: 2.168
start validation test
0.624948453608
0.63369481871
0.595348358547
0.613923378966
0.625000421141
62.925
batch start
#iterations: 380
currently lose_sum: 89.00621503591537
time_elpased: 2.386
batch start
#iterations: 381
currently lose_sum: 89.0020843744278
time_elpased: 2.356
batch start
#iterations: 382
currently lose_sum: 88.83259373903275
time_elpased: 2.52
batch start
#iterations: 383
currently lose_sum: 89.25356614589691
time_elpased: 2.147
batch start
#iterations: 384
currently lose_sum: 89.75696951150894
time_elpased: 2.008
batch start
#iterations: 385
currently lose_sum: 88.68149554729462
time_elpased: 2.312
batch start
#iterations: 386
currently lose_sum: 89.14016753435135
time_elpased: 2.496
batch start
#iterations: 387
currently lose_sum: 89.32876026630402
time_elpased: 2.337
batch start
#iterations: 388
currently lose_sum: 89.20056194067001
time_elpased: 2.379
batch start
#iterations: 389
currently lose_sum: 89.37305861711502
time_elpased: 2.493
batch start
#iterations: 390
currently lose_sum: 88.92580151557922
time_elpased: 2.109
batch start
#iterations: 391
currently lose_sum: 88.93703430891037
time_elpased: 2.336
batch start
#iterations: 392
currently lose_sum: 88.8111252784729
time_elpased: 2.363
batch start
#iterations: 393
currently lose_sum: 88.90698742866516
time_elpased: 2.278
batch start
#iterations: 394
currently lose_sum: 88.60674649477005
time_elpased: 2.497
batch start
#iterations: 395
currently lose_sum: 88.72879844903946
time_elpased: 2.535
batch start
#iterations: 396
currently lose_sum: 88.9645431637764
time_elpased: 2.37
batch start
#iterations: 397
currently lose_sum: 88.83347344398499
time_elpased: 2.542
batch start
#iterations: 398
currently lose_sum: 88.66799229383469
time_elpased: 2.489
batch start
#iterations: 399
currently lose_sum: 88.94264364242554
time_elpased: 2.455
start validation test
0.61675257732
0.619752308984
0.60769784913
0.613665887243
0.616768474292
63.208
acc: 0.640
pre: 0.629
rec: 0.685
F1: 0.656
auc: 0.694
