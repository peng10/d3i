start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.50103306770325
time_elpased: 1.57
batch start
#iterations: 1
currently lose_sum: 100.3437237739563
time_elpased: 1.516
batch start
#iterations: 2
currently lose_sum: 100.19533312320709
time_elpased: 1.516
batch start
#iterations: 3
currently lose_sum: 100.02201068401337
time_elpased: 1.531
batch start
#iterations: 4
currently lose_sum: 99.89628320932388
time_elpased: 1.539
batch start
#iterations: 5
currently lose_sum: 99.84919047355652
time_elpased: 1.533
batch start
#iterations: 6
currently lose_sum: 99.58185625076294
time_elpased: 1.516
batch start
#iterations: 7
currently lose_sum: 99.44720846414566
time_elpased: 1.567
batch start
#iterations: 8
currently lose_sum: 99.53151261806488
time_elpased: 1.599
batch start
#iterations: 9
currently lose_sum: 99.21453833580017
time_elpased: 1.529
batch start
#iterations: 10
currently lose_sum: 99.05580246448517
time_elpased: 1.52
batch start
#iterations: 11
currently lose_sum: 98.83356112241745
time_elpased: 1.615
batch start
#iterations: 12
currently lose_sum: 98.77679151296616
time_elpased: 1.551
batch start
#iterations: 13
currently lose_sum: 98.55384236574173
time_elpased: 1.553
batch start
#iterations: 14
currently lose_sum: 98.71809220314026
time_elpased: 1.499
batch start
#iterations: 15
currently lose_sum: 98.54078751802444
time_elpased: 1.537
batch start
#iterations: 16
currently lose_sum: 98.31065613031387
time_elpased: 1.518
batch start
#iterations: 17
currently lose_sum: 98.37094849348068
time_elpased: 1.581
batch start
#iterations: 18
currently lose_sum: 98.21037125587463
time_elpased: 1.53
batch start
#iterations: 19
currently lose_sum: 97.88284462690353
time_elpased: 1.528
start validation test
0.605051546392
0.607381848599
0.59787978592
0.602593360996
0.605063395645
63.946
batch start
#iterations: 20
currently lose_sum: 97.84506034851074
time_elpased: 1.521
batch start
#iterations: 21
currently lose_sum: 97.82740122079849
time_elpased: 1.533
batch start
#iterations: 22
currently lose_sum: 97.76019215583801
time_elpased: 1.499
batch start
#iterations: 23
currently lose_sum: 97.63833278417587
time_elpased: 1.527
batch start
#iterations: 24
currently lose_sum: 97.47851514816284
time_elpased: 1.519
batch start
#iterations: 25
currently lose_sum: 96.96849578619003
time_elpased: 1.522
batch start
#iterations: 26
currently lose_sum: 97.52190661430359
time_elpased: 1.53
batch start
#iterations: 27
currently lose_sum: 97.26081782579422
time_elpased: 1.503
batch start
#iterations: 28
currently lose_sum: 96.95341503620148
time_elpased: 1.558
batch start
#iterations: 29
currently lose_sum: 97.2617500424385
time_elpased: 1.534
batch start
#iterations: 30
currently lose_sum: 97.19807451963425
time_elpased: 1.528
batch start
#iterations: 31
currently lose_sum: 96.93235504627228
time_elpased: 1.574
batch start
#iterations: 32
currently lose_sum: 96.71911549568176
time_elpased: 1.534
batch start
#iterations: 33
currently lose_sum: 96.94956338405609
time_elpased: 1.578
batch start
#iterations: 34
currently lose_sum: 97.15833926200867
time_elpased: 1.492
batch start
#iterations: 35
currently lose_sum: 96.73273229598999
time_elpased: 1.546
batch start
#iterations: 36
currently lose_sum: 96.64760208129883
time_elpased: 1.493
batch start
#iterations: 37
currently lose_sum: 96.68665736913681
time_elpased: 1.516
batch start
#iterations: 38
currently lose_sum: 96.6987829208374
time_elpased: 1.491
batch start
#iterations: 39
currently lose_sum: 96.9388410449028
time_elpased: 1.526
start validation test
0.612886597938
0.609794943261
0.630506381227
0.619977735047
0.612857486359
63.326
batch start
#iterations: 40
currently lose_sum: 96.55958610773087
time_elpased: 1.501
batch start
#iterations: 41
currently lose_sum: 96.67382699251175
time_elpased: 1.509
batch start
#iterations: 42
currently lose_sum: 96.43701177835464
time_elpased: 1.49
batch start
#iterations: 43
currently lose_sum: 96.17405116558075
time_elpased: 1.521
batch start
#iterations: 44
currently lose_sum: 96.4185528755188
time_elpased: 1.533
batch start
#iterations: 45
currently lose_sum: 96.53582519292831
time_elpased: 1.535
batch start
#iterations: 46
currently lose_sum: 96.2952167391777
time_elpased: 1.531
batch start
#iterations: 47
currently lose_sum: 96.04707115888596
time_elpased: 1.528
batch start
#iterations: 48
currently lose_sum: 96.28376895189285
time_elpased: 1.519
batch start
#iterations: 49
currently lose_sum: 96.17098325490952
time_elpased: 1.519
batch start
#iterations: 50
currently lose_sum: 96.23506098985672
time_elpased: 1.56
batch start
#iterations: 51
currently lose_sum: 96.08130288124084
time_elpased: 1.531
batch start
#iterations: 52
currently lose_sum: 96.03010487556458
time_elpased: 1.537
batch start
#iterations: 53
currently lose_sum: 96.11084747314453
time_elpased: 1.591
batch start
#iterations: 54
currently lose_sum: 96.3209337592125
time_elpased: 1.561
batch start
#iterations: 55
currently lose_sum: 96.17790967226028
time_elpased: 1.579
batch start
#iterations: 56
currently lose_sum: 96.19317090511322
time_elpased: 1.564
batch start
#iterations: 57
currently lose_sum: 95.58806347846985
time_elpased: 1.51
batch start
#iterations: 58
currently lose_sum: 96.0545602440834
time_elpased: 1.531
batch start
#iterations: 59
currently lose_sum: 95.88411206007004
time_elpased: 1.532
start validation test
0.611340206186
0.590185676393
0.732811856731
0.653810835629
0.61113950953
63.147
batch start
#iterations: 60
currently lose_sum: 95.6964362859726
time_elpased: 1.564
batch start
#iterations: 61
currently lose_sum: 96.04093706607819
time_elpased: 1.546
batch start
#iterations: 62
currently lose_sum: 96.04506301879883
time_elpased: 1.579
batch start
#iterations: 63
currently lose_sum: 95.88440102338791
time_elpased: 1.531
batch start
#iterations: 64
currently lose_sum: 95.99430656433105
time_elpased: 1.512
batch start
#iterations: 65
currently lose_sum: 95.49185943603516
time_elpased: 1.576
batch start
#iterations: 66
currently lose_sum: 96.10980653762817
time_elpased: 1.591
batch start
#iterations: 67
currently lose_sum: 95.94458740949631
time_elpased: 1.608
batch start
#iterations: 68
currently lose_sum: 95.40166020393372
time_elpased: 1.544
batch start
#iterations: 69
currently lose_sum: 95.35009765625
time_elpased: 1.571
batch start
#iterations: 70
currently lose_sum: 95.70476168394089
time_elpased: 1.511
batch start
#iterations: 71
currently lose_sum: 95.11848556995392
time_elpased: 1.544
batch start
#iterations: 72
currently lose_sum: 95.50688409805298
time_elpased: 1.498
batch start
#iterations: 73
currently lose_sum: 95.50834995508194
time_elpased: 1.606
batch start
#iterations: 74
currently lose_sum: 95.33650529384613
time_elpased: 1.529
batch start
#iterations: 75
currently lose_sum: 95.1621704697609
time_elpased: 1.504
batch start
#iterations: 76
currently lose_sum: 95.17403399944305
time_elpased: 1.562
batch start
#iterations: 77
currently lose_sum: 95.35265201330185
time_elpased: 1.533
batch start
#iterations: 78
currently lose_sum: 95.45280355215073
time_elpased: 1.592
batch start
#iterations: 79
currently lose_sum: 95.45949274301529
time_elpased: 1.531
start validation test
0.641082474227
0.632165146423
0.677645121449
0.654115543192
0.641022065062
61.355
batch start
#iterations: 80
currently lose_sum: 95.08154273033142
time_elpased: 1.547
batch start
#iterations: 81
currently lose_sum: 95.22808277606964
time_elpased: 1.518
batch start
#iterations: 82
currently lose_sum: 95.40727680921555
time_elpased: 1.548
batch start
#iterations: 83
currently lose_sum: 95.38892978429794
time_elpased: 1.568
batch start
#iterations: 84
currently lose_sum: 94.84268248081207
time_elpased: 1.582
batch start
#iterations: 85
currently lose_sum: 94.83722847700119
time_elpased: 1.539
batch start
#iterations: 86
currently lose_sum: 94.95020043849945
time_elpased: 1.553
batch start
#iterations: 87
currently lose_sum: 94.72537404298782
time_elpased: 1.517
batch start
#iterations: 88
currently lose_sum: 95.40069246292114
time_elpased: 1.594
batch start
#iterations: 89
currently lose_sum: 95.041100025177
time_elpased: 1.531
batch start
#iterations: 90
currently lose_sum: 95.16920053958893
time_elpased: 1.517
batch start
#iterations: 91
currently lose_sum: 94.61824399232864
time_elpased: 1.537
batch start
#iterations: 92
currently lose_sum: 94.9247755408287
time_elpased: 1.623
batch start
#iterations: 93
currently lose_sum: 94.55709517002106
time_elpased: 1.569
batch start
#iterations: 94
currently lose_sum: 94.7853302359581
time_elpased: 1.562
batch start
#iterations: 95
currently lose_sum: 94.67690247297287
time_elpased: 1.58
batch start
#iterations: 96
currently lose_sum: 94.60738068819046
time_elpased: 1.527
batch start
#iterations: 97
currently lose_sum: 94.66448050737381
time_elpased: 1.596
batch start
#iterations: 98
currently lose_sum: 94.66352260112762
time_elpased: 1.535
batch start
#iterations: 99
currently lose_sum: 94.68929302692413
time_elpased: 1.518
start validation test
0.635567010309
0.643617021277
0.61023054755
0.626479289941
0.635608871462
61.756
batch start
#iterations: 100
currently lose_sum: 94.59195256233215
time_elpased: 1.578
batch start
#iterations: 101
currently lose_sum: 94.9515717625618
time_elpased: 1.562
batch start
#iterations: 102
currently lose_sum: 94.42514139413834
time_elpased: 1.624
batch start
#iterations: 103
currently lose_sum: 94.76495444774628
time_elpased: 1.549
batch start
#iterations: 104
currently lose_sum: 94.74465262889862
time_elpased: 1.612
batch start
#iterations: 105
currently lose_sum: 94.47395944595337
time_elpased: 1.551
batch start
#iterations: 106
currently lose_sum: 94.6295838356018
time_elpased: 1.51
batch start
#iterations: 107
currently lose_sum: 94.67072892189026
time_elpased: 1.523
batch start
#iterations: 108
currently lose_sum: 94.15404218435287
time_elpased: 1.519
batch start
#iterations: 109
currently lose_sum: 94.5362611413002
time_elpased: 1.546
batch start
#iterations: 110
currently lose_sum: 94.76984733343124
time_elpased: 1.525
batch start
#iterations: 111
currently lose_sum: 94.44469618797302
time_elpased: 1.579
batch start
#iterations: 112
currently lose_sum: 94.19649654626846
time_elpased: 1.565
batch start
#iterations: 113
currently lose_sum: 93.87343829870224
time_elpased: 1.52
batch start
#iterations: 114
currently lose_sum: 94.48661029338837
time_elpased: 1.532
batch start
#iterations: 115
currently lose_sum: 94.34192931652069
time_elpased: 1.524
batch start
#iterations: 116
currently lose_sum: 94.77513635158539
time_elpased: 1.561
batch start
#iterations: 117
currently lose_sum: 94.00646734237671
time_elpased: 1.617
batch start
#iterations: 118
currently lose_sum: 94.75445139408112
time_elpased: 1.548
batch start
#iterations: 119
currently lose_sum: 94.39898920059204
time_elpased: 1.512
start validation test
0.620927835052
0.601722652885
0.719020172911
0.655162712182
0.620765765927
62.151
batch start
#iterations: 120
currently lose_sum: 94.36731088161469
time_elpased: 1.568
batch start
#iterations: 121
currently lose_sum: 94.16187053918839
time_elpased: 1.588
batch start
#iterations: 122
currently lose_sum: 94.07778769731522
time_elpased: 1.574
batch start
#iterations: 123
currently lose_sum: 94.32192200422287
time_elpased: 1.513
batch start
#iterations: 124
currently lose_sum: 94.56298410892487
time_elpased: 1.538
batch start
#iterations: 125
currently lose_sum: 94.69038814306259
time_elpased: 1.538
batch start
#iterations: 126
currently lose_sum: 93.9165632724762
time_elpased: 1.534
batch start
#iterations: 127
currently lose_sum: 94.07234156131744
time_elpased: 1.592
batch start
#iterations: 128
currently lose_sum: 94.08720588684082
time_elpased: 1.553
batch start
#iterations: 129
currently lose_sum: 93.61067456007004
time_elpased: 1.593
batch start
#iterations: 130
currently lose_sum: 94.3517102599144
time_elpased: 1.514
batch start
#iterations: 131
currently lose_sum: 94.27813673019409
time_elpased: 1.555
batch start
#iterations: 132
currently lose_sum: 94.08593463897705
time_elpased: 1.528
batch start
#iterations: 133
currently lose_sum: 93.8978716135025
time_elpased: 1.589
batch start
#iterations: 134
currently lose_sum: 93.86171895265579
time_elpased: 1.549
batch start
#iterations: 135
currently lose_sum: 94.11820471286774
time_elpased: 1.522
batch start
#iterations: 136
currently lose_sum: 94.22255563735962
time_elpased: 1.535
batch start
#iterations: 137
currently lose_sum: 93.94038373231888
time_elpased: 1.572
batch start
#iterations: 138
currently lose_sum: 93.77482295036316
time_elpased: 1.568
batch start
#iterations: 139
currently lose_sum: 93.64617812633514
time_elpased: 1.54
start validation test
0.649896907216
0.63554607825
0.705537258131
0.668715247293
0.649804977682
60.625
batch start
#iterations: 140
currently lose_sum: 93.68718826770782
time_elpased: 1.537
batch start
#iterations: 141
currently lose_sum: 93.54613679647446
time_elpased: 1.537
batch start
#iterations: 142
currently lose_sum: 93.5633847117424
time_elpased: 1.537
batch start
#iterations: 143
currently lose_sum: 93.92518043518066
time_elpased: 1.54
batch start
#iterations: 144
currently lose_sum: 93.64445853233337
time_elpased: 1.625
batch start
#iterations: 145
currently lose_sum: 93.58279383182526
time_elpased: 1.524
batch start
#iterations: 146
currently lose_sum: 93.47290802001953
time_elpased: 1.512
batch start
#iterations: 147
currently lose_sum: 93.72553706169128
time_elpased: 1.546
batch start
#iterations: 148
currently lose_sum: 93.98037707805634
time_elpased: 1.517
batch start
#iterations: 149
currently lose_sum: 93.74980354309082
time_elpased: 1.578
batch start
#iterations: 150
currently lose_sum: 93.95569151639938
time_elpased: 1.587
batch start
#iterations: 151
currently lose_sum: 94.07436764240265
time_elpased: 1.498
batch start
#iterations: 152
currently lose_sum: 93.961798787117
time_elpased: 1.54
batch start
#iterations: 153
currently lose_sum: 93.7360548377037
time_elpased: 1.584
batch start
#iterations: 154
currently lose_sum: 93.80565237998962
time_elpased: 1.53
batch start
#iterations: 155
currently lose_sum: 93.76591485738754
time_elpased: 1.528
batch start
#iterations: 156
currently lose_sum: 93.51768445968628
time_elpased: 1.551
batch start
#iterations: 157
currently lose_sum: 93.0487322807312
time_elpased: 1.526
batch start
#iterations: 158
currently lose_sum: 93.58248859643936
time_elpased: 1.544
batch start
#iterations: 159
currently lose_sum: 93.37681168317795
time_elpased: 1.533
start validation test
0.633505154639
0.617049946101
0.706978180321
0.658960092095
0.633383761784
61.408
batch start
#iterations: 160
currently lose_sum: 93.32753252983093
time_elpased: 1.558
batch start
#iterations: 161
currently lose_sum: 93.70785111188889
time_elpased: 1.555
batch start
#iterations: 162
currently lose_sum: 93.35445815324783
time_elpased: 1.54
batch start
#iterations: 163
currently lose_sum: 93.66650825738907
time_elpased: 1.554
batch start
#iterations: 164
currently lose_sum: 93.46237230300903
time_elpased: 1.555
batch start
#iterations: 165
currently lose_sum: 93.71584045886993
time_elpased: 1.543
batch start
#iterations: 166
currently lose_sum: 93.57899975776672
time_elpased: 1.529
batch start
#iterations: 167
currently lose_sum: 93.16408568620682
time_elpased: 1.524
batch start
#iterations: 168
currently lose_sum: 93.44338297843933
time_elpased: 1.515
batch start
#iterations: 169
currently lose_sum: 93.10408860445023
time_elpased: 1.506
batch start
#iterations: 170
currently lose_sum: 93.19587451219559
time_elpased: 1.483
batch start
#iterations: 171
currently lose_sum: 93.03081357479095
time_elpased: 1.424
batch start
#iterations: 172
currently lose_sum: 93.22160291671753
time_elpased: 1.439
batch start
#iterations: 173
currently lose_sum: 93.36935079097748
time_elpased: 1.426
batch start
#iterations: 174
currently lose_sum: 93.04404026269913
time_elpased: 1.431
batch start
#iterations: 175
currently lose_sum: 92.96986615657806
time_elpased: 1.452
batch start
#iterations: 176
currently lose_sum: 93.44990676641464
time_elpased: 1.476
batch start
#iterations: 177
currently lose_sum: 93.6628400683403
time_elpased: 1.439
batch start
#iterations: 178
currently lose_sum: 93.34026199579239
time_elpased: 1.42
batch start
#iterations: 179
currently lose_sum: 92.74254494905472
time_elpased: 1.454
start validation test
0.646288659794
0.639929397921
0.671675586661
0.655418298684
0.646246715264
60.691
batch start
#iterations: 180
currently lose_sum: 92.7663009762764
time_elpased: 1.424
batch start
#iterations: 181
currently lose_sum: 93.26824802160263
time_elpased: 1.419
batch start
#iterations: 182
currently lose_sum: 93.62536460161209
time_elpased: 1.474
batch start
#iterations: 183
currently lose_sum: 93.17281532287598
time_elpased: 1.481
batch start
#iterations: 184
currently lose_sum: 93.13582503795624
time_elpased: 1.466
batch start
#iterations: 185
currently lose_sum: 93.31560373306274
time_elpased: 1.418
batch start
#iterations: 186
currently lose_sum: 92.82336246967316
time_elpased: 1.494
batch start
#iterations: 187
currently lose_sum: 93.15806120634079
time_elpased: 1.436
batch start
#iterations: 188
currently lose_sum: 93.39030265808105
time_elpased: 1.445
batch start
#iterations: 189
currently lose_sum: 93.26761865615845
time_elpased: 1.471
batch start
#iterations: 190
currently lose_sum: 93.19077479839325
time_elpased: 1.491
batch start
#iterations: 191
currently lose_sum: 92.72466170787811
time_elpased: 1.511
batch start
#iterations: 192
currently lose_sum: 93.11912381649017
time_elpased: 1.406
batch start
#iterations: 193
currently lose_sum: 93.04192662239075
time_elpased: 1.448
batch start
#iterations: 194
currently lose_sum: 93.26700609922409
time_elpased: 1.412
batch start
#iterations: 195
currently lose_sum: 93.29528468847275
time_elpased: 1.453
batch start
#iterations: 196
currently lose_sum: 92.74168902635574
time_elpased: 1.453
batch start
#iterations: 197
currently lose_sum: 93.11403179168701
time_elpased: 1.424
batch start
#iterations: 198
currently lose_sum: 92.71871781349182
time_elpased: 1.429
batch start
#iterations: 199
currently lose_sum: 92.49868333339691
time_elpased: 1.43
start validation test
0.636958762887
0.633931255637
0.651090983944
0.642396547347
0.636935413492
61.328
batch start
#iterations: 200
currently lose_sum: 93.0403184890747
time_elpased: 1.466
batch start
#iterations: 201
currently lose_sum: 92.78498268127441
time_elpased: 1.462
batch start
#iterations: 202
currently lose_sum: 92.87157839536667
time_elpased: 1.451
batch start
#iterations: 203
currently lose_sum: 93.02889788150787
time_elpased: 1.433
batch start
#iterations: 204
currently lose_sum: 93.0356730222702
time_elpased: 1.415
batch start
#iterations: 205
currently lose_sum: 92.72291034460068
time_elpased: 1.436
batch start
#iterations: 206
currently lose_sum: 92.84844571352005
time_elpased: 1.443
batch start
#iterations: 207
currently lose_sum: 92.57835918664932
time_elpased: 1.416
batch start
#iterations: 208
currently lose_sum: 92.39059942960739
time_elpased: 1.441
batch start
#iterations: 209
currently lose_sum: 92.93640577793121
time_elpased: 1.431
batch start
#iterations: 210
currently lose_sum: 92.5060304403305
time_elpased: 1.44
batch start
#iterations: 211
currently lose_sum: 92.05500727891922
time_elpased: 1.479
batch start
#iterations: 212
currently lose_sum: 92.62477272748947
time_elpased: 1.439
batch start
#iterations: 213
currently lose_sum: 92.91488337516785
time_elpased: 1.443
batch start
#iterations: 214
currently lose_sum: 92.59570729732513
time_elpased: 1.494
batch start
#iterations: 215
currently lose_sum: 92.38428592681885
time_elpased: 1.455
batch start
#iterations: 216
currently lose_sum: 92.3741010427475
time_elpased: 1.42
batch start
#iterations: 217
currently lose_sum: 92.39983212947845
time_elpased: 1.483
batch start
#iterations: 218
currently lose_sum: 92.6785791516304
time_elpased: 1.423
batch start
#iterations: 219
currently lose_sum: 92.26863098144531
time_elpased: 1.433
start validation test
0.629587628866
0.620384468976
0.670955125566
0.644679588608
0.629519281081
61.623
batch start
#iterations: 220
currently lose_sum: 92.85676115751266
time_elpased: 1.447
batch start
#iterations: 221
currently lose_sum: 92.173859000206
time_elpased: 1.437
batch start
#iterations: 222
currently lose_sum: 92.30555164813995
time_elpased: 1.418
batch start
#iterations: 223
currently lose_sum: 91.75161737203598
time_elpased: 1.434
batch start
#iterations: 224
currently lose_sum: 92.83939862251282
time_elpased: 1.475
batch start
#iterations: 225
currently lose_sum: 92.49097740650177
time_elpased: 1.487
batch start
#iterations: 226
currently lose_sum: 91.96079868078232
time_elpased: 1.438
batch start
#iterations: 227
currently lose_sum: 92.4352850317955
time_elpased: 1.422
batch start
#iterations: 228
currently lose_sum: 92.35224878787994
time_elpased: 1.477
batch start
#iterations: 229
currently lose_sum: 92.49824005365372
time_elpased: 1.423
batch start
#iterations: 230
currently lose_sum: 92.31764924526215
time_elpased: 1.443
batch start
#iterations: 231
currently lose_sum: 92.62334740161896
time_elpased: 1.418
batch start
#iterations: 232
currently lose_sum: 92.50081169605255
time_elpased: 1.435
batch start
#iterations: 233
currently lose_sum: 92.59022814035416
time_elpased: 1.416
batch start
#iterations: 234
currently lose_sum: 92.28456634283066
time_elpased: 1.446
batch start
#iterations: 235
currently lose_sum: 92.27796483039856
time_elpased: 1.472
batch start
#iterations: 236
currently lose_sum: 92.83084952831268
time_elpased: 1.506
batch start
#iterations: 237
currently lose_sum: 92.33088201284409
time_elpased: 1.419
batch start
#iterations: 238
currently lose_sum: 92.31979095935822
time_elpased: 1.438
batch start
#iterations: 239
currently lose_sum: 92.43240731954575
time_elpased: 1.453
start validation test
0.636237113402
0.639550750499
0.627109921779
0.633269240763
0.636252193438
61.523
batch start
#iterations: 240
currently lose_sum: 92.1481237411499
time_elpased: 1.495
batch start
#iterations: 241
currently lose_sum: 92.11472278833389
time_elpased: 1.444
batch start
#iterations: 242
currently lose_sum: 92.14329779148102
time_elpased: 1.44
batch start
#iterations: 243
currently lose_sum: 92.38937658071518
time_elpased: 1.432
batch start
#iterations: 244
currently lose_sum: 92.0266905426979
time_elpased: 1.475
batch start
#iterations: 245
currently lose_sum: 92.6119943857193
time_elpased: 1.464
batch start
#iterations: 246
currently lose_sum: 92.39382630586624
time_elpased: 1.452
batch start
#iterations: 247
currently lose_sum: 92.04761862754822
time_elpased: 1.478
batch start
#iterations: 248
currently lose_sum: 92.07642447948456
time_elpased: 1.448
batch start
#iterations: 249
currently lose_sum: 92.09306073188782
time_elpased: 1.427
batch start
#iterations: 250
currently lose_sum: 92.225312769413
time_elpased: 1.454
batch start
#iterations: 251
currently lose_sum: 92.11869370937347
time_elpased: 1.438
batch start
#iterations: 252
currently lose_sum: 92.4201642870903
time_elpased: 1.449
batch start
#iterations: 253
currently lose_sum: 92.14094704389572
time_elpased: 1.487
batch start
#iterations: 254
currently lose_sum: 91.62867087125778
time_elpased: 1.493
batch start
#iterations: 255
currently lose_sum: 91.63726758956909
time_elpased: 1.44
batch start
#iterations: 256
currently lose_sum: 92.44155550003052
time_elpased: 1.431
batch start
#iterations: 257
currently lose_sum: 91.88881295919418
time_elpased: 1.502
batch start
#iterations: 258
currently lose_sum: 91.79239994287491
time_elpased: 1.448
batch start
#iterations: 259
currently lose_sum: 92.01069176197052
time_elpased: 1.462
start validation test
0.633711340206
0.638593882753
0.618876080692
0.628580388877
0.633735851168
61.450
batch start
#iterations: 260
currently lose_sum: 92.73050063848495
time_elpased: 1.448
batch start
#iterations: 261
currently lose_sum: 92.20977085828781
time_elpased: 1.54
batch start
#iterations: 262
currently lose_sum: 91.79178708791733
time_elpased: 1.427
batch start
#iterations: 263
currently lose_sum: 92.03528434038162
time_elpased: 1.476
batch start
#iterations: 264
currently lose_sum: 92.12029135227203
time_elpased: 1.465
batch start
#iterations: 265
currently lose_sum: 91.62396305799484
time_elpased: 1.474
batch start
#iterations: 266
currently lose_sum: 91.66499143838882
time_elpased: 1.416
batch start
#iterations: 267
currently lose_sum: 91.53305983543396
time_elpased: 1.435
batch start
#iterations: 268
currently lose_sum: 91.76800262928009
time_elpased: 1.437
batch start
#iterations: 269
currently lose_sum: 91.93300819396973
time_elpased: 1.446
batch start
#iterations: 270
currently lose_sum: 91.2847370505333
time_elpased: 1.447
batch start
#iterations: 271
currently lose_sum: 91.99291324615479
time_elpased: 1.43
batch start
#iterations: 272
currently lose_sum: 91.8207973241806
time_elpased: 1.472
batch start
#iterations: 273
currently lose_sum: 91.74778634309769
time_elpased: 1.443
batch start
#iterations: 274
currently lose_sum: 91.46070700883865
time_elpased: 1.431
batch start
#iterations: 275
currently lose_sum: 91.7005118727684
time_elpased: 1.459
batch start
#iterations: 276
currently lose_sum: 91.41695195436478
time_elpased: 1.432
batch start
#iterations: 277
currently lose_sum: 91.2614535689354
time_elpased: 1.469
batch start
#iterations: 278
currently lose_sum: 91.6551913022995
time_elpased: 1.435
batch start
#iterations: 279
currently lose_sum: 91.50527441501617
time_elpased: 1.463
start validation test
0.623453608247
0.623653708073
0.625771922602
0.624711019779
0.623449777906
61.986
batch start
#iterations: 280
currently lose_sum: 91.7984419465065
time_elpased: 1.473
batch start
#iterations: 281
currently lose_sum: 91.46219849586487
time_elpased: 1.455
batch start
#iterations: 282
currently lose_sum: 91.28420287370682
time_elpased: 1.476
batch start
#iterations: 283
currently lose_sum: 91.8447031378746
time_elpased: 1.453
batch start
#iterations: 284
currently lose_sum: 91.64020192623138
time_elpased: 1.437
batch start
#iterations: 285
currently lose_sum: 91.24025279283524
time_elpased: 1.467
batch start
#iterations: 286
currently lose_sum: 91.57760518789291
time_elpased: 1.432
batch start
#iterations: 287
currently lose_sum: 91.92715060710907
time_elpased: 1.482
batch start
#iterations: 288
currently lose_sum: 91.43108421564102
time_elpased: 1.463
batch start
#iterations: 289
currently lose_sum: 91.20443868637085
time_elpased: 1.465
batch start
#iterations: 290
currently lose_sum: 91.41775047779083
time_elpased: 1.439
batch start
#iterations: 291
currently lose_sum: 91.23195284605026
time_elpased: 1.457
batch start
#iterations: 292
currently lose_sum: 91.73105597496033
time_elpased: 1.426
batch start
#iterations: 293
currently lose_sum: 91.46231174468994
time_elpased: 1.452
batch start
#iterations: 294
currently lose_sum: 91.37959611415863
time_elpased: 1.433
batch start
#iterations: 295
currently lose_sum: 91.41109347343445
time_elpased: 1.465
batch start
#iterations: 296
currently lose_sum: 91.3693276643753
time_elpased: 1.423
batch start
#iterations: 297
currently lose_sum: 91.60185766220093
time_elpased: 1.432
batch start
#iterations: 298
currently lose_sum: 91.06165635585785
time_elpased: 1.423
batch start
#iterations: 299
currently lose_sum: 91.40560454130173
time_elpased: 1.425
start validation test
0.630154639175
0.62147432833
0.668999588308
0.644361833953
0.630090459168
61.856
batch start
#iterations: 300
currently lose_sum: 91.5861582159996
time_elpased: 1.502
batch start
#iterations: 301
currently lose_sum: 91.41834437847137
time_elpased: 1.433
batch start
#iterations: 302
currently lose_sum: 90.94877827167511
time_elpased: 1.455
batch start
#iterations: 303
currently lose_sum: 91.0111283659935
time_elpased: 1.429
batch start
#iterations: 304
currently lose_sum: 91.01606768369675
time_elpased: 1.431
batch start
#iterations: 305
currently lose_sum: 91.02081656455994
time_elpased: 1.446
batch start
#iterations: 306
currently lose_sum: 91.32256430387497
time_elpased: 1.476
batch start
#iterations: 307
currently lose_sum: 91.64007604122162
time_elpased: 1.452
batch start
#iterations: 308
currently lose_sum: 91.09622210264206
time_elpased: 1.465
batch start
#iterations: 309
currently lose_sum: 90.8411637544632
time_elpased: 1.469
batch start
#iterations: 310
currently lose_sum: 91.09859329462051
time_elpased: 1.432
batch start
#iterations: 311
currently lose_sum: 91.05333536863327
time_elpased: 1.438
batch start
#iterations: 312
currently lose_sum: 91.14272725582123
time_elpased: 1.427
batch start
#iterations: 313
currently lose_sum: 91.08699595928192
time_elpased: 1.423
batch start
#iterations: 314
currently lose_sum: 91.0740094780922
time_elpased: 1.413
batch start
#iterations: 315
currently lose_sum: 90.95187044143677
time_elpased: 1.451
batch start
#iterations: 316
currently lose_sum: 91.03514492511749
time_elpased: 1.442
batch start
#iterations: 317
currently lose_sum: 90.79390436410904
time_elpased: 1.457
batch start
#iterations: 318
currently lose_sum: 91.10256403684616
time_elpased: 1.507
batch start
#iterations: 319
currently lose_sum: 91.04210466146469
time_elpased: 1.453
start validation test
0.631237113402
0.635041113219
0.620008233841
0.627434642225
0.631255665867
61.804
batch start
#iterations: 320
currently lose_sum: 91.24845033884048
time_elpased: 1.438
batch start
#iterations: 321
currently lose_sum: 91.39110952615738
time_elpased: 1.455
batch start
#iterations: 322
currently lose_sum: 90.97129344940186
time_elpased: 1.444
batch start
#iterations: 323
currently lose_sum: 91.09302008152008
time_elpased: 1.466
batch start
#iterations: 324
currently lose_sum: 91.52084743976593
time_elpased: 1.434
batch start
#iterations: 325
currently lose_sum: 90.69355273246765
time_elpased: 1.473
batch start
#iterations: 326
currently lose_sum: 90.76943618059158
time_elpased: 1.47
batch start
#iterations: 327
currently lose_sum: 91.24992418289185
time_elpased: 1.453
batch start
#iterations: 328
currently lose_sum: 90.7930160164833
time_elpased: 1.457
batch start
#iterations: 329
currently lose_sum: 90.99351984262466
time_elpased: 1.438
batch start
#iterations: 330
currently lose_sum: 90.28086072206497
time_elpased: 1.472
batch start
#iterations: 331
currently lose_sum: 90.85980486869812
time_elpased: 1.443
batch start
#iterations: 332
currently lose_sum: 90.56881839036942
time_elpased: 1.453
batch start
#iterations: 333
currently lose_sum: 91.01181960105896
time_elpased: 1.431
batch start
#iterations: 334
currently lose_sum: 90.86284732818604
time_elpased: 1.45
batch start
#iterations: 335
currently lose_sum: 90.8676118850708
time_elpased: 1.439
batch start
#iterations: 336
currently lose_sum: 90.64928221702576
time_elpased: 1.433
batch start
#iterations: 337
currently lose_sum: 90.9994547367096
time_elpased: 1.452
batch start
#iterations: 338
currently lose_sum: 90.74777120351791
time_elpased: 1.461
batch start
#iterations: 339
currently lose_sum: 90.56198626756668
time_elpased: 1.478
start validation test
0.629432989691
0.6338878881
0.61568546727
0.624654101185
0.629455703482
61.791
batch start
#iterations: 340
currently lose_sum: 91.16711914539337
time_elpased: 1.509
batch start
#iterations: 341
currently lose_sum: 90.59654533863068
time_elpased: 1.434
batch start
#iterations: 342
currently lose_sum: 90.44406259059906
time_elpased: 1.402
batch start
#iterations: 343
currently lose_sum: 90.96338534355164
time_elpased: 1.514
batch start
#iterations: 344
currently lose_sum: 91.02047777175903
time_elpased: 1.466
batch start
#iterations: 345
currently lose_sum: 91.05476987361908
time_elpased: 1.433
batch start
#iterations: 346
currently lose_sum: 90.23988616466522
time_elpased: 1.407
batch start
#iterations: 347
currently lose_sum: 90.83897268772125
time_elpased: 1.474
batch start
#iterations: 348
currently lose_sum: 90.42257934808731
time_elpased: 1.454
batch start
#iterations: 349
currently lose_sum: 91.11486208438873
time_elpased: 1.432
batch start
#iterations: 350
currently lose_sum: 90.58668065071106
time_elpased: 1.43
batch start
#iterations: 351
currently lose_sum: 90.38776344060898
time_elpased: 1.432
batch start
#iterations: 352
currently lose_sum: 90.89183616638184
time_elpased: 1.414
batch start
#iterations: 353
currently lose_sum: 90.91574895381927
time_elpased: 1.42
batch start
#iterations: 354
currently lose_sum: 90.13015323877335
time_elpased: 1.446
batch start
#iterations: 355
currently lose_sum: 90.66452449560165
time_elpased: 1.444
batch start
#iterations: 356
currently lose_sum: 90.00199913978577
time_elpased: 1.436
batch start
#iterations: 357
currently lose_sum: 90.75671398639679
time_elpased: 1.481
batch start
#iterations: 358
currently lose_sum: 90.49544793367386
time_elpased: 1.445
batch start
#iterations: 359
currently lose_sum: 89.96448224782944
time_elpased: 1.432
start validation test
0.630721649485
0.632392612575
0.627315767806
0.629843959905
0.630727276716
62.019
batch start
#iterations: 360
currently lose_sum: 90.93319118022919
time_elpased: 1.436
batch start
#iterations: 361
currently lose_sum: 90.31702870130539
time_elpased: 1.419
batch start
#iterations: 362
currently lose_sum: 90.20567208528519
time_elpased: 1.442
batch start
#iterations: 363
currently lose_sum: 90.72069442272186
time_elpased: 1.426
batch start
#iterations: 364
currently lose_sum: 89.81521487236023
time_elpased: 1.433
batch start
#iterations: 365
currently lose_sum: 90.45132631063461
time_elpased: 1.451
batch start
#iterations: 366
currently lose_sum: 90.81183582544327
time_elpased: 1.479
batch start
#iterations: 367
currently lose_sum: 89.81572705507278
time_elpased: 1.464
batch start
#iterations: 368
currently lose_sum: 90.73184287548065
time_elpased: 1.44
batch start
#iterations: 369
currently lose_sum: 90.19165354967117
time_elpased: 1.494
batch start
#iterations: 370
currently lose_sum: 90.19904935359955
time_elpased: 1.445
batch start
#iterations: 371
currently lose_sum: 90.23014664649963
time_elpased: 1.4
batch start
#iterations: 372
currently lose_sum: 90.4295055270195
time_elpased: 1.427
batch start
#iterations: 373
currently lose_sum: 90.47005200386047
time_elpased: 1.408
batch start
#iterations: 374
currently lose_sum: 90.43114686012268
time_elpased: 1.454
batch start
#iterations: 375
currently lose_sum: 90.32022780179977
time_elpased: 1.439
batch start
#iterations: 376
currently lose_sum: 89.98489099740982
time_elpased: 1.462
batch start
#iterations: 377
currently lose_sum: 89.96860063076019
time_elpased: 1.414
batch start
#iterations: 378
currently lose_sum: 90.14433056116104
time_elpased: 1.438
batch start
#iterations: 379
currently lose_sum: 90.04339224100113
time_elpased: 1.439
start validation test
0.626340206186
0.635267024893
0.596233017703
0.615131404301
0.626389949578
62.394
batch start
#iterations: 380
currently lose_sum: 89.77708965539932
time_elpased: 1.4
batch start
#iterations: 381
currently lose_sum: 89.96871584653854
time_elpased: 1.422
batch start
#iterations: 382
currently lose_sum: 90.40503239631653
time_elpased: 1.443
batch start
#iterations: 383
currently lose_sum: 90.49658244848251
time_elpased: 1.424
batch start
#iterations: 384
currently lose_sum: 89.52027809619904
time_elpased: 1.448
batch start
#iterations: 385
currently lose_sum: 89.94823086261749
time_elpased: 1.431
batch start
#iterations: 386
currently lose_sum: 90.09319233894348
time_elpased: 1.436
batch start
#iterations: 387
currently lose_sum: 89.71933895349503
time_elpased: 1.443
batch start
#iterations: 388
currently lose_sum: 89.89298272132874
time_elpased: 1.452
batch start
#iterations: 389
currently lose_sum: 89.32311064004898
time_elpased: 1.444
batch start
#iterations: 390
currently lose_sum: 89.73122787475586
time_elpased: 1.447
batch start
#iterations: 391
currently lose_sum: 89.7733982205391
time_elpased: 1.437
batch start
#iterations: 392
currently lose_sum: 89.85235434770584
time_elpased: 1.451
batch start
#iterations: 393
currently lose_sum: 89.92024034261703
time_elpased: 1.429
batch start
#iterations: 394
currently lose_sum: 89.44408839941025
time_elpased: 1.452
batch start
#iterations: 395
currently lose_sum: 89.85451191663742
time_elpased: 1.436
batch start
#iterations: 396
currently lose_sum: 89.92160820960999
time_elpased: 1.463
batch start
#iterations: 397
currently lose_sum: 89.84049725532532
time_elpased: 1.405
batch start
#iterations: 398
currently lose_sum: 90.07409453392029
time_elpased: 1.416
batch start
#iterations: 399
currently lose_sum: 89.6494802236557
time_elpased: 1.446
start validation test
0.617422680412
0.635068299576
0.555063812268
0.592376977153
0.617525710347
63.250
acc: 0.660
pre: 0.645
rec: 0.714
F1: 0.678
auc: 0.712
