start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.58927541971207
time_elpased: 2.314
batch start
#iterations: 1
currently lose_sum: 100.34685683250427
time_elpased: 2.436
batch start
#iterations: 2
currently lose_sum: 100.26733726263046
time_elpased: 2.298
batch start
#iterations: 3
currently lose_sum: 99.92628127336502
time_elpased: 2.696
batch start
#iterations: 4
currently lose_sum: 99.97033500671387
time_elpased: 2.488
batch start
#iterations: 5
currently lose_sum: 99.92826515436172
time_elpased: 2.493
batch start
#iterations: 6
currently lose_sum: 99.69390261173248
time_elpased: 2.448
batch start
#iterations: 7
currently lose_sum: 99.64970713853836
time_elpased: 2.398
batch start
#iterations: 8
currently lose_sum: 99.71136629581451
time_elpased: 2.412
batch start
#iterations: 9
currently lose_sum: 99.52796095609665
time_elpased: 2.458
batch start
#iterations: 10
currently lose_sum: 99.39912545681
time_elpased: 2.522
batch start
#iterations: 11
currently lose_sum: 99.32443022727966
time_elpased: 2.494
batch start
#iterations: 12
currently lose_sum: 99.41696494817734
time_elpased: 2.617
batch start
#iterations: 13
currently lose_sum: 99.17551189661026
time_elpased: 2.396
batch start
#iterations: 14
currently lose_sum: 99.13342219591141
time_elpased: 2.506
batch start
#iterations: 15
currently lose_sum: 99.13717728853226
time_elpased: 2.529
batch start
#iterations: 16
currently lose_sum: 99.00669807195663
time_elpased: 2.475
batch start
#iterations: 17
currently lose_sum: 98.99882912635803
time_elpased: 2.461
batch start
#iterations: 18
currently lose_sum: 98.65197199583054
time_elpased: 2.467
batch start
#iterations: 19
currently lose_sum: 98.65606933832169
time_elpased: 2.312
start validation test
0.584587628866
0.606091630407
0.487393228363
0.540300039929
0.584758268627
65.220
batch start
#iterations: 20
currently lose_sum: 98.49500262737274
time_elpased: 2.24
batch start
#iterations: 21
currently lose_sum: 98.41196668148041
time_elpased: 2.398
batch start
#iterations: 22
currently lose_sum: 98.4262684583664
time_elpased: 2.47
batch start
#iterations: 23
currently lose_sum: 98.47816872596741
time_elpased: 2.172
batch start
#iterations: 24
currently lose_sum: 98.28466200828552
time_elpased: 2.412
batch start
#iterations: 25
currently lose_sum: 97.79672849178314
time_elpased: 2.376
batch start
#iterations: 26
currently lose_sum: 97.85471761226654
time_elpased: 2.386
batch start
#iterations: 27
currently lose_sum: 97.691909968853
time_elpased: 2.361
batch start
#iterations: 28
currently lose_sum: 97.90910226106644
time_elpased: 2.532
batch start
#iterations: 29
currently lose_sum: 97.51482874155045
time_elpased: 2.631
batch start
#iterations: 30
currently lose_sum: 97.09984421730042
time_elpased: 2.541
batch start
#iterations: 31
currently lose_sum: 97.35242080688477
time_elpased: 2.489
batch start
#iterations: 32
currently lose_sum: 97.06251871585846
time_elpased: 2.351
batch start
#iterations: 33
currently lose_sum: 97.22552078962326
time_elpased: 2.353
batch start
#iterations: 34
currently lose_sum: 97.21605658531189
time_elpased: 2.386
batch start
#iterations: 35
currently lose_sum: 97.05546224117279
time_elpased: 2.468
batch start
#iterations: 36
currently lose_sum: 96.88851016759872
time_elpased: 2.467
batch start
#iterations: 37
currently lose_sum: 96.5922521352768
time_elpased: 2.378
batch start
#iterations: 38
currently lose_sum: 96.77335864305496
time_elpased: 2.458
batch start
#iterations: 39
currently lose_sum: 96.55886483192444
time_elpased: 2.471
start validation test
0.634536082474
0.626285934045
0.670371513842
0.647579282235
0.634473167847
62.711
batch start
#iterations: 40
currently lose_sum: 96.4894392490387
time_elpased: 2.522
batch start
#iterations: 41
currently lose_sum: 96.9782412648201
time_elpased: 2.451
batch start
#iterations: 42
currently lose_sum: 96.70578807592392
time_elpased: 2.594
batch start
#iterations: 43
currently lose_sum: 96.1013930439949
time_elpased: 2.563
batch start
#iterations: 44
currently lose_sum: 96.2635954618454
time_elpased: 2.315
batch start
#iterations: 45
currently lose_sum: 95.94968801736832
time_elpased: 2.655
batch start
#iterations: 46
currently lose_sum: 96.1866175532341
time_elpased: 2.522
batch start
#iterations: 47
currently lose_sum: 96.13669520616531
time_elpased: 2.491
batch start
#iterations: 48
currently lose_sum: 96.13552582263947
time_elpased: 2.503
batch start
#iterations: 49
currently lose_sum: 96.42838126420975
time_elpased: 2.272
batch start
#iterations: 50
currently lose_sum: 95.89617413282394
time_elpased: 2.285
batch start
#iterations: 51
currently lose_sum: 96.07529920339584
time_elpased: 2.584
batch start
#iterations: 52
currently lose_sum: 95.87751960754395
time_elpased: 2.555
batch start
#iterations: 53
currently lose_sum: 95.58935755491257
time_elpased: 2.277
batch start
#iterations: 54
currently lose_sum: 96.04678279161453
time_elpased: 2.427
batch start
#iterations: 55
currently lose_sum: 95.69006162881851
time_elpased: 2.56
batch start
#iterations: 56
currently lose_sum: 95.69377863407135
time_elpased: 2.502
batch start
#iterations: 57
currently lose_sum: 95.41865772008896
time_elpased: 2.513
batch start
#iterations: 58
currently lose_sum: 95.74978220462799
time_elpased: 2.558
batch start
#iterations: 59
currently lose_sum: 95.63264948129654
time_elpased: 2.349
start validation test
0.622216494845
0.615317751594
0.655655037563
0.63484629565
0.622157788326
62.651
batch start
#iterations: 60
currently lose_sum: 95.19401115179062
time_elpased: 2.279
batch start
#iterations: 61
currently lose_sum: 95.85198247432709
time_elpased: 2.628
batch start
#iterations: 62
currently lose_sum: 95.54348593950272
time_elpased: 2.769
batch start
#iterations: 63
currently lose_sum: 95.6738270521164
time_elpased: 2.272
batch start
#iterations: 64
currently lose_sum: 95.5856985449791
time_elpased: 2.542
batch start
#iterations: 65
currently lose_sum: 95.04448729753494
time_elpased: 2.603
batch start
#iterations: 66
currently lose_sum: 94.92847949266434
time_elpased: 2.548
batch start
#iterations: 67
currently lose_sum: 95.24820053577423
time_elpased: 2.595
batch start
#iterations: 68
currently lose_sum: 95.59166651964188
time_elpased: 2.768
batch start
#iterations: 69
currently lose_sum: 95.08004289865494
time_elpased: 2.58
batch start
#iterations: 70
currently lose_sum: 94.8490976691246
time_elpased: 2.462
batch start
#iterations: 71
currently lose_sum: 95.00329279899597
time_elpased: 2.72
batch start
#iterations: 72
currently lose_sum: 94.61443835496902
time_elpased: 2.524
batch start
#iterations: 73
currently lose_sum: 94.67722821235657
time_elpased: 2.757
batch start
#iterations: 74
currently lose_sum: 94.77827787399292
time_elpased: 2.22
batch start
#iterations: 75
currently lose_sum: 94.73599928617477
time_elpased: 2.519
batch start
#iterations: 76
currently lose_sum: 94.6642182469368
time_elpased: 2.43
batch start
#iterations: 77
currently lose_sum: 95.05715972185135
time_elpased: 2.39
batch start
#iterations: 78
currently lose_sum: 94.81802093982697
time_elpased: 2.358
batch start
#iterations: 79
currently lose_sum: 94.51442378759384
time_elpased: 2.561
start validation test
0.616134020619
0.599456712233
0.704023875682
0.647546026788
0.615979716422
62.842
batch start
#iterations: 80
currently lose_sum: 95.07708650827408
time_elpased: 2.507
batch start
#iterations: 81
currently lose_sum: 94.7128626704216
time_elpased: 2.376
batch start
#iterations: 82
currently lose_sum: 94.52689564228058
time_elpased: 2.396
batch start
#iterations: 83
currently lose_sum: 95.17058229446411
time_elpased: 2.215
batch start
#iterations: 84
currently lose_sum: 94.79830920696259
time_elpased: 2.572
batch start
#iterations: 85
currently lose_sum: 94.54979032278061
time_elpased: 2.47
batch start
#iterations: 86
currently lose_sum: 94.71598625183105
time_elpased: 2.426
batch start
#iterations: 87
currently lose_sum: 94.53509312868118
time_elpased: 2.415
batch start
#iterations: 88
currently lose_sum: 94.47037011384964
time_elpased: 2.515
batch start
#iterations: 89
currently lose_sum: 94.42364513874054
time_elpased: 2.527
batch start
#iterations: 90
currently lose_sum: 94.8227578997612
time_elpased: 2.209
batch start
#iterations: 91
currently lose_sum: 94.11355638504028
time_elpased: 2.259
batch start
#iterations: 92
currently lose_sum: 94.72765064239502
time_elpased: 2.537
batch start
#iterations: 93
currently lose_sum: 94.16960781812668
time_elpased: 2.617
batch start
#iterations: 94
currently lose_sum: 94.2944803237915
time_elpased: 2.472
batch start
#iterations: 95
currently lose_sum: 94.34747171401978
time_elpased: 2.571
batch start
#iterations: 96
currently lose_sum: 94.54829877614975
time_elpased: 2.401
batch start
#iterations: 97
currently lose_sum: 94.61927425861359
time_elpased: 2.476
batch start
#iterations: 98
currently lose_sum: 94.06047201156616
time_elpased: 2.321
batch start
#iterations: 99
currently lose_sum: 94.19255024194717
time_elpased: 2.422
start validation test
0.638969072165
0.637311468772
0.647936605948
0.642580118392
0.638953328276
61.317
batch start
#iterations: 100
currently lose_sum: 94.21895551681519
time_elpased: 2.591
batch start
#iterations: 101
currently lose_sum: 94.17208129167557
time_elpased: 2.392
batch start
#iterations: 102
currently lose_sum: 94.17503833770752
time_elpased: 2.396
batch start
#iterations: 103
currently lose_sum: 94.12297213077545
time_elpased: 2.494
batch start
#iterations: 104
currently lose_sum: 93.65449148416519
time_elpased: 2.541
batch start
#iterations: 105
currently lose_sum: 94.15965437889099
time_elpased: 2.337
batch start
#iterations: 106
currently lose_sum: 94.04497927427292
time_elpased: 2.461
batch start
#iterations: 107
currently lose_sum: 93.59679007530212
time_elpased: 2.406
batch start
#iterations: 108
currently lose_sum: 93.27319526672363
time_elpased: 2.388
batch start
#iterations: 109
currently lose_sum: 94.327077627182
time_elpased: 2.472
batch start
#iterations: 110
currently lose_sum: 93.99028295278549
time_elpased: 2.291
batch start
#iterations: 111
currently lose_sum: 93.70686441659927
time_elpased: 2.423
batch start
#iterations: 112
currently lose_sum: 93.69359683990479
time_elpased: 2.434
batch start
#iterations: 113
currently lose_sum: 93.91805136203766
time_elpased: 2.53
batch start
#iterations: 114
currently lose_sum: 93.84761083126068
time_elpased: 2.51
batch start
#iterations: 115
currently lose_sum: 93.74742239713669
time_elpased: 2.349
batch start
#iterations: 116
currently lose_sum: 93.59978139400482
time_elpased: 2.29
batch start
#iterations: 117
currently lose_sum: 93.84766870737076
time_elpased: 2.438
batch start
#iterations: 118
currently lose_sum: 93.34356224536896
time_elpased: 2.472
batch start
#iterations: 119
currently lose_sum: 93.66358065605164
time_elpased: 2.348
start validation test
0.626958762887
0.62008522177
0.658948235052
0.638926308437
0.626902600434
61.986
batch start
#iterations: 120
currently lose_sum: 94.04805666208267
time_elpased: 2.377
batch start
#iterations: 121
currently lose_sum: 93.60738623142242
time_elpased: 2.509
batch start
#iterations: 122
currently lose_sum: 93.47311866283417
time_elpased: 2.65
batch start
#iterations: 123
currently lose_sum: 93.53100645542145
time_elpased: 2.475
batch start
#iterations: 124
currently lose_sum: 93.89581072330475
time_elpased: 2.654
batch start
#iterations: 125
currently lose_sum: 93.91019207239151
time_elpased: 2.54
batch start
#iterations: 126
currently lose_sum: 93.8214795589447
time_elpased: 2.479
batch start
#iterations: 127
currently lose_sum: 93.60203003883362
time_elpased: 2.403
batch start
#iterations: 128
currently lose_sum: 93.61207050085068
time_elpased: 2.558
batch start
#iterations: 129
currently lose_sum: 93.3134406208992
time_elpased: 2.54
batch start
#iterations: 130
currently lose_sum: 93.10541844367981
time_elpased: 2.439
batch start
#iterations: 131
currently lose_sum: 93.45206236839294
time_elpased: 2.518
batch start
#iterations: 132
currently lose_sum: 93.42950111627579
time_elpased: 2.471
batch start
#iterations: 133
currently lose_sum: 93.22254371643066
time_elpased: 2.538
batch start
#iterations: 134
currently lose_sum: 93.36356502771378
time_elpased: 2.534
batch start
#iterations: 135
currently lose_sum: 93.92962318658829
time_elpased: 2.407
batch start
#iterations: 136
currently lose_sum: 93.08854204416275
time_elpased: 2.392
batch start
#iterations: 137
currently lose_sum: 92.80955564975739
time_elpased: 2.638
batch start
#iterations: 138
currently lose_sum: 92.95120245218277
time_elpased: 2.583
batch start
#iterations: 139
currently lose_sum: 93.30774080753326
time_elpased: 2.308
start validation test
0.631597938144
0.625146084924
0.660594833796
0.64238178634
0.631547029622
61.607
batch start
#iterations: 140
currently lose_sum: 93.27344036102295
time_elpased: 2.592
batch start
#iterations: 141
currently lose_sum: 93.41975790262222
time_elpased: 2.532
batch start
#iterations: 142
currently lose_sum: 92.80290883779526
time_elpased: 2.39
batch start
#iterations: 143
currently lose_sum: 93.14345043897629
time_elpased: 2.398
batch start
#iterations: 144
currently lose_sum: 92.61094772815704
time_elpased: 2.494
batch start
#iterations: 145
currently lose_sum: 93.15828663110733
time_elpased: 2.474
batch start
#iterations: 146
currently lose_sum: 93.1069375872612
time_elpased: 2.453
batch start
#iterations: 147
currently lose_sum: 92.75772279500961
time_elpased: 2.478
batch start
#iterations: 148
currently lose_sum: 93.14605647325516
time_elpased: 2.537
batch start
#iterations: 149
currently lose_sum: 92.96994829177856
time_elpased: 2.508
batch start
#iterations: 150
currently lose_sum: 93.17476534843445
time_elpased: 2.507
batch start
#iterations: 151
currently lose_sum: 92.74192327260971
time_elpased: 2.405
batch start
#iterations: 152
currently lose_sum: 93.0558180809021
time_elpased: 2.422
batch start
#iterations: 153
currently lose_sum: 92.78901153802872
time_elpased: 2.505
batch start
#iterations: 154
currently lose_sum: 93.1652724146843
time_elpased: 2.561
batch start
#iterations: 155
currently lose_sum: 92.82639837265015
time_elpased: 2.353
batch start
#iterations: 156
currently lose_sum: 92.4771077632904
time_elpased: 2.627
batch start
#iterations: 157
currently lose_sum: 92.66717624664307
time_elpased: 2.424
batch start
#iterations: 158
currently lose_sum: 92.76522320508957
time_elpased: 2.621
batch start
#iterations: 159
currently lose_sum: 93.05278491973877
time_elpased: 2.579
start validation test
0.625463917526
0.623775376225
0.635587115365
0.629625853808
0.625446144691
61.971
batch start
#iterations: 160
currently lose_sum: 92.80065935850143
time_elpased: 2.654
batch start
#iterations: 161
currently lose_sum: 92.26485121250153
time_elpased: 2.559
batch start
#iterations: 162
currently lose_sum: 92.57913333177567
time_elpased: 2.581
batch start
#iterations: 163
currently lose_sum: 92.65451085567474
time_elpased: 2.708
batch start
#iterations: 164
currently lose_sum: 92.72612196207047
time_elpased: 2.508
batch start
#iterations: 165
currently lose_sum: 92.78849798440933
time_elpased: 2.643
batch start
#iterations: 166
currently lose_sum: 92.84523785114288
time_elpased: 2.58
batch start
#iterations: 167
currently lose_sum: 92.44426101446152
time_elpased: 2.688
batch start
#iterations: 168
currently lose_sum: 92.20325165987015
time_elpased: 2.568
batch start
#iterations: 169
currently lose_sum: 92.48465609550476
time_elpased: 2.383
batch start
#iterations: 170
currently lose_sum: 92.11259377002716
time_elpased: 2.65
batch start
#iterations: 171
currently lose_sum: 92.33721387386322
time_elpased: 2.434
batch start
#iterations: 172
currently lose_sum: 92.0325533747673
time_elpased: 2.652
batch start
#iterations: 173
currently lose_sum: 92.14592027664185
time_elpased: 2.44
batch start
#iterations: 174
currently lose_sum: 92.17820113897324
time_elpased: 2.299
batch start
#iterations: 175
currently lose_sum: 92.39192813634872
time_elpased: 2.298
batch start
#iterations: 176
currently lose_sum: 92.77074790000916
time_elpased: 2.559
batch start
#iterations: 177
currently lose_sum: 92.05184280872345
time_elpased: 2.506
batch start
#iterations: 178
currently lose_sum: 92.17444628477097
time_elpased: 2.659
batch start
#iterations: 179
currently lose_sum: 91.87464970350266
time_elpased: 2.392
start validation test
0.621340206186
0.624934134261
0.610270659669
0.617515359783
0.621359640482
62.215
batch start
#iterations: 180
currently lose_sum: 92.00035864114761
time_elpased: 2.279
batch start
#iterations: 181
currently lose_sum: 92.02980595827103
time_elpased: 2.419
batch start
#iterations: 182
currently lose_sum: 92.37299555540085
time_elpased: 2.525
batch start
#iterations: 183
currently lose_sum: 91.67888242006302
time_elpased: 2.601
batch start
#iterations: 184
currently lose_sum: 92.22275042533875
time_elpased: 2.534
batch start
#iterations: 185
currently lose_sum: 91.54981875419617
time_elpased: 2.584
batch start
#iterations: 186
currently lose_sum: 91.54572987556458
time_elpased: 2.451
batch start
#iterations: 187
currently lose_sum: 91.96521437168121
time_elpased: 2.53
batch start
#iterations: 188
currently lose_sum: 91.72355425357819
time_elpased: 2.225
batch start
#iterations: 189
currently lose_sum: 91.89866018295288
time_elpased: 2.493
batch start
#iterations: 190
currently lose_sum: 91.78985899686813
time_elpased: 2.455
batch start
#iterations: 191
currently lose_sum: 91.6166108250618
time_elpased: 2.443
batch start
#iterations: 192
currently lose_sum: 91.33738625049591
time_elpased: 2.543
batch start
#iterations: 193
currently lose_sum: 91.95029938220978
time_elpased: 2.356
batch start
#iterations: 194
currently lose_sum: 91.87121772766113
time_elpased: 2.542
batch start
#iterations: 195
currently lose_sum: 91.90266758203506
time_elpased: 2.633
batch start
#iterations: 196
currently lose_sum: 91.54295784235
time_elpased: 2.528
batch start
#iterations: 197
currently lose_sum: 91.82093322277069
time_elpased: 2.518
batch start
#iterations: 198
currently lose_sum: 92.003034055233
time_elpased: 2.441
batch start
#iterations: 199
currently lose_sum: 92.28926646709442
time_elpased: 2.512
start validation test
0.623659793814
0.649098987904
0.541216424822
0.590268814187
0.623804535865
62.045
batch start
#iterations: 200
currently lose_sum: 91.53547042608261
time_elpased: 2.469
batch start
#iterations: 201
currently lose_sum: 91.1293197274208
time_elpased: 2.495
batch start
#iterations: 202
currently lose_sum: 91.65996664762497
time_elpased: 2.536
batch start
#iterations: 203
currently lose_sum: 91.67714494466782
time_elpased: 2.65
batch start
#iterations: 204
currently lose_sum: 91.69237321615219
time_elpased: 2.313
batch start
#iterations: 205
currently lose_sum: 91.59192544221878
time_elpased: 2.583
batch start
#iterations: 206
currently lose_sum: 91.62872260808945
time_elpased: 2.5
batch start
#iterations: 207
currently lose_sum: 91.56012004613876
time_elpased: 2.413
batch start
#iterations: 208
currently lose_sum: 91.26060998439789
time_elpased: 2.475
batch start
#iterations: 209
currently lose_sum: 91.50244444608688
time_elpased: 2.471
batch start
#iterations: 210
currently lose_sum: 91.60713183879852
time_elpased: 2.586
batch start
#iterations: 211
currently lose_sum: 91.08534616231918
time_elpased: 2.587
batch start
#iterations: 212
currently lose_sum: 90.95465058088303
time_elpased: 2.514
batch start
#iterations: 213
currently lose_sum: 90.95628076791763
time_elpased: 2.465
batch start
#iterations: 214
currently lose_sum: 91.0298416018486
time_elpased: 2.858
batch start
#iterations: 215
currently lose_sum: 91.30500549077988
time_elpased: 2.807
batch start
#iterations: 216
currently lose_sum: 91.22175002098083
time_elpased: 2.763
batch start
#iterations: 217
currently lose_sum: 91.20350527763367
time_elpased: 2.622
batch start
#iterations: 218
currently lose_sum: 91.42645114660263
time_elpased: 2.47
batch start
#iterations: 219
currently lose_sum: 91.42549479007721
time_elpased: 2.523
start validation test
0.624329896907
0.625633598841
0.622414325409
0.624019810153
0.624333259988
61.876
batch start
#iterations: 220
currently lose_sum: 90.95930868387222
time_elpased: 2.582
batch start
#iterations: 221
currently lose_sum: 91.15968626737595
time_elpased: 2.581
batch start
#iterations: 222
currently lose_sum: 91.59707516431808
time_elpased: 2.292
batch start
#iterations: 223
currently lose_sum: 91.06201595067978
time_elpased: 2.276
batch start
#iterations: 224
currently lose_sum: 91.41895860433578
time_elpased: 2.476
batch start
#iterations: 225
currently lose_sum: 90.97710072994232
time_elpased: 2.272
batch start
#iterations: 226
currently lose_sum: 91.03551161289215
time_elpased: 2.545
batch start
#iterations: 227
currently lose_sum: 90.54571974277496
time_elpased: 2.453
batch start
#iterations: 228
currently lose_sum: 90.8495728969574
time_elpased: 2.605
batch start
#iterations: 229
currently lose_sum: 90.84803712368011
time_elpased: 2.543
batch start
#iterations: 230
currently lose_sum: 90.51542729139328
time_elpased: 2.374
batch start
#iterations: 231
currently lose_sum: 90.73783755302429
time_elpased: 2.202
batch start
#iterations: 232
currently lose_sum: 90.41992890834808
time_elpased: 2.497
batch start
#iterations: 233
currently lose_sum: 90.88650768995285
time_elpased: 2.558
batch start
#iterations: 234
currently lose_sum: 90.45002615451813
time_elpased: 2.626
batch start
#iterations: 235
currently lose_sum: 90.35876083374023
time_elpased: 2.454
batch start
#iterations: 236
currently lose_sum: 91.29236298799515
time_elpased: 2.557
batch start
#iterations: 237
currently lose_sum: 90.50324368476868
time_elpased: 2.511
batch start
#iterations: 238
currently lose_sum: 90.59999722242355
time_elpased: 2.358
batch start
#iterations: 239
currently lose_sum: 90.66691380739212
time_elpased: 2.535
start validation test
0.60824742268
0.626357884684
0.539981475764
0.579971261192
0.60836727408
62.882
batch start
#iterations: 240
currently lose_sum: 90.74218827486038
time_elpased: 2.489
batch start
#iterations: 241
currently lose_sum: 90.8300770521164
time_elpased: 2.432
batch start
#iterations: 242
currently lose_sum: 90.54022711515427
time_elpased: 2.027
batch start
#iterations: 243
currently lose_sum: 90.95027828216553
time_elpased: 2.483
batch start
#iterations: 244
currently lose_sum: 90.48712867498398
time_elpased: 2.509
batch start
#iterations: 245
currently lose_sum: 90.91673129796982
time_elpased: 2.511
batch start
#iterations: 246
currently lose_sum: 90.82808494567871
time_elpased: 2.429
batch start
#iterations: 247
currently lose_sum: 90.23235136270523
time_elpased: 2.547
batch start
#iterations: 248
currently lose_sum: 90.73607927560806
time_elpased: 2.626
batch start
#iterations: 249
currently lose_sum: 90.60304832458496
time_elpased: 2.505
batch start
#iterations: 250
currently lose_sum: 90.39571940898895
time_elpased: 2.381
batch start
#iterations: 251
currently lose_sum: 90.4331842660904
time_elpased: 2.482
batch start
#iterations: 252
currently lose_sum: 90.2914606332779
time_elpased: 2.563
batch start
#iterations: 253
currently lose_sum: 90.3561241030693
time_elpased: 2.764
batch start
#iterations: 254
currently lose_sum: 90.66428333520889
time_elpased: 2.871
batch start
#iterations: 255
currently lose_sum: 90.20973205566406
time_elpased: 2.598
batch start
#iterations: 256
currently lose_sum: 90.77177858352661
time_elpased: 2.473
batch start
#iterations: 257
currently lose_sum: 90.77946543693542
time_elpased: 2.267
batch start
#iterations: 258
currently lose_sum: 90.16843408346176
time_elpased: 2.568
batch start
#iterations: 259
currently lose_sum: 89.93561625480652
time_elpased: 2.598
start validation test
0.619690721649
0.626010128219
0.597921169085
0.611643330877
0.619728941457
62.270
batch start
#iterations: 260
currently lose_sum: 89.71110892295837
time_elpased: 2.496
batch start
#iterations: 261
currently lose_sum: 90.29698830842972
time_elpased: 2.746
batch start
#iterations: 262
currently lose_sum: 90.24333572387695
time_elpased: 2.583
batch start
#iterations: 263
currently lose_sum: 90.13674736022949
time_elpased: 2.498
batch start
#iterations: 264
currently lose_sum: 89.78906333446503
time_elpased: 2.265
batch start
#iterations: 265
currently lose_sum: 90.19243687391281
time_elpased: 2.236
batch start
#iterations: 266
currently lose_sum: 90.30290114879608
time_elpased: 2.504
batch start
#iterations: 267
currently lose_sum: 90.00630760192871
time_elpased: 2.279
batch start
#iterations: 268
currently lose_sum: 90.63030558824539
time_elpased: 2.531
batch start
#iterations: 269
currently lose_sum: 89.78936201334
time_elpased: 2.501
batch start
#iterations: 270
currently lose_sum: 90.13813889026642
time_elpased: 2.457
batch start
#iterations: 271
currently lose_sum: 89.6573771238327
time_elpased: 2.486
batch start
#iterations: 272
currently lose_sum: 90.0735827088356
time_elpased: 2.522
batch start
#iterations: 273
currently lose_sum: 90.14963752031326
time_elpased: 2.548
batch start
#iterations: 274
currently lose_sum: 89.58492678403854
time_elpased: 2.597
batch start
#iterations: 275
currently lose_sum: 89.7822293639183
time_elpased: 2.596
batch start
#iterations: 276
currently lose_sum: 89.95231884717941
time_elpased: 2.622
batch start
#iterations: 277
currently lose_sum: 90.05407810211182
time_elpased: 2.552
batch start
#iterations: 278
currently lose_sum: 89.53552907705307
time_elpased: 2.649
batch start
#iterations: 279
currently lose_sum: 90.28582340478897
time_elpased: 2.522
start validation test
0.625618556701
0.63513215859
0.593495934959
0.613608554557
0.625674952918
62.106
batch start
#iterations: 280
currently lose_sum: 89.7295600771904
time_elpased: 2.659
batch start
#iterations: 281
currently lose_sum: 89.55366271734238
time_elpased: 2.575
batch start
#iterations: 282
currently lose_sum: 89.60944479703903
time_elpased: 2.522
batch start
#iterations: 283
currently lose_sum: 89.8809124827385
time_elpased: 2.077
batch start
#iterations: 284
currently lose_sum: 89.63698595762253
time_elpased: 2.427
batch start
#iterations: 285
currently lose_sum: 89.7173723578453
time_elpased: 2.644
batch start
#iterations: 286
currently lose_sum: 89.67047190666199
time_elpased: 2.315
batch start
#iterations: 287
currently lose_sum: 89.51516211032867
time_elpased: 2.482
batch start
#iterations: 288
currently lose_sum: 89.65826600790024
time_elpased: 2.451
batch start
#iterations: 289
currently lose_sum: 88.90117180347443
time_elpased: 2.447
batch start
#iterations: 290
currently lose_sum: 89.15990936756134
time_elpased: 2.42
batch start
#iterations: 291
currently lose_sum: 89.4772498011589
time_elpased: 2.398
batch start
#iterations: 292
currently lose_sum: 89.65346956253052
time_elpased: 2.395
batch start
#iterations: 293
currently lose_sum: 89.06168115139008
time_elpased: 2.646
batch start
#iterations: 294
currently lose_sum: 88.82781946659088
time_elpased: 2.661
batch start
#iterations: 295
currently lose_sum: 89.30182552337646
time_elpased: 2.583
batch start
#iterations: 296
currently lose_sum: 89.43381303548813
time_elpased: 2.574
batch start
#iterations: 297
currently lose_sum: 89.39928388595581
time_elpased: 2.325
batch start
#iterations: 298
currently lose_sum: 89.27742272615433
time_elpased: 2.454
batch start
#iterations: 299
currently lose_sum: 89.00943100452423
time_elpased: 2.481
start validation test
0.625360824742
0.631879375337
0.603787177112
0.6175139459
0.625398700608
62.232
batch start
#iterations: 300
currently lose_sum: 89.2927713394165
time_elpased: 2.528
batch start
#iterations: 301
currently lose_sum: 89.68051946163177
time_elpased: 2.51
batch start
#iterations: 302
currently lose_sum: 89.2731185555458
time_elpased: 2.356
batch start
#iterations: 303
currently lose_sum: 89.36967635154724
time_elpased: 2.62
batch start
#iterations: 304
currently lose_sum: 88.92069381475449
time_elpased: 2.386
batch start
#iterations: 305
currently lose_sum: 89.25434547662735
time_elpased: 2.102
batch start
#iterations: 306
currently lose_sum: 89.13774049282074
time_elpased: 2.562
batch start
#iterations: 307
currently lose_sum: 88.82959538698196
time_elpased: 2.353
batch start
#iterations: 308
currently lose_sum: 89.5091238617897
time_elpased: 2.678
batch start
#iterations: 309
currently lose_sum: 89.19460338354111
time_elpased: 2.538
batch start
#iterations: 310
currently lose_sum: 89.21889758110046
time_elpased: 2.362
batch start
#iterations: 311
currently lose_sum: 88.98284095525742
time_elpased: 2.566
batch start
#iterations: 312
currently lose_sum: 89.29483652114868
time_elpased: 2.232
batch start
#iterations: 313
currently lose_sum: 88.16255909204483
time_elpased: 2.401
batch start
#iterations: 314
currently lose_sum: 89.22102558612823
time_elpased: 2.439
batch start
#iterations: 315
currently lose_sum: 89.10631215572357
time_elpased: 2.468
batch start
#iterations: 316
currently lose_sum: 89.23857617378235
time_elpased: 2.459
batch start
#iterations: 317
currently lose_sum: 88.87670332193375
time_elpased: 2.488
batch start
#iterations: 318
currently lose_sum: 88.8594908118248
time_elpased: 2.484
batch start
#iterations: 319
currently lose_sum: 89.10341101884842
time_elpased: 2.551
start validation test
0.615309278351
0.630590961761
0.560049397962
0.593230500899
0.615406295594
63.130
batch start
#iterations: 320
currently lose_sum: 89.01132464408875
time_elpased: 2.584
batch start
#iterations: 321
currently lose_sum: 88.60641294717789
time_elpased: 2.424
batch start
#iterations: 322
currently lose_sum: 89.3274314403534
time_elpased: 2.348
batch start
#iterations: 323
currently lose_sum: 89.31108897924423
time_elpased: 2.39
batch start
#iterations: 324
currently lose_sum: 89.13426423072815
time_elpased: 2.434
batch start
#iterations: 325
currently lose_sum: 88.96198838949203
time_elpased: 2.583
batch start
#iterations: 326
currently lose_sum: 88.43535763025284
time_elpased: 2.527
batch start
#iterations: 327
currently lose_sum: 88.84662699699402
time_elpased: 2.505
batch start
#iterations: 328
currently lose_sum: 88.09522205591202
time_elpased: 2.389
batch start
#iterations: 329
currently lose_sum: 88.818135201931
time_elpased: 2.429
batch start
#iterations: 330
currently lose_sum: 88.2994374036789
time_elpased: 2.413
batch start
#iterations: 331
currently lose_sum: 88.90172374248505
time_elpased: 2.369
batch start
#iterations: 332
currently lose_sum: 88.36471217870712
time_elpased: 2.595
batch start
#iterations: 333
currently lose_sum: 88.16565495729446
time_elpased: 2.619
batch start
#iterations: 334
currently lose_sum: 88.7709436416626
time_elpased: 2.174
batch start
#iterations: 335
currently lose_sum: 88.59539890289307
time_elpased: 2.587
batch start
#iterations: 336
currently lose_sum: 88.70758879184723
time_elpased: 2.462
batch start
#iterations: 337
currently lose_sum: 88.70520371198654
time_elpased: 2.45
batch start
#iterations: 338
currently lose_sum: 88.21594738960266
time_elpased: 2.621
batch start
#iterations: 339
currently lose_sum: 88.2102358341217
time_elpased: 2.642
start validation test
0.616030927835
0.629216043756
0.568282391685
0.59719894014
0.616114757755
63.017
batch start
#iterations: 340
currently lose_sum: 88.4363340139389
time_elpased: 2.459
batch start
#iterations: 341
currently lose_sum: 87.8243682384491
time_elpased: 2.757
batch start
#iterations: 342
currently lose_sum: 88.51814550161362
time_elpased: 2.362
batch start
#iterations: 343
currently lose_sum: 88.29167318344116
time_elpased: 2.681
batch start
#iterations: 344
currently lose_sum: 88.02378004789352
time_elpased: 2.864
batch start
#iterations: 345
currently lose_sum: 88.04349023103714
time_elpased: 2.538
batch start
#iterations: 346
currently lose_sum: 88.71805864572525
time_elpased: 2.564
batch start
#iterations: 347
currently lose_sum: 87.99778139591217
time_elpased: 2.254
batch start
#iterations: 348
currently lose_sum: 88.43951743841171
time_elpased: 2.502
batch start
#iterations: 349
currently lose_sum: 87.90085607767105
time_elpased: 2.476
batch start
#iterations: 350
currently lose_sum: 87.39742654561996
time_elpased: 2.589
batch start
#iterations: 351
currently lose_sum: 87.61417478322983
time_elpased: 2.552
batch start
#iterations: 352
currently lose_sum: 87.9836785197258
time_elpased: 2.516
batch start
#iterations: 353
currently lose_sum: 88.79589658975601
time_elpased: 2.554
batch start
#iterations: 354
currently lose_sum: 88.69964879751205
time_elpased: 2.302
batch start
#iterations: 355
currently lose_sum: 87.71452575922012
time_elpased: 2.507
batch start
#iterations: 356
currently lose_sum: 87.59548163414001
time_elpased: 2.637
batch start
#iterations: 357
currently lose_sum: 88.2974420785904
time_elpased: 2.454
batch start
#iterations: 358
currently lose_sum: 88.1023468375206
time_elpased: 2.149
batch start
#iterations: 359
currently lose_sum: 88.40594667196274
time_elpased: 2.575
start validation test
0.618298969072
0.642716049383
0.535762066481
0.584385699051
0.618443875335
63.336
batch start
#iterations: 360
currently lose_sum: 88.43404513597488
time_elpased: 2.523
batch start
#iterations: 361
currently lose_sum: 88.29932230710983
time_elpased: 2.453
batch start
#iterations: 362
currently lose_sum: 87.73862308263779
time_elpased: 2.545
batch start
#iterations: 363
currently lose_sum: 88.08349508047104
time_elpased: 2.466
batch start
#iterations: 364
currently lose_sum: 88.48716473579407
time_elpased: 2.436
batch start
#iterations: 365
currently lose_sum: 88.08221787214279
time_elpased: 2.499
batch start
#iterations: 366
currently lose_sum: 88.05245524644852
time_elpased: 2.515
batch start
#iterations: 367
currently lose_sum: 87.73010367155075
time_elpased: 2.445
batch start
#iterations: 368
currently lose_sum: 87.95253700017929
time_elpased: 2.438
batch start
#iterations: 369
currently lose_sum: 87.2658354640007
time_elpased: 2.601
batch start
#iterations: 370
currently lose_sum: 87.46212822198868
time_elpased: 2.426
batch start
#iterations: 371
currently lose_sum: 87.59104269742966
time_elpased: 2.552
batch start
#iterations: 372
currently lose_sum: 87.87040555477142
time_elpased: 2.317
batch start
#iterations: 373
currently lose_sum: 87.51892912387848
time_elpased: 2.597
batch start
#iterations: 374
currently lose_sum: 87.27119612693787
time_elpased: 2.595
batch start
#iterations: 375
currently lose_sum: 87.36330914497375
time_elpased: 2.596
batch start
#iterations: 376
currently lose_sum: 87.5441244840622
time_elpased: 2.535
batch start
#iterations: 377
currently lose_sum: 87.55365073680878
time_elpased: 2.549
batch start
#iterations: 378
currently lose_sum: 87.42130553722382
time_elpased: 2.577
batch start
#iterations: 379
currently lose_sum: 87.84073567390442
time_elpased: 2.094
start validation test
0.606443298969
0.626982190778
0.52896984666
0.573820820541
0.606579315564
63.999
batch start
#iterations: 380
currently lose_sum: 87.27492266893387
time_elpased: 2.322
batch start
#iterations: 381
currently lose_sum: 87.75109320878983
time_elpased: 2.436
batch start
#iterations: 382
currently lose_sum: 87.1469851732254
time_elpased: 2.554
batch start
#iterations: 383
currently lose_sum: 87.5035707950592
time_elpased: 2.589
batch start
#iterations: 384
currently lose_sum: 87.6924278140068
time_elpased: 2.513
batch start
#iterations: 385
currently lose_sum: 87.38970810174942
time_elpased: 2.542
batch start
#iterations: 386
currently lose_sum: 87.48612660169601
time_elpased: 2.516
batch start
#iterations: 387
currently lose_sum: 87.40884751081467
time_elpased: 2.421
batch start
#iterations: 388
currently lose_sum: 87.72012907266617
time_elpased: 2.271
batch start
#iterations: 389
currently lose_sum: 87.2570093870163
time_elpased: 2.724
batch start
#iterations: 390
currently lose_sum: 88.23403418064117
time_elpased: 2.515
batch start
#iterations: 391
currently lose_sum: 87.21790337562561
time_elpased: 2.108
batch start
#iterations: 392
currently lose_sum: 87.21693986654282
time_elpased: 2.198
batch start
#iterations: 393
currently lose_sum: 86.98492288589478
time_elpased: 2.469
batch start
#iterations: 394
currently lose_sum: 87.32026696205139
time_elpased: 2.478
batch start
#iterations: 395
currently lose_sum: 87.19599348306656
time_elpased: 2.688
batch start
#iterations: 396
currently lose_sum: 86.82580125331879
time_elpased: 2.278
batch start
#iterations: 397
currently lose_sum: 86.58858525753021
time_elpased: 2.536
batch start
#iterations: 398
currently lose_sum: 87.37240201234818
time_elpased: 2.252
batch start
#iterations: 399
currently lose_sum: 87.27284914255142
time_elpased: 2.361
start validation test
0.612268041237
0.62607696726
0.560872697335
0.591683856259
0.612358273691
63.972
acc: 0.643
pre: 0.642
rec: 0.651
F1: 0.646
auc: 0.699
