start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.50476634502411
time_elpased: 2.742
batch start
#iterations: 1
currently lose_sum: 100.4178074002266
time_elpased: 2.968
batch start
#iterations: 2
currently lose_sum: 100.33523154258728
time_elpased: 3.17
batch start
#iterations: 3
currently lose_sum: 100.12596493959427
time_elpased: 3.259
batch start
#iterations: 4
currently lose_sum: 100.13194531202316
time_elpased: 3.303
batch start
#iterations: 5
currently lose_sum: 100.01864248514175
time_elpased: 3.279
batch start
#iterations: 6
currently lose_sum: 99.70929837226868
time_elpased: 3.099
batch start
#iterations: 7
currently lose_sum: 99.66162770986557
time_elpased: 3.163
batch start
#iterations: 8
currently lose_sum: 99.5601595044136
time_elpased: 3.159
batch start
#iterations: 9
currently lose_sum: 99.45202368497849
time_elpased: 3.162
batch start
#iterations: 10
currently lose_sum: 99.33961886167526
time_elpased: 3.009
batch start
#iterations: 11
currently lose_sum: 99.11277276277542
time_elpased: 3.123
batch start
#iterations: 12
currently lose_sum: 99.10320472717285
time_elpased: 2.937
batch start
#iterations: 13
currently lose_sum: 98.97261309623718
time_elpased: 3.162
batch start
#iterations: 14
currently lose_sum: 98.701913356781
time_elpased: 3.058
batch start
#iterations: 15
currently lose_sum: 98.5940409898758
time_elpased: 2.871
batch start
#iterations: 16
currently lose_sum: 98.4727972149849
time_elpased: 3.191
batch start
#iterations: 17
currently lose_sum: 98.47166055440903
time_elpased: 3.298
batch start
#iterations: 18
currently lose_sum: 97.95851522684097
time_elpased: 3.271
batch start
#iterations: 19
currently lose_sum: 98.10927617549896
time_elpased: 3.082
start validation test
0.604948453608
0.59190616886
0.68035401873
0.633055635354
0.604816067508
64.725
batch start
#iterations: 20
currently lose_sum: 97.85871571302414
time_elpased: 2.888
batch start
#iterations: 21
currently lose_sum: 97.81486427783966
time_elpased: 3.16
batch start
#iterations: 22
currently lose_sum: 97.96845066547394
time_elpased: 3.02
batch start
#iterations: 23
currently lose_sum: 98.09517252445221
time_elpased: 3.293
batch start
#iterations: 24
currently lose_sum: 97.63793408870697
time_elpased: 3.005
batch start
#iterations: 25
currently lose_sum: 97.07379567623138
time_elpased: 3.21
batch start
#iterations: 26
currently lose_sum: 97.26498830318451
time_elpased: 3.068
batch start
#iterations: 27
currently lose_sum: 97.04053246974945
time_elpased: 2.97
batch start
#iterations: 28
currently lose_sum: 97.20621931552887
time_elpased: 2.806
batch start
#iterations: 29
currently lose_sum: 96.71872782707214
time_elpased: 3.394
batch start
#iterations: 30
currently lose_sum: 96.6723707318306
time_elpased: 3.09
batch start
#iterations: 31
currently lose_sum: 96.68945342302322
time_elpased: 2.882
batch start
#iterations: 32
currently lose_sum: 96.65572196245193
time_elpased: 3.102
batch start
#iterations: 33
currently lose_sum: 96.49337065219879
time_elpased: 3.306
batch start
#iterations: 34
currently lose_sum: 96.64209598302841
time_elpased: 3.133
batch start
#iterations: 35
currently lose_sum: 96.85590082406998
time_elpased: 3.017
batch start
#iterations: 36
currently lose_sum: 96.34387683868408
time_elpased: 3.203
batch start
#iterations: 37
currently lose_sum: 95.97747468948364
time_elpased: 3.161
batch start
#iterations: 38
currently lose_sum: 96.38320392370224
time_elpased: 3.188
batch start
#iterations: 39
currently lose_sum: 96.18642163276672
time_elpased: 3.092
start validation test
0.642577319588
0.633502830279
0.679530719358
0.655710029791
0.642512442195
62.055
batch start
#iterations: 40
currently lose_sum: 96.08131808042526
time_elpased: 3.086
batch start
#iterations: 41
currently lose_sum: 96.65486764907837
time_elpased: 3.339
batch start
#iterations: 42
currently lose_sum: 96.27907085418701
time_elpased: 3.201
batch start
#iterations: 43
currently lose_sum: 95.80519443750381
time_elpased: 3.143
batch start
#iterations: 44
currently lose_sum: 95.81980460882187
time_elpased: 3.091
batch start
#iterations: 45
currently lose_sum: 96.00249767303467
time_elpased: 3.346
batch start
#iterations: 46
currently lose_sum: 95.86774563789368
time_elpased: 3.158
batch start
#iterations: 47
currently lose_sum: 95.91552352905273
time_elpased: 3.041
batch start
#iterations: 48
currently lose_sum: 95.80386990308762
time_elpased: 2.784
batch start
#iterations: 49
currently lose_sum: 96.32355040311813
time_elpased: 3.253
batch start
#iterations: 50
currently lose_sum: 95.56046468019485
time_elpased: 3.123
batch start
#iterations: 51
currently lose_sum: 95.70464640855789
time_elpased: 3.09
batch start
#iterations: 52
currently lose_sum: 95.64413207769394
time_elpased: 3.095
batch start
#iterations: 53
currently lose_sum: 95.19897264242172
time_elpased: 3.07
batch start
#iterations: 54
currently lose_sum: 95.82202684879303
time_elpased: 3.301
batch start
#iterations: 55
currently lose_sum: 95.5625741481781
time_elpased: 3.04
batch start
#iterations: 56
currently lose_sum: 95.3027133345604
time_elpased: 3.053
batch start
#iterations: 57
currently lose_sum: 95.3326023221016
time_elpased: 3.171
batch start
#iterations: 58
currently lose_sum: 95.45594120025635
time_elpased: 3.254
batch start
#iterations: 59
currently lose_sum: 95.50565892457962
time_elpased: 3.156
start validation test
0.632422680412
0.621431254696
0.680971493259
0.649840412472
0.632337445483
61.895
batch start
#iterations: 60
currently lose_sum: 95.1235648393631
time_elpased: 3.228
batch start
#iterations: 61
currently lose_sum: 95.82738304138184
time_elpased: 3.099
batch start
#iterations: 62
currently lose_sum: 95.38242703676224
time_elpased: 3.3
batch start
#iterations: 63
currently lose_sum: 95.54073965549469
time_elpased: 3.168
batch start
#iterations: 64
currently lose_sum: 95.50146281719208
time_elpased: 3.012
batch start
#iterations: 65
currently lose_sum: 94.96287006139755
time_elpased: 3.121
batch start
#iterations: 66
currently lose_sum: 94.79776066541672
time_elpased: 3.095
batch start
#iterations: 67
currently lose_sum: 94.82034385204315
time_elpased: 3.012
batch start
#iterations: 68
currently lose_sum: 95.57034569978714
time_elpased: 3.229
batch start
#iterations: 69
currently lose_sum: 94.79440873861313
time_elpased: 2.767
batch start
#iterations: 70
currently lose_sum: 94.84283697605133
time_elpased: 2.786
batch start
#iterations: 71
currently lose_sum: 94.79863625764847
time_elpased: 2.977
batch start
#iterations: 72
currently lose_sum: 94.52045494318008
time_elpased: 3.235
batch start
#iterations: 73
currently lose_sum: 94.56439781188965
time_elpased: 3.173
batch start
#iterations: 74
currently lose_sum: 94.6111022233963
time_elpased: 3.184
batch start
#iterations: 75
currently lose_sum: 94.65961354970932
time_elpased: 3.058
batch start
#iterations: 76
currently lose_sum: 94.36225497722626
time_elpased: 3.129
batch start
#iterations: 77
currently lose_sum: 94.77004909515381
time_elpased: 3.158
batch start
#iterations: 78
currently lose_sum: 94.63548803329468
time_elpased: 3.035
batch start
#iterations: 79
currently lose_sum: 94.26304149627686
time_elpased: 3.125
start validation test
0.598092783505
0.607768298159
0.55716785016
0.581369127517
0.598164633538
63.321
batch start
#iterations: 80
currently lose_sum: 94.97489994764328
time_elpased: 3.252
batch start
#iterations: 81
currently lose_sum: 94.5693850517273
time_elpased: 3.31
batch start
#iterations: 82
currently lose_sum: 94.49244666099548
time_elpased: 2.936
batch start
#iterations: 83
currently lose_sum: 94.88195317983627
time_elpased: 2.974
batch start
#iterations: 84
currently lose_sum: 94.6565151810646
time_elpased: 3.131
batch start
#iterations: 85
currently lose_sum: 94.37626725435257
time_elpased: 2.811
batch start
#iterations: 86
currently lose_sum: 94.5168731212616
time_elpased: 3.279
batch start
#iterations: 87
currently lose_sum: 94.38176828622818
time_elpased: 2.846
batch start
#iterations: 88
currently lose_sum: 94.29366153478622
time_elpased: 2.995
batch start
#iterations: 89
currently lose_sum: 94.04142451286316
time_elpased: 3.262
batch start
#iterations: 90
currently lose_sum: 94.6612948179245
time_elpased: 3.285
batch start
#iterations: 91
currently lose_sum: 94.05990898609161
time_elpased: 3.167
batch start
#iterations: 92
currently lose_sum: 94.38896816968918
time_elpased: 3.309
batch start
#iterations: 93
currently lose_sum: 94.21053910255432
time_elpased: 3.121
batch start
#iterations: 94
currently lose_sum: 93.84524124860764
time_elpased: 3.32
batch start
#iterations: 95
currently lose_sum: 94.03706002235413
time_elpased: 3.074
batch start
#iterations: 96
currently lose_sum: 94.02914601564407
time_elpased: 3.175
batch start
#iterations: 97
currently lose_sum: 94.51629441976547
time_elpased: 3.258
batch start
#iterations: 98
currently lose_sum: 94.00539219379425
time_elpased: 3.08
batch start
#iterations: 99
currently lose_sum: 93.85880661010742
time_elpased: 3.114
start validation test
0.642989690722
0.625686751328
0.714932592364
0.667339097022
0.642863383861
61.107
batch start
#iterations: 100
currently lose_sum: 93.88481450080872
time_elpased: 3.386
batch start
#iterations: 101
currently lose_sum: 93.7023149728775
time_elpased: 3.229
batch start
#iterations: 102
currently lose_sum: 93.97202616930008
time_elpased: 3.141
batch start
#iterations: 103
currently lose_sum: 93.78079903125763
time_elpased: 3.221
batch start
#iterations: 104
currently lose_sum: 93.49120265245438
time_elpased: 3.053
batch start
#iterations: 105
currently lose_sum: 93.74688732624054
time_elpased: 3.097
batch start
#iterations: 106
currently lose_sum: 93.79048675298691
time_elpased: 2.88
batch start
#iterations: 107
currently lose_sum: 93.48125195503235
time_elpased: 3.152
batch start
#iterations: 108
currently lose_sum: 92.99031352996826
time_elpased: 3.35
batch start
#iterations: 109
currently lose_sum: 93.89783084392548
time_elpased: 3.097
batch start
#iterations: 110
currently lose_sum: 93.77242457866669
time_elpased: 2.962
batch start
#iterations: 111
currently lose_sum: 93.4449217915535
time_elpased: 3.262
batch start
#iterations: 112
currently lose_sum: 93.66914248466492
time_elpased: 3.348
batch start
#iterations: 113
currently lose_sum: 93.65556979179382
time_elpased: 3.206
batch start
#iterations: 114
currently lose_sum: 93.65654146671295
time_elpased: 3.248
batch start
#iterations: 115
currently lose_sum: 93.31741976737976
time_elpased: 3.472
batch start
#iterations: 116
currently lose_sum: 93.45808696746826
time_elpased: 3.233
batch start
#iterations: 117
currently lose_sum: 93.28593498468399
time_elpased: 3.226
batch start
#iterations: 118
currently lose_sum: 93.14687585830688
time_elpased: 3.222
batch start
#iterations: 119
currently lose_sum: 92.9789491891861
time_elpased: 3.078
start validation test
0.626494845361
0.640381774798
0.580014407739
0.608705043741
0.626576448938
61.756
batch start
#iterations: 120
currently lose_sum: 93.61662727594376
time_elpased: 3.207
batch start
#iterations: 121
currently lose_sum: 93.49991625547409
time_elpased: 3.047
batch start
#iterations: 122
currently lose_sum: 93.23462563753128
time_elpased: 3.222
batch start
#iterations: 123
currently lose_sum: 93.17838591337204
time_elpased: 3.188
batch start
#iterations: 124
currently lose_sum: 93.58205276727676
time_elpased: 3.26
batch start
#iterations: 125
currently lose_sum: 93.64986968040466
time_elpased: 3.402
batch start
#iterations: 126
currently lose_sum: 93.3662958741188
time_elpased: 3.111
batch start
#iterations: 127
currently lose_sum: 93.35893440246582
time_elpased: 2.661
batch start
#iterations: 128
currently lose_sum: 93.3040834069252
time_elpased: 2.951
batch start
#iterations: 129
currently lose_sum: 93.02259427309036
time_elpased: 3.244
batch start
#iterations: 130
currently lose_sum: 92.59491163492203
time_elpased: 3.49
batch start
#iterations: 131
currently lose_sum: 93.42119473218918
time_elpased: 3.171
batch start
#iterations: 132
currently lose_sum: 93.21792274713516
time_elpased: 2.989
batch start
#iterations: 133
currently lose_sum: 92.62979382276535
time_elpased: 3.251
batch start
#iterations: 134
currently lose_sum: 92.88448983430862
time_elpased: 2.939
batch start
#iterations: 135
currently lose_sum: 93.59034067392349
time_elpased: 3.198
batch start
#iterations: 136
currently lose_sum: 92.73123842477798
time_elpased: 3.298
batch start
#iterations: 137
currently lose_sum: 92.68480008840561
time_elpased: 3.263
batch start
#iterations: 138
currently lose_sum: 92.84149247407913
time_elpased: 3.262
batch start
#iterations: 139
currently lose_sum: 92.80470186471939
time_elpased: 2.961
start validation test
0.632680412371
0.622575456524
0.677163733663
0.648723257419
0.63260231504
61.345
batch start
#iterations: 140
currently lose_sum: 92.8008741736412
time_elpased: 3.157
batch start
#iterations: 141
currently lose_sum: 93.12679648399353
time_elpased: 2.744
batch start
#iterations: 142
currently lose_sum: 92.60176193714142
time_elpased: 3.139
batch start
#iterations: 143
currently lose_sum: 92.7395168542862
time_elpased: 3.26
batch start
#iterations: 144
currently lose_sum: 92.52802586555481
time_elpased: 2.935
batch start
#iterations: 145
currently lose_sum: 92.90130233764648
time_elpased: 3.149
batch start
#iterations: 146
currently lose_sum: 92.93630599975586
time_elpased: 3.182
batch start
#iterations: 147
currently lose_sum: 92.41636484861374
time_elpased: 3.199
batch start
#iterations: 148
currently lose_sum: 92.72256517410278
time_elpased: 3.055
batch start
#iterations: 149
currently lose_sum: 92.5575140118599
time_elpased: 3.028
batch start
#iterations: 150
currently lose_sum: 92.35645282268524
time_elpased: 3.238
batch start
#iterations: 151
currently lose_sum: 92.57378005981445
time_elpased: 3.242
batch start
#iterations: 152
currently lose_sum: 92.7840866446495
time_elpased: 3.275
batch start
#iterations: 153
currently lose_sum: 92.17315763235092
time_elpased: 3.182
batch start
#iterations: 154
currently lose_sum: 93.04381221532822
time_elpased: 3.458
batch start
#iterations: 155
currently lose_sum: 92.59097987413406
time_elpased: 3.29
batch start
#iterations: 156
currently lose_sum: 92.21604365110397
time_elpased: 2.834
batch start
#iterations: 157
currently lose_sum: 92.0822326540947
time_elpased: 3.023
batch start
#iterations: 158
currently lose_sum: 92.36773580312729
time_elpased: 3.222
batch start
#iterations: 159
currently lose_sum: 92.49772155284882
time_elpased: 2.732
start validation test
0.635154639175
0.634931997137
0.638983225275
0.636951169471
0.635147917502
61.703
batch start
#iterations: 160
currently lose_sum: 92.39947581291199
time_elpased: 3.108
batch start
#iterations: 161
currently lose_sum: 91.90175104141235
time_elpased: 3.207
batch start
#iterations: 162
currently lose_sum: 92.40980768203735
time_elpased: 3.085
batch start
#iterations: 163
currently lose_sum: 92.30597358942032
time_elpased: 3.117
batch start
#iterations: 164
currently lose_sum: 92.12126463651657
time_elpased: 3.227
batch start
#iterations: 165
currently lose_sum: 92.10529071092606
time_elpased: 3.12
batch start
#iterations: 166
currently lose_sum: 92.34706550836563
time_elpased: 3.022
batch start
#iterations: 167
currently lose_sum: 92.05431407690048
time_elpased: 3.293
batch start
#iterations: 168
currently lose_sum: 91.94083344936371
time_elpased: 3.265
batch start
#iterations: 169
currently lose_sum: 92.03173989057541
time_elpased: 2.88
batch start
#iterations: 170
currently lose_sum: 91.678680062294
time_elpased: 3.089
batch start
#iterations: 171
currently lose_sum: 91.73498171567917
time_elpased: 3.074
batch start
#iterations: 172
currently lose_sum: 92.1397413611412
time_elpased: 3.23
batch start
#iterations: 173
currently lose_sum: 91.80247855186462
time_elpased: 3.123
batch start
#iterations: 174
currently lose_sum: 91.79236423969269
time_elpased: 3.11
batch start
#iterations: 175
currently lose_sum: 91.91532135009766
time_elpased: 3.369
batch start
#iterations: 176
currently lose_sum: 92.15520232915878
time_elpased: 3.127
batch start
#iterations: 177
currently lose_sum: 91.78332138061523
time_elpased: 3.231
batch start
#iterations: 178
currently lose_sum: 91.60943323373795
time_elpased: 3.017
batch start
#iterations: 179
currently lose_sum: 91.43669229745865
time_elpased: 2.908
start validation test
0.623659793814
0.635730337079
0.582278481013
0.607831551807
0.623732445092
62.554
batch start
#iterations: 180
currently lose_sum: 91.51914441585541
time_elpased: 3.171
batch start
#iterations: 181
currently lose_sum: 91.4430695772171
time_elpased: 3.142
batch start
#iterations: 182
currently lose_sum: 92.04134142398834
time_elpased: 3.185
batch start
#iterations: 183
currently lose_sum: 91.0188262462616
time_elpased: 3.389
batch start
#iterations: 184
currently lose_sum: 91.70063537359238
time_elpased: 3.244
batch start
#iterations: 185
currently lose_sum: 91.24768155813217
time_elpased: 3.063
batch start
#iterations: 186
currently lose_sum: 91.27462702989578
time_elpased: 3.295
batch start
#iterations: 187
currently lose_sum: 91.2892404794693
time_elpased: 3.197
batch start
#iterations: 188
currently lose_sum: 91.42277985811234
time_elpased: 3.433
batch start
#iterations: 189
currently lose_sum: 91.22741317749023
time_elpased: 3.422
batch start
#iterations: 190
currently lose_sum: 91.30516588687897
time_elpased: 3.099
batch start
#iterations: 191
currently lose_sum: 91.20952212810516
time_elpased: 3.164
batch start
#iterations: 192
currently lose_sum: 90.50970441102982
time_elpased: 3.165
batch start
#iterations: 193
currently lose_sum: 90.9408591389656
time_elpased: 3.245
batch start
#iterations: 194
currently lose_sum: 90.97955423593521
time_elpased: 3.343
batch start
#iterations: 195
currently lose_sum: 91.11694699525833
time_elpased: 3.202
batch start
#iterations: 196
currently lose_sum: 91.00904113054276
time_elpased: 3.283
batch start
#iterations: 197
currently lose_sum: 91.2857551574707
time_elpased: 2.971
batch start
#iterations: 198
currently lose_sum: 91.10709822177887
time_elpased: 2.92
batch start
#iterations: 199
currently lose_sum: 91.60703814029694
time_elpased: 3.218
start validation test
0.627422680412
0.631679187388
0.61438715653
0.622913188648
0.627445566285
62.063
batch start
#iterations: 200
currently lose_sum: 91.02778106927872
time_elpased: 2.887
batch start
#iterations: 201
currently lose_sum: 90.63084173202515
time_elpased: 2.801
batch start
#iterations: 202
currently lose_sum: 91.07677501440048
time_elpased: 3.124
batch start
#iterations: 203
currently lose_sum: 91.02614015340805
time_elpased: 3.23
batch start
#iterations: 204
currently lose_sum: 90.93710052967072
time_elpased: 2.84
batch start
#iterations: 205
currently lose_sum: 90.78503900766373
time_elpased: 3.309
batch start
#iterations: 206
currently lose_sum: 91.03551137447357
time_elpased: 3.04
batch start
#iterations: 207
currently lose_sum: 90.86421376466751
time_elpased: 3.168
batch start
#iterations: 208
currently lose_sum: 90.19112306833267
time_elpased: 3.06
batch start
#iterations: 209
currently lose_sum: 90.84012585878372
time_elpased: 3.152
batch start
#iterations: 210
currently lose_sum: 90.67491918802261
time_elpased: 3.147
batch start
#iterations: 211
currently lose_sum: 90.46308261156082
time_elpased: 3.071
batch start
#iterations: 212
currently lose_sum: 90.11300724744797
time_elpased: 3.091
batch start
#iterations: 213
currently lose_sum: 90.24899387359619
time_elpased: 3.087
batch start
#iterations: 214
currently lose_sum: 90.04164129495621
time_elpased: 3.109
batch start
#iterations: 215
currently lose_sum: 90.49758380651474
time_elpased: 3.213
batch start
#iterations: 216
currently lose_sum: 89.98650240898132
time_elpased: 3.044
batch start
#iterations: 217
currently lose_sum: 90.11747860908508
time_elpased: 3.206
batch start
#iterations: 218
currently lose_sum: 90.66276609897614
time_elpased: 3.263
batch start
#iterations: 219
currently lose_sum: 90.7016116976738
time_elpased: 2.907
start validation test
0.630103092784
0.621149995232
0.670371513842
0.644822807365
0.630032395359
62.291
batch start
#iterations: 220
currently lose_sum: 90.22442227602005
time_elpased: 3.247
batch start
#iterations: 221
currently lose_sum: 89.99916446208954
time_elpased: 3.22
batch start
#iterations: 222
currently lose_sum: 90.4880428314209
time_elpased: 3.057
batch start
#iterations: 223
currently lose_sum: 89.69399946928024
time_elpased: 2.94
batch start
#iterations: 224
currently lose_sum: 90.54701733589172
time_elpased: 2.983
batch start
#iterations: 225
currently lose_sum: 90.02822238206863
time_elpased: 3.06
batch start
#iterations: 226
currently lose_sum: 89.93178659677505
time_elpased: 2.843
batch start
#iterations: 227
currently lose_sum: 89.80576115846634
time_elpased: 3.209
batch start
#iterations: 228
currently lose_sum: 89.85157370567322
time_elpased: 3.313
batch start
#iterations: 229
currently lose_sum: 89.92824399471283
time_elpased: 3.344
batch start
#iterations: 230
currently lose_sum: 89.40034490823746
time_elpased: 3.113
batch start
#iterations: 231
currently lose_sum: 89.64104682207108
time_elpased: 3.147
batch start
#iterations: 232
currently lose_sum: 89.21578794717789
time_elpased: 2.921
batch start
#iterations: 233
currently lose_sum: 90.09017711877823
time_elpased: 3.208
batch start
#iterations: 234
currently lose_sum: 89.68232363462448
time_elpased: 3.123
batch start
#iterations: 235
currently lose_sum: 89.05108994245529
time_elpased: 3.186
batch start
#iterations: 236
currently lose_sum: 89.8918873667717
time_elpased: 3.165
batch start
#iterations: 237
currently lose_sum: 89.63642370700836
time_elpased: 3.103
batch start
#iterations: 238
currently lose_sum: 89.32515180110931
time_elpased: 3.065
batch start
#iterations: 239
currently lose_sum: 89.34126031398773
time_elpased: 3.159
start validation test
0.617577319588
0.629333633498
0.57538334877
0.601150475781
0.617651397611
63.215
batch start
#iterations: 240
currently lose_sum: 89.64691030979156
time_elpased: 2.944
batch start
#iterations: 241
currently lose_sum: 89.72067677974701
time_elpased: 3.226
batch start
#iterations: 242
currently lose_sum: 89.25085145235062
time_elpased: 3.294
batch start
#iterations: 243
currently lose_sum: 89.44239795207977
time_elpased: 3.248
batch start
#iterations: 244
currently lose_sum: 89.02720504999161
time_elpased: 2.865
batch start
#iterations: 245
currently lose_sum: 89.53599041700363
time_elpased: 2.988
batch start
#iterations: 246
currently lose_sum: 89.28193145990372
time_elpased: 3.176
batch start
#iterations: 247
currently lose_sum: 89.08566504716873
time_elpased: 2.792
batch start
#iterations: 248
currently lose_sum: 89.28556990623474
time_elpased: 3.278
batch start
#iterations: 249
currently lose_sum: 89.00254184007645
time_elpased: 3.018
batch start
#iterations: 250
currently lose_sum: 88.82863891124725
time_elpased: 3.157
batch start
#iterations: 251
currently lose_sum: 88.67661887407303
time_elpased: 2.913
batch start
#iterations: 252
currently lose_sum: 88.60076189041138
time_elpased: 2.962
batch start
#iterations: 253
currently lose_sum: 88.92638248205185
time_elpased: 3.207
batch start
#iterations: 254
currently lose_sum: 88.82002878189087
time_elpased: 3.201
batch start
#iterations: 255
currently lose_sum: 88.7341405749321
time_elpased: 3.081
batch start
#iterations: 256
currently lose_sum: 88.89373409748077
time_elpased: 2.949
batch start
#iterations: 257
currently lose_sum: 88.90582293272018
time_elpased: 3.446
batch start
#iterations: 258
currently lose_sum: 88.45041573047638
time_elpased: 3.055
batch start
#iterations: 259
currently lose_sum: 88.72289025783539
time_elpased: 3.215
start validation test
0.625515463918
0.617613200307
0.662550169805
0.63929298446
0.625450443779
62.924
batch start
#iterations: 260
currently lose_sum: 88.43808555603027
time_elpased: 3.471
batch start
#iterations: 261
currently lose_sum: 88.93915742635727
time_elpased: 2.944
batch start
#iterations: 262
currently lose_sum: 88.51078605651855
time_elpased: 3.458
batch start
#iterations: 263
currently lose_sum: 88.27254271507263
time_elpased: 3.067
batch start
#iterations: 264
currently lose_sum: 88.08009397983551
time_elpased: 3.346
batch start
#iterations: 265
currently lose_sum: 88.51396507024765
time_elpased: 3.074
batch start
#iterations: 266
currently lose_sum: 88.50425893068314
time_elpased: 3.259
batch start
#iterations: 267
currently lose_sum: 88.08276629447937
time_elpased: 3.066
batch start
#iterations: 268
currently lose_sum: 88.42594289779663
time_elpased: 3.042
batch start
#iterations: 269
currently lose_sum: 87.89777946472168
time_elpased: 2.938
batch start
#iterations: 270
currently lose_sum: 88.04328054189682
time_elpased: 3.14
batch start
#iterations: 271
currently lose_sum: 87.80932050943375
time_elpased: 3.209
batch start
#iterations: 272
currently lose_sum: 88.12178564071655
time_elpased: 3.119
batch start
#iterations: 273
currently lose_sum: 87.79879319667816
time_elpased: 3.067
batch start
#iterations: 274
currently lose_sum: 87.82064861059189
time_elpased: 3.263
batch start
#iterations: 275
currently lose_sum: 87.76045286655426
time_elpased: 3.079
batch start
#iterations: 276
currently lose_sum: 87.80823230743408
time_elpased: 3.048
batch start
#iterations: 277
currently lose_sum: 88.0888701081276
time_elpased: 3.286
batch start
#iterations: 278
currently lose_sum: 87.54337483644485
time_elpased: 2.929
batch start
#iterations: 279
currently lose_sum: 87.79758727550507
time_elpased: 3.309
start validation test
0.617525773196
0.630793759253
0.570031902851
0.598875554114
0.617609156011
66.978
batch start
#iterations: 280
currently lose_sum: 87.32451385259628
time_elpased: 3.368
batch start
#iterations: 281
currently lose_sum: 87.33062154054642
time_elpased: 3.14
batch start
#iterations: 282
currently lose_sum: 87.12542259693146
time_elpased: 3.194
batch start
#iterations: 283
currently lose_sum: 87.5422078371048
time_elpased: 3.22
batch start
#iterations: 284
currently lose_sum: 87.10308200120926
time_elpased: 3.016
batch start
#iterations: 285
currently lose_sum: 87.25784462690353
time_elpased: 3.119
batch start
#iterations: 286
currently lose_sum: 87.26746845245361
time_elpased: 3.235
batch start
#iterations: 287
currently lose_sum: 87.33183646202087
time_elpased: 3.118
batch start
#iterations: 288
currently lose_sum: 86.94318479299545
time_elpased: 3.164
batch start
#iterations: 289
currently lose_sum: 86.8479483127594
time_elpased: 3.257
batch start
#iterations: 290
currently lose_sum: 86.53227055072784
time_elpased: 3.14
batch start
#iterations: 291
currently lose_sum: 86.67089998722076
time_elpased: 3.028
batch start
#iterations: 292
currently lose_sum: 86.6901176571846
time_elpased: 3.142
batch start
#iterations: 293
currently lose_sum: 86.98810756206512
time_elpased: 3.329
batch start
#iterations: 294
currently lose_sum: 86.74737447500229
time_elpased: 3.112
batch start
#iterations: 295
currently lose_sum: 86.52645587921143
time_elpased: 3.105
batch start
#iterations: 296
currently lose_sum: 86.6617721915245
time_elpased: 2.924
batch start
#iterations: 297
currently lose_sum: 86.74767673015594
time_elpased: 3.172
batch start
#iterations: 298
currently lose_sum: 86.23890799283981
time_elpased: 3.105
batch start
#iterations: 299
currently lose_sum: 86.37218421697617
time_elpased: 2.932
start validation test
0.620515463918
0.620288078455
0.624884223526
0.62257766841
0.620507793886
66.443
batch start
#iterations: 300
currently lose_sum: 86.28951072692871
time_elpased: 3.096
batch start
#iterations: 301
currently lose_sum: 86.81695127487183
time_elpased: 3.013
batch start
#iterations: 302
currently lose_sum: 86.40761244297028
time_elpased: 3.028
batch start
#iterations: 303
currently lose_sum: 86.02913385629654
time_elpased: 3.065
batch start
#iterations: 304
currently lose_sum: 86.18038827180862
time_elpased: 3.425
batch start
#iterations: 305
currently lose_sum: 86.41874861717224
time_elpased: 3.309
batch start
#iterations: 306
currently lose_sum: 85.78959655761719
time_elpased: 3.188
batch start
#iterations: 307
currently lose_sum: 86.0499404668808
time_elpased: 3.135
batch start
#iterations: 308
currently lose_sum: 85.82571578025818
time_elpased: 2.859
batch start
#iterations: 309
currently lose_sum: 86.0156620144844
time_elpased: 3.151
batch start
#iterations: 310
currently lose_sum: 86.03364461660385
time_elpased: 3.304
batch start
#iterations: 311
currently lose_sum: 86.04393833875656
time_elpased: 3.083
batch start
#iterations: 312
currently lose_sum: 85.12386858463287
time_elpased: 2.995
batch start
#iterations: 313
currently lose_sum: 84.86082828044891
time_elpased: 3.121
batch start
#iterations: 314
currently lose_sum: 85.74684244394302
time_elpased: 3.109
batch start
#iterations: 315
currently lose_sum: 85.74659496545792
time_elpased: 3.301
batch start
#iterations: 316
currently lose_sum: 85.18782514333725
time_elpased: 3.462
batch start
#iterations: 317
currently lose_sum: 85.64780801534653
time_elpased: 3.074
batch start
#iterations: 318
currently lose_sum: 85.44985765218735
time_elpased: 3.362
batch start
#iterations: 319
currently lose_sum: 84.96150243282318
time_elpased: 3.171
start validation test
0.612886597938
0.629351775876
0.552536791191
0.588448049101
0.612992551332
69.301
batch start
#iterations: 320
currently lose_sum: 85.85738599300385
time_elpased: 3.19
batch start
#iterations: 321
currently lose_sum: 84.73077934980392
time_elpased: 3.119
batch start
#iterations: 322
currently lose_sum: 85.38704216480255
time_elpased: 3.273
batch start
#iterations: 323
currently lose_sum: 85.42551290988922
time_elpased: 3.342
batch start
#iterations: 324
currently lose_sum: 85.55799412727356
time_elpased: 3.082
batch start
#iterations: 325
currently lose_sum: 84.99472558498383
time_elpased: 3.238
batch start
#iterations: 326
currently lose_sum: 84.33942717313766
time_elpased: 3.071
batch start
#iterations: 327
currently lose_sum: 84.83258026838303
time_elpased: 2.966
batch start
#iterations: 328
currently lose_sum: 83.92375081777573
time_elpased: 2.995
batch start
#iterations: 329
currently lose_sum: 84.35572004318237
time_elpased: 2.961
batch start
#iterations: 330
currently lose_sum: 84.34766298532486
time_elpased: 3.256
batch start
#iterations: 331
currently lose_sum: 84.26144203543663
time_elpased: 3.187
batch start
#iterations: 332
currently lose_sum: 84.33685100078583
time_elpased: 3.177
batch start
#iterations: 333
currently lose_sum: 83.58697360754013
time_elpased: 2.911
batch start
#iterations: 334
currently lose_sum: 84.62352979183197
time_elpased: 3.114
batch start
#iterations: 335
currently lose_sum: 84.39796990156174
time_elpased: 3.173
batch start
#iterations: 336
currently lose_sum: 84.18390747904778
time_elpased: 3.316
batch start
#iterations: 337
currently lose_sum: 84.56222450733185
time_elpased: 3.119
batch start
#iterations: 338
currently lose_sum: 84.77796828746796
time_elpased: 3.146
batch start
#iterations: 339
currently lose_sum: 83.66518050432205
time_elpased: 2.943
start validation test
0.598969072165
0.615807724501
0.529998970876
0.569690265487
0.599090159816
70.816
batch start
#iterations: 340
currently lose_sum: 84.02387547492981
time_elpased: 3.348
batch start
#iterations: 341
currently lose_sum: 84.34248250722885
time_elpased: 3.19
batch start
#iterations: 342
currently lose_sum: 84.41399663686752
time_elpased: 3.323
batch start
#iterations: 343
currently lose_sum: 83.83785545825958
time_elpased: 2.982
batch start
#iterations: 344
currently lose_sum: 83.92702004313469
time_elpased: 3.126
batch start
#iterations: 345
currently lose_sum: 83.39217031002045
time_elpased: 3.392
batch start
#iterations: 346
currently lose_sum: 83.79617410898209
time_elpased: 2.939
batch start
#iterations: 347
currently lose_sum: 83.14627066254616
time_elpased: 3.096
batch start
#iterations: 348
currently lose_sum: 83.67176470160484
time_elpased: 3.108
batch start
#iterations: 349
currently lose_sum: 82.67969888448715
time_elpased: 2.868
batch start
#iterations: 350
currently lose_sum: 82.75827306509018
time_elpased: 2.854
batch start
#iterations: 351
currently lose_sum: 83.03224611282349
time_elpased: 2.982
batch start
#iterations: 352
currently lose_sum: 83.11388957500458
time_elpased: 3.103
batch start
#iterations: 353
currently lose_sum: 84.0052524805069
time_elpased: 3.373
batch start
#iterations: 354
currently lose_sum: 83.39676529169083
time_elpased: 3.058
batch start
#iterations: 355
currently lose_sum: 83.00874298810959
time_elpased: 3.165
batch start
#iterations: 356
currently lose_sum: 83.2619378566742
time_elpased: 3.209
batch start
#iterations: 357
currently lose_sum: 83.10413819551468
time_elpased: 2.985
batch start
#iterations: 358
currently lose_sum: 82.66744887828827
time_elpased: 3.106
batch start
#iterations: 359
currently lose_sum: 82.31837338209152
time_elpased: 3.053
start validation test
0.597371134021
0.635832383124
0.459092312442
0.533197872468
0.59761390382
75.527
batch start
#iterations: 360
currently lose_sum: 82.65877148509026
time_elpased: 3.116
batch start
#iterations: 361
currently lose_sum: 83.15720021724701
time_elpased: 3.027
batch start
#iterations: 362
currently lose_sum: 82.38353884220123
time_elpased: 2.937
batch start
#iterations: 363
currently lose_sum: 82.15358871221542
time_elpased: 3.271
batch start
#iterations: 364
currently lose_sum: 82.92010116577148
time_elpased: 3.297
batch start
#iterations: 365
currently lose_sum: 81.88125973939896
time_elpased: 3.261
batch start
#iterations: 366
currently lose_sum: 82.58306157588959
time_elpased: 3.151
batch start
#iterations: 367
currently lose_sum: 82.21634072065353
time_elpased: 2.999
batch start
#iterations: 368
currently lose_sum: 82.28026044368744
time_elpased: 3.154
batch start
#iterations: 369
currently lose_sum: 81.87047702074051
time_elpased: 3.22
batch start
#iterations: 370
currently lose_sum: 81.44687122106552
time_elpased: 3.198
batch start
#iterations: 371
currently lose_sum: 81.60984826087952
time_elpased: 3.181
batch start
#iterations: 372
currently lose_sum: 81.74929291009903
time_elpased: 3.153
batch start
#iterations: 373
currently lose_sum: 81.86001175642014
time_elpased: 3.157
batch start
#iterations: 374
currently lose_sum: 81.70592167973518
time_elpased: 3.249
batch start
#iterations: 375
currently lose_sum: 81.30384141206741
time_elpased: 2.99
batch start
#iterations: 376
currently lose_sum: 81.18807232379913
time_elpased: 3.161
batch start
#iterations: 377
currently lose_sum: 81.28461158275604
time_elpased: 3.367
batch start
#iterations: 378
currently lose_sum: 81.37890461087227
time_elpased: 3.459
batch start
#iterations: 379
currently lose_sum: 81.10481598973274
time_elpased: 2.851
start validation test
0.599020618557
0.617454545455
0.52423587527
0.56703957255
0.599151914708
74.372
batch start
#iterations: 380
currently lose_sum: 80.60227164626122
time_elpased: 3.243
batch start
#iterations: 381
currently lose_sum: 81.11232674121857
time_elpased: 2.955
batch start
#iterations: 382
currently lose_sum: 80.95252311229706
time_elpased: 3.272
batch start
#iterations: 383
currently lose_sum: 81.05904790759087
time_elpased: 3.143
batch start
#iterations: 384
currently lose_sum: 80.5185230076313
time_elpased: 3.244
batch start
#iterations: 385
currently lose_sum: 80.65011557936668
time_elpased: 2.964
batch start
#iterations: 386
currently lose_sum: 80.35829409956932
time_elpased: 3.191
batch start
#iterations: 387
currently lose_sum: 79.74177992343903
time_elpased: 3.12
batch start
#iterations: 388
currently lose_sum: 81.1807718873024
time_elpased: 3.099
batch start
#iterations: 389
currently lose_sum: 80.04389837384224
time_elpased: 3.042
batch start
#iterations: 390
currently lose_sum: 80.91133514046669
time_elpased: 3.351
batch start
#iterations: 391
currently lose_sum: 80.21089667081833
time_elpased: 3.164
batch start
#iterations: 392
currently lose_sum: 80.17781129479408
time_elpased: 2.959
batch start
#iterations: 393
currently lose_sum: 79.62853494286537
time_elpased: 3.074
batch start
#iterations: 394
currently lose_sum: 79.97828668355942
time_elpased: 3.187
batch start
#iterations: 395
currently lose_sum: 79.75805565714836
time_elpased: 3.072
batch start
#iterations: 396
currently lose_sum: 79.74416244029999
time_elpased: 3.225
batch start
#iterations: 397
currently lose_sum: 79.36727645993233
time_elpased: 3.158
batch start
#iterations: 398
currently lose_sum: 79.54129952192307
time_elpased: 3.058
batch start
#iterations: 399
currently lose_sum: 80.23284241557121
time_elpased: 3.24
start validation test
0.593350515464
0.613006923838
0.510239785942
0.556922212862
0.593496429169
79.792
acc: 0.651
pre: 0.633
rec: 0.723
F1: 0.675
auc: 0.700
