start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.45371186733246
time_elpased: 2.966
batch start
#iterations: 1
currently lose_sum: 100.4442988038063
time_elpased: 3.093
batch start
#iterations: 2
currently lose_sum: 100.23644554615021
time_elpased: 3.183
batch start
#iterations: 3
currently lose_sum: 100.0973242521286
time_elpased: 3.117
batch start
#iterations: 4
currently lose_sum: 100.08869165182114
time_elpased: 3.074
batch start
#iterations: 5
currently lose_sum: 99.9118492603302
time_elpased: 3.126
batch start
#iterations: 6
currently lose_sum: 99.69558387994766
time_elpased: 3.363
batch start
#iterations: 7
currently lose_sum: 99.59295219182968
time_elpased: 2.871
batch start
#iterations: 8
currently lose_sum: 99.32605123519897
time_elpased: 3.021
batch start
#iterations: 9
currently lose_sum: 99.2269252538681
time_elpased: 3.138
batch start
#iterations: 10
currently lose_sum: 99.07822287082672
time_elpased: 3.025
batch start
#iterations: 11
currently lose_sum: 98.87160485982895
time_elpased: 3.044
batch start
#iterations: 12
currently lose_sum: 98.86654275655746
time_elpased: 3.17
batch start
#iterations: 13
currently lose_sum: 98.809938788414
time_elpased: 3.06
batch start
#iterations: 14
currently lose_sum: 98.7188201546669
time_elpased: 3.105
batch start
#iterations: 15
currently lose_sum: 98.38572084903717
time_elpased: 3.096
batch start
#iterations: 16
currently lose_sum: 98.54277646541595
time_elpased: 3.07
batch start
#iterations: 17
currently lose_sum: 98.07554864883423
time_elpased: 2.983
batch start
#iterations: 18
currently lose_sum: 98.45803385972977
time_elpased: 3.076
batch start
#iterations: 19
currently lose_sum: 97.80268162488937
time_elpased: 2.945
start validation test
0.602680412371
0.577457182534
0.77037875669
0.66011112091
0.602403339518
64.039
batch start
#iterations: 20
currently lose_sum: 97.94241321086884
time_elpased: 2.797
batch start
#iterations: 21
currently lose_sum: 98.00020575523376
time_elpased: 2.962
batch start
#iterations: 22
currently lose_sum: 97.88905090093613
time_elpased: 2.911
batch start
#iterations: 23
currently lose_sum: 97.61336523294449
time_elpased: 3.143
batch start
#iterations: 24
currently lose_sum: 97.63282138109207
time_elpased: 3.006
batch start
#iterations: 25
currently lose_sum: 97.36023861169815
time_elpased: 3.043
batch start
#iterations: 26
currently lose_sum: 97.4899297952652
time_elpased: 2.976
batch start
#iterations: 27
currently lose_sum: 97.35460674762726
time_elpased: 3.188
batch start
#iterations: 28
currently lose_sum: 97.13180315494537
time_elpased: 3.269
batch start
#iterations: 29
currently lose_sum: 97.37970793247223
time_elpased: 2.848
batch start
#iterations: 30
currently lose_sum: 96.99999213218689
time_elpased: 2.982
batch start
#iterations: 31
currently lose_sum: 97.06704312562943
time_elpased: 2.844
batch start
#iterations: 32
currently lose_sum: 97.05841052532196
time_elpased: 3.045
batch start
#iterations: 33
currently lose_sum: 97.48152059316635
time_elpased: 2.97
batch start
#iterations: 34
currently lose_sum: 96.93480557203293
time_elpased: 3.199
batch start
#iterations: 35
currently lose_sum: 96.75801366567612
time_elpased: 2.986
batch start
#iterations: 36
currently lose_sum: 96.47249794006348
time_elpased: 2.833
batch start
#iterations: 37
currently lose_sum: 96.7894555926323
time_elpased: 2.899
batch start
#iterations: 38
currently lose_sum: 96.63445246219635
time_elpased: 3.085
batch start
#iterations: 39
currently lose_sum: 96.85723859071732
time_elpased: 3.157
start validation test
0.64824742268
0.619149637442
0.773363524084
0.687717371408
0.648040704628
61.825
batch start
#iterations: 40
currently lose_sum: 96.70937222242355
time_elpased: 2.799
batch start
#iterations: 41
currently lose_sum: 96.63967216014862
time_elpased: 3.086
batch start
#iterations: 42
currently lose_sum: 96.31614089012146
time_elpased: 3.109
batch start
#iterations: 43
currently lose_sum: 96.21797692775726
time_elpased: 3.171
batch start
#iterations: 44
currently lose_sum: 96.27351522445679
time_elpased: 3.098
batch start
#iterations: 45
currently lose_sum: 96.35573452711105
time_elpased: 2.937
batch start
#iterations: 46
currently lose_sum: 96.29680955410004
time_elpased: 3.152
batch start
#iterations: 47
currently lose_sum: 95.99441248178482
time_elpased: 3.103
batch start
#iterations: 48
currently lose_sum: 95.83209013938904
time_elpased: 3.066
batch start
#iterations: 49
currently lose_sum: 96.08712768554688
time_elpased: 3.166
batch start
#iterations: 50
currently lose_sum: 96.19283682107925
time_elpased: 2.847
batch start
#iterations: 51
currently lose_sum: 95.83800095319748
time_elpased: 2.694
batch start
#iterations: 52
currently lose_sum: 95.63540083169937
time_elpased: 3.081
batch start
#iterations: 53
currently lose_sum: 96.1272252202034
time_elpased: 3.019
batch start
#iterations: 54
currently lose_sum: 96.06891685724258
time_elpased: 3.025
batch start
#iterations: 55
currently lose_sum: 95.52318286895752
time_elpased: 3.304
batch start
#iterations: 56
currently lose_sum: 95.37661296129227
time_elpased: 3.114
batch start
#iterations: 57
currently lose_sum: 95.88825142383575
time_elpased: 3.137
batch start
#iterations: 58
currently lose_sum: 95.84175634384155
time_elpased: 3.109
batch start
#iterations: 59
currently lose_sum: 95.22802007198334
time_elpased: 2.974
start validation test
0.641958762887
0.615647962592
0.758851379168
0.679789784252
0.641765631757
61.631
batch start
#iterations: 60
currently lose_sum: 95.29766863584518
time_elpased: 3.152
batch start
#iterations: 61
currently lose_sum: 95.97254610061646
time_elpased: 3.313
batch start
#iterations: 62
currently lose_sum: 95.60970675945282
time_elpased: 3.267
batch start
#iterations: 63
currently lose_sum: 95.80010509490967
time_elpased: 3.06
batch start
#iterations: 64
currently lose_sum: 95.46441560983658
time_elpased: 3.082
batch start
#iterations: 65
currently lose_sum: 95.45691412687302
time_elpased: 3.046
batch start
#iterations: 66
currently lose_sum: 95.5511565208435
time_elpased: 3.096
batch start
#iterations: 67
currently lose_sum: 95.48352301120758
time_elpased: 3.003
batch start
#iterations: 68
currently lose_sum: 95.0095346570015
time_elpased: 3.019
batch start
#iterations: 69
currently lose_sum: 95.03688764572144
time_elpased: 3.101
batch start
#iterations: 70
currently lose_sum: 95.07389217615128
time_elpased: 3.024
batch start
#iterations: 71
currently lose_sum: 94.87420946359634
time_elpased: 2.709
batch start
#iterations: 72
currently lose_sum: 94.86826646327972
time_elpased: 2.956
batch start
#iterations: 73
currently lose_sum: 94.94004648923874
time_elpased: 2.999
batch start
#iterations: 74
currently lose_sum: 94.66622704267502
time_elpased: 3.08
batch start
#iterations: 75
currently lose_sum: 94.98330104351044
time_elpased: 3.199
batch start
#iterations: 76
currently lose_sum: 94.92658865451813
time_elpased: 3.159
batch start
#iterations: 77
currently lose_sum: 95.06218528747559
time_elpased: 3.054
batch start
#iterations: 78
currently lose_sum: 95.10434883832932
time_elpased: 3.136
batch start
#iterations: 79
currently lose_sum: 94.95333760976791
time_elpased: 2.677
start validation test
0.634381443299
0.615377848157
0.719946480033
0.663567803444
0.634240071904
61.830
batch start
#iterations: 80
currently lose_sum: 94.9069921374321
time_elpased: 3.327
batch start
#iterations: 81
currently lose_sum: 94.7243846654892
time_elpased: 2.991
batch start
#iterations: 82
currently lose_sum: 95.22748893499374
time_elpased: 3.101
batch start
#iterations: 83
currently lose_sum: 94.70585942268372
time_elpased: 3.222
batch start
#iterations: 84
currently lose_sum: 94.31811493635178
time_elpased: 2.849
batch start
#iterations: 85
currently lose_sum: 94.64769345521927
time_elpased: 2.948
batch start
#iterations: 86
currently lose_sum: 94.38351851701736
time_elpased: 3.135
batch start
#iterations: 87
currently lose_sum: 94.74809569120407
time_elpased: 2.992
batch start
#iterations: 88
currently lose_sum: 94.62795811891556
time_elpased: 2.921
batch start
#iterations: 89
currently lose_sum: 94.63013607263565
time_elpased: 2.829
batch start
#iterations: 90
currently lose_sum: 94.73832315206528
time_elpased: 3.43
batch start
#iterations: 91
currently lose_sum: 94.36008018255234
time_elpased: 3.095
batch start
#iterations: 92
currently lose_sum: 94.49315923452377
time_elpased: 2.838
batch start
#iterations: 93
currently lose_sum: 94.41463482379913
time_elpased: 2.917
batch start
#iterations: 94
currently lose_sum: 94.36724710464478
time_elpased: 2.573
batch start
#iterations: 95
currently lose_sum: 94.26043105125427
time_elpased: 2.783
batch start
#iterations: 96
currently lose_sum: 94.47497993707657
time_elpased: 3.046
batch start
#iterations: 97
currently lose_sum: 94.11295676231384
time_elpased: 3.201
batch start
#iterations: 98
currently lose_sum: 93.78324174880981
time_elpased: 2.66
batch start
#iterations: 99
currently lose_sum: 94.21752017736435
time_elpased: 2.939
start validation test
0.647989690722
0.629380657883
0.7227254014
0.672830929909
0.647866211646
60.984
batch start
#iterations: 100
currently lose_sum: 93.94188219308853
time_elpased: 3.101
batch start
#iterations: 101
currently lose_sum: 94.40560227632523
time_elpased: 3.007
batch start
#iterations: 102
currently lose_sum: 94.51108193397522
time_elpased: 3.177
batch start
#iterations: 103
currently lose_sum: 94.11320835351944
time_elpased: 3.05
batch start
#iterations: 104
currently lose_sum: 94.04239439964294
time_elpased: 3.099
batch start
#iterations: 105
currently lose_sum: 93.97468268871307
time_elpased: 3.192
batch start
#iterations: 106
currently lose_sum: 94.12639772891998
time_elpased: 3.038
batch start
#iterations: 107
currently lose_sum: 93.78588932752609
time_elpased: 3.01
batch start
#iterations: 108
currently lose_sum: 93.89950412511826
time_elpased: 3.088
batch start
#iterations: 109
currently lose_sum: 94.34131282567978
time_elpased: 3.053
batch start
#iterations: 110
currently lose_sum: 93.61740154027939
time_elpased: 3.007
batch start
#iterations: 111
currently lose_sum: 93.83437865972519
time_elpased: 2.983
batch start
#iterations: 112
currently lose_sum: 93.44101613759995
time_elpased: 2.953
batch start
#iterations: 113
currently lose_sum: 93.48758333921432
time_elpased: 2.971
batch start
#iterations: 114
currently lose_sum: 93.9376545548439
time_elpased: 3.017
batch start
#iterations: 115
currently lose_sum: 93.94616162776947
time_elpased: 2.866
batch start
#iterations: 116
currently lose_sum: 93.83965265750885
time_elpased: 3.023
batch start
#iterations: 117
currently lose_sum: 93.94906681776047
time_elpased: 3.119
batch start
#iterations: 118
currently lose_sum: 93.67692214250565
time_elpased: 2.93
batch start
#iterations: 119
currently lose_sum: 93.7913727760315
time_elpased: 3.105
start validation test
0.637371134021
0.620798413986
0.709036640593
0.66199010234
0.637252727566
61.248
batch start
#iterations: 120
currently lose_sum: 94.06461828947067
time_elpased: 3.148
batch start
#iterations: 121
currently lose_sum: 93.3291164636612
time_elpased: 3.196
batch start
#iterations: 122
currently lose_sum: 93.37155830860138
time_elpased: 2.635
batch start
#iterations: 123
currently lose_sum: 94.17862927913666
time_elpased: 3.229
batch start
#iterations: 124
currently lose_sum: 93.93944078683853
time_elpased: 2.658
batch start
#iterations: 125
currently lose_sum: 93.39424133300781
time_elpased: 3.073
batch start
#iterations: 126
currently lose_sum: 93.56469595432281
time_elpased: 3.141
batch start
#iterations: 127
currently lose_sum: 93.69396823644638
time_elpased: 3.261
batch start
#iterations: 128
currently lose_sum: 93.28221642971039
time_elpased: 3.052
batch start
#iterations: 129
currently lose_sum: 93.3270366191864
time_elpased: 3.036
batch start
#iterations: 130
currently lose_sum: 93.27823609113693
time_elpased: 3.272
batch start
#iterations: 131
currently lose_sum: 93.6699338555336
time_elpased: 3.315
batch start
#iterations: 132
currently lose_sum: 93.11108267307281
time_elpased: 3.015
batch start
#iterations: 133
currently lose_sum: 93.23495572805405
time_elpased: 3.112
batch start
#iterations: 134
currently lose_sum: 93.28232097625732
time_elpased: 2.836
batch start
#iterations: 135
currently lose_sum: 93.36311799287796
time_elpased: 2.806
batch start
#iterations: 136
currently lose_sum: 93.44459021091461
time_elpased: 3.01
batch start
#iterations: 137
currently lose_sum: 93.19363874197006
time_elpased: 3.082
batch start
#iterations: 138
currently lose_sum: 93.01546049118042
time_elpased: 2.914
batch start
#iterations: 139
currently lose_sum: 92.97050929069519
time_elpased: 2.887
start validation test
0.644948453608
0.631315007429
0.699670646357
0.66373755126
0.644858041063
61.056
batch start
#iterations: 140
currently lose_sum: 92.745620906353
time_elpased: 3.021
batch start
#iterations: 141
currently lose_sum: 92.95785921812057
time_elpased: 3.095
batch start
#iterations: 142
currently lose_sum: 92.90973615646362
time_elpased: 3.057
batch start
#iterations: 143
currently lose_sum: 93.01301842927933
time_elpased: 2.785
batch start
#iterations: 144
currently lose_sum: 92.81198519468307
time_elpased: 2.94
batch start
#iterations: 145
currently lose_sum: 93.04964923858643
time_elpased: 3.005
batch start
#iterations: 146
currently lose_sum: 92.69047915935516
time_elpased: 3.059
batch start
#iterations: 147
currently lose_sum: 92.7143292427063
time_elpased: 2.972
batch start
#iterations: 148
currently lose_sum: 93.0566456913948
time_elpased: 2.874
batch start
#iterations: 149
currently lose_sum: 93.32265102863312
time_elpased: 3.076
batch start
#iterations: 150
currently lose_sum: 93.11894285678864
time_elpased: 2.537
batch start
#iterations: 151
currently lose_sum: 93.19141733646393
time_elpased: 3.154
batch start
#iterations: 152
currently lose_sum: 92.95418548583984
time_elpased: 3.119
batch start
#iterations: 153
currently lose_sum: 92.9387377500534
time_elpased: 2.987
batch start
#iterations: 154
currently lose_sum: 92.87783235311508
time_elpased: 3.087
batch start
#iterations: 155
currently lose_sum: 93.05055052042007
time_elpased: 3.226
batch start
#iterations: 156
currently lose_sum: 92.5502524971962
time_elpased: 3.279
batch start
#iterations: 157
currently lose_sum: 92.43350154161453
time_elpased: 3.108
batch start
#iterations: 158
currently lose_sum: 92.56937164068222
time_elpased: 3.125
batch start
#iterations: 159
currently lose_sum: 92.67079442739487
time_elpased: 3.002
start validation test
0.629175257732
0.615922044493
0.689584191025
0.65067495387
0.629075449499
61.769
batch start
#iterations: 160
currently lose_sum: 92.32046508789062
time_elpased: 2.93
batch start
#iterations: 161
currently lose_sum: 92.53377640247345
time_elpased: 3.106
batch start
#iterations: 162
currently lose_sum: 92.80506920814514
time_elpased: 3.124
batch start
#iterations: 163
currently lose_sum: 92.4553319811821
time_elpased: 3.374
batch start
#iterations: 164
currently lose_sum: 92.54186922311783
time_elpased: 2.691
batch start
#iterations: 165
currently lose_sum: 92.59542202949524
time_elpased: 3.014
batch start
#iterations: 166
currently lose_sum: 92.35065340995789
time_elpased: 2.785
batch start
#iterations: 167
currently lose_sum: 92.1938311457634
time_elpased: 2.653
batch start
#iterations: 168
currently lose_sum: 91.94376730918884
time_elpased: 3.125
batch start
#iterations: 169
currently lose_sum: 91.92913496494293
time_elpased: 2.646
batch start
#iterations: 170
currently lose_sum: 91.91342014074326
time_elpased: 2.966
batch start
#iterations: 171
currently lose_sum: 92.05648899078369
time_elpased: 2.969
batch start
#iterations: 172
currently lose_sum: 92.29440224170685
time_elpased: 3.031
batch start
#iterations: 173
currently lose_sum: 92.04871916770935
time_elpased: 3.068
batch start
#iterations: 174
currently lose_sum: 91.98395609855652
time_elpased: 3.1
batch start
#iterations: 175
currently lose_sum: 91.73035997152328
time_elpased: 3.018
batch start
#iterations: 176
currently lose_sum: 92.52433264255524
time_elpased: 2.63
batch start
#iterations: 177
currently lose_sum: 92.04797458648682
time_elpased: 3.067
batch start
#iterations: 178
currently lose_sum: 91.8064336180687
time_elpased: 2.983
batch start
#iterations: 179
currently lose_sum: 91.5593489408493
time_elpased: 2.906
start validation test
0.632783505155
0.6232173417
0.674660354055
0.647919343679
0.632714315813
61.773
batch start
#iterations: 180
currently lose_sum: 91.82028818130493
time_elpased: 3.013
batch start
#iterations: 181
currently lose_sum: 92.31839561462402
time_elpased: 2.77
batch start
#iterations: 182
currently lose_sum: 92.10444861650467
time_elpased: 3.002
batch start
#iterations: 183
currently lose_sum: 91.6996219754219
time_elpased: 3.01
batch start
#iterations: 184
currently lose_sum: 92.61760848760605
time_elpased: 3.26
batch start
#iterations: 185
currently lose_sum: 91.73701387643814
time_elpased: 3.138
batch start
#iterations: 186
currently lose_sum: 91.58835422992706
time_elpased: 2.819
batch start
#iterations: 187
currently lose_sum: 91.25161129236221
time_elpased: 3.249
batch start
#iterations: 188
currently lose_sum: 92.01156741380692
time_elpased: 3.261
batch start
#iterations: 189
currently lose_sum: 92.2405526638031
time_elpased: 3.204
batch start
#iterations: 190
currently lose_sum: 91.56296908855438
time_elpased: 3.049
batch start
#iterations: 191
currently lose_sum: 91.41885441541672
time_elpased: 3.077
batch start
#iterations: 192
currently lose_sum: 91.50780832767487
time_elpased: 3.095
batch start
#iterations: 193
currently lose_sum: 92.07248240709305
time_elpased: 2.939
batch start
#iterations: 194
currently lose_sum: 92.16136229038239
time_elpased: 2.869
batch start
#iterations: 195
currently lose_sum: 91.60903143882751
time_elpased: 3.16
batch start
#iterations: 196
currently lose_sum: 91.49108445644379
time_elpased: 2.984
batch start
#iterations: 197
currently lose_sum: 91.71655112504959
time_elpased: 2.668
batch start
#iterations: 198
currently lose_sum: 91.12146723270416
time_elpased: 2.923
batch start
#iterations: 199
currently lose_sum: 90.94476795196533
time_elpased: 3.126
start validation test
0.626030927835
0.633793628357
0.599938246192
0.616401417015
0.62607403842
62.999
batch start
#iterations: 200
currently lose_sum: 91.44375175237656
time_elpased: 3.113
batch start
#iterations: 201
currently lose_sum: 91.14108443260193
time_elpased: 3.033
batch start
#iterations: 202
currently lose_sum: 91.606929063797
time_elpased: 3.033
batch start
#iterations: 203
currently lose_sum: 91.7477195262909
time_elpased: 2.908
batch start
#iterations: 204
currently lose_sum: 91.61957985162735
time_elpased: 3.071
batch start
#iterations: 205
currently lose_sum: 91.25040817260742
time_elpased: 3.07
batch start
#iterations: 206
currently lose_sum: 91.1843490600586
time_elpased: 2.982
batch start
#iterations: 207
currently lose_sum: 91.14576572179794
time_elpased: 3.062
batch start
#iterations: 208
currently lose_sum: 91.06759691238403
time_elpased: 3.17
batch start
#iterations: 209
currently lose_sum: 90.81537717580795
time_elpased: 2.936
batch start
#iterations: 210
currently lose_sum: 90.63710904121399
time_elpased: 2.921
batch start
#iterations: 211
currently lose_sum: 90.80068320035934
time_elpased: 3.058
batch start
#iterations: 212
currently lose_sum: 91.27982676029205
time_elpased: 3.235
batch start
#iterations: 213
currently lose_sum: 90.87685823440552
time_elpased: 2.996
batch start
#iterations: 214
currently lose_sum: 90.64139747619629
time_elpased: 3.204
batch start
#iterations: 215
currently lose_sum: 91.18107903003693
time_elpased: 3.041
batch start
#iterations: 216
currently lose_sum: 90.79267781972885
time_elpased: 3.154
batch start
#iterations: 217
currently lose_sum: 90.69901937246323
time_elpased: 3.045
batch start
#iterations: 218
currently lose_sum: 91.04450452327728
time_elpased: 2.977
batch start
#iterations: 219
currently lose_sum: 90.42867708206177
time_elpased: 3.047
start validation test
0.625773195876
0.619292791918
0.65613421161
0.637181409295
0.625723033108
62.840
batch start
#iterations: 220
currently lose_sum: 90.70910024642944
time_elpased: 3.112
batch start
#iterations: 221
currently lose_sum: 90.66555857658386
time_elpased: 3.2
batch start
#iterations: 222
currently lose_sum: 90.16993021965027
time_elpased: 3.062
batch start
#iterations: 223
currently lose_sum: 90.51865983009338
time_elpased: 3.129
batch start
#iterations: 224
currently lose_sum: 90.82862484455109
time_elpased: 3.151
batch start
#iterations: 225
currently lose_sum: 90.29117435216904
time_elpased: 3.094
batch start
#iterations: 226
currently lose_sum: 90.09308069944382
time_elpased: 2.768
batch start
#iterations: 227
currently lose_sum: 90.48800051212311
time_elpased: 2.822
batch start
#iterations: 228
currently lose_sum: 90.189113676548
time_elpased: 3.33
batch start
#iterations: 229
currently lose_sum: 90.16404813528061
time_elpased: 3.302
batch start
#iterations: 230
currently lose_sum: 90.58427214622498
time_elpased: 2.827
batch start
#iterations: 231
currently lose_sum: 90.54914945363998
time_elpased: 2.585
batch start
#iterations: 232
currently lose_sum: 90.38765794038773
time_elpased: 3.037
batch start
#iterations: 233
currently lose_sum: 90.07330852746964
time_elpased: 3.131
batch start
#iterations: 234
currently lose_sum: 90.18980938196182
time_elpased: 2.702
batch start
#iterations: 235
currently lose_sum: 90.27366000413895
time_elpased: 3.115
batch start
#iterations: 236
currently lose_sum: 90.37371516227722
time_elpased: 2.98
batch start
#iterations: 237
currently lose_sum: 90.19788473844528
time_elpased: 3.132
batch start
#iterations: 238
currently lose_sum: 90.34290510416031
time_elpased: 3.159
batch start
#iterations: 239
currently lose_sum: 89.72147977352142
time_elpased: 3.079
start validation test
0.631134020619
0.63104013104
0.634417455743
0.632724286594
0.631128595695
63.651
batch start
#iterations: 240
currently lose_sum: 89.80366080999374
time_elpased: 3.113
batch start
#iterations: 241
currently lose_sum: 90.11929547786713
time_elpased: 3.122
batch start
#iterations: 242
currently lose_sum: 89.70622462034225
time_elpased: 2.848
batch start
#iterations: 243
currently lose_sum: 90.03929615020752
time_elpased: 2.792
batch start
#iterations: 244
currently lose_sum: 89.91525119543076
time_elpased: 3.028
batch start
#iterations: 245
currently lose_sum: 89.99742895364761
time_elpased: 3.306
batch start
#iterations: 246
currently lose_sum: 89.4393293261528
time_elpased: 3.263
batch start
#iterations: 247
currently lose_sum: 89.79613161087036
time_elpased: 2.935
batch start
#iterations: 248
currently lose_sum: 89.37573230266571
time_elpased: 3.103
batch start
#iterations: 249
currently lose_sum: 90.05694228410721
time_elpased: 2.894
batch start
#iterations: 250
currently lose_sum: 89.66563814878464
time_elpased: 2.975
batch start
#iterations: 251
currently lose_sum: 89.65993160009384
time_elpased: 3.222
batch start
#iterations: 252
currently lose_sum: 89.85334694385529
time_elpased: 3.09
batch start
#iterations: 253
currently lose_sum: 89.0329978466034
time_elpased: 3.044
batch start
#iterations: 254
currently lose_sum: 89.27153903245926
time_elpased: 2.956
batch start
#iterations: 255
currently lose_sum: 89.28559958934784
time_elpased: 2.769
batch start
#iterations: 256
currently lose_sum: 89.39002376794815
time_elpased: 3.094
batch start
#iterations: 257
currently lose_sum: 89.17171388864517
time_elpased: 2.998
batch start
#iterations: 258
currently lose_sum: 89.27855068445206
time_elpased: 3.032
batch start
#iterations: 259
currently lose_sum: 89.44315445423126
time_elpased: 2.894
start validation test
0.608608247423
0.616712479384
0.577295183203
0.596353197597
0.608659983175
64.211
batch start
#iterations: 260
currently lose_sum: 89.2406547665596
time_elpased: 3.126
batch start
#iterations: 261
currently lose_sum: 89.17415589094162
time_elpased: 3.057
batch start
#iterations: 262
currently lose_sum: 88.71787750720978
time_elpased: 3.013
batch start
#iterations: 263
currently lose_sum: 88.97547870874405
time_elpased: 3.153
batch start
#iterations: 264
currently lose_sum: 89.00389897823334
time_elpased: 3.246
batch start
#iterations: 265
currently lose_sum: 88.4532750248909
time_elpased: 3.205
batch start
#iterations: 266
currently lose_sum: 88.2604666352272
time_elpased: 2.787
batch start
#iterations: 267
currently lose_sum: 88.90589755773544
time_elpased: 3.259
batch start
#iterations: 268
currently lose_sum: 88.31736159324646
time_elpased: 3.067
batch start
#iterations: 269
currently lose_sum: 88.33433955907822
time_elpased: 3.078
batch start
#iterations: 270
currently lose_sum: 88.3206804394722
time_elpased: 3.137
batch start
#iterations: 271
currently lose_sum: 89.03269374370575
time_elpased: 3.0
batch start
#iterations: 272
currently lose_sum: 88.1666202545166
time_elpased: 2.884
batch start
#iterations: 273
currently lose_sum: 88.32158201932907
time_elpased: 3.079
batch start
#iterations: 274
currently lose_sum: 88.38754683732986
time_elpased: 3.096
batch start
#iterations: 275
currently lose_sum: 88.77477151155472
time_elpased: 2.869
batch start
#iterations: 276
currently lose_sum: 87.9903016090393
time_elpased: 2.959
batch start
#iterations: 277
currently lose_sum: 87.4535641670227
time_elpased: 3.153
batch start
#iterations: 278
currently lose_sum: 88.35428386926651
time_elpased: 3.106
batch start
#iterations: 279
currently lose_sum: 88.05349057912827
time_elpased: 3.195
start validation test
0.623298969072
0.618901836856
0.645018526142
0.631690353795
0.623263083806
63.754
batch start
#iterations: 280
currently lose_sum: 88.52935135364532
time_elpased: 3.285
batch start
#iterations: 281
currently lose_sum: 87.40718239545822
time_elpased: 3.112
batch start
#iterations: 282
currently lose_sum: 88.03435909748077
time_elpased: 3.219
batch start
#iterations: 283
currently lose_sum: 87.86551570892334
time_elpased: 3.125
batch start
#iterations: 284
currently lose_sum: 87.52141016721725
time_elpased: 3.231
batch start
#iterations: 285
currently lose_sum: 87.65564489364624
time_elpased: 3.246
batch start
#iterations: 286
currently lose_sum: 88.20521223545074
time_elpased: 2.74
batch start
#iterations: 287
currently lose_sum: 87.71206003427505
time_elpased: 2.947
batch start
#iterations: 288
currently lose_sum: 87.68223881721497
time_elpased: 2.501
batch start
#iterations: 289
currently lose_sum: 87.46345096826553
time_elpased: 3.149
batch start
#iterations: 290
currently lose_sum: 87.41888958215714
time_elpased: 2.8
batch start
#iterations: 291
currently lose_sum: 87.85171830654144
time_elpased: 3.219
batch start
#iterations: 292
currently lose_sum: 87.40446573495865
time_elpased: 2.943
batch start
#iterations: 293
currently lose_sum: 87.40343487262726
time_elpased: 3.179
batch start
#iterations: 294
currently lose_sum: 87.00170242786407
time_elpased: 3.048
batch start
#iterations: 295
currently lose_sum: 87.56359422206879
time_elpased: 2.972
batch start
#iterations: 296
currently lose_sum: 87.04466170072556
time_elpased: 3.356
batch start
#iterations: 297
currently lose_sum: 87.22448062896729
time_elpased: 3.161
batch start
#iterations: 298
currently lose_sum: 86.7082906961441
time_elpased: 3.291
batch start
#iterations: 299
currently lose_sum: 86.90428441762924
time_elpased: 2.918
start validation test
0.60293814433
0.611006948274
0.570193495266
0.589895117926
0.602992245361
66.711
batch start
#iterations: 300
currently lose_sum: 86.69762372970581
time_elpased: 2.63
batch start
#iterations: 301
currently lose_sum: 86.92162990570068
time_elpased: 3.154
batch start
#iterations: 302
currently lose_sum: 86.94108200073242
time_elpased: 2.9
batch start
#iterations: 303
currently lose_sum: 86.69140958786011
time_elpased: 3.112
batch start
#iterations: 304
currently lose_sum: 86.52963411808014
time_elpased: 3.072
batch start
#iterations: 305
currently lose_sum: 86.70040047168732
time_elpased: 3.06
batch start
#iterations: 306
currently lose_sum: 87.07695543766022
time_elpased: 3.021
batch start
#iterations: 307
currently lose_sum: 86.3198167681694
time_elpased: 3.148
batch start
#iterations: 308
currently lose_sum: 86.31761908531189
time_elpased: 2.575
batch start
#iterations: 309
currently lose_sum: 86.05836099386215
time_elpased: 3.145
batch start
#iterations: 310
currently lose_sum: 86.47126132249832
time_elpased: 3.07
batch start
#iterations: 311
currently lose_sum: 86.1152862906456
time_elpased: 3.007
batch start
#iterations: 312
currently lose_sum: 86.21991020441055
time_elpased: 3.099
batch start
#iterations: 313
currently lose_sum: 85.75864386558533
time_elpased: 2.793
batch start
#iterations: 314
currently lose_sum: 85.53877741098404
time_elpased: 3.127
batch start
#iterations: 315
currently lose_sum: 85.95119309425354
time_elpased: 2.691
batch start
#iterations: 316
currently lose_sum: 86.08325481414795
time_elpased: 2.955
batch start
#iterations: 317
currently lose_sum: 86.05236285924911
time_elpased: 3.054
batch start
#iterations: 318
currently lose_sum: 85.80508828163147
time_elpased: 2.928
batch start
#iterations: 319
currently lose_sum: 85.92368942499161
time_elpased: 2.947
start validation test
0.604278350515
0.623351482154
0.530259365994
0.573049329848
0.604400645409
67.033
batch start
#iterations: 320
currently lose_sum: 85.36517369747162
time_elpased: 3.127
batch start
#iterations: 321
currently lose_sum: 85.38263213634491
time_elpased: 3.105
batch start
#iterations: 322
currently lose_sum: 85.33263713121414
time_elpased: 3.071
batch start
#iterations: 323
currently lose_sum: 85.10166317224503
time_elpased: 2.762
batch start
#iterations: 324
currently lose_sum: 84.81956481933594
time_elpased: 2.739
batch start
#iterations: 325
currently lose_sum: 84.79582464694977
time_elpased: 2.739
batch start
#iterations: 326
currently lose_sum: 85.11093586683273
time_elpased: 3.164
batch start
#iterations: 327
currently lose_sum: 85.15261733531952
time_elpased: 3.116
batch start
#iterations: 328
currently lose_sum: 85.16944324970245
time_elpased: 2.944
batch start
#iterations: 329
currently lose_sum: 84.80875861644745
time_elpased: 3.283
batch start
#iterations: 330
currently lose_sum: 84.30857765674591
time_elpased: 3.05
batch start
#iterations: 331
currently lose_sum: 84.50736331939697
time_elpased: 3.29
batch start
#iterations: 332
currently lose_sum: 84.78663957118988
time_elpased: 2.624
batch start
#iterations: 333
currently lose_sum: 84.71172648668289
time_elpased: 2.945
batch start
#iterations: 334
currently lose_sum: 84.38598209619522
time_elpased: 2.984
batch start
#iterations: 335
currently lose_sum: 84.32681941986084
time_elpased: 2.615
batch start
#iterations: 336
currently lose_sum: 84.9666258096695
time_elpased: 2.997
batch start
#iterations: 337
currently lose_sum: 84.45233315229416
time_elpased: 3.178
batch start
#iterations: 338
currently lose_sum: 84.74002915620804
time_elpased: 3.062
batch start
#iterations: 339
currently lose_sum: 84.29014086723328
time_elpased: 2.961
start validation test
0.599536082474
0.620587142326
0.515644298065
0.563269436168
0.599674689305
69.689
batch start
#iterations: 340
currently lose_sum: 84.46613276004791
time_elpased: 3.183
batch start
#iterations: 341
currently lose_sum: 84.33209228515625
time_elpased: 3.012
batch start
#iterations: 342
currently lose_sum: 83.76405507326126
time_elpased: 3.227
batch start
#iterations: 343
currently lose_sum: 84.1923058629036
time_elpased: 2.992
batch start
#iterations: 344
currently lose_sum: 83.51250284910202
time_elpased: 3.027
batch start
#iterations: 345
currently lose_sum: 83.65766936540604
time_elpased: 3.161
batch start
#iterations: 346
currently lose_sum: 83.9258446097374
time_elpased: 2.903
batch start
#iterations: 347
currently lose_sum: 83.56255006790161
time_elpased: 2.736
batch start
#iterations: 348
currently lose_sum: 83.87385678291321
time_elpased: 2.997
batch start
#iterations: 349
currently lose_sum: 83.15143913030624
time_elpased: 3.062
batch start
#iterations: 350
currently lose_sum: 84.15410608053207
time_elpased: 2.831
batch start
#iterations: 351
currently lose_sum: 83.85423308610916
time_elpased: 3.043
batch start
#iterations: 352
currently lose_sum: 82.90185391902924
time_elpased: 3.01
batch start
#iterations: 353
currently lose_sum: 83.51503717899323
time_elpased: 3.067
batch start
#iterations: 354
currently lose_sum: 83.12059405446053
time_elpased: 2.964
batch start
#iterations: 355
currently lose_sum: 83.32271510362625
time_elpased: 3.097
batch start
#iterations: 356
currently lose_sum: 83.24460297822952
time_elpased: 3.204
batch start
#iterations: 357
currently lose_sum: 82.78771579265594
time_elpased: 3.056
batch start
#iterations: 358
currently lose_sum: 82.57638156414032
time_elpased: 2.818
batch start
#iterations: 359
currently lose_sum: 82.19620877504349
time_elpased: 2.654
start validation test
0.603556701031
0.615332042374
0.555990119391
0.584157880508
0.603635291005
70.301
batch start
#iterations: 360
currently lose_sum: 82.83458143472672
time_elpased: 3.004
batch start
#iterations: 361
currently lose_sum: 82.10098695755005
time_elpased: 3.144
batch start
#iterations: 362
currently lose_sum: 82.8008970618248
time_elpased: 3.191
batch start
#iterations: 363
currently lose_sum: 82.48372146487236
time_elpased: 3.159
batch start
#iterations: 364
currently lose_sum: 82.22481572628021
time_elpased: 3.129
batch start
#iterations: 365
currently lose_sum: 82.19263273477554
time_elpased: 3.062
batch start
#iterations: 366
currently lose_sum: 82.80913054943085
time_elpased: 3.237
batch start
#iterations: 367
currently lose_sum: 82.20888882875443
time_elpased: 3.184
batch start
#iterations: 368
currently lose_sum: 81.67566847801208
time_elpased: 2.989
batch start
#iterations: 369
currently lose_sum: 81.60617280006409
time_elpased: 3.174
batch start
#iterations: 370
currently lose_sum: 81.85747727751732
time_elpased: 2.628
batch start
#iterations: 371
currently lose_sum: 81.94825205206871
time_elpased: 3.025
batch start
#iterations: 372
currently lose_sum: 81.91342893242836
time_elpased: 2.925
batch start
#iterations: 373
currently lose_sum: 81.33406487107277
time_elpased: 3.004
batch start
#iterations: 374
currently lose_sum: 81.59739148616791
time_elpased: 3.355
batch start
#iterations: 375
currently lose_sum: 81.70610129833221
time_elpased: 2.983
batch start
#iterations: 376
currently lose_sum: 81.2584802210331
time_elpased: 2.966
batch start
#iterations: 377
currently lose_sum: 81.50034525990486
time_elpased: 3.159
batch start
#iterations: 378
currently lose_sum: 81.13259547948837
time_elpased: 3.091
batch start
#iterations: 379
currently lose_sum: 80.97883719205856
time_elpased: 3.093
start validation test
0.602835051546
0.623967451609
0.520893371758
0.567790430246
0.602970436395
73.888
batch start
#iterations: 380
currently lose_sum: 80.67563515901566
time_elpased: 2.907
batch start
#iterations: 381
currently lose_sum: 81.33014822006226
time_elpased: 3.221
batch start
#iterations: 382
currently lose_sum: 81.81733685731888
time_elpased: 3.202
batch start
#iterations: 383
currently lose_sum: 80.30841320753098
time_elpased: 3.128
batch start
#iterations: 384
currently lose_sum: 80.54861345887184
time_elpased: 2.829
batch start
#iterations: 385
currently lose_sum: 80.76948934793472
time_elpased: 2.956
batch start
#iterations: 386
currently lose_sum: 80.4969542324543
time_elpased: 3.054
batch start
#iterations: 387
currently lose_sum: 79.84084475040436
time_elpased: 2.931
batch start
#iterations: 388
currently lose_sum: 79.94637650251389
time_elpased: 3.144
batch start
#iterations: 389
currently lose_sum: 79.69419777393341
time_elpased: 2.909
batch start
#iterations: 390
currently lose_sum: 80.28483799099922
time_elpased: 2.941
batch start
#iterations: 391
currently lose_sum: 79.63020086288452
time_elpased: 2.893
batch start
#iterations: 392
currently lose_sum: 80.19373491406441
time_elpased: 2.855
batch start
#iterations: 393
currently lose_sum: 80.12727046012878
time_elpased: 3.117
batch start
#iterations: 394
currently lose_sum: 79.61931478977203
time_elpased: 3.309
batch start
#iterations: 395
currently lose_sum: 79.97461435198784
time_elpased: 2.846
batch start
#iterations: 396
currently lose_sum: 79.6775894165039
time_elpased: 2.897
batch start
#iterations: 397
currently lose_sum: 79.5707565844059
time_elpased: 2.998
batch start
#iterations: 398
currently lose_sum: 79.83571800589561
time_elpased: 2.952
batch start
#iterations: 399
currently lose_sum: 79.07091590762138
time_elpased: 3.1
start validation test
0.589639175258
0.629215137682
0.439790037052
0.517719755255
0.589886757477
75.308
acc: 0.656
pre: 0.637
rec: 0.728
F1: 0.679
auc: 0.710
