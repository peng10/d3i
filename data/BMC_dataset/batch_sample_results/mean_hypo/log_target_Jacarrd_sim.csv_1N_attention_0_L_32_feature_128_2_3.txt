start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.45888769626617
time_elpased: 3.068
batch start
#iterations: 1
currently lose_sum: 100.09104371070862
time_elpased: 3.002
batch start
#iterations: 2
currently lose_sum: 99.93652552366257
time_elpased: 2.583
batch start
#iterations: 3
currently lose_sum: 99.83574992418289
time_elpased: 2.888
batch start
#iterations: 4
currently lose_sum: 99.83262991905212
time_elpased: 3.156
batch start
#iterations: 5
currently lose_sum: 99.7490536570549
time_elpased: 2.918
batch start
#iterations: 6
currently lose_sum: 99.5356775522232
time_elpased: 3.042
batch start
#iterations: 7
currently lose_sum: 99.49557566642761
time_elpased: 3.054
batch start
#iterations: 8
currently lose_sum: 99.39347475767136
time_elpased: 2.924
batch start
#iterations: 9
currently lose_sum: 99.32614016532898
time_elpased: 2.93
batch start
#iterations: 10
currently lose_sum: 99.25385588407516
time_elpased: 3.08
batch start
#iterations: 11
currently lose_sum: 99.44166713953018
time_elpased: 3.061
batch start
#iterations: 12
currently lose_sum: 99.084945499897
time_elpased: 3.114
batch start
#iterations: 13
currently lose_sum: 99.29881197214127
time_elpased: 3.197
batch start
#iterations: 14
currently lose_sum: 99.18287163972855
time_elpased: 3.079
batch start
#iterations: 15
currently lose_sum: 99.13889694213867
time_elpased: 3.082
batch start
#iterations: 16
currently lose_sum: 99.13429915904999
time_elpased: 2.765
batch start
#iterations: 17
currently lose_sum: 99.03660893440247
time_elpased: 2.787
batch start
#iterations: 18
currently lose_sum: 98.61392027139664
time_elpased: 2.796
batch start
#iterations: 19
currently lose_sum: 98.39528131484985
time_elpased: 3.05
start validation test
0.627371134021
0.630551181102
0.618155619597
0.624291876722
0.627386359984
63.729
batch start
#iterations: 20
currently lose_sum: 98.08830827474594
time_elpased: 3.052
batch start
#iterations: 21
currently lose_sum: 97.86031883955002
time_elpased: 2.978
batch start
#iterations: 22
currently lose_sum: 97.5443605184555
time_elpased: 3.029
batch start
#iterations: 23
currently lose_sum: 97.45805299282074
time_elpased: 3.021
batch start
#iterations: 24
currently lose_sum: 97.46592223644257
time_elpased: 3.036
batch start
#iterations: 25
currently lose_sum: 97.03943037986755
time_elpased: 2.922
batch start
#iterations: 26
currently lose_sum: 96.67189174890518
time_elpased: 3.015
batch start
#iterations: 27
currently lose_sum: 96.92101752758026
time_elpased: 2.974
batch start
#iterations: 28
currently lose_sum: 96.58442795276642
time_elpased: 2.852
batch start
#iterations: 29
currently lose_sum: 96.8253618478775
time_elpased: 3.509
batch start
#iterations: 30
currently lose_sum: 96.49620699882507
time_elpased: 2.896
batch start
#iterations: 31
currently lose_sum: 96.35339951515198
time_elpased: 2.987
batch start
#iterations: 32
currently lose_sum: 96.16798514127731
time_elpased: 2.999
batch start
#iterations: 33
currently lose_sum: 96.45866119861603
time_elpased: 3.093
batch start
#iterations: 34
currently lose_sum: 95.78261488676071
time_elpased: 2.846
batch start
#iterations: 35
currently lose_sum: 96.384022295475
time_elpased: 2.824
batch start
#iterations: 36
currently lose_sum: 95.72382712364197
time_elpased: 2.889
batch start
#iterations: 37
currently lose_sum: 95.99684238433838
time_elpased: 3.053
batch start
#iterations: 38
currently lose_sum: 95.93872624635696
time_elpased: 2.932
batch start
#iterations: 39
currently lose_sum: 95.88695418834686
time_elpased: 3.041
start validation test
0.626958762887
0.61810385898
0.667661589131
0.641927663154
0.626891513277
62.545
batch start
#iterations: 40
currently lose_sum: 95.73257094621658
time_elpased: 3.156
batch start
#iterations: 41
currently lose_sum: 95.9188323020935
time_elpased: 3.184
batch start
#iterations: 42
currently lose_sum: 95.45704787969589
time_elpased: 3.289
batch start
#iterations: 43
currently lose_sum: 95.6914234161377
time_elpased: 3.232
batch start
#iterations: 44
currently lose_sum: 95.66695886850357
time_elpased: 2.929
batch start
#iterations: 45
currently lose_sum: 95.62294644117355
time_elpased: 2.73
batch start
#iterations: 46
currently lose_sum: 95.45806384086609
time_elpased: 2.768
batch start
#iterations: 47
currently lose_sum: 95.05659872293472
time_elpased: 2.823
batch start
#iterations: 48
currently lose_sum: 95.01845371723175
time_elpased: 2.975
batch start
#iterations: 49
currently lose_sum: 95.11995202302933
time_elpased: 3.021
batch start
#iterations: 50
currently lose_sum: 95.39993089437485
time_elpased: 2.674
batch start
#iterations: 51
currently lose_sum: 94.81420409679413
time_elpased: 2.993
batch start
#iterations: 52
currently lose_sum: 95.3054336309433
time_elpased: 2.92
batch start
#iterations: 53
currently lose_sum: 94.93522185087204
time_elpased: 3.097
batch start
#iterations: 54
currently lose_sum: 94.99443185329437
time_elpased: 3.149
batch start
#iterations: 55
currently lose_sum: 94.94980585575104
time_elpased: 2.929
batch start
#iterations: 56
currently lose_sum: 95.10153144598007
time_elpased: 2.973
batch start
#iterations: 57
currently lose_sum: 94.96812498569489
time_elpased: 3.256
batch start
#iterations: 58
currently lose_sum: 94.6848493218422
time_elpased: 3.083
batch start
#iterations: 59
currently lose_sum: 94.61438065767288
time_elpased: 3.019
start validation test
0.637680412371
0.635748206527
0.647591601482
0.641615255188
0.637664037007
61.705
batch start
#iterations: 60
currently lose_sum: 94.44425618648529
time_elpased: 3.053
batch start
#iterations: 61
currently lose_sum: 94.90141481161118
time_elpased: 3.115
batch start
#iterations: 62
currently lose_sum: 94.88040524721146
time_elpased: 3.051
batch start
#iterations: 63
currently lose_sum: 94.57084113359451
time_elpased: 3.011
batch start
#iterations: 64
currently lose_sum: 94.5799310207367
time_elpased: 2.672
batch start
#iterations: 65
currently lose_sum: 94.46189445257187
time_elpased: 2.39
batch start
#iterations: 66
currently lose_sum: 94.57028323411942
time_elpased: 2.859
batch start
#iterations: 67
currently lose_sum: 94.46158933639526
time_elpased: 2.977
batch start
#iterations: 68
currently lose_sum: 94.12992078065872
time_elpased: 2.891
batch start
#iterations: 69
currently lose_sum: 94.21847116947174
time_elpased: 2.643
batch start
#iterations: 70
currently lose_sum: 94.23516499996185
time_elpased: 3.176
batch start
#iterations: 71
currently lose_sum: 94.02661365270615
time_elpased: 2.67
batch start
#iterations: 72
currently lose_sum: 93.89789700508118
time_elpased: 2.897
batch start
#iterations: 73
currently lose_sum: 93.89367145299911
time_elpased: 3.087
batch start
#iterations: 74
currently lose_sum: 94.00857776403427
time_elpased: 3.081
batch start
#iterations: 75
currently lose_sum: 94.37353682518005
time_elpased: 2.883
batch start
#iterations: 76
currently lose_sum: 94.41679406166077
time_elpased: 3.043
batch start
#iterations: 77
currently lose_sum: 94.15098655223846
time_elpased: 3.057
batch start
#iterations: 78
currently lose_sum: 94.39081573486328
time_elpased: 3.155
batch start
#iterations: 79
currently lose_sum: 93.64511466026306
time_elpased: 3.07
start validation test
0.648556701031
0.641919686582
0.674557431042
0.657833985747
0.648513742369
60.982
batch start
#iterations: 80
currently lose_sum: 94.25981867313385
time_elpased: 2.546
batch start
#iterations: 81
currently lose_sum: 94.08094614744186
time_elpased: 2.758
batch start
#iterations: 82
currently lose_sum: 93.72167384624481
time_elpased: 3.121
batch start
#iterations: 83
currently lose_sum: 93.97569060325623
time_elpased: 2.695
batch start
#iterations: 84
currently lose_sum: 93.53131657838821
time_elpased: 2.61
batch start
#iterations: 85
currently lose_sum: 93.18716049194336
time_elpased: 2.801
batch start
#iterations: 86
currently lose_sum: 93.6587695479393
time_elpased: 2.811
batch start
#iterations: 87
currently lose_sum: 93.53676152229309
time_elpased: 3.079
batch start
#iterations: 88
currently lose_sum: 93.81985938549042
time_elpased: 3.11
batch start
#iterations: 89
currently lose_sum: 93.9313697218895
time_elpased: 2.928
batch start
#iterations: 90
currently lose_sum: 93.5006332397461
time_elpased: 2.91
batch start
#iterations: 91
currently lose_sum: 93.47063279151917
time_elpased: 2.974
batch start
#iterations: 92
currently lose_sum: 93.53101807832718
time_elpased: 3.02
batch start
#iterations: 93
currently lose_sum: 93.39708626270294
time_elpased: 2.949
batch start
#iterations: 94
currently lose_sum: 93.39553040266037
time_elpased: 2.92
batch start
#iterations: 95
currently lose_sum: 93.87828940153122
time_elpased: 2.805
batch start
#iterations: 96
currently lose_sum: 93.05374377965927
time_elpased: 3.042
batch start
#iterations: 97
currently lose_sum: 92.81424671411514
time_elpased: 2.907
batch start
#iterations: 98
currently lose_sum: 93.36558079719543
time_elpased: 2.648
batch start
#iterations: 99
currently lose_sum: 93.31865859031677
time_elpased: 2.795
start validation test
0.632680412371
0.630491737203
0.643989296007
0.63716904277
0.632661727722
61.618
batch start
#iterations: 100
currently lose_sum: 93.28670710325241
time_elpased: 3.046
batch start
#iterations: 101
currently lose_sum: 93.36364722251892
time_elpased: 2.983
batch start
#iterations: 102
currently lose_sum: 93.34510940313339
time_elpased: 3.099
batch start
#iterations: 103
currently lose_sum: 92.77304112911224
time_elpased: 2.952
batch start
#iterations: 104
currently lose_sum: 92.77500265836716
time_elpased: 2.877
batch start
#iterations: 105
currently lose_sum: 92.94847160577774
time_elpased: 3.038
batch start
#iterations: 106
currently lose_sum: 92.81147360801697
time_elpased: 3.225
batch start
#iterations: 107
currently lose_sum: 93.26620501279831
time_elpased: 2.975
batch start
#iterations: 108
currently lose_sum: 93.12634086608887
time_elpased: 3.262
batch start
#iterations: 109
currently lose_sum: 93.13553756475449
time_elpased: 3.009
batch start
#iterations: 110
currently lose_sum: 92.97486501932144
time_elpased: 3.053
batch start
#iterations: 111
currently lose_sum: 93.05717921257019
time_elpased: 3.045
batch start
#iterations: 112
currently lose_sum: 92.67147105932236
time_elpased: 2.744
batch start
#iterations: 113
currently lose_sum: 92.7749634385109
time_elpased: 2.805
batch start
#iterations: 114
currently lose_sum: 92.62205648422241
time_elpased: 2.901
batch start
#iterations: 115
currently lose_sum: 92.7025738954544
time_elpased: 2.78
batch start
#iterations: 116
currently lose_sum: 93.15885579586029
time_elpased: 3.087
batch start
#iterations: 117
currently lose_sum: 92.68082076311111
time_elpased: 2.735
batch start
#iterations: 118
currently lose_sum: 93.06579864025116
time_elpased: 2.863
batch start
#iterations: 119
currently lose_sum: 92.61475718021393
time_elpased: 2.994
start validation test
0.63
0.632796149016
0.622375463154
0.627542548775
0.630012597335
61.560
batch start
#iterations: 120
currently lose_sum: 91.95415014028549
time_elpased: 2.709
batch start
#iterations: 121
currently lose_sum: 92.46432214975357
time_elpased: 3.028
batch start
#iterations: 122
currently lose_sum: 92.2891657948494
time_elpased: 2.835
batch start
#iterations: 123
currently lose_sum: 92.68408983945847
time_elpased: 3.17
batch start
#iterations: 124
currently lose_sum: 92.12318736314774
time_elpased: 3.088
batch start
#iterations: 125
currently lose_sum: 92.47497349977493
time_elpased: 2.888
batch start
#iterations: 126
currently lose_sum: 92.08553975820541
time_elpased: 2.915
batch start
#iterations: 127
currently lose_sum: 92.36699306964874
time_elpased: 3.096
batch start
#iterations: 128
currently lose_sum: 92.25443410873413
time_elpased: 3.013
batch start
#iterations: 129
currently lose_sum: 92.27837151288986
time_elpased: 2.668
batch start
#iterations: 130
currently lose_sum: 92.23417407274246
time_elpased: 3.044
batch start
#iterations: 131
currently lose_sum: 91.5368400812149
time_elpased: 2.991
batch start
#iterations: 132
currently lose_sum: 92.1765655875206
time_elpased: 2.999
batch start
#iterations: 133
currently lose_sum: 92.20338147878647
time_elpased: 2.885
batch start
#iterations: 134
currently lose_sum: 91.99416118860245
time_elpased: 2.967
batch start
#iterations: 135
currently lose_sum: 91.72413051128387
time_elpased: 3.007
batch start
#iterations: 136
currently lose_sum: 91.88462394475937
time_elpased: 2.9
batch start
#iterations: 137
currently lose_sum: 91.91796839237213
time_elpased: 2.95
batch start
#iterations: 138
currently lose_sum: 92.09782528877258
time_elpased: 2.966
batch start
#iterations: 139
currently lose_sum: 91.42042112350464
time_elpased: 3.061
start validation test
0.636494845361
0.631698635555
0.6575751338
0.644377206253
0.636460016301
61.221
batch start
#iterations: 140
currently lose_sum: 91.08200913667679
time_elpased: 2.8
batch start
#iterations: 141
currently lose_sum: 91.74228578805923
time_elpased: 2.753
batch start
#iterations: 142
currently lose_sum: 91.15915995836258
time_elpased: 2.336
batch start
#iterations: 143
currently lose_sum: 91.75350815057755
time_elpased: 2.569
batch start
#iterations: 144
currently lose_sum: 91.65636068582535
time_elpased: 2.973
batch start
#iterations: 145
currently lose_sum: 91.82017928361893
time_elpased: 2.795
batch start
#iterations: 146
currently lose_sum: 90.99528563022614
time_elpased: 2.944
batch start
#iterations: 147
currently lose_sum: 91.48083674907684
time_elpased: 2.565
batch start
#iterations: 148
currently lose_sum: 91.63004100322723
time_elpased: 2.881
batch start
#iterations: 149
currently lose_sum: 91.68939572572708
time_elpased: 2.898
batch start
#iterations: 150
currently lose_sum: 91.25550979375839
time_elpased: 2.94
batch start
#iterations: 151
currently lose_sum: 91.56800043582916
time_elpased: 3.042
batch start
#iterations: 152
currently lose_sum: 91.39255821704865
time_elpased: 3.125
batch start
#iterations: 153
currently lose_sum: 91.41475975513458
time_elpased: 2.886
batch start
#iterations: 154
currently lose_sum: 91.24522572755814
time_elpased: 2.792
batch start
#iterations: 155
currently lose_sum: 91.03499048948288
time_elpased: 3.039
batch start
#iterations: 156
currently lose_sum: 91.00398254394531
time_elpased: 2.732
batch start
#iterations: 157
currently lose_sum: 90.96996647119522
time_elpased: 3.07
batch start
#iterations: 158
currently lose_sum: 91.53947305679321
time_elpased: 2.877
batch start
#iterations: 159
currently lose_sum: 91.051902115345
time_elpased: 3.05
start validation test
0.619072164948
0.628111918925
0.586867023466
0.606789400873
0.6191253746
61.822
batch start
#iterations: 160
currently lose_sum: 90.54696768522263
time_elpased: 3.064
batch start
#iterations: 161
currently lose_sum: 91.19286072254181
time_elpased: 2.964
batch start
#iterations: 162
currently lose_sum: 91.06261152029037
time_elpased: 2.897
batch start
#iterations: 163
currently lose_sum: 91.02348470687866
time_elpased: 3.029
batch start
#iterations: 164
currently lose_sum: 91.16689372062683
time_elpased: 2.832
batch start
#iterations: 165
currently lose_sum: 91.03176027536392
time_elpased: 2.564
batch start
#iterations: 166
currently lose_sum: 90.61913096904755
time_elpased: 2.738
batch start
#iterations: 167
currently lose_sum: 90.6862633228302
time_elpased: 2.677
batch start
#iterations: 168
currently lose_sum: 90.8811342716217
time_elpased: 3.164
batch start
#iterations: 169
currently lose_sum: 90.49618554115295
time_elpased: 2.795
batch start
#iterations: 170
currently lose_sum: 90.43810939788818
time_elpased: 2.713
batch start
#iterations: 171
currently lose_sum: 90.24536776542664
time_elpased: 2.877
batch start
#iterations: 172
currently lose_sum: 90.28429341316223
time_elpased: 3.087
batch start
#iterations: 173
currently lose_sum: 90.64410209655762
time_elpased: 3.03
batch start
#iterations: 174
currently lose_sum: 90.99948120117188
time_elpased: 2.875
batch start
#iterations: 175
currently lose_sum: 90.56465244293213
time_elpased: 2.813
batch start
#iterations: 176
currently lose_sum: 90.19325983524323
time_elpased: 3.272
batch start
#iterations: 177
currently lose_sum: 90.04822731018066
time_elpased: 3.04
batch start
#iterations: 178
currently lose_sum: 90.58557295799255
time_elpased: 2.599
batch start
#iterations: 179
currently lose_sum: 90.2654755115509
time_elpased: 2.551
start validation test
0.616804123711
0.637436762226
0.544668587896
0.587412587413
0.616923306753
62.113
batch start
#iterations: 180
currently lose_sum: 90.02975821495056
time_elpased: 2.882
batch start
#iterations: 181
currently lose_sum: 90.48021548986435
time_elpased: 2.934
batch start
#iterations: 182
currently lose_sum: 90.59499508142471
time_elpased: 2.917
batch start
#iterations: 183
currently lose_sum: 89.97077351808548
time_elpased: 2.904
batch start
#iterations: 184
currently lose_sum: 90.59160494804382
time_elpased: 2.995
batch start
#iterations: 185
currently lose_sum: 89.97080439329147
time_elpased: 3.191
batch start
#iterations: 186
currently lose_sum: 90.10149669647217
time_elpased: 3.033
batch start
#iterations: 187
currently lose_sum: 89.88781648874283
time_elpased: 2.886
batch start
#iterations: 188
currently lose_sum: 90.49008584022522
time_elpased: 2.968
batch start
#iterations: 189
currently lose_sum: 90.12588113546371
time_elpased: 3.008
batch start
#iterations: 190
currently lose_sum: 90.11781477928162
time_elpased: 2.914
batch start
#iterations: 191
currently lose_sum: 90.14444732666016
time_elpased: 2.913
batch start
#iterations: 192
currently lose_sum: 89.8861956000328
time_elpased: 2.785
batch start
#iterations: 193
currently lose_sum: 89.58552372455597
time_elpased: 2.777
batch start
#iterations: 194
currently lose_sum: 89.98128092288971
time_elpased: 3.253
batch start
#iterations: 195
currently lose_sum: 89.47874361276627
time_elpased: 2.968
batch start
#iterations: 196
currently lose_sum: 89.86780846118927
time_elpased: 3.019
batch start
#iterations: 197
currently lose_sum: 89.50352025032043
time_elpased: 2.717
batch start
#iterations: 198
currently lose_sum: 89.49053227901459
time_elpased: 2.953
batch start
#iterations: 199
currently lose_sum: 89.29552918672562
time_elpased: 3.041
start validation test
0.604175257732
0.61800486618
0.548991354467
0.581457458985
0.604266433119
62.595
batch start
#iterations: 200
currently lose_sum: 89.5369473695755
time_elpased: 2.947
batch start
#iterations: 201
currently lose_sum: 89.4205471277237
time_elpased: 3.121
batch start
#iterations: 202
currently lose_sum: 90.10995614528656
time_elpased: 2.768
batch start
#iterations: 203
currently lose_sum: 89.90966045856476
time_elpased: 2.844
batch start
#iterations: 204
currently lose_sum: 89.5107769370079
time_elpased: 3.36
batch start
#iterations: 205
currently lose_sum: 89.44717663526535
time_elpased: 3.241
batch start
#iterations: 206
currently lose_sum: 89.31771522760391
time_elpased: 2.845
batch start
#iterations: 207
currently lose_sum: 89.0693531036377
time_elpased: 3.009
batch start
#iterations: 208
currently lose_sum: 88.7255477309227
time_elpased: 2.575
batch start
#iterations: 209
currently lose_sum: 89.12516248226166
time_elpased: 2.952
batch start
#iterations: 210
currently lose_sum: 89.50379687547684
time_elpased: 2.655
batch start
#iterations: 211
currently lose_sum: 89.19323670864105
time_elpased: 2.586
batch start
#iterations: 212
currently lose_sum: 88.60599780082703
time_elpased: 3.151
batch start
#iterations: 213
currently lose_sum: 88.64321678876877
time_elpased: 2.99
batch start
#iterations: 214
currently lose_sum: 89.39092898368835
time_elpased: 2.962
batch start
#iterations: 215
currently lose_sum: 89.05647832155228
time_elpased: 2.825
batch start
#iterations: 216
currently lose_sum: 88.77376109361649
time_elpased: 2.962
batch start
#iterations: 217
currently lose_sum: 89.29754626750946
time_elpased: 2.896
batch start
#iterations: 218
currently lose_sum: 88.71641099452972
time_elpased: 3.16
batch start
#iterations: 219
currently lose_sum: 88.6878291964531
time_elpased: 3.046
start validation test
0.62118556701
0.625437201908
0.60734870317
0.616260247507
0.621208428413
62.090
batch start
#iterations: 220
currently lose_sum: 88.81909096240997
time_elpased: 2.887
batch start
#iterations: 221
currently lose_sum: 88.65379065275192
time_elpased: 2.823
batch start
#iterations: 222
currently lose_sum: 88.67804598808289
time_elpased: 2.859
batch start
#iterations: 223
currently lose_sum: 88.39695274829865
time_elpased: 3.114
batch start
#iterations: 224
currently lose_sum: 88.39840364456177
time_elpased: 2.918
batch start
#iterations: 225
currently lose_sum: 88.50822442770004
time_elpased: 2.948
batch start
#iterations: 226
currently lose_sum: 87.96289986371994
time_elpased: 2.898
batch start
#iterations: 227
currently lose_sum: 88.81066793203354
time_elpased: 2.873
batch start
#iterations: 228
currently lose_sum: 88.87474632263184
time_elpased: 2.839
batch start
#iterations: 229
currently lose_sum: 88.27255934476852
time_elpased: 3.061
batch start
#iterations: 230
currently lose_sum: 88.25743567943573
time_elpased: 3.04
batch start
#iterations: 231
currently lose_sum: 89.04586333036423
time_elpased: 3.04
batch start
#iterations: 232
currently lose_sum: 88.62125164270401
time_elpased: 3.002
batch start
#iterations: 233
currently lose_sum: 88.79522567987442
time_elpased: 3.118
batch start
#iterations: 234
currently lose_sum: 88.18341475725174
time_elpased: 3.186
batch start
#iterations: 235
currently lose_sum: 88.8084887266159
time_elpased: 2.656
batch start
#iterations: 236
currently lose_sum: 88.63831943273544
time_elpased: 2.651
batch start
#iterations: 237
currently lose_sum: 88.25314688682556
time_elpased: 2.985
batch start
#iterations: 238
currently lose_sum: 87.71653920412064
time_elpased: 3.042
batch start
#iterations: 239
currently lose_sum: 88.23224502801895
time_elpased: 2.948
start validation test
0.614896907216
0.630084598447
0.559592424866
0.592750068138
0.614988281826
62.788
batch start
#iterations: 240
currently lose_sum: 88.52428042888641
time_elpased: 2.945
batch start
#iterations: 241
currently lose_sum: 87.75589555501938
time_elpased: 2.895
batch start
#iterations: 242
currently lose_sum: 88.03822964429855
time_elpased: 3.064
batch start
#iterations: 243
currently lose_sum: 87.98758870363235
time_elpased: 2.978
batch start
#iterations: 244
currently lose_sum: 87.7042276263237
time_elpased: 2.697
batch start
#iterations: 245
currently lose_sum: 88.00423842668533
time_elpased: 2.817
batch start
#iterations: 246
currently lose_sum: 87.86167055368423
time_elpased: 2.831
batch start
#iterations: 247
currently lose_sum: 87.86351591348648
time_elpased: 3.172
batch start
#iterations: 248
currently lose_sum: 87.58521330356598
time_elpased: 2.99
batch start
#iterations: 249
currently lose_sum: 88.38597029447556
time_elpased: 2.889
batch start
#iterations: 250
currently lose_sum: 87.58028274774551
time_elpased: 2.994
batch start
#iterations: 251
currently lose_sum: 87.85389786958694
time_elpased: 3.211
batch start
#iterations: 252
currently lose_sum: 87.15598773956299
time_elpased: 2.969
batch start
#iterations: 253
currently lose_sum: 87.73458582162857
time_elpased: 3.053
batch start
#iterations: 254
currently lose_sum: 87.84894090890884
time_elpased: 2.915
batch start
#iterations: 255
currently lose_sum: 87.27417623996735
time_elpased: 3.145
batch start
#iterations: 256
currently lose_sum: 87.29843252897263
time_elpased: 3.413
batch start
#iterations: 257
currently lose_sum: 87.65731751918793
time_elpased: 2.964
batch start
#iterations: 258
currently lose_sum: 87.63112115859985
time_elpased: 3.145
batch start
#iterations: 259
currently lose_sum: 87.74382293224335
time_elpased: 3.137
start validation test
0.609381443299
0.62857830166
0.537875669
0.579700499168
0.609499585842
63.559
batch start
#iterations: 260
currently lose_sum: 87.49433070421219
time_elpased: 2.986
batch start
#iterations: 261
currently lose_sum: 87.28837847709656
time_elpased: 3.025
batch start
#iterations: 262
currently lose_sum: 87.66238117218018
time_elpased: 3.088
batch start
#iterations: 263
currently lose_sum: 86.81960320472717
time_elpased: 3.061
batch start
#iterations: 264
currently lose_sum: 87.50497496128082
time_elpased: 2.902
batch start
#iterations: 265
currently lose_sum: 87.31576937437057
time_elpased: 2.851
batch start
#iterations: 266
currently lose_sum: 86.89119106531143
time_elpased: 3.035
batch start
#iterations: 267
currently lose_sum: 87.13187843561172
time_elpased: 2.955
batch start
#iterations: 268
currently lose_sum: 86.77220493555069
time_elpased: 2.901
batch start
#iterations: 269
currently lose_sum: 87.47742295265198
time_elpased: 2.883
batch start
#iterations: 270
currently lose_sum: 86.65879315137863
time_elpased: 2.94
batch start
#iterations: 271
currently lose_sum: 87.0254682302475
time_elpased: 2.951
batch start
#iterations: 272
currently lose_sum: 87.26878601312637
time_elpased: 2.762
batch start
#iterations: 273
currently lose_sum: 86.9044536948204
time_elpased: 2.603
batch start
#iterations: 274
currently lose_sum: 87.04389190673828
time_elpased: 2.87
batch start
#iterations: 275
currently lose_sum: 86.91119706630707
time_elpased: 2.87
batch start
#iterations: 276
currently lose_sum: 86.1870664358139
time_elpased: 3.008
batch start
#iterations: 277
currently lose_sum: 86.21515625715256
time_elpased: 2.755
batch start
#iterations: 278
currently lose_sum: 86.65492337942123
time_elpased: 2.953
batch start
#iterations: 279
currently lose_sum: 87.1776305437088
time_elpased: 2.982
start validation test
0.609845360825
0.626457768877
0.547344586249
0.584235100247
0.609948625219
63.789
batch start
#iterations: 280
currently lose_sum: 86.32752996683121
time_elpased: 3.08
batch start
#iterations: 281
currently lose_sum: 86.72932559251785
time_elpased: 3.081
batch start
#iterations: 282
currently lose_sum: 86.78611052036285
time_elpased: 3.162
batch start
#iterations: 283
currently lose_sum: 86.69974285364151
time_elpased: 2.912
batch start
#iterations: 284
currently lose_sum: 86.96695584058762
time_elpased: 2.883
batch start
#iterations: 285
currently lose_sum: 86.42215639352798
time_elpased: 2.855
batch start
#iterations: 286
currently lose_sum: 85.73425167798996
time_elpased: 3.143
batch start
#iterations: 287
currently lose_sum: 86.88398790359497
time_elpased: 3.027
batch start
#iterations: 288
currently lose_sum: 86.84463429450989
time_elpased: 3.12
batch start
#iterations: 289
currently lose_sum: 86.79428195953369
time_elpased: 2.931
batch start
#iterations: 290
currently lose_sum: 86.4709233045578
time_elpased: 3.118
batch start
#iterations: 291
currently lose_sum: 86.80374199151993
time_elpased: 3.019
batch start
#iterations: 292
currently lose_sum: 86.31252485513687
time_elpased: 3.308
batch start
#iterations: 293
currently lose_sum: 86.23326057195663
time_elpased: 3.116
batch start
#iterations: 294
currently lose_sum: 86.67738497257233
time_elpased: 3.114
batch start
#iterations: 295
currently lose_sum: 85.95453429222107
time_elpased: 3.035
batch start
#iterations: 296
currently lose_sum: 86.53402310609818
time_elpased: 3.134
batch start
#iterations: 297
currently lose_sum: 86.94493925571442
time_elpased: 3.028
batch start
#iterations: 298
currently lose_sum: 85.8348822593689
time_elpased: 3.2
batch start
#iterations: 299
currently lose_sum: 85.81960529088974
time_elpased: 2.546
start validation test
0.606804123711
0.619669876204
0.556401811445
0.586334056399
0.606887398907
63.861
batch start
#iterations: 300
currently lose_sum: 85.51663041114807
time_elpased: 3.092
batch start
#iterations: 301
currently lose_sum: 85.8770917057991
time_elpased: 3.044
batch start
#iterations: 302
currently lose_sum: 85.93896621465683
time_elpased: 2.958
batch start
#iterations: 303
currently lose_sum: 85.78461629152298
time_elpased: 2.924
batch start
#iterations: 304
currently lose_sum: 85.37790763378143
time_elpased: 3.078
batch start
#iterations: 305
currently lose_sum: 85.92781496047974
time_elpased: 3.146
batch start
#iterations: 306
currently lose_sum: 85.78847563266754
time_elpased: 2.805
batch start
#iterations: 307
currently lose_sum: 85.96023267507553
time_elpased: 2.88
batch start
#iterations: 308
currently lose_sum: 86.27982866764069
time_elpased: 3.02
batch start
#iterations: 309
currently lose_sum: 85.3868772983551
time_elpased: 3.064
batch start
#iterations: 310
currently lose_sum: 85.34658563137054
time_elpased: 3.012
batch start
#iterations: 311
currently lose_sum: 85.54659032821655
time_elpased: 2.868
batch start
#iterations: 312
currently lose_sum: 85.39116537570953
time_elpased: 2.894
batch start
#iterations: 313
currently lose_sum: 85.18108373880386
time_elpased: 3.089
batch start
#iterations: 314
currently lose_sum: 85.55972808599472
time_elpased: 2.974
batch start
#iterations: 315
currently lose_sum: 84.99497240781784
time_elpased: 2.477
batch start
#iterations: 316
currently lose_sum: 85.467050075531
time_elpased: 2.871
batch start
#iterations: 317
currently lose_sum: 85.83221626281738
time_elpased: 2.819
batch start
#iterations: 318
currently lose_sum: 85.49496620893478
time_elpased: 2.927
batch start
#iterations: 319
currently lose_sum: 85.66500341892242
time_elpased: 2.971
start validation test
0.614226804124
0.630955174842
0.553417044051
0.589647987718
0.614327274607
64.194
batch start
#iterations: 320
currently lose_sum: 85.38217830657959
time_elpased: 2.739
batch start
#iterations: 321
currently lose_sum: 85.15265387296677
time_elpased: 3.226
batch start
#iterations: 322
currently lose_sum: 85.20418530702591
time_elpased: 2.994
batch start
#iterations: 323
currently lose_sum: 84.9139152765274
time_elpased: 2.857
batch start
#iterations: 324
currently lose_sum: 85.27928358316422
time_elpased: 2.913
batch start
#iterations: 325
currently lose_sum: 84.91735500097275
time_elpased: 2.926
batch start
#iterations: 326
currently lose_sum: 84.73969227075577
time_elpased: 3.132
batch start
#iterations: 327
currently lose_sum: 85.34421563148499
time_elpased: 2.977
batch start
#iterations: 328
currently lose_sum: 84.88164496421814
time_elpased: 3.02
batch start
#iterations: 329
currently lose_sum: 84.1509981751442
time_elpased: 3.088
batch start
#iterations: 330
currently lose_sum: 84.97039058804512
time_elpased: 2.989
batch start
#iterations: 331
currently lose_sum: 85.08041995763779
time_elpased: 2.891
batch start
#iterations: 332
currently lose_sum: 84.77527844905853
time_elpased: 2.851
batch start
#iterations: 333
currently lose_sum: 84.12264281511307
time_elpased: 2.824
batch start
#iterations: 334
currently lose_sum: 84.3765167593956
time_elpased: 3.086
batch start
#iterations: 335
currently lose_sum: 84.76302182674408
time_elpased: 3.046
batch start
#iterations: 336
currently lose_sum: 84.95304107666016
time_elpased: 2.692
batch start
#iterations: 337
currently lose_sum: 84.61893194913864
time_elpased: 2.883
batch start
#iterations: 338
currently lose_sum: 84.32152652740479
time_elpased: 3.06
batch start
#iterations: 339
currently lose_sum: 84.30049240589142
time_elpased: 3.068
start validation test
0.604381443299
0.626975239517
0.518629065459
0.567678702191
0.604523124221
64.957
batch start
#iterations: 340
currently lose_sum: 84.87368822097778
time_elpased: 3.176
batch start
#iterations: 341
currently lose_sum: 84.99137979745865
time_elpased: 3.071
batch start
#iterations: 342
currently lose_sum: 84.70736277103424
time_elpased: 3.307
batch start
#iterations: 343
currently lose_sum: 84.37520241737366
time_elpased: 2.936
batch start
#iterations: 344
currently lose_sum: 84.11236387491226
time_elpased: 2.958
batch start
#iterations: 345
currently lose_sum: 84.4195853471756
time_elpased: 3.155
batch start
#iterations: 346
currently lose_sum: 84.64787298440933
time_elpased: 3.063
batch start
#iterations: 347
currently lose_sum: 83.61040931940079
time_elpased: 3.191
batch start
#iterations: 348
currently lose_sum: 84.45108991861343
time_elpased: 2.643
batch start
#iterations: 349
currently lose_sum: 84.69299674034119
time_elpased: 2.79
batch start
#iterations: 350
currently lose_sum: 84.32090854644775
time_elpased: 2.548
batch start
#iterations: 351
currently lose_sum: 84.58460235595703
time_elpased: 2.928
batch start
#iterations: 352
currently lose_sum: 83.5358245074749
time_elpased: 2.927
batch start
#iterations: 353
currently lose_sum: 84.33286574482918
time_elpased: 2.933
batch start
#iterations: 354
currently lose_sum: 84.16285133361816
time_elpased: 3.055
batch start
#iterations: 355
currently lose_sum: 83.75414860248566
time_elpased: 2.959
batch start
#iterations: 356
currently lose_sum: 83.74227094650269
time_elpased: 2.961
batch start
#iterations: 357
currently lose_sum: 84.15634399652481
time_elpased: 3.071
batch start
#iterations: 358
currently lose_sum: 83.84124451875687
time_elpased: 2.909
batch start
#iterations: 359
currently lose_sum: 83.35511541366577
time_elpased: 3.055
start validation test
0.612989690722
0.625740318907
0.565459036641
0.594074394464
0.613068221336
64.870
batch start
#iterations: 360
currently lose_sum: 84.30915260314941
time_elpased: 3.177
batch start
#iterations: 361
currently lose_sum: 83.70718508958817
time_elpased: 2.99
batch start
#iterations: 362
currently lose_sum: 83.86069619655609
time_elpased: 3.12
batch start
#iterations: 363
currently lose_sum: 83.94827336072922
time_elpased: 2.951
batch start
#iterations: 364
currently lose_sum: 83.73472425341606
time_elpased: 3.196
batch start
#iterations: 365
currently lose_sum: 83.60535848140717
time_elpased: 2.963
batch start
#iterations: 366
currently lose_sum: 83.56138768792152
time_elpased: 3.062
batch start
#iterations: 367
currently lose_sum: 83.52449178695679
time_elpased: 3.117
batch start
#iterations: 368
currently lose_sum: 83.44511806964874
time_elpased: 2.803
batch start
#iterations: 369
currently lose_sum: 83.74359065294266
time_elpased: 2.735
batch start
#iterations: 370
currently lose_sum: 83.70306867361069
time_elpased: 2.971
batch start
#iterations: 371
currently lose_sum: 83.46360474824905
time_elpased: 2.885
batch start
#iterations: 372
currently lose_sum: 84.09975951910019
time_elpased: 2.543
batch start
#iterations: 373
currently lose_sum: 83.39317041635513
time_elpased: 2.678
batch start
#iterations: 374
currently lose_sum: 83.23981860280037
time_elpased: 3.093
batch start
#iterations: 375
currently lose_sum: 82.93710997700691
time_elpased: 3.196
batch start
#iterations: 376
currently lose_sum: 83.04454469680786
time_elpased: 3.141
batch start
#iterations: 377
currently lose_sum: 83.56543666124344
time_elpased: 3.119
batch start
#iterations: 378
currently lose_sum: 83.62761914730072
time_elpased: 3.085
batch start
#iterations: 379
currently lose_sum: 83.18448233604431
time_elpased: 3.118
start validation test
0.596288659794
0.621799844841
0.494956772334
0.5511747851
0.596456081334
66.368
batch start
#iterations: 380
currently lose_sum: 83.00312182307243
time_elpased: 2.989
batch start
#iterations: 381
currently lose_sum: 82.83498233556747
time_elpased: 2.793
batch start
#iterations: 382
currently lose_sum: 83.26534777879715
time_elpased: 3.092
batch start
#iterations: 383
currently lose_sum: 83.24497452378273
time_elpased: 3.069
batch start
#iterations: 384
currently lose_sum: 82.24241030216217
time_elpased: 3.129
batch start
#iterations: 385
currently lose_sum: 82.35882472991943
time_elpased: 3.021
batch start
#iterations: 386
currently lose_sum: 83.0913619697094
time_elpased: 3.163
batch start
#iterations: 387
currently lose_sum: 83.00812321901321
time_elpased: 2.863
batch start
#iterations: 388
currently lose_sum: 82.74866622686386
time_elpased: 2.448
batch start
#iterations: 389
currently lose_sum: 82.9777734875679
time_elpased: 2.993
batch start
#iterations: 390
currently lose_sum: 82.89217293262482
time_elpased: 2.94
batch start
#iterations: 391
currently lose_sum: 82.15479648113251
time_elpased: 3.073
batch start
#iterations: 392
currently lose_sum: 82.92174851894379
time_elpased: 3.01
batch start
#iterations: 393
currently lose_sum: 82.88008779287338
time_elpased: 3.12
batch start
#iterations: 394
currently lose_sum: 82.23917835950851
time_elpased: 3.154
batch start
#iterations: 395
currently lose_sum: 82.51253390312195
time_elpased: 2.877
batch start
#iterations: 396
currently lose_sum: 82.83721083402634
time_elpased: 2.937
batch start
#iterations: 397
currently lose_sum: 82.74410080909729
time_elpased: 2.954
batch start
#iterations: 398
currently lose_sum: 82.39602461457253
time_elpased: 2.91
batch start
#iterations: 399
currently lose_sum: 82.10828721523285
time_elpased: 3.01
start validation test
0.593298969072
0.62153887114
0.480547550432
0.542024611098
0.593485258074
66.704
acc: 0.656
pre: 0.648
rec: 0.686
F1: 0.667
auc: 0.708
