start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.51504236459732
time_elpased: 3.592
batch start
#iterations: 1
currently lose_sum: 100.16984117031097
time_elpased: 3.718
batch start
#iterations: 2
currently lose_sum: 100.22732245922089
time_elpased: 3.502
batch start
#iterations: 3
currently lose_sum: 100.0208705663681
time_elpased: 3.536
batch start
#iterations: 4
currently lose_sum: 99.97084575891495
time_elpased: 3.79
batch start
#iterations: 5
currently lose_sum: 99.69727885723114
time_elpased: 3.503
batch start
#iterations: 6
currently lose_sum: 99.63744240999222
time_elpased: 3.748
batch start
#iterations: 7
currently lose_sum: 99.03530776500702
time_elpased: 3.152
batch start
#iterations: 8
currently lose_sum: 98.91722792387009
time_elpased: 3.494
batch start
#iterations: 9
currently lose_sum: 98.22153061628342
time_elpased: 3.548
batch start
#iterations: 10
currently lose_sum: 98.1001872420311
time_elpased: 3.709
batch start
#iterations: 11
currently lose_sum: 97.98351585865021
time_elpased: 3.818
batch start
#iterations: 12
currently lose_sum: 97.64531648159027
time_elpased: 3.895
batch start
#iterations: 13
currently lose_sum: 97.7565489411354
time_elpased: 3.323
batch start
#iterations: 14
currently lose_sum: 97.30192589759827
time_elpased: 3.068
batch start
#iterations: 15
currently lose_sum: 97.25108754634857
time_elpased: 3.651
batch start
#iterations: 16
currently lose_sum: 97.63203555345535
time_elpased: 3.353
batch start
#iterations: 17
currently lose_sum: 96.95787405967712
time_elpased: 3.4
batch start
#iterations: 18
currently lose_sum: 96.73807138204575
time_elpased: 3.453
batch start
#iterations: 19
currently lose_sum: 97.15598011016846
time_elpased: 3.494
start validation test
0.617113402062
0.596655687864
0.727076258104
0.655441135541
0.616920345307
63.371
batch start
#iterations: 20
currently lose_sum: 97.21661406755447
time_elpased: 3.665
batch start
#iterations: 21
currently lose_sum: 96.47412818670273
time_elpased: 3.481
batch start
#iterations: 22
currently lose_sum: 96.8185173869133
time_elpased: 3.83
batch start
#iterations: 23
currently lose_sum: 96.83561360836029
time_elpased: 3.546
batch start
#iterations: 24
currently lose_sum: 96.59728610515594
time_elpased: 3.394
batch start
#iterations: 25
currently lose_sum: 96.63280934095383
time_elpased: 3.5
batch start
#iterations: 26
currently lose_sum: 96.5689806342125
time_elpased: 2.945
batch start
#iterations: 27
currently lose_sum: 96.33113425970078
time_elpased: 3.307
batch start
#iterations: 28
currently lose_sum: 96.34874850511551
time_elpased: 3.399
batch start
#iterations: 29
currently lose_sum: 95.90160447359085
time_elpased: 3.326
batch start
#iterations: 30
currently lose_sum: 96.07337844371796
time_elpased: 3.464
batch start
#iterations: 31
currently lose_sum: 96.12119418382645
time_elpased: 3.678
batch start
#iterations: 32
currently lose_sum: 96.33785659074783
time_elpased: 4.066
batch start
#iterations: 33
currently lose_sum: 95.86448776721954
time_elpased: 3.475
batch start
#iterations: 34
currently lose_sum: 95.82922267913818
time_elpased: 3.516
batch start
#iterations: 35
currently lose_sum: 95.63440436124802
time_elpased: 3.882
batch start
#iterations: 36
currently lose_sum: 95.92797887325287
time_elpased: 3.903
batch start
#iterations: 37
currently lose_sum: 95.77977687120438
time_elpased: 3.27
batch start
#iterations: 38
currently lose_sum: 95.79808443784714
time_elpased: 3.448
batch start
#iterations: 39
currently lose_sum: 95.48271769285202
time_elpased: 3.492
start validation test
0.639020618557
0.630706992872
0.673870536174
0.651574705209
0.638959434151
61.803
batch start
#iterations: 40
currently lose_sum: 95.74344539642334
time_elpased: 3.493
batch start
#iterations: 41
currently lose_sum: 94.97682148218155
time_elpased: 3.827
batch start
#iterations: 42
currently lose_sum: 95.30580997467041
time_elpased: 3.456
batch start
#iterations: 43
currently lose_sum: 95.20102459192276
time_elpased: 3.572
batch start
#iterations: 44
currently lose_sum: 95.14178222417831
time_elpased: 3.479
batch start
#iterations: 45
currently lose_sum: 95.01073116064072
time_elpased: 3.736
batch start
#iterations: 46
currently lose_sum: 95.02897936105728
time_elpased: 3.477
batch start
#iterations: 47
currently lose_sum: 95.1821950674057
time_elpased: 3.24
batch start
#iterations: 48
currently lose_sum: 95.13013392686844
time_elpased: 3.413
batch start
#iterations: 49
currently lose_sum: 94.80643999576569
time_elpased: 3.626
batch start
#iterations: 50
currently lose_sum: 94.37665164470673
time_elpased: 3.28
batch start
#iterations: 51
currently lose_sum: 94.6200138926506
time_elpased: 3.693
batch start
#iterations: 52
currently lose_sum: 94.92794078588486
time_elpased: 3.583
batch start
#iterations: 53
currently lose_sum: 94.36567080020905
time_elpased: 3.817
batch start
#iterations: 54
currently lose_sum: 94.29566740989685
time_elpased: 3.263
batch start
#iterations: 55
currently lose_sum: 94.4462097287178
time_elpased: 3.789
batch start
#iterations: 56
currently lose_sum: 94.10619968175888
time_elpased: 3.629
batch start
#iterations: 57
currently lose_sum: 94.2514134645462
time_elpased: 3.838
batch start
#iterations: 58
currently lose_sum: 94.38033175468445
time_elpased: 3.666
batch start
#iterations: 59
currently lose_sum: 94.58187371492386
time_elpased: 3.847
start validation test
0.633556701031
0.636773652192
0.624781311104
0.630720482053
0.633572107581
61.717
batch start
#iterations: 60
currently lose_sum: 94.0681865811348
time_elpased: 3.619
batch start
#iterations: 61
currently lose_sum: 94.05201387405396
time_elpased: 3.748
batch start
#iterations: 62
currently lose_sum: 94.02983385324478
time_elpased: 3.414
batch start
#iterations: 63
currently lose_sum: 93.8309473991394
time_elpased: 3.531
batch start
#iterations: 64
currently lose_sum: 93.74662041664124
time_elpased: 3.698
batch start
#iterations: 65
currently lose_sum: 93.64394360780716
time_elpased: 3.837
batch start
#iterations: 66
currently lose_sum: 93.6731019616127
time_elpased: 3.768
batch start
#iterations: 67
currently lose_sum: 93.38304394483566
time_elpased: 3.626
batch start
#iterations: 68
currently lose_sum: 93.32154470682144
time_elpased: 3.295
batch start
#iterations: 69
currently lose_sum: 93.54093110561371
time_elpased: 3.608
batch start
#iterations: 70
currently lose_sum: 93.4527815580368
time_elpased: 3.806
batch start
#iterations: 71
currently lose_sum: 93.3478193283081
time_elpased: 3.272
batch start
#iterations: 72
currently lose_sum: 92.84529262781143
time_elpased: 3.552
batch start
#iterations: 73
currently lose_sum: 93.5720357298851
time_elpased: 3.656
batch start
#iterations: 74
currently lose_sum: 92.70039463043213
time_elpased: 3.546
batch start
#iterations: 75
currently lose_sum: 93.44001466035843
time_elpased: 3.57
batch start
#iterations: 76
currently lose_sum: 93.07201033830643
time_elpased: 3.588
batch start
#iterations: 77
currently lose_sum: 92.79453027248383
time_elpased: 3.614
batch start
#iterations: 78
currently lose_sum: 93.0852472782135
time_elpased: 3.175
batch start
#iterations: 79
currently lose_sum: 92.74273937940598
time_elpased: 3.14
start validation test
0.637989690722
0.64933481153
0.602758052897
0.625180124887
0.638051545296
61.162
batch start
#iterations: 80
currently lose_sum: 92.69930446147919
time_elpased: 3.5
batch start
#iterations: 81
currently lose_sum: 92.70294868946075
time_elpased: 3.691
batch start
#iterations: 82
currently lose_sum: 93.0426225066185
time_elpased: 3.527
batch start
#iterations: 83
currently lose_sum: 92.65234899520874
time_elpased: 3.525
batch start
#iterations: 84
currently lose_sum: 92.86573112010956
time_elpased: 3.51
batch start
#iterations: 85
currently lose_sum: 92.7660459280014
time_elpased: 3.698
batch start
#iterations: 86
currently lose_sum: 92.78961199522018
time_elpased: 3.758
batch start
#iterations: 87
currently lose_sum: 92.24103426933289
time_elpased: 3.744
batch start
#iterations: 88
currently lose_sum: 92.37099534273148
time_elpased: 3.755
batch start
#iterations: 89
currently lose_sum: 92.64467495679855
time_elpased: 3.443
batch start
#iterations: 90
currently lose_sum: 91.95134884119034
time_elpased: 3.99
batch start
#iterations: 91
currently lose_sum: 92.22373867034912
time_elpased: 3.854
batch start
#iterations: 92
currently lose_sum: 92.27399677038193
time_elpased: 3.772
batch start
#iterations: 93
currently lose_sum: 92.15787971019745
time_elpased: 4.023
batch start
#iterations: 94
currently lose_sum: 91.95480698347092
time_elpased: 3.557
batch start
#iterations: 95
currently lose_sum: 91.96405225992203
time_elpased: 3.764
batch start
#iterations: 96
currently lose_sum: 92.02470284700394
time_elpased: 3.771
batch start
#iterations: 97
currently lose_sum: 92.16406750679016
time_elpased: 3.762
batch start
#iterations: 98
currently lose_sum: 91.81142228841782
time_elpased: 3.778
batch start
#iterations: 99
currently lose_sum: 92.10709702968597
time_elpased: 3.85
start validation test
0.629793814433
0.62732295329
0.64268807245
0.634912566084
0.629771176574
61.184
batch start
#iterations: 100
currently lose_sum: 91.96141147613525
time_elpased: 3.519
batch start
#iterations: 101
currently lose_sum: 91.76332360506058
time_elpased: 3.487
batch start
#iterations: 102
currently lose_sum: 92.26207631826401
time_elpased: 3.399
batch start
#iterations: 103
currently lose_sum: 91.38269817829132
time_elpased: 3.298
batch start
#iterations: 104
currently lose_sum: 91.43746763467789
time_elpased: 3.574
batch start
#iterations: 105
currently lose_sum: 91.514388859272
time_elpased: 3.67
batch start
#iterations: 106
currently lose_sum: 91.43328893184662
time_elpased: 3.891
batch start
#iterations: 107
currently lose_sum: 91.42800343036652
time_elpased: 3.492
batch start
#iterations: 108
currently lose_sum: 91.46120494604111
time_elpased: 3.527
batch start
#iterations: 109
currently lose_sum: 91.71194761991501
time_elpased: 3.716
batch start
#iterations: 110
currently lose_sum: 90.77490121126175
time_elpased: 3.67
batch start
#iterations: 111
currently lose_sum: 91.02969890832901
time_elpased: 3.934
batch start
#iterations: 112
currently lose_sum: 90.98300153017044
time_elpased: 3.92
batch start
#iterations: 113
currently lose_sum: 90.90653485059738
time_elpased: 3.552
batch start
#iterations: 114
currently lose_sum: 90.58328473567963
time_elpased: 3.497
batch start
#iterations: 115
currently lose_sum: 90.9221875667572
time_elpased: 3.418
batch start
#iterations: 116
currently lose_sum: 91.04989820718765
time_elpased: 3.712
batch start
#iterations: 117
currently lose_sum: 90.81252789497375
time_elpased: 3.882
batch start
#iterations: 118
currently lose_sum: 91.17516106367111
time_elpased: 3.624
batch start
#iterations: 119
currently lose_sum: 90.60127478837967
time_elpased: 2.986
start validation test
0.631340206186
0.643633105611
0.591437686529
0.616432478816
0.631410261213
61.207
batch start
#iterations: 120
currently lose_sum: 89.99076801538467
time_elpased: 3.652
batch start
#iterations: 121
currently lose_sum: 90.60630172491074
time_elpased: 3.684
batch start
#iterations: 122
currently lose_sum: 90.1837626695633
time_elpased: 3.902
batch start
#iterations: 123
currently lose_sum: 90.80418127775192
time_elpased: 3.75
batch start
#iterations: 124
currently lose_sum: 90.86899393796921
time_elpased: 3.744
batch start
#iterations: 125
currently lose_sum: 90.06063348054886
time_elpased: 3.311
batch start
#iterations: 126
currently lose_sum: 89.9534991979599
time_elpased: 3.689
batch start
#iterations: 127
currently lose_sum: 90.16467767953873
time_elpased: 3.167
batch start
#iterations: 128
currently lose_sum: 90.46371626853943
time_elpased: 3.238
batch start
#iterations: 129
currently lose_sum: 90.00869971513748
time_elpased: 3.662
batch start
#iterations: 130
currently lose_sum: 89.72149127721786
time_elpased: 3.601
batch start
#iterations: 131
currently lose_sum: 89.87597811222076
time_elpased: 3.999
batch start
#iterations: 132
currently lose_sum: 89.96163469552994
time_elpased: 3.344
batch start
#iterations: 133
currently lose_sum: 90.23742920160294
time_elpased: 3.744
batch start
#iterations: 134
currently lose_sum: 89.90224552154541
time_elpased: 3.438
batch start
#iterations: 135
currently lose_sum: 89.93134486675262
time_elpased: 3.435
batch start
#iterations: 136
currently lose_sum: 89.60943406820297
time_elpased: 3.557
batch start
#iterations: 137
currently lose_sum: 89.70251077413559
time_elpased: 3.699
batch start
#iterations: 138
currently lose_sum: 89.18266665935516
time_elpased: 3.724
batch start
#iterations: 139
currently lose_sum: 89.46396315097809
time_elpased: 3.589
start validation test
0.639587628866
0.650402914229
0.606359987651
0.627609714529
0.639645965115
61.224
batch start
#iterations: 140
currently lose_sum: 89.47568422555923
time_elpased: 3.388
batch start
#iterations: 141
currently lose_sum: 89.00924599170685
time_elpased: 3.527
batch start
#iterations: 142
currently lose_sum: 89.08459675312042
time_elpased: 3.569
batch start
#iterations: 143
currently lose_sum: 89.25413262844086
time_elpased: 3.264
batch start
#iterations: 144
currently lose_sum: 89.61158007383347
time_elpased: 3.028
batch start
#iterations: 145
currently lose_sum: 89.01419472694397
time_elpased: 3.308
batch start
#iterations: 146
currently lose_sum: 89.22303974628448
time_elpased: 3.562
batch start
#iterations: 147
currently lose_sum: 89.02893102169037
time_elpased: 4.04
batch start
#iterations: 148
currently lose_sum: 89.54473972320557
time_elpased: 3.728
batch start
#iterations: 149
currently lose_sum: 89.05390238761902
time_elpased: 3.649
batch start
#iterations: 150
currently lose_sum: 88.41996222734451
time_elpased: 3.42
batch start
#iterations: 151
currently lose_sum: 88.97111594676971
time_elpased: 3.387
batch start
#iterations: 152
currently lose_sum: 88.440269947052
time_elpased: 3.471
batch start
#iterations: 153
currently lose_sum: 88.73447316884995
time_elpased: 3.812
batch start
#iterations: 154
currently lose_sum: 88.120296895504
time_elpased: 3.76
batch start
#iterations: 155
currently lose_sum: 88.57645624876022
time_elpased: 3.559
batch start
#iterations: 156
currently lose_sum: 88.29424452781677
time_elpased: 3.278
batch start
#iterations: 157
currently lose_sum: 88.77022469043732
time_elpased: 3.656
batch start
#iterations: 158
currently lose_sum: 88.4804915189743
time_elpased: 3.534
batch start
#iterations: 159
currently lose_sum: 88.44944250583649
time_elpased: 3.901
start validation test
0.636855670103
0.657510021221
0.573942574869
0.612890818177
0.636966123745
61.275
batch start
#iterations: 160
currently lose_sum: 87.94677406549454
time_elpased: 3.766
batch start
#iterations: 161
currently lose_sum: 87.51049137115479
time_elpased: 3.415
batch start
#iterations: 162
currently lose_sum: 87.50771498680115
time_elpased: 3.601
batch start
#iterations: 163
currently lose_sum: 87.99780404567719
time_elpased: 3.672
batch start
#iterations: 164
currently lose_sum: 88.64506793022156
time_elpased: 3.388
batch start
#iterations: 165
currently lose_sum: 87.53862470388412
time_elpased: 3.544
batch start
#iterations: 166
currently lose_sum: 87.41049063205719
time_elpased: 3.907
batch start
#iterations: 167
currently lose_sum: 87.71618205308914
time_elpased: 3.834
batch start
#iterations: 168
currently lose_sum: 87.37466514110565
time_elpased: 3.736
batch start
#iterations: 169
currently lose_sum: 87.22464597225189
time_elpased: 3.535
batch start
#iterations: 170
currently lose_sum: 86.60976696014404
time_elpased: 3.535
batch start
#iterations: 171
currently lose_sum: 86.64293229579926
time_elpased: 3.618
batch start
#iterations: 172
currently lose_sum: 87.04053193330765
time_elpased: 3.631
batch start
#iterations: 173
currently lose_sum: 86.78603702783585
time_elpased: 3.302
batch start
#iterations: 174
currently lose_sum: 87.46900755167007
time_elpased: 3.815
batch start
#iterations: 175
currently lose_sum: 86.49155110120773
time_elpased: 3.896
batch start
#iterations: 176
currently lose_sum: 87.05397534370422
time_elpased: 3.506
batch start
#iterations: 177
currently lose_sum: 86.34574967622757
time_elpased: 3.434
batch start
#iterations: 178
currently lose_sum: 86.70006370544434
time_elpased: 3.259
batch start
#iterations: 179
currently lose_sum: 86.57361441850662
time_elpased: 3.251
start validation test
0.625721649485
0.631844535108
0.6056396007
0.618464610373
0.625756906619
62.202
batch start
#iterations: 180
currently lose_sum: 86.10948187112808
time_elpased: 3.446
batch start
#iterations: 181
currently lose_sum: 86.68079364299774
time_elpased: 3.709
batch start
#iterations: 182
currently lose_sum: 86.05585187673569
time_elpased: 3.814
batch start
#iterations: 183
currently lose_sum: 86.06914895772934
time_elpased: 3.601
batch start
#iterations: 184
currently lose_sum: 86.05873131752014
time_elpased: 3.796
batch start
#iterations: 185
currently lose_sum: 85.93936264514923
time_elpased: 3.627
batch start
#iterations: 186
currently lose_sum: 86.07144665718079
time_elpased: 3.588
batch start
#iterations: 187
currently lose_sum: 85.94265699386597
time_elpased: 3.55
batch start
#iterations: 188
currently lose_sum: 85.7355101108551
time_elpased: 3.798
batch start
#iterations: 189
currently lose_sum: 85.52797144651413
time_elpased: 3.444
batch start
#iterations: 190
currently lose_sum: 85.3318213224411
time_elpased: 3.637
batch start
#iterations: 191
currently lose_sum: 85.94527888298035
time_elpased: 3.637
batch start
#iterations: 192
currently lose_sum: 85.45320123434067
time_elpased: 3.152
batch start
#iterations: 193
currently lose_sum: 85.27128773927689
time_elpased: 3.198
batch start
#iterations: 194
currently lose_sum: 84.54989498853683
time_elpased: 3.468
batch start
#iterations: 195
currently lose_sum: 84.44257742166519
time_elpased: 3.418
batch start
#iterations: 196
currently lose_sum: 84.45148813724518
time_elpased: 3.389
batch start
#iterations: 197
currently lose_sum: 85.0891427397728
time_elpased: 3.557
batch start
#iterations: 198
currently lose_sum: 84.89666843414307
time_elpased: 3.424
batch start
#iterations: 199
currently lose_sum: 84.92116671800613
time_elpased: 3.539
start validation test
0.607164948454
0.631724484666
0.517237830606
0.568777230804
0.607322829379
64.850
batch start
#iterations: 200
currently lose_sum: 84.80749130249023
time_elpased: 3.75
batch start
#iterations: 201
currently lose_sum: 84.41890799999237
time_elpased: 3.489
batch start
#iterations: 202
currently lose_sum: 84.11775934696198
time_elpased: 3.283
batch start
#iterations: 203
currently lose_sum: 84.63037365674973
time_elpased: 3.019
batch start
#iterations: 204
currently lose_sum: 84.16709560155869
time_elpased: 3.581
batch start
#iterations: 205
currently lose_sum: 83.81034088134766
time_elpased: 3.641
batch start
#iterations: 206
currently lose_sum: 83.37914735078812
time_elpased: 3.662
batch start
#iterations: 207
currently lose_sum: 83.6269069314003
time_elpased: 3.632
batch start
#iterations: 208
currently lose_sum: 83.127601146698
time_elpased: 3.514
batch start
#iterations: 209
currently lose_sum: 83.52482545375824
time_elpased: 3.285
batch start
#iterations: 210
currently lose_sum: 82.81119525432587
time_elpased: 3.657
batch start
#iterations: 211
currently lose_sum: 82.82288789749146
time_elpased: 3.616
batch start
#iterations: 212
currently lose_sum: 82.40855062007904
time_elpased: 3.574
batch start
#iterations: 213
currently lose_sum: 82.70673531293869
time_elpased: 3.792
batch start
#iterations: 214
currently lose_sum: 81.96420806646347
time_elpased: 3.65
batch start
#iterations: 215
currently lose_sum: 83.44956654310226
time_elpased: 3.275
batch start
#iterations: 216
currently lose_sum: 82.64464670419693
time_elpased: 3.494
batch start
#iterations: 217
currently lose_sum: 81.82729399204254
time_elpased: 3.861
batch start
#iterations: 218
currently lose_sum: 82.36988860368729
time_elpased: 3.369
batch start
#iterations: 219
currently lose_sum: 81.65293741226196
time_elpased: 3.843
start validation test
0.601855670103
0.636562971084
0.478028198003
0.546020923945
0.602073068329
66.745
batch start
#iterations: 220
currently lose_sum: 82.10182595252991
time_elpased: 3.785
batch start
#iterations: 221
currently lose_sum: 81.7544239461422
time_elpased: 3.614
batch start
#iterations: 222
currently lose_sum: 81.85942381620407
time_elpased: 3.63
batch start
#iterations: 223
currently lose_sum: 81.4214817583561
time_elpased: 3.475
batch start
#iterations: 224
currently lose_sum: 81.45163145661354
time_elpased: 3.793
batch start
#iterations: 225
currently lose_sum: 81.43135488033295
time_elpased: 3.399
batch start
#iterations: 226
currently lose_sum: 80.50863355398178
time_elpased: 3.476
batch start
#iterations: 227
currently lose_sum: 80.95258119702339
time_elpased: 3.276
batch start
#iterations: 228
currently lose_sum: 80.14086681604385
time_elpased: 3.466
batch start
#iterations: 229
currently lose_sum: 80.71300083398819
time_elpased: 3.614
batch start
#iterations: 230
currently lose_sum: 80.80828753113747
time_elpased: 3.762
batch start
#iterations: 231
currently lose_sum: 79.87568601965904
time_elpased: 3.782
batch start
#iterations: 232
currently lose_sum: 79.74799594283104
time_elpased: 3.477
batch start
#iterations: 233
currently lose_sum: 80.3866902589798
time_elpased: 3.368
batch start
#iterations: 234
currently lose_sum: 79.84998100996017
time_elpased: 3.216
batch start
#iterations: 235
currently lose_sum: 80.02367919683456
time_elpased: 3.263
batch start
#iterations: 236
currently lose_sum: 79.17413210868835
time_elpased: 3.635
batch start
#iterations: 237
currently lose_sum: 78.99812471866608
time_elpased: 3.697
batch start
#iterations: 238
currently lose_sum: 78.82000637054443
time_elpased: 3.312
batch start
#iterations: 239
currently lose_sum: 79.27691492438316
time_elpased: 3.176
start validation test
0.599226804124
0.645533573141
0.443243799527
0.525596436634
0.599500656347
68.531
batch start
#iterations: 240
currently lose_sum: 78.71666514873505
time_elpased: 3.728
batch start
#iterations: 241
currently lose_sum: 78.77389374375343
time_elpased: 3.509
batch start
#iterations: 242
currently lose_sum: 78.27168190479279
time_elpased: 3.518
batch start
#iterations: 243
currently lose_sum: 78.23985284566879
time_elpased: 3.738
batch start
#iterations: 244
currently lose_sum: 78.21321254968643
time_elpased: 3.309
batch start
#iterations: 245
currently lose_sum: 78.10762640833855
time_elpased: 3.559
batch start
#iterations: 246
currently lose_sum: 77.95779028534889
time_elpased: 3.772
batch start
#iterations: 247
currently lose_sum: 77.76672029495239
time_elpased: 3.209
batch start
#iterations: 248
currently lose_sum: 77.66287443041801
time_elpased: 3.301
batch start
#iterations: 249
currently lose_sum: 76.73458459973335
time_elpased: 3.262
batch start
#iterations: 250
currently lose_sum: 76.55703818798065
time_elpased: 3.358
batch start
#iterations: 251
currently lose_sum: 77.04905387759209
time_elpased: 3.831
batch start
#iterations: 252
currently lose_sum: 77.02275085449219
time_elpased: 3.329
batch start
#iterations: 253
currently lose_sum: 76.4659578204155
time_elpased: 3.476
batch start
#iterations: 254
currently lose_sum: 75.81391623616219
time_elpased: 3.501
batch start
#iterations: 255
currently lose_sum: 76.19654393196106
time_elpased: 3.64
batch start
#iterations: 256
currently lose_sum: 75.40016135573387
time_elpased: 3.158
batch start
#iterations: 257
currently lose_sum: 75.71979576349258
time_elpased: 3.498
batch start
#iterations: 258
currently lose_sum: 76.06498917937279
time_elpased: 3.682
batch start
#iterations: 259
currently lose_sum: 75.30748802423477
time_elpased: 3.61
start validation test
0.585824742268
0.633619319987
0.410414737059
0.498157516707
0.586132701587
72.618
batch start
#iterations: 260
currently lose_sum: 75.65135395526886
time_elpased: 3.385
batch start
#iterations: 261
currently lose_sum: 74.89618477225304
time_elpased: 3.763
batch start
#iterations: 262
currently lose_sum: 75.21109053492546
time_elpased: 3.775
batch start
#iterations: 263
currently lose_sum: 74.44026219844818
time_elpased: 3.734
batch start
#iterations: 264
currently lose_sum: 75.21593728661537
time_elpased: 3.539
batch start
#iterations: 265
currently lose_sum: 75.0706153512001
time_elpased: 3.511
batch start
#iterations: 266
currently lose_sum: 74.61759215593338
time_elpased: 3.389
batch start
#iterations: 267
currently lose_sum: 73.84642127156258
time_elpased: 3.827
batch start
#iterations: 268
currently lose_sum: 74.60629290342331
time_elpased: 3.752
batch start
#iterations: 269
currently lose_sum: 72.66455578804016
time_elpased: 3.807
batch start
#iterations: 270
currently lose_sum: 73.30993342399597
time_elpased: 3.61
batch start
#iterations: 271
currently lose_sum: 72.62721782922745
time_elpased: 3.536
batch start
#iterations: 272
currently lose_sum: 72.75310695171356
time_elpased: 3.49
batch start
#iterations: 273
currently lose_sum: 72.47763940691948
time_elpased: 3.338
batch start
#iterations: 274
currently lose_sum: 72.63365849852562
time_elpased: 3.53
batch start
#iterations: 275
currently lose_sum: 72.41928797960281
time_elpased: 3.59
batch start
#iterations: 276
currently lose_sum: 71.9654301404953
time_elpased: 3.678
batch start
#iterations: 277
currently lose_sum: 72.0143352150917
time_elpased: 3.508
batch start
#iterations: 278
currently lose_sum: 71.86680033802986
time_elpased: 3.432
batch start
#iterations: 279
currently lose_sum: 71.1732666194439
time_elpased: 3.911
start validation test
0.575154639175
0.624262847515
0.381290521766
0.47342192691
0.575494997535
77.209
batch start
#iterations: 280
currently lose_sum: 71.10945236682892
time_elpased: 3.844
batch start
#iterations: 281
currently lose_sum: 71.35120290517807
time_elpased: 3.477
batch start
#iterations: 282
currently lose_sum: 71.38508597016335
time_elpased: 3.571
batch start
#iterations: 283
currently lose_sum: 70.47625070810318
time_elpased: 3.12
batch start
#iterations: 284
currently lose_sum: 70.45255580544472
time_elpased: 3.635
batch start
#iterations: 285
currently lose_sum: 69.40344485640526
time_elpased: 3.459
batch start
#iterations: 286
currently lose_sum: 70.23325905203819
time_elpased: 3.527
batch start
#iterations: 287
currently lose_sum: 69.18493708968163
time_elpased: 3.095
batch start
#iterations: 288
currently lose_sum: 69.29003468155861
time_elpased: 3.44
batch start
#iterations: 289
currently lose_sum: 69.87516158819199
time_elpased: 3.27
batch start
#iterations: 290
currently lose_sum: 69.15177536010742
time_elpased: 3.684
batch start
#iterations: 291
currently lose_sum: 68.63614794611931
time_elpased: 3.343
batch start
#iterations: 292
currently lose_sum: 68.67110857367516
time_elpased: 3.745
batch start
#iterations: 293
currently lose_sum: 68.82717898488045
time_elpased: 3.976
batch start
#iterations: 294
currently lose_sum: 67.567451775074
time_elpased: 4.165
batch start
#iterations: 295
currently lose_sum: 67.79615733027458
time_elpased: 3.455
batch start
#iterations: 296
currently lose_sum: 67.2862206697464
time_elpased: 3.324
batch start
#iterations: 297
currently lose_sum: 68.60020139813423
time_elpased: 3.688
batch start
#iterations: 298
currently lose_sum: 67.00988346338272
time_elpased: 3.556
batch start
#iterations: 299
currently lose_sum: 66.86706352233887
time_elpased: 3.234
start validation test
0.567010309278
0.626610267256
0.335391581764
0.436921839389
0.567416951679
83.928
batch start
#iterations: 300
currently lose_sum: 66.9127439558506
time_elpased: 3.569
batch start
#iterations: 301
currently lose_sum: 66.19840449094772
time_elpased: 3.705
batch start
#iterations: 302
currently lose_sum: 66.43673527240753
time_elpased: 3.786
batch start
#iterations: 303
currently lose_sum: 66.37382301688194
time_elpased: 3.724
batch start
#iterations: 304
currently lose_sum: 66.75302308797836
time_elpased: 3.98
batch start
#iterations: 305
currently lose_sum: 66.56772881746292
time_elpased: 3.604
batch start
#iterations: 306
currently lose_sum: 65.64857393503189
time_elpased: 3.529
batch start
#iterations: 307
currently lose_sum: 64.9507851600647
time_elpased: 3.399
batch start
#iterations: 308
currently lose_sum: 65.32626953721046
time_elpased: 3.625
batch start
#iterations: 309
currently lose_sum: 64.87324526906013
time_elpased: 3.611
batch start
#iterations: 310
currently lose_sum: 65.30324789881706
time_elpased: 3.504
batch start
#iterations: 311
currently lose_sum: 64.00108987092972
time_elpased: 3.509
batch start
#iterations: 312
currently lose_sum: 64.3335999250412
time_elpased: 2.889
batch start
#iterations: 313
currently lose_sum: 64.09132415056229
time_elpased: 2.886
batch start
#iterations: 314
currently lose_sum: 63.848256915807724
time_elpased: 3.592
batch start
#iterations: 315
currently lose_sum: 62.974091827869415
time_elpased: 3.405
batch start
#iterations: 316
currently lose_sum: 63.75375524163246
time_elpased: 3.654
batch start
#iterations: 317
currently lose_sum: 63.10755568742752
time_elpased: 3.247
batch start
#iterations: 318
currently lose_sum: 62.90220007300377
time_elpased: 3.327
batch start
#iterations: 319
currently lose_sum: 63.20940247178078
time_elpased: 3.495
start validation test
0.552835051546
0.615829257448
0.285067407636
0.389729159339
0.553305158946
92.022
batch start
#iterations: 320
currently lose_sum: 62.47868573665619
time_elpased: 3.49
batch start
#iterations: 321
currently lose_sum: 62.213230550289154
time_elpased: 3.671
batch start
#iterations: 322
currently lose_sum: 62.16758927702904
time_elpased: 3.69
batch start
#iterations: 323
currently lose_sum: 62.109753459692
time_elpased: 3.833
batch start
#iterations: 324
currently lose_sum: 61.587945491075516
time_elpased: 3.373
batch start
#iterations: 325
currently lose_sum: 61.46066689491272
time_elpased: 3.645
batch start
#iterations: 326
currently lose_sum: 60.99559113383293
time_elpased: 3.882
batch start
#iterations: 327
currently lose_sum: 61.196037858724594
time_elpased: 3.7
batch start
#iterations: 328
currently lose_sum: 60.16583988070488
time_elpased: 3.656
batch start
#iterations: 329
currently lose_sum: 60.92780867218971
time_elpased: 3.9
batch start
#iterations: 330
currently lose_sum: 60.479470789432526
time_elpased: 3.452
batch start
#iterations: 331
currently lose_sum: 60.125573962926865
time_elpased: 3.461
batch start
#iterations: 332
currently lose_sum: 59.648037135601044
time_elpased: 3.561
batch start
#iterations: 333
currently lose_sum: 59.63642191886902
time_elpased: 3.564
batch start
#iterations: 334
currently lose_sum: 59.86454224586487
time_elpased: 3.595
batch start
#iterations: 335
currently lose_sum: 59.01428174972534
time_elpased: 3.596
batch start
#iterations: 336
currently lose_sum: 59.39867767691612
time_elpased: 3.807
batch start
#iterations: 337
currently lose_sum: 58.446149438619614
time_elpased: 3.634
batch start
#iterations: 338
currently lose_sum: 59.3353405892849
time_elpased: 3.26
batch start
#iterations: 339
currently lose_sum: 58.90343141555786
time_elpased: 3.668
start validation test
0.551597938144
0.609698275862
0.291139240506
0.394093473567
0.552055213561
97.846
batch start
#iterations: 340
currently lose_sum: 58.07586920261383
time_elpased: 3.772
batch start
#iterations: 341
currently lose_sum: 58.16087689995766
time_elpased: 3.725
batch start
#iterations: 342
currently lose_sum: 57.67650654911995
time_elpased: 3.454
batch start
#iterations: 343
currently lose_sum: 57.66750222444534
time_elpased: 3.866
batch start
#iterations: 344
currently lose_sum: 57.76332429051399
time_elpased: 3.685
batch start
#iterations: 345
currently lose_sum: 57.190626591444016
time_elpased: 3.953
batch start
#iterations: 346
currently lose_sum: 56.36500370502472
time_elpased: 3.757
batch start
#iterations: 347
currently lose_sum: 56.202457934617996
time_elpased: 3.405
batch start
#iterations: 348
currently lose_sum: 56.92115208506584
time_elpased: 3.616
batch start
#iterations: 349
currently lose_sum: 55.91113471984863
time_elpased: 3.703
batch start
#iterations: 350
currently lose_sum: 55.99807435274124
time_elpased: 3.614
batch start
#iterations: 351
currently lose_sum: 56.10722127556801
time_elpased: 3.841
batch start
#iterations: 352
currently lose_sum: 55.70492559671402
time_elpased: 3.377
batch start
#iterations: 353
currently lose_sum: 55.67250621318817
time_elpased: 3.843
batch start
#iterations: 354
currently lose_sum: 55.64177802205086
time_elpased: 3.394
batch start
#iterations: 355
currently lose_sum: 54.57827326655388
time_elpased: 3.188
batch start
#iterations: 356
currently lose_sum: 54.85478267073631
time_elpased: 3.758
batch start
#iterations: 357
currently lose_sum: 54.35657396912575
time_elpased: 3.541
batch start
#iterations: 358
currently lose_sum: 53.938090056180954
time_elpased: 3.727
batch start
#iterations: 359
currently lose_sum: 54.451315969228745
time_elpased: 3.26
start validation test
0.537628865979
0.591881918819
0.247607286199
0.349151066609
0.538138043595
107.736
batch start
#iterations: 360
currently lose_sum: 54.82978719472885
time_elpased: 3.36
batch start
#iterations: 361
currently lose_sum: 53.56404039263725
time_elpased: 3.739
batch start
#iterations: 362
currently lose_sum: 53.260764718055725
time_elpased: 3.761
batch start
#iterations: 363
currently lose_sum: 52.671342223882675
time_elpased: 3.467
batch start
#iterations: 364
currently lose_sum: 52.5964449942112
time_elpased: 3.242
batch start
#iterations: 365
currently lose_sum: 52.91826915740967
time_elpased: 3.475
batch start
#iterations: 366
currently lose_sum: 53.08553144335747
time_elpased: 3.375
batch start
#iterations: 367
currently lose_sum: 52.459772169589996
time_elpased: 3.324
batch start
#iterations: 368
currently lose_sum: 51.70780575275421
time_elpased: 3.638
batch start
#iterations: 369
currently lose_sum: 51.988550424575806
time_elpased: 3.624
batch start
#iterations: 370
currently lose_sum: 51.6590256690979
time_elpased: 3.762
batch start
#iterations: 371
currently lose_sum: 51.0400527715683
time_elpased: 3.385
batch start
#iterations: 372
currently lose_sum: 51.494363248348236
time_elpased: 3.364
batch start
#iterations: 373
currently lose_sum: 51.73203366994858
time_elpased: 3.532
batch start
#iterations: 374
currently lose_sum: 51.35337609052658
time_elpased: 3.336
batch start
#iterations: 375
currently lose_sum: 50.91021031141281
time_elpased: 3.224
batch start
#iterations: 376
currently lose_sum: 51.93739503622055
time_elpased: 3.665
batch start
#iterations: 377
currently lose_sum: 50.65714758634567
time_elpased: 3.529
batch start
#iterations: 378
currently lose_sum: 50.75023916363716
time_elpased: 3.572
batch start
#iterations: 379
currently lose_sum: 50.664177387952805
time_elpased: 3.16
start validation test
0.534072164948
0.588836477987
0.231244211176
0.332077144757
0.534603826129
114.965
batch start
#iterations: 380
currently lose_sum: 49.67898187041283
time_elpased: 3.623
batch start
#iterations: 381
currently lose_sum: 50.28368765115738
time_elpased: 3.632
batch start
#iterations: 382
currently lose_sum: 49.67630052566528
time_elpased: 3.584
batch start
#iterations: 383
currently lose_sum: 49.51380971074104
time_elpased: 3.586
batch start
#iterations: 384
currently lose_sum: 48.842083394527435
time_elpased: 3.238
batch start
#iterations: 385
currently lose_sum: 48.96264696121216
time_elpased: 3.718
batch start
#iterations: 386
currently lose_sum: 49.2130389213562
time_elpased: 3.874
batch start
#iterations: 387
currently lose_sum: 48.72471097111702
time_elpased: 3.595
batch start
#iterations: 388
currently lose_sum: 48.8643259704113
time_elpased: 3.728
batch start
#iterations: 389
currently lose_sum: 48.65691378712654
time_elpased: 3.373
batch start
#iterations: 390
currently lose_sum: 49.25198742747307
time_elpased: 3.059
batch start
#iterations: 391
currently lose_sum: 47.864660799503326
time_elpased: 3.086
batch start
#iterations: 392
currently lose_sum: 48.32840967178345
time_elpased: 3.385
batch start
#iterations: 393
currently lose_sum: 47.88529574871063
time_elpased: 3.426
batch start
#iterations: 394
currently lose_sum: 47.9586426615715
time_elpased: 3.391
batch start
#iterations: 395
currently lose_sum: 47.597436651587486
time_elpased: 2.924
batch start
#iterations: 396
currently lose_sum: 47.2759744822979
time_elpased: 3.791
batch start
#iterations: 397
currently lose_sum: 46.74730506539345
time_elpased: 3.473
batch start
#iterations: 398
currently lose_sum: 46.87680061161518
time_elpased: 3.678
batch start
#iterations: 399
currently lose_sum: 46.60413187742233
time_elpased: 3.488
start validation test
0.533762886598
0.585452695829
0.23690439436
0.337314088944
0.534284067468
122.354
acc: 0.631
pre: 0.642
rec: 0.592
F1: 0.616
auc: 0.694
