start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.44219636917114
time_elpased: 2.563
batch start
#iterations: 1
currently lose_sum: 100.20850974321365
time_elpased: 2.507
batch start
#iterations: 2
currently lose_sum: 100.07275688648224
time_elpased: 2.722
batch start
#iterations: 3
currently lose_sum: 99.87644082307816
time_elpased: 2.773
batch start
#iterations: 4
currently lose_sum: 99.79991412162781
time_elpased: 2.699
batch start
#iterations: 5
currently lose_sum: 99.54689705371857
time_elpased: 2.534
batch start
#iterations: 6
currently lose_sum: 99.0793309211731
time_elpased: 2.389
batch start
#iterations: 7
currently lose_sum: 98.78213155269623
time_elpased: 2.577
batch start
#iterations: 8
currently lose_sum: 98.64617681503296
time_elpased: 2.577
batch start
#iterations: 9
currently lose_sum: 98.51773083209991
time_elpased: 2.573
batch start
#iterations: 10
currently lose_sum: 98.34766608476639
time_elpased: 2.437
batch start
#iterations: 11
currently lose_sum: 98.08583498001099
time_elpased: 2.559
batch start
#iterations: 12
currently lose_sum: 98.07555997371674
time_elpased: 2.466
batch start
#iterations: 13
currently lose_sum: 97.80132400989532
time_elpased: 2.268
batch start
#iterations: 14
currently lose_sum: 97.5313109755516
time_elpased: 2.59
batch start
#iterations: 15
currently lose_sum: 96.93283897638321
time_elpased: 2.393
batch start
#iterations: 16
currently lose_sum: 97.32386296987534
time_elpased: 2.563
batch start
#iterations: 17
currently lose_sum: 97.01830583810806
time_elpased: 2.556
batch start
#iterations: 18
currently lose_sum: 96.86143851280212
time_elpased: 2.612
batch start
#iterations: 19
currently lose_sum: 96.59249639511108
time_elpased: 2.55
start validation test
0.635103092784
0.614516409099
0.728414119584
0.666635271957
0.634939270884
62.889
batch start
#iterations: 20
currently lose_sum: 96.50081598758698
time_elpased: 2.516
batch start
#iterations: 21
currently lose_sum: 96.17802852392197
time_elpased: 2.566
batch start
#iterations: 22
currently lose_sum: 96.52896654605865
time_elpased: 2.625
batch start
#iterations: 23
currently lose_sum: 96.28027576208115
time_elpased: 2.559
batch start
#iterations: 24
currently lose_sum: 96.15187275409698
time_elpased: 2.184
batch start
#iterations: 25
currently lose_sum: 95.53660720586777
time_elpased: 2.385
batch start
#iterations: 26
currently lose_sum: 95.90350538492203
time_elpased: 2.734
batch start
#iterations: 27
currently lose_sum: 95.76886457204819
time_elpased: 2.556
batch start
#iterations: 28
currently lose_sum: 95.748779296875
time_elpased: 2.674
batch start
#iterations: 29
currently lose_sum: 95.91254967451096
time_elpased: 2.48
batch start
#iterations: 30
currently lose_sum: 95.41528421640396
time_elpased: 2.594
batch start
#iterations: 31
currently lose_sum: 95.58811581134796
time_elpased: 2.661
batch start
#iterations: 32
currently lose_sum: 94.88850790262222
time_elpased: 2.469
batch start
#iterations: 33
currently lose_sum: 95.61581915616989
time_elpased: 2.459
batch start
#iterations: 34
currently lose_sum: 95.18616271018982
time_elpased: 2.77
batch start
#iterations: 35
currently lose_sum: 95.54009521007538
time_elpased: 2.6
batch start
#iterations: 36
currently lose_sum: 95.58685821294785
time_elpased: 2.63
batch start
#iterations: 37
currently lose_sum: 95.31480014324188
time_elpased: 2.619
batch start
#iterations: 38
currently lose_sum: 95.22418946027756
time_elpased: 2.529
batch start
#iterations: 39
currently lose_sum: 95.12655371427536
time_elpased: 2.607
start validation test
0.647371134021
0.637056805185
0.687866625502
0.661487456084
0.647300037939
61.211
batch start
#iterations: 40
currently lose_sum: 95.04787200689316
time_elpased: 2.525
batch start
#iterations: 41
currently lose_sum: 95.36457246541977
time_elpased: 2.598
batch start
#iterations: 42
currently lose_sum: 94.9809188246727
time_elpased: 2.623
batch start
#iterations: 43
currently lose_sum: 94.89087587594986
time_elpased: 2.569
batch start
#iterations: 44
currently lose_sum: 95.08812004327774
time_elpased: 2.572
batch start
#iterations: 45
currently lose_sum: 94.71525192260742
time_elpased: 2.553
batch start
#iterations: 46
currently lose_sum: 94.52278769016266
time_elpased: 2.574
batch start
#iterations: 47
currently lose_sum: 94.47318720817566
time_elpased: 2.538
batch start
#iterations: 48
currently lose_sum: 94.88911193609238
time_elpased: 2.605
batch start
#iterations: 49
currently lose_sum: 94.93739795684814
time_elpased: 2.465
batch start
#iterations: 50
currently lose_sum: 95.45672923326492
time_elpased: 2.589
batch start
#iterations: 51
currently lose_sum: 94.26026386022568
time_elpased: 2.659
batch start
#iterations: 52
currently lose_sum: 94.39664137363434
time_elpased: 2.639
batch start
#iterations: 53
currently lose_sum: 94.5264778137207
time_elpased: 2.473
batch start
#iterations: 54
currently lose_sum: 94.97250038385391
time_elpased: 2.562
batch start
#iterations: 55
currently lose_sum: 94.37212508916855
time_elpased: 2.565
batch start
#iterations: 56
currently lose_sum: 94.88983172178268
time_elpased: 2.798
batch start
#iterations: 57
currently lose_sum: 94.20768004655838
time_elpased: 2.112
batch start
#iterations: 58
currently lose_sum: 94.61694371700287
time_elpased: 2.359
batch start
#iterations: 59
currently lose_sum: 94.3064324259758
time_elpased: 2.716
start validation test
0.646958762887
0.62607701776
0.73283935371
0.675264330757
0.646807986263
61.149
batch start
#iterations: 60
currently lose_sum: 94.72444689273834
time_elpased: 2.428
batch start
#iterations: 61
currently lose_sum: 94.66074764728546
time_elpased: 2.669
batch start
#iterations: 62
currently lose_sum: 94.52939105033875
time_elpased: 2.607
batch start
#iterations: 63
currently lose_sum: 94.48977667093277
time_elpased: 2.488
batch start
#iterations: 64
currently lose_sum: 94.80236691236496
time_elpased: 2.464
batch start
#iterations: 65
currently lose_sum: 94.20497971773148
time_elpased: 2.555
batch start
#iterations: 66
currently lose_sum: 93.88135725259781
time_elpased: 2.471
batch start
#iterations: 67
currently lose_sum: 93.70298719406128
time_elpased: 2.595
batch start
#iterations: 68
currently lose_sum: 94.18612563610077
time_elpased: 2.37
batch start
#iterations: 69
currently lose_sum: 94.37288850545883
time_elpased: 2.374
batch start
#iterations: 70
currently lose_sum: 94.0251704454422
time_elpased: 2.464
batch start
#iterations: 71
currently lose_sum: 93.86978542804718
time_elpased: 2.637
batch start
#iterations: 72
currently lose_sum: 93.47940564155579
time_elpased: 2.595
batch start
#iterations: 73
currently lose_sum: 93.27524101734161
time_elpased: 2.652
batch start
#iterations: 74
currently lose_sum: 93.55194866657257
time_elpased: 2.578
batch start
#iterations: 75
currently lose_sum: 93.63798666000366
time_elpased: 2.537
batch start
#iterations: 76
currently lose_sum: 93.61565172672272
time_elpased: 2.229
batch start
#iterations: 77
currently lose_sum: 93.75905328989029
time_elpased: 2.408
batch start
#iterations: 78
currently lose_sum: 93.47030973434448
time_elpased: 2.603
batch start
#iterations: 79
currently lose_sum: 93.7641299366951
time_elpased: 2.648
start validation test
0.623195876289
0.618210391906
0.647730781105
0.632626394613
0.623152801479
62.063
batch start
#iterations: 80
currently lose_sum: 93.1368408203125
time_elpased: 2.426
batch start
#iterations: 81
currently lose_sum: 93.7925996184349
time_elpased: 2.575
batch start
#iterations: 82
currently lose_sum: 93.43223947286606
time_elpased: 2.555
batch start
#iterations: 83
currently lose_sum: 93.88616329431534
time_elpased: 2.771
batch start
#iterations: 84
currently lose_sum: 93.47757613658905
time_elpased: 2.484
batch start
#iterations: 85
currently lose_sum: 93.6585544347763
time_elpased: 2.289
batch start
#iterations: 86
currently lose_sum: 93.34400594234467
time_elpased: 2.339
batch start
#iterations: 87
currently lose_sum: 93.30113953351974
time_elpased: 2.487
batch start
#iterations: 88
currently lose_sum: 93.42342358827591
time_elpased: 2.602
batch start
#iterations: 89
currently lose_sum: 93.02937859296799
time_elpased: 2.676
batch start
#iterations: 90
currently lose_sum: 93.21172577142715
time_elpased: 2.284
batch start
#iterations: 91
currently lose_sum: 93.04081869125366
time_elpased: 2.234
batch start
#iterations: 92
currently lose_sum: 92.89702051877975
time_elpased: 2.503
batch start
#iterations: 93
currently lose_sum: 92.76317197084427
time_elpased: 2.692
batch start
#iterations: 94
currently lose_sum: 93.16793024539948
time_elpased: 2.333
batch start
#iterations: 95
currently lose_sum: 93.14686524868011
time_elpased: 2.641
batch start
#iterations: 96
currently lose_sum: 92.71346718072891
time_elpased: 2.482
batch start
#iterations: 97
currently lose_sum: 92.56611865758896
time_elpased: 2.342
batch start
#iterations: 98
currently lose_sum: 92.55203300714493
time_elpased: 2.664
batch start
#iterations: 99
currently lose_sum: 93.00820600986481
time_elpased: 2.635
start validation test
0.646855670103
0.634301780694
0.69651126891
0.663952518762
0.646768492041
60.532
batch start
#iterations: 100
currently lose_sum: 92.63175189495087
time_elpased: 2.445
batch start
#iterations: 101
currently lose_sum: 92.83118164539337
time_elpased: 2.577
batch start
#iterations: 102
currently lose_sum: 92.64706116914749
time_elpased: 2.569
batch start
#iterations: 103
currently lose_sum: 92.76096993684769
time_elpased: 2.554
batch start
#iterations: 104
currently lose_sum: 92.08379447460175
time_elpased: 2.493
batch start
#iterations: 105
currently lose_sum: 92.34754920005798
time_elpased: 2.542
batch start
#iterations: 106
currently lose_sum: 92.63203209638596
time_elpased: 2.438
batch start
#iterations: 107
currently lose_sum: 92.1902904510498
time_elpased: 2.521
batch start
#iterations: 108
currently lose_sum: 92.16793876886368
time_elpased: 2.495
batch start
#iterations: 109
currently lose_sum: 92.1103515625
time_elpased: 2.528
batch start
#iterations: 110
currently lose_sum: 92.2952950000763
time_elpased: 2.535
batch start
#iterations: 111
currently lose_sum: 92.38245171308517
time_elpased: 2.583
batch start
#iterations: 112
currently lose_sum: 92.20555174350739
time_elpased: 2.68
batch start
#iterations: 113
currently lose_sum: 91.7592117190361
time_elpased: 2.653
batch start
#iterations: 114
currently lose_sum: 92.29655230045319
time_elpased: 2.555
batch start
#iterations: 115
currently lose_sum: 92.14343929290771
time_elpased: 2.656
batch start
#iterations: 116
currently lose_sum: 91.77767437696457
time_elpased: 2.47
batch start
#iterations: 117
currently lose_sum: 92.12742245197296
time_elpased: 2.596
batch start
#iterations: 118
currently lose_sum: 92.1805214881897
time_elpased: 2.52
batch start
#iterations: 119
currently lose_sum: 92.03082001209259
time_elpased: 2.548
start validation test
0.646855670103
0.647640634659
0.646907481733
0.64727385059
0.64685557914
60.379
batch start
#iterations: 120
currently lose_sum: 91.83452844619751
time_elpased: 2.487
batch start
#iterations: 121
currently lose_sum: 92.09025418758392
time_elpased: 2.457
batch start
#iterations: 122
currently lose_sum: 91.70181632041931
time_elpased: 2.434
batch start
#iterations: 123
currently lose_sum: 91.89958214759827
time_elpased: 2.644
batch start
#iterations: 124
currently lose_sum: 92.18497860431671
time_elpased: 2.546
batch start
#iterations: 125
currently lose_sum: 91.88543647527695
time_elpased: 2.561
batch start
#iterations: 126
currently lose_sum: 92.09300637245178
time_elpased: 2.526
batch start
#iterations: 127
currently lose_sum: 91.97670370340347
time_elpased: 2.534
batch start
#iterations: 128
currently lose_sum: 91.8777095079422
time_elpased: 2.724
batch start
#iterations: 129
currently lose_sum: 91.58411753177643
time_elpased: 2.426
batch start
#iterations: 130
currently lose_sum: 91.36097365617752
time_elpased: 2.421
batch start
#iterations: 131
currently lose_sum: 91.73527747392654
time_elpased: 2.477
batch start
#iterations: 132
currently lose_sum: 91.732392847538
time_elpased: 2.253
batch start
#iterations: 133
currently lose_sum: 91.53586566448212
time_elpased: 2.65
batch start
#iterations: 134
currently lose_sum: 90.97214984893799
time_elpased: 2.665
batch start
#iterations: 135
currently lose_sum: 91.87779885530472
time_elpased: 2.404
batch start
#iterations: 136
currently lose_sum: 91.63626843690872
time_elpased: 2.301
batch start
#iterations: 137
currently lose_sum: 91.20537042617798
time_elpased: 2.569
batch start
#iterations: 138
currently lose_sum: 91.0745798945427
time_elpased: 2.675
batch start
#iterations: 139
currently lose_sum: 91.62996661663055
time_elpased: 2.573
start validation test
0.637731958763
0.643658510525
0.619944427292
0.631578947368
0.637763187518
60.566
batch start
#iterations: 140
currently lose_sum: 91.55976063013077
time_elpased: 2.606
batch start
#iterations: 141
currently lose_sum: 91.5919075012207
time_elpased: 2.617
batch start
#iterations: 142
currently lose_sum: 91.21427845954895
time_elpased: 2.627
batch start
#iterations: 143
currently lose_sum: 91.12168186903
time_elpased: 2.544
batch start
#iterations: 144
currently lose_sum: 90.98105561733246
time_elpased: 2.535
batch start
#iterations: 145
currently lose_sum: 91.45960432291031
time_elpased: 2.627
batch start
#iterations: 146
currently lose_sum: 91.17320680618286
time_elpased: 2.589
batch start
#iterations: 147
currently lose_sum: 91.38125717639923
time_elpased: 2.619
batch start
#iterations: 148
currently lose_sum: 90.78303039073944
time_elpased: 2.578
batch start
#iterations: 149
currently lose_sum: 91.70666402578354
time_elpased: 2.41
batch start
#iterations: 150
currently lose_sum: 91.18491965532303
time_elpased: 2.454
batch start
#iterations: 151
currently lose_sum: 91.13096225261688
time_elpased: 2.625
batch start
#iterations: 152
currently lose_sum: 91.12562108039856
time_elpased: 2.655
batch start
#iterations: 153
currently lose_sum: 91.0423344373703
time_elpased: 2.66
batch start
#iterations: 154
currently lose_sum: 90.97362929582596
time_elpased: 2.207
batch start
#iterations: 155
currently lose_sum: 90.97629988193512
time_elpased: 2.642
batch start
#iterations: 156
currently lose_sum: 90.6425244808197
time_elpased: 2.411
batch start
#iterations: 157
currently lose_sum: 90.63473439216614
time_elpased: 2.671
batch start
#iterations: 158
currently lose_sum: 90.93009179830551
time_elpased: 2.381
batch start
#iterations: 159
currently lose_sum: 91.20604658126831
time_elpased: 2.282
start validation test
0.646701030928
0.638617217004
0.678707419986
0.658052284973
0.646644838775
60.495
batch start
#iterations: 160
currently lose_sum: 91.27611058950424
time_elpased: 2.367
batch start
#iterations: 161
currently lose_sum: 90.41671377420425
time_elpased: 2.62
batch start
#iterations: 162
currently lose_sum: 90.35409563779831
time_elpased: 2.377
batch start
#iterations: 163
currently lose_sum: 90.78200590610504
time_elpased: 2.398
batch start
#iterations: 164
currently lose_sum: 90.76004946231842
time_elpased: 2.514
batch start
#iterations: 165
currently lose_sum: 90.71220445632935
time_elpased: 2.63
batch start
#iterations: 166
currently lose_sum: 90.73161745071411
time_elpased: 2.472
batch start
#iterations: 167
currently lose_sum: 90.4968438744545
time_elpased: 2.26
batch start
#iterations: 168
currently lose_sum: 90.49844044446945
time_elpased: 2.457
batch start
#iterations: 169
currently lose_sum: 90.71854084730148
time_elpased: 2.615
batch start
#iterations: 170
currently lose_sum: 90.27937054634094
time_elpased: 2.438
batch start
#iterations: 171
currently lose_sum: 90.42578971385956
time_elpased: 2.41
batch start
#iterations: 172
currently lose_sum: 89.88592064380646
time_elpased: 2.581
batch start
#iterations: 173
currently lose_sum: 90.27047663927078
time_elpased: 2.571
batch start
#iterations: 174
currently lose_sum: 90.26566433906555
time_elpased: 2.606
batch start
#iterations: 175
currently lose_sum: 90.395931661129
time_elpased: 2.763
batch start
#iterations: 176
currently lose_sum: 90.50124472379684
time_elpased: 2.57
batch start
#iterations: 177
currently lose_sum: 90.78126174211502
time_elpased: 2.514
batch start
#iterations: 178
currently lose_sum: 89.80301213264465
time_elpased: 2.685
batch start
#iterations: 179
currently lose_sum: 89.8974637389183
time_elpased: 2.602
start validation test
0.651082474227
0.638170228721
0.700627765771
0.6679421143
0.650995489826
60.401
batch start
#iterations: 180
currently lose_sum: 90.18401855230331
time_elpased: 2.609
batch start
#iterations: 181
currently lose_sum: 89.76511037349701
time_elpased: 2.169
batch start
#iterations: 182
currently lose_sum: 90.35786581039429
time_elpased: 2.628
batch start
#iterations: 183
currently lose_sum: 89.66065913438797
time_elpased: 2.619
batch start
#iterations: 184
currently lose_sum: 89.93390250205994
time_elpased: 2.305
batch start
#iterations: 185
currently lose_sum: 89.90282958745956
time_elpased: 2.639
batch start
#iterations: 186
currently lose_sum: 89.87401056289673
time_elpased: 2.549
batch start
#iterations: 187
currently lose_sum: 89.84947377443314
time_elpased: 2.449
batch start
#iterations: 188
currently lose_sum: 89.88965928554535
time_elpased: 2.527
batch start
#iterations: 189
currently lose_sum: 89.95630675554276
time_elpased: 2.84
batch start
#iterations: 190
currently lose_sum: 89.32857340574265
time_elpased: 2.175
batch start
#iterations: 191
currently lose_sum: 89.4633201956749
time_elpased: 2.678
batch start
#iterations: 192
currently lose_sum: 89.73451060056686
time_elpased: 2.573
batch start
#iterations: 193
currently lose_sum: 89.64530432224274
time_elpased: 2.557
batch start
#iterations: 194
currently lose_sum: 89.63565874099731
time_elpased: 2.706
batch start
#iterations: 195
currently lose_sum: 89.66819739341736
time_elpased: 2.61
batch start
#iterations: 196
currently lose_sum: 89.74148392677307
time_elpased: 2.738
batch start
#iterations: 197
currently lose_sum: 89.6284009218216
time_elpased: 2.61
batch start
#iterations: 198
currently lose_sum: 89.63181459903717
time_elpased: 2.64
batch start
#iterations: 199
currently lose_sum: 90.28247570991516
time_elpased: 2.608
start validation test
0.639329896907
0.647826086957
0.613358032315
0.630121055136
0.639375494521
61.296
batch start
#iterations: 200
currently lose_sum: 89.79373860359192
time_elpased: 2.647
batch start
#iterations: 201
currently lose_sum: 89.63206630945206
time_elpased: 2.594
batch start
#iterations: 202
currently lose_sum: 89.19492155313492
time_elpased: 2.417
batch start
#iterations: 203
currently lose_sum: 89.71807020902634
time_elpased: 2.62
batch start
#iterations: 204
currently lose_sum: 89.55229526758194
time_elpased: 2.624
batch start
#iterations: 205
currently lose_sum: 89.52412205934525
time_elpased: 2.65
batch start
#iterations: 206
currently lose_sum: 89.44292134046555
time_elpased: 2.514
batch start
#iterations: 207
currently lose_sum: 89.2618505358696
time_elpased: 2.527
batch start
#iterations: 208
currently lose_sum: 89.04375326633453
time_elpased: 2.678
batch start
#iterations: 209
currently lose_sum: 89.3930846452713
time_elpased: 2.398
batch start
#iterations: 210
currently lose_sum: 89.5104061961174
time_elpased: 2.514
batch start
#iterations: 211
currently lose_sum: 89.07576954364777
time_elpased: 2.548
batch start
#iterations: 212
currently lose_sum: 89.24800169467926
time_elpased: 2.438
batch start
#iterations: 213
currently lose_sum: 88.72691416740417
time_elpased: 2.475
batch start
#iterations: 214
currently lose_sum: 89.02976351976395
time_elpased: 2.526
batch start
#iterations: 215
currently lose_sum: 89.28435230255127
time_elpased: 2.599
batch start
#iterations: 216
currently lose_sum: 89.12837958335876
time_elpased: 2.584
batch start
#iterations: 217
currently lose_sum: 89.09439730644226
time_elpased: 2.541
batch start
#iterations: 218
currently lose_sum: 89.20335292816162
time_elpased: 2.706
batch start
#iterations: 219
currently lose_sum: 89.43865495920181
time_elpased: 2.502
start validation test
0.634226804124
0.626630592328
0.667387053617
0.646366988937
0.634168586191
61.619
batch start
#iterations: 220
currently lose_sum: 89.3134395480156
time_elpased: 2.592
batch start
#iterations: 221
currently lose_sum: 89.06713527441025
time_elpased: 2.418
batch start
#iterations: 222
currently lose_sum: 88.65971153974533
time_elpased: 2.45
batch start
#iterations: 223
currently lose_sum: 89.07993417978287
time_elpased: 2.594
batch start
#iterations: 224
currently lose_sum: 89.24124383926392
time_elpased: 2.557
batch start
#iterations: 225
currently lose_sum: 88.76432394981384
time_elpased: 2.709
batch start
#iterations: 226
currently lose_sum: 88.6751937866211
time_elpased: 2.578
batch start
#iterations: 227
currently lose_sum: 88.66231375932693
time_elpased: 2.742
batch start
#iterations: 228
currently lose_sum: 88.13157534599304
time_elpased: 2.506
batch start
#iterations: 229
currently lose_sum: 88.5676440000534
time_elpased: 2.627
batch start
#iterations: 230
currently lose_sum: 88.3079913854599
time_elpased: 2.561
batch start
#iterations: 231
currently lose_sum: 88.40468770265579
time_elpased: 2.666
batch start
#iterations: 232
currently lose_sum: 88.26922661066055
time_elpased: 2.394
batch start
#iterations: 233
currently lose_sum: 88.7754362821579
time_elpased: 2.667
batch start
#iterations: 234
currently lose_sum: 88.88974767923355
time_elpased: 2.542
batch start
#iterations: 235
currently lose_sum: 88.62840020656586
time_elpased: 2.615
batch start
#iterations: 236
currently lose_sum: 88.4324198961258
time_elpased: 2.543
batch start
#iterations: 237
currently lose_sum: 88.67069602012634
time_elpased: 2.596
batch start
#iterations: 238
currently lose_sum: 88.2768907546997
time_elpased: 2.648
batch start
#iterations: 239
currently lose_sum: 88.29279243946075
time_elpased: 2.567
start validation test
0.63175257732
0.623168980373
0.669856951734
0.645670072413
0.631685679213
61.831
batch start
#iterations: 240
currently lose_sum: 88.31230264902115
time_elpased: 2.514
batch start
#iterations: 241
currently lose_sum: 88.06905829906464
time_elpased: 2.627
batch start
#iterations: 242
currently lose_sum: 88.26220601797104
time_elpased: 2.579
batch start
#iterations: 243
currently lose_sum: 88.28290647268295
time_elpased: 2.527
batch start
#iterations: 244
currently lose_sum: 88.55947971343994
time_elpased: 2.514
batch start
#iterations: 245
currently lose_sum: 88.53153324127197
time_elpased: 2.522
batch start
#iterations: 246
currently lose_sum: 88.4734496474266
time_elpased: 2.545
batch start
#iterations: 247
currently lose_sum: 87.90499180555344
time_elpased: 2.67
batch start
#iterations: 248
currently lose_sum: 88.63772392272949
time_elpased: 2.721
batch start
#iterations: 249
currently lose_sum: 88.25001835823059
time_elpased: 2.538
batch start
#iterations: 250
currently lose_sum: 88.42391192913055
time_elpased: 2.687
batch start
#iterations: 251
currently lose_sum: 87.64560955762863
time_elpased: 2.62
batch start
#iterations: 252
currently lose_sum: 88.13608139753342
time_elpased: 2.602
batch start
#iterations: 253
currently lose_sum: 87.62174618244171
time_elpased: 2.65
batch start
#iterations: 254
currently lose_sum: 87.51803398132324
time_elpased: 2.438
batch start
#iterations: 255
currently lose_sum: 87.47497129440308
time_elpased: 2.743
batch start
#iterations: 256
currently lose_sum: 87.65911275148392
time_elpased: 2.266
batch start
#iterations: 257
currently lose_sum: 88.31583559513092
time_elpased: 2.592
batch start
#iterations: 258
currently lose_sum: 87.88474774360657
time_elpased: 2.49
batch start
#iterations: 259
currently lose_sum: 87.18337625265121
time_elpased: 2.611
start validation test
0.632989690722
0.630778527546
0.644540496038
0.637585259086
0.632969411501
62.084
batch start
#iterations: 260
currently lose_sum: 87.42509889602661
time_elpased: 2.529
batch start
#iterations: 261
currently lose_sum: 87.70185607671738
time_elpased: 2.513
batch start
#iterations: 262
currently lose_sum: 87.87434267997742
time_elpased: 2.376
batch start
#iterations: 263
currently lose_sum: 87.00734704732895
time_elpased: 2.669
batch start
#iterations: 264
currently lose_sum: 87.6395696401596
time_elpased: 2.691
batch start
#iterations: 265
currently lose_sum: 86.85249090194702
time_elpased: 2.578
batch start
#iterations: 266
currently lose_sum: 87.82413709163666
time_elpased: 2.619
batch start
#iterations: 267
currently lose_sum: 87.2043639421463
time_elpased: 2.532
batch start
#iterations: 268
currently lose_sum: 87.36946654319763
time_elpased: 2.406
batch start
#iterations: 269
currently lose_sum: 87.08022373914719
time_elpased: 2.442
batch start
#iterations: 270
currently lose_sum: 87.2935152053833
time_elpased: 2.43
batch start
#iterations: 271
currently lose_sum: 87.39390420913696
time_elpased: 2.403
batch start
#iterations: 272
currently lose_sum: 87.39001947641373
time_elpased: 2.593
batch start
#iterations: 273
currently lose_sum: 87.5674467086792
time_elpased: 2.637
batch start
#iterations: 274
currently lose_sum: 87.43739157915115
time_elpased: 2.613
batch start
#iterations: 275
currently lose_sum: 86.79760313034058
time_elpased: 2.436
batch start
#iterations: 276
currently lose_sum: 87.289606153965
time_elpased: 2.352
batch start
#iterations: 277
currently lose_sum: 87.26389729976654
time_elpased: 2.362
batch start
#iterations: 278
currently lose_sum: 87.59618359804153
time_elpased: 2.65
batch start
#iterations: 279
currently lose_sum: 87.3625316619873
time_elpased: 2.545
start validation test
0.635154639175
0.645720596356
0.601728928682
0.622949073088
0.635213323166
62.265
batch start
#iterations: 280
currently lose_sum: 87.40746635198593
time_elpased: 2.36
batch start
#iterations: 281
currently lose_sum: 87.22433984279633
time_elpased: 2.526
batch start
#iterations: 282
currently lose_sum: 86.81698620319366
time_elpased: 2.329
batch start
#iterations: 283
currently lose_sum: 86.85647416114807
time_elpased: 2.613
batch start
#iterations: 284
currently lose_sum: 86.82843232154846
time_elpased: 2.405
batch start
#iterations: 285
currently lose_sum: 86.48175448179245
time_elpased: 2.407
batch start
#iterations: 286
currently lose_sum: 86.7046874165535
time_elpased: 2.556
batch start
#iterations: 287
currently lose_sum: 86.67687851190567
time_elpased: 2.587
batch start
#iterations: 288
currently lose_sum: 86.64047992229462
time_elpased: 2.531
batch start
#iterations: 289
currently lose_sum: 86.49969106912613
time_elpased: 2.484
batch start
#iterations: 290
currently lose_sum: 86.75848716497421
time_elpased: 2.592
batch start
#iterations: 291
currently lose_sum: 86.43697935342789
time_elpased: 2.622
batch start
#iterations: 292
currently lose_sum: 86.62181562185287
time_elpased: 2.37
batch start
#iterations: 293
currently lose_sum: 86.39392268657684
time_elpased: 2.378
batch start
#iterations: 294
currently lose_sum: 86.31397938728333
time_elpased: 2.526
batch start
#iterations: 295
currently lose_sum: 86.19926232099533
time_elpased: 2.643
batch start
#iterations: 296
currently lose_sum: 86.17885959148407
time_elpased: 2.611
batch start
#iterations: 297
currently lose_sum: 86.27559286355972
time_elpased: 2.613
batch start
#iterations: 298
currently lose_sum: 86.30258232355118
time_elpased: 2.576
batch start
#iterations: 299
currently lose_sum: 86.21561372280121
time_elpased: 2.691
start validation test
0.621804123711
0.638339920949
0.565092106617
0.599486871554
0.621903690405
63.064
batch start
#iterations: 300
currently lose_sum: 86.52099812030792
time_elpased: 2.277
batch start
#iterations: 301
currently lose_sum: 86.42522531747818
time_elpased: 2.523
batch start
#iterations: 302
currently lose_sum: 86.62558257579803
time_elpased: 2.428
batch start
#iterations: 303
currently lose_sum: 86.15299588441849
time_elpased: 2.675
batch start
#iterations: 304
currently lose_sum: 86.21652311086655
time_elpased: 2.578
batch start
#iterations: 305
currently lose_sum: 86.01902437210083
time_elpased: 2.513
batch start
#iterations: 306
currently lose_sum: 86.41089671850204
time_elpased: 2.557
batch start
#iterations: 307
currently lose_sum: 85.88022667169571
time_elpased: 2.183
batch start
#iterations: 308
currently lose_sum: 86.77853280305862
time_elpased: 2.72
batch start
#iterations: 309
currently lose_sum: 85.98955982923508
time_elpased: 2.335
batch start
#iterations: 310
currently lose_sum: 85.70008319616318
time_elpased: 2.609
batch start
#iterations: 311
currently lose_sum: 85.81756246089935
time_elpased: 2.571
batch start
#iterations: 312
currently lose_sum: 85.75025010108948
time_elpased: 2.713
batch start
#iterations: 313
currently lose_sum: 85.35277199745178
time_elpased: 2.486
batch start
#iterations: 314
currently lose_sum: 85.74204325675964
time_elpased: 2.384
batch start
#iterations: 315
currently lose_sum: 85.8618888258934
time_elpased: 2.237
batch start
#iterations: 316
currently lose_sum: 85.84750121831894
time_elpased: 2.651
batch start
#iterations: 317
currently lose_sum: 86.2630352973938
time_elpased: 2.537
batch start
#iterations: 318
currently lose_sum: 85.62576246261597
time_elpased: 2.569
batch start
#iterations: 319
currently lose_sum: 85.55875939130783
time_elpased: 2.611
start validation test
0.622835051546
0.639990667289
0.56457754451
0.599923451255
0.622937331586
64.127
batch start
#iterations: 320
currently lose_sum: 85.60804921388626
time_elpased: 2.577
batch start
#iterations: 321
currently lose_sum: 85.7832579612732
time_elpased: 2.576
batch start
#iterations: 322
currently lose_sum: 85.9481218457222
time_elpased: 2.407
batch start
#iterations: 323
currently lose_sum: 85.21303844451904
time_elpased: 2.533
batch start
#iterations: 324
currently lose_sum: 85.57595407962799
time_elpased: 2.44
batch start
#iterations: 325
currently lose_sum: 85.56588435173035
time_elpased: 2.313
batch start
#iterations: 326
currently lose_sum: 85.31904971599579
time_elpased: 2.661
batch start
#iterations: 327
currently lose_sum: 85.29082584381104
time_elpased: 2.729
batch start
#iterations: 328
currently lose_sum: 85.20163679122925
time_elpased: 2.664
batch start
#iterations: 329
currently lose_sum: 85.34526890516281
time_elpased: 2.58
batch start
#iterations: 330
currently lose_sum: 85.57408899068832
time_elpased: 2.456
batch start
#iterations: 331
currently lose_sum: 85.61130166053772
time_elpased: 2.478
batch start
#iterations: 332
currently lose_sum: 84.71548235416412
time_elpased: 2.622
batch start
#iterations: 333
currently lose_sum: 85.34879404306412
time_elpased: 2.412
batch start
#iterations: 334
currently lose_sum: 85.25471985340118
time_elpased: 2.538
batch start
#iterations: 335
currently lose_sum: 85.15880364179611
time_elpased: 2.601
batch start
#iterations: 336
currently lose_sum: 85.2703110575676
time_elpased: 2.766
batch start
#iterations: 337
currently lose_sum: 85.2097098827362
time_elpased: 2.745
batch start
#iterations: 338
currently lose_sum: 85.438880443573
time_elpased: 2.625
batch start
#iterations: 339
currently lose_sum: 84.95367807149887
time_elpased: 2.455
start validation test
0.620515463918
0.633548826131
0.574868786663
0.602784072515
0.6205956037
64.324
batch start
#iterations: 340
currently lose_sum: 85.40494519472122
time_elpased: 2.556
batch start
#iterations: 341
currently lose_sum: 84.1187589764595
time_elpased: 2.515
batch start
#iterations: 342
currently lose_sum: 84.83033096790314
time_elpased: 2.413
batch start
#iterations: 343
currently lose_sum: 85.0933250784874
time_elpased: 2.679
batch start
#iterations: 344
currently lose_sum: 84.33053243160248
time_elpased: 2.763
batch start
#iterations: 345
currently lose_sum: 84.77944600582123
time_elpased: 2.553
batch start
#iterations: 346
currently lose_sum: 84.2798165678978
time_elpased: 2.589
batch start
#iterations: 347
currently lose_sum: 85.40694892406464
time_elpased: 2.442
batch start
#iterations: 348
currently lose_sum: 84.95909798145294
time_elpased: 2.596
batch start
#iterations: 349
currently lose_sum: 84.7575985789299
time_elpased: 2.429
batch start
#iterations: 350
currently lose_sum: 84.91690850257874
time_elpased: 2.6
batch start
#iterations: 351
currently lose_sum: 84.57007122039795
time_elpased: 2.66
batch start
#iterations: 352
currently lose_sum: 84.22035849094391
time_elpased: 2.637
batch start
#iterations: 353
currently lose_sum: 84.88655889034271
time_elpased: 2.592
batch start
#iterations: 354
currently lose_sum: 84.77622205018997
time_elpased: 2.553
batch start
#iterations: 355
currently lose_sum: 84.52778285741806
time_elpased: 2.65
batch start
#iterations: 356
currently lose_sum: 84.53342521190643
time_elpased: 2.681
batch start
#iterations: 357
currently lose_sum: 84.22509586811066
time_elpased: 2.689
batch start
#iterations: 358
currently lose_sum: 84.89298629760742
time_elpased: 2.511
batch start
#iterations: 359
currently lose_sum: 84.36149632930756
time_elpased: 2.721
start validation test
0.616907216495
0.629726354037
0.570752289801
0.598790757936
0.616988248588
65.413
batch start
#iterations: 360
currently lose_sum: 84.26561665534973
time_elpased: 2.637
batch start
#iterations: 361
currently lose_sum: 84.05578503012657
time_elpased: 2.678
batch start
#iterations: 362
currently lose_sum: 84.09249287843704
time_elpased: 2.623
batch start
#iterations: 363
currently lose_sum: 83.96525228023529
time_elpased: 2.656
batch start
#iterations: 364
currently lose_sum: 84.09452030062675
time_elpased: 2.578
batch start
#iterations: 365
currently lose_sum: 84.27615582942963
time_elpased: 2.543
batch start
#iterations: 366
currently lose_sum: 84.0045554637909
time_elpased: 2.633
batch start
#iterations: 367
currently lose_sum: 83.98747718334198
time_elpased: 2.888
batch start
#iterations: 368
currently lose_sum: 83.76773270964622
time_elpased: 2.313
batch start
#iterations: 369
currently lose_sum: 83.78151285648346
time_elpased: 2.623
batch start
#iterations: 370
currently lose_sum: 83.68237608671188
time_elpased: 2.411
batch start
#iterations: 371
currently lose_sum: 83.83477401733398
time_elpased: 2.492
batch start
#iterations: 372
currently lose_sum: 83.84608596563339
time_elpased: 2.582
batch start
#iterations: 373
currently lose_sum: 83.76974776387215
time_elpased: 2.572
batch start
#iterations: 374
currently lose_sum: 83.81056076288223
time_elpased: 2.69
batch start
#iterations: 375
currently lose_sum: 84.0352013707161
time_elpased: 2.551
batch start
#iterations: 376
currently lose_sum: 83.59011596441269
time_elpased: 2.596
batch start
#iterations: 377
currently lose_sum: 83.69682168960571
time_elpased: 2.454
batch start
#iterations: 378
currently lose_sum: 83.75642055273056
time_elpased: 2.54
batch start
#iterations: 379
currently lose_sum: 83.59330487251282
time_elpased: 2.552
start validation test
0.608505154639
0.629674896113
0.530204795719
0.575674618694
0.608642622996
66.531
batch start
#iterations: 380
currently lose_sum: 83.34723180532455
time_elpased: 2.627
batch start
#iterations: 381
currently lose_sum: 83.30655944347382
time_elpased: 2.789
batch start
#iterations: 382
currently lose_sum: 83.1509864628315
time_elpased: 2.796
batch start
#iterations: 383
currently lose_sum: 83.6009612083435
time_elpased: 2.549
batch start
#iterations: 384
currently lose_sum: 83.78643667697906
time_elpased: 2.549
batch start
#iterations: 385
currently lose_sum: 83.7137154340744
time_elpased: 2.551
batch start
#iterations: 386
currently lose_sum: 83.9179736673832
time_elpased: 2.716
batch start
#iterations: 387
currently lose_sum: 83.22532248497009
time_elpased: 2.553
batch start
#iterations: 388
currently lose_sum: 82.88599318265915
time_elpased: 2.577
batch start
#iterations: 389
currently lose_sum: 84.12073540687561
time_elpased: 2.457
batch start
#iterations: 390
currently lose_sum: 83.67269796133041
time_elpased: 2.477
batch start
#iterations: 391
currently lose_sum: 83.70149981975555
time_elpased: 2.452
batch start
#iterations: 392
currently lose_sum: 83.17864447832108
time_elpased: 2.467
batch start
#iterations: 393
currently lose_sum: 83.14597433805466
time_elpased: 2.449
batch start
#iterations: 394
currently lose_sum: 83.28771924972534
time_elpased: 2.582
batch start
#iterations: 395
currently lose_sum: 82.55052351951599
time_elpased: 2.606
batch start
#iterations: 396
currently lose_sum: 82.81997042894363
time_elpased: 2.66
batch start
#iterations: 397
currently lose_sum: 82.08919250965118
time_elpased: 2.648
batch start
#iterations: 398
currently lose_sum: 82.5883566737175
time_elpased: 2.663
batch start
#iterations: 399
currently lose_sum: 83.31058722734451
time_elpased: 2.611
start validation test
0.61118556701
0.626395348837
0.554389214778
0.588196757111
0.611285281767
67.087
acc: 0.647
pre: 0.647
rec: 0.650
F1: 0.648
auc: 0.704
