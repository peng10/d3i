start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.28600907325745
time_elpased: 2.432
batch start
#iterations: 1
currently lose_sum: 100.14484632015228
time_elpased: 2.407
batch start
#iterations: 2
currently lose_sum: 99.89169591665268
time_elpased: 2.424
batch start
#iterations: 3
currently lose_sum: 99.67016458511353
time_elpased: 2.436
batch start
#iterations: 4
currently lose_sum: 99.56308877468109
time_elpased: 2.102
batch start
#iterations: 5
currently lose_sum: 99.64616918563843
time_elpased: 2.279
batch start
#iterations: 6
currently lose_sum: 99.41959226131439
time_elpased: 2.279
batch start
#iterations: 7
currently lose_sum: 99.3216741681099
time_elpased: 2.325
batch start
#iterations: 8
currently lose_sum: 99.33735448122025
time_elpased: 2.353
batch start
#iterations: 9
currently lose_sum: 99.30889320373535
time_elpased: 2.325
batch start
#iterations: 10
currently lose_sum: 99.18920302391052
time_elpased: 2.381
batch start
#iterations: 11
currently lose_sum: 99.31376475095749
time_elpased: 2.379
batch start
#iterations: 12
currently lose_sum: 99.43866676092148
time_elpased: 2.328
batch start
#iterations: 13
currently lose_sum: 99.18196338415146
time_elpased: 2.282
batch start
#iterations: 14
currently lose_sum: 99.08246088027954
time_elpased: 2.34
batch start
#iterations: 15
currently lose_sum: 99.13687109947205
time_elpased: 2.335
batch start
#iterations: 16
currently lose_sum: 99.12053006887436
time_elpased: 2.42
batch start
#iterations: 17
currently lose_sum: 98.90599524974823
time_elpased: 2.392
batch start
#iterations: 18
currently lose_sum: 99.08758211135864
time_elpased: 2.418
batch start
#iterations: 19
currently lose_sum: 98.89060235023499
time_elpased: 2.342
start validation test
0.618762886598
0.63797408156
0.552228053926
0.592012356575
0.618879698759
64.388
batch start
#iterations: 20
currently lose_sum: 99.1169006228447
time_elpased: 2.322
batch start
#iterations: 21
currently lose_sum: 99.09447407722473
time_elpased: 2.326
batch start
#iterations: 22
currently lose_sum: 98.92635411024094
time_elpased: 2.167
batch start
#iterations: 23
currently lose_sum: 98.96022266149521
time_elpased: 2.162
batch start
#iterations: 24
currently lose_sum: 98.82102966308594
time_elpased: 2.311
batch start
#iterations: 25
currently lose_sum: 99.09402072429657
time_elpased: 2.394
batch start
#iterations: 26
currently lose_sum: 98.93348211050034
time_elpased: 2.232
batch start
#iterations: 27
currently lose_sum: 99.0006252527237
time_elpased: 2.334
batch start
#iterations: 28
currently lose_sum: 98.93649411201477
time_elpased: 2.191
batch start
#iterations: 29
currently lose_sum: 99.25866383314133
time_elpased: 2.324
batch start
#iterations: 30
currently lose_sum: 98.91611313819885
time_elpased: 2.328
batch start
#iterations: 31
currently lose_sum: 98.71478861570358
time_elpased: 2.352
batch start
#iterations: 32
currently lose_sum: 98.76881122589111
time_elpased: 2.247
batch start
#iterations: 33
currently lose_sum: 99.0429270863533
time_elpased: 2.566
batch start
#iterations: 34
currently lose_sum: 98.88859534263611
time_elpased: 2.209
batch start
#iterations: 35
currently lose_sum: 98.92480206489563
time_elpased: 2.34
batch start
#iterations: 36
currently lose_sum: 98.69770550727844
time_elpased: 2.356
batch start
#iterations: 37
currently lose_sum: 98.89879041910172
time_elpased: 2.338
batch start
#iterations: 38
currently lose_sum: 98.76778030395508
time_elpased: 2.333
batch start
#iterations: 39
currently lose_sum: 99.0379889011383
time_elpased: 2.368
start validation test
0.623453608247
0.654973014649
0.524544612535
0.582547574147
0.623627258245
64.139
batch start
#iterations: 40
currently lose_sum: 98.88946324586868
time_elpased: 2.268
batch start
#iterations: 41
currently lose_sum: 98.87422442436218
time_elpased: 2.088
batch start
#iterations: 42
currently lose_sum: 98.87300181388855
time_elpased: 2.147
batch start
#iterations: 43
currently lose_sum: 98.74736708402634
time_elpased: 2.364
batch start
#iterations: 44
currently lose_sum: 98.94273942708969
time_elpased: 2.332
batch start
#iterations: 45
currently lose_sum: 98.79004085063934
time_elpased: 2.318
batch start
#iterations: 46
currently lose_sum: 98.68245869874954
time_elpased: 2.277
batch start
#iterations: 47
currently lose_sum: 98.90518802404404
time_elpased: 2.347
batch start
#iterations: 48
currently lose_sum: 98.87436765432358
time_elpased: 2.268
batch start
#iterations: 49
currently lose_sum: 98.70192140340805
time_elpased: 2.312
batch start
#iterations: 50
currently lose_sum: 98.82328289747238
time_elpased: 2.063
batch start
#iterations: 51
currently lose_sum: 98.7540711760521
time_elpased: 2.206
batch start
#iterations: 52
currently lose_sum: 98.77383190393448
time_elpased: 2.375
batch start
#iterations: 53
currently lose_sum: 98.80598777532578
time_elpased: 2.361
batch start
#iterations: 54
currently lose_sum: 98.49141901731491
time_elpased: 2.329
batch start
#iterations: 55
currently lose_sum: 98.76113319396973
time_elpased: 2.252
batch start
#iterations: 56
currently lose_sum: 98.76690697669983
time_elpased: 2.044
batch start
#iterations: 57
currently lose_sum: 98.68621551990509
time_elpased: 2.282
batch start
#iterations: 58
currently lose_sum: 98.75145304203033
time_elpased: 2.386
batch start
#iterations: 59
currently lose_sum: 98.75919657945633
time_elpased: 2.31
start validation test
0.63912371134
0.645054475539
0.621488113615
0.633052046753
0.639154673352
63.802
batch start
#iterations: 60
currently lose_sum: 98.78644001483917
time_elpased: 2.346
batch start
#iterations: 61
currently lose_sum: 98.49858123064041
time_elpased: 2.373
batch start
#iterations: 62
currently lose_sum: 98.80313342809677
time_elpased: 2.36
batch start
#iterations: 63
currently lose_sum: 98.89183163642883
time_elpased: 2.143
batch start
#iterations: 64
currently lose_sum: 98.73256677389145
time_elpased: 2.371
batch start
#iterations: 65
currently lose_sum: 98.6181760430336
time_elpased: 2.297
batch start
#iterations: 66
currently lose_sum: 98.77456790208817
time_elpased: 2.497
batch start
#iterations: 67
currently lose_sum: 98.78201007843018
time_elpased: 2.366
batch start
#iterations: 68
currently lose_sum: 98.64079636335373
time_elpased: 2.357
batch start
#iterations: 69
currently lose_sum: 98.6031396985054
time_elpased: 2.148
batch start
#iterations: 70
currently lose_sum: 98.57302534580231
time_elpased: 2.27
batch start
#iterations: 71
currently lose_sum: 98.87145519256592
time_elpased: 2.297
batch start
#iterations: 72
currently lose_sum: 98.55968737602234
time_elpased: 2.156
batch start
#iterations: 73
currently lose_sum: 98.7267434000969
time_elpased: 1.953
batch start
#iterations: 74
currently lose_sum: 98.87161946296692
time_elpased: 2.112
batch start
#iterations: 75
currently lose_sum: 98.80416768789291
time_elpased: 2.423
batch start
#iterations: 76
currently lose_sum: 98.55394643545151
time_elpased: 1.934
batch start
#iterations: 77
currently lose_sum: 98.6673269867897
time_elpased: 2.239
batch start
#iterations: 78
currently lose_sum: 98.7558862566948
time_elpased: 2.321
batch start
#iterations: 79
currently lose_sum: 98.95420169830322
time_elpased: 2.352
start validation test
0.61824742268
0.64797029069
0.520736852938
0.577427821522
0.618418617526
63.895
batch start
#iterations: 80
currently lose_sum: 98.88972520828247
time_elpased: 2.273
batch start
#iterations: 81
currently lose_sum: 98.72504585981369
time_elpased: 2.165
batch start
#iterations: 82
currently lose_sum: 98.73168987035751
time_elpased: 2.291
batch start
#iterations: 83
currently lose_sum: 98.56054127216339
time_elpased: 2.154
batch start
#iterations: 84
currently lose_sum: 98.7505407333374
time_elpased: 2.328
batch start
#iterations: 85
currently lose_sum: 98.76349598169327
time_elpased: 2.343
batch start
#iterations: 86
currently lose_sum: 98.76366311311722
time_elpased: 2.181
batch start
#iterations: 87
currently lose_sum: 98.57807421684265
time_elpased: 2.208
batch start
#iterations: 88
currently lose_sum: 98.66591966152191
time_elpased: 2.376
batch start
#iterations: 89
currently lose_sum: 98.65782886743546
time_elpased: 2.441
batch start
#iterations: 90
currently lose_sum: 98.79984420537949
time_elpased: 2.397
batch start
#iterations: 91
currently lose_sum: 98.71749019622803
time_elpased: 2.248
batch start
#iterations: 92
currently lose_sum: 98.5700061917305
time_elpased: 2.471
batch start
#iterations: 93
currently lose_sum: 98.72125142812729
time_elpased: 2.223
batch start
#iterations: 94
currently lose_sum: 98.88553434610367
time_elpased: 2.44
batch start
#iterations: 95
currently lose_sum: 98.5253683924675
time_elpased: 2.499
batch start
#iterations: 96
currently lose_sum: 98.65130740404129
time_elpased: 2.395
batch start
#iterations: 97
currently lose_sum: 98.6406842470169
time_elpased: 2.413
batch start
#iterations: 98
currently lose_sum: 98.69952762126923
time_elpased: 2.301
batch start
#iterations: 99
currently lose_sum: 98.610307097435
time_elpased: 2.252
start validation test
0.630927835052
0.65600976205
0.553257178141
0.600267976775
0.63106419787
63.774
batch start
#iterations: 100
currently lose_sum: 98.8689061999321
time_elpased: 1.945
batch start
#iterations: 101
currently lose_sum: 98.65650808811188
time_elpased: 2.328
batch start
#iterations: 102
currently lose_sum: 98.71877306699753
time_elpased: 2.398
batch start
#iterations: 103
currently lose_sum: 98.70323234796524
time_elpased: 2.355
batch start
#iterations: 104
currently lose_sum: 98.48043102025986
time_elpased: 2.283
batch start
#iterations: 105
currently lose_sum: 98.54358398914337
time_elpased: 2.307
batch start
#iterations: 106
currently lose_sum: 98.77301847934723
time_elpased: 2.334
batch start
#iterations: 107
currently lose_sum: 98.7452340722084
time_elpased: 2.35
batch start
#iterations: 108
currently lose_sum: 98.7826172709465
time_elpased: 2.023
batch start
#iterations: 109
currently lose_sum: 98.80609130859375
time_elpased: 2.38
batch start
#iterations: 110
currently lose_sum: 98.67819547653198
time_elpased: 2.409
batch start
#iterations: 111
currently lose_sum: 98.59130144119263
time_elpased: 2.333
batch start
#iterations: 112
currently lose_sum: 98.65333288908005
time_elpased: 2.01
batch start
#iterations: 113
currently lose_sum: 98.53192538022995
time_elpased: 2.116
batch start
#iterations: 114
currently lose_sum: 98.53822028636932
time_elpased: 2.385
batch start
#iterations: 115
currently lose_sum: 98.69511198997498
time_elpased: 2.328
batch start
#iterations: 116
currently lose_sum: 98.46498042345047
time_elpased: 1.988
batch start
#iterations: 117
currently lose_sum: 98.68999010324478
time_elpased: 2.306
batch start
#iterations: 118
currently lose_sum: 98.50960332155228
time_elpased: 2.003
batch start
#iterations: 119
currently lose_sum: 98.61477941274643
time_elpased: 2.317
start validation test
0.633092783505
0.663397460078
0.542965935988
0.597170345218
0.633251015087
63.628
batch start
#iterations: 120
currently lose_sum: 98.58826667070389
time_elpased: 2.291
batch start
#iterations: 121
currently lose_sum: 98.41590440273285
time_elpased: 2.34
batch start
#iterations: 122
currently lose_sum: 98.65121978521347
time_elpased: 2.131
batch start
#iterations: 123
currently lose_sum: 98.72393590211868
time_elpased: 2.343
batch start
#iterations: 124
currently lose_sum: 98.68183904886246
time_elpased: 2.227
batch start
#iterations: 125
currently lose_sum: 98.58374518156052
time_elpased: 2.336
batch start
#iterations: 126
currently lose_sum: 98.8250242471695
time_elpased: 2.32
batch start
#iterations: 127
currently lose_sum: 98.81878888607025
time_elpased: 2.316
batch start
#iterations: 128
currently lose_sum: 98.63964480161667
time_elpased: 2.335
batch start
#iterations: 129
currently lose_sum: 98.73883759975433
time_elpased: 2.317
batch start
#iterations: 130
currently lose_sum: 98.53608065843582
time_elpased: 2.223
batch start
#iterations: 131
currently lose_sum: 98.73725980520248
time_elpased: 2.249
batch start
#iterations: 132
currently lose_sum: 98.73400658369064
time_elpased: 2.454
batch start
#iterations: 133
currently lose_sum: 98.56875574588776
time_elpased: 2.365
batch start
#iterations: 134
currently lose_sum: 98.5956546664238
time_elpased: 2.324
batch start
#iterations: 135
currently lose_sum: 98.61497670412064
time_elpased: 2.228
batch start
#iterations: 136
currently lose_sum: 98.60199987888336
time_elpased: 2.331
batch start
#iterations: 137
currently lose_sum: 98.4365746974945
time_elpased: 2.329
batch start
#iterations: 138
currently lose_sum: 98.6649602651596
time_elpased: 2.328
batch start
#iterations: 139
currently lose_sum: 98.45772588253021
time_elpased: 2.383
start validation test
0.637113402062
0.666459395598
0.551507666975
0.603558959342
0.637263696134
63.516
batch start
#iterations: 140
currently lose_sum: 98.60989344120026
time_elpased: 2.414
batch start
#iterations: 141
currently lose_sum: 98.81923419237137
time_elpased: 2.258
batch start
#iterations: 142
currently lose_sum: 98.58322125673294
time_elpased: 2.35
batch start
#iterations: 143
currently lose_sum: 98.6112534403801
time_elpased: 2.368
batch start
#iterations: 144
currently lose_sum: 98.81265413761139
time_elpased: 2.321
batch start
#iterations: 145
currently lose_sum: 98.59858900308609
time_elpased: 2.337
batch start
#iterations: 146
currently lose_sum: 98.51625102758408
time_elpased: 2.37
batch start
#iterations: 147
currently lose_sum: 98.34913182258606
time_elpased: 2.325
batch start
#iterations: 148
currently lose_sum: 98.45653313398361
time_elpased: 2.228
batch start
#iterations: 149
currently lose_sum: 98.59392821788788
time_elpased: 2.47
batch start
#iterations: 150
currently lose_sum: 98.55195564031601
time_elpased: 2.365
batch start
#iterations: 151
currently lose_sum: 98.53011935949326
time_elpased: 2.335
batch start
#iterations: 152
currently lose_sum: 98.54790115356445
time_elpased: 2.34
batch start
#iterations: 153
currently lose_sum: 98.77242797613144
time_elpased: 2.301
batch start
#iterations: 154
currently lose_sum: 98.7184506058693
time_elpased: 2.223
batch start
#iterations: 155
currently lose_sum: 98.60695827007294
time_elpased: 2.204
batch start
#iterations: 156
currently lose_sum: 98.66162270307541
time_elpased: 2.339
batch start
#iterations: 157
currently lose_sum: 98.4618119597435
time_elpased: 2.305
batch start
#iterations: 158
currently lose_sum: 98.6227975487709
time_elpased: 2.103
batch start
#iterations: 159
currently lose_sum: 98.64307397603989
time_elpased: 2.392
start validation test
0.63675257732
0.663282778865
0.558094061953
0.606158833063
0.636890674476
63.583
batch start
#iterations: 160
currently lose_sum: 98.46509426832199
time_elpased: 2.379
batch start
#iterations: 161
currently lose_sum: 98.85451835393906
time_elpased: 2.459
batch start
#iterations: 162
currently lose_sum: 98.80381399393082
time_elpased: 2.419
batch start
#iterations: 163
currently lose_sum: 98.71356183290482
time_elpased: 2.224
batch start
#iterations: 164
currently lose_sum: 98.40429073572159
time_elpased: 2.266
batch start
#iterations: 165
currently lose_sum: 98.59288680553436
time_elpased: 2.445
batch start
#iterations: 166
currently lose_sum: 98.51728081703186
time_elpased: 2.554
batch start
#iterations: 167
currently lose_sum: 98.50752019882202
time_elpased: 2.203
batch start
#iterations: 168
currently lose_sum: 98.39128714799881
time_elpased: 2.266
batch start
#iterations: 169
currently lose_sum: 98.6417915225029
time_elpased: 2.19
batch start
#iterations: 170
currently lose_sum: 98.69164061546326
time_elpased: 2.269
batch start
#iterations: 171
currently lose_sum: 98.72241365909576
time_elpased: 2.382
batch start
#iterations: 172
currently lose_sum: 98.67509829998016
time_elpased: 2.344
batch start
#iterations: 173
currently lose_sum: 98.66103482246399
time_elpased: 2.437
batch start
#iterations: 174
currently lose_sum: 98.24345630407333
time_elpased: 2.128
batch start
#iterations: 175
currently lose_sum: 98.50888955593109
time_elpased: 2.24
batch start
#iterations: 176
currently lose_sum: 98.52528440952301
time_elpased: 2.307
batch start
#iterations: 177
currently lose_sum: 98.57460355758667
time_elpased: 2.295
batch start
#iterations: 178
currently lose_sum: 98.58983421325684
time_elpased: 2.315
batch start
#iterations: 179
currently lose_sum: 98.56947356462479
time_elpased: 2.301
start validation test
0.632371134021
0.657142857143
0.556241638366
0.602496934567
0.632504791092
63.535
batch start
#iterations: 180
currently lose_sum: 98.6529535651207
time_elpased: 2.365
batch start
#iterations: 181
currently lose_sum: 98.5748890042305
time_elpased: 2.134
batch start
#iterations: 182
currently lose_sum: 98.55282753705978
time_elpased: 1.981
batch start
#iterations: 183
currently lose_sum: 98.53135412931442
time_elpased: 2.356
batch start
#iterations: 184
currently lose_sum: 98.67168980836868
time_elpased: 2.386
batch start
#iterations: 185
currently lose_sum: 98.41514307260513
time_elpased: 2.479
batch start
#iterations: 186
currently lose_sum: 98.68815237283707
time_elpased: 2.408
batch start
#iterations: 187
currently lose_sum: 98.54200732707977
time_elpased: 2.11
batch start
#iterations: 188
currently lose_sum: 98.5481538772583
time_elpased: 2.281
batch start
#iterations: 189
currently lose_sum: 98.71800309419632
time_elpased: 2.143
batch start
#iterations: 190
currently lose_sum: 98.53002887964249
time_elpased: 2.171
batch start
#iterations: 191
currently lose_sum: 98.46230274438858
time_elpased: 2.214
batch start
#iterations: 192
currently lose_sum: 98.5111476778984
time_elpased: 1.984
batch start
#iterations: 193
currently lose_sum: 98.52008974552155
time_elpased: 2.372
batch start
#iterations: 194
currently lose_sum: 98.38101553916931
time_elpased: 2.52
batch start
#iterations: 195
currently lose_sum: 98.6661434173584
time_elpased: 2.394
batch start
#iterations: 196
currently lose_sum: 98.43645453453064
time_elpased: 2.465
batch start
#iterations: 197
currently lose_sum: 98.59190768003464
time_elpased: 2.349
batch start
#iterations: 198
currently lose_sum: 98.60652619600296
time_elpased: 2.209
batch start
#iterations: 199
currently lose_sum: 98.47334289550781
time_elpased: 2.122
start validation test
0.640773195876
0.656885133592
0.592055161058
0.622787550744
0.640858727901
63.431
batch start
#iterations: 200
currently lose_sum: 98.60188013315201
time_elpased: 2.41
batch start
#iterations: 201
currently lose_sum: 98.57656264305115
time_elpased: 2.013
batch start
#iterations: 202
currently lose_sum: 98.39036220312119
time_elpased: 2.317
batch start
#iterations: 203
currently lose_sum: 98.5417657494545
time_elpased: 2.253
batch start
#iterations: 204
currently lose_sum: 98.49946582317352
time_elpased: 1.943
batch start
#iterations: 205
currently lose_sum: 98.4083559513092
time_elpased: 2.284
batch start
#iterations: 206
currently lose_sum: 98.43221259117126
time_elpased: 2.268
batch start
#iterations: 207
currently lose_sum: 98.53791922330856
time_elpased: 2.248
batch start
#iterations: 208
currently lose_sum: 98.47161692380905
time_elpased: 2.338
batch start
#iterations: 209
currently lose_sum: 98.3717560172081
time_elpased: 2.329
batch start
#iterations: 210
currently lose_sum: 98.22976666688919
time_elpased: 2.327
batch start
#iterations: 211
currently lose_sum: 98.68836116790771
time_elpased: 2.319
batch start
#iterations: 212
currently lose_sum: 98.73011606931686
time_elpased: 2.373
batch start
#iterations: 213
currently lose_sum: 98.60752296447754
time_elpased: 2.448
batch start
#iterations: 214
currently lose_sum: 98.51430594921112
time_elpased: 2.307
batch start
#iterations: 215
currently lose_sum: 98.54879480600357
time_elpased: 2.4
batch start
#iterations: 216
currently lose_sum: 98.544793009758
time_elpased: 2.38
batch start
#iterations: 217
currently lose_sum: 98.44094252586365
time_elpased: 2.339
batch start
#iterations: 218
currently lose_sum: 98.32034081220627
time_elpased: 2.328
batch start
#iterations: 219
currently lose_sum: 98.71034014225006
time_elpased: 2.054
start validation test
0.633711340206
0.665315942763
0.540701862715
0.596570909504
0.63387463269
63.536
batch start
#iterations: 220
currently lose_sum: 98.48322010040283
time_elpased: 2.393
batch start
#iterations: 221
currently lose_sum: 98.72200268507004
time_elpased: 2.267
batch start
#iterations: 222
currently lose_sum: 98.35487502813339
time_elpased: 1.984
batch start
#iterations: 223
currently lose_sum: 98.38023149967194
time_elpased: 2.346
batch start
#iterations: 224
currently lose_sum: 98.38538944721222
time_elpased: 2.295
batch start
#iterations: 225
currently lose_sum: 98.70785844326019
time_elpased: 2.342
batch start
#iterations: 226
currently lose_sum: 98.36812418699265
time_elpased: 2.267
batch start
#iterations: 227
currently lose_sum: 98.56923234462738
time_elpased: 2.137
batch start
#iterations: 228
currently lose_sum: 98.70158988237381
time_elpased: 2.329
batch start
#iterations: 229
currently lose_sum: 98.40747559070587
time_elpased: 2.21
batch start
#iterations: 230
currently lose_sum: 98.574074447155
time_elpased: 2.352
batch start
#iterations: 231
currently lose_sum: 98.19281309843063
time_elpased: 2.15
batch start
#iterations: 232
currently lose_sum: 98.49004691839218
time_elpased: 2.205
batch start
#iterations: 233
currently lose_sum: 98.63059961795807
time_elpased: 2.42
batch start
#iterations: 234
currently lose_sum: 98.67578309774399
time_elpased: 2.333
batch start
#iterations: 235
currently lose_sum: 98.47331547737122
time_elpased: 2.276
batch start
#iterations: 236
currently lose_sum: 98.4786486029625
time_elpased: 2.307
batch start
#iterations: 237
currently lose_sum: 98.7227777838707
time_elpased: 2.368
batch start
#iterations: 238
currently lose_sum: 98.63387352228165
time_elpased: 2.247
batch start
#iterations: 239
currently lose_sum: 98.63263362646103
time_elpased: 2.453
start validation test
0.641494845361
0.661143523921
0.583101780385
0.619675179089
0.641597363393
63.465
batch start
#iterations: 240
currently lose_sum: 98.35409986972809
time_elpased: 2.369
batch start
#iterations: 241
currently lose_sum: 98.62099820375443
time_elpased: 2.277
batch start
#iterations: 242
currently lose_sum: 98.388074696064
time_elpased: 2.329
batch start
#iterations: 243
currently lose_sum: 98.43328523635864
time_elpased: 2.43
batch start
#iterations: 244
currently lose_sum: 98.54451954364777
time_elpased: 2.343
batch start
#iterations: 245
currently lose_sum: 98.67283684015274
time_elpased: 2.123
batch start
#iterations: 246
currently lose_sum: 98.50744593143463
time_elpased: 2.323
batch start
#iterations: 247
currently lose_sum: 98.28777176141739
time_elpased: 2.367
batch start
#iterations: 248
currently lose_sum: 98.37233197689056
time_elpased: 2.389
batch start
#iterations: 249
currently lose_sum: 98.56769466400146
time_elpased: 1.835
batch start
#iterations: 250
currently lose_sum: 98.42696368694305
time_elpased: 2.227
batch start
#iterations: 251
currently lose_sum: 98.57050448656082
time_elpased: 2.32
batch start
#iterations: 252
currently lose_sum: 98.47126150131226
time_elpased: 2.297
batch start
#iterations: 253
currently lose_sum: 98.25990307331085
time_elpased: 2.318
batch start
#iterations: 254
currently lose_sum: 98.50529778003693
time_elpased: 2.284
batch start
#iterations: 255
currently lose_sum: 98.43810796737671
time_elpased: 2.306
batch start
#iterations: 256
currently lose_sum: 98.15611040592194
time_elpased: 2.349
batch start
#iterations: 257
currently lose_sum: 98.54284000396729
time_elpased: 2.39
batch start
#iterations: 258
currently lose_sum: 98.59087842702866
time_elpased: 2.335
batch start
#iterations: 259
currently lose_sum: 98.32874244451523
time_elpased: 2.557
start validation test
0.638195876289
0.671803362201
0.542863023567
0.600489498549
0.638363247816
63.335
batch start
#iterations: 260
currently lose_sum: 98.46377938985825
time_elpased: 2.082
batch start
#iterations: 261
currently lose_sum: 98.24132096767426
time_elpased: 2.17
batch start
#iterations: 262
currently lose_sum: 98.4318265914917
time_elpased: 2.418
batch start
#iterations: 263
currently lose_sum: 98.5717122554779
time_elpased: 2.36
batch start
#iterations: 264
currently lose_sum: 98.35214638710022
time_elpased: 2.459
batch start
#iterations: 265
currently lose_sum: 98.47221666574478
time_elpased: 1.993
batch start
#iterations: 266
currently lose_sum: 98.49755811691284
time_elpased: 2.261
batch start
#iterations: 267
currently lose_sum: 98.3753314614296
time_elpased: 2.209
batch start
#iterations: 268
currently lose_sum: 98.50898641347885
time_elpased: 2.137
batch start
#iterations: 269
currently lose_sum: 98.46302944421768
time_elpased: 2.194
batch start
#iterations: 270
currently lose_sum: 98.41413056850433
time_elpased: 2.269
batch start
#iterations: 271
currently lose_sum: 98.68628966808319
time_elpased: 2.406
batch start
#iterations: 272
currently lose_sum: 98.34688174724579
time_elpased: 2.415
batch start
#iterations: 273
currently lose_sum: 98.43786120414734
time_elpased: 2.037
batch start
#iterations: 274
currently lose_sum: 98.59430581331253
time_elpased: 2.294
batch start
#iterations: 275
currently lose_sum: 98.60110485553741
time_elpased: 2.334
batch start
#iterations: 276
currently lose_sum: 98.44009244441986
time_elpased: 2.255
batch start
#iterations: 277
currently lose_sum: 98.46101367473602
time_elpased: 1.869
batch start
#iterations: 278
currently lose_sum: 98.602199614048
time_elpased: 2.108
batch start
#iterations: 279
currently lose_sum: 98.44732469320297
time_elpased: 2.401
start validation test
0.646030927835
0.668240850059
0.582484305856
0.622422609556
0.646142493731
63.344
batch start
#iterations: 280
currently lose_sum: 98.52382582426071
time_elpased: 2.41
batch start
#iterations: 281
currently lose_sum: 98.32634800672531
time_elpased: 2.343
batch start
#iterations: 282
currently lose_sum: 98.5361442565918
time_elpased: 2.422
batch start
#iterations: 283
currently lose_sum: 98.46631759405136
time_elpased: 2.326
batch start
#iterations: 284
currently lose_sum: 98.55169367790222
time_elpased: 2.223
batch start
#iterations: 285
currently lose_sum: 98.403753221035
time_elpased: 2.404
batch start
#iterations: 286
currently lose_sum: 98.19396817684174
time_elpased: 2.51
batch start
#iterations: 287
currently lose_sum: 98.2177003622055
time_elpased: 2.394
batch start
#iterations: 288
currently lose_sum: 98.54447388648987
time_elpased: 2.061
batch start
#iterations: 289
currently lose_sum: 98.37605148553848
time_elpased: 2.214
batch start
#iterations: 290
currently lose_sum: 98.32850855588913
time_elpased: 2.316
batch start
#iterations: 291
currently lose_sum: 98.54310780763626
time_elpased: 2.289
batch start
#iterations: 292
currently lose_sum: 98.38211196660995
time_elpased: 2.335
batch start
#iterations: 293
currently lose_sum: 98.68899041414261
time_elpased: 2.514
batch start
#iterations: 294
currently lose_sum: 98.67213064432144
time_elpased: 2.316
batch start
#iterations: 295
currently lose_sum: 98.47800850868225
time_elpased: 2.591
batch start
#iterations: 296
currently lose_sum: 98.44929265975952
time_elpased: 2.304
batch start
#iterations: 297
currently lose_sum: 98.313261449337
time_elpased: 2.232
batch start
#iterations: 298
currently lose_sum: 98.45479375123978
time_elpased: 2.025
batch start
#iterations: 299
currently lose_sum: 98.3845294713974
time_elpased: 2.399
start validation test
0.645257731959
0.66709890369
0.582381393434
0.621868131868
0.645368121069
63.230
batch start
#iterations: 300
currently lose_sum: 98.37884777784348
time_elpased: 2.313
batch start
#iterations: 301
currently lose_sum: 98.65628296136856
time_elpased: 2.306
batch start
#iterations: 302
currently lose_sum: 98.39532136917114
time_elpased: 2.327
batch start
#iterations: 303
currently lose_sum: 98.46243679523468
time_elpased: 2.355
batch start
#iterations: 304
currently lose_sum: 98.60795438289642
time_elpased: 2.187
batch start
#iterations: 305
currently lose_sum: 98.47016906738281
time_elpased: 2.105
batch start
#iterations: 306
currently lose_sum: 98.51605296134949
time_elpased: 2.317
batch start
#iterations: 307
currently lose_sum: 98.50171375274658
time_elpased: 2.328
batch start
#iterations: 308
currently lose_sum: 98.18642061948776
time_elpased: 2.328
batch start
#iterations: 309
currently lose_sum: 98.3028022646904
time_elpased: 2.332
batch start
#iterations: 310
currently lose_sum: 98.73482811450958
time_elpased: 2.166
batch start
#iterations: 311
currently lose_sum: 98.44784545898438
time_elpased: 2.452
batch start
#iterations: 312
currently lose_sum: 98.27788436412811
time_elpased: 2.357
batch start
#iterations: 313
currently lose_sum: 98.52935737371445
time_elpased: 2.387
batch start
#iterations: 314
currently lose_sum: 98.38845926523209
time_elpased: 2.395
batch start
#iterations: 315
currently lose_sum: 98.43466782569885
time_elpased: 2.129
batch start
#iterations: 316
currently lose_sum: 98.36634403467178
time_elpased: 2.286
batch start
#iterations: 317
currently lose_sum: 98.23084288835526
time_elpased: 2.179
batch start
#iterations: 318
currently lose_sum: 98.660877764225
time_elpased: 1.931
batch start
#iterations: 319
currently lose_sum: 98.41054028272629
time_elpased: 2.245
start validation test
0.644845360825
0.666040173852
0.583513430071
0.622051563357
0.644953038489
63.307
batch start
#iterations: 320
currently lose_sum: 98.1539197564125
time_elpased: 2.3
batch start
#iterations: 321
currently lose_sum: 98.52846306562424
time_elpased: 2.309
batch start
#iterations: 322
currently lose_sum: 98.5613529086113
time_elpased: 2.334
batch start
#iterations: 323
currently lose_sum: 98.5570473074913
time_elpased: 2.267
batch start
#iterations: 324
currently lose_sum: 98.42225670814514
time_elpased: 2.323
batch start
#iterations: 325
currently lose_sum: 98.71944326162338
time_elpased: 1.952
batch start
#iterations: 326
currently lose_sum: 98.41914623975754
time_elpased: 2.239
batch start
#iterations: 327
currently lose_sum: 98.53164058923721
time_elpased: 2.256
batch start
#iterations: 328
currently lose_sum: 98.59667325019836
time_elpased: 2.155
batch start
#iterations: 329
currently lose_sum: 98.20096528530121
time_elpased: 2.38
batch start
#iterations: 330
currently lose_sum: 98.50033050775528
time_elpased: 2.317
batch start
#iterations: 331
currently lose_sum: 98.5411547422409
time_elpased: 2.414
batch start
#iterations: 332
currently lose_sum: 98.43116670846939
time_elpased: 2.09
batch start
#iterations: 333
currently lose_sum: 98.35615175962448
time_elpased: 2.183
batch start
#iterations: 334
currently lose_sum: 98.1300642490387
time_elpased: 2.371
batch start
#iterations: 335
currently lose_sum: 98.53143709897995
time_elpased: 2.374
batch start
#iterations: 336
currently lose_sum: 98.16128647327423
time_elpased: 2.421
batch start
#iterations: 337
currently lose_sum: 98.63700658082962
time_elpased: 2.28
batch start
#iterations: 338
currently lose_sum: 98.43345731496811
time_elpased: 2.13
batch start
#iterations: 339
currently lose_sum: 98.52658301591873
time_elpased: 2.407
start validation test
0.643298969072
0.671279853031
0.564062982402
0.613018678
0.643438080068
63.332
batch start
#iterations: 340
currently lose_sum: 98.43033438920975
time_elpased: 2.185
batch start
#iterations: 341
currently lose_sum: 98.44416534900665
time_elpased: 2.298
batch start
#iterations: 342
currently lose_sum: 98.4852095246315
time_elpased: 2.306
batch start
#iterations: 343
currently lose_sum: 98.32112461328506
time_elpased: 2.311
batch start
#iterations: 344
currently lose_sum: 98.2191903591156
time_elpased: 2.311
batch start
#iterations: 345
currently lose_sum: 98.4842255115509
time_elpased: 2.313
batch start
#iterations: 346
currently lose_sum: 98.26088392734528
time_elpased: 2.307
batch start
#iterations: 347
currently lose_sum: 98.39272546768188
time_elpased: 2.293
batch start
#iterations: 348
currently lose_sum: 98.37219786643982
time_elpased: 2.269
batch start
#iterations: 349
currently lose_sum: 98.63630431890488
time_elpased: 2.303
batch start
#iterations: 350
currently lose_sum: 98.25895422697067
time_elpased: 2.296
batch start
#iterations: 351
currently lose_sum: 98.30635678768158
time_elpased: 2.319
batch start
#iterations: 352
currently lose_sum: 98.47923040390015
time_elpased: 2.322
batch start
#iterations: 353
currently lose_sum: 98.4358834028244
time_elpased: 2.425
batch start
#iterations: 354
currently lose_sum: 98.70754289627075
time_elpased: 2.433
batch start
#iterations: 355
currently lose_sum: 98.247802734375
time_elpased: 2.35
batch start
#iterations: 356
currently lose_sum: 98.41080093383789
time_elpased: 2.331
batch start
#iterations: 357
currently lose_sum: 98.51016730070114
time_elpased: 2.336
batch start
#iterations: 358
currently lose_sum: 98.53184390068054
time_elpased: 2.317
batch start
#iterations: 359
currently lose_sum: 98.41493594646454
time_elpased: 2.395
start validation test
0.64381443299
0.671514114628
0.565503756303
0.613966480447
0.643951919461
63.271
batch start
#iterations: 360
currently lose_sum: 98.6868833899498
time_elpased: 2.226
batch start
#iterations: 361
currently lose_sum: 98.62510979175568
time_elpased: 2.407
batch start
#iterations: 362
currently lose_sum: 98.75264871120453
time_elpased: 2.295
batch start
#iterations: 363
currently lose_sum: 98.3626457452774
time_elpased: 2.168
batch start
#iterations: 364
currently lose_sum: 98.1492971777916
time_elpased: 2.201
batch start
#iterations: 365
currently lose_sum: 98.54138207435608
time_elpased: 2.113
batch start
#iterations: 366
currently lose_sum: 98.25257009267807
time_elpased: 2.312
batch start
#iterations: 367
currently lose_sum: 98.51865303516388
time_elpased: 2.219
batch start
#iterations: 368
currently lose_sum: 98.5734561085701
time_elpased: 2.367
batch start
#iterations: 369
currently lose_sum: 98.61519056558609
time_elpased: 2.383
batch start
#iterations: 370
currently lose_sum: 98.46349358558655
time_elpased: 2.334
batch start
#iterations: 371
currently lose_sum: 98.41841435432434
time_elpased: 2.256
batch start
#iterations: 372
currently lose_sum: 98.29389709234238
time_elpased: 2.302
batch start
#iterations: 373
currently lose_sum: 98.35166144371033
time_elpased: 2.301
batch start
#iterations: 374
currently lose_sum: 98.55412340164185
time_elpased: 2.27
batch start
#iterations: 375
currently lose_sum: 98.1348465681076
time_elpased: 2.307
batch start
#iterations: 376
currently lose_sum: 98.54262489080429
time_elpased: 2.308
batch start
#iterations: 377
currently lose_sum: 98.53533846139908
time_elpased: 2.03
batch start
#iterations: 378
currently lose_sum: 98.43396145105362
time_elpased: 2.399
batch start
#iterations: 379
currently lose_sum: 98.23369151353836
time_elpased: 2.376
start validation test
0.644381443299
0.663116462144
0.58948235052
0.624135113048
0.644477827124
63.162
batch start
#iterations: 380
currently lose_sum: 98.73425728082657
time_elpased: 2.326
batch start
#iterations: 381
currently lose_sum: 98.34256833791733
time_elpased: 2.456
batch start
#iterations: 382
currently lose_sum: 98.4953578710556
time_elpased: 2.352
batch start
#iterations: 383
currently lose_sum: 98.48865878582001
time_elpased: 2.229
batch start
#iterations: 384
currently lose_sum: 98.68199169635773
time_elpased: 2.455
batch start
#iterations: 385
currently lose_sum: 98.60807567834854
time_elpased: 2.413
batch start
#iterations: 386
currently lose_sum: 98.46034628152847
time_elpased: 2.268
batch start
#iterations: 387
currently lose_sum: 98.7680151462555
time_elpased: 2.186
batch start
#iterations: 388
currently lose_sum: 98.44264614582062
time_elpased: 2.442
batch start
#iterations: 389
currently lose_sum: 98.48172920942307
time_elpased: 2.4
batch start
#iterations: 390
currently lose_sum: 98.3783313035965
time_elpased: 2.375
batch start
#iterations: 391
currently lose_sum: 98.49260705709457
time_elpased: 1.976
batch start
#iterations: 392
currently lose_sum: 98.3651756644249
time_elpased: 2.349
batch start
#iterations: 393
currently lose_sum: 98.48866039514542
time_elpased: 2.403
batch start
#iterations: 394
currently lose_sum: 98.52203100919724
time_elpased: 2.289
batch start
#iterations: 395
currently lose_sum: 98.34608590602875
time_elpased: 2.074
batch start
#iterations: 396
currently lose_sum: 98.42412012815475
time_elpased: 2.062
batch start
#iterations: 397
currently lose_sum: 98.34997415542603
time_elpased: 2.304
batch start
#iterations: 398
currently lose_sum: 98.35218250751495
time_elpased: 2.465
batch start
#iterations: 399
currently lose_sum: 98.46137255430222
time_elpased: 2.308
start validation test
0.637989690722
0.670939086294
0.544097972625
0.600897880321
0.638154532115
63.302
acc: 0.647
pre: 0.666
rec: 0.591
F1: 0.626
auc: 0.698
