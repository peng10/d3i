start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.3812181353569
time_elpased: 2.519
batch start
#iterations: 1
currently lose_sum: 100.17004227638245
time_elpased: 2.341
batch start
#iterations: 2
currently lose_sum: 99.98472774028778
time_elpased: 2.506
batch start
#iterations: 3
currently lose_sum: 99.70129293203354
time_elpased: 2.472
batch start
#iterations: 4
currently lose_sum: 99.65703052282333
time_elpased: 2.313
batch start
#iterations: 5
currently lose_sum: 99.72901582717896
time_elpased: 2.477
batch start
#iterations: 6
currently lose_sum: 99.48379212617874
time_elpased: 2.507
batch start
#iterations: 7
currently lose_sum: 99.35235899686813
time_elpased: 2.469
batch start
#iterations: 8
currently lose_sum: 99.39282220602036
time_elpased: 2.495
batch start
#iterations: 9
currently lose_sum: 99.32588857412338
time_elpased: 2.473
batch start
#iterations: 10
currently lose_sum: 99.19387573003769
time_elpased: 2.312
batch start
#iterations: 11
currently lose_sum: 99.3159744143486
time_elpased: 2.577
batch start
#iterations: 12
currently lose_sum: 99.43813794851303
time_elpased: 2.271
batch start
#iterations: 13
currently lose_sum: 99.11929905414581
time_elpased: 2.374
batch start
#iterations: 14
currently lose_sum: 99.020951628685
time_elpased: 2.584
batch start
#iterations: 15
currently lose_sum: 99.06481528282166
time_elpased: 2.41
batch start
#iterations: 16
currently lose_sum: 99.00314420461655
time_elpased: 2.481
batch start
#iterations: 17
currently lose_sum: 98.74366807937622
time_elpased: 2.205
batch start
#iterations: 18
currently lose_sum: 98.88454169034958
time_elpased: 2.459
batch start
#iterations: 19
currently lose_sum: 98.66334241628647
time_elpased: 2.001
start validation test
0.624226804124
0.633424958769
0.59287846043
0.612481394854
0.624281840976
64.000
batch start
#iterations: 20
currently lose_sum: 98.67524802684784
time_elpased: 2.682
batch start
#iterations: 21
currently lose_sum: 98.61361652612686
time_elpased: 2.669
batch start
#iterations: 22
currently lose_sum: 98.29324895143509
time_elpased: 2.507
batch start
#iterations: 23
currently lose_sum: 98.12041568756104
time_elpased: 2.467
batch start
#iterations: 24
currently lose_sum: 97.8493178486824
time_elpased: 2.276
batch start
#iterations: 25
currently lose_sum: 98.00484162569046
time_elpased: 2.067
batch start
#iterations: 26
currently lose_sum: 97.6989364027977
time_elpased: 2.223
batch start
#iterations: 27
currently lose_sum: 97.44960963726044
time_elpased: 2.453
batch start
#iterations: 28
currently lose_sum: 97.27012544870377
time_elpased: 2.573
batch start
#iterations: 29
currently lose_sum: 97.5124043226242
time_elpased: 2.474
batch start
#iterations: 30
currently lose_sum: 96.86099982261658
time_elpased: 2.437
batch start
#iterations: 31
currently lose_sum: 96.61431670188904
time_elpased: 2.494
batch start
#iterations: 32
currently lose_sum: 96.46511149406433
time_elpased: 2.529
batch start
#iterations: 33
currently lose_sum: 96.90830141305923
time_elpased: 2.438
batch start
#iterations: 34
currently lose_sum: 96.32000195980072
time_elpased: 2.442
batch start
#iterations: 35
currently lose_sum: 96.50764983892441
time_elpased: 2.446
batch start
#iterations: 36
currently lose_sum: 96.00863361358643
time_elpased: 2.57
batch start
#iterations: 37
currently lose_sum: 96.34068363904953
time_elpased: 2.522
batch start
#iterations: 38
currently lose_sum: 96.1264420747757
time_elpased: 2.456
batch start
#iterations: 39
currently lose_sum: 96.08446317911148
time_elpased: 2.482
start validation test
0.646649484536
0.625087412587
0.735926726356
0.67599376093
0.646492744568
61.765
batch start
#iterations: 40
currently lose_sum: 95.78290259838104
time_elpased: 2.463
batch start
#iterations: 41
currently lose_sum: 95.88182944059372
time_elpased: 2.455
batch start
#iterations: 42
currently lose_sum: 95.9279454946518
time_elpased: 2.375
batch start
#iterations: 43
currently lose_sum: 95.48243123292923
time_elpased: 2.54
batch start
#iterations: 44
currently lose_sum: 95.52706998586655
time_elpased: 2.426
batch start
#iterations: 45
currently lose_sum: 95.69712233543396
time_elpased: 2.24
batch start
#iterations: 46
currently lose_sum: 95.22023630142212
time_elpased: 2.061
batch start
#iterations: 47
currently lose_sum: 95.18312311172485
time_elpased: 2.396
batch start
#iterations: 48
currently lose_sum: 95.48592352867126
time_elpased: 2.583
batch start
#iterations: 49
currently lose_sum: 95.24487614631653
time_elpased: 2.608
batch start
#iterations: 50
currently lose_sum: 95.00656217336655
time_elpased: 2.021
batch start
#iterations: 51
currently lose_sum: 94.77538061141968
time_elpased: 2.263
batch start
#iterations: 52
currently lose_sum: 95.01737463474274
time_elpased: 2.542
batch start
#iterations: 53
currently lose_sum: 95.17571872472763
time_elpased: 2.522
batch start
#iterations: 54
currently lose_sum: 94.7384581565857
time_elpased: 2.296
batch start
#iterations: 55
currently lose_sum: 94.66594755649567
time_elpased: 2.331
batch start
#iterations: 56
currently lose_sum: 94.94288951158524
time_elpased: 2.422
batch start
#iterations: 57
currently lose_sum: 94.91287517547607
time_elpased: 2.536
batch start
#iterations: 58
currently lose_sum: 94.71759551763535
time_elpased: 2.536
batch start
#iterations: 59
currently lose_sum: 94.9987758398056
time_elpased: 2.466
start validation test
0.647783505155
0.645892351275
0.656992899043
0.651395336973
0.647767336643
61.091
batch start
#iterations: 60
currently lose_sum: 94.52710545063019
time_elpased: 2.409
batch start
#iterations: 61
currently lose_sum: 94.53137719631195
time_elpased: 2.331
batch start
#iterations: 62
currently lose_sum: 94.7422388792038
time_elpased: 2.211
batch start
#iterations: 63
currently lose_sum: 94.67474508285522
time_elpased: 2.389
batch start
#iterations: 64
currently lose_sum: 94.79739844799042
time_elpased: 2.554
batch start
#iterations: 65
currently lose_sum: 94.14016968011856
time_elpased: 2.549
batch start
#iterations: 66
currently lose_sum: 94.41375297307968
time_elpased: 2.443
batch start
#iterations: 67
currently lose_sum: 94.16924548149109
time_elpased: 2.508
batch start
#iterations: 68
currently lose_sum: 93.9478811621666
time_elpased: 2.456
batch start
#iterations: 69
currently lose_sum: 94.17772018909454
time_elpased: 2.533
batch start
#iterations: 70
currently lose_sum: 93.69437164068222
time_elpased: 2.207
batch start
#iterations: 71
currently lose_sum: 94.2946326136589
time_elpased: 2.389
batch start
#iterations: 72
currently lose_sum: 94.09891891479492
time_elpased: 2.482
batch start
#iterations: 73
currently lose_sum: 94.16235154867172
time_elpased: 2.716
batch start
#iterations: 74
currently lose_sum: 94.06682646274567
time_elpased: 2.533
batch start
#iterations: 75
currently lose_sum: 93.78477984666824
time_elpased: 2.213
batch start
#iterations: 76
currently lose_sum: 93.4934875369072
time_elpased: 2.404
batch start
#iterations: 77
currently lose_sum: 93.46848487854004
time_elpased: 2.488
batch start
#iterations: 78
currently lose_sum: 93.95727914571762
time_elpased: 2.54
batch start
#iterations: 79
currently lose_sum: 93.96310359239578
time_elpased: 2.642
start validation test
0.63793814433
0.630918813807
0.667798703303
0.648835116488
0.637885719513
61.397
batch start
#iterations: 80
currently lose_sum: 93.97526067495346
time_elpased: 2.504
batch start
#iterations: 81
currently lose_sum: 93.48247247934341
time_elpased: 2.529
batch start
#iterations: 82
currently lose_sum: 94.1282868385315
time_elpased: 2.495
batch start
#iterations: 83
currently lose_sum: 93.63966929912567
time_elpased: 2.308
batch start
#iterations: 84
currently lose_sum: 93.9578105211258
time_elpased: 2.497
batch start
#iterations: 85
currently lose_sum: 93.80772000551224
time_elpased: 2.512
batch start
#iterations: 86
currently lose_sum: 93.4591771364212
time_elpased: 2.392
batch start
#iterations: 87
currently lose_sum: 93.69605976343155
time_elpased: 2.522
batch start
#iterations: 88
currently lose_sum: 93.61277055740356
time_elpased: 2.616
batch start
#iterations: 89
currently lose_sum: 93.80961573123932
time_elpased: 2.595
batch start
#iterations: 90
currently lose_sum: 93.45160681009293
time_elpased: 2.417
batch start
#iterations: 91
currently lose_sum: 93.55618065595627
time_elpased: 2.442
batch start
#iterations: 92
currently lose_sum: 93.31531339883804
time_elpased: 2.088
batch start
#iterations: 93
currently lose_sum: 93.53905779123306
time_elpased: 2.435
batch start
#iterations: 94
currently lose_sum: 93.37027168273926
time_elpased: 2.521
batch start
#iterations: 95
currently lose_sum: 92.83855891227722
time_elpased: 2.464
batch start
#iterations: 96
currently lose_sum: 93.2550430893898
time_elpased: 2.43
batch start
#iterations: 97
currently lose_sum: 93.08115750551224
time_elpased: 2.43
batch start
#iterations: 98
currently lose_sum: 93.35988563299179
time_elpased: 2.149
batch start
#iterations: 99
currently lose_sum: 93.31325417757034
time_elpased: 2.431
start validation test
0.654072164948
0.645442229534
0.6864258516
0.665303476136
0.654015363061
60.188
batch start
#iterations: 100
currently lose_sum: 93.4732174873352
time_elpased: 2.525
batch start
#iterations: 101
currently lose_sum: 93.16903907060623
time_elpased: 2.474
batch start
#iterations: 102
currently lose_sum: 93.23893654346466
time_elpased: 2.496
batch start
#iterations: 103
currently lose_sum: 93.20479661226273
time_elpased: 2.558
batch start
#iterations: 104
currently lose_sum: 93.39085394144058
time_elpased: 2.313
batch start
#iterations: 105
currently lose_sum: 92.6729102730751
time_elpased: 2.428
batch start
#iterations: 106
currently lose_sum: 93.08952105045319
time_elpased: 2.435
batch start
#iterations: 107
currently lose_sum: 92.90910786390305
time_elpased: 2.349
batch start
#iterations: 108
currently lose_sum: 92.75332862138748
time_elpased: 2.586
batch start
#iterations: 109
currently lose_sum: 93.07654255628586
time_elpased: 2.505
batch start
#iterations: 110
currently lose_sum: 93.16372662782669
time_elpased: 2.58
batch start
#iterations: 111
currently lose_sum: 92.73024886846542
time_elpased: 2.527
batch start
#iterations: 112
currently lose_sum: 92.58955085277557
time_elpased: 2.534
batch start
#iterations: 113
currently lose_sum: 92.51809400320053
time_elpased: 2.686
batch start
#iterations: 114
currently lose_sum: 92.60108762979507
time_elpased: 2.288
batch start
#iterations: 115
currently lose_sum: 93.10457050800323
time_elpased: 2.59
batch start
#iterations: 116
currently lose_sum: 92.34707117080688
time_elpased: 2.423
batch start
#iterations: 117
currently lose_sum: 92.41746783256531
time_elpased: 2.55
batch start
#iterations: 118
currently lose_sum: 92.16850024461746
time_elpased: 2.595
batch start
#iterations: 119
currently lose_sum: 92.3603343963623
time_elpased: 2.534
start validation test
0.646030927835
0.645854657114
0.64937737985
0.647611227998
0.646025052622
60.359
batch start
#iterations: 120
currently lose_sum: 92.14036095142365
time_elpased: 2.505
batch start
#iterations: 121
currently lose_sum: 91.97193330526352
time_elpased: 2.469
batch start
#iterations: 122
currently lose_sum: 92.28285729885101
time_elpased: 2.421
batch start
#iterations: 123
currently lose_sum: 92.35091251134872
time_elpased: 2.517
batch start
#iterations: 124
currently lose_sum: 92.86572265625
time_elpased: 2.588
batch start
#iterations: 125
currently lose_sum: 92.32070523500443
time_elpased: 2.473
batch start
#iterations: 126
currently lose_sum: 92.69205403327942
time_elpased: 2.449
batch start
#iterations: 127
currently lose_sum: 92.58002597093582
time_elpased: 2.49
batch start
#iterations: 128
currently lose_sum: 92.22381663322449
time_elpased: 2.554
batch start
#iterations: 129
currently lose_sum: 92.5936005115509
time_elpased: 2.476
batch start
#iterations: 130
currently lose_sum: 92.03917461633682
time_elpased: 2.248
batch start
#iterations: 131
currently lose_sum: 92.46721583604813
time_elpased: 2.561
batch start
#iterations: 132
currently lose_sum: 91.63032960891724
time_elpased: 2.432
batch start
#iterations: 133
currently lose_sum: 92.09013390541077
time_elpased: 2.5
batch start
#iterations: 134
currently lose_sum: 92.10723674297333
time_elpased: 2.215
batch start
#iterations: 135
currently lose_sum: 92.31250756978989
time_elpased: 2.392
batch start
#iterations: 136
currently lose_sum: 92.0050927400589
time_elpased: 2.295
batch start
#iterations: 137
currently lose_sum: 92.04724895954132
time_elpased: 2.466
batch start
#iterations: 138
currently lose_sum: 91.88975483179092
time_elpased: 2.585
batch start
#iterations: 139
currently lose_sum: 91.7784184217453
time_elpased: 2.627
start validation test
0.646804123711
0.642070812258
0.666255016981
0.653939393939
0.646769974668
60.366
batch start
#iterations: 140
currently lose_sum: 91.88852608203888
time_elpased: 2.569
batch start
#iterations: 141
currently lose_sum: 91.89958882331848
time_elpased: 2.411
batch start
#iterations: 142
currently lose_sum: 91.49834513664246
time_elpased: 2.281
batch start
#iterations: 143
currently lose_sum: 92.08457869291306
time_elpased: 2.609
batch start
#iterations: 144
currently lose_sum: 91.69818896055222
time_elpased: 2.243
batch start
#iterations: 145
currently lose_sum: 91.71571832895279
time_elpased: 2.506
batch start
#iterations: 146
currently lose_sum: 91.80140942335129
time_elpased: 2.492
batch start
#iterations: 147
currently lose_sum: 91.89901340007782
time_elpased: 2.257
batch start
#iterations: 148
currently lose_sum: 91.37320619821548
time_elpased: 2.68
batch start
#iterations: 149
currently lose_sum: 91.66677695512772
time_elpased: 2.633
batch start
#iterations: 150
currently lose_sum: 91.85181367397308
time_elpased: 2.463
batch start
#iterations: 151
currently lose_sum: 91.4062871336937
time_elpased: 2.574
batch start
#iterations: 152
currently lose_sum: 91.59209388494492
time_elpased: 2.5
batch start
#iterations: 153
currently lose_sum: 91.6203002333641
time_elpased: 2.204
batch start
#iterations: 154
currently lose_sum: 91.15338629484177
time_elpased: 2.508
batch start
#iterations: 155
currently lose_sum: 91.21764695644379
time_elpased: 2.483
batch start
#iterations: 156
currently lose_sum: 91.86541801691055
time_elpased: 2.375
batch start
#iterations: 157
currently lose_sum: 91.1175257563591
time_elpased: 2.417
batch start
#iterations: 158
currently lose_sum: 91.34996539354324
time_elpased: 2.447
batch start
#iterations: 159
currently lose_sum: 91.07031065225601
time_elpased: 2.525
start validation test
0.642525773196
0.64414507772
0.639703612226
0.641916662364
0.642530727935
60.336
batch start
#iterations: 160
currently lose_sum: 91.24178504943848
time_elpased: 2.624
batch start
#iterations: 161
currently lose_sum: 91.3887488245964
time_elpased: 2.617
batch start
#iterations: 162
currently lose_sum: 91.39326453208923
time_elpased: 2.303
batch start
#iterations: 163
currently lose_sum: 91.30726164579391
time_elpased: 2.274
batch start
#iterations: 164
currently lose_sum: 90.56327199935913
time_elpased: 2.494
batch start
#iterations: 165
currently lose_sum: 90.75782960653305
time_elpased: 2.622
batch start
#iterations: 166
currently lose_sum: 91.14764493703842
time_elpased: 2.589
batch start
#iterations: 167
currently lose_sum: 91.27896589040756
time_elpased: 2.736
batch start
#iterations: 168
currently lose_sum: 90.94188892841339
time_elpased: 2.708
batch start
#iterations: 169
currently lose_sum: 90.94797843694687
time_elpased: 2.158
batch start
#iterations: 170
currently lose_sum: 91.52419465780258
time_elpased: 2.56
batch start
#iterations: 171
currently lose_sum: 91.21739917993546
time_elpased: 2.339
batch start
#iterations: 172
currently lose_sum: 91.66200089454651
time_elpased: 2.367
batch start
#iterations: 173
currently lose_sum: 90.78122639656067
time_elpased: 2.169
batch start
#iterations: 174
currently lose_sum: 90.83132183551788
time_elpased: 2.103
batch start
#iterations: 175
currently lose_sum: 90.64821314811707
time_elpased: 2.431
batch start
#iterations: 176
currently lose_sum: 90.33292841911316
time_elpased: 2.503
batch start
#iterations: 177
currently lose_sum: 90.56952100992203
time_elpased: 2.124
batch start
#iterations: 178
currently lose_sum: 90.75755172967911
time_elpased: 2.446
batch start
#iterations: 179
currently lose_sum: 90.40800958871841
time_elpased: 2.318
start validation test
0.650412371134
0.652499220617
0.646187094782
0.649327817994
0.650419789258
60.016
batch start
#iterations: 180
currently lose_sum: 90.4259849190712
time_elpased: 2.357
batch start
#iterations: 181
currently lose_sum: 91.02949744462967
time_elpased: 2.274
batch start
#iterations: 182
currently lose_sum: 90.41695594787598
time_elpased: 2.462
batch start
#iterations: 183
currently lose_sum: 90.37088948488235
time_elpased: 2.479
batch start
#iterations: 184
currently lose_sum: 90.95748996734619
time_elpased: 2.453
batch start
#iterations: 185
currently lose_sum: 90.32461076974869
time_elpased: 2.555
batch start
#iterations: 186
currently lose_sum: 90.91820138692856
time_elpased: 2.507
batch start
#iterations: 187
currently lose_sum: 90.37524950504303
time_elpased: 2.507
batch start
#iterations: 188
currently lose_sum: 90.66173589229584
time_elpased: 2.406
batch start
#iterations: 189
currently lose_sum: 90.81795781850815
time_elpased: 2.51
batch start
#iterations: 190
currently lose_sum: 90.34824460744858
time_elpased: 2.225
batch start
#iterations: 191
currently lose_sum: 90.458771109581
time_elpased: 2.333
batch start
#iterations: 192
currently lose_sum: 90.91077417135239
time_elpased: 2.419
batch start
#iterations: 193
currently lose_sum: 90.2289160490036
time_elpased: 2.47
batch start
#iterations: 194
currently lose_sum: 89.64669823646545
time_elpased: 2.529
batch start
#iterations: 195
currently lose_sum: 90.5561113357544
time_elpased: 2.16
batch start
#iterations: 196
currently lose_sum: 90.27250647544861
time_elpased: 2.508
batch start
#iterations: 197
currently lose_sum: 90.22166270017624
time_elpased: 2.644
batch start
#iterations: 198
currently lose_sum: 89.94965201616287
time_elpased: 2.424
batch start
#iterations: 199
currently lose_sum: 89.78232872486115
time_elpased: 2.514
start validation test
0.632680412371
0.637307896131
0.618812390656
0.627923976608
0.632704759822
60.897
batch start
#iterations: 200
currently lose_sum: 89.9243882894516
time_elpased: 2.611
batch start
#iterations: 201
currently lose_sum: 90.29056644439697
time_elpased: 2.559
batch start
#iterations: 202
currently lose_sum: 89.62060552835464
time_elpased: 2.632
batch start
#iterations: 203
currently lose_sum: 90.08430790901184
time_elpased: 2.398
batch start
#iterations: 204
currently lose_sum: 89.7629469037056
time_elpased: 2.527
batch start
#iterations: 205
currently lose_sum: 89.60240924358368
time_elpased: 2.539
batch start
#iterations: 206
currently lose_sum: 90.09721475839615
time_elpased: 2.557
batch start
#iterations: 207
currently lose_sum: 90.37244254350662
time_elpased: 2.732
batch start
#iterations: 208
currently lose_sum: 89.87317329645157
time_elpased: 2.573
batch start
#iterations: 209
currently lose_sum: 89.9255627989769
time_elpased: 2.432
batch start
#iterations: 210
currently lose_sum: 89.65072572231293
time_elpased: 2.542
batch start
#iterations: 211
currently lose_sum: 89.99464446306229
time_elpased: 2.557
batch start
#iterations: 212
currently lose_sum: 90.22444546222687
time_elpased: 2.322
batch start
#iterations: 213
currently lose_sum: 89.77015763521194
time_elpased: 2.139
batch start
#iterations: 214
currently lose_sum: 89.69921213388443
time_elpased: 2.452
batch start
#iterations: 215
currently lose_sum: 89.3021211028099
time_elpased: 2.48
batch start
#iterations: 216
currently lose_sum: 89.639941573143
time_elpased: 2.448
batch start
#iterations: 217
currently lose_sum: 89.82192814350128
time_elpased: 2.273
batch start
#iterations: 218
currently lose_sum: 89.2454217672348
time_elpased: 2.149
batch start
#iterations: 219
currently lose_sum: 89.76528543233871
time_elpased: 2.498
start validation test
0.634948453608
0.644668935983
0.604198826798
0.623778155546
0.635002439321
60.927
batch start
#iterations: 220
currently lose_sum: 89.7505555152893
time_elpased: 2.471
batch start
#iterations: 221
currently lose_sum: 89.68830227851868
time_elpased: 2.504
batch start
#iterations: 222
currently lose_sum: 88.90887087583542
time_elpased: 2.461
batch start
#iterations: 223
currently lose_sum: 89.4006872177124
time_elpased: 2.452
batch start
#iterations: 224
currently lose_sum: 89.16512352228165
time_elpased: 2.411
batch start
#iterations: 225
currently lose_sum: 89.96614062786102
time_elpased: 2.552
batch start
#iterations: 226
currently lose_sum: 89.45827966928482
time_elpased: 2.608
batch start
#iterations: 227
currently lose_sum: 89.34861749410629
time_elpased: 2.548
batch start
#iterations: 228
currently lose_sum: 89.23394548892975
time_elpased: 2.426
batch start
#iterations: 229
currently lose_sum: 89.21205109357834
time_elpased: 2.447
batch start
#iterations: 230
currently lose_sum: 89.29143404960632
time_elpased: 2.476
batch start
#iterations: 231
currently lose_sum: 88.96927696466446
time_elpased: 2.238
batch start
#iterations: 232
currently lose_sum: 89.62005257606506
time_elpased: 2.075
batch start
#iterations: 233
currently lose_sum: 89.26220226287842
time_elpased: 2.412
batch start
#iterations: 234
currently lose_sum: 89.2880727648735
time_elpased: 2.182
batch start
#iterations: 235
currently lose_sum: 88.63259148597717
time_elpased: 2.535
batch start
#iterations: 236
currently lose_sum: 89.1961595416069
time_elpased: 2.534
batch start
#iterations: 237
currently lose_sum: 89.06285136938095
time_elpased: 2.594
batch start
#iterations: 238
currently lose_sum: 89.16702038049698
time_elpased: 2.347
batch start
#iterations: 239
currently lose_sum: 89.13622897863388
time_elpased: 2.279
start validation test
0.626855670103
0.638621615574
0.587424102089
0.611953899759
0.626924898303
61.230
batch start
#iterations: 240
currently lose_sum: 88.26882416009903
time_elpased: 2.443
batch start
#iterations: 241
currently lose_sum: 89.18700331449509
time_elpased: 2.504
batch start
#iterations: 242
currently lose_sum: 88.71826839447021
time_elpased: 2.625
batch start
#iterations: 243
currently lose_sum: 88.89677059650421
time_elpased: 2.18
batch start
#iterations: 244
currently lose_sum: 89.09172034263611
time_elpased: 2.422
batch start
#iterations: 245
currently lose_sum: 88.83937078714371
time_elpased: 2.465
batch start
#iterations: 246
currently lose_sum: 89.23759406805038
time_elpased: 2.439
batch start
#iterations: 247
currently lose_sum: 88.21592158079147
time_elpased: 2.483
batch start
#iterations: 248
currently lose_sum: 88.44811809062958
time_elpased: 2.486
batch start
#iterations: 249
currently lose_sum: 88.65257775783539
time_elpased: 2.248
batch start
#iterations: 250
currently lose_sum: 88.44706124067307
time_elpased: 2.543
batch start
#iterations: 251
currently lose_sum: 88.64060038328171
time_elpased: 2.516
batch start
#iterations: 252
currently lose_sum: 88.64337795972824
time_elpased: 2.673
batch start
#iterations: 253
currently lose_sum: 88.15177446603775
time_elpased: 2.556
batch start
#iterations: 254
currently lose_sum: 88.8816893696785
time_elpased: 2.677
batch start
#iterations: 255
currently lose_sum: 88.65729707479477
time_elpased: 2.781
batch start
#iterations: 256
currently lose_sum: 88.11342895030975
time_elpased: 2.451
batch start
#iterations: 257
currently lose_sum: 88.38637459278107
time_elpased: 2.544
batch start
#iterations: 258
currently lose_sum: 88.87539339065552
time_elpased: 2.521
batch start
#iterations: 259
currently lose_sum: 88.44449281692505
time_elpased: 2.261
start validation test
0.636701030928
0.64153144554
0.622517237831
0.631881332915
0.636725932764
60.808
batch start
#iterations: 260
currently lose_sum: 88.65665709972382
time_elpased: 2.663
batch start
#iterations: 261
currently lose_sum: 87.99884742498398
time_elpased: 2.54
batch start
#iterations: 262
currently lose_sum: 88.53725296258926
time_elpased: 2.668
batch start
#iterations: 263
currently lose_sum: 88.94654095172882
time_elpased: 2.038
batch start
#iterations: 264
currently lose_sum: 87.93935739994049
time_elpased: 2.445
batch start
#iterations: 265
currently lose_sum: 88.21134346723557
time_elpased: 2.303
batch start
#iterations: 266
currently lose_sum: 88.04758697748184
time_elpased: 2.614
batch start
#iterations: 267
currently lose_sum: 87.87080240249634
time_elpased: 2.515
batch start
#iterations: 268
currently lose_sum: 88.12525290250778
time_elpased: 2.507
batch start
#iterations: 269
currently lose_sum: 87.99986100196838
time_elpased: 2.427
batch start
#iterations: 270
currently lose_sum: 88.41537356376648
time_elpased: 2.443
batch start
#iterations: 271
currently lose_sum: 88.17272561788559
time_elpased: 2.434
batch start
#iterations: 272
currently lose_sum: 87.60521310567856
time_elpased: 2.603
batch start
#iterations: 273
currently lose_sum: 87.87210595607758
time_elpased: 2.531
batch start
#iterations: 274
currently lose_sum: 88.17381101846695
time_elpased: 2.497
batch start
#iterations: 275
currently lose_sum: 88.45896506309509
time_elpased: 2.496
batch start
#iterations: 276
currently lose_sum: 87.81681138277054
time_elpased: 2.459
batch start
#iterations: 277
currently lose_sum: 87.6374945640564
time_elpased: 2.459
batch start
#iterations: 278
currently lose_sum: 87.58500236272812
time_elpased: 2.532
batch start
#iterations: 279
currently lose_sum: 87.86105740070343
time_elpased: 2.602
start validation test
0.630463917526
0.648450244698
0.57270762581
0.608229957921
0.630565317604
61.351
batch start
#iterations: 280
currently lose_sum: 88.15115422010422
time_elpased: 2.478
batch start
#iterations: 281
currently lose_sum: 87.41047215461731
time_elpased: 2.543
batch start
#iterations: 282
currently lose_sum: 87.6804826259613
time_elpased: 2.297
batch start
#iterations: 283
currently lose_sum: 87.72288358211517
time_elpased: 2.49
batch start
#iterations: 284
currently lose_sum: 87.28046602010727
time_elpased: 2.439
batch start
#iterations: 285
currently lose_sum: 87.49681609869003
time_elpased: 2.49
batch start
#iterations: 286
currently lose_sum: 87.84632390737534
time_elpased: 2.444
batch start
#iterations: 287
currently lose_sum: 87.39867997169495
time_elpased: 2.441
batch start
#iterations: 288
currently lose_sum: 87.19936269521713
time_elpased: 2.466
batch start
#iterations: 289
currently lose_sum: 87.33777099847794
time_elpased: 2.498
batch start
#iterations: 290
currently lose_sum: 87.50158554315567
time_elpased: 2.343
batch start
#iterations: 291
currently lose_sum: 87.79048013687134
time_elpased: 2.257
batch start
#iterations: 292
currently lose_sum: 87.15158993005753
time_elpased: 2.281
batch start
#iterations: 293
currently lose_sum: 87.62089085578918
time_elpased: 2.431
batch start
#iterations: 294
currently lose_sum: 87.42746841907501
time_elpased: 2.506
batch start
#iterations: 295
currently lose_sum: 87.92888271808624
time_elpased: 2.698
batch start
#iterations: 296
currently lose_sum: 87.51347994804382
time_elpased: 2.621
batch start
#iterations: 297
currently lose_sum: 87.45727682113647
time_elpased: 2.439
batch start
#iterations: 298
currently lose_sum: 87.51541835069656
time_elpased: 2.362
batch start
#iterations: 299
currently lose_sum: 87.09947782754898
time_elpased: 2.495
start validation test
0.631340206186
0.648661180016
0.575897910878
0.610117749673
0.631437543686
61.774
batch start
#iterations: 300
currently lose_sum: 87.29955720901489
time_elpased: 2.514
batch start
#iterations: 301
currently lose_sum: 87.39340806007385
time_elpased: 2.078
batch start
#iterations: 302
currently lose_sum: 86.83783847093582
time_elpased: 2.059
batch start
#iterations: 303
currently lose_sum: 87.61557108163834
time_elpased: 2.217
batch start
#iterations: 304
currently lose_sum: 87.76844495534897
time_elpased: 2.506
batch start
#iterations: 305
currently lose_sum: 86.77873027324677
time_elpased: 2.591
batch start
#iterations: 306
currently lose_sum: 86.3939801454544
time_elpased: 2.429
batch start
#iterations: 307
currently lose_sum: 87.04135447740555
time_elpased: 2.47
batch start
#iterations: 308
currently lose_sum: 86.54909020662308
time_elpased: 2.477
batch start
#iterations: 309
currently lose_sum: 86.92595225572586
time_elpased: 2.597
batch start
#iterations: 310
currently lose_sum: 87.0475977063179
time_elpased: 2.507
batch start
#iterations: 311
currently lose_sum: 86.91572499275208
time_elpased: 2.4
batch start
#iterations: 312
currently lose_sum: 87.0071160197258
time_elpased: 2.517
batch start
#iterations: 313
currently lose_sum: 86.81668531894684
time_elpased: 2.454
batch start
#iterations: 314
currently lose_sum: 86.87663662433624
time_elpased: 2.194
batch start
#iterations: 315
currently lose_sum: 86.76623177528381
time_elpased: 2.499
batch start
#iterations: 316
currently lose_sum: 86.77593380212784
time_elpased: 2.136
batch start
#iterations: 317
currently lose_sum: 86.83903801441193
time_elpased: 2.395
batch start
#iterations: 318
currently lose_sum: 87.12329185009003
time_elpased: 2.541
batch start
#iterations: 319
currently lose_sum: 87.14034003019333
time_elpased: 2.33
start validation test
0.620567010309
0.644504416094
0.540701862715
0.5880575298
0.620707225894
62.283
batch start
#iterations: 320
currently lose_sum: 86.51435935497284
time_elpased: 2.424
batch start
#iterations: 321
currently lose_sum: 87.08654481172562
time_elpased: 2.524
batch start
#iterations: 322
currently lose_sum: 87.11995869874954
time_elpased: 2.48
batch start
#iterations: 323
currently lose_sum: 86.22427088022232
time_elpased: 2.457
batch start
#iterations: 324
currently lose_sum: 86.22751235961914
time_elpased: 2.445
batch start
#iterations: 325
currently lose_sum: 86.68925642967224
time_elpased: 2.188
batch start
#iterations: 326
currently lose_sum: 86.06030267477036
time_elpased: 2.194
batch start
#iterations: 327
currently lose_sum: 86.72488915920258
time_elpased: 2.459
batch start
#iterations: 328
currently lose_sum: 86.68698680400848
time_elpased: 2.433
batch start
#iterations: 329
currently lose_sum: 86.42642784118652
time_elpased: 2.485
batch start
#iterations: 330
currently lose_sum: 86.85672640800476
time_elpased: 2.505
batch start
#iterations: 331
currently lose_sum: 86.82632207870483
time_elpased: 2.226
batch start
#iterations: 332
currently lose_sum: 86.19303345680237
time_elpased: 2.404
batch start
#iterations: 333
currently lose_sum: 86.1837608218193
time_elpased: 2.526
batch start
#iterations: 334
currently lose_sum: 86.58828145265579
time_elpased: 2.498
batch start
#iterations: 335
currently lose_sum: 86.2914879322052
time_elpased: 2.418
batch start
#iterations: 336
currently lose_sum: 86.50424486398697
time_elpased: 2.488
batch start
#iterations: 337
currently lose_sum: 86.51613277196884
time_elpased: 2.47
batch start
#iterations: 338
currently lose_sum: 86.2970684170723
time_elpased: 2.472
batch start
#iterations: 339
currently lose_sum: 86.50178182125092
time_elpased: 2.515
start validation test
0.625824742268
0.642641597029
0.569826078008
0.604047346315
0.62592305656
62.194
batch start
#iterations: 340
currently lose_sum: 85.973020195961
time_elpased: 2.506
batch start
#iterations: 341
currently lose_sum: 86.49990540742874
time_elpased: 2.402
batch start
#iterations: 342
currently lose_sum: 86.16620737314224
time_elpased: 2.509
batch start
#iterations: 343
currently lose_sum: 85.89204996824265
time_elpased: 2.305
batch start
#iterations: 344
currently lose_sum: 85.77235150337219
time_elpased: 2.025
batch start
#iterations: 345
currently lose_sum: 86.49359256029129
time_elpased: 2.358
batch start
#iterations: 346
currently lose_sum: 86.22589021921158
time_elpased: 2.384
batch start
#iterations: 347
currently lose_sum: 85.39977419376373
time_elpased: 2.093
batch start
#iterations: 348
currently lose_sum: 85.72691076993942
time_elpased: 2.51
batch start
#iterations: 349
currently lose_sum: 86.70672518014908
time_elpased: 2.648
batch start
#iterations: 350
currently lose_sum: 85.66579085588455
time_elpased: 2.466
batch start
#iterations: 351
currently lose_sum: 85.83826738595963
time_elpased: 2.74
batch start
#iterations: 352
currently lose_sum: 85.55205625295639
time_elpased: 2.653
batch start
#iterations: 353
currently lose_sum: 85.81628519296646
time_elpased: 2.434
batch start
#iterations: 354
currently lose_sum: 85.7289964556694
time_elpased: 2.548
batch start
#iterations: 355
currently lose_sum: 85.84478276968002
time_elpased: 2.64
batch start
#iterations: 356
currently lose_sum: 86.12752687931061
time_elpased: 2.461
batch start
#iterations: 357
currently lose_sum: 85.5499609708786
time_elpased: 2.492
batch start
#iterations: 358
currently lose_sum: 85.68784672021866
time_elpased: 2.564
batch start
#iterations: 359
currently lose_sum: 85.71801257133484
time_elpased: 2.186
start validation test
0.622113402062
0.645204479065
0.545538746527
0.591200579936
0.62224784068
62.748
batch start
#iterations: 360
currently lose_sum: 86.11795365810394
time_elpased: 2.367
batch start
#iterations: 361
currently lose_sum: 85.79227644205093
time_elpased: 2.589
batch start
#iterations: 362
currently lose_sum: 86.02758061885834
time_elpased: 2.476
batch start
#iterations: 363
currently lose_sum: 85.86677223443985
time_elpased: 2.56
batch start
#iterations: 364
currently lose_sum: 85.7232957482338
time_elpased: 2.507
batch start
#iterations: 365
currently lose_sum: 85.90226006507874
time_elpased: 2.566
batch start
#iterations: 366
currently lose_sum: 85.01532936096191
time_elpased: 2.503
batch start
#iterations: 367
currently lose_sum: 85.3742163181305
time_elpased: 2.389
batch start
#iterations: 368
currently lose_sum: 85.67944073677063
time_elpased: 2.51
batch start
#iterations: 369
currently lose_sum: 85.14909547567368
time_elpased: 2.535
batch start
#iterations: 370
currently lose_sum: 85.80580133199692
time_elpased: 2.423
batch start
#iterations: 371
currently lose_sum: 85.4996846318245
time_elpased: 2.172
batch start
#iterations: 372
currently lose_sum: 84.68357527256012
time_elpased: 2.522
batch start
#iterations: 373
currently lose_sum: 85.34668666124344
time_elpased: 2.488
batch start
#iterations: 374
currently lose_sum: 85.60661387443542
time_elpased: 2.568
batch start
#iterations: 375
currently lose_sum: 84.86965298652649
time_elpased: 2.307
batch start
#iterations: 376
currently lose_sum: 85.92912364006042
time_elpased: 2.52
batch start
#iterations: 377
currently lose_sum: 85.40688651800156
time_elpased: 2.641
batch start
#iterations: 378
currently lose_sum: 85.26616221666336
time_elpased: 2.468
batch start
#iterations: 379
currently lose_sum: 85.0693815946579
time_elpased: 2.588
start validation test
0.615154639175
0.645282044662
0.514459195225
0.572491983509
0.615331425558
63.099
batch start
#iterations: 380
currently lose_sum: 85.13728857040405
time_elpased: 2.515
batch start
#iterations: 381
currently lose_sum: 85.25814843177795
time_elpased: 2.115
batch start
#iterations: 382
currently lose_sum: 85.00627994537354
time_elpased: 2.702
batch start
#iterations: 383
currently lose_sum: 85.0284087061882
time_elpased: 2.548
batch start
#iterations: 384
currently lose_sum: 85.60656434297562
time_elpased: 2.457
batch start
#iterations: 385
currently lose_sum: 84.81477910280228
time_elpased: 2.454
batch start
#iterations: 386
currently lose_sum: 84.56229865550995
time_elpased: 2.555
batch start
#iterations: 387
currently lose_sum: 84.87191170454025
time_elpased: 2.398
batch start
#iterations: 388
currently lose_sum: 85.11552542448044
time_elpased: 2.607
batch start
#iterations: 389
currently lose_sum: 84.5013610124588
time_elpased: 2.624
batch start
#iterations: 390
currently lose_sum: 85.26000702381134
time_elpased: 2.434
batch start
#iterations: 391
currently lose_sum: 84.62970048189163
time_elpased: 2.513
batch start
#iterations: 392
currently lose_sum: 85.11603063344955
time_elpased: 2.226
batch start
#iterations: 393
currently lose_sum: 85.17596083879471
time_elpased: 2.348
batch start
#iterations: 394
currently lose_sum: 85.15175187587738
time_elpased: 2.598
batch start
#iterations: 395
currently lose_sum: 84.16987806558609
time_elpased: 2.479
batch start
#iterations: 396
currently lose_sum: 84.30845940113068
time_elpased: 2.485
batch start
#iterations: 397
currently lose_sum: 84.65456676483154
time_elpased: 2.627
batch start
#iterations: 398
currently lose_sum: 83.78449648618698
time_elpased: 2.568
batch start
#iterations: 399
currently lose_sum: 85.17894703149796
time_elpased: 2.598
start validation test
0.616804123711
0.644475382863
0.524030050427
0.578045181065
0.616967002906
63.427
acc: 0.648
pre: 0.649
rec: 0.648
F1: 0.649
auc: 0.706
