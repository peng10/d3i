start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.43694406747818
time_elpased: 2.708
batch start
#iterations: 1
currently lose_sum: 100.21762192249298
time_elpased: 2.573
batch start
#iterations: 2
currently lose_sum: 100.11480140686035
time_elpased: 2.835
batch start
#iterations: 3
currently lose_sum: 99.88979464769363
time_elpased: 2.793
batch start
#iterations: 4
currently lose_sum: 99.93844294548035
time_elpased: 3.026
batch start
#iterations: 5
currently lose_sum: 99.8171455860138
time_elpased: 2.592
batch start
#iterations: 6
currently lose_sum: 99.4411032795906
time_elpased: 2.571
batch start
#iterations: 7
currently lose_sum: 99.432712495327
time_elpased: 2.811
batch start
#iterations: 8
currently lose_sum: 98.9458321928978
time_elpased: 2.384
batch start
#iterations: 9
currently lose_sum: 98.57574570178986
time_elpased: 2.751
batch start
#iterations: 10
currently lose_sum: 98.40753108263016
time_elpased: 2.627
batch start
#iterations: 11
currently lose_sum: 98.22583651542664
time_elpased: 2.615
batch start
#iterations: 12
currently lose_sum: 98.18132334947586
time_elpased: 2.731
batch start
#iterations: 13
currently lose_sum: 97.84302866458893
time_elpased: 2.744
batch start
#iterations: 14
currently lose_sum: 97.53945940732956
time_elpased: 2.745
batch start
#iterations: 15
currently lose_sum: 97.48104041814804
time_elpased: 2.779
batch start
#iterations: 16
currently lose_sum: 97.44705802202225
time_elpased: 2.655
batch start
#iterations: 17
currently lose_sum: 97.22860777378082
time_elpased: 2.636
batch start
#iterations: 18
currently lose_sum: 96.99808984994888
time_elpased: 2.805
batch start
#iterations: 19
currently lose_sum: 96.83587658405304
time_elpased: 2.572
start validation test
0.609690721649
0.599131158148
0.667078316353
0.631281651734
0.609589968876
63.799
batch start
#iterations: 20
currently lose_sum: 96.94050168991089
time_elpased: 2.871
batch start
#iterations: 21
currently lose_sum: 96.53496408462524
time_elpased: 2.816
batch start
#iterations: 22
currently lose_sum: 96.60163527727127
time_elpased: 2.606
batch start
#iterations: 23
currently lose_sum: 96.62643212080002
time_elpased: 2.724
batch start
#iterations: 24
currently lose_sum: 96.3697116971016
time_elpased: 2.839
batch start
#iterations: 25
currently lose_sum: 96.07108879089355
time_elpased: 2.875
batch start
#iterations: 26
currently lose_sum: 96.19137477874756
time_elpased: 3.12
batch start
#iterations: 27
currently lose_sum: 96.14323526620865
time_elpased: 2.879
batch start
#iterations: 28
currently lose_sum: 96.3942728638649
time_elpased: 3.024
batch start
#iterations: 29
currently lose_sum: 95.72342771291733
time_elpased: 2.951
batch start
#iterations: 30
currently lose_sum: 95.68666768074036
time_elpased: 2.802
batch start
#iterations: 31
currently lose_sum: 95.42326283454895
time_elpased: 2.646
batch start
#iterations: 32
currently lose_sum: 95.18986177444458
time_elpased: 2.833
batch start
#iterations: 33
currently lose_sum: 95.64545351266861
time_elpased: 2.989
batch start
#iterations: 34
currently lose_sum: 95.4786486029625
time_elpased: 2.911
batch start
#iterations: 35
currently lose_sum: 95.44770807027817
time_elpased: 2.788
batch start
#iterations: 36
currently lose_sum: 95.57067400217056
time_elpased: 2.719
batch start
#iterations: 37
currently lose_sum: 95.11249935626984
time_elpased: 2.846
batch start
#iterations: 38
currently lose_sum: 95.2637420296669
time_elpased: 2.71
batch start
#iterations: 39
currently lose_sum: 95.07533937692642
time_elpased: 2.617
start validation test
0.644742268041
0.634485385128
0.685808377071
0.659149357072
0.644670170153
61.342
batch start
#iterations: 40
currently lose_sum: 95.09980398416519
time_elpased: 2.743
batch start
#iterations: 41
currently lose_sum: 95.3188264966011
time_elpased: 2.919
batch start
#iterations: 42
currently lose_sum: 95.03060591220856
time_elpased: 2.497
batch start
#iterations: 43
currently lose_sum: 94.28394258022308
time_elpased: 2.842
batch start
#iterations: 44
currently lose_sum: 94.87593567371368
time_elpased: 2.722
batch start
#iterations: 45
currently lose_sum: 94.52186703681946
time_elpased: 2.865
batch start
#iterations: 46
currently lose_sum: 94.22770744562149
time_elpased: 2.792
batch start
#iterations: 47
currently lose_sum: 94.43004900217056
time_elpased: 2.682
batch start
#iterations: 48
currently lose_sum: 94.4169391989708
time_elpased: 2.73
batch start
#iterations: 49
currently lose_sum: 95.04624533653259
time_elpased: 2.693
batch start
#iterations: 50
currently lose_sum: 94.44894832372665
time_elpased: 2.803
batch start
#iterations: 51
currently lose_sum: 94.05902326107025
time_elpased: 2.703
batch start
#iterations: 52
currently lose_sum: 94.14168632030487
time_elpased: 2.767
batch start
#iterations: 53
currently lose_sum: 94.13127356767654
time_elpased: 2.62
batch start
#iterations: 54
currently lose_sum: 94.21304565668106
time_elpased: 2.818
batch start
#iterations: 55
currently lose_sum: 94.39402079582214
time_elpased: 2.771
batch start
#iterations: 56
currently lose_sum: 93.88653111457825
time_elpased: 2.776
batch start
#iterations: 57
currently lose_sum: 93.68924582004547
time_elpased: 2.988
batch start
#iterations: 58
currently lose_sum: 93.87797892093658
time_elpased: 2.786
batch start
#iterations: 59
currently lose_sum: 94.23786997795105
time_elpased: 2.747
start validation test
0.639381443299
0.631844170947
0.670988988371
0.650828508684
0.639325951378
61.215
batch start
#iterations: 60
currently lose_sum: 93.84168314933777
time_elpased: 2.967
batch start
#iterations: 61
currently lose_sum: 94.20544648170471
time_elpased: 2.847
batch start
#iterations: 62
currently lose_sum: 93.8858956694603
time_elpased: 2.918
batch start
#iterations: 63
currently lose_sum: 93.60965418815613
time_elpased: 2.663
batch start
#iterations: 64
currently lose_sum: 94.01297688484192
time_elpased: 2.839
batch start
#iterations: 65
currently lose_sum: 93.61145704984665
time_elpased: 2.869
batch start
#iterations: 66
currently lose_sum: 92.84576141834259
time_elpased: 2.855
batch start
#iterations: 67
currently lose_sum: 93.2949857711792
time_elpased: 2.878
batch start
#iterations: 68
currently lose_sum: 93.8605043888092
time_elpased: 2.815
batch start
#iterations: 69
currently lose_sum: 93.27399003505707
time_elpased: 2.943
batch start
#iterations: 70
currently lose_sum: 93.0157899260521
time_elpased: 2.653
batch start
#iterations: 71
currently lose_sum: 92.98018425703049
time_elpased: 2.817
batch start
#iterations: 72
currently lose_sum: 92.5298643708229
time_elpased: 2.734
batch start
#iterations: 73
currently lose_sum: 93.00799435377121
time_elpased: 2.953
batch start
#iterations: 74
currently lose_sum: 92.98137331008911
time_elpased: 2.584
batch start
#iterations: 75
currently lose_sum: 92.86420583724976
time_elpased: 2.845
batch start
#iterations: 76
currently lose_sum: 92.6881252527237
time_elpased: 2.806
batch start
#iterations: 77
currently lose_sum: 92.98706549406052
time_elpased: 2.859
batch start
#iterations: 78
currently lose_sum: 92.93571174144745
time_elpased: 2.911
batch start
#iterations: 79
currently lose_sum: 92.61333346366882
time_elpased: 2.979
start validation test
0.634484536082
0.622458496549
0.686837501286
0.653065218455
0.634392622377
61.440
batch start
#iterations: 80
currently lose_sum: 93.20919674634933
time_elpased: 2.659
batch start
#iterations: 81
currently lose_sum: 92.66682291030884
time_elpased: 2.713
batch start
#iterations: 82
currently lose_sum: 92.7490022778511
time_elpased: 2.633
batch start
#iterations: 83
currently lose_sum: 93.34675300121307
time_elpased: 2.65
batch start
#iterations: 84
currently lose_sum: 92.58900171518326
time_elpased: 2.534
batch start
#iterations: 85
currently lose_sum: 92.67673003673553
time_elpased: 2.818
batch start
#iterations: 86
currently lose_sum: 92.89943331480026
time_elpased: 2.87
batch start
#iterations: 87
currently lose_sum: 92.7420164346695
time_elpased: 2.91
batch start
#iterations: 88
currently lose_sum: 92.2271980047226
time_elpased: 2.869
batch start
#iterations: 89
currently lose_sum: 92.2045618891716
time_elpased: 2.866
batch start
#iterations: 90
currently lose_sum: 92.79018521308899
time_elpased: 2.654
batch start
#iterations: 91
currently lose_sum: 91.92836672067642
time_elpased: 2.726
batch start
#iterations: 92
currently lose_sum: 92.45692932605743
time_elpased: 2.635
batch start
#iterations: 93
currently lose_sum: 92.05269360542297
time_elpased: 2.697
batch start
#iterations: 94
currently lose_sum: 92.63993483781815
time_elpased: 3.027
batch start
#iterations: 95
currently lose_sum: 91.91678112745285
time_elpased: 2.954
batch start
#iterations: 96
currently lose_sum: 92.33203399181366
time_elpased: 2.811
batch start
#iterations: 97
currently lose_sum: 92.02213287353516
time_elpased: 2.688
batch start
#iterations: 98
currently lose_sum: 91.80183130502701
time_elpased: 2.957
batch start
#iterations: 99
currently lose_sum: 92.15589028596878
time_elpased: 2.888
start validation test
0.650206185567
0.638424482856
0.695585057116
0.665780141844
0.650126515959
60.380
batch start
#iterations: 100
currently lose_sum: 92.09213560819626
time_elpased: 2.771
batch start
#iterations: 101
currently lose_sum: 91.98603677749634
time_elpased: 2.762
batch start
#iterations: 102
currently lose_sum: 92.13000190258026
time_elpased: 2.999
batch start
#iterations: 103
currently lose_sum: 91.65389847755432
time_elpased: 2.883
batch start
#iterations: 104
currently lose_sum: 91.5746420621872
time_elpased: 2.489
batch start
#iterations: 105
currently lose_sum: 91.72412461042404
time_elpased: 2.926
batch start
#iterations: 106
currently lose_sum: 91.78705263137817
time_elpased: 2.796
batch start
#iterations: 107
currently lose_sum: 91.38966697454453
time_elpased: 2.832
batch start
#iterations: 108
currently lose_sum: 91.28541731834412
time_elpased: 2.782
batch start
#iterations: 109
currently lose_sum: 91.97866880893707
time_elpased: 2.888
batch start
#iterations: 110
currently lose_sum: 91.29692763090134
time_elpased: 2.902
batch start
#iterations: 111
currently lose_sum: 91.65099537372589
time_elpased: 2.809
batch start
#iterations: 112
currently lose_sum: 91.15463978052139
time_elpased: 3.072
batch start
#iterations: 113
currently lose_sum: 91.37071883678436
time_elpased: 2.67
batch start
#iterations: 114
currently lose_sum: 91.53171366453171
time_elpased: 2.848
batch start
#iterations: 115
currently lose_sum: 91.42519581317902
time_elpased: 2.604
batch start
#iterations: 116
currently lose_sum: 91.41568166017532
time_elpased: 2.676
batch start
#iterations: 117
currently lose_sum: 91.57523173093796
time_elpased: 2.787
batch start
#iterations: 118
currently lose_sum: 91.23939102888107
time_elpased: 2.816
batch start
#iterations: 119
currently lose_sum: 91.073979139328
time_elpased: 2.865
start validation test
0.641082474227
0.647683397683
0.621488113615
0.63431542461
0.641116875149
60.567
batch start
#iterations: 120
currently lose_sum: 90.9704293012619
time_elpased: 2.83
batch start
#iterations: 121
currently lose_sum: 91.00078052282333
time_elpased: 2.563
batch start
#iterations: 122
currently lose_sum: 90.92502969503403
time_elpased: 2.548
batch start
#iterations: 123
currently lose_sum: 91.18590319156647
time_elpased: 2.847
batch start
#iterations: 124
currently lose_sum: 91.65088045597076
time_elpased: 2.629
batch start
#iterations: 125
currently lose_sum: 91.50279200077057
time_elpased: 2.485
batch start
#iterations: 126
currently lose_sum: 91.20642137527466
time_elpased: 2.591
batch start
#iterations: 127
currently lose_sum: 91.08855932950974
time_elpased: 2.911
batch start
#iterations: 128
currently lose_sum: 91.08332121372223
time_elpased: 2.663
batch start
#iterations: 129
currently lose_sum: 90.50394415855408
time_elpased: 2.908
batch start
#iterations: 130
currently lose_sum: 90.35357749462128
time_elpased: 2.793
batch start
#iterations: 131
currently lose_sum: 90.95227134227753
time_elpased: 2.914
batch start
#iterations: 132
currently lose_sum: 90.56331098079681
time_elpased: 3.068
batch start
#iterations: 133
currently lose_sum: 90.44144558906555
time_elpased: 2.792
batch start
#iterations: 134
currently lose_sum: 90.39021533727646
time_elpased: 2.61
batch start
#iterations: 135
currently lose_sum: 91.45660132169724
time_elpased: 2.946
batch start
#iterations: 136
currently lose_sum: 90.65901339054108
time_elpased: 2.945
batch start
#iterations: 137
currently lose_sum: 89.95820826292038
time_elpased: 2.878
batch start
#iterations: 138
currently lose_sum: 90.5327422618866
time_elpased: 2.881
batch start
#iterations: 139
currently lose_sum: 90.65649425983429
time_elpased: 2.862
start validation test
0.641494845361
0.64367457345
0.636719152002
0.640177970925
0.641503229827
60.776
batch start
#iterations: 140
currently lose_sum: 90.69519942998886
time_elpased: 2.797
batch start
#iterations: 141
currently lose_sum: 90.37715297937393
time_elpased: 3.012
batch start
#iterations: 142
currently lose_sum: 90.19672346115112
time_elpased: 2.549
batch start
#iterations: 143
currently lose_sum: 90.18146902322769
time_elpased: 2.808
batch start
#iterations: 144
currently lose_sum: 90.16182774305344
time_elpased: 2.728
batch start
#iterations: 145
currently lose_sum: 90.50313001871109
time_elpased: 2.888
batch start
#iterations: 146
currently lose_sum: 90.39391946792603
time_elpased: 2.778
batch start
#iterations: 147
currently lose_sum: 89.98203200101852
time_elpased: 2.697
batch start
#iterations: 148
currently lose_sum: 90.0719820857048
time_elpased: 2.683
batch start
#iterations: 149
currently lose_sum: 90.23075950145721
time_elpased: 2.87
batch start
#iterations: 150
currently lose_sum: 90.39515256881714
time_elpased: 2.832
batch start
#iterations: 151
currently lose_sum: 89.5432659983635
time_elpased: 2.575
batch start
#iterations: 152
currently lose_sum: 90.44649237394333
time_elpased: 2.746
batch start
#iterations: 153
currently lose_sum: 89.83937323093414
time_elpased: 2.7
batch start
#iterations: 154
currently lose_sum: 90.06600552797318
time_elpased: 2.66
batch start
#iterations: 155
currently lose_sum: 89.44795620441437
time_elpased: 2.765
batch start
#iterations: 156
currently lose_sum: 89.91418647766113
time_elpased: 2.981
batch start
#iterations: 157
currently lose_sum: 89.91683381795883
time_elpased: 2.603
batch start
#iterations: 158
currently lose_sum: 89.85743927955627
time_elpased: 2.828
batch start
#iterations: 159
currently lose_sum: 90.02820837497711
time_elpased: 2.935
start validation test
0.634278350515
0.648303167421
0.589791087784
0.617664493183
0.634356454767
61.145
batch start
#iterations: 160
currently lose_sum: 89.65289402008057
time_elpased: 2.933
batch start
#iterations: 161
currently lose_sum: 89.28409641981125
time_elpased: 2.816
batch start
#iterations: 162
currently lose_sum: 89.57227003574371
time_elpased: 2.668
batch start
#iterations: 163
currently lose_sum: 89.41816455125809
time_elpased: 2.747
batch start
#iterations: 164
currently lose_sum: 89.34073495864868
time_elpased: 2.929
batch start
#iterations: 165
currently lose_sum: 89.32065373659134
time_elpased: 2.505
batch start
#iterations: 166
currently lose_sum: 89.65859115123749
time_elpased: 2.78
batch start
#iterations: 167
currently lose_sum: 89.23970276117325
time_elpased: 2.924
batch start
#iterations: 168
currently lose_sum: 89.0034738779068
time_elpased: 2.854
batch start
#iterations: 169
currently lose_sum: 89.11104947328568
time_elpased: 2.778
batch start
#iterations: 170
currently lose_sum: 88.79930007457733
time_elpased: 2.9
batch start
#iterations: 171
currently lose_sum: 88.79816299676895
time_elpased: 2.855
batch start
#iterations: 172
currently lose_sum: 88.50097107887268
time_elpased: 2.549
batch start
#iterations: 173
currently lose_sum: 88.55420380830765
time_elpased: 3.093
batch start
#iterations: 174
currently lose_sum: 88.69157963991165
time_elpased: 2.962
batch start
#iterations: 175
currently lose_sum: 89.0595800280571
time_elpased: 2.574
batch start
#iterations: 176
currently lose_sum: 89.45830309391022
time_elpased: 2.959
batch start
#iterations: 177
currently lose_sum: 88.42913430929184
time_elpased: 3.104
batch start
#iterations: 178
currently lose_sum: 88.2910230755806
time_elpased: 2.748
batch start
#iterations: 179
currently lose_sum: 88.77022755146027
time_elpased: 2.828
start validation test
0.625515463918
0.638187556357
0.582690130699
0.609177470547
0.625590650395
62.263
batch start
#iterations: 180
currently lose_sum: 88.62644445896149
time_elpased: 2.729
batch start
#iterations: 181
currently lose_sum: 88.5871017575264
time_elpased: 2.633
batch start
#iterations: 182
currently lose_sum: 88.88565045595169
time_elpased: 2.812
batch start
#iterations: 183
currently lose_sum: 87.7633318901062
time_elpased: 2.631
batch start
#iterations: 184
currently lose_sum: 88.59693676233292
time_elpased: 2.813
batch start
#iterations: 185
currently lose_sum: 88.16413313150406
time_elpased: 2.969
batch start
#iterations: 186
currently lose_sum: 87.99854463338852
time_elpased: 3.117
batch start
#iterations: 187
currently lose_sum: 88.69540584087372
time_elpased: 2.842
batch start
#iterations: 188
currently lose_sum: 87.81731295585632
time_elpased: 2.908
batch start
#iterations: 189
currently lose_sum: 88.29944080114365
time_elpased: 2.788
batch start
#iterations: 190
currently lose_sum: 87.99624317884445
time_elpased: 2.721
batch start
#iterations: 191
currently lose_sum: 87.4211933016777
time_elpased: 2.68
batch start
#iterations: 192
currently lose_sum: 87.4317194223404
time_elpased: 2.731
batch start
#iterations: 193
currently lose_sum: 87.87624192237854
time_elpased: 2.982
batch start
#iterations: 194
currently lose_sum: 87.61049377918243
time_elpased: 2.83
batch start
#iterations: 195
currently lose_sum: 87.96379315853119
time_elpased: 2.809
batch start
#iterations: 196
currently lose_sum: 87.74607139825821
time_elpased: 2.821
batch start
#iterations: 197
currently lose_sum: 87.95986145734787
time_elpased: 2.888
batch start
#iterations: 198
currently lose_sum: 88.20299649238586
time_elpased: 2.64
batch start
#iterations: 199
currently lose_sum: 88.20409870147705
time_elpased: 2.563
start validation test
0.626030927835
0.63575209528
0.593290110116
0.613787596487
0.62608840939
62.594
batch start
#iterations: 200
currently lose_sum: 87.72711551189423
time_elpased: 2.793
batch start
#iterations: 201
currently lose_sum: 87.13615030050278
time_elpased: 2.743
batch start
#iterations: 202
currently lose_sum: 87.6724596619606
time_elpased: 2.787
batch start
#iterations: 203
currently lose_sum: 87.87485378980637
time_elpased: 2.826
batch start
#iterations: 204
currently lose_sum: 87.38127547502518
time_elpased: 2.783
batch start
#iterations: 205
currently lose_sum: 87.56534737348557
time_elpased: 2.829
batch start
#iterations: 206
currently lose_sum: 87.41332936286926
time_elpased: 2.779
batch start
#iterations: 207
currently lose_sum: 87.40260380506516
time_elpased: 3.039
batch start
#iterations: 208
currently lose_sum: 86.79760646820068
time_elpased: 2.836
batch start
#iterations: 209
currently lose_sum: 86.97742521762848
time_elpased: 2.982
batch start
#iterations: 210
currently lose_sum: 87.13163208961487
time_elpased: 2.353
batch start
#iterations: 211
currently lose_sum: 86.39208453893661
time_elpased: 2.846
batch start
#iterations: 212
currently lose_sum: 86.42652171850204
time_elpased: 2.911
batch start
#iterations: 213
currently lose_sum: 86.19820547103882
time_elpased: 2.864
batch start
#iterations: 214
currently lose_sum: 86.98443907499313
time_elpased: 2.619
batch start
#iterations: 215
currently lose_sum: 86.78247255086899
time_elpased: 2.798
batch start
#iterations: 216
currently lose_sum: 86.72817814350128
time_elpased: 3.204
batch start
#iterations: 217
currently lose_sum: 86.57601523399353
time_elpased: 2.819
batch start
#iterations: 218
currently lose_sum: 87.05016177892685
time_elpased: 2.932
batch start
#iterations: 219
currently lose_sum: 87.03098160028458
time_elpased: 2.721
start validation test
0.620927835052
0.636953749855
0.565503756303
0.599105974706
0.62102514057
63.266
batch start
#iterations: 220
currently lose_sum: 86.7477833032608
time_elpased: 2.645
batch start
#iterations: 221
currently lose_sum: 86.19688022136688
time_elpased: 2.76
batch start
#iterations: 222
currently lose_sum: 86.54717695713043
time_elpased: 2.973
batch start
#iterations: 223
currently lose_sum: 86.01372766494751
time_elpased: 2.762
batch start
#iterations: 224
currently lose_sum: 86.5682470202446
time_elpased: 2.655
batch start
#iterations: 225
currently lose_sum: 86.07250785827637
time_elpased: 2.889
batch start
#iterations: 226
currently lose_sum: 85.68409955501556
time_elpased: 2.796
batch start
#iterations: 227
currently lose_sum: 86.02024674415588
time_elpased: 3.008
batch start
#iterations: 228
currently lose_sum: 85.68972277641296
time_elpased: 2.65
batch start
#iterations: 229
currently lose_sum: 85.67400813102722
time_elpased: 2.96
batch start
#iterations: 230
currently lose_sum: 85.39565390348434
time_elpased: 2.882
batch start
#iterations: 231
currently lose_sum: 85.1872044801712
time_elpased: 2.748
batch start
#iterations: 232
currently lose_sum: 85.46658217906952
time_elpased: 2.467
batch start
#iterations: 233
currently lose_sum: 85.77245020866394
time_elpased: 3.017
batch start
#iterations: 234
currently lose_sum: 85.40041726827621
time_elpased: 2.906
batch start
#iterations: 235
currently lose_sum: 85.09504920244217
time_elpased: 2.871
batch start
#iterations: 236
currently lose_sum: 85.90626776218414
time_elpased: 2.967
batch start
#iterations: 237
currently lose_sum: 85.15173506736755
time_elpased: 2.494
batch start
#iterations: 238
currently lose_sum: 85.20151355862617
time_elpased: 2.757
batch start
#iterations: 239
currently lose_sum: 85.26469141244888
time_elpased: 2.937
start validation test
0.620051546392
0.638915206063
0.55521251415
0.59413027917
0.620165381314
63.782
batch start
#iterations: 240
currently lose_sum: 84.99212735891342
time_elpased: 2.782
batch start
#iterations: 241
currently lose_sum: 84.99071913957596
time_elpased: 2.849
batch start
#iterations: 242
currently lose_sum: 84.65219402313232
time_elpased: 2.553
batch start
#iterations: 243
currently lose_sum: 84.76138716936111
time_elpased: 2.756
batch start
#iterations: 244
currently lose_sum: 85.44708108901978
time_elpased: 2.941
batch start
#iterations: 245
currently lose_sum: 85.64611142873764
time_elpased: 2.988
batch start
#iterations: 246
currently lose_sum: 84.50704115629196
time_elpased: 2.675
batch start
#iterations: 247
currently lose_sum: 84.60033428668976
time_elpased: 2.761
batch start
#iterations: 248
currently lose_sum: 85.27775865793228
time_elpased: 2.797
batch start
#iterations: 249
currently lose_sum: 84.68886557221413
time_elpased: 2.456
batch start
#iterations: 250
currently lose_sum: 84.19366812705994
time_elpased: 2.597
batch start
#iterations: 251
currently lose_sum: 84.44133913516998
time_elpased: 2.985
batch start
#iterations: 252
currently lose_sum: 84.49074602127075
time_elpased: 2.647
batch start
#iterations: 253
currently lose_sum: 84.41439443826675
time_elpased: 2.723
batch start
#iterations: 254
currently lose_sum: 84.1140827536583
time_elpased: 2.873
batch start
#iterations: 255
currently lose_sum: 83.90527057647705
time_elpased: 2.87
batch start
#iterations: 256
currently lose_sum: 84.34295845031738
time_elpased: 2.765
batch start
#iterations: 257
currently lose_sum: 84.60895144939423
time_elpased: 2.615
batch start
#iterations: 258
currently lose_sum: 83.90540283918381
time_elpased: 2.832
batch start
#iterations: 259
currently lose_sum: 83.51409298181534
time_elpased: 2.754
start validation test
0.611958762887
0.625300515169
0.562107646393
0.592022544982
0.612046284211
64.525
batch start
#iterations: 260
currently lose_sum: 83.37397056818008
time_elpased: 2.934
batch start
#iterations: 261
currently lose_sum: 84.02855703234673
time_elpased: 3.034
batch start
#iterations: 262
currently lose_sum: 83.75233316421509
time_elpased: 2.964
batch start
#iterations: 263
currently lose_sum: 83.33979469537735
time_elpased: 2.805
batch start
#iterations: 264
currently lose_sum: 83.43946021795273
time_elpased: 3.063
batch start
#iterations: 265
currently lose_sum: 83.37633556127548
time_elpased: 2.828
batch start
#iterations: 266
currently lose_sum: 83.50508284568787
time_elpased: 2.871
batch start
#iterations: 267
currently lose_sum: 83.17123568058014
time_elpased: 2.964
batch start
#iterations: 268
currently lose_sum: 83.05268412828445
time_elpased: 2.758
batch start
#iterations: 269
currently lose_sum: 83.04542446136475
time_elpased: 2.985
batch start
#iterations: 270
currently lose_sum: 83.78170052170753
time_elpased: 2.756
batch start
#iterations: 271
currently lose_sum: 82.5796617269516
time_elpased: 2.488
batch start
#iterations: 272
currently lose_sum: 83.35860472917557
time_elpased: 2.675
batch start
#iterations: 273
currently lose_sum: 82.74418741464615
time_elpased: 2.897
batch start
#iterations: 274
currently lose_sum: 82.50234359502792
time_elpased: 2.823
batch start
#iterations: 275
currently lose_sum: 82.88969579339027
time_elpased: 2.615
batch start
#iterations: 276
currently lose_sum: 82.81737539172173
time_elpased: 2.565
batch start
#iterations: 277
currently lose_sum: 82.76912903785706
time_elpased: 2.924
batch start
#iterations: 278
currently lose_sum: 82.30092769861221
time_elpased: 2.745
batch start
#iterations: 279
currently lose_sum: 83.00738722085953
time_elpased: 2.966
start validation test
0.607835051546
0.637861158321
0.502109704641
0.561902568237
0.6080206687
65.928
batch start
#iterations: 280
currently lose_sum: 82.4710955619812
time_elpased: 2.909
batch start
#iterations: 281
currently lose_sum: 82.80769044160843
time_elpased: 2.661
batch start
#iterations: 282
currently lose_sum: 81.96786004304886
time_elpased: 2.759
batch start
#iterations: 283
currently lose_sum: 82.01936459541321
time_elpased: 2.793
batch start
#iterations: 284
currently lose_sum: 81.75241357088089
time_elpased: 2.865
batch start
#iterations: 285
currently lose_sum: 81.36172068119049
time_elpased: 2.721
batch start
#iterations: 286
currently lose_sum: 81.77564084529877
time_elpased: 2.711
batch start
#iterations: 287
currently lose_sum: 81.45640882849693
time_elpased: 2.818
batch start
#iterations: 288
currently lose_sum: 81.72846639156342
time_elpased: 2.999
batch start
#iterations: 289
currently lose_sum: 81.95042306184769
time_elpased: 2.816
batch start
#iterations: 290
currently lose_sum: 81.52832478284836
time_elpased: 2.918
batch start
#iterations: 291
currently lose_sum: 81.28244858980179
time_elpased: 3.005
batch start
#iterations: 292
currently lose_sum: 81.5862675011158
time_elpased: 2.846
batch start
#iterations: 293
currently lose_sum: 80.98100379109383
time_elpased: 2.691
batch start
#iterations: 294
currently lose_sum: 81.16063621640205
time_elpased: 2.809
batch start
#iterations: 295
currently lose_sum: 80.98811945319176
time_elpased: 2.764
batch start
#iterations: 296
currently lose_sum: 81.19247406721115
time_elpased: 2.766
batch start
#iterations: 297
currently lose_sum: 80.89579871296883
time_elpased: 2.697
batch start
#iterations: 298
currently lose_sum: 81.31720086932182
time_elpased: 2.763
batch start
#iterations: 299
currently lose_sum: 80.74185049533844
time_elpased: 2.411
start validation test
0.611958762887
0.642605863192
0.507564062982
0.567157313707
0.612142043884
67.268
batch start
#iterations: 300
currently lose_sum: 80.74310240149498
time_elpased: 2.942
batch start
#iterations: 301
currently lose_sum: 81.53314355015755
time_elpased: 2.565
batch start
#iterations: 302
currently lose_sum: 80.15132629871368
time_elpased: 2.722
batch start
#iterations: 303
currently lose_sum: 80.14057809114456
time_elpased: 2.91
batch start
#iterations: 304
currently lose_sum: 80.1110365986824
time_elpased: 2.751
batch start
#iterations: 305
currently lose_sum: 80.21909099817276
time_elpased: 2.851
batch start
#iterations: 306
currently lose_sum: 80.41089993715286
time_elpased: 2.526
batch start
#iterations: 307
currently lose_sum: 79.7149039208889
time_elpased: 2.82
batch start
#iterations: 308
currently lose_sum: 80.44160902500153
time_elpased: 2.925
batch start
#iterations: 309
currently lose_sum: 79.6736895442009
time_elpased: 2.817
batch start
#iterations: 310
currently lose_sum: 80.6141465306282
time_elpased: 2.883
batch start
#iterations: 311
currently lose_sum: 79.09861579537392
time_elpased: 2.904
batch start
#iterations: 312
currently lose_sum: 79.19338899850845
time_elpased: 3.108
batch start
#iterations: 313
currently lose_sum: 79.27030232548714
time_elpased: 2.951
batch start
#iterations: 314
currently lose_sum: 79.44703328609467
time_elpased: 2.956
batch start
#iterations: 315
currently lose_sum: 79.95242407917976
time_elpased: 2.581
batch start
#iterations: 316
currently lose_sum: 79.27898782491684
time_elpased: 2.915
batch start
#iterations: 317
currently lose_sum: 79.61373484134674
time_elpased: 2.673
batch start
#iterations: 318
currently lose_sum: 79.48239973187447
time_elpased: 2.933
batch start
#iterations: 319
currently lose_sum: 79.41894227266312
time_elpased: 2.834
start validation test
0.596443298969
0.635204812375
0.456416589482
0.53116953111
0.596689137455
69.786
batch start
#iterations: 320
currently lose_sum: 79.3385548889637
time_elpased: 2.931
batch start
#iterations: 321
currently lose_sum: 79.05422452092171
time_elpased: 2.8
batch start
#iterations: 322
currently lose_sum: 79.29129016399384
time_elpased: 2.742
batch start
#iterations: 323
currently lose_sum: 79.04843956232071
time_elpased: 2.853
batch start
#iterations: 324
currently lose_sum: 78.3989834189415
time_elpased: 2.543
batch start
#iterations: 325
currently lose_sum: 78.6626206934452
time_elpased: 2.854
batch start
#iterations: 326
currently lose_sum: 78.31350380182266
time_elpased: 2.682
batch start
#iterations: 327
currently lose_sum: 78.1083652973175
time_elpased: 2.931
batch start
#iterations: 328
currently lose_sum: 77.84588006138802
time_elpased: 2.788
batch start
#iterations: 329
currently lose_sum: 78.40141367912292
time_elpased: 2.736
batch start
#iterations: 330
currently lose_sum: 77.78050994873047
time_elpased: 2.873
batch start
#iterations: 331
currently lose_sum: 78.52452763915062
time_elpased: 2.875
batch start
#iterations: 332
currently lose_sum: 77.88088989257812
time_elpased: 2.765
batch start
#iterations: 333
currently lose_sum: 77.74034330248833
time_elpased: 2.918
batch start
#iterations: 334
currently lose_sum: 78.45372757315636
time_elpased: 2.716
batch start
#iterations: 335
currently lose_sum: 77.50433513522148
time_elpased: 3.001
batch start
#iterations: 336
currently lose_sum: 78.12030324339867
time_elpased: 2.766
batch start
#iterations: 337
currently lose_sum: 78.08254790306091
time_elpased: 2.771
batch start
#iterations: 338
currently lose_sum: 77.55826076865196
time_elpased: 2.917
batch start
#iterations: 339
currently lose_sum: 77.13550469279289
time_elpased: 2.752
start validation test
0.588505154639
0.629712746858
0.433158382217
0.513261386501
0.588777889859
71.325
batch start
#iterations: 340
currently lose_sum: 77.5526370704174
time_elpased: 2.933
batch start
#iterations: 341
currently lose_sum: 77.35626348853111
time_elpased: 3.014
batch start
#iterations: 342
currently lose_sum: 77.32664361596107
time_elpased: 2.952
batch start
#iterations: 343
currently lose_sum: 77.34605556726456
time_elpased: 2.876
batch start
#iterations: 344
currently lose_sum: 76.70166397094727
time_elpased: 2.683
batch start
#iterations: 345
currently lose_sum: 76.98574382066727
time_elpased: 2.811
batch start
#iterations: 346
currently lose_sum: 76.64715668559074
time_elpased: 2.803
batch start
#iterations: 347
currently lose_sum: 77.37743467092514
time_elpased: 2.885
batch start
#iterations: 348
currently lose_sum: 76.37754103541374
time_elpased: 2.827
batch start
#iterations: 349
currently lose_sum: 77.2002238035202
time_elpased: 2.688
batch start
#iterations: 350
currently lose_sum: 76.59516388177872
time_elpased: 2.672
batch start
#iterations: 351
currently lose_sum: 76.80249297618866
time_elpased: 2.589
batch start
#iterations: 352
currently lose_sum: 76.05791968107224
time_elpased: 2.694
batch start
#iterations: 353
currently lose_sum: 77.114582747221
time_elpased: 2.566
batch start
#iterations: 354
currently lose_sum: 76.06594479084015
time_elpased: 2.765
batch start
#iterations: 355
currently lose_sum: 75.7456990480423
time_elpased: 2.956
batch start
#iterations: 356
currently lose_sum: 75.92773950099945
time_elpased: 2.784
batch start
#iterations: 357
currently lose_sum: 75.95997992157936
time_elpased: 2.95
batch start
#iterations: 358
currently lose_sum: 76.37925466895103
time_elpased: 2.633
batch start
#iterations: 359
currently lose_sum: 75.79578810930252
time_elpased: 2.746
start validation test
0.587989690722
0.629546137662
0.431100133786
0.51175859752
0.588265134537
73.442
batch start
#iterations: 360
currently lose_sum: 75.70498469471931
time_elpased: 2.762
batch start
#iterations: 361
currently lose_sum: 75.84921365976334
time_elpased: 2.781
batch start
#iterations: 362
currently lose_sum: 75.00584143400192
time_elpased: 2.408
batch start
#iterations: 363
currently lose_sum: 75.2757782638073
time_elpased: 2.936
batch start
#iterations: 364
currently lose_sum: 76.06199735403061
time_elpased: 2.626
batch start
#iterations: 365
currently lose_sum: 75.18321391940117
time_elpased: 2.677
batch start
#iterations: 366
currently lose_sum: 74.92254930734634
time_elpased: 2.857
batch start
#iterations: 367
currently lose_sum: 75.17729115486145
time_elpased: 2.793
batch start
#iterations: 368
currently lose_sum: 74.89013329148293
time_elpased: 2.888
batch start
#iterations: 369
currently lose_sum: 74.37282484769821
time_elpased: 2.785
batch start
#iterations: 370
currently lose_sum: 75.18868917226791
time_elpased: 2.712
batch start
#iterations: 371
currently lose_sum: 74.79093396663666
time_elpased: 2.601
batch start
#iterations: 372
currently lose_sum: 75.23162311315536
time_elpased: 2.98
batch start
#iterations: 373
currently lose_sum: 74.69502034783363
time_elpased: 2.833
batch start
#iterations: 374
currently lose_sum: 75.02332696318626
time_elpased: 2.569
batch start
#iterations: 375
currently lose_sum: 75.0027047097683
time_elpased: 2.774
batch start
#iterations: 376
currently lose_sum: 74.07978567481041
time_elpased: 2.467
batch start
#iterations: 377
currently lose_sum: 73.68998947739601
time_elpased: 2.783
batch start
#iterations: 378
currently lose_sum: 73.43420824408531
time_elpased: 2.847
batch start
#iterations: 379
currently lose_sum: 73.79720973968506
time_elpased: 2.721
start validation test
0.585567010309
0.626719056974
0.426777812082
0.507775192849
0.585845789238
74.987
batch start
#iterations: 380
currently lose_sum: 73.56225436925888
time_elpased: 2.671
batch start
#iterations: 381
currently lose_sum: 73.49551290273666
time_elpased: 2.616
batch start
#iterations: 382
currently lose_sum: 73.62972968816757
time_elpased: 2.959
batch start
#iterations: 383
currently lose_sum: 73.70466715097427
time_elpased: 2.968
batch start
#iterations: 384
currently lose_sum: 73.7496432363987
time_elpased: 2.964
batch start
#iterations: 385
currently lose_sum: 73.45801872015
time_elpased: 2.743
batch start
#iterations: 386
currently lose_sum: 73.21902641654015
time_elpased: 2.797
batch start
#iterations: 387
currently lose_sum: 73.48313283920288
time_elpased: 3.019
batch start
#iterations: 388
currently lose_sum: 73.56151866912842
time_elpased: 2.644
batch start
#iterations: 389
currently lose_sum: 73.05869600176811
time_elpased: 3.16
batch start
#iterations: 390
currently lose_sum: 73.33428490161896
time_elpased: 2.895
batch start
#iterations: 391
currently lose_sum: 72.80085501074791
time_elpased: 2.923
batch start
#iterations: 392
currently lose_sum: 72.02300363779068
time_elpased: 3.324
batch start
#iterations: 393
currently lose_sum: 72.11458992958069
time_elpased: 2.639
batch start
#iterations: 394
currently lose_sum: 72.69067403674126
time_elpased: 3.038
batch start
#iterations: 395
currently lose_sum: 72.66692420840263
time_elpased: 2.906
batch start
#iterations: 396
currently lose_sum: 71.83713564276695
time_elpased: 2.846
batch start
#iterations: 397
currently lose_sum: 72.3746939599514
time_elpased: 2.708
batch start
#iterations: 398
currently lose_sum: 72.69612294435501
time_elpased: 2.791
batch start
#iterations: 399
currently lose_sum: 72.20107167959213
time_elpased: 2.735
start validation test
0.571907216495
0.616926134482
0.383348770197
0.472865756903
0.57223825993
77.516
acc: 0.651
pre: 0.639
rec: 0.697
F1: 0.667
auc: 0.702
