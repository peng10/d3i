start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.59618955850601
time_elpased: 1.662
batch start
#iterations: 1
currently lose_sum: 100.44502228498459
time_elpased: 1.577
batch start
#iterations: 2
currently lose_sum: 100.37665611505508
time_elpased: 1.611
batch start
#iterations: 3
currently lose_sum: 100.3249072432518
time_elpased: 1.544
batch start
#iterations: 4
currently lose_sum: 100.25837141275406
time_elpased: 1.594
batch start
#iterations: 5
currently lose_sum: 100.27563738822937
time_elpased: 1.558
batch start
#iterations: 6
currently lose_sum: 100.25540590286255
time_elpased: 1.539
batch start
#iterations: 7
currently lose_sum: 100.22046881914139
time_elpased: 1.532
batch start
#iterations: 8
currently lose_sum: 100.20278608798981
time_elpased: 1.56
batch start
#iterations: 9
currently lose_sum: 100.10912662744522
time_elpased: 1.524
batch start
#iterations: 10
currently lose_sum: 100.21623796224594
time_elpased: 1.569
batch start
#iterations: 11
currently lose_sum: 100.03909534215927
time_elpased: 1.547
batch start
#iterations: 12
currently lose_sum: 99.98154473304749
time_elpased: 1.61
batch start
#iterations: 13
currently lose_sum: 99.87919288873672
time_elpased: 1.524
batch start
#iterations: 14
currently lose_sum: 99.93074810504913
time_elpased: 1.505
batch start
#iterations: 15
currently lose_sum: 100.11519241333008
time_elpased: 1.625
batch start
#iterations: 16
currently lose_sum: 99.86841171979904
time_elpased: 1.844
batch start
#iterations: 17
currently lose_sum: 99.87957227230072
time_elpased: 1.783
batch start
#iterations: 18
currently lose_sum: 99.75445532798767
time_elpased: 1.895
batch start
#iterations: 19
currently lose_sum: 99.7837445139885
time_elpased: 1.762
start validation test
0.5774226804123711
0.5848129536571748
0.5389523515488319
0.5609468723221936
0.577490221008331
65.997
batch start
#iterations: 20
currently lose_sum: 99.8536918759346
time_elpased: 1.511
batch start
#iterations: 21
currently lose_sum: 99.83489322662354
time_elpased: 1.507
batch start
#iterations: 22
currently lose_sum: 99.732970058918
time_elpased: 1.603
batch start
#iterations: 23
currently lose_sum: 99.95547360181808
time_elpased: 1.617
batch start
#iterations: 24
currently lose_sum: 99.63153928518295
time_elpased: 1.602
batch start
#iterations: 25
currently lose_sum: 99.805624127388
time_elpased: 1.612
batch start
#iterations: 26
currently lose_sum: 99.55797582864761
time_elpased: 1.601
batch start
#iterations: 27
currently lose_sum: 99.68267530202866
time_elpased: 1.578
batch start
#iterations: 28
currently lose_sum: 99.54585182666779
time_elpased: 1.532
batch start
#iterations: 29
currently lose_sum: 99.63198083639145
time_elpased: 1.531
batch start
#iterations: 30
currently lose_sum: 99.51668524742126
time_elpased: 1.595
batch start
#iterations: 31
currently lose_sum: 99.60641556978226
time_elpased: 1.562
batch start
#iterations: 32
currently lose_sum: 99.46040123701096
time_elpased: 1.577
batch start
#iterations: 33
currently lose_sum: 99.6352504491806
time_elpased: 1.596
batch start
#iterations: 34
currently lose_sum: 99.52668362855911
time_elpased: 1.55
batch start
#iterations: 35
currently lose_sum: 99.55257207155228
time_elpased: 1.567
batch start
#iterations: 36
currently lose_sum: 99.47622185945511
time_elpased: 1.584
batch start
#iterations: 37
currently lose_sum: 99.42845332622528
time_elpased: 1.604
batch start
#iterations: 38
currently lose_sum: 99.54907202720642
time_elpased: 1.585
batch start
#iterations: 39
currently lose_sum: 99.1821637749672
time_elpased: 1.544
start validation test
0.598659793814433
0.623449686740826
0.5018009673767624
0.5560497206066827
0.5988298444236906
65.389
batch start
#iterations: 40
currently lose_sum: 99.32540845870972
time_elpased: 1.596
batch start
#iterations: 41
currently lose_sum: 99.417043030262
time_elpased: 1.579
batch start
#iterations: 42
currently lose_sum: 99.38048148155212
time_elpased: 1.56
batch start
#iterations: 43
currently lose_sum: 99.3033674955368
time_elpased: 1.561
batch start
#iterations: 44
currently lose_sum: 99.38883417844772
time_elpased: 1.578
batch start
#iterations: 45
currently lose_sum: 99.2385241985321
time_elpased: 1.584
batch start
#iterations: 46
currently lose_sum: 99.3691571354866
time_elpased: 1.612
batch start
#iterations: 47
currently lose_sum: 99.3131931424141
time_elpased: 1.553
batch start
#iterations: 48
currently lose_sum: 99.40755361318588
time_elpased: 1.565
batch start
#iterations: 49
currently lose_sum: 99.2861077785492
time_elpased: 1.557
batch start
#iterations: 50
currently lose_sum: 99.33662277460098
time_elpased: 1.585
batch start
#iterations: 51
currently lose_sum: 99.40860199928284
time_elpased: 1.563
batch start
#iterations: 52
currently lose_sum: 99.43986558914185
time_elpased: 1.586
batch start
#iterations: 53
currently lose_sum: 98.99641555547714
time_elpased: 1.571
batch start
#iterations: 54
currently lose_sum: 99.31533294916153
time_elpased: 1.57
batch start
#iterations: 55
currently lose_sum: 99.43532031774521
time_elpased: 1.544
batch start
#iterations: 56
currently lose_sum: 99.10860282182693
time_elpased: 1.562
batch start
#iterations: 57
currently lose_sum: 99.11606460809708
time_elpased: 1.613
batch start
#iterations: 58
currently lose_sum: 99.17316675186157
time_elpased: 1.574
batch start
#iterations: 59
currently lose_sum: 98.99774289131165
time_elpased: 1.593
start validation test
0.5966494845360825
0.6230169050715214
0.4930534115467737
0.5504682024472913
0.5968313634208102
65.288
batch start
#iterations: 60
currently lose_sum: 99.13348400592804
time_elpased: 1.596
batch start
#iterations: 61
currently lose_sum: 99.28019469976425
time_elpased: 1.581
batch start
#iterations: 62
currently lose_sum: 99.05080592632294
time_elpased: 1.558
batch start
#iterations: 63
currently lose_sum: 99.07437258958817
time_elpased: 1.585
batch start
#iterations: 64
currently lose_sum: 99.0517298579216
time_elpased: 1.555
batch start
#iterations: 65
currently lose_sum: 99.09169501066208
time_elpased: 1.619
batch start
#iterations: 66
currently lose_sum: 99.05377072095871
time_elpased: 1.543
batch start
#iterations: 67
currently lose_sum: 98.97311353683472
time_elpased: 1.523
batch start
#iterations: 68
currently lose_sum: 98.96733963489532
time_elpased: 1.604
batch start
#iterations: 69
currently lose_sum: 99.05759608745575
time_elpased: 1.538
batch start
#iterations: 70
currently lose_sum: 99.05180811882019
time_elpased: 1.55
batch start
#iterations: 71
currently lose_sum: 98.943712413311
time_elpased: 1.546
batch start
#iterations: 72
currently lose_sum: 99.10602086782455
time_elpased: 1.588
batch start
#iterations: 73
currently lose_sum: 98.84325885772705
time_elpased: 1.625
batch start
#iterations: 74
currently lose_sum: 98.8992954492569
time_elpased: 1.571
batch start
#iterations: 75
currently lose_sum: 99.12530618906021
time_elpased: 1.647
batch start
#iterations: 76
currently lose_sum: 99.07279640436172
time_elpased: 1.531
batch start
#iterations: 77
currently lose_sum: 99.01817089319229
time_elpased: 1.575
batch start
#iterations: 78
currently lose_sum: 98.90816181898117
time_elpased: 1.601
batch start
#iterations: 79
currently lose_sum: 99.1730243563652
time_elpased: 1.621
start validation test
0.5957731958762886
0.6360865147336333
0.45096223114129874
0.5277610502228111
0.5960274338604356
65.173
batch start
#iterations: 80
currently lose_sum: 98.93371140956879
time_elpased: 1.576
batch start
#iterations: 81
currently lose_sum: 98.88573485612869
time_elpased: 1.597
batch start
#iterations: 82
currently lose_sum: 98.69668644666672
time_elpased: 1.57
batch start
#iterations: 83
currently lose_sum: 99.068812251091
time_elpased: 1.544
batch start
#iterations: 84
currently lose_sum: 99.03153771162033
time_elpased: 1.548
batch start
#iterations: 85
currently lose_sum: 98.64520925283432
time_elpased: 1.624
batch start
#iterations: 86
currently lose_sum: 98.84273588657379
time_elpased: 1.551
batch start
#iterations: 87
currently lose_sum: 98.90150272846222
time_elpased: 1.598
batch start
#iterations: 88
currently lose_sum: 98.89124530553818
time_elpased: 1.563
batch start
#iterations: 89
currently lose_sum: 98.98946976661682
time_elpased: 1.552
batch start
#iterations: 90
currently lose_sum: 98.78815931081772
time_elpased: 1.575
batch start
#iterations: 91
currently lose_sum: 98.75904989242554
time_elpased: 1.524
batch start
#iterations: 92
currently lose_sum: 98.75197571516037
time_elpased: 1.526
batch start
#iterations: 93
currently lose_sum: 98.80118483304977
time_elpased: 1.631
batch start
#iterations: 94
currently lose_sum: 98.81440353393555
time_elpased: 1.567
batch start
#iterations: 95
currently lose_sum: 98.8350270986557
time_elpased: 1.584
batch start
#iterations: 96
currently lose_sum: 98.8241360783577
time_elpased: 1.587
batch start
#iterations: 97
currently lose_sum: 98.85026735067368
time_elpased: 1.62
batch start
#iterations: 98
currently lose_sum: 98.61140447854996
time_elpased: 1.59
batch start
#iterations: 99
currently lose_sum: 98.7576595544815
time_elpased: 1.644
start validation test
0.5770103092783505
0.5874522514179882
0.5222805392610889
0.5529527130093702
0.577106395831102
65.896
batch start
#iterations: 100
currently lose_sum: 98.85457241535187
time_elpased: 1.608
batch start
#iterations: 101
currently lose_sum: 98.86669319868088
time_elpased: 1.642
batch start
#iterations: 102
currently lose_sum: 98.68866699934006
time_elpased: 1.57
batch start
#iterations: 103
currently lose_sum: 98.89607280492783
time_elpased: 1.55
batch start
#iterations: 104
currently lose_sum: 98.69125235080719
time_elpased: 1.615
batch start
#iterations: 105
currently lose_sum: 98.666000187397
time_elpased: 1.541
batch start
#iterations: 106
currently lose_sum: 98.73951023817062
time_elpased: 1.573
batch start
#iterations: 107
currently lose_sum: 98.70157498121262
time_elpased: 1.622
batch start
#iterations: 108
currently lose_sum: 98.56735438108444
time_elpased: 1.637
batch start
#iterations: 109
currently lose_sum: 98.5880212187767
time_elpased: 1.585
batch start
#iterations: 110
currently lose_sum: 98.64796870946884
time_elpased: 1.622
batch start
#iterations: 111
currently lose_sum: 98.90679407119751
time_elpased: 1.608
batch start
#iterations: 112
currently lose_sum: 98.62622606754303
time_elpased: 1.564
batch start
#iterations: 113
currently lose_sum: 98.69672441482544
time_elpased: 1.556
batch start
#iterations: 114
currently lose_sum: 98.54637062549591
time_elpased: 1.567
batch start
#iterations: 115
currently lose_sum: 98.64344382286072
time_elpased: 1.565
batch start
#iterations: 116
currently lose_sum: 98.80481117963791
time_elpased: 1.525
batch start
#iterations: 117
currently lose_sum: 98.63595634698868
time_elpased: 1.643
batch start
#iterations: 118
currently lose_sum: 98.60317087173462
time_elpased: 1.565
batch start
#iterations: 119
currently lose_sum: 98.75389301776886
time_elpased: 1.546
start validation test
0.6004123711340206
0.6160935838355193
0.5365853658536586
0.5735973597359736
0.6005244292864286
65.024
batch start
#iterations: 120
currently lose_sum: 98.51345300674438
time_elpased: 1.601
batch start
#iterations: 121
currently lose_sum: 98.70352560281754
time_elpased: 1.582
batch start
#iterations: 122
currently lose_sum: 98.64300227165222
time_elpased: 1.59
batch start
#iterations: 123
currently lose_sum: 98.7408259510994
time_elpased: 1.565
batch start
#iterations: 124
currently lose_sum: 98.89091765880585
time_elpased: 1.539
batch start
#iterations: 125
currently lose_sum: 98.78621697425842
time_elpased: 1.643
batch start
#iterations: 126
currently lose_sum: 98.65915459394455
time_elpased: 1.583
batch start
#iterations: 127
currently lose_sum: 98.71843284368515
time_elpased: 1.539
batch start
#iterations: 128
currently lose_sum: 98.26626408100128
time_elpased: 1.635
batch start
#iterations: 129
currently lose_sum: 98.37021768093109
time_elpased: 1.591
batch start
#iterations: 130
currently lose_sum: 98.75101453065872
time_elpased: 1.568
batch start
#iterations: 131
currently lose_sum: 98.5299386382103
time_elpased: 1.551
batch start
#iterations: 132
currently lose_sum: 98.49196261167526
time_elpased: 1.642
batch start
#iterations: 133
currently lose_sum: 98.29052114486694
time_elpased: 1.61
batch start
#iterations: 134
currently lose_sum: 98.33654779195786
time_elpased: 1.567
batch start
#iterations: 135
currently lose_sum: 98.38739913702011
time_elpased: 1.586
batch start
#iterations: 136
currently lose_sum: 98.65276348590851
time_elpased: 1.567
batch start
#iterations: 137
currently lose_sum: 98.50319981575012
time_elpased: 1.58
batch start
#iterations: 138
currently lose_sum: 98.70710456371307
time_elpased: 1.562
batch start
#iterations: 139
currently lose_sum: 98.55834823846817
time_elpased: 1.571
start validation test
0.5922680412371134
0.6321099575961397
0.4448903982710713
0.5222275912056052
0.5925267854207779
65.205
batch start
#iterations: 140
currently lose_sum: 98.5354905128479
time_elpased: 1.592
batch start
#iterations: 141
currently lose_sum: 98.33756399154663
time_elpased: 1.572
batch start
#iterations: 142
currently lose_sum: 98.50283116102219
time_elpased: 1.602
batch start
#iterations: 143
currently lose_sum: 98.15777468681335
time_elpased: 1.541
batch start
#iterations: 144
currently lose_sum: 98.29079991579056
time_elpased: 1.652
batch start
#iterations: 145
currently lose_sum: 98.42081415653229
time_elpased: 1.659
batch start
#iterations: 146
currently lose_sum: 98.45814043283463
time_elpased: 1.588
batch start
#iterations: 147
currently lose_sum: 98.49811154603958
time_elpased: 1.621
batch start
#iterations: 148
currently lose_sum: 98.14533632993698
time_elpased: 1.614
batch start
#iterations: 149
currently lose_sum: 98.58047658205032
time_elpased: 1.576
batch start
#iterations: 150
currently lose_sum: 98.34109228849411
time_elpased: 1.571
batch start
#iterations: 151
currently lose_sum: 98.43010741472244
time_elpased: 1.592
batch start
#iterations: 152
currently lose_sum: 98.47342032194138
time_elpased: 1.582
batch start
#iterations: 153
currently lose_sum: 98.05175220966339
time_elpased: 1.632
batch start
#iterations: 154
currently lose_sum: 98.44111359119415
time_elpased: 1.527
batch start
#iterations: 155
currently lose_sum: 98.4053036570549
time_elpased: 1.563
batch start
#iterations: 156
currently lose_sum: 98.32688957452774
time_elpased: 1.548
batch start
#iterations: 157
currently lose_sum: 98.37731486558914
time_elpased: 1.56
batch start
#iterations: 158
currently lose_sum: 98.25967812538147
time_elpased: 1.544
batch start
#iterations: 159
currently lose_sum: 98.50880706310272
time_elpased: 1.582
start validation test
0.5908762886597938
0.6170744540910287
0.48276216939384586
0.5417171892141578
0.5910660996716208
65.079
batch start
#iterations: 160
currently lose_sum: 98.40572530031204
time_elpased: 1.601
batch start
#iterations: 161
currently lose_sum: 98.40790903568268
time_elpased: 1.624
batch start
#iterations: 162
currently lose_sum: 98.1606034040451
time_elpased: 1.555
batch start
#iterations: 163
currently lose_sum: 98.54312288761139
time_elpased: 1.558
batch start
#iterations: 164
currently lose_sum: 98.40461760759354
time_elpased: 1.525
batch start
#iterations: 165
currently lose_sum: 98.22416263818741
time_elpased: 1.63
batch start
#iterations: 166
currently lose_sum: 98.19184595346451
time_elpased: 1.565
batch start
#iterations: 167
currently lose_sum: 98.17352122068405
time_elpased: 1.574
batch start
#iterations: 168
currently lose_sum: 98.3890084028244
time_elpased: 1.517
batch start
#iterations: 169
currently lose_sum: 98.29500246047974
time_elpased: 1.576
batch start
#iterations: 170
currently lose_sum: 98.39862912893295
time_elpased: 1.572
batch start
#iterations: 171
currently lose_sum: 98.15657287836075
time_elpased: 1.555
batch start
#iterations: 172
currently lose_sum: 98.09157210588455
time_elpased: 1.602
batch start
#iterations: 173
currently lose_sum: 98.21551531553268
time_elpased: 1.563
batch start
#iterations: 174
currently lose_sum: 98.26363319158554
time_elpased: 1.565
batch start
#iterations: 175
currently lose_sum: 98.38715171813965
time_elpased: 1.534
batch start
#iterations: 176
currently lose_sum: 98.21940231323242
time_elpased: 1.567
batch start
#iterations: 177
currently lose_sum: 98.49295508861542
time_elpased: 1.544
batch start
#iterations: 178
currently lose_sum: 98.35748088359833
time_elpased: 1.566
batch start
#iterations: 179
currently lose_sum: 97.94657498598099
time_elpased: 1.554
start validation test
0.5847938144329897
0.6002170767004341
0.5121951219512195
0.5527236381809095
0.5849212726352194
65.359
batch start
#iterations: 180
currently lose_sum: 98.21666288375854
time_elpased: 1.646
batch start
#iterations: 181
currently lose_sum: 98.45190167427063
time_elpased: 1.542
batch start
#iterations: 182
currently lose_sum: 98.35285472869873
time_elpased: 1.61
batch start
#iterations: 183
currently lose_sum: 98.23151171207428
time_elpased: 1.579
batch start
#iterations: 184
currently lose_sum: 98.0473102927208
time_elpased: 1.556
batch start
#iterations: 185
currently lose_sum: 98.19882780313492
time_elpased: 1.61
batch start
#iterations: 186
currently lose_sum: 98.1350120306015
time_elpased: 1.577
batch start
#iterations: 187
currently lose_sum: 98.1662546992302
time_elpased: 1.546
batch start
#iterations: 188
currently lose_sum: 97.93478137254715
time_elpased: 1.567
batch start
#iterations: 189
currently lose_sum: 97.92588633298874
time_elpased: 1.603
batch start
#iterations: 190
currently lose_sum: 98.3231395483017
time_elpased: 1.558
batch start
#iterations: 191
currently lose_sum: 98.15340602397919
time_elpased: 1.575
batch start
#iterations: 192
currently lose_sum: 98.28477919101715
time_elpased: 1.571
batch start
#iterations: 193
currently lose_sum: 98.19160890579224
time_elpased: 1.646
batch start
#iterations: 194
currently lose_sum: 98.01263111829758
time_elpased: 1.564
batch start
#iterations: 195
currently lose_sum: 98.04359579086304
time_elpased: 1.518
batch start
#iterations: 196
currently lose_sum: 97.87267476320267
time_elpased: 1.569
batch start
#iterations: 197
currently lose_sum: 97.86531859636307
time_elpased: 1.554
batch start
#iterations: 198
currently lose_sum: 98.07077264785767
time_elpased: 1.551
batch start
#iterations: 199
currently lose_sum: 98.02559196949005
time_elpased: 1.612
start validation test
0.5960824742268042
0.6217160605668436
0.4943912730266543
0.5507911029580371
0.5962610088152998
64.928
batch start
#iterations: 200
currently lose_sum: 97.92965412139893
time_elpased: 1.611
batch start
#iterations: 201
currently lose_sum: 98.02928167581558
time_elpased: 1.569
batch start
#iterations: 202
currently lose_sum: 97.65637147426605
time_elpased: 1.566
batch start
#iterations: 203
currently lose_sum: 97.78377836942673
time_elpased: 1.572
batch start
#iterations: 204
currently lose_sum: 98.09735751152039
time_elpased: 1.591
batch start
#iterations: 205
currently lose_sum: 97.94378364086151
time_elpased: 1.609
batch start
#iterations: 206
currently lose_sum: 97.86248356103897
time_elpased: 1.561
batch start
#iterations: 207
currently lose_sum: 97.96004736423492
time_elpased: 1.572
batch start
#iterations: 208
currently lose_sum: 97.94011956453323
time_elpased: 1.567
batch start
#iterations: 209
currently lose_sum: 97.84675592184067
time_elpased: 1.563
batch start
#iterations: 210
currently lose_sum: 98.03568196296692
time_elpased: 1.568
batch start
#iterations: 211
currently lose_sum: 98.03658467531204
time_elpased: 1.597
batch start
#iterations: 212
currently lose_sum: 98.0410333275795
time_elpased: 1.617
batch start
#iterations: 213
currently lose_sum: 98.04487663507462
time_elpased: 1.538
batch start
#iterations: 214
currently lose_sum: 97.7780390381813
time_elpased: 1.559
batch start
#iterations: 215
currently lose_sum: 97.69078773260117
time_elpased: 1.549
batch start
#iterations: 216
currently lose_sum: 97.94679707288742
time_elpased: 1.622
batch start
#iterations: 217
currently lose_sum: 97.90902769565582
time_elpased: 1.554
batch start
#iterations: 218
currently lose_sum: 97.71740168333054
time_elpased: 1.647
batch start
#iterations: 219
currently lose_sum: 97.78572052717209
time_elpased: 1.597
start validation test
0.5941237113402061
0.6291520672740014
0.4619738602449316
0.5327557559933539
0.5943557207865161
65.019
batch start
#iterations: 220
currently lose_sum: 97.73021531105042
time_elpased: 1.584
batch start
#iterations: 221
currently lose_sum: 97.73068445920944
time_elpased: 1.554
batch start
#iterations: 222
currently lose_sum: 97.71275609731674
time_elpased: 1.569
batch start
#iterations: 223
currently lose_sum: 97.71598798036575
time_elpased: 1.572
batch start
#iterations: 224
currently lose_sum: 97.78765511512756
time_elpased: 1.607
batch start
#iterations: 225
currently lose_sum: 97.85578918457031
time_elpased: 1.594
batch start
#iterations: 226
currently lose_sum: 97.7162299156189
time_elpased: 1.565
batch start
#iterations: 227
currently lose_sum: 97.92268776893616
time_elpased: 1.569
batch start
#iterations: 228
currently lose_sum: 97.78216969966888
time_elpased: 1.586
batch start
#iterations: 229
currently lose_sum: 98.01422268152237
time_elpased: 1.572
batch start
#iterations: 230
currently lose_sum: 97.81807827949524
time_elpased: 1.657
batch start
#iterations: 231
currently lose_sum: 97.88765734434128
time_elpased: 1.56
batch start
#iterations: 232
currently lose_sum: 97.81311929225922
time_elpased: 1.624
batch start
#iterations: 233
currently lose_sum: 97.66349697113037
time_elpased: 1.616
batch start
#iterations: 234
currently lose_sum: 97.80893903970718
time_elpased: 1.549
batch start
#iterations: 235
currently lose_sum: 97.61297976970673
time_elpased: 1.534
batch start
#iterations: 236
currently lose_sum: 97.93870806694031
time_elpased: 1.548
batch start
#iterations: 237
currently lose_sum: 97.87838906049728
time_elpased: 1.549
batch start
#iterations: 238
currently lose_sum: 97.79199689626694
time_elpased: 1.593
batch start
#iterations: 239
currently lose_sum: 97.66602605581284
time_elpased: 1.597
start validation test
0.5928865979381444
0.6118833804896051
0.5118863846866317
0.5574358399641377
0.5930288063059308
65.207
batch start
#iterations: 240
currently lose_sum: 97.58236461877823
time_elpased: 1.615
batch start
#iterations: 241
currently lose_sum: 97.49472385644913
time_elpased: 1.535
batch start
#iterations: 242
currently lose_sum: 97.6139515042305
time_elpased: 1.58
batch start
#iterations: 243
currently lose_sum: 97.68579769134521
time_elpased: 1.606
batch start
#iterations: 244
currently lose_sum: 97.5272975564003
time_elpased: 1.566
batch start
#iterations: 245
currently lose_sum: 97.64013123512268
time_elpased: 1.565
batch start
#iterations: 246
currently lose_sum: 97.60787099599838
time_elpased: 1.543
batch start
#iterations: 247
currently lose_sum: 97.50617355108261
time_elpased: 1.539
batch start
#iterations: 248
currently lose_sum: 97.38643997907639
time_elpased: 1.547
batch start
#iterations: 249
currently lose_sum: 97.62085795402527
time_elpased: 1.52
batch start
#iterations: 250
currently lose_sum: 97.49971693754196
time_elpased: 1.551
batch start
#iterations: 251
currently lose_sum: 97.90737700462341
time_elpased: 1.553
batch start
#iterations: 252
currently lose_sum: 97.30102872848511
time_elpased: 1.532
batch start
#iterations: 253
currently lose_sum: 97.58966445922852
time_elpased: 1.541
batch start
#iterations: 254
currently lose_sum: 97.18852943181992
time_elpased: 1.602
batch start
#iterations: 255
currently lose_sum: 97.46196830272675
time_elpased: 1.531
batch start
#iterations: 256
currently lose_sum: 97.46368509531021
time_elpased: 1.593
batch start
#iterations: 257
currently lose_sum: 97.38607639074326
time_elpased: 1.585
batch start
#iterations: 258
currently lose_sum: 97.30506235361099
time_elpased: 1.592
batch start
#iterations: 259
currently lose_sum: 97.3322988152504
time_elpased: 1.579
start validation test
0.5941752577319588
0.6315639269406392
0.455490377688587
0.5292675635276531
0.594418740429546
65.094
batch start
#iterations: 260
currently lose_sum: 97.59608727693558
time_elpased: 1.556
batch start
#iterations: 261
currently lose_sum: 97.22246712446213
time_elpased: 1.581
batch start
#iterations: 262
currently lose_sum: 97.42455810308456
time_elpased: 1.561
batch start
#iterations: 263
currently lose_sum: 97.64629930257797
time_elpased: 1.576
batch start
#iterations: 264
currently lose_sum: 97.77709776163101
time_elpased: 1.623
batch start
#iterations: 265
currently lose_sum: 97.43432438373566
time_elpased: 1.587
batch start
#iterations: 266
currently lose_sum: 97.61785155534744
time_elpased: 1.552
batch start
#iterations: 267
currently lose_sum: 97.08195066452026
time_elpased: 1.601
batch start
#iterations: 268
currently lose_sum: 97.55767852067947
time_elpased: 1.635
batch start
#iterations: 269
currently lose_sum: 97.4848141670227
time_elpased: 1.577
batch start
#iterations: 270
currently lose_sum: 97.55580657720566
time_elpased: 1.593
batch start
#iterations: 271
currently lose_sum: 97.27591371536255
time_elpased: 1.595
batch start
#iterations: 272
currently lose_sum: 97.23884725570679
time_elpased: 1.727
batch start
#iterations: 273
currently lose_sum: 97.44679141044617
time_elpased: 1.584
batch start
#iterations: 274
currently lose_sum: 97.17863589525223
time_elpased: 1.582
batch start
#iterations: 275
currently lose_sum: 97.52232348918915
time_elpased: 1.573
batch start
#iterations: 276
currently lose_sum: 97.47236430644989
time_elpased: 1.637
batch start
#iterations: 277
currently lose_sum: 97.23883616924286
time_elpased: 1.58
batch start
#iterations: 278
currently lose_sum: 96.9550352692604
time_elpased: 1.593
batch start
#iterations: 279
currently lose_sum: 97.32076400518417
time_elpased: 1.575
start validation test
0.5912371134020619
0.6169962026973943
0.4849233302459607
0.5430448311628444
0.5914237636461652
65.244
batch start
#iterations: 280
currently lose_sum: 97.1302524805069
time_elpased: 1.573
batch start
#iterations: 281
currently lose_sum: 97.21569246053696
time_elpased: 1.591
batch start
#iterations: 282
currently lose_sum: 97.4278953075409
time_elpased: 1.562
batch start
#iterations: 283
currently lose_sum: 97.24545741081238
time_elpased: 1.555
batch start
#iterations: 284
currently lose_sum: 97.2069274187088
time_elpased: 1.576
batch start
#iterations: 285
currently lose_sum: 97.27646315097809
time_elpased: 1.638
batch start
#iterations: 286
currently lose_sum: 97.21086794137955
time_elpased: 1.554
batch start
#iterations: 287
currently lose_sum: 97.10122096538544
time_elpased: 1.538
batch start
#iterations: 288
currently lose_sum: 96.98706710338593
time_elpased: 1.572
batch start
#iterations: 289
currently lose_sum: 97.2308549284935
time_elpased: 1.587
batch start
#iterations: 290
currently lose_sum: 97.0790855884552
time_elpased: 1.584
batch start
#iterations: 291
currently lose_sum: 97.15613090991974
time_elpased: 1.547
batch start
#iterations: 292
currently lose_sum: 96.97212165594101
time_elpased: 1.53
batch start
#iterations: 293
currently lose_sum: 97.35035771131516
time_elpased: 1.598
batch start
#iterations: 294
currently lose_sum: 97.18176877498627
time_elpased: 1.591
batch start
#iterations: 295
currently lose_sum: 97.24567139148712
time_elpased: 1.63
batch start
#iterations: 296
currently lose_sum: 97.30307227373123
time_elpased: 1.601
batch start
#iterations: 297
currently lose_sum: 96.9202196598053
time_elpased: 1.594
batch start
#iterations: 298
currently lose_sum: 97.19617229700089
time_elpased: 1.687
batch start
#iterations: 299
currently lose_sum: 97.21339201927185
time_elpased: 1.599
start validation test
0.5865979381443299
0.6047918982339138
0.5039621282288772
0.549792298192433
0.5867430180543334
65.279
batch start
#iterations: 300
currently lose_sum: 97.1431320309639
time_elpased: 1.6
batch start
#iterations: 301
currently lose_sum: 97.02525901794434
time_elpased: 1.622
batch start
#iterations: 302
currently lose_sum: 97.09463435411453
time_elpased: 1.611
batch start
#iterations: 303
currently lose_sum: 97.2374877333641
time_elpased: 1.574
batch start
#iterations: 304
currently lose_sum: 96.6945818066597
time_elpased: 1.593
batch start
#iterations: 305
currently lose_sum: 97.06228369474411
time_elpased: 1.524
batch start
#iterations: 306
currently lose_sum: 96.969142973423
time_elpased: 1.6
batch start
#iterations: 307
currently lose_sum: 97.24376678466797
time_elpased: 1.599
batch start
#iterations: 308
currently lose_sum: 96.97797966003418
time_elpased: 1.58
batch start
#iterations: 309
currently lose_sum: 96.97469878196716
time_elpased: 1.589
batch start
#iterations: 310
currently lose_sum: 96.86542785167694
time_elpased: 1.617
batch start
#iterations: 311
currently lose_sum: 97.32349240779877
time_elpased: 1.574
batch start
#iterations: 312
currently lose_sum: 97.09854006767273
time_elpased: 1.558
batch start
#iterations: 313
currently lose_sum: 96.8874032497406
time_elpased: 1.574
batch start
#iterations: 314
currently lose_sum: 97.01150059700012
time_elpased: 1.583
batch start
#iterations: 315
currently lose_sum: 96.85789602994919
time_elpased: 1.529
batch start
#iterations: 316
currently lose_sum: 96.97542828321457
time_elpased: 1.569
batch start
#iterations: 317
currently lose_sum: 96.85639429092407
time_elpased: 1.541
batch start
#iterations: 318
currently lose_sum: 97.12192344665527
time_elpased: 1.556
batch start
#iterations: 319
currently lose_sum: 97.00156581401825
time_elpased: 1.628
start validation test
0.5864948453608247
0.6166230906839136
0.46115056087269735
0.5276731040979745
0.5867149065852695
65.413
batch start
#iterations: 320
currently lose_sum: 96.83978021144867
time_elpased: 1.617
batch start
#iterations: 321
currently lose_sum: 96.86508846282959
time_elpased: 1.552
batch start
#iterations: 322
currently lose_sum: 96.63947868347168
time_elpased: 1.555
batch start
#iterations: 323
currently lose_sum: 96.90548926591873
time_elpased: 1.586
batch start
#iterations: 324
currently lose_sum: 96.68531596660614
time_elpased: 1.562
batch start
#iterations: 325
currently lose_sum: 97.0033712387085
time_elpased: 1.584
batch start
#iterations: 326
currently lose_sum: 96.86704611778259
time_elpased: 1.61
batch start
#iterations: 327
currently lose_sum: 96.89677834510803
time_elpased: 1.578
batch start
#iterations: 328
currently lose_sum: 96.84730857610703
time_elpased: 1.524
batch start
#iterations: 329
currently lose_sum: 96.6985205411911
time_elpased: 1.583
batch start
#iterations: 330
currently lose_sum: 96.64473444223404
time_elpased: 1.659
batch start
#iterations: 331
currently lose_sum: 96.8245747089386
time_elpased: 1.568
batch start
#iterations: 332
currently lose_sum: 96.66467142105103
time_elpased: 1.597
batch start
#iterations: 333
currently lose_sum: 96.69154369831085
time_elpased: 1.575
batch start
#iterations: 334
currently lose_sum: 96.66698509454727
time_elpased: 1.552
batch start
#iterations: 335
currently lose_sum: 96.43888694047928
time_elpased: 1.577
batch start
#iterations: 336
currently lose_sum: 96.79505228996277
time_elpased: 1.648
batch start
#iterations: 337
currently lose_sum: 96.70777344703674
time_elpased: 1.614
batch start
#iterations: 338
currently lose_sum: 96.59613394737244
time_elpased: 1.525
batch start
#iterations: 339
currently lose_sum: 96.71274209022522
time_elpased: 1.603
start validation test
0.5838144329896907
0.634959750287498
0.39775650921066175
0.48911667932169084
0.5841410863723453
65.864
batch start
#iterations: 340
currently lose_sum: 96.66566330194473
time_elpased: 1.531
batch start
#iterations: 341
currently lose_sum: 96.21270793676376
time_elpased: 1.486
batch start
#iterations: 342
currently lose_sum: 96.77890235185623
time_elpased: 1.517
batch start
#iterations: 343
currently lose_sum: 96.76348787546158
time_elpased: 1.536
batch start
#iterations: 344
currently lose_sum: 96.54916542768478
time_elpased: 1.509
batch start
#iterations: 345
currently lose_sum: 96.65107667446136
time_elpased: 1.494
batch start
#iterations: 346
currently lose_sum: 96.36995762586594
time_elpased: 1.49
batch start
#iterations: 347
currently lose_sum: 96.76421058177948
time_elpased: 1.552
batch start
#iterations: 348
currently lose_sum: 96.72896260023117
time_elpased: 1.488
batch start
#iterations: 349
currently lose_sum: 96.61556619405746
time_elpased: 1.509
batch start
#iterations: 350
currently lose_sum: 96.85176217556
time_elpased: 1.527
batch start
#iterations: 351
currently lose_sum: 96.49540549516678
time_elpased: 1.531
batch start
#iterations: 352
currently lose_sum: 96.69540137052536
time_elpased: 1.562
batch start
#iterations: 353
currently lose_sum: 96.59752345085144
time_elpased: 1.52
batch start
#iterations: 354
currently lose_sum: 96.61142456531525
time_elpased: 1.492
batch start
#iterations: 355
currently lose_sum: 96.33395093679428
time_elpased: 1.497
batch start
#iterations: 356
currently lose_sum: 96.69517743587494
time_elpased: 1.508
batch start
#iterations: 357
currently lose_sum: 96.86515164375305
time_elpased: 1.51
batch start
#iterations: 358
currently lose_sum: 96.60766154527664
time_elpased: 1.51
batch start
#iterations: 359
currently lose_sum: 96.45412451028824
time_elpased: 1.488
start validation test
0.5847422680412371
0.6221143949419203
0.43542245549037767
0.5122896234410946
0.5850044220031667
65.655
batch start
#iterations: 360
currently lose_sum: 96.43177819252014
time_elpased: 1.514
batch start
#iterations: 361
currently lose_sum: 96.5917295217514
time_elpased: 1.478
batch start
#iterations: 362
currently lose_sum: 96.67522078752518
time_elpased: 1.52
batch start
#iterations: 363
currently lose_sum: 96.40091443061829
time_elpased: 1.516
batch start
#iterations: 364
currently lose_sum: 96.49558609724045
time_elpased: 1.496
batch start
#iterations: 365
currently lose_sum: 96.74474602937698
time_elpased: 1.574
batch start
#iterations: 366
currently lose_sum: 96.06075578927994
time_elpased: 1.534
batch start
#iterations: 367
currently lose_sum: 96.34772086143494
time_elpased: 1.564
batch start
#iterations: 368
currently lose_sum: 96.1193876862526
time_elpased: 1.606
batch start
#iterations: 369
currently lose_sum: 96.50342309474945
time_elpased: 1.501
batch start
#iterations: 370
currently lose_sum: 96.18427908420563
time_elpased: 1.508
batch start
#iterations: 371
currently lose_sum: 96.37095642089844
time_elpased: 1.528
batch start
#iterations: 372
currently lose_sum: 96.0066551566124
time_elpased: 1.538
batch start
#iterations: 373
currently lose_sum: 96.10563570261002
time_elpased: 1.505
batch start
#iterations: 374
currently lose_sum: 96.34833627939224
time_elpased: 1.499
batch start
#iterations: 375
currently lose_sum: 96.624855697155
time_elpased: 1.499
batch start
#iterations: 376
currently lose_sum: 96.1516302227974
time_elpased: 1.52
batch start
#iterations: 377
currently lose_sum: 96.38834297657013
time_elpased: 1.517
batch start
#iterations: 378
currently lose_sum: 96.01198214292526
time_elpased: 1.532
batch start
#iterations: 379
currently lose_sum: 96.3081784248352
time_elpased: 1.498
start validation test
0.5784020618556701
0.602124833997344
0.4666049192137491
0.5257725981330086
0.578598338983101
66.052
batch start
#iterations: 380
currently lose_sum: 96.14256191253662
time_elpased: 1.527
batch start
#iterations: 381
currently lose_sum: 96.13060986995697
time_elpased: 1.465
batch start
#iterations: 382
currently lose_sum: 96.27332109212875
time_elpased: 1.52
batch start
#iterations: 383
currently lose_sum: 96.16523069143295
time_elpased: 1.497
batch start
#iterations: 384
currently lose_sum: 95.77028852701187
time_elpased: 1.517
batch start
#iterations: 385
currently lose_sum: 96.0745587348938
time_elpased: 1.524
batch start
#iterations: 386
currently lose_sum: 95.99742919206619
time_elpased: 1.551
batch start
#iterations: 387
currently lose_sum: 96.52464157342911
time_elpased: 1.539
batch start
#iterations: 388
currently lose_sum: 96.31462407112122
time_elpased: 1.503
batch start
#iterations: 389
currently lose_sum: 95.77069532871246
time_elpased: 1.532
batch start
#iterations: 390
currently lose_sum: 95.92347353696823
time_elpased: 1.515
batch start
#iterations: 391
currently lose_sum: 96.00336754322052
time_elpased: 1.52
batch start
#iterations: 392
currently lose_sum: 96.24656200408936
time_elpased: 1.545
batch start
#iterations: 393
currently lose_sum: 96.12865686416626
time_elpased: 1.532
batch start
#iterations: 394
currently lose_sum: 95.99019277095795
time_elpased: 1.535
batch start
#iterations: 395
currently lose_sum: 96.1911558508873
time_elpased: 1.477
batch start
#iterations: 396
currently lose_sum: 95.90593683719635
time_elpased: 1.496
batch start
#iterations: 397
currently lose_sum: 95.94629609584808
time_elpased: 1.498
batch start
#iterations: 398
currently lose_sum: 95.86463671922684
time_elpased: 1.499
batch start
#iterations: 399
currently lose_sum: 96.0344734787941
time_elpased: 1.486
start validation test
0.5880412371134021
0.6173948550428746
0.46681074405680767
0.5316455696202532
0.5882540759424799
65.768
acc: 0.593
pre: 0.619
rec: 0.490
F1: 0.547
auc: 0.623
