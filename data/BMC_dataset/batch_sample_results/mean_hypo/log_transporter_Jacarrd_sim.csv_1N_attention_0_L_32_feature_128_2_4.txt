start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.68598306179047
time_elpased: 1.655
batch start
#iterations: 1
currently lose_sum: 100.37876009941101
time_elpased: 1.654
batch start
#iterations: 2
currently lose_sum: 100.4549098610878
time_elpased: 1.693
batch start
#iterations: 3
currently lose_sum: 100.25518131256104
time_elpased: 1.619
batch start
#iterations: 4
currently lose_sum: 100.239741563797
time_elpased: 1.677
batch start
#iterations: 5
currently lose_sum: 100.38580363988876
time_elpased: 1.606
batch start
#iterations: 6
currently lose_sum: 100.39873552322388
time_elpased: 1.639
batch start
#iterations: 7
currently lose_sum: 100.13227081298828
time_elpased: 1.671
batch start
#iterations: 8
currently lose_sum: 100.22410184144974
time_elpased: 1.69
batch start
#iterations: 9
currently lose_sum: 100.30998945236206
time_elpased: 1.625
batch start
#iterations: 10
currently lose_sum: 100.11750334501266
time_elpased: 1.655
batch start
#iterations: 11
currently lose_sum: 100.15851587057114
time_elpased: 1.654
batch start
#iterations: 12
currently lose_sum: 100.20210200548172
time_elpased: 1.661
batch start
#iterations: 13
currently lose_sum: 100.02983146905899
time_elpased: 1.637
batch start
#iterations: 14
currently lose_sum: 100.05342727899551
time_elpased: 1.667
batch start
#iterations: 15
currently lose_sum: 99.92766487598419
time_elpased: 1.659
batch start
#iterations: 16
currently lose_sum: 99.99981820583344
time_elpased: 1.725
batch start
#iterations: 17
currently lose_sum: 99.93132084608078
time_elpased: 1.634
batch start
#iterations: 18
currently lose_sum: 99.92579555511475
time_elpased: 1.664
batch start
#iterations: 19
currently lose_sum: 99.8900254368782
time_elpased: 1.586
start validation test
0.5791237113402062
0.5910798122065728
0.5182669548214469
0.552283818610517
0.5792305547627837
65.895
batch start
#iterations: 20
currently lose_sum: 100.04865074157715
time_elpased: 1.633
batch start
#iterations: 21
currently lose_sum: 99.79060071706772
time_elpased: 1.715
batch start
#iterations: 22
currently lose_sum: 99.82228714227676
time_elpased: 1.638
batch start
#iterations: 23
currently lose_sum: 99.76368004083633
time_elpased: 1.656
batch start
#iterations: 24
currently lose_sum: 99.74619609117508
time_elpased: 1.631
batch start
#iterations: 25
currently lose_sum: 99.6726124882698
time_elpased: 1.647
batch start
#iterations: 26
currently lose_sum: 99.6998473405838
time_elpased: 1.644
batch start
#iterations: 27
currently lose_sum: 99.70390945672989
time_elpased: 1.623
batch start
#iterations: 28
currently lose_sum: 99.54002386331558
time_elpased: 1.649
batch start
#iterations: 29
currently lose_sum: 99.66510623693466
time_elpased: 1.618
batch start
#iterations: 30
currently lose_sum: 99.52097344398499
time_elpased: 1.618
batch start
#iterations: 31
currently lose_sum: 99.56804174184799
time_elpased: 1.68
batch start
#iterations: 32
currently lose_sum: 99.52825379371643
time_elpased: 1.679
batch start
#iterations: 33
currently lose_sum: 99.61066633462906
time_elpased: 1.68
batch start
#iterations: 34
currently lose_sum: 99.67059886455536
time_elpased: 1.779
batch start
#iterations: 35
currently lose_sum: 99.5117557644844
time_elpased: 1.718
batch start
#iterations: 36
currently lose_sum: 99.1362476348877
time_elpased: 1.594
batch start
#iterations: 37
currently lose_sum: 99.5414731502533
time_elpased: 1.671
batch start
#iterations: 38
currently lose_sum: 99.41956079006195
time_elpased: 1.683
batch start
#iterations: 39
currently lose_sum: 99.43635666370392
time_elpased: 1.695
start validation test
0.5939175257731959
0.6472849591542528
0.41586909539981476
0.5063909774436092
0.5942301172547974
65.524
batch start
#iterations: 40
currently lose_sum: 99.48784750699997
time_elpased: 1.649
batch start
#iterations: 41
currently lose_sum: 99.33755230903625
time_elpased: 1.649
batch start
#iterations: 42
currently lose_sum: 99.40363621711731
time_elpased: 1.709
batch start
#iterations: 43
currently lose_sum: 99.29783487319946
time_elpased: 1.713
batch start
#iterations: 44
currently lose_sum: 99.39832717180252
time_elpased: 1.639
batch start
#iterations: 45
currently lose_sum: 99.15398329496384
time_elpased: 1.646
batch start
#iterations: 46
currently lose_sum: 99.1733592748642
time_elpased: 1.615
batch start
#iterations: 47
currently lose_sum: 99.29027098417282
time_elpased: 1.681
batch start
#iterations: 48
currently lose_sum: 99.44526034593582
time_elpased: 1.642
batch start
#iterations: 49
currently lose_sum: 99.27918714284897
time_elpased: 1.669
batch start
#iterations: 50
currently lose_sum: 99.47582256793976
time_elpased: 1.627
batch start
#iterations: 51
currently lose_sum: 99.45205694437027
time_elpased: 1.662
batch start
#iterations: 52
currently lose_sum: 99.36793494224548
time_elpased: 1.675
batch start
#iterations: 53
currently lose_sum: 99.2299274802208
time_elpased: 1.674
batch start
#iterations: 54
currently lose_sum: 99.0259815454483
time_elpased: 1.695
batch start
#iterations: 55
currently lose_sum: 99.15723747015
time_elpased: 1.695
batch start
#iterations: 56
currently lose_sum: 99.1459292769432
time_elpased: 1.68
batch start
#iterations: 57
currently lose_sum: 99.21312636137009
time_elpased: 1.626
batch start
#iterations: 58
currently lose_sum: 99.1250981092453
time_elpased: 1.625
batch start
#iterations: 59
currently lose_sum: 99.14187204837799
time_elpased: 1.661
start validation test
0.5895360824742268
0.6399170389278876
0.4127817227539364
0.501845480137629
0.5898464020152002
65.577
batch start
#iterations: 60
currently lose_sum: 99.22036355733871
time_elpased: 1.663
batch start
#iterations: 61
currently lose_sum: 99.22079253196716
time_elpased: 1.632
batch start
#iterations: 62
currently lose_sum: 99.38354456424713
time_elpased: 1.664
batch start
#iterations: 63
currently lose_sum: 98.96406906843185
time_elpased: 1.675
batch start
#iterations: 64
currently lose_sum: 99.09872257709503
time_elpased: 1.674
batch start
#iterations: 65
currently lose_sum: 98.91558915376663
time_elpased: 1.642
batch start
#iterations: 66
currently lose_sum: 98.93259507417679
time_elpased: 1.647
batch start
#iterations: 67
currently lose_sum: 99.02120643854141
time_elpased: 1.651
batch start
#iterations: 68
currently lose_sum: 98.92359691858292
time_elpased: 1.62
batch start
#iterations: 69
currently lose_sum: 99.01305931806564
time_elpased: 1.649
batch start
#iterations: 70
currently lose_sum: 98.94894272089005
time_elpased: 1.663
batch start
#iterations: 71
currently lose_sum: 98.84420615434647
time_elpased: 1.63
batch start
#iterations: 72
currently lose_sum: 98.62359201908112
time_elpased: 1.638
batch start
#iterations: 73
currently lose_sum: 99.06762474775314
time_elpased: 1.701
batch start
#iterations: 74
currently lose_sum: 98.72379618883133
time_elpased: 1.623
batch start
#iterations: 75
currently lose_sum: 99.06205588579178
time_elpased: 1.623
batch start
#iterations: 76
currently lose_sum: 98.92297959327698
time_elpased: 1.703
batch start
#iterations: 77
currently lose_sum: 98.9594202041626
time_elpased: 1.67
batch start
#iterations: 78
currently lose_sum: 98.9351316690445
time_elpased: 1.67
batch start
#iterations: 79
currently lose_sum: 98.77337175607681
time_elpased: 1.644
start validation test
0.6077835051546392
0.6255060728744939
0.5405989502933004
0.5799613579906155
0.6079014580032029
64.766
batch start
#iterations: 80
currently lose_sum: 98.88357925415039
time_elpased: 1.624
batch start
#iterations: 81
currently lose_sum: 98.82985728979111
time_elpased: 1.653
batch start
#iterations: 82
currently lose_sum: 98.80561196804047
time_elpased: 1.622
batch start
#iterations: 83
currently lose_sum: 98.91738587617874
time_elpased: 1.607
batch start
#iterations: 84
currently lose_sum: 99.00883650779724
time_elpased: 1.673
batch start
#iterations: 85
currently lose_sum: 99.07962483167648
time_elpased: 1.668
batch start
#iterations: 86
currently lose_sum: 98.89277404546738
time_elpased: 1.657
batch start
#iterations: 87
currently lose_sum: 98.73195588588715
time_elpased: 1.699
batch start
#iterations: 88
currently lose_sum: 99.0006315112114
time_elpased: 1.663
batch start
#iterations: 89
currently lose_sum: 98.81463027000427
time_elpased: 1.62
batch start
#iterations: 90
currently lose_sum: 98.96604073047638
time_elpased: 1.641
batch start
#iterations: 91
currently lose_sum: 98.78142249584198
time_elpased: 1.643
batch start
#iterations: 92
currently lose_sum: 98.75460362434387
time_elpased: 1.64
batch start
#iterations: 93
currently lose_sum: 98.87391233444214
time_elpased: 1.675
batch start
#iterations: 94
currently lose_sum: 98.94768565893173
time_elpased: 1.63
batch start
#iterations: 95
currently lose_sum: 98.71189594268799
time_elpased: 1.653
batch start
#iterations: 96
currently lose_sum: 98.39444744586945
time_elpased: 1.644
batch start
#iterations: 97
currently lose_sum: 98.81607329845428
time_elpased: 1.655
batch start
#iterations: 98
currently lose_sum: 98.58932620286942
time_elpased: 1.676
batch start
#iterations: 99
currently lose_sum: 98.60613876581192
time_elpased: 1.641
start validation test
0.5957731958762886
0.6077462360648201
0.5442008850468252
0.5742208708871755
0.5958637390224315
65.234
batch start
#iterations: 100
currently lose_sum: 98.74379193782806
time_elpased: 1.585
batch start
#iterations: 101
currently lose_sum: 98.88780558109283
time_elpased: 1.67
batch start
#iterations: 102
currently lose_sum: 98.71400219202042
time_elpased: 1.636
batch start
#iterations: 103
currently lose_sum: 98.61559027433395
time_elpased: 1.715
batch start
#iterations: 104
currently lose_sum: 98.80646115541458
time_elpased: 1.602
batch start
#iterations: 105
currently lose_sum: 98.82542675733566
time_elpased: 1.654
batch start
#iterations: 106
currently lose_sum: 98.62215214967728
time_elpased: 1.694
batch start
#iterations: 107
currently lose_sum: 98.71800535917282
time_elpased: 1.666
batch start
#iterations: 108
currently lose_sum: 98.57619220018387
time_elpased: 1.633
batch start
#iterations: 109
currently lose_sum: 98.43233466148376
time_elpased: 1.631
batch start
#iterations: 110
currently lose_sum: 98.61609953641891
time_elpased: 1.725
batch start
#iterations: 111
currently lose_sum: 98.56025284528732
time_elpased: 1.649
batch start
#iterations: 112
currently lose_sum: 98.65631783008575
time_elpased: 1.642
batch start
#iterations: 113
currently lose_sum: 98.65937620401382
time_elpased: 1.636
batch start
#iterations: 114
currently lose_sum: 98.50649797916412
time_elpased: 1.662
batch start
#iterations: 115
currently lose_sum: 98.21418696641922
time_elpased: 1.692
batch start
#iterations: 116
currently lose_sum: 98.64195507764816
time_elpased: 1.652
batch start
#iterations: 117
currently lose_sum: 98.67002999782562
time_elpased: 1.659
batch start
#iterations: 118
currently lose_sum: 98.76001596450806
time_elpased: 1.659
batch start
#iterations: 119
currently lose_sum: 98.67122972011566
time_elpased: 1.638
start validation test
0.6044845360824742
0.6388209725618038
0.4839971184521972
0.5507348205398442
0.6046960703280297
64.679
batch start
#iterations: 120
currently lose_sum: 98.598668217659
time_elpased: 1.683
batch start
#iterations: 121
currently lose_sum: 98.63267755508423
time_elpased: 1.671
batch start
#iterations: 122
currently lose_sum: 98.25359070301056
time_elpased: 1.64
batch start
#iterations: 123
currently lose_sum: 98.41617435216904
time_elpased: 1.655
batch start
#iterations: 124
currently lose_sum: 98.68039000034332
time_elpased: 1.69
batch start
#iterations: 125
currently lose_sum: 98.40855741500854
time_elpased: 1.627
batch start
#iterations: 126
currently lose_sum: 98.4886229634285
time_elpased: 1.635
batch start
#iterations: 127
currently lose_sum: 98.37293124198914
time_elpased: 1.622
batch start
#iterations: 128
currently lose_sum: 98.32794857025146
time_elpased: 1.692
batch start
#iterations: 129
currently lose_sum: 98.25333493947983
time_elpased: 1.628
batch start
#iterations: 130
currently lose_sum: 98.25831949710846
time_elpased: 1.668
batch start
#iterations: 131
currently lose_sum: 98.30950516462326
time_elpased: 1.633
batch start
#iterations: 132
currently lose_sum: 98.2475534081459
time_elpased: 1.748
batch start
#iterations: 133
currently lose_sum: 98.33869314193726
time_elpased: 1.634
batch start
#iterations: 134
currently lose_sum: 98.13619250059128
time_elpased: 1.688
batch start
#iterations: 135
currently lose_sum: 98.6260871887207
time_elpased: 1.594
batch start
#iterations: 136
currently lose_sum: 98.35818141698837
time_elpased: 1.654
batch start
#iterations: 137
currently lose_sum: 98.64256489276886
time_elpased: 1.646
batch start
#iterations: 138
currently lose_sum: 98.2950257062912
time_elpased: 1.668
batch start
#iterations: 139
currently lose_sum: 98.43672281503677
time_elpased: 1.638
start validation test
0.594278350515464
0.6529158383035123
0.4055778532468869
0.5003491398463785
0.5946096433434682
64.959
batch start
#iterations: 140
currently lose_sum: 98.36528670787811
time_elpased: 1.627
batch start
#iterations: 141
currently lose_sum: 98.01587671041489
time_elpased: 1.614
batch start
#iterations: 142
currently lose_sum: 98.43885886669159
time_elpased: 1.635
batch start
#iterations: 143
currently lose_sum: 98.20659816265106
time_elpased: 1.75
batch start
#iterations: 144
currently lose_sum: 98.35792165994644
time_elpased: 1.659
batch start
#iterations: 145
currently lose_sum: 98.41725480556488
time_elpased: 1.678
batch start
#iterations: 146
currently lose_sum: 98.06739628314972
time_elpased: 1.683
batch start
#iterations: 147
currently lose_sum: 98.31518316268921
time_elpased: 1.681
batch start
#iterations: 148
currently lose_sum: 98.23511332273483
time_elpased: 1.606
batch start
#iterations: 149
currently lose_sum: 98.27792763710022
time_elpased: 1.697
batch start
#iterations: 150
currently lose_sum: 98.30682879686356
time_elpased: 1.692
batch start
#iterations: 151
currently lose_sum: 98.43563264608383
time_elpased: 1.674
batch start
#iterations: 152
currently lose_sum: 98.21423155069351
time_elpased: 1.642
batch start
#iterations: 153
currently lose_sum: 98.4506008028984
time_elpased: 1.642
batch start
#iterations: 154
currently lose_sum: 98.10645574331284
time_elpased: 1.663
batch start
#iterations: 155
currently lose_sum: 98.4390457868576
time_elpased: 1.653
batch start
#iterations: 156
currently lose_sum: 98.30598849058151
time_elpased: 1.574
batch start
#iterations: 157
currently lose_sum: 97.9411192536354
time_elpased: 1.619
batch start
#iterations: 158
currently lose_sum: 98.36561489105225
time_elpased: 1.644
batch start
#iterations: 159
currently lose_sum: 98.33631235361099
time_elpased: 1.627
start validation test
0.6055154639175258
0.6406761177753544
0.4836883811876093
0.5512226587697179
0.605729350151793
64.649
batch start
#iterations: 160
currently lose_sum: 98.13938015699387
time_elpased: 1.664
batch start
#iterations: 161
currently lose_sum: 98.19910538196564
time_elpased: 1.613
batch start
#iterations: 162
currently lose_sum: 98.21315068006516
time_elpased: 1.65
batch start
#iterations: 163
currently lose_sum: 97.9715946316719
time_elpased: 1.643
batch start
#iterations: 164
currently lose_sum: 98.18973088264465
time_elpased: 1.636
batch start
#iterations: 165
currently lose_sum: 98.11122274398804
time_elpased: 1.642
batch start
#iterations: 166
currently lose_sum: 98.04393607378006
time_elpased: 1.705
batch start
#iterations: 167
currently lose_sum: 98.15220355987549
time_elpased: 1.693
batch start
#iterations: 168
currently lose_sum: 98.07223308086395
time_elpased: 1.659
batch start
#iterations: 169
currently lose_sum: 98.12743467092514
time_elpased: 1.627
batch start
#iterations: 170
currently lose_sum: 98.11492437124252
time_elpased: 1.71
batch start
#iterations: 171
currently lose_sum: 98.15923923254013
time_elpased: 1.669
batch start
#iterations: 172
currently lose_sum: 98.17756068706512
time_elpased: 1.691
batch start
#iterations: 173
currently lose_sum: 98.0395735502243
time_elpased: 1.621
batch start
#iterations: 174
currently lose_sum: 98.25721722841263
time_elpased: 1.576
batch start
#iterations: 175
currently lose_sum: 98.21220129728317
time_elpased: 1.684
batch start
#iterations: 176
currently lose_sum: 98.01923871040344
time_elpased: 1.656
batch start
#iterations: 177
currently lose_sum: 97.93875920772552
time_elpased: 1.643
batch start
#iterations: 178
currently lose_sum: 98.15267592668533
time_elpased: 1.672
batch start
#iterations: 179
currently lose_sum: 98.02783936262131
time_elpased: 1.684
start validation test
0.599381443298969
0.636147277054459
0.4676340434290419
0.5390272835112694
0.5996127461800792
64.614
batch start
#iterations: 180
currently lose_sum: 98.13917738199234
time_elpased: 1.68
batch start
#iterations: 181
currently lose_sum: 98.05678671598434
time_elpased: 1.658
batch start
#iterations: 182
currently lose_sum: 97.94066625833511
time_elpased: 1.607
batch start
#iterations: 183
currently lose_sum: 98.12044155597687
time_elpased: 1.631
batch start
#iterations: 184
currently lose_sum: 97.97264003753662
time_elpased: 1.635
batch start
#iterations: 185
currently lose_sum: 97.88764977455139
time_elpased: 1.649
batch start
#iterations: 186
currently lose_sum: 98.24449056386948
time_elpased: 1.662
batch start
#iterations: 187
currently lose_sum: 98.08293783664703
time_elpased: 1.655
batch start
#iterations: 188
currently lose_sum: 97.87084770202637
time_elpased: 1.622
batch start
#iterations: 189
currently lose_sum: 97.69167524576187
time_elpased: 1.67
batch start
#iterations: 190
currently lose_sum: 97.90046107769012
time_elpased: 1.671
batch start
#iterations: 191
currently lose_sum: 98.06917542219162
time_elpased: 1.717
batch start
#iterations: 192
currently lose_sum: 98.08748203516006
time_elpased: 1.652
batch start
#iterations: 193
currently lose_sum: 98.19702166318893
time_elpased: 1.656
batch start
#iterations: 194
currently lose_sum: 97.49612724781036
time_elpased: 1.698
batch start
#iterations: 195
currently lose_sum: 97.69925379753113
time_elpased: 1.69
batch start
#iterations: 196
currently lose_sum: 97.78594601154327
time_elpased: 1.615
batch start
#iterations: 197
currently lose_sum: 98.08535373210907
time_elpased: 1.66
batch start
#iterations: 198
currently lose_sum: 97.86140048503876
time_elpased: 1.679
batch start
#iterations: 199
currently lose_sum: 97.5908173918724
time_elpased: 1.677
start validation test
0.591958762886598
0.6131139304107525
0.5023155294844087
0.552211788663876
0.592116145409353
64.948
batch start
#iterations: 200
currently lose_sum: 98.25669956207275
time_elpased: 1.664
batch start
#iterations: 201
currently lose_sum: 97.82297825813293
time_elpased: 1.643
batch start
#iterations: 202
currently lose_sum: 97.63620793819427
time_elpased: 1.665
batch start
#iterations: 203
currently lose_sum: 97.84073102474213
time_elpased: 1.685
batch start
#iterations: 204
currently lose_sum: 97.72001045942307
time_elpased: 1.712
batch start
#iterations: 205
currently lose_sum: 97.62054455280304
time_elpased: 1.661
batch start
#iterations: 206
currently lose_sum: 98.09292560815811
time_elpased: 1.674
batch start
#iterations: 207
currently lose_sum: 97.86282122135162
time_elpased: 1.733
batch start
#iterations: 208
currently lose_sum: 97.60381418466568
time_elpased: 1.704
batch start
#iterations: 209
currently lose_sum: 97.86303567886353
time_elpased: 1.761
batch start
#iterations: 210
currently lose_sum: 97.71577894687653
time_elpased: 1.665
batch start
#iterations: 211
currently lose_sum: 97.89632749557495
time_elpased: 1.659
batch start
#iterations: 212
currently lose_sum: 97.80264741182327
time_elpased: 1.678
batch start
#iterations: 213
currently lose_sum: 97.54393094778061
time_elpased: 1.6
batch start
#iterations: 214
currently lose_sum: 97.88840985298157
time_elpased: 1.674
batch start
#iterations: 215
currently lose_sum: 97.4292271733284
time_elpased: 1.63
batch start
#iterations: 216
currently lose_sum: 97.59153860807419
time_elpased: 1.673
batch start
#iterations: 217
currently lose_sum: 97.5308501124382
time_elpased: 1.613
batch start
#iterations: 218
currently lose_sum: 97.46768361330032
time_elpased: 1.71
batch start
#iterations: 219
currently lose_sum: 97.7287409901619
time_elpased: 1.629
start validation test
0.5964948453608248
0.6316743343092152
0.4662961819491613
0.5365304914150386
0.5967234291962062
64.907
batch start
#iterations: 220
currently lose_sum: 97.46759933233261
time_elpased: 1.663
batch start
#iterations: 221
currently lose_sum: 97.6894058585167
time_elpased: 1.613
batch start
#iterations: 222
currently lose_sum: 97.8483555316925
time_elpased: 1.685
batch start
#iterations: 223
currently lose_sum: 97.41557657718658
time_elpased: 1.68
batch start
#iterations: 224
currently lose_sum: 97.42690640687943
time_elpased: 1.645
batch start
#iterations: 225
currently lose_sum: 97.5378206372261
time_elpased: 1.659
batch start
#iterations: 226
currently lose_sum: 97.63447767496109
time_elpased: 1.658
batch start
#iterations: 227
currently lose_sum: 97.67563623189926
time_elpased: 1.72
batch start
#iterations: 228
currently lose_sum: 97.63983821868896
time_elpased: 1.645
batch start
#iterations: 229
currently lose_sum: 97.74128770828247
time_elpased: 1.674
batch start
#iterations: 230
currently lose_sum: 97.66127550601959
time_elpased: 1.646
batch start
#iterations: 231
currently lose_sum: 97.49153476953506
time_elpased: 1.677
batch start
#iterations: 232
currently lose_sum: 97.39397871494293
time_elpased: 1.629
batch start
#iterations: 233
currently lose_sum: 97.44748443365097
time_elpased: 1.655
batch start
#iterations: 234
currently lose_sum: 97.45974838733673
time_elpased: 1.621
batch start
#iterations: 235
currently lose_sum: 97.67626506090164
time_elpased: 1.673
batch start
#iterations: 236
currently lose_sum: 97.5676766037941
time_elpased: 1.674
batch start
#iterations: 237
currently lose_sum: 97.37772852182388
time_elpased: 1.676
batch start
#iterations: 238
currently lose_sum: 97.31555479764938
time_elpased: 1.657
batch start
#iterations: 239
currently lose_sum: 97.2702579498291
time_elpased: 1.695
start validation test
0.5981443298969072
0.6343168787582156
0.46681074405680767
0.5378230969883804
0.5983749062636615
64.887
batch start
#iterations: 240
currently lose_sum: 97.62108808755875
time_elpased: 1.651
batch start
#iterations: 241
currently lose_sum: 97.33185750246048
time_elpased: 1.638
batch start
#iterations: 242
currently lose_sum: 97.87378597259521
time_elpased: 1.693
batch start
#iterations: 243
currently lose_sum: 97.49092942476273
time_elpased: 1.616
batch start
#iterations: 244
currently lose_sum: 97.73673689365387
time_elpased: 1.636
batch start
#iterations: 245
currently lose_sum: 97.60582274198532
time_elpased: 1.668
batch start
#iterations: 246
currently lose_sum: 97.10903304815292
time_elpased: 1.63
batch start
#iterations: 247
currently lose_sum: 97.30549222230911
time_elpased: 1.636
batch start
#iterations: 248
currently lose_sum: 97.48335456848145
time_elpased: 1.695
batch start
#iterations: 249
currently lose_sum: 97.3630433678627
time_elpased: 1.617
batch start
#iterations: 250
currently lose_sum: 97.43984454870224
time_elpased: 1.631
batch start
#iterations: 251
currently lose_sum: 97.5584168434143
time_elpased: 1.634
batch start
#iterations: 252
currently lose_sum: 97.48772633075714
time_elpased: 1.683
batch start
#iterations: 253
currently lose_sum: 97.24465095996857
time_elpased: 1.666
batch start
#iterations: 254
currently lose_sum: 97.44749408960342
time_elpased: 1.631
batch start
#iterations: 255
currently lose_sum: 97.34029287099838
time_elpased: 1.608
batch start
#iterations: 256
currently lose_sum: 97.06383806467056
time_elpased: 1.704
batch start
#iterations: 257
currently lose_sum: 97.33294636011124
time_elpased: 1.69
batch start
#iterations: 258
currently lose_sum: 97.1149035692215
time_elpased: 1.693
batch start
#iterations: 259
currently lose_sum: 97.52182525396347
time_elpased: 1.646
start validation test
0.5978350515463917
0.6273779433284555
0.4853349799320778
0.5472902402228154
0.5980325627740529
64.792
batch start
#iterations: 260
currently lose_sum: 97.39164525270462
time_elpased: 1.671
batch start
#iterations: 261
currently lose_sum: 96.98184251785278
time_elpased: 1.621
batch start
#iterations: 262
currently lose_sum: 97.51977777481079
time_elpased: 1.591
batch start
#iterations: 263
currently lose_sum: 97.09579533338547
time_elpased: 1.687
batch start
#iterations: 264
currently lose_sum: 97.19768571853638
time_elpased: 1.651
batch start
#iterations: 265
currently lose_sum: 97.09101819992065
time_elpased: 1.64
batch start
#iterations: 266
currently lose_sum: 97.39016830921173
time_elpased: 1.626
batch start
#iterations: 267
currently lose_sum: 97.08354425430298
time_elpased: 1.631
batch start
#iterations: 268
currently lose_sum: 97.41901302337646
time_elpased: 1.748
batch start
#iterations: 269
currently lose_sum: 97.08463597297668
time_elpased: 1.665
batch start
#iterations: 270
currently lose_sum: 97.01833498477936
time_elpased: 1.711
batch start
#iterations: 271
currently lose_sum: 97.307468354702
time_elpased: 1.645
batch start
#iterations: 272
currently lose_sum: 97.205517411232
time_elpased: 1.691
batch start
#iterations: 273
currently lose_sum: 97.01196986436844
time_elpased: 1.676
batch start
#iterations: 274
currently lose_sum: 97.47167992591858
time_elpased: 1.7
batch start
#iterations: 275
currently lose_sum: 96.93443894386292
time_elpased: 1.651
batch start
#iterations: 276
currently lose_sum: 97.02275204658508
time_elpased: 1.66
batch start
#iterations: 277
currently lose_sum: 96.76659387350082
time_elpased: 1.648
batch start
#iterations: 278
currently lose_sum: 97.03995168209076
time_elpased: 1.679
batch start
#iterations: 279
currently lose_sum: 97.24340450763702
time_elpased: 1.717
start validation test
0.5928865979381444
0.6188422840716059
0.48739322836266336
0.545308002302821
0.593071807819667
65.052
batch start
#iterations: 280
currently lose_sum: 96.86831820011139
time_elpased: 1.656
batch start
#iterations: 281
currently lose_sum: 97.28852933645248
time_elpased: 1.712
batch start
#iterations: 282
currently lose_sum: 97.07805860042572
time_elpased: 1.716
batch start
#iterations: 283
currently lose_sum: 97.13943088054657
time_elpased: 1.622
batch start
#iterations: 284
currently lose_sum: 96.99379926919937
time_elpased: 1.727
batch start
#iterations: 285
currently lose_sum: 96.87980526685715
time_elpased: 1.669
batch start
#iterations: 286
currently lose_sum: 97.21546429395676
time_elpased: 1.628
batch start
#iterations: 287
currently lose_sum: 96.79190987348557
time_elpased: 1.669
batch start
#iterations: 288
currently lose_sum: 97.0188484787941
time_elpased: 1.655
batch start
#iterations: 289
currently lose_sum: 96.93600606918335
time_elpased: 1.686
batch start
#iterations: 290
currently lose_sum: 96.89011579751968
time_elpased: 1.723
batch start
#iterations: 291
currently lose_sum: 96.92638897895813
time_elpased: 1.731
batch start
#iterations: 292
currently lose_sum: 97.12855911254883
time_elpased: 1.622
batch start
#iterations: 293
currently lose_sum: 96.9088739156723
time_elpased: 1.647
batch start
#iterations: 294
currently lose_sum: 96.6226909160614
time_elpased: 1.664
batch start
#iterations: 295
currently lose_sum: 97.17374002933502
time_elpased: 1.638
batch start
#iterations: 296
currently lose_sum: 96.87713795900345
time_elpased: 1.606
batch start
#iterations: 297
currently lose_sum: 96.93100821971893
time_elpased: 1.665
batch start
#iterations: 298
currently lose_sum: 96.65339243412018
time_elpased: 1.669
batch start
#iterations: 299
currently lose_sum: 96.96071547269821
time_elpased: 1.602
start validation test
0.5877319587628866
0.6148296593186373
0.47360296387774004
0.5350540634809907
0.5879323298165938
65.421
batch start
#iterations: 300
currently lose_sum: 97.13704925775528
time_elpased: 1.626
batch start
#iterations: 301
currently lose_sum: 96.94213163852692
time_elpased: 1.651
batch start
#iterations: 302
currently lose_sum: 96.59832346439362
time_elpased: 1.64
batch start
#iterations: 303
currently lose_sum: 96.58806604146957
time_elpased: 1.637
batch start
#iterations: 304
currently lose_sum: 96.70651990175247
time_elpased: 1.664
batch start
#iterations: 305
currently lose_sum: 96.8570054769516
time_elpased: 1.665
batch start
#iterations: 306
currently lose_sum: 96.77726918458939
time_elpased: 1.651
batch start
#iterations: 307
currently lose_sum: 96.51923257112503
time_elpased: 1.611
batch start
#iterations: 308
currently lose_sum: 96.42835980653763
time_elpased: 1.687
batch start
#iterations: 309
currently lose_sum: 96.5347591638565
time_elpased: 1.651
batch start
#iterations: 310
currently lose_sum: 96.84781390428543
time_elpased: 1.652
batch start
#iterations: 311
currently lose_sum: 96.65162616968155
time_elpased: 1.587
batch start
#iterations: 312
currently lose_sum: 96.75292104482651
time_elpased: 1.656
batch start
#iterations: 313
currently lose_sum: 96.62263858318329
time_elpased: 1.633
batch start
#iterations: 314
currently lose_sum: 96.73109424114227
time_elpased: 1.672
batch start
#iterations: 315
currently lose_sum: 96.46738255023956
time_elpased: 1.62
batch start
#iterations: 316
currently lose_sum: 96.53754538297653
time_elpased: 1.612
batch start
#iterations: 317
currently lose_sum: 96.98963451385498
time_elpased: 1.648
batch start
#iterations: 318
currently lose_sum: 96.65665543079376
time_elpased: 1.63
batch start
#iterations: 319
currently lose_sum: 96.5628451704979
time_elpased: 1.663
start validation test
0.5905154639175257
0.6250528988573847
0.4560049397962334
0.5273116744019993
0.5907516178894417
65.244
batch start
#iterations: 320
currently lose_sum: 96.52771031856537
time_elpased: 1.66
batch start
#iterations: 321
currently lose_sum: 96.81105256080627
time_elpased: 1.688
batch start
#iterations: 322
currently lose_sum: 96.59614706039429
time_elpased: 1.676
batch start
#iterations: 323
currently lose_sum: 96.47806751728058
time_elpased: 1.608
batch start
#iterations: 324
currently lose_sum: 96.91238695383072
time_elpased: 1.714
batch start
#iterations: 325
currently lose_sum: 96.63770127296448
time_elpased: 1.631
batch start
#iterations: 326
currently lose_sum: 96.68143582344055
time_elpased: 1.688
batch start
#iterations: 327
currently lose_sum: 96.53734290599823
time_elpased: 1.678
batch start
#iterations: 328
currently lose_sum: 96.46105396747589
time_elpased: 1.709
batch start
#iterations: 329
currently lose_sum: 96.53358393907547
time_elpased: 1.705
batch start
#iterations: 330
currently lose_sum: 96.46025538444519
time_elpased: 1.699
batch start
#iterations: 331
currently lose_sum: 96.2356630563736
time_elpased: 1.65
batch start
#iterations: 332
currently lose_sum: 96.68644744157791
time_elpased: 1.704
batch start
#iterations: 333
currently lose_sum: 96.71024888753891
time_elpased: 1.682
batch start
#iterations: 334
currently lose_sum: 96.67084068059921
time_elpased: 1.652
batch start
#iterations: 335
currently lose_sum: 96.52927505970001
time_elpased: 1.637
batch start
#iterations: 336
currently lose_sum: 96.49435156583786
time_elpased: 1.618
batch start
#iterations: 337
currently lose_sum: 96.16320377588272
time_elpased: 1.632
batch start
#iterations: 338
currently lose_sum: 96.57406604290009
time_elpased: 1.658
batch start
#iterations: 339
currently lose_sum: 96.72465550899506
time_elpased: 1.633
start validation test
0.590979381443299
0.610545905707196
0.5064320263455799
0.5536367216065703
0.5911278173657054
65.406
batch start
#iterations: 340
currently lose_sum: 96.15663826465607
time_elpased: 1.676
batch start
#iterations: 341
currently lose_sum: 96.36147421598434
time_elpased: 1.639
batch start
#iterations: 342
currently lose_sum: 96.14871871471405
time_elpased: 1.616
batch start
#iterations: 343
currently lose_sum: 96.53420829772949
time_elpased: 1.651
batch start
#iterations: 344
currently lose_sum: 96.33760851621628
time_elpased: 1.641
batch start
#iterations: 345
currently lose_sum: 96.4812713265419
time_elpased: 1.624
batch start
#iterations: 346
currently lose_sum: 96.24089175462723
time_elpased: 1.693
batch start
#iterations: 347
currently lose_sum: 96.21252804994583
time_elpased: 1.619
batch start
#iterations: 348
currently lose_sum: 96.22738897800446
time_elpased: 1.628
batch start
#iterations: 349
currently lose_sum: 95.89261710643768
time_elpased: 1.661
batch start
#iterations: 350
currently lose_sum: 96.15163671970367
time_elpased: 1.649
batch start
#iterations: 351
currently lose_sum: 96.1132299900055
time_elpased: 1.649
batch start
#iterations: 352
currently lose_sum: 96.53540647029877
time_elpased: 1.635
batch start
#iterations: 353
currently lose_sum: 95.84974700212479
time_elpased: 1.646
batch start
#iterations: 354
currently lose_sum: 96.44517290592194
time_elpased: 1.673
batch start
#iterations: 355
currently lose_sum: 96.23680913448334
time_elpased: 1.63
batch start
#iterations: 356
currently lose_sum: 96.51034891605377
time_elpased: 1.711
batch start
#iterations: 357
currently lose_sum: 96.3506184220314
time_elpased: 1.691
batch start
#iterations: 358
currently lose_sum: 96.05253744125366
time_elpased: 1.68
batch start
#iterations: 359
currently lose_sum: 96.35883176326752
time_elpased: 1.657
start validation test
0.5892268041237113
0.6190087145969498
0.46783986827210045
0.5329113182111248
0.5894399176122457
65.441
batch start
#iterations: 360
currently lose_sum: 96.53680473566055
time_elpased: 1.623
batch start
#iterations: 361
currently lose_sum: 96.42310947179794
time_elpased: 1.681
batch start
#iterations: 362
currently lose_sum: 96.30954962968826
time_elpased: 1.624
batch start
#iterations: 363
currently lose_sum: 96.09090095758438
time_elpased: 1.692
batch start
#iterations: 364
currently lose_sum: 95.9416818022728
time_elpased: 1.661
batch start
#iterations: 365
currently lose_sum: 96.02667778730392
time_elpased: 1.644
batch start
#iterations: 366
currently lose_sum: 96.06155705451965
time_elpased: 1.656
batch start
#iterations: 367
currently lose_sum: 96.35524612665176
time_elpased: 1.653
batch start
#iterations: 368
currently lose_sum: 95.96418923139572
time_elpased: 1.657
batch start
#iterations: 369
currently lose_sum: 96.3181819319725
time_elpased: 1.649
batch start
#iterations: 370
currently lose_sum: 95.93916726112366
time_elpased: 1.657
batch start
#iterations: 371
currently lose_sum: 96.00564444065094
time_elpased: 1.646
batch start
#iterations: 372
currently lose_sum: 95.93046814203262
time_elpased: 1.653
batch start
#iterations: 373
currently lose_sum: 96.14173227548599
time_elpased: 1.633
batch start
#iterations: 374
currently lose_sum: 96.23230743408203
time_elpased: 1.691
batch start
#iterations: 375
currently lose_sum: 96.37124735116959
time_elpased: 1.719
batch start
#iterations: 376
currently lose_sum: 96.07316392660141
time_elpased: 1.684
batch start
#iterations: 377
currently lose_sum: 95.81724506616592
time_elpased: 1.664
batch start
#iterations: 378
currently lose_sum: 96.10220915079117
time_elpased: 1.672
batch start
#iterations: 379
currently lose_sum: 95.8530690073967
time_elpased: 1.591
start validation test
0.5867525773195876
0.6354797577303156
0.4103118246372337
0.49865549371521484
0.5870623462750353
65.627
batch start
#iterations: 380
currently lose_sum: 95.9979019165039
time_elpased: 1.691
batch start
#iterations: 381
currently lose_sum: 95.62195420265198
time_elpased: 1.674
batch start
#iterations: 382
currently lose_sum: 95.88632923364639
time_elpased: 1.705
batch start
#iterations: 383
currently lose_sum: 95.95087385177612
time_elpased: 1.699
batch start
#iterations: 384
currently lose_sum: 95.86634212732315
time_elpased: 1.69
batch start
#iterations: 385
currently lose_sum: 95.99230390787125
time_elpased: 1.704
batch start
#iterations: 386
currently lose_sum: 95.77246189117432
time_elpased: 1.637
batch start
#iterations: 387
currently lose_sum: 95.96460664272308
time_elpased: 1.609
batch start
#iterations: 388
currently lose_sum: 95.80206090211868
time_elpased: 1.695
batch start
#iterations: 389
currently lose_sum: 96.10117173194885
time_elpased: 1.691
batch start
#iterations: 390
currently lose_sum: 95.86448484659195
time_elpased: 1.615
batch start
#iterations: 391
currently lose_sum: 95.71437358856201
time_elpased: 1.683
batch start
#iterations: 392
currently lose_sum: 95.58154135942459
time_elpased: 1.667
batch start
#iterations: 393
currently lose_sum: 95.67972731590271
time_elpased: 1.672
batch start
#iterations: 394
currently lose_sum: 95.81198966503143
time_elpased: 1.681
batch start
#iterations: 395
currently lose_sum: 95.78234225511551
time_elpased: 1.69
batch start
#iterations: 396
currently lose_sum: 95.503741979599
time_elpased: 1.623
batch start
#iterations: 397
currently lose_sum: 96.02237117290497
time_elpased: 1.657
batch start
#iterations: 398
currently lose_sum: 95.90979039669037
time_elpased: 1.668
batch start
#iterations: 399
currently lose_sum: 95.73385775089264
time_elpased: 1.614
start validation test
0.5796907216494845
0.626476776177375
0.3983739837398374
0.48704076497232013
0.5800090511490678
66.285
acc: 0.597
pre: 0.633
rec: 0.464
F1: 0.536
auc: 0.633
