start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.4254459142685
time_elpased: 2.024
batch start
#iterations: 1
currently lose_sum: 100.40284943580627
time_elpased: 1.918
batch start
#iterations: 2
currently lose_sum: 100.34062564373016
time_elpased: 1.962
batch start
#iterations: 3
currently lose_sum: 100.31815898418427
time_elpased: 1.947
batch start
#iterations: 4
currently lose_sum: 100.25332129001617
time_elpased: 1.903
batch start
#iterations: 5
currently lose_sum: 100.2793493270874
time_elpased: 1.923
batch start
#iterations: 6
currently lose_sum: 100.26368075609207
time_elpased: 1.926
batch start
#iterations: 7
currently lose_sum: 100.22941863536835
time_elpased: 1.886
batch start
#iterations: 8
currently lose_sum: 100.19738155603409
time_elpased: 1.947
batch start
#iterations: 9
currently lose_sum: 100.08558136224747
time_elpased: 1.96
batch start
#iterations: 10
currently lose_sum: 100.16884624958038
time_elpased: 1.823
batch start
#iterations: 11
currently lose_sum: 100.02981954813004
time_elpased: 1.968
batch start
#iterations: 12
currently lose_sum: 99.99540686607361
time_elpased: 2.159
batch start
#iterations: 13
currently lose_sum: 99.84139454364777
time_elpased: 2.253
batch start
#iterations: 14
currently lose_sum: 99.91856563091278
time_elpased: 2.498
batch start
#iterations: 15
currently lose_sum: 100.00902491807938
time_elpased: 1.755
batch start
#iterations: 16
currently lose_sum: 99.8192053437233
time_elpased: 1.84
batch start
#iterations: 17
currently lose_sum: 99.8431014418602
time_elpased: 1.985
batch start
#iterations: 18
currently lose_sum: 99.67645061016083
time_elpased: 1.973
batch start
#iterations: 19
currently lose_sum: 99.77654337882996
time_elpased: 1.99
start validation test
0.5882474226804124
0.6131100353264425
0.4822476072861994
0.5398617511520737
0.5884335217056837
65.792
batch start
#iterations: 20
currently lose_sum: 99.73975306749344
time_elpased: 1.944
batch start
#iterations: 21
currently lose_sum: 99.82293409109116
time_elpased: 1.951
batch start
#iterations: 22
currently lose_sum: 99.61542791128159
time_elpased: 2.007
batch start
#iterations: 23
currently lose_sum: 99.93218451738358
time_elpased: 1.955
batch start
#iterations: 24
currently lose_sum: 99.61774450540543
time_elpased: 1.961
batch start
#iterations: 25
currently lose_sum: 99.75132513046265
time_elpased: 2.001
batch start
#iterations: 26
currently lose_sum: 99.59660303592682
time_elpased: 1.957
batch start
#iterations: 27
currently lose_sum: 99.76161694526672
time_elpased: 1.966
batch start
#iterations: 28
currently lose_sum: 99.53234338760376
time_elpased: 2.009
batch start
#iterations: 29
currently lose_sum: 99.54965221881866
time_elpased: 1.928
batch start
#iterations: 30
currently lose_sum: 99.50145769119263
time_elpased: 1.975
batch start
#iterations: 31
currently lose_sum: 99.55188739299774
time_elpased: 2.03
batch start
#iterations: 32
currently lose_sum: 99.40009677410126
time_elpased: 1.961
batch start
#iterations: 33
currently lose_sum: 99.64022344350815
time_elpased: 1.939
batch start
#iterations: 34
currently lose_sum: 99.47993475198746
time_elpased: 1.876
batch start
#iterations: 35
currently lose_sum: 99.64727932214737
time_elpased: 2.017
batch start
#iterations: 36
currently lose_sum: 99.39305436611176
time_elpased: 1.913
batch start
#iterations: 37
currently lose_sum: 99.39070922136307
time_elpased: 1.994
batch start
#iterations: 38
currently lose_sum: 99.4946556687355
time_elpased: 2.039
batch start
#iterations: 39
currently lose_sum: 99.1742137670517
time_elpased: 1.964
start validation test
0.5944845360824742
0.6186201590151321
0.49644952145723986
0.5508421353125892
0.5946566516715095
65.453
batch start
#iterations: 40
currently lose_sum: 99.27767860889435
time_elpased: 2.004
batch start
#iterations: 41
currently lose_sum: 99.39166927337646
time_elpased: 1.942
batch start
#iterations: 42
currently lose_sum: 99.41559910774231
time_elpased: 1.973
batch start
#iterations: 43
currently lose_sum: 99.30509722232819
time_elpased: 2.052
batch start
#iterations: 44
currently lose_sum: 99.3222239613533
time_elpased: 1.95
batch start
#iterations: 45
currently lose_sum: 99.3288204073906
time_elpased: 1.949
batch start
#iterations: 46
currently lose_sum: 99.19238412380219
time_elpased: 1.891
batch start
#iterations: 47
currently lose_sum: 99.23420584201813
time_elpased: 1.953
batch start
#iterations: 48
currently lose_sum: 99.46261942386627
time_elpased: 1.988
batch start
#iterations: 49
currently lose_sum: 99.2059001326561
time_elpased: 1.945
batch start
#iterations: 50
currently lose_sum: 99.25153541564941
time_elpased: 2.008
batch start
#iterations: 51
currently lose_sum: 99.30875331163406
time_elpased: 1.967
batch start
#iterations: 52
currently lose_sum: 99.48996621370316
time_elpased: 1.923
batch start
#iterations: 53
currently lose_sum: 98.99286729097366
time_elpased: 1.904
batch start
#iterations: 54
currently lose_sum: 99.25197422504425
time_elpased: 1.958
batch start
#iterations: 55
currently lose_sum: 99.33667021989822
time_elpased: 1.931
batch start
#iterations: 56
currently lose_sum: 99.12554156780243
time_elpased: 1.938
batch start
#iterations: 57
currently lose_sum: 99.14176630973816
time_elpased: 1.969
batch start
#iterations: 58
currently lose_sum: 99.28184813261032
time_elpased: 1.976
batch start
#iterations: 59
currently lose_sum: 99.02745598554611
time_elpased: 1.973
start validation test
0.5947938144329897
0.6211488250652741
0.4896573016363075
0.5476204177936352
0.5949783977973957
65.321
batch start
#iterations: 60
currently lose_sum: 99.06546258926392
time_elpased: 2.058
batch start
#iterations: 61
currently lose_sum: 99.21751779317856
time_elpased: 1.994
batch start
#iterations: 62
currently lose_sum: 99.06199717521667
time_elpased: 1.977
batch start
#iterations: 63
currently lose_sum: 99.07489967346191
time_elpased: 1.969
batch start
#iterations: 64
currently lose_sum: 98.9761289358139
time_elpased: 1.943
batch start
#iterations: 65
currently lose_sum: 99.12439841032028
time_elpased: 1.978
batch start
#iterations: 66
currently lose_sum: 99.06850630044937
time_elpased: 2.01
batch start
#iterations: 67
currently lose_sum: 98.93151390552521
time_elpased: 1.981
batch start
#iterations: 68
currently lose_sum: 98.88719087839127
time_elpased: 1.982
batch start
#iterations: 69
currently lose_sum: 98.98098069429398
time_elpased: 1.985
batch start
#iterations: 70
currently lose_sum: 98.94699448347092
time_elpased: 1.989
batch start
#iterations: 71
currently lose_sum: 98.9785897731781
time_elpased: 2.003
batch start
#iterations: 72
currently lose_sum: 99.0287469625473
time_elpased: 1.981
batch start
#iterations: 73
currently lose_sum: 98.74482917785645
time_elpased: 1.991
batch start
#iterations: 74
currently lose_sum: 98.89548498392105
time_elpased: 1.982
batch start
#iterations: 75
currently lose_sum: 99.02322107553482
time_elpased: 1.928
batch start
#iterations: 76
currently lose_sum: 99.00479519367218
time_elpased: 2.016
batch start
#iterations: 77
currently lose_sum: 99.06881260871887
time_elpased: 1.904
batch start
#iterations: 78
currently lose_sum: 98.84206563234329
time_elpased: 1.996
batch start
#iterations: 79
currently lose_sum: 99.06457829475403
time_elpased: 1.902
start validation test
0.5994329896907217
0.642543217111046
0.45137388082741586
0.5302544883032099
0.5996929302928776
64.752
batch start
#iterations: 80
currently lose_sum: 98.87452441453934
time_elpased: 1.88
batch start
#iterations: 81
currently lose_sum: 98.82045197486877
time_elpased: 1.941
batch start
#iterations: 82
currently lose_sum: 98.89374059438705
time_elpased: 1.893
batch start
#iterations: 83
currently lose_sum: 99.11114519834518
time_elpased: 1.949
batch start
#iterations: 84
currently lose_sum: 98.953289270401
time_elpased: 1.981
batch start
#iterations: 85
currently lose_sum: 98.5685852766037
time_elpased: 1.898
batch start
#iterations: 86
currently lose_sum: 98.81315279006958
time_elpased: 1.923
batch start
#iterations: 87
currently lose_sum: 98.87412029504776
time_elpased: 1.963
batch start
#iterations: 88
currently lose_sum: 98.84377783536911
time_elpased: 1.952
batch start
#iterations: 89
currently lose_sum: 98.88045144081116
time_elpased: 2.046
batch start
#iterations: 90
currently lose_sum: 98.7920350432396
time_elpased: 1.979
batch start
#iterations: 91
currently lose_sum: 98.74662470817566
time_elpased: 2.0
batch start
#iterations: 92
currently lose_sum: 98.75412327051163
time_elpased: 1.94
batch start
#iterations: 93
currently lose_sum: 98.74216866493225
time_elpased: 1.985
batch start
#iterations: 94
currently lose_sum: 98.79074382781982
time_elpased: 1.967
batch start
#iterations: 95
currently lose_sum: 98.9350329041481
time_elpased: 2.037
batch start
#iterations: 96
currently lose_sum: 98.74585086107254
time_elpased: 1.939
batch start
#iterations: 97
currently lose_sum: 98.95627814531326
time_elpased: 1.942
batch start
#iterations: 98
currently lose_sum: 98.48033487796783
time_elpased: 2.02
batch start
#iterations: 99
currently lose_sum: 98.71658283472061
time_elpased: 1.975
start validation test
0.5822680412371134
0.5940086257139526
0.5244417001132037
0.5570616528202886
0.582369564298056
65.738
batch start
#iterations: 100
currently lose_sum: 98.814857006073
time_elpased: 1.98
batch start
#iterations: 101
currently lose_sum: 98.74555104970932
time_elpased: 1.937
batch start
#iterations: 102
currently lose_sum: 98.65647959709167
time_elpased: 1.956
batch start
#iterations: 103
currently lose_sum: 98.96630436182022
time_elpased: 2.004
batch start
#iterations: 104
currently lose_sum: 98.64829218387604
time_elpased: 1.968
batch start
#iterations: 105
currently lose_sum: 98.72329699993134
time_elpased: 1.969
batch start
#iterations: 106
currently lose_sum: 98.84901463985443
time_elpased: 1.967
batch start
#iterations: 107
currently lose_sum: 98.64146882295609
time_elpased: 2.005
batch start
#iterations: 108
currently lose_sum: 98.59278583526611
time_elpased: 1.945
batch start
#iterations: 109
currently lose_sum: 98.58473801612854
time_elpased: 1.993
batch start
#iterations: 110
currently lose_sum: 98.69544720649719
time_elpased: 1.918
batch start
#iterations: 111
currently lose_sum: 98.86960071325302
time_elpased: 1.929
batch start
#iterations: 112
currently lose_sum: 98.7349197268486
time_elpased: 1.971
batch start
#iterations: 113
currently lose_sum: 98.74306881427765
time_elpased: 1.966
batch start
#iterations: 114
currently lose_sum: 98.4644056558609
time_elpased: 2.041
batch start
#iterations: 115
currently lose_sum: 98.6077252626419
time_elpased: 1.983
batch start
#iterations: 116
currently lose_sum: 98.7904109954834
time_elpased: 1.933
batch start
#iterations: 117
currently lose_sum: 98.73619782924652
time_elpased: 1.936
batch start
#iterations: 118
currently lose_sum: 98.6358568072319
time_elpased: 1.962
batch start
#iterations: 119
currently lose_sum: 98.78445184230804
time_elpased: 1.963
start validation test
0.6028865979381444
0.6237855122371172
0.521971801996501
0.5683549977588525
0.6030286563426686
64.692
batch start
#iterations: 120
currently lose_sum: 98.57154858112335
time_elpased: 1.92
batch start
#iterations: 121
currently lose_sum: 98.83238083124161
time_elpased: 1.925
batch start
#iterations: 122
currently lose_sum: 98.5659539103508
time_elpased: 2.053
batch start
#iterations: 123
currently lose_sum: 98.66149765253067
time_elpased: 1.922
batch start
#iterations: 124
currently lose_sum: 98.77646142244339
time_elpased: 2.044
batch start
#iterations: 125
currently lose_sum: 98.86839151382446
time_elpased: 1.939
batch start
#iterations: 126
currently lose_sum: 98.64926505088806
time_elpased: 1.961
batch start
#iterations: 127
currently lose_sum: 98.81109899282455
time_elpased: 1.946
batch start
#iterations: 128
currently lose_sum: 98.27752888202667
time_elpased: 1.965
batch start
#iterations: 129
currently lose_sum: 98.39052933454514
time_elpased: 1.972
batch start
#iterations: 130
currently lose_sum: 98.73236238956451
time_elpased: 1.919
batch start
#iterations: 131
currently lose_sum: 98.56202524900436
time_elpased: 2.016
batch start
#iterations: 132
currently lose_sum: 98.62275320291519
time_elpased: 2.03
batch start
#iterations: 133
currently lose_sum: 98.2788490653038
time_elpased: 2.032
batch start
#iterations: 134
currently lose_sum: 98.41134876012802
time_elpased: 1.996
batch start
#iterations: 135
currently lose_sum: 98.50381708145142
time_elpased: 2.048
batch start
#iterations: 136
currently lose_sum: 98.52531760931015
time_elpased: 1.919
batch start
#iterations: 137
currently lose_sum: 98.56781828403473
time_elpased: 1.954
batch start
#iterations: 138
currently lose_sum: 98.83014351129532
time_elpased: 1.97
batch start
#iterations: 139
currently lose_sum: 98.62229019403458
time_elpased: 1.994
start validation test
0.5951546391752577
0.6187986226246652
0.4993310692600597
0.5526825378744732
0.5953228722320127
65.267
batch start
#iterations: 140
currently lose_sum: 98.7964289188385
time_elpased: 1.931
batch start
#iterations: 141
currently lose_sum: 98.48955726623535
time_elpased: 1.976
batch start
#iterations: 142
currently lose_sum: 98.72815603017807
time_elpased: 1.907
batch start
#iterations: 143
currently lose_sum: 98.39486128091812
time_elpased: 1.98
batch start
#iterations: 144
currently lose_sum: 98.4556337594986
time_elpased: 1.967
batch start
#iterations: 145
currently lose_sum: 98.58201503753662
time_elpased: 1.871
batch start
#iterations: 146
currently lose_sum: 98.59565633535385
time_elpased: 2.016
batch start
#iterations: 147
currently lose_sum: 98.560129404068
time_elpased: 1.986
batch start
#iterations: 148
currently lose_sum: 98.37624031305313
time_elpased: 2.0
batch start
#iterations: 149
currently lose_sum: 98.6339847445488
time_elpased: 1.954
batch start
#iterations: 150
currently lose_sum: 98.46292585134506
time_elpased: 2.009
batch start
#iterations: 151
currently lose_sum: 98.60023474693298
time_elpased: 1.958
batch start
#iterations: 152
currently lose_sum: 98.59061938524246
time_elpased: 1.923
batch start
#iterations: 153
currently lose_sum: 98.11106371879578
time_elpased: 2.016
batch start
#iterations: 154
currently lose_sum: 98.51853227615356
time_elpased: 2.027
batch start
#iterations: 155
currently lose_sum: 98.46422147750854
time_elpased: 1.973
batch start
#iterations: 156
currently lose_sum: 98.46044826507568
time_elpased: 2.003
batch start
#iterations: 157
currently lose_sum: 98.56460684537888
time_elpased: 2.005
batch start
#iterations: 158
currently lose_sum: 98.42237788438797
time_elpased: 2.018
batch start
#iterations: 159
currently lose_sum: 98.56007069349289
time_elpased: 1.918
start validation test
0.5995876288659794
0.6198499569548641
0.5186786045075641
0.5647691618108472
0.599729677137599
64.862
batch start
#iterations: 160
currently lose_sum: 98.38344740867615
time_elpased: 1.955
batch start
#iterations: 161
currently lose_sum: 98.55522900819778
time_elpased: 2.018
batch start
#iterations: 162
currently lose_sum: 98.47150141000748
time_elpased: 2.03
batch start
#iterations: 163
currently lose_sum: 98.43481361865997
time_elpased: 1.906
batch start
#iterations: 164
currently lose_sum: 98.48673182725906
time_elpased: 1.915
batch start
#iterations: 165
currently lose_sum: 98.35555320978165
time_elpased: 2.024
batch start
#iterations: 166
currently lose_sum: 98.22925126552582
time_elpased: 1.978
batch start
#iterations: 167
currently lose_sum: 98.40098416805267
time_elpased: 1.95
batch start
#iterations: 168
currently lose_sum: 98.48944109678268
time_elpased: 1.996
batch start
#iterations: 169
currently lose_sum: 98.42174577713013
time_elpased: 2.003
batch start
#iterations: 170
currently lose_sum: 98.38990086317062
time_elpased: 1.993
batch start
#iterations: 171
currently lose_sum: 98.32920032739639
time_elpased: 2.012
batch start
#iterations: 172
currently lose_sum: 98.38880306482315
time_elpased: 2.073
batch start
#iterations: 173
currently lose_sum: 98.38260507583618
time_elpased: 1.995
batch start
#iterations: 174
currently lose_sum: 98.47165167331696
time_elpased: 2.002
batch start
#iterations: 175
currently lose_sum: 98.54907757043839
time_elpased: 1.997
batch start
#iterations: 176
currently lose_sum: 98.32510995864868
time_elpased: 1.991
batch start
#iterations: 177
currently lose_sum: 98.64017593860626
time_elpased: 1.975
batch start
#iterations: 178
currently lose_sum: 98.51765316724777
time_elpased: 1.93
batch start
#iterations: 179
currently lose_sum: 98.09743249416351
time_elpased: 2.01
start validation test
0.5886082474226804
0.6121157323688969
0.4877019656272512
0.5428718712411935
0.5887854039640955
65.497
batch start
#iterations: 180
currently lose_sum: 98.49673837423325
time_elpased: 1.981
batch start
#iterations: 181
currently lose_sum: 98.53541296720505
time_elpased: 1.967
batch start
#iterations: 182
currently lose_sum: 98.39971721172333
time_elpased: 1.907
batch start
#iterations: 183
currently lose_sum: 98.34404689073563
time_elpased: 1.981
batch start
#iterations: 184
currently lose_sum: 98.39313423633575
time_elpased: 1.91
batch start
#iterations: 185
currently lose_sum: 98.27553510665894
time_elpased: 1.965
batch start
#iterations: 186
currently lose_sum: 98.3891087770462
time_elpased: 1.983
batch start
#iterations: 187
currently lose_sum: 98.26439565420151
time_elpased: 2.033
batch start
#iterations: 188
currently lose_sum: 98.27287006378174
time_elpased: 1.956
batch start
#iterations: 189
currently lose_sum: 97.93108361959457
time_elpased: 1.979
batch start
#iterations: 190
currently lose_sum: 98.37804704904556
time_elpased: 1.954
batch start
#iterations: 191
currently lose_sum: 98.3336952328682
time_elpased: 2.062
batch start
#iterations: 192
currently lose_sum: 98.49057918787003
time_elpased: 1.906
batch start
#iterations: 193
currently lose_sum: 98.3873952627182
time_elpased: 2.024
batch start
#iterations: 194
currently lose_sum: 98.21244007349014
time_elpased: 1.926
batch start
#iterations: 195
currently lose_sum: 98.18905460834503
time_elpased: 1.989
batch start
#iterations: 196
currently lose_sum: 98.10824531316757
time_elpased: 1.971
batch start
#iterations: 197
currently lose_sum: 98.11794793605804
time_elpased: 2.016
batch start
#iterations: 198
currently lose_sum: 98.26963078975677
time_elpased: 1.932
batch start
#iterations: 199
currently lose_sum: 98.23906004428864
time_elpased: 2.036
start validation test
0.6035051546391753
0.6151484135107472
0.5566532880518679
0.5844408427876824
0.6035874103173726
64.907
batch start
#iterations: 200
currently lose_sum: 98.17172169685364
time_elpased: 1.976
batch start
#iterations: 201
currently lose_sum: 98.20407009124756
time_elpased: 1.917
batch start
#iterations: 202
currently lose_sum: 97.9138468503952
time_elpased: 2.026
batch start
#iterations: 203
currently lose_sum: 97.78991776704788
time_elpased: 1.927
batch start
#iterations: 204
currently lose_sum: 98.25638496875763
time_elpased: 1.951
batch start
#iterations: 205
currently lose_sum: 98.2649781703949
time_elpased: 2.101
batch start
#iterations: 206
currently lose_sum: 97.97062975168228
time_elpased: 1.994
batch start
#iterations: 207
currently lose_sum: 98.04422509670258
time_elpased: 1.976
batch start
#iterations: 208
currently lose_sum: 98.28812497854233
time_elpased: 1.979
batch start
#iterations: 209
currently lose_sum: 98.09034025669098
time_elpased: 1.938
batch start
#iterations: 210
currently lose_sum: 98.18036460876465
time_elpased: 1.995
batch start
#iterations: 211
currently lose_sum: 98.22289800643921
time_elpased: 1.984
batch start
#iterations: 212
currently lose_sum: 98.22630995512009
time_elpased: 2.008
batch start
#iterations: 213
currently lose_sum: 98.46614336967468
time_elpased: 1.933
batch start
#iterations: 214
currently lose_sum: 98.00946968793869
time_elpased: 2.011
batch start
#iterations: 215
currently lose_sum: 97.80844646692276
time_elpased: 1.969
batch start
#iterations: 216
currently lose_sum: 97.85301434993744
time_elpased: 1.993
batch start
#iterations: 217
currently lose_sum: 98.13612860441208
time_elpased: 2.001
batch start
#iterations: 218
currently lose_sum: 97.70402199029922
time_elpased: 2.023
batch start
#iterations: 219
currently lose_sum: 97.83668583631516
time_elpased: 1.965
start validation test
0.6018041237113402
0.6381031613976705
0.47360296387774004
0.5436824384192804
0.6020292006210965
64.982
batch start
#iterations: 220
currently lose_sum: 97.97919201850891
time_elpased: 2.068
batch start
#iterations: 221
currently lose_sum: 97.93294459581375
time_elpased: 2.055
batch start
#iterations: 222
currently lose_sum: 97.75690180063248
time_elpased: 1.915
batch start
#iterations: 223
currently lose_sum: 97.85146284103394
time_elpased: 1.915
batch start
#iterations: 224
currently lose_sum: 97.90399396419525
time_elpased: 2.059
batch start
#iterations: 225
currently lose_sum: 97.8149670958519
time_elpased: 2.099
batch start
#iterations: 226
currently lose_sum: 97.76848340034485
time_elpased: 1.999
batch start
#iterations: 227
currently lose_sum: 98.12493705749512
time_elpased: 1.901
batch start
#iterations: 228
currently lose_sum: 97.69931876659393
time_elpased: 2.024
batch start
#iterations: 229
currently lose_sum: 98.11443972587585
time_elpased: 1.932
batch start
#iterations: 230
currently lose_sum: 97.86765712499619
time_elpased: 1.93
batch start
#iterations: 231
currently lose_sum: 97.98816829919815
time_elpased: 1.959
batch start
#iterations: 232
currently lose_sum: 97.9761735200882
time_elpased: 1.952
batch start
#iterations: 233
currently lose_sum: 97.70676308870316
time_elpased: 1.997
batch start
#iterations: 234
currently lose_sum: 97.68198353052139
time_elpased: 1.941
batch start
#iterations: 235
currently lose_sum: 97.70740103721619
time_elpased: 1.962
batch start
#iterations: 236
currently lose_sum: 97.79140597581863
time_elpased: 2.039
batch start
#iterations: 237
currently lose_sum: 97.7367462515831
time_elpased: 1.997
batch start
#iterations: 238
currently lose_sum: 97.67108488082886
time_elpased: 2.002
batch start
#iterations: 239
currently lose_sum: 97.67906051874161
time_elpased: 2.029
start validation test
0.6055154639175258
0.642817603099917
0.47802819800349905
0.5483090361801334
0.6057392874763958
65.004
batch start
#iterations: 240
currently lose_sum: 97.70511001348495
time_elpased: 2.035
batch start
#iterations: 241
currently lose_sum: 97.45646876096725
time_elpased: 1.956
batch start
#iterations: 242
currently lose_sum: 97.58769464492798
time_elpased: 1.926
batch start
#iterations: 243
currently lose_sum: 97.6547360420227
time_elpased: 1.917
batch start
#iterations: 244
currently lose_sum: 97.54311782121658
time_elpased: 1.945
batch start
#iterations: 245
currently lose_sum: 97.5872603058815
time_elpased: 1.996
batch start
#iterations: 246
currently lose_sum: 97.64981877803802
time_elpased: 1.97
batch start
#iterations: 247
currently lose_sum: 97.57979488372803
time_elpased: 2.023
batch start
#iterations: 248
currently lose_sum: 97.66556006669998
time_elpased: 1.964
batch start
#iterations: 249
currently lose_sum: 97.41236114501953
time_elpased: 1.952
batch start
#iterations: 250
currently lose_sum: 97.48577529191971
time_elpased: 1.987
batch start
#iterations: 251
currently lose_sum: 97.83234000205994
time_elpased: 1.975
batch start
#iterations: 252
currently lose_sum: 97.10802954435349
time_elpased: 2.059
batch start
#iterations: 253
currently lose_sum: 97.49752622842789
time_elpased: 1.974
batch start
#iterations: 254
currently lose_sum: 97.07222998142242
time_elpased: 1.898
batch start
#iterations: 255
currently lose_sum: 97.3240515589714
time_elpased: 2.008
batch start
#iterations: 256
currently lose_sum: 97.40124922990799
time_elpased: 2.063
batch start
#iterations: 257
currently lose_sum: 97.30705606937408
time_elpased: 2.009
batch start
#iterations: 258
currently lose_sum: 97.02163749933243
time_elpased: 1.996
batch start
#iterations: 259
currently lose_sum: 97.12328386306763
time_elpased: 2.034
start validation test
0.5965463917525773
0.6258992805755396
0.4834825563445508
0.5455495558265111
0.596744892754533
65.057
batch start
#iterations: 260
currently lose_sum: 97.65351068973541
time_elpased: 1.987
batch start
#iterations: 261
currently lose_sum: 97.08629459142685
time_elpased: 1.955
batch start
#iterations: 262
currently lose_sum: 97.25379127264023
time_elpased: 1.936
batch start
#iterations: 263
currently lose_sum: 97.3948066830635
time_elpased: 1.974
batch start
#iterations: 264
currently lose_sum: 97.400241792202
time_elpased: 1.941
batch start
#iterations: 265
currently lose_sum: 97.295902967453
time_elpased: 1.957
batch start
#iterations: 266
currently lose_sum: 97.28081715106964
time_elpased: 2.006
batch start
#iterations: 267
currently lose_sum: 96.90333712100983
time_elpased: 2.007
batch start
#iterations: 268
currently lose_sum: 97.32917946577072
time_elpased: 1.996
batch start
#iterations: 269
currently lose_sum: 97.23864537477493
time_elpased: 1.932
batch start
#iterations: 270
currently lose_sum: 97.36425471305847
time_elpased: 1.973
batch start
#iterations: 271
currently lose_sum: 96.93123298883438
time_elpased: 1.958
batch start
#iterations: 272
currently lose_sum: 97.04885244369507
time_elpased: 2.03
batch start
#iterations: 273
currently lose_sum: 97.08615678548813
time_elpased: 1.94
batch start
#iterations: 274
currently lose_sum: 96.940988779068
time_elpased: 2.028
batch start
#iterations: 275
currently lose_sum: 97.15130734443665
time_elpased: 2.03
batch start
#iterations: 276
currently lose_sum: 96.95279800891876
time_elpased: 1.938
batch start
#iterations: 277
currently lose_sum: 96.81504756212234
time_elpased: 1.952
batch start
#iterations: 278
currently lose_sum: 96.53017389774323
time_elpased: 1.947
batch start
#iterations: 279
currently lose_sum: 97.11310839653015
time_elpased: 2.034
start validation test
0.5889175257731959
0.614394536380352
0.48142430791396523
0.5398419017944723
0.5891062466968359
65.998
batch start
#iterations: 280
currently lose_sum: 96.57286620140076
time_elpased: 1.987
batch start
#iterations: 281
currently lose_sum: 96.86545902490616
time_elpased: 2.026
batch start
#iterations: 282
currently lose_sum: 96.80241394042969
time_elpased: 1.946
batch start
#iterations: 283
currently lose_sum: 96.79284048080444
time_elpased: 1.933
batch start
#iterations: 284
currently lose_sum: 96.7497832775116
time_elpased: 1.992
batch start
#iterations: 285
currently lose_sum: 96.69830071926117
time_elpased: 1.988
batch start
#iterations: 286
currently lose_sum: 96.94448620080948
time_elpased: 1.999
batch start
#iterations: 287
currently lose_sum: 96.78143644332886
time_elpased: 1.961
batch start
#iterations: 288
currently lose_sum: 96.60546040534973
time_elpased: 1.926
batch start
#iterations: 289
currently lose_sum: 96.63455671072006
time_elpased: 1.915
batch start
#iterations: 290
currently lose_sum: 96.65622293949127
time_elpased: 1.937
batch start
#iterations: 291
currently lose_sum: 96.59134882688522
time_elpased: 1.914
batch start
#iterations: 292
currently lose_sum: 96.2095394730568
time_elpased: 1.959
batch start
#iterations: 293
currently lose_sum: 96.59347462654114
time_elpased: 1.905
batch start
#iterations: 294
currently lose_sum: 96.45498365163803
time_elpased: 2.009
batch start
#iterations: 295
currently lose_sum: 96.63734066486359
time_elpased: 1.977
batch start
#iterations: 296
currently lose_sum: 96.81529414653778
time_elpased: 1.909
batch start
#iterations: 297
currently lose_sum: 96.34621697664261
time_elpased: 1.956
batch start
#iterations: 298
currently lose_sum: 96.54195070266724
time_elpased: 2.005
batch start
#iterations: 299
currently lose_sum: 96.2004873752594
time_elpased: 1.963
start validation test
0.5939690721649484
0.6252041371801851
0.4727796645055058
0.5384119542924113
0.5941818388622747
65.765
batch start
#iterations: 300
currently lose_sum: 96.3312017917633
time_elpased: 1.995
batch start
#iterations: 301
currently lose_sum: 96.32043826580048
time_elpased: 1.983
batch start
#iterations: 302
currently lose_sum: 95.94922244548798
time_elpased: 2.008
batch start
#iterations: 303
currently lose_sum: 96.38033819198608
time_elpased: 1.989
batch start
#iterations: 304
currently lose_sum: 95.56832200288773
time_elpased: 1.973
batch start
#iterations: 305
currently lose_sum: 96.35630714893341
time_elpased: 1.996
batch start
#iterations: 306
currently lose_sum: 95.83605140447617
time_elpased: 1.928
batch start
#iterations: 307
currently lose_sum: 96.07093399763107
time_elpased: 1.986
batch start
#iterations: 308
currently lose_sum: 95.77517795562744
time_elpased: 1.98
batch start
#iterations: 309
currently lose_sum: 95.88885962963104
time_elpased: 1.917
batch start
#iterations: 310
currently lose_sum: 95.84590691328049
time_elpased: 2.016
batch start
#iterations: 311
currently lose_sum: 95.94697380065918
time_elpased: 1.921
batch start
#iterations: 312
currently lose_sum: 95.64170527458191
time_elpased: 1.973
batch start
#iterations: 313
currently lose_sum: 95.82025676965714
time_elpased: 1.971
batch start
#iterations: 314
currently lose_sum: 95.68932324647903
time_elpased: 1.936
batch start
#iterations: 315
currently lose_sum: 95.4055261015892
time_elpased: 1.958
batch start
#iterations: 316
currently lose_sum: 95.52451837062836
time_elpased: 1.889
batch start
#iterations: 317
currently lose_sum: 95.48621833324432
time_elpased: 1.878
batch start
#iterations: 318
currently lose_sum: 95.63060474395752
time_elpased: 1.93
batch start
#iterations: 319
currently lose_sum: 95.6416842341423
time_elpased: 1.973
start validation test
0.5958762886597938
0.6283682122828614
0.4727796645055058
0.5395818651632605
0.5960924037698447
65.976
batch start
#iterations: 320
currently lose_sum: 95.32038885354996
time_elpased: 2.015
batch start
#iterations: 321
currently lose_sum: 95.22686284780502
time_elpased: 1.943
batch start
#iterations: 322
currently lose_sum: 95.1726781129837
time_elpased: 1.983
batch start
#iterations: 323
currently lose_sum: 95.19531035423279
time_elpased: 1.973
batch start
#iterations: 324
currently lose_sum: 94.82413119077682
time_elpased: 1.948
batch start
#iterations: 325
currently lose_sum: 95.10465079545975
time_elpased: 1.924
batch start
#iterations: 326
currently lose_sum: 94.97979873418808
time_elpased: 1.997
batch start
#iterations: 327
currently lose_sum: 94.97716760635376
time_elpased: 2.0
batch start
#iterations: 328
currently lose_sum: 94.74809867143631
time_elpased: 1.983
batch start
#iterations: 329
currently lose_sum: 94.70143228769302
time_elpased: 1.925
batch start
#iterations: 330
currently lose_sum: 94.44808548688889
time_elpased: 1.901
batch start
#iterations: 331
currently lose_sum: 94.70032757520676
time_elpased: 2.036
batch start
#iterations: 332
currently lose_sum: 94.5317912697792
time_elpased: 2.156
batch start
#iterations: 333
currently lose_sum: 94.5724333524704
time_elpased: 1.944
batch start
#iterations: 334
currently lose_sum: 94.62299966812134
time_elpased: 1.998
batch start
#iterations: 335
currently lose_sum: 94.14203691482544
time_elpased: 2.062
batch start
#iterations: 336
currently lose_sum: 94.36555922031403
time_elpased: 1.992
batch start
#iterations: 337
currently lose_sum: 94.40093666315079
time_elpased: 1.932
batch start
#iterations: 338
currently lose_sum: 94.4329736828804
time_elpased: 1.945
batch start
#iterations: 339
currently lose_sum: 94.36647814512253
time_elpased: 1.964
start validation test
0.5894845360824742
0.6337147215865752
0.42749819903262326
0.5105703048180924
0.5897689280818388
68.022
batch start
#iterations: 340
currently lose_sum: 94.00619584321976
time_elpased: 1.947
batch start
#iterations: 341
currently lose_sum: 93.68481868505478
time_elpased: 2.079
batch start
#iterations: 342
currently lose_sum: 94.03730982542038
time_elpased: 1.951
batch start
#iterations: 343
currently lose_sum: 93.90149116516113
time_elpased: 1.96
batch start
#iterations: 344
currently lose_sum: 93.77076011896133
time_elpased: 2.0
batch start
#iterations: 345
currently lose_sum: 93.80750566720963
time_elpased: 1.935
batch start
#iterations: 346
currently lose_sum: 93.60496705770493
time_elpased: 1.965
batch start
#iterations: 347
currently lose_sum: 93.55506592988968
time_elpased: 1.908
batch start
#iterations: 348
currently lose_sum: 93.67811578512192
time_elpased: 2.031
batch start
#iterations: 349
currently lose_sum: 93.33910983800888
time_elpased: 1.904
batch start
#iterations: 350
currently lose_sum: 93.52406948804855
time_elpased: 1.971
batch start
#iterations: 351
currently lose_sum: 93.04559999704361
time_elpased: 1.943
batch start
#iterations: 352
currently lose_sum: 93.19613736867905
time_elpased: 1.946
batch start
#iterations: 353
currently lose_sum: 92.78064888715744
time_elpased: 1.959
batch start
#iterations: 354
currently lose_sum: 93.5804796218872
time_elpased: 1.944
batch start
#iterations: 355
currently lose_sum: 92.48516029119492
time_elpased: 1.972
batch start
#iterations: 356
currently lose_sum: 92.91215974092484
time_elpased: 1.955
batch start
#iterations: 357
currently lose_sum: 92.86412638425827
time_elpased: 1.982
batch start
#iterations: 358
currently lose_sum: 93.11005121469498
time_elpased: 1.98
batch start
#iterations: 359
currently lose_sum: 92.18375808000565
time_elpased: 1.968
start validation test
0.5812886597938144
0.6152067071407922
0.43799526602860966
0.511692215208897
0.5815402334480547
70.508
batch start
#iterations: 360
currently lose_sum: 92.4268689751625
time_elpased: 1.964
batch start
#iterations: 361
currently lose_sum: 92.17106348276138
time_elpased: 1.968
batch start
#iterations: 362
currently lose_sum: 92.51112908124924
time_elpased: 1.957
batch start
#iterations: 363
currently lose_sum: 92.3102245926857
time_elpased: 1.934
batch start
#iterations: 364
currently lose_sum: 92.08916622400284
time_elpased: 1.936
batch start
#iterations: 365
currently lose_sum: 92.04491341114044
time_elpased: 2.033
batch start
#iterations: 366
currently lose_sum: 91.21182650327682
time_elpased: 1.912
batch start
#iterations: 367
currently lose_sum: 91.58175206184387
time_elpased: 1.898
batch start
#iterations: 368
currently lose_sum: 91.10354346036911
time_elpased: 1.965
batch start
#iterations: 369
currently lose_sum: 91.45721262693405
time_elpased: 1.947
batch start
#iterations: 370
currently lose_sum: 91.17570340633392
time_elpased: 1.998
batch start
#iterations: 371
currently lose_sum: 91.36145758628845
time_elpased: 1.978
batch start
#iterations: 372
currently lose_sum: 91.01768726110458
time_elpased: 1.978
batch start
#iterations: 373
currently lose_sum: 90.93380361795425
time_elpased: 1.968
batch start
#iterations: 374
currently lose_sum: 90.92273420095444
time_elpased: 1.9
batch start
#iterations: 375
currently lose_sum: 91.01862412691116
time_elpased: 1.922
batch start
#iterations: 376
currently lose_sum: 91.03521490097046
time_elpased: 2.006
batch start
#iterations: 377
currently lose_sum: 91.25162142515182
time_elpased: 2.06
batch start
#iterations: 378
currently lose_sum: 90.75635039806366
time_elpased: 2.0
batch start
#iterations: 379
currently lose_sum: 90.32512092590332
time_elpased: 1.964
start validation test
0.5787628865979382
0.6139212505530158
0.42842441082638677
0.5046672323918051
0.5790268289802696
74.931
batch start
#iterations: 380
currently lose_sum: 89.85530579090118
time_elpased: 2.009
batch start
#iterations: 381
currently lose_sum: 90.08453422784805
time_elpased: 1.998
batch start
#iterations: 382
currently lose_sum: 89.81776505708694
time_elpased: 2.038
batch start
#iterations: 383
currently lose_sum: 89.97821348905563
time_elpased: 2.037
batch start
#iterations: 384
currently lose_sum: 89.62152373790741
time_elpased: 1.939
batch start
#iterations: 385
currently lose_sum: 89.73455137014389
time_elpased: 2.014
batch start
#iterations: 386
currently lose_sum: 89.8273543715477
time_elpased: 2.022
batch start
#iterations: 387
currently lose_sum: 89.27067530155182
time_elpased: 1.992
batch start
#iterations: 388
currently lose_sum: 89.41846317052841
time_elpased: 1.997
batch start
#iterations: 389
currently lose_sum: 89.42309236526489
time_elpased: 2.011
batch start
#iterations: 390
currently lose_sum: 88.87706619501114
time_elpased: 1.992
batch start
#iterations: 391
currently lose_sum: 88.87476694583893
time_elpased: 2.083
batch start
#iterations: 392
currently lose_sum: 89.1375584602356
time_elpased: 1.94
batch start
#iterations: 393
currently lose_sum: 89.20192712545395
time_elpased: 1.967
batch start
#iterations: 394
currently lose_sum: 88.4994586110115
time_elpased: 1.926
batch start
#iterations: 395
currently lose_sum: 88.6971806883812
time_elpased: 1.967
batch start
#iterations: 396
currently lose_sum: 88.59169256687164
time_elpased: 1.969
batch start
#iterations: 397
currently lose_sum: 88.44346058368683
time_elpased: 2.027
batch start
#iterations: 398
currently lose_sum: 87.58431905508041
time_elpased: 2.026
batch start
#iterations: 399
currently lose_sum: 88.54050439596176
time_elpased: 1.99
start validation test
0.5795876288659794
0.6207643509206251
0.4128846351754657
0.495920889987639
0.5798803016835709
77.193
acc: 0.601
pre: 0.621
rec: 0.523
F1: 0.568
auc: 0.633
