start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.48280954360962
time_elpased: 3.019
batch start
#iterations: 1
currently lose_sum: 100.35155391693115
time_elpased: 2.169
batch start
#iterations: 2
currently lose_sum: 100.40970891714096
time_elpased: 2.077
batch start
#iterations: 3
currently lose_sum: 100.29694026708603
time_elpased: 2.282
batch start
#iterations: 4
currently lose_sum: 100.21639502048492
time_elpased: 2.291
batch start
#iterations: 5
currently lose_sum: 100.35304814577103
time_elpased: 2.468
batch start
#iterations: 6
currently lose_sum: 100.37335693836212
time_elpased: 2.426
batch start
#iterations: 7
currently lose_sum: 100.12026923894882
time_elpased: 2.347
batch start
#iterations: 8
currently lose_sum: 100.23214197158813
time_elpased: 2.262
batch start
#iterations: 9
currently lose_sum: 100.24635964632034
time_elpased: 2.357
batch start
#iterations: 10
currently lose_sum: 100.07935678958893
time_elpased: 2.288
batch start
#iterations: 11
currently lose_sum: 100.02991807460785
time_elpased: 2.41
batch start
#iterations: 12
currently lose_sum: 100.03778582811356
time_elpased: 2.456
batch start
#iterations: 13
currently lose_sum: 99.95950222015381
time_elpased: 2.248
batch start
#iterations: 14
currently lose_sum: 99.94663333892822
time_elpased: 2.276
batch start
#iterations: 15
currently lose_sum: 99.8864397406578
time_elpased: 2.332
batch start
#iterations: 16
currently lose_sum: 99.92352396249771
time_elpased: 2.321
batch start
#iterations: 17
currently lose_sum: 99.79858285188675
time_elpased: 2.309
batch start
#iterations: 18
currently lose_sum: 99.85031235218048
time_elpased: 2.503
batch start
#iterations: 19
currently lose_sum: 99.82157099246979
time_elpased: 2.283
start validation test
0.5898969072164948
0.59814959313343
0.5522280539261089
0.5742722602739726
0.589963040698467
65.652
batch start
#iterations: 20
currently lose_sum: 99.9419949054718
time_elpased: 2.213
batch start
#iterations: 21
currently lose_sum: 99.68599009513855
time_elpased: 2.427
batch start
#iterations: 22
currently lose_sum: 99.73422676324844
time_elpased: 2.201
batch start
#iterations: 23
currently lose_sum: 99.64963752031326
time_elpased: 2.213
batch start
#iterations: 24
currently lose_sum: 99.67275446653366
time_elpased: 2.344
batch start
#iterations: 25
currently lose_sum: 99.58029246330261
time_elpased: 2.358
batch start
#iterations: 26
currently lose_sum: 99.65596908330917
time_elpased: 2.361
batch start
#iterations: 27
currently lose_sum: 99.670578956604
time_elpased: 2.365
batch start
#iterations: 28
currently lose_sum: 99.51960480213165
time_elpased: 2.384
batch start
#iterations: 29
currently lose_sum: 99.67593026161194
time_elpased: 2.36
batch start
#iterations: 30
currently lose_sum: 99.51888597011566
time_elpased: 2.348
batch start
#iterations: 31
currently lose_sum: 99.4940076470375
time_elpased: 2.394
batch start
#iterations: 32
currently lose_sum: 99.40443569421768
time_elpased: 2.332
batch start
#iterations: 33
currently lose_sum: 99.59640204906464
time_elpased: 2.273
batch start
#iterations: 34
currently lose_sum: 99.59740698337555
time_elpased: 2.264
batch start
#iterations: 35
currently lose_sum: 99.45282012224197
time_elpased: 2.468
batch start
#iterations: 36
currently lose_sum: 99.14354401826859
time_elpased: 2.364
batch start
#iterations: 37
currently lose_sum: 99.52875292301178
time_elpased: 2.404
batch start
#iterations: 38
currently lose_sum: 99.33694177865982
time_elpased: 2.374
batch start
#iterations: 39
currently lose_sum: 99.38399440050125
time_elpased: 2.339
start validation test
0.594381443298969
0.6412844036697247
0.43161469589379436
0.5159623546779848
0.5946672054290824
65.554
batch start
#iterations: 40
currently lose_sum: 99.3722602725029
time_elpased: 2.26
batch start
#iterations: 41
currently lose_sum: 99.29467904567719
time_elpased: 2.246
batch start
#iterations: 42
currently lose_sum: 99.33030468225479
time_elpased: 2.364
batch start
#iterations: 43
currently lose_sum: 99.2464856505394
time_elpased: 2.235
batch start
#iterations: 44
currently lose_sum: 99.24674397706985
time_elpased: 2.266
batch start
#iterations: 45
currently lose_sum: 99.01338881254196
time_elpased: 2.349
batch start
#iterations: 46
currently lose_sum: 99.06644743680954
time_elpased: 2.299
batch start
#iterations: 47
currently lose_sum: 99.25896310806274
time_elpased: 2.305
batch start
#iterations: 48
currently lose_sum: 99.35370641946793
time_elpased: 2.263
batch start
#iterations: 49
currently lose_sum: 99.16310691833496
time_elpased: 2.32
batch start
#iterations: 50
currently lose_sum: 99.42034459114075
time_elpased: 2.242
batch start
#iterations: 51
currently lose_sum: 99.18013662099838
time_elpased: 2.373
batch start
#iterations: 52
currently lose_sum: 99.19811058044434
time_elpased: 2.3
batch start
#iterations: 53
currently lose_sum: 99.15838599205017
time_elpased: 2.415
batch start
#iterations: 54
currently lose_sum: 98.89454752206802
time_elpased: 2.386
batch start
#iterations: 55
currently lose_sum: 99.04937034845352
time_elpased: 2.416
batch start
#iterations: 56
currently lose_sum: 99.07054620981216
time_elpased: 2.348
batch start
#iterations: 57
currently lose_sum: 99.02332717180252
time_elpased: 2.36
batch start
#iterations: 58
currently lose_sum: 98.93038129806519
time_elpased: 2.32
batch start
#iterations: 59
currently lose_sum: 99.00985431671143
time_elpased: 2.406
start validation test
0.5862886597938144
0.6402853824456612
0.39713903468148604
0.4902184959349594
0.5866207411350216
65.936
batch start
#iterations: 60
currently lose_sum: 99.09850859642029
time_elpased: 2.238
batch start
#iterations: 61
currently lose_sum: 99.01725822687149
time_elpased: 2.298
batch start
#iterations: 62
currently lose_sum: 99.2392019033432
time_elpased: 2.408
batch start
#iterations: 63
currently lose_sum: 98.82161295413971
time_elpased: 2.375
batch start
#iterations: 64
currently lose_sum: 98.96483361721039
time_elpased: 2.306
batch start
#iterations: 65
currently lose_sum: 98.84444028139114
time_elpased: 2.314
batch start
#iterations: 66
currently lose_sum: 98.70189487934113
time_elpased: 2.4
batch start
#iterations: 67
currently lose_sum: 98.94757747650146
time_elpased: 2.386
batch start
#iterations: 68
currently lose_sum: 98.74407368898392
time_elpased: 2.287
batch start
#iterations: 69
currently lose_sum: 98.76608169078827
time_elpased: 2.376
batch start
#iterations: 70
currently lose_sum: 98.7381654381752
time_elpased: 2.358
batch start
#iterations: 71
currently lose_sum: 98.66406553983688
time_elpased: 2.418
batch start
#iterations: 72
currently lose_sum: 98.60013562440872
time_elpased: 2.387
batch start
#iterations: 73
currently lose_sum: 98.89276087284088
time_elpased: 2.266
batch start
#iterations: 74
currently lose_sum: 98.55092006921768
time_elpased: 2.416
batch start
#iterations: 75
currently lose_sum: 98.87477016448975
time_elpased: 2.348
batch start
#iterations: 76
currently lose_sum: 98.85238111019135
time_elpased: 2.39
batch start
#iterations: 77
currently lose_sum: 98.79074281454086
time_elpased: 2.267
batch start
#iterations: 78
currently lose_sum: 98.8259899020195
time_elpased: 2.343
batch start
#iterations: 79
currently lose_sum: 98.58257323503494
time_elpased: 2.272
start validation test
0.6062886597938144
0.6303121474238436
0.5174436554492127
0.5683282468633435
0.6064446409023406
64.757
batch start
#iterations: 80
currently lose_sum: 98.77467292547226
time_elpased: 2.302
batch start
#iterations: 81
currently lose_sum: 98.84872651100159
time_elpased: 2.302
batch start
#iterations: 82
currently lose_sum: 98.69635528326035
time_elpased: 2.344
batch start
#iterations: 83
currently lose_sum: 98.84868264198303
time_elpased: 2.308
batch start
#iterations: 84
currently lose_sum: 98.96414697170258
time_elpased: 2.207
batch start
#iterations: 85
currently lose_sum: 98.8575102686882
time_elpased: 2.299
batch start
#iterations: 86
currently lose_sum: 98.72272235155106
time_elpased: 2.429
batch start
#iterations: 87
currently lose_sum: 98.6739901304245
time_elpased: 2.183
batch start
#iterations: 88
currently lose_sum: 98.92619603872299
time_elpased: 2.372
batch start
#iterations: 89
currently lose_sum: 98.77119308710098
time_elpased: 2.378
batch start
#iterations: 90
currently lose_sum: 98.9411758184433
time_elpased: 2.354
batch start
#iterations: 91
currently lose_sum: 98.73022729158401
time_elpased: 2.319
batch start
#iterations: 92
currently lose_sum: 98.67078286409378
time_elpased: 2.287
batch start
#iterations: 93
currently lose_sum: 98.84571593999863
time_elpased: 2.223
batch start
#iterations: 94
currently lose_sum: 98.78118401765823
time_elpased: 2.437
batch start
#iterations: 95
currently lose_sum: 98.63513767719269
time_elpased: 2.27
batch start
#iterations: 96
currently lose_sum: 98.49579083919525
time_elpased: 2.309
batch start
#iterations: 97
currently lose_sum: 98.75527542829514
time_elpased: 2.431
batch start
#iterations: 98
currently lose_sum: 98.52516359090805
time_elpased: 2.278
batch start
#iterations: 99
currently lose_sum: 98.41679960489273
time_elpased: 2.308
start validation test
0.5960824742268042
0.6154129341023439
0.5161057939693321
0.5614015448337624
0.5962228856245504
65.082
batch start
#iterations: 100
currently lose_sum: 98.64742058515549
time_elpased: 2.305
batch start
#iterations: 101
currently lose_sum: 98.75772327184677
time_elpased: 2.212
batch start
#iterations: 102
currently lose_sum: 98.68832457065582
time_elpased: 2.352
batch start
#iterations: 103
currently lose_sum: 98.48430413007736
time_elpased: 2.379
batch start
#iterations: 104
currently lose_sum: 98.58972376585007
time_elpased: 2.329
batch start
#iterations: 105
currently lose_sum: 98.61403775215149
time_elpased: 2.255
batch start
#iterations: 106
currently lose_sum: 98.53797888755798
time_elpased: 2.304
batch start
#iterations: 107
currently lose_sum: 98.55551785230637
time_elpased: 2.384
batch start
#iterations: 108
currently lose_sum: 98.41739201545715
time_elpased: 2.315
batch start
#iterations: 109
currently lose_sum: 98.35447174310684
time_elpased: 2.329
batch start
#iterations: 110
currently lose_sum: 98.43982690572739
time_elpased: 2.34
batch start
#iterations: 111
currently lose_sum: 98.62727117538452
time_elpased: 2.31
batch start
#iterations: 112
currently lose_sum: 98.63360184431076
time_elpased: 2.277
batch start
#iterations: 113
currently lose_sum: 98.60602468252182
time_elpased: 2.403
batch start
#iterations: 114
currently lose_sum: 98.4534775018692
time_elpased: 2.322
batch start
#iterations: 115
currently lose_sum: 98.17998421192169
time_elpased: 2.414
batch start
#iterations: 116
currently lose_sum: 98.62774020433426
time_elpased: 2.342
batch start
#iterations: 117
currently lose_sum: 98.52395355701447
time_elpased: 2.238
batch start
#iterations: 118
currently lose_sum: 98.6577867269516
time_elpased: 2.287
batch start
#iterations: 119
currently lose_sum: 98.61970901489258
time_elpased: 2.322
start validation test
0.5988144329896907
0.6343428730202834
0.469898116702686
0.5398758498374224
0.5990407654669063
64.817
batch start
#iterations: 120
currently lose_sum: 98.58026248216629
time_elpased: 2.343
batch start
#iterations: 121
currently lose_sum: 98.6352424621582
time_elpased: 2.308
batch start
#iterations: 122
currently lose_sum: 98.11989778280258
time_elpased: 2.367
batch start
#iterations: 123
currently lose_sum: 98.53650891780853
time_elpased: 2.374
batch start
#iterations: 124
currently lose_sum: 98.6234900355339
time_elpased: 2.32
batch start
#iterations: 125
currently lose_sum: 98.38805949687958
time_elpased: 2.236
batch start
#iterations: 126
currently lose_sum: 98.3649714589119
time_elpased: 2.322
batch start
#iterations: 127
currently lose_sum: 98.36619478464127
time_elpased: 2.401
batch start
#iterations: 128
currently lose_sum: 98.2738670706749
time_elpased: 2.262
batch start
#iterations: 129
currently lose_sum: 98.31444293260574
time_elpased: 2.522
batch start
#iterations: 130
currently lose_sum: 98.10785406827927
time_elpased: 2.427
batch start
#iterations: 131
currently lose_sum: 98.31337022781372
time_elpased: 2.382
batch start
#iterations: 132
currently lose_sum: 98.06411182880402
time_elpased: 2.341
batch start
#iterations: 133
currently lose_sum: 98.29995894432068
time_elpased: 2.294
batch start
#iterations: 134
currently lose_sum: 98.10984992980957
time_elpased: 2.296
batch start
#iterations: 135
currently lose_sum: 98.67749029397964
time_elpased: 2.377
batch start
#iterations: 136
currently lose_sum: 98.19126945734024
time_elpased: 2.33
batch start
#iterations: 137
currently lose_sum: 98.6320772767067
time_elpased: 2.368
batch start
#iterations: 138
currently lose_sum: 98.1590496301651
time_elpased: 2.379
batch start
#iterations: 139
currently lose_sum: 98.03763276338577
time_elpased: 2.311
start validation test
0.5724226804123711
0.5552705223880597
0.735103426983637
0.6326557725521457
0.5721370692699864
65.070
batch start
#iterations: 140
currently lose_sum: 98.18695157766342
time_elpased: 2.45
batch start
#iterations: 141
currently lose_sum: 97.76193726062775
time_elpased: 2.3
batch start
#iterations: 142
currently lose_sum: 98.41882711648941
time_elpased: 2.32
batch start
#iterations: 143
currently lose_sum: 98.07415336370468
time_elpased: 2.239
batch start
#iterations: 144
currently lose_sum: 98.1512479186058
time_elpased: 2.327
batch start
#iterations: 145
currently lose_sum: 98.1668410897255
time_elpased: 2.264
batch start
#iterations: 146
currently lose_sum: 97.97864729166031
time_elpased: 2.313
batch start
#iterations: 147
currently lose_sum: 98.24351650476456
time_elpased: 2.31
batch start
#iterations: 148
currently lose_sum: 98.19903564453125
time_elpased: 2.327
batch start
#iterations: 149
currently lose_sum: 98.18483000993729
time_elpased: 2.385
batch start
#iterations: 150
currently lose_sum: 98.05158853530884
time_elpased: 2.309
batch start
#iterations: 151
currently lose_sum: 98.32870280742645
time_elpased: 2.325
batch start
#iterations: 152
currently lose_sum: 98.13545924425125
time_elpased: 2.246
batch start
#iterations: 153
currently lose_sum: 98.25107640028
time_elpased: 2.391
batch start
#iterations: 154
currently lose_sum: 98.07333952188492
time_elpased: 2.279
batch start
#iterations: 155
currently lose_sum: 98.36922258138657
time_elpased: 2.296
batch start
#iterations: 156
currently lose_sum: 98.09405183792114
time_elpased: 2.378
batch start
#iterations: 157
currently lose_sum: 97.79139751195908
time_elpased: 2.331
batch start
#iterations: 158
currently lose_sum: 98.29466152191162
time_elpased: 2.4
batch start
#iterations: 159
currently lose_sum: 98.16946679353714
time_elpased: 2.359
start validation test
0.5995360824742269
0.6358059118795315
0.46928064217351034
0.5399964473917934
0.5997647659901941
65.053
batch start
#iterations: 160
currently lose_sum: 98.06371492147446
time_elpased: 2.434
batch start
#iterations: 161
currently lose_sum: 98.03990507125854
time_elpased: 2.337
batch start
#iterations: 162
currently lose_sum: 98.1134724020958
time_elpased: 2.246
batch start
#iterations: 163
currently lose_sum: 97.8735294342041
time_elpased: 2.279
batch start
#iterations: 164
currently lose_sum: 98.05872231721878
time_elpased: 2.318
batch start
#iterations: 165
currently lose_sum: 97.944360435009
time_elpased: 2.359
batch start
#iterations: 166
currently lose_sum: 97.91784197092056
time_elpased: 2.335
batch start
#iterations: 167
currently lose_sum: 98.11654752492905
time_elpased: 2.311
batch start
#iterations: 168
currently lose_sum: 97.96565383672714
time_elpased: 2.332
batch start
#iterations: 169
currently lose_sum: 97.92104905843735
time_elpased: 2.341
batch start
#iterations: 170
currently lose_sum: 98.00293928384781
time_elpased: 2.319
batch start
#iterations: 171
currently lose_sum: 98.07794827222824
time_elpased: 2.344
batch start
#iterations: 172
currently lose_sum: 98.01862126588821
time_elpased: 2.383
batch start
#iterations: 173
currently lose_sum: 97.93327325582504
time_elpased: 2.375
batch start
#iterations: 174
currently lose_sum: 98.08751964569092
time_elpased: 2.335
batch start
#iterations: 175
currently lose_sum: 98.12126088142395
time_elpased: 2.389
batch start
#iterations: 176
currently lose_sum: 97.8907830119133
time_elpased: 2.334
batch start
#iterations: 177
currently lose_sum: 97.77988058328629
time_elpased: 2.36
batch start
#iterations: 178
currently lose_sum: 97.9445948600769
time_elpased: 2.472
batch start
#iterations: 179
currently lose_sum: 97.87416625022888
time_elpased: 2.389
start validation test
0.5999484536082474
0.6325921908893709
0.48018935885561387
0.5459544842918154
0.6001587091706552
64.971
batch start
#iterations: 180
currently lose_sum: 97.934967815876
time_elpased: 2.287
batch start
#iterations: 181
currently lose_sum: 97.78842681646347
time_elpased: 2.395
batch start
#iterations: 182
currently lose_sum: 97.74262988567352
time_elpased: 2.335
batch start
#iterations: 183
currently lose_sum: 97.84269165992737
time_elpased: 2.3
batch start
#iterations: 184
currently lose_sum: 97.7249276638031
time_elpased: 2.304
batch start
#iterations: 185
currently lose_sum: 97.76310348510742
time_elpased: 2.338
batch start
#iterations: 186
currently lose_sum: 97.9317718744278
time_elpased: 2.214
batch start
#iterations: 187
currently lose_sum: 97.8085504770279
time_elpased: 2.335
batch start
#iterations: 188
currently lose_sum: 97.84064424037933
time_elpased: 2.425
batch start
#iterations: 189
currently lose_sum: 97.39294803142548
time_elpased: 2.265
batch start
#iterations: 190
currently lose_sum: 97.6571050286293
time_elpased: 2.354
batch start
#iterations: 191
currently lose_sum: 97.77790755033493
time_elpased: 2.336
batch start
#iterations: 192
currently lose_sum: 97.8079201579094
time_elpased: 2.294
batch start
#iterations: 193
currently lose_sum: 98.02698302268982
time_elpased: 2.215
batch start
#iterations: 194
currently lose_sum: 97.37083631753922
time_elpased: 2.333
batch start
#iterations: 195
currently lose_sum: 97.54989194869995
time_elpased: 2.301
batch start
#iterations: 196
currently lose_sum: 97.51568537950516
time_elpased: 2.285
batch start
#iterations: 197
currently lose_sum: 97.80323249101639
time_elpased: 2.343
batch start
#iterations: 198
currently lose_sum: 97.56951868534088
time_elpased: 2.367
batch start
#iterations: 199
currently lose_sum: 97.20478546619415
time_elpased: 2.393
start validation test
0.6047938144329897
0.649897630886224
0.45734280127611404
0.5368770764119601
0.6050526874293407
64.801
batch start
#iterations: 200
currently lose_sum: 98.02105587720871
time_elpased: 2.356
batch start
#iterations: 201
currently lose_sum: 97.54914098978043
time_elpased: 2.324
batch start
#iterations: 202
currently lose_sum: 97.31800240278244
time_elpased: 2.287
batch start
#iterations: 203
currently lose_sum: 97.7157307267189
time_elpased: 2.277
batch start
#iterations: 204
currently lose_sum: 97.4192944765091
time_elpased: 2.375
batch start
#iterations: 205
currently lose_sum: 97.52309656143188
time_elpased: 2.412
batch start
#iterations: 206
currently lose_sum: 97.58515930175781
time_elpased: 2.264
batch start
#iterations: 207
currently lose_sum: 97.44772362709045
time_elpased: 2.293
batch start
#iterations: 208
currently lose_sum: 97.3441618680954
time_elpased: 2.362
batch start
#iterations: 209
currently lose_sum: 97.42831814289093
time_elpased: 2.351
batch start
#iterations: 210
currently lose_sum: 97.38291710615158
time_elpased: 2.326
batch start
#iterations: 211
currently lose_sum: 97.54505270719528
time_elpased: 2.331
batch start
#iterations: 212
currently lose_sum: 97.6035566329956
time_elpased: 2.295
batch start
#iterations: 213
currently lose_sum: 97.05140548944473
time_elpased: 2.296
batch start
#iterations: 214
currently lose_sum: 97.43014913797379
time_elpased: 2.362
batch start
#iterations: 215
currently lose_sum: 97.1265954375267
time_elpased: 2.398
batch start
#iterations: 216
currently lose_sum: 96.95058435201645
time_elpased: 2.353
batch start
#iterations: 217
currently lose_sum: 97.02300941944122
time_elpased: 2.28
batch start
#iterations: 218
currently lose_sum: 97.04609882831573
time_elpased: 2.279
batch start
#iterations: 219
currently lose_sum: 97.26350897550583
time_elpased: 2.359
start validation test
0.5943298969072165
0.6326296136722677
0.45332921683647215
0.5281774580335732
0.5945774453489394
65.231
batch start
#iterations: 220
currently lose_sum: 96.94534409046173
time_elpased: 2.325
batch start
#iterations: 221
currently lose_sum: 97.34788727760315
time_elpased: 2.453
batch start
#iterations: 222
currently lose_sum: 97.37675195932388
time_elpased: 2.259
batch start
#iterations: 223
currently lose_sum: 96.88962435722351
time_elpased: 2.338
batch start
#iterations: 224
currently lose_sum: 96.71879482269287
time_elpased: 2.388
batch start
#iterations: 225
currently lose_sum: 97.09853267669678
time_elpased: 2.41
batch start
#iterations: 226
currently lose_sum: 97.22607064247131
time_elpased: 2.31
batch start
#iterations: 227
currently lose_sum: 97.13480240106583
time_elpased: 2.276
batch start
#iterations: 228
currently lose_sum: 97.05019736289978
time_elpased: 2.348
batch start
#iterations: 229
currently lose_sum: 97.03597569465637
time_elpased: 2.322
batch start
#iterations: 230
currently lose_sum: 97.16368079185486
time_elpased: 2.323
batch start
#iterations: 231
currently lose_sum: 96.91883653402328
time_elpased: 2.254
batch start
#iterations: 232
currently lose_sum: 96.68935668468475
time_elpased: 2.238
batch start
#iterations: 233
currently lose_sum: 96.73154205083847
time_elpased: 2.361
batch start
#iterations: 234
currently lose_sum: 96.7682574391365
time_elpased: 2.36
batch start
#iterations: 235
currently lose_sum: 96.96729028224945
time_elpased: 2.315
batch start
#iterations: 236
currently lose_sum: 96.84096318483353
time_elpased: 2.37
batch start
#iterations: 237
currently lose_sum: 96.49743431806564
time_elpased: 2.332
batch start
#iterations: 238
currently lose_sum: 96.99050521850586
time_elpased: 2.333
batch start
#iterations: 239
currently lose_sum: 96.40053111314774
time_elpased: 2.338
start validation test
0.5878350515463917
0.6367392340696011
0.4123700730678193
0.5005621486570894
0.5881431073797219
65.804
batch start
#iterations: 240
currently lose_sum: 96.69673931598663
time_elpased: 2.282
batch start
#iterations: 241
currently lose_sum: 96.41144639253616
time_elpased: 2.355
batch start
#iterations: 242
currently lose_sum: 96.76482486724854
time_elpased: 2.397
batch start
#iterations: 243
currently lose_sum: 96.55574017763138
time_elpased: 2.323
batch start
#iterations: 244
currently lose_sum: 96.78298568725586
time_elpased: 2.398
batch start
#iterations: 245
currently lose_sum: 96.6218010187149
time_elpased: 2.371
batch start
#iterations: 246
currently lose_sum: 96.10504186153412
time_elpased: 2.263
batch start
#iterations: 247
currently lose_sum: 96.06283909082413
time_elpased: 2.296
batch start
#iterations: 248
currently lose_sum: 96.4591988325119
time_elpased: 2.324
batch start
#iterations: 249
currently lose_sum: 96.24201130867004
time_elpased: 2.344
batch start
#iterations: 250
currently lose_sum: 96.06270253658295
time_elpased: 2.357
batch start
#iterations: 251
currently lose_sum: 96.3735499382019
time_elpased: 2.494
batch start
#iterations: 252
currently lose_sum: 96.42398184537888
time_elpased: 2.293
batch start
#iterations: 253
currently lose_sum: 95.93447834253311
time_elpased: 2.286
batch start
#iterations: 254
currently lose_sum: 96.37158125638962
time_elpased: 2.306
batch start
#iterations: 255
currently lose_sum: 96.28346395492554
time_elpased: 2.346
batch start
#iterations: 256
currently lose_sum: 95.69797921180725
time_elpased: 2.372
batch start
#iterations: 257
currently lose_sum: 96.28133916854858
time_elpased: 2.389
batch start
#iterations: 258
currently lose_sum: 95.91203254461288
time_elpased: 2.382
batch start
#iterations: 259
currently lose_sum: 96.61815202236176
time_elpased: 2.302
start validation test
0.589381443298969
0.6280157917824243
0.4420088504682515
0.5188451316743173
0.589640178616342
66.506
batch start
#iterations: 260
currently lose_sum: 96.06308740377426
time_elpased: 2.32
batch start
#iterations: 261
currently lose_sum: 95.38358962535858
time_elpased: 2.294
batch start
#iterations: 262
currently lose_sum: 95.76202893257141
time_elpased: 2.269
batch start
#iterations: 263
currently lose_sum: 95.87236827611923
time_elpased: 2.247
batch start
#iterations: 264
currently lose_sum: 95.4612147808075
time_elpased: 2.247
batch start
#iterations: 265
currently lose_sum: 95.54679065942764
time_elpased: 2.281
batch start
#iterations: 266
currently lose_sum: 95.6892511844635
time_elpased: 2.353
batch start
#iterations: 267
currently lose_sum: 95.30103504657745
time_elpased: 2.359
batch start
#iterations: 268
currently lose_sum: 95.82198375463486
time_elpased: 2.28
batch start
#iterations: 269
currently lose_sum: 95.29858261346817
time_elpased: 2.359
batch start
#iterations: 270
currently lose_sum: 95.41116845607758
time_elpased: 2.324
batch start
#iterations: 271
currently lose_sum: 95.56909358501434
time_elpased: 2.338
batch start
#iterations: 272
currently lose_sum: 95.38572478294373
time_elpased: 2.411
batch start
#iterations: 273
currently lose_sum: 95.04947656393051
time_elpased: 2.389
batch start
#iterations: 274
currently lose_sum: 95.6661262512207
time_elpased: 2.36
batch start
#iterations: 275
currently lose_sum: 94.7575169801712
time_elpased: 2.252
batch start
#iterations: 276
currently lose_sum: 94.96496570110321
time_elpased: 2.244
batch start
#iterations: 277
currently lose_sum: 94.79869502782822
time_elpased: 2.331
batch start
#iterations: 278
currently lose_sum: 94.66879343986511
time_elpased: 2.271
batch start
#iterations: 279
currently lose_sum: 95.12011885643005
time_elpased: 2.295
start validation test
0.5874742268041238
0.641046741277156
0.4008438818565401
0.49325650604698273
0.5878018851604295
67.618
batch start
#iterations: 280
currently lose_sum: 94.71270388364792
time_elpased: 2.337
batch start
#iterations: 281
currently lose_sum: 95.23486655950546
time_elpased: 2.401
batch start
#iterations: 282
currently lose_sum: 94.16835337877274
time_elpased: 2.365
batch start
#iterations: 283
currently lose_sum: 94.53952723741531
time_elpased: 2.295
batch start
#iterations: 284
currently lose_sum: 94.60331642627716
time_elpased: 2.29
batch start
#iterations: 285
currently lose_sum: 94.30171084403992
time_elpased: 2.295
batch start
#iterations: 286
currently lose_sum: 94.58465707302094
time_elpased: 2.304
batch start
#iterations: 287
currently lose_sum: 94.36588698625565
time_elpased: 2.229
batch start
#iterations: 288
currently lose_sum: 94.25444406270981
time_elpased: 2.315
batch start
#iterations: 289
currently lose_sum: 94.12326979637146
time_elpased: 2.262
batch start
#iterations: 290
currently lose_sum: 94.00262743234634
time_elpased: 2.349
batch start
#iterations: 291
currently lose_sum: 94.06926393508911
time_elpased: 2.411
batch start
#iterations: 292
currently lose_sum: 94.19053506851196
time_elpased: 2.391
batch start
#iterations: 293
currently lose_sum: 93.76891005039215
time_elpased: 2.321
batch start
#iterations: 294
currently lose_sum: 93.9468258023262
time_elpased: 2.379
batch start
#iterations: 295
currently lose_sum: 93.88453680276871
time_elpased: 2.363
batch start
#iterations: 296
currently lose_sum: 93.72364497184753
time_elpased: 2.352
batch start
#iterations: 297
currently lose_sum: 93.39166831970215
time_elpased: 2.338
batch start
#iterations: 298
currently lose_sum: 93.35647022724152
time_elpased: 2.281
batch start
#iterations: 299
currently lose_sum: 93.39418476819992
time_elpased: 2.374
start validation test
0.5887113402061855
0.63578125
0.41875064320263455
0.5049326797791153
0.5890097324244092
69.229
batch start
#iterations: 300
currently lose_sum: 93.42976850271225
time_elpased: 2.264
batch start
#iterations: 301
currently lose_sum: 93.07349973917007
time_elpased: 2.289
batch start
#iterations: 302
currently lose_sum: 93.08252090215683
time_elpased: 2.272
batch start
#iterations: 303
currently lose_sum: 93.12083828449249
time_elpased: 2.281
batch start
#iterations: 304
currently lose_sum: 93.17634731531143
time_elpased: 2.261
batch start
#iterations: 305
currently lose_sum: 93.0241226553917
time_elpased: 2.265
batch start
#iterations: 306
currently lose_sum: 92.77077865600586
time_elpased: 2.388
batch start
#iterations: 307
currently lose_sum: 92.65808063745499
time_elpased: 2.379
batch start
#iterations: 308
currently lose_sum: 92.41655987501144
time_elpased: 2.291
batch start
#iterations: 309
currently lose_sum: 92.50544917583466
time_elpased: 2.279
batch start
#iterations: 310
currently lose_sum: 92.34802848100662
time_elpased: 2.351
batch start
#iterations: 311
currently lose_sum: 92.69229900836945
time_elpased: 2.382
batch start
#iterations: 312
currently lose_sum: 92.04065835475922
time_elpased: 2.357
batch start
#iterations: 313
currently lose_sum: 91.60378915071487
time_elpased: 2.315
batch start
#iterations: 314
currently lose_sum: 91.76716619729996
time_elpased: 2.478
batch start
#iterations: 315
currently lose_sum: 91.64750242233276
time_elpased: 2.351
batch start
#iterations: 316
currently lose_sum: 91.45281928777695
time_elpased: 2.442
batch start
#iterations: 317
currently lose_sum: 92.03139412403107
time_elpased: 2.375
batch start
#iterations: 318
currently lose_sum: 91.38894748687744
time_elpased: 2.471
batch start
#iterations: 319
currently lose_sum: 91.52229595184326
time_elpased: 2.339
start validation test
0.5817010309278351
0.622627066748316
0.418544818359576
0.5005846513631608
0.581987476824113
73.793
batch start
#iterations: 320
currently lose_sum: 91.41929978132248
time_elpased: 2.389
batch start
#iterations: 321
currently lose_sum: 91.47517162561417
time_elpased: 2.422
batch start
#iterations: 322
currently lose_sum: 91.01803851127625
time_elpased: 2.362
batch start
#iterations: 323
currently lose_sum: 91.13082700967789
time_elpased: 2.345
batch start
#iterations: 324
currently lose_sum: 90.99837636947632
time_elpased: 2.242
batch start
#iterations: 325
currently lose_sum: 90.77379506826401
time_elpased: 2.333
batch start
#iterations: 326
currently lose_sum: 91.21995770931244
time_elpased: 2.33
batch start
#iterations: 327
currently lose_sum: 90.29714953899384
time_elpased: 2.321
batch start
#iterations: 328
currently lose_sum: 89.92884719371796
time_elpased: 2.311
batch start
#iterations: 329
currently lose_sum: 90.89757823944092
time_elpased: 2.329
batch start
#iterations: 330
currently lose_sum: 90.16799426078796
time_elpased: 2.273
batch start
#iterations: 331
currently lose_sum: 90.15587228536606
time_elpased: 2.376
batch start
#iterations: 332
currently lose_sum: 90.20298779010773
time_elpased: 2.358
batch start
#iterations: 333
currently lose_sum: 90.04415500164032
time_elpased: 2.297
batch start
#iterations: 334
currently lose_sum: 89.80388152599335
time_elpased: 2.332
batch start
#iterations: 335
currently lose_sum: 89.8035152554512
time_elpased: 2.304
batch start
#iterations: 336
currently lose_sum: 89.54574620723724
time_elpased: 2.335
batch start
#iterations: 337
currently lose_sum: 89.14941275119781
time_elpased: 2.375
batch start
#iterations: 338
currently lose_sum: 89.25878602266312
time_elpased: 2.333
batch start
#iterations: 339
currently lose_sum: 88.8916426897049
time_elpased: 2.36
start validation test
0.576958762886598
0.6201464035646085
0.40104970669959866
0.48709455659021317
0.5772675983668395
77.122
batch start
#iterations: 340
currently lose_sum: 88.99098765850067
time_elpased: 2.207
batch start
#iterations: 341
currently lose_sum: 88.99480080604553
time_elpased: 2.328
batch start
#iterations: 342
currently lose_sum: 88.72072160243988
time_elpased: 2.499
batch start
#iterations: 343
currently lose_sum: 89.05727779865265
time_elpased: 2.498
batch start
#iterations: 344
currently lose_sum: 88.38611447811127
time_elpased: 2.29
batch start
#iterations: 345
currently lose_sum: 88.10926258563995
time_elpased: 2.316
batch start
#iterations: 346
currently lose_sum: 88.43227010965347
time_elpased: 2.377
batch start
#iterations: 347
currently lose_sum: 88.16110271215439
time_elpased: 2.313
batch start
#iterations: 348
currently lose_sum: 88.00215315818787
time_elpased: 2.252
batch start
#iterations: 349
currently lose_sum: 87.3843504190445
time_elpased: 2.249
batch start
#iterations: 350
currently lose_sum: 87.74378496408463
time_elpased: 2.338
batch start
#iterations: 351
currently lose_sum: 87.7937707901001
time_elpased: 2.336
batch start
#iterations: 352
currently lose_sum: 87.77227693796158
time_elpased: 2.344
batch start
#iterations: 353
currently lose_sum: 87.23026704788208
time_elpased: 2.409
batch start
#iterations: 354
currently lose_sum: 87.4138640165329
time_elpased: 2.343
batch start
#iterations: 355
currently lose_sum: 87.3607971072197
time_elpased: 2.431
batch start
#iterations: 356
currently lose_sum: 87.52393329143524
time_elpased: 2.328
batch start
#iterations: 357
currently lose_sum: 87.65375012159348
time_elpased: 2.313
batch start
#iterations: 358
currently lose_sum: 86.13989502191544
time_elpased: 2.373
batch start
#iterations: 359
currently lose_sum: 86.85416877269745
time_elpased: 2.298
start validation test
0.5710309278350515
0.6240882405265967
0.3610167747247093
0.4574260007823706
0.5713996400732914
84.938
batch start
#iterations: 360
currently lose_sum: 86.97539657354355
time_elpased: 2.36
batch start
#iterations: 361
currently lose_sum: 87.00648885965347
time_elpased: 2.384
batch start
#iterations: 362
currently lose_sum: 86.6580902338028
time_elpased: 2.257
batch start
#iterations: 363
currently lose_sum: 86.36886006593704
time_elpased: 2.4
batch start
#iterations: 364
currently lose_sum: 86.36311376094818
time_elpased: 2.368
batch start
#iterations: 365
currently lose_sum: 85.82862836122513
time_elpased: 2.293
batch start
#iterations: 366
currently lose_sum: 86.42071384191513
time_elpased: 2.485
batch start
#iterations: 367
currently lose_sum: 85.54766827821732
time_elpased: 2.35
batch start
#iterations: 368
currently lose_sum: 85.31495380401611
time_elpased: 2.413
batch start
#iterations: 369
currently lose_sum: 85.62393951416016
time_elpased: 2.364
batch start
#iterations: 370
currently lose_sum: 85.36621367931366
time_elpased: 2.234
batch start
#iterations: 371
currently lose_sum: 84.95001298189163
time_elpased: 2.341
batch start
#iterations: 372
currently lose_sum: 86.12164103984833
time_elpased: 2.329
batch start
#iterations: 373
currently lose_sum: 85.11712515354156
time_elpased: 2.295
batch start
#iterations: 374
currently lose_sum: 86.64895242452621
time_elpased: 2.435
batch start
#iterations: 375
currently lose_sum: 85.39788043498993
time_elpased: 2.351
batch start
#iterations: 376
currently lose_sum: 85.13100826740265
time_elpased: 2.432
batch start
#iterations: 377
currently lose_sum: 85.52660077810287
time_elpased: 2.457
batch start
#iterations: 378
currently lose_sum: 85.5934351682663
time_elpased: 2.325
batch start
#iterations: 379
currently lose_sum: 84.97985672950745
time_elpased: 2.359
start validation test
0.5696907216494845
0.6228243315987798
0.357209015128126
0.4540222367560497
0.5700637660583313
90.120
batch start
#iterations: 380
currently lose_sum: 84.20224559307098
time_elpased: 2.412
batch start
#iterations: 381
currently lose_sum: 83.77955329418182
time_elpased: 2.413
batch start
#iterations: 382
currently lose_sum: 83.66225409507751
time_elpased: 2.311
batch start
#iterations: 383
currently lose_sum: 83.83753526210785
time_elpased: 2.368
batch start
#iterations: 384
currently lose_sum: 84.49631363153458
time_elpased: 2.347
batch start
#iterations: 385
currently lose_sum: 84.06910687685013
time_elpased: 2.235
batch start
#iterations: 386
currently lose_sum: 84.4834223985672
time_elpased: 2.499
batch start
#iterations: 387
currently lose_sum: 84.29042083024979
time_elpased: 2.366
batch start
#iterations: 388
currently lose_sum: 83.92065662145615
time_elpased: 2.357
batch start
#iterations: 389
currently lose_sum: 83.98394501209259
time_elpased: 2.267
batch start
#iterations: 390
currently lose_sum: 83.48418807983398
time_elpased: 2.252
batch start
#iterations: 391
currently lose_sum: 84.18335348367691
time_elpased: 2.394
batch start
#iterations: 392
currently lose_sum: 84.50475841760635
time_elpased: 2.369
batch start
#iterations: 393
currently lose_sum: 83.75344961881638
time_elpased: 2.351
batch start
#iterations: 394
currently lose_sum: 83.31491190195084
time_elpased: 2.394
batch start
#iterations: 395
currently lose_sum: 83.59518045186996
time_elpased: 2.365
batch start
#iterations: 396
currently lose_sum: 83.26537853479385
time_elpased: 2.333
batch start
#iterations: 397
currently lose_sum: 83.53056454658508
time_elpased: 2.352
batch start
#iterations: 398
currently lose_sum: 83.55260437726974
time_elpased: 2.457
batch start
#iterations: 399
currently lose_sum: 82.90780586004257
time_elpased: 2.353
start validation test
0.5607731958762887
0.6186507936507937
0.3208809303282906
0.42257911499627293
0.5611943637492945
98.184
acc: 0.601
pre: 0.625
rec: 0.511
F1: 0.562
auc: 0.635
