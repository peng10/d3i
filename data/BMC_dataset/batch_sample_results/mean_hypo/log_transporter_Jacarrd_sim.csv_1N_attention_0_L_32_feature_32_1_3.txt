start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.6788055896759
time_elpased: 2.238
batch start
#iterations: 1
currently lose_sum: 100.38520961999893
time_elpased: 1.99
batch start
#iterations: 2
currently lose_sum: 100.29478096961975
time_elpased: 2.09
batch start
#iterations: 3
currently lose_sum: 100.17943155765533
time_elpased: 2.103
batch start
#iterations: 4
currently lose_sum: 100.07693004608154
time_elpased: 2.064
batch start
#iterations: 5
currently lose_sum: 100.02513241767883
time_elpased: 2.076
batch start
#iterations: 6
currently lose_sum: 100.05863118171692
time_elpased: 2.025
batch start
#iterations: 7
currently lose_sum: 99.84535312652588
time_elpased: 2.15
batch start
#iterations: 8
currently lose_sum: 100.0811453461647
time_elpased: 1.806
batch start
#iterations: 9
currently lose_sum: 100.11738073825836
time_elpased: 2.31
batch start
#iterations: 10
currently lose_sum: 99.95287442207336
time_elpased: 1.956
batch start
#iterations: 11
currently lose_sum: 99.98772251605988
time_elpased: 1.794
batch start
#iterations: 12
currently lose_sum: 99.96127873659134
time_elpased: 2.15
batch start
#iterations: 13
currently lose_sum: 100.11428970098495
time_elpased: 2.092
batch start
#iterations: 14
currently lose_sum: 99.90652984380722
time_elpased: 1.903
batch start
#iterations: 15
currently lose_sum: 99.93471145629883
time_elpased: 2.217
batch start
#iterations: 16
currently lose_sum: 99.95148009061813
time_elpased: 2.097
batch start
#iterations: 17
currently lose_sum: 99.96499681472778
time_elpased: 1.773
batch start
#iterations: 18
currently lose_sum: 99.89745169878006
time_elpased: 2.16
batch start
#iterations: 19
currently lose_sum: 99.77134495973587
time_elpased: 2.094
start validation test
0.561494845361
0.629139072848
0.30310827501
0.409113009655
0.561921754192
66.269
batch start
#iterations: 20
currently lose_sum: 99.90103924274445
time_elpased: 2.102
batch start
#iterations: 21
currently lose_sum: 99.90330582857132
time_elpased: 2.304
batch start
#iterations: 22
currently lose_sum: 99.8623349070549
time_elpased: 2.16
batch start
#iterations: 23
currently lose_sum: 99.8099713921547
time_elpased: 1.843
batch start
#iterations: 24
currently lose_sum: 99.75797015428543
time_elpased: 2.025
batch start
#iterations: 25
currently lose_sum: 99.7502498626709
time_elpased: 2.198
batch start
#iterations: 26
currently lose_sum: 99.83776199817657
time_elpased: 2.165
batch start
#iterations: 27
currently lose_sum: 99.78644973039627
time_elpased: 2.138
batch start
#iterations: 28
currently lose_sum: 99.91652983427048
time_elpased: 2.163
batch start
#iterations: 29
currently lose_sum: 99.87525457143784
time_elpased: 2.175
batch start
#iterations: 30
currently lose_sum: 99.63245511054993
time_elpased: 2.128
batch start
#iterations: 31
currently lose_sum: 99.7848933339119
time_elpased: 2.141
batch start
#iterations: 32
currently lose_sum: 99.83079159259796
time_elpased: 2.052
batch start
#iterations: 33
currently lose_sum: 99.75650811195374
time_elpased: 2.256
batch start
#iterations: 34
currently lose_sum: 99.80659538507462
time_elpased: 2.191
batch start
#iterations: 35
currently lose_sum: 99.71380400657654
time_elpased: 2.122
batch start
#iterations: 36
currently lose_sum: 99.76690644025803
time_elpased: 2.095
batch start
#iterations: 37
currently lose_sum: 99.72084909677505
time_elpased: 2.287
batch start
#iterations: 38
currently lose_sum: 99.68649816513062
time_elpased: 2.209
batch start
#iterations: 39
currently lose_sum: 99.55781018733978
time_elpased: 2.137
start validation test
0.581030927835
0.622003687769
0.416632358995
0.499013806706
0.581302548767
65.777
batch start
#iterations: 40
currently lose_sum: 99.67556029558182
time_elpased: 2.079
batch start
#iterations: 41
currently lose_sum: 99.70620256662369
time_elpased: 2.114
batch start
#iterations: 42
currently lose_sum: 99.7036817073822
time_elpased: 2.257
batch start
#iterations: 43
currently lose_sum: 99.65415745973587
time_elpased: 2.195
batch start
#iterations: 44
currently lose_sum: 99.60288071632385
time_elpased: 2.215
batch start
#iterations: 45
currently lose_sum: 99.73783946037292
time_elpased: 2.258
batch start
#iterations: 46
currently lose_sum: 99.52954143285751
time_elpased: 2.222
batch start
#iterations: 47
currently lose_sum: 99.66498756408691
time_elpased: 2.109
batch start
#iterations: 48
currently lose_sum: 99.67402124404907
time_elpased: 2.144
batch start
#iterations: 49
currently lose_sum: 99.73689550161362
time_elpased: 2.23
batch start
#iterations: 50
currently lose_sum: 99.6807541847229
time_elpased: 1.85
batch start
#iterations: 51
currently lose_sum: 99.5832132101059
time_elpased: 2.107
batch start
#iterations: 52
currently lose_sum: 99.60816031694412
time_elpased: 2.29
batch start
#iterations: 53
currently lose_sum: 99.59801256656647
time_elpased: 2.182
batch start
#iterations: 54
currently lose_sum: 99.72263199090958
time_elpased: 2.294
batch start
#iterations: 55
currently lose_sum: 99.74132561683655
time_elpased: 2.314
batch start
#iterations: 56
currently lose_sum: 99.63640373945236
time_elpased: 2.15
batch start
#iterations: 57
currently lose_sum: 99.65878492593765
time_elpased: 2.131
batch start
#iterations: 58
currently lose_sum: 99.6079073548317
time_elpased: 2.251
batch start
#iterations: 59
currently lose_sum: 99.57064086198807
time_elpased: 2.037
start validation test
0.570051546392
0.627764356068
0.347673939893
0.447506127045
0.570418960859
65.897
batch start
#iterations: 60
currently lose_sum: 99.52066719532013
time_elpased: 2.176
batch start
#iterations: 61
currently lose_sum: 99.74448043107986
time_elpased: 2.182
batch start
#iterations: 62
currently lose_sum: 99.7559415102005
time_elpased: 2.153
batch start
#iterations: 63
currently lose_sum: 99.56455487012863
time_elpased: 2.116
batch start
#iterations: 64
currently lose_sum: 99.62636029720306
time_elpased: 2.216
batch start
#iterations: 65
currently lose_sum: 99.55370855331421
time_elpased: 2.094
batch start
#iterations: 66
currently lose_sum: 99.70151877403259
time_elpased: 2.152
batch start
#iterations: 67
currently lose_sum: 99.45433413982391
time_elpased: 2.051
batch start
#iterations: 68
currently lose_sum: 99.68349349498749
time_elpased: 2.202
batch start
#iterations: 69
currently lose_sum: 99.53241574764252
time_elpased: 1.837
batch start
#iterations: 70
currently lose_sum: 99.55069881677628
time_elpased: 1.839
batch start
#iterations: 71
currently lose_sum: 99.5241464972496
time_elpased: 2.082
batch start
#iterations: 72
currently lose_sum: 99.54575634002686
time_elpased: 2.095
batch start
#iterations: 73
currently lose_sum: 99.61027657985687
time_elpased: 2.134
batch start
#iterations: 74
currently lose_sum: 99.60977685451508
time_elpased: 1.893
batch start
#iterations: 75
currently lose_sum: 99.76815712451935
time_elpased: 1.549
batch start
#iterations: 76
currently lose_sum: 99.6777805685997
time_elpased: 2.113
batch start
#iterations: 77
currently lose_sum: 99.54841881990433
time_elpased: 2.09
batch start
#iterations: 78
currently lose_sum: 99.63004332780838
time_elpased: 2.113
batch start
#iterations: 79
currently lose_sum: 99.55642992258072
time_elpased: 2.06
start validation test
0.593659793814
0.623334679047
0.47673939893
0.540269434887
0.59385297084
65.484
batch start
#iterations: 80
currently lose_sum: 99.56467670202255
time_elpased: 2.015
batch start
#iterations: 81
currently lose_sum: 99.68918490409851
time_elpased: 2.188
batch start
#iterations: 82
currently lose_sum: 99.57606548070908
time_elpased: 2.169
batch start
#iterations: 83
currently lose_sum: 99.49663424491882
time_elpased: 2.141
batch start
#iterations: 84
currently lose_sum: 99.58878540992737
time_elpased: 2.002
batch start
#iterations: 85
currently lose_sum: 99.62937188148499
time_elpased: 2.067
batch start
#iterations: 86
currently lose_sum: 99.70540034770966
time_elpased: 2.195
batch start
#iterations: 87
currently lose_sum: 99.44271528720856
time_elpased: 2.148
batch start
#iterations: 88
currently lose_sum: 99.55409055948257
time_elpased: 2.208
batch start
#iterations: 89
currently lose_sum: 99.46121168136597
time_elpased: 2.107
batch start
#iterations: 90
currently lose_sum: 99.48631697893143
time_elpased: 2.102
batch start
#iterations: 91
currently lose_sum: 99.64341151714325
time_elpased: 2.171
batch start
#iterations: 92
currently lose_sum: 99.45608419179916
time_elpased: 2.164
batch start
#iterations: 93
currently lose_sum: 99.40230172872543
time_elpased: 2.009
batch start
#iterations: 94
currently lose_sum: 99.7010954618454
time_elpased: 2.185
batch start
#iterations: 95
currently lose_sum: 99.6269805431366
time_elpased: 2.108
batch start
#iterations: 96
currently lose_sum: 99.49590891599655
time_elpased: 2.109
batch start
#iterations: 97
currently lose_sum: 99.49227130413055
time_elpased: 2.091
batch start
#iterations: 98
currently lose_sum: 99.57500398159027
time_elpased: 2.047
batch start
#iterations: 99
currently lose_sum: 99.44662797451019
time_elpased: 2.197
start validation test
0.599020618557
0.631536058672
0.478592013174
0.544528368171
0.599219591882
65.441
batch start
#iterations: 100
currently lose_sum: 99.69087588787079
time_elpased: 2.108
batch start
#iterations: 101
currently lose_sum: 99.56738591194153
time_elpased: 2.07
batch start
#iterations: 102
currently lose_sum: 99.60706704854965
time_elpased: 1.916
batch start
#iterations: 103
currently lose_sum: 99.63795059919357
time_elpased: 2.199
batch start
#iterations: 104
currently lose_sum: 99.65207839012146
time_elpased: 1.925
batch start
#iterations: 105
currently lose_sum: 99.69706267118454
time_elpased: 2.165
batch start
#iterations: 106
currently lose_sum: 99.54581761360168
time_elpased: 2.117
batch start
#iterations: 107
currently lose_sum: 99.50416141748428
time_elpased: 1.922
batch start
#iterations: 108
currently lose_sum: 99.38716244697571
time_elpased: 2.185
batch start
#iterations: 109
currently lose_sum: 99.57760274410248
time_elpased: 2.126
batch start
#iterations: 110
currently lose_sum: 99.63782948255539
time_elpased: 2.263
batch start
#iterations: 111
currently lose_sum: 99.50229185819626
time_elpased: 2.043
batch start
#iterations: 112
currently lose_sum: 99.41163897514343
time_elpased: 2.292
batch start
#iterations: 113
currently lose_sum: 99.59349852800369
time_elpased: 1.963
batch start
#iterations: 114
currently lose_sum: 99.6998108625412
time_elpased: 2.005
batch start
#iterations: 115
currently lose_sum: 99.46473318338394
time_elpased: 2.007
batch start
#iterations: 116
currently lose_sum: 99.64070957899094
time_elpased: 2.092
batch start
#iterations: 117
currently lose_sum: 99.48227244615555
time_elpased: 2.237
batch start
#iterations: 118
currently lose_sum: 99.78698319196701
time_elpased: 2.143
batch start
#iterations: 119
currently lose_sum: 99.63480705022812
time_elpased: 2.13
start validation test
0.593659793814
0.627699595931
0.463668176204
0.53335701178
0.593874567243
65.463
batch start
#iterations: 120
currently lose_sum: 99.4639744758606
time_elpased: 1.842
batch start
#iterations: 121
currently lose_sum: 99.63588112592697
time_elpased: 2.123
batch start
#iterations: 122
currently lose_sum: 99.65134435892105
time_elpased: 2.043
batch start
#iterations: 123
currently lose_sum: 99.69165128469467
time_elpased: 2.111
batch start
#iterations: 124
currently lose_sum: 99.56493419408798
time_elpased: 2.049
batch start
#iterations: 125
currently lose_sum: 99.67542213201523
time_elpased: 2.093
batch start
#iterations: 126
currently lose_sum: 99.53591448068619
time_elpased: 1.965
batch start
#iterations: 127
currently lose_sum: 99.55036300420761
time_elpased: 1.977
batch start
#iterations: 128
currently lose_sum: 99.5460034608841
time_elpased: 1.871
batch start
#iterations: 129
currently lose_sum: 99.51097524166107
time_elpased: 2.107
batch start
#iterations: 130
currently lose_sum: 99.53412109613419
time_elpased: 2.138
batch start
#iterations: 131
currently lose_sum: 99.44745808839798
time_elpased: 2.133
batch start
#iterations: 132
currently lose_sum: 99.57801878452301
time_elpased: 2.175
batch start
#iterations: 133
currently lose_sum: 99.59047967195511
time_elpased: 1.96
batch start
#iterations: 134
currently lose_sum: 99.49972450733185
time_elpased: 2.144
batch start
#iterations: 135
currently lose_sum: 99.56004494428635
time_elpased: 2.222
batch start
#iterations: 136
currently lose_sum: 99.60146975517273
time_elpased: 2.16
batch start
#iterations: 137
currently lose_sum: 99.65475815534592
time_elpased: 2.14
batch start
#iterations: 138
currently lose_sum: 99.56635004281998
time_elpased: 2.092
batch start
#iterations: 139
currently lose_sum: 99.49256885051727
time_elpased: 2.195
start validation test
0.593969072165
0.624408063861
0.474989707699
0.539545215409
0.594165651041
65.456
batch start
#iterations: 140
currently lose_sum: 99.54762762784958
time_elpased: 1.971
batch start
#iterations: 141
currently lose_sum: 99.65226411819458
time_elpased: 2.094
batch start
#iterations: 142
currently lose_sum: 99.59545665979385
time_elpased: 1.9
batch start
#iterations: 143
currently lose_sum: 99.59110593795776
time_elpased: 1.882
batch start
#iterations: 144
currently lose_sum: 99.55625200271606
time_elpased: 2.086
batch start
#iterations: 145
currently lose_sum: 99.57310301065445
time_elpased: 2.127
batch start
#iterations: 146
currently lose_sum: 99.5178496837616
time_elpased: 2.196
batch start
#iterations: 147
currently lose_sum: 99.5422939658165
time_elpased: 2.257
batch start
#iterations: 148
currently lose_sum: 99.5176493525505
time_elpased: 2.071
batch start
#iterations: 149
currently lose_sum: 99.53329503536224
time_elpased: 2.25
batch start
#iterations: 150
currently lose_sum: 99.56876093149185
time_elpased: 2.119
batch start
#iterations: 151
currently lose_sum: 99.48351663351059
time_elpased: 1.996
batch start
#iterations: 152
currently lose_sum: 99.48697030544281
time_elpased: 2.053
batch start
#iterations: 153
currently lose_sum: 99.55174577236176
time_elpased: 2.088
batch start
#iterations: 154
currently lose_sum: 99.57904928922653
time_elpased: 2.008
batch start
#iterations: 155
currently lose_sum: 99.52904838323593
time_elpased: 2.126
batch start
#iterations: 156
currently lose_sum: 99.40789878368378
time_elpased: 1.863
batch start
#iterations: 157
currently lose_sum: 99.46882086992264
time_elpased: 2.119
batch start
#iterations: 158
currently lose_sum: 99.52057188749313
time_elpased: 2.185
batch start
#iterations: 159
currently lose_sum: 99.41226518154144
time_elpased: 2.141
start validation test
0.604175257732
0.623349884946
0.529744750926
0.572748010905
0.604298232547
65.274
batch start
#iterations: 160
currently lose_sum: 99.53642904758453
time_elpased: 2.192
batch start
#iterations: 161
currently lose_sum: 99.59689921140671
time_elpased: 1.947
batch start
#iterations: 162
currently lose_sum: 99.5227432847023
time_elpased: 2.06
batch start
#iterations: 163
currently lose_sum: 99.48748701810837
time_elpased: 2.324
batch start
#iterations: 164
currently lose_sum: 99.54484528303146
time_elpased: 2.077
batch start
#iterations: 165
currently lose_sum: 99.48562383651733
time_elpased: 1.955
batch start
#iterations: 166
currently lose_sum: 99.5317451953888
time_elpased: 2.126
batch start
#iterations: 167
currently lose_sum: 99.48300635814667
time_elpased: 2.083
batch start
#iterations: 168
currently lose_sum: 99.74056386947632
time_elpased: 2.188
batch start
#iterations: 169
currently lose_sum: 99.48894375562668
time_elpased: 2.333
batch start
#iterations: 170
currently lose_sum: 99.41457140445709
time_elpased: 2.083
batch start
#iterations: 171
currently lose_sum: 99.56533616781235
time_elpased: 2.153
batch start
#iterations: 172
currently lose_sum: 99.61574530601501
time_elpased: 2.11
batch start
#iterations: 173
currently lose_sum: 99.45441287755966
time_elpased: 2.173
batch start
#iterations: 174
currently lose_sum: 99.45593148469925
time_elpased: 2.28
batch start
#iterations: 175
currently lose_sum: 99.51316046714783
time_elpased: 2.205
batch start
#iterations: 176
currently lose_sum: 99.36809462308884
time_elpased: 1.714
batch start
#iterations: 177
currently lose_sum: 99.62507677078247
time_elpased: 2.166
batch start
#iterations: 178
currently lose_sum: 99.51951277256012
time_elpased: 2.182
batch start
#iterations: 179
currently lose_sum: 99.37903785705566
time_elpased: 2.207
start validation test
0.600257731959
0.625689014229
0.502367229312
0.557287206713
0.60041946761
65.304
batch start
#iterations: 180
currently lose_sum: 99.38658028841019
time_elpased: 2.286
batch start
#iterations: 181
currently lose_sum: 99.5229709148407
time_elpased: 2.265
batch start
#iterations: 182
currently lose_sum: 99.5164275765419
time_elpased: 2.177
batch start
#iterations: 183
currently lose_sum: 99.47108572721481
time_elpased: 2.126
batch start
#iterations: 184
currently lose_sum: 99.3506590127945
time_elpased: 1.994
batch start
#iterations: 185
currently lose_sum: 99.51472020149231
time_elpased: 2.122
batch start
#iterations: 186
currently lose_sum: 99.54477322101593
time_elpased: 2.067
batch start
#iterations: 187
currently lose_sum: 99.60665827989578
time_elpased: 2.078
batch start
#iterations: 188
currently lose_sum: 99.50002264976501
time_elpased: 2.27
batch start
#iterations: 189
currently lose_sum: 99.61232095956802
time_elpased: 2.234
batch start
#iterations: 190
currently lose_sum: 99.57613039016724
time_elpased: 2.077
batch start
#iterations: 191
currently lose_sum: 99.39876067638397
time_elpased: 1.801
batch start
#iterations: 192
currently lose_sum: 99.52568686008453
time_elpased: 2.036
batch start
#iterations: 193
currently lose_sum: 99.50239396095276
time_elpased: 2.196
batch start
#iterations: 194
currently lose_sum: 99.54090309143066
time_elpased: 2.137
batch start
#iterations: 195
currently lose_sum: 99.60092514753342
time_elpased: 2.134
batch start
#iterations: 196
currently lose_sum: 99.51910668611526
time_elpased: 2.105
batch start
#iterations: 197
currently lose_sum: 99.3064700961113
time_elpased: 2.169
batch start
#iterations: 198
currently lose_sum: 99.44271647930145
time_elpased: 2.076
batch start
#iterations: 199
currently lose_sum: 99.54377830028534
time_elpased: 2.125
start validation test
0.593659793814
0.639222239101
0.433100041169
0.516350696362
0.593925072216
65.402
batch start
#iterations: 200
currently lose_sum: 99.59390771389008
time_elpased: 1.771
batch start
#iterations: 201
currently lose_sum: 99.49149698019028
time_elpased: 1.792
batch start
#iterations: 202
currently lose_sum: 99.32547628879547
time_elpased: 1.757
batch start
#iterations: 203
currently lose_sum: 99.41426569223404
time_elpased: 1.963
batch start
#iterations: 204
currently lose_sum: 99.51812535524368
time_elpased: 2.107
batch start
#iterations: 205
currently lose_sum: 99.54630613327026
time_elpased: 2.15
batch start
#iterations: 206
currently lose_sum: 99.50370639562607
time_elpased: 2.212
batch start
#iterations: 207
currently lose_sum: 99.46911060810089
time_elpased: 2.14
batch start
#iterations: 208
currently lose_sum: 99.78661906719208
time_elpased: 2.143
batch start
#iterations: 209
currently lose_sum: 99.59387981891632
time_elpased: 2.15
batch start
#iterations: 210
currently lose_sum: 99.50534522533417
time_elpased: 1.734
batch start
#iterations: 211
currently lose_sum: 99.06391870975494
time_elpased: 1.969
batch start
#iterations: 212
currently lose_sum: 99.42897701263428
time_elpased: 2.247
batch start
#iterations: 213
currently lose_sum: 99.5001151561737
time_elpased: 1.918
batch start
#iterations: 214
currently lose_sum: 99.53683167695999
time_elpased: 1.968
batch start
#iterations: 215
currently lose_sum: 99.49873435497284
time_elpased: 2.178
batch start
#iterations: 216
currently lose_sum: 99.48793995380402
time_elpased: 2.084
batch start
#iterations: 217
currently lose_sum: 99.2841066122055
time_elpased: 2.112
batch start
#iterations: 218
currently lose_sum: 99.49756747484207
time_elpased: 2.142
batch start
#iterations: 219
currently lose_sum: 99.45643842220306
time_elpased: 2.106
start validation test
0.599587628866
0.632806108536
0.477665706052
0.544398826979
0.599789069465
65.214
batch start
#iterations: 220
currently lose_sum: 99.25000667572021
time_elpased: 1.88
batch start
#iterations: 221
currently lose_sum: 99.39689064025879
time_elpased: 1.95
batch start
#iterations: 222
currently lose_sum: 99.4461720585823
time_elpased: 2.418
batch start
#iterations: 223
currently lose_sum: 99.27602130174637
time_elpased: 2.205
batch start
#iterations: 224
currently lose_sum: 99.48004388809204
time_elpased: 2.034
batch start
#iterations: 225
currently lose_sum: 99.44443809986115
time_elpased: 2.184
batch start
#iterations: 226
currently lose_sum: 99.4797500371933
time_elpased: 2.174
batch start
#iterations: 227
currently lose_sum: 99.52564930915833
time_elpased: 2.222
batch start
#iterations: 228
currently lose_sum: 99.50085717439651
time_elpased: 2.083
batch start
#iterations: 229
currently lose_sum: 99.43820780515671
time_elpased: 2.183
batch start
#iterations: 230
currently lose_sum: 99.41883111000061
time_elpased: 2.228
batch start
#iterations: 231
currently lose_sum: 99.57168072462082
time_elpased: 2.126
batch start
#iterations: 232
currently lose_sum: 99.55758547782898
time_elpased: 2.161
batch start
#iterations: 233
currently lose_sum: 99.38622778654099
time_elpased: 1.89
batch start
#iterations: 234
currently lose_sum: 99.44695401191711
time_elpased: 2.106
batch start
#iterations: 235
currently lose_sum: 99.57062870264053
time_elpased: 2.161
batch start
#iterations: 236
currently lose_sum: 99.54264402389526
time_elpased: 2.094
batch start
#iterations: 237
currently lose_sum: 99.47351723909378
time_elpased: 2.114
batch start
#iterations: 238
currently lose_sum: 99.4301056265831
time_elpased: 1.999
batch start
#iterations: 239
currently lose_sum: 99.73969841003418
time_elpased: 2.094
start validation test
0.592577319588
0.644682210156
0.415500205846
0.505319814745
0.592869888136
65.362
batch start
#iterations: 240
currently lose_sum: 99.4060360789299
time_elpased: 2.137
batch start
#iterations: 241
currently lose_sum: 99.57856947183609
time_elpased: 2.148
batch start
#iterations: 242
currently lose_sum: 99.45120996236801
time_elpased: 2.14
batch start
#iterations: 243
currently lose_sum: 99.56297570466995
time_elpased: 2.272
batch start
#iterations: 244
currently lose_sum: 99.37537413835526
time_elpased: 2.294
batch start
#iterations: 245
currently lose_sum: 99.47806298732758
time_elpased: 2.361
batch start
#iterations: 246
currently lose_sum: 99.6869745850563
time_elpased: 2.31
batch start
#iterations: 247
currently lose_sum: 99.40154141187668
time_elpased: 2.257
batch start
#iterations: 248
currently lose_sum: 99.62036830186844
time_elpased: 2.172
batch start
#iterations: 249
currently lose_sum: 99.38821178674698
time_elpased: 1.961
batch start
#iterations: 250
currently lose_sum: 99.5130809545517
time_elpased: 2.258
batch start
#iterations: 251
currently lose_sum: 99.35652911663055
time_elpased: 2.2
batch start
#iterations: 252
currently lose_sum: 99.57019299268723
time_elpased: 2.158
batch start
#iterations: 253
currently lose_sum: 99.44620764255524
time_elpased: 2.257
batch start
#iterations: 254
currently lose_sum: 99.36462277173996
time_elpased: 2.303
batch start
#iterations: 255
currently lose_sum: 99.37578731775284
time_elpased: 2.151
batch start
#iterations: 256
currently lose_sum: 99.32395660877228
time_elpased: 1.85
batch start
#iterations: 257
currently lose_sum: 99.56735247373581
time_elpased: 1.858
batch start
#iterations: 258
currently lose_sum: 99.4334043264389
time_elpased: 1.742
batch start
#iterations: 259
currently lose_sum: 99.52185797691345
time_elpased: 1.616
start validation test
0.601237113402
0.633603238866
0.483223548786
0.548289150998
0.601432096574
65.277
batch start
#iterations: 260
currently lose_sum: 99.45379251241684
time_elpased: 2.359
batch start
#iterations: 261
currently lose_sum: 99.52854466438293
time_elpased: 2.075
batch start
#iterations: 262
currently lose_sum: 99.37934422492981
time_elpased: 1.66
batch start
#iterations: 263
currently lose_sum: 99.45776396989822
time_elpased: 2.067
batch start
#iterations: 264
currently lose_sum: 99.61196327209473
time_elpased: 2.131
batch start
#iterations: 265
currently lose_sum: 99.47764366865158
time_elpased: 2.202
batch start
#iterations: 266
currently lose_sum: 99.45500206947327
time_elpased: 2.295
batch start
#iterations: 267
currently lose_sum: 99.48379278182983
time_elpased: 2.177
batch start
#iterations: 268
currently lose_sum: 99.47656518220901
time_elpased: 2.105
batch start
#iterations: 269
currently lose_sum: 99.27381312847137
time_elpased: 2.164
batch start
#iterations: 270
currently lose_sum: 99.31660050153732
time_elpased: 2.152
batch start
#iterations: 271
currently lose_sum: 99.39716082811356
time_elpased: 2.213
batch start
#iterations: 272
currently lose_sum: 99.57790845632553
time_elpased: 1.976
batch start
#iterations: 273
currently lose_sum: 99.48201411962509
time_elpased: 2.002
batch start
#iterations: 274
currently lose_sum: 99.51797586679459
time_elpased: 1.787
batch start
#iterations: 275
currently lose_sum: 99.4226747751236
time_elpased: 2.206
batch start
#iterations: 276
currently lose_sum: 99.68221771717072
time_elpased: 2.129
batch start
#iterations: 277
currently lose_sum: 99.3502584695816
time_elpased: 2.208
batch start
#iterations: 278
currently lose_sum: 99.45715779066086
time_elpased: 2.165
batch start
#iterations: 279
currently lose_sum: 99.52009218931198
time_elpased: 2.338
start validation test
0.603350515464
0.636204340208
0.485796624125
0.550919171287
0.603544739159
65.238
batch start
#iterations: 280
currently lose_sum: 99.58374446630478
time_elpased: 2.163
batch start
#iterations: 281
currently lose_sum: 99.37504577636719
time_elpased: 2.214
batch start
#iterations: 282
currently lose_sum: 99.5412894487381
time_elpased: 2.251
batch start
#iterations: 283
currently lose_sum: 99.55108100175858
time_elpased: 2.189
batch start
#iterations: 284
currently lose_sum: 99.23579496145248
time_elpased: 2.124
batch start
#iterations: 285
currently lose_sum: 99.46690833568573
time_elpased: 2.066
batch start
#iterations: 286
currently lose_sum: 99.5358549952507
time_elpased: 2.238
batch start
#iterations: 287
currently lose_sum: 99.45206445455551
time_elpased: 2.104
batch start
#iterations: 288
currently lose_sum: 99.37793761491776
time_elpased: 2.194
batch start
#iterations: 289
currently lose_sum: 99.2092552781105
time_elpased: 2.211
batch start
#iterations: 290
currently lose_sum: 99.4256916642189
time_elpased: 2.241
batch start
#iterations: 291
currently lose_sum: 99.2298834323883
time_elpased: 2.155
batch start
#iterations: 292
currently lose_sum: 99.40526783466339
time_elpased: 2.108
batch start
#iterations: 293
currently lose_sum: 99.50561904907227
time_elpased: 2.236
batch start
#iterations: 294
currently lose_sum: 99.4993183016777
time_elpased: 2.156
batch start
#iterations: 295
currently lose_sum: 99.4229199886322
time_elpased: 2.118
batch start
#iterations: 296
currently lose_sum: 99.59722059965134
time_elpased: 2.229
batch start
#iterations: 297
currently lose_sum: 99.58313804864883
time_elpased: 2.2
batch start
#iterations: 298
currently lose_sum: 99.24333965778351
time_elpased: 2.071
batch start
#iterations: 299
currently lose_sum: 99.46882158517838
time_elpased: 2.194
start validation test
0.604742268041
0.631958762887
0.504734458625
0.56122682536
0.604907501927
65.186
batch start
#iterations: 300
currently lose_sum: 99.50081938505173
time_elpased: 1.881
batch start
#iterations: 301
currently lose_sum: 99.43139171600342
time_elpased: 2.133
batch start
#iterations: 302
currently lose_sum: 99.35993719100952
time_elpased: 2.131
batch start
#iterations: 303
currently lose_sum: 99.40072059631348
time_elpased: 2.253
batch start
#iterations: 304
currently lose_sum: 99.30920815467834
time_elpased: 2.26
batch start
#iterations: 305
currently lose_sum: 99.5141413807869
time_elpased: 2.132
batch start
#iterations: 306
currently lose_sum: 99.42525905370712
time_elpased: 2.097
batch start
#iterations: 307
currently lose_sum: 99.54388970136642
time_elpased: 2.195
batch start
#iterations: 308
currently lose_sum: 99.417056620121
time_elpased: 2.173
batch start
#iterations: 309
currently lose_sum: 99.41618210077286
time_elpased: 2.166
batch start
#iterations: 310
currently lose_sum: 99.52105581760406
time_elpased: 2.078
batch start
#iterations: 311
currently lose_sum: 99.23414039611816
time_elpased: 2.13
batch start
#iterations: 312
currently lose_sum: 99.52488303184509
time_elpased: 2.165
batch start
#iterations: 313
currently lose_sum: 99.54980528354645
time_elpased: 2.117
batch start
#iterations: 314
currently lose_sum: 99.50483602285385
time_elpased: 2.058
batch start
#iterations: 315
currently lose_sum: 99.38207268714905
time_elpased: 2.082
batch start
#iterations: 316
currently lose_sum: 99.46050494909286
time_elpased: 2.178
batch start
#iterations: 317
currently lose_sum: 99.58283293247223
time_elpased: 2.254
batch start
#iterations: 318
currently lose_sum: 99.39607852697372
time_elpased: 2.367
batch start
#iterations: 319
currently lose_sum: 99.56807768344879
time_elpased: 2.138
start validation test
0.605309278351
0.624200747979
0.532523672293
0.574729241877
0.605429535444
65.147
batch start
#iterations: 320
currently lose_sum: 99.50813156366348
time_elpased: 2.177
batch start
#iterations: 321
currently lose_sum: 99.47490602731705
time_elpased: 2.202
batch start
#iterations: 322
currently lose_sum: 99.50698608160019
time_elpased: 2.153
batch start
#iterations: 323
currently lose_sum: 99.48928219079971
time_elpased: 2.129
batch start
#iterations: 324
currently lose_sum: 99.53882783651352
time_elpased: 2.181
batch start
#iterations: 325
currently lose_sum: 99.34291195869446
time_elpased: 2.11
batch start
#iterations: 326
currently lose_sum: 99.31924116611481
time_elpased: 2.188
batch start
#iterations: 327
currently lose_sum: 99.70015043020248
time_elpased: 2.084
batch start
#iterations: 328
currently lose_sum: 99.40281760692596
time_elpased: 1.655
batch start
#iterations: 329
currently lose_sum: 99.46207511425018
time_elpased: 1.947
batch start
#iterations: 330
currently lose_sum: 99.47614914178848
time_elpased: 2.24
batch start
#iterations: 331
currently lose_sum: 99.59779649972916
time_elpased: 2.236
batch start
#iterations: 332
currently lose_sum: 99.28949236869812
time_elpased: 2.269
batch start
#iterations: 333
currently lose_sum: 99.48926913738251
time_elpased: 2.073
batch start
#iterations: 334
currently lose_sum: 99.53875946998596
time_elpased: 2.075
batch start
#iterations: 335
currently lose_sum: 99.3752623796463
time_elpased: 2.135
batch start
#iterations: 336
currently lose_sum: 99.32250374555588
time_elpased: 2.227
batch start
#iterations: 337
currently lose_sum: 99.25590467453003
time_elpased: 2.188
batch start
#iterations: 338
currently lose_sum: 99.47571086883545
time_elpased: 1.997
batch start
#iterations: 339
currently lose_sum: 99.36630964279175
time_elpased: 2.157
start validation test
0.602371134021
0.640156818818
0.470564018114
0.542413097639
0.602588907033
65.155
batch start
#iterations: 340
currently lose_sum: 99.46627467870712
time_elpased: 2.144
batch start
#iterations: 341
currently lose_sum: 99.45973342657089
time_elpased: 2.184
batch start
#iterations: 342
currently lose_sum: 99.52377730607986
time_elpased: 2.152
batch start
#iterations: 343
currently lose_sum: 99.67293971776962
time_elpased: 2.177
batch start
#iterations: 344
currently lose_sum: 99.45893508195877
time_elpased: 2.071
batch start
#iterations: 345
currently lose_sum: 99.39646875858307
time_elpased: 2.148
batch start
#iterations: 346
currently lose_sum: 99.2767054438591
time_elpased: 2.224
batch start
#iterations: 347
currently lose_sum: 99.40695124864578
time_elpased: 2.15
batch start
#iterations: 348
currently lose_sum: 99.66647320985794
time_elpased: 2.144
batch start
#iterations: 349
currently lose_sum: 99.51514077186584
time_elpased: 2.07
batch start
#iterations: 350
currently lose_sum: 99.41555362939835
time_elpased: 2.136
batch start
#iterations: 351
currently lose_sum: 99.55268412828445
time_elpased: 2.121
batch start
#iterations: 352
currently lose_sum: 99.43932282924652
time_elpased: 2.105
batch start
#iterations: 353
currently lose_sum: 99.65455359220505
time_elpased: 2.142
batch start
#iterations: 354
currently lose_sum: 99.43472474813461
time_elpased: 2.089
batch start
#iterations: 355
currently lose_sum: 99.48979502916336
time_elpased: 2.106
batch start
#iterations: 356
currently lose_sum: 99.52782845497131
time_elpased: 2.075
batch start
#iterations: 357
currently lose_sum: 99.55551189184189
time_elpased: 2.006
batch start
#iterations: 358
currently lose_sum: 99.4139055609703
time_elpased: 2.025
batch start
#iterations: 359
currently lose_sum: 99.3290885090828
time_elpased: 2.186
start validation test
0.605721649485
0.628241717335
0.521099217785
0.569676511955
0.605861463498
65.172
batch start
#iterations: 360
currently lose_sum: 99.33203887939453
time_elpased: 2.024
batch start
#iterations: 361
currently lose_sum: 99.45025283098221
time_elpased: 2.089
batch start
#iterations: 362
currently lose_sum: 99.59043318033218
time_elpased: 1.938
batch start
#iterations: 363
currently lose_sum: 99.42172056436539
time_elpased: 2.12
batch start
#iterations: 364
currently lose_sum: 99.28935700654984
time_elpased: 2.166
batch start
#iterations: 365
currently lose_sum: 99.5281354188919
time_elpased: 2.258
batch start
#iterations: 366
currently lose_sum: 99.52440387010574
time_elpased: 2.074
batch start
#iterations: 367
currently lose_sum: 99.35055911540985
time_elpased: 2.248
batch start
#iterations: 368
currently lose_sum: 99.42451739311218
time_elpased: 1.846
batch start
#iterations: 369
currently lose_sum: 99.36686444282532
time_elpased: 1.955
batch start
#iterations: 370
currently lose_sum: 99.57866936922073
time_elpased: 2.181
batch start
#iterations: 371
currently lose_sum: 99.34685838222504
time_elpased: 2.183
batch start
#iterations: 372
currently lose_sum: 99.37577509880066
time_elpased: 2.158
batch start
#iterations: 373
currently lose_sum: 99.39705508947372
time_elpased: 2.021
batch start
#iterations: 374
currently lose_sum: 99.43000102043152
time_elpased: 2.144
batch start
#iterations: 375
currently lose_sum: 99.2425252199173
time_elpased: 2.148
batch start
#iterations: 376
currently lose_sum: 99.35300922393799
time_elpased: 2.143
batch start
#iterations: 377
currently lose_sum: 99.29982763528824
time_elpased: 2.051
batch start
#iterations: 378
currently lose_sum: 99.60768568515778
time_elpased: 2.135
batch start
#iterations: 379
currently lose_sum: 99.38147127628326
time_elpased: 2.002
start validation test
0.595360824742
0.642095644228
0.433923425278
0.517872497236
0.595627553201
65.298
batch start
#iterations: 380
currently lose_sum: 99.53184413909912
time_elpased: 2.024
batch start
#iterations: 381
currently lose_sum: 99.34583121538162
time_elpased: 2.142
batch start
#iterations: 382
currently lose_sum: 99.41011434793472
time_elpased: 2.153
batch start
#iterations: 383
currently lose_sum: 99.51745104789734
time_elpased: 2.086
batch start
#iterations: 384
currently lose_sum: 99.44980096817017
time_elpased: 2.239
batch start
#iterations: 385
currently lose_sum: 99.7053615450859
time_elpased: 2.187
batch start
#iterations: 386
currently lose_sum: 99.54509288072586
time_elpased: 2.121
batch start
#iterations: 387
currently lose_sum: 99.42940598726273
time_elpased: 2.237
batch start
#iterations: 388
currently lose_sum: 99.4997808933258
time_elpased: 2.253
batch start
#iterations: 389
currently lose_sum: 99.3531186580658
time_elpased: 2.111
batch start
#iterations: 390
currently lose_sum: 99.40314370393753
time_elpased: 2.186
batch start
#iterations: 391
currently lose_sum: 99.43327063322067
time_elpased: 2.098
batch start
#iterations: 392
currently lose_sum: 99.47683280706406
time_elpased: 2.306
batch start
#iterations: 393
currently lose_sum: 99.46655833721161
time_elpased: 2.266
batch start
#iterations: 394
currently lose_sum: 99.40449970960617
time_elpased: 2.188
batch start
#iterations: 395
currently lose_sum: 99.40042954683304
time_elpased: 2.307
batch start
#iterations: 396
currently lose_sum: 99.44480234384537
time_elpased: 2.166
batch start
#iterations: 397
currently lose_sum: 99.59317320585251
time_elpased: 2.125
batch start
#iterations: 398
currently lose_sum: 99.45461589097977
time_elpased: 2.233
batch start
#iterations: 399
currently lose_sum: 99.54105371236801
time_elpased: 1.979
start validation test
0.594226804124
0.645609602021
0.420749279539
0.509471585244
0.594513425395
65.374
acc: 0.608
pre: 0.627
rec: 0.538
F1: 0.579
auc: 0.645
