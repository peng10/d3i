start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.41254162788391
time_elpased: 2.305
batch start
#iterations: 1
currently lose_sum: 100.26518374681473
time_elpased: 2.399
batch start
#iterations: 2
currently lose_sum: 100.21974557638168
time_elpased: 2.371
batch start
#iterations: 3
currently lose_sum: 100.33421915769577
time_elpased: 1.99
batch start
#iterations: 4
currently lose_sum: 100.18485796451569
time_elpased: 2.362
batch start
#iterations: 5
currently lose_sum: 100.21069753170013
time_elpased: 2.575
batch start
#iterations: 6
currently lose_sum: 100.25632059574127
time_elpased: 2.436
batch start
#iterations: 7
currently lose_sum: 100.03885942697525
time_elpased: 2.422
batch start
#iterations: 8
currently lose_sum: 100.12148904800415
time_elpased: 2.34
batch start
#iterations: 9
currently lose_sum: 100.01942324638367
time_elpased: 2.27
batch start
#iterations: 10
currently lose_sum: 100.0058319568634
time_elpased: 2.277
batch start
#iterations: 11
currently lose_sum: 99.92401337623596
time_elpased: 2.293
batch start
#iterations: 12
currently lose_sum: 99.98364877700806
time_elpased: 2.325
batch start
#iterations: 13
currently lose_sum: 99.9803454875946
time_elpased: 2.351
batch start
#iterations: 14
currently lose_sum: 99.87494796514511
time_elpased: 2.544
batch start
#iterations: 15
currently lose_sum: 99.8319821357727
time_elpased: 2.508
batch start
#iterations: 16
currently lose_sum: 99.68654406070709
time_elpased: 2.39
batch start
#iterations: 17
currently lose_sum: 99.73964780569077
time_elpased: 2.279
batch start
#iterations: 18
currently lose_sum: 99.75475418567657
time_elpased: 2.395
batch start
#iterations: 19
currently lose_sum: 99.71267879009247
time_elpased: 2.523
start validation test
0.589072164948
0.606675632718
0.510651435628
0.554537326777
0.589209844634
65.575
batch start
#iterations: 20
currently lose_sum: 99.65371906757355
time_elpased: 2.393
batch start
#iterations: 21
currently lose_sum: 99.60977202653885
time_elpased: 2.348
batch start
#iterations: 22
currently lose_sum: 99.7222346663475
time_elpased: 2.427
batch start
#iterations: 23
currently lose_sum: 99.53509855270386
time_elpased: 2.342
batch start
#iterations: 24
currently lose_sum: 99.66896045207977
time_elpased: 2.44
batch start
#iterations: 25
currently lose_sum: 99.68361055850983
time_elpased: 2.278
batch start
#iterations: 26
currently lose_sum: 99.60173964500427
time_elpased: 2.369
batch start
#iterations: 27
currently lose_sum: 99.41865795850754
time_elpased: 2.359
batch start
#iterations: 28
currently lose_sum: 99.42778491973877
time_elpased: 2.507
batch start
#iterations: 29
currently lose_sum: 99.37399780750275
time_elpased: 2.39
batch start
#iterations: 30
currently lose_sum: 99.55105078220367
time_elpased: 2.317
batch start
#iterations: 31
currently lose_sum: 99.30537211894989
time_elpased: 2.451
batch start
#iterations: 32
currently lose_sum: 99.29551863670349
time_elpased: 2.391
batch start
#iterations: 33
currently lose_sum: 99.354816198349
time_elpased: 2.328
batch start
#iterations: 34
currently lose_sum: 99.35185420513153
time_elpased: 2.385
batch start
#iterations: 35
currently lose_sum: 99.26061403751373
time_elpased: 2.264
batch start
#iterations: 36
currently lose_sum: 99.10908836126328
time_elpased: 2.441
batch start
#iterations: 37
currently lose_sum: 99.43924152851105
time_elpased: 2.36
batch start
#iterations: 38
currently lose_sum: 99.02483421564102
time_elpased: 2.403
batch start
#iterations: 39
currently lose_sum: 99.24017518758774
time_elpased: 2.335
start validation test
0.600103092784
0.609453570231
0.561284347021
0.5843780135
0.600171245079
65.246
batch start
#iterations: 40
currently lose_sum: 99.2033970952034
time_elpased: 2.482
batch start
#iterations: 41
currently lose_sum: 99.20360165834427
time_elpased: 2.485
batch start
#iterations: 42
currently lose_sum: 98.94473218917847
time_elpased: 2.389
batch start
#iterations: 43
currently lose_sum: 98.94831436872482
time_elpased: 2.344
batch start
#iterations: 44
currently lose_sum: 99.23731637001038
time_elpased: 2.4
batch start
#iterations: 45
currently lose_sum: 99.02322989702225
time_elpased: 2.441
batch start
#iterations: 46
currently lose_sum: 98.7983912229538
time_elpased: 2.424
batch start
#iterations: 47
currently lose_sum: 99.07499599456787
time_elpased: 2.371
batch start
#iterations: 48
currently lose_sum: 99.13347631692886
time_elpased: 2.426
batch start
#iterations: 49
currently lose_sum: 99.20122534036636
time_elpased: 2.34
batch start
#iterations: 50
currently lose_sum: 99.0614984035492
time_elpased: 2.243
batch start
#iterations: 51
currently lose_sum: 99.03752607107162
time_elpased: 2.43
batch start
#iterations: 52
currently lose_sum: 98.9785338640213
time_elpased: 2.499
batch start
#iterations: 53
currently lose_sum: 99.02048128843307
time_elpased: 2.196
batch start
#iterations: 54
currently lose_sum: 99.08655732870102
time_elpased: 2.142
batch start
#iterations: 55
currently lose_sum: 99.11999922990799
time_elpased: 2.388
batch start
#iterations: 56
currently lose_sum: 99.08041894435883
time_elpased: 2.481
batch start
#iterations: 57
currently lose_sum: 98.97384697198868
time_elpased: 2.466
batch start
#iterations: 58
currently lose_sum: 98.92718309164047
time_elpased: 2.49
batch start
#iterations: 59
currently lose_sum: 98.91230905056
time_elpased: 2.482
start validation test
0.580618556701
0.587551223834
0.545950396213
0.565987410648
0.580679422004
65.539
batch start
#iterations: 60
currently lose_sum: 99.03689330816269
time_elpased: 2.411
batch start
#iterations: 61
currently lose_sum: 98.97288507223129
time_elpased: 2.407
batch start
#iterations: 62
currently lose_sum: 98.78784656524658
time_elpased: 2.434
batch start
#iterations: 63
currently lose_sum: 99.16115939617157
time_elpased: 2.336
batch start
#iterations: 64
currently lose_sum: 99.0551917552948
time_elpased: 2.433
batch start
#iterations: 65
currently lose_sum: 98.78089946508408
time_elpased: 2.518
batch start
#iterations: 66
currently lose_sum: 98.90041142702103
time_elpased: 2.409
batch start
#iterations: 67
currently lose_sum: 98.88665878772736
time_elpased: 2.306
batch start
#iterations: 68
currently lose_sum: 98.79154390096664
time_elpased: 1.78
batch start
#iterations: 69
currently lose_sum: 98.88800555467606
time_elpased: 2.231
batch start
#iterations: 70
currently lose_sum: 99.00474792718887
time_elpased: 2.416
batch start
#iterations: 71
currently lose_sum: 99.00889056921005
time_elpased: 2.528
batch start
#iterations: 72
currently lose_sum: 98.87369555234909
time_elpased: 2.459
batch start
#iterations: 73
currently lose_sum: 98.82897806167603
time_elpased: 2.33
batch start
#iterations: 74
currently lose_sum: 98.75341081619263
time_elpased: 2.345
batch start
#iterations: 75
currently lose_sum: 98.7110065817833
time_elpased: 2.496
batch start
#iterations: 76
currently lose_sum: 98.55897116661072
time_elpased: 2.38
batch start
#iterations: 77
currently lose_sum: 98.81522691249847
time_elpased: 2.199
batch start
#iterations: 78
currently lose_sum: 98.69371086359024
time_elpased: 2.384
batch start
#iterations: 79
currently lose_sum: 98.80504995584488
time_elpased: 2.415
start validation test
0.598556701031
0.606657082826
0.56457754451
0.584861407249
0.598616356681
65.093
batch start
#iterations: 80
currently lose_sum: 98.56162643432617
time_elpased: 2.382
batch start
#iterations: 81
currently lose_sum: 98.59574717283249
time_elpased: 2.456
batch start
#iterations: 82
currently lose_sum: 98.66291654109955
time_elpased: 2.314
batch start
#iterations: 83
currently lose_sum: 98.62823259830475
time_elpased: 2.302
batch start
#iterations: 84
currently lose_sum: 98.70197468996048
time_elpased: 2.531
batch start
#iterations: 85
currently lose_sum: 98.7908251285553
time_elpased: 2.347
batch start
#iterations: 86
currently lose_sum: 98.8611267209053
time_elpased: 2.528
batch start
#iterations: 87
currently lose_sum: 98.69987601041794
time_elpased: 2.421
batch start
#iterations: 88
currently lose_sum: 98.49467569589615
time_elpased: 2.403
batch start
#iterations: 89
currently lose_sum: 98.44245135784149
time_elpased: 2.419
batch start
#iterations: 90
currently lose_sum: 98.96103703975677
time_elpased: 2.44
batch start
#iterations: 91
currently lose_sum: 98.60473245382309
time_elpased: 2.331
batch start
#iterations: 92
currently lose_sum: 98.41944146156311
time_elpased: 2.368
batch start
#iterations: 93
currently lose_sum: 98.55002409219742
time_elpased: 2.378
batch start
#iterations: 94
currently lose_sum: 98.57501405477524
time_elpased: 2.404
batch start
#iterations: 95
currently lose_sum: 98.6363126039505
time_elpased: 2.361
batch start
#iterations: 96
currently lose_sum: 98.45855045318604
time_elpased: 2.344
batch start
#iterations: 97
currently lose_sum: 98.68855404853821
time_elpased: 2.183
batch start
#iterations: 98
currently lose_sum: 98.72159445285797
time_elpased: 2.354
batch start
#iterations: 99
currently lose_sum: 98.57233864068985
time_elpased: 2.572
start validation test
0.597268041237
0.620080726539
0.505917464238
0.557211674695
0.597428421265
64.929
batch start
#iterations: 100
currently lose_sum: 98.40821641683578
time_elpased: 2.282
batch start
#iterations: 101
currently lose_sum: 98.39259046316147
time_elpased: 2.419
batch start
#iterations: 102
currently lose_sum: 98.56515485048294
time_elpased: 2.307
batch start
#iterations: 103
currently lose_sum: 98.74851006269455
time_elpased: 2.444
batch start
#iterations: 104
currently lose_sum: 98.78135734796524
time_elpased: 2.386
batch start
#iterations: 105
currently lose_sum: 98.37545490264893
time_elpased: 2.498
batch start
#iterations: 106
currently lose_sum: 98.38855618238449
time_elpased: 2.325
batch start
#iterations: 107
currently lose_sum: 98.65462893247604
time_elpased: 2.265
batch start
#iterations: 108
currently lose_sum: 98.70014137029648
time_elpased: 2.498
batch start
#iterations: 109
currently lose_sum: 98.48457485437393
time_elpased: 2.495
batch start
#iterations: 110
currently lose_sum: 98.46998977661133
time_elpased: 2.358
batch start
#iterations: 111
currently lose_sum: 98.47633993625641
time_elpased: 2.371
batch start
#iterations: 112
currently lose_sum: 98.43764400482178
time_elpased: 2.561
batch start
#iterations: 113
currently lose_sum: 98.52769482135773
time_elpased: 2.349
batch start
#iterations: 114
currently lose_sum: 98.44681906700134
time_elpased: 2.511
batch start
#iterations: 115
currently lose_sum: 98.35522294044495
time_elpased: 2.502
batch start
#iterations: 116
currently lose_sum: 98.51723688840866
time_elpased: 2.451
batch start
#iterations: 117
currently lose_sum: 98.52614200115204
time_elpased: 2.488
batch start
#iterations: 118
currently lose_sum: 98.51944589614868
time_elpased: 2.309
batch start
#iterations: 119
currently lose_sum: 98.55180394649506
time_elpased: 2.434
start validation test
0.600773195876
0.641018306636
0.461253473294
0.536477347537
0.601018144269
64.696
batch start
#iterations: 120
currently lose_sum: 98.51427173614502
time_elpased: 2.405
batch start
#iterations: 121
currently lose_sum: 98.46741759777069
time_elpased: 2.289
batch start
#iterations: 122
currently lose_sum: 98.51084262132645
time_elpased: 2.37
batch start
#iterations: 123
currently lose_sum: 98.46115493774414
time_elpased: 2.332
batch start
#iterations: 124
currently lose_sum: 98.18952298164368
time_elpased: 2.412
batch start
#iterations: 125
currently lose_sum: 98.5772197842598
time_elpased: 2.253
batch start
#iterations: 126
currently lose_sum: 98.26423627138138
time_elpased: 2.27
batch start
#iterations: 127
currently lose_sum: 98.52311545610428
time_elpased: 2.276
batch start
#iterations: 128
currently lose_sum: 98.52913129329681
time_elpased: 2.362
batch start
#iterations: 129
currently lose_sum: 98.21035778522491
time_elpased: 2.313
batch start
#iterations: 130
currently lose_sum: 98.40047180652618
time_elpased: 2.421
batch start
#iterations: 131
currently lose_sum: 98.25088101625443
time_elpased: 2.239
batch start
#iterations: 132
currently lose_sum: 98.35374754667282
time_elpased: 2.322
batch start
#iterations: 133
currently lose_sum: 98.29722863435745
time_elpased: 2.248
batch start
#iterations: 134
currently lose_sum: 98.29057198762894
time_elpased: 2.377
batch start
#iterations: 135
currently lose_sum: 98.2125358581543
time_elpased: 2.428
batch start
#iterations: 136
currently lose_sum: 98.12874609231949
time_elpased: 2.476
batch start
#iterations: 137
currently lose_sum: 98.23904114961624
time_elpased: 2.471
batch start
#iterations: 138
currently lose_sum: 98.2310506105423
time_elpased: 2.314
batch start
#iterations: 139
currently lose_sum: 98.441410779953
time_elpased: 2.54
start validation test
0.600154639175
0.629355860612
0.490686425852
0.551436997629
0.600346827508
64.780
batch start
#iterations: 140
currently lose_sum: 98.33237886428833
time_elpased: 2.289
batch start
#iterations: 141
currently lose_sum: 98.29557859897614
time_elpased: 2.319
batch start
#iterations: 142
currently lose_sum: 98.4148251414299
time_elpased: 2.418
batch start
#iterations: 143
currently lose_sum: 98.33846110105515
time_elpased: 2.497
batch start
#iterations: 144
currently lose_sum: 98.27807307243347
time_elpased: 2.486
batch start
#iterations: 145
currently lose_sum: 98.28447163105011
time_elpased: 2.343
batch start
#iterations: 146
currently lose_sum: 98.16220408678055
time_elpased: 2.363
batch start
#iterations: 147
currently lose_sum: 98.10782486200333
time_elpased: 2.283
batch start
#iterations: 148
currently lose_sum: 98.3376921415329
time_elpased: 2.35
batch start
#iterations: 149
currently lose_sum: 98.08313369750977
time_elpased: 2.388
batch start
#iterations: 150
currently lose_sum: 97.99510699510574
time_elpased: 2.377
batch start
#iterations: 151
currently lose_sum: 98.3000939488411
time_elpased: 2.333
batch start
#iterations: 152
currently lose_sum: 98.26460283994675
time_elpased: 2.341
batch start
#iterations: 153
currently lose_sum: 98.43030816316605
time_elpased: 2.389
batch start
#iterations: 154
currently lose_sum: 98.358254134655
time_elpased: 2.272
batch start
#iterations: 155
currently lose_sum: 98.12040877342224
time_elpased: 2.374
batch start
#iterations: 156
currently lose_sum: 98.1147358417511
time_elpased: 2.475
batch start
#iterations: 157
currently lose_sum: 98.12662595510483
time_elpased: 2.601
batch start
#iterations: 158
currently lose_sum: 98.36244243383408
time_elpased: 2.299
batch start
#iterations: 159
currently lose_sum: 98.52843165397644
time_elpased: 2.429
start validation test
0.59824742268
0.635556182151
0.463929196254
0.536347412255
0.598483239044
64.923
batch start
#iterations: 160
currently lose_sum: 98.33766740560532
time_elpased: 2.413
batch start
#iterations: 161
currently lose_sum: 98.16495776176453
time_elpased: 2.355
batch start
#iterations: 162
currently lose_sum: 97.90958470106125
time_elpased: 2.427
batch start
#iterations: 163
currently lose_sum: 98.24431955814362
time_elpased: 2.368
batch start
#iterations: 164
currently lose_sum: 98.13663107156754
time_elpased: 2.376
batch start
#iterations: 165
currently lose_sum: 98.11402636766434
time_elpased: 2.331
batch start
#iterations: 166
currently lose_sum: 97.95518505573273
time_elpased: 2.509
batch start
#iterations: 167
currently lose_sum: 98.54788035154343
time_elpased: 2.507
batch start
#iterations: 168
currently lose_sum: 97.96400761604309
time_elpased: 2.49
batch start
#iterations: 169
currently lose_sum: 97.96598851680756
time_elpased: 2.291
batch start
#iterations: 170
currently lose_sum: 98.13511914014816
time_elpased: 2.342
batch start
#iterations: 171
currently lose_sum: 98.07744264602661
time_elpased: 2.358
batch start
#iterations: 172
currently lose_sum: 98.1033484339714
time_elpased: 2.366
batch start
#iterations: 173
currently lose_sum: 98.01967877149582
time_elpased: 2.48
batch start
#iterations: 174
currently lose_sum: 98.07232558727264
time_elpased: 2.147
batch start
#iterations: 175
currently lose_sum: 97.8055357336998
time_elpased: 2.354
batch start
#iterations: 176
currently lose_sum: 97.94874560832977
time_elpased: 2.39
batch start
#iterations: 177
currently lose_sum: 97.76489561796188
time_elpased: 2.406
batch start
#iterations: 178
currently lose_sum: 97.80142241716385
time_elpased: 2.324
batch start
#iterations: 179
currently lose_sum: 98.17980426549911
time_elpased: 2.333
start validation test
0.601443298969
0.626513702996
0.505814551816
0.5597312379
0.601611189984
64.752
batch start
#iterations: 180
currently lose_sum: 97.84372228384018
time_elpased: 2.465
batch start
#iterations: 181
currently lose_sum: 98.1840090751648
time_elpased: 2.405
batch start
#iterations: 182
currently lose_sum: 98.0407532453537
time_elpased: 2.432
batch start
#iterations: 183
currently lose_sum: 98.14179986715317
time_elpased: 2.461
batch start
#iterations: 184
currently lose_sum: 97.95277363061905
time_elpased: 2.44
batch start
#iterations: 185
currently lose_sum: 98.0609667301178
time_elpased: 2.498
batch start
#iterations: 186
currently lose_sum: 98.05879360437393
time_elpased: 2.593
batch start
#iterations: 187
currently lose_sum: 97.75415700674057
time_elpased: 2.431
batch start
#iterations: 188
currently lose_sum: 98.0240433216095
time_elpased: 2.508
batch start
#iterations: 189
currently lose_sum: 97.75980627536774
time_elpased: 2.521
batch start
#iterations: 190
currently lose_sum: 98.20961534976959
time_elpased: 2.542
batch start
#iterations: 191
currently lose_sum: 98.04812216758728
time_elpased: 2.305
batch start
#iterations: 192
currently lose_sum: 97.90602213144302
time_elpased: 2.249
batch start
#iterations: 193
currently lose_sum: 98.18673777580261
time_elpased: 2.322
batch start
#iterations: 194
currently lose_sum: 98.0222447514534
time_elpased: 2.354
batch start
#iterations: 195
currently lose_sum: 97.9143870472908
time_elpased: 2.361
batch start
#iterations: 196
currently lose_sum: 98.20796978473663
time_elpased: 2.455
batch start
#iterations: 197
currently lose_sum: 97.68797713518143
time_elpased: 2.389
batch start
#iterations: 198
currently lose_sum: 97.71634024381638
time_elpased: 2.242
batch start
#iterations: 199
currently lose_sum: 97.78398180007935
time_elpased: 2.419
start validation test
0.59
0.618211076841
0.47442626325
0.536858041225
0.590202907521
65.213
batch start
#iterations: 200
currently lose_sum: 97.63912963867188
time_elpased: 2.363
batch start
#iterations: 201
currently lose_sum: 97.87269026041031
time_elpased: 2.446
batch start
#iterations: 202
currently lose_sum: 97.91286945343018
time_elpased: 2.533
batch start
#iterations: 203
currently lose_sum: 97.75397378206253
time_elpased: 2.451
batch start
#iterations: 204
currently lose_sum: 97.70594835281372
time_elpased: 2.5
batch start
#iterations: 205
currently lose_sum: 97.65485680103302
time_elpased: 2.586
batch start
#iterations: 206
currently lose_sum: 97.74964654445648
time_elpased: 2.533
batch start
#iterations: 207
currently lose_sum: 97.86299967765808
time_elpased: 2.351
batch start
#iterations: 208
currently lose_sum: 97.76715975999832
time_elpased: 2.439
batch start
#iterations: 209
currently lose_sum: 97.46433758735657
time_elpased: 2.378
batch start
#iterations: 210
currently lose_sum: 97.66970145702362
time_elpased: 2.413
batch start
#iterations: 211
currently lose_sum: 97.56965214014053
time_elpased: 2.203
batch start
#iterations: 212
currently lose_sum: 97.53990942239761
time_elpased: 2.28
batch start
#iterations: 213
currently lose_sum: 97.96110773086548
time_elpased: 2.488
batch start
#iterations: 214
currently lose_sum: 97.90840220451355
time_elpased: 2.494
batch start
#iterations: 215
currently lose_sum: 97.81605392694473
time_elpased: 2.266
batch start
#iterations: 216
currently lose_sum: 97.55459594726562
time_elpased: 2.282
batch start
#iterations: 217
currently lose_sum: 97.83350777626038
time_elpased: 2.334
batch start
#iterations: 218
currently lose_sum: 97.65889739990234
time_elpased: 2.384
batch start
#iterations: 219
currently lose_sum: 97.83050441741943
time_elpased: 2.387
start validation test
0.59881443299
0.631029810298
0.479263147062
0.544773936948
0.599024323712
64.885
batch start
#iterations: 220
currently lose_sum: 97.5966175198555
time_elpased: 2.401
batch start
#iterations: 221
currently lose_sum: 97.27802908420563
time_elpased: 2.428
batch start
#iterations: 222
currently lose_sum: 97.5874752998352
time_elpased: 2.341
batch start
#iterations: 223
currently lose_sum: 97.35471725463867
time_elpased: 1.984
batch start
#iterations: 224
currently lose_sum: 97.56897675991058
time_elpased: 2.129
batch start
#iterations: 225
currently lose_sum: 97.56703346967697
time_elpased: 1.963
batch start
#iterations: 226
currently lose_sum: 97.50682419538498
time_elpased: 1.855
batch start
#iterations: 227
currently lose_sum: 97.69337540864944
time_elpased: 2.149
batch start
#iterations: 228
currently lose_sum: 97.43787628412247
time_elpased: 1.836
batch start
#iterations: 229
currently lose_sum: 97.66409230232239
time_elpased: 1.876
batch start
#iterations: 230
currently lose_sum: 97.91745609045029
time_elpased: 2.173
batch start
#iterations: 231
currently lose_sum: 97.70191341638565
time_elpased: 1.985
batch start
#iterations: 232
currently lose_sum: 97.66068959236145
time_elpased: 2.088
batch start
#iterations: 233
currently lose_sum: 97.77847093343735
time_elpased: 2.066
batch start
#iterations: 234
currently lose_sum: 97.44176578521729
time_elpased: 1.928
batch start
#iterations: 235
currently lose_sum: 97.80900716781616
time_elpased: 1.824
batch start
#iterations: 236
currently lose_sum: 97.5451927781105
time_elpased: 1.815
batch start
#iterations: 237
currently lose_sum: 97.5698561668396
time_elpased: 2.159
batch start
#iterations: 238
currently lose_sum: 97.4417495727539
time_elpased: 2.296
batch start
#iterations: 239
currently lose_sum: 97.63164204359055
time_elpased: 1.769
start validation test
0.595103092784
0.626460201032
0.474632088093
0.540078458926
0.595314598214
65.125
batch start
#iterations: 240
currently lose_sum: 97.17549920082092
time_elpased: 1.876
batch start
#iterations: 241
currently lose_sum: 97.72668188810349
time_elpased: 2.171
batch start
#iterations: 242
currently lose_sum: 97.3734141588211
time_elpased: 2.026
batch start
#iterations: 243
currently lose_sum: 97.37722307443619
time_elpased: 1.995
batch start
#iterations: 244
currently lose_sum: 97.3618146777153
time_elpased: 1.957
batch start
#iterations: 245
currently lose_sum: 97.65232253074646
time_elpased: 1.921
batch start
#iterations: 246
currently lose_sum: 97.57194125652313
time_elpased: 1.985
batch start
#iterations: 247
currently lose_sum: 97.538056910038
time_elpased: 1.969
batch start
#iterations: 248
currently lose_sum: 97.47406762838364
time_elpased: 2.076
batch start
#iterations: 249
currently lose_sum: 97.10915446281433
time_elpased: 2.004
batch start
#iterations: 250
currently lose_sum: 97.49797284603119
time_elpased: 2.197
batch start
#iterations: 251
currently lose_sum: 97.41492581367493
time_elpased: 2.029
batch start
#iterations: 252
currently lose_sum: 97.06659388542175
time_elpased: 1.782
batch start
#iterations: 253
currently lose_sum: 97.47772651910782
time_elpased: 2.053
batch start
#iterations: 254
currently lose_sum: 97.57333272695541
time_elpased: 2.029
batch start
#iterations: 255
currently lose_sum: 97.33129572868347
time_elpased: 2.023
batch start
#iterations: 256
currently lose_sum: 97.05675315856934
time_elpased: 2.039
batch start
#iterations: 257
currently lose_sum: 97.12993901968002
time_elpased: 1.964
batch start
#iterations: 258
currently lose_sum: 97.12291288375854
time_elpased: 1.859
batch start
#iterations: 259
currently lose_sum: 97.3170639872551
time_elpased: 2.033
start validation test
0.594329896907
0.619826132088
0.491612637645
0.548324150597
0.594510232899
65.079
batch start
#iterations: 260
currently lose_sum: 97.38283711671829
time_elpased: 2.106
batch start
#iterations: 261
currently lose_sum: 97.31902527809143
time_elpased: 2.137
batch start
#iterations: 262
currently lose_sum: 97.17028212547302
time_elpased: 2.082
batch start
#iterations: 263
currently lose_sum: 96.98968428373337
time_elpased: 2.176
batch start
#iterations: 264
currently lose_sum: 97.2612156867981
time_elpased: 1.735
batch start
#iterations: 265
currently lose_sum: 97.2329952120781
time_elpased: 1.855
batch start
#iterations: 266
currently lose_sum: 97.27011096477509
time_elpased: 1.997
batch start
#iterations: 267
currently lose_sum: 97.18223124742508
time_elpased: 2.072
batch start
#iterations: 268
currently lose_sum: 97.31703954935074
time_elpased: 2.081
batch start
#iterations: 269
currently lose_sum: 97.37968915700912
time_elpased: 2.29
batch start
#iterations: 270
currently lose_sum: 96.94051957130432
time_elpased: 2.324
batch start
#iterations: 271
currently lose_sum: 97.22813874483109
time_elpased: 2.361
batch start
#iterations: 272
currently lose_sum: 97.35802751779556
time_elpased: 2.293
batch start
#iterations: 273
currently lose_sum: 97.36508274078369
time_elpased: 2.293
batch start
#iterations: 274
currently lose_sum: 97.07350891828537
time_elpased: 2.482
batch start
#iterations: 275
currently lose_sum: 97.10633808374405
time_elpased: 2.472
batch start
#iterations: 276
currently lose_sum: 96.95088028907776
time_elpased: 2.577
batch start
#iterations: 277
currently lose_sum: 96.90760052204132
time_elpased: 2.4
batch start
#iterations: 278
currently lose_sum: 97.3714616894722
time_elpased: 2.389
batch start
#iterations: 279
currently lose_sum: 97.37558943033218
time_elpased: 2.296
start validation test
0.589845360825
0.617364630568
0.476381599259
0.537786813825
0.590044563959
65.378
batch start
#iterations: 280
currently lose_sum: 97.20248681306839
time_elpased: 2.351
batch start
#iterations: 281
currently lose_sum: 96.79560160636902
time_elpased: 2.454
batch start
#iterations: 282
currently lose_sum: 97.01670962572098
time_elpased: 2.437
batch start
#iterations: 283
currently lose_sum: 96.85878473520279
time_elpased: 2.422
batch start
#iterations: 284
currently lose_sum: 97.08490508794785
time_elpased: 2.423
batch start
#iterations: 285
currently lose_sum: 97.14113980531693
time_elpased: 2.441
batch start
#iterations: 286
currently lose_sum: 96.87869709730148
time_elpased: 2.106
batch start
#iterations: 287
currently lose_sum: 97.02277904748917
time_elpased: 2.156
batch start
#iterations: 288
currently lose_sum: 96.94357204437256
time_elpased: 2.255
batch start
#iterations: 289
currently lose_sum: 97.16101628541946
time_elpased: 2.343
batch start
#iterations: 290
currently lose_sum: 97.13490730524063
time_elpased: 2.411
batch start
#iterations: 291
currently lose_sum: 96.83763009309769
time_elpased: 2.244
batch start
#iterations: 292
currently lose_sum: 96.685761988163
time_elpased: 2.358
batch start
#iterations: 293
currently lose_sum: 96.96389412879944
time_elpased: 2.311
batch start
#iterations: 294
currently lose_sum: 96.68620151281357
time_elpased: 2.43
batch start
#iterations: 295
currently lose_sum: 97.21860378980637
time_elpased: 2.363
batch start
#iterations: 296
currently lose_sum: 97.08688539266586
time_elpased: 2.36
batch start
#iterations: 297
currently lose_sum: 96.86241471767426
time_elpased: 2.334
batch start
#iterations: 298
currently lose_sum: 96.77236431837082
time_elpased: 2.422
batch start
#iterations: 299
currently lose_sum: 96.87137407064438
time_elpased: 2.469
start validation test
0.585103092784
0.621645274212
0.438612740558
0.514330537621
0.585360279191
65.888
batch start
#iterations: 300
currently lose_sum: 96.44854128360748
time_elpased: 2.379
batch start
#iterations: 301
currently lose_sum: 96.90299165248871
time_elpased: 2.43
batch start
#iterations: 302
currently lose_sum: 96.79514211416245
time_elpased: 2.433
batch start
#iterations: 303
currently lose_sum: 97.24484640359879
time_elpased: 2.487
batch start
#iterations: 304
currently lose_sum: 96.67326009273529
time_elpased: 2.54
batch start
#iterations: 305
currently lose_sum: 96.95738071203232
time_elpased: 2.574
batch start
#iterations: 306
currently lose_sum: 96.64294469356537
time_elpased: 2.28
batch start
#iterations: 307
currently lose_sum: 96.8216181397438
time_elpased: 2.472
batch start
#iterations: 308
currently lose_sum: 96.87607443332672
time_elpased: 2.092
batch start
#iterations: 309
currently lose_sum: 97.09068268537521
time_elpased: 2.504
batch start
#iterations: 310
currently lose_sum: 97.03983557224274
time_elpased: 2.466
batch start
#iterations: 311
currently lose_sum: 96.46420258283615
time_elpased: 2.493
batch start
#iterations: 312
currently lose_sum: 96.77692317962646
time_elpased: 2.251
batch start
#iterations: 313
currently lose_sum: 96.67241966724396
time_elpased: 2.448
batch start
#iterations: 314
currently lose_sum: 96.87874191999435
time_elpased: 2.302
batch start
#iterations: 315
currently lose_sum: 96.54485577344894
time_elpased: 2.465
batch start
#iterations: 316
currently lose_sum: 96.91320496797562
time_elpased: 2.414
batch start
#iterations: 317
currently lose_sum: 96.66954272985458
time_elpased: 2.338
batch start
#iterations: 318
currently lose_sum: 96.88877087831497
time_elpased: 2.479
batch start
#iterations: 319
currently lose_sum: 96.78919857740402
time_elpased: 2.427
start validation test
0.59087628866
0.626600284495
0.453329216836
0.526064369738
0.59111777376
65.439
batch start
#iterations: 320
currently lose_sum: 96.60202497243881
time_elpased: 2.441
batch start
#iterations: 321
currently lose_sum: 96.72697341442108
time_elpased: 2.401
batch start
#iterations: 322
currently lose_sum: 97.10278314352036
time_elpased: 2.439
batch start
#iterations: 323
currently lose_sum: 96.52319890260696
time_elpased: 2.337
batch start
#iterations: 324
currently lose_sum: 97.0908054113388
time_elpased: 2.435
batch start
#iterations: 325
currently lose_sum: 96.95831263065338
time_elpased: 2.393
batch start
#iterations: 326
currently lose_sum: 96.54636538028717
time_elpased: 2.303
batch start
#iterations: 327
currently lose_sum: 96.64171928167343
time_elpased: 2.342
batch start
#iterations: 328
currently lose_sum: 96.72830253839493
time_elpased: 2.293
batch start
#iterations: 329
currently lose_sum: 96.62184703350067
time_elpased: 2.351
batch start
#iterations: 330
currently lose_sum: 96.80690783262253
time_elpased: 2.403
batch start
#iterations: 331
currently lose_sum: 96.39694446325302
time_elpased: 2.449
batch start
#iterations: 332
currently lose_sum: 96.16409522294998
time_elpased: 2.449
batch start
#iterations: 333
currently lose_sum: 96.4322720170021
time_elpased: 2.466
batch start
#iterations: 334
currently lose_sum: 96.4367755651474
time_elpased: 2.501
batch start
#iterations: 335
currently lose_sum: 96.79061532020569
time_elpased: 2.047
batch start
#iterations: 336
currently lose_sum: 96.49328947067261
time_elpased: 2.142
batch start
#iterations: 337
currently lose_sum: 96.47507256269455
time_elpased: 2.324
batch start
#iterations: 338
currently lose_sum: 96.50580298900604
time_elpased: 2.267
batch start
#iterations: 339
currently lose_sum: 96.6019259095192
time_elpased: 2.289
start validation test
0.591391752577
0.620815334773
0.473294226613
0.537109489051
0.591599090999
65.243
batch start
#iterations: 340
currently lose_sum: 96.54304850101471
time_elpased: 2.452
batch start
#iterations: 341
currently lose_sum: 96.9590762257576
time_elpased: 2.309
batch start
#iterations: 342
currently lose_sum: 96.53699147701263
time_elpased: 2.37
batch start
#iterations: 343
currently lose_sum: 96.31540513038635
time_elpased: 2.35
batch start
#iterations: 344
currently lose_sum: 96.40620797872543
time_elpased: 2.397
batch start
#iterations: 345
currently lose_sum: 96.32852238416672
time_elpased: 2.305
batch start
#iterations: 346
currently lose_sum: 96.608982026577
time_elpased: 2.359
batch start
#iterations: 347
currently lose_sum: 96.4958723783493
time_elpased: 2.354
batch start
#iterations: 348
currently lose_sum: 96.35624551773071
time_elpased: 2.503
batch start
#iterations: 349
currently lose_sum: 96.30566531419754
time_elpased: 2.347
batch start
#iterations: 350
currently lose_sum: 96.10323691368103
time_elpased: 2.501
batch start
#iterations: 351
currently lose_sum: 96.82466024160385
time_elpased: 2.401
batch start
#iterations: 352
currently lose_sum: 96.44331699609756
time_elpased: 2.511
batch start
#iterations: 353
currently lose_sum: 96.59385180473328
time_elpased: 2.514
batch start
#iterations: 354
currently lose_sum: 96.47749137878418
time_elpased: 2.359
batch start
#iterations: 355
currently lose_sum: 96.49191331863403
time_elpased: 2.412
batch start
#iterations: 356
currently lose_sum: 96.02696114778519
time_elpased: 2.285
batch start
#iterations: 357
currently lose_sum: 95.9099463224411
time_elpased: 2.453
batch start
#iterations: 358
currently lose_sum: 96.31340342760086
time_elpased: 2.441
batch start
#iterations: 359
currently lose_sum: 96.48933690786362
time_elpased: 2.373
start validation test
0.589432989691
0.623763775078
0.454358341052
0.525751711819
0.58967013407
65.717
batch start
#iterations: 360
currently lose_sum: 96.54262453317642
time_elpased: 2.424
batch start
#iterations: 361
currently lose_sum: 96.72545766830444
time_elpased: 2.458
batch start
#iterations: 362
currently lose_sum: 96.15815353393555
time_elpased: 2.66
batch start
#iterations: 363
currently lose_sum: 95.93197667598724
time_elpased: 2.429
batch start
#iterations: 364
currently lose_sum: 96.4693922996521
time_elpased: 2.334
batch start
#iterations: 365
currently lose_sum: 96.40281218290329
time_elpased: 2.413
batch start
#iterations: 366
currently lose_sum: 96.15452522039413
time_elpased: 2.406
batch start
#iterations: 367
currently lose_sum: 95.94316124916077
time_elpased: 2.522
batch start
#iterations: 368
currently lose_sum: 96.21028804779053
time_elpased: 2.362
batch start
#iterations: 369
currently lose_sum: 95.95437198877335
time_elpased: 2.406
batch start
#iterations: 370
currently lose_sum: 96.08338057994843
time_elpased: 2.423
batch start
#iterations: 371
currently lose_sum: 96.05194759368896
time_elpased: 2.442
batch start
#iterations: 372
currently lose_sum: 96.32152277231216
time_elpased: 2.417
batch start
#iterations: 373
currently lose_sum: 96.14394867420197
time_elpased: 2.587
batch start
#iterations: 374
currently lose_sum: 96.01314514875412
time_elpased: 2.452
batch start
#iterations: 375
currently lose_sum: 96.25800770521164
time_elpased: 2.437
batch start
#iterations: 376
currently lose_sum: 96.46461379528046
time_elpased: 2.428
batch start
#iterations: 377
currently lose_sum: 96.0188689827919
time_elpased: 2.433
batch start
#iterations: 378
currently lose_sum: 96.48136001825333
time_elpased: 2.467
batch start
#iterations: 379
currently lose_sum: 96.18027204275131
time_elpased: 2.53
start validation test
0.587371134021
0.615208613728
0.47041267881
0.533154487665
0.587576472628
66.006
batch start
#iterations: 380
currently lose_sum: 96.17579138278961
time_elpased: 2.387
batch start
#iterations: 381
currently lose_sum: 95.98746258020401
time_elpased: 2.416
batch start
#iterations: 382
currently lose_sum: 96.05816674232483
time_elpased: 2.367
batch start
#iterations: 383
currently lose_sum: 95.75291657447815
time_elpased: 2.396
batch start
#iterations: 384
currently lose_sum: 95.866490483284
time_elpased: 2.262
batch start
#iterations: 385
currently lose_sum: 95.8900134563446
time_elpased: 2.204
batch start
#iterations: 386
currently lose_sum: 96.06881833076477
time_elpased: 2.376
batch start
#iterations: 387
currently lose_sum: 95.77721244096756
time_elpased: 2.392
batch start
#iterations: 388
currently lose_sum: 95.91889518499374
time_elpased: 2.519
batch start
#iterations: 389
currently lose_sum: 95.972585439682
time_elpased: 2.479
batch start
#iterations: 390
currently lose_sum: 95.95385086536407
time_elpased: 2.247
batch start
#iterations: 391
currently lose_sum: 96.13076382875443
time_elpased: 2.373
batch start
#iterations: 392
currently lose_sum: 95.64505767822266
time_elpased: 2.451
batch start
#iterations: 393
currently lose_sum: 95.4088858962059
time_elpased: 2.455
batch start
#iterations: 394
currently lose_sum: 95.81295359134674
time_elpased: 2.589
batch start
#iterations: 395
currently lose_sum: 96.04472655057907
time_elpased: 2.479
batch start
#iterations: 396
currently lose_sum: 95.50248885154724
time_elpased: 2.481
batch start
#iterations: 397
currently lose_sum: 95.73623049259186
time_elpased: 2.348
batch start
#iterations: 398
currently lose_sum: 95.87691241502762
time_elpased: 2.324
batch start
#iterations: 399
currently lose_sum: 95.81388056278229
time_elpased: 2.294
start validation test
0.582989690722
0.606298183719
0.477513635896
0.534254461716
0.583174870204
66.111
acc: 0.599
pre: 0.639
rec: 0.459
F1: 0.534
auc: 0.634
