start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.3823436498642
time_elpased: 2.378
batch start
#iterations: 1
currently lose_sum: 100.34130924940109
time_elpased: 2.57
batch start
#iterations: 2
currently lose_sum: 100.31284910440445
time_elpased: 2.565
batch start
#iterations: 3
currently lose_sum: 100.3419708609581
time_elpased: 2.223
batch start
#iterations: 4
currently lose_sum: 100.19465619325638
time_elpased: 2.548
batch start
#iterations: 5
currently lose_sum: 100.18739801645279
time_elpased: 2.619
batch start
#iterations: 6
currently lose_sum: 100.082504093647
time_elpased: 2.673
batch start
#iterations: 7
currently lose_sum: 100.07031840085983
time_elpased: 2.254
batch start
#iterations: 8
currently lose_sum: 100.05864948034286
time_elpased: 2.707
batch start
#iterations: 9
currently lose_sum: 99.99936890602112
time_elpased: 2.703
batch start
#iterations: 10
currently lose_sum: 99.83728456497192
time_elpased: 2.757
batch start
#iterations: 11
currently lose_sum: 100.07850170135498
time_elpased: 2.753
batch start
#iterations: 12
currently lose_sum: 99.97655594348907
time_elpased: 2.757
batch start
#iterations: 13
currently lose_sum: 100.05893868207932
time_elpased: 2.697
batch start
#iterations: 14
currently lose_sum: 99.84925371408463
time_elpased: 2.381
batch start
#iterations: 15
currently lose_sum: 99.75984865427017
time_elpased: 2.467
batch start
#iterations: 16
currently lose_sum: 99.68753743171692
time_elpased: 2.555
batch start
#iterations: 17
currently lose_sum: 99.80964660644531
time_elpased: 2.408
batch start
#iterations: 18
currently lose_sum: 99.65462666749954
time_elpased: 2.711
batch start
#iterations: 19
currently lose_sum: 99.63864743709564
time_elpased: 2.547
start validation test
0.58881443299
0.589432565789
0.590099825049
0.589766006686
0.588812176286
65.678
batch start
#iterations: 20
currently lose_sum: 99.57620346546173
time_elpased: 2.633
batch start
#iterations: 21
currently lose_sum: 99.60675740242004
time_elpased: 2.546
batch start
#iterations: 22
currently lose_sum: 99.66434186697006
time_elpased: 2.5
batch start
#iterations: 23
currently lose_sum: 99.63082659244537
time_elpased: 2.331
batch start
#iterations: 24
currently lose_sum: 99.50168734788895
time_elpased: 2.474
batch start
#iterations: 25
currently lose_sum: 99.51706141233444
time_elpased: 2.568
batch start
#iterations: 26
currently lose_sum: 99.5995944738388
time_elpased: 2.642
batch start
#iterations: 27
currently lose_sum: 99.40724521875381
time_elpased: 2.594
batch start
#iterations: 28
currently lose_sum: 99.34114062786102
time_elpased: 2.736
batch start
#iterations: 29
currently lose_sum: 99.45552599430084
time_elpased: 2.797
batch start
#iterations: 30
currently lose_sum: 99.35612118244171
time_elpased: 2.543
batch start
#iterations: 31
currently lose_sum: 99.35304456949234
time_elpased: 2.472
batch start
#iterations: 32
currently lose_sum: 99.29835110902786
time_elpased: 2.514
batch start
#iterations: 33
currently lose_sum: 99.27341067790985
time_elpased: 2.546
batch start
#iterations: 34
currently lose_sum: 99.2981185913086
time_elpased: 2.565
batch start
#iterations: 35
currently lose_sum: 99.3470887541771
time_elpased: 2.613
batch start
#iterations: 36
currently lose_sum: 99.28867256641388
time_elpased: 2.667
batch start
#iterations: 37
currently lose_sum: 99.25920760631561
time_elpased: 2.411
batch start
#iterations: 38
currently lose_sum: 99.29998743534088
time_elpased: 2.107
batch start
#iterations: 39
currently lose_sum: 99.31448358297348
time_elpased: 2.311
start validation test
0.600103092784
0.618425825172
0.526397036122
0.568712474983
0.600232495134
65.040
batch start
#iterations: 40
currently lose_sum: 99.20410752296448
time_elpased: 2.587
batch start
#iterations: 41
currently lose_sum: 99.29828572273254
time_elpased: 2.531
batch start
#iterations: 42
currently lose_sum: 99.08393555879593
time_elpased: 2.471
batch start
#iterations: 43
currently lose_sum: 99.16552752256393
time_elpased: 2.524
batch start
#iterations: 44
currently lose_sum: 99.13639706373215
time_elpased: 2.707
batch start
#iterations: 45
currently lose_sum: 99.11571496725082
time_elpased: 2.675
batch start
#iterations: 46
currently lose_sum: 98.82336562871933
time_elpased: 2.313
batch start
#iterations: 47
currently lose_sum: 99.08394598960876
time_elpased: 2.433
batch start
#iterations: 48
currently lose_sum: 99.08724391460419
time_elpased: 2.61
batch start
#iterations: 49
currently lose_sum: 98.83616989850998
time_elpased: 2.522
batch start
#iterations: 50
currently lose_sum: 99.05886906385422
time_elpased: 2.374
batch start
#iterations: 51
currently lose_sum: 98.89844214916229
time_elpased: 2.664
batch start
#iterations: 52
currently lose_sum: 99.08873724937439
time_elpased: 2.659
batch start
#iterations: 53
currently lose_sum: 98.96872651576996
time_elpased: 2.29
batch start
#iterations: 54
currently lose_sum: 99.0971702337265
time_elpased: 2.574
batch start
#iterations: 55
currently lose_sum: 98.99707889556885
time_elpased: 2.569
batch start
#iterations: 56
currently lose_sum: 98.95564371347427
time_elpased: 2.531
batch start
#iterations: 57
currently lose_sum: 99.06086593866348
time_elpased: 2.535
batch start
#iterations: 58
currently lose_sum: 98.95742982625961
time_elpased: 2.726
batch start
#iterations: 59
currently lose_sum: 99.03840106725693
time_elpased: 2.705
start validation test
0.597835051546
0.621834839038
0.502933004014
0.556099226218
0.598001666728
65.050
batch start
#iterations: 60
currently lose_sum: 98.82813429832458
time_elpased: 2.273
batch start
#iterations: 61
currently lose_sum: 98.93759578466415
time_elpased: 2.489
batch start
#iterations: 62
currently lose_sum: 98.87820893526077
time_elpased: 2.544
batch start
#iterations: 63
currently lose_sum: 99.10966569185257
time_elpased: 2.526
batch start
#iterations: 64
currently lose_sum: 98.98235893249512
time_elpased: 2.441
batch start
#iterations: 65
currently lose_sum: 98.89040446281433
time_elpased: 2.058
batch start
#iterations: 66
currently lose_sum: 98.64068579673767
time_elpased: 2.464
batch start
#iterations: 67
currently lose_sum: 98.72297006845474
time_elpased: 2.672
batch start
#iterations: 68
currently lose_sum: 98.95015805959702
time_elpased: 2.735
batch start
#iterations: 69
currently lose_sum: 98.69647508859634
time_elpased: 2.58
batch start
#iterations: 70
currently lose_sum: 98.81326597929001
time_elpased: 2.576
batch start
#iterations: 71
currently lose_sum: 98.92847347259521
time_elpased: 2.549
batch start
#iterations: 72
currently lose_sum: 98.85673719644547
time_elpased: 2.556
batch start
#iterations: 73
currently lose_sum: 98.80699527263641
time_elpased: 2.582
batch start
#iterations: 74
currently lose_sum: 98.91360604763031
time_elpased: 2.685
batch start
#iterations: 75
currently lose_sum: 98.92713302373886
time_elpased: 2.691
batch start
#iterations: 76
currently lose_sum: 98.86779165267944
time_elpased: 2.539
batch start
#iterations: 77
currently lose_sum: 99.06611180305481
time_elpased: 2.529
batch start
#iterations: 78
currently lose_sum: 98.69203436374664
time_elpased: 2.531
batch start
#iterations: 79
currently lose_sum: 98.97726327180862
time_elpased: 2.516
start validation test
0.602422680412
0.648400473934
0.450550581455
0.531665553464
0.602689315307
64.560
batch start
#iterations: 80
currently lose_sum: 98.85549277067184
time_elpased: 2.535
batch start
#iterations: 81
currently lose_sum: 98.5681961774826
time_elpased: 2.551
batch start
#iterations: 82
currently lose_sum: 98.87909775972366
time_elpased: 2.584
batch start
#iterations: 83
currently lose_sum: 98.72489494085312
time_elpased: 2.616
batch start
#iterations: 84
currently lose_sum: 98.71356326341629
time_elpased: 2.689
batch start
#iterations: 85
currently lose_sum: 98.64676696062088
time_elpased: 2.576
batch start
#iterations: 86
currently lose_sum: 98.7355106472969
time_elpased: 2.504
batch start
#iterations: 87
currently lose_sum: 98.81276148557663
time_elpased: 2.587
batch start
#iterations: 88
currently lose_sum: 98.66875964403152
time_elpased: 2.583
batch start
#iterations: 89
currently lose_sum: 98.68455225229263
time_elpased: 2.629
batch start
#iterations: 90
currently lose_sum: 98.82870012521744
time_elpased: 2.603
batch start
#iterations: 91
currently lose_sum: 98.98507928848267
time_elpased: 2.737
batch start
#iterations: 92
currently lose_sum: 98.63182097673416
time_elpased: 2.508
batch start
#iterations: 93
currently lose_sum: 98.70869010686874
time_elpased: 2.5
batch start
#iterations: 94
currently lose_sum: 98.87656605243683
time_elpased: 2.49
batch start
#iterations: 95
currently lose_sum: 98.56568974256516
time_elpased: 2.467
batch start
#iterations: 96
currently lose_sum: 98.60900241136551
time_elpased: 2.581
batch start
#iterations: 97
currently lose_sum: 98.48327612876892
time_elpased: 2.51
batch start
#iterations: 98
currently lose_sum: 98.69712752103806
time_elpased: 2.184
batch start
#iterations: 99
currently lose_sum: 98.5119104385376
time_elpased: 2.38
start validation test
0.603865979381
0.631742738589
0.501389317691
0.559068219634
0.604045892967
64.495
batch start
#iterations: 100
currently lose_sum: 98.51117217540741
time_elpased: 2.506
batch start
#iterations: 101
currently lose_sum: 98.50556993484497
time_elpased: 2.723
batch start
#iterations: 102
currently lose_sum: 98.84997582435608
time_elpased: 2.417
batch start
#iterations: 103
currently lose_sum: 98.48989754915237
time_elpased: 2.646
batch start
#iterations: 104
currently lose_sum: 98.68165296316147
time_elpased: 2.608
batch start
#iterations: 105
currently lose_sum: 98.66258436441422
time_elpased: 2.673
batch start
#iterations: 106
currently lose_sum: 98.63449537754059
time_elpased: 2.353
batch start
#iterations: 107
currently lose_sum: 98.6451386809349
time_elpased: 2.579
batch start
#iterations: 108
currently lose_sum: 98.55544829368591
time_elpased: 2.525
batch start
#iterations: 109
currently lose_sum: 98.49338054656982
time_elpased: 2.62
batch start
#iterations: 110
currently lose_sum: 98.59548193216324
time_elpased: 2.555
batch start
#iterations: 111
currently lose_sum: 98.39813965559006
time_elpased: 2.588
batch start
#iterations: 112
currently lose_sum: 98.62684470415115
time_elpased: 2.467
batch start
#iterations: 113
currently lose_sum: 98.46550345420837
time_elpased: 2.597
batch start
#iterations: 114
currently lose_sum: 98.60168331861496
time_elpased: 2.648
batch start
#iterations: 115
currently lose_sum: 98.64417457580566
time_elpased: 2.583
batch start
#iterations: 116
currently lose_sum: 98.60010576248169
time_elpased: 2.458
batch start
#iterations: 117
currently lose_sum: 98.0680239200592
time_elpased: 2.582
batch start
#iterations: 118
currently lose_sum: 98.69800043106079
time_elpased: 2.476
batch start
#iterations: 119
currently lose_sum: 98.64395523071289
time_elpased: 2.603
start validation test
0.603092783505
0.65045502014
0.448698157868
0.531059683313
0.603363847084
64.693
batch start
#iterations: 120
currently lose_sum: 98.48836874961853
time_elpased: 2.606
batch start
#iterations: 121
currently lose_sum: 98.2356670498848
time_elpased: 2.661
batch start
#iterations: 122
currently lose_sum: 98.78567808866501
time_elpased: 2.575
batch start
#iterations: 123
currently lose_sum: 98.41279220581055
time_elpased: 2.527
batch start
#iterations: 124
currently lose_sum: 98.61901724338531
time_elpased: 2.461
batch start
#iterations: 125
currently lose_sum: 98.67109352350235
time_elpased: 2.386
batch start
#iterations: 126
currently lose_sum: 98.26383703947067
time_elpased: 2.48
batch start
#iterations: 127
currently lose_sum: 98.71604949235916
time_elpased: 2.472
batch start
#iterations: 128
currently lose_sum: 98.52004015445709
time_elpased: 2.443
batch start
#iterations: 129
currently lose_sum: 98.67339950799942
time_elpased: 2.54
batch start
#iterations: 130
currently lose_sum: 98.46380180120468
time_elpased: 2.56
batch start
#iterations: 131
currently lose_sum: 98.60691702365875
time_elpased: 2.648
batch start
#iterations: 132
currently lose_sum: 98.6444320678711
time_elpased: 2.261
batch start
#iterations: 133
currently lose_sum: 98.50104486942291
time_elpased: 2.518
batch start
#iterations: 134
currently lose_sum: 98.56980895996094
time_elpased: 2.593
batch start
#iterations: 135
currently lose_sum: 98.38969939947128
time_elpased: 2.432
batch start
#iterations: 136
currently lose_sum: 98.3380891084671
time_elpased: 2.561
batch start
#iterations: 137
currently lose_sum: 98.53637170791626
time_elpased: 2.596
batch start
#iterations: 138
currently lose_sum: 98.42124164104462
time_elpased: 2.531
batch start
#iterations: 139
currently lose_sum: 98.53572392463684
time_elpased: 2.333
start validation test
0.595463917526
0.608928779578
0.537614490069
0.571053782247
0.595565481118
64.992
batch start
#iterations: 140
currently lose_sum: 98.34656596183777
time_elpased: 2.617
batch start
#iterations: 141
currently lose_sum: 98.57139050960541
time_elpased: 2.409
batch start
#iterations: 142
currently lose_sum: 98.18745785951614
time_elpased: 2.586
batch start
#iterations: 143
currently lose_sum: 98.49289160966873
time_elpased: 2.502
batch start
#iterations: 144
currently lose_sum: 98.37526309490204
time_elpased: 2.715
batch start
#iterations: 145
currently lose_sum: 98.24032056331635
time_elpased: 2.619
batch start
#iterations: 146
currently lose_sum: 98.23210942745209
time_elpased: 2.53
batch start
#iterations: 147
currently lose_sum: 98.11772471666336
time_elpased: 2.372
batch start
#iterations: 148
currently lose_sum: 98.14161843061447
time_elpased: 2.688
batch start
#iterations: 149
currently lose_sum: 98.43150496482849
time_elpased: 2.575
batch start
#iterations: 150
currently lose_sum: 98.19648271799088
time_elpased: 2.662
batch start
#iterations: 151
currently lose_sum: 98.25819110870361
time_elpased: 2.591
batch start
#iterations: 152
currently lose_sum: 98.32700377702713
time_elpased: 2.529
batch start
#iterations: 153
currently lose_sum: 98.42824685573578
time_elpased: 2.532
batch start
#iterations: 154
currently lose_sum: 98.51402276754379
time_elpased: 2.519
batch start
#iterations: 155
currently lose_sum: 98.05908119678497
time_elpased: 2.665
batch start
#iterations: 156
currently lose_sum: 98.21890515089035
time_elpased: 2.624
batch start
#iterations: 157
currently lose_sum: 98.01682639122009
time_elpased: 2.357
batch start
#iterations: 158
currently lose_sum: 98.22629064321518
time_elpased: 2.299
batch start
#iterations: 159
currently lose_sum: 98.10859018564224
time_elpased: 2.526
start validation test
0.606546391753
0.626333656644
0.53164556962
0.575118285555
0.606677891698
64.511
batch start
#iterations: 160
currently lose_sum: 98.44142812490463
time_elpased: 2.516
batch start
#iterations: 161
currently lose_sum: 98.23626744747162
time_elpased: 2.488
batch start
#iterations: 162
currently lose_sum: 98.24313628673553
time_elpased: 2.4
batch start
#iterations: 163
currently lose_sum: 98.12459725141525
time_elpased: 2.617
batch start
#iterations: 164
currently lose_sum: 97.85605716705322
time_elpased: 2.679
batch start
#iterations: 165
currently lose_sum: 98.04280096292496
time_elpased: 2.516
batch start
#iterations: 166
currently lose_sum: 97.8791953921318
time_elpased: 2.573
batch start
#iterations: 167
currently lose_sum: 98.29396069049835
time_elpased: 2.594
batch start
#iterations: 168
currently lose_sum: 97.94873642921448
time_elpased: 2.53
batch start
#iterations: 169
currently lose_sum: 98.13910555839539
time_elpased: 2.35
batch start
#iterations: 170
currently lose_sum: 98.05179953575134
time_elpased: 2.624
batch start
#iterations: 171
currently lose_sum: 98.28363960981369
time_elpased: 2.665
batch start
#iterations: 172
currently lose_sum: 98.23038130998611
time_elpased: 2.654
batch start
#iterations: 173
currently lose_sum: 98.10333251953125
time_elpased: 2.627
batch start
#iterations: 174
currently lose_sum: 98.04712384939194
time_elpased: 2.567
batch start
#iterations: 175
currently lose_sum: 97.78422963619232
time_elpased: 2.632
batch start
#iterations: 176
currently lose_sum: 97.8569318652153
time_elpased: 2.44
batch start
#iterations: 177
currently lose_sum: 97.96958649158478
time_elpased: 2.656
batch start
#iterations: 178
currently lose_sum: 98.20657455921173
time_elpased: 2.578
batch start
#iterations: 179
currently lose_sum: 97.76576465368271
time_elpased: 2.658
start validation test
0.596288659794
0.615828929581
0.515694144283
0.56133079422
0.596430155897
64.844
batch start
#iterations: 180
currently lose_sum: 97.77554935216904
time_elpased: 2.552
batch start
#iterations: 181
currently lose_sum: 98.10786253213882
time_elpased: 2.289
batch start
#iterations: 182
currently lose_sum: 98.0932999253273
time_elpased: 2.571
batch start
#iterations: 183
currently lose_sum: 97.86429101228714
time_elpased: 2.606
batch start
#iterations: 184
currently lose_sum: 97.96763736009598
time_elpased: 2.634
batch start
#iterations: 185
currently lose_sum: 98.16777628660202
time_elpased: 2.52
batch start
#iterations: 186
currently lose_sum: 97.89715903997421
time_elpased: 2.468
batch start
#iterations: 187
currently lose_sum: 97.84686875343323
time_elpased: 2.547
batch start
#iterations: 188
currently lose_sum: 97.99261629581451
time_elpased: 2.544
batch start
#iterations: 189
currently lose_sum: 97.94716161489487
time_elpased: 2.582
batch start
#iterations: 190
currently lose_sum: 97.89784550666809
time_elpased: 2.619
batch start
#iterations: 191
currently lose_sum: 97.6699389219284
time_elpased: 2.631
batch start
#iterations: 192
currently lose_sum: 97.77002382278442
time_elpased: 2.511
batch start
#iterations: 193
currently lose_sum: 98.19331777095795
time_elpased: 2.57
batch start
#iterations: 194
currently lose_sum: 97.73565965890884
time_elpased: 2.616
batch start
#iterations: 195
currently lose_sum: 97.87655049562454
time_elpased: 2.521
batch start
#iterations: 196
currently lose_sum: 97.77579993009567
time_elpased: 2.663
batch start
#iterations: 197
currently lose_sum: 97.81135761737823
time_elpased: 2.578
batch start
#iterations: 198
currently lose_sum: 97.9302231669426
time_elpased: 2.356
batch start
#iterations: 199
currently lose_sum: 97.73580235242844
time_elpased: 2.549
start validation test
0.60118556701
0.626923076923
0.503241741278
0.558314779928
0.601357522503
64.805
batch start
#iterations: 200
currently lose_sum: 97.80451202392578
time_elpased: 2.378
batch start
#iterations: 201
currently lose_sum: 97.77854669094086
time_elpased: 2.142
batch start
#iterations: 202
currently lose_sum: 97.68274176120758
time_elpased: 2.629
batch start
#iterations: 203
currently lose_sum: 97.72107923030853
time_elpased: 2.614
batch start
#iterations: 204
currently lose_sum: 97.54211407899857
time_elpased: 2.596
batch start
#iterations: 205
currently lose_sum: 97.79603964090347
time_elpased: 2.681
batch start
#iterations: 206
currently lose_sum: 97.58578139543533
time_elpased: 2.677
batch start
#iterations: 207
currently lose_sum: 97.74545133113861
time_elpased: 2.674
batch start
#iterations: 208
currently lose_sum: 97.90338724851608
time_elpased: 2.528
batch start
#iterations: 209
currently lose_sum: 97.71089535951614
time_elpased: 2.091
batch start
#iterations: 210
currently lose_sum: 97.56198287010193
time_elpased: 2.351
batch start
#iterations: 211
currently lose_sum: 97.60362613201141
time_elpased: 2.537
batch start
#iterations: 212
currently lose_sum: 97.65151059627533
time_elpased: 2.448
batch start
#iterations: 213
currently lose_sum: 97.39267092943192
time_elpased: 2.603
batch start
#iterations: 214
currently lose_sum: 97.75605773925781
time_elpased: 2.363
batch start
#iterations: 215
currently lose_sum: 97.94847685098648
time_elpased: 2.542
batch start
#iterations: 216
currently lose_sum: 97.475676715374
time_elpased: 2.817
batch start
#iterations: 217
currently lose_sum: 97.68263417482376
time_elpased: 2.491
batch start
#iterations: 218
currently lose_sum: 97.1758239865303
time_elpased: 2.589
batch start
#iterations: 219
currently lose_sum: 97.32737845182419
time_elpased: 2.593
start validation test
0.599896907216
0.635369062457
0.472162189976
0.541740465226
0.600121165214
64.767
batch start
#iterations: 220
currently lose_sum: 97.64965003728867
time_elpased: 2.573
batch start
#iterations: 221
currently lose_sum: 97.3424740433693
time_elpased: 2.439
batch start
#iterations: 222
currently lose_sum: 97.53203588724136
time_elpased: 2.699
batch start
#iterations: 223
currently lose_sum: 97.60996294021606
time_elpased: 2.535
batch start
#iterations: 224
currently lose_sum: 97.61488199234009
time_elpased: 2.807
batch start
#iterations: 225
currently lose_sum: 97.39338201284409
time_elpased: 2.519
batch start
#iterations: 226
currently lose_sum: 97.74411118030548
time_elpased: 2.564
batch start
#iterations: 227
currently lose_sum: 97.26830267906189
time_elpased: 2.322
batch start
#iterations: 228
currently lose_sum: 97.69377356767654
time_elpased: 2.494
batch start
#iterations: 229
currently lose_sum: 97.45869612693787
time_elpased: 2.614
batch start
#iterations: 230
currently lose_sum: 97.20297998189926
time_elpased: 2.404
batch start
#iterations: 231
currently lose_sum: 97.49708831310272
time_elpased: 2.465
batch start
#iterations: 232
currently lose_sum: 97.11302381753922
time_elpased: 2.342
batch start
#iterations: 233
currently lose_sum: 97.4884050488472
time_elpased: 2.608
batch start
#iterations: 234
currently lose_sum: 97.03453570604324
time_elpased: 2.555
batch start
#iterations: 235
currently lose_sum: 97.52943587303162
time_elpased: 2.665
batch start
#iterations: 236
currently lose_sum: 97.3591742515564
time_elpased: 2.518
batch start
#iterations: 237
currently lose_sum: 97.38248574733734
time_elpased: 2.689
batch start
#iterations: 238
currently lose_sum: 97.24425971508026
time_elpased: 2.368
batch start
#iterations: 239
currently lose_sum: 97.49320977926254
time_elpased: 2.643
start validation test
0.596288659794
0.636297903109
0.452814654729
0.529100529101
0.596540550539
64.892
batch start
#iterations: 240
currently lose_sum: 97.19851356744766
time_elpased: 2.625
batch start
#iterations: 241
currently lose_sum: 97.32655417919159
time_elpased: 2.482
batch start
#iterations: 242
currently lose_sum: 97.31493616104126
time_elpased: 2.616
batch start
#iterations: 243
currently lose_sum: 97.30566900968552
time_elpased: 2.412
batch start
#iterations: 244
currently lose_sum: 97.51183927059174
time_elpased: 2.673
batch start
#iterations: 245
currently lose_sum: 97.05337506532669
time_elpased: 2.583
batch start
#iterations: 246
currently lose_sum: 97.28358113765717
time_elpased: 2.517
batch start
#iterations: 247
currently lose_sum: 97.04078048467636
time_elpased: 2.553
batch start
#iterations: 248
currently lose_sum: 97.12830340862274
time_elpased: 2.609
batch start
#iterations: 249
currently lose_sum: 97.01543635129929
time_elpased: 2.564
batch start
#iterations: 250
currently lose_sum: 97.12826526165009
time_elpased: 2.512
batch start
#iterations: 251
currently lose_sum: 97.01248532533646
time_elpased: 2.632
batch start
#iterations: 252
currently lose_sum: 97.17378318309784
time_elpased: 2.545
batch start
#iterations: 253
currently lose_sum: 97.11838835477829
time_elpased: 2.591
batch start
#iterations: 254
currently lose_sum: 96.81858050823212
time_elpased: 2.594
batch start
#iterations: 255
currently lose_sum: 97.04530620574951
time_elpased: 2.609
batch start
#iterations: 256
currently lose_sum: 97.25592428445816
time_elpased: 2.567
batch start
#iterations: 257
currently lose_sum: 96.97714966535568
time_elpased: 2.323
batch start
#iterations: 258
currently lose_sum: 96.91867792606354
time_elpased: 2.601
batch start
#iterations: 259
currently lose_sum: 97.07373529672623
time_elpased: 2.45
start validation test
0.593659793814
0.629666289593
0.45826901307
0.530466376794
0.593897493213
65.082
batch start
#iterations: 260
currently lose_sum: 97.09880644083023
time_elpased: 2.583
batch start
#iterations: 261
currently lose_sum: 96.64228135347366
time_elpased: 2.589
batch start
#iterations: 262
currently lose_sum: 96.89998346567154
time_elpased: 2.439
batch start
#iterations: 263
currently lose_sum: 97.13291984796524
time_elpased: 2.612
batch start
#iterations: 264
currently lose_sum: 96.87383502721786
time_elpased: 2.399
batch start
#iterations: 265
currently lose_sum: 96.81034994125366
time_elpased: 2.616
batch start
#iterations: 266
currently lose_sum: 96.81019705533981
time_elpased: 2.521
batch start
#iterations: 267
currently lose_sum: 97.04406011104584
time_elpased: 2.661
batch start
#iterations: 268
currently lose_sum: 96.93573325872421
time_elpased: 2.786
batch start
#iterations: 269
currently lose_sum: 97.19067192077637
time_elpased: 2.65
batch start
#iterations: 270
currently lose_sum: 96.96945226192474
time_elpased: 2.645
batch start
#iterations: 271
currently lose_sum: 96.82629752159119
time_elpased: 2.762
batch start
#iterations: 272
currently lose_sum: 96.82255786657333
time_elpased: 2.682
batch start
#iterations: 273
currently lose_sum: 96.56212812662125
time_elpased: 2.7
batch start
#iterations: 274
currently lose_sum: 97.1684137582779
time_elpased: 2.613
batch start
#iterations: 275
currently lose_sum: 97.0146513581276
time_elpased: 2.634
batch start
#iterations: 276
currently lose_sum: 96.71302080154419
time_elpased: 2.614
batch start
#iterations: 277
currently lose_sum: 96.74092829227448
time_elpased: 2.573
batch start
#iterations: 278
currently lose_sum: 96.65919142961502
time_elpased: 2.627
batch start
#iterations: 279
currently lose_sum: 97.12805169820786
time_elpased: 2.393
start validation test
0.595103092784
0.616871704745
0.505711639395
0.555788045015
0.595260033268
65.264
batch start
#iterations: 280
currently lose_sum: 96.76892101764679
time_elpased: 2.697
batch start
#iterations: 281
currently lose_sum: 96.38454109430313
time_elpased: 2.393
batch start
#iterations: 282
currently lose_sum: 96.89983516931534
time_elpased: 2.46
batch start
#iterations: 283
currently lose_sum: 96.60965794324875
time_elpased: 2.444
batch start
#iterations: 284
currently lose_sum: 96.44979703426361
time_elpased: 2.61
batch start
#iterations: 285
currently lose_sum: 96.74988830089569
time_elpased: 2.616
batch start
#iterations: 286
currently lose_sum: 96.7110013961792
time_elpased: 2.522
batch start
#iterations: 287
currently lose_sum: 96.80339652299881
time_elpased: 2.608
batch start
#iterations: 288
currently lose_sum: 96.60151726007462
time_elpased: 2.574
batch start
#iterations: 289
currently lose_sum: 96.7194322347641
time_elpased: 2.606
batch start
#iterations: 290
currently lose_sum: 96.49463623762131
time_elpased: 2.477
batch start
#iterations: 291
currently lose_sum: 96.92499130964279
time_elpased: 2.456
batch start
#iterations: 292
currently lose_sum: 96.21035796403885
time_elpased: 2.465
batch start
#iterations: 293
currently lose_sum: 96.62282538414001
time_elpased: 2.449
batch start
#iterations: 294
currently lose_sum: 96.8771630525589
time_elpased: 2.253
batch start
#iterations: 295
currently lose_sum: 96.50658750534058
time_elpased: 2.566
batch start
#iterations: 296
currently lose_sum: 96.29196614027023
time_elpased: 2.589
batch start
#iterations: 297
currently lose_sum: 96.5645341873169
time_elpased: 2.433
batch start
#iterations: 298
currently lose_sum: 96.71251940727234
time_elpased: 2.537
batch start
#iterations: 299
currently lose_sum: 96.36742275953293
time_elpased: 2.544
start validation test
0.587474226804
0.612822538178
0.479057322219
0.537746202276
0.587664569402
65.950
batch start
#iterations: 300
currently lose_sum: 96.51472645998001
time_elpased: 2.207
batch start
#iterations: 301
currently lose_sum: 96.52361822128296
time_elpased: 2.524
batch start
#iterations: 302
currently lose_sum: 96.51444488763809
time_elpased: 2.726
batch start
#iterations: 303
currently lose_sum: 96.60378795862198
time_elpased: 2.658
batch start
#iterations: 304
currently lose_sum: 96.60074228048325
time_elpased: 2.57
batch start
#iterations: 305
currently lose_sum: 96.03027904033661
time_elpased: 2.618
batch start
#iterations: 306
currently lose_sum: 96.41237843036652
time_elpased: 2.561
batch start
#iterations: 307
currently lose_sum: 96.64248883724213
time_elpased: 2.498
batch start
#iterations: 308
currently lose_sum: 96.67637985944748
time_elpased: 2.545
batch start
#iterations: 309
currently lose_sum: 96.36807990074158
time_elpased: 2.532
batch start
#iterations: 310
currently lose_sum: 96.55716753005981
time_elpased: 2.277
batch start
#iterations: 311
currently lose_sum: 96.32489585876465
time_elpased: 2.727
batch start
#iterations: 312
currently lose_sum: 96.43315905332565
time_elpased: 2.637
batch start
#iterations: 313
currently lose_sum: 96.375548183918
time_elpased: 2.627
batch start
#iterations: 314
currently lose_sum: 96.31352376937866
time_elpased: 2.514
batch start
#iterations: 315
currently lose_sum: 96.31282138824463
time_elpased: 2.532
batch start
#iterations: 316
currently lose_sum: 96.30946427583694
time_elpased: 2.592
batch start
#iterations: 317
currently lose_sum: 96.39305979013443
time_elpased: 2.615
batch start
#iterations: 318
currently lose_sum: 96.13593983650208
time_elpased: 2.58
batch start
#iterations: 319
currently lose_sum: 96.45229840278625
time_elpased: 2.599
start validation test
0.588092783505
0.611932555123
0.485540804775
0.541458656108
0.588272829321
66.164
batch start
#iterations: 320
currently lose_sum: 96.10587674379349
time_elpased: 2.177
batch start
#iterations: 321
currently lose_sum: 96.1906732916832
time_elpased: 2.562
batch start
#iterations: 322
currently lose_sum: 96.12363022565842
time_elpased: 2.57
batch start
#iterations: 323
currently lose_sum: 96.3633536696434
time_elpased: 2.581
batch start
#iterations: 324
currently lose_sum: 96.33682352304459
time_elpased: 2.554
batch start
#iterations: 325
currently lose_sum: 96.31639802455902
time_elpased: 2.508
batch start
#iterations: 326
currently lose_sum: 96.14832836389542
time_elpased: 2.59
batch start
#iterations: 327
currently lose_sum: 96.34447795152664
time_elpased: 2.65
batch start
#iterations: 328
currently lose_sum: 96.40515089035034
time_elpased: 2.602
batch start
#iterations: 329
currently lose_sum: 95.87037044763565
time_elpased: 2.582
batch start
#iterations: 330
currently lose_sum: 96.25152885913849
time_elpased: 2.572
batch start
#iterations: 331
currently lose_sum: 96.43248987197876
time_elpased: 2.597
batch start
#iterations: 332
currently lose_sum: 96.01111793518066
time_elpased: 2.554
batch start
#iterations: 333
currently lose_sum: 95.76522815227509
time_elpased: 2.631
batch start
#iterations: 334
currently lose_sum: 96.29407250881195
time_elpased: 2.647
batch start
#iterations: 335
currently lose_sum: 96.09547364711761
time_elpased: 2.537
batch start
#iterations: 336
currently lose_sum: 95.81198525428772
time_elpased: 2.569
batch start
#iterations: 337
currently lose_sum: 96.62140327692032
time_elpased: 2.231
batch start
#iterations: 338
currently lose_sum: 95.8965255022049
time_elpased: 2.656
batch start
#iterations: 339
currently lose_sum: 96.02681136131287
time_elpased: 2.608
start validation test
0.595206185567
0.62774122807
0.471338890604
0.538411802739
0.595423653709
66.088
batch start
#iterations: 340
currently lose_sum: 95.76276904344559
time_elpased: 2.371
batch start
#iterations: 341
currently lose_sum: 96.09891378879547
time_elpased: 2.45
batch start
#iterations: 342
currently lose_sum: 96.43213230371475
time_elpased: 2.557
batch start
#iterations: 343
currently lose_sum: 95.90005093812943
time_elpased: 2.571
batch start
#iterations: 344
currently lose_sum: 95.87062519788742
time_elpased: 2.631
batch start
#iterations: 345
currently lose_sum: 96.2208137512207
time_elpased: 2.497
batch start
#iterations: 346
currently lose_sum: 96.0347221493721
time_elpased: 1.964
batch start
#iterations: 347
currently lose_sum: 95.84570038318634
time_elpased: 2.602
batch start
#iterations: 348
currently lose_sum: 95.9778813123703
time_elpased: 2.594
batch start
#iterations: 349
currently lose_sum: 95.673095703125
time_elpased: 2.627
batch start
#iterations: 350
currently lose_sum: 95.97808372974396
time_elpased: 2.635
batch start
#iterations: 351
currently lose_sum: 96.1383318901062
time_elpased: 2.603
batch start
#iterations: 352
currently lose_sum: 95.53358227014542
time_elpased: 2.502
batch start
#iterations: 353
currently lose_sum: 95.70244044065475
time_elpased: 2.483
batch start
#iterations: 354
currently lose_sum: 96.04984229803085
time_elpased: 2.519
batch start
#iterations: 355
currently lose_sum: 95.37770348787308
time_elpased: 2.517
batch start
#iterations: 356
currently lose_sum: 95.52033984661102
time_elpased: 2.61
batch start
#iterations: 357
currently lose_sum: 95.89358448982239
time_elpased: 2.688
batch start
#iterations: 358
currently lose_sum: 95.67001003026962
time_elpased: 2.634
batch start
#iterations: 359
currently lose_sum: 95.99340462684631
time_elpased: 2.515
start validation test
0.586907216495
0.619542327671
0.454152516209
0.524109263658
0.587140287847
66.122
batch start
#iterations: 360
currently lose_sum: 95.69730705022812
time_elpased: 2.472
batch start
#iterations: 361
currently lose_sum: 95.69373160600662
time_elpased: 2.625
batch start
#iterations: 362
currently lose_sum: 95.67548418045044
time_elpased: 2.691
batch start
#iterations: 363
currently lose_sum: 95.80902761220932
time_elpased: 2.517
batch start
#iterations: 364
currently lose_sum: 95.56185799837112
time_elpased: 2.537
batch start
#iterations: 365
currently lose_sum: 95.64519667625427
time_elpased: 2.462
batch start
#iterations: 366
currently lose_sum: 95.67446130514145
time_elpased: 2.67
batch start
#iterations: 367
currently lose_sum: 95.2960250377655
time_elpased: 2.271
batch start
#iterations: 368
currently lose_sum: 95.48088812828064
time_elpased: 2.554
batch start
#iterations: 369
currently lose_sum: 95.33891201019287
time_elpased: 2.579
batch start
#iterations: 370
currently lose_sum: 95.40086930990219
time_elpased: 2.2
batch start
#iterations: 371
currently lose_sum: 95.51732647418976
time_elpased: 2.638
batch start
#iterations: 372
currently lose_sum: 95.47843366861343
time_elpased: 2.357
batch start
#iterations: 373
currently lose_sum: 95.1479532122612
time_elpased: 2.388
batch start
#iterations: 374
currently lose_sum: 95.6695203781128
time_elpased: 2.515
batch start
#iterations: 375
currently lose_sum: 95.33059054613113
time_elpased: 2.661
batch start
#iterations: 376
currently lose_sum: 95.12722438573837
time_elpased: 2.699
batch start
#iterations: 377
currently lose_sum: 95.15499305725098
time_elpased: 2.591
batch start
#iterations: 378
currently lose_sum: 95.51249504089355
time_elpased: 2.65
batch start
#iterations: 379
currently lose_sum: 95.4731092453003
time_elpased: 2.555
start validation test
0.588195876289
0.629768699309
0.431511783472
0.512122137405
0.58847095938
66.473
batch start
#iterations: 380
currently lose_sum: 95.2275025844574
time_elpased: 2.72
batch start
#iterations: 381
currently lose_sum: 95.01942729949951
time_elpased: 2.25
batch start
#iterations: 382
currently lose_sum: 95.2368877530098
time_elpased: 2.632
batch start
#iterations: 383
currently lose_sum: 95.41003918647766
time_elpased: 2.436
batch start
#iterations: 384
currently lose_sum: 95.53238272666931
time_elpased: 2.349
batch start
#iterations: 385
currently lose_sum: 95.30647897720337
time_elpased: 2.649
batch start
#iterations: 386
currently lose_sum: 95.4107112288475
time_elpased: 2.292
batch start
#iterations: 387
currently lose_sum: 95.13268387317657
time_elpased: 2.618
batch start
#iterations: 388
currently lose_sum: 94.9905686378479
time_elpased: 2.456
batch start
#iterations: 389
currently lose_sum: 95.39404785633087
time_elpased: 2.713
batch start
#iterations: 390
currently lose_sum: 95.27315682172775
time_elpased: 2.803
batch start
#iterations: 391
currently lose_sum: 95.21706569194794
time_elpased: 2.562
batch start
#iterations: 392
currently lose_sum: 95.56310510635376
time_elpased: 2.587
batch start
#iterations: 393
currently lose_sum: 95.06866383552551
time_elpased: 2.545
batch start
#iterations: 394
currently lose_sum: 95.2725595831871
time_elpased: 2.563
batch start
#iterations: 395
currently lose_sum: 94.89263725280762
time_elpased: 2.496
batch start
#iterations: 396
currently lose_sum: 94.85453081130981
time_elpased: 2.632
batch start
#iterations: 397
currently lose_sum: 95.18370896577835
time_elpased: 2.389
batch start
#iterations: 398
currently lose_sum: 95.070720911026
time_elpased: 2.41
batch start
#iterations: 399
currently lose_sum: 94.697294652462
time_elpased: 2.594
start validation test
0.589690721649
0.623959362213
0.455078728002
0.526303261128
0.589927053767
67.142
acc: 0.604
pre: 0.634
rec: 0.495
F1: 0.556
auc: 0.638
