start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.54503512382507
time_elpased: 1.455
batch start
#iterations: 1
currently lose_sum: 100.34291899204254
time_elpased: 1.502
batch start
#iterations: 2
currently lose_sum: 100.3738699555397
time_elpased: 1.483
batch start
#iterations: 3
currently lose_sum: 100.20100426673889
time_elpased: 1.47
batch start
#iterations: 4
currently lose_sum: 100.30721080303192
time_elpased: 1.466
batch start
#iterations: 5
currently lose_sum: 100.32536101341248
time_elpased: 1.436
batch start
#iterations: 6
currently lose_sum: 100.28071814775467
time_elpased: 1.415
batch start
#iterations: 7
currently lose_sum: 100.24831330776215
time_elpased: 1.484
batch start
#iterations: 8
currently lose_sum: 100.32310050725937
time_elpased: 1.492
batch start
#iterations: 9
currently lose_sum: 100.13099139928818
time_elpased: 1.417
batch start
#iterations: 10
currently lose_sum: 100.04148519039154
time_elpased: 1.474
batch start
#iterations: 11
currently lose_sum: 100.10916459560394
time_elpased: 1.47
batch start
#iterations: 12
currently lose_sum: 100.19409853219986
time_elpased: 1.432
batch start
#iterations: 13
currently lose_sum: 99.92948877811432
time_elpased: 1.454
batch start
#iterations: 14
currently lose_sum: 100.09112799167633
time_elpased: 1.449
batch start
#iterations: 15
currently lose_sum: 99.91846430301666
time_elpased: 1.429
batch start
#iterations: 16
currently lose_sum: 99.91500002145767
time_elpased: 1.422
batch start
#iterations: 17
currently lose_sum: 99.84602534770966
time_elpased: 1.483
batch start
#iterations: 18
currently lose_sum: 99.87042111158371
time_elpased: 1.505
batch start
#iterations: 19
currently lose_sum: 99.79033821821213
time_elpased: 1.5
start validation test
0.5835051546391753
0.5987929993964997
0.510548523206751
0.5511609821130985
0.5836332412584411
65.862
batch start
#iterations: 20
currently lose_sum: 99.93990397453308
time_elpased: 1.444
batch start
#iterations: 21
currently lose_sum: 99.82070082426071
time_elpased: 1.438
batch start
#iterations: 22
currently lose_sum: 99.89538985490799
time_elpased: 1.455
batch start
#iterations: 23
currently lose_sum: 99.65635275840759
time_elpased: 1.463
batch start
#iterations: 24
currently lose_sum: 99.73413240909576
time_elpased: 1.467
batch start
#iterations: 25
currently lose_sum: 99.67190712690353
time_elpased: 1.464
batch start
#iterations: 26
currently lose_sum: 99.56529796123505
time_elpased: 1.462
batch start
#iterations: 27
currently lose_sum: 99.75414729118347
time_elpased: 1.416
batch start
#iterations: 28
currently lose_sum: 99.5945919752121
time_elpased: 1.458
batch start
#iterations: 29
currently lose_sum: 99.6772363781929
time_elpased: 1.517
batch start
#iterations: 30
currently lose_sum: 99.49841022491455
time_elpased: 1.43
batch start
#iterations: 31
currently lose_sum: 99.61139661073685
time_elpased: 1.458
batch start
#iterations: 32
currently lose_sum: 99.53665101528168
time_elpased: 1.453
batch start
#iterations: 33
currently lose_sum: 99.59911978244781
time_elpased: 1.437
batch start
#iterations: 34
currently lose_sum: 99.40265703201294
time_elpased: 1.456
batch start
#iterations: 35
currently lose_sum: 99.45136553049088
time_elpased: 1.429
batch start
#iterations: 36
currently lose_sum: 99.37645906209946
time_elpased: 1.448
batch start
#iterations: 37
currently lose_sum: 99.49955409765244
time_elpased: 1.49
batch start
#iterations: 38
currently lose_sum: 99.0864742398262
time_elpased: 1.513
batch start
#iterations: 39
currently lose_sum: 99.50605976581573
time_elpased: 1.483
start validation test
0.5843814432989691
0.6036340852130326
0.49572913450653494
0.5443860541334691
0.5845370861007322
65.703
batch start
#iterations: 40
currently lose_sum: 99.325619161129
time_elpased: 1.511
batch start
#iterations: 41
currently lose_sum: 99.35135978460312
time_elpased: 1.454
batch start
#iterations: 42
currently lose_sum: 99.46234595775604
time_elpased: 1.445
batch start
#iterations: 43
currently lose_sum: 99.16676461696625
time_elpased: 1.43
batch start
#iterations: 44
currently lose_sum: 99.28013545274734
time_elpased: 1.476
batch start
#iterations: 45
currently lose_sum: 99.20295184850693
time_elpased: 1.445
batch start
#iterations: 46
currently lose_sum: 99.21581721305847
time_elpased: 1.412
batch start
#iterations: 47
currently lose_sum: 99.24718862771988
time_elpased: 1.448
batch start
#iterations: 48
currently lose_sum: 99.0995123386383
time_elpased: 1.48
batch start
#iterations: 49
currently lose_sum: 99.12499064207077
time_elpased: 1.444
batch start
#iterations: 50
currently lose_sum: 99.45269131660461
time_elpased: 1.446
batch start
#iterations: 51
currently lose_sum: 99.27362358570099
time_elpased: 1.44
batch start
#iterations: 52
currently lose_sum: 99.48958510160446
time_elpased: 1.46
batch start
#iterations: 53
currently lose_sum: 99.37801229953766
time_elpased: 1.441
batch start
#iterations: 54
currently lose_sum: 99.17336237430573
time_elpased: 1.417
batch start
#iterations: 55
currently lose_sum: 98.9718080163002
time_elpased: 1.452
batch start
#iterations: 56
currently lose_sum: 99.17359864711761
time_elpased: 1.485
batch start
#iterations: 57
currently lose_sum: 99.10878866910934
time_elpased: 1.458
batch start
#iterations: 58
currently lose_sum: 98.91485077142715
time_elpased: 1.445
batch start
#iterations: 59
currently lose_sum: 99.12167656421661
time_elpased: 1.441
start validation test
0.5759278350515464
0.6441005802707931
0.3426983636924977
0.4473701887552899
0.5763373053616883
65.752
batch start
#iterations: 60
currently lose_sum: 99.14259880781174
time_elpased: 1.455
batch start
#iterations: 61
currently lose_sum: 99.13886934518814
time_elpased: 1.45
batch start
#iterations: 62
currently lose_sum: 99.19489336013794
time_elpased: 1.494
batch start
#iterations: 63
currently lose_sum: 99.00474834442139
time_elpased: 1.473
batch start
#iterations: 64
currently lose_sum: 99.06239587068558
time_elpased: 1.481
batch start
#iterations: 65
currently lose_sum: 98.79008388519287
time_elpased: 1.474
batch start
#iterations: 66
currently lose_sum: 98.84025996923447
time_elpased: 1.427
batch start
#iterations: 67
currently lose_sum: 98.89381259679794
time_elpased: 1.512
batch start
#iterations: 68
currently lose_sum: 98.9793912768364
time_elpased: 1.422
batch start
#iterations: 69
currently lose_sum: 98.91263061761856
time_elpased: 1.465
batch start
#iterations: 70
currently lose_sum: 98.7510614991188
time_elpased: 1.436
batch start
#iterations: 71
currently lose_sum: 98.63979828357697
time_elpased: 1.454
batch start
#iterations: 72
currently lose_sum: 98.74302190542221
time_elpased: 1.475
batch start
#iterations: 73
currently lose_sum: 98.84492641687393
time_elpased: 1.526
batch start
#iterations: 74
currently lose_sum: 98.63314175605774
time_elpased: 1.431
batch start
#iterations: 75
currently lose_sum: 98.79363077878952
time_elpased: 1.426
batch start
#iterations: 76
currently lose_sum: 98.69783109426498
time_elpased: 1.459
batch start
#iterations: 77
currently lose_sum: 99.01401090621948
time_elpased: 1.482
batch start
#iterations: 78
currently lose_sum: 98.72426617145538
time_elpased: 1.508
batch start
#iterations: 79
currently lose_sum: 98.7375540137291
time_elpased: 1.452
start validation test
0.6073711340206186
0.6224347014925373
0.5493465061232891
0.5836112174055649
0.6074730052045754
64.829
batch start
#iterations: 80
currently lose_sum: 98.9115658402443
time_elpased: 1.501
batch start
#iterations: 81
currently lose_sum: 98.8046789765358
time_elpased: 1.466
batch start
#iterations: 82
currently lose_sum: 98.4869157075882
time_elpased: 1.471
batch start
#iterations: 83
currently lose_sum: 98.67795407772064
time_elpased: 1.468
batch start
#iterations: 84
currently lose_sum: 98.90746796131134
time_elpased: 1.464
batch start
#iterations: 85
currently lose_sum: 98.97769302129745
time_elpased: 1.483
batch start
#iterations: 86
currently lose_sum: 98.69401395320892
time_elpased: 1.514
batch start
#iterations: 87
currently lose_sum: 98.71907061338425
time_elpased: 1.491
batch start
#iterations: 88
currently lose_sum: 98.95370626449585
time_elpased: 1.511
batch start
#iterations: 89
currently lose_sum: 98.61291086673737
time_elpased: 1.491
batch start
#iterations: 90
currently lose_sum: 98.85596168041229
time_elpased: 1.417
batch start
#iterations: 91
currently lose_sum: 98.71439272165298
time_elpased: 1.455
batch start
#iterations: 92
currently lose_sum: 98.69924247264862
time_elpased: 1.446
batch start
#iterations: 93
currently lose_sum: 98.65987187623978
time_elpased: 1.46
batch start
#iterations: 94
currently lose_sum: 98.70884168148041
time_elpased: 1.453
batch start
#iterations: 95
currently lose_sum: 98.722864985466
time_elpased: 1.482
batch start
#iterations: 96
currently lose_sum: 98.37782061100006
time_elpased: 1.476
batch start
#iterations: 97
currently lose_sum: 98.64925390481949
time_elpased: 1.484
batch start
#iterations: 98
currently lose_sum: 98.38346099853516
time_elpased: 1.483
batch start
#iterations: 99
currently lose_sum: 98.59071797132492
time_elpased: 1.476
start validation test
0.6077319587628865
0.6230580539656582
0.548934856437172
0.5836524783893204
0.6078351861448484
64.837
batch start
#iterations: 100
currently lose_sum: 98.60111612081528
time_elpased: 1.5
batch start
#iterations: 101
currently lose_sum: 98.64908844232559
time_elpased: 1.491
batch start
#iterations: 102
currently lose_sum: 98.68727433681488
time_elpased: 1.434
batch start
#iterations: 103
currently lose_sum: 98.57153886556625
time_elpased: 1.441
batch start
#iterations: 104
currently lose_sum: 98.50579160451889
time_elpased: 1.442
batch start
#iterations: 105
currently lose_sum: 98.6243108510971
time_elpased: 1.419
batch start
#iterations: 106
currently lose_sum: 98.49038088321686
time_elpased: 1.436
batch start
#iterations: 107
currently lose_sum: 98.53716534376144
time_elpased: 1.45
batch start
#iterations: 108
currently lose_sum: 98.57949209213257
time_elpased: 1.45
batch start
#iterations: 109
currently lose_sum: 98.39596593379974
time_elpased: 1.485
batch start
#iterations: 110
currently lose_sum: 98.64065873622894
time_elpased: 1.489
batch start
#iterations: 111
currently lose_sum: 98.43393558263779
time_elpased: 1.469
batch start
#iterations: 112
currently lose_sum: 98.59407305717468
time_elpased: 1.486
batch start
#iterations: 113
currently lose_sum: 98.40612822771072
time_elpased: 1.456
batch start
#iterations: 114
currently lose_sum: 98.49718773365021
time_elpased: 1.464
batch start
#iterations: 115
currently lose_sum: 98.3763558268547
time_elpased: 1.476
batch start
#iterations: 116
currently lose_sum: 98.2857517004013
time_elpased: 1.489
batch start
#iterations: 117
currently lose_sum: 98.5342805981636
time_elpased: 1.468
batch start
#iterations: 118
currently lose_sum: 98.53608965873718
time_elpased: 1.536
batch start
#iterations: 119
currently lose_sum: 98.64238893985748
time_elpased: 1.453
start validation test
0.5970618556701031
0.6457055214723927
0.43326129463826285
0.518568701114738
0.5973494328194929
65.068
batch start
#iterations: 120
currently lose_sum: 98.60087394714355
time_elpased: 1.499
batch start
#iterations: 121
currently lose_sum: 98.34763962030411
time_elpased: 1.452
batch start
#iterations: 122
currently lose_sum: 98.14211648702621
time_elpased: 1.484
batch start
#iterations: 123
currently lose_sum: 98.41339123249054
time_elpased: 1.47
batch start
#iterations: 124
currently lose_sum: 98.30129683017731
time_elpased: 1.41
batch start
#iterations: 125
currently lose_sum: 98.51990550756454
time_elpased: 1.472
batch start
#iterations: 126
currently lose_sum: 98.32838869094849
time_elpased: 1.471
batch start
#iterations: 127
currently lose_sum: 98.2481700181961
time_elpased: 1.42
batch start
#iterations: 128
currently lose_sum: 98.31826430559158
time_elpased: 1.498
batch start
#iterations: 129
currently lose_sum: 98.07832223176956
time_elpased: 1.438
batch start
#iterations: 130
currently lose_sum: 97.99162602424622
time_elpased: 1.437
batch start
#iterations: 131
currently lose_sum: 98.38013219833374
time_elpased: 1.473
batch start
#iterations: 132
currently lose_sum: 98.30110549926758
time_elpased: 1.449
batch start
#iterations: 133
currently lose_sum: 98.1576116681099
time_elpased: 1.465
batch start
#iterations: 134
currently lose_sum: 97.93019980192184
time_elpased: 1.453
batch start
#iterations: 135
currently lose_sum: 98.50082910060883
time_elpased: 1.479
batch start
#iterations: 136
currently lose_sum: 98.43745583295822
time_elpased: 1.508
batch start
#iterations: 137
currently lose_sum: 98.64259248971939
time_elpased: 1.438
batch start
#iterations: 138
currently lose_sum: 98.0342218875885
time_elpased: 1.479
batch start
#iterations: 139
currently lose_sum: 98.32247620820999
time_elpased: 1.465
start validation test
0.5995360824742269
0.6296592119275826
0.4867757538334877
0.5490742353009461
0.5997340506232397
64.815
batch start
#iterations: 140
currently lose_sum: 97.94968271255493
time_elpased: 1.482
batch start
#iterations: 141
currently lose_sum: 98.01772546768188
time_elpased: 1.458
batch start
#iterations: 142
currently lose_sum: 98.33442163467407
time_elpased: 1.46
batch start
#iterations: 143
currently lose_sum: 98.02687847614288
time_elpased: 1.461
batch start
#iterations: 144
currently lose_sum: 98.29405206441879
time_elpased: 1.428
batch start
#iterations: 145
currently lose_sum: 98.0804780125618
time_elpased: 1.471
batch start
#iterations: 146
currently lose_sum: 97.99371862411499
time_elpased: 1.47
batch start
#iterations: 147
currently lose_sum: 98.17811924219131
time_elpased: 1.452
batch start
#iterations: 148
currently lose_sum: 97.8744432926178
time_elpased: 1.49
batch start
#iterations: 149
currently lose_sum: 98.41855853796005
time_elpased: 1.495
batch start
#iterations: 150
currently lose_sum: 98.24195855855942
time_elpased: 1.448
batch start
#iterations: 151
currently lose_sum: 98.13699609041214
time_elpased: 1.459
batch start
#iterations: 152
currently lose_sum: 98.09866213798523
time_elpased: 1.488
batch start
#iterations: 153
currently lose_sum: 98.20265024900436
time_elpased: 1.455
batch start
#iterations: 154
currently lose_sum: 98.21957862377167
time_elpased: 1.453
batch start
#iterations: 155
currently lose_sum: 98.09705740213394
time_elpased: 1.469
batch start
#iterations: 156
currently lose_sum: 98.17812407016754
time_elpased: 1.436
batch start
#iterations: 157
currently lose_sum: 97.9679297208786
time_elpased: 1.495
batch start
#iterations: 158
currently lose_sum: 98.22612380981445
time_elpased: 1.449
batch start
#iterations: 159
currently lose_sum: 98.01610243320465
time_elpased: 1.551
start validation test
0.6006701030927835
0.6259913021233052
0.5036533909642894
0.5581978899344169
0.6008404308947235
64.800
batch start
#iterations: 160
currently lose_sum: 98.21165013313293
time_elpased: 1.469
batch start
#iterations: 161
currently lose_sum: 98.01910161972046
time_elpased: 1.451
batch start
#iterations: 162
currently lose_sum: 98.1167026758194
time_elpased: 1.485
batch start
#iterations: 163
currently lose_sum: 98.1417224407196
time_elpased: 1.433
batch start
#iterations: 164
currently lose_sum: 97.93449252843857
time_elpased: 1.423
batch start
#iterations: 165
currently lose_sum: 98.04953771829605
time_elpased: 1.421
batch start
#iterations: 166
currently lose_sum: 97.80233293771744
time_elpased: 1.444
batch start
#iterations: 167
currently lose_sum: 97.9253677725792
time_elpased: 1.452
batch start
#iterations: 168
currently lose_sum: 97.84302151203156
time_elpased: 1.459
batch start
#iterations: 169
currently lose_sum: 98.02336865663528
time_elpased: 1.511
batch start
#iterations: 170
currently lose_sum: 98.21641510725021
time_elpased: 1.489
batch start
#iterations: 171
currently lose_sum: 97.9745123386383
time_elpased: 1.482
batch start
#iterations: 172
currently lose_sum: 97.97025090456009
time_elpased: 1.456
batch start
#iterations: 173
currently lose_sum: 97.67883056402206
time_elpased: 1.421
batch start
#iterations: 174
currently lose_sum: 97.95088040828705
time_elpased: 1.411
batch start
#iterations: 175
currently lose_sum: 98.03212189674377
time_elpased: 1.427
batch start
#iterations: 176
currently lose_sum: 98.22916680574417
time_elpased: 1.453
batch start
#iterations: 177
currently lose_sum: 97.69065034389496
time_elpased: 1.44
batch start
#iterations: 178
currently lose_sum: 97.88048356771469
time_elpased: 1.467
batch start
#iterations: 179
currently lose_sum: 97.93585157394409
time_elpased: 1.473
start validation test
0.5913917525773196
0.6476897689768977
0.40393125450241846
0.49755973886036636
0.5917208683954827
65.356
batch start
#iterations: 180
currently lose_sum: 97.79040586948395
time_elpased: 1.45
batch start
#iterations: 181
currently lose_sum: 97.94560086727142
time_elpased: 1.427
batch start
#iterations: 182
currently lose_sum: 98.07931435108185
time_elpased: 1.502
batch start
#iterations: 183
currently lose_sum: 98.02163606882095
time_elpased: 1.411
batch start
#iterations: 184
currently lose_sum: 98.00129872560501
time_elpased: 1.48
batch start
#iterations: 185
currently lose_sum: 97.93094170093536
time_elpased: 1.438
batch start
#iterations: 186
currently lose_sum: 97.94673854112625
time_elpased: 1.426
batch start
#iterations: 187
currently lose_sum: 98.00948935747147
time_elpased: 1.437
batch start
#iterations: 188
currently lose_sum: 97.9835239648819
time_elpased: 1.464
batch start
#iterations: 189
currently lose_sum: 97.53326368331909
time_elpased: 1.423
batch start
#iterations: 190
currently lose_sum: 97.81580793857574
time_elpased: 1.444
batch start
#iterations: 191
currently lose_sum: 97.7829459309578
time_elpased: 1.442
batch start
#iterations: 192
currently lose_sum: 97.95354676246643
time_elpased: 1.505
batch start
#iterations: 193
currently lose_sum: 97.80312711000443
time_elpased: 1.431
batch start
#iterations: 194
currently lose_sum: 97.61320233345032
time_elpased: 1.442
batch start
#iterations: 195
currently lose_sum: 97.7280844449997
time_elpased: 1.458
batch start
#iterations: 196
currently lose_sum: 97.9585987329483
time_elpased: 1.477
batch start
#iterations: 197
currently lose_sum: 97.70521259307861
time_elpased: 1.5
batch start
#iterations: 198
currently lose_sum: 97.80288863182068
time_elpased: 1.461
batch start
#iterations: 199
currently lose_sum: 97.58640837669373
time_elpased: 1.437
start validation test
0.5915463917525773
0.61052891135495
0.5096223114129875
0.555530625981602
0.5916902221115335
65.055
batch start
#iterations: 200
currently lose_sum: 97.81366837024689
time_elpased: 1.439
batch start
#iterations: 201
currently lose_sum: 97.65284699201584
time_elpased: 1.477
batch start
#iterations: 202
currently lose_sum: 97.74588471651077
time_elpased: 1.472
batch start
#iterations: 203
currently lose_sum: 97.77766227722168
time_elpased: 1.449
batch start
#iterations: 204
currently lose_sum: 97.77644246816635
time_elpased: 1.458
batch start
#iterations: 205
currently lose_sum: 97.61708188056946
time_elpased: 1.494
batch start
#iterations: 206
currently lose_sum: 97.54180783033371
time_elpased: 1.497
batch start
#iterations: 207
currently lose_sum: 97.53146916627884
time_elpased: 1.494
batch start
#iterations: 208
currently lose_sum: 97.65909737348557
time_elpased: 1.461
batch start
#iterations: 209
currently lose_sum: 97.80205541849136
time_elpased: 1.434
batch start
#iterations: 210
currently lose_sum: 97.56692904233932
time_elpased: 1.439
batch start
#iterations: 211
currently lose_sum: 97.61198896169662
time_elpased: 1.45
batch start
#iterations: 212
currently lose_sum: 97.56732195615768
time_elpased: 1.533
batch start
#iterations: 213
currently lose_sum: 97.66700875759125
time_elpased: 1.438
batch start
#iterations: 214
currently lose_sum: 97.55300962924957
time_elpased: 1.425
batch start
#iterations: 215
currently lose_sum: 97.29656732082367
time_elpased: 1.434
batch start
#iterations: 216
currently lose_sum: 97.67421442270279
time_elpased: 1.489
batch start
#iterations: 217
currently lose_sum: 97.72336286306381
time_elpased: 1.466
batch start
#iterations: 218
currently lose_sum: 97.33322185277939
time_elpased: 1.437
batch start
#iterations: 219
currently lose_sum: 97.56173074245453
time_elpased: 1.427
start validation test
0.5964432989690721
0.6276365603028664
0.4777194607389112
0.5425115409337931
0.5966517369789774
64.918
batch start
#iterations: 220
currently lose_sum: 97.23627710342407
time_elpased: 1.464
batch start
#iterations: 221
currently lose_sum: 97.69387793540955
time_elpased: 1.464
batch start
#iterations: 222
currently lose_sum: 97.54409897327423
time_elpased: 1.425
batch start
#iterations: 223
currently lose_sum: 97.40614348649979
time_elpased: 1.475
batch start
#iterations: 224
currently lose_sum: 97.45082545280457
time_elpased: 1.46
batch start
#iterations: 225
currently lose_sum: 97.47209239006042
time_elpased: 1.457
batch start
#iterations: 226
currently lose_sum: 97.45328313112259
time_elpased: 1.425
batch start
#iterations: 227
currently lose_sum: 97.5154042840004
time_elpased: 1.469
batch start
#iterations: 228
currently lose_sum: 97.35857200622559
time_elpased: 1.454
batch start
#iterations: 229
currently lose_sum: 97.61444562673569
time_elpased: 1.471
batch start
#iterations: 230
currently lose_sum: 97.64085429906845
time_elpased: 1.494
batch start
#iterations: 231
currently lose_sum: 97.67988389730453
time_elpased: 1.485
batch start
#iterations: 232
currently lose_sum: 97.26221019029617
time_elpased: 1.458
batch start
#iterations: 233
currently lose_sum: 97.49728202819824
time_elpased: 1.462
batch start
#iterations: 234
currently lose_sum: 97.35407787561417
time_elpased: 1.457
batch start
#iterations: 235
currently lose_sum: 97.47132611274719
time_elpased: 1.469
batch start
#iterations: 236
currently lose_sum: 97.59492409229279
time_elpased: 1.433
batch start
#iterations: 237
currently lose_sum: 97.54373663663864
time_elpased: 1.469
batch start
#iterations: 238
currently lose_sum: 97.24620056152344
time_elpased: 1.484
batch start
#iterations: 239
currently lose_sum: 97.14859855175018
time_elpased: 1.531
start validation test
0.5942268041237113
0.6186495176848874
0.49500874755582996
0.5499656986050765
0.5944009967253485
65.166
batch start
#iterations: 240
currently lose_sum: 97.405965924263
time_elpased: 1.502
batch start
#iterations: 241
currently lose_sum: 97.24044215679169
time_elpased: 1.425
batch start
#iterations: 242
currently lose_sum: 97.6265219449997
time_elpased: 1.463
batch start
#iterations: 243
currently lose_sum: 97.2293741106987
time_elpased: 1.451
batch start
#iterations: 244
currently lose_sum: 97.54278194904327
time_elpased: 1.497
batch start
#iterations: 245
currently lose_sum: 97.49794942140579
time_elpased: 1.5
batch start
#iterations: 246
currently lose_sum: 97.40587431192398
time_elpased: 1.457
batch start
#iterations: 247
currently lose_sum: 97.3978340625763
time_elpased: 1.459
batch start
#iterations: 248
currently lose_sum: 97.11456650495529
time_elpased: 1.462
batch start
#iterations: 249
currently lose_sum: 97.37679529190063
time_elpased: 1.447
batch start
#iterations: 250
currently lose_sum: 97.47566819190979
time_elpased: 1.478
batch start
#iterations: 251
currently lose_sum: 97.31203031539917
time_elpased: 1.474
batch start
#iterations: 252
currently lose_sum: 97.93808794021606
time_elpased: 1.431
batch start
#iterations: 253
currently lose_sum: 97.37598222494125
time_elpased: 1.424
batch start
#iterations: 254
currently lose_sum: 97.22706943750381
time_elpased: 1.467
batch start
#iterations: 255
currently lose_sum: 96.95091885328293
time_elpased: 1.444
batch start
#iterations: 256
currently lose_sum: 97.51421093940735
time_elpased: 1.46
batch start
#iterations: 257
currently lose_sum: 97.33130955696106
time_elpased: 1.493
batch start
#iterations: 258
currently lose_sum: 97.01774364709854
time_elpased: 1.464
batch start
#iterations: 259
currently lose_sum: 97.35409432649612
time_elpased: 1.492
start validation test
0.5941237113402061
0.6348207754206291
0.4465369970155398
0.5242870952150797
0.5943828225808877
65.091
batch start
#iterations: 260
currently lose_sum: 97.18514204025269
time_elpased: 1.481
batch start
#iterations: 261
currently lose_sum: 97.50069743394852
time_elpased: 1.44
batch start
#iterations: 262
currently lose_sum: 97.3187940120697
time_elpased: 1.46
batch start
#iterations: 263
currently lose_sum: 97.23321181535721
time_elpased: 1.455
batch start
#iterations: 264
currently lose_sum: 97.07905197143555
time_elpased: 1.487
batch start
#iterations: 265
currently lose_sum: 96.95419251918793
time_elpased: 1.423
batch start
#iterations: 266
currently lose_sum: 97.37130230665207
time_elpased: 1.437
batch start
#iterations: 267
currently lose_sum: 96.90859663486481
time_elpased: 1.458
batch start
#iterations: 268
currently lose_sum: 97.29529404640198
time_elpased: 1.472
batch start
#iterations: 269
currently lose_sum: 97.24609756469727
time_elpased: 1.483
batch start
#iterations: 270
currently lose_sum: 97.14461600780487
time_elpased: 1.541
batch start
#iterations: 271
currently lose_sum: 97.22865122556686
time_elpased: 1.446
batch start
#iterations: 272
currently lose_sum: 97.04239255189896
time_elpased: 1.484
batch start
#iterations: 273
currently lose_sum: 97.05437314510345
time_elpased: 1.447
batch start
#iterations: 274
currently lose_sum: 97.1993955373764
time_elpased: 1.443
batch start
#iterations: 275
currently lose_sum: 96.95884174108505
time_elpased: 1.456
batch start
#iterations: 276
currently lose_sum: 97.29067188501358
time_elpased: 1.503
batch start
#iterations: 277
currently lose_sum: 96.69517177343369
time_elpased: 1.457
batch start
#iterations: 278
currently lose_sum: 97.03366786241531
time_elpased: 1.454
batch start
#iterations: 279
currently lose_sum: 96.96614116430283
time_elpased: 1.444
start validation test
0.589381443298969
0.6177538668459986
0.4726767520839765
0.5355643656716418
0.5895863363848572
65.555
batch start
#iterations: 280
currently lose_sum: 96.9881916642189
time_elpased: 1.464
batch start
#iterations: 281
currently lose_sum: 97.4358246922493
time_elpased: 1.416
batch start
#iterations: 282
currently lose_sum: 97.1357484459877
time_elpased: 1.473
batch start
#iterations: 283
currently lose_sum: 97.02081173658371
time_elpased: 1.491
batch start
#iterations: 284
currently lose_sum: 97.02227956056595
time_elpased: 1.48
batch start
#iterations: 285
currently lose_sum: 96.97851920127869
time_elpased: 1.46
batch start
#iterations: 286
currently lose_sum: 97.36626732349396
time_elpased: 1.517
batch start
#iterations: 287
currently lose_sum: 96.95690792798996
time_elpased: 1.43
batch start
#iterations: 288
currently lose_sum: 96.90654146671295
time_elpased: 1.436
batch start
#iterations: 289
currently lose_sum: 97.12481170892715
time_elpased: 1.501
batch start
#iterations: 290
currently lose_sum: 97.11853331327438
time_elpased: 1.419
batch start
#iterations: 291
currently lose_sum: 96.86030834913254
time_elpased: 1.413
batch start
#iterations: 292
currently lose_sum: 97.16744840145111
time_elpased: 1.468
batch start
#iterations: 293
currently lose_sum: 96.90421497821808
time_elpased: 1.451
batch start
#iterations: 294
currently lose_sum: 96.93502873182297
time_elpased: 1.452
batch start
#iterations: 295
currently lose_sum: 96.84082943201065
time_elpased: 1.474
batch start
#iterations: 296
currently lose_sum: 96.77265304327011
time_elpased: 1.47
batch start
#iterations: 297
currently lose_sum: 97.11929905414581
time_elpased: 1.457
batch start
#iterations: 298
currently lose_sum: 97.10954302549362
time_elpased: 1.431
batch start
#iterations: 299
currently lose_sum: 96.86698943376541
time_elpased: 1.448
start validation test
0.5797938144329897
0.6044029352901935
0.46619326952763196
0.5263769463165233
0.5799932577112495
65.871
batch start
#iterations: 300
currently lose_sum: 96.72492581605911
time_elpased: 1.469
batch start
#iterations: 301
currently lose_sum: 97.1807552576065
time_elpased: 1.438
batch start
#iterations: 302
currently lose_sum: 96.76252913475037
time_elpased: 1.441
batch start
#iterations: 303
currently lose_sum: 96.86989706754684
time_elpased: 1.433
batch start
#iterations: 304
currently lose_sum: 96.96273005008698
time_elpased: 1.456
batch start
#iterations: 305
currently lose_sum: 96.8660180568695
time_elpased: 1.464
batch start
#iterations: 306
currently lose_sum: 96.75364571809769
time_elpased: 1.488
batch start
#iterations: 307
currently lose_sum: 96.80206912755966
time_elpased: 1.491
batch start
#iterations: 308
currently lose_sum: 96.61749404668808
time_elpased: 1.489
batch start
#iterations: 309
currently lose_sum: 96.6344565153122
time_elpased: 1.454
batch start
#iterations: 310
currently lose_sum: 96.62437605857849
time_elpased: 1.413
batch start
#iterations: 311
currently lose_sum: 96.67874318361282
time_elpased: 1.442
batch start
#iterations: 312
currently lose_sum: 96.86141693592072
time_elpased: 1.471
batch start
#iterations: 313
currently lose_sum: 97.06516182422638
time_elpased: 1.47
batch start
#iterations: 314
currently lose_sum: 96.75461941957474
time_elpased: 1.456
batch start
#iterations: 315
currently lose_sum: 96.42569386959076
time_elpased: 1.528
batch start
#iterations: 316
currently lose_sum: 96.94820892810822
time_elpased: 1.472
batch start
#iterations: 317
currently lose_sum: 96.6448734998703
time_elpased: 1.437
batch start
#iterations: 318
currently lose_sum: 96.98169404268265
time_elpased: 1.473
batch start
#iterations: 319
currently lose_sum: 96.89680099487305
time_elpased: 1.449
start validation test
0.5923195876288659
0.625520688697584
0.4636204589894
0.5325373840061469
0.5925455387996674
65.429
batch start
#iterations: 320
currently lose_sum: 96.77681970596313
time_elpased: 1.447
batch start
#iterations: 321
currently lose_sum: 96.91204577684402
time_elpased: 1.424
batch start
#iterations: 322
currently lose_sum: 96.65826082229614
time_elpased: 1.483
batch start
#iterations: 323
currently lose_sum: 96.42349660396576
time_elpased: 1.487
batch start
#iterations: 324
currently lose_sum: 96.74053305387497
time_elpased: 1.456
batch start
#iterations: 325
currently lose_sum: 96.82627546787262
time_elpased: 1.446
batch start
#iterations: 326
currently lose_sum: 97.17416942119598
time_elpased: 1.451
batch start
#iterations: 327
currently lose_sum: 96.44100219011307
time_elpased: 1.455
batch start
#iterations: 328
currently lose_sum: 96.76539367437363
time_elpased: 1.436
batch start
#iterations: 329
currently lose_sum: 96.74752795696259
time_elpased: 1.432
batch start
#iterations: 330
currently lose_sum: 96.68384099006653
time_elpased: 1.476
batch start
#iterations: 331
currently lose_sum: 96.65128588676453
time_elpased: 1.43
batch start
#iterations: 332
currently lose_sum: 96.51169043779373
time_elpased: 1.452
batch start
#iterations: 333
currently lose_sum: 96.70305043458939
time_elpased: 1.43
batch start
#iterations: 334
currently lose_sum: 96.81854546070099
time_elpased: 1.45
batch start
#iterations: 335
currently lose_sum: 96.63490718603134
time_elpased: 1.454
batch start
#iterations: 336
currently lose_sum: 96.63522815704346
time_elpased: 1.494
batch start
#iterations: 337
currently lose_sum: 96.47024184465408
time_elpased: 1.486
batch start
#iterations: 338
currently lose_sum: 96.66574716567993
time_elpased: 1.438
batch start
#iterations: 339
currently lose_sum: 96.55330169200897
time_elpased: 1.414
start validation test
0.5854123711340207
0.6155758077879039
0.45878357517752394
0.5257385459048293
0.5856346875164704
65.554
batch start
#iterations: 340
currently lose_sum: 96.64283758401871
time_elpased: 1.438
batch start
#iterations: 341
currently lose_sum: 96.24993872642517
time_elpased: 1.46
batch start
#iterations: 342
currently lose_sum: 96.15422320365906
time_elpased: 1.437
batch start
#iterations: 343
currently lose_sum: 96.80492490530014
time_elpased: 1.429
batch start
#iterations: 344
currently lose_sum: 96.70137894153595
time_elpased: 1.444
batch start
#iterations: 345
currently lose_sum: 96.80330389738083
time_elpased: 1.433
batch start
#iterations: 346
currently lose_sum: 96.58083701133728
time_elpased: 1.522
batch start
#iterations: 347
currently lose_sum: 96.4440284371376
time_elpased: 1.471
batch start
#iterations: 348
currently lose_sum: 96.56038463115692
time_elpased: 1.455
batch start
#iterations: 349
currently lose_sum: 96.27343839406967
time_elpased: 1.433
batch start
#iterations: 350
currently lose_sum: 96.43946814537048
time_elpased: 1.461
batch start
#iterations: 351
currently lose_sum: 96.28759479522705
time_elpased: 1.482
batch start
#iterations: 352
currently lose_sum: 96.50389474630356
time_elpased: 1.436
batch start
#iterations: 353
currently lose_sum: 96.10301923751831
time_elpased: 1.477
batch start
#iterations: 354
currently lose_sum: 96.57355862855911
time_elpased: 1.453
batch start
#iterations: 355
currently lose_sum: 96.32126098871231
time_elpased: 1.439
batch start
#iterations: 356
currently lose_sum: 96.58410477638245
time_elpased: 1.468
batch start
#iterations: 357
currently lose_sum: 96.66952729225159
time_elpased: 1.441
batch start
#iterations: 358
currently lose_sum: 96.59694957733154
time_elpased: 1.496
batch start
#iterations: 359
currently lose_sum: 96.47766047716141
time_elpased: 1.446
start validation test
0.5946907216494846
0.6304164321890827
0.46115056087269735
0.5326597325408617
0.594925171998881
65.233
batch start
#iterations: 360
currently lose_sum: 96.6701774597168
time_elpased: 1.494
batch start
#iterations: 361
currently lose_sum: 96.46859735250473
time_elpased: 1.454
batch start
#iterations: 362
currently lose_sum: 96.77806109189987
time_elpased: 1.464
batch start
#iterations: 363
currently lose_sum: 96.4573877453804
time_elpased: 1.457
batch start
#iterations: 364
currently lose_sum: 96.17121922969818
time_elpased: 1.473
batch start
#iterations: 365
currently lose_sum: 96.42654746770859
time_elpased: 1.473
batch start
#iterations: 366
currently lose_sum: 96.40343874692917
time_elpased: 1.448
batch start
#iterations: 367
currently lose_sum: 96.28770822286606
time_elpased: 1.465
batch start
#iterations: 368
currently lose_sum: 96.33433628082275
time_elpased: 1.446
batch start
#iterations: 369
currently lose_sum: 96.30822747945786
time_elpased: 1.445
batch start
#iterations: 370
currently lose_sum: 96.23420882225037
time_elpased: 1.515
batch start
#iterations: 371
currently lose_sum: 96.33873742818832
time_elpased: 1.497
batch start
#iterations: 372
currently lose_sum: 96.42986804246902
time_elpased: 1.468
batch start
#iterations: 373
currently lose_sum: 96.42154097557068
time_elpased: 1.455
batch start
#iterations: 374
currently lose_sum: 96.41193294525146
time_elpased: 1.462
batch start
#iterations: 375
currently lose_sum: 96.5668363571167
time_elpased: 1.431
batch start
#iterations: 376
currently lose_sum: 96.44589293003082
time_elpased: 1.455
batch start
#iterations: 377
currently lose_sum: 96.10943394899368
time_elpased: 1.459
batch start
#iterations: 378
currently lose_sum: 96.52314805984497
time_elpased: 1.487
batch start
#iterations: 379
currently lose_sum: 96.29609835147858
time_elpased: 1.453
start validation test
0.5865979381443299
0.6141838245189073
0.46969229185962746
0.5323069745742943
0.586803184037838
65.780
batch start
#iterations: 380
currently lose_sum: 96.43839138746262
time_elpased: 1.454
batch start
#iterations: 381
currently lose_sum: 96.08547747135162
time_elpased: 1.468
batch start
#iterations: 382
currently lose_sum: 95.82839119434357
time_elpased: 1.462
batch start
#iterations: 383
currently lose_sum: 96.32561880350113
time_elpased: 1.425
batch start
#iterations: 384
currently lose_sum: 96.21076953411102
time_elpased: 1.44
batch start
#iterations: 385
currently lose_sum: 96.29455667734146
time_elpased: 1.426
batch start
#iterations: 386
currently lose_sum: 96.57378995418549
time_elpased: 1.459
batch start
#iterations: 387
currently lose_sum: 96.30685943365097
time_elpased: 1.444
batch start
#iterations: 388
currently lose_sum: 96.07822835445404
time_elpased: 1.437
batch start
#iterations: 389
currently lose_sum: 96.38127398490906
time_elpased: 1.493
batch start
#iterations: 390
currently lose_sum: 96.27925753593445
time_elpased: 1.48
batch start
#iterations: 391
currently lose_sum: 96.15322554111481
time_elpased: 1.461
batch start
#iterations: 392
currently lose_sum: 96.3428326845169
time_elpased: 1.466
batch start
#iterations: 393
currently lose_sum: 96.2676351070404
time_elpased: 1.45
batch start
#iterations: 394
currently lose_sum: 96.18460500240326
time_elpased: 1.452
batch start
#iterations: 395
currently lose_sum: 96.35748499631882
time_elpased: 1.46
batch start
#iterations: 396
currently lose_sum: 96.13989013433456
time_elpased: 1.46
batch start
#iterations: 397
currently lose_sum: 96.34864169359207
time_elpased: 1.732
batch start
#iterations: 398
currently lose_sum: 96.19405549764633
time_elpased: 1.737
batch start
#iterations: 399
currently lose_sum: 96.4002121090889
time_elpased: 1.533
start validation test
0.5841237113402061
0.6223112297878653
0.4317176083153237
0.5097824766071213
0.5843912837610906
66.016
acc: 0.598
pre: 0.623
rec: 0.497
F1: 0.553
auc: 0.626
