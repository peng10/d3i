#!/bin/tcsh
foreach method (max mean)
	foreach sim_file (chem enzyme indication offsideeffect pathway target transporter)
		echo $sim_file
		foreach folds (0 1 2 3 4)
			set num_pred = `less ./$method\_hypo_prediction_value/prediction_value_0_$sim_file\_Jacarrd_sim.csv_PNN_entire_test$folds.txt_32_128_2 | wc | awk '{print $1}'`
			echo $num_pred
			less ../PNN_entire_test$folds.txt | awk -v "num=$num_pred" '{if(NR<=num) {print $NF} }' > ground_$folds
				foreach layer (1 2 4 6)
					foreach dim (32 64 128)
						python evaluation.py --ground_truth ground_$folds --prediction_value ./$method\_hypo_prediction_value/prediction_value_0_$sim_file\_Jacarrd_sim.csv_PNN_entire_test$folds.txt_32_$dim\_$layer --out_path true.txt
						set true_auc = `less true.txt | awk '{printf ("%.3f\n", $1)}'`
						less $method\_hypo/log_$sim_file\_Jacarrd_sim.csv_1N_attention_0_L_32_feature_$dim\_$layer\_$folds.txt | awk -v "true_auc=$true_auc" '{if(NR == 1749) {$2=true_auc}; print $0}' > ./test_$dim\_$layer\_$folds.txt
						mv ./test_$dim\_$layer\_$folds.txt $method\_hypo/log_$sim_file\_Jacarrd_sim.csv_1N_attention_0_L_32_feature_$dim\_$layer\_$folds.txt
					end
				end
		end
	end
end
