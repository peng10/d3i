#!/bin/tcsh
foreach method (log)
	foreach sim_file (chem enzyme indication offsideeffect pathway target transporter)
		echo $sim_file
		foreach folds (0 1 2 3 4)
			set num_pred = `less ./$method\_hypo_prediction_value/prediction_value_1_$sim_file\_Jacarrd_sim.csv_PNN_entire_test$folds.txt_32_128_1 | wc | awk '{print $1}'`
			echo $num_pred
			less ../PNN_entire_test$folds.txt | awk -v "num=$num_pred" '{if(NR<=num) {print $NF} }' > ground_$folds
				foreach layer (8 16 32 64 128)
					foreach dim (32 64 128)
						python evaluation.py --ground_truth ground_$folds --prediction_value ./$method\_hypo_prediction_value/prediction_value_1_$sim_file\_Jacarrd_sim.csv_PNN_entire_test$folds.txt_$layer\_$dim\_1 --out_path true.txt
						set true_auc = `less true.txt | awk '{printf ("%.3f\n", $1)}'`
						less $method\_hypo/log_$sim_file\_Jacarrd_sim.csv_1N_attention_1_L_$layer\_feature_$dim\_1_$folds.txt | awk -v "true_auc=$true_auc" '{if(NR == 1749) {$2=true_auc}; print $0}' > ./test_$dim\_$layer\_$folds.txt
						mv ./test_$dim\_$layer\_$folds.txt $method\_hypo/log_$sim_file\_Jacarrd_sim.csv_1N_attention_1_L_$layer\_feature_$dim\_1_$folds.txt
					end
				end
		end
	end
end
