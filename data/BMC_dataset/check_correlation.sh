#!/bin/tcsh
foreach folds (0 1 2 3 4)
	less PNN_Jure_train$folds.txt | awk '{if($3==0) {print $1; print $2} }' | sort | uniq -c | sort -k2n > train_freN$folds.txt
	set num_N = `less train_freN$folds.txt | wc | awk '{print $1}'`
	less PNN_Jure_train$folds.txt | awk '{if($3==1) {print $1; print $2} }' | sort | uniq -c | sort -k2n > train_freP$folds.txt
	set num_P = `less train_freP$folds.txt | wc | awk '{print $1}'` 
	cat train_freN$folds.txt train_freP$folds.txt PNN_Jure_test$folds.txt	| awk -v "nN=$num_N" -v "np=$num_P" '{if(NR<=nN) {nl[$2] = $1} else if(NR<=np+nN) {pl[$2] = $1} else{print (pl[$1]-nl[$1] + pl[$2]-nl[$2])}}' > difference$folds.txt
	python correlation.py difference$folds.txt prediction_value_1_chem_Jacarrd_sim.csv_PNN_Jure_test$folds.txt >> z
end
less z | awk 'BEGIN{sum=0}{sum+=$1}END{print sum/5}' > ./Jure_remove_sample_correlation/cor_Jure.txt
cat difference0.txt difference1.txt difference2.txt difference3.txt difference4.txt | awk 'BEGIN{sum=0;counter=0}{sum+=$1;counter+=1}END{print sum/counter}' >> ./Jure_remove_sample_correlation/cor_Jure.txt
