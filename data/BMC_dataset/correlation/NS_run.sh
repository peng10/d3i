#!/bin/tcsh
foreach folds (0 1 2 3 4)
	set num_test = `less PNN_entire_test$folds.txt | wc | awk '{print $1}'`
	set num_val = `less PNN_entire_validation$folds.txt | wc | awk '{print $1}'`
	set num_train = `less PNN_entire_train$folds.txt | wc | awk '{print $1}'`
	set num_train2 = `less PN2N_entire_train$folds.txt | wc | awk '{print $1}'`

	foreach sim_file (chem_Jacarrd_sim.csv)
		foreach L (8)
			python frame_work_test.py --attention $1 --train PNN_entire_train$folds.txt --test PNN_entire_test$folds.txt --validation PNN_entire_validation$folds.txt --num_train_sample $num_train --num_validation_sample $num_val --num_test_sample $num_test --sim_file $sim_file --L_dim $L > ./results/log_$sim_file\_1N_attention_$1_L_$L\_feature_50_1_$folds.txt
			python frame_work_test.py --attention $1 --train PN2N_entire_train$folds.txt --test PNN_entire_test$folds.txt --validation PNN_entire_validation$folds.txt --num_train_sample $num_train2 --num_validation_sample $num_val --num_test_sample $num_test --sim_file $sim_file --L_dim $L > ./results/log_$sim_file\_2N_attention_$1_L_$L\_feature_50_1_$folds.txt
		end
	end
end
