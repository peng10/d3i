#!/bin/tcsh
foreach folds (0 1 2 3 4)
        foreach step (train validation test)
                set num_sample = `less ../PN_$step\_$folds.txt | wc | awk '{print $1}'`
                if($step == validation || $step == test) then
                        cat ../PN_$step\_$folds.txt ../PN_train_$folds.txt > temp
                        less temp | awk '{print$1;print$2}' | sort | uniq -c | sort -k2n > freq_PN$step$folds.txt
                        python negsamp.py --num_sample $num_sample --train_file temp --freq_file freq_PN$step$folds.txt --outfile PN_$step\_N_set$folds.txt
                        rm temp
                else
                        less ../PN_$step\_$folds.txt | awk '{print$1;print$2}' | sort | uniq -c | sort -k2n > freq_PN$step$folds.txt
                        python negsamp.py --num_sample $num_sample --train_file PN_$step\_$folds.txt --freq_file freq_PN$step$folds.txt --outfile PN_$step\_N_set$folds.txt
                endif
                cat ../PN_$step\_$folds.txt PN_$step\_N_set$folds.txt > PNN_entire_$step$folds.txt
                cat ../PN_$step\_$folds.txt PN_$step\_N_set$folds.txt PN_$step\_N_set$folds.txt > PN2N_entire_$step$folds.txt
        end
end
