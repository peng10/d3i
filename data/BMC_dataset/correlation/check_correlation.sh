#!/bin/tcsh
foreach folds (0 1 2 3 4)
	less ../PN_train_$folds.txt | awk '{if($3==1) {print $1; print $2} }' | sort | uniq -c | sort -k2n > train_freP$folds.txt
	set num_P = `less train_freP$folds.txt | wc | awk '{print $1}'` 
	cat train_freP$folds.txt ../PNN_entire_test$folds.txt | awk -v "np=$num_P" '{if(NR<=np+nN) {pl[$2] = $1} else{print (pl[$1] + pl[$2])}}' > difference$folds.txt
	set filename = ../batch_sample_results/log_hypo_prediction_value/prediction_value_1_$1_Jacarrd_sim.csv_PNN_entire_test$folds.txt_16_64_1
	set num_instance = `less $filename | wc | awk '{print $1}'`
	python correlation.py difference$folds.txt $filename $num_instance >> x
end
less x | awk 'BEGIN{sum=0}{sum+=$1}END{print sum/5}' > cor_$2.txt
cat difference0.txt difference1.txt difference2.txt difference3.txt difference4.txt | awk 'BEGIN{sum=0;counter=0}{sum+=$1;counter+=1}END{print sum/counter}' >> cor_$2.txt

rm difference0.txt difference1.txt difference2.txt difference3.txt difference4.txt
rm train_freP0.txt train_freP1.txt train_freP2.txt train_freP3.txt train_freP4.txt
rm x
