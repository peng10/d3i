import numpy as np
import sys
import pdb
file_1 = sys.argv[1]
file_2 = sys.argv[2]
num_instance = int(sys.argv[3])

array_1 = np.loadtxt(file_1)[:num_instance]
array_2 = np.loadtxt(file_2)
#pdb.set_trace()
print "%.3f" % (np.corrcoef(array_1, array_2)[0,1]) 
