#!/bin/tcsh
foreach folds (0 1 2 3 4)
	less log_chem_Jacarrd_sim.csv_1N_attention_1_L_32_feature_50_1_$folds.txt | tail -5 > temp$folds
end
paste temp0 temp1 temp2 temp3 temp4 | awk '{print $1"\t"($2+$4+$6+$8+$10)/5}' > reports_1N.txt
rm temp0 temp1 temp2 temp3 temp4
