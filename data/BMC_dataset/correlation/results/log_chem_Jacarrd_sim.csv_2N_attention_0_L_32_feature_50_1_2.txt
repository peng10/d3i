start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 556.9674863219261
time_elpased: 1.563
batch start
#iterations: 1
currently lose_sum: 554.8369576334953
time_elpased: 1.448
batch start
#iterations: 2
currently lose_sum: 555.3310713171959
time_elpased: 1.41
batch start
#iterations: 3
currently lose_sum: 553.363559961319
time_elpased: 1.413
batch start
#iterations: 4
currently lose_sum: 552.2181984186172
time_elpased: 1.435
batch start
#iterations: 5
currently lose_sum: 552.4918118715286
time_elpased: 1.4
batch start
#iterations: 6
currently lose_sum: 549.6286853551865
time_elpased: 1.419
batch start
#iterations: 7
currently lose_sum: 550.681037902832
time_elpased: 1.417
batch start
#iterations: 8
currently lose_sum: 547.9067987203598
time_elpased: 1.432
batch start
#iterations: 9
currently lose_sum: 549.3355079293251
time_elpased: 1.415
batch start
#iterations: 10
currently lose_sum: 548.0403971672058
time_elpased: 1.413
batch start
#iterations: 11
currently lose_sum: 549.029071033001
time_elpased: 1.424
batch start
#iterations: 12
currently lose_sum: 546.6167203187943
time_elpased: 1.414
batch start
#iterations: 13
currently lose_sum: 544.875444829464
time_elpased: 1.425
batch start
#iterations: 14
currently lose_sum: 543.9548069238663
time_elpased: 1.404
batch start
#iterations: 15
currently lose_sum: 542.606348335743
time_elpased: 1.428
batch start
#iterations: 16
currently lose_sum: 543.4355625510216
time_elpased: 1.408
batch start
#iterations: 17
currently lose_sum: 541.4590025544167
time_elpased: 1.4
batch start
#iterations: 18
currently lose_sum: 540.1367816925049
time_elpased: 1.413
batch start
#iterations: 19
currently lose_sum: 540.4562647938728
time_elpased: 1.399
start validation test
0.532628865979
0.649219467401
0.145518164042
0.237746952501
0
validation finish
batch start
#iterations: 20
currently lose_sum: 538.7252551317215
time_elpased: 1.414
batch start
#iterations: 21
currently lose_sum: 540.4277167022228
time_elpased: 1.424
batch start
#iterations: 22
currently lose_sum: 537.1663203835487
time_elpased: 1.423
batch start
#iterations: 23
currently lose_sum: 537.1548379957676
time_elpased: 1.391
batch start
#iterations: 24
currently lose_sum: 534.3267571926117
time_elpased: 1.433
batch start
#iterations: 25
currently lose_sum: 535.5116762518883
time_elpased: 1.41
batch start
#iterations: 26
currently lose_sum: 535.2170107662678
time_elpased: 1.399
batch start
#iterations: 27
currently lose_sum: 534.6791682839394
time_elpased: 1.395
batch start
#iterations: 28
currently lose_sum: 533.2490881979465
time_elpased: 1.405
batch start
#iterations: 29
currently lose_sum: 532.1943826675415
time_elpased: 1.399
batch start
#iterations: 30
currently lose_sum: 532.243310958147
time_elpased: 1.396
batch start
#iterations: 31
currently lose_sum: 531.1433928906918
time_elpased: 1.391
batch start
#iterations: 32
currently lose_sum: 533.1911074519157
time_elpased: 1.42
batch start
#iterations: 33
currently lose_sum: 530.2515861988068
time_elpased: 1.403
batch start
#iterations: 34
currently lose_sum: 531.3562590181828
time_elpased: 1.396
batch start
#iterations: 35
currently lose_sum: 528.3132650256157
time_elpased: 1.408
batch start
#iterations: 36
currently lose_sum: 529.577633023262
time_elpased: 1.395
batch start
#iterations: 37
currently lose_sum: 528.5755314230919
time_elpased: 1.398
batch start
#iterations: 38
currently lose_sum: 529.3950697779655
time_elpased: 1.397
batch start
#iterations: 39
currently lose_sum: 529.6878945231438
time_elpased: 1.381
start validation test
0.518402061856
0.650563607085
0.0831532365957
0.147458709736
0
validation finish
batch start
#iterations: 40
currently lose_sum: 531.5203987658024
time_elpased: 1.391
batch start
#iterations: 41
currently lose_sum: 530.3655315637589
time_elpased: 1.393
batch start
#iterations: 42
currently lose_sum: 528.8346427083015
time_elpased: 1.385
batch start
#iterations: 43
currently lose_sum: 527.2422231137753
time_elpased: 1.388
batch start
#iterations: 44
currently lose_sum: 528.455198943615
time_elpased: 1.385
batch start
#iterations: 45
currently lose_sum: 527.5876946747303
time_elpased: 1.416
batch start
#iterations: 46
currently lose_sum: 526.6388718485832
time_elpased: 1.404
batch start
#iterations: 47
currently lose_sum: 527.4899551570415
time_elpased: 1.404
batch start
#iterations: 48
currently lose_sum: 526.632248044014
time_elpased: 1.394
batch start
#iterations: 49
currently lose_sum: 526.1486059725285
time_elpased: 1.393
batch start
#iterations: 50
currently lose_sum: 527.7750143408775
time_elpased: 1.396
batch start
#iterations: 51
currently lose_sum: 526.0152364075184
time_elpased: 1.388
batch start
#iterations: 52
currently lose_sum: 526.0533526539803
time_elpased: 1.387
batch start
#iterations: 53
currently lose_sum: 527.4805935025215
time_elpased: 1.399
batch start
#iterations: 54
currently lose_sum: 524.8418366014957
time_elpased: 1.397
batch start
#iterations: 55
currently lose_sum: 526.4161332249641
time_elpased: 1.397
batch start
#iterations: 56
currently lose_sum: 525.4982572197914
time_elpased: 1.38
batch start
#iterations: 57
currently lose_sum: 524.5006172657013
time_elpased: 1.397
batch start
#iterations: 58
currently lose_sum: 526.8558560013771
time_elpased: 1.394
batch start
#iterations: 59
currently lose_sum: 523.8267526328564
time_elpased: 1.391
start validation test
0.52912371134
0.643632773939
0.134197797674
0.222089755599
0
validation finish
batch start
#iterations: 60
currently lose_sum: 525.7030543386936
time_elpased: 1.387
batch start
#iterations: 61
currently lose_sum: 525.3007853031158
time_elpased: 1.39
batch start
#iterations: 62
currently lose_sum: 527.3995095491409
time_elpased: 1.39
batch start
#iterations: 63
currently lose_sum: 525.3460300564766
time_elpased: 1.394
batch start
#iterations: 64
currently lose_sum: 525.33068972826
time_elpased: 1.392
batch start
#iterations: 65
currently lose_sum: 526.4003746211529
time_elpased: 1.388
batch start
#iterations: 66
currently lose_sum: 523.7982448935509
time_elpased: 1.415
batch start
#iterations: 67
currently lose_sum: 522.3929167091846
time_elpased: 1.383
batch start
#iterations: 68
currently lose_sum: 526.2403960227966
time_elpased: 1.387
batch start
#iterations: 69
currently lose_sum: 524.3155517280102
time_elpased: 1.421
batch start
#iterations: 70
currently lose_sum: 524.2243009507656
time_elpased: 1.458
batch start
#iterations: 71
currently lose_sum: 524.3349070549011
time_elpased: 1.399
batch start
#iterations: 72
currently lose_sum: 523.2294019460678
time_elpased: 1.39
batch start
#iterations: 73
currently lose_sum: 522.5929682850838
time_elpased: 1.39
batch start
#iterations: 74
currently lose_sum: 523.0611433982849
time_elpased: 1.395
batch start
#iterations: 75
currently lose_sum: 523.2044358551502
time_elpased: 1.389
batch start
#iterations: 76
currently lose_sum: 524.3783853948116
time_elpased: 1.39
batch start
#iterations: 77
currently lose_sum: 523.448045104742
time_elpased: 1.387
batch start
#iterations: 78
currently lose_sum: 523.8239324390888
time_elpased: 1.392
batch start
#iterations: 79
currently lose_sum: 524.6014830172062
time_elpased: 1.392
start validation test
0.542010309278
0.660990712074
0.175774415972
0.277700999919
0
validation finish
batch start
#iterations: 80
currently lose_sum: 522.3503133058548
time_elpased: 1.397
batch start
#iterations: 81
currently lose_sum: 523.8680802583694
time_elpased: 1.397
batch start
#iterations: 82
currently lose_sum: 523.4424887001514
time_elpased: 1.428
batch start
#iterations: 83
currently lose_sum: 523.3598589301109
time_elpased: 1.404
batch start
#iterations: 84
currently lose_sum: 524.0041656792164
time_elpased: 1.398
batch start
#iterations: 85
currently lose_sum: 521.3833817541599
time_elpased: 1.395
batch start
#iterations: 86
currently lose_sum: 522.4139648675919
time_elpased: 1.403
batch start
#iterations: 87
currently lose_sum: 521.1016988456249
time_elpased: 1.395
batch start
#iterations: 88
currently lose_sum: 522.5041479170322
time_elpased: 1.415
batch start
#iterations: 89
currently lose_sum: 521.7195817828178
time_elpased: 1.383
batch start
#iterations: 90
currently lose_sum: 523.1165373027325
time_elpased: 1.389
batch start
#iterations: 91
currently lose_sum: 520.7450450658798
time_elpased: 1.4
batch start
#iterations: 92
currently lose_sum: 523.3043472766876
time_elpased: 1.39
batch start
#iterations: 93
currently lose_sum: 522.3253605365753
time_elpased: 1.398
batch start
#iterations: 94
currently lose_sum: 521.3604212105274
time_elpased: 1.405
batch start
#iterations: 95
currently lose_sum: 521.0267499983311
time_elpased: 1.401
batch start
#iterations: 96
currently lose_sum: 523.5263105630875
time_elpased: 1.395
batch start
#iterations: 97
currently lose_sum: 521.2415433228016
time_elpased: 1.407
batch start
#iterations: 98
currently lose_sum: 522.248180180788
time_elpased: 1.393
batch start
#iterations: 99
currently lose_sum: 521.8866807222366
time_elpased: 1.387
start validation test
0.546958762887
0.662692847125
0.19450447669
0.300739915666
0
validation finish
batch start
#iterations: 100
currently lose_sum: 522.6737940311432
time_elpased: 1.392
batch start
#iterations: 101
currently lose_sum: 520.7592905461788
time_elpased: 1.391
batch start
#iterations: 102
currently lose_sum: 519.8728892505169
time_elpased: 1.402
batch start
#iterations: 103
currently lose_sum: 522.0915541052818
time_elpased: 1.388
batch start
#iterations: 104
currently lose_sum: 521.2720718979836
time_elpased: 1.41
batch start
#iterations: 105
currently lose_sum: 521.8354907631874
time_elpased: 1.4
batch start
#iterations: 106
currently lose_sum: 522.2439562380314
time_elpased: 1.393
batch start
#iterations: 107
currently lose_sum: 521.8409790098667
time_elpased: 1.398
batch start
#iterations: 108
currently lose_sum: 519.587608397007
time_elpased: 1.406
batch start
#iterations: 109
currently lose_sum: 521.9238163530827
time_elpased: 1.396
batch start
#iterations: 110
currently lose_sum: 520.884107530117
time_elpased: 1.419
batch start
#iterations: 111
currently lose_sum: 524.1837475597858
time_elpased: 1.401
batch start
#iterations: 112
currently lose_sum: 519.378428965807
time_elpased: 1.386
batch start
#iterations: 113
currently lose_sum: 521.8227088451385
time_elpased: 1.4
batch start
#iterations: 114
currently lose_sum: 520.0317638814449
time_elpased: 1.418
batch start
#iterations: 115
currently lose_sum: 521.2898366749287
time_elpased: 1.411
batch start
#iterations: 116
currently lose_sum: 519.8465088903904
time_elpased: 1.393
batch start
#iterations: 117
currently lose_sum: 521.4166143238544
time_elpased: 1.395
batch start
#iterations: 118
currently lose_sum: 519.8785335719585
time_elpased: 1.41
batch start
#iterations: 119
currently lose_sum: 520.1955764293671
time_elpased: 1.388
start validation test
0.570103092784
0.678784731239
0.269013069878
0.385318396226
0
validation finish
batch start
#iterations: 120
currently lose_sum: 521.1988106966019
time_elpased: 1.404
batch start
#iterations: 121
currently lose_sum: 519.4544966220856
time_elpased: 1.398
batch start
#iterations: 122
currently lose_sum: 519.1687537133694
time_elpased: 1.397
batch start
#iterations: 123
currently lose_sum: 518.8202360272408
time_elpased: 1.398
batch start
#iterations: 124
currently lose_sum: 519.602254152298
time_elpased: 1.38
batch start
#iterations: 125
currently lose_sum: 520.3467609882355
time_elpased: 1.405
batch start
#iterations: 126
currently lose_sum: 519.7550272941589
time_elpased: 1.388
batch start
#iterations: 127
currently lose_sum: 521.0364550948143
time_elpased: 1.404
batch start
#iterations: 128
currently lose_sum: 518.8503057360649
time_elpased: 1.409
batch start
#iterations: 129
currently lose_sum: 520.1765452325344
time_elpased: 1.406
batch start
#iterations: 130
currently lose_sum: 520.6246420741081
time_elpased: 1.397
batch start
#iterations: 131
currently lose_sum: 519.9725626409054
time_elpased: 1.4
batch start
#iterations: 132
currently lose_sum: 521.2821189463139
time_elpased: 1.406
batch start
#iterations: 133
currently lose_sum: 520.2226786613464
time_elpased: 1.393
batch start
#iterations: 134
currently lose_sum: 520.5493130683899
time_elpased: 1.393
batch start
#iterations: 135
currently lose_sum: 517.6575539410114
time_elpased: 1.397
batch start
#iterations: 136
currently lose_sum: 519.2148567140102
time_elpased: 1.407
batch start
#iterations: 137
currently lose_sum: 518.8480115830898
time_elpased: 1.402
batch start
#iterations: 138
currently lose_sum: 520.1257594823837
time_elpased: 1.39
batch start
#iterations: 139
currently lose_sum: 520.5348971486092
time_elpased: 1.415
start validation test
0.531855670103
0.650402652771
0.14129875476
0.23216097396
0
validation finish
batch start
#iterations: 140
currently lose_sum: 517.0586583614349
time_elpased: 1.399
batch start
#iterations: 141
currently lose_sum: 521.1262358427048
time_elpased: 1.397
batch start
#iterations: 142
currently lose_sum: 519.694514721632
time_elpased: 1.404
batch start
#iterations: 143
currently lose_sum: 520.2546651959419
time_elpased: 1.399
batch start
#iterations: 144
currently lose_sum: 520.6739203631878
time_elpased: 1.39
batch start
#iterations: 145
currently lose_sum: 521.1001352667809
time_elpased: 1.394
batch start
#iterations: 146
currently lose_sum: 522.0743338465691
time_elpased: 1.404
batch start
#iterations: 147
currently lose_sum: 519.5423267781734
time_elpased: 1.389
batch start
#iterations: 148
currently lose_sum: 517.9616070389748
time_elpased: 1.396
batch start
#iterations: 149
currently lose_sum: 520.6794304549694
time_elpased: 1.388
batch start
#iterations: 150
currently lose_sum: 518.3044504523277
time_elpased: 1.401
batch start
#iterations: 151
currently lose_sum: 519.3594377040863
time_elpased: 1.389
batch start
#iterations: 152
currently lose_sum: 520.0320504009724
time_elpased: 1.381
batch start
#iterations: 153
currently lose_sum: 516.9354794025421
time_elpased: 1.39
batch start
#iterations: 154
currently lose_sum: 522.2797987759113
time_elpased: 1.394
batch start
#iterations: 155
currently lose_sum: 518.0109203159809
time_elpased: 1.388
batch start
#iterations: 156
currently lose_sum: 520.1698135733604
time_elpased: 1.416
batch start
#iterations: 157
currently lose_sum: 518.7960620522499
time_elpased: 1.393
batch start
#iterations: 158
currently lose_sum: 517.7122013866901
time_elpased: 1.387
batch start
#iterations: 159
currently lose_sum: 519.8640885055065
time_elpased: 1.403
start validation test
0.527319587629
0.641489912054
0.127611402696
0.212875536481
0
validation finish
batch start
#iterations: 160
currently lose_sum: 519.4022660255432
time_elpased: 1.399
batch start
#iterations: 161
currently lose_sum: 519.5733381509781
time_elpased: 1.424
batch start
#iterations: 162
currently lose_sum: 518.4306800365448
time_elpased: 1.39
batch start
#iterations: 163
currently lose_sum: 516.6288189589977
time_elpased: 1.395
batch start
#iterations: 164
currently lose_sum: 521.1556646227837
time_elpased: 1.4
batch start
#iterations: 165
currently lose_sum: 518.3796010315418
time_elpased: 1.392
batch start
#iterations: 166
currently lose_sum: 518.5360688269138
time_elpased: 1.398
batch start
#iterations: 167
currently lose_sum: 519.9121560454369
time_elpased: 1.393
batch start
#iterations: 168
currently lose_sum: 519.1540638506413
time_elpased: 1.402
batch start
#iterations: 169
currently lose_sum: 520.9983253777027
time_elpased: 1.396
batch start
#iterations: 170
currently lose_sum: 519.0803954899311
time_elpased: 1.394
batch start
#iterations: 171
currently lose_sum: 518.311759263277
time_elpased: 1.395
batch start
#iterations: 172
currently lose_sum: 521.06949827075
time_elpased: 1.396
batch start
#iterations: 173
currently lose_sum: 518.8097975850105
time_elpased: 1.408
batch start
#iterations: 174
currently lose_sum: 520.068416595459
time_elpased: 1.393
batch start
#iterations: 175
currently lose_sum: 519.3760235607624
time_elpased: 1.405
batch start
#iterations: 176
currently lose_sum: 518.4434837400913
time_elpased: 1.399
batch start
#iterations: 177
currently lose_sum: 519.4950585067272
time_elpased: 1.402
batch start
#iterations: 178
currently lose_sum: 517.7344482839108
time_elpased: 1.389
batch start
#iterations: 179
currently lose_sum: 519.0088355541229
time_elpased: 1.404
start validation test
0.545309278351
0.661035226456
0.189255943192
0.294263541083
0
validation finish
batch start
#iterations: 180
currently lose_sum: 518.7469190955162
time_elpased: 1.401
batch start
#iterations: 181
currently lose_sum: 520.6479342579842
time_elpased: 1.393
batch start
#iterations: 182
currently lose_sum: 517.4956639111042
time_elpased: 1.402
batch start
#iterations: 183
currently lose_sum: 519.2721350193024
time_elpased: 1.39
batch start
#iterations: 184
currently lose_sum: 518.7234415113926
time_elpased: 1.421
batch start
#iterations: 185
currently lose_sum: 516.9335647523403
time_elpased: 1.435
batch start
#iterations: 186
currently lose_sum: 520.4381977021694
time_elpased: 1.396
batch start
#iterations: 187
currently lose_sum: 517.6909123063087
time_elpased: 1.401
batch start
#iterations: 188
currently lose_sum: 517.4234215319157
time_elpased: 1.39
batch start
#iterations: 189
currently lose_sum: 518.1138169765472
time_elpased: 1.426
batch start
#iterations: 190
currently lose_sum: 516.1875886917114
time_elpased: 1.399
batch start
#iterations: 191
currently lose_sum: 519.338951587677
time_elpased: 1.411
batch start
#iterations: 192
currently lose_sum: 518.6769585311413
time_elpased: 1.408
batch start
#iterations: 193
currently lose_sum: 518.6252661049366
time_elpased: 1.391
batch start
#iterations: 194
currently lose_sum: 518.6949483454227
time_elpased: 1.397
batch start
#iterations: 195
currently lose_sum: 519.1492686271667
time_elpased: 1.395
batch start
#iterations: 196
currently lose_sum: 517.096078902483
time_elpased: 1.388
batch start
#iterations: 197
currently lose_sum: 519.7859167456627
time_elpased: 1.391
batch start
#iterations: 198
currently lose_sum: 518.7897822260857
time_elpased: 1.385
batch start
#iterations: 199
currently lose_sum: 518.0010650455952
time_elpased: 1.418
start validation test
0.546030927835
0.661691542289
0.191622928888
0.297182986194
0
validation finish
batch start
#iterations: 200
currently lose_sum: 520.3382018506527
time_elpased: 1.402
batch start
#iterations: 201
currently lose_sum: 518.3678996860981
time_elpased: 1.409
batch start
#iterations: 202
currently lose_sum: 518.6415109932423
time_elpased: 1.391
batch start
#iterations: 203
currently lose_sum: 517.2123303711414
time_elpased: 1.416
batch start
#iterations: 204
currently lose_sum: 518.8384403586388
time_elpased: 1.403
batch start
#iterations: 205
currently lose_sum: 519.5473555922508
time_elpased: 1.402
batch start
#iterations: 206
currently lose_sum: 519.3593899607658
time_elpased: 1.405
batch start
#iterations: 207
currently lose_sum: 517.048563182354
time_elpased: 1.411
batch start
#iterations: 208
currently lose_sum: 518.2226277291775
time_elpased: 1.396
batch start
#iterations: 209
currently lose_sum: 517.4707753062248
time_elpased: 1.399
batch start
#iterations: 210
currently lose_sum: 519.6787869632244
time_elpased: 1.407
batch start
#iterations: 211
currently lose_sum: 519.3337216377258
time_elpased: 1.399
batch start
#iterations: 212
currently lose_sum: 518.4268244802952
time_elpased: 1.409
batch start
#iterations: 213
currently lose_sum: 517.1847257912159
time_elpased: 1.395
batch start
#iterations: 214
currently lose_sum: 519.0670498609543
time_elpased: 1.4
batch start
#iterations: 215
currently lose_sum: 519.0788505077362
time_elpased: 1.41
batch start
#iterations: 216
currently lose_sum: 518.4902248978615
time_elpased: 1.394
batch start
#iterations: 217
currently lose_sum: 517.9470301568508
time_elpased: 1.391
batch start
#iterations: 218
currently lose_sum: 519.5688008069992
time_elpased: 1.404
batch start
#iterations: 219
currently lose_sum: 519.1534367203712
time_elpased: 1.385
start validation test
0.525824742268
0.641840087623
0.120613358032
0.203066793728
0
validation finish
batch start
#iterations: 220
currently lose_sum: 516.8872060775757
time_elpased: 1.395
batch start
#iterations: 221
currently lose_sum: 519.1672605276108
time_elpased: 1.397
batch start
#iterations: 222
currently lose_sum: 517.6038105487823
time_elpased: 1.391
batch start
#iterations: 223
currently lose_sum: 518.594286352396
time_elpased: 1.393
batch start
#iterations: 224
currently lose_sum: 517.4255023598671
time_elpased: 1.384
batch start
#iterations: 225
currently lose_sum: 520.239433735609
time_elpased: 1.424
batch start
#iterations: 226
currently lose_sum: 519.0333596169949
time_elpased: 1.382
batch start
#iterations: 227
currently lose_sum: 516.3558288514614
time_elpased: 1.402
batch start
#iterations: 228
currently lose_sum: 515.7472960352898
time_elpased: 1.398
batch start
#iterations: 229
currently lose_sum: 517.8063307404518
time_elpased: 1.393
batch start
#iterations: 230
currently lose_sum: 519.3046742975712
time_elpased: 1.386
batch start
#iterations: 231
currently lose_sum: 520.606884598732
time_elpased: 1.395
batch start
#iterations: 232
currently lose_sum: 516.9069023728371
time_elpased: 1.402
batch start
#iterations: 233
currently lose_sum: 516.1017041802406
time_elpased: 1.399
batch start
#iterations: 234
currently lose_sum: 516.2754387557507
time_elpased: 1.395
batch start
#iterations: 235
currently lose_sum: 517.3482954204082
time_elpased: 1.394
batch start
#iterations: 236
currently lose_sum: 516.6086653769016
time_elpased: 1.402
batch start
#iterations: 237
currently lose_sum: 517.7102518677711
time_elpased: 1.395
batch start
#iterations: 238
currently lose_sum: 516.2093823552132
time_elpased: 1.384
batch start
#iterations: 239
currently lose_sum: 516.712016582489
time_elpased: 1.393
start validation test
0.515670103093
0.617754952311
0.0866522589277
0.151985559567
0
validation finish
batch start
#iterations: 240
currently lose_sum: 516.3185067474842
time_elpased: 1.418
batch start
#iterations: 241
currently lose_sum: 518.983207821846
time_elpased: 1.402
batch start
#iterations: 242
currently lose_sum: 516.1881835758686
time_elpased: 1.401
batch start
#iterations: 243
currently lose_sum: 516.1150238513947
time_elpased: 1.398
batch start
#iterations: 244
currently lose_sum: 516.9124833643436
time_elpased: 1.392
batch start
#iterations: 245
currently lose_sum: 517.9367789626122
time_elpased: 1.402
batch start
#iterations: 246
currently lose_sum: 517.443962007761
time_elpased: 1.397
batch start
#iterations: 247
currently lose_sum: 517.9345152378082
time_elpased: 1.399
batch start
#iterations: 248
currently lose_sum: 517.3732234239578
time_elpased: 1.394
batch start
#iterations: 249
currently lose_sum: 518.3143690228462
time_elpased: 1.394
batch start
#iterations: 250
currently lose_sum: 517.0298772454262
time_elpased: 1.387
batch start
#iterations: 251
currently lose_sum: 518.8671960532665
time_elpased: 1.396
batch start
#iterations: 252
currently lose_sum: 517.92165979743
time_elpased: 1.398
batch start
#iterations: 253
currently lose_sum: 516.8398619890213
time_elpased: 1.419
batch start
#iterations: 254
currently lose_sum: 518.5798509418964
time_elpased: 1.421
batch start
#iterations: 255
currently lose_sum: 516.3855500519276
time_elpased: 1.397
batch start
#iterations: 256
currently lose_sum: 516.77469009161
time_elpased: 1.386
batch start
#iterations: 257
currently lose_sum: 515.7073948383331
time_elpased: 1.386
batch start
#iterations: 258
currently lose_sum: 518.2306009829044
time_elpased: 1.414
batch start
#iterations: 259
currently lose_sum: 518.1106612384319
time_elpased: 1.395
start validation test
0.53381443299
0.649356413671
0.150560872697
0.244444444444
0
validation finish
batch start
#iterations: 260
currently lose_sum: 520.1905519664288
time_elpased: 1.403
batch start
#iterations: 261
currently lose_sum: 519.0562955141068
time_elpased: 1.411
batch start
#iterations: 262
currently lose_sum: 517.5207912623882
time_elpased: 1.39
batch start
#iterations: 263
currently lose_sum: 517.3945915699005
time_elpased: 1.402
batch start
#iterations: 264
currently lose_sum: 518.1624769866467
time_elpased: 1.393
batch start
#iterations: 265
currently lose_sum: 518.1563001275063
time_elpased: 1.402
batch start
#iterations: 266
currently lose_sum: 516.5657603144646
time_elpased: 1.387
batch start
#iterations: 267
currently lose_sum: 515.5525485277176
time_elpased: 1.408
batch start
#iterations: 268
currently lose_sum: 517.1192247867584
time_elpased: 1.388
batch start
#iterations: 269
currently lose_sum: 517.9146015644073
time_elpased: 1.398
batch start
#iterations: 270
currently lose_sum: 516.189742654562
time_elpased: 1.41
batch start
#iterations: 271
currently lose_sum: 515.8377668261528
time_elpased: 1.388
batch start
#iterations: 272
currently lose_sum: 517.696709305048
time_elpased: 1.391
batch start
#iterations: 273
currently lose_sum: 515.3162424564362
time_elpased: 1.406
batch start
#iterations: 274
currently lose_sum: 517.0845214724541
time_elpased: 1.392
batch start
#iterations: 275
currently lose_sum: 516.4472032487392
time_elpased: 1.394
batch start
#iterations: 276
currently lose_sum: 517.5682951807976
time_elpased: 1.399
batch start
#iterations: 277
currently lose_sum: 517.4906356036663
time_elpased: 1.399
batch start
#iterations: 278
currently lose_sum: 517.4686700999737
time_elpased: 1.39
batch start
#iterations: 279
currently lose_sum: 519.2103715837002
time_elpased: 1.383
start validation test
0.532164948454
0.645615629259
0.146238550993
0.238462829334
0
validation finish
batch start
#iterations: 280
currently lose_sum: 515.3449524641037
time_elpased: 1.383
batch start
#iterations: 281
currently lose_sum: 515.7156533896923
time_elpased: 1.411
batch start
#iterations: 282
currently lose_sum: 518.0607800781727
time_elpased: 1.39
batch start
#iterations: 283
currently lose_sum: 516.890263646841
time_elpased: 1.413
batch start
#iterations: 284
currently lose_sum: 518.0673860013485
time_elpased: 2.365
batch start
#iterations: 285
currently lose_sum: 517.0850553512573
time_elpased: 2.314
batch start
#iterations: 286
currently lose_sum: 518.6197988390923
time_elpased: 2.357
batch start
#iterations: 287
currently lose_sum: 517.7363716959953
time_elpased: 2.4
batch start
#iterations: 288
currently lose_sum: 517.8752718269825
time_elpased: 2.017
batch start
#iterations: 289
currently lose_sum: 517.5006732344627
time_elpased: 1.402
batch start
#iterations: 290
currently lose_sum: 516.0721361637115
time_elpased: 1.38
batch start
#iterations: 291
currently lose_sum: 517.622361689806
time_elpased: 1.38
batch start
#iterations: 292
currently lose_sum: 517.8178659975529
time_elpased: 1.384
batch start
#iterations: 293
currently lose_sum: 517.6563867330551
time_elpased: 1.388
batch start
#iterations: 294
currently lose_sum: 518.6454282701015
time_elpased: 1.397
batch start
#iterations: 295
currently lose_sum: 517.2146497666836
time_elpased: 1.386
batch start
#iterations: 296
currently lose_sum: 515.552737057209
time_elpased: 1.397
batch start
#iterations: 297
currently lose_sum: 518.2851582467556
time_elpased: 1.399
batch start
#iterations: 298
currently lose_sum: 516.318907648325
time_elpased: 1.38
batch start
#iterations: 299
currently lose_sum: 517.6136205792427
time_elpased: 1.381
start validation test
0.557319587629
0.673638880345
0.225378203149
0.337754472548
0
validation finish
batch start
#iterations: 300
currently lose_sum: 516.6031496822834
time_elpased: 1.39
batch start
#iterations: 301
currently lose_sum: 517.1801560521126
time_elpased: 1.384
batch start
#iterations: 302
currently lose_sum: 519.3425227999687
time_elpased: 1.398
batch start
#iterations: 303
currently lose_sum: 519.9709962010384
time_elpased: 1.387
batch start
#iterations: 304
currently lose_sum: 513.3187585175037
time_elpased: 1.386
batch start
#iterations: 305
currently lose_sum: 517.9626225233078
time_elpased: 1.389
batch start
#iterations: 306
currently lose_sum: 516.5558415055275
time_elpased: 1.396
batch start
#iterations: 307
currently lose_sum: 516.716206073761
time_elpased: 1.387
batch start
#iterations: 308
currently lose_sum: 516.8100213110447
time_elpased: 1.381
batch start
#iterations: 309
currently lose_sum: 517.8147624731064
time_elpased: 1.389
batch start
#iterations: 310
currently lose_sum: 516.6879161596298
time_elpased: 1.379
batch start
#iterations: 311
currently lose_sum: 515.2587205171585
time_elpased: 1.375
batch start
#iterations: 312
currently lose_sum: 517.0929942727089
time_elpased: 1.378
batch start
#iterations: 313
currently lose_sum: 518.5436384379864
time_elpased: 1.375
batch start
#iterations: 314
currently lose_sum: 516.0091060698032
time_elpased: 1.394
batch start
#iterations: 315
currently lose_sum: 516.7238962948322
time_elpased: 1.399
batch start
#iterations: 316
currently lose_sum: 516.715734988451
time_elpased: 1.399
batch start
#iterations: 317
currently lose_sum: 517.0611149072647
time_elpased: 1.406
batch start
#iterations: 318
currently lose_sum: 519.4968998134136
time_elpased: 1.381
batch start
#iterations: 319
currently lose_sum: 516.3722638487816
time_elpased: 1.397
start validation test
0.520773195876
0.629151291513
0.105279407224
0.180375562021
0
validation finish
batch start
#iterations: 320
currently lose_sum: 517.4417521953583
time_elpased: 1.375
batch start
#iterations: 321
currently lose_sum: 518.980318158865
time_elpased: 1.372
batch start
#iterations: 322
currently lose_sum: 516.3321098089218
time_elpased: 1.384
batch start
#iterations: 323
currently lose_sum: 515.0500616133213
time_elpased: 1.389
batch start
#iterations: 324
currently lose_sum: 519.7846530973911
time_elpased: 1.385
batch start
#iterations: 325
currently lose_sum: 515.684152930975
time_elpased: 1.394
batch start
#iterations: 326
currently lose_sum: 516.0255014896393
time_elpased: 1.396
batch start
#iterations: 327
currently lose_sum: 517.1497254669666
time_elpased: 1.38
batch start
#iterations: 328
currently lose_sum: 516.8899794518948
time_elpased: 1.388
batch start
#iterations: 329
currently lose_sum: 516.6623192131519
time_elpased: 1.389
batch start
#iterations: 330
currently lose_sum: 519.3160645067692
time_elpased: 1.395
batch start
#iterations: 331
currently lose_sum: 519.6378918886185
time_elpased: 1.391
batch start
#iterations: 332
currently lose_sum: 518.7107352912426
time_elpased: 1.38
batch start
#iterations: 333
currently lose_sum: 516.5890050828457
time_elpased: 1.388
batch start
#iterations: 334
currently lose_sum: 516.3892149329185
time_elpased: 1.372
batch start
#iterations: 335
currently lose_sum: 516.5837700664997
time_elpased: 1.378
batch start
#iterations: 336
currently lose_sum: 518.5080967247486
time_elpased: 1.397
batch start
#iterations: 337
currently lose_sum: 517.5947195291519
time_elpased: 1.39
batch start
#iterations: 338
currently lose_sum: 516.6323530972004
time_elpased: 1.376
batch start
#iterations: 339
currently lose_sum: 517.029502004385
time_elpased: 1.388
start validation test
0.544278350515
0.659041394336
0.186786045076
0.291075294684
0
validation finish
batch start
#iterations: 340
currently lose_sum: 517.4824145436287
time_elpased: 1.392
batch start
#iterations: 341
currently lose_sum: 517.7810056805611
time_elpased: 1.388
batch start
#iterations: 342
currently lose_sum: 519.1325916051865
time_elpased: 1.387
batch start
#iterations: 343
currently lose_sum: 518.2210353314877
time_elpased: 1.376
batch start
#iterations: 344
currently lose_sum: 518.1923609972
time_elpased: 1.38
batch start
#iterations: 345
currently lose_sum: 517.9430487751961
time_elpased: 1.387
batch start
#iterations: 346
currently lose_sum: 514.8138076663017
time_elpased: 1.395
batch start
#iterations: 347
currently lose_sum: 516.5577259063721
time_elpased: 1.384
batch start
#iterations: 348
currently lose_sum: 515.9607474803925
time_elpased: 1.398
batch start
#iterations: 349
currently lose_sum: 517.0400280058384
time_elpased: 1.406
batch start
#iterations: 350
currently lose_sum: 517.278116196394
time_elpased: 1.391
batch start
#iterations: 351
currently lose_sum: 516.0947530269623
time_elpased: 1.398
batch start
#iterations: 352
currently lose_sum: 516.5528067946434
time_elpased: 1.382
batch start
#iterations: 353
currently lose_sum: 517.8837874531746
time_elpased: 1.378
batch start
#iterations: 354
currently lose_sum: 515.7056749761105
time_elpased: 1.391
batch start
#iterations: 355
currently lose_sum: 516.7380184829235
time_elpased: 1.387
batch start
#iterations: 356
currently lose_sum: 517.3829624950886
time_elpased: 1.388
batch start
#iterations: 357
currently lose_sum: 517.0333929955959
time_elpased: 1.391
batch start
#iterations: 358
currently lose_sum: 516.218343526125
time_elpased: 1.4
batch start
#iterations: 359
currently lose_sum: 516.3990980386734
time_elpased: 1.383
start validation test
0.537319587629
0.652657601978
0.163013275702
0.260869565217
0
validation finish
batch start
#iterations: 360
currently lose_sum: 517.9360418617725
time_elpased: 1.377
batch start
#iterations: 361
currently lose_sum: 515.1152769327164
time_elpased: 1.39
batch start
#iterations: 362
currently lose_sum: 514.6302761733532
time_elpased: 1.39
batch start
#iterations: 363
currently lose_sum: 517.1715176999569
time_elpased: 1.395
batch start
#iterations: 364
currently lose_sum: 517.6056550741196
time_elpased: 1.391
batch start
#iterations: 365
currently lose_sum: 516.2477198839188
time_elpased: 1.388
batch start
#iterations: 366
currently lose_sum: 515.9556941390038
time_elpased: 1.384
batch start
#iterations: 367
currently lose_sum: 518.1345912218094
time_elpased: 1.382
batch start
#iterations: 368
currently lose_sum: 514.1188915371895
time_elpased: 1.407
batch start
#iterations: 369
currently lose_sum: 516.1049168407917
time_elpased: 1.387
batch start
#iterations: 370
currently lose_sum: 515.5015734434128
time_elpased: 1.392
batch start
#iterations: 371
currently lose_sum: 517.9936651289463
time_elpased: 1.398
batch start
#iterations: 372
currently lose_sum: 516.6469672620296
time_elpased: 1.398
batch start
#iterations: 373
currently lose_sum: 517.4125179946423
time_elpased: 1.384
batch start
#iterations: 374
currently lose_sum: 518.443395704031
time_elpased: 1.404
batch start
#iterations: 375
currently lose_sum: 518.6630058586597
time_elpased: 1.394
batch start
#iterations: 376
currently lose_sum: 517.0071799755096
time_elpased: 1.382
batch start
#iterations: 377
currently lose_sum: 515.7547657489777
time_elpased: 1.387
batch start
#iterations: 378
currently lose_sum: 516.3583006262779
time_elpased: 1.389
batch start
#iterations: 379
currently lose_sum: 514.5829219818115
time_elpased: 1.392
start validation test
0.536701030928
0.653732602278
0.15951425337
0.256452680344
0
validation finish
batch start
#iterations: 380
currently lose_sum: 517.9202892184258
time_elpased: 1.387
batch start
#iterations: 381
currently lose_sum: 516.2685185968876
time_elpased: 1.393
batch start
#iterations: 382
currently lose_sum: 517.6162860989571
time_elpased: 1.373
batch start
#iterations: 383
currently lose_sum: 515.1743672192097
time_elpased: 1.397
batch start
#iterations: 384
currently lose_sum: 514.9502012431622
time_elpased: 1.404
batch start
#iterations: 385
currently lose_sum: 515.4027444124222
time_elpased: 1.377
batch start
#iterations: 386
currently lose_sum: 516.1596174240112
time_elpased: 1.396
batch start
#iterations: 387
currently lose_sum: 516.5505732297897
time_elpased: 1.38
batch start
#iterations: 388
currently lose_sum: 516.8034088611603
time_elpased: 1.396
batch start
#iterations: 389
currently lose_sum: 516.772048085928
time_elpased: 1.38
batch start
#iterations: 390
currently lose_sum: 517.0264136195183
time_elpased: 1.385
batch start
#iterations: 391
currently lose_sum: 517.028687030077
time_elpased: 1.4
batch start
#iterations: 392
currently lose_sum: 516.0861828327179
time_elpased: 1.378
batch start
#iterations: 393
currently lose_sum: 515.9093203544617
time_elpased: 1.393
batch start
#iterations: 394
currently lose_sum: 516.8222661912441
time_elpased: 1.378
batch start
#iterations: 395
currently lose_sum: 515.3402259349823
time_elpased: 1.396
batch start
#iterations: 396
currently lose_sum: 517.2810861766338
time_elpased: 1.385
batch start
#iterations: 397
currently lose_sum: 518.1592162847519
time_elpased: 1.389
batch start
#iterations: 398
currently lose_sum: 516.2344750463963
time_elpased: 1.385
batch start
#iterations: 399
currently lose_sum: 515.7666678726673
time_elpased: 1.395
start validation test
0.53793814433
0.654620123203
0.164042399918
0.262343647136
0
validation finish
acc: 0.575
pre: 0.688
rec: 0.277
F1: 0.395
auc: 0.000
