start to construct graph
graph construct over
87453
epochs start
batch start
#iterations: 0
currently lose_sum: 556.2621031403542
time_elpased: 1.461
batch start
#iterations: 1
currently lose_sum: 555.9520964622498
time_elpased: 1.405
batch start
#iterations: 2
currently lose_sum: 555.4314681887627
time_elpased: 1.396
batch start
#iterations: 3
currently lose_sum: 555.2128466963768
time_elpased: 1.384
batch start
#iterations: 4
currently lose_sum: 550.7714598178864
time_elpased: 1.382
batch start
#iterations: 5
currently lose_sum: 551.4503127932549
time_elpased: 1.391
batch start
#iterations: 6
currently lose_sum: 550.1797358393669
time_elpased: 1.387
batch start
#iterations: 7
currently lose_sum: 548.7626261711121
time_elpased: 1.406
batch start
#iterations: 8
currently lose_sum: 549.6513254642487
time_elpased: 1.4
batch start
#iterations: 9
currently lose_sum: 546.7999144792557
time_elpased: 1.392
batch start
#iterations: 10
currently lose_sum: 546.7434902787209
time_elpased: 1.407
batch start
#iterations: 11
currently lose_sum: 547.1497959494591
time_elpased: 1.412
batch start
#iterations: 12
currently lose_sum: 547.0819361209869
time_elpased: 1.387
batch start
#iterations: 13
currently lose_sum: 542.9426574707031
time_elpased: 1.393
batch start
#iterations: 14
currently lose_sum: 544.2228965163231
time_elpased: 1.392
batch start
#iterations: 15
currently lose_sum: 542.1004420518875
time_elpased: 1.398
batch start
#iterations: 16
currently lose_sum: 543.1452361941338
time_elpased: 1.388
batch start
#iterations: 17
currently lose_sum: 541.2093713283539
time_elpased: 1.41
batch start
#iterations: 18
currently lose_sum: 540.5894855856895
time_elpased: 1.407
batch start
#iterations: 19
currently lose_sum: 540.0783728957176
time_elpased: 1.396
start validation test
0.515
0.654929577465
0.0669959864156
0.121557277565
0
validation finish
batch start
#iterations: 20
currently lose_sum: 540.6690112948418
time_elpased: 1.402
batch start
#iterations: 21
currently lose_sum: 539.3561109006405
time_elpased: 1.394
batch start
#iterations: 22
currently lose_sum: 539.4753354787827
time_elpased: 1.406
batch start
#iterations: 23
currently lose_sum: 536.3083842396736
time_elpased: 1.406
batch start
#iterations: 24
currently lose_sum: 537.2117640674114
time_elpased: 1.401
batch start
#iterations: 25
currently lose_sum: 536.3637502789497
time_elpased: 1.397
batch start
#iterations: 26
currently lose_sum: 534.0070990025997
time_elpased: 1.383
batch start
#iterations: 27
currently lose_sum: 535.9192159771919
time_elpased: 1.381
batch start
#iterations: 28
currently lose_sum: 534.234520971775
time_elpased: 1.38
batch start
#iterations: 29
currently lose_sum: 532.0922163724899
time_elpased: 1.4
batch start
#iterations: 30
currently lose_sum: 532.6059589982033
time_elpased: 1.383
batch start
#iterations: 31
currently lose_sum: 534.519848048687
time_elpased: 1.413
batch start
#iterations: 32
currently lose_sum: 533.0265010297298
time_elpased: 1.401
batch start
#iterations: 33
currently lose_sum: 532.9641475081444
time_elpased: 1.415
batch start
#iterations: 34
currently lose_sum: 532.8289442062378
time_elpased: 1.404
batch start
#iterations: 35
currently lose_sum: 531.6021190881729
time_elpased: 1.395
batch start
#iterations: 36
currently lose_sum: 532.8740427494049
time_elpased: 1.407
batch start
#iterations: 37
currently lose_sum: 530.3031850755215
time_elpased: 1.39
batch start
#iterations: 38
currently lose_sum: 531.5617228150368
time_elpased: 1.39
batch start
#iterations: 39
currently lose_sum: 532.0904776453972
time_elpased: 1.386
start validation test
0.553917525773
0.681709401709
0.205207368529
0.315456415124
0
validation finish
batch start
#iterations: 40
currently lose_sum: 531.9800890982151
time_elpased: 1.381
batch start
#iterations: 41
currently lose_sum: 531.4668644964695
time_elpased: 1.391
batch start
#iterations: 42
currently lose_sum: 531.3149798810482
time_elpased: 1.393
batch start
#iterations: 43
currently lose_sum: 531.5523233115673
time_elpased: 1.4
batch start
#iterations: 44
currently lose_sum: 529.9743067324162
time_elpased: 1.41
batch start
#iterations: 45
currently lose_sum: 529.1424807310104
time_elpased: 1.404
batch start
#iterations: 46
currently lose_sum: 532.0265164077282
time_elpased: 1.387
batch start
#iterations: 47
currently lose_sum: 529.9401278197765
time_elpased: 1.404
batch start
#iterations: 48
currently lose_sum: 527.9961532354355
time_elpased: 1.397
batch start
#iterations: 49
currently lose_sum: 527.5721212923527
time_elpased: 1.397
batch start
#iterations: 50
currently lose_sum: 526.5855211615562
time_elpased: 1.395
batch start
#iterations: 51
currently lose_sum: 526.6334560215473
time_elpased: 1.402
batch start
#iterations: 52
currently lose_sum: 530.3195748329163
time_elpased: 1.41
batch start
#iterations: 53
currently lose_sum: 527.9749772846699
time_elpased: 1.392
batch start
#iterations: 54
currently lose_sum: 529.1318469941616
time_elpased: 1.414
batch start
#iterations: 55
currently lose_sum: 526.66888281703
time_elpased: 1.404
batch start
#iterations: 56
currently lose_sum: 527.3014089465141
time_elpased: 1.4
batch start
#iterations: 57
currently lose_sum: 527.9886283576488
time_elpased: 1.411
batch start
#iterations: 58
currently lose_sum: 526.4776526689529
time_elpased: 1.406
batch start
#iterations: 59
currently lose_sum: 525.6422272622585
time_elpased: 1.408
start validation test
0.515309278351
0.646455223881
0.0713183081198
0.128464176476
0
validation finish
batch start
#iterations: 60
currently lose_sum: 525.8829587101936
time_elpased: 1.398
batch start
#iterations: 61
currently lose_sum: 524.8438584208488
time_elpased: 1.404
batch start
#iterations: 62
currently lose_sum: 526.1817381381989
time_elpased: 1.394
batch start
#iterations: 63
currently lose_sum: 527.4778337478638
time_elpased: 1.396
batch start
#iterations: 64
currently lose_sum: 524.3065474927425
time_elpased: 1.387
batch start
#iterations: 65
currently lose_sum: 527.3380452394485
time_elpased: 1.385
batch start
#iterations: 66
currently lose_sum: 526.8650382459164
time_elpased: 1.398
batch start
#iterations: 67
currently lose_sum: 523.7713724374771
time_elpased: 1.413
batch start
#iterations: 68
currently lose_sum: 524.2872714400291
time_elpased: 1.406
batch start
#iterations: 69
currently lose_sum: 523.9363104403019
time_elpased: 1.39
batch start
#iterations: 70
currently lose_sum: 527.1429837942123
time_elpased: 1.399
batch start
#iterations: 71
currently lose_sum: 525.1489537656307
time_elpased: 1.398
batch start
#iterations: 72
currently lose_sum: 523.8860115408897
time_elpased: 1.398
batch start
#iterations: 73
currently lose_sum: 524.9400755465031
time_elpased: 1.39
batch start
#iterations: 74
currently lose_sum: 523.7574929594994
time_elpased: 1.393
batch start
#iterations: 75
currently lose_sum: 523.8499858379364
time_elpased: 1.413
batch start
#iterations: 76
currently lose_sum: 524.1041025817394
time_elpased: 1.389
batch start
#iterations: 77
currently lose_sum: 523.4927254617214
time_elpased: 1.412
batch start
#iterations: 78
currently lose_sum: 524.0904379487038
time_elpased: 1.429
batch start
#iterations: 79
currently lose_sum: 524.7119360864162
time_elpased: 1.396
start validation test
0.569484536082
0.681660899654
0.263558711536
0.38013952798
0
validation finish
batch start
#iterations: 80
currently lose_sum: 524.698514431715
time_elpased: 1.41
batch start
#iterations: 81
currently lose_sum: 524.9361416995525
time_elpased: 1.41
batch start
#iterations: 82
currently lose_sum: 522.7241690158844
time_elpased: 1.402
batch start
#iterations: 83
currently lose_sum: 521.4691934883595
time_elpased: 1.401
batch start
#iterations: 84
currently lose_sum: 522.468245446682
time_elpased: 1.411
batch start
#iterations: 85
currently lose_sum: 521.9206091165543
time_elpased: 1.39
batch start
#iterations: 86
currently lose_sum: 523.4192287921906
time_elpased: 1.386
batch start
#iterations: 87
currently lose_sum: 523.3185538649559
time_elpased: 1.395
batch start
#iterations: 88
currently lose_sum: 524.6483002305031
time_elpased: 1.404
batch start
#iterations: 89
currently lose_sum: 521.8622897565365
time_elpased: 1.39
batch start
#iterations: 90
currently lose_sum: 523.1891493201256
time_elpased: 1.386
batch start
#iterations: 91
currently lose_sum: 521.6717116832733
time_elpased: 1.392
batch start
#iterations: 92
currently lose_sum: 521.5653382837772
time_elpased: 1.407
batch start
#iterations: 93
currently lose_sum: 523.2337435185909
time_elpased: 1.391
batch start
#iterations: 94
currently lose_sum: 521.7427284121513
time_elpased: 1.401
batch start
#iterations: 95
currently lose_sum: 522.3746877908707
time_elpased: 1.411
batch start
#iterations: 96
currently lose_sum: 521.6713424324989
time_elpased: 1.42
batch start
#iterations: 97
currently lose_sum: 523.0509835481644
time_elpased: 1.393
batch start
#iterations: 98
currently lose_sum: 518.8761595189571
time_elpased: 1.384
batch start
#iterations: 99
currently lose_sum: 524.2921376824379
time_elpased: 1.397
start validation test
0.534226804124
0.659484777518
0.144900689513
0.237597030037
0
validation finish
batch start
#iterations: 100
currently lose_sum: 522.037367939949
time_elpased: 1.427
batch start
#iterations: 101
currently lose_sum: 520.3727510273457
time_elpased: 1.404
batch start
#iterations: 102
currently lose_sum: 524.4844987094402
time_elpased: 1.396
batch start
#iterations: 103
currently lose_sum: 520.7031346559525
time_elpased: 1.402
batch start
#iterations: 104
currently lose_sum: 520.164892911911
time_elpased: 1.396
batch start
#iterations: 105
currently lose_sum: 520.8727924227715
time_elpased: 1.409
batch start
#iterations: 106
currently lose_sum: 521.7040957212448
time_elpased: 1.402
batch start
#iterations: 107
currently lose_sum: 520.8716199994087
time_elpased: 1.391
batch start
#iterations: 108
currently lose_sum: 520.5849391520023
time_elpased: 1.393
batch start
#iterations: 109
currently lose_sum: 523.7393158972263
time_elpased: 1.411
batch start
#iterations: 110
currently lose_sum: 521.8922136127949
time_elpased: 1.395
batch start
#iterations: 111
currently lose_sum: 523.673386156559
time_elpased: 1.398
batch start
#iterations: 112
currently lose_sum: 521.4192430377007
time_elpased: 1.394
batch start
#iterations: 113
currently lose_sum: 521.4873174130917
time_elpased: 1.428
batch start
#iterations: 114
currently lose_sum: 521.5979609489441
time_elpased: 1.399
batch start
#iterations: 115
currently lose_sum: 520.360909819603
time_elpased: 1.394
batch start
#iterations: 116
currently lose_sum: 521.7697824239731
time_elpased: 1.406
batch start
#iterations: 117
currently lose_sum: 520.4924118816853
time_elpased: 1.407
batch start
#iterations: 118
currently lose_sum: 521.3399367928505
time_elpased: 1.42
batch start
#iterations: 119
currently lose_sum: 520.0291607379913
time_elpased: 1.398
start validation test
0.567886597938
0.680856832972
0.25841309046
0.374636329728
0
validation finish
batch start
#iterations: 120
currently lose_sum: 520.6852400302887
time_elpased: 1.417
batch start
#iterations: 121
currently lose_sum: 522.3931915163994
time_elpased: 1.403
batch start
#iterations: 122
currently lose_sum: 523.5409199595451
time_elpased: 1.407
batch start
#iterations: 123
currently lose_sum: 521.9574899673462
time_elpased: 1.406
batch start
#iterations: 124
currently lose_sum: 520.829679787159
time_elpased: 1.398
batch start
#iterations: 125
currently lose_sum: 519.2249283790588
time_elpased: 1.393
batch start
#iterations: 126
currently lose_sum: 519.2533875703812
time_elpased: 1.395
batch start
#iterations: 127
currently lose_sum: 521.5085342228413
time_elpased: 1.395
batch start
#iterations: 128
currently lose_sum: 523.1465227901936
time_elpased: 1.44
batch start
#iterations: 129
currently lose_sum: 519.4623722136021
time_elpased: 1.391
batch start
#iterations: 130
currently lose_sum: 519.7961394786835
time_elpased: 1.408
batch start
#iterations: 131
currently lose_sum: 520.7246136963367
time_elpased: 1.415
batch start
#iterations: 132
currently lose_sum: 519.3083078563213
time_elpased: 1.39
batch start
#iterations: 133
currently lose_sum: 520.5043176114559
time_elpased: 1.397
batch start
#iterations: 134
currently lose_sum: 519.751985013485
time_elpased: 1.396
batch start
#iterations: 135
currently lose_sum: 520.0812433362007
time_elpased: 1.39
batch start
#iterations: 136
currently lose_sum: 522.0774681270123
time_elpased: 1.398
batch start
#iterations: 137
currently lose_sum: 518.946111023426
time_elpased: 1.409
batch start
#iterations: 138
currently lose_sum: 521.6614652276039
time_elpased: 1.389
batch start
#iterations: 139
currently lose_sum: 521.7494388520718
time_elpased: 1.404
start validation test
0.533711340206
0.660603159406
0.14201914171
0.233779434186
0
validation finish
batch start
#iterations: 140
currently lose_sum: 519.5070330798626
time_elpased: 1.403
batch start
#iterations: 141
currently lose_sum: 519.8332955539227
time_elpased: 1.406
batch start
#iterations: 142
currently lose_sum: 521.3534691929817
time_elpased: 1.398
batch start
#iterations: 143
currently lose_sum: 520.3233150541782
time_elpased: 1.405
batch start
#iterations: 144
currently lose_sum: 519.2075423300266
time_elpased: 1.399
batch start
#iterations: 145
currently lose_sum: 520.3457828760147
time_elpased: 1.412
batch start
#iterations: 146
currently lose_sum: 520.1424970626831
time_elpased: 1.396
batch start
#iterations: 147
currently lose_sum: 518.0170797109604
time_elpased: 1.396
batch start
#iterations: 148
currently lose_sum: 519.523229598999
time_elpased: 1.402
batch start
#iterations: 149
currently lose_sum: 520.8449669778347
time_elpased: 1.408
batch start
#iterations: 150
currently lose_sum: 519.7722844183445
time_elpased: 1.398
batch start
#iterations: 151
currently lose_sum: 518.1356094181538
time_elpased: 1.396
batch start
#iterations: 152
currently lose_sum: 518.6535546779633
time_elpased: 1.396
batch start
#iterations: 153
currently lose_sum: 517.5935177505016
time_elpased: 1.395
batch start
#iterations: 154
currently lose_sum: 520.2550567090511
time_elpased: 1.401
batch start
#iterations: 155
currently lose_sum: 520.5789497494698
time_elpased: 1.392
batch start
#iterations: 156
currently lose_sum: 519.5358652770519
time_elpased: 1.407
batch start
#iterations: 157
currently lose_sum: 518.2521738111973
time_elpased: 1.388
batch start
#iterations: 158
currently lose_sum: 520.0348566770554
time_elpased: 1.411
batch start
#iterations: 159
currently lose_sum: 518.9086698889732
time_elpased: 1.397
start validation test
0.534020618557
0.659745162813
0.143871565298
0.236228455559
0
validation finish
batch start
#iterations: 160
currently lose_sum: 520.1037694811821
time_elpased: 1.425
batch start
#iterations: 161
currently lose_sum: 519.5522913336754
time_elpased: 1.399
batch start
#iterations: 162
currently lose_sum: 517.9507979154587
time_elpased: 1.385
batch start
#iterations: 163
currently lose_sum: 519.7723609209061
time_elpased: 1.4
batch start
#iterations: 164
currently lose_sum: 519.2128077447414
time_elpased: 1.407
batch start
#iterations: 165
currently lose_sum: 522.374369084835
time_elpased: 1.385
batch start
#iterations: 166
currently lose_sum: 520.060657531023
time_elpased: 1.404
batch start
#iterations: 167
currently lose_sum: 520.6793793439865
time_elpased: 1.415
batch start
#iterations: 168
currently lose_sum: 519.0574540793896
time_elpased: 1.398
batch start
#iterations: 169
currently lose_sum: 517.9130015671253
time_elpased: 1.398
batch start
#iterations: 170
currently lose_sum: 520.1918801963329
time_elpased: 1.407
batch start
#iterations: 171
currently lose_sum: 519.7202551662922
time_elpased: 1.418
batch start
#iterations: 172
currently lose_sum: 519.4778643846512
time_elpased: 1.399
batch start
#iterations: 173
currently lose_sum: 518.01069688797
time_elpased: 1.41
batch start
#iterations: 174
currently lose_sum: 520.9071126580238
time_elpased: 1.392
batch start
#iterations: 175
currently lose_sum: 520.3496752977371
time_elpased: 1.39
batch start
#iterations: 176
currently lose_sum: 521.0413321554661
time_elpased: 1.389
batch start
#iterations: 177
currently lose_sum: 520.037683725357
time_elpased: 1.396
batch start
#iterations: 178
currently lose_sum: 518.5760628581047
time_elpased: 1.396
batch start
#iterations: 179
currently lose_sum: 521.5936089158058
time_elpased: 1.399
start validation test
0.535721649485
0.660198555957
0.150560872697
0.245202379955
0
validation finish
batch start
#iterations: 180
currently lose_sum: 519.1281965076923
time_elpased: 1.395
batch start
#iterations: 181
currently lose_sum: 520.3840399384499
time_elpased: 1.386
batch start
#iterations: 182
currently lose_sum: 519.0284876525402
time_elpased: 1.389
batch start
#iterations: 183
currently lose_sum: 520.0062726140022
time_elpased: 1.4
batch start
#iterations: 184
currently lose_sum: 519.2821942567825
time_elpased: 1.402
batch start
#iterations: 185
currently lose_sum: 518.5559438765049
time_elpased: 1.401
batch start
#iterations: 186
currently lose_sum: 518.5391561985016
time_elpased: 1.411
batch start
#iterations: 187
currently lose_sum: 520.1555910110474
time_elpased: 1.412
batch start
#iterations: 188
currently lose_sum: 519.2372549474239
time_elpased: 1.397
batch start
#iterations: 189
currently lose_sum: 519.3196103572845
time_elpased: 1.421
batch start
#iterations: 190
currently lose_sum: 518.2062670588493
time_elpased: 1.412
batch start
#iterations: 191
currently lose_sum: 517.7031013071537
time_elpased: 1.396
batch start
#iterations: 192
currently lose_sum: 519.3372419178486
time_elpased: 1.392
batch start
#iterations: 193
currently lose_sum: 519.3681673109531
time_elpased: 1.416
batch start
#iterations: 194
currently lose_sum: 519.5272431969643
time_elpased: 1.413
batch start
#iterations: 195
currently lose_sum: 517.6817474961281
time_elpased: 1.386
batch start
#iterations: 196
currently lose_sum: 519.0527352690697
time_elpased: 1.399
batch start
#iterations: 197
currently lose_sum: 519.6127408444881
time_elpased: 1.408
batch start
#iterations: 198
currently lose_sum: 519.104296296835
time_elpased: 1.402
batch start
#iterations: 199
currently lose_sum: 520.9567985832691
time_elpased: 1.388
start validation test
0.539639175258
0.66253101737
0.16486569929
0.264029666255
0
validation finish
batch start
#iterations: 200
currently lose_sum: 514.5516228079796
time_elpased: 1.394
batch start
#iterations: 201
currently lose_sum: 519.6722132265568
time_elpased: 1.414
batch start
#iterations: 202
currently lose_sum: 518.0437723994255
time_elpased: 1.418
batch start
#iterations: 203
currently lose_sum: 519.9275968074799
time_elpased: 1.432
batch start
#iterations: 204
currently lose_sum: 518.3142372369766
time_elpased: 1.408
batch start
#iterations: 205
currently lose_sum: 518.317301005125
time_elpased: 1.412
batch start
#iterations: 206
currently lose_sum: 516.2743039727211
time_elpased: 1.42
batch start
#iterations: 207
currently lose_sum: 518.1261408925056
time_elpased: 1.401
batch start
#iterations: 208
currently lose_sum: 519.0368995666504
time_elpased: 1.395
batch start
#iterations: 209
currently lose_sum: 519.1127757430077
time_elpased: 1.408
batch start
#iterations: 210
currently lose_sum: 519.375526368618
time_elpased: 1.392
batch start
#iterations: 211
currently lose_sum: 518.6771494448185
time_elpased: 1.395
batch start
#iterations: 212
currently lose_sum: 517.9488004744053
time_elpased: 1.382
batch start
#iterations: 213
currently lose_sum: 519.5879365205765
time_elpased: 1.392
batch start
#iterations: 214
currently lose_sum: 519.2084524333477
time_elpased: 1.395
batch start
#iterations: 215
currently lose_sum: 517.8447526395321
time_elpased: 1.384
batch start
#iterations: 216
currently lose_sum: 520.1889440715313
time_elpased: 1.404
batch start
#iterations: 217
currently lose_sum: 518.843773573637
time_elpased: 1.395
batch start
#iterations: 218
currently lose_sum: 519.3186815679073
time_elpased: 1.391
batch start
#iterations: 219
currently lose_sum: 520.7569298744202
time_elpased: 1.401
start validation test
0.525927835052
0.658536585366
0.111145415252
0.190191071586
0
validation finish
batch start
#iterations: 220
currently lose_sum: 518.1157702207565
time_elpased: 1.394
batch start
#iterations: 221
currently lose_sum: 520.9424487948418
time_elpased: 1.41
batch start
#iterations: 222
currently lose_sum: 518.19962900877
time_elpased: 1.394
batch start
#iterations: 223
currently lose_sum: 519.1958107054234
time_elpased: 1.387
batch start
#iterations: 224
currently lose_sum: 520.6580615341663
time_elpased: 1.404
batch start
#iterations: 225
currently lose_sum: 519.0259363055229
time_elpased: 1.395
batch start
#iterations: 226
currently lose_sum: 518.0364937186241
time_elpased: 1.394
batch start
#iterations: 227
currently lose_sum: 518.1636704802513
time_elpased: 1.396
batch start
#iterations: 228
currently lose_sum: 516.6022325456142
time_elpased: 1.392
batch start
#iterations: 229
currently lose_sum: 516.7412714660168
time_elpased: 1.4
batch start
#iterations: 230
currently lose_sum: 518.2455574572086
time_elpased: 1.392
batch start
#iterations: 231
currently lose_sum: 518.7535846829414
time_elpased: 1.411
batch start
#iterations: 232
currently lose_sum: 518.7256668806076
time_elpased: 1.411
batch start
#iterations: 233
currently lose_sum: 518.0961411595345
time_elpased: 1.393
batch start
#iterations: 234
currently lose_sum: 520.1261204481125
time_elpased: 1.405
batch start
#iterations: 235
currently lose_sum: 516.780683785677
time_elpased: 1.392
batch start
#iterations: 236
currently lose_sum: 515.0986837744713
time_elpased: 1.398
batch start
#iterations: 237
currently lose_sum: 517.2540020942688
time_elpased: 1.426
batch start
#iterations: 238
currently lose_sum: 516.308724462986
time_elpased: 1.382
batch start
#iterations: 239
currently lose_sum: 519.5622871816158
time_elpased: 1.4
start validation test
0.555927835052
0.678086619263
0.215910260368
0.327531028023
0
validation finish
batch start
#iterations: 240
currently lose_sum: 517.2281791865826
time_elpased: 1.399
batch start
#iterations: 241
currently lose_sum: 519.3441188633442
time_elpased: 1.392
batch start
#iterations: 242
currently lose_sum: 519.1411149799824
time_elpased: 1.388
batch start
#iterations: 243
currently lose_sum: 516.8624256849289
time_elpased: 1.384
batch start
#iterations: 244
currently lose_sum: 520.144553810358
time_elpased: 1.412
batch start
#iterations: 245
currently lose_sum: 517.5391761958599
time_elpased: 1.418
batch start
#iterations: 246
currently lose_sum: 520.0366497039795
time_elpased: 1.395
batch start
#iterations: 247
currently lose_sum: 520.6181854605675
time_elpased: 1.42
batch start
#iterations: 248
currently lose_sum: 517.8526029288769
time_elpased: 1.432
batch start
#iterations: 249
currently lose_sum: 519.0358893871307
time_elpased: 1.401
batch start
#iterations: 250
currently lose_sum: 519.382919639349
time_elpased: 1.4
batch start
#iterations: 251
currently lose_sum: 516.6361662447453
time_elpased: 1.396
batch start
#iterations: 252
currently lose_sum: 516.3883352279663
time_elpased: 1.398
batch start
#iterations: 253
currently lose_sum: 516.7477311193943
time_elpased: 1.399
batch start
#iterations: 254
currently lose_sum: 516.5972526073456
time_elpased: 1.401
batch start
#iterations: 255
currently lose_sum: 517.2092642188072
time_elpased: 1.386
batch start
#iterations: 256
currently lose_sum: 517.7209274768829
time_elpased: 1.393
batch start
#iterations: 257
currently lose_sum: 515.2901983261108
time_elpased: 1.402
batch start
#iterations: 258
currently lose_sum: 517.0196126103401
time_elpased: 1.417
batch start
#iterations: 259
currently lose_sum: 520.322237610817
time_elpased: 1.406
start validation test
0.542164948454
0.665346534653
0.172892868169
0.274464956706
0
validation finish
batch start
#iterations: 260
currently lose_sum: 516.0561838150024
time_elpased: 1.414
batch start
#iterations: 261
currently lose_sum: 517.5232250392437
time_elpased: 1.39
batch start
#iterations: 262
currently lose_sum: 519.4839523434639
time_elpased: 1.389
batch start
#iterations: 263
currently lose_sum: 518.0887292027473
time_elpased: 1.389
batch start
#iterations: 264
currently lose_sum: 520.8861504197121
time_elpased: 1.397
batch start
#iterations: 265
currently lose_sum: 518.0286750495434
time_elpased: 1.401
batch start
#iterations: 266
currently lose_sum: 518.0159069597721
time_elpased: 1.407
batch start
#iterations: 267
currently lose_sum: 518.972649782896
time_elpased: 1.4
batch start
#iterations: 268
currently lose_sum: 516.099694699049
time_elpased: 1.403
batch start
#iterations: 269
currently lose_sum: 516.1185015439987
time_elpased: 1.394
batch start
#iterations: 270
currently lose_sum: 518.6673365235329
time_elpased: 1.4
batch start
#iterations: 271
currently lose_sum: 519.1218381524086
time_elpased: 1.398
batch start
#iterations: 272
currently lose_sum: 518.545730650425
time_elpased: 1.388
batch start
#iterations: 273
currently lose_sum: 517.6421117782593
time_elpased: 1.402
batch start
#iterations: 274
currently lose_sum: 519.0372234582901
time_elpased: 1.4
batch start
#iterations: 275
currently lose_sum: 515.8048145473003
time_elpased: 1.387
batch start
#iterations: 276
currently lose_sum: 521.6647543907166
time_elpased: 1.396
batch start
#iterations: 277
currently lose_sum: 519.5746357142925
time_elpased: 1.398
batch start
#iterations: 278
currently lose_sum: 517.3263694047928
time_elpased: 1.39
batch start
#iterations: 279
currently lose_sum: 517.4260690808296
time_elpased: 1.395
start validation test
0.551391752577
0.672097759674
0.203766594628
0.312722103767
0
validation finish
batch start
#iterations: 280
currently lose_sum: 518.7386921346188
time_elpased: 1.4
batch start
#iterations: 281
currently lose_sum: 516.7578925788403
time_elpased: 1.395
batch start
#iterations: 282
currently lose_sum: 517.7609546780586
time_elpased: 1.407
batch start
#iterations: 283
currently lose_sum: 518.2254933416843
time_elpased: 1.386
batch start
#iterations: 284
currently lose_sum: 516.0251064300537
time_elpased: 1.4
batch start
#iterations: 285
currently lose_sum: 516.0946279168129
time_elpased: 1.399
batch start
#iterations: 286
currently lose_sum: 515.9202187359333
time_elpased: 1.41
batch start
#iterations: 287
currently lose_sum: 517.8704806864262
time_elpased: 1.417
batch start
#iterations: 288
currently lose_sum: 519.3803683519363
time_elpased: 1.412
batch start
#iterations: 289
currently lose_sum: 518.4093796014786
time_elpased: 1.413
batch start
#iterations: 290
currently lose_sum: 518.0911013782024
time_elpased: 1.394
batch start
#iterations: 291
currently lose_sum: 516.1047475337982
time_elpased: 1.395
batch start
#iterations: 292
currently lose_sum: 517.2109480500221
time_elpased: 1.39
batch start
#iterations: 293
currently lose_sum: 519.4689629673958
time_elpased: 1.403
batch start
#iterations: 294
currently lose_sum: 516.6750786304474
time_elpased: 1.406
batch start
#iterations: 295
currently lose_sum: 517.6271784603596
time_elpased: 1.401
batch start
#iterations: 296
currently lose_sum: 516.9538278877735
time_elpased: 1.402
batch start
#iterations: 297
currently lose_sum: 517.5751989781857
time_elpased: 1.401
batch start
#iterations: 298
currently lose_sum: 517.1442097127438
time_elpased: 1.413
batch start
#iterations: 299
currently lose_sum: 519.0071704387665
time_elpased: 1.384
start validation test
0.539845360825
0.66652613828
0.162704538438
0.261560095955
0
validation finish
batch start
#iterations: 300
currently lose_sum: 517.8177913725376
time_elpased: 1.41
batch start
#iterations: 301
currently lose_sum: 517.9050092101097
time_elpased: 1.392
batch start
#iterations: 302
currently lose_sum: 518.0031373798847
time_elpased: 1.392
batch start
#iterations: 303
currently lose_sum: 519.3346136212349
time_elpased: 1.406
batch start
#iterations: 304
currently lose_sum: 518.4131043553352
time_elpased: 1.392
batch start
#iterations: 305
currently lose_sum: 517.0315979719162
time_elpased: 1.389
batch start
#iterations: 306
currently lose_sum: 516.8359972536564
time_elpased: 1.415
batch start
#iterations: 307
currently lose_sum: 516.5069954991341
time_elpased: 1.388
batch start
#iterations: 308
currently lose_sum: 518.1781796813011
time_elpased: 1.401
batch start
#iterations: 309
currently lose_sum: 517.3036139309406
time_elpased: 1.415
batch start
#iterations: 310
currently lose_sum: 517.3743407726288
time_elpased: 1.406
batch start
#iterations: 311
currently lose_sum: 517.9426741600037
time_elpased: 1.397
batch start
#iterations: 312
currently lose_sum: 516.2858454287052
time_elpased: 1.399
batch start
#iterations: 313
currently lose_sum: 518.2845086455345
time_elpased: 1.415
batch start
#iterations: 314
currently lose_sum: 518.0556936860085
time_elpased: 1.407
batch start
#iterations: 315
currently lose_sum: 519.0452117919922
time_elpased: 1.407
batch start
#iterations: 316
currently lose_sum: 517.9590407311916
time_elpased: 1.406
batch start
#iterations: 317
currently lose_sum: 516.906811773777
time_elpased: 1.41
batch start
#iterations: 318
currently lose_sum: 515.2121625542641
time_elpased: 1.403
batch start
#iterations: 319
currently lose_sum: 516.8908196687698
time_elpased: 1.402
start validation test
0.539329896907
0.666952054795
0.160337552743
0.258524848585
0
validation finish
batch start
#iterations: 320
currently lose_sum: 518.4132801890373
time_elpased: 1.41
batch start
#iterations: 321
currently lose_sum: 515.6005219519138
time_elpased: 1.402
batch start
#iterations: 322
currently lose_sum: 516.7032792270184
time_elpased: 1.404
batch start
#iterations: 323
currently lose_sum: 518.4613524675369
time_elpased: 1.401
batch start
#iterations: 324
currently lose_sum: 514.9190489053726
time_elpased: 1.402
batch start
#iterations: 325
currently lose_sum: 519.3387433588505
time_elpased: 1.398
batch start
#iterations: 326
currently lose_sum: 517.843744635582
time_elpased: 1.406
batch start
#iterations: 327
currently lose_sum: 517.9741797447205
time_elpased: 1.41
batch start
#iterations: 328
currently lose_sum: 517.639253526926
time_elpased: 1.397
batch start
#iterations: 329
currently lose_sum: 516.2869291901588
time_elpased: 1.399
batch start
#iterations: 330
currently lose_sum: 518.7268810272217
time_elpased: 1.457
batch start
#iterations: 331
currently lose_sum: 516.294793009758
time_elpased: 1.412
batch start
#iterations: 332
currently lose_sum: 516.6603578031063
time_elpased: 1.397
batch start
#iterations: 333
currently lose_sum: 516.970985352993
time_elpased: 1.421
batch start
#iterations: 334
currently lose_sum: 516.1605878770351
time_elpased: 1.41
batch start
#iterations: 335
currently lose_sum: 516.009021282196
time_elpased: 1.392
batch start
#iterations: 336
currently lose_sum: 516.9444908499718
time_elpased: 1.404
batch start
#iterations: 337
currently lose_sum: 517.5472264289856
time_elpased: 1.41
batch start
#iterations: 338
currently lose_sum: 519.434924185276
time_elpased: 1.41
batch start
#iterations: 339
currently lose_sum: 515.8353153467178
time_elpased: 1.401
start validation test
0.565103092784
0.68244013683
0.246372337141
0.362041587902
0
validation finish
batch start
#iterations: 340
currently lose_sum: 517.4272185862064
time_elpased: 1.405
batch start
#iterations: 341
currently lose_sum: 519.1387061774731
time_elpased: 1.397
batch start
#iterations: 342
currently lose_sum: 516.7051835954189
time_elpased: 1.396
batch start
#iterations: 343
currently lose_sum: 518.0476880371571
time_elpased: 1.39
batch start
#iterations: 344
currently lose_sum: 518.0842415690422
time_elpased: 1.403
batch start
#iterations: 345
currently lose_sum: 519.4149041771889
time_elpased: 1.389
batch start
#iterations: 346
currently lose_sum: 515.5983854234219
time_elpased: 1.395
batch start
#iterations: 347
currently lose_sum: 517.8529151380062
time_elpased: 1.423
batch start
#iterations: 348
currently lose_sum: 517.7804263830185
time_elpased: 1.398
batch start
#iterations: 349
currently lose_sum: 516.3917730748653
time_elpased: 1.406
batch start
#iterations: 350
currently lose_sum: 516.3018544912338
time_elpased: 1.379
batch start
#iterations: 351
currently lose_sum: 518.1451689302921
time_elpased: 1.414
batch start
#iterations: 352
currently lose_sum: 518.879445284605
time_elpased: 1.41
batch start
#iterations: 353
currently lose_sum: 516.3562394976616
time_elpased: 1.403
batch start
#iterations: 354
currently lose_sum: 519.1100335419178
time_elpased: 1.41
batch start
#iterations: 355
currently lose_sum: 518.041905850172
time_elpased: 1.394
batch start
#iterations: 356
currently lose_sum: 517.9098040759563
time_elpased: 1.395
batch start
#iterations: 357
currently lose_sum: 517.2264783978462
time_elpased: 1.388
batch start
#iterations: 358
currently lose_sum: 517.5577327311039
time_elpased: 1.41
batch start
#iterations: 359
currently lose_sum: 515.19479778409
time_elpased: 1.43
start validation test
0.550618556701
0.675571177504
0.197797674179
0.306002228944
0
validation finish
batch start
#iterations: 360
currently lose_sum: 516.4783245027065
time_elpased: 1.397
batch start
#iterations: 361
currently lose_sum: 515.9593219161034
time_elpased: 1.404
batch start
#iterations: 362
currently lose_sum: 517.4363942444324
time_elpased: 1.407
batch start
#iterations: 363
currently lose_sum: 516.1925024986267
time_elpased: 1.406
batch start
#iterations: 364
currently lose_sum: 517.2184080779552
time_elpased: 1.418
batch start
#iterations: 365
currently lose_sum: 516.744145989418
time_elpased: 1.396
batch start
#iterations: 366
currently lose_sum: 516.3737896382809
time_elpased: 1.4
batch start
#iterations: 367
currently lose_sum: 518.8103025853634
time_elpased: 1.393
batch start
#iterations: 368
currently lose_sum: 518.3823241889477
time_elpased: 1.402
batch start
#iterations: 369
currently lose_sum: 516.6176149249077
time_elpased: 1.402
batch start
#iterations: 370
currently lose_sum: 517.1855828762054
time_elpased: 1.385
batch start
#iterations: 371
currently lose_sum: 515.6310158669949
time_elpased: 1.389
batch start
#iterations: 372
currently lose_sum: 518.7262569665909
time_elpased: 1.42
batch start
#iterations: 373
currently lose_sum: 519.3813608288765
time_elpased: 1.401
batch start
#iterations: 374
currently lose_sum: 515.9042155444622
time_elpased: 1.39
batch start
#iterations: 375
currently lose_sum: 519.2730089426041
time_elpased: 1.387
batch start
#iterations: 376
currently lose_sum: 515.7907072305679
time_elpased: 1.392
batch start
#iterations: 377
currently lose_sum: 516.3674512505531
time_elpased: 1.409
batch start
#iterations: 378
currently lose_sum: 519.274338722229
time_elpased: 1.397
batch start
#iterations: 379
currently lose_sum: 514.7840705811977
time_elpased: 1.401
start validation test
0.550567010309
0.674597620714
0.198415148708
0.306640159046
0
validation finish
batch start
#iterations: 380
currently lose_sum: 516.651771068573
time_elpased: 1.395
batch start
#iterations: 381
currently lose_sum: 515.9571219980717
time_elpased: 1.406
batch start
#iterations: 382
currently lose_sum: 515.9998455941677
time_elpased: 1.394
batch start
#iterations: 383
currently lose_sum: 519.0382654368877
time_elpased: 1.391
batch start
#iterations: 384
currently lose_sum: 517.3815248906612
time_elpased: 1.408
batch start
#iterations: 385
currently lose_sum: 517.3330808579922
time_elpased: 1.392
batch start
#iterations: 386
currently lose_sum: 519.5173485279083
time_elpased: 1.405
batch start
#iterations: 387
currently lose_sum: 517.1749996244907
time_elpased: 1.397
batch start
#iterations: 388
currently lose_sum: 517.431146889925
time_elpased: 1.409
batch start
#iterations: 389
currently lose_sum: 515.9095330536366
time_elpased: 1.397
batch start
#iterations: 390
currently lose_sum: 517.2327205836773
time_elpased: 1.415
batch start
#iterations: 391
currently lose_sum: 516.4933815002441
time_elpased: 1.385
batch start
#iterations: 392
currently lose_sum: 516.3671313226223
time_elpased: 1.392
batch start
#iterations: 393
currently lose_sum: 517.8427230417728
time_elpased: 1.4
batch start
#iterations: 394
currently lose_sum: 516.227910220623
time_elpased: 1.414
batch start
#iterations: 395
currently lose_sum: 518.7218080163002
time_elpased: 1.393
batch start
#iterations: 396
currently lose_sum: 516.9385640919209
time_elpased: 1.414
batch start
#iterations: 397
currently lose_sum: 517.7544732987881
time_elpased: 1.417
batch start
#iterations: 398
currently lose_sum: 516.0795046687126
time_elpased: 1.411
batch start
#iterations: 399
currently lose_sum: 516.6335844993591
time_elpased: 1.391
start validation test
0.536907216495
0.663251670379
0.153236595657
0.248955024243
0
validation finish
acc: 0.568
pre: 0.674
rec: 0.265
F1: 0.380
auc: 0.000
