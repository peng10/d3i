start to construct graph
graph construct over
87453
epochs start
batch start
#iterations: 0
currently lose_sum: 556.0161413550377
time_elpased: 1.758
batch start
#iterations: 1
currently lose_sum: 555.3946338295937
time_elpased: 1.709
batch start
#iterations: 2
currently lose_sum: 554.1480293869972
time_elpased: 1.702
batch start
#iterations: 3
currently lose_sum: 553.9660542011261
time_elpased: 1.688
batch start
#iterations: 4
currently lose_sum: 549.1662622690201
time_elpased: 1.706
batch start
#iterations: 5
currently lose_sum: 549.2020890116692
time_elpased: 1.688
batch start
#iterations: 6
currently lose_sum: 547.5987903475761
time_elpased: 1.704
batch start
#iterations: 7
currently lose_sum: 545.3904443383217
time_elpased: 1.709
batch start
#iterations: 8
currently lose_sum: 545.5047045946121
time_elpased: 1.707
batch start
#iterations: 9
currently lose_sum: 542.2536587119102
time_elpased: 1.745
batch start
#iterations: 10
currently lose_sum: 541.1836742162704
time_elpased: 1.72
batch start
#iterations: 11
currently lose_sum: 541.9714421629906
time_elpased: 1.703
batch start
#iterations: 12
currently lose_sum: 541.5264833569527
time_elpased: 1.71
batch start
#iterations: 13
currently lose_sum: 537.1377651691437
time_elpased: 1.719
batch start
#iterations: 14
currently lose_sum: 537.9758341610432
time_elpased: 1.788
batch start
#iterations: 15
currently lose_sum: 535.8575840592384
time_elpased: 1.702
batch start
#iterations: 16
currently lose_sum: 536.8060561716557
time_elpased: 1.722
batch start
#iterations: 17
currently lose_sum: 535.434050142765
time_elpased: 1.687
batch start
#iterations: 18
currently lose_sum: 534.5931969881058
time_elpased: 1.7
batch start
#iterations: 19
currently lose_sum: 533.9693002402782
time_elpased: 1.71
start validation test
0.526494845361
0.693654266958
0.0978697128743
0.171536796537
0
validation finish
batch start
#iterations: 20
currently lose_sum: 535.6086960434914
time_elpased: 1.698
batch start
#iterations: 21
currently lose_sum: 532.3490595817566
time_elpased: 1.707
batch start
#iterations: 22
currently lose_sum: 532.7801695466042
time_elpased: 1.69
batch start
#iterations: 23
currently lose_sum: 530.3347350656986
time_elpased: 1.687
batch start
#iterations: 24
currently lose_sum: 531.4182937145233
time_elpased: 1.682
batch start
#iterations: 25
currently lose_sum: 530.2180554568768
time_elpased: 1.72
batch start
#iterations: 26
currently lose_sum: 528.5317791998386
time_elpased: 1.677
batch start
#iterations: 27
currently lose_sum: 530.0176278054714
time_elpased: 1.686
batch start
#iterations: 28
currently lose_sum: 528.2929502129555
time_elpased: 1.684
batch start
#iterations: 29
currently lose_sum: 525.4710150957108
time_elpased: 1.752
batch start
#iterations: 30
currently lose_sum: 525.6201294958591
time_elpased: 1.723
batch start
#iterations: 31
currently lose_sum: 527.5721054673195
time_elpased: 1.691
batch start
#iterations: 32
currently lose_sum: 526.7286661267281
time_elpased: 1.692
batch start
#iterations: 33
currently lose_sum: 527.7673830091953
time_elpased: 1.693
batch start
#iterations: 34
currently lose_sum: 525.848237901926
time_elpased: 1.704
batch start
#iterations: 35
currently lose_sum: 526.3040138483047
time_elpased: 1.699
batch start
#iterations: 36
currently lose_sum: 526.1766921877861
time_elpased: 1.688
batch start
#iterations: 37
currently lose_sum: 523.0648074746132
time_elpased: 1.702
batch start
#iterations: 38
currently lose_sum: 525.9658934473991
time_elpased: 1.686
batch start
#iterations: 39
currently lose_sum: 525.3787981271744
time_elpased: 1.673
start validation test
0.533917525773
0.690570299266
0.12586189153
0.212917827298
0
validation finish
batch start
#iterations: 40
currently lose_sum: 524.8954835832119
time_elpased: 1.68
batch start
#iterations: 41
currently lose_sum: 524.238182246685
time_elpased: 1.691
batch start
#iterations: 42
currently lose_sum: 524.1549252271652
time_elpased: 1.701
batch start
#iterations: 43
currently lose_sum: 524.5610295236111
time_elpased: 1.718
batch start
#iterations: 44
currently lose_sum: 522.5654180645943
time_elpased: 1.703
batch start
#iterations: 45
currently lose_sum: 522.1679041981697
time_elpased: 1.706
batch start
#iterations: 46
currently lose_sum: 523.5890507400036
time_elpased: 1.692
batch start
#iterations: 47
currently lose_sum: 522.6672074496746
time_elpased: 1.684
batch start
#iterations: 48
currently lose_sum: 520.4828147888184
time_elpased: 1.695
batch start
#iterations: 49
currently lose_sum: 520.0174835622311
time_elpased: 1.675
batch start
#iterations: 50
currently lose_sum: 519.0255009233952
time_elpased: 1.692
batch start
#iterations: 51
currently lose_sum: 519.2256605327129
time_elpased: 1.695
batch start
#iterations: 52
currently lose_sum: 522.329531788826
time_elpased: 1.705
batch start
#iterations: 53
currently lose_sum: 521.5921314656734
time_elpased: 1.684
batch start
#iterations: 54
currently lose_sum: 520.4637094438076
time_elpased: 1.683
batch start
#iterations: 55
currently lose_sum: 519.774143755436
time_elpased: 1.686
batch start
#iterations: 56
currently lose_sum: 518.8736797273159
time_elpased: 1.691
batch start
#iterations: 57
currently lose_sum: 518.1140736043453
time_elpased: 1.691
batch start
#iterations: 58
currently lose_sum: 517.7604774236679
time_elpased: 1.692
batch start
#iterations: 59
currently lose_sum: 517.4681425094604
time_elpased: 1.716
start validation test
0.51118556701
0.720754716981
0.0393125450242
0.0745584073387
0
validation finish
batch start
#iterations: 60
currently lose_sum: 518.7459377646446
time_elpased: 1.708
batch start
#iterations: 61
currently lose_sum: 516.6761855781078
time_elpased: 1.696
batch start
#iterations: 62
currently lose_sum: 517.5833562910557
time_elpased: 1.69
batch start
#iterations: 63
currently lose_sum: 518.5922984480858
time_elpased: 1.688
batch start
#iterations: 64
currently lose_sum: 516.4967533349991
time_elpased: 1.683
batch start
#iterations: 65
currently lose_sum: 518.1640430390835
time_elpased: 1.69
batch start
#iterations: 66
currently lose_sum: 517.9899462163448
time_elpased: 1.689
batch start
#iterations: 67
currently lose_sum: 515.1486875712872
time_elpased: 1.696
batch start
#iterations: 68
currently lose_sum: 516.5419601202011
time_elpased: 1.682
batch start
#iterations: 69
currently lose_sum: 516.2446412742138
time_elpased: 1.734
batch start
#iterations: 70
currently lose_sum: 518.2677136361599
time_elpased: 1.697
batch start
#iterations: 71
currently lose_sum: 517.1366690099239
time_elpased: 1.67
batch start
#iterations: 72
currently lose_sum: 515.4523683488369
time_elpased: 1.696
batch start
#iterations: 73
currently lose_sum: 516.3569082915783
time_elpased: 1.704
batch start
#iterations: 74
currently lose_sum: 514.1154247224331
time_elpased: 1.687
batch start
#iterations: 75
currently lose_sum: 513.75145855546
time_elpased: 1.689
batch start
#iterations: 76
currently lose_sum: 515.3582526445389
time_elpased: 1.698
batch start
#iterations: 77
currently lose_sum: 514.1362875401974
time_elpased: 1.688
batch start
#iterations: 78
currently lose_sum: 515.802444756031
time_elpased: 1.712
batch start
#iterations: 79
currently lose_sum: 514.9170460700989
time_elpased: 1.707
start validation test
0.59675257732
0.690619967794
0.353092518267
0.467279536942
0
validation finish
batch start
#iterations: 80
currently lose_sum: 515.5407490432262
time_elpased: 1.71
batch start
#iterations: 81
currently lose_sum: 514.8309697806835
time_elpased: 1.75
batch start
#iterations: 82
currently lose_sum: 512.8633162379265
time_elpased: 1.701
batch start
#iterations: 83
currently lose_sum: 512.509669572115
time_elpased: 1.704
batch start
#iterations: 84
currently lose_sum: 513.6557312607765
time_elpased: 1.721
batch start
#iterations: 85
currently lose_sum: 513.9675620496273
time_elpased: 1.721
batch start
#iterations: 86
currently lose_sum: 514.4992237091064
time_elpased: 1.691
batch start
#iterations: 87
currently lose_sum: 514.2136756181717
time_elpased: 1.69
batch start
#iterations: 88
currently lose_sum: 515.278498262167
time_elpased: 1.702
batch start
#iterations: 89
currently lose_sum: 512.6591901779175
time_elpased: 1.697
batch start
#iterations: 90
currently lose_sum: 512.8987441062927
time_elpased: 1.709
batch start
#iterations: 91
currently lose_sum: 512.6957892179489
time_elpased: 1.695
batch start
#iterations: 92
currently lose_sum: 511.9385041296482
time_elpased: 1.709
batch start
#iterations: 93
currently lose_sum: 513.9490615725517
time_elpased: 1.709
batch start
#iterations: 94
currently lose_sum: 512.4556884467602
time_elpased: 1.702
batch start
#iterations: 95
currently lose_sum: 513.2803623080254
time_elpased: 1.689
batch start
#iterations: 96
currently lose_sum: 512.7490838468075
time_elpased: 1.726
batch start
#iterations: 97
currently lose_sum: 512.6634851694107
time_elpased: 1.687
batch start
#iterations: 98
currently lose_sum: 509.0060721039772
time_elpased: 1.713
batch start
#iterations: 99
currently lose_sum: 514.5627818703651
time_elpased: 1.73
start validation test
0.54675257732
0.698453608247
0.167335597407
0.2699875467
0
validation finish
batch start
#iterations: 100
currently lose_sum: 512.9610887467861
time_elpased: 1.736
batch start
#iterations: 101
currently lose_sum: 511.1951988041401
time_elpased: 2.414
batch start
#iterations: 102
currently lose_sum: 513.6184166073799
time_elpased: 2.3
batch start
#iterations: 103
currently lose_sum: 511.1618871986866
time_elpased: 2.256
batch start
#iterations: 104
currently lose_sum: 510.412146627903
time_elpased: 2.133
batch start
#iterations: 105
currently lose_sum: 510.1269043982029
time_elpased: 1.812
batch start
#iterations: 106
currently lose_sum: 512.0954643785954
time_elpased: 1.728
batch start
#iterations: 107
currently lose_sum: 511.56053200364113
time_elpased: 1.707
batch start
#iterations: 108
currently lose_sum: 510.299482434988
time_elpased: 1.725
batch start
#iterations: 109
currently lose_sum: 513.0621200799942
time_elpased: 1.817
batch start
#iterations: 110
currently lose_sum: 511.68684220314026
time_elpased: 1.703
batch start
#iterations: 111
currently lose_sum: 512.6179493963718
time_elpased: 1.71
batch start
#iterations: 112
currently lose_sum: 511.1888871192932
time_elpased: 1.704
batch start
#iterations: 113
currently lose_sum: 511.4714922308922
time_elpased: 1.706
batch start
#iterations: 114
currently lose_sum: 510.842587351799
time_elpased: 1.696
batch start
#iterations: 115
currently lose_sum: 508.3659469783306
time_elpased: 1.707
batch start
#iterations: 116
currently lose_sum: 512.1376101672649
time_elpased: 1.699
batch start
#iterations: 117
currently lose_sum: 509.3411872982979
time_elpased: 1.714
batch start
#iterations: 118
currently lose_sum: 510.33877140283585
time_elpased: 1.704
batch start
#iterations: 119
currently lose_sum: 508.10414430499077
time_elpased: 1.703
start validation test
0.573402061856
0.689855072464
0.269424719564
0.387507400829
0
validation finish
batch start
#iterations: 120
currently lose_sum: 510.8170644044876
time_elpased: 1.69
batch start
#iterations: 121
currently lose_sum: 510.38961005210876
time_elpased: 1.696
batch start
#iterations: 122
currently lose_sum: 511.9042418599129
time_elpased: 1.714
batch start
#iterations: 123
currently lose_sum: 511.0283014178276
time_elpased: 1.684
batch start
#iterations: 124
currently lose_sum: 509.2033863067627
time_elpased: 1.754
batch start
#iterations: 125
currently lose_sum: 507.69204476475716
time_elpased: 1.704
batch start
#iterations: 126
currently lose_sum: 508.03181555867195
time_elpased: 1.699
batch start
#iterations: 127
currently lose_sum: 509.1815767586231
time_elpased: 1.709
batch start
#iterations: 128
currently lose_sum: 510.5392369031906
time_elpased: 1.819
batch start
#iterations: 129
currently lose_sum: 508.56025952100754
time_elpased: 1.725
batch start
#iterations: 130
currently lose_sum: 508.50416815280914
time_elpased: 1.729
batch start
#iterations: 131
currently lose_sum: 510.2416300177574
time_elpased: 1.717
batch start
#iterations: 132
currently lose_sum: 508.985139220953
time_elpased: 1.712
batch start
#iterations: 133
currently lose_sum: 508.6519197821617
time_elpased: 1.696
batch start
#iterations: 134
currently lose_sum: 507.72922399640083
time_elpased: 1.691
batch start
#iterations: 135
currently lose_sum: 508.97899067401886
time_elpased: 1.694
batch start
#iterations: 136
currently lose_sum: 510.78930220007896
time_elpased: 1.685
batch start
#iterations: 137
currently lose_sum: 506.1392437815666
time_elpased: 1.686
batch start
#iterations: 138
currently lose_sum: 510.7163217961788
time_elpased: 1.677
batch start
#iterations: 139
currently lose_sum: 509.96958231925964
time_elpased: 1.703
start validation test
0.528092783505
0.704512372635
0.0996192240403
0.174555946263
0
validation finish
batch start
#iterations: 140
currently lose_sum: 507.452739328146
time_elpased: 1.684
batch start
#iterations: 141
currently lose_sum: 507.64045745134354
time_elpased: 1.683
batch start
#iterations: 142
currently lose_sum: 509.24802950024605
time_elpased: 1.698
batch start
#iterations: 143
currently lose_sum: 507.42096894979477
time_elpased: 1.681
batch start
#iterations: 144
currently lose_sum: 508.89847829937935
time_elpased: 1.676
batch start
#iterations: 145
currently lose_sum: 509.5399004817009
time_elpased: 1.698
batch start
#iterations: 146
currently lose_sum: 508.16292601823807
time_elpased: 1.699
batch start
#iterations: 147
currently lose_sum: 505.48667401075363
time_elpased: 1.738
batch start
#iterations: 148
currently lose_sum: 507.0172108411789
time_elpased: 1.713
batch start
#iterations: 149
currently lose_sum: 508.6847108602524
time_elpased: 1.821
batch start
#iterations: 150
currently lose_sum: 507.1111181974411
time_elpased: 1.73
batch start
#iterations: 151
currently lose_sum: 507.47238969802856
time_elpased: 1.726
batch start
#iterations: 152
currently lose_sum: 506.7769159078598
time_elpased: 1.71
batch start
#iterations: 153
currently lose_sum: 506.0153686106205
time_elpased: 1.721
batch start
#iterations: 154
currently lose_sum: 508.41839718818665
time_elpased: 1.694
batch start
#iterations: 155
currently lose_sum: 507.629543364048
time_elpased: 1.698
batch start
#iterations: 156
currently lose_sum: 508.141859382391
time_elpased: 1.717
batch start
#iterations: 157
currently lose_sum: 505.89520049095154
time_elpased: 1.729
batch start
#iterations: 158
currently lose_sum: 507.02854922413826
time_elpased: 1.721
batch start
#iterations: 159
currently lose_sum: 506.43422654271126
time_elpased: 1.72
start validation test
0.519536082474
0.701219512195
0.0710095708552
0.128959910289
0
validation finish
batch start
#iterations: 160
currently lose_sum: 507.6326242685318
time_elpased: 1.717
batch start
#iterations: 161
currently lose_sum: 507.14058062434196
time_elpased: 1.77
batch start
#iterations: 162
currently lose_sum: 505.6601558327675
time_elpased: 1.72
batch start
#iterations: 163
currently lose_sum: 506.53239208459854
time_elpased: 1.719
batch start
#iterations: 164
currently lose_sum: 507.9901898801327
time_elpased: 1.684
batch start
#iterations: 165
currently lose_sum: 509.4936459362507
time_elpased: 1.69
batch start
#iterations: 166
currently lose_sum: 507.31323462724686
time_elpased: 1.688
batch start
#iterations: 167
currently lose_sum: 508.064822435379
time_elpased: 1.706
batch start
#iterations: 168
currently lose_sum: 507.17276272177696
time_elpased: 1.696
batch start
#iterations: 169
currently lose_sum: 505.67770355939865
time_elpased: 1.698
batch start
#iterations: 170
currently lose_sum: 507.2873925566673
time_elpased: 1.694
batch start
#iterations: 171
currently lose_sum: 506.45882999897003
time_elpased: 1.682
batch start
#iterations: 172
currently lose_sum: 505.67188888788223
time_elpased: 1.7
batch start
#iterations: 173
currently lose_sum: 506.22993180155754
time_elpased: 1.707
batch start
#iterations: 174
currently lose_sum: 508.1172811985016
time_elpased: 1.694
batch start
#iterations: 175
currently lose_sum: 507.27093479037285
time_elpased: 1.699
batch start
#iterations: 176
currently lose_sum: 508.5700575709343
time_elpased: 1.675
batch start
#iterations: 177
currently lose_sum: 506.3471073806286
time_elpased: 1.681
batch start
#iterations: 178
currently lose_sum: 506.15979501605034
time_elpased: 1.697
batch start
#iterations: 179
currently lose_sum: 508.4412490129471
time_elpased: 1.689
start validation test
0.536649484536
0.701104972376
0.130595862921
0.220178710853
0
validation finish
batch start
#iterations: 180
currently lose_sum: 506.50133883953094
time_elpased: 1.764
batch start
#iterations: 181
currently lose_sum: 507.0684468150139
time_elpased: 1.711
batch start
#iterations: 182
currently lose_sum: 506.54940435290337
time_elpased: 1.718
batch start
#iterations: 183
currently lose_sum: 507.0081441104412
time_elpased: 1.71
batch start
#iterations: 184
currently lose_sum: 506.81243109703064
time_elpased: 1.706
batch start
#iterations: 185
currently lose_sum: 505.6049270629883
time_elpased: 1.701
batch start
#iterations: 186
currently lose_sum: 506.45019868016243
time_elpased: 1.686
batch start
#iterations: 187
currently lose_sum: 507.60120528936386
time_elpased: 1.699
batch start
#iterations: 188
currently lose_sum: 507.0281194448471
time_elpased: 1.729
batch start
#iterations: 189
currently lose_sum: 506.25972098112106
time_elpased: 1.715
batch start
#iterations: 190
currently lose_sum: 506.20628836750984
time_elpased: 1.696
batch start
#iterations: 191
currently lose_sum: 505.3116973042488
time_elpased: 1.752
batch start
#iterations: 192
currently lose_sum: 504.8269492685795
time_elpased: 1.744
batch start
#iterations: 193
currently lose_sum: 506.54379150271416
time_elpased: 1.789
batch start
#iterations: 194
currently lose_sum: 507.6379209160805
time_elpased: 1.739
batch start
#iterations: 195
currently lose_sum: 504.43119341135025
time_elpased: 1.696
batch start
#iterations: 196
currently lose_sum: 505.42237251996994
time_elpased: 1.71
batch start
#iterations: 197
currently lose_sum: 506.0684002637863
time_elpased: 1.704
batch start
#iterations: 198
currently lose_sum: 506.30620807409286
time_elpased: 1.728
batch start
#iterations: 199
currently lose_sum: 507.65654027462006
time_elpased: 1.782
start validation test
0.534278350515
0.692655367232
0.126170628795
0.213458692435
0
validation finish
batch start
#iterations: 200
currently lose_sum: 500.8653763830662
time_elpased: 1.726
batch start
#iterations: 201
currently lose_sum: 506.5075227022171
time_elpased: 1.797
batch start
#iterations: 202
currently lose_sum: 505.1479653120041
time_elpased: 1.715
batch start
#iterations: 203
currently lose_sum: 505.995076328516
time_elpased: 1.705
batch start
#iterations: 204
currently lose_sum: 506.5411508977413
time_elpased: 1.71
batch start
#iterations: 205
currently lose_sum: 503.81241884827614
time_elpased: 1.711
batch start
#iterations: 206
currently lose_sum: 502.6107133626938
time_elpased: 1.678
batch start
#iterations: 207
currently lose_sum: 504.3224807679653
time_elpased: 1.695
batch start
#iterations: 208
currently lose_sum: 505.1056394279003
time_elpased: 1.702
batch start
#iterations: 209
currently lose_sum: 505.57973155379295
time_elpased: 1.696
batch start
#iterations: 210
currently lose_sum: 506.795309484005
time_elpased: 1.713
batch start
#iterations: 211
currently lose_sum: 504.9987946152687
time_elpased: 1.701
batch start
#iterations: 212
currently lose_sum: 504.13485556840897
time_elpased: 1.697
batch start
#iterations: 213
currently lose_sum: 506.1349695920944
time_elpased: 1.692
batch start
#iterations: 214
currently lose_sum: 505.7012800872326
time_elpased: 1.681
batch start
#iterations: 215
currently lose_sum: 505.4296208024025
time_elpased: 1.696
batch start
#iterations: 216
currently lose_sum: 506.3237019479275
time_elpased: 1.702
batch start
#iterations: 217
currently lose_sum: 504.946222782135
time_elpased: 1.734
batch start
#iterations: 218
currently lose_sum: 505.59067326784134
time_elpased: 1.686
batch start
#iterations: 219
currently lose_sum: 507.49167451262474
time_elpased: 1.701
start validation test
0.516288659794
0.724696356275
0.0552639703612
0.102696500287
0
validation finish
batch start
#iterations: 220
currently lose_sum: 504.27440318465233
time_elpased: 1.703
batch start
#iterations: 221
currently lose_sum: 507.20013159513474
time_elpased: 1.693
batch start
#iterations: 222
currently lose_sum: 503.83840161561966
time_elpased: 1.701
batch start
#iterations: 223
currently lose_sum: 504.70999467372894
time_elpased: 1.701
batch start
#iterations: 224
currently lose_sum: 506.2734795510769
time_elpased: 1.696
batch start
#iterations: 225
currently lose_sum: 505.66041868925095
time_elpased: 1.72
batch start
#iterations: 226
currently lose_sum: 504.02331963181496
time_elpased: 1.715
batch start
#iterations: 227
currently lose_sum: 504.79071351885796
time_elpased: 1.738
batch start
#iterations: 228
currently lose_sum: 503.1301774382591
time_elpased: 1.706
batch start
#iterations: 229
currently lose_sum: 503.6796740293503
time_elpased: 1.707
batch start
#iterations: 230
currently lose_sum: 504.67191326618195
time_elpased: 1.691
batch start
#iterations: 231
currently lose_sum: 505.12106385827065
time_elpased: 1.691
batch start
#iterations: 232
currently lose_sum: 505.5172574520111
time_elpased: 1.686
batch start
#iterations: 233
currently lose_sum: 504.37362760305405
time_elpased: 1.706
batch start
#iterations: 234
currently lose_sum: 506.1540302336216
time_elpased: 1.742
batch start
#iterations: 235
currently lose_sum: 502.21674144268036
time_elpased: 1.722
batch start
#iterations: 236
currently lose_sum: 500.3610832095146
time_elpased: 1.703
batch start
#iterations: 237
currently lose_sum: 502.4160189330578
time_elpased: 1.705
batch start
#iterations: 238
currently lose_sum: 501.17407554388046
time_elpased: 1.697
batch start
#iterations: 239
currently lose_sum: 505.18124637007713
time_elpased: 1.712
start validation test
0.571134020619
0.68914161928
0.261912112792
0.37956748695
0
validation finish
batch start
#iterations: 240
currently lose_sum: 502.3956934809685
time_elpased: 1.723
batch start
#iterations: 241
currently lose_sum: 504.5655112862587
time_elpased: 1.708
batch start
#iterations: 242
currently lose_sum: 504.2006765604019
time_elpased: 1.702
batch start
#iterations: 243
currently lose_sum: 503.5586460530758
time_elpased: 1.719
batch start
#iterations: 244
currently lose_sum: 505.7462814748287
time_elpased: 1.686
batch start
#iterations: 245
currently lose_sum: 503.17692655324936
time_elpased: 1.701
batch start
#iterations: 246
currently lose_sum: 505.3413088917732
time_elpased: 1.704
batch start
#iterations: 247
currently lose_sum: 507.3588265180588
time_elpased: 1.703
batch start
#iterations: 248
currently lose_sum: 503.55113485455513
time_elpased: 1.698
batch start
#iterations: 249
currently lose_sum: 503.9259640574455
time_elpased: 1.715
batch start
#iterations: 250
currently lose_sum: 506.06975641846657
time_elpased: 1.724
batch start
#iterations: 251
currently lose_sum: 503.1305067539215
time_elpased: 1.706
batch start
#iterations: 252
currently lose_sum: 502.44669499993324
time_elpased: 1.698
batch start
#iterations: 253
currently lose_sum: 502.0895531773567
time_elpased: 1.694
batch start
#iterations: 254
currently lose_sum: 502.03244438767433
time_elpased: 1.727
batch start
#iterations: 255
currently lose_sum: 503.41089406609535
time_elpased: 1.748
batch start
#iterations: 256
currently lose_sum: 504.0713441669941
time_elpased: 1.699
batch start
#iterations: 257
currently lose_sum: 502.11459958553314
time_elpased: 1.737
batch start
#iterations: 258
currently lose_sum: 501.85494896769524
time_elpased: 1.705
batch start
#iterations: 259
currently lose_sum: 506.22850850224495
time_elpased: 1.714
start validation test
0.545927835052
0.711162790698
0.157353092518
0.257689390747
0
validation finish
batch start
#iterations: 260
currently lose_sum: 500.58024632930756
time_elpased: 1.735
batch start
#iterations: 261
currently lose_sum: 503.79350847005844
time_elpased: 1.735
batch start
#iterations: 262
currently lose_sum: 505.0918346643448
time_elpased: 1.705
batch start
#iterations: 263
currently lose_sum: 503.81505385041237
time_elpased: 1.742
batch start
#iterations: 264
currently lose_sum: 506.2918028831482
time_elpased: 1.701
batch start
#iterations: 265
currently lose_sum: 503.5442071557045
time_elpased: 1.725
batch start
#iterations: 266
currently lose_sum: 503.33580803871155
time_elpased: 1.719
batch start
#iterations: 267
currently lose_sum: 504.4576011300087
time_elpased: 1.725
batch start
#iterations: 268
currently lose_sum: 501.62015399336815
time_elpased: 1.696
batch start
#iterations: 269
currently lose_sum: 503.49759247899055
time_elpased: 1.735
batch start
#iterations: 270
currently lose_sum: 504.70387837290764
time_elpased: 1.713
batch start
#iterations: 271
currently lose_sum: 504.3964956998825
time_elpased: 1.703
batch start
#iterations: 272
currently lose_sum: 503.58512327075005
time_elpased: 1.71
batch start
#iterations: 273
currently lose_sum: 503.8686888515949
time_elpased: 1.696
batch start
#iterations: 274
currently lose_sum: 504.0157226026058
time_elpased: 1.694
batch start
#iterations: 275
currently lose_sum: 501.8422255218029
time_elpased: 1.709
batch start
#iterations: 276
currently lose_sum: 505.4519028067589
time_elpased: 1.706
batch start
#iterations: 277
currently lose_sum: 505.13146218657494
time_elpased: 1.7
batch start
#iterations: 278
currently lose_sum: 502.5375228226185
time_elpased: 1.711
batch start
#iterations: 279
currently lose_sum: 502.84526485204697
time_elpased: 1.706
start validation test
0.547422680412
0.701159295835
0.168055984357
0.271127345177
0
validation finish
batch start
#iterations: 280
currently lose_sum: 503.4612668156624
time_elpased: 1.708
batch start
#iterations: 281
currently lose_sum: 501.77256178855896
time_elpased: 1.762
batch start
#iterations: 282
currently lose_sum: 502.9303387403488
time_elpased: 1.692
batch start
#iterations: 283
currently lose_sum: 503.4792998433113
time_elpased: 1.827
batch start
#iterations: 284
currently lose_sum: 502.36112281680107
time_elpased: 1.804
batch start
#iterations: 285
currently lose_sum: 502.926249563694
time_elpased: 1.715
batch start
#iterations: 286
currently lose_sum: 501.3040493130684
time_elpased: 1.713
batch start
#iterations: 287
currently lose_sum: 503.9088225364685
time_elpased: 1.741
batch start
#iterations: 288
currently lose_sum: 504.2074038684368
time_elpased: 1.7
batch start
#iterations: 289
currently lose_sum: 504.7975259721279
time_elpased: 1.692
batch start
#iterations: 290
currently lose_sum: 502.07666781544685
time_elpased: 1.725
batch start
#iterations: 291
currently lose_sum: 501.3532488644123
time_elpased: 1.704
batch start
#iterations: 292
currently lose_sum: 502.43257880210876
time_elpased: 1.712
batch start
#iterations: 293
currently lose_sum: 504.61171105504036
time_elpased: 1.697
batch start
#iterations: 294
currently lose_sum: 501.189537525177
time_elpased: 1.709
batch start
#iterations: 295
currently lose_sum: 503.81511893868446
time_elpased: 1.713
batch start
#iterations: 296
currently lose_sum: 501.91709262132645
time_elpased: 1.7
batch start
#iterations: 297
currently lose_sum: 501.9761417210102
time_elpased: 1.707
batch start
#iterations: 298
currently lose_sum: 502.6088317632675
time_elpased: 1.753
batch start
#iterations: 299
currently lose_sum: 502.9509345293045
time_elpased: 1.713
start validation test
0.540206185567
0.709847288046
0.138725944221
0.232093663912
0
validation finish
batch start
#iterations: 300
currently lose_sum: 502.6848624944687
time_elpased: 1.802
batch start
#iterations: 301
currently lose_sum: 503.64301213622093
time_elpased: 1.764
batch start
#iterations: 302
currently lose_sum: 502.7669401168823
time_elpased: 1.825
batch start
#iterations: 303
currently lose_sum: 504.66491612792015
time_elpased: 1.822
batch start
#iterations: 304
currently lose_sum: 502.1869721710682
time_elpased: 1.71
batch start
#iterations: 305
currently lose_sum: 504.36785769462585
time_elpased: 1.702
batch start
#iterations: 306
currently lose_sum: 502.8473878800869
time_elpased: 1.732
batch start
#iterations: 307
currently lose_sum: 500.79575872421265
time_elpased: 1.733
batch start
#iterations: 308
currently lose_sum: 503.5322351157665
time_elpased: 1.699
batch start
#iterations: 309
currently lose_sum: 502.8285837173462
time_elpased: 1.715
batch start
#iterations: 310
currently lose_sum: 502.83819168806076
time_elpased: 1.731
batch start
#iterations: 311
currently lose_sum: 502.51105639338493
time_elpased: 1.704
batch start
#iterations: 312
currently lose_sum: 501.0480422079563
time_elpased: 1.693
batch start
#iterations: 313
currently lose_sum: 503.0647222995758
time_elpased: 1.709
batch start
#iterations: 314
currently lose_sum: 503.2402138710022
time_elpased: 1.733
batch start
#iterations: 315
currently lose_sum: 503.9371039569378
time_elpased: 1.789
batch start
#iterations: 316
currently lose_sum: 502.93083494901657
time_elpased: 1.711
batch start
#iterations: 317
currently lose_sum: 502.5859918296337
time_elpased: 1.719
batch start
#iterations: 318
currently lose_sum: 500.3241759836674
time_elpased: 1.714
batch start
#iterations: 319
currently lose_sum: 500.3688533604145
time_elpased: 1.69
start validation test
0.519948453608
0.687037037037
0.0763610167747
0.137445586737
0
validation finish
batch start
#iterations: 320
currently lose_sum: 503.2915332913399
time_elpased: 1.723
batch start
#iterations: 321
currently lose_sum: 500.30213633179665
time_elpased: 1.707
batch start
#iterations: 322
currently lose_sum: 502.34273728728294
time_elpased: 1.716
batch start
#iterations: 323
currently lose_sum: 502.1201156079769
time_elpased: 1.699
batch start
#iterations: 324
currently lose_sum: 498.9843667447567
time_elpased: 1.702
batch start
#iterations: 325
currently lose_sum: 504.2782257795334
time_elpased: 1.694
batch start
#iterations: 326
currently lose_sum: 502.0189146101475
time_elpased: 1.688
batch start
#iterations: 327
currently lose_sum: 503.0356628894806
time_elpased: 1.715
batch start
#iterations: 328
currently lose_sum: 502.16053771972656
time_elpased: 1.716
batch start
#iterations: 329
currently lose_sum: 499.8341800272465
time_elpased: 1.707
batch start
#iterations: 330
currently lose_sum: 503.21799606084824
time_elpased: 1.698
batch start
#iterations: 331
currently lose_sum: 501.07431676983833
time_elpased: 1.728
batch start
#iterations: 332
currently lose_sum: 501.0806791484356
time_elpased: 1.729
batch start
#iterations: 333
currently lose_sum: 501.6195303797722
time_elpased: 1.724
batch start
#iterations: 334
currently lose_sum: 500.44131553173065
time_elpased: 1.758
batch start
#iterations: 335
currently lose_sum: 500.49829959869385
time_elpased: 1.793
batch start
#iterations: 336
currently lose_sum: 500.50910529494286
time_elpased: 1.819
batch start
#iterations: 337
currently lose_sum: 502.23480305075645
time_elpased: 1.719
batch start
#iterations: 338
currently lose_sum: 504.4406541585922
time_elpased: 1.702
batch start
#iterations: 339
currently lose_sum: 500.68774330616
time_elpased: 1.727
start validation test
0.580618556701
0.692195477753
0.292991664094
0.411713665944
0
validation finish
batch start
#iterations: 340
currently lose_sum: 502.310299128294
time_elpased: 1.707
batch start
#iterations: 341
currently lose_sum: 502.1530957221985
time_elpased: 1.737
batch start
#iterations: 342
currently lose_sum: 501.40272691845894
time_elpased: 1.708
batch start
#iterations: 343
currently lose_sum: 502.7040910124779
time_elpased: 1.717
batch start
#iterations: 344
currently lose_sum: 503.38541850447655
time_elpased: 1.716
batch start
#iterations: 345
currently lose_sum: 503.4216363430023
time_elpased: 1.703
batch start
#iterations: 346
currently lose_sum: 500.83513528108597
time_elpased: 1.719
batch start
#iterations: 347
currently lose_sum: 501.4937669634819
time_elpased: 1.729
batch start
#iterations: 348
currently lose_sum: 502.47043737769127
time_elpased: 1.709
batch start
#iterations: 349
currently lose_sum: 501.9761635065079
time_elpased: 1.711
batch start
#iterations: 350
currently lose_sum: 500.64190408587456
time_elpased: 1.704
batch start
#iterations: 351
currently lose_sum: 502.6816956400871
time_elpased: 1.696
batch start
#iterations: 352
currently lose_sum: 503.6595710217953
time_elpased: 1.706
batch start
#iterations: 353
currently lose_sum: 500.59759670495987
time_elpased: 1.717
batch start
#iterations: 354
currently lose_sum: 503.0247155427933
time_elpased: 1.715
batch start
#iterations: 355
currently lose_sum: 504.0697756111622
time_elpased: 1.726
batch start
#iterations: 356
currently lose_sum: 502.48890870809555
time_elpased: 1.704
batch start
#iterations: 357
currently lose_sum: 501.1108640730381
time_elpased: 1.746
batch start
#iterations: 358
currently lose_sum: 501.70886239409447
time_elpased: 1.698
batch start
#iterations: 359
currently lose_sum: 499.75951024889946
time_elpased: 1.782
start validation test
0.555154639175
0.690101434068
0.203046207677
0.313772264631
0
validation finish
batch start
#iterations: 360
currently lose_sum: 500.35729387402534
time_elpased: 1.716
batch start
#iterations: 361
currently lose_sum: 501.5018129646778
time_elpased: 1.705
batch start
#iterations: 362
currently lose_sum: 500.86860689520836
time_elpased: 1.704
batch start
#iterations: 363
currently lose_sum: 500.4423169493675
time_elpased: 1.679
batch start
#iterations: 364
currently lose_sum: 500.88580390810966
time_elpased: 1.72
batch start
#iterations: 365
currently lose_sum: 500.92170894145966
time_elpased: 1.702
batch start
#iterations: 366
currently lose_sum: 500.02277758717537
time_elpased: 1.768
batch start
#iterations: 367
currently lose_sum: 503.8299267590046
time_elpased: 1.729
batch start
#iterations: 368
currently lose_sum: 501.853323161602
time_elpased: 1.711
batch start
#iterations: 369
currently lose_sum: 499.8533755838871
time_elpased: 1.713
batch start
#iterations: 370
currently lose_sum: 501.4209314584732
time_elpased: 1.826
batch start
#iterations: 371
currently lose_sum: 499.3670172393322
time_elpased: 1.737
batch start
#iterations: 372
currently lose_sum: 501.8312628865242
time_elpased: 1.707
batch start
#iterations: 373
currently lose_sum: 503.16605162620544
time_elpased: 1.721
batch start
#iterations: 374
currently lose_sum: 499.9351815879345
time_elpased: 1.703
batch start
#iterations: 375
currently lose_sum: 504.5680314004421
time_elpased: 1.771
batch start
#iterations: 376
currently lose_sum: 500.44845792651176
time_elpased: 1.757
batch start
#iterations: 377
currently lose_sum: 500.4221956729889
time_elpased: 1.699
batch start
#iterations: 378
currently lose_sum: 503.44928497076035
time_elpased: 1.693
batch start
#iterations: 379
currently lose_sum: 498.5393805205822
time_elpased: 1.705
start validation test
0.552731958763
0.692592592593
0.19244622826
0.301199967786
0
validation finish
batch start
#iterations: 380
currently lose_sum: 500.9994966983795
time_elpased: 1.715
batch start
#iterations: 381
currently lose_sum: 500.82704281806946
time_elpased: 1.714
batch start
#iterations: 382
currently lose_sum: 500.43576914072037
time_elpased: 1.741
batch start
#iterations: 383
currently lose_sum: 503.2601608335972
time_elpased: 1.732
batch start
#iterations: 384
currently lose_sum: 501.9675417840481
time_elpased: 1.709
batch start
#iterations: 385
currently lose_sum: 501.0917755663395
time_elpased: 1.863
batch start
#iterations: 386
currently lose_sum: 502.3915307223797
time_elpased: 2.404
batch start
#iterations: 387
currently lose_sum: 501.0314590930939
time_elpased: 1.948
batch start
#iterations: 388
currently lose_sum: 501.2981389760971
time_elpased: 1.981
batch start
#iterations: 389
currently lose_sum: 500.3703570663929
time_elpased: 1.71
batch start
#iterations: 390
currently lose_sum: 501.3044354915619
time_elpased: 1.788
batch start
#iterations: 391
currently lose_sum: 500.97052785754204
time_elpased: 1.729
batch start
#iterations: 392
currently lose_sum: 500.3466257750988
time_elpased: 1.862
batch start
#iterations: 393
currently lose_sum: 502.3071773648262
time_elpased: 1.872
batch start
#iterations: 394
currently lose_sum: 500.62229853868484
time_elpased: 1.784
batch start
#iterations: 395
currently lose_sum: 502.5167854130268
time_elpased: 2.288
batch start
#iterations: 396
currently lose_sum: 500.55247071385384
time_elpased: 2.503
batch start
#iterations: 397
currently lose_sum: 500.4129240810871
time_elpased: 2.631
batch start
#iterations: 398
currently lose_sum: 500.75561225414276
time_elpased: 2.576
batch start
#iterations: 399
currently lose_sum: 500.202799141407
time_elpased: 1.816
start validation test
0.534690721649
0.699421965318
0.12452403005
0.211409102822
0
validation finish
acc: 0.594
pre: 0.683
rec: 0.353
F1: 0.466
auc: 0.000
