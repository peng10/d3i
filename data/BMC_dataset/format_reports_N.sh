#!/bin/tcsh
foreach sim_file (chem_Jacarrd_sim.csv enzyme_Jacarrd_sim.csv indication_Jacarrd_sim.csv offsideeffect_Jacarrd_sim.csv pathway_Jacarrd_sim.csv target_Jacarrd_sim.csv transporter_Jacarrd_sim.csv)
	foreach N (1N 2N)
		touch ./reports_N/$sim_file
		echo N: $N >> ./reports_N/$sim_file
		foreach att (0 1)
			echo att: $att >> ./reports_N/$sim_file
			if($att == 1) then
				foreach L (8 16 32 64)
					echo L: $L >> ./reports_N/$sim_file
					foreach folds (0 1 2 3 4)
						less ./simmilarity_effects_results/log_$sim_file\_$N\_attention_$att\_L_$L\_feature_50_1_$folds.txt | tail -5 > temp_$folds
					end
					paste temp_0 temp_1 temp_2 temp_3 temp_4 | awk '{mean = ($2+$4+$6+$8+$10)/5.0;sum=0;for(i=2;i<=10;i=i+2) {sum=sum+($i-mean)^2};std=sqrt(sum/4.0); print $1"\t"mean"\tstd:\t"std }' >> ./reports_N/$sim_file
					echo "" >> ./reports_N/$sim_file
				end
			else
				foreach folds (0 1 2 3 4)
					less ./simmilarity_effects_results/log_$sim_file\_$N\_attention_$att\_L_32_feature_50_1_$folds.txt | tail -5 > temp_$folds
				end
				paste temp_0 temp_1 temp_2 temp_3 temp_4 | awk '{mean = ($2+$4+$6+$8+$10)/5.0;sum=0;for(i=2;i<=10;i=i+2) {sum=sum+($i-mean)^2};std=sqrt(sum/4.0);print $1"\t"mean"\tstd:\t"std }' >> ./reports_N/$sim_file
				echo "" >> ./reports_N/$sim_file
			endif
		end
	end
end
