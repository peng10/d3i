import scipy.io as sio
import numpy as np
import random
import argparse
import pdb

path = './'
random.seed(123)
np.random.seed(123)

parser = argparse.ArgumentParser()
parser.add_argument('--num_sample', default = 29150, type = int)
parser.add_argument('--num_drugs', default = 548, type = int)
parser.add_argument('--train_file', type = str)
parser.add_argument('--freq_file',  type = str)
parser.add_argument('--outfile',  type = str)

args = parser.parse_args()

with open(path + args.freq_file, 'r') as f:
        pre = f.read().splitlines()

pre_t = [int(e.lstrip(' ').split(' ')[0]) for e in pre]
temp = sum(pre_t)
preb = [(float(e)/temp) for e in pre_t]
print len(pre_t)

drugId = [int(e.lstrip(' ').split(' ')[1]) for e in pre]
ID_drugId = {}
for i in range(len(drugId)):
	ID_drugId[i] = drugId[i]
#pdb.set_trace()

positive_triplets = []
with open(args.train_file, 'r') as f:
	for line in f:
		temp = line.rstrip('\n').split('\t')
		positive_triplets.append((temp[0], temp[1]))
data = np.array(positive_triplets, dtype=int)
result = np.zeros(shape=(args.num_drugs, args.num_drugs), dtype=int)
result[data[:,1], data[:,0]] = 1
result[data[:,0], data[:,1]] = 1

#pos_mat = np.loadtxt('drug_drug_matrix.csv', dtype = int, delimiter=',', skiprows=1, usecols=range(1,549))
candidate_list = []
lose_list = []
#pdb.set_trace()

counter = 0
for i in xrange(args.num_sample):
	counter_2 = 0
	while True:
		te1, te2 = np.random.choice(len(pre_t), 2, p = preb)
		drug_1 = ID_drugId[te1]
		drug_2 = ID_drugId[te2]
		counter_resample = 1
		while(drug_1 == drug_2 or (drug_1, drug_2) in candidate_list or (drug_2, drug_1) in candidate_list or (drug_1, drug_2) in lose_list):
			te1, te2 = np.random.choice(len(pre_t), 2, p = preb)
                        drug_1 = ID_drugId[te1]
                        drug_2 = ID_drugId[te2]
			counter_resample += 1
			if(counter_resample > 250000):
				print 'gg'
				break
		if(counter_resample > 250000):
                        break
		if(result[drug_1, drug_2] == 0):
			candidate_list.append((drug_1,drug_2))
			break
		else:
			lose_list.append((drug_1,drug_2))
			counter_2 += 1
		if(counter_2%1000 == 0):
			print 'counter_2' + str(counter_2)

	counter+=1
	if(counter % 5000 == 0):
        	print 'counter: ' + str(counter)

f = open(path + args.outfile,'w')
for e in candidate_list:
	#print e
	f.write('%d\t%d\t0\n' % (e[0], e[1]))
f.close()
