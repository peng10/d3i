start to construct graph
graph construct over
58300
epochs start
batch start
#iterations: 0
currently lose_sum: 405.52056980133057
time_elpased: 2.528
batch start
#iterations: 1
currently lose_sum: 396.6293833851814
time_elpased: 2.547
batch start
#iterations: 2
currently lose_sum: 387.7184201478958
time_elpased: 2.537
batch start
#iterations: 3
currently lose_sum: 379.9562714099884
time_elpased: 2.557
batch start
#iterations: 4
currently lose_sum: 367.9778336286545
time_elpased: 2.547
batch start
#iterations: 5
currently lose_sum: 355.4123661518097
time_elpased: 2.52
batch start
#iterations: 6
currently lose_sum: 336.054978787899
time_elpased: 2.551
batch start
#iterations: 7
currently lose_sum: 319.47149190306664
time_elpased: 2.522
batch start
#iterations: 8
currently lose_sum: 302.2443179488182
time_elpased: 2.542
batch start
#iterations: 9
currently lose_sum: 295.01801827549934
time_elpased: 2.566
start validation test
0.759845360825
0.738915779284
0.795518499218
0.76617314931
0.760227378115
validation finish
batch start
#iterations: 10
currently lose_sum: 284.97775849699974
time_elpased: 2.547
batch start
#iterations: 11
currently lose_sum: 272.8572898507118
time_elpased: 2.527
batch start
#iterations: 12
currently lose_sum: 268.98705622553825
time_elpased: 2.551
batch start
#iterations: 13
currently lose_sum: 260.07832941412926
time_elpased: 2.535
batch start
#iterations: 14
currently lose_sum: 259.6639798283577
time_elpased: 2.552
batch start
#iterations: 15
currently lose_sum: 254.6156586408615
time_elpased: 2.531
batch start
#iterations: 16
currently lose_sum: 256.02093210816383
time_elpased: 2.527
batch start
#iterations: 17
currently lose_sum: 250.20730182528496
time_elpased: 2.543
batch start
#iterations: 18
currently lose_sum: 247.61709940433502
time_elpased: 2.544
batch start
#iterations: 19
currently lose_sum: 244.20405128598213
time_elpased: 2.55
start validation test
0.792989690722
0.813532651456
0.754351224596
0.782825005408
0.792575918264
validation finish
batch start
#iterations: 20
currently lose_sum: 242.09522196650505
time_elpased: 2.535
batch start
#iterations: 21
currently lose_sum: 239.41225510835648
time_elpased: 2.556
batch start
#iterations: 22
currently lose_sum: 237.4627404808998
time_elpased: 2.533
batch start
#iterations: 23
currently lose_sum: 235.14945924282074
time_elpased: 2.537
batch start
#iterations: 24
currently lose_sum: 233.15102526545525
time_elpased: 2.555
batch start
#iterations: 25
currently lose_sum: 234.06722059845924
time_elpased: 2.53
batch start
#iterations: 26
currently lose_sum: 230.52550111711025
time_elpased: 2.532
batch start
#iterations: 27
currently lose_sum: 226.59916271269321
time_elpased: 2.53
batch start
#iterations: 28
currently lose_sum: 226.59081754088402
time_elpased: 2.551
batch start
#iterations: 29
currently lose_sum: 226.3856795579195
time_elpased: 2.517
start validation test
0.800051546392
0.801032230883
0.792600312663
0.796793965111
0.799971752456
validation finish
batch start
#iterations: 30
currently lose_sum: 224.38606891036034
time_elpased: 2.536
batch start
#iterations: 31
currently lose_sum: 224.05309310555458
time_elpased: 2.539
batch start
#iterations: 32
currently lose_sum: 221.01311680674553
time_elpased: 2.534
batch start
#iterations: 33
currently lose_sum: 222.35394670069218
time_elpased: 2.53
batch start
#iterations: 34
currently lose_sum: 219.4266430735588
time_elpased: 2.535
batch start
#iterations: 35
currently lose_sum: 216.77495095133781
time_elpased: 2.559
batch start
#iterations: 36
currently lose_sum: 216.93125377595425
time_elpased: 2.55
batch start
#iterations: 37
currently lose_sum: 212.84121637046337
time_elpased: 2.53
batch start
#iterations: 38
currently lose_sum: 215.0249273777008
time_elpased: 2.517
batch start
#iterations: 39
currently lose_sum: 213.16133970022202
time_elpased: 2.514
start validation test
0.798969072165
0.776107825075
0.834184471079
0.804098854732
0.799346187605
validation finish
batch start
#iterations: 40
currently lose_sum: 208.16827695071697
time_elpased: 2.55
batch start
#iterations: 41
currently lose_sum: 212.07345277071
time_elpased: 2.552
batch start
#iterations: 42
currently lose_sum: 209.08446940779686
time_elpased: 2.538
batch start
#iterations: 43
currently lose_sum: 207.48267896473408
time_elpased: 2.555
batch start
#iterations: 44
currently lose_sum: 207.89814595878124
time_elpased: 2.521
batch start
#iterations: 45
currently lose_sum: 205.9242890328169
time_elpased: 2.535
batch start
#iterations: 46
currently lose_sum: 203.19291116297245
time_elpased: 2.543
batch start
#iterations: 47
currently lose_sum: 202.10987113416195
time_elpased: 2.533
batch start
#iterations: 48
currently lose_sum: 199.8829020410776
time_elpased: 2.541
batch start
#iterations: 49
currently lose_sum: 199.70085902512074
time_elpased: 2.533
start validation test
0.814845360825
0.854242889177
0.754351224596
0.801195483728
0.814197539886
validation finish
batch start
#iterations: 50
currently lose_sum: 198.5250644236803
time_elpased: 2.542
batch start
#iterations: 51
currently lose_sum: 200.3052051961422
time_elpased: 2.526
batch start
#iterations: 52
currently lose_sum: 194.31330712139606
time_elpased: 2.533
batch start
#iterations: 53
currently lose_sum: 193.5784278512001
time_elpased: 2.508
batch start
#iterations: 54
currently lose_sum: 194.17359103262424
time_elpased: 2.53
batch start
#iterations: 55
currently lose_sum: 192.95838740468025
time_elpased: 2.529
batch start
#iterations: 56
currently lose_sum: 193.6852846890688
time_elpased: 2.538
batch start
#iterations: 57
currently lose_sum: 189.68947856128216
time_elpased: 2.557
batch start
#iterations: 58
currently lose_sum: 192.15373672544956
time_elpased: 2.531
batch start
#iterations: 59
currently lose_sum: 186.28105108439922
time_elpased: 2.532
start validation test
0.817010309278
0.837520938023
0.78165711308
0.808625336927
0.816631718192
validation finish
batch start
#iterations: 60
currently lose_sum: 184.82213319838047
time_elpased: 2.535
batch start
#iterations: 61
currently lose_sum: 183.24899752438068
time_elpased: 2.562
batch start
#iterations: 62
currently lose_sum: 180.52835509181023
time_elpased: 2.524
batch start
#iterations: 63
currently lose_sum: 181.0154389590025
time_elpased: 2.534
batch start
#iterations: 64
currently lose_sum: 180.53444522619247
time_elpased: 2.541
batch start
#iterations: 65
currently lose_sum: 180.6134735494852
time_elpased: 2.52
batch start
#iterations: 66
currently lose_sum: 177.5017143934965
time_elpased: 2.547
batch start
#iterations: 67
currently lose_sum: 179.0702712237835
time_elpased: 2.535
batch start
#iterations: 68
currently lose_sum: 177.42735643684864
time_elpased: 2.545
batch start
#iterations: 69
currently lose_sum: 171.64377476274967
time_elpased: 2.54
start validation test
0.812371134021
0.834964562943
0.773527879104
0.803072927938
0.811955168517
validation finish
batch start
#iterations: 70
currently lose_sum: 171.45210775732994
time_elpased: 2.538
batch start
#iterations: 71
currently lose_sum: 171.49150751531124
time_elpased: 2.622
batch start
#iterations: 72
currently lose_sum: 171.58342373371124
time_elpased: 2.58
batch start
#iterations: 73
currently lose_sum: 169.22742442786694
time_elpased: 2.542
batch start
#iterations: 74
currently lose_sum: 168.6219924390316
time_elpased: 2.54
batch start
#iterations: 75
currently lose_sum: 166.8803856819868
time_elpased: 2.523
batch start
#iterations: 76
currently lose_sum: 164.7899561971426
time_elpased: 2.538
batch start
#iterations: 77
currently lose_sum: 165.09431992471218
time_elpased: 2.568
batch start
#iterations: 78
currently lose_sum: 162.5438376367092
time_elpased: 2.549
batch start
#iterations: 79
currently lose_sum: 159.80153305083513
time_elpased: 2.556
start validation test
0.811701030928
0.855212816834
0.745492443981
0.796592237875
0.810992014953
validation finish
batch start
#iterations: 80
currently lose_sum: 159.2502623051405
time_elpased: 2.533
batch start
#iterations: 81
currently lose_sum: 156.8754484653473
time_elpased: 2.569
batch start
#iterations: 82
currently lose_sum: 155.05593520402908
time_elpased: 2.496
batch start
#iterations: 83
currently lose_sum: 154.40585041046143
time_elpased: 2.506
batch start
#iterations: 84
currently lose_sum: 152.88438975811005
time_elpased: 2.523
batch start
#iterations: 85
currently lose_sum: 152.31408061087132
time_elpased: 2.546
batch start
#iterations: 86
currently lose_sum: 150.7507637888193
time_elpased: 2.529
batch start
#iterations: 87
currently lose_sum: nan
time_elpased: 2.53
train finish final lose is: nan
acc: 0.817
pre: 0.841
rec: 0.784
F1: 0.811
auc: 0.817
