start to construct graph
graph construct over
58300
epochs start
batch start
#iterations: 0
currently lose_sum: 401.98456424474716
time_elpased: 1.923
batch start
#iterations: 1
currently lose_sum: 392.10356414318085
time_elpased: 1.901
batch start
#iterations: 2
currently lose_sum: 381.5220919251442
time_elpased: 1.961
batch start
#iterations: 3
currently lose_sum: 370.9176204800606
time_elpased: 2.021
batch start
#iterations: 4
currently lose_sum: 356.8438707590103
time_elpased: 1.97
batch start
#iterations: 5
currently lose_sum: 350.8142578303814
time_elpased: 2.011
batch start
#iterations: 6
currently lose_sum: 341.18172949552536
time_elpased: 1.978
batch start
#iterations: 7
currently lose_sum: 332.3966489434242
time_elpased: 1.887
batch start
#iterations: 8
currently lose_sum: 326.1752633154392
time_elpased: 1.897
batch start
#iterations: 9
currently lose_sum: 320.46156921982765
time_elpased: 1.888
batch start
#iterations: 10
currently lose_sum: 314.2028474211693
time_elpased: 1.887
batch start
#iterations: 11
currently lose_sum: 309.69152772426605
time_elpased: 1.891
batch start
#iterations: 12
currently lose_sum: 304.67004054784775
time_elpased: 1.904
batch start
#iterations: 13
currently lose_sum: 300.27787175774574
time_elpased: 1.961
batch start
#iterations: 14
currently lose_sum: 296.101778537035
time_elpased: 2.009
batch start
#iterations: 15
currently lose_sum: 290.7945330440998
time_elpased: 2.002
batch start
#iterations: 16
currently lose_sum: 291.16929319500923
time_elpased: 2.042
batch start
#iterations: 17
currently lose_sum: 285.63902112841606
time_elpased: 1.99
batch start
#iterations: 18
currently lose_sum: 286.7060263454914
time_elpased: 2.056
batch start
#iterations: 19
currently lose_sum: 283.373865455389
time_elpased: 1.967
batch start
#iterations: 20
currently lose_sum: 284.36897403001785
time_elpased: 2.028
batch start
#iterations: 21
currently lose_sum: 276.609962284565
time_elpased: 1.998
batch start
#iterations: 22
currently lose_sum: 273.9581378698349
time_elpased: 2.041
batch start
#iterations: 23
currently lose_sum: 272.63548332452774
time_elpased: 1.977
batch start
#iterations: 24
currently lose_sum: 270.6306732594967
time_elpased: 2.002
batch start
#iterations: 25
currently lose_sum: 270.27407309412956
time_elpased: 1.964
batch start
#iterations: 26
currently lose_sum: 271.03995499014854
time_elpased: 2.012
batch start
#iterations: 27
currently lose_sum: 268.9417105615139
time_elpased: 1.985
batch start
#iterations: 28
currently lose_sum: 268.4695866703987
time_elpased: 1.987
batch start
#iterations: 29
currently lose_sum: 269.1238679289818
time_elpased: 1.979
batch start
#iterations: 30
currently lose_sum: 261.6926099359989
time_elpased: 1.997
batch start
#iterations: 31
currently lose_sum: 266.42470875382423
time_elpased: 1.985
batch start
#iterations: 32
currently lose_sum: 264.5435252189636
time_elpased: 2.017
batch start
#iterations: 33
currently lose_sum: 259.35290709137917
time_elpased: 1.957
batch start
#iterations: 34
currently lose_sum: 262.32067519426346
time_elpased: 1.974
batch start
#iterations: 35
currently lose_sum: 259.48617857694626
time_elpased: 1.999
batch start
#iterations: 36
currently lose_sum: 260.93430346250534
time_elpased: 1.99
batch start
#iterations: 37
currently lose_sum: 256.839230209589
time_elpased: 1.919
batch start
#iterations: 38
currently lose_sum: 257.0684629380703
time_elpased: 1.961
batch start
#iterations: 39
currently lose_sum: 260.4168567061424
time_elpased: 1.97
start validation test
0.791855670103
0.807254229791
0.760812923398
0.783345852559
0.791523238853
validation finish
batch start
#iterations: 40
currently lose_sum: 254.74397119879723
time_elpased: 1.991
batch start
#iterations: 41
currently lose_sum: 254.48785331845284
time_elpased: 1.9
batch start
#iterations: 42
currently lose_sum: 255.26833978295326
time_elpased: 1.943
batch start
#iterations: 43
currently lose_sum: 250.80545035004616
time_elpased: 1.939
batch start
#iterations: 44
currently lose_sum: 250.8683044910431
time_elpased: 1.949
batch start
#iterations: 45
currently lose_sum: 249.01817336678505
time_elpased: 1.966
batch start
#iterations: 46
currently lose_sum: 247.57928922772408
time_elpased: 1.963
batch start
#iterations: 47
currently lose_sum: 249.31128779053688
time_elpased: 1.955
batch start
#iterations: 48
currently lose_sum: 248.69418323040009
time_elpased: 1.918
batch start
#iterations: 49
currently lose_sum: 250.2855605483055
time_elpased: 1.886
batch start
#iterations: 50
currently lose_sum: 248.55736926198006
time_elpased: 2.014
batch start
#iterations: 51
currently lose_sum: 246.42892920970917
time_elpased: 1.984
batch start
#iterations: 52
currently lose_sum: 246.7009518444538
time_elpased: 1.984
batch start
#iterations: 53
currently lose_sum: 244.46486541628838
time_elpased: 2.001
batch start
#iterations: 54
currently lose_sum: 245.80971437692642
time_elpased: 1.946
batch start
#iterations: 55
currently lose_sum: 242.21080484986305
time_elpased: 1.99
batch start
#iterations: 56
currently lose_sum: 243.96798686683178
time_elpased: 1.992
batch start
#iterations: 57
currently lose_sum: 242.5617468804121
time_elpased: 1.967
batch start
#iterations: 58
currently lose_sum: 243.39433199167252
time_elpased: 1.944
batch start
#iterations: 59
currently lose_sum: 241.9155366718769
time_elpased: 2.024
batch start
#iterations: 60
currently lose_sum: 239.97127157449722
time_elpased: 1.969
batch start
#iterations: 61
currently lose_sum: 241.61357110738754
time_elpased: 1.989
batch start
#iterations: 62
currently lose_sum: 243.96343058347702
time_elpased: 1.963
batch start
#iterations: 63
currently lose_sum: 241.12361669540405
time_elpased: 1.95
batch start
#iterations: 64
currently lose_sum: 241.13384625315666
time_elpased: 1.961
batch start
#iterations: 65
currently lose_sum: 239.72981250286102
time_elpased: 1.918
batch start
#iterations: 66
currently lose_sum: 241.70252537727356
time_elpased: 1.962
batch start
#iterations: 67
currently lose_sum: 240.46032750606537
time_elpased: 1.941
batch start
#iterations: 68
currently lose_sum: 240.70850551128387
time_elpased: 1.948
batch start
#iterations: 69
currently lose_sum: 241.13725399971008
time_elpased: 1.92
batch start
#iterations: 70
currently lose_sum: 239.87777757644653
time_elpased: 1.987
batch start
#iterations: 71
currently lose_sum: 239.08690118789673
time_elpased: 1.949
batch start
#iterations: 72
currently lose_sum: 239.36580234766006
time_elpased: 1.942
batch start
#iterations: 73
currently lose_sum: 240.85209175944328
time_elpased: 1.925
batch start
#iterations: 74
currently lose_sum: 239.20063400268555
time_elpased: 1.935
batch start
#iterations: 75
currently lose_sum: 237.0492994338274
time_elpased: 1.894
batch start
#iterations: 76
currently lose_sum: 237.1166351735592
time_elpased: 1.913
batch start
#iterations: 77
currently lose_sum: 234.7192513346672
time_elpased: 1.947
batch start
#iterations: 78
currently lose_sum: 238.36914804577827
time_elpased: 1.946
batch start
#iterations: 79
currently lose_sum: 238.81511244177818
time_elpased: 1.957
start validation test
0.805721649485
0.87998956431
0.703074517978
0.781646486299
0.804622419621
validation finish
batch start
#iterations: 80
currently lose_sum: 240.00246107578278
time_elpased: 1.96
batch start
#iterations: 81
currently lose_sum: 237.6312612593174
time_elpased: 1.935
batch start
#iterations: 82
currently lose_sum: 235.94334676861763
time_elpased: 1.919
batch start
#iterations: 83
currently lose_sum: 236.91638788580894
time_elpased: 1.938
batch start
#iterations: 84
currently lose_sum: 235.73579287528992
time_elpased: 1.919
batch start
#iterations: 85
currently lose_sum: 235.86566284298897
time_elpased: 1.942
batch start
#iterations: 86
currently lose_sum: 233.57378135621548
time_elpased: 1.917
batch start
#iterations: 87
currently lose_sum: 234.25139845907688
time_elpased: 1.962
batch start
#iterations: 88
currently lose_sum: 231.5957018584013
time_elpased: 1.981
batch start
#iterations: 89
currently lose_sum: 233.12019750475883
time_elpased: 1.933
batch start
#iterations: 90
currently lose_sum: 234.66400408744812
time_elpased: 1.93
batch start
#iterations: 91
currently lose_sum: 235.25829169154167
time_elpased: 1.933
batch start
#iterations: 92
currently lose_sum: 233.14169165492058
time_elpased: 1.927
batch start
#iterations: 93
currently lose_sum: 232.83767721056938
time_elpased: 1.936
batch start
#iterations: 94
currently lose_sum: 230.77265059947968
time_elpased: 1.913
batch start
#iterations: 95
currently lose_sum: 229.27717012166977
time_elpased: 1.913
batch start
#iterations: 96
currently lose_sum: 231.3381425589323
time_elpased: 1.927
batch start
#iterations: 97
currently lose_sum: 229.03195582330227
time_elpased: 1.905
batch start
#iterations: 98
currently lose_sum: 229.95839509367943
time_elpased: 1.94
batch start
#iterations: 99
currently lose_sum: 231.8180824816227
time_elpased: 1.898
batch start
#iterations: 100
currently lose_sum: 232.33590266108513
time_elpased: 1.915
batch start
#iterations: 101
currently lose_sum: 231.05453181266785
time_elpased: 1.931
batch start
#iterations: 102
currently lose_sum: 229.879797488451
time_elpased: 1.895
batch start
#iterations: 103
currently lose_sum: 229.04426646232605
time_elpased: 1.892
batch start
#iterations: 104
currently lose_sum: 232.1757686138153
time_elpased: 1.913
batch start
#iterations: 105
currently lose_sum: 229.73637709021568
time_elpased: 1.933
batch start
#iterations: 106
currently lose_sum: 229.30218905210495
time_elpased: 1.924
batch start
#iterations: 107
currently lose_sum: 230.05570724606514
time_elpased: 1.907
batch start
#iterations: 108
currently lose_sum: 229.87603241205215
time_elpased: 1.918
batch start
#iterations: 109
currently lose_sum: 228.42581191658974
time_elpased: 1.889
batch start
#iterations: 110
currently lose_sum: 227.7402041554451
time_elpased: 1.928
batch start
#iterations: 111
currently lose_sum: 230.8151532113552
time_elpased: 1.937
batch start
#iterations: 112
currently lose_sum: 229.35415245592594
time_elpased: 1.924
batch start
#iterations: 113
currently lose_sum: 228.58221562206745
time_elpased: 1.918
batch start
#iterations: 114
currently lose_sum: 230.6531514674425
time_elpased: 1.901
batch start
#iterations: 115
currently lose_sum: 226.6604716181755
time_elpased: 1.954
batch start
#iterations: 116
currently lose_sum: 228.48024007678032
time_elpased: 1.966
batch start
#iterations: 117
currently lose_sum: 225.96552351117134
time_elpased: 1.911
batch start
#iterations: 118
currently lose_sum: 228.95292676985264
time_elpased: 1.925
batch start
#iterations: 119
currently lose_sum: 227.8918722420931
time_elpased: 1.92
start validation test
0.811288659794
0.85876662636
0.740177175612
0.795074167366
0.810527139565
validation finish
batch start
#iterations: 120
currently lose_sum: 226.85346102714539
time_elpased: 1.927
batch start
#iterations: 121
currently lose_sum: 228.16394692659378
time_elpased: 1.936
batch start
#iterations: 122
currently lose_sum: 227.6302945315838
time_elpased: 1.944
batch start
#iterations: 123
currently lose_sum: 227.59860852360725
time_elpased: 1.904
batch start
#iterations: 124
currently lose_sum: 224.97372901439667
time_elpased: 1.926
batch start
#iterations: 125
currently lose_sum: 224.51004126667976
time_elpased: 1.972
batch start
#iterations: 126
currently lose_sum: 225.75423055887222
time_elpased: 1.944
batch start
#iterations: 127
currently lose_sum: 226.85056993365288
time_elpased: 1.947
batch start
#iterations: 128
currently lose_sum: 224.36311230063438
time_elpased: 1.929
batch start
#iterations: 129
currently lose_sum: 224.7864110171795
time_elpased: 1.927
batch start
#iterations: 130
currently lose_sum: 224.03179705142975
time_elpased: 1.996
batch start
#iterations: 131
currently lose_sum: 227.15232890844345
time_elpased: 1.98
batch start
#iterations: 132
currently lose_sum: 226.40007081627846
time_elpased: 1.939
batch start
#iterations: 133
currently lose_sum: 223.05029341578484
time_elpased: 1.969
batch start
#iterations: 134
currently lose_sum: 225.39383426308632
time_elpased: 1.963
batch start
#iterations: 135
currently lose_sum: 226.62207821011543
time_elpased: 1.99
batch start
#iterations: 136
currently lose_sum: 224.36325252056122
time_elpased: 1.947
batch start
#iterations: 137
currently lose_sum: 222.36413502693176
time_elpased: 1.932
batch start
#iterations: 138
currently lose_sum: 223.37043990194798
time_elpased: 1.933
batch start
#iterations: 139
currently lose_sum: 222.2283505499363
time_elpased: 1.949
batch start
#iterations: 140
currently lose_sum: 222.50313186645508
time_elpased: 1.941
batch start
#iterations: 141
currently lose_sum: 224.36628119647503
time_elpased: 1.962
batch start
#iterations: 142
currently lose_sum: 219.17905615270138
time_elpased: 1.991
batch start
#iterations: 143
currently lose_sum: 222.47256380319595
time_elpased: 1.95
batch start
#iterations: 144
currently lose_sum: 220.7776416540146
time_elpased: 1.982
batch start
#iterations: 145
currently lose_sum: 220.3772453069687
time_elpased: 1.972
batch start
#iterations: 146
currently lose_sum: 222.0257585644722
time_elpased: 1.946
batch start
#iterations: 147
currently lose_sum: 221.37680013477802
time_elpased: 1.977
batch start
#iterations: 148
currently lose_sum: 219.329061627388
time_elpased: 1.964
batch start
#iterations: 149
currently lose_sum: 219.45387330651283
time_elpased: 1.95
batch start
#iterations: 150
currently lose_sum: 219.7278575003147
time_elpased: 1.959
batch start
#iterations: 151
currently lose_sum: 219.6070036292076
time_elpased: 1.95
batch start
#iterations: 152
currently lose_sum: 220.07364009320736
time_elpased: 1.951
batch start
#iterations: 153
currently lose_sum: 221.79769711196423
time_elpased: 1.963
batch start
#iterations: 154
currently lose_sum: 218.735871180892
time_elpased: 1.957
batch start
#iterations: 155
currently lose_sum: 220.234878718853
time_elpased: 1.951
batch start
#iterations: 156
currently lose_sum: 218.00464196503162
time_elpased: 1.967
batch start
#iterations: 157
currently lose_sum: 215.04550813138485
time_elpased: 2.026
batch start
#iterations: 158
currently lose_sum: 219.61474333703518
time_elpased: 1.955
batch start
#iterations: 159
currently lose_sum: 219.28436601161957
time_elpased: 1.969
start validation test
0.815824742268
0.844586861982
0.76915059927
0.805105547374
0.815324917177
validation finish
batch start
#iterations: 160
currently lose_sum: 216.27682834863663
time_elpased: 1.987
batch start
#iterations: 161
currently lose_sum: 216.28997744619846
time_elpased: 2.023
batch start
#iterations: 162
currently lose_sum: 215.48219937086105
time_elpased: 1.951
batch start
#iterations: 163
currently lose_sum: 216.70928463339806
time_elpased: 1.985
batch start
#iterations: 164
currently lose_sum: 215.37676618993282
time_elpased: 1.91
batch start
#iterations: 165
currently lose_sum: 217.7799221277237
time_elpased: 1.97
batch start
#iterations: 166
currently lose_sum: 218.10728658735752
time_elpased: 1.968
batch start
#iterations: 167
currently lose_sum: 217.4137224406004
time_elpased: 1.905
batch start
#iterations: 168
currently lose_sum: 214.7328314334154
time_elpased: 1.917
batch start
#iterations: 169
currently lose_sum: 214.7075390815735
time_elpased: 1.984
batch start
#iterations: 170
currently lose_sum: 218.52149268984795
time_elpased: 1.972
batch start
#iterations: 171
currently lose_sum: 215.80040274560452
time_elpased: 1.967
batch start
#iterations: 172
currently lose_sum: 214.03543919324875
time_elpased: 1.961
batch start
#iterations: 173
currently lose_sum: 215.36805091798306
time_elpased: 1.964
batch start
#iterations: 174
currently lose_sum: 216.0571717619896
time_elpased: 1.957
batch start
#iterations: 175
currently lose_sum: 213.90221318602562
time_elpased: 2.041
batch start
#iterations: 176
currently lose_sum: 214.6256905645132
time_elpased: 1.982
batch start
#iterations: 177
currently lose_sum: 214.3352316468954
time_elpased: 1.995
batch start
#iterations: 178
currently lose_sum: 212.16851748526096
time_elpased: 1.957
batch start
#iterations: 179
currently lose_sum: 213.4471464306116
time_elpased: 1.986
batch start
#iterations: 180
currently lose_sum: 214.77051323652267
time_elpased: 1.968
batch start
#iterations: 181
currently lose_sum: 212.3794010579586
time_elpased: 1.993
batch start
#iterations: 182
currently lose_sum: 212.7466753423214
time_elpased: 1.978
batch start
#iterations: 183
currently lose_sum: 214.96086114645004
time_elpased: 2.029
batch start
#iterations: 184
currently lose_sum: 214.5665688365698
time_elpased: 1.955
batch start
#iterations: 185
currently lose_sum: 213.23773708939552
time_elpased: 1.947
batch start
#iterations: 186
currently lose_sum: 212.94006583094597
time_elpased: 1.955
batch start
#iterations: 187
currently lose_sum: 212.55507437884808
time_elpased: 1.968
batch start
#iterations: 188
currently lose_sum: 212.2994356751442
time_elpased: 1.985
batch start
#iterations: 189
currently lose_sum: 211.87992519140244
time_elpased: 2.037
batch start
#iterations: 190
currently lose_sum: 211.09944768249989
time_elpased: 1.993
batch start
#iterations: 191
currently lose_sum: 211.5803885757923
time_elpased: 2.014
batch start
#iterations: 192
currently lose_sum: 210.32434061169624
time_elpased: 1.939
batch start
#iterations: 193
currently lose_sum: 210.86426684260368
time_elpased: 1.899
batch start
#iterations: 194
currently lose_sum: 213.02593363821507
time_elpased: 1.913
batch start
#iterations: 195
currently lose_sum: 212.7565541267395
time_elpased: 1.897
batch start
#iterations: 196
currently lose_sum: 211.68420706689358
time_elpased: 1.873
batch start
#iterations: 197
currently lose_sum: 210.43607385456562
time_elpased: 1.9
batch start
#iterations: 198
currently lose_sum: 209.50108429789543
time_elpased: 1.877
batch start
#iterations: 199
currently lose_sum: 211.0896925777197
time_elpased: 1.957
start validation test
0.817680412371
0.844439390494
0.773944762897
0.807656751319
0.817212055085
validation finish
batch start
#iterations: 200
currently lose_sum: 209.81718634068966
time_elpased: 1.897
batch start
#iterations: 201
currently lose_sum: 210.09739263355732
time_elpased: 2.008
batch start
#iterations: 202
currently lose_sum: 210.97735050320625
time_elpased: 2.017
batch start
#iterations: 203
currently lose_sum: 207.20568312704563
time_elpased: 1.989
batch start
#iterations: 204
currently lose_sum: 208.07905441522598
time_elpased: 1.925
batch start
#iterations: 205
currently lose_sum: 209.34793232381344
time_elpased: 1.892
batch start
#iterations: 206
currently lose_sum: 209.0536125600338
time_elpased: 1.955
batch start
#iterations: 207
currently lose_sum: 208.1279006153345
time_elpased: 1.952
batch start
#iterations: 208
currently lose_sum: 208.5909785181284
time_elpased: 1.906
batch start
#iterations: 209
currently lose_sum: 209.36115989089012
time_elpased: 1.913
batch start
#iterations: 210
currently lose_sum: 207.23769709467888
time_elpased: 1.888
batch start
#iterations: 211
currently lose_sum: 209.2411272227764
time_elpased: 1.895
batch start
#iterations: 212
currently lose_sum: 207.56002748012543
time_elpased: 1.879
batch start
#iterations: 213
currently lose_sum: 210.57692031562328
time_elpased: 1.894
batch start
#iterations: 214
currently lose_sum: 208.77690900862217
time_elpased: 1.916
batch start
#iterations: 215
currently lose_sum: 206.93643260002136
time_elpased: 1.891
batch start
#iterations: 216
currently lose_sum: 204.23409135639668
time_elpased: 1.89
batch start
#iterations: 217
currently lose_sum: 206.75311395525932
time_elpased: 1.87
batch start
#iterations: 218
currently lose_sum: 206.04800382256508
time_elpased: 1.898
batch start
#iterations: 219
currently lose_sum: 207.32358613610268
time_elpased: 1.885
batch start
#iterations: 220
currently lose_sum: 207.18777760863304
time_elpased: 1.899
batch start
#iterations: 221
currently lose_sum: 207.23638172447681
time_elpased: 1.905
batch start
#iterations: 222
currently lose_sum: 206.0261372178793
time_elpased: 1.919
batch start
#iterations: 223
currently lose_sum: 205.53847804665565
time_elpased: 1.933
batch start
#iterations: 224
currently lose_sum: 206.99618235230446
time_elpased: 1.936
batch start
#iterations: 225
currently lose_sum: 204.77829356491566
time_elpased: 1.937
batch start
#iterations: 226
currently lose_sum: 206.23697972297668
time_elpased: 1.957
batch start
#iterations: 227
currently lose_sum: 204.678259447217
time_elpased: 1.938
batch start
#iterations: 228
currently lose_sum: 206.05408862233162
time_elpased: 1.945
batch start
#iterations: 229
currently lose_sum: 205.39475986361504
time_elpased: 1.928
batch start
#iterations: 230
currently lose_sum: 206.64822652935982
time_elpased: 1.983
batch start
#iterations: 231
currently lose_sum: 203.405125066638
time_elpased: 1.949
batch start
#iterations: 232
currently lose_sum: 205.79998061060905
time_elpased: 1.961
batch start
#iterations: 233
currently lose_sum: 204.21823219954967
time_elpased: 1.935
batch start
#iterations: 234
currently lose_sum: 204.9920316785574
time_elpased: 1.991
batch start
#iterations: 235
currently lose_sum: 203.4133678972721
time_elpased: 1.949
batch start
#iterations: 236
currently lose_sum: 202.0366566926241
time_elpased: 2.009
batch start
#iterations: 237
currently lose_sum: 205.06946715712547
time_elpased: 1.94
batch start
#iterations: 238
currently lose_sum: 204.58365273475647
time_elpased: 1.992
batch start
#iterations: 239
currently lose_sum: 202.30433447659016
time_elpased: 1.966
start validation test
0.82206185567
0.850507816958
0.776758728504
0.811962087373
0.821576712544
validation finish
batch start
#iterations: 240
currently lose_sum: 201.435226932168
time_elpased: 1.991
batch start
#iterations: 241
currently lose_sum: 203.18217647075653
time_elpased: 1.911
batch start
#iterations: 242
currently lose_sum: 201.74623818695545
time_elpased: 1.984
batch start
#iterations: 243
currently lose_sum: 203.00939577817917
time_elpased: 1.938
batch start
#iterations: 244
currently lose_sum: 203.6203205883503
time_elpased: 1.97
batch start
#iterations: 245
currently lose_sum: 203.5522716343403
time_elpased: 1.939
batch start
#iterations: 246
currently lose_sum: 202.16688457131386
time_elpased: 1.982
batch start
#iterations: 247
currently lose_sum: 203.4259921759367
time_elpased: 1.977
batch start
#iterations: 248
currently lose_sum: 202.64082692563534
time_elpased: 1.97
batch start
#iterations: 249
currently lose_sum: 202.58588089048862
time_elpased: 1.933
batch start
#iterations: 250
currently lose_sum: 200.2794370651245
time_elpased: 2.011
batch start
#iterations: 251
currently lose_sum: 201.23823049664497
time_elpased: 1.986
batch start
#iterations: 252
currently lose_sum: 202.77835036814213
time_elpased: 1.945
batch start
#iterations: 253
currently lose_sum: 201.11483846604824
time_elpased: 1.966
batch start
#iterations: 254
currently lose_sum: 199.8610838651657
time_elpased: 1.996
batch start
#iterations: 255
currently lose_sum: 202.9034796655178
time_elpased: 1.968
batch start
#iterations: 256
currently lose_sum: 202.3920756727457
time_elpased: 1.949
batch start
#iterations: 257
currently lose_sum: 201.8662880808115
time_elpased: 1.964
batch start
#iterations: 258
currently lose_sum: 200.75666645169258
time_elpased: 2.006
batch start
#iterations: 259
currently lose_sum: 201.13862624764442
time_elpased: 1.969
batch start
#iterations: 260
currently lose_sum: 200.24143961071968
time_elpased: 1.971
batch start
#iterations: 261
currently lose_sum: 199.25387912988663
time_elpased: 1.973
batch start
#iterations: 262
currently lose_sum: 199.74850675463676
time_elpased: 1.998
batch start
#iterations: 263
currently lose_sum: 199.9520896524191
time_elpased: 1.992
batch start
#iterations: 264
currently lose_sum: 201.49412254989147
time_elpased: 1.987
batch start
#iterations: 265
currently lose_sum: 200.90064206719398
time_elpased: 1.95
batch start
#iterations: 266
currently lose_sum: 200.43244898319244
time_elpased: 1.974
batch start
#iterations: 267
currently lose_sum: 201.5786031037569
time_elpased: 1.971
batch start
#iterations: 268
currently lose_sum: 200.07858976721764
time_elpased: 1.989
batch start
#iterations: 269
currently lose_sum: 201.5524572879076
time_elpased: 1.961
batch start
#iterations: 270
currently lose_sum: 200.02529248595238
time_elpased: 1.997
batch start
#iterations: 271
currently lose_sum: 198.61057744920254
time_elpased: 1.998
batch start
#iterations: 272
currently lose_sum: 200.40678910911083
time_elpased: 1.955
batch start
#iterations: 273
currently lose_sum: 199.99977008998394
time_elpased: 1.96
batch start
#iterations: 274
currently lose_sum: 201.51401463150978
time_elpased: 2.01
batch start
#iterations: 275
currently lose_sum: 199.85098619759083
time_elpased: 1.981
batch start
#iterations: 276
currently lose_sum: 198.24913428723812
time_elpased: 1.988
batch start
#iterations: 277
currently lose_sum: 200.76719610393047
time_elpased: 1.97
batch start
#iterations: 278
currently lose_sum: 199.94718600809574
time_elpased: 1.962
batch start
#iterations: 279
currently lose_sum: 195.5382608473301
time_elpased: 2.023
start validation test
0.82381443299
0.84651632447
0.786347055758
0.815323103523
0.823413201515
validation finish
batch start
#iterations: 280
currently lose_sum: 198.1847848445177
time_elpased: 1.956
batch start
#iterations: 281
currently lose_sum: 196.71210025250912
time_elpased: 1.97
batch start
#iterations: 282
currently lose_sum: 195.51716388761997
time_elpased: 1.959
batch start
#iterations: 283
currently lose_sum: 198.51974269747734
time_elpased: 1.969
batch start
#iterations: 284
currently lose_sum: 198.41882714629173
time_elpased: 1.958
batch start
#iterations: 285
currently lose_sum: 198.78194953501225
time_elpased: 1.944
batch start
#iterations: 286
currently lose_sum: 196.94615578651428
time_elpased: 1.957
batch start
#iterations: 287
currently lose_sum: 197.17119753360748
time_elpased: 1.976
batch start
#iterations: 288
currently lose_sum: 196.84787711501122
time_elpased: 2.005
batch start
#iterations: 289
currently lose_sum: 197.6503345221281
time_elpased: 1.941
batch start
#iterations: 290
currently lose_sum: 196.2639643251896
time_elpased: 1.958
batch start
#iterations: 291
currently lose_sum: 197.20341949164867
time_elpased: 1.964
batch start
#iterations: 292
currently lose_sum: 192.41727973520756
time_elpased: 1.949
batch start
#iterations: 293
currently lose_sum: 196.68138626217842
time_elpased: 2.019
batch start
#iterations: 294
currently lose_sum: 196.2455792427063
time_elpased: 1.961
batch start
#iterations: 295
currently lose_sum: 196.07383200526237
time_elpased: 1.988
batch start
#iterations: 296
currently lose_sum: 195.51002492010593
time_elpased: 1.961
batch start
#iterations: 297
currently lose_sum: 194.20252403616905
time_elpased: 2.056
batch start
#iterations: 298
currently lose_sum: 196.90386568009853
time_elpased: 1.935
batch start
#iterations: 299
currently lose_sum: 194.9840747565031
time_elpased: 2.012
batch start
#iterations: 300
currently lose_sum: 195.88487981259823
time_elpased: 1.948
batch start
#iterations: 301
currently lose_sum: 194.0039498358965
time_elpased: 2.018
batch start
#iterations: 302
currently lose_sum: 198.34378656744957
time_elpased: 1.981
batch start
#iterations: 303
currently lose_sum: 193.47077651321888
time_elpased: 2.002
batch start
#iterations: 304
currently lose_sum: 196.68601356446743
time_elpased: 1.957
batch start
#iterations: 305
currently lose_sum: 196.27453818917274
time_elpased: 2.039
batch start
#iterations: 306
currently lose_sum: 193.54443754255772
time_elpased: 1.985
batch start
#iterations: 307
currently lose_sum: 196.3437893241644
time_elpased: 2.033
batch start
#iterations: 308
currently lose_sum: 192.47021225094795
time_elpased: 1.964
batch start
#iterations: 309
currently lose_sum: 192.5265722721815
time_elpased: 1.993
batch start
#iterations: 310
currently lose_sum: 194.38535085320473
time_elpased: 1.945
batch start
#iterations: 311
currently lose_sum: 193.85773119330406
time_elpased: 2.097
batch start
#iterations: 312
currently lose_sum: 191.52211919426918
time_elpased: 1.958
batch start
#iterations: 313
currently lose_sum: 195.46291762590408
time_elpased: 2.045
batch start
#iterations: 314
currently lose_sum: 192.9467897117138
time_elpased: 1.972
batch start
#iterations: 315
currently lose_sum: 195.01619611680508
time_elpased: 2.009
batch start
#iterations: 316
currently lose_sum: 195.98983559012413
time_elpased: 2.002
batch start
#iterations: 317
currently lose_sum: 193.19751389324665
time_elpased: 2.009
batch start
#iterations: 318
currently lose_sum: 193.45353384315968
time_elpased: 1.982
batch start
#iterations: 319
currently lose_sum: 194.34743696451187
time_elpased: 1.967
start validation test
0.817680412371
0.830965909091
0.792600312663
0.81132981277
0.817411834047
validation finish
batch start
#iterations: 320
currently lose_sum: 193.11911496520042
time_elpased: 1.982
batch start
#iterations: 321
currently lose_sum: 191.63292218744755
time_elpased: 1.971
batch start
#iterations: 322
currently lose_sum: 191.05601234734058
time_elpased: 1.982
batch start
#iterations: 323
currently lose_sum: 192.7095751017332
time_elpased: 1.974
batch start
#iterations: 324
currently lose_sum: 191.8421856611967
time_elpased: 1.977
batch start
#iterations: 325
currently lose_sum: 192.08379723131657
time_elpased: 1.959
batch start
#iterations: 326
currently lose_sum: 194.36642196774483
time_elpased: 1.985
batch start
#iterations: 327
currently lose_sum: 193.94584043323994
time_elpased: 1.965
batch start
#iterations: 328
currently lose_sum: 192.22328539192677
time_elpased: 2.008
batch start
#iterations: 329
currently lose_sum: 190.37522481381893
time_elpased: 1.978
batch start
#iterations: 330
currently lose_sum: 190.54282176494598
time_elpased: 1.955
batch start
#iterations: 331
currently lose_sum: 192.73130236566067
time_elpased: 2.024
batch start
#iterations: 332
currently lose_sum: 193.5746846795082
time_elpased: 1.983
batch start
#iterations: 333
currently lose_sum: 192.21340470016003
time_elpased: 2.007
batch start
#iterations: 334
currently lose_sum: 190.88877601921558
time_elpased: 1.943
batch start
#iterations: 335
currently lose_sum: 192.75740848481655
time_elpased: 1.989
batch start
#iterations: 336
currently lose_sum: 189.03571720421314
time_elpased: 1.979
batch start
#iterations: 337
currently lose_sum: 190.02499479055405
time_elpased: 2.0
batch start
#iterations: 338
currently lose_sum: 191.36298397183418
time_elpased: 2.029
batch start
#iterations: 339
currently lose_sum: 189.39518804848194
time_elpased: 1.992
batch start
#iterations: 340
currently lose_sum: 191.82026936113834
time_elpased: 1.953
batch start
#iterations: 341
currently lose_sum: 193.1979599148035
time_elpased: 2.004
batch start
#iterations: 342
currently lose_sum: 190.28017012774944
time_elpased: 1.976
batch start
#iterations: 343
currently lose_sum: 189.98286393284798
time_elpased: 2.015
batch start
#iterations: 344
currently lose_sum: 190.60994984209538
time_elpased: 1.935
batch start
#iterations: 345
currently lose_sum: 190.48694616556168
time_elpased: 1.969
batch start
#iterations: 346
currently lose_sum: 190.07286994159222
time_elpased: 1.937
batch start
#iterations: 347
currently lose_sum: 190.29945930838585
time_elpased: 2.015
batch start
#iterations: 348
currently lose_sum: 189.31303565204144
time_elpased: 1.981
batch start
#iterations: 349
currently lose_sum: 191.47226426005363
time_elpased: 1.958
batch start
#iterations: 350
currently lose_sum: 189.12819942831993
time_elpased: 1.933
batch start
#iterations: 351
currently lose_sum: 189.9702465236187
time_elpased: 1.996
batch start
#iterations: 352
currently lose_sum: 189.2877233326435
time_elpased: 1.938
batch start
#iterations: 353
currently lose_sum: 190.2244484424591
time_elpased: 2.002
batch start
#iterations: 354
currently lose_sum: 189.42191156744957
time_elpased: 1.989
batch start
#iterations: 355
currently lose_sum: 190.85542425513268
time_elpased: 2.015
batch start
#iterations: 356
currently lose_sum: 190.5040904134512
time_elpased: 1.976
batch start
#iterations: 357
currently lose_sum: 188.63351845741272
time_elpased: 1.995
batch start
#iterations: 358
currently lose_sum: 188.51208497583866
time_elpased: 1.997
batch start
#iterations: 359
currently lose_sum: 187.36580955982208
time_elpased: 2.012
start validation test
0.820979381443
0.849509020324
0.775403856175
0.810766632158
0.820491321254
validation finish
batch start
#iterations: 360
currently lose_sum: 190.03361156582832
time_elpased: 1.998
batch start
#iterations: 361
currently lose_sum: 187.76525788009167
time_elpased: 1.978
batch start
#iterations: 362
currently lose_sum: 188.72268380224705
time_elpased: 1.924
batch start
#iterations: 363
currently lose_sum: 189.34071139991283
time_elpased: 2.011
batch start
#iterations: 364
currently lose_sum: 188.9554782062769
time_elpased: 1.967
batch start
#iterations: 365
currently lose_sum: 187.97000375390053
time_elpased: 2.032
batch start
#iterations: 366
currently lose_sum: 187.15590608119965
time_elpased: 1.969
batch start
#iterations: 367
currently lose_sum: 186.28626677393913
time_elpased: 2.044
batch start
#iterations: 368
currently lose_sum: 188.37834538519382
time_elpased: 1.962
batch start
#iterations: 369
currently lose_sum: 188.6315575838089
time_elpased: 1.98
batch start
#iterations: 370
currently lose_sum: 188.19776417315006
time_elpased: 1.925
batch start
#iterations: 371
currently lose_sum: 186.71775750815868
time_elpased: 1.994
batch start
#iterations: 372
currently lose_sum: 189.0490266084671
time_elpased: 1.964
batch start
#iterations: 373
currently lose_sum: 187.59840412437916
time_elpased: 2.007
batch start
#iterations: 374
currently lose_sum: 187.31322447955608
time_elpased: 1.971
batch start
#iterations: 375
currently lose_sum: 188.1562084853649
time_elpased: 2.046
batch start
#iterations: 376
currently lose_sum: 188.02553236484528
time_elpased: 1.94
batch start
#iterations: 377
currently lose_sum: 188.31416761875153
time_elpased: 2.029
batch start
#iterations: 378
currently lose_sum: 187.73188684880733
time_elpased: 1.951
batch start
#iterations: 379
currently lose_sum: 187.54190549254417
time_elpased: 2.029
batch start
#iterations: 380
currently lose_sum: 187.18353217840195
time_elpased: 1.981
batch start
#iterations: 381
currently lose_sum: 185.6795696169138
time_elpased: 2.027
batch start
#iterations: 382
currently lose_sum: 185.75302705168724
time_elpased: 1.963
batch start
#iterations: 383
currently lose_sum: 188.24641655385494
time_elpased: 2.054
batch start
#iterations: 384
currently lose_sum: 188.57184581458569
time_elpased: 1.98
batch start
#iterations: 385
currently lose_sum: 187.9539639055729
time_elpased: 2.028
batch start
#iterations: 386
currently lose_sum: 188.66848689317703
time_elpased: 1.96
batch start
#iterations: 387
currently lose_sum: 187.62921027839184
time_elpased: 2.038
batch start
#iterations: 388
currently lose_sum: 187.07462519407272
time_elpased: 2.001
batch start
#iterations: 389
currently lose_sum: 186.56261685490608
time_elpased: 2.056
batch start
#iterations: 390
currently lose_sum: 182.7803605645895
time_elpased: 2.033
batch start
#iterations: 391
currently lose_sum: 186.53881664574146
time_elpased: 2.011
batch start
#iterations: 392
currently lose_sum: 184.56723377108574
time_elpased: 1.945
batch start
#iterations: 393
currently lose_sum: 183.72873164713383
time_elpased: 2.009
batch start
#iterations: 394
currently lose_sum: 184.85454387962818
time_elpased: 1.962
batch start
#iterations: 395
currently lose_sum: 185.02832047641277
time_elpased: 2.058
batch start
#iterations: 396
currently lose_sum: 185.34086123108864
time_elpased: 2.011
batch start
#iterations: 397
currently lose_sum: 186.01271553337574
time_elpased: 2.056
batch start
#iterations: 398
currently lose_sum: 187.24864377081394
time_elpased: 1.99
batch start
#iterations: 399
currently lose_sum: 185.13492193818092
time_elpased: 1.997
start validation test
0.822216494845
0.853787704352
0.772902553413
0.811334172091
0.821688400623
validation finish
batch start
#iterations: 400
currently lose_sum: 186.56172055006027
time_elpased: 1.97
batch start
#iterations: 401
currently lose_sum: 185.73579366505146
time_elpased: 2.001
batch start
#iterations: 402
currently lose_sum: 184.4222213625908
time_elpased: 1.974
batch start
#iterations: 403
currently lose_sum: 184.09699633717537
time_elpased: 2.005
batch start
#iterations: 404
currently lose_sum: 184.95645546913147
time_elpased: 1.996
batch start
#iterations: 405
currently lose_sum: 183.95220917463303
time_elpased: 2.001
batch start
#iterations: 406
currently lose_sum: 185.58865213394165
time_elpased: 1.981
batch start
#iterations: 407
currently lose_sum: 185.10874339938164
time_elpased: 1.973
batch start
#iterations: 408
currently lose_sum: 185.38234904408455
time_elpased: 1.994
batch start
#iterations: 409
currently lose_sum: 185.27403192222118
time_elpased: 1.991
batch start
#iterations: 410
currently lose_sum: 186.5327247083187
time_elpased: 2.008
batch start
#iterations: 411
currently lose_sum: 185.3080274015665
time_elpased: 2.015
batch start
#iterations: 412
currently lose_sum: 184.94732125103474
time_elpased: 2.006
batch start
#iterations: 413
currently lose_sum: 183.0114248842001
time_elpased: 2.017
batch start
#iterations: 414
currently lose_sum: 183.3575320839882
time_elpased: 1.983
batch start
#iterations: 415
currently lose_sum: nan
time_elpased: 2.066
train finish final lose is: nan
acc: 0.819
pre: 0.846
rec: 0.782
F1: 0.813
auc: 0.819
