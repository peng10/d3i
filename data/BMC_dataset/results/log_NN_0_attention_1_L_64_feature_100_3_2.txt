start to construct graph
graph construct over
58301
epochs start
batch start
#iterations: 0
currently lose_sum: 402.61162585020065
time_elpased: 2.65
batch start
#iterations: 1
currently lose_sum: 394.4207621216774
time_elpased: 2.638
batch start
#iterations: 2
currently lose_sum: 382.3313791155815
time_elpased: 2.644
batch start
#iterations: 3
currently lose_sum: 367.24491959810257
time_elpased: 2.639
batch start
#iterations: 4
currently lose_sum: 353.21928760409355
time_elpased: 2.645
batch start
#iterations: 5
currently lose_sum: 345.44557347893715
time_elpased: 2.631
batch start
#iterations: 6
currently lose_sum: 336.6707702577114
time_elpased: 2.642
batch start
#iterations: 7
currently lose_sum: 326.93753042817116
time_elpased: 2.659
batch start
#iterations: 8
currently lose_sum: 321.5910007953644
time_elpased: 2.641
batch start
#iterations: 9
currently lose_sum: 320.22133579850197
time_elpased: 2.643
batch start
#iterations: 10
currently lose_sum: 311.0297946333885
time_elpased: 2.657
batch start
#iterations: 11
currently lose_sum: 304.1787149608135
time_elpased: 2.635
batch start
#iterations: 12
currently lose_sum: 304.99529778957367
time_elpased: 2.647
batch start
#iterations: 13
currently lose_sum: 295.09066411852837
time_elpased: 2.643
batch start
#iterations: 14
currently lose_sum: 294.32068225741386
time_elpased: 2.648
batch start
#iterations: 15
currently lose_sum: 287.4711272418499
time_elpased: 2.639
batch start
#iterations: 16
currently lose_sum: 288.09749510884285
time_elpased: 2.65
batch start
#iterations: 17
currently lose_sum: 283.5409788489342
time_elpased: 2.646
batch start
#iterations: 18
currently lose_sum: 281.0061413347721
time_elpased: 2.655
batch start
#iterations: 19
currently lose_sum: 277.26686972379684
time_elpased: 2.649
batch start
#iterations: 20
currently lose_sum: 279.03849652409554
time_elpased: 2.644
batch start
#iterations: 21
currently lose_sum: 276.97374761104584
time_elpased: 2.656
batch start
#iterations: 22
currently lose_sum: 273.9048929810524
time_elpased: 2.66
batch start
#iterations: 23
currently lose_sum: 275.16788882017136
time_elpased: 2.65
batch start
#iterations: 24
currently lose_sum: 271.25619184970856
time_elpased: 2.659
batch start
#iterations: 25
currently lose_sum: 272.3147423863411
time_elpased: 2.647
batch start
#iterations: 26
currently lose_sum: 268.6348216831684
time_elpased: 2.651
batch start
#iterations: 27
currently lose_sum: 267.23665592074394
time_elpased: 2.658
batch start
#iterations: 28
currently lose_sum: 266.75962713360786
time_elpased: 2.656
batch start
#iterations: 29
currently lose_sum: 266.12986198067665
time_elpased: 2.641
batch start
#iterations: 30
currently lose_sum: 265.38832741975784
time_elpased: 2.66
batch start
#iterations: 31
currently lose_sum: 264.36201253533363
time_elpased: 2.655
batch start
#iterations: 32
currently lose_sum: 265.4102159142494
time_elpased: 2.642
batch start
#iterations: 33
currently lose_sum: 265.49217933416367
time_elpased: 2.656
batch start
#iterations: 34
currently lose_sum: 262.15751749277115
time_elpased: 2.647
batch start
#iterations: 35
currently lose_sum: 262.6334297955036
time_elpased: 2.658
batch start
#iterations: 36
currently lose_sum: 262.6017647087574
time_elpased: 2.661
batch start
#iterations: 37
currently lose_sum: 261.8018765449524
time_elpased: 2.649
batch start
#iterations: 38
currently lose_sum: 262.7520526647568
time_elpased: 2.644
batch start
#iterations: 39
currently lose_sum: 259.1579219996929
time_elpased: 2.671
start validation test
0.792731958763
0.824916599563
0.741571871768
0.781027065294
0.792574219543
validation finish
batch start
#iterations: 40
currently lose_sum: 259.04292410612106
time_elpased: 2.655
batch start
#iterations: 41
currently lose_sum: 254.79399454593658
time_elpased: 2.647
batch start
#iterations: 42
currently lose_sum: 256.628683835268
time_elpased: 2.648
batch start
#iterations: 43
currently lose_sum: 253.39726787805557
time_elpased: 2.644
batch start
#iterations: 44
currently lose_sum: 255.7030258178711
time_elpased: 2.652
batch start
#iterations: 45
currently lose_sum: 254.3396320939064
time_elpased: 2.65
batch start
#iterations: 46
currently lose_sum: 255.35333648324013
time_elpased: 2.66
batch start
#iterations: 47
currently lose_sum: 252.8009250164032
time_elpased: 2.652
batch start
#iterations: 48
currently lose_sum: 250.84960934519768
time_elpased: 2.648
batch start
#iterations: 49
currently lose_sum: 251.27822417020798
time_elpased: 2.664
batch start
#iterations: 50
currently lose_sum: 251.42572936415672
time_elpased: 2.663
batch start
#iterations: 51
currently lose_sum: 250.4361927807331
time_elpased: 2.692
batch start
#iterations: 52
currently lose_sum: 248.78949826955795
time_elpased: 2.684
batch start
#iterations: 53
currently lose_sum: 249.67302304506302
time_elpased: 2.663
batch start
#iterations: 54
currently lose_sum: 251.85552302002907
time_elpased: 2.669
batch start
#iterations: 55
currently lose_sum: 246.67778605222702
time_elpased: 2.664
batch start
#iterations: 56
currently lose_sum: 246.85794273018837
time_elpased: 2.651
batch start
#iterations: 57
currently lose_sum: 246.74897101521492
time_elpased: 2.655
batch start
#iterations: 58
currently lose_sum: 247.02967962622643
time_elpased: 2.651
batch start
#iterations: 59
currently lose_sum: 246.58980557322502
time_elpased: 2.64
batch start
#iterations: 60
currently lose_sum: 244.3982181251049
time_elpased: 2.657
batch start
#iterations: 61
currently lose_sum: 245.43248400092125
time_elpased: 2.65
batch start
#iterations: 62
currently lose_sum: 243.1111436188221
time_elpased: 2.638
batch start
#iterations: 63
currently lose_sum: 245.7990837097168
time_elpased: 2.65
batch start
#iterations: 64
currently lose_sum: 241.18696931004524
time_elpased: 2.647
batch start
#iterations: 65
currently lose_sum: 244.0746165215969
time_elpased: 2.636
batch start
#iterations: 66
currently lose_sum: 241.45292913913727
time_elpased: 2.647
batch start
#iterations: 67
currently lose_sum: 243.6787725687027
time_elpased: 2.642
batch start
#iterations: 68
currently lose_sum: 243.10552033782005
time_elpased: 2.628
batch start
#iterations: 69
currently lose_sum: 243.2699150443077
time_elpased: 2.652
batch start
#iterations: 70
currently lose_sum: 238.84053967893124
time_elpased: 2.648
batch start
#iterations: 71
currently lose_sum: 237.69166158139706
time_elpased: 2.637
batch start
#iterations: 72
currently lose_sum: 242.44708713889122
time_elpased: 2.638
batch start
#iterations: 73
currently lose_sum: 238.75543639063835
time_elpased: 2.639
batch start
#iterations: 74
currently lose_sum: 238.65080825984478
time_elpased: 2.63
batch start
#iterations: 75
currently lose_sum: 239.7114674448967
time_elpased: 2.641
batch start
#iterations: 76
currently lose_sum: 241.75317731499672
time_elpased: 2.646
batch start
#iterations: 77
currently lose_sum: 238.844944357872
time_elpased: 2.644
batch start
#iterations: 78
currently lose_sum: 237.98117384314537
time_elpased: 2.642
batch start
#iterations: 79
currently lose_sum: 239.37750940024853
time_elpased: 2.649
start validation test
0.807268041237
0.843666705296
0.752843846949
0.795671894639
0.807100237966
validation finish
batch start
#iterations: 80
currently lose_sum: 238.846757248044
time_elpased: 2.634
batch start
#iterations: 81
currently lose_sum: 235.5887831747532
time_elpased: 2.653
batch start
#iterations: 82
currently lose_sum: 235.0398580133915
time_elpased: 2.661
batch start
#iterations: 83
currently lose_sum: 237.49061155319214
time_elpased: 2.641
batch start
#iterations: 84
currently lose_sum: 237.9027718603611
time_elpased: 2.66
batch start
#iterations: 85
currently lose_sum: 237.99476850032806
time_elpased: 2.648
batch start
#iterations: 86
currently lose_sum: 237.54902482032776
time_elpased: 2.666
batch start
#iterations: 87
currently lose_sum: 238.9457580447197
time_elpased: 2.668
batch start
#iterations: 88
currently lose_sum: 234.77592170238495
time_elpased: 2.663
batch start
#iterations: 89
currently lose_sum: 237.9871859550476
time_elpased: 2.665
batch start
#iterations: 90
currently lose_sum: 235.50261878967285
time_elpased: 2.665
batch start
#iterations: 91
currently lose_sum: 234.72895246744156
time_elpased: 2.66
batch start
#iterations: 92
currently lose_sum: 235.4464450776577
time_elpased: 2.648
batch start
#iterations: 93
currently lose_sum: 233.43536753952503
time_elpased: 2.663
batch start
#iterations: 94
currently lose_sum: 232.60275974869728
time_elpased: 2.679
batch start
#iterations: 95
currently lose_sum: 233.90529975295067
time_elpased: 2.664
batch start
#iterations: 96
currently lose_sum: 232.95272552967072
time_elpased: 2.676
batch start
#iterations: 97
currently lose_sum: 233.08929058909416
time_elpased: 2.668
batch start
#iterations: 98
currently lose_sum: 235.00407084822655
time_elpased: 2.664
batch start
#iterations: 99
currently lose_sum: 231.82682652771473
time_elpased: 2.676
batch start
#iterations: 100
currently lose_sum: 234.42190065979958
time_elpased: 2.672
batch start
#iterations: 101
currently lose_sum: 230.88145777583122
time_elpased: 2.664
batch start
#iterations: 102
currently lose_sum: 228.8965810984373
time_elpased: 2.674
batch start
#iterations: 103
currently lose_sum: 229.84077206254005
time_elpased: 2.695
batch start
#iterations: 104
currently lose_sum: 230.69298523664474
time_elpased: 2.661
batch start
#iterations: 105
currently lose_sum: 233.72246345877647
time_elpased: 2.686
batch start
#iterations: 106
currently lose_sum: 230.04784414172173
time_elpased: 2.661
batch start
#iterations: 107
currently lose_sum: 232.646896392107
time_elpased: 2.667
batch start
#iterations: 108
currently lose_sum: 229.94459655880928
time_elpased: 2.67
batch start
#iterations: 109
currently lose_sum: 231.81183014810085
time_elpased: 2.683
batch start
#iterations: 110
currently lose_sum: 232.52855598926544
time_elpased: 2.649
batch start
#iterations: 111
currently lose_sum: 229.56815975904465
time_elpased: 2.662
batch start
#iterations: 112
currently lose_sum: 229.07753318548203
time_elpased: 2.663
batch start
#iterations: 113
currently lose_sum: 231.59998884797096
time_elpased: 2.653
batch start
#iterations: 114
currently lose_sum: 228.26202024519444
time_elpased: 2.661
batch start
#iterations: 115
currently lose_sum: 228.9638115465641
time_elpased: 2.675
batch start
#iterations: 116
currently lose_sum: 227.70949611067772
time_elpased: 2.672
batch start
#iterations: 117
currently lose_sum: 229.73994767665863
time_elpased: 2.68
batch start
#iterations: 118
currently lose_sum: 227.48006683588028
time_elpased: 2.666
batch start
#iterations: 119
currently lose_sum: 228.54154515266418
time_elpased: 2.66
start validation test
0.811443298969
0.824832504863
0.789348500517
0.806700486155
0.811375175233
validation finish
batch start
#iterations: 120
currently lose_sum: 226.7417904138565
time_elpased: 2.667
batch start
#iterations: 121
currently lose_sum: 227.5867749452591
time_elpased: 2.69
batch start
#iterations: 122
currently lose_sum: 223.6100114583969
time_elpased: 2.691
batch start
#iterations: 123
currently lose_sum: 227.29932922124863
time_elpased: 2.678
batch start
#iterations: 124
currently lose_sum: 224.73372408747673
time_elpased: 2.696
batch start
#iterations: 125
currently lose_sum: 223.44668605923653
time_elpased: 2.705
batch start
#iterations: 126
currently lose_sum: 222.00671570003033
time_elpased: 2.698
batch start
#iterations: 127
currently lose_sum: 225.09405013918877
time_elpased: 2.721
batch start
#iterations: 128
currently lose_sum: 225.10827089846134
time_elpased: 2.701
batch start
#iterations: 129
currently lose_sum: 225.6265224814415
time_elpased: 2.699
batch start
#iterations: 130
currently lose_sum: 222.38575848937035
time_elpased: 2.693
batch start
#iterations: 131
currently lose_sum: 224.32706362009048
time_elpased: 2.695
batch start
#iterations: 132
currently lose_sum: 222.1534543633461
time_elpased: 2.688
batch start
#iterations: 133
currently lose_sum: 223.3275166451931
time_elpased: 2.689
batch start
#iterations: 134
currently lose_sum: 221.52511504292488
time_elpased: 2.69
batch start
#iterations: 135
currently lose_sum: 220.38817250728607
time_elpased: 2.681
batch start
#iterations: 136
currently lose_sum: 223.72018598020077
time_elpased: 2.695
batch start
#iterations: 137
currently lose_sum: 221.72284199297428
time_elpased: 2.716
batch start
#iterations: 138
currently lose_sum: 220.97477109730244
time_elpased: 2.693
batch start
#iterations: 139
currently lose_sum: 222.24164167046547
time_elpased: 2.715
batch start
#iterations: 140
currently lose_sum: 220.46891689300537
time_elpased: 2.7
batch start
#iterations: 141
currently lose_sum: 219.75612792372704
time_elpased: 2.702
batch start
#iterations: 142
currently lose_sum: 219.50417511165142
time_elpased: 2.716
batch start
#iterations: 143
currently lose_sum: 219.78973706066608
time_elpased: 2.725
batch start
#iterations: 144
currently lose_sum: 219.7225442379713
time_elpased: 2.703
batch start
#iterations: 145
currently lose_sum: 220.09422327578068
time_elpased: 2.708
batch start
#iterations: 146
currently lose_sum: 219.68396367132664
time_elpased: 2.682
batch start
#iterations: 147
currently lose_sum: 218.34953179955482
time_elpased: 2.648
batch start
#iterations: 148
currently lose_sum: 218.38008281588554
time_elpased: 2.651
batch start
#iterations: 149
currently lose_sum: 220.91503590345383
time_elpased: 2.663
batch start
#iterations: 150
currently lose_sum: 220.4886203110218
time_elpased: 2.642
batch start
#iterations: 151
currently lose_sum: 216.3236551731825
time_elpased: 2.64
batch start
#iterations: 152
currently lose_sum: 215.6469403654337
time_elpased: 2.622
batch start
#iterations: 153
currently lose_sum: 213.62412150204182
time_elpased: 2.629
batch start
#iterations: 154
currently lose_sum: 216.5220046043396
time_elpased: 2.63
batch start
#iterations: 155
currently lose_sum: 216.9381803572178
time_elpased: 2.633
batch start
#iterations: 156
currently lose_sum: 214.19529128074646
time_elpased: 2.638
batch start
#iterations: 157
currently lose_sum: 216.6413468569517
time_elpased: 2.639
batch start
#iterations: 158
currently lose_sum: 212.04344905912876
time_elpased: 2.642
batch start
#iterations: 159
currently lose_sum: 212.7239164710045
time_elpased: 2.634
start validation test
0.802835051546
0.785819070905
0.830920372285
0.807740638351
0.802921645547
validation finish
batch start
#iterations: 160
currently lose_sum: 212.69675706326962
time_elpased: 2.649
batch start
#iterations: 161
currently lose_sum: 212.2889001518488
time_elpased: 2.642
batch start
#iterations: 162
currently lose_sum: 212.21082384884357
time_elpased: 2.637
batch start
#iterations: 163
currently lose_sum: 211.04848864674568
time_elpased: 2.648
batch start
#iterations: 164
currently lose_sum: 213.21246726810932
time_elpased: 2.645
batch start
#iterations: 165
currently lose_sum: 212.09963902831078
time_elpased: 2.639
batch start
#iterations: 166
currently lose_sum: 213.71369986236095
time_elpased: 2.634
batch start
#iterations: 167
currently lose_sum: 209.63981166481972
time_elpased: 2.635
batch start
#iterations: 168
currently lose_sum: 213.87111915647984
time_elpased: 2.636
batch start
#iterations: 169
currently lose_sum: 210.19364742934704
time_elpased: 2.632
batch start
#iterations: 170
currently lose_sum: 211.991037607193
time_elpased: 2.635
batch start
#iterations: 171
currently lose_sum: 211.11147940158844
time_elpased: 2.62
batch start
#iterations: 172
currently lose_sum: 211.27184203267097
time_elpased: 2.631
batch start
#iterations: 173
currently lose_sum: 210.80643126368523
time_elpased: 2.629
batch start
#iterations: 174
currently lose_sum: 209.12988290190697
time_elpased: 2.627
batch start
#iterations: 175
currently lose_sum: 207.6572906076908
time_elpased: 2.645
batch start
#iterations: 176
currently lose_sum: 209.2436189800501
time_elpased: 2.64
batch start
#iterations: 177
currently lose_sum: 207.09300704300404
time_elpased: 2.643
batch start
#iterations: 178
currently lose_sum: 206.98042352497578
time_elpased: 2.646
batch start
#iterations: 179
currently lose_sum: 206.93889635801315
time_elpased: 2.648
batch start
#iterations: 180
currently lose_sum: 206.35886289179325
time_elpased: 2.641
batch start
#iterations: 181
currently lose_sum: 208.1253757327795
time_elpased: 2.666
batch start
#iterations: 182
currently lose_sum: 206.237188488245
time_elpased: 2.685
batch start
#iterations: 183
currently lose_sum: 207.20763033628464
time_elpased: 2.658
batch start
#iterations: 184
currently lose_sum: 208.16180139780045
time_elpased: 2.654
batch start
#iterations: 185
currently lose_sum: 205.89065086841583
time_elpased: 2.675
batch start
#iterations: 186
currently lose_sum: 206.6840952038765
time_elpased: 2.649
batch start
#iterations: 187
currently lose_sum: 207.5283446907997
time_elpased: 2.666
batch start
#iterations: 188
currently lose_sum: 207.6373716890812
time_elpased: 2.667
batch start
#iterations: 189
currently lose_sum: 202.29921129345894
time_elpased: 2.661
batch start
#iterations: 190
currently lose_sum: 204.76548847556114
time_elpased: 2.673
batch start
#iterations: 191
currently lose_sum: 204.75442734360695
time_elpased: 2.657
batch start
#iterations: 192
currently lose_sum: 204.50196473300457
time_elpased: 2.644
batch start
#iterations: 193
currently lose_sum: 202.747643917799
time_elpased: 2.652
batch start
#iterations: 194
currently lose_sum: 200.50988790392876
time_elpased: 2.638
batch start
#iterations: 195
currently lose_sum: 203.87381522357464
time_elpased: 2.637
batch start
#iterations: 196
currently lose_sum: 202.54359537363052
time_elpased: 2.645
batch start
#iterations: 197
currently lose_sum: 203.70632460713387
time_elpased: 2.678
batch start
#iterations: 198
currently lose_sum: 201.05279190838337
time_elpased: 2.65
batch start
#iterations: 199
currently lose_sum: 202.3792726546526
time_elpased: 2.657
start validation test
0.814432989691
0.828534314787
0.79152016546
0.809604400254
0.814362343778
validation finish
batch start
#iterations: 200
currently lose_sum: 200.3323054909706
time_elpased: 2.656
batch start
#iterations: 201
currently lose_sum: 201.36503034830093
time_elpased: 2.643
batch start
#iterations: 202
currently lose_sum: 199.91239307820797
time_elpased: 2.643
batch start
#iterations: 203
currently lose_sum: 199.4931042790413
time_elpased: 2.658
batch start
#iterations: 204
currently lose_sum: 201.81623768806458
time_elpased: 2.637
batch start
#iterations: 205
currently lose_sum: 200.7171398550272
time_elpased: 2.648
batch start
#iterations: 206
currently lose_sum: 200.26965995132923
time_elpased: 2.685
batch start
#iterations: 207
currently lose_sum: 198.58267745375633
time_elpased: 2.661
batch start
#iterations: 208
currently lose_sum: 201.41326147317886
time_elpased: 2.689
batch start
#iterations: 209
currently lose_sum: 200.3475974947214
time_elpased: 2.679
batch start
#iterations: 210
currently lose_sum: 197.35333125293255
time_elpased: 2.676
batch start
#iterations: 211
currently lose_sum: 198.42266902327538
time_elpased: 2.686
batch start
#iterations: 212
currently lose_sum: 198.67081275582314
time_elpased: 2.688
batch start
#iterations: 213
currently lose_sum: 197.20624463260174
time_elpased: 2.683
batch start
#iterations: 214
currently lose_sum: 198.4296118915081
time_elpased: 2.703
batch start
#iterations: 215
currently lose_sum: 197.65171188116074
time_elpased: 2.698
batch start
#iterations: 216
currently lose_sum: 196.673969745636
time_elpased: 2.702
batch start
#iterations: 217
currently lose_sum: 194.58020302653313
time_elpased: 2.688
batch start
#iterations: 218
currently lose_sum: 195.08510230481625
time_elpased: 2.706
batch start
#iterations: 219
currently lose_sum: 195.62463618814945
time_elpased: 2.688
batch start
#iterations: 220
currently lose_sum: 194.8471326380968
time_elpased: 2.696
batch start
#iterations: 221
currently lose_sum: 195.53834073245525
time_elpased: 2.714
batch start
#iterations: 222
currently lose_sum: 194.13646058738232
time_elpased: 2.704
batch start
#iterations: 223
currently lose_sum: 196.41260296106339
time_elpased: 2.696
batch start
#iterations: 224
currently lose_sum: 194.53562152385712
time_elpased: 2.704
batch start
#iterations: 225
currently lose_sum: 192.5007638335228
time_elpased: 2.711
batch start
#iterations: 226
currently lose_sum: 193.94029881060123
time_elpased: 2.698
batch start
#iterations: 227
currently lose_sum: 194.38421504199505
time_elpased: 2.714
batch start
#iterations: 228
currently lose_sum: 192.82743038237095
time_elpased: 2.708
batch start
#iterations: 229
currently lose_sum: 191.0781173557043
time_elpased: 2.685
batch start
#iterations: 230
currently lose_sum: 191.99925754964352
time_elpased: 2.718
batch start
#iterations: 231
currently lose_sum: 192.861882686615
time_elpased: 2.725
batch start
#iterations: 232
currently lose_sum: 192.16875448822975
time_elpased: 2.705
batch start
#iterations: 233
currently lose_sum: 193.9751279503107
time_elpased: 2.7
batch start
#iterations: 234
currently lose_sum: 190.13135235011578
time_elpased: 2.723
batch start
#iterations: 235
currently lose_sum: 192.0443544536829
time_elpased: 2.703
batch start
#iterations: 236
currently lose_sum: 190.1802609115839
time_elpased: 2.713
batch start
#iterations: 237
currently lose_sum: 193.5137768983841
time_elpased: 2.703
batch start
#iterations: 238
currently lose_sum: 191.4592160731554
time_elpased: 2.712
batch start
#iterations: 239
currently lose_sum: 189.36232271790504
time_elpased: 2.711
start validation test
0.812113402062
0.828409462553
0.785832471562
0.806559465053
0.812032371444
validation finish
batch start
#iterations: 240
currently lose_sum: 188.10094584524632
time_elpased: 2.711
batch start
#iterations: 241
currently lose_sum: 189.15047998726368
time_elpased: 2.7
batch start
#iterations: 242
currently lose_sum: 188.04819019138813
time_elpased: 2.717
batch start
#iterations: 243
currently lose_sum: 187.5522940158844
time_elpased: 2.721
batch start
#iterations: 244
currently lose_sum: 187.08321924507618
time_elpased: 2.72
batch start
#iterations: 245
currently lose_sum: 187.3586451560259
time_elpased: 2.7
batch start
#iterations: 246
currently lose_sum: 188.6855173110962
time_elpased: 2.711
batch start
#iterations: 247
currently lose_sum: 190.12553071975708
time_elpased: 2.701
batch start
#iterations: 248
currently lose_sum: 189.52588619291782
time_elpased: 2.699
batch start
#iterations: 249
currently lose_sum: 185.4809308797121
time_elpased: 2.71
batch start
#iterations: 250
currently lose_sum: 187.08125135302544
time_elpased: 2.715
batch start
#iterations: 251
currently lose_sum: 188.4472313374281
time_elpased: 2.709
batch start
#iterations: 252
currently lose_sum: 185.3124132603407
time_elpased: 2.715
batch start
#iterations: 253
currently lose_sum: 186.10032750666142
time_elpased: 2.702
batch start
#iterations: 254
currently lose_sum: 185.42751663923264
time_elpased: 2.713
batch start
#iterations: 255
currently lose_sum: 185.46574611961842
time_elpased: 2.707
batch start
#iterations: 256
currently lose_sum: 186.77725106477737
time_elpased: 2.721
batch start
#iterations: 257
currently lose_sum: 186.512969866395
time_elpased: 2.692
batch start
#iterations: 258
currently lose_sum: 184.36769546568394
time_elpased: 2.714
batch start
#iterations: 259
currently lose_sum: 183.3673221617937
time_elpased: 2.696
batch start
#iterations: 260
currently lose_sum: 184.9992373585701
time_elpased: 2.696
batch start
#iterations: 261
currently lose_sum: 183.4911940842867
time_elpased: 2.718
batch start
#iterations: 262
currently lose_sum: 181.45805995166302
time_elpased: 2.712
batch start
#iterations: 263
currently lose_sum: 182.74873034656048
time_elpased: 2.698
batch start
#iterations: 264
currently lose_sum: 180.63316482305527
time_elpased: 2.717
batch start
#iterations: 265
currently lose_sum: 176.95307657122612
time_elpased: 2.71
batch start
#iterations: 266
currently lose_sum: 180.6314446479082
time_elpased: 2.699
batch start
#iterations: 267
currently lose_sum: 179.95947940647602
time_elpased: 2.723
batch start
#iterations: 268
currently lose_sum: 177.2012638747692
time_elpased: 2.72
batch start
#iterations: 269
currently lose_sum: 180.68764869868755
time_elpased: 2.701
batch start
#iterations: 270
currently lose_sum: 179.01780904829502
time_elpased: 2.696
batch start
#iterations: 271
currently lose_sum: 178.68638336658478
time_elpased: 2.706
batch start
#iterations: 272
currently lose_sum: 178.42195436358452
time_elpased: 2.702
batch start
#iterations: 273
currently lose_sum: 177.87878814339638
time_elpased: 2.703
batch start
#iterations: 274
currently lose_sum: 179.84678049385548
time_elpased: 2.706
batch start
#iterations: 275
currently lose_sum: 177.3303254544735
time_elpased: 2.692
batch start
#iterations: 276
currently lose_sum: 179.03449475765228
time_elpased: 2.708
batch start
#iterations: 277
currently lose_sum: 179.7533696591854
time_elpased: 2.709
batch start
#iterations: 278
currently lose_sum: 177.87167197465897
time_elpased: 2.688
batch start
#iterations: 279
currently lose_sum: 175.55565237998962
time_elpased: 2.697
start validation test
0.813969072165
0.833351666483
0.783453981386
0.807632855392
0.813874986582
validation finish
batch start
#iterations: 280
currently lose_sum: 178.4700802564621
time_elpased: 2.707
batch start
#iterations: 281
currently lose_sum: 178.2274781614542
time_elpased: 2.717
batch start
#iterations: 282
currently lose_sum: 176.00463439524174
time_elpased: 2.694
batch start
#iterations: 283
currently lose_sum: 175.60458312928677
time_elpased: 2.714
batch start
#iterations: 284
currently lose_sum: 177.4277834892273
time_elpased: 2.721
batch start
#iterations: 285
currently lose_sum: 174.42750734090805
time_elpased: 2.704
batch start
#iterations: 286
currently lose_sum: 175.2248776257038
time_elpased: 2.731
batch start
#iterations: 287
currently lose_sum: 174.46265935897827
time_elpased: 2.702
batch start
#iterations: 288
currently lose_sum: 175.11787335574627
time_elpased: 2.697
batch start
#iterations: 289
currently lose_sum: 173.96722480654716
time_elpased: 2.698
batch start
#iterations: 290
currently lose_sum: 173.4418472647667
time_elpased: 2.708
batch start
#iterations: 291
currently lose_sum: 172.69459621608257
time_elpased: 2.698
batch start
#iterations: 292
currently lose_sum: 172.207558080554
time_elpased: 2.711
batch start
#iterations: 293
currently lose_sum: 172.27106109261513
time_elpased: 2.698
batch start
#iterations: 294
currently lose_sum: 172.3432373404503
time_elpased: 2.711
batch start
#iterations: 295
currently lose_sum: 175.7086292654276
time_elpased: 2.696
batch start
#iterations: 296
currently lose_sum: 173.32780458033085
time_elpased: 2.711
batch start
#iterations: 297
currently lose_sum: 171.28197956085205
time_elpased: 2.713
batch start
#iterations: 298
currently lose_sum: 171.69848492741585
time_elpased: 2.702
batch start
#iterations: 299
currently lose_sum: 171.156210064888
time_elpased: 2.7
batch start
#iterations: 300
currently lose_sum: 172.47017677128315
time_elpased: 2.703
batch start
#iterations: 301
currently lose_sum: 172.52872805297375
time_elpased: 2.712
batch start
#iterations: 302
currently lose_sum: 169.86417093873024
time_elpased: 2.683
batch start
#iterations: 303
currently lose_sum: 168.81470522284508
time_elpased: 2.685
batch start
#iterations: 304
currently lose_sum: 169.5888211876154
time_elpased: 2.696
batch start
#iterations: 305
currently lose_sum: 169.5368960350752
time_elpased: 2.703
batch start
#iterations: 306
currently lose_sum: 170.33385153114796
time_elpased: 2.694
batch start
#iterations: 307
currently lose_sum: 170.5538124293089
time_elpased: 2.688
batch start
#iterations: 308
currently lose_sum: 168.06100225448608
time_elpased: 2.714
batch start
#iterations: 309
currently lose_sum: 168.59786945581436
time_elpased: 2.71
batch start
#iterations: 310
currently lose_sum: 168.84464882314205
time_elpased: 2.714
batch start
#iterations: 311
currently lose_sum: 168.4555495530367
time_elpased: 2.684
batch start
#iterations: 312
currently lose_sum: 169.17552684247494
time_elpased: 2.694
batch start
#iterations: 313
currently lose_sum: 166.6806589514017
time_elpased: 2.684
batch start
#iterations: 314
currently lose_sum: 166.95692491531372
time_elpased: 2.713
batch start
#iterations: 315
currently lose_sum: 165.7221491932869
time_elpased: 2.686
batch start
#iterations: 316
currently lose_sum: 165.78299835324287
time_elpased: 2.679
batch start
#iterations: 317
currently lose_sum: 165.07941816747189
time_elpased: 2.69
batch start
#iterations: 318
currently lose_sum: 163.9676597714424
time_elpased: 2.697
batch start
#iterations: 319
currently lose_sum: 164.9703450202942
time_elpased: 2.686
start validation test
0.807525773196
0.832660838377
0.768252326784
0.799160929432
0.807404683433
validation finish
batch start
#iterations: 320
currently lose_sum: 164.13887760043144
time_elpased: 2.677
batch start
#iterations: 321
currently lose_sum: 164.74402394890785
time_elpased: 2.696
batch start
#iterations: 322
currently lose_sum: 167.00593262910843
time_elpased: 2.685
batch start
#iterations: 323
currently lose_sum: 164.4101915806532
time_elpased: 2.698
batch start
#iterations: 324
currently lose_sum: 166.04374134540558
time_elpased: 2.697
batch start
#iterations: 325
currently lose_sum: 165.06098741292953
time_elpased: 2.7
batch start
#iterations: 326
currently lose_sum: 163.07599902153015
time_elpased: 2.684
batch start
#iterations: 327
currently lose_sum: 160.1229911595583
time_elpased: 2.689
batch start
#iterations: 328
currently lose_sum: 163.568629860878
time_elpased: 2.692
batch start
#iterations: 329
currently lose_sum: 164.5823905915022
time_elpased: 2.688
batch start
#iterations: 330
currently lose_sum: 162.29910016059875
time_elpased: 2.701
batch start
#iterations: 331
currently lose_sum: 161.47874507308006
time_elpased: 2.689
batch start
#iterations: 332
currently lose_sum: 161.53573316335678
time_elpased: 2.703
batch start
#iterations: 333
currently lose_sum: 162.9711511284113
time_elpased: 2.697
batch start
#iterations: 334
currently lose_sum: 162.948235809803
time_elpased: 2.701
batch start
#iterations: 335
currently lose_sum: 162.3814806342125
time_elpased: 2.696
batch start
#iterations: 336
currently lose_sum: 163.7946719378233
time_elpased: 2.703
batch start
#iterations: 337
currently lose_sum: 159.80957747995853
time_elpased: 2.696
batch start
#iterations: 338
currently lose_sum: 160.36359278857708
time_elpased: 2.693
batch start
#iterations: 339
currently lose_sum: 162.53583644330502
time_elpased: 2.695
batch start
#iterations: 340
currently lose_sum: 161.76504120230675
time_elpased: 2.705
batch start
#iterations: 341
currently lose_sum: 159.9626616090536
time_elpased: 2.702
batch start
#iterations: 342
currently lose_sum: 161.15565006434917
time_elpased: 2.704
batch start
#iterations: 343
currently lose_sum: 159.2489935606718
time_elpased: 2.681
batch start
#iterations: 344
currently lose_sum: 159.10899848490953
time_elpased: 2.716
batch start
#iterations: 345
currently lose_sum: 160.55686824023724
time_elpased: 2.704
batch start
#iterations: 346
currently lose_sum: 157.73045967519283
time_elpased: 2.643
batch start
#iterations: 347
currently lose_sum: 158.17228169739246
time_elpased: 2.632
batch start
#iterations: 348
currently lose_sum: 156.3877302184701
time_elpased: 2.648
batch start
#iterations: 349
currently lose_sum: 157.76808819174767
time_elpased: 2.656
batch start
#iterations: 350
currently lose_sum: 156.13503807783127
time_elpased: 2.636
batch start
#iterations: 351
currently lose_sum: 157.0660514831543
time_elpased: 2.641
batch start
#iterations: 352
currently lose_sum: 155.03804264962673
time_elpased: 2.65
batch start
#iterations: 353
currently lose_sum: 156.73140615969896
time_elpased: 2.662
batch start
#iterations: 354
currently lose_sum: 155.21490667760372
time_elpased: 2.648
batch start
#iterations: 355
currently lose_sum: 153.73560674488544
time_elpased: 2.644
batch start
#iterations: 356
currently lose_sum: 156.43121379613876
time_elpased: 2.634
batch start
#iterations: 357
currently lose_sum: 154.43489730358124
time_elpased: 2.654
batch start
#iterations: 358
currently lose_sum: 154.37958221137524
time_elpased: 2.655
batch start
#iterations: 359
currently lose_sum: 154.94388814270496
time_elpased: 2.652
start validation test
0.808711340206
0.830211681259
0.774663908997
0.801476488525
0.808606363543
validation finish
batch start
#iterations: 360
currently lose_sum: 152.9692302942276
time_elpased: 2.65
batch start
#iterations: 361
currently lose_sum: 153.9861957281828
time_elpased: 2.66
batch start
#iterations: 362
currently lose_sum: 153.1979425996542
time_elpased: 2.663
batch start
#iterations: 363
currently lose_sum: 152.4456289112568
time_elpased: 2.65
batch start
#iterations: 364
currently lose_sum: 155.34335155785084
time_elpased: 2.674
batch start
#iterations: 365
currently lose_sum: 154.29269026219845
time_elpased: 2.657
batch start
#iterations: 366
currently lose_sum: 154.5208281353116
time_elpased: 2.66
batch start
#iterations: 367
currently lose_sum: 155.01224859058857
time_elpased: 2.646
batch start
#iterations: 368
currently lose_sum: 153.60105063021183
time_elpased: 2.644
batch start
#iterations: 369
currently lose_sum: 153.30537505447865
time_elpased: 2.664
batch start
#iterations: 370
currently lose_sum: 152.71054250001907
time_elpased: 2.666
batch start
#iterations: 371
currently lose_sum: 149.8725094795227
time_elpased: 2.659
batch start
#iterations: 372
currently lose_sum: 153.05782252550125
time_elpased: 2.668
batch start
#iterations: 373
currently lose_sum: 152.82709096372128
time_elpased: 2.672
batch start
#iterations: 374
currently lose_sum: 152.6069537550211
time_elpased: 2.651
batch start
#iterations: 375
currently lose_sum: 151.68458127975464
time_elpased: 2.68
batch start
#iterations: 376
currently lose_sum: 151.01984357833862
time_elpased: 2.677
batch start
#iterations: 377
currently lose_sum: 149.84936155378819
time_elpased: 2.672
batch start
#iterations: 378
currently lose_sum: 149.47842010855675
time_elpased: 2.681
batch start
#iterations: 379
currently lose_sum: 151.29363018274307
time_elpased: 2.671
batch start
#iterations: 380
currently lose_sum: 149.12931832671165
time_elpased: 2.661
batch start
#iterations: 381
currently lose_sum: 149.6868679523468
time_elpased: 2.666
batch start
#iterations: 382
currently lose_sum: 149.25543536245823
time_elpased: 2.665
batch start
#iterations: 383
currently lose_sum: 150.50338381528854
time_elpased: 2.661
batch start
#iterations: 384
currently lose_sum: 147.40888937562704
time_elpased: 2.662
batch start
#iterations: 385
currently lose_sum: 148.0568526685238
time_elpased: 2.667
batch start
#iterations: 386
currently lose_sum: 149.49180029332638
time_elpased: 2.659
batch start
#iterations: 387
currently lose_sum: 147.57891100645065
time_elpased: 2.671
batch start
#iterations: 388
currently lose_sum: 146.99606017768383
time_elpased: 2.682
batch start
#iterations: 389
currently lose_sum: 148.34016796201468
time_elpased: 2.671
batch start
#iterations: 390
currently lose_sum: 147.36517260968685
time_elpased: 2.672
batch start
#iterations: 391
currently lose_sum: 148.55116856098175
time_elpased: 2.662
batch start
#iterations: 392
currently lose_sum: 149.21020479500294
time_elpased: 2.661
batch start
#iterations: 393
currently lose_sum: 147.95064505934715
time_elpased: 2.666
batch start
#iterations: 394
currently lose_sum: 145.3101848512888
time_elpased: 2.668
batch start
#iterations: 395
currently lose_sum: 146.08297232538462
time_elpased: 2.668
batch start
#iterations: 396
currently lose_sum: 147.40677985548973
time_elpased: 2.684
batch start
#iterations: 397
currently lose_sum: 147.8684635385871
time_elpased: 2.681
batch start
#iterations: 398
currently lose_sum: 144.83327801525593
time_elpased: 2.667
batch start
#iterations: 399
currently lose_sum: 145.74044525623322
time_elpased: 2.68
start validation test
0.806597938144
0.816335257644
0.789658738366
0.802775441548
0.806545710396
validation finish
batch start
#iterations: 400
currently lose_sum: 146.3613507002592
time_elpased: 2.68
batch start
#iterations: 401
currently lose_sum: 144.01880610734224
time_elpased: 2.663
batch start
#iterations: 402
currently lose_sum: 145.3068755120039
time_elpased: 2.65
batch start
#iterations: 403
currently lose_sum: 142.80232341587543
time_elpased: 2.667
batch start
#iterations: 404
currently lose_sum: 145.13840363919735
time_elpased: 2.657
batch start
#iterations: 405
currently lose_sum: 143.14721831679344
time_elpased: 2.671
batch start
#iterations: 406
currently lose_sum: 142.76736153662205
time_elpased: 2.685
batch start
#iterations: 407
currently lose_sum: 142.481740988791
time_elpased: 2.665
batch start
#iterations: 408
currently lose_sum: 141.53588797152042
time_elpased: 2.66
batch start
#iterations: 409
currently lose_sum: 143.05091270804405
time_elpased: 2.675
batch start
#iterations: 410
currently lose_sum: 143.63787338882685
time_elpased: 2.666
batch start
#iterations: 411
currently lose_sum: 142.28544809669256
time_elpased: 2.668
batch start
#iterations: 412
currently lose_sum: 145.1220052987337
time_elpased: 2.675
batch start
#iterations: 413
currently lose_sum: 141.52457187324762
time_elpased: 2.67
batch start
#iterations: 414
currently lose_sum: 143.11791636049747
time_elpased: 2.665
batch start
#iterations: 415
currently lose_sum: 142.3242897465825
time_elpased: 2.672
batch start
#iterations: 416
currently lose_sum: 142.7665790170431
time_elpased: 2.68
batch start
#iterations: 417
currently lose_sum: 140.93090104311705
time_elpased: 2.685
batch start
#iterations: 418
currently lose_sum: 141.25370447337627
time_elpased: 2.69
batch start
#iterations: 419
currently lose_sum: 141.7290735244751
time_elpased: 2.682
batch start
#iterations: 420
currently lose_sum: 140.7045749500394
time_elpased: 2.672
batch start
#iterations: 421
currently lose_sum: 141.16934743523598
time_elpased: 2.678
batch start
#iterations: 422
currently lose_sum: 143.184725061059
time_elpased: 2.676
batch start
#iterations: 423
currently lose_sum: 139.35887102782726
time_elpased: 2.668
batch start
#iterations: 424
currently lose_sum: 140.9218763485551
time_elpased: 2.688
batch start
#iterations: 425
currently lose_sum: nan
time_elpased: 2.669
train finish final lose is: nan
acc: 0.815
pre: 0.833
rec: 0.794
F1: 0.813
auc: 0.816
