start to construct graph
graph construct over
58301
epochs start
batch start
#iterations: 0
currently lose_sum: 402.21184128522873
time_elpased: 3.212
batch start
#iterations: 1
currently lose_sum: 395.0421333909035
time_elpased: 3.182
batch start
#iterations: 2
currently lose_sum: 384.7439447045326
time_elpased: 3.204
batch start
#iterations: 3
currently lose_sum: 367.8945409655571
time_elpased: 3.207
batch start
#iterations: 4
currently lose_sum: 354.91264736652374
time_elpased: 3.193
batch start
#iterations: 5
currently lose_sum: 346.4627729356289
time_elpased: 3.192
batch start
#iterations: 6
currently lose_sum: 335.00515881180763
time_elpased: 3.199
batch start
#iterations: 7
currently lose_sum: 330.5198276937008
time_elpased: 3.211
batch start
#iterations: 8
currently lose_sum: 322.05999049544334
time_elpased: 3.204
batch start
#iterations: 9
currently lose_sum: 319.2059027850628
time_elpased: 3.209
batch start
#iterations: 10
currently lose_sum: 314.8747071623802
time_elpased: 3.214
batch start
#iterations: 11
currently lose_sum: 308.9143251180649
time_elpased: 3.205
batch start
#iterations: 12
currently lose_sum: 311.11825731396675
time_elpased: 3.196
batch start
#iterations: 13
currently lose_sum: 303.00793635845184
time_elpased: 3.209
batch start
#iterations: 14
currently lose_sum: 304.673510402441
time_elpased: 3.214
batch start
#iterations: 15
currently lose_sum: 297.3300397992134
time_elpased: 3.211
batch start
#iterations: 16
currently lose_sum: 296.1232575774193
time_elpased: 3.207
batch start
#iterations: 17
currently lose_sum: 290.61818635463715
time_elpased: 3.219
batch start
#iterations: 18
currently lose_sum: 288.4363772571087
time_elpased: 3.206
batch start
#iterations: 19
currently lose_sum: 284.3294672071934
time_elpased: 3.241
batch start
#iterations: 20
currently lose_sum: 284.73266783356667
time_elpased: 3.201
batch start
#iterations: 21
currently lose_sum: 282.9924557507038
time_elpased: 3.194
batch start
#iterations: 22
currently lose_sum: 279.6574601829052
time_elpased: 3.197
batch start
#iterations: 23
currently lose_sum: 281.42227333784103
time_elpased: 3.206
batch start
#iterations: 24
currently lose_sum: 276.9665265381336
time_elpased: 3.196
batch start
#iterations: 25
currently lose_sum: 277.20091485977173
time_elpased: 3.197
batch start
#iterations: 26
currently lose_sum: 273.5835650265217
time_elpased: 3.188
batch start
#iterations: 27
currently lose_sum: 270.8886014223099
time_elpased: 3.206
batch start
#iterations: 28
currently lose_sum: 268.89354345202446
time_elpased: 3.191
batch start
#iterations: 29
currently lose_sum: 268.72946402430534
time_elpased: 3.214
batch start
#iterations: 30
currently lose_sum: 266.9314323961735
time_elpased: 3.186
batch start
#iterations: 31
currently lose_sum: 264.7091084718704
time_elpased: 3.183
batch start
#iterations: 32
currently lose_sum: 266.8107340633869
time_elpased: 3.211
batch start
#iterations: 33
currently lose_sum: 267.2297453582287
time_elpased: 3.194
batch start
#iterations: 34
currently lose_sum: 261.7188654243946
time_elpased: 3.203
batch start
#iterations: 35
currently lose_sum: 260.32414960861206
time_elpased: 3.195
batch start
#iterations: 36
currently lose_sum: 263.4878679513931
time_elpased: 3.197
batch start
#iterations: 37
currently lose_sum: 261.50589925050735
time_elpased: 3.2
batch start
#iterations: 38
currently lose_sum: 261.26524540781975
time_elpased: 3.187
batch start
#iterations: 39
currently lose_sum: 258.4596937596798
time_elpased: 3.177
start validation test
0.795257731959
0.835729436719
0.733402275078
0.781229345671
0.795067016264
validation finish
batch start
#iterations: 40
currently lose_sum: 258.0446105003357
time_elpased: 3.186
batch start
#iterations: 41
currently lose_sum: 254.40362632274628
time_elpased: 3.178
batch start
#iterations: 42
currently lose_sum: 257.2042465209961
time_elpased: 3.181
batch start
#iterations: 43
currently lose_sum: 254.54918682575226
time_elpased: 3.196
batch start
#iterations: 44
currently lose_sum: 256.08309003710747
time_elpased: 3.191
batch start
#iterations: 45
currently lose_sum: 255.26825094223022
time_elpased: 3.199
batch start
#iterations: 46
currently lose_sum: 256.710764169693
time_elpased: 3.194
batch start
#iterations: 47
currently lose_sum: 253.9145084619522
time_elpased: 3.2
batch start
#iterations: 48
currently lose_sum: 253.76837226748466
time_elpased: 3.197
batch start
#iterations: 49
currently lose_sum: 254.16090813279152
time_elpased: 3.201
batch start
#iterations: 50
currently lose_sum: 253.04381358623505
time_elpased: 3.194
batch start
#iterations: 51
currently lose_sum: 252.6822939813137
time_elpased: 3.194
batch start
#iterations: 52
currently lose_sum: 250.49780136346817
time_elpased: 3.204
batch start
#iterations: 53
currently lose_sum: 252.3987547159195
time_elpased: 3.192
batch start
#iterations: 54
currently lose_sum: 255.46995186805725
time_elpased: 3.189
batch start
#iterations: 55
currently lose_sum: 250.65933918952942
time_elpased: 3.201
batch start
#iterations: 56
currently lose_sum: 250.40589585900307
time_elpased: 3.188
batch start
#iterations: 57
currently lose_sum: 251.20439568161964
time_elpased: 3.189
batch start
#iterations: 58
currently lose_sum: 250.11521232128143
time_elpased: 3.196
batch start
#iterations: 59
currently lose_sum: 251.1622774899006
time_elpased: 3.191
batch start
#iterations: 60
currently lose_sum: 249.38245198130608
time_elpased: 3.191
batch start
#iterations: 61
currently lose_sum: 248.59115836024284
time_elpased: 3.191
batch start
#iterations: 62
currently lose_sum: 247.86739027500153
time_elpased: 3.179
batch start
#iterations: 63
currently lose_sum: 248.57652673125267
time_elpased: 3.196
batch start
#iterations: 64
currently lose_sum: 243.34353882074356
time_elpased: 3.183
batch start
#iterations: 65
currently lose_sum: 246.72993391752243
time_elpased: 3.19
batch start
#iterations: 66
currently lose_sum: 242.1067588031292
time_elpased: 3.189
batch start
#iterations: 67
currently lose_sum: 243.33360600471497
time_elpased: 3.193
batch start
#iterations: 68
currently lose_sum: 243.51388174295425
time_elpased: 3.191
batch start
#iterations: 69
currently lose_sum: 243.9153091609478
time_elpased: 3.204
batch start
#iterations: 70
currently lose_sum: 241.3630727827549
time_elpased: 3.192
batch start
#iterations: 71
currently lose_sum: 238.7288774996996
time_elpased: 3.188
batch start
#iterations: 72
currently lose_sum: 242.95601108670235
time_elpased: 3.196
batch start
#iterations: 73
currently lose_sum: 237.91943192481995
time_elpased: 3.19
batch start
#iterations: 74
currently lose_sum: 238.2490118443966
time_elpased: 3.204
batch start
#iterations: 75
currently lose_sum: 238.09190040826797
time_elpased: 3.188
batch start
#iterations: 76
currently lose_sum: 242.0429467856884
time_elpased: 3.194
batch start
#iterations: 77
currently lose_sum: 238.79484644532204
time_elpased: 3.217
batch start
#iterations: 78
currently lose_sum: 238.04249115288258
time_elpased: 3.201
batch start
#iterations: 79
currently lose_sum: 239.75734025239944
time_elpased: 3.204
start validation test
0.806907216495
0.834386994807
0.764322647363
0.797819516408
0.806775917721
validation finish
batch start
#iterations: 80
currently lose_sum: 238.84725277125835
time_elpased: 3.193
batch start
#iterations: 81
currently lose_sum: 235.76810002326965
time_elpased: 3.2
batch start
#iterations: 82
currently lose_sum: 236.75197619199753
time_elpased: 3.211
batch start
#iterations: 83
currently lose_sum: 237.69829282164574
time_elpased: 3.206
batch start
#iterations: 84
currently lose_sum: 237.34161898493767
time_elpased: 3.192
batch start
#iterations: 85
currently lose_sum: 238.81847348809242
time_elpased: 3.192
batch start
#iterations: 86
currently lose_sum: 238.78137838840485
time_elpased: 3.207
batch start
#iterations: 87
currently lose_sum: 239.71030896902084
time_elpased: 3.187
batch start
#iterations: 88
currently lose_sum: 235.23918549716473
time_elpased: 3.196
batch start
#iterations: 89
currently lose_sum: 237.87698051333427
time_elpased: 3.194
batch start
#iterations: 90
currently lose_sum: 235.16707149147987
time_elpased: 3.191
batch start
#iterations: 91
currently lose_sum: 235.7305428981781
time_elpased: 3.199
batch start
#iterations: 92
currently lose_sum: 236.6013804078102
time_elpased: 3.191
batch start
#iterations: 93
currently lose_sum: 233.67578491568565
time_elpased: 3.198
batch start
#iterations: 94
currently lose_sum: 233.3913748115301
time_elpased: 3.204
batch start
#iterations: 95
currently lose_sum: 235.81451362371445
time_elpased: 3.202
batch start
#iterations: 96
currently lose_sum: 232.89178474247456
time_elpased: 3.188
batch start
#iterations: 97
currently lose_sum: 233.34694585204124
time_elpased: 3.201
batch start
#iterations: 98
currently lose_sum: 234.1492931842804
time_elpased: 3.202
batch start
#iterations: 99
currently lose_sum: 231.65489400923252
time_elpased: 3.194
batch start
#iterations: 100
currently lose_sum: 234.54092326760292
time_elpased: 3.2
batch start
#iterations: 101
currently lose_sum: 230.80173902213573
time_elpased: 3.205
batch start
#iterations: 102
currently lose_sum: 229.64143884181976
time_elpased: 3.179
batch start
#iterations: 103
currently lose_sum: 229.01311269402504
time_elpased: 3.19
batch start
#iterations: 104
currently lose_sum: 231.26753675937653
time_elpased: 3.199
batch start
#iterations: 105
currently lose_sum: 234.22244909405708
time_elpased: 3.203
batch start
#iterations: 106
currently lose_sum: 230.14362186193466
time_elpased: 3.198
batch start
#iterations: 107
currently lose_sum: 232.0014734864235
time_elpased: 3.201
batch start
#iterations: 108
currently lose_sum: 228.58572851121426
time_elpased: 3.209
batch start
#iterations: 109
currently lose_sum: 229.88852021098137
time_elpased: 3.221
batch start
#iterations: 110
currently lose_sum: 230.5446798801422
time_elpased: 3.212
batch start
#iterations: 111
currently lose_sum: 227.70879851281643
time_elpased: 3.2
batch start
#iterations: 112
currently lose_sum: 230.4756509512663
time_elpased: 3.198
batch start
#iterations: 113
currently lose_sum: 229.34199368953705
time_elpased: 3.201
batch start
#iterations: 114
currently lose_sum: 227.61562287807465
time_elpased: 3.204
batch start
#iterations: 115
currently lose_sum: 228.49937748908997
time_elpased: 3.196
batch start
#iterations: 116
currently lose_sum: 226.55600361526012
time_elpased: 3.199
batch start
#iterations: 117
currently lose_sum: 228.3092267215252
time_elpased: 3.199
batch start
#iterations: 118
currently lose_sum: 226.75085358321667
time_elpased: 3.195
batch start
#iterations: 119
currently lose_sum: 227.68605783581734
time_elpased: 3.202
start validation test
0.811288659794
0.82261355095
0.792244053775
0.807143233419
0.811229940556
validation finish
batch start
#iterations: 120
currently lose_sum: 226.4688272178173
time_elpased: 3.222
batch start
#iterations: 121
currently lose_sum: 228.39614175260067
time_elpased: 3.21
batch start
#iterations: 122
currently lose_sum: 224.60510118305683
time_elpased: 3.201
batch start
#iterations: 123
currently lose_sum: 225.93001581728458
time_elpased: 3.192
batch start
#iterations: 124
currently lose_sum: 224.95941208302975
time_elpased: 3.208
batch start
#iterations: 125
currently lose_sum: 222.3798063993454
time_elpased: 3.193
batch start
#iterations: 126
currently lose_sum: 222.7148865610361
time_elpased: 3.197
batch start
#iterations: 127
currently lose_sum: 224.57226590812206
time_elpased: 3.204
batch start
#iterations: 128
currently lose_sum: 224.8863528072834
time_elpased: 3.194
batch start
#iterations: 129
currently lose_sum: 224.90700364112854
time_elpased: 3.208
batch start
#iterations: 130
currently lose_sum: 220.92670184373856
time_elpased: 3.209
batch start
#iterations: 131
currently lose_sum: 222.89975082874298
time_elpased: 3.209
batch start
#iterations: 132
currently lose_sum: 221.45314966142178
time_elpased: 3.198
batch start
#iterations: 133
currently lose_sum: 223.26756809651852
time_elpased: 3.193
batch start
#iterations: 134
currently lose_sum: 220.05300806462765
time_elpased: 3.21
batch start
#iterations: 135
currently lose_sum: 219.89401058852673
time_elpased: 3.21
batch start
#iterations: 136
currently lose_sum: 221.84028083086014
time_elpased: 3.211
batch start
#iterations: 137
currently lose_sum: 220.46970088779926
time_elpased: 3.2
batch start
#iterations: 138
currently lose_sum: 220.4433713555336
time_elpased: 3.207
batch start
#iterations: 139
currently lose_sum: 219.90998078882694
time_elpased: 3.203
batch start
#iterations: 140
currently lose_sum: 219.4550725221634
time_elpased: 3.207
batch start
#iterations: 141
currently lose_sum: 218.65341365337372
time_elpased: 3.202
batch start
#iterations: 142
currently lose_sum: 219.585921600461
time_elpased: 3.199
batch start
#iterations: 143
currently lose_sum: 218.65553042292595
time_elpased: 3.206
batch start
#iterations: 144
currently lose_sum: 217.93788895010948
time_elpased: 3.205
batch start
#iterations: 145
currently lose_sum: 218.38297559320927
time_elpased: 3.217
batch start
#iterations: 146
currently lose_sum: 218.2450043708086
time_elpased: 3.2
batch start
#iterations: 147
currently lose_sum: 218.05652306973934
time_elpased: 3.202
batch start
#iterations: 148
currently lose_sum: 217.69916778802872
time_elpased: 3.202
batch start
#iterations: 149
currently lose_sum: 219.43110631406307
time_elpased: 3.219
batch start
#iterations: 150
currently lose_sum: 220.2123156040907
time_elpased: 3.192
batch start
#iterations: 151
currently lose_sum: 215.82767756283283
time_elpased: 3.202
batch start
#iterations: 152
currently lose_sum: 215.4514201283455
time_elpased: 3.19
batch start
#iterations: 153
currently lose_sum: 213.23198294639587
time_elpased: 3.2
batch start
#iterations: 154
currently lose_sum: 215.6172822713852
time_elpased: 3.199
batch start
#iterations: 155
currently lose_sum: 215.70325300097466
time_elpased: 3.198
batch start
#iterations: 156
currently lose_sum: 211.4468572884798
time_elpased: 3.203
batch start
#iterations: 157
currently lose_sum: 215.27040147781372
time_elpased: 3.196
batch start
#iterations: 158
currently lose_sum: 210.79720228910446
time_elpased: 3.188
batch start
#iterations: 159
currently lose_sum: 210.8940228074789
time_elpased: 3.207
start validation test
0.805154639175
0.795860960418
0.819234746639
0.807378719935
0.805198051634
validation finish
batch start
#iterations: 160
currently lose_sum: 212.75894609093666
time_elpased: 3.203
batch start
#iterations: 161
currently lose_sum: 210.48017443716526
time_elpased: 3.189
batch start
#iterations: 162
currently lose_sum: 211.75772930681705
time_elpased: 3.193
batch start
#iterations: 163
currently lose_sum: 209.23488864302635
time_elpased: 3.189
batch start
#iterations: 164
currently lose_sum: 210.10169062018394
time_elpased: 3.193
batch start
#iterations: 165
currently lose_sum: 210.23680777847767
time_elpased: 3.197
batch start
#iterations: 166
currently lose_sum: 210.5437961369753
time_elpased: 3.193
batch start
#iterations: 167
currently lose_sum: 207.714899122715
time_elpased: 3.198
batch start
#iterations: 168
currently lose_sum: 210.57354928553104
time_elpased: 3.187
batch start
#iterations: 169
currently lose_sum: 207.69917234778404
time_elpased: 3.202
batch start
#iterations: 170
currently lose_sum: 209.81320522725582
time_elpased: 3.192
batch start
#iterations: 171
currently lose_sum: 208.4745086580515
time_elpased: 3.198
batch start
#iterations: 172
currently lose_sum: 207.76200607419014
time_elpased: 3.193
batch start
#iterations: 173
currently lose_sum: 208.34477469325066
time_elpased: 3.187
batch start
#iterations: 174
currently lose_sum: 207.23069551587105
time_elpased: 3.193
batch start
#iterations: 175
currently lose_sum: 203.19704501330853
time_elpased: 3.187
batch start
#iterations: 176
currently lose_sum: 205.2555175423622
time_elpased: 3.195
batch start
#iterations: 177
currently lose_sum: 205.10986590385437
time_elpased: 3.201
batch start
#iterations: 178
currently lose_sum: 204.6557675153017
time_elpased: 3.196
batch start
#iterations: 179
currently lose_sum: 204.37257996201515
time_elpased: 3.19
batch start
#iterations: 180
currently lose_sum: 202.9333518743515
time_elpased: 3.195
batch start
#iterations: 181
currently lose_sum: 202.75984351336956
time_elpased: 3.193
batch start
#iterations: 182
currently lose_sum: 199.64170809090137
time_elpased: 3.187
batch start
#iterations: 183
currently lose_sum: 202.35167364776134
time_elpased: 3.196
batch start
#iterations: 184
currently lose_sum: 203.72342345118523
time_elpased: 3.204
batch start
#iterations: 185
currently lose_sum: 200.2725836187601
time_elpased: 3.209
batch start
#iterations: 186
currently lose_sum: 201.01587828993797
time_elpased: 3.213
batch start
#iterations: 187
currently lose_sum: 202.70005808770657
time_elpased: 3.191
batch start
#iterations: 188
currently lose_sum: 199.58943063020706
time_elpased: 3.176
batch start
#iterations: 189
currently lose_sum: 196.260056078434
time_elpased: 3.182
batch start
#iterations: 190
currently lose_sum: 197.21219047904015
time_elpased: 3.175
batch start
#iterations: 191
currently lose_sum: 196.74524250626564
time_elpased: 3.206
batch start
#iterations: 192
currently lose_sum: 196.91725336015224
time_elpased: 3.204
batch start
#iterations: 193
currently lose_sum: 196.71112176775932
time_elpased: 3.2
batch start
#iterations: 194
currently lose_sum: 193.23027348518372
time_elpased: 3.197
batch start
#iterations: 195
currently lose_sum: 196.82554088532925
time_elpased: 3.188
batch start
#iterations: 196
currently lose_sum: 195.61835169792175
time_elpased: 3.191
batch start
#iterations: 197
currently lose_sum: 196.3597083836794
time_elpased: 3.206
batch start
#iterations: 198
currently lose_sum: 192.6249112188816
time_elpased: 3.194
batch start
#iterations: 199
currently lose_sum: 192.27508564293385
time_elpased: 3.19
start validation test
0.805979381443
0.810449957948
0.797207859359
0.803774371807
0.805952336668
validation finish
batch start
#iterations: 200
currently lose_sum: 192.06682343780994
time_elpased: 3.196
batch start
#iterations: 201
currently lose_sum: 192.0533057898283
time_elpased: 3.201
batch start
#iterations: 202
currently lose_sum: 190.45764020085335
time_elpased: 3.212
batch start
#iterations: 203
currently lose_sum: 190.06696717441082
time_elpased: 3.197
batch start
#iterations: 204
currently lose_sum: 192.45042541623116
time_elpased: 3.199
batch start
#iterations: 205
currently lose_sum: 190.28618203103542
time_elpased: 3.196
batch start
#iterations: 206
currently lose_sum: 190.91362999379635
time_elpased: 3.214
batch start
#iterations: 207
currently lose_sum: 186.8264766484499
time_elpased: 3.204
batch start
#iterations: 208
currently lose_sum: 188.57472218573093
time_elpased: 3.198
batch start
#iterations: 209
currently lose_sum: 188.4517486989498
time_elpased: 3.193
batch start
#iterations: 210
currently lose_sum: 185.83495558798313
time_elpased: 3.202
batch start
#iterations: 211
currently lose_sum: 186.99024304747581
time_elpased: 3.19
batch start
#iterations: 212
currently lose_sum: 184.9364096224308
time_elpased: 3.198
batch start
#iterations: 213
currently lose_sum: 185.73749448359013
time_elpased: 3.196
batch start
#iterations: 214
currently lose_sum: 185.27351880073547
time_elpased: 3.197
batch start
#iterations: 215
currently lose_sum: 185.75810454785824
time_elpased: 3.2
batch start
#iterations: 216
currently lose_sum: 181.9301998615265
time_elpased: 3.201
batch start
#iterations: 217
currently lose_sum: 178.6820434331894
time_elpased: 3.198
batch start
#iterations: 218
currently lose_sum: 179.25905765593052
time_elpased: 3.185
batch start
#iterations: 219
currently lose_sum: 179.49070554971695
time_elpased: 3.201
batch start
#iterations: 220
currently lose_sum: 179.10615120828152
time_elpased: 3.199
batch start
#iterations: 221
currently lose_sum: 180.74226930737495
time_elpased: 3.2
batch start
#iterations: 222
currently lose_sum: 177.6034653186798
time_elpased: 3.203
batch start
#iterations: 223
currently lose_sum: 179.23311726748943
time_elpased: 3.207
batch start
#iterations: 224
currently lose_sum: 177.61361522972584
time_elpased: 3.197
batch start
#iterations: 225
currently lose_sum: 175.79001486301422
time_elpased: 3.189
batch start
#iterations: 226
currently lose_sum: 175.88891325891018
time_elpased: 3.196
batch start
#iterations: 227
currently lose_sum: 176.25534602999687
time_elpased: 3.188
batch start
#iterations: 228
currently lose_sum: 175.28272245824337
time_elpased: 3.198
batch start
#iterations: 229
currently lose_sum: 173.48842184245586
time_elpased: 3.183
batch start
#iterations: 230
currently lose_sum: 171.9783651381731
time_elpased: 3.195
batch start
#iterations: 231
currently lose_sum: 173.78765623271465
time_elpased: 3.199
batch start
#iterations: 232
currently lose_sum: 170.1828989237547
time_elpased: 3.205
batch start
#iterations: 233
currently lose_sum: 170.88279835879803
time_elpased: 3.202
batch start
#iterations: 234
currently lose_sum: 168.33911029994488
time_elpased: 3.175
batch start
#iterations: 235
currently lose_sum: 169.39781963825226
time_elpased: 3.181
batch start
#iterations: 236
currently lose_sum: 168.28265251219273
time_elpased: 3.19
batch start
#iterations: 237
currently lose_sum: 170.40444321930408
time_elpased: 3.207
batch start
#iterations: 238
currently lose_sum: 168.84357669949532
time_elpased: 3.196
batch start
#iterations: 239
currently lose_sum: 164.39132672548294
time_elpased: 3.191
start validation test
0.803608247423
0.811569544875
0.789245087901
0.800251651463
0.803563962244
validation finish
batch start
#iterations: 240
currently lose_sum: 163.23363584280014
time_elpased: 3.186
batch start
#iterations: 241
currently lose_sum: 163.94206741452217
time_elpased: 3.202
batch start
#iterations: 242
currently lose_sum: 163.21740600466728
time_elpased: 3.174
batch start
#iterations: 243
currently lose_sum: 163.22787503898144
time_elpased: 3.181
batch start
#iterations: 244
currently lose_sum: 164.06959742307663
time_elpased: 3.17
batch start
#iterations: 245
currently lose_sum: 161.44947902858257
time_elpased: 3.171
batch start
#iterations: 246
currently lose_sum: 161.44527678191662
time_elpased: 3.188
batch start
#iterations: 247
currently lose_sum: 163.9652190208435
time_elpased: 3.203
batch start
#iterations: 248
currently lose_sum: 161.24977779388428
time_elpased: 3.203
batch start
#iterations: 249
currently lose_sum: 157.24634071439505
time_elpased: 3.194
batch start
#iterations: 250
currently lose_sum: 157.48298926651478
time_elpased: 3.184
batch start
#iterations: 251
currently lose_sum: 158.6233950406313
time_elpased: 3.181
batch start
#iterations: 252
currently lose_sum: 155.2382847815752
time_elpased: 3.189
batch start
#iterations: 253
currently lose_sum: 157.56532363593578
time_elpased: 3.195
batch start
#iterations: 254
currently lose_sum: 153.93259949982166
time_elpased: 3.185
batch start
#iterations: 255
currently lose_sum: 155.570979103446
time_elpased: 3.187
batch start
#iterations: 256
currently lose_sum: 153.7460470944643
time_elpased: 3.195
batch start
#iterations: 257
currently lose_sum: 152.81959608197212
time_elpased: 3.195
batch start
#iterations: 258
currently lose_sum: 150.32101027667522
time_elpased: 3.191
batch start
#iterations: 259
currently lose_sum: 150.62641832232475
time_elpased: 3.184
batch start
#iterations: 260
currently lose_sum: 150.39320699870586
time_elpased: 3.19
batch start
#iterations: 261
currently lose_sum: 147.76981026679277
time_elpased: 3.2
batch start
#iterations: 262
currently lose_sum: 147.52218402177095
time_elpased: 3.203
batch start
#iterations: 263
currently lose_sum: 146.76684042066336
time_elpased: 3.193
batch start
#iterations: 264
currently lose_sum: 143.29004484415054
time_elpased: 3.2
batch start
#iterations: 265
currently lose_sum: 142.51525004208088
time_elpased: 3.19
batch start
#iterations: 266
currently lose_sum: 142.12877024710178
time_elpased: 3.188
batch start
#iterations: 267
currently lose_sum: 142.75006157159805
time_elpased: 3.193
batch start
#iterations: 268
currently lose_sum: 139.85232516378164
time_elpased: 3.204
batch start
#iterations: 269
currently lose_sum: nan
time_elpased: 3.196
train finish final lose is: nan
acc: 0.802
pre: 0.797
rec: 0.817
F1: 0.807
auc: 0.802
