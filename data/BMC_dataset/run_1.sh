#!/bin/tcsh
foreach feature_size (50 100 150 200)
	foreach L (32 64 96 128)
                python frame_work_test.py --attention $1 --L_dim $L --graph_feature_size $feature_size > ./results/log_Mia_attention\_$1_L\_$L\_$feature_size\_1.0.txt
	end
end


