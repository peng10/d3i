start to construct graph
graph construct over
58302
epochs start
batch start
#iterations: 0
currently lose_sum: 405.39737915992737
time_elpased: 1.773
batch start
#iterations: 1
currently lose_sum: 403.0587657690048
time_elpased: 1.766
batch start
#iterations: 2
currently lose_sum: 403.0652146935463
time_elpased: 1.767
batch start
#iterations: 3
currently lose_sum: 402.30494034290314
time_elpased: 1.757
batch start
#iterations: 4
currently lose_sum: 401.97187745571136
time_elpased: 1.774
batch start
#iterations: 5
currently lose_sum: 401.3502086997032
time_elpased: 1.749
batch start
#iterations: 6
currently lose_sum: 400.60192799568176
time_elpased: 1.765
batch start
#iterations: 7
currently lose_sum: 400.12386721372604
time_elpased: 1.765
batch start
#iterations: 8
currently lose_sum: 399.55093985795975
time_elpased: 1.763
batch start
#iterations: 9
currently lose_sum: 398.6648758649826
time_elpased: 1.774
batch start
#iterations: 10
currently lose_sum: 396.7763385772705
time_elpased: 1.771
batch start
#iterations: 11
currently lose_sum: 396.4498521089554
time_elpased: 1.776
batch start
#iterations: 12
currently lose_sum: 395.7318671345711
time_elpased: 1.747
batch start
#iterations: 13
currently lose_sum: 395.2693862915039
time_elpased: 1.776
batch start
#iterations: 14
currently lose_sum: 394.2640350461006
time_elpased: 1.766
batch start
#iterations: 15
currently lose_sum: 392.70461452007294
time_elpased: 1.769
batch start
#iterations: 16
currently lose_sum: 392.8909974694252
time_elpased: 1.763
batch start
#iterations: 17
currently lose_sum: 391.3641457557678
time_elpased: 1.769
batch start
#iterations: 18
currently lose_sum: 390.5863404273987
time_elpased: 1.759
batch start
#iterations: 19
currently lose_sum: 390.03393667936325
time_elpased: 1.761
start validation test
0.67587628866
0.669669081653
0.701250768915
0.685096153846
0
validation finish
batch start
#iterations: 20
currently lose_sum: 388.97430366277695
time_elpased: 1.763
batch start
#iterations: 21
currently lose_sum: 389.5693534016609
time_elpased: 1.766
batch start
#iterations: 22
currently lose_sum: 388.0904359817505
time_elpased: 1.772
batch start
#iterations: 23
currently lose_sum: 387.78389126062393
time_elpased: 1.764
batch start
#iterations: 24
currently lose_sum: 386.8274047970772
time_elpased: 1.781
batch start
#iterations: 25
currently lose_sum: 386.59835517406464
time_elpased: 1.768
batch start
#iterations: 26
currently lose_sum: 386.1193071603775
time_elpased: 1.76
batch start
#iterations: 27
currently lose_sum: 385.6219393014908
time_elpased: 1.765
batch start
#iterations: 28
currently lose_sum: 384.71820229291916
time_elpased: 1.763
batch start
#iterations: 29
currently lose_sum: 385.3602088689804
time_elpased: 1.766
batch start
#iterations: 30
currently lose_sum: 385.2183327078819
time_elpased: 1.764
batch start
#iterations: 31
currently lose_sum: 384.9943336844444
time_elpased: 1.77
batch start
#iterations: 32
currently lose_sum: 385.0513170361519
time_elpased: 1.776
batch start
#iterations: 33
currently lose_sum: 384.3407825231552
time_elpased: 1.775
batch start
#iterations: 34
currently lose_sum: 382.7369487285614
time_elpased: 1.763
batch start
#iterations: 35
currently lose_sum: 383.5597496032715
time_elpased: 1.771
batch start
#iterations: 36
currently lose_sum: 382.2171600461006
time_elpased: 1.772
batch start
#iterations: 37
currently lose_sum: 382.61209839582443
time_elpased: 1.764
batch start
#iterations: 38
currently lose_sum: 382.2578934431076
time_elpased: 1.774
batch start
#iterations: 39
currently lose_sum: 381.69399267435074
time_elpased: 1.767
start validation test
0.693350515464
0.648482010458
0.851855648965
0.736384986928
0
validation finish
batch start
#iterations: 40
currently lose_sum: 381.8622728586197
time_elpased: 1.777
batch start
#iterations: 41
currently lose_sum: 381.5029783844948
time_elpased: 1.77
batch start
#iterations: 42
currently lose_sum: 381.31074196100235
time_elpased: 1.778
batch start
#iterations: 43
currently lose_sum: 379.70386350154877
time_elpased: 1.768
batch start
#iterations: 44
currently lose_sum: 379.4257422089577
time_elpased: 1.782
batch start
#iterations: 45
currently lose_sum: 380.66750061511993
time_elpased: 1.761
batch start
#iterations: 46
currently lose_sum: 379.7148258090019
time_elpased: 1.776
batch start
#iterations: 47
currently lose_sum: 380.88970524072647
time_elpased: 1.766
batch start
#iterations: 48
currently lose_sum: 380.6793748140335
time_elpased: 1.763
batch start
#iterations: 49
currently lose_sum: 380.25820475816727
time_elpased: 1.779
batch start
#iterations: 50
currently lose_sum: 379.1958698630333
time_elpased: 1.775
batch start
#iterations: 51
currently lose_sum: 378.5760895013809
time_elpased: 1.781
batch start
#iterations: 52
currently lose_sum: 378.7514227628708
time_elpased: 1.763
batch start
#iterations: 53
currently lose_sum: 378.98033779859543
time_elpased: 1.769
batch start
#iterations: 54
currently lose_sum: 379.55957210063934
time_elpased: 1.763
batch start
#iterations: 55
currently lose_sum: 378.567102432251
time_elpased: 1.787
batch start
#iterations: 56
currently lose_sum: 377.39708030223846
time_elpased: 1.778
batch start
#iterations: 57
currently lose_sum: 376.8910332918167
time_elpased: 1.766
batch start
#iterations: 58
currently lose_sum: 377.9740557074547
time_elpased: 1.769
batch start
#iterations: 59
currently lose_sum: 377.8021525144577
time_elpased: 1.762
start validation test
0.740824742268
0.773622047244
0.684949764199
0.726590538336
0
validation finish
batch start
#iterations: 60
currently lose_sum: 377.63421726226807
time_elpased: 1.772
batch start
#iterations: 61
currently lose_sum: 377.56183809041977
time_elpased: 1.767
batch start
#iterations: 62
currently lose_sum: 377.25416374206543
time_elpased: 1.782
batch start
#iterations: 63
currently lose_sum: 376.7996869087219
time_elpased: 1.759
batch start
#iterations: 64
currently lose_sum: 376.83914732933044
time_elpased: 1.77
batch start
#iterations: 65
currently lose_sum: 376.08235281705856
time_elpased: 1.766
batch start
#iterations: 66
currently lose_sum: 376.2424041032791
time_elpased: 1.777
batch start
#iterations: 67
currently lose_sum: 375.309887111187
time_elpased: 1.767
batch start
#iterations: 68
currently lose_sum: 376.2753870487213
time_elpased: 1.759
batch start
#iterations: 69
currently lose_sum: 376.4750971198082
time_elpased: 1.777
batch start
#iterations: 70
currently lose_sum: 377.1018882393837
time_elpased: 1.769
batch start
#iterations: 71
currently lose_sum: 375.9390835762024
time_elpased: 1.785
batch start
#iterations: 72
currently lose_sum: 373.89825439453125
time_elpased: 1.766
batch start
#iterations: 73
currently lose_sum: 375.3272449374199
time_elpased: 1.767
batch start
#iterations: 74
currently lose_sum: 375.236152112484
time_elpased: 1.776
batch start
#iterations: 75
currently lose_sum: 376.0135101079941
time_elpased: 1.771
batch start
#iterations: 76
currently lose_sum: 375.3105551600456
time_elpased: 1.768
batch start
#iterations: 77
currently lose_sum: 373.8089163303375
time_elpased: 1.757
batch start
#iterations: 78
currently lose_sum: 374.91313821077347
time_elpased: 1.781
batch start
#iterations: 79
currently lose_sum: 373.696240067482
time_elpased: 1.765
start validation test
0.75675257732
0.775588396278
0.726368669264
0.750172057811
0
validation finish
batch start
#iterations: 80
currently lose_sum: 374.81582736968994
time_elpased: 1.776
batch start
#iterations: 81
currently lose_sum: 375.67104732990265
time_elpased: 1.766
batch start
#iterations: 82
currently lose_sum: 373.99658727645874
time_elpased: 1.776
batch start
#iterations: 83
currently lose_sum: 372.28974509239197
time_elpased: 1.762
batch start
#iterations: 84
currently lose_sum: 373.5358432531357
time_elpased: 1.773
batch start
#iterations: 85
currently lose_sum: 373.7226679325104
time_elpased: 1.777
batch start
#iterations: 86
currently lose_sum: 373.7171792984009
time_elpased: 1.761
batch start
#iterations: 87
currently lose_sum: 374.211330473423
time_elpased: 1.771
batch start
#iterations: 88
currently lose_sum: 372.72611105442047
time_elpased: 1.76
batch start
#iterations: 89
currently lose_sum: 372.74480950832367
time_elpased: 1.777
batch start
#iterations: 90
currently lose_sum: 375.0383685231209
time_elpased: 1.759
batch start
#iterations: 91
currently lose_sum: 371.61718916893005
time_elpased: 1.771
batch start
#iterations: 92
currently lose_sum: 376.0641521215439
time_elpased: 1.774
batch start
#iterations: 93
currently lose_sum: 373.0347703099251
time_elpased: 1.773
batch start
#iterations: 94
currently lose_sum: 373.5245786309242
time_elpased: 1.767
batch start
#iterations: 95
currently lose_sum: 372.18921262025833
time_elpased: 1.769
batch start
#iterations: 96
currently lose_sum: 374.64819544553757
time_elpased: 1.774
batch start
#iterations: 97
currently lose_sum: 372.157553255558
time_elpased: 1.768
batch start
#iterations: 98
currently lose_sum: 373.2952790260315
time_elpased: 1.779
batch start
#iterations: 99
currently lose_sum: 371.45001804828644
time_elpased: 1.772
start validation test
0.758608247423
0.769189935237
0.742772196022
0.755750273823
0
validation finish
batch start
#iterations: 100
currently lose_sum: 373.13490837812424
time_elpased: 1.781
batch start
#iterations: 101
currently lose_sum: 373.6984187364578
time_elpased: 1.758
batch start
#iterations: 102
currently lose_sum: 372.1760635972023
time_elpased: 1.769
batch start
#iterations: 103
currently lose_sum: 373.09366911649704
time_elpased: 1.77
batch start
#iterations: 104
currently lose_sum: 372.7092704772949
time_elpased: 1.77
batch start
#iterations: 105
currently lose_sum: 373.1373706459999
time_elpased: 1.77
batch start
#iterations: 106
currently lose_sum: 372.5692492723465
time_elpased: 1.768
batch start
#iterations: 107
currently lose_sum: 373.7004538178444
time_elpased: 1.775
batch start
#iterations: 108
currently lose_sum: 372.10156762599945
time_elpased: 1.772
batch start
#iterations: 109
currently lose_sum: 372.5089908838272
time_elpased: 1.77
batch start
#iterations: 110
currently lose_sum: 373.6239495873451
time_elpased: 1.77
batch start
#iterations: 111
currently lose_sum: 370.60152316093445
time_elpased: 1.771
batch start
#iterations: 112
currently lose_sum: 371.34326434135437
time_elpased: 1.774
batch start
#iterations: 113
currently lose_sum: 371.96618765592575
time_elpased: 1.776
batch start
#iterations: 114
currently lose_sum: 372.02438133955
time_elpased: 1.775
batch start
#iterations: 115
currently lose_sum: 370.9891322851181
time_elpased: 1.767
batch start
#iterations: 116
currently lose_sum: 371.35728842020035
time_elpased: 1.784
batch start
#iterations: 117
currently lose_sum: 371.36191642284393
time_elpased: 1.765
batch start
#iterations: 118
currently lose_sum: 370.77869868278503
time_elpased: 1.777
batch start
#iterations: 119
currently lose_sum: 371.2116101384163
time_elpased: 1.768
start validation test
0.726958762887
0.815517485488
0.590526963297
0.685021109591
0
validation finish
batch start
#iterations: 120
currently lose_sum: 370.53792214393616
time_elpased: 1.776
batch start
#iterations: 121
currently lose_sum: 370.4530930519104
time_elpased: 1.764
batch start
#iterations: 122
currently lose_sum: 371.6469376683235
time_elpased: 1.771
batch start
#iterations: 123
currently lose_sum: 370.64951425790787
time_elpased: 1.777
batch start
#iterations: 124
currently lose_sum: 370.5177869796753
time_elpased: 1.777
batch start
#iterations: 125
currently lose_sum: 370.5096333026886
time_elpased: 1.772
batch start
#iterations: 126
currently lose_sum: 369.5637817978859
time_elpased: 1.766
batch start
#iterations: 127
currently lose_sum: 369.5735165476799
time_elpased: 1.779
batch start
#iterations: 128
currently lose_sum: 370.387058198452
time_elpased: 1.776
batch start
#iterations: 129
currently lose_sum: 370.8868175148964
time_elpased: 1.776
batch start
#iterations: 130
currently lose_sum: 370.193299472332
time_elpased: 1.768
batch start
#iterations: 131
currently lose_sum: 370.2288301587105
time_elpased: 1.763
batch start
#iterations: 132
currently lose_sum: 369.95660293102264
time_elpased: 1.777
batch start
#iterations: 133
currently lose_sum: 369.71875858306885
time_elpased: 1.782
batch start
#iterations: 134
currently lose_sum: 368.7788578271866
time_elpased: 1.775
batch start
#iterations: 135
currently lose_sum: 369.9312934875488
time_elpased: 1.774
batch start
#iterations: 136
currently lose_sum: 368.95465356111526
time_elpased: 1.782
batch start
#iterations: 137
currently lose_sum: 371.1066419482231
time_elpased: 1.768
batch start
#iterations: 138
currently lose_sum: 370.1422479748726
time_elpased: 1.776
batch start
#iterations: 139
currently lose_sum: 369.8640228509903
time_elpased: 1.772
start validation test
0.766855670103
0.78242090487
0.742874718064
0.762135156455
0
validation finish
batch start
#iterations: 140
currently lose_sum: 370.43163269758224
time_elpased: 1.778
batch start
#iterations: 141
currently lose_sum: 369.46319299936295
time_elpased: 1.768
batch start
#iterations: 142
currently lose_sum: 370.3149898648262
time_elpased: 1.779
batch start
#iterations: 143
currently lose_sum: 368.68847090005875
time_elpased: 1.774
batch start
#iterations: 144
currently lose_sum: 370.743768453598
time_elpased: 1.773
batch start
#iterations: 145
currently lose_sum: 368.81857693195343
time_elpased: 1.776
batch start
#iterations: 146
currently lose_sum: 370.0722657442093
time_elpased: 1.78
batch start
#iterations: 147
currently lose_sum: 369.4238688349724
time_elpased: 1.786
batch start
#iterations: 148
currently lose_sum: 369.6928126811981
time_elpased: 1.785
batch start
#iterations: 149
currently lose_sum: 369.17892175912857
time_elpased: 1.775
batch start
#iterations: 150
currently lose_sum: 369.64311826229095
time_elpased: 1.773
batch start
#iterations: 151
currently lose_sum: 369.7208134531975
time_elpased: 1.772
batch start
#iterations: 152
currently lose_sum: 368.7681514620781
time_elpased: 1.781
batch start
#iterations: 153
currently lose_sum: 369.1457494497299
time_elpased: 1.773
batch start
#iterations: 154
currently lose_sum: 367.03049689531326
time_elpased: 1.779
batch start
#iterations: 155
currently lose_sum: 369.8001746535301
time_elpased: 1.773
batch start
#iterations: 156
currently lose_sum: 368.03860157728195
time_elpased: 1.793
batch start
#iterations: 157
currently lose_sum: 368.4942662715912
time_elpased: 1.772
batch start
#iterations: 158
currently lose_sum: 368.089912712574
time_elpased: 1.77
batch start
#iterations: 159
currently lose_sum: 368.5408504605293
time_elpased: 1.773
start validation test
0.77087628866
0.774140245792
0.768505228624
0.771312445336
0
validation finish
batch start
#iterations: 160
currently lose_sum: 370.74088233709335
time_elpased: 1.775
batch start
#iterations: 161
currently lose_sum: 369.2964930534363
time_elpased: 1.775
batch start
#iterations: 162
currently lose_sum: 368.50593400001526
time_elpased: 1.782
batch start
#iterations: 163
currently lose_sum: 369.56621438264847
time_elpased: 1.765
batch start
#iterations: 164
currently lose_sum: 368.96319115161896
time_elpased: 1.774
batch start
#iterations: 165
currently lose_sum: 369.3049179315567
time_elpased: 1.776
batch start
#iterations: 166
currently lose_sum: 368.99874943494797
time_elpased: 1.772
batch start
#iterations: 167
currently lose_sum: 368.4841905236244
time_elpased: 1.783
batch start
#iterations: 168
currently lose_sum: 368.4597741961479
time_elpased: 1.766
batch start
#iterations: 169
currently lose_sum: 368.9150930047035
time_elpased: 1.771
batch start
#iterations: 170
currently lose_sum: 367.54972636699677
time_elpased: 1.783
batch start
#iterations: 171
currently lose_sum: 368.09867399930954
time_elpased: 1.765
batch start
#iterations: 172
currently lose_sum: 368.2587434053421
time_elpased: 1.78
batch start
#iterations: 173
currently lose_sum: 368.3938084244728
time_elpased: 1.754
batch start
#iterations: 174
currently lose_sum: 369.33580219745636
time_elpased: 1.76
batch start
#iterations: 175
currently lose_sum: 369.11268121004105
time_elpased: 1.761
batch start
#iterations: 176
currently lose_sum: 368.9593608379364
time_elpased: 1.779
batch start
#iterations: 177
currently lose_sum: 368.26565384864807
time_elpased: 1.751
batch start
#iterations: 178
currently lose_sum: 368.24924808740616
time_elpased: 1.77
batch start
#iterations: 179
currently lose_sum: 368.9320365190506
time_elpased: 1.754
start validation test
0.765360824742
0.768310295028
0.763584170597
0.765939942411
0
validation finish
batch start
#iterations: 180
currently lose_sum: 367.8452682495117
time_elpased: 1.763
batch start
#iterations: 181
currently lose_sum: 367.16955357789993
time_elpased: 1.761
batch start
#iterations: 182
currently lose_sum: 368.1314607858658
time_elpased: 1.775
batch start
#iterations: 183
currently lose_sum: 368.3048627972603
time_elpased: 1.778
batch start
#iterations: 184
currently lose_sum: 367.24367702007294
time_elpased: 1.774
batch start
#iterations: 185
currently lose_sum: 367.06876373291016
time_elpased: 1.786
batch start
#iterations: 186
currently lose_sum: 367.9254814386368
time_elpased: 1.783
batch start
#iterations: 187
currently lose_sum: 368.3958665728569
time_elpased: 1.778
batch start
#iterations: 188
currently lose_sum: 365.772794008255
time_elpased: 1.772
batch start
#iterations: 189
currently lose_sum: 368.00756496191025
time_elpased: 1.78
batch start
#iterations: 190
currently lose_sum: 366.87499195337296
time_elpased: 1.787
batch start
#iterations: 191
currently lose_sum: 366.3282739520073
time_elpased: 1.777
batch start
#iterations: 192
currently lose_sum: 367.4401928782463
time_elpased: 1.771
batch start
#iterations: 193
currently lose_sum: 366.67156648635864
time_elpased: 1.78
batch start
#iterations: 194
currently lose_sum: 367.09530836343765
time_elpased: 1.771
batch start
#iterations: 195
currently lose_sum: 366.27943444252014
time_elpased: 1.769
batch start
#iterations: 196
currently lose_sum: 368.29761785268784
time_elpased: 1.786
batch start
#iterations: 197
currently lose_sum: 365.75988644361496
time_elpased: 1.778
batch start
#iterations: 198
currently lose_sum: 367.32411456108093
time_elpased: 1.787
batch start
#iterations: 199
currently lose_sum: 367.5351804494858
time_elpased: 1.765
start validation test
0.772525773196
0.772973525503
0.775271683412
0.774120898807
0
validation finish
batch start
#iterations: 200
currently lose_sum: 367.46831476688385
time_elpased: 1.78
batch start
#iterations: 201
currently lose_sum: 366.6164095401764
time_elpased: 1.78
batch start
#iterations: 202
currently lose_sum: 366.7028411626816
time_elpased: 1.765
batch start
#iterations: 203
currently lose_sum: 368.52993136644363
time_elpased: 1.782
batch start
#iterations: 204
currently lose_sum: 368.0222007036209
time_elpased: 1.776
batch start
#iterations: 205
currently lose_sum: 366.2790495157242
time_elpased: 1.786
batch start
#iterations: 206
currently lose_sum: 367.1798396706581
time_elpased: 1.776
batch start
#iterations: 207
currently lose_sum: 366.64400070905685
time_elpased: 1.784
batch start
#iterations: 208
currently lose_sum: 367.8718719482422
time_elpased: 1.769
batch start
#iterations: 209
currently lose_sum: 365.6295477747917
time_elpased: 1.776
batch start
#iterations: 210
currently lose_sum: 366.00003200769424
time_elpased: 1.784
batch start
#iterations: 211
currently lose_sum: 366.45195639133453
time_elpased: 1.775
batch start
#iterations: 212
currently lose_sum: 366.6228567957878
time_elpased: 1.784
batch start
#iterations: 213
currently lose_sum: 367.02130740880966
time_elpased: 1.775
batch start
#iterations: 214
currently lose_sum: 366.83001309633255
time_elpased: 1.789
batch start
#iterations: 215
currently lose_sum: 365.80580335855484
time_elpased: 1.768
batch start
#iterations: 216
currently lose_sum: 366.19256538152695
time_elpased: 1.773
batch start
#iterations: 217
currently lose_sum: 366.8896976709366
time_elpased: 1.769
batch start
#iterations: 218
currently lose_sum: 365.9162765741348
time_elpased: 1.775
batch start
#iterations: 219
currently lose_sum: 365.785116314888
time_elpased: 1.785
start validation test
0.771701030928
0.791780821918
0.740721755177
0.765400709783
0
validation finish
batch start
#iterations: 220
currently lose_sum: 367.53178972005844
time_elpased: 1.781
batch start
#iterations: 221
currently lose_sum: 366.8134096264839
time_elpased: 1.779
batch start
#iterations: 222
currently lose_sum: 365.8523671030998
time_elpased: 1.783
batch start
#iterations: 223
currently lose_sum: 365.58867144584656
time_elpased: 1.775
batch start
#iterations: 224
currently lose_sum: 366.51033145189285
time_elpased: 1.777
batch start
#iterations: 225
currently lose_sum: 367.78074872493744
time_elpased: 1.792
batch start
#iterations: 226
currently lose_sum: 365.041391313076
time_elpased: 1.784
batch start
#iterations: 227
currently lose_sum: 365.90830689668655
time_elpased: 1.789
batch start
#iterations: 228
currently lose_sum: 366.86226880550385
time_elpased: 1.772
batch start
#iterations: 229
currently lose_sum: 366.0245691537857
time_elpased: 1.782
batch start
#iterations: 230
currently lose_sum: 366.68059915304184
time_elpased: 1.772
batch start
#iterations: 231
currently lose_sum: 366.4648146033287
time_elpased: 1.762
batch start
#iterations: 232
currently lose_sum: 367.52622306346893
time_elpased: 1.779
batch start
#iterations: 233
currently lose_sum: 366.51953452825546
time_elpased: 1.763
batch start
#iterations: 234
currently lose_sum: 365.1473850607872
time_elpased: 1.783
batch start
#iterations: 235
currently lose_sum: 365.51965922117233
time_elpased: 1.757
batch start
#iterations: 236
currently lose_sum: 366.30752581357956
time_elpased: 1.772
batch start
#iterations: 237
currently lose_sum: 365.6651858687401
time_elpased: 1.774
batch start
#iterations: 238
currently lose_sum: 366.9810379743576
time_elpased: 1.775
batch start
#iterations: 239
currently lose_sum: 368.3933581709862
time_elpased: 1.775
start validation test
0.759742268041
0.809529597666
0.682796801312
0.740781936489
0
validation finish
batch start
#iterations: 240
currently lose_sum: 365.75263780355453
time_elpased: 1.765
batch start
#iterations: 241
currently lose_sum: 365.7832045555115
time_elpased: 1.757
batch start
#iterations: 242
currently lose_sum: 365.29678094387054
time_elpased: 1.766
batch start
#iterations: 243
currently lose_sum: 367.2951253056526
time_elpased: 1.78
batch start
#iterations: 244
currently lose_sum: 367.53252214193344
time_elpased: 1.769
batch start
#iterations: 245
currently lose_sum: 367.054302752018
time_elpased: 1.79
batch start
#iterations: 246
currently lose_sum: 365.8640180826187
time_elpased: 1.753
batch start
#iterations: 247
currently lose_sum: 366.9088141322136
time_elpased: 1.781
batch start
#iterations: 248
currently lose_sum: 366.3471179008484
time_elpased: 1.755
batch start
#iterations: 249
currently lose_sum: 364.6854338645935
time_elpased: 1.763
batch start
#iterations: 250
currently lose_sum: 365.3832646012306
time_elpased: 1.769
batch start
#iterations: 251
currently lose_sum: 365.6296783089638
time_elpased: 1.78
batch start
#iterations: 252
currently lose_sum: 367.38913971185684
time_elpased: 1.773
batch start
#iterations: 253
currently lose_sum: 367.2925162911415
time_elpased: 1.754
batch start
#iterations: 254
currently lose_sum: 364.9480758905411
time_elpased: 1.78
batch start
#iterations: 255
currently lose_sum: 365.91482877731323
time_elpased: 1.765
batch start
#iterations: 256
currently lose_sum: 365.9126287102699
time_elpased: 1.769
batch start
#iterations: 257
currently lose_sum: 366.0159341096878
time_elpased: 1.755
batch start
#iterations: 258
currently lose_sum: 365.17931431531906
time_elpased: 1.767
batch start
#iterations: 259
currently lose_sum: 364.80304008722305
time_elpased: 1.756
start validation test
0.777422680412
0.775268381608
0.78480623334
0.78000815162
0
validation finish
batch start
#iterations: 260
currently lose_sum: 365.9841743707657
time_elpased: 1.752
batch start
#iterations: 261
currently lose_sum: 365.3966775536537
time_elpased: 1.777
batch start
#iterations: 262
currently lose_sum: 366.3022756576538
time_elpased: 1.785
batch start
#iterations: 263
currently lose_sum: 365.6448562145233
time_elpased: 1.786
batch start
#iterations: 264
currently lose_sum: 365.0265253186226
time_elpased: 1.78
batch start
#iterations: 265
currently lose_sum: 366.10908085107803
time_elpased: 1.783
batch start
#iterations: 266
currently lose_sum: 365.7507561445236
time_elpased: 1.757
batch start
#iterations: 267
currently lose_sum: 365.80111515522003
time_elpased: 1.783
batch start
#iterations: 268
currently lose_sum: 365.1235692501068
time_elpased: 1.777
batch start
#iterations: 269
currently lose_sum: 365.06128919124603
time_elpased: 1.776
batch start
#iterations: 270
currently lose_sum: 364.78749245405197
time_elpased: 1.781
batch start
#iterations: 271
currently lose_sum: 364.3967052102089
time_elpased: 1.774
batch start
#iterations: 272
currently lose_sum: 365.00934767723083
time_elpased: 1.781
batch start
#iterations: 273
currently lose_sum: 364.0194384455681
time_elpased: 1.766
batch start
#iterations: 274
currently lose_sum: 366.36318135261536
time_elpased: 1.768
batch start
#iterations: 275
currently lose_sum: 365.59259301424026
time_elpased: 1.752
batch start
#iterations: 276
currently lose_sum: 365.9674172401428
time_elpased: 1.761
batch start
#iterations: 277
currently lose_sum: 366.16519796848297
time_elpased: 1.752
batch start
#iterations: 278
currently lose_sum: 365.06585144996643
time_elpased: 1.773
batch start
#iterations: 279
currently lose_sum: 364.5926327109337
time_elpased: 1.765
start validation test
0.779793814433
0.774979935795
0.791982776297
0.78338910861
0
validation finish
batch start
#iterations: 280
currently lose_sum: 364.453426361084
time_elpased: 1.749
batch start
#iterations: 281
currently lose_sum: 365.16737043857574
time_elpased: 1.764
batch start
#iterations: 282
currently lose_sum: 366.3504950404167
time_elpased: 1.77
batch start
#iterations: 283
currently lose_sum: 366.55342584848404
time_elpased: 1.8
batch start
#iterations: 284
currently lose_sum: 365.0070288181305
time_elpased: 1.768
batch start
#iterations: 285
currently lose_sum: 364.4588323235512
time_elpased: 1.795
batch start
#iterations: 286
currently lose_sum: 363.8123981356621
time_elpased: 1.784
batch start
#iterations: 287
currently lose_sum: 364.262670814991
time_elpased: 1.786
batch start
#iterations: 288
currently lose_sum: 366.638865172863
time_elpased: 1.777
batch start
#iterations: 289
currently lose_sum: 364.2748586535454
time_elpased: 1.779
batch start
#iterations: 290
currently lose_sum: 365.15762516856194
time_elpased: 1.784
batch start
#iterations: 291
currently lose_sum: 363.6679828763008
time_elpased: 1.786
batch start
#iterations: 292
currently lose_sum: 364.7082875967026
time_elpased: 1.796
batch start
#iterations: 293
currently lose_sum: 364.15757101774216
time_elpased: 1.793
batch start
#iterations: 294
currently lose_sum: 364.6735516190529
time_elpased: 1.797
batch start
#iterations: 295
currently lose_sum: 363.1888198852539
time_elpased: 1.783
batch start
#iterations: 296
currently lose_sum: 365.0513402223587
time_elpased: 1.784
batch start
#iterations: 297
currently lose_sum: 365.08980709314346
time_elpased: 1.784
batch start
#iterations: 298
currently lose_sum: 365.37077045440674
time_elpased: 1.788
batch start
#iterations: 299
currently lose_sum: 365.8849149942398
time_elpased: 1.786
start validation test
0.778298969072
0.777280585783
0.783575968833
0.780415581763
0
validation finish
batch start
#iterations: 300
currently lose_sum: 365.9575255513191
time_elpased: 1.775
batch start
#iterations: 301
currently lose_sum: 364.3114750981331
time_elpased: 1.78
batch start
#iterations: 302
currently lose_sum: 364.2095022201538
time_elpased: 1.774
batch start
#iterations: 303
currently lose_sum: 363.9625519514084
time_elpased: 1.786
batch start
#iterations: 304
currently lose_sum: 364.51890420913696
time_elpased: 1.771
batch start
#iterations: 305
currently lose_sum: 365.35295075178146
time_elpased: 1.774
batch start
#iterations: 306
currently lose_sum: 364.6071769595146
time_elpased: 1.783
batch start
#iterations: 307
currently lose_sum: 365.3198879957199
time_elpased: 1.789
batch start
#iterations: 308
currently lose_sum: 366.1100906729698
time_elpased: 1.784
batch start
#iterations: 309
currently lose_sum: 364.54289853572845
time_elpased: 1.785
batch start
#iterations: 310
currently lose_sum: 365.82362097501755
time_elpased: 1.783
batch start
#iterations: 311
currently lose_sum: 362.8189646601677
time_elpased: 1.778
batch start
#iterations: 312
currently lose_sum: 364.2900386452675
time_elpased: 1.797
batch start
#iterations: 313
currently lose_sum: 365.4956399202347
time_elpased: 1.777
batch start
#iterations: 314
currently lose_sum: 362.77625036239624
time_elpased: 1.784
batch start
#iterations: 315
currently lose_sum: 362.34238147735596
time_elpased: 1.755
batch start
#iterations: 316
currently lose_sum: 365.5720325708389
time_elpased: 1.777
batch start
#iterations: 317
currently lose_sum: 363.28614085912704
time_elpased: 1.768
batch start
#iterations: 318
currently lose_sum: 363.2615832686424
time_elpased: 1.776
batch start
#iterations: 319
currently lose_sum: 364.75030237436295
time_elpased: 1.777
start validation test
0.776443298969
0.786704774002
0.761943817921
0.774126347586
0
validation finish
batch start
#iterations: 320
currently lose_sum: 362.7079048752785
time_elpased: 1.781
batch start
#iterations: 321
currently lose_sum: 364.6501797437668
time_elpased: 1.77
batch start
#iterations: 322
currently lose_sum: 365.8551427721977
time_elpased: 1.774
batch start
#iterations: 323
currently lose_sum: 362.8617154955864
time_elpased: 1.797
batch start
#iterations: 324
currently lose_sum: 362.94569754600525
time_elpased: 1.783
batch start
#iterations: 325
currently lose_sum: 363.6031056046486
time_elpased: 1.786
batch start
#iterations: 326
currently lose_sum: 364.56105703115463
time_elpased: 1.78
batch start
#iterations: 327
currently lose_sum: 364.48927468061447
time_elpased: 1.779
batch start
#iterations: 328
currently lose_sum: 363.7459067106247
time_elpased: 1.784
batch start
#iterations: 329
currently lose_sum: 363.9028971195221
time_elpased: 1.792
batch start
#iterations: 330
currently lose_sum: 363.0022156238556
time_elpased: 1.785
batch start
#iterations: 331
currently lose_sum: 363.67419987916946
time_elpased: 1.77
batch start
#iterations: 332
currently lose_sum: 364.55153346061707
time_elpased: 1.782
batch start
#iterations: 333
currently lose_sum: 364.4847059249878
time_elpased: 1.777
batch start
#iterations: 334
currently lose_sum: 362.8720718026161
time_elpased: 1.784
batch start
#iterations: 335
currently lose_sum: 362.7659990787506
time_elpased: 1.774
batch start
#iterations: 336
currently lose_sum: 363.1386893391609
time_elpased: 1.781
batch start
#iterations: 337
currently lose_sum: 363.7443718314171
time_elpased: 1.774
batch start
#iterations: 338
currently lose_sum: 365.4821484684944
time_elpased: 1.792
batch start
#iterations: 339
currently lose_sum: 365.66346299648285
time_elpased: 1.786
start validation test
0.773762886598
0.790282436966
0.748718474472
0.768939194525
0
validation finish
batch start
#iterations: 340
currently lose_sum: 365.2535206079483
time_elpased: 1.783
batch start
#iterations: 341
currently lose_sum: 362.7075642347336
time_elpased: 1.783
batch start
#iterations: 342
currently lose_sum: 361.7129928469658
time_elpased: 1.773
batch start
#iterations: 343
currently lose_sum: 364.73386454582214
time_elpased: 1.786
batch start
#iterations: 344
currently lose_sum: 364.8545804619789
time_elpased: 1.767
batch start
#iterations: 345
currently lose_sum: 364.1850576996803
time_elpased: 1.791
batch start
#iterations: 346
currently lose_sum: 363.59797954559326
time_elpased: 1.768
batch start
#iterations: 347
currently lose_sum: 363.3354054093361
time_elpased: 1.782
batch start
#iterations: 348
currently lose_sum: 364.9222283959389
time_elpased: 1.783
batch start
#iterations: 349
currently lose_sum: 364.07023084163666
time_elpased: 1.774
batch start
#iterations: 350
currently lose_sum: 363.01197278499603
time_elpased: 1.781
batch start
#iterations: 351
currently lose_sum: 364.1826576590538
time_elpased: 1.787
batch start
#iterations: 352
currently lose_sum: 363.8620772957802
time_elpased: 1.785
batch start
#iterations: 353
currently lose_sum: 363.3769391775131
time_elpased: 1.772
batch start
#iterations: 354
currently lose_sum: 362.5868978500366
time_elpased: 1.776
batch start
#iterations: 355
currently lose_sum: 362.75304704904556
time_elpased: 1.773
batch start
#iterations: 356
currently lose_sum: 365.1365495920181
time_elpased: 1.78
batch start
#iterations: 357
currently lose_sum: 363.41903203725815
time_elpased: 1.769
batch start
#iterations: 358
currently lose_sum: 362.9107683300972
time_elpased: 1.777
batch start
#iterations: 359
currently lose_sum: 364.6290212273598
time_elpased: 1.775
start validation test
0.779896907216
0.775688719083
0.790957555875
0.783248730964
0
validation finish
batch start
#iterations: 360
currently lose_sum: 362.22580152750015
time_elpased: 1.778
batch start
#iterations: 361
currently lose_sum: 363.61729538440704
time_elpased: 1.766
batch start
#iterations: 362
currently lose_sum: 363.9032200574875
time_elpased: 1.765
batch start
#iterations: 363
currently lose_sum: 363.6512062549591
time_elpased: 1.78
batch start
#iterations: 364
currently lose_sum: 363.7466658949852
time_elpased: 1.763
batch start
#iterations: 365
currently lose_sum: 362.16449296474457
time_elpased: 1.774
batch start
#iterations: 366
currently lose_sum: 363.9976641535759
time_elpased: 1.77
batch start
#iterations: 367
currently lose_sum: 363.61139792203903
time_elpased: 1.789
batch start
#iterations: 368
currently lose_sum: 364.48714888095856
time_elpased: 1.783
batch start
#iterations: 369
currently lose_sum: 364.19598466157913
time_elpased: 1.775
batch start
#iterations: 370
currently lose_sum: 364.21540755033493
time_elpased: 1.78
batch start
#iterations: 371
currently lose_sum: 363.37423956394196
time_elpased: 1.774
batch start
#iterations: 372
currently lose_sum: 362.47606033086777
time_elpased: 1.79
batch start
#iterations: 373
currently lose_sum: 363.1443749666214
time_elpased: 1.78
batch start
#iterations: 374
currently lose_sum: 364.50474858283997
time_elpased: 1.79
batch start
#iterations: 375
currently lose_sum: 362.6763174533844
time_elpased: 1.783
batch start
#iterations: 376
currently lose_sum: 363.84891152381897
time_elpased: 1.785
batch start
#iterations: 377
currently lose_sum: 363.13128638267517
time_elpased: 1.777
batch start
#iterations: 378
currently lose_sum: 361.85555613040924
time_elpased: 1.774
batch start
#iterations: 379
currently lose_sum: 363.2264440059662
time_elpased: 1.779
start validation test
0.778969072165
0.770433405897
0.798236620873
0.784088620342
0
validation finish
batch start
#iterations: 380
currently lose_sum: 363.7642247080803
time_elpased: 1.767
batch start
#iterations: 381
currently lose_sum: 362.7347710132599
time_elpased: 1.782
batch start
#iterations: 382
currently lose_sum: 362.5660591721535
time_elpased: 1.765
batch start
#iterations: 383
currently lose_sum: 363.19465726614
time_elpased: 1.779
batch start
#iterations: 384
currently lose_sum: 363.27683532238007
time_elpased: 1.762
batch start
#iterations: 385
currently lose_sum: 361.76708233356476
time_elpased: 1.784
batch start
#iterations: 386
currently lose_sum: 362.5628322958946
time_elpased: 1.79
batch start
#iterations: 387
currently lose_sum: 363.3350858092308
time_elpased: 1.783
batch start
#iterations: 388
currently lose_sum: 362.97859275341034
time_elpased: 1.783
batch start
#iterations: 389
currently lose_sum: 363.370802462101
time_elpased: 1.777
batch start
#iterations: 390
currently lose_sum: 363.47378420829773
time_elpased: 1.782
batch start
#iterations: 391
currently lose_sum: 363.4028188586235
time_elpased: 1.772
batch start
#iterations: 392
currently lose_sum: 362.80234122276306
time_elpased: 1.786
batch start
#iterations: 393
currently lose_sum: 361.41952925920486
time_elpased: 1.784
batch start
#iterations: 394
currently lose_sum: 362.6486352086067
time_elpased: 1.82
batch start
#iterations: 395
currently lose_sum: 362.38406920433044
time_elpased: 1.784
batch start
#iterations: 396
currently lose_sum: 362.49794298410416
time_elpased: 1.785
batch start
#iterations: 397
currently lose_sum: 362.97823053598404
time_elpased: 1.78
batch start
#iterations: 398
currently lose_sum: 362.787501513958
time_elpased: 1.758
batch start
#iterations: 399
currently lose_sum: 362.38150757551193
time_elpased: 1.765
start validation test
0.779484536082
0.765876869295
0.80859134714
0.786654697786
0
validation finish
acc: 0.779
pre: 0.768
rec: 0.804
F1: 0.786
auc: 0.000
