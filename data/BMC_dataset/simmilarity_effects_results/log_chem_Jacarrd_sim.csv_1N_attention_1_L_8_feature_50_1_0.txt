start to construct graph
graph construct over
58300
epochs start
batch start
#iterations: 0
currently lose_sum: 405.48076218366623
time_elpased: 1.698
batch start
#iterations: 1
currently lose_sum: 403.5479158759117
time_elpased: 1.677
batch start
#iterations: 2
currently lose_sum: 402.8140882253647
time_elpased: 1.69
batch start
#iterations: 3
currently lose_sum: 402.14569783210754
time_elpased: 1.68
batch start
#iterations: 4
currently lose_sum: 401.9949031472206
time_elpased: 1.673
batch start
#iterations: 5
currently lose_sum: 401.8175703883171
time_elpased: 1.696
batch start
#iterations: 6
currently lose_sum: 401.0289527773857
time_elpased: 1.675
batch start
#iterations: 7
currently lose_sum: 400.5852028131485
time_elpased: 1.686
batch start
#iterations: 8
currently lose_sum: 399.96935510635376
time_elpased: 1.67
batch start
#iterations: 9
currently lose_sum: 399.005739569664
time_elpased: 1.679
batch start
#iterations: 10
currently lose_sum: 397.2630608677864
time_elpased: 1.669
batch start
#iterations: 11
currently lose_sum: 396.63473999500275
time_elpased: 1.656
batch start
#iterations: 12
currently lose_sum: 395.63226413726807
time_elpased: 1.689
batch start
#iterations: 13
currently lose_sum: 394.33269476890564
time_elpased: 1.685
batch start
#iterations: 14
currently lose_sum: 392.9574548602104
time_elpased: 1.674
batch start
#iterations: 15
currently lose_sum: 391.9045749902725
time_elpased: 1.666
batch start
#iterations: 16
currently lose_sum: 391.6946634054184
time_elpased: 1.669
batch start
#iterations: 17
currently lose_sum: 391.50715684890747
time_elpased: 1.698
batch start
#iterations: 18
currently lose_sum: 389.5573336482048
time_elpased: 1.678
batch start
#iterations: 19
currently lose_sum: 389.1270872950554
time_elpased: 1.693
start validation test
0.609845360825
0.808277541083
0.276810838979
0.412390342365
0
validation finish
batch start
#iterations: 20
currently lose_sum: 388.1019766330719
time_elpased: 1.659
batch start
#iterations: 21
currently lose_sum: 388.2907303571701
time_elpased: 1.695
batch start
#iterations: 22
currently lose_sum: 387.9003212451935
time_elpased: 1.695
batch start
#iterations: 23
currently lose_sum: 387.81978619098663
time_elpased: 1.689
batch start
#iterations: 24
currently lose_sum: 385.87872236967087
time_elpased: 1.675
batch start
#iterations: 25
currently lose_sum: 386.2921541929245
time_elpased: 1.69
batch start
#iterations: 26
currently lose_sum: 385.4833396077156
time_elpased: 1.698
batch start
#iterations: 27
currently lose_sum: 384.61285346746445
time_elpased: 1.678
batch start
#iterations: 28
currently lose_sum: 384.8310840725899
time_elpased: 1.689
batch start
#iterations: 29
currently lose_sum: 384.52012062072754
time_elpased: 1.685
batch start
#iterations: 30
currently lose_sum: 384.7039240002632
time_elpased: 1.682
batch start
#iterations: 31
currently lose_sum: 383.36846709251404
time_elpased: 1.69
batch start
#iterations: 32
currently lose_sum: 382.25880539417267
time_elpased: 1.683
batch start
#iterations: 33
currently lose_sum: 382.5209150314331
time_elpased: 1.704
batch start
#iterations: 34
currently lose_sum: 381.19341003894806
time_elpased: 1.688
batch start
#iterations: 35
currently lose_sum: 381.3759813308716
time_elpased: 1.693
batch start
#iterations: 36
currently lose_sum: 383.63519448041916
time_elpased: 1.681
batch start
#iterations: 37
currently lose_sum: 380.797507584095
time_elpased: 1.683
batch start
#iterations: 38
currently lose_sum: 382.6493777036667
time_elpased: 1.701
batch start
#iterations: 39
currently lose_sum: 380.78306341171265
time_elpased: 1.69
start validation test
0.719278350515
0.771709233792
0.614069828035
0.683923389437
0
validation finish
batch start
#iterations: 40
currently lose_sum: 379.6934902071953
time_elpased: 1.695
batch start
#iterations: 41
currently lose_sum: 380.27387046813965
time_elpased: 1.67
batch start
#iterations: 42
currently lose_sum: 379.5598633289337
time_elpased: 1.692
batch start
#iterations: 43
currently lose_sum: 379.9573780298233
time_elpased: 1.693
batch start
#iterations: 44
currently lose_sum: 380.73546463251114
time_elpased: 1.661
batch start
#iterations: 45
currently lose_sum: 379.53309631347656
time_elpased: 1.699
batch start
#iterations: 46
currently lose_sum: 377.93544703722
time_elpased: 1.678
batch start
#iterations: 47
currently lose_sum: 379.21326768398285
time_elpased: 1.692
batch start
#iterations: 48
currently lose_sum: 378.2527297139168
time_elpased: 1.666
batch start
#iterations: 49
currently lose_sum: 378.8310525417328
time_elpased: 1.68
batch start
#iterations: 50
currently lose_sum: 378.8065955042839
time_elpased: 1.678
batch start
#iterations: 51
currently lose_sum: 377.6394723057747
time_elpased: 1.662
batch start
#iterations: 52
currently lose_sum: 376.9739525914192
time_elpased: 1.7
batch start
#iterations: 53
currently lose_sum: 378.0523122549057
time_elpased: 1.669
batch start
#iterations: 54
currently lose_sum: 377.39575111866
time_elpased: 1.69
batch start
#iterations: 55
currently lose_sum: 377.96218317747116
time_elpased: 1.676
batch start
#iterations: 56
currently lose_sum: 376.94146740436554
time_elpased: 1.656
batch start
#iterations: 57
currently lose_sum: 377.96759206056595
time_elpased: 1.685
batch start
#iterations: 58
currently lose_sum: 379.00131607055664
time_elpased: 1.654
batch start
#iterations: 59
currently lose_sum: 378.0336747765541
time_elpased: 1.685
start validation test
0.74793814433
0.770994125101
0.697550807712
0.732435981615
0
validation finish
batch start
#iterations: 60
currently lose_sum: 378.4557807445526
time_elpased: 1.655
batch start
#iterations: 61
currently lose_sum: 375.906165599823
time_elpased: 1.682
batch start
#iterations: 62
currently lose_sum: 376.4537083506584
time_elpased: 1.661
batch start
#iterations: 63
currently lose_sum: 377.21205681562424
time_elpased: 1.66
batch start
#iterations: 64
currently lose_sum: 376.1190091371536
time_elpased: 1.68
batch start
#iterations: 65
currently lose_sum: 375.34433591365814
time_elpased: 1.674
batch start
#iterations: 66
currently lose_sum: 377.7562891840935
time_elpased: 1.692
batch start
#iterations: 67
currently lose_sum: 376.476728618145
time_elpased: 1.656
batch start
#iterations: 68
currently lose_sum: 375.6212087869644
time_elpased: 1.67
batch start
#iterations: 69
currently lose_sum: 375.5786452293396
time_elpased: 1.674
batch start
#iterations: 70
currently lose_sum: 374.65183794498444
time_elpased: 1.671
batch start
#iterations: 71
currently lose_sum: 375.75941079854965
time_elpased: 1.69
batch start
#iterations: 72
currently lose_sum: 375.61225432157516
time_elpased: 1.68
batch start
#iterations: 73
currently lose_sum: 375.85022634267807
time_elpased: 1.685
batch start
#iterations: 74
currently lose_sum: 376.10282558202744
time_elpased: 1.677
batch start
#iterations: 75
currently lose_sum: 375.1864361166954
time_elpased: 1.677
batch start
#iterations: 76
currently lose_sum: 374.68443262577057
time_elpased: 1.667
batch start
#iterations: 77
currently lose_sum: 374.80482882261276
time_elpased: 1.659
batch start
#iterations: 78
currently lose_sum: 374.5849587917328
time_elpased: 1.692
batch start
#iterations: 79
currently lose_sum: 375.1151297092438
time_elpased: 1.658
start validation test
0.755103092784
0.73537414966
0.788639916623
0.761076188081
0
validation finish
batch start
#iterations: 80
currently lose_sum: 374.4908550977707
time_elpased: 1.681
batch start
#iterations: 81
currently lose_sum: 376.02505069971085
time_elpased: 1.65
batch start
#iterations: 82
currently lose_sum: 374.8755912780762
time_elpased: 1.679
batch start
#iterations: 83
currently lose_sum: 373.5471039414406
time_elpased: 1.665
batch start
#iterations: 84
currently lose_sum: 374.0151002407074
time_elpased: 1.667
batch start
#iterations: 85
currently lose_sum: 373.94663363695145
time_elpased: 1.693
batch start
#iterations: 86
currently lose_sum: 373.65586483478546
time_elpased: 1.659
batch start
#iterations: 87
currently lose_sum: 372.97652792930603
time_elpased: 1.685
batch start
#iterations: 88
currently lose_sum: 373.8493849635124
time_elpased: 1.68
batch start
#iterations: 89
currently lose_sum: 372.66500103473663
time_elpased: 1.667
batch start
#iterations: 90
currently lose_sum: 373.718679189682
time_elpased: 1.682
batch start
#iterations: 91
currently lose_sum: 373.5462266802788
time_elpased: 1.665
batch start
#iterations: 92
currently lose_sum: 372.95475590229034
time_elpased: 1.687
batch start
#iterations: 93
currently lose_sum: 374.62909960746765
time_elpased: 1.669
batch start
#iterations: 94
currently lose_sum: 373.56892824172974
time_elpased: 1.675
batch start
#iterations: 95
currently lose_sum: 372.853939473629
time_elpased: 1.676
batch start
#iterations: 96
currently lose_sum: 372.089914560318
time_elpased: 1.656
batch start
#iterations: 97
currently lose_sum: 373.0746839046478
time_elpased: 1.693
batch start
#iterations: 98
currently lose_sum: 372.87690353393555
time_elpased: 1.682
batch start
#iterations: 99
currently lose_sum: 373.0384830236435
time_elpased: 1.696
start validation test
0.765824742268
0.786915038619
0.722042730589
0.753084406761
0
validation finish
batch start
#iterations: 100
currently lose_sum: 373.3202553987503
time_elpased: 1.667
batch start
#iterations: 101
currently lose_sum: 372.1165239214897
time_elpased: 1.692
batch start
#iterations: 102
currently lose_sum: 371.75544434785843
time_elpased: 1.672
batch start
#iterations: 103
currently lose_sum: 372.104227244854
time_elpased: 1.679
batch start
#iterations: 104
currently lose_sum: 372.41851592063904
time_elpased: 1.69
batch start
#iterations: 105
currently lose_sum: 372.6110448241234
time_elpased: 1.665
batch start
#iterations: 106
currently lose_sum: 374.716767847538
time_elpased: 1.68
batch start
#iterations: 107
currently lose_sum: 371.6935449242592
time_elpased: 1.671
batch start
#iterations: 108
currently lose_sum: 372.3519585132599
time_elpased: 1.683
batch start
#iterations: 109
currently lose_sum: 372.2537395954132
time_elpased: 1.684
batch start
#iterations: 110
currently lose_sum: 370.9732596874237
time_elpased: 1.685
batch start
#iterations: 111
currently lose_sum: 370.8978428244591
time_elpased: 1.698
batch start
#iterations: 112
currently lose_sum: 372.3885780572891
time_elpased: 1.676
batch start
#iterations: 113
currently lose_sum: 373.0735067129135
time_elpased: 1.688
batch start
#iterations: 114
currently lose_sum: 373.1488550901413
time_elpased: 1.667
batch start
#iterations: 115
currently lose_sum: 373.0612204670906
time_elpased: 1.674
batch start
#iterations: 116
currently lose_sum: 372.25618201494217
time_elpased: 1.69
batch start
#iterations: 117
currently lose_sum: 370.9682111144066
time_elpased: 1.666
batch start
#iterations: 118
currently lose_sum: 371.6972681879997
time_elpased: 1.688
batch start
#iterations: 119
currently lose_sum: 372.7413075566292
time_elpased: 1.669
start validation test
0.768608247423
0.777379400261
0.745700885878
0.761210702697
0
validation finish
batch start
#iterations: 120
currently lose_sum: 370.88561540842056
time_elpased: 1.68
batch start
#iterations: 121
currently lose_sum: 372.685419857502
time_elpased: 1.673
batch start
#iterations: 122
currently lose_sum: 371.0437135696411
time_elpased: 1.669
batch start
#iterations: 123
currently lose_sum: 371.0629794597626
time_elpased: 1.68
batch start
#iterations: 124
currently lose_sum: 370.97934126853943
time_elpased: 1.667
batch start
#iterations: 125
currently lose_sum: 371.3882561326027
time_elpased: 1.696
batch start
#iterations: 126
currently lose_sum: 370.8001534342766
time_elpased: 1.662
batch start
#iterations: 127
currently lose_sum: 371.70577627420425
time_elpased: 1.688
batch start
#iterations: 128
currently lose_sum: 371.94673413038254
time_elpased: 1.676
batch start
#iterations: 129
currently lose_sum: 369.5443776845932
time_elpased: 1.671
batch start
#iterations: 130
currently lose_sum: 371.9001267552376
time_elpased: 1.681
batch start
#iterations: 131
currently lose_sum: 370.2361662387848
time_elpased: 1.672
batch start
#iterations: 132
currently lose_sum: 372.47323471307755
time_elpased: 1.691
batch start
#iterations: 133
currently lose_sum: 369.754225730896
time_elpased: 1.671
batch start
#iterations: 134
currently lose_sum: 370.9766582250595
time_elpased: 1.677
batch start
#iterations: 135
currently lose_sum: 371.45671516656876
time_elpased: 1.675
batch start
#iterations: 136
currently lose_sum: 371.80045652389526
time_elpased: 1.666
batch start
#iterations: 137
currently lose_sum: 369.22932225465775
time_elpased: 1.693
batch start
#iterations: 138
currently lose_sum: 371.4771864414215
time_elpased: 1.679
batch start
#iterations: 139
currently lose_sum: 369.6416031718254
time_elpased: 1.682
start validation test
0.773865979381
0.764097363083
0.785200625326
0.774505268568
0
validation finish
batch start
#iterations: 140
currently lose_sum: 371.48128443956375
time_elpased: 1.681
batch start
#iterations: 141
currently lose_sum: 371.18614023923874
time_elpased: 1.68
batch start
#iterations: 142
currently lose_sum: 370.30943435430527
time_elpased: 1.679
batch start
#iterations: 143
currently lose_sum: 370.35440295934677
time_elpased: 1.673
batch start
#iterations: 144
currently lose_sum: 369.07198590040207
time_elpased: 1.673
batch start
#iterations: 145
currently lose_sum: 370.0105569958687
time_elpased: 1.674
batch start
#iterations: 146
currently lose_sum: 369.76224291324615
time_elpased: 1.684
batch start
#iterations: 147
currently lose_sum: 370.91307801008224
time_elpased: 1.676
batch start
#iterations: 148
currently lose_sum: 369.4862970113754
time_elpased: 1.672
batch start
#iterations: 149
currently lose_sum: 368.58296328783035
time_elpased: 1.676
batch start
#iterations: 150
currently lose_sum: 370.11000937223434
time_elpased: 1.658
batch start
#iterations: 151
currently lose_sum: 368.7401784658432
time_elpased: 1.693
batch start
#iterations: 152
currently lose_sum: 370.6742659807205
time_elpased: 1.664
batch start
#iterations: 153
currently lose_sum: 370.52095127105713
time_elpased: 1.673
batch start
#iterations: 154
currently lose_sum: 369.36361253261566
time_elpased: 1.68
batch start
#iterations: 155
currently lose_sum: 370.4491119980812
time_elpased: 1.671
batch start
#iterations: 156
currently lose_sum: 368.7974860072136
time_elpased: 1.682
batch start
#iterations: 157
currently lose_sum: 369.6624005436897
time_elpased: 1.682
batch start
#iterations: 158
currently lose_sum: 369.3735541701317
time_elpased: 1.693
batch start
#iterations: 159
currently lose_sum: 368.13569182157516
time_elpased: 1.666
start validation test
0.772268041237
0.77752760802
0.755810317874
0.76651516753
0
validation finish
batch start
#iterations: 160
currently lose_sum: 370.5125029683113
time_elpased: 1.691
batch start
#iterations: 161
currently lose_sum: 371.40758860111237
time_elpased: 1.667
batch start
#iterations: 162
currently lose_sum: 369.28140699863434
time_elpased: 1.686
batch start
#iterations: 163
currently lose_sum: 370.3848803639412
time_elpased: 1.684
batch start
#iterations: 164
currently lose_sum: 369.40803122520447
time_elpased: 1.659
batch start
#iterations: 165
currently lose_sum: 369.7527542114258
time_elpased: 1.69
batch start
#iterations: 166
currently lose_sum: 370.15759283304214
time_elpased: 1.672
batch start
#iterations: 167
currently lose_sum: 369.16908782720566
time_elpased: 1.676
batch start
#iterations: 168
currently lose_sum: 367.52493411302567
time_elpased: 1.672
batch start
#iterations: 169
currently lose_sum: 369.25358760356903
time_elpased: 1.688
batch start
#iterations: 170
currently lose_sum: 370.35947877168655
time_elpased: 1.696
batch start
#iterations: 171
currently lose_sum: 369.8940798640251
time_elpased: 1.67
batch start
#iterations: 172
currently lose_sum: 369.49048978090286
time_elpased: 1.683
batch start
#iterations: 173
currently lose_sum: 368.434489607811
time_elpased: 1.669
batch start
#iterations: 174
currently lose_sum: 369.00736033916473
time_elpased: 1.674
batch start
#iterations: 175
currently lose_sum: 370.3250764608383
time_elpased: 1.675
batch start
#iterations: 176
currently lose_sum: 368.7305793762207
time_elpased: 1.68
batch start
#iterations: 177
currently lose_sum: 368.89132928848267
time_elpased: 1.694
batch start
#iterations: 178
currently lose_sum: 369.98604649305344
time_elpased: 1.678
batch start
#iterations: 179
currently lose_sum: 369.88101148605347
time_elpased: 1.684
start validation test
0.769536082474
0.792132269099
0.724022928609
0.756547781105
0
validation finish
batch start
#iterations: 180
currently lose_sum: 369.2940219640732
time_elpased: 1.675
batch start
#iterations: 181
currently lose_sum: 367.4598568677902
time_elpased: 1.692
batch start
#iterations: 182
currently lose_sum: 369.42763245105743
time_elpased: 1.678
batch start
#iterations: 183
currently lose_sum: 369.3680154681206
time_elpased: 1.676
batch start
#iterations: 184
currently lose_sum: 368.5783079266548
time_elpased: 1.695
batch start
#iterations: 185
currently lose_sum: 369.36670142412186
time_elpased: 1.677
batch start
#iterations: 186
currently lose_sum: 368.8748924732208
time_elpased: 1.691
batch start
#iterations: 187
currently lose_sum: 368.1884015202522
time_elpased: 1.667
batch start
#iterations: 188
currently lose_sum: 368.4624319076538
time_elpased: 1.675
batch start
#iterations: 189
currently lose_sum: 369.32846385240555
time_elpased: 1.675
batch start
#iterations: 190
currently lose_sum: 369.09438037872314
time_elpased: 1.674
batch start
#iterations: 191
currently lose_sum: 368.8213104605675
time_elpased: 1.693
batch start
#iterations: 192
currently lose_sum: 370.0880196094513
time_elpased: 1.676
batch start
#iterations: 193
currently lose_sum: 368.5498711466789
time_elpased: 1.678
batch start
#iterations: 194
currently lose_sum: 368.605073928833
time_elpased: 1.669
batch start
#iterations: 195
currently lose_sum: 368.7265838980675
time_elpased: 1.677
batch start
#iterations: 196
currently lose_sum: 370.93506413698196
time_elpased: 1.699
batch start
#iterations: 197
currently lose_sum: 367.69668793678284
time_elpased: 1.681
batch start
#iterations: 198
currently lose_sum: 368.17881774902344
time_elpased: 1.693
batch start
#iterations: 199
currently lose_sum: 368.0361837744713
time_elpased: 1.681
start validation test
0.777164948454
0.777356902357
0.769984366858
0.773653070841
0
validation finish
batch start
#iterations: 200
currently lose_sum: 367.2383152246475
time_elpased: 1.678
batch start
#iterations: 201
currently lose_sum: 368.110868036747
time_elpased: 1.666
batch start
#iterations: 202
currently lose_sum: 367.8925330042839
time_elpased: 1.673
batch start
#iterations: 203
currently lose_sum: 367.4208959341049
time_elpased: 1.676
batch start
#iterations: 204
currently lose_sum: 367.857705116272
time_elpased: 1.671
batch start
#iterations: 205
currently lose_sum: 369.3802435994148
time_elpased: 1.683
batch start
#iterations: 206
currently lose_sum: 366.2125582098961
time_elpased: 1.684
batch start
#iterations: 207
currently lose_sum: 367.9685022830963
time_elpased: 1.689
batch start
#iterations: 208
currently lose_sum: 367.24111557006836
time_elpased: 1.688
batch start
#iterations: 209
currently lose_sum: 368.1391988992691
time_elpased: 1.676
batch start
#iterations: 210
currently lose_sum: 367.9758124947548
time_elpased: 1.695
batch start
#iterations: 211
currently lose_sum: 367.9130819439888
time_elpased: 1.679
batch start
#iterations: 212
currently lose_sum: 368.9931126832962
time_elpased: 1.677
batch start
#iterations: 213
currently lose_sum: 367.1816749572754
time_elpased: 1.672
batch start
#iterations: 214
currently lose_sum: 368.3778953552246
time_elpased: 1.684
batch start
#iterations: 215
currently lose_sum: 367.2398017048836
time_elpased: 1.681
batch start
#iterations: 216
currently lose_sum: 369.0670655965805
time_elpased: 1.677
batch start
#iterations: 217
currently lose_sum: 366.03623431921005
time_elpased: 1.694
batch start
#iterations: 218
currently lose_sum: 367.63368594646454
time_elpased: 1.671
batch start
#iterations: 219
currently lose_sum: 369.39113146066666
time_elpased: 1.685
start validation test
0.778505154639
0.780852417303
0.767587285044
0.774163031482
0
validation finish
batch start
#iterations: 220
currently lose_sum: 368.32020503282547
time_elpased: 1.659
batch start
#iterations: 221
currently lose_sum: 366.2878648042679
time_elpased: 1.681
batch start
#iterations: 222
currently lose_sum: 367.77105897665024
time_elpased: 1.674
batch start
#iterations: 223
currently lose_sum: 368.2010540366173
time_elpased: 1.664
batch start
#iterations: 224
currently lose_sum: 367.84848088026047
time_elpased: 1.684
batch start
#iterations: 225
currently lose_sum: 366.9098456501961
time_elpased: 1.671
batch start
#iterations: 226
currently lose_sum: 367.0588408112526
time_elpased: 1.692
batch start
#iterations: 227
currently lose_sum: 366.83386701345444
time_elpased: 1.67
batch start
#iterations: 228
currently lose_sum: 367.8925633430481
time_elpased: 1.674
batch start
#iterations: 229
currently lose_sum: 366.87731486558914
time_elpased: 1.686
batch start
#iterations: 230
currently lose_sum: 367.29733127355576
time_elpased: 1.666
batch start
#iterations: 231
currently lose_sum: 367.16428542137146
time_elpased: 1.706
batch start
#iterations: 232
currently lose_sum: 367.3064863681793
time_elpased: 1.675
batch start
#iterations: 233
currently lose_sum: 367.59522342681885
time_elpased: 1.678
batch start
#iterations: 234
currently lose_sum: 367.6258278489113
time_elpased: 1.686
batch start
#iterations: 235
currently lose_sum: 367.3988002538681
time_elpased: 1.677
batch start
#iterations: 236
currently lose_sum: 367.0136566758156
time_elpased: 1.691
batch start
#iterations: 237
currently lose_sum: 367.84350222349167
time_elpased: 1.683
batch start
#iterations: 238
currently lose_sum: 367.04061967134476
time_elpased: 1.684
batch start
#iterations: 239
currently lose_sum: 366.8990088701248
time_elpased: 1.678
start validation test
0.780154639175
0.794931385569
0.748619072434
0.771080457302
0
validation finish
batch start
#iterations: 240
currently lose_sum: 367.4238116145134
time_elpased: 1.696
batch start
#iterations: 241
currently lose_sum: 367.7263757586479
time_elpased: 1.689
batch start
#iterations: 242
currently lose_sum: 367.83466255664825
time_elpased: 1.679
batch start
#iterations: 243
currently lose_sum: 367.9734220504761
time_elpased: 1.702
batch start
#iterations: 244
currently lose_sum: 367.0479338169098
time_elpased: 1.68
batch start
#iterations: 245
currently lose_sum: 367.57836949825287
time_elpased: 1.699
batch start
#iterations: 246
currently lose_sum: 367.74767822027206
time_elpased: 1.676
batch start
#iterations: 247
currently lose_sum: 365.9148052930832
time_elpased: 1.704
batch start
#iterations: 248
currently lose_sum: 366.78285616636276
time_elpased: 1.692
batch start
#iterations: 249
currently lose_sum: 366.2126524448395
time_elpased: 1.67
batch start
#iterations: 250
currently lose_sum: 367.15839809179306
time_elpased: 1.693
batch start
#iterations: 251
currently lose_sum: 367.1022028326988
time_elpased: 1.684
batch start
#iterations: 252
currently lose_sum: 366.5542243719101
time_elpased: 1.702
batch start
#iterations: 253
currently lose_sum: 366.60946959257126
time_elpased: 1.686
batch start
#iterations: 254
currently lose_sum: 365.672547519207
time_elpased: 1.694
batch start
#iterations: 255
currently lose_sum: 366.3742998242378
time_elpased: 1.687
batch start
#iterations: 256
currently lose_sum: 365.58940130472183
time_elpased: 1.667
batch start
#iterations: 257
currently lose_sum: 365.9142084121704
time_elpased: 1.706
batch start
#iterations: 258
currently lose_sum: 365.8812372684479
time_elpased: 1.686
batch start
#iterations: 259
currently lose_sum: 365.7946172952652
time_elpased: 1.7
start validation test
0.776546391753
0.802716390424
0.726836894216
0.762894492151
0
validation finish
batch start
#iterations: 260
currently lose_sum: 366.4902783036232
time_elpased: 1.692
batch start
#iterations: 261
currently lose_sum: 365.7713760137558
time_elpased: 1.695
batch start
#iterations: 262
currently lose_sum: 365.7607504725456
time_elpased: 1.697
batch start
#iterations: 263
currently lose_sum: 365.78871661424637
time_elpased: 1.694
batch start
#iterations: 264
currently lose_sum: 367.20886731147766
time_elpased: 1.698
batch start
#iterations: 265
currently lose_sum: 366.13122141361237
time_elpased: 1.694
batch start
#iterations: 266
currently lose_sum: 366.00084632635117
time_elpased: 1.689
batch start
#iterations: 267
currently lose_sum: 365.8636418581009
time_elpased: 1.688
batch start
#iterations: 268
currently lose_sum: 366.44596868753433
time_elpased: 1.693
batch start
#iterations: 269
currently lose_sum: 367.0558212399483
time_elpased: 1.702
batch start
#iterations: 270
currently lose_sum: 366.43273025751114
time_elpased: 1.673
batch start
#iterations: 271
currently lose_sum: 365.4002442955971
time_elpased: 1.682
batch start
#iterations: 272
currently lose_sum: 366.00960516929626
time_elpased: 1.684
batch start
#iterations: 273
currently lose_sum: 366.1132054924965
time_elpased: 1.687
batch start
#iterations: 274
currently lose_sum: 366.7140225172043
time_elpased: 1.689
batch start
#iterations: 275
currently lose_sum: 367.2260491847992
time_elpased: 1.693
batch start
#iterations: 276
currently lose_sum: 367.15048384666443
time_elpased: 1.689
batch start
#iterations: 277
currently lose_sum: 365.739237844944
time_elpased: 1.678
batch start
#iterations: 278
currently lose_sum: 367.12668681144714
time_elpased: 1.698
batch start
#iterations: 279
currently lose_sum: 365.34732657670975
time_elpased: 1.651
start validation test
0.784484536082
0.785367910605
0.776446065659
0.780881505162
0
validation finish
batch start
#iterations: 280
currently lose_sum: 366.86934030056
time_elpased: 1.672
batch start
#iterations: 281
currently lose_sum: 365.62041944265366
time_elpased: 1.651
batch start
#iterations: 282
currently lose_sum: 366.3314656615257
time_elpased: 1.652
batch start
#iterations: 283
currently lose_sum: 366.6390198469162
time_elpased: 1.649
batch start
#iterations: 284
currently lose_sum: 367.0585322380066
time_elpased: 1.659
batch start
#iterations: 285
currently lose_sum: 365.8059474825859
time_elpased: 1.699
batch start
#iterations: 286
currently lose_sum: 366.25789016485214
time_elpased: 1.651
batch start
#iterations: 287
currently lose_sum: 366.9887281656265
time_elpased: 1.69
batch start
#iterations: 288
currently lose_sum: 365.94711273908615
time_elpased: 1.686
batch start
#iterations: 289
currently lose_sum: 364.31901901960373
time_elpased: 1.679
batch start
#iterations: 290
currently lose_sum: 365.1245741844177
time_elpased: 1.699
batch start
#iterations: 291
currently lose_sum: 365.5253321528435
time_elpased: 1.683
batch start
#iterations: 292
currently lose_sum: 366.3869176506996
time_elpased: 1.693
batch start
#iterations: 293
currently lose_sum: 366.207157433033
time_elpased: 1.675
batch start
#iterations: 294
currently lose_sum: 364.309326171875
time_elpased: 1.659
batch start
#iterations: 295
currently lose_sum: 367.46728056669235
time_elpased: 1.66
batch start
#iterations: 296
currently lose_sum: 364.9295040369034
time_elpased: 1.67
batch start
#iterations: 297
currently lose_sum: 366.50586354732513
time_elpased: 1.678
batch start
#iterations: 298
currently lose_sum: 365.3656311035156
time_elpased: 1.672
batch start
#iterations: 299
currently lose_sum: 365.46712470054626
time_elpased: 1.697
start validation test
0.782371134021
0.79616359828
0.752683689422
0.773813350477
0
validation finish
batch start
#iterations: 300
currently lose_sum: 365.1839072108269
time_elpased: 1.672
batch start
#iterations: 301
currently lose_sum: 365.86108034849167
time_elpased: 1.702
batch start
#iterations: 302
currently lose_sum: 365.05192786455154
time_elpased: 1.685
batch start
#iterations: 303
currently lose_sum: 366.5044075846672
time_elpased: 1.69
batch start
#iterations: 304
currently lose_sum: 366.219544172287
time_elpased: 1.687
batch start
#iterations: 305
currently lose_sum: 365.60640501976013
time_elpased: 1.685
batch start
#iterations: 306
currently lose_sum: 364.68881618976593
time_elpased: 1.694
batch start
#iterations: 307
currently lose_sum: 364.7463331222534
time_elpased: 1.67
batch start
#iterations: 308
currently lose_sum: 365.47803860902786
time_elpased: 1.682
batch start
#iterations: 309
currently lose_sum: 365.5328980088234
time_elpased: 1.69
batch start
#iterations: 310
currently lose_sum: 367.11406672000885
time_elpased: 1.678
batch start
#iterations: 311
currently lose_sum: 364.94098258018494
time_elpased: 1.708
batch start
#iterations: 312
currently lose_sum: 364.39838594198227
time_elpased: 1.666
batch start
#iterations: 313
currently lose_sum: 364.93673688173294
time_elpased: 1.699
batch start
#iterations: 314
currently lose_sum: 365.0348636507988
time_elpased: 1.698
batch start
#iterations: 315
currently lose_sum: 365.2839548587799
time_elpased: 1.7
batch start
#iterations: 316
currently lose_sum: 363.5875845551491
time_elpased: 1.704
batch start
#iterations: 317
currently lose_sum: 366.6319645643234
time_elpased: 1.696
batch start
#iterations: 318
currently lose_sum: 365.0887512564659
time_elpased: 1.723
batch start
#iterations: 319
currently lose_sum: 365.2098085284233
time_elpased: 1.699
start validation test
0.780051546392
0.793650793651
0.750390828557
0.771414796164
0
validation finish
batch start
#iterations: 320
currently lose_sum: 364.3766344189644
time_elpased: 1.717
batch start
#iterations: 321
currently lose_sum: 365.612321972847
time_elpased: 1.703
batch start
#iterations: 322
currently lose_sum: 365.21476179361343
time_elpased: 1.712
batch start
#iterations: 323
currently lose_sum: 365.4100539088249
time_elpased: 1.688
batch start
#iterations: 324
currently lose_sum: 363.9517421722412
time_elpased: 1.685
batch start
#iterations: 325
currently lose_sum: 365.95780116319656
time_elpased: 1.704
batch start
#iterations: 326
currently lose_sum: 365.7968186736107
time_elpased: 1.684
batch start
#iterations: 327
currently lose_sum: 365.57777684926987
time_elpased: 1.686
batch start
#iterations: 328
currently lose_sum: 366.1783113479614
time_elpased: 1.676
batch start
#iterations: 329
currently lose_sum: 364.9435335993767
time_elpased: 1.685
batch start
#iterations: 330
currently lose_sum: 366.0220381617546
time_elpased: 1.687
batch start
#iterations: 331
currently lose_sum: 365.6466415524483
time_elpased: 1.673
batch start
#iterations: 332
currently lose_sum: 365.8268061876297
time_elpased: 1.714
batch start
#iterations: 333
currently lose_sum: 365.0817195177078
time_elpased: 1.685
batch start
#iterations: 334
currently lose_sum: 364.4951258301735
time_elpased: 1.71
batch start
#iterations: 335
currently lose_sum: 363.76148492097855
time_elpased: 1.694
batch start
#iterations: 336
currently lose_sum: 363.87237560749054
time_elpased: 1.698
batch start
#iterations: 337
currently lose_sum: 364.6551139354706
time_elpased: 1.699
batch start
#iterations: 338
currently lose_sum: 365.0483075976372
time_elpased: 1.689
batch start
#iterations: 339
currently lose_sum: 365.08848893642426
time_elpased: 1.714
start validation test
0.783144329897
0.78215333054
0.778322042731
0.780232983336
0
validation finish
batch start
#iterations: 340
currently lose_sum: 366.4227612018585
time_elpased: 1.698
batch start
#iterations: 341
currently lose_sum: 365.4014031291008
time_elpased: 1.708
batch start
#iterations: 342
currently lose_sum: 366.57243633270264
time_elpased: 1.69
batch start
#iterations: 343
currently lose_sum: 363.9358752965927
time_elpased: 1.702
batch start
#iterations: 344
currently lose_sum: 366.0715260505676
time_elpased: 1.699
batch start
#iterations: 345
currently lose_sum: 365.4527324438095
time_elpased: 1.685
batch start
#iterations: 346
currently lose_sum: 363.70943409204483
time_elpased: 1.715
batch start
#iterations: 347
currently lose_sum: 363.76824176311493
time_elpased: 1.698
batch start
#iterations: 348
currently lose_sum: 362.96609246730804
time_elpased: 1.716
batch start
#iterations: 349
currently lose_sum: 363.85699927806854
time_elpased: 1.681
batch start
#iterations: 350
currently lose_sum: 365.68554681539536
time_elpased: 1.698
batch start
#iterations: 351
currently lose_sum: 365.00904816389084
time_elpased: 1.692
batch start
#iterations: 352
currently lose_sum: 363.81370079517365
time_elpased: 1.702
batch start
#iterations: 353
currently lose_sum: 365.29971075057983
time_elpased: 1.709
batch start
#iterations: 354
currently lose_sum: 363.90038418769836
time_elpased: 1.69
batch start
#iterations: 355
currently lose_sum: 366.9286673069
time_elpased: 1.706
batch start
#iterations: 356
currently lose_sum: 365.4905489683151
time_elpased: 1.685
batch start
#iterations: 357
currently lose_sum: 364.09394258260727
time_elpased: 1.707
batch start
#iterations: 358
currently lose_sum: 364.685894548893
time_elpased: 1.701
batch start
#iterations: 359
currently lose_sum: 366.84487795829773
time_elpased: 1.696
start validation test
0.782989690722
0.777777777778
0.785721730068
0.781729572791
0
validation finish
batch start
#iterations: 360
currently lose_sum: 365.07372093200684
time_elpased: 1.716
batch start
#iterations: 361
currently lose_sum: 365.75878459215164
time_elpased: 1.695
batch start
#iterations: 362
currently lose_sum: 365.21680557727814
time_elpased: 1.734
batch start
#iterations: 363
currently lose_sum: 365.7528091073036
time_elpased: 1.697
batch start
#iterations: 364
currently lose_sum: 364.8629981279373
time_elpased: 1.719
batch start
#iterations: 365
currently lose_sum: 364.3840106725693
time_elpased: 1.707
batch start
#iterations: 366
currently lose_sum: 364.9320898652077
time_elpased: 1.717
batch start
#iterations: 367
currently lose_sum: 364.75206059217453
time_elpased: 1.72
batch start
#iterations: 368
currently lose_sum: 363.67472743988037
time_elpased: 1.703
batch start
#iterations: 369
currently lose_sum: 362.59076154232025
time_elpased: 1.729
batch start
#iterations: 370
currently lose_sum: 363.3012892007828
time_elpased: 1.694
batch start
#iterations: 371
currently lose_sum: 363.70826905965805
time_elpased: 1.729
batch start
#iterations: 372
currently lose_sum: 363.3718120455742
time_elpased: 1.696
batch start
#iterations: 373
currently lose_sum: 364.44709265232086
time_elpased: 1.718
batch start
#iterations: 374
currently lose_sum: 366.08962619304657
time_elpased: 1.731
batch start
#iterations: 375
currently lose_sum: 364.0444040298462
time_elpased: 1.718
batch start
#iterations: 376
currently lose_sum: 364.0141657590866
time_elpased: 1.737
batch start
#iterations: 377
currently lose_sum: 365.36771354079247
time_elpased: 1.711
batch start
#iterations: 378
currently lose_sum: 364.9071995317936
time_elpased: 1.724
batch start
#iterations: 379
currently lose_sum: 365.54304015636444
time_elpased: 1.717
start validation test
0.784329896907
0.781441797566
0.782907764461
0.782174094127
0
validation finish
batch start
#iterations: 380
currently lose_sum: 365.03636103868484
time_elpased: 1.722
batch start
#iterations: 381
currently lose_sum: 364.18759685754776
time_elpased: 1.715
batch start
#iterations: 382
currently lose_sum: 365.2219004034996
time_elpased: 1.712
batch start
#iterations: 383
currently lose_sum: 364.2267141342163
time_elpased: 1.727
batch start
#iterations: 384
currently lose_sum: 362.8001652956009
time_elpased: 1.698
batch start
#iterations: 385
currently lose_sum: 364.78342443704605
time_elpased: 1.723
batch start
#iterations: 386
currently lose_sum: 363.6015413403511
time_elpased: 1.695
batch start
#iterations: 387
currently lose_sum: 363.79804158210754
time_elpased: 1.705
batch start
#iterations: 388
currently lose_sum: 363.65908658504486
time_elpased: 1.699
batch start
#iterations: 389
currently lose_sum: 365.4245781302452
time_elpased: 1.69
batch start
#iterations: 390
currently lose_sum: 363.06373339891434
time_elpased: 1.7
batch start
#iterations: 391
currently lose_sum: 364.0061735510826
time_elpased: 1.693
batch start
#iterations: 392
currently lose_sum: 364.07847660779953
time_elpased: 1.717
batch start
#iterations: 393
currently lose_sum: 362.82111644744873
time_elpased: 1.674
batch start
#iterations: 394
currently lose_sum: 364.0817302465439
time_elpased: 1.726
batch start
#iterations: 395
currently lose_sum: 363.6533011198044
time_elpased: 1.707
batch start
#iterations: 396
currently lose_sum: 364.7760270833969
time_elpased: 1.71
batch start
#iterations: 397
currently lose_sum: 365.7480853199959
time_elpased: 1.711
batch start
#iterations: 398
currently lose_sum: 364.78827953338623
time_elpased: 1.686
batch start
#iterations: 399
currently lose_sum: 362.6418470144272
time_elpased: 1.731
start validation test
0.783762886598
0.790260159106
0.766128191767
0.778007091073
0
validation finish
acc: 0.780
pre: 0.779
rec: 0.786
F1: 0.782
auc: 0.000
