start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 557.4211259484291
time_elpased: 1.627
batch start
#iterations: 1
currently lose_sum: 554.5452742576599
time_elpased: 1.601
batch start
#iterations: 2
currently lose_sum: 555.2129882574081
time_elpased: 1.602
batch start
#iterations: 3
currently lose_sum: 553.4320040345192
time_elpased: 1.598
batch start
#iterations: 4
currently lose_sum: 552.2295379638672
time_elpased: 1.583
batch start
#iterations: 5
currently lose_sum: 551.4680798053741
time_elpased: 1.584
batch start
#iterations: 6
currently lose_sum: 548.5196747779846
time_elpased: 1.611
batch start
#iterations: 7
currently lose_sum: 550.0866842269897
time_elpased: 1.599
batch start
#iterations: 8
currently lose_sum: 546.1596447825432
time_elpased: 1.599
batch start
#iterations: 9
currently lose_sum: 545.8399026989937
time_elpased: 1.597
batch start
#iterations: 10
currently lose_sum: 542.8049441277981
time_elpased: 1.592
batch start
#iterations: 11
currently lose_sum: 544.5752503275871
time_elpased: 1.605
batch start
#iterations: 12
currently lose_sum: 540.4691662788391
time_elpased: 1.586
batch start
#iterations: 13
currently lose_sum: 538.1587744951248
time_elpased: 1.593
batch start
#iterations: 14
currently lose_sum: 535.856391787529
time_elpased: 1.607
batch start
#iterations: 15
currently lose_sum: 535.5611519813538
time_elpased: 1.591
batch start
#iterations: 16
currently lose_sum: 535.3745462298393
time_elpased: 1.608
batch start
#iterations: 17
currently lose_sum: 533.817964553833
time_elpased: 1.595
batch start
#iterations: 18
currently lose_sum: 532.9511405229568
time_elpased: 1.597
batch start
#iterations: 19
currently lose_sum: 531.1035949289799
time_elpased: 1.616
start validation test
0.552010309278
0.895788722341
0.12806122449
0.224087135077
0
validation finish
batch start
#iterations: 20
currently lose_sum: 531.2729703187943
time_elpased: 1.611
batch start
#iterations: 21
currently lose_sum: 532.5263237059116
time_elpased: 1.606
batch start
#iterations: 22
currently lose_sum: 531.4548043012619
time_elpased: 1.603
batch start
#iterations: 23
currently lose_sum: 529.5697086155415
time_elpased: 1.601
batch start
#iterations: 24
currently lose_sum: 528.9557141661644
time_elpased: 1.604
batch start
#iterations: 25
currently lose_sum: 529.1779917180538
time_elpased: 1.591
batch start
#iterations: 26
currently lose_sum: 529.5893785059452
time_elpased: 1.596
batch start
#iterations: 27
currently lose_sum: 527.9707360267639
time_elpased: 1.592
batch start
#iterations: 28
currently lose_sum: 526.6331341564655
time_elpased: 1.59
batch start
#iterations: 29
currently lose_sum: 527.4024177193642
time_elpased: 1.593
batch start
#iterations: 30
currently lose_sum: 526.2378259003162
time_elpased: 1.595
batch start
#iterations: 31
currently lose_sum: 526.5570037961006
time_elpased: 1.605
batch start
#iterations: 32
currently lose_sum: 526.518073529005
time_elpased: 1.614
batch start
#iterations: 33
currently lose_sum: 524.236570417881
time_elpased: 1.599
batch start
#iterations: 34
currently lose_sum: 525.6846983730793
time_elpased: 1.6
batch start
#iterations: 35
currently lose_sum: 523.9144425392151
time_elpased: 1.591
batch start
#iterations: 36
currently lose_sum: 524.2759556770325
time_elpased: 1.6
batch start
#iterations: 37
currently lose_sum: 523.7414824664593
time_elpased: 1.596
batch start
#iterations: 38
currently lose_sum: 523.2068465054035
time_elpased: 1.622
batch start
#iterations: 39
currently lose_sum: 524.9774216413498
time_elpased: 1.607
start validation test
0.502319587629
0.944785276074
0.0157142857143
0.0309143832179
0
validation finish
batch start
#iterations: 40
currently lose_sum: 523.7896958589554
time_elpased: 1.596
batch start
#iterations: 41
currently lose_sum: 522.0488784015179
time_elpased: 1.61
batch start
#iterations: 42
currently lose_sum: 521.3962587416172
time_elpased: 1.582
batch start
#iterations: 43
currently lose_sum: 521.1817382276058
time_elpased: 1.593
batch start
#iterations: 44
currently lose_sum: 520.6853513419628
time_elpased: 1.593
batch start
#iterations: 45
currently lose_sum: 520.9392287433147
time_elpased: 1.628
batch start
#iterations: 46
currently lose_sum: 520.0653142035007
time_elpased: 1.596
batch start
#iterations: 47
currently lose_sum: 521.5229366719723
time_elpased: 1.59
batch start
#iterations: 48
currently lose_sum: 518.6363815367222
time_elpased: 1.59
batch start
#iterations: 49
currently lose_sum: 518.002496689558
time_elpased: 1.612
batch start
#iterations: 50
currently lose_sum: 520.5094261169434
time_elpased: 1.598
batch start
#iterations: 51
currently lose_sum: 520.7295950949192
time_elpased: 1.594
batch start
#iterations: 52
currently lose_sum: 519.7819884121418
time_elpased: 1.611
batch start
#iterations: 53
currently lose_sum: 519.2711916863918
time_elpased: 1.602
batch start
#iterations: 54
currently lose_sum: 518.0601215064526
time_elpased: 1.6
batch start
#iterations: 55
currently lose_sum: 519.4373923838139
time_elpased: 1.602
batch start
#iterations: 56
currently lose_sum: 519.4112554490566
time_elpased: 1.593
batch start
#iterations: 57
currently lose_sum: 518.051980048418
time_elpased: 1.589
batch start
#iterations: 58
currently lose_sum: 517.1219143271446
time_elpased: 1.6
batch start
#iterations: 59
currently lose_sum: 515.7378149330616
time_elpased: 1.587
start validation test
0.524432989691
0.900837988827
0.0658163265306
0.122670216812
0
validation finish
batch start
#iterations: 60
currently lose_sum: 517.7769632935524
time_elpased: 1.609
batch start
#iterations: 61
currently lose_sum: 516.3511570692062
time_elpased: 1.591
batch start
#iterations: 62
currently lose_sum: 518.1509240865707
time_elpased: 1.602
batch start
#iterations: 63
currently lose_sum: 517.2961482107639
time_elpased: 1.597
batch start
#iterations: 64
currently lose_sum: 516.9653077423573
time_elpased: 1.592
batch start
#iterations: 65
currently lose_sum: 517.1311776936054
time_elpased: 1.606
batch start
#iterations: 66
currently lose_sum: 514.9724846184254
time_elpased: 1.606
batch start
#iterations: 67
currently lose_sum: 515.5671718716621
time_elpased: 1.573
batch start
#iterations: 68
currently lose_sum: 517.9889396131039
time_elpased: 1.638
batch start
#iterations: 69
currently lose_sum: 516.5946223139763
time_elpased: 1.598
batch start
#iterations: 70
currently lose_sum: 514.7636743783951
time_elpased: 1.608
batch start
#iterations: 71
currently lose_sum: 514.7481807470322
time_elpased: 1.594
batch start
#iterations: 72
currently lose_sum: 517.450723618269
time_elpased: 1.604
batch start
#iterations: 73
currently lose_sum: 515.4472556114197
time_elpased: 1.602
batch start
#iterations: 74
currently lose_sum: 514.9314235448837
time_elpased: 1.599
batch start
#iterations: 75
currently lose_sum: 516.0056244432926
time_elpased: 1.615
batch start
#iterations: 76
currently lose_sum: 515.1125054955482
time_elpased: 1.602
batch start
#iterations: 77
currently lose_sum: 515.1101062595844
time_elpased: 1.59
batch start
#iterations: 78
currently lose_sum: 514.7401211559772
time_elpased: 1.599
batch start
#iterations: 79
currently lose_sum: 516.2366940975189
time_elpased: 1.609
start validation test
0.575670103093
0.889275074479
0.182755102041
0.303199593702
0
validation finish
batch start
#iterations: 80
currently lose_sum: 514.6983450353146
time_elpased: 1.615
batch start
#iterations: 81
currently lose_sum: 514.7233546078205
time_elpased: 1.61
batch start
#iterations: 82
currently lose_sum: 514.0637425482273
time_elpased: 1.585
batch start
#iterations: 83
currently lose_sum: 513.848226994276
time_elpased: 1.593
batch start
#iterations: 84
currently lose_sum: 513.2976023256779
time_elpased: 1.594
batch start
#iterations: 85
currently lose_sum: 514.7763623297215
time_elpased: 1.604
batch start
#iterations: 86
currently lose_sum: 513.6010789871216
time_elpased: 1.616
batch start
#iterations: 87
currently lose_sum: 512.3806541264057
time_elpased: 1.594
batch start
#iterations: 88
currently lose_sum: 512.6530682444572
time_elpased: 1.601
batch start
#iterations: 89
currently lose_sum: 515.731369048357
time_elpased: 1.598
batch start
#iterations: 90
currently lose_sum: 512.8867623507977
time_elpased: 1.597
batch start
#iterations: 91
currently lose_sum: 511.6269653439522
time_elpased: 1.592
batch start
#iterations: 92
currently lose_sum: 514.6547502875328
time_elpased: 1.591
batch start
#iterations: 93
currently lose_sum: 513.0769960284233
time_elpased: 1.593
batch start
#iterations: 94
currently lose_sum: 511.4102919995785
time_elpased: 1.596
batch start
#iterations: 95
currently lose_sum: 512.8722872436047
time_elpased: 1.612
batch start
#iterations: 96
currently lose_sum: 514.4760621190071
time_elpased: 1.607
batch start
#iterations: 97
currently lose_sum: 511.0166711509228
time_elpased: 1.598
batch start
#iterations: 98
currently lose_sum: 512.9313513338566
time_elpased: 1.602
batch start
#iterations: 99
currently lose_sum: 513.5260882973671
time_elpased: 1.594
start validation test
0.645927835052
0.873756694721
0.349591836735
0.499380511625
0
validation finish
batch start
#iterations: 100
currently lose_sum: 512.478998541832
time_elpased: 1.613
batch start
#iterations: 101
currently lose_sum: 511.7617955803871
time_elpased: 1.604
batch start
#iterations: 102
currently lose_sum: 510.1585704386234
time_elpased: 1.606
batch start
#iterations: 103
currently lose_sum: 510.6151985824108
time_elpased: 1.588
batch start
#iterations: 104
currently lose_sum: 512.9746782183647
time_elpased: 1.589
batch start
#iterations: 105
currently lose_sum: 510.97856798768044
time_elpased: 1.61
batch start
#iterations: 106
currently lose_sum: 514.5056165158749
time_elpased: 1.598
batch start
#iterations: 107
currently lose_sum: 513.5982349514961
time_elpased: 1.605
batch start
#iterations: 108
currently lose_sum: 510.3825144171715
time_elpased: 1.597
batch start
#iterations: 109
currently lose_sum: 510.04752174019814
time_elpased: 1.589
batch start
#iterations: 110
currently lose_sum: 510.6898089647293
time_elpased: 1.591
batch start
#iterations: 111
currently lose_sum: 513.5288592875004
time_elpased: 1.592
batch start
#iterations: 112
currently lose_sum: 510.785668194294
time_elpased: 1.598
batch start
#iterations: 113
currently lose_sum: 510.32922354340553
time_elpased: 1.6
batch start
#iterations: 114
currently lose_sum: 511.8432184755802
time_elpased: 1.601
batch start
#iterations: 115
currently lose_sum: 512.1821564435959
time_elpased: 1.606
batch start
#iterations: 116
currently lose_sum: 510.3282664716244
time_elpased: 1.603
batch start
#iterations: 117
currently lose_sum: 510.5853605568409
time_elpased: 1.599
batch start
#iterations: 118
currently lose_sum: 511.9491841197014
time_elpased: 1.609
batch start
#iterations: 119
currently lose_sum: 510.9716907739639
time_elpased: 1.599
start validation test
0.704793814433
0.860378694036
0.49612244898
0.629344379005
0
validation finish
batch start
#iterations: 120
currently lose_sum: 510.2943088412285
time_elpased: 1.633
batch start
#iterations: 121
currently lose_sum: 509.79089561104774
time_elpased: 1.608
batch start
#iterations: 122
currently lose_sum: 510.91783943772316
time_elpased: 1.598
batch start
#iterations: 123
currently lose_sum: 510.41447338461876
time_elpased: 1.6
batch start
#iterations: 124
currently lose_sum: 509.8933999836445
time_elpased: 1.585
batch start
#iterations: 125
currently lose_sum: 510.84780248999596
time_elpased: 1.597
batch start
#iterations: 126
currently lose_sum: 509.52752017974854
time_elpased: 1.602
batch start
#iterations: 127
currently lose_sum: 512.5405073463917
time_elpased: 1.603
batch start
#iterations: 128
currently lose_sum: 509.7345384657383
time_elpased: 1.6
batch start
#iterations: 129
currently lose_sum: 510.01476415991783
time_elpased: 1.61
batch start
#iterations: 130
currently lose_sum: 510.170897603035
time_elpased: 1.598
batch start
#iterations: 131
currently lose_sum: 509.1160865724087
time_elpased: 1.593
batch start
#iterations: 132
currently lose_sum: 510.4842175245285
time_elpased: 1.602
batch start
#iterations: 133
currently lose_sum: 510.7220788896084
time_elpased: 1.6
batch start
#iterations: 134
currently lose_sum: 509.7287138402462
time_elpased: 1.593
batch start
#iterations: 135
currently lose_sum: 507.7225250899792
time_elpased: 1.596
batch start
#iterations: 136
currently lose_sum: 508.8287675976753
time_elpased: 1.594
batch start
#iterations: 137
currently lose_sum: 507.85800537467003
time_elpased: 1.603
batch start
#iterations: 138
currently lose_sum: 509.57854932546616
time_elpased: 1.6
batch start
#iterations: 139
currently lose_sum: 509.66247206926346
time_elpased: 1.596
start validation test
0.536134020619
0.886956521739
0.0936734693878
0.169450853715
0
validation finish
batch start
#iterations: 140
currently lose_sum: 507.36464273929596
time_elpased: 1.612
batch start
#iterations: 141
currently lose_sum: 506.9203744828701
time_elpased: 1.602
batch start
#iterations: 142
currently lose_sum: 508.5194501578808
time_elpased: 1.609
batch start
#iterations: 143
currently lose_sum: 509.51370933651924
time_elpased: 1.607
batch start
#iterations: 144
currently lose_sum: 509.8606648147106
time_elpased: 1.595
batch start
#iterations: 145
currently lose_sum: 507.59028750658035
time_elpased: 1.594
batch start
#iterations: 146
currently lose_sum: 510.7546310722828
time_elpased: 1.626
batch start
#iterations: 147
currently lose_sum: 510.1440150141716
time_elpased: 1.595
batch start
#iterations: 148
currently lose_sum: 507.7464873492718
time_elpased: 1.599
batch start
#iterations: 149
currently lose_sum: 510.32833832502365
time_elpased: 1.602
batch start
#iterations: 150
currently lose_sum: 506.9801835119724
time_elpased: 1.613
batch start
#iterations: 151
currently lose_sum: 505.5312221944332
time_elpased: 1.617
batch start
#iterations: 152
currently lose_sum: 509.250554472208
time_elpased: 1.612
batch start
#iterations: 153
currently lose_sum: 505.8675112724304
time_elpased: 1.598
batch start
#iterations: 154
currently lose_sum: 511.21113058924675
time_elpased: 1.595
batch start
#iterations: 155
currently lose_sum: 506.70192474126816
time_elpased: 1.595
batch start
#iterations: 156
currently lose_sum: 509.08701863884926
time_elpased: 1.616
batch start
#iterations: 157
currently lose_sum: 507.8484852015972
time_elpased: 1.626
batch start
#iterations: 158
currently lose_sum: 506.3194258213043
time_elpased: 1.602
batch start
#iterations: 159
currently lose_sum: 510.4823548793793
time_elpased: 1.603
start validation test
0.525360824742
0.879487179487
0.07
0.129678638941
0
validation finish
batch start
#iterations: 160
currently lose_sum: 509.47130477428436
time_elpased: 1.623
batch start
#iterations: 161
currently lose_sum: 508.3647726178169
time_elpased: 1.609
batch start
#iterations: 162
currently lose_sum: 506.4869214594364
time_elpased: 1.608
batch start
#iterations: 163
currently lose_sum: 505.2680170238018
time_elpased: 1.601
batch start
#iterations: 164
currently lose_sum: 508.94435653090477
time_elpased: 1.608
batch start
#iterations: 165
currently lose_sum: 509.1429514884949
time_elpased: 1.605
batch start
#iterations: 166
currently lose_sum: 508.64727342128754
time_elpased: 1.591
batch start
#iterations: 167
currently lose_sum: 506.79112938046455
time_elpased: 1.603
batch start
#iterations: 168
currently lose_sum: 508.1828864812851
time_elpased: 1.617
batch start
#iterations: 169
currently lose_sum: 507.25778636336327
time_elpased: 1.603
batch start
#iterations: 170
currently lose_sum: 507.0495255589485
time_elpased: 1.604
batch start
#iterations: 171
currently lose_sum: 506.0520501732826
time_elpased: 1.6
batch start
#iterations: 172
currently lose_sum: 510.2146645486355
time_elpased: 1.611
batch start
#iterations: 173
currently lose_sum: 507.02841103076935
time_elpased: 1.601
batch start
#iterations: 174
currently lose_sum: 509.6032654941082
time_elpased: 1.596
batch start
#iterations: 175
currently lose_sum: 506.8239884674549
time_elpased: 1.6
batch start
#iterations: 176
currently lose_sum: 507.08280742168427
time_elpased: 1.616
batch start
#iterations: 177
currently lose_sum: 506.15127366781235
time_elpased: 1.616
batch start
#iterations: 178
currently lose_sum: 507.2480613589287
time_elpased: 1.595
batch start
#iterations: 179
currently lose_sum: 508.0054982602596
time_elpased: 1.597
start validation test
0.568092783505
0.879743452699
0.167959183673
0.282066660955
0
validation finish
batch start
#iterations: 180
currently lose_sum: 506.87621903419495
time_elpased: 1.633
batch start
#iterations: 181
currently lose_sum: 507.78099966049194
time_elpased: 1.6
batch start
#iterations: 182
currently lose_sum: 506.76467567682266
time_elpased: 1.611
batch start
#iterations: 183
currently lose_sum: 506.35116970539093
time_elpased: 1.613
batch start
#iterations: 184
currently lose_sum: 506.53114050626755
time_elpased: 1.623
batch start
#iterations: 185
currently lose_sum: 506.6869204342365
time_elpased: 1.611
batch start
#iterations: 186
currently lose_sum: 507.9485214948654
time_elpased: 1.595
batch start
#iterations: 187
currently lose_sum: 504.78855434060097
time_elpased: 1.607
batch start
#iterations: 188
currently lose_sum: 506.04945009946823
time_elpased: 1.603
batch start
#iterations: 189
currently lose_sum: 507.9688046872616
time_elpased: 1.605
batch start
#iterations: 190
currently lose_sum: 506.1524589955807
time_elpased: 1.606
batch start
#iterations: 191
currently lose_sum: 507.0916947424412
time_elpased: 1.608
batch start
#iterations: 192
currently lose_sum: 505.84016731381416
time_elpased: 1.611
batch start
#iterations: 193
currently lose_sum: 505.3059234917164
time_elpased: 1.609
batch start
#iterations: 194
currently lose_sum: 507.468975096941
time_elpased: 1.61
batch start
#iterations: 195
currently lose_sum: 506.0962381064892
time_elpased: 1.61
batch start
#iterations: 196
currently lose_sum: 503.26102086901665
time_elpased: 1.602
batch start
#iterations: 197
currently lose_sum: 507.080195248127
time_elpased: 1.603
batch start
#iterations: 198
currently lose_sum: 505.5042158663273
time_elpased: 1.597
batch start
#iterations: 199
currently lose_sum: 504.49209198355675
time_elpased: 1.614
start validation test
0.592525773196
0.880369329587
0.223775510204
0.356846473029
0
validation finish
batch start
#iterations: 200
currently lose_sum: 505.662167519331
time_elpased: 1.607
batch start
#iterations: 201
currently lose_sum: 505.90891963243484
time_elpased: 1.602
batch start
#iterations: 202
currently lose_sum: 505.6916407048702
time_elpased: 1.598
batch start
#iterations: 203
currently lose_sum: 505.6406524181366
time_elpased: 1.594
batch start
#iterations: 204
currently lose_sum: 504.8987113535404
time_elpased: 1.633
batch start
#iterations: 205
currently lose_sum: 505.42534536123276
time_elpased: 1.602
batch start
#iterations: 206
currently lose_sum: 508.70352175831795
time_elpased: 1.604
batch start
#iterations: 207
currently lose_sum: 506.7293833196163
time_elpased: 1.606
batch start
#iterations: 208
currently lose_sum: 505.3125051856041
time_elpased: 1.599
batch start
#iterations: 209
currently lose_sum: 504.90010833740234
time_elpased: 1.607
batch start
#iterations: 210
currently lose_sum: 506.1896446943283
time_elpased: 1.595
batch start
#iterations: 211
currently lose_sum: 506.70890694856644
time_elpased: 1.604
batch start
#iterations: 212
currently lose_sum: 505.62251204252243
time_elpased: 1.61
batch start
#iterations: 213
currently lose_sum: 505.17450577020645
time_elpased: 1.59
batch start
#iterations: 214
currently lose_sum: 506.5390908420086
time_elpased: 1.603
batch start
#iterations: 215
currently lose_sum: 505.48322880268097
time_elpased: 1.612
batch start
#iterations: 216
currently lose_sum: 505.52950569987297
time_elpased: 1.624
batch start
#iterations: 217
currently lose_sum: 505.76975616812706
time_elpased: 1.6
batch start
#iterations: 218
currently lose_sum: 506.2507267296314
time_elpased: 1.598
batch start
#iterations: 219
currently lose_sum: 506.66954693198204
time_elpased: 1.603
start validation test
0.532164948454
0.890928725702
0.0841836734694
0.153831810554
0
validation finish
batch start
#iterations: 220
currently lose_sum: 504.28109753131866
time_elpased: 1.599
batch start
#iterations: 221
currently lose_sum: 508.2969890832901
time_elpased: 1.601
batch start
#iterations: 222
currently lose_sum: 504.4075045287609
time_elpased: 1.606
batch start
#iterations: 223
currently lose_sum: 504.1355744898319
time_elpased: 1.599
batch start
#iterations: 224
currently lose_sum: 507.52369314432144
time_elpased: 1.607
batch start
#iterations: 225
currently lose_sum: 506.37670516967773
time_elpased: 1.603
batch start
#iterations: 226
currently lose_sum: 507.14130982756615
time_elpased: 1.605
batch start
#iterations: 227
currently lose_sum: 506.5478552877903
time_elpased: 1.62
batch start
#iterations: 228
currently lose_sum: 503.1830855309963
time_elpased: 1.612
batch start
#iterations: 229
currently lose_sum: 506.1957377791405
time_elpased: 1.618
batch start
#iterations: 230
currently lose_sum: 506.49879655241966
time_elpased: 1.616
batch start
#iterations: 231
currently lose_sum: 507.18839889764786
time_elpased: 1.612
batch start
#iterations: 232
currently lose_sum: 502.16278222203255
time_elpased: 1.615
batch start
#iterations: 233
currently lose_sum: 503.86999955773354
time_elpased: 1.597
batch start
#iterations: 234
currently lose_sum: 505.44098234176636
time_elpased: 1.596
batch start
#iterations: 235
currently lose_sum: 505.418326407671
time_elpased: 1.602
batch start
#iterations: 236
currently lose_sum: 504.55551195144653
time_elpased: 1.596
batch start
#iterations: 237
currently lose_sum: 505.14681857824326
time_elpased: 1.605
batch start
#iterations: 238
currently lose_sum: 503.36832424998283
time_elpased: 1.615
batch start
#iterations: 239
currently lose_sum: 503.9798219501972
time_elpased: 1.618
start validation test
0.51912371134
0.884176182708
0.055306122449
0.104100643426
0
validation finish
batch start
#iterations: 240
currently lose_sum: 502.73419520258904
time_elpased: 1.615
batch start
#iterations: 241
currently lose_sum: 503.8592404425144
time_elpased: 1.609
batch start
#iterations: 242
currently lose_sum: 502.29015761613846
time_elpased: 1.601
batch start
#iterations: 243
currently lose_sum: 503.4735968410969
time_elpased: 1.59
batch start
#iterations: 244
currently lose_sum: 504.3167612552643
time_elpased: 1.618
batch start
#iterations: 245
currently lose_sum: 504.5799403786659
time_elpased: 1.602
batch start
#iterations: 246
currently lose_sum: 505.46988520026207
time_elpased: 1.604
batch start
#iterations: 247
currently lose_sum: 504.832478672266
time_elpased: 1.601
batch start
#iterations: 248
currently lose_sum: 503.83205139636993
time_elpased: 1.596
batch start
#iterations: 249
currently lose_sum: 505.9480620622635
time_elpased: 1.605
batch start
#iterations: 250
currently lose_sum: 503.0041435956955
time_elpased: 1.623
batch start
#iterations: 251
currently lose_sum: 503.96842420101166
time_elpased: 1.597
batch start
#iterations: 252
currently lose_sum: 506.1466381251812
time_elpased: 1.591
batch start
#iterations: 253
currently lose_sum: 502.587897002697
time_elpased: 1.608
batch start
#iterations: 254
currently lose_sum: 505.5872054696083
time_elpased: 1.597
batch start
#iterations: 255
currently lose_sum: 505.3654851913452
time_elpased: 1.603
batch start
#iterations: 256
currently lose_sum: 502.909126996994
time_elpased: 1.609
batch start
#iterations: 257
currently lose_sum: 504.09483775496483
time_elpased: 1.599
batch start
#iterations: 258
currently lose_sum: 503.831870675087
time_elpased: 1.613
batch start
#iterations: 259
currently lose_sum: 505.89956068992615
time_elpased: 1.596
start validation test
0.563195876289
0.882794457275
0.156020408163
0.265175164759
0
validation finish
batch start
#iterations: 260
currently lose_sum: 505.41927510499954
time_elpased: 1.607
batch start
#iterations: 261
currently lose_sum: 505.06854596734047
time_elpased: 1.607
batch start
#iterations: 262
currently lose_sum: 505.711413949728
time_elpased: 1.605
batch start
#iterations: 263
currently lose_sum: 504.7619606256485
time_elpased: 1.634
batch start
#iterations: 264
currently lose_sum: 503.9604376554489
time_elpased: 1.609
batch start
#iterations: 265
currently lose_sum: 503.52298587560654
time_elpased: 1.616
batch start
#iterations: 266
currently lose_sum: 503.7028518617153
time_elpased: 1.607
batch start
#iterations: 267
currently lose_sum: 504.49644136428833
time_elpased: 1.603
batch start
#iterations: 268
currently lose_sum: 504.8607521057129
time_elpased: 1.595
batch start
#iterations: 269
currently lose_sum: 505.15430077910423
time_elpased: 1.612
batch start
#iterations: 270
currently lose_sum: 503.63821056485176
time_elpased: 1.612
batch start
#iterations: 271
currently lose_sum: 500.37101942300797
time_elpased: 1.604
batch start
#iterations: 272
currently lose_sum: 504.51696875691414
time_elpased: 1.606
batch start
#iterations: 273
currently lose_sum: 502.7743394970894
time_elpased: 1.594
batch start
#iterations: 274
currently lose_sum: 504.60363137722015
time_elpased: 1.609
batch start
#iterations: 275
currently lose_sum: 505.1624782681465
time_elpased: 1.595
batch start
#iterations: 276
currently lose_sum: 502.5816336274147
time_elpased: 1.611
batch start
#iterations: 277
currently lose_sum: 505.7397398352623
time_elpased: 1.606
batch start
#iterations: 278
currently lose_sum: 505.6634221971035
time_elpased: 1.612
batch start
#iterations: 279
currently lose_sum: 503.88441625237465
time_elpased: 1.623
start validation test
0.575154639175
0.861988847584
0.189285714286
0.310408299866
0
validation finish
batch start
#iterations: 280
currently lose_sum: 502.9988562762737
time_elpased: 1.606
batch start
#iterations: 281
currently lose_sum: 503.449406683445
time_elpased: 1.614
batch start
#iterations: 282
currently lose_sum: 503.41973009705544
time_elpased: 1.597
batch start
#iterations: 283
currently lose_sum: 503.89300069212914
time_elpased: 1.598
batch start
#iterations: 284
currently lose_sum: 504.594160169363
time_elpased: 1.598
batch start
#iterations: 285
currently lose_sum: 503.1480901837349
time_elpased: 1.591
batch start
#iterations: 286
currently lose_sum: 504.25324457883835
time_elpased: 1.603
batch start
#iterations: 287
currently lose_sum: 504.08737993240356
time_elpased: 1.589
batch start
#iterations: 288
currently lose_sum: 504.51990231871605
time_elpased: 1.612
batch start
#iterations: 289
currently lose_sum: 503.91596579551697
time_elpased: 1.594
batch start
#iterations: 290
currently lose_sum: 502.1598205566406
time_elpased: 1.607
batch start
#iterations: 291
currently lose_sum: 503.9779051244259
time_elpased: 1.604
batch start
#iterations: 292
currently lose_sum: 503.7550622522831
time_elpased: 1.613
batch start
#iterations: 293
currently lose_sum: 503.1280636191368
time_elpased: 1.597
batch start
#iterations: 294
currently lose_sum: 503.61028093099594
time_elpased: 1.618
batch start
#iterations: 295
currently lose_sum: 504.255574464798
time_elpased: 1.619
batch start
#iterations: 296
currently lose_sum: 501.4627725481987
time_elpased: 1.614
batch start
#iterations: 297
currently lose_sum: 503.46555826067924
time_elpased: 1.608
batch start
#iterations: 298
currently lose_sum: 502.1351753473282
time_elpased: 1.587
batch start
#iterations: 299
currently lose_sum: 502.3942439854145
time_elpased: 1.621
start validation test
0.635309278351
0.868941240184
0.327448979592
0.475654042837
0
validation finish
batch start
#iterations: 300
currently lose_sum: 501.6646240055561
time_elpased: 1.617
batch start
#iterations: 301
currently lose_sum: 504.0273592174053
time_elpased: 1.61
batch start
#iterations: 302
currently lose_sum: 504.60589650273323
time_elpased: 1.596
batch start
#iterations: 303
currently lose_sum: 503.2447740137577
time_elpased: 1.612
batch start
#iterations: 304
currently lose_sum: 503.85365468263626
time_elpased: 1.593
batch start
#iterations: 305
currently lose_sum: 504.80508878827095
time_elpased: 1.6
batch start
#iterations: 306
currently lose_sum: 503.19782385230064
time_elpased: 1.616
batch start
#iterations: 307
currently lose_sum: 506.67075714468956
time_elpased: 1.593
batch start
#iterations: 308
currently lose_sum: 503.861024081707
time_elpased: 1.605
batch start
#iterations: 309
currently lose_sum: 504.8424735367298
time_elpased: 1.592
batch start
#iterations: 310
currently lose_sum: 501.7517594397068
time_elpased: 1.6
batch start
#iterations: 311
currently lose_sum: 501.7058391869068
time_elpased: 1.62
batch start
#iterations: 312
currently lose_sum: 501.90135791897774
time_elpased: 1.61
batch start
#iterations: 313
currently lose_sum: 504.0121336877346
time_elpased: 1.606
batch start
#iterations: 314
currently lose_sum: 501.8593429028988
time_elpased: 1.59
batch start
#iterations: 315
currently lose_sum: 503.86182379722595
time_elpased: 1.601
batch start
#iterations: 316
currently lose_sum: 502.152081489563
time_elpased: 1.611
batch start
#iterations: 317
currently lose_sum: 503.3003346025944
time_elpased: 1.599
batch start
#iterations: 318
currently lose_sum: 504.91199335455894
time_elpased: 1.6
batch start
#iterations: 319
currently lose_sum: 503.51603454351425
time_elpased: 1.618
start validation test
0.508556701031
0.869444444444
0.0319387755102
0.0616141732283
0
validation finish
batch start
#iterations: 320
currently lose_sum: 503.25414794683456
time_elpased: 1.617
batch start
#iterations: 321
currently lose_sum: 501.91078338027
time_elpased: 1.608
batch start
#iterations: 322
currently lose_sum: 502.01098158955574
time_elpased: 1.583
batch start
#iterations: 323
currently lose_sum: 503.2945447266102
time_elpased: 1.595
batch start
#iterations: 324
currently lose_sum: 505.0315617918968
time_elpased: 1.605
batch start
#iterations: 325
currently lose_sum: 504.1087219417095
time_elpased: 1.605
batch start
#iterations: 326
currently lose_sum: 504.037898927927
time_elpased: 1.596
batch start
#iterations: 327
currently lose_sum: 502.7232001721859
time_elpased: 1.593
batch start
#iterations: 328
currently lose_sum: 502.69289967417717
time_elpased: 1.61
batch start
#iterations: 329
currently lose_sum: 503.3213225901127
time_elpased: 1.605
batch start
#iterations: 330
currently lose_sum: 504.38647812604904
time_elpased: 1.606
batch start
#iterations: 331
currently lose_sum: 504.0179128050804
time_elpased: 1.616
batch start
#iterations: 332
currently lose_sum: 502.6958344876766
time_elpased: 1.622
batch start
#iterations: 333
currently lose_sum: 503.9780040383339
time_elpased: 1.609
batch start
#iterations: 334
currently lose_sum: 504.2016368508339
time_elpased: 1.6
batch start
#iterations: 335
currently lose_sum: 499.79934760928154
time_elpased: 1.601
batch start
#iterations: 336
currently lose_sum: 504.05881065130234
time_elpased: 1.603
batch start
#iterations: 337
currently lose_sum: 501.76863861083984
time_elpased: 1.627
batch start
#iterations: 338
currently lose_sum: 504.1052428781986
time_elpased: 1.618
batch start
#iterations: 339
currently lose_sum: 502.26542553305626
time_elpased: 1.611
start validation test
0.580670103093
0.876526458616
0.197755102041
0.322704187828
0
validation finish
batch start
#iterations: 340
currently lose_sum: 502.49578922986984
time_elpased: 1.613
batch start
#iterations: 341
currently lose_sum: 502.8211061656475
time_elpased: 1.617
batch start
#iterations: 342
currently lose_sum: 503.10404911637306
time_elpased: 1.605
batch start
#iterations: 343
currently lose_sum: 503.32579731941223
time_elpased: 1.616
batch start
#iterations: 344
currently lose_sum: 503.9816571176052
time_elpased: 1.599
batch start
#iterations: 345
currently lose_sum: 503.5373164117336
time_elpased: 1.61
batch start
#iterations: 346
currently lose_sum: 501.8360888659954
time_elpased: 1.601
batch start
#iterations: 347
currently lose_sum: 502.8018275797367
time_elpased: 1.613
batch start
#iterations: 348
currently lose_sum: 504.3193749189377
time_elpased: 1.619
batch start
#iterations: 349
currently lose_sum: 503.6415950357914
time_elpased: 1.622
batch start
#iterations: 350
currently lose_sum: 503.60698714852333
time_elpased: 1.605
batch start
#iterations: 351
currently lose_sum: 502.5685833096504
time_elpased: 1.596
batch start
#iterations: 352
currently lose_sum: 502.4185582995415
time_elpased: 1.621
batch start
#iterations: 353
currently lose_sum: 504.33475667238235
time_elpased: 1.611
batch start
#iterations: 354
currently lose_sum: 501.8995354771614
time_elpased: 1.605
batch start
#iterations: 355
currently lose_sum: 503.17661145329475
time_elpased: 1.61
batch start
#iterations: 356
currently lose_sum: 503.46413859725
time_elpased: 1.6
batch start
#iterations: 357
currently lose_sum: 503.85487642884254
time_elpased: 1.597
batch start
#iterations: 358
currently lose_sum: 503.3066810667515
time_elpased: 1.603
batch start
#iterations: 359
currently lose_sum: 503.34986141324043
time_elpased: 1.602
start validation test
0.576443298969
0.871421867668
0.189489795918
0.311289917023
0
validation finish
batch start
#iterations: 360
currently lose_sum: 502.9326034784317
time_elpased: 1.643
batch start
#iterations: 361
currently lose_sum: 501.07004857063293
time_elpased: 1.594
batch start
#iterations: 362
currently lose_sum: 502.1431014239788
time_elpased: 1.613
batch start
#iterations: 363
currently lose_sum: 502.259076744318
time_elpased: 1.59
batch start
#iterations: 364
currently lose_sum: 502.6193015575409
time_elpased: 1.614
batch start
#iterations: 365
currently lose_sum: 502.65289863944054
time_elpased: 1.589
batch start
#iterations: 366
currently lose_sum: 503.40901201963425
time_elpased: 1.611
batch start
#iterations: 367
currently lose_sum: 503.383579403162
time_elpased: 1.604
batch start
#iterations: 368
currently lose_sum: 500.8776391148567
time_elpased: 1.617
batch start
#iterations: 369
currently lose_sum: 502.4105169773102
time_elpased: 1.595
batch start
#iterations: 370
currently lose_sum: 501.50570979714394
time_elpased: 1.605
batch start
#iterations: 371
currently lose_sum: 502.32599663734436
time_elpased: 1.6
batch start
#iterations: 372
currently lose_sum: 502.6925844550133
time_elpased: 1.605
batch start
#iterations: 373
currently lose_sum: 503.1475158035755
time_elpased: 1.593
batch start
#iterations: 374
currently lose_sum: 503.55854028463364
time_elpased: 1.601
batch start
#iterations: 375
currently lose_sum: 501.9012114405632
time_elpased: 1.603
batch start
#iterations: 376
currently lose_sum: 502.69627186656
time_elpased: 1.601
batch start
#iterations: 377
currently lose_sum: 504.2835239171982
time_elpased: 1.608
batch start
#iterations: 378
currently lose_sum: 502.6727068424225
time_elpased: 1.606
batch start
#iterations: 379
currently lose_sum: 501.7035802304745
time_elpased: 1.606
start validation test
0.590051546392
0.869547819128
0.221734693878
0.353362061956
0
validation finish
batch start
#iterations: 380
currently lose_sum: 502.489622592926
time_elpased: 1.601
batch start
#iterations: 381
currently lose_sum: 500.43884843587875
time_elpased: 1.611
batch start
#iterations: 382
currently lose_sum: 502.24884337186813
time_elpased: 1.6
batch start
#iterations: 383
currently lose_sum: 500.65368911623955
time_elpased: 1.605
batch start
#iterations: 384
currently lose_sum: 501.57730984687805
time_elpased: 1.61
batch start
#iterations: 385
currently lose_sum: 501.0467747747898
time_elpased: 1.597
batch start
#iterations: 386
currently lose_sum: 502.98600417375565
time_elpased: 1.61
batch start
#iterations: 387
currently lose_sum: 502.99193811416626
time_elpased: 1.602
batch start
#iterations: 388
currently lose_sum: 502.5743463933468
time_elpased: 1.602
batch start
#iterations: 389
currently lose_sum: 500.1382804811001
time_elpased: 1.601
batch start
#iterations: 390
currently lose_sum: 501.42497503757477
time_elpased: 1.619
batch start
#iterations: 391
currently lose_sum: 502.7460077404976
time_elpased: 1.598
batch start
#iterations: 392
currently lose_sum: 502.1498566865921
time_elpased: 1.605
batch start
#iterations: 393
currently lose_sum: 502.22538062930107
time_elpased: 1.606
batch start
#iterations: 394
currently lose_sum: 501.7546995282173
time_elpased: 1.599
batch start
#iterations: 395
currently lose_sum: 501.5168813765049
time_elpased: 1.614
batch start
#iterations: 396
currently lose_sum: 503.09945675730705
time_elpased: 1.6
batch start
#iterations: 397
currently lose_sum: 502.7909091114998
time_elpased: 1.613
batch start
#iterations: 398
currently lose_sum: 501.4383935332298
time_elpased: 1.595
batch start
#iterations: 399
currently lose_sum: 503.2664919793606
time_elpased: 1.606
start validation test
0.537216494845
0.879852125693
0.0971428571429
0.174967836795
0
validation finish
acc: 0.707
pre: 0.852
rec: 0.494
F1: 0.626
auc: 0.000
