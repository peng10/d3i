start to construct graph
graph construct over
87453
epochs start
batch start
#iterations: 0
currently lose_sum: 556.0525614023209
time_elpased: 1.654
batch start
#iterations: 1
currently lose_sum: 555.5268649458885
time_elpased: 1.594
batch start
#iterations: 2
currently lose_sum: 554.7717798948288
time_elpased: 1.595
batch start
#iterations: 3
currently lose_sum: 555.1489391326904
time_elpased: 1.606
batch start
#iterations: 4
currently lose_sum: 550.8804017901421
time_elpased: 1.602
batch start
#iterations: 5
currently lose_sum: 551.2653898596764
time_elpased: 1.603
batch start
#iterations: 6
currently lose_sum: 550.5830245614052
time_elpased: 1.598
batch start
#iterations: 7
currently lose_sum: 548.6169331073761
time_elpased: 1.59
batch start
#iterations: 8
currently lose_sum: 548.7043615579605
time_elpased: 1.614
batch start
#iterations: 9
currently lose_sum: 545.3213468790054
time_elpased: 1.587
batch start
#iterations: 10
currently lose_sum: 543.8457470536232
time_elpased: 1.589
batch start
#iterations: 11
currently lose_sum: 541.7122501730919
time_elpased: 1.606
batch start
#iterations: 12
currently lose_sum: 543.7017876207829
time_elpased: 1.588
batch start
#iterations: 13
currently lose_sum: 538.6502963900566
time_elpased: 1.589
batch start
#iterations: 14
currently lose_sum: 539.1705724895
time_elpased: 1.589
batch start
#iterations: 15
currently lose_sum: 536.0281403064728
time_elpased: 1.598
batch start
#iterations: 16
currently lose_sum: 535.0527350604534
time_elpased: 1.608
batch start
#iterations: 17
currently lose_sum: 535.0818539857864
time_elpased: 1.597
batch start
#iterations: 18
currently lose_sum: 534.6813946664333
time_elpased: 1.596
batch start
#iterations: 19
currently lose_sum: 532.6261691749096
time_elpased: 1.599
start validation test
0.598144329897
0.87792706334
0.234203789042
0.369765561843
0
validation finish
batch start
#iterations: 20
currently lose_sum: 533.7142130434513
time_elpased: 1.631
batch start
#iterations: 21
currently lose_sum: 531.2707955241203
time_elpased: 1.599
batch start
#iterations: 22
currently lose_sum: 531.6794471740723
time_elpased: 1.611
batch start
#iterations: 23
currently lose_sum: 529.5016908943653
time_elpased: 1.594
batch start
#iterations: 24
currently lose_sum: 530.359049320221
time_elpased: 1.607
batch start
#iterations: 25
currently lose_sum: 528.1506313681602
time_elpased: 1.593
batch start
#iterations: 26
currently lose_sum: 527.7402398586273
time_elpased: 1.6
batch start
#iterations: 27
currently lose_sum: 528.9905428290367
time_elpased: 1.596
batch start
#iterations: 28
currently lose_sum: 526.5654143691063
time_elpased: 1.606
batch start
#iterations: 29
currently lose_sum: 526.5915926992893
time_elpased: 1.598
batch start
#iterations: 30
currently lose_sum: 526.049755692482
time_elpased: 1.606
batch start
#iterations: 31
currently lose_sum: 526.7521106302738
time_elpased: 1.593
batch start
#iterations: 32
currently lose_sum: 527.0631909668446
time_elpased: 1.591
batch start
#iterations: 33
currently lose_sum: 525.4179240465164
time_elpased: 1.6
batch start
#iterations: 34
currently lose_sum: 524.4580845832825
time_elpased: 1.592
batch start
#iterations: 35
currently lose_sum: 525.7664930522442
time_elpased: 1.592
batch start
#iterations: 36
currently lose_sum: 525.0318369567394
time_elpased: 1.608
batch start
#iterations: 37
currently lose_sum: 524.0731735527515
time_elpased: 1.598
batch start
#iterations: 38
currently lose_sum: 523.8675628006458
time_elpased: 1.598
batch start
#iterations: 39
currently lose_sum: 523.6526191532612
time_elpased: 1.601
start validation test
0.578041237113
0.883066472586
0.186379928315
0.307796380856
0
validation finish
batch start
#iterations: 40
currently lose_sum: 524.1135123968124
time_elpased: 1.617
batch start
#iterations: 41
currently lose_sum: 524.810476988554
time_elpased: 1.593
batch start
#iterations: 42
currently lose_sum: 522.5634626448154
time_elpased: 1.605
batch start
#iterations: 43
currently lose_sum: 523.8170178234577
time_elpased: 1.591
batch start
#iterations: 44
currently lose_sum: 519.9824449121952
time_elpased: 1.595
batch start
#iterations: 45
currently lose_sum: 520.7229415476322
time_elpased: 1.593
batch start
#iterations: 46
currently lose_sum: 524.141173273325
time_elpased: 1.594
batch start
#iterations: 47
currently lose_sum: 521.6100762784481
time_elpased: 1.599
batch start
#iterations: 48
currently lose_sum: 519.5010609924793
time_elpased: 1.602
batch start
#iterations: 49
currently lose_sum: 520.1891706585884
time_elpased: 1.597
batch start
#iterations: 50
currently lose_sum: 519.9804976284504
time_elpased: 1.606
batch start
#iterations: 51
currently lose_sum: 520.9376167654991
time_elpased: 1.591
batch start
#iterations: 52
currently lose_sum: 521.0014988481998
time_elpased: 1.603
batch start
#iterations: 53
currently lose_sum: 519.7740521430969
time_elpased: 1.609
batch start
#iterations: 54
currently lose_sum: 520.195572823286
time_elpased: 1.613
batch start
#iterations: 55
currently lose_sum: 518.3588563501835
time_elpased: 1.61
batch start
#iterations: 56
currently lose_sum: 517.4552126824856
time_elpased: 1.608
batch start
#iterations: 57
currently lose_sum: 519.0991770923138
time_elpased: 1.602
batch start
#iterations: 58
currently lose_sum: 518.3151785135269
time_elpased: 1.599
batch start
#iterations: 59
currently lose_sum: 519.2249545454979
time_elpased: 1.597
start validation test
0.505206185567
0.923469387755
0.0185355862775
0.0363417327578
0
validation finish
batch start
#iterations: 60
currently lose_sum: 517.8804306983948
time_elpased: 1.594
batch start
#iterations: 61
currently lose_sum: 517.0267909169197
time_elpased: 1.589
batch start
#iterations: 62
currently lose_sum: 517.9182466566563
time_elpased: 1.588
batch start
#iterations: 63
currently lose_sum: 520.8674125671387
time_elpased: 1.605
batch start
#iterations: 64
currently lose_sum: 517.1773802042007
time_elpased: 1.604
batch start
#iterations: 65
currently lose_sum: 518.4879133999348
time_elpased: 1.599
batch start
#iterations: 66
currently lose_sum: 517.0343083739281
time_elpased: 1.593
batch start
#iterations: 67
currently lose_sum: 514.8246389329433
time_elpased: 1.603
batch start
#iterations: 68
currently lose_sum: 515.2417504191399
time_elpased: 1.588
batch start
#iterations: 69
currently lose_sum: 515.4310652911663
time_elpased: 1.603
batch start
#iterations: 70
currently lose_sum: 517.4845876693726
time_elpased: 1.595
batch start
#iterations: 71
currently lose_sum: 516.5606774091721
time_elpased: 1.595
batch start
#iterations: 72
currently lose_sum: 515.0977029502392
time_elpased: 1.59
batch start
#iterations: 73
currently lose_sum: 516.956384241581
time_elpased: 1.592
batch start
#iterations: 74
currently lose_sum: 515.4791898429394
time_elpased: 1.61
batch start
#iterations: 75
currently lose_sum: 515.9397352933884
time_elpased: 1.595
batch start
#iterations: 76
currently lose_sum: 515.8382056951523
time_elpased: 1.591
batch start
#iterations: 77
currently lose_sum: 515.8209454417229
time_elpased: 1.585
batch start
#iterations: 78
currently lose_sum: 514.5138784646988
time_elpased: 1.598
batch start
#iterations: 79
currently lose_sum: 516.2699013650417
time_elpased: 1.59
start validation test
0.65587628866
0.87047253538
0.371633384537
0.520884168222
0
validation finish
batch start
#iterations: 80
currently lose_sum: 516.1601854264736
time_elpased: 1.634
batch start
#iterations: 81
currently lose_sum: 516.132858723402
time_elpased: 1.597
batch start
#iterations: 82
currently lose_sum: 515.4300417602062
time_elpased: 1.592
batch start
#iterations: 83
currently lose_sum: 512.6188659369946
time_elpased: 1.595
batch start
#iterations: 84
currently lose_sum: 515.0442419350147
time_elpased: 1.612
batch start
#iterations: 85
currently lose_sum: 515.5572852194309
time_elpased: 1.594
batch start
#iterations: 86
currently lose_sum: 515.7383848130703
time_elpased: 1.596
batch start
#iterations: 87
currently lose_sum: 515.075470328331
time_elpased: 1.593
batch start
#iterations: 88
currently lose_sum: 515.2628566026688
time_elpased: 1.593
batch start
#iterations: 89
currently lose_sum: 513.3191222250462
time_elpased: 1.592
batch start
#iterations: 90
currently lose_sum: 513.5066593885422
time_elpased: 1.595
batch start
#iterations: 91
currently lose_sum: 512.5777829885483
time_elpased: 1.609
batch start
#iterations: 92
currently lose_sum: 512.6557686328888
time_elpased: 1.596
batch start
#iterations: 93
currently lose_sum: 512.3584441244602
time_elpased: 1.601
batch start
#iterations: 94
currently lose_sum: 514.0422273874283
time_elpased: 1.609
batch start
#iterations: 95
currently lose_sum: 513.1039053201675
time_elpased: 1.584
batch start
#iterations: 96
currently lose_sum: 513.6028419733047
time_elpased: 1.597
batch start
#iterations: 97
currently lose_sum: 515.7068618834019
time_elpased: 1.595
batch start
#iterations: 98
currently lose_sum: 511.0950258374214
time_elpased: 1.588
batch start
#iterations: 99
currently lose_sum: 513.4950757324696
time_elpased: 1.604
start validation test
0.569329896907
0.914218566392
0.159344598054
0.271387459667
0
validation finish
batch start
#iterations: 100
currently lose_sum: 512.6349105238914
time_elpased: 1.6
batch start
#iterations: 101
currently lose_sum: 512.3767741918564
time_elpased: 1.594
batch start
#iterations: 102
currently lose_sum: 513.657636910677
time_elpased: 1.59
batch start
#iterations: 103
currently lose_sum: 511.51928666234016
time_elpased: 1.604
batch start
#iterations: 104
currently lose_sum: 510.9118314385414
time_elpased: 1.605
batch start
#iterations: 105
currently lose_sum: 512.4665894806385
time_elpased: 1.597
batch start
#iterations: 106
currently lose_sum: 512.8097906410694
time_elpased: 1.6
batch start
#iterations: 107
currently lose_sum: 512.4227423071861
time_elpased: 1.593
batch start
#iterations: 108
currently lose_sum: 510.9563075900078
time_elpased: 1.596
batch start
#iterations: 109
currently lose_sum: 514.430106818676
time_elpased: 1.613
batch start
#iterations: 110
currently lose_sum: 511.9862174987793
time_elpased: 1.613
batch start
#iterations: 111
currently lose_sum: 514.0142461359501
time_elpased: 1.612
batch start
#iterations: 112
currently lose_sum: 510.50387409329414
time_elpased: 1.59
batch start
#iterations: 113
currently lose_sum: 513.1881720721722
time_elpased: 1.604
batch start
#iterations: 114
currently lose_sum: 513.1482472121716
time_elpased: 1.615
batch start
#iterations: 115
currently lose_sum: 513.1689702570438
time_elpased: 1.597
batch start
#iterations: 116
currently lose_sum: 512.1670914590359
time_elpased: 1.601
batch start
#iterations: 117
currently lose_sum: 511.0554583966732
time_elpased: 1.598
batch start
#iterations: 118
currently lose_sum: 512.3014668822289
time_elpased: 1.593
batch start
#iterations: 119
currently lose_sum: 510.67431485652924
time_elpased: 1.614
start validation test
0.659381443299
0.873609467456
0.377982590886
0.527662616154
0
validation finish
batch start
#iterations: 120
currently lose_sum: 512.1779171824455
time_elpased: 1.627
batch start
#iterations: 121
currently lose_sum: 511.0637800693512
time_elpased: 1.598
batch start
#iterations: 122
currently lose_sum: 512.806156039238
time_elpased: 1.605
batch start
#iterations: 123
currently lose_sum: 514.5846337974072
time_elpased: 1.585
batch start
#iterations: 124
currently lose_sum: 511.34407618641853
time_elpased: 1.595
batch start
#iterations: 125
currently lose_sum: 509.0525501072407
time_elpased: 1.609
batch start
#iterations: 126
currently lose_sum: 509.6636783182621
time_elpased: 1.603
batch start
#iterations: 127
currently lose_sum: 511.2437410056591
time_elpased: 1.62
batch start
#iterations: 128
currently lose_sum: 510.60259968042374
time_elpased: 1.591
batch start
#iterations: 129
currently lose_sum: 510.1225266158581
time_elpased: 1.589
batch start
#iterations: 130
currently lose_sum: 509.815786331892
time_elpased: 1.606
batch start
#iterations: 131
currently lose_sum: 509.7019712924957
time_elpased: 1.589
batch start
#iterations: 132
currently lose_sum: 509.67346957325935
time_elpased: 1.6
batch start
#iterations: 133
currently lose_sum: 510.7081803381443
time_elpased: 1.589
batch start
#iterations: 134
currently lose_sum: 509.7080884575844
time_elpased: 1.602
batch start
#iterations: 135
currently lose_sum: 509.53757879137993
time_elpased: 1.592
batch start
#iterations: 136
currently lose_sum: 511.46466878056526
time_elpased: 1.595
batch start
#iterations: 137
currently lose_sum: 509.40493738651276
time_elpased: 1.595
batch start
#iterations: 138
currently lose_sum: 511.076792627573
time_elpased: 1.59
batch start
#iterations: 139
currently lose_sum: 510.4035353064537
time_elpased: 1.604
start validation test
0.549536082474
0.90015600624
0.118177163338
0.208925500136
0
validation finish
batch start
#iterations: 140
currently lose_sum: 509.4581209719181
time_elpased: 1.604
batch start
#iterations: 141
currently lose_sum: 508.1889184117317
time_elpased: 1.598
batch start
#iterations: 142
currently lose_sum: 509.9092502593994
time_elpased: 1.602
batch start
#iterations: 143
currently lose_sum: 509.2510726749897
time_elpased: 1.596
batch start
#iterations: 144
currently lose_sum: 510.1871862709522
time_elpased: 1.593
batch start
#iterations: 145
currently lose_sum: 511.09075677394867
time_elpased: 1.609
batch start
#iterations: 146
currently lose_sum: 509.6079640388489
time_elpased: 1.627
batch start
#iterations: 147
currently lose_sum: 508.7138517796993
time_elpased: 1.613
batch start
#iterations: 148
currently lose_sum: 508.8697608113289
time_elpased: 1.607
batch start
#iterations: 149
currently lose_sum: 511.0403838157654
time_elpased: 1.61
batch start
#iterations: 150
currently lose_sum: 509.6762617826462
time_elpased: 1.603
batch start
#iterations: 151
currently lose_sum: 508.87772619724274
time_elpased: 1.615
batch start
#iterations: 152
currently lose_sum: 508.6806834936142
time_elpased: 1.61
batch start
#iterations: 153
currently lose_sum: 507.75010272860527
time_elpased: 1.622
batch start
#iterations: 154
currently lose_sum: 511.1255717277527
time_elpased: 1.607
batch start
#iterations: 155
currently lose_sum: 511.3965911567211
time_elpased: 1.606
batch start
#iterations: 156
currently lose_sum: 510.0035453438759
time_elpased: 1.593
batch start
#iterations: 157
currently lose_sum: 509.17409616708755
time_elpased: 1.583
batch start
#iterations: 158
currently lose_sum: 509.0019297301769
time_elpased: 1.615
batch start
#iterations: 159
currently lose_sum: 508.57227024435997
time_elpased: 1.599
start validation test
0.534896907216
0.918735891648
0.0833589349718
0.1528494977
0
validation finish
batch start
#iterations: 160
currently lose_sum: 508.8094846010208
time_elpased: 1.599
batch start
#iterations: 161
currently lose_sum: 509.5334053635597
time_elpased: 1.602
batch start
#iterations: 162
currently lose_sum: 508.3346211910248
time_elpased: 1.596
batch start
#iterations: 163
currently lose_sum: 508.5890558063984
time_elpased: 1.605
batch start
#iterations: 164
currently lose_sum: 509.2655194103718
time_elpased: 1.597
batch start
#iterations: 165
currently lose_sum: 508.0805803835392
time_elpased: 1.6
batch start
#iterations: 166
currently lose_sum: 508.39126193523407
time_elpased: 1.623
batch start
#iterations: 167
currently lose_sum: 507.56280279159546
time_elpased: 1.589
batch start
#iterations: 168
currently lose_sum: 509.04170778393745
time_elpased: 1.619
batch start
#iterations: 169
currently lose_sum: 507.9926178753376
time_elpased: 1.624
batch start
#iterations: 170
currently lose_sum: 509.05484479665756
time_elpased: 1.588
batch start
#iterations: 171
currently lose_sum: 509.6969352066517
time_elpased: 1.603
batch start
#iterations: 172
currently lose_sum: 508.07949236035347
time_elpased: 1.591
batch start
#iterations: 173
currently lose_sum: 506.69841676950455
time_elpased: 1.6
batch start
#iterations: 174
currently lose_sum: 507.63848301768303
time_elpased: 1.608
batch start
#iterations: 175
currently lose_sum: 506.66555148363113
time_elpased: 1.585
batch start
#iterations: 176
currently lose_sum: 508.9131630063057
time_elpased: 1.602
batch start
#iterations: 177
currently lose_sum: 508.14482513070107
time_elpased: 1.585
batch start
#iterations: 178
currently lose_sum: 508.57208809256554
time_elpased: 1.605
batch start
#iterations: 179
currently lose_sum: 510.10672652721405
time_elpased: 1.604
start validation test
0.577164948454
0.888944223108
0.182795698925
0.303236218466
0
validation finish
batch start
#iterations: 180
currently lose_sum: 508.7519377171993
time_elpased: 1.596
batch start
#iterations: 181
currently lose_sum: 509.6735078692436
time_elpased: 1.609
batch start
#iterations: 182
currently lose_sum: 507.4570574462414
time_elpased: 1.612
batch start
#iterations: 183
currently lose_sum: 508.58396449685097
time_elpased: 1.594
batch start
#iterations: 184
currently lose_sum: 507.80521047115326
time_elpased: 1.615
batch start
#iterations: 185
currently lose_sum: 507.98314821720123
time_elpased: 1.609
batch start
#iterations: 186
currently lose_sum: 506.68589076399803
time_elpased: 1.605
batch start
#iterations: 187
currently lose_sum: 509.51535934209824
time_elpased: 1.598
batch start
#iterations: 188
currently lose_sum: 507.699152469635
time_elpased: 1.591
batch start
#iterations: 189
currently lose_sum: 507.80093425512314
time_elpased: 1.602
batch start
#iterations: 190
currently lose_sum: 509.3176743388176
time_elpased: 1.611
batch start
#iterations: 191
currently lose_sum: 508.0833785831928
time_elpased: 1.599
batch start
#iterations: 192
currently lose_sum: 506.681038826704
time_elpased: 1.595
batch start
#iterations: 193
currently lose_sum: 507.4877708554268
time_elpased: 1.6
batch start
#iterations: 194
currently lose_sum: 509.41991299390793
time_elpased: 1.597
batch start
#iterations: 195
currently lose_sum: 506.911945939064
time_elpased: 1.595
batch start
#iterations: 196
currently lose_sum: 508.359517455101
time_elpased: 1.607
batch start
#iterations: 197
currently lose_sum: 508.64458745718
time_elpased: 1.585
batch start
#iterations: 198
currently lose_sum: 506.66121432185173
time_elpased: 1.604
batch start
#iterations: 199
currently lose_sum: 509.3597777187824
time_elpased: 1.609
start validation test
0.568608247423
0.902073732719
0.160368663594
0.272324145726
0
validation finish
batch start
#iterations: 200
currently lose_sum: 503.5532940030098
time_elpased: 1.61
batch start
#iterations: 201
currently lose_sum: 507.45045843720436
time_elpased: 1.596
batch start
#iterations: 202
currently lose_sum: 504.7210969030857
time_elpased: 1.609
batch start
#iterations: 203
currently lose_sum: 507.5067608356476
time_elpased: 1.603
batch start
#iterations: 204
currently lose_sum: 506.8778305053711
time_elpased: 1.592
batch start
#iterations: 205
currently lose_sum: 507.0841679275036
time_elpased: 1.602
batch start
#iterations: 206
currently lose_sum: 505.378294557333
time_elpased: 1.61
batch start
#iterations: 207
currently lose_sum: 507.3525990843773
time_elpased: 1.615
batch start
#iterations: 208
currently lose_sum: 506.94796255230904
time_elpased: 1.606
batch start
#iterations: 209
currently lose_sum: 507.8711576163769
time_elpased: 1.592
batch start
#iterations: 210
currently lose_sum: 506.9790903329849
time_elpased: 1.599
batch start
#iterations: 211
currently lose_sum: 506.2421986758709
time_elpased: 1.605
batch start
#iterations: 212
currently lose_sum: 507.24880224466324
time_elpased: 1.594
batch start
#iterations: 213
currently lose_sum: 508.9055317938328
time_elpased: 1.607
batch start
#iterations: 214
currently lose_sum: 508.91403514146805
time_elpased: 1.614
batch start
#iterations: 215
currently lose_sum: 506.09335282444954
time_elpased: 1.608
batch start
#iterations: 216
currently lose_sum: 507.4169120192528
time_elpased: 1.604
batch start
#iterations: 217
currently lose_sum: 505.6991950273514
time_elpased: 1.605
batch start
#iterations: 218
currently lose_sum: 508.64350286126137
time_elpased: 1.602
batch start
#iterations: 219
currently lose_sum: 507.20847672224045
time_elpased: 1.6
start validation test
0.512886597938
0.906976744186
0.0359447004608
0.0691489361702
0
validation finish
batch start
#iterations: 220
currently lose_sum: 507.46186113357544
time_elpased: 1.611
batch start
#iterations: 221
currently lose_sum: 509.238604336977
time_elpased: 1.601
batch start
#iterations: 222
currently lose_sum: 508.2588964700699
time_elpased: 1.605
batch start
#iterations: 223
currently lose_sum: 503.71588441729546
time_elpased: 1.6
batch start
#iterations: 224
currently lose_sum: 508.42068991065025
time_elpased: 1.614
batch start
#iterations: 225
currently lose_sum: 506.8983411192894
time_elpased: 1.6
batch start
#iterations: 226
currently lose_sum: 506.7395187020302
time_elpased: 1.605
batch start
#iterations: 227
currently lose_sum: 505.1386757493019
time_elpased: 1.605
batch start
#iterations: 228
currently lose_sum: 505.47301745414734
time_elpased: 1.605
batch start
#iterations: 229
currently lose_sum: 506.2564332485199
time_elpased: 1.594
batch start
#iterations: 230
currently lose_sum: 506.4491171836853
time_elpased: 1.61
batch start
#iterations: 231
currently lose_sum: 510.2734931409359
time_elpased: 1.604
batch start
#iterations: 232
currently lose_sum: 506.88552871346474
time_elpased: 1.612
batch start
#iterations: 233
currently lose_sum: 506.93001973629
time_elpased: 1.604
batch start
#iterations: 234
currently lose_sum: 505.80888697505
time_elpased: 1.602
batch start
#iterations: 235
currently lose_sum: 507.05662247538567
time_elpased: 1.595
batch start
#iterations: 236
currently lose_sum: 505.17311254143715
time_elpased: 1.617
batch start
#iterations: 237
currently lose_sum: 504.5261323750019
time_elpased: 1.588
batch start
#iterations: 238
currently lose_sum: 505.3992650806904
time_elpased: 1.605
batch start
#iterations: 239
currently lose_sum: 510.01754945516586
time_elpased: 1.597
start validation test
0.604845360825
0.883729433272
0.247516641065
0.38672
0
validation finish
batch start
#iterations: 240
currently lose_sum: 507.4914445281029
time_elpased: 1.615
batch start
#iterations: 241
currently lose_sum: 506.2902131676674
time_elpased: 1.596
batch start
#iterations: 242
currently lose_sum: 507.4627189338207
time_elpased: 1.598
batch start
#iterations: 243
currently lose_sum: 506.618172287941
time_elpased: 1.598
batch start
#iterations: 244
currently lose_sum: 509.4518294632435
time_elpased: 1.606
batch start
#iterations: 245
currently lose_sum: 505.84869363904
time_elpased: 1.599
batch start
#iterations: 246
currently lose_sum: 506.65532153844833
time_elpased: 1.632
batch start
#iterations: 247
currently lose_sum: 507.2658880650997
time_elpased: 1.613
batch start
#iterations: 248
currently lose_sum: 504.1602286994457
time_elpased: 1.602
batch start
#iterations: 249
currently lose_sum: 507.25620597600937
time_elpased: 1.605
batch start
#iterations: 250
currently lose_sum: 506.8253980278969
time_elpased: 1.616
batch start
#iterations: 251
currently lose_sum: 504.2369993031025
time_elpased: 1.602
batch start
#iterations: 252
currently lose_sum: 508.2094707787037
time_elpased: 1.601
batch start
#iterations: 253
currently lose_sum: 505.52919098734856
time_elpased: 1.612
batch start
#iterations: 254
currently lose_sum: 508.2570858001709
time_elpased: 1.599
batch start
#iterations: 255
currently lose_sum: 506.2637256383896
time_elpased: 1.603
batch start
#iterations: 256
currently lose_sum: 506.41953057050705
time_elpased: 1.612
batch start
#iterations: 257
currently lose_sum: 503.92546075582504
time_elpased: 1.609
batch start
#iterations: 258
currently lose_sum: 504.34430596232414
time_elpased: 1.605
batch start
#iterations: 259
currently lose_sum: 506.668867200613
time_elpased: 1.593
start validation test
0.579690721649
0.897777777778
0.186175115207
0.308396946565
0
validation finish
batch start
#iterations: 260
currently lose_sum: 506.3840634226799
time_elpased: 1.615
batch start
#iterations: 261
currently lose_sum: 505.5871495306492
time_elpased: 1.592
batch start
#iterations: 262
currently lose_sum: 507.0609256327152
time_elpased: 1.612
batch start
#iterations: 263
currently lose_sum: 506.2635023891926
time_elpased: 1.607
batch start
#iterations: 264
currently lose_sum: 506.7226228415966
time_elpased: 1.605
batch start
#iterations: 265
currently lose_sum: 506.52712562680244
time_elpased: 1.594
batch start
#iterations: 266
currently lose_sum: 506.2460391819477
time_elpased: 1.595
batch start
#iterations: 267
currently lose_sum: 505.978137999773
time_elpased: 1.604
batch start
#iterations: 268
currently lose_sum: 503.99237355589867
time_elpased: 1.602
batch start
#iterations: 269
currently lose_sum: 505.0499211847782
time_elpased: 1.599
batch start
#iterations: 270
currently lose_sum: 507.47663831710815
time_elpased: 1.607
batch start
#iterations: 271
currently lose_sum: 507.7772854566574
time_elpased: 1.605
batch start
#iterations: 272
currently lose_sum: 505.96188485622406
time_elpased: 1.6
batch start
#iterations: 273
currently lose_sum: 506.82281601428986
time_elpased: 1.598
batch start
#iterations: 274
currently lose_sum: 506.1395008265972
time_elpased: 1.611
batch start
#iterations: 275
currently lose_sum: 505.1257019340992
time_elpased: 1.606
batch start
#iterations: 276
currently lose_sum: 506.9298859834671
time_elpased: 1.613
batch start
#iterations: 277
currently lose_sum: 505.1699505150318
time_elpased: 1.605
batch start
#iterations: 278
currently lose_sum: 503.61815360188484
time_elpased: 1.642
batch start
#iterations: 279
currently lose_sum: 504.33798736333847
time_elpased: 1.605
start validation test
0.600773195876
0.8905645785
0.235842293907
0.372925269209
0
validation finish
batch start
#iterations: 280
currently lose_sum: 507.2614296376705
time_elpased: 1.622
batch start
#iterations: 281
currently lose_sum: 505.1374812722206
time_elpased: 1.597
batch start
#iterations: 282
currently lose_sum: 507.0953788161278
time_elpased: 1.601
batch start
#iterations: 283
currently lose_sum: 504.51443496346474
time_elpased: 1.628
batch start
#iterations: 284
currently lose_sum: 504.52731770277023
time_elpased: 1.599
batch start
#iterations: 285
currently lose_sum: 502.8041090667248
time_elpased: 1.623
batch start
#iterations: 286
currently lose_sum: 505.48286604881287
time_elpased: 1.594
batch start
#iterations: 287
currently lose_sum: 506.33428248763084
time_elpased: 1.608
batch start
#iterations: 288
currently lose_sum: 507.3219020664692
time_elpased: 1.595
batch start
#iterations: 289
currently lose_sum: 506.4238865673542
time_elpased: 1.603
batch start
#iterations: 290
currently lose_sum: 504.9578769803047
time_elpased: 1.593
batch start
#iterations: 291
currently lose_sum: 503.70456126332283
time_elpased: 1.616
batch start
#iterations: 292
currently lose_sum: 504.4509229063988
time_elpased: 1.613
batch start
#iterations: 293
currently lose_sum: 506.28753954172134
time_elpased: 1.614
batch start
#iterations: 294
currently lose_sum: 504.7749719619751
time_elpased: 1.588
batch start
#iterations: 295
currently lose_sum: 504.4232848286629
time_elpased: 1.606
batch start
#iterations: 296
currently lose_sum: 505.47904539108276
time_elpased: 1.6
batch start
#iterations: 297
currently lose_sum: 502.86834955215454
time_elpased: 1.604
batch start
#iterations: 298
currently lose_sum: 505.4043489098549
time_elpased: 1.614
batch start
#iterations: 299
currently lose_sum: 506.7074725627899
time_elpased: 1.62
start validation test
0.579329896907
0.891984359726
0.186891961086
0.309033951401
0
validation finish
batch start
#iterations: 300
currently lose_sum: 505.3915451467037
time_elpased: 1.604
batch start
#iterations: 301
currently lose_sum: 504.0507205724716
time_elpased: 1.592
batch start
#iterations: 302
currently lose_sum: 506.00176653265953
time_elpased: 1.603
batch start
#iterations: 303
currently lose_sum: 505.1735292971134
time_elpased: 1.596
batch start
#iterations: 304
currently lose_sum: 503.975619494915
time_elpased: 1.597
batch start
#iterations: 305
currently lose_sum: 505.5246350765228
time_elpased: 1.595
batch start
#iterations: 306
currently lose_sum: 505.80251783132553
time_elpased: 1.599
batch start
#iterations: 307
currently lose_sum: 503.7119170129299
time_elpased: 1.609
batch start
#iterations: 308
currently lose_sum: 505.9559125006199
time_elpased: 1.598
batch start
#iterations: 309
currently lose_sum: 505.2235345840454
time_elpased: 1.594
batch start
#iterations: 310
currently lose_sum: 506.5716528892517
time_elpased: 1.601
batch start
#iterations: 311
currently lose_sum: 503.89021587371826
time_elpased: 1.588
batch start
#iterations: 312
currently lose_sum: 503.78830471634865
time_elpased: 1.609
batch start
#iterations: 313
currently lose_sum: 503.9920896291733
time_elpased: 1.612
batch start
#iterations: 314
currently lose_sum: 505.94508627057076
time_elpased: 1.6
batch start
#iterations: 315
currently lose_sum: 504.74467903375626
time_elpased: 1.611
batch start
#iterations: 316
currently lose_sum: 504.0324117541313
time_elpased: 1.606
batch start
#iterations: 317
currently lose_sum: 505.2817409336567
time_elpased: 1.595
batch start
#iterations: 318
currently lose_sum: 503.3153811991215
time_elpased: 1.588
batch start
#iterations: 319
currently lose_sum: 504.92302510142326
time_elpased: 1.596
start validation test
0.546340206186
0.906408094435
0.110087045571
0.196329102365
0
validation finish
batch start
#iterations: 320
currently lose_sum: 507.8842344582081
time_elpased: 1.619
batch start
#iterations: 321
currently lose_sum: 504.6193514764309
time_elpased: 1.597
batch start
#iterations: 322
currently lose_sum: 502.6119915544987
time_elpased: 1.605
batch start
#iterations: 323
currently lose_sum: 504.79926213622093
time_elpased: 1.6
batch start
#iterations: 324
currently lose_sum: 504.12239572405815
time_elpased: 1.602
batch start
#iterations: 325
currently lose_sum: 504.8333540260792
time_elpased: 1.603
batch start
#iterations: 326
currently lose_sum: 505.0767390727997
time_elpased: 1.59
batch start
#iterations: 327
currently lose_sum: 504.97940772771835
time_elpased: 1.612
batch start
#iterations: 328
currently lose_sum: 504.7829946279526
time_elpased: 1.599
batch start
#iterations: 329
currently lose_sum: 504.4235529899597
time_elpased: 1.61
batch start
#iterations: 330
currently lose_sum: 505.3897500038147
time_elpased: 1.612
batch start
#iterations: 331
currently lose_sum: 505.45754688978195
time_elpased: 1.586
batch start
#iterations: 332
currently lose_sum: 505.5129562318325
time_elpased: 1.597
batch start
#iterations: 333
currently lose_sum: 503.30897986888885
time_elpased: 1.618
batch start
#iterations: 334
currently lose_sum: 503.01404601335526
time_elpased: 1.607
batch start
#iterations: 335
currently lose_sum: 503.90569376945496
time_elpased: 1.609
batch start
#iterations: 336
currently lose_sum: 504.8319534957409
time_elpased: 1.598
batch start
#iterations: 337
currently lose_sum: 505.04847753047943
time_elpased: 1.603
batch start
#iterations: 338
currently lose_sum: 504.69720807671547
time_elpased: 1.596
batch start
#iterations: 339
currently lose_sum: 506.401030421257
time_elpased: 1.596
start validation test
0.651546391753
0.875343492381
0.358832565284
0.509006391633
0
validation finish
batch start
#iterations: 340
currently lose_sum: 506.1467694044113
time_elpased: 1.624
batch start
#iterations: 341
currently lose_sum: 503.29533940553665
time_elpased: 1.62
batch start
#iterations: 342
currently lose_sum: 503.85813587903976
time_elpased: 1.602
batch start
#iterations: 343
currently lose_sum: 505.4502766132355
time_elpased: 1.593
batch start
#iterations: 344
currently lose_sum: 506.50657987594604
time_elpased: 1.597
batch start
#iterations: 345
currently lose_sum: 507.20573168992996
time_elpased: 1.614
batch start
#iterations: 346
currently lose_sum: 502.2882996201515
time_elpased: 1.61
batch start
#iterations: 347
currently lose_sum: 502.90192940831184
time_elpased: 1.601
batch start
#iterations: 348
currently lose_sum: 504.36613833904266
time_elpased: 1.607
batch start
#iterations: 349
currently lose_sum: 504.7460628449917
time_elpased: 1.608
batch start
#iterations: 350
currently lose_sum: 504.8832753896713
time_elpased: 1.593
batch start
#iterations: 351
currently lose_sum: 505.41840821504593
time_elpased: 1.609
batch start
#iterations: 352
currently lose_sum: 505.26500684022903
time_elpased: 1.601
batch start
#iterations: 353
currently lose_sum: 504.1404922902584
time_elpased: 1.605
batch start
#iterations: 354
currently lose_sum: 507.3647484779358
time_elpased: 1.602
batch start
#iterations: 355
currently lose_sum: 506.2128578424454
time_elpased: 1.603
batch start
#iterations: 356
currently lose_sum: 503.83198940753937
time_elpased: 1.612
batch start
#iterations: 357
currently lose_sum: 504.48266381025314
time_elpased: 1.614
batch start
#iterations: 358
currently lose_sum: 503.71583849191666
time_elpased: 1.606
batch start
#iterations: 359
currently lose_sum: 504.7062953710556
time_elpased: 1.599
start validation test
0.646649484536
0.879697286013
0.3452124936
0.495844671619
0
validation finish
batch start
#iterations: 360
currently lose_sum: 505.28207927942276
time_elpased: 1.613
batch start
#iterations: 361
currently lose_sum: 506.03661131858826
time_elpased: 1.598
batch start
#iterations: 362
currently lose_sum: 504.6331585943699
time_elpased: 1.599
batch start
#iterations: 363
currently lose_sum: 503.01016241312027
time_elpased: 1.607
batch start
#iterations: 364
currently lose_sum: 503.33735224604607
time_elpased: 1.597
batch start
#iterations: 365
currently lose_sum: 504.09900349378586
time_elpased: 1.608
batch start
#iterations: 366
currently lose_sum: 505.3351110816002
time_elpased: 1.608
batch start
#iterations: 367
currently lose_sum: 507.42179611325264
time_elpased: 1.609
batch start
#iterations: 368
currently lose_sum: 503.41884791851044
time_elpased: 1.599
batch start
#iterations: 369
currently lose_sum: 504.6963899731636
time_elpased: 1.611
batch start
#iterations: 370
currently lose_sum: 505.9074258208275
time_elpased: 1.619
batch start
#iterations: 371
currently lose_sum: 501.93703761696815
time_elpased: 1.604
batch start
#iterations: 372
currently lose_sum: 505.64119505882263
time_elpased: 1.61
batch start
#iterations: 373
currently lose_sum: 506.6644121706486
time_elpased: 1.605
batch start
#iterations: 374
currently lose_sum: 503.21618512272835
time_elpased: 1.603
batch start
#iterations: 375
currently lose_sum: 504.59026914834976
time_elpased: 1.602
batch start
#iterations: 376
currently lose_sum: 503.9958874285221
time_elpased: 1.606
batch start
#iterations: 377
currently lose_sum: 503.3214281499386
time_elpased: 1.602
batch start
#iterations: 378
currently lose_sum: 505.65846452116966
time_elpased: 1.594
batch start
#iterations: 379
currently lose_sum: 502.00526547431946
time_elpased: 1.602
start validation test
0.634896907216
0.883800801374
0.316231438812
0.465796817256
0
validation finish
batch start
#iterations: 380
currently lose_sum: 503.5004600584507
time_elpased: 1.611
batch start
#iterations: 381
currently lose_sum: 501.93193128705025
time_elpased: 1.603
batch start
#iterations: 382
currently lose_sum: 501.0587174296379
time_elpased: 1.595
batch start
#iterations: 383
currently lose_sum: 502.9421496987343
time_elpased: 1.606
batch start
#iterations: 384
currently lose_sum: 505.3943824470043
time_elpased: 1.611
batch start
#iterations: 385
currently lose_sum: 505.1758861243725
time_elpased: 1.605
batch start
#iterations: 386
currently lose_sum: 506.6917581856251
time_elpased: 1.601
batch start
#iterations: 387
currently lose_sum: 503.60645535588264
time_elpased: 1.624
batch start
#iterations: 388
currently lose_sum: 503.7012938261032
time_elpased: 1.615
batch start
#iterations: 389
currently lose_sum: 505.1711210310459
time_elpased: 1.618
batch start
#iterations: 390
currently lose_sum: 503.18387508392334
time_elpased: 1.6
batch start
#iterations: 391
currently lose_sum: 501.856400847435
time_elpased: 1.599
batch start
#iterations: 392
currently lose_sum: 504.0096455812454
time_elpased: 1.596
batch start
#iterations: 393
currently lose_sum: 506.4000742137432
time_elpased: 1.625
batch start
#iterations: 394
currently lose_sum: 503.2627082467079
time_elpased: 1.603
batch start
#iterations: 395
currently lose_sum: 504.28192630410194
time_elpased: 1.61
batch start
#iterations: 396
currently lose_sum: 502.86256846785545
time_elpased: 1.609
batch start
#iterations: 397
currently lose_sum: 504.36916667222977
time_elpased: 1.611
batch start
#iterations: 398
currently lose_sum: 503.2287651896477
time_elpased: 1.604
batch start
#iterations: 399
currently lose_sum: 504.54119125008583
time_elpased: 1.616
start validation test
0.540721649485
0.898415657036
0.0987199180748
0.177892600111
0
validation finish
acc: 0.666
pre: 0.878
rec: 0.384
F1: 0.534
auc: 0.000
