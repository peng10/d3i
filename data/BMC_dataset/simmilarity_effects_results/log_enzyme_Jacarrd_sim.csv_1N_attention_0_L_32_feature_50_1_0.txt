start to construct graph
graph construct over
58300
epochs start
batch start
#iterations: 0
currently lose_sum: 401.7306618094444
time_elpased: 1.423
batch start
#iterations: 1
currently lose_sum: 396.5456243157387
time_elpased: 1.424
batch start
#iterations: 2
currently lose_sum: 394.54753106832504
time_elpased: 1.431
batch start
#iterations: 3
currently lose_sum: 392.18285113573074
time_elpased: 1.428
batch start
#iterations: 4
currently lose_sum: 391.3265842795372
time_elpased: 1.422
batch start
#iterations: 5
currently lose_sum: 390.8051221370697
time_elpased: 1.433
batch start
#iterations: 6
currently lose_sum: 389.6347344517708
time_elpased: 1.419
batch start
#iterations: 7
currently lose_sum: 388.3151361346245
time_elpased: 1.436
batch start
#iterations: 8
currently lose_sum: 389.4766568541527
time_elpased: 1.427
batch start
#iterations: 9
currently lose_sum: 388.3211936354637
time_elpased: 1.429
batch start
#iterations: 10
currently lose_sum: 387.4727176427841
time_elpased: 1.437
batch start
#iterations: 11
currently lose_sum: 387.4339390397072
time_elpased: 1.421
batch start
#iterations: 12
currently lose_sum: 386.9478898048401
time_elpased: 1.428
batch start
#iterations: 13
currently lose_sum: 387.0773301124573
time_elpased: 1.424
batch start
#iterations: 14
currently lose_sum: 386.0562554001808
time_elpased: 1.426
batch start
#iterations: 15
currently lose_sum: 386.1690857410431
time_elpased: 1.424
batch start
#iterations: 16
currently lose_sum: 387.4182786345482
time_elpased: 1.433
batch start
#iterations: 17
currently lose_sum: 386.5587341785431
time_elpased: 1.412
batch start
#iterations: 18
currently lose_sum: 385.31463545560837
time_elpased: 1.422
batch start
#iterations: 19
currently lose_sum: 385.4002903699875
time_elpased: 1.424
start validation test
0.698350515464
0.822728056561
0.497238144867
0.619851890347
0
validation finish
batch start
#iterations: 20
currently lose_sum: 386.0640987753868
time_elpased: 1.405
batch start
#iterations: 21
currently lose_sum: 385.7873337864876
time_elpased: 1.433
batch start
#iterations: 22
currently lose_sum: 386.20764541625977
time_elpased: 1.425
batch start
#iterations: 23
currently lose_sum: 385.6903290748596
time_elpased: 1.434
batch start
#iterations: 24
currently lose_sum: 385.6439592242241
time_elpased: 1.425
batch start
#iterations: 25
currently lose_sum: 385.1976362466812
time_elpased: 1.425
batch start
#iterations: 26
currently lose_sum: 384.4376990199089
time_elpased: 1.432
batch start
#iterations: 27
currently lose_sum: 384.90455973148346
time_elpased: 1.437
batch start
#iterations: 28
currently lose_sum: 384.23706942796707
time_elpased: 1.428
batch start
#iterations: 29
currently lose_sum: 384.84626454114914
time_elpased: 1.443
batch start
#iterations: 30
currently lose_sum: 386.1169062256813
time_elpased: 1.413
batch start
#iterations: 31
currently lose_sum: 385.722447514534
time_elpased: 1.418
batch start
#iterations: 32
currently lose_sum: 384.18965870141983
time_elpased: 1.433
batch start
#iterations: 33
currently lose_sum: 385.44293385744095
time_elpased: 1.409
batch start
#iterations: 34
currently lose_sum: 384.48683685064316
time_elpased: 1.415
batch start
#iterations: 35
currently lose_sum: 384.64538180828094
time_elpased: 1.426
batch start
#iterations: 36
currently lose_sum: 384.8381897807121
time_elpased: 1.413
batch start
#iterations: 37
currently lose_sum: 385.17446398735046
time_elpased: 1.42
batch start
#iterations: 38
currently lose_sum: 385.7115872502327
time_elpased: 1.423
batch start
#iterations: 39
currently lose_sum: 384.8960322737694
time_elpased: 1.413
start validation test
0.697319587629
0.836071493049
0.48264721209
0.611999471389
0
validation finish
batch start
#iterations: 40
currently lose_sum: 385.4326092004776
time_elpased: 1.425
batch start
#iterations: 41
currently lose_sum: 384.98196905851364
time_elpased: 1.416
batch start
#iterations: 42
currently lose_sum: 383.9158248901367
time_elpased: 1.419
batch start
#iterations: 43
currently lose_sum: 384.56980180740356
time_elpased: 1.439
batch start
#iterations: 44
currently lose_sum: 385.2712745666504
time_elpased: 1.417
batch start
#iterations: 45
currently lose_sum: 385.14610373973846
time_elpased: 1.421
batch start
#iterations: 46
currently lose_sum: 382.67571127414703
time_elpased: 1.424
batch start
#iterations: 47
currently lose_sum: 385.01838773489
time_elpased: 1.419
batch start
#iterations: 48
currently lose_sum: 384.0461146235466
time_elpased: 1.427
batch start
#iterations: 49
currently lose_sum: 385.1211858987808
time_elpased: 1.428
batch start
#iterations: 50
currently lose_sum: 383.3300873041153
time_elpased: 1.418
batch start
#iterations: 51
currently lose_sum: 383.6810262799263
time_elpased: 1.435
batch start
#iterations: 52
currently lose_sum: 384.54951173067093
time_elpased: 1.416
batch start
#iterations: 53
currently lose_sum: 384.7304226756096
time_elpased: 1.417
batch start
#iterations: 54
currently lose_sum: 384.2863567471504
time_elpased: 1.421
batch start
#iterations: 55
currently lose_sum: 384.67441886663437
time_elpased: 1.404
batch start
#iterations: 56
currently lose_sum: 384.6097095608711
time_elpased: 1.411
batch start
#iterations: 57
currently lose_sum: 385.2205290198326
time_elpased: 1.428
batch start
#iterations: 58
currently lose_sum: 385.1126919388771
time_elpased: 1.424
batch start
#iterations: 59
currently lose_sum: 384.0444289445877
time_elpased: 1.414
start validation test
0.720927835052
0.81393602643
0.564877540386
0.666912759936
0
validation finish
batch start
#iterations: 60
currently lose_sum: 385.3602514863014
time_elpased: 1.413
batch start
#iterations: 61
currently lose_sum: 383.6666125655174
time_elpased: 1.417
batch start
#iterations: 62
currently lose_sum: 384.397740483284
time_elpased: 1.421
batch start
#iterations: 63
currently lose_sum: 385.1544410586357
time_elpased: 1.424
batch start
#iterations: 64
currently lose_sum: 384.2398073077202
time_elpased: 1.419
batch start
#iterations: 65
currently lose_sum: 383.90994691848755
time_elpased: 1.421
batch start
#iterations: 66
currently lose_sum: 385.06439876556396
time_elpased: 1.426
batch start
#iterations: 67
currently lose_sum: 385.12466126680374
time_elpased: 1.415
batch start
#iterations: 68
currently lose_sum: 384.0575122833252
time_elpased: 1.424
batch start
#iterations: 69
currently lose_sum: 383.7878233194351
time_elpased: 1.416
batch start
#iterations: 70
currently lose_sum: 383.70883202552795
time_elpased: 1.417
batch start
#iterations: 71
currently lose_sum: 383.0805305838585
time_elpased: 1.431
batch start
#iterations: 72
currently lose_sum: 384.4802158474922
time_elpased: 1.421
batch start
#iterations: 73
currently lose_sum: 384.832622051239
time_elpased: 1.41
batch start
#iterations: 74
currently lose_sum: 384.6993414759636
time_elpased: 1.42
batch start
#iterations: 75
currently lose_sum: 383.4778259396553
time_elpased: 1.422
batch start
#iterations: 76
currently lose_sum: 383.14045786857605
time_elpased: 1.431
batch start
#iterations: 77
currently lose_sum: 384.20227617025375
time_elpased: 1.418
batch start
#iterations: 78
currently lose_sum: 383.2888860106468
time_elpased: 1.422
batch start
#iterations: 79
currently lose_sum: 384.34155613183975
time_elpased: 1.418
start validation test
0.734742268041
0.795693207497
0.623866597186
0.699380768781
0
validation finish
batch start
#iterations: 80
currently lose_sum: 384.14346319437027
time_elpased: 1.419
batch start
#iterations: 81
currently lose_sum: 385.1995111107826
time_elpased: 1.416
batch start
#iterations: 82
currently lose_sum: 384.0808140039444
time_elpased: 1.431
batch start
#iterations: 83
currently lose_sum: 384.06379133462906
time_elpased: 1.421
batch start
#iterations: 84
currently lose_sum: 383.3115801811218
time_elpased: 1.418
batch start
#iterations: 85
currently lose_sum: 383.53107738494873
time_elpased: 1.41
batch start
#iterations: 86
currently lose_sum: 384.02507269382477
time_elpased: 1.409
batch start
#iterations: 87
currently lose_sum: 383.3985911011696
time_elpased: 1.421
batch start
#iterations: 88
currently lose_sum: 383.8486756682396
time_elpased: 1.42
batch start
#iterations: 89
currently lose_sum: 383.42433220148087
time_elpased: 1.415
batch start
#iterations: 90
currently lose_sum: 383.2433875799179
time_elpased: 1.401
batch start
#iterations: 91
currently lose_sum: 383.37821489572525
time_elpased: 1.408
batch start
#iterations: 92
currently lose_sum: 384.36596435308456
time_elpased: 1.418
batch start
#iterations: 93
currently lose_sum: 384.3606191277504
time_elpased: 1.416
batch start
#iterations: 94
currently lose_sum: 383.65514063835144
time_elpased: 1.413
batch start
#iterations: 95
currently lose_sum: 384.329597055912
time_elpased: 1.412
batch start
#iterations: 96
currently lose_sum: 383.74686056375504
time_elpased: 1.436
batch start
#iterations: 97
currently lose_sum: 383.6016515493393
time_elpased: 1.419
batch start
#iterations: 98
currently lose_sum: 383.86569982767105
time_elpased: 1.418
batch start
#iterations: 99
currently lose_sum: 384.18720495700836
time_elpased: 1.43
start validation test
0.73412371134
0.782503501846
0.640437727983
0.704378725355
0
validation finish
batch start
#iterations: 100
currently lose_sum: 384.00993221998215
time_elpased: 1.411
batch start
#iterations: 101
currently lose_sum: 383.51389914751053
time_elpased: 1.416
batch start
#iterations: 102
currently lose_sum: 383.2141826748848
time_elpased: 1.419
batch start
#iterations: 103
currently lose_sum: 384.13681161403656
time_elpased: 1.422
batch start
#iterations: 104
currently lose_sum: 383.0599786043167
time_elpased: 1.43
batch start
#iterations: 105
currently lose_sum: 382.97302609682083
time_elpased: 1.427
batch start
#iterations: 106
currently lose_sum: 385.1702762246132
time_elpased: 1.418
batch start
#iterations: 107
currently lose_sum: 384.1474789381027
time_elpased: 1.439
batch start
#iterations: 108
currently lose_sum: 384.0225168466568
time_elpased: 1.406
batch start
#iterations: 109
currently lose_sum: 383.2105704545975
time_elpased: 1.417
batch start
#iterations: 110
currently lose_sum: 382.9549600481987
time_elpased: 1.442
batch start
#iterations: 111
currently lose_sum: 382.9481259584427
time_elpased: 1.423
batch start
#iterations: 112
currently lose_sum: 383.4877880215645
time_elpased: 1.423
batch start
#iterations: 113
currently lose_sum: 384.7645763158798
time_elpased: 1.424
batch start
#iterations: 114
currently lose_sum: 385.3683307170868
time_elpased: 1.429
batch start
#iterations: 115
currently lose_sum: 384.0710458159447
time_elpased: 1.422
batch start
#iterations: 116
currently lose_sum: 383.7748199701309
time_elpased: 1.426
batch start
#iterations: 117
currently lose_sum: 383.7117057442665
time_elpased: 1.431
batch start
#iterations: 118
currently lose_sum: 383.822489798069
time_elpased: 1.425
batch start
#iterations: 119
currently lose_sum: 384.3389371037483
time_elpased: 1.414
start validation test
0.725670103093
0.810582933566
0.581136008338
0.676945489863
0
validation finish
batch start
#iterations: 120
currently lose_sum: 383.3622627258301
time_elpased: 1.413
batch start
#iterations: 121
currently lose_sum: 385.2442224621773
time_elpased: 1.422
batch start
#iterations: 122
currently lose_sum: 383.3584496974945
time_elpased: 1.421
batch start
#iterations: 123
currently lose_sum: 383.92692971229553
time_elpased: 1.422
batch start
#iterations: 124
currently lose_sum: 383.7129943370819
time_elpased: 1.419
batch start
#iterations: 125
currently lose_sum: 383.92226189374924
time_elpased: 1.408
batch start
#iterations: 126
currently lose_sum: 384.1837447285652
time_elpased: 1.412
batch start
#iterations: 127
currently lose_sum: 383.83361852169037
time_elpased: 1.426
batch start
#iterations: 128
currently lose_sum: 383.66260462999344
time_elpased: 1.413
batch start
#iterations: 129
currently lose_sum: 383.77270007133484
time_elpased: 1.406
batch start
#iterations: 130
currently lose_sum: 384.3027799129486
time_elpased: 1.41
batch start
#iterations: 131
currently lose_sum: 383.20444440841675
time_elpased: 1.427
batch start
#iterations: 132
currently lose_sum: 384.1067662835121
time_elpased: 1.426
batch start
#iterations: 133
currently lose_sum: 383.52356791496277
time_elpased: 1.422
batch start
#iterations: 134
currently lose_sum: 383.51604652404785
time_elpased: 1.41
batch start
#iterations: 135
currently lose_sum: 383.9722526669502
time_elpased: 1.422
batch start
#iterations: 136
currently lose_sum: 384.0854285955429
time_elpased: 1.41
batch start
#iterations: 137
currently lose_sum: 383.5710926055908
time_elpased: 1.413
batch start
#iterations: 138
currently lose_sum: 383.50992423295975
time_elpased: 1.426
batch start
#iterations: 139
currently lose_sum: 383.4861489534378
time_elpased: 1.42
start validation test
0.73412371134
0.802701596398
0.6131318395
0.695225714961
0
validation finish
batch start
#iterations: 140
currently lose_sum: 383.10699075460434
time_elpased: 1.414
batch start
#iterations: 141
currently lose_sum: 383.0345684289932
time_elpased: 1.418
batch start
#iterations: 142
currently lose_sum: 384.20121401548386
time_elpased: 1.419
batch start
#iterations: 143
currently lose_sum: 383.9794369339943
time_elpased: 1.411
batch start
#iterations: 144
currently lose_sum: 383.7277148962021
time_elpased: 1.417
batch start
#iterations: 145
currently lose_sum: 383.61787486076355
time_elpased: 1.424
batch start
#iterations: 146
currently lose_sum: 382.2785801887512
time_elpased: 1.434
batch start
#iterations: 147
currently lose_sum: 383.17506992816925
time_elpased: 1.423
batch start
#iterations: 148
currently lose_sum: 383.9046331644058
time_elpased: 1.414
batch start
#iterations: 149
currently lose_sum: 382.9018794298172
time_elpased: 1.445
batch start
#iterations: 150
currently lose_sum: 383.6243265867233
time_elpased: 1.416
batch start
#iterations: 151
currently lose_sum: 383.2906566262245
time_elpased: 1.418
batch start
#iterations: 152
currently lose_sum: 384.55815356969833
time_elpased: 1.431
batch start
#iterations: 153
currently lose_sum: 383.8317169547081
time_elpased: 1.417
batch start
#iterations: 154
currently lose_sum: 383.04494935274124
time_elpased: 1.425
batch start
#iterations: 155
currently lose_sum: 383.5184042453766
time_elpased: 1.421
batch start
#iterations: 156
currently lose_sum: 383.1100042462349
time_elpased: 1.415
batch start
#iterations: 157
currently lose_sum: 383.1099463701248
time_elpased: 1.413
batch start
#iterations: 158
currently lose_sum: 382.39621728658676
time_elpased: 1.411
batch start
#iterations: 159
currently lose_sum: 383.0923752784729
time_elpased: 1.406
start validation test
0.739639175258
0.788984991096
0.646482542991
0.710660480037
0
validation finish
batch start
#iterations: 160
currently lose_sum: 383.75129783153534
time_elpased: 1.415
batch start
#iterations: 161
currently lose_sum: 383.93708288669586
time_elpased: 1.404
batch start
#iterations: 162
currently lose_sum: 382.50468224287033
time_elpased: 1.404
batch start
#iterations: 163
currently lose_sum: 383.6265422105789
time_elpased: 1.41
batch start
#iterations: 164
currently lose_sum: 382.3247577548027
time_elpased: 1.404
batch start
#iterations: 165
currently lose_sum: 383.83175653219223
time_elpased: 1.417
batch start
#iterations: 166
currently lose_sum: 384.2177795767784
time_elpased: 1.412
batch start
#iterations: 167
currently lose_sum: 382.9857454299927
time_elpased: 1.414
batch start
#iterations: 168
currently lose_sum: 382.3237074017525
time_elpased: 1.423
batch start
#iterations: 169
currently lose_sum: 382.8879051208496
time_elpased: 1.425
batch start
#iterations: 170
currently lose_sum: 384.1657464504242
time_elpased: 1.43
batch start
#iterations: 171
currently lose_sum: 383.8377130627632
time_elpased: 1.413
batch start
#iterations: 172
currently lose_sum: 383.5884590148926
time_elpased: 1.414
batch start
#iterations: 173
currently lose_sum: 382.9382520914078
time_elpased: 1.418
batch start
#iterations: 174
currently lose_sum: 384.01585203409195
time_elpased: 1.436
batch start
#iterations: 175
currently lose_sum: 383.5212643146515
time_elpased: 1.414
batch start
#iterations: 176
currently lose_sum: 383.52260768413544
time_elpased: 1.426
batch start
#iterations: 177
currently lose_sum: 384.0030084848404
time_elpased: 1.412
batch start
#iterations: 178
currently lose_sum: 384.4931468963623
time_elpased: 1.417
batch start
#iterations: 179
currently lose_sum: 383.64989227056503
time_elpased: 1.435
start validation test
0.729278350515
0.80770865807
0.594059405941
0.684602450156
0
validation finish
batch start
#iterations: 180
currently lose_sum: 384.01621329784393
time_elpased: 1.413
batch start
#iterations: 181
currently lose_sum: 382.5169595479965
time_elpased: 1.427
batch start
#iterations: 182
currently lose_sum: 384.1312873363495
time_elpased: 1.426
batch start
#iterations: 183
currently lose_sum: 384.209095954895
time_elpased: 1.412
batch start
#iterations: 184
currently lose_sum: 383.82296377420425
time_elpased: 1.416
batch start
#iterations: 185
currently lose_sum: 383.6595469713211
time_elpased: 1.434
batch start
#iterations: 186
currently lose_sum: 384.33777540922165
time_elpased: 1.422
batch start
#iterations: 187
currently lose_sum: 383.98565554618835
time_elpased: 1.415
batch start
#iterations: 188
currently lose_sum: 383.5294282436371
time_elpased: 1.426
batch start
#iterations: 189
currently lose_sum: 384.53105932474136
time_elpased: 1.413
batch start
#iterations: 190
currently lose_sum: 383.87110435962677
time_elpased: 1.432
batch start
#iterations: 191
currently lose_sum: 383.4740453362465
time_elpased: 1.438
batch start
#iterations: 192
currently lose_sum: 383.86866122484207
time_elpased: 1.437
batch start
#iterations: 193
currently lose_sum: 383.7786484360695
time_elpased: 1.422
batch start
#iterations: 194
currently lose_sum: 383.4425475001335
time_elpased: 1.433
batch start
#iterations: 195
currently lose_sum: 382.6783429980278
time_elpased: 1.415
batch start
#iterations: 196
currently lose_sum: 385.0257999300957
time_elpased: 1.415
batch start
#iterations: 197
currently lose_sum: 383.124047935009
time_elpased: 1.424
batch start
#iterations: 198
currently lose_sum: 383.03863364458084
time_elpased: 1.423
batch start
#iterations: 199
currently lose_sum: 382.9153552055359
time_elpased: 1.424
start validation test
0.725670103093
0.819023443333
0.571651902032
0.673336606924
0
validation finish
batch start
#iterations: 200
currently lose_sum: 383.7612668275833
time_elpased: 1.399
batch start
#iterations: 201
currently lose_sum: 383.69601649045944
time_elpased: 1.408
batch start
#iterations: 202
currently lose_sum: 383.9754905104637
time_elpased: 1.428
batch start
#iterations: 203
currently lose_sum: 383.4549777507782
time_elpased: 1.408
batch start
#iterations: 204
currently lose_sum: 383.94778805971146
time_elpased: 1.418
batch start
#iterations: 205
currently lose_sum: 382.8112369775772
time_elpased: 1.424
batch start
#iterations: 206
currently lose_sum: 383.1029132604599
time_elpased: 1.413
batch start
#iterations: 207
currently lose_sum: 383.3797008395195
time_elpased: 1.426
batch start
#iterations: 208
currently lose_sum: 382.90304189920425
time_elpased: 1.417
batch start
#iterations: 209
currently lose_sum: 383.8318894505501
time_elpased: 1.414
batch start
#iterations: 210
currently lose_sum: 383.6198077201843
time_elpased: 1.429
batch start
#iterations: 211
currently lose_sum: 384.45877265930176
time_elpased: 1.416
batch start
#iterations: 212
currently lose_sum: 383.85190027952194
time_elpased: 1.415
batch start
#iterations: 213
currently lose_sum: 382.7946299314499
time_elpased: 1.424
batch start
#iterations: 214
currently lose_sum: 383.79399341344833
time_elpased: 1.432
batch start
#iterations: 215
currently lose_sum: 383.47726488113403
time_elpased: 1.41
batch start
#iterations: 216
currently lose_sum: 384.0402945280075
time_elpased: 1.418
batch start
#iterations: 217
currently lose_sum: 383.10623121261597
time_elpased: 1.426
batch start
#iterations: 218
currently lose_sum: 383.1888976097107
time_elpased: 1.415
batch start
#iterations: 219
currently lose_sum: 384.76699417829514
time_elpased: 1.419
start validation test
0.726237113402
0.814445096888
0.578217821782
0.676296702627
0
validation finish
batch start
#iterations: 220
currently lose_sum: 383.22809487581253
time_elpased: 1.409
batch start
#iterations: 221
currently lose_sum: 382.6109883785248
time_elpased: 1.422
batch start
#iterations: 222
currently lose_sum: 384.1210950613022
time_elpased: 1.409
batch start
#iterations: 223
currently lose_sum: 382.6892890930176
time_elpased: 1.411
batch start
#iterations: 224
currently lose_sum: 383.4933829307556
time_elpased: 1.423
batch start
#iterations: 225
currently lose_sum: 382.7210233807564
time_elpased: 1.415
batch start
#iterations: 226
currently lose_sum: 382.5776655077934
time_elpased: 1.415
batch start
#iterations: 227
currently lose_sum: 384.62312054634094
time_elpased: 1.42
batch start
#iterations: 228
currently lose_sum: 384.10148990154266
time_elpased: 1.416
batch start
#iterations: 229
currently lose_sum: 382.82782930135727
time_elpased: 1.419
batch start
#iterations: 230
currently lose_sum: 383.46742594242096
time_elpased: 1.412
batch start
#iterations: 231
currently lose_sum: 383.77898102998734
time_elpased: 1.409
batch start
#iterations: 232
currently lose_sum: 382.96912586688995
time_elpased: 1.425
batch start
#iterations: 233
currently lose_sum: 384.3339684009552
time_elpased: 1.413
batch start
#iterations: 234
currently lose_sum: 384.08261984586716
time_elpased: 1.412
batch start
#iterations: 235
currently lose_sum: 383.07430362701416
time_elpased: 1.434
batch start
#iterations: 236
currently lose_sum: 383.3980939388275
time_elpased: 1.419
batch start
#iterations: 237
currently lose_sum: 384.34894597530365
time_elpased: 1.406
batch start
#iterations: 238
currently lose_sum: 383.25134778022766
time_elpased: 1.435
batch start
#iterations: 239
currently lose_sum: 383.0628454685211
time_elpased: 1.414
start validation test
0.727010309278
0.814852790391
0.579781136008
0.677505784923
0
validation finish
batch start
#iterations: 240
currently lose_sum: 383.72452652454376
time_elpased: 1.415
batch start
#iterations: 241
currently lose_sum: 383.20556354522705
time_elpased: 1.412
batch start
#iterations: 242
currently lose_sum: 384.2239072918892
time_elpased: 1.427
batch start
#iterations: 243
currently lose_sum: 383.6586837172508
time_elpased: 1.413
batch start
#iterations: 244
currently lose_sum: 383.29404670000076
time_elpased: 1.427
batch start
#iterations: 245
currently lose_sum: 383.431842982769
time_elpased: 1.424
batch start
#iterations: 246
currently lose_sum: 383.22166270017624
time_elpased: 1.42
batch start
#iterations: 247
currently lose_sum: 382.23192900419235
time_elpased: 1.414
batch start
#iterations: 248
currently lose_sum: 383.9541895389557
time_elpased: 1.43
batch start
#iterations: 249
currently lose_sum: 382.9803782105446
time_elpased: 1.43
batch start
#iterations: 250
currently lose_sum: 383.7653965950012
time_elpased: 1.421
batch start
#iterations: 251
currently lose_sum: 383.0448051095009
time_elpased: 1.411
batch start
#iterations: 252
currently lose_sum: 383.42898404598236
time_elpased: 1.418
batch start
#iterations: 253
currently lose_sum: 384.27636325359344
time_elpased: 1.412
batch start
#iterations: 254
currently lose_sum: 383.26510441303253
time_elpased: 1.439
batch start
#iterations: 255
currently lose_sum: 382.856362760067
time_elpased: 1.421
batch start
#iterations: 256
currently lose_sum: 383.31971192359924
time_elpased: 1.414
batch start
#iterations: 257
currently lose_sum: 383.73611587285995
time_elpased: 1.413
batch start
#iterations: 258
currently lose_sum: 383.38685512542725
time_elpased: 1.423
batch start
#iterations: 259
currently lose_sum: 383.24184358119965
time_elpased: 1.409
start validation test
0.734381443299
0.807617728532
0.607712350182
0.693547427892
0
validation finish
batch start
#iterations: 260
currently lose_sum: 384.3010355234146
time_elpased: 1.426
batch start
#iterations: 261
currently lose_sum: 382.92992663383484
time_elpased: 1.429
batch start
#iterations: 262
currently lose_sum: 382.22905588150024
time_elpased: 1.42
batch start
#iterations: 263
currently lose_sum: 382.9869142770767
time_elpased: 1.423
batch start
#iterations: 264
currently lose_sum: 384.61484813690186
time_elpased: 1.413
batch start
#iterations: 265
currently lose_sum: 382.7597608566284
time_elpased: 1.423
batch start
#iterations: 266
currently lose_sum: 383.1269418001175
time_elpased: 1.425
batch start
#iterations: 267
currently lose_sum: 383.66383731365204
time_elpased: 1.424
batch start
#iterations: 268
currently lose_sum: 383.5241035223007
time_elpased: 1.432
batch start
#iterations: 269
currently lose_sum: 383.631309568882
time_elpased: 1.421
batch start
#iterations: 270
currently lose_sum: 383.70280796289444
time_elpased: 1.421
batch start
#iterations: 271
currently lose_sum: 383.3121003508568
time_elpased: 1.42
batch start
#iterations: 272
currently lose_sum: 383.22268038988113
time_elpased: 1.406
batch start
#iterations: 273
currently lose_sum: 383.65561777353287
time_elpased: 1.401
batch start
#iterations: 274
currently lose_sum: 383.6223883032799
time_elpased: 1.419
batch start
#iterations: 275
currently lose_sum: 383.60783594846725
time_elpased: 1.413
batch start
#iterations: 276
currently lose_sum: 384.4286306500435
time_elpased: 1.405
batch start
#iterations: 277
currently lose_sum: 383.60210168361664
time_elpased: 1.426
batch start
#iterations: 278
currently lose_sum: 384.3034167289734
time_elpased: 1.382
batch start
#iterations: 279
currently lose_sum: 382.2093315124512
time_elpased: 1.417
start validation test
0.725979381443
0.819853490806
0.571547681084
0.67354458364
0
validation finish
batch start
#iterations: 280
currently lose_sum: 383.1857007741928
time_elpased: 1.389
batch start
#iterations: 281
currently lose_sum: 382.37358570098877
time_elpased: 1.369
batch start
#iterations: 282
currently lose_sum: 383.5723356604576
time_elpased: 1.39
batch start
#iterations: 283
currently lose_sum: 383.74916422367096
time_elpased: 1.379
batch start
#iterations: 284
currently lose_sum: 383.17909175157547
time_elpased: 1.376
batch start
#iterations: 285
currently lose_sum: 383.2251526713371
time_elpased: 1.405
batch start
#iterations: 286
currently lose_sum: 383.3677446246147
time_elpased: 1.381
batch start
#iterations: 287
currently lose_sum: 383.8780117034912
time_elpased: 1.39
batch start
#iterations: 288
currently lose_sum: 383.10243433713913
time_elpased: 1.407
batch start
#iterations: 289
currently lose_sum: 382.0652579665184
time_elpased: 1.382
batch start
#iterations: 290
currently lose_sum: 382.4676282405853
time_elpased: 1.364
batch start
#iterations: 291
currently lose_sum: 382.93146097660065
time_elpased: 1.41
batch start
#iterations: 292
currently lose_sum: 383.39407044649124
time_elpased: 1.368
batch start
#iterations: 293
currently lose_sum: 383.72378438711166
time_elpased: 1.38
batch start
#iterations: 294
currently lose_sum: 383.3205834031105
time_elpased: 1.4
batch start
#iterations: 295
currently lose_sum: 384.262271463871
time_elpased: 1.383
batch start
#iterations: 296
currently lose_sum: 383.4501564502716
time_elpased: 1.387
batch start
#iterations: 297
currently lose_sum: 383.62599951028824
time_elpased: 1.406
batch start
#iterations: 298
currently lose_sum: 383.3205435872078
time_elpased: 1.393
batch start
#iterations: 299
currently lose_sum: 383.1647753715515
time_elpased: 1.39
start validation test
0.737268041237
0.803099730458
0.621052631579
0.70044078754
0
validation finish
batch start
#iterations: 300
currently lose_sum: 383.25060826539993
time_elpased: 1.386
batch start
#iterations: 301
currently lose_sum: 383.29507982730865
time_elpased: 1.395
batch start
#iterations: 302
currently lose_sum: 383.4368615746498
time_elpased: 1.431
batch start
#iterations: 303
currently lose_sum: 383.8589308857918
time_elpased: 1.421
batch start
#iterations: 304
currently lose_sum: 383.48185259103775
time_elpased: 1.408
batch start
#iterations: 305
currently lose_sum: 382.8609866499901
time_elpased: 1.43
batch start
#iterations: 306
currently lose_sum: 383.27727419137955
time_elpased: 1.402
batch start
#iterations: 307
currently lose_sum: 382.2653031349182
time_elpased: 1.4
batch start
#iterations: 308
currently lose_sum: 382.2499651312828
time_elpased: 1.426
batch start
#iterations: 309
currently lose_sum: 382.8271237015724
time_elpased: 1.404
batch start
#iterations: 310
currently lose_sum: 383.31336748600006
time_elpased: 1.418
batch start
#iterations: 311
currently lose_sum: 383.11486464738846
time_elpased: 1.422
batch start
#iterations: 312
currently lose_sum: 382.3959751725197
time_elpased: 1.423
batch start
#iterations: 313
currently lose_sum: 383.334693133831
time_elpased: 1.435
batch start
#iterations: 314
currently lose_sum: 382.36066722869873
time_elpased: 1.429
batch start
#iterations: 315
currently lose_sum: 383.02939224243164
time_elpased: 1.419
batch start
#iterations: 316
currently lose_sum: 382.41469728946686
time_elpased: 1.44
batch start
#iterations: 317
currently lose_sum: 383.40350902080536
time_elpased: 1.428
batch start
#iterations: 318
currently lose_sum: 382.57376432418823
time_elpased: 1.435
batch start
#iterations: 319
currently lose_sum: 383.53242141008377
time_elpased: 1.444
start validation test
0.735670103093
0.802355489373
0.61771756123
0.698033211636
0
validation finish
batch start
#iterations: 320
currently lose_sum: 381.94301241636276
time_elpased: 1.428
batch start
#iterations: 321
currently lose_sum: 383.575568318367
time_elpased: 1.445
batch start
#iterations: 322
currently lose_sum: 382.2459310889244
time_elpased: 1.452
batch start
#iterations: 323
currently lose_sum: 382.75735771656036
time_elpased: 1.453
batch start
#iterations: 324
currently lose_sum: 382.8561817407608
time_elpased: 1.462
batch start
#iterations: 325
currently lose_sum: 383.23323953151703
time_elpased: 1.44
batch start
#iterations: 326
currently lose_sum: 384.3538122177124
time_elpased: 1.441
batch start
#iterations: 327
currently lose_sum: 383.2233897447586
time_elpased: 1.443
batch start
#iterations: 328
currently lose_sum: 384.1321792602539
time_elpased: 1.434
batch start
#iterations: 329
currently lose_sum: 383.43628454208374
time_elpased: 1.428
batch start
#iterations: 330
currently lose_sum: 383.21805107593536
time_elpased: 1.459
batch start
#iterations: 331
currently lose_sum: 383.5704317688942
time_elpased: 1.437
batch start
#iterations: 332
currently lose_sum: 384.0569506287575
time_elpased: 1.437
batch start
#iterations: 333
currently lose_sum: 382.8484498858452
time_elpased: 1.449
batch start
#iterations: 334
currently lose_sum: 382.6002097725868
time_elpased: 1.435
batch start
#iterations: 335
currently lose_sum: 382.6058536171913
time_elpased: 1.433
batch start
#iterations: 336
currently lose_sum: 382.3752757906914
time_elpased: 1.44
batch start
#iterations: 337
currently lose_sum: 382.98648476600647
time_elpased: 1.433
batch start
#iterations: 338
currently lose_sum: 383.54835176467896
time_elpased: 1.456
batch start
#iterations: 339
currently lose_sum: 382.91304552555084
time_elpased: 1.436
start validation test
0.733556701031
0.813544913573
0.598436685774
0.689605476491
0
validation finish
batch start
#iterations: 340
currently lose_sum: 383.4840037226677
time_elpased: 1.433
batch start
#iterations: 341
currently lose_sum: 383.9871927499771
time_elpased: 1.439
batch start
#iterations: 342
currently lose_sum: 383.52586245536804
time_elpased: 1.416
batch start
#iterations: 343
currently lose_sum: 383.3999248743057
time_elpased: 1.421
batch start
#iterations: 344
currently lose_sum: 383.7759107351303
time_elpased: 1.437
batch start
#iterations: 345
currently lose_sum: 382.8237312436104
time_elpased: 1.417
batch start
#iterations: 346
currently lose_sum: 383.47975927591324
time_elpased: 1.425
batch start
#iterations: 347
currently lose_sum: 382.44029957056046
time_elpased: 1.425
batch start
#iterations: 348
currently lose_sum: 381.6948704123497
time_elpased: 1.423
batch start
#iterations: 349
currently lose_sum: 382.58365470170975
time_elpased: 1.42
batch start
#iterations: 350
currently lose_sum: 383.2892414331436
time_elpased: 1.425
batch start
#iterations: 351
currently lose_sum: 382.2870925664902
time_elpased: 1.425
batch start
#iterations: 352
currently lose_sum: 384.98691004514694
time_elpased: 1.433
batch start
#iterations: 353
currently lose_sum: 383.67355042696
time_elpased: 1.424
batch start
#iterations: 354
currently lose_sum: 383.78890800476074
time_elpased: 1.414
batch start
#iterations: 355
currently lose_sum: 384.9247686266899
time_elpased: 1.427
batch start
#iterations: 356
currently lose_sum: 382.7123118042946
time_elpased: 1.409
batch start
#iterations: 357
currently lose_sum: 382.94645857810974
time_elpased: 1.401
batch start
#iterations: 358
currently lose_sum: 382.63732224702835
time_elpased: 1.423
batch start
#iterations: 359
currently lose_sum: 383.83264124393463
time_elpased: 1.416
start validation test
0.723556701031
0.818099819603
0.567170401251
0.669908290761
0
validation finish
batch start
#iterations: 360
currently lose_sum: 383.37230479717255
time_elpased: 1.413
batch start
#iterations: 361
currently lose_sum: 383.2278911471367
time_elpased: 1.408
batch start
#iterations: 362
currently lose_sum: 383.98618400096893
time_elpased: 1.416
batch start
#iterations: 363
currently lose_sum: 383.24025535583496
time_elpased: 1.431
batch start
#iterations: 364
currently lose_sum: 382.6414797902107
time_elpased: 1.414
batch start
#iterations: 365
currently lose_sum: 383.06941998004913
time_elpased: 1.407
batch start
#iterations: 366
currently lose_sum: 383.6161262989044
time_elpased: 1.428
batch start
#iterations: 367
currently lose_sum: 383.6785274744034
time_elpased: 1.418
batch start
#iterations: 368
currently lose_sum: 383.609195291996
time_elpased: 1.417
batch start
#iterations: 369
currently lose_sum: 382.39577490091324
time_elpased: 1.424
batch start
#iterations: 370
currently lose_sum: 382.47654992341995
time_elpased: 1.419
batch start
#iterations: 371
currently lose_sum: 383.47898852825165
time_elpased: 1.426
batch start
#iterations: 372
currently lose_sum: 383.0082328915596
time_elpased: 1.416
batch start
#iterations: 373
currently lose_sum: 383.2481756210327
time_elpased: 1.432
batch start
#iterations: 374
currently lose_sum: 383.6680860519409
time_elpased: 1.416
batch start
#iterations: 375
currently lose_sum: 383.2243193387985
time_elpased: 1.42
batch start
#iterations: 376
currently lose_sum: 383.1182886958122
time_elpased: 1.42
batch start
#iterations: 377
currently lose_sum: 382.797787129879
time_elpased: 1.419
batch start
#iterations: 378
currently lose_sum: 382.4746543765068
time_elpased: 1.425
batch start
#iterations: 379
currently lose_sum: 384.3076658844948
time_elpased: 1.418
start validation test
0.731649484536
0.810439949073
0.597081813445
0.687590014402
0
validation finish
batch start
#iterations: 380
currently lose_sum: 383.7531827688217
time_elpased: 1.415
batch start
#iterations: 381
currently lose_sum: 383.3607556819916
time_elpased: 1.42
batch start
#iterations: 382
currently lose_sum: 383.7005690932274
time_elpased: 1.428
batch start
#iterations: 383
currently lose_sum: 381.5694677233696
time_elpased: 1.42
batch start
#iterations: 384
currently lose_sum: 382.3237106204033
time_elpased: 1.425
batch start
#iterations: 385
currently lose_sum: 383.71183604002
time_elpased: 1.418
batch start
#iterations: 386
currently lose_sum: 382.7054954171181
time_elpased: 1.418
batch start
#iterations: 387
currently lose_sum: 383.04419511556625
time_elpased: 1.402
batch start
#iterations: 388
currently lose_sum: 382.8885020017624
time_elpased: 1.418
batch start
#iterations: 389
currently lose_sum: 383.30133974552155
time_elpased: 1.414
batch start
#iterations: 390
currently lose_sum: 382.62626326084137
time_elpased: 1.403
batch start
#iterations: 391
currently lose_sum: 382.39533138275146
time_elpased: 1.406
batch start
#iterations: 392
currently lose_sum: 383.23933547735214
time_elpased: 1.398
batch start
#iterations: 393
currently lose_sum: 382.34210711717606
time_elpased: 1.399
batch start
#iterations: 394
currently lose_sum: 383.9609236717224
time_elpased: 1.424
batch start
#iterations: 395
currently lose_sum: 383.0549061894417
time_elpased: 1.408
batch start
#iterations: 396
currently lose_sum: 382.06466019153595
time_elpased: 1.409
batch start
#iterations: 397
currently lose_sum: 383.4015381336212
time_elpased: 1.407
batch start
#iterations: 398
currently lose_sum: 382.86276882886887
time_elpased: 1.398
batch start
#iterations: 399
currently lose_sum: 383.879465341568
time_elpased: 1.412
start validation test
0.729948453608
0.814921920185
0.587389265242
0.682696384229
0
validation finish
acc: 0.733
pre: 0.784
rec: 0.647
F1: 0.708
auc: 0.000
