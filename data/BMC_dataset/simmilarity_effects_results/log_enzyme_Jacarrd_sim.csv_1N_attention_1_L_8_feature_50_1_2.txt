start to construct graph
graph construct over
58300
epochs start
batch start
#iterations: 0
currently lose_sum: 402.06823444366455
time_elpased: 1.11
batch start
#iterations: 1
currently lose_sum: 396.1706438064575
time_elpased: 1.08
batch start
#iterations: 2
currently lose_sum: 392.21579736471176
time_elpased: 1.077
batch start
#iterations: 3
currently lose_sum: 390.08058696985245
time_elpased: 1.071
batch start
#iterations: 4
currently lose_sum: 388.4364516735077
time_elpased: 1.074
batch start
#iterations: 5
currently lose_sum: 387.67169350385666
time_elpased: 1.069
batch start
#iterations: 6
currently lose_sum: 386.40049493312836
time_elpased: 1.068
batch start
#iterations: 7
currently lose_sum: 385.8540064096451
time_elpased: 1.076
batch start
#iterations: 8
currently lose_sum: 385.0554059743881
time_elpased: 1.077
batch start
#iterations: 9
currently lose_sum: 385.65956366062164
time_elpased: 1.077
batch start
#iterations: 10
currently lose_sum: 384.1962728500366
time_elpased: 1.082
batch start
#iterations: 11
currently lose_sum: 383.68680584430695
time_elpased: 1.068
batch start
#iterations: 12
currently lose_sum: 383.39006930589676
time_elpased: 1.076
batch start
#iterations: 13
currently lose_sum: 384.5790978074074
time_elpased: 1.071
batch start
#iterations: 14
currently lose_sum: 380.80887508392334
time_elpased: 1.063
batch start
#iterations: 15
currently lose_sum: 382.57399612665176
time_elpased: 1.072
batch start
#iterations: 16
currently lose_sum: 382.1142233014107
time_elpased: 1.074
batch start
#iterations: 17
currently lose_sum: 382.4882000684738
time_elpased: 1.071
batch start
#iterations: 18
currently lose_sum: 380.747094810009
time_elpased: 1.063
batch start
#iterations: 19
currently lose_sum: 381.81949627399445
time_elpased: 1.076
start validation test
0.694329896907
0.822747670003
0.492967942089
0.616528711847
0
validation finish
batch start
#iterations: 20
currently lose_sum: 381.54353910684586
time_elpased: 1.082
batch start
#iterations: 21
currently lose_sum: 381.1082144379616
time_elpased: 1.064
batch start
#iterations: 22
currently lose_sum: 381.4494425058365
time_elpased: 1.072
batch start
#iterations: 23
currently lose_sum: 382.0264626145363
time_elpased: 1.064
batch start
#iterations: 24
currently lose_sum: 381.1411602497101
time_elpased: 1.074
batch start
#iterations: 25
currently lose_sum: 380.784751534462
time_elpased: 1.067
batch start
#iterations: 26
currently lose_sum: 379.8135025501251
time_elpased: 1.085
batch start
#iterations: 27
currently lose_sum: 380.02791541814804
time_elpased: 1.064
batch start
#iterations: 28
currently lose_sum: 379.1367629766464
time_elpased: 1.078
batch start
#iterations: 29
currently lose_sum: 379.82588320970535
time_elpased: 1.063
batch start
#iterations: 30
currently lose_sum: 380.4913005232811
time_elpased: 1.07
batch start
#iterations: 31
currently lose_sum: 378.93776243925095
time_elpased: 1.077
batch start
#iterations: 32
currently lose_sum: 380.6795751452446
time_elpased: 1.069
batch start
#iterations: 33
currently lose_sum: 380.23733443021774
time_elpased: 1.065
batch start
#iterations: 34
currently lose_sum: 379.9907864332199
time_elpased: 1.069
batch start
#iterations: 35
currently lose_sum: 381.05658757686615
time_elpased: 1.066
batch start
#iterations: 36
currently lose_sum: 379.6323980689049
time_elpased: 1.061
batch start
#iterations: 37
currently lose_sum: 378.57641583681107
time_elpased: 1.07
batch start
#iterations: 38
currently lose_sum: 379.86232328414917
time_elpased: 1.075
batch start
#iterations: 39
currently lose_sum: 379.8969752192497
time_elpased: 1.072
start validation test
0.719536082474
0.825558121632
0.554498448811
0.663408598825
0
validation finish
batch start
#iterations: 40
currently lose_sum: 379.0202832221985
time_elpased: 1.074
batch start
#iterations: 41
currently lose_sum: 378.8149279952049
time_elpased: 1.078
batch start
#iterations: 42
currently lose_sum: 378.4433373808861
time_elpased: 1.081
batch start
#iterations: 43
currently lose_sum: 379.5277829170227
time_elpased: 1.058
batch start
#iterations: 44
currently lose_sum: 378.50312304496765
time_elpased: 1.076
batch start
#iterations: 45
currently lose_sum: 378.36830312013626
time_elpased: 1.068
batch start
#iterations: 46
currently lose_sum: 378.1761185526848
time_elpased: 1.07
batch start
#iterations: 47
currently lose_sum: 379.6518904566765
time_elpased: 1.068
batch start
#iterations: 48
currently lose_sum: 378.54260355234146
time_elpased: 1.074
batch start
#iterations: 49
currently lose_sum: 379.0399051308632
time_elpased: 1.065
batch start
#iterations: 50
currently lose_sum: 377.1385905742645
time_elpased: 1.077
batch start
#iterations: 51
currently lose_sum: 377.9895263314247
time_elpased: 1.071
batch start
#iterations: 52
currently lose_sum: 378.9177240729332
time_elpased: 1.062
batch start
#iterations: 53
currently lose_sum: 379.53773188591003
time_elpased: 1.076
batch start
#iterations: 54
currently lose_sum: 379.6846277117729
time_elpased: 1.072
batch start
#iterations: 55
currently lose_sum: 377.856700360775
time_elpased: 1.075
batch start
#iterations: 56
currently lose_sum: 378.8890508413315
time_elpased: 1.074
batch start
#iterations: 57
currently lose_sum: 378.6913434267044
time_elpased: 1.061
batch start
#iterations: 58
currently lose_sum: 377.58515626192093
time_elpased: 1.075
batch start
#iterations: 59
currently lose_sum: 378.45722609758377
time_elpased: 1.065
start validation test
0.729226804124
0.785964003626
0.627714581179
0.69798194676
0
validation finish
batch start
#iterations: 60
currently lose_sum: 377.92263996601105
time_elpased: 1.08
batch start
#iterations: 61
currently lose_sum: 378.847165286541
time_elpased: 1.068
batch start
#iterations: 62
currently lose_sum: 378.2023189663887
time_elpased: 1.064
batch start
#iterations: 63
currently lose_sum: 377.74588537216187
time_elpased: 1.063
batch start
#iterations: 64
currently lose_sum: 378.6665865778923
time_elpased: 1.077
batch start
#iterations: 65
currently lose_sum: 378.1515074968338
time_elpased: 1.067
batch start
#iterations: 66
currently lose_sum: 377.0014979839325
time_elpased: 1.063
batch start
#iterations: 67
currently lose_sum: 378.08920031785965
time_elpased: 1.068
batch start
#iterations: 68
currently lose_sum: 378.2332283258438
time_elpased: 1.074
batch start
#iterations: 69
currently lose_sum: 377.96663868427277
time_elpased: 1.072
batch start
#iterations: 70
currently lose_sum: 377.1040980219841
time_elpased: 1.065
batch start
#iterations: 71
currently lose_sum: 378.36314421892166
time_elpased: 1.067
batch start
#iterations: 72
currently lose_sum: 378.2297183275223
time_elpased: 1.073
batch start
#iterations: 73
currently lose_sum: 377.77378964424133
time_elpased: 1.06
batch start
#iterations: 74
currently lose_sum: 377.90378963947296
time_elpased: 1.077
batch start
#iterations: 75
currently lose_sum: 377.1690125465393
time_elpased: 1.064
batch start
#iterations: 76
currently lose_sum: 377.9507783651352
time_elpased: 1.069
batch start
#iterations: 77
currently lose_sum: 377.314300596714
time_elpased: 1.074
batch start
#iterations: 78
currently lose_sum: 378.23609286546707
time_elpased: 1.067
batch start
#iterations: 79
currently lose_sum: 378.0701089501381
time_elpased: 1.073
start validation test
0.730515463918
0.796449546183
0.617063081696
0.695373499592
0
validation finish
batch start
#iterations: 80
currently lose_sum: 376.8421870470047
time_elpased: 1.077
batch start
#iterations: 81
currently lose_sum: 378.5400688648224
time_elpased: 1.071
batch start
#iterations: 82
currently lose_sum: 377.7470467686653
time_elpased: 1.073
batch start
#iterations: 83
currently lose_sum: 377.3081946372986
time_elpased: 1.082
batch start
#iterations: 84
currently lose_sum: 378.50231993198395
time_elpased: 1.075
batch start
#iterations: 85
currently lose_sum: 378.27912479639053
time_elpased: 1.071
batch start
#iterations: 86
currently lose_sum: 377.78400951623917
time_elpased: 1.073
batch start
#iterations: 87
currently lose_sum: 377.51961624622345
time_elpased: 1.066
batch start
#iterations: 88
currently lose_sum: 376.66732770204544
time_elpased: 1.069
batch start
#iterations: 89
currently lose_sum: 377.9318497776985
time_elpased: 1.085
batch start
#iterations: 90
currently lose_sum: 378.1166515350342
time_elpased: 1.076
batch start
#iterations: 91
currently lose_sum: 377.2607634663582
time_elpased: 1.067
batch start
#iterations: 92
currently lose_sum: 377.0241963863373
time_elpased: 1.073
batch start
#iterations: 93
currently lose_sum: 378.1166708469391
time_elpased: 1.071
batch start
#iterations: 94
currently lose_sum: 376.32067185640335
time_elpased: 1.07
batch start
#iterations: 95
currently lose_sum: 376.4245815873146
time_elpased: 1.065
batch start
#iterations: 96
currently lose_sum: 376.91013902425766
time_elpased: 1.121
batch start
#iterations: 97
currently lose_sum: 377.8836566209793
time_elpased: 1.069
batch start
#iterations: 98
currently lose_sum: 377.5267621278763
time_elpased: 1.067
batch start
#iterations: 99
currently lose_sum: 377.91502088308334
time_elpased: 1.069
start validation test
0.729793814433
0.79967514889
0.610961737332
0.692695509438
0
validation finish
batch start
#iterations: 100
currently lose_sum: 377.66395473480225
time_elpased: 1.072
batch start
#iterations: 101
currently lose_sum: 377.31870448589325
time_elpased: 1.064
batch start
#iterations: 102
currently lose_sum: 376.7261207103729
time_elpased: 1.071
batch start
#iterations: 103
currently lose_sum: 376.62430787086487
time_elpased: 1.072
batch start
#iterations: 104
currently lose_sum: 377.8017238378525
time_elpased: 1.081
batch start
#iterations: 105
currently lose_sum: 376.3876328468323
time_elpased: 1.067
batch start
#iterations: 106
currently lose_sum: 377.87847727537155
time_elpased: 1.07
batch start
#iterations: 107
currently lose_sum: 377.00651854276657
time_elpased: 1.093
batch start
#iterations: 108
currently lose_sum: 377.1788013577461
time_elpased: 1.098
batch start
#iterations: 109
currently lose_sum: 376.4103150963783
time_elpased: 1.067
batch start
#iterations: 110
currently lose_sum: 377.3279178738594
time_elpased: 1.074
batch start
#iterations: 111
currently lose_sum: 376.2810606956482
time_elpased: 1.073
batch start
#iterations: 112
currently lose_sum: 376.6805616617203
time_elpased: 1.079
batch start
#iterations: 113
currently lose_sum: 377.31083714962006
time_elpased: 1.085
batch start
#iterations: 114
currently lose_sum: 376.5846548676491
time_elpased: 1.073
batch start
#iterations: 115
currently lose_sum: 376.6908360719681
time_elpased: 1.078
batch start
#iterations: 116
currently lose_sum: 378.09149318933487
time_elpased: 1.089
batch start
#iterations: 117
currently lose_sum: 375.97446632385254
time_elpased: 1.092
batch start
#iterations: 118
currently lose_sum: 375.90885800123215
time_elpased: 1.077
batch start
#iterations: 119
currently lose_sum: 376.99000960588455
time_elpased: 1.067
start validation test
0.725360824742
0.800942611589
0.597518097208
0.684434968017
0
validation finish
batch start
#iterations: 120
currently lose_sum: 377.262479364872
time_elpased: 1.087
batch start
#iterations: 121
currently lose_sum: 376.29613721370697
time_elpased: 1.078
batch start
#iterations: 122
currently lose_sum: 376.291203558445
time_elpased: 1.094
batch start
#iterations: 123
currently lose_sum: 376.44032806158066
time_elpased: 1.073
batch start
#iterations: 124
currently lose_sum: 375.94774985313416
time_elpased: 1.076
batch start
#iterations: 125
currently lose_sum: 376.94334322214127
time_elpased: 1.081
batch start
#iterations: 126
currently lose_sum: 377.89168524742126
time_elpased: 1.072
batch start
#iterations: 127
currently lose_sum: 377.7535818219185
time_elpased: 1.09
batch start
#iterations: 128
currently lose_sum: 376.3487860560417
time_elpased: 1.073
batch start
#iterations: 129
currently lose_sum: 377.2232672572136
time_elpased: 1.083
batch start
#iterations: 130
currently lose_sum: 376.7743163704872
time_elpased: 1.083
batch start
#iterations: 131
currently lose_sum: 376.2092520594597
time_elpased: 1.08
batch start
#iterations: 132
currently lose_sum: 376.0292235612869
time_elpased: 1.075
batch start
#iterations: 133
currently lose_sum: 377.3634931445122
time_elpased: 1.07
batch start
#iterations: 134
currently lose_sum: 377.04094791412354
time_elpased: 1.084
batch start
#iterations: 135
currently lose_sum: 376.60109972953796
time_elpased: 1.066
batch start
#iterations: 136
currently lose_sum: 377.04377073049545
time_elpased: 1.077
batch start
#iterations: 137
currently lose_sum: 376.8381041288376
time_elpased: 1.077
batch start
#iterations: 138
currently lose_sum: 377.2470757961273
time_elpased: 1.077
batch start
#iterations: 139
currently lose_sum: 375.90906459093094
time_elpased: 1.082
start validation test
0.729587628866
0.795959325662
0.615201654602
0.694003733084
0
validation finish
batch start
#iterations: 140
currently lose_sum: 376.37473529577255
time_elpased: 1.079
batch start
#iterations: 141
currently lose_sum: 377.77923476696014
time_elpased: 1.077
batch start
#iterations: 142
currently lose_sum: 376.7776769399643
time_elpased: 1.07
batch start
#iterations: 143
currently lose_sum: 376.673187315464
time_elpased: 1.074
batch start
#iterations: 144
currently lose_sum: 377.24119102954865
time_elpased: 1.068
batch start
#iterations: 145
currently lose_sum: 376.73682057857513
time_elpased: 1.079
batch start
#iterations: 146
currently lose_sum: 375.9592736363411
time_elpased: 1.067
batch start
#iterations: 147
currently lose_sum: 377.0140392780304
time_elpased: 1.07
batch start
#iterations: 148
currently lose_sum: 377.08171647787094
time_elpased: 1.076
batch start
#iterations: 149
currently lose_sum: 376.44890213012695
time_elpased: 1.078
batch start
#iterations: 150
currently lose_sum: 375.8821177482605
time_elpased: 1.067
batch start
#iterations: 151
currently lose_sum: 376.2616522908211
time_elpased: 1.067
batch start
#iterations: 152
currently lose_sum: 376.0236341357231
time_elpased: 1.077
batch start
#iterations: 153
currently lose_sum: 377.6108219027519
time_elpased: 1.083
batch start
#iterations: 154
currently lose_sum: 376.5124433040619
time_elpased: 1.071
batch start
#iterations: 155
currently lose_sum: 377.53212666511536
time_elpased: 1.069
batch start
#iterations: 156
currently lose_sum: 377.6288446187973
time_elpased: 1.075
batch start
#iterations: 157
currently lose_sum: 375.9451068043709
time_elpased: 1.062
batch start
#iterations: 158
currently lose_sum: 377.39224207401276
time_elpased: 1.071
batch start
#iterations: 159
currently lose_sum: 378.22183698415756
time_elpased: 1.072
start validation test
0.731288659794
0.791116917048
0.62626680455
0.699105339105
0
validation finish
batch start
#iterations: 160
currently lose_sum: 377.41936242580414
time_elpased: 1.085
batch start
#iterations: 161
currently lose_sum: 377.3875969052315
time_elpased: 1.067
batch start
#iterations: 162
currently lose_sum: 376.2389506697655
time_elpased: 1.075
batch start
#iterations: 163
currently lose_sum: 376.23940885066986
time_elpased: 1.071
batch start
#iterations: 164
currently lose_sum: 375.78557109832764
time_elpased: 1.071
batch start
#iterations: 165
currently lose_sum: 374.9829137325287
time_elpased: 1.088
batch start
#iterations: 166
currently lose_sum: 376.86183273792267
time_elpased: 1.082
batch start
#iterations: 167
currently lose_sum: 376.539517223835
time_elpased: 1.073
batch start
#iterations: 168
currently lose_sum: 376.2421783208847
time_elpased: 1.068
batch start
#iterations: 169
currently lose_sum: 376.38462966680527
time_elpased: 1.093
batch start
#iterations: 170
currently lose_sum: 376.4914501905441
time_elpased: 1.076
batch start
#iterations: 171
currently lose_sum: 376.1616523861885
time_elpased: 1.069
batch start
#iterations: 172
currently lose_sum: 376.3828517794609
time_elpased: 1.067
batch start
#iterations: 173
currently lose_sum: 377.0423660874367
time_elpased: 1.078
batch start
#iterations: 174
currently lose_sum: 375.78476160764694
time_elpased: 1.077
batch start
#iterations: 175
currently lose_sum: 377.50213462114334
time_elpased: 1.076
batch start
#iterations: 176
currently lose_sum: 376.4019833803177
time_elpased: 1.063
batch start
#iterations: 177
currently lose_sum: 376.493026137352
time_elpased: 1.093
batch start
#iterations: 178
currently lose_sum: 377.1614481806755
time_elpased: 1.074
batch start
#iterations: 179
currently lose_sum: 377.2542831301689
time_elpased: 1.071
start validation test
0.730670103093
0.79440985561
0.620165460186
0.696556129857
0
validation finish
batch start
#iterations: 180
currently lose_sum: 376.5010195374489
time_elpased: 1.071
batch start
#iterations: 181
currently lose_sum: 375.97081530094147
time_elpased: 1.076
batch start
#iterations: 182
currently lose_sum: 376.0225861668587
time_elpased: 1.063
batch start
#iterations: 183
currently lose_sum: 376.1174329519272
time_elpased: 1.058
batch start
#iterations: 184
currently lose_sum: 376.8806441426277
time_elpased: 1.064
batch start
#iterations: 185
currently lose_sum: 376.0404682159424
time_elpased: 1.077
batch start
#iterations: 186
currently lose_sum: 376.40485340356827
time_elpased: 1.074
batch start
#iterations: 187
currently lose_sum: 375.2295423746109
time_elpased: 1.096
batch start
#iterations: 188
currently lose_sum: 377.15823072195053
time_elpased: 1.075
batch start
#iterations: 189
currently lose_sum: 377.34086632728577
time_elpased: 1.068
batch start
#iterations: 190
currently lose_sum: 375.7943977713585
time_elpased: 1.074
batch start
#iterations: 191
currently lose_sum: 375.87997967004776
time_elpased: 1.07
batch start
#iterations: 192
currently lose_sum: 376.8468765616417
time_elpased: 1.082
batch start
#iterations: 193
currently lose_sum: 377.4680188894272
time_elpased: 1.08
batch start
#iterations: 194
currently lose_sum: 375.95568656921387
time_elpased: 1.068
batch start
#iterations: 195
currently lose_sum: 376.11372590065
time_elpased: 1.084
batch start
#iterations: 196
currently lose_sum: 377.8561986088753
time_elpased: 1.068
batch start
#iterations: 197
currently lose_sum: 376.52676671743393
time_elpased: 1.081
batch start
#iterations: 198
currently lose_sum: 377.09796887636185
time_elpased: 1.074
batch start
#iterations: 199
currently lose_sum: 376.18943017721176
time_elpased: 1.074
start validation test
0.729690721649
0.8046530837
0.604446742503
0.690327152474
0
validation finish
batch start
#iterations: 200
currently lose_sum: 377.06096094846725
time_elpased: 1.077
batch start
#iterations: 201
currently lose_sum: 375.2547684311867
time_elpased: 1.079
batch start
#iterations: 202
currently lose_sum: 375.3699751496315
time_elpased: 1.072
batch start
#iterations: 203
currently lose_sum: 375.88636142015457
time_elpased: 1.087
batch start
#iterations: 204
currently lose_sum: 377.2266954779625
time_elpased: 1.084
batch start
#iterations: 205
currently lose_sum: 375.48637771606445
time_elpased: 1.075
batch start
#iterations: 206
currently lose_sum: 375.9829833507538
time_elpased: 1.088
batch start
#iterations: 207
currently lose_sum: 377.070836186409
time_elpased: 1.062
batch start
#iterations: 208
currently lose_sum: 376.2408428788185
time_elpased: 1.068
batch start
#iterations: 209
currently lose_sum: 376.24564200639725
time_elpased: 1.065
batch start
#iterations: 210
currently lose_sum: 375.69270408153534
time_elpased: 1.069
batch start
#iterations: 211
currently lose_sum: 376.4214225411415
time_elpased: 1.086
batch start
#iterations: 212
currently lose_sum: 376.65357226133347
time_elpased: 1.071
batch start
#iterations: 213
currently lose_sum: 376.9825918674469
time_elpased: 1.084
batch start
#iterations: 214
currently lose_sum: 377.02721804380417
time_elpased: 1.066
batch start
#iterations: 215
currently lose_sum: 376.7337894439697
time_elpased: 1.072
batch start
#iterations: 216
currently lose_sum: 376.95415037870407
time_elpased: 1.07
batch start
#iterations: 217
currently lose_sum: 375.52791005373
time_elpased: 1.077
batch start
#iterations: 218
currently lose_sum: 376.17451959848404
time_elpased: 1.07
batch start
#iterations: 219
currently lose_sum: 376.71719694137573
time_elpased: 1.076
start validation test
0.73
0.799297676931
0.611995863495
0.693217757995
0
validation finish
batch start
#iterations: 220
currently lose_sum: 376.3706374168396
time_elpased: 1.076
batch start
#iterations: 221
currently lose_sum: 376.60753655433655
time_elpased: 1.068
batch start
#iterations: 222
currently lose_sum: 376.6558914780617
time_elpased: 1.075
batch start
#iterations: 223
currently lose_sum: 376.46778547763824
time_elpased: 1.079
batch start
#iterations: 224
currently lose_sum: 376.9098957180977
time_elpased: 1.069
batch start
#iterations: 225
currently lose_sum: 375.43868869543076
time_elpased: 1.072
batch start
#iterations: 226
currently lose_sum: 377.02444565296173
time_elpased: 1.089
batch start
#iterations: 227
currently lose_sum: 376.80283826589584
time_elpased: 1.083
batch start
#iterations: 228
currently lose_sum: 375.51720130443573
time_elpased: 1.069
batch start
#iterations: 229
currently lose_sum: 376.11221319437027
time_elpased: 1.084
batch start
#iterations: 230
currently lose_sum: 377.0671655535698
time_elpased: 1.079
batch start
#iterations: 231
currently lose_sum: 376.3752586245537
time_elpased: 1.069
batch start
#iterations: 232
currently lose_sum: 376.4294434785843
time_elpased: 1.078
batch start
#iterations: 233
currently lose_sum: 376.8883594274521
time_elpased: 1.067
batch start
#iterations: 234
currently lose_sum: 376.48338705301285
time_elpased: 1.063
batch start
#iterations: 235
currently lose_sum: 375.1977560520172
time_elpased: 1.077
batch start
#iterations: 236
currently lose_sum: 376.09804463386536
time_elpased: 1.075
batch start
#iterations: 237
currently lose_sum: 376.05913001298904
time_elpased: 1.065
batch start
#iterations: 238
currently lose_sum: 376.8366949558258
time_elpased: 1.071
batch start
#iterations: 239
currently lose_sum: 376.0110141634941
time_elpased: 1.065
start validation test
0.730206185567
0.797319034853
0.615098241986
0.694454173964
0
validation finish
batch start
#iterations: 240
currently lose_sum: 375.7795878648758
time_elpased: 1.102
batch start
#iterations: 241
currently lose_sum: 375.4813394546509
time_elpased: 1.082
batch start
#iterations: 242
currently lose_sum: 374.61801421642303
time_elpased: 1.077
batch start
#iterations: 243
currently lose_sum: 377.29698979854584
time_elpased: 1.075
batch start
#iterations: 244
currently lose_sum: 376.64776480197906
time_elpased: 1.067
batch start
#iterations: 245
currently lose_sum: 375.4487814307213
time_elpased: 1.07
batch start
#iterations: 246
currently lose_sum: 376.6436527967453
time_elpased: 1.082
batch start
#iterations: 247
currently lose_sum: 376.8165862560272
time_elpased: 1.07
batch start
#iterations: 248
currently lose_sum: 377.07692670822144
time_elpased: 1.078
batch start
#iterations: 249
currently lose_sum: 376.16277635097504
time_elpased: 1.081
batch start
#iterations: 250
currently lose_sum: 375.72927153110504
time_elpased: 1.072
batch start
#iterations: 251
currently lose_sum: 377.8613011240959
time_elpased: 1.077
batch start
#iterations: 252
currently lose_sum: 376.58181923627853
time_elpased: 1.076
batch start
#iterations: 253
currently lose_sum: 375.63518065214157
time_elpased: 1.087
batch start
#iterations: 254
currently lose_sum: 376.98856526613235
time_elpased: 1.068
batch start
#iterations: 255
currently lose_sum: 377.4513965845108
time_elpased: 1.073
batch start
#iterations: 256
currently lose_sum: 375.60667085647583
time_elpased: 1.075
batch start
#iterations: 257
currently lose_sum: 376.75263983011246
time_elpased: 1.082
batch start
#iterations: 258
currently lose_sum: 377.7762290239334
time_elpased: 1.087
batch start
#iterations: 259
currently lose_sum: 375.7165355682373
time_elpased: 1.081
start validation test
0.733041237113
0.793567786639
0.627714581179
0.700964258906
0
validation finish
batch start
#iterations: 260
currently lose_sum: 376.10647344589233
time_elpased: 1.076
batch start
#iterations: 261
currently lose_sum: 375.0735607743263
time_elpased: 1.081
batch start
#iterations: 262
currently lose_sum: 376.65175664424896
time_elpased: 1.085
batch start
#iterations: 263
currently lose_sum: 378.1238650083542
time_elpased: 1.072
batch start
#iterations: 264
currently lose_sum: 376.63814598321915
time_elpased: 1.067
batch start
#iterations: 265
currently lose_sum: 375.965822994709
time_elpased: 1.069
batch start
#iterations: 266
currently lose_sum: 376.93963396549225
time_elpased: 1.075
batch start
#iterations: 267
currently lose_sum: 375.87703198194504
time_elpased: 1.072
batch start
#iterations: 268
currently lose_sum: 376.0267173051834
time_elpased: 1.073
batch start
#iterations: 269
currently lose_sum: 376.6390909552574
time_elpased: 1.075
batch start
#iterations: 270
currently lose_sum: 375.55277532339096
time_elpased: 1.087
batch start
#iterations: 271
currently lose_sum: 377.0374169945717
time_elpased: 1.071
batch start
#iterations: 272
currently lose_sum: 375.7884038090706
time_elpased: 1.085
batch start
#iterations: 273
currently lose_sum: 376.7889454960823
time_elpased: 1.08
batch start
#iterations: 274
currently lose_sum: 376.29817086458206
time_elpased: 1.067
batch start
#iterations: 275
currently lose_sum: 377.5439745783806
time_elpased: 1.072
batch start
#iterations: 276
currently lose_sum: 375.37375408411026
time_elpased: 1.083
batch start
#iterations: 277
currently lose_sum: 376.4347833991051
time_elpased: 1.076
batch start
#iterations: 278
currently lose_sum: 376.2074506878853
time_elpased: 1.085
batch start
#iterations: 279
currently lose_sum: 376.5559076666832
time_elpased: 1.075
start validation test
0.729020618557
0.801228668942
0.606928645295
0.690673727567
0
validation finish
batch start
#iterations: 280
currently lose_sum: 376.29573422670364
time_elpased: 1.084
batch start
#iterations: 281
currently lose_sum: 376.91997730731964
time_elpased: 1.071
batch start
#iterations: 282
currently lose_sum: 376.66581922769547
time_elpased: 1.076
batch start
#iterations: 283
currently lose_sum: 375.39396131038666
time_elpased: 1.091
batch start
#iterations: 284
currently lose_sum: 375.8928149342537
time_elpased: 1.072
batch start
#iterations: 285
currently lose_sum: 374.80271619558334
time_elpased: 1.068
batch start
#iterations: 286
currently lose_sum: 375.91590839624405
time_elpased: 1.078
batch start
#iterations: 287
currently lose_sum: 375.1500070095062
time_elpased: 1.074
batch start
#iterations: 288
currently lose_sum: 376.2218768000603
time_elpased: 1.08
batch start
#iterations: 289
currently lose_sum: 375.8736236691475
time_elpased: 1.078
batch start
#iterations: 290
currently lose_sum: 375.33096331357956
time_elpased: 1.08
batch start
#iterations: 291
currently lose_sum: 376.3067692518234
time_elpased: 1.083
batch start
#iterations: 292
currently lose_sum: 376.12718069553375
time_elpased: 1.075
batch start
#iterations: 293
currently lose_sum: 376.86484611034393
time_elpased: 1.078
batch start
#iterations: 294
currently lose_sum: 376.15571224689484
time_elpased: 1.072
batch start
#iterations: 295
currently lose_sum: 375.58762818574905
time_elpased: 1.083
batch start
#iterations: 296
currently lose_sum: 375.2009562253952
time_elpased: 1.072
batch start
#iterations: 297
currently lose_sum: 376.15322494506836
time_elpased: 1.075
batch start
#iterations: 298
currently lose_sum: 375.29129433631897
time_elpased: 1.084
batch start
#iterations: 299
currently lose_sum: 374.97719979286194
time_elpased: 1.079
start validation test
0.730051546392
0.791070256074
0.622957600827
0.697020538039
0
validation finish
batch start
#iterations: 300
currently lose_sum: 375.1844732761383
time_elpased: 1.096
batch start
#iterations: 301
currently lose_sum: 376.0607684850693
time_elpased: 1.082
batch start
#iterations: 302
currently lose_sum: 377.3088161945343
time_elpased: 1.091
batch start
#iterations: 303
currently lose_sum: 377.5890109539032
time_elpased: 1.077
batch start
#iterations: 304
currently lose_sum: 376.3865599036217
time_elpased: 1.092
batch start
#iterations: 305
currently lose_sum: 376.62883138656616
time_elpased: 1.076
batch start
#iterations: 306
currently lose_sum: 375.2982539534569
time_elpased: 1.073
batch start
#iterations: 307
currently lose_sum: 376.13366585969925
time_elpased: 1.078
batch start
#iterations: 308
currently lose_sum: 375.7925575375557
time_elpased: 1.079
batch start
#iterations: 309
currently lose_sum: 376.3308830857277
time_elpased: 1.089
batch start
#iterations: 310
currently lose_sum: 374.7446036338806
time_elpased: 1.068
batch start
#iterations: 311
currently lose_sum: 377.06561744213104
time_elpased: 1.07
batch start
#iterations: 312
currently lose_sum: 375.24239271879196
time_elpased: 1.069
batch start
#iterations: 313
currently lose_sum: 375.29343515634537
time_elpased: 1.098
batch start
#iterations: 314
currently lose_sum: 376.66773349046707
time_elpased: 1.083
batch start
#iterations: 315
currently lose_sum: 376.1955157518387
time_elpased: 1.069
batch start
#iterations: 316
currently lose_sum: 375.7284668684006
time_elpased: 1.075
batch start
#iterations: 317
currently lose_sum: 376.78282594680786
time_elpased: 1.068
batch start
#iterations: 318
currently lose_sum: 377.09847646951675
time_elpased: 1.085
batch start
#iterations: 319
currently lose_sum: 376.3598969578743
time_elpased: 1.069
start validation test
0.732835051546
0.791099000908
0.63050672182
0.701732174714
0
validation finish
batch start
#iterations: 320
currently lose_sum: 376.09375363588333
time_elpased: 1.083
batch start
#iterations: 321
currently lose_sum: 376.04574239254
time_elpased: 1.065
batch start
#iterations: 322
currently lose_sum: 376.2750900387764
time_elpased: 1.065
batch start
#iterations: 323
currently lose_sum: 376.5866664648056
time_elpased: 1.082
batch start
#iterations: 324
currently lose_sum: 375.62396562099457
time_elpased: 1.072
batch start
#iterations: 325
currently lose_sum: 377.02978122234344
time_elpased: 1.071
batch start
#iterations: 326
currently lose_sum: 376.12605398893356
time_elpased: 1.079
batch start
#iterations: 327
currently lose_sum: 375.6509932279587
time_elpased: 1.069
batch start
#iterations: 328
currently lose_sum: 375.0365235209465
time_elpased: 1.075
batch start
#iterations: 329
currently lose_sum: 375.84829223155975
time_elpased: 1.075
batch start
#iterations: 330
currently lose_sum: 375.7409453988075
time_elpased: 1.081
batch start
#iterations: 331
currently lose_sum: 375.8218396306038
time_elpased: 1.075
batch start
#iterations: 332
currently lose_sum: 375.4590599536896
time_elpased: 1.088
batch start
#iterations: 333
currently lose_sum: 375.6751288175583
time_elpased: 1.075
batch start
#iterations: 334
currently lose_sum: 376.44031143188477
time_elpased: 1.076
batch start
#iterations: 335
currently lose_sum: 375.40398186445236
time_elpased: 1.074
batch start
#iterations: 336
currently lose_sum: 375.84340929985046
time_elpased: 1.071
batch start
#iterations: 337
currently lose_sum: 375.1808883547783
time_elpased: 1.069
batch start
#iterations: 338
currently lose_sum: 376.71628987789154
time_elpased: 1.079
batch start
#iterations: 339
currently lose_sum: 376.8036844134331
time_elpased: 1.074
start validation test
0.730051546392
0.796046480566
0.616339193382
0.694760156204
0
validation finish
batch start
#iterations: 340
currently lose_sum: 376.3294929265976
time_elpased: 1.082
batch start
#iterations: 341
currently lose_sum: 376.60169583559036
time_elpased: 1.071
batch start
#iterations: 342
currently lose_sum: 375.86858308315277
time_elpased: 1.077
batch start
#iterations: 343
currently lose_sum: 375.6235226392746
time_elpased: 1.077
batch start
#iterations: 344
currently lose_sum: 375.896570622921
time_elpased: 1.091
batch start
#iterations: 345
currently lose_sum: 375.99555891752243
time_elpased: 1.084
batch start
#iterations: 346
currently lose_sum: 376.64840537309647
time_elpased: 1.081
batch start
#iterations: 347
currently lose_sum: 376.1859395503998
time_elpased: 1.094
batch start
#iterations: 348
currently lose_sum: 374.4206258058548
time_elpased: 1.064
batch start
#iterations: 349
currently lose_sum: 375.20057821273804
time_elpased: 1.07
batch start
#iterations: 350
currently lose_sum: 376.1961331963539
time_elpased: 1.081
batch start
#iterations: 351
currently lose_sum: 376.3354848623276
time_elpased: 1.073
batch start
#iterations: 352
currently lose_sum: 377.0291901230812
time_elpased: 1.074
batch start
#iterations: 353
currently lose_sum: 375.37355774641037
time_elpased: 1.073
batch start
#iterations: 354
currently lose_sum: 375.26410126686096
time_elpased: 1.083
batch start
#iterations: 355
currently lose_sum: 375.0586978197098
time_elpased: 1.08
batch start
#iterations: 356
currently lose_sum: 376.1156573295593
time_elpased: 1.085
batch start
#iterations: 357
currently lose_sum: 375.8549567461014
time_elpased: 1.076
batch start
#iterations: 358
currently lose_sum: 376.1764544248581
time_elpased: 1.072
batch start
#iterations: 359
currently lose_sum: 374.6201071739197
time_elpased: 1.079
start validation test
0.728298969072
0.800848037204
0.605480868666
0.6895942524
0
validation finish
batch start
#iterations: 360
currently lose_sum: 376.05601340532303
time_elpased: 1.08
batch start
#iterations: 361
currently lose_sum: 375.33890038728714
time_elpased: 1.074
batch start
#iterations: 362
currently lose_sum: 374.7788972258568
time_elpased: 1.084
batch start
#iterations: 363
currently lose_sum: 377.2095493674278
time_elpased: 1.069
batch start
#iterations: 364
currently lose_sum: 375.55743235349655
time_elpased: 1.08
batch start
#iterations: 365
currently lose_sum: 375.54077261686325
time_elpased: 1.078
batch start
#iterations: 366
currently lose_sum: 376.29160928726196
time_elpased: 1.08
batch start
#iterations: 367
currently lose_sum: 376.5383477807045
time_elpased: 1.076
batch start
#iterations: 368
currently lose_sum: 375.461018204689
time_elpased: 1.073
batch start
#iterations: 369
currently lose_sum: 374.84634643793106
time_elpased: 1.072
batch start
#iterations: 370
currently lose_sum: 374.8032366037369
time_elpased: 1.081
batch start
#iterations: 371
currently lose_sum: 376.54974269866943
time_elpased: 1.067
batch start
#iterations: 372
currently lose_sum: 375.58182793855667
time_elpased: 1.067
batch start
#iterations: 373
currently lose_sum: 376.6433799266815
time_elpased: 1.076
batch start
#iterations: 374
currently lose_sum: 376.0204564332962
time_elpased: 1.07
batch start
#iterations: 375
currently lose_sum: 375.0824398994446
time_elpased: 1.083
batch start
#iterations: 376
currently lose_sum: 375.3528039455414
time_elpased: 1.071
batch start
#iterations: 377
currently lose_sum: 375.6515054702759
time_elpased: 1.086
batch start
#iterations: 378
currently lose_sum: 375.948749601841
time_elpased: 1.065
batch start
#iterations: 379
currently lose_sum: 376.429595887661
time_elpased: 1.075
start validation test
0.731855670103
0.795894039735
0.621406411582
0.697909407666
0
validation finish
batch start
#iterations: 380
currently lose_sum: 375.76410472393036
time_elpased: 1.086
batch start
#iterations: 381
currently lose_sum: 376.60243260860443
time_elpased: 1.075
batch start
#iterations: 382
currently lose_sum: 375.50432723760605
time_elpased: 1.081
batch start
#iterations: 383
currently lose_sum: 375.5933673977852
time_elpased: 1.072
batch start
#iterations: 384
currently lose_sum: 375.1681478023529
time_elpased: 1.082
batch start
#iterations: 385
currently lose_sum: 375.40590113401413
time_elpased: 1.072
batch start
#iterations: 386
currently lose_sum: 377.4894682765007
time_elpased: 1.074
batch start
#iterations: 387
currently lose_sum: 375.58272010087967
time_elpased: 1.081
batch start
#iterations: 388
currently lose_sum: 375.6356797814369
time_elpased: 1.086
batch start
#iterations: 389
currently lose_sum: 375.2679636478424
time_elpased: 1.069
batch start
#iterations: 390
currently lose_sum: 376.1057956814766
time_elpased: 1.084
batch start
#iterations: 391
currently lose_sum: 374.85051226615906
time_elpased: 1.072
batch start
#iterations: 392
currently lose_sum: 375.053577542305
time_elpased: 1.077
batch start
#iterations: 393
currently lose_sum: 376.37514346838
time_elpased: 1.08
batch start
#iterations: 394
currently lose_sum: 375.3401635289192
time_elpased: 1.074
batch start
#iterations: 395
currently lose_sum: 375.72997546195984
time_elpased: 1.079
batch start
#iterations: 396
currently lose_sum: 376.34225153923035
time_elpased: 1.08
batch start
#iterations: 397
currently lose_sum: 374.9032755494118
time_elpased: 1.076
batch start
#iterations: 398
currently lose_sum: 376.26032596826553
time_elpased: 1.064
batch start
#iterations: 399
currently lose_sum: 375.2662580013275
time_elpased: 1.079
start validation test
0.730412371134
0.792028413575
0.622647362978
0.697197776748
0
validation finish
acc: 0.729
pre: 0.794
rec: 0.625
F1: 0.700
auc: 0.000
