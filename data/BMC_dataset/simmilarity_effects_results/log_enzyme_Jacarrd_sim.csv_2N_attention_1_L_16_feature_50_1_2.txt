start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 563.8144896626472
time_elpased: 1.656
batch start
#iterations: 1
currently lose_sum: 547.3612794876099
time_elpased: 1.622
batch start
#iterations: 2
currently lose_sum: 540.9696715176105
time_elpased: 1.635
batch start
#iterations: 3
currently lose_sum: 537.0144852995872
time_elpased: 1.612
batch start
#iterations: 4
currently lose_sum: 535.3697805404663
time_elpased: 1.608
batch start
#iterations: 5
currently lose_sum: 535.347905009985
time_elpased: 1.626
batch start
#iterations: 6
currently lose_sum: 530.9323625564575
time_elpased: 1.64
batch start
#iterations: 7
currently lose_sum: 532.5550593435764
time_elpased: 1.62
batch start
#iterations: 8
currently lose_sum: 530.1997438967228
time_elpased: 1.622
batch start
#iterations: 9
currently lose_sum: 529.7483744621277
time_elpased: 1.614
batch start
#iterations: 10
currently lose_sum: 529.1737601459026
time_elpased: 1.641
batch start
#iterations: 11
currently lose_sum: 530.4248577356339
time_elpased: 1.64
batch start
#iterations: 12
currently lose_sum: 527.8409753143787
time_elpased: 1.618
batch start
#iterations: 13
currently lose_sum: 527.3316762149334
time_elpased: 1.623
batch start
#iterations: 14
currently lose_sum: 527.8213065564632
time_elpased: 1.626
batch start
#iterations: 15
currently lose_sum: 525.4998523294926
time_elpased: 1.623
batch start
#iterations: 16
currently lose_sum: 526.680306494236
time_elpased: 1.613
batch start
#iterations: 17
currently lose_sum: 526.8707441389561
time_elpased: 1.614
batch start
#iterations: 18
currently lose_sum: 526.5948653817177
time_elpased: 1.632
batch start
#iterations: 19
currently lose_sum: 526.1010705828667
time_elpased: 1.618
start validation test
0.589175257732
0.915851272016
0.193588417787
0.319617551648
0
validation finish
batch start
#iterations: 20
currently lose_sum: 525.6312257945538
time_elpased: 1.642
batch start
#iterations: 21
currently lose_sum: 528.165888428688
time_elpased: 1.612
batch start
#iterations: 22
currently lose_sum: 525.2565560936928
time_elpased: 1.619
batch start
#iterations: 23
currently lose_sum: 525.7949221134186
time_elpased: 1.622
batch start
#iterations: 24
currently lose_sum: 524.0357454419136
time_elpased: 1.623
batch start
#iterations: 25
currently lose_sum: 527.311463534832
time_elpased: 1.64
batch start
#iterations: 26
currently lose_sum: 524.6802852451801
time_elpased: 1.615
batch start
#iterations: 27
currently lose_sum: 524.9864860773087
time_elpased: 1.615
batch start
#iterations: 28
currently lose_sum: 522.4650434851646
time_elpased: 1.613
batch start
#iterations: 29
currently lose_sum: 522.8766501843929
time_elpased: 1.621
batch start
#iterations: 30
currently lose_sum: 523.1647525727749
time_elpased: 1.63
batch start
#iterations: 31
currently lose_sum: 522.7407888770103
time_elpased: 1.624
batch start
#iterations: 32
currently lose_sum: 525.1398028433323
time_elpased: 1.625
batch start
#iterations: 33
currently lose_sum: 522.8019888997078
time_elpased: 1.633
batch start
#iterations: 34
currently lose_sum: 523.9746497273445
time_elpased: 1.619
batch start
#iterations: 35
currently lose_sum: 521.5065436661243
time_elpased: 1.634
batch start
#iterations: 36
currently lose_sum: 522.5723057091236
time_elpased: 1.619
batch start
#iterations: 37
currently lose_sum: 521.9274581372738
time_elpased: 1.619
batch start
#iterations: 38
currently lose_sum: 523.2663488090038
time_elpased: 1.644
batch start
#iterations: 39
currently lose_sum: 523.7028660178185
time_elpased: 1.614
start validation test
0.563865979381
0.966075558982
0.129576008273
0.228503692897
0
validation finish
batch start
#iterations: 40
currently lose_sum: 524.1960312724113
time_elpased: 1.628
batch start
#iterations: 41
currently lose_sum: 524.2479221522808
time_elpased: 1.613
batch start
#iterations: 42
currently lose_sum: 521.9121624231339
time_elpased: 1.62
batch start
#iterations: 43
currently lose_sum: 521.9105175733566
time_elpased: 1.612
batch start
#iterations: 44
currently lose_sum: 521.3787488937378
time_elpased: 1.612
batch start
#iterations: 45
currently lose_sum: 521.7023181915283
time_elpased: 1.627
batch start
#iterations: 46
currently lose_sum: 521.9331066310406
time_elpased: 1.626
batch start
#iterations: 47
currently lose_sum: 522.2085031867027
time_elpased: 1.633
batch start
#iterations: 48
currently lose_sum: 521.0293020308018
time_elpased: 1.621
batch start
#iterations: 49
currently lose_sum: 521.617884427309
time_elpased: 1.618
batch start
#iterations: 50
currently lose_sum: 523.5096192061901
time_elpased: 1.622
batch start
#iterations: 51
currently lose_sum: 522.003135561943
time_elpased: 1.631
batch start
#iterations: 52
currently lose_sum: 521.8533436655998
time_elpased: 1.613
batch start
#iterations: 53
currently lose_sum: 523.2107233703136
time_elpased: 1.615
batch start
#iterations: 54
currently lose_sum: 520.5708439052105
time_elpased: 1.625
batch start
#iterations: 55
currently lose_sum: 523.0719053149223
time_elpased: 1.622
batch start
#iterations: 56
currently lose_sum: 522.3674430847168
time_elpased: 1.617
batch start
#iterations: 57
currently lose_sum: 520.6090948283672
time_elpased: 1.628
batch start
#iterations: 58
currently lose_sum: 522.6010093986988
time_elpased: 1.623
batch start
#iterations: 59
currently lose_sum: 520.1010110974312
time_elpased: 1.628
start validation test
0.586649484536
0.942153186931
0.181902792141
0.304931958048
0
validation finish
batch start
#iterations: 60
currently lose_sum: 522.251956731081
time_elpased: 1.633
batch start
#iterations: 61
currently lose_sum: 522.1705946326256
time_elpased: 1.617
batch start
#iterations: 62
currently lose_sum: 523.4992790818214
time_elpased: 1.623
batch start
#iterations: 63
currently lose_sum: 522.7485834658146
time_elpased: 1.621
batch start
#iterations: 64
currently lose_sum: 523.0185180604458
time_elpased: 1.626
batch start
#iterations: 65
currently lose_sum: 522.3586227893829
time_elpased: 1.613
batch start
#iterations: 66
currently lose_sum: 522.1518914103508
time_elpased: 1.64
batch start
#iterations: 67
currently lose_sum: 519.6250662505627
time_elpased: 1.627
batch start
#iterations: 68
currently lose_sum: 523.0981722772121
time_elpased: 1.612
batch start
#iterations: 69
currently lose_sum: 522.3594055771828
time_elpased: 1.627
batch start
#iterations: 70
currently lose_sum: 522.0424545109272
time_elpased: 1.618
batch start
#iterations: 71
currently lose_sum: 522.0107476115227
time_elpased: 1.629
batch start
#iterations: 72
currently lose_sum: 521.6290555298328
time_elpased: 1.62
batch start
#iterations: 73
currently lose_sum: 519.4572674036026
time_elpased: 1.612
batch start
#iterations: 74
currently lose_sum: 521.4873948395252
time_elpased: 1.617
batch start
#iterations: 75
currently lose_sum: 520.9510162770748
time_elpased: 1.61
batch start
#iterations: 76
currently lose_sum: 523.2557993531227
time_elpased: 1.619
batch start
#iterations: 77
currently lose_sum: 520.6498527824879
time_elpased: 1.641
batch start
#iterations: 78
currently lose_sum: 522.3487206101418
time_elpased: 1.643
batch start
#iterations: 79
currently lose_sum: 522.5294993519783
time_elpased: 1.623
start validation test
0.618969072165
0.92627245509
0.25594622544
0.401069518717
0
validation finish
batch start
#iterations: 80
currently lose_sum: 520.5678324997425
time_elpased: 1.635
batch start
#iterations: 81
currently lose_sum: 521.3449229896069
time_elpased: 1.626
batch start
#iterations: 82
currently lose_sum: 521.9188256263733
time_elpased: 1.632
batch start
#iterations: 83
currently lose_sum: 521.7301651835442
time_elpased: 1.625
batch start
#iterations: 84
currently lose_sum: 522.4904381036758
time_elpased: 1.643
batch start
#iterations: 85
currently lose_sum: 519.9539441764355
time_elpased: 1.626
batch start
#iterations: 86
currently lose_sum: 519.8922843635082
time_elpased: 1.633
batch start
#iterations: 87
currently lose_sum: 519.9401827454567
time_elpased: 1.638
batch start
#iterations: 88
currently lose_sum: 521.1791643798351
time_elpased: 1.627
batch start
#iterations: 89
currently lose_sum: 521.594359099865
time_elpased: 1.617
batch start
#iterations: 90
currently lose_sum: 522.6499295830727
time_elpased: 1.622
batch start
#iterations: 91
currently lose_sum: 519.6573017239571
time_elpased: 1.638
batch start
#iterations: 92
currently lose_sum: 522.2746891379356
time_elpased: 1.615
batch start
#iterations: 93
currently lose_sum: 520.1240371763706
time_elpased: 1.623
batch start
#iterations: 94
currently lose_sum: 520.3999540507793
time_elpased: 1.628
batch start
#iterations: 95
currently lose_sum: 520.1336694955826
time_elpased: 1.614
batch start
#iterations: 96
currently lose_sum: 521.9930500984192
time_elpased: 1.638
batch start
#iterations: 97
currently lose_sum: 520.4649255275726
time_elpased: 1.633
batch start
#iterations: 98
currently lose_sum: 521.0466460883617
time_elpased: 1.636
batch start
#iterations: 99
currently lose_sum: 520.9704339504242
time_elpased: 1.63
start validation test
0.551443298969
0.959203036053
0.104550155119
0.188549048862
0
validation finish
batch start
#iterations: 100
currently lose_sum: 520.8723374903202
time_elpased: 1.621
batch start
#iterations: 101
currently lose_sum: 520.9165489077568
time_elpased: 1.623
batch start
#iterations: 102
currently lose_sum: 518.3561612665653
time_elpased: 1.627
batch start
#iterations: 103
currently lose_sum: 520.2157314121723
time_elpased: 1.635
batch start
#iterations: 104
currently lose_sum: 521.2732128798962
time_elpased: 1.606
batch start
#iterations: 105
currently lose_sum: 519.929882645607
time_elpased: 1.635
batch start
#iterations: 106
currently lose_sum: 522.0051712095737
time_elpased: 1.624
batch start
#iterations: 107
currently lose_sum: 521.3982407450676
time_elpased: 1.616
batch start
#iterations: 108
currently lose_sum: 518.4073843061924
time_elpased: 1.625
batch start
#iterations: 109
currently lose_sum: 521.8334204554558
time_elpased: 1.631
batch start
#iterations: 110
currently lose_sum: 520.780604660511
time_elpased: 1.624
batch start
#iterations: 111
currently lose_sum: 522.7544840276241
time_elpased: 1.629
batch start
#iterations: 112
currently lose_sum: 519.0456193983555
time_elpased: 1.634
batch start
#iterations: 113
currently lose_sum: 520.6811698675156
time_elpased: 1.625
batch start
#iterations: 114
currently lose_sum: 520.812781482935
time_elpased: 1.631
batch start
#iterations: 115
currently lose_sum: 520.3819710016251
time_elpased: 1.645
batch start
#iterations: 116
currently lose_sum: 518.7936420440674
time_elpased: 1.615
batch start
#iterations: 117
currently lose_sum: 521.1174598038197
time_elpased: 1.622
batch start
#iterations: 118
currently lose_sum: 519.874153137207
time_elpased: 1.631
batch start
#iterations: 119
currently lose_sum: 519.2106528282166
time_elpased: 1.628
start validation test
0.596082474227
0.929307116105
0.205274043433
0.336269693376
0
validation finish
batch start
#iterations: 120
currently lose_sum: 520.4097119271755
time_elpased: 1.639
batch start
#iterations: 121
currently lose_sum: 518.8068491220474
time_elpased: 1.631
batch start
#iterations: 122
currently lose_sum: 518.3272384703159
time_elpased: 1.634
batch start
#iterations: 123
currently lose_sum: 519.1473673284054
time_elpased: 1.626
batch start
#iterations: 124
currently lose_sum: 519.3902882039547
time_elpased: 1.629
batch start
#iterations: 125
currently lose_sum: 520.237467110157
time_elpased: 1.619
batch start
#iterations: 126
currently lose_sum: 518.7498767971992
time_elpased: 1.628
batch start
#iterations: 127
currently lose_sum: 521.3689827024937
time_elpased: 1.629
batch start
#iterations: 128
currently lose_sum: 519.61142629385
time_elpased: 1.627
batch start
#iterations: 129
currently lose_sum: 520.2477861046791
time_elpased: 1.624
batch start
#iterations: 130
currently lose_sum: 519.3560184836388
time_elpased: 1.633
batch start
#iterations: 131
currently lose_sum: 519.1138103306293
time_elpased: 1.623
batch start
#iterations: 132
currently lose_sum: 520.6380604803562
time_elpased: 1.639
batch start
#iterations: 133
currently lose_sum: 519.5080143213272
time_elpased: 1.621
batch start
#iterations: 134
currently lose_sum: 520.5374101102352
time_elpased: 1.628
batch start
#iterations: 135
currently lose_sum: 518.0429152548313
time_elpased: 1.626
batch start
#iterations: 136
currently lose_sum: 519.6030432879925
time_elpased: 1.637
batch start
#iterations: 137
currently lose_sum: 517.8006830215454
time_elpased: 1.619
batch start
#iterations: 138
currently lose_sum: 519.9149926006794
time_elpased: 1.628
batch start
#iterations: 139
currently lose_sum: 520.826366007328
time_elpased: 1.64
start validation test
0.549432989691
0.936970837253
0.102998965874
0.185595825957
0
validation finish
batch start
#iterations: 140
currently lose_sum: 517.0964443683624
time_elpased: 1.633
batch start
#iterations: 141
currently lose_sum: 519.91608273983
time_elpased: 1.623
batch start
#iterations: 142
currently lose_sum: 519.8084364235401
time_elpased: 1.624
batch start
#iterations: 143
currently lose_sum: 521.6861311495304
time_elpased: 1.63
batch start
#iterations: 144
currently lose_sum: 520.9610605835915
time_elpased: 1.637
batch start
#iterations: 145
currently lose_sum: 520.2779579162598
time_elpased: 1.63
batch start
#iterations: 146
currently lose_sum: 521.452214807272
time_elpased: 1.632
batch start
#iterations: 147
currently lose_sum: 520.4594076871872
time_elpased: 1.617
batch start
#iterations: 148
currently lose_sum: 518.4428535103798
time_elpased: 1.629
batch start
#iterations: 149
currently lose_sum: 522.6209990382195
time_elpased: 1.64
batch start
#iterations: 150
currently lose_sum: 519.8072030246258
time_elpased: 1.626
batch start
#iterations: 151
currently lose_sum: 519.4271662831306
time_elpased: 1.629
batch start
#iterations: 152
currently lose_sum: 519.9850963354111
time_elpased: 1.645
batch start
#iterations: 153
currently lose_sum: 517.2436280548573
time_elpased: 1.629
batch start
#iterations: 154
currently lose_sum: 522.0739704966545
time_elpased: 1.634
batch start
#iterations: 155
currently lose_sum: 518.1356141865253
time_elpased: 1.625
batch start
#iterations: 156
currently lose_sum: 519.5763549208641
time_elpased: 1.624
batch start
#iterations: 157
currently lose_sum: 518.9590827226639
time_elpased: 1.66
batch start
#iterations: 158
currently lose_sum: 517.8869171738625
time_elpased: 1.642
batch start
#iterations: 159
currently lose_sum: 520.4418173730373
time_elpased: 1.63
start validation test
0.585154639175
0.936961206897
0.179834539814
0.301752559431
0
validation finish
batch start
#iterations: 160
currently lose_sum: 520.7993167340755
time_elpased: 1.632
batch start
#iterations: 161
currently lose_sum: 520.3429202437401
time_elpased: 1.638
batch start
#iterations: 162
currently lose_sum: 518.536758184433
time_elpased: 1.627
batch start
#iterations: 163
currently lose_sum: 517.8958252370358
time_elpased: 1.621
batch start
#iterations: 164
currently lose_sum: 520.9515823125839
time_elpased: 1.626
batch start
#iterations: 165
currently lose_sum: 518.915563493967
time_elpased: 1.619
batch start
#iterations: 166
currently lose_sum: 520.5374336838722
time_elpased: 1.61
batch start
#iterations: 167
currently lose_sum: 520.9073566198349
time_elpased: 1.621
batch start
#iterations: 168
currently lose_sum: 519.9498706758022
time_elpased: 1.641
batch start
#iterations: 169
currently lose_sum: 522.1068652868271
time_elpased: 1.646
batch start
#iterations: 170
currently lose_sum: 520.1491547524929
time_elpased: 1.621
batch start
#iterations: 171
currently lose_sum: 518.4447893500328
time_elpased: 1.606
batch start
#iterations: 172
currently lose_sum: 521.1502917408943
time_elpased: 1.628
batch start
#iterations: 173
currently lose_sum: 519.8801724910736
time_elpased: 1.629
batch start
#iterations: 174
currently lose_sum: 520.9890592396259
time_elpased: 1.627
batch start
#iterations: 175
currently lose_sum: 519.2002281844616
time_elpased: 1.624
batch start
#iterations: 176
currently lose_sum: 519.3893142640591
time_elpased: 1.631
batch start
#iterations: 177
currently lose_sum: 520.8978862762451
time_elpased: 1.631
batch start
#iterations: 178
currently lose_sum: 519.4542796313763
time_elpased: 1.632
batch start
#iterations: 179
currently lose_sum: 519.1706987321377
time_elpased: 1.629
start validation test
0.60587628866
0.927726120034
0.226990692865
0.364739115985
0
validation finish
batch start
#iterations: 180
currently lose_sum: 520.1901413798332
time_elpased: 1.635
batch start
#iterations: 181
currently lose_sum: 521.9007377624512
time_elpased: 1.635
batch start
#iterations: 182
currently lose_sum: 518.5216227769852
time_elpased: 1.642
batch start
#iterations: 183
currently lose_sum: 520.8920166790485
time_elpased: 1.631
batch start
#iterations: 184
currently lose_sum: 519.7655865848064
time_elpased: 1.626
batch start
#iterations: 185
currently lose_sum: 518.7637241482735
time_elpased: 1.628
batch start
#iterations: 186
currently lose_sum: 521.5278311371803
time_elpased: 1.62
batch start
#iterations: 187
currently lose_sum: 519.8186525404453
time_elpased: 1.614
batch start
#iterations: 188
currently lose_sum: 517.8254223763943
time_elpased: 1.641
batch start
#iterations: 189
currently lose_sum: 518.7784978747368
time_elpased: 1.618
batch start
#iterations: 190
currently lose_sum: 518.0640231668949
time_elpased: 1.633
batch start
#iterations: 191
currently lose_sum: 520.1472559273243
time_elpased: 1.645
batch start
#iterations: 192
currently lose_sum: 519.2525741755962
time_elpased: 1.616
batch start
#iterations: 193
currently lose_sum: 518.3485670983791
time_elpased: 1.634
batch start
#iterations: 194
currently lose_sum: 520.7194048762321
time_elpased: 1.63
batch start
#iterations: 195
currently lose_sum: 520.0319878757
time_elpased: 1.643
batch start
#iterations: 196
currently lose_sum: 518.7695738971233
time_elpased: 1.638
batch start
#iterations: 197
currently lose_sum: 521.1326477527618
time_elpased: 1.622
batch start
#iterations: 198
currently lose_sum: 519.1597954630852
time_elpased: 1.639
batch start
#iterations: 199
currently lose_sum: 519.9466309547424
time_elpased: 1.628
start validation test
0.567628865979
0.939643347051
0.141675284385
0.24622573688
0
validation finish
batch start
#iterations: 200
currently lose_sum: 520.9022984206676
time_elpased: 1.629
batch start
#iterations: 201
currently lose_sum: 519.754721224308
time_elpased: 1.622
batch start
#iterations: 202
currently lose_sum: 519.0971177220345
time_elpased: 1.644
batch start
#iterations: 203
currently lose_sum: 518.8638590276241
time_elpased: 1.616
batch start
#iterations: 204
currently lose_sum: 520.1524079442024
time_elpased: 1.623
batch start
#iterations: 205
currently lose_sum: 520.0639972090721
time_elpased: 1.634
batch start
#iterations: 206
currently lose_sum: 520.1576665639877
time_elpased: 1.626
batch start
#iterations: 207
currently lose_sum: 518.7800625264645
time_elpased: 1.62
batch start
#iterations: 208
currently lose_sum: 518.6287997066975
time_elpased: 1.647
batch start
#iterations: 209
currently lose_sum: 519.3753342330456
time_elpased: 1.635
batch start
#iterations: 210
currently lose_sum: 521.0776413083076
time_elpased: 1.623
batch start
#iterations: 211
currently lose_sum: 520.6612034142017
time_elpased: 1.636
batch start
#iterations: 212
currently lose_sum: 519.8338591754436
time_elpased: 1.624
batch start
#iterations: 213
currently lose_sum: 518.905466735363
time_elpased: 1.624
batch start
#iterations: 214
currently lose_sum: 520.291598290205
time_elpased: 1.625
batch start
#iterations: 215
currently lose_sum: 518.8679836690426
time_elpased: 1.627
batch start
#iterations: 216
currently lose_sum: 519.3033810853958
time_elpased: 1.625
batch start
#iterations: 217
currently lose_sum: 518.6376404166222
time_elpased: 1.627
batch start
#iterations: 218
currently lose_sum: 521.3008264601231
time_elpased: 1.623
batch start
#iterations: 219
currently lose_sum: 520.3097036778927
time_elpased: 1.63
start validation test
0.570103092784
0.935779816514
0.147673216132
0.255091103966
0
validation finish
batch start
#iterations: 220
currently lose_sum: 518.0155243873596
time_elpased: 1.637
batch start
#iterations: 221
currently lose_sum: 521.085509210825
time_elpased: 1.614
batch start
#iterations: 222
currently lose_sum: 518.8675756454468
time_elpased: 1.629
batch start
#iterations: 223
currently lose_sum: 520.4382163584232
time_elpased: 1.631
batch start
#iterations: 224
currently lose_sum: 519.7642186284065
time_elpased: 1.637
batch start
#iterations: 225
currently lose_sum: 521.787318110466
time_elpased: 1.636
batch start
#iterations: 226
currently lose_sum: 520.4633221030235
time_elpased: 1.624
batch start
#iterations: 227
currently lose_sum: 518.497145742178
time_elpased: 1.624
batch start
#iterations: 228
currently lose_sum: 517.2506072223186
time_elpased: 1.623
batch start
#iterations: 229
currently lose_sum: 517.4885265827179
time_elpased: 1.635
batch start
#iterations: 230
currently lose_sum: 521.5201290249825
time_elpased: 1.629
batch start
#iterations: 231
currently lose_sum: 522.3252931237221
time_elpased: 1.614
batch start
#iterations: 232
currently lose_sum: 518.3163907825947
time_elpased: 1.629
batch start
#iterations: 233
currently lose_sum: 517.182304829359
time_elpased: 1.631
batch start
#iterations: 234
currently lose_sum: 518.2414067089558
time_elpased: 1.623
batch start
#iterations: 235
currently lose_sum: 518.186864823103
time_elpased: 1.621
batch start
#iterations: 236
currently lose_sum: 518.7183701694012
time_elpased: 1.626
batch start
#iterations: 237
currently lose_sum: 519.2707858681679
time_elpased: 1.629
batch start
#iterations: 238
currently lose_sum: 518.3809184134007
time_elpased: 1.625
batch start
#iterations: 239
currently lose_sum: 517.5877899825573
time_elpased: 1.622
start validation test
0.545
0.93769470405
0.0933815925543
0.169848584595
0
validation finish
batch start
#iterations: 240
currently lose_sum: 518.8166562616825
time_elpased: 1.663
batch start
#iterations: 241
currently lose_sum: 519.7557440400124
time_elpased: 1.626
batch start
#iterations: 242
currently lose_sum: 518.0961991250515
time_elpased: 1.642
batch start
#iterations: 243
currently lose_sum: 518.4110639691353
time_elpased: 1.644
batch start
#iterations: 244
currently lose_sum: 519.7045405507088
time_elpased: 1.626
batch start
#iterations: 245
currently lose_sum: 518.2592711150646
time_elpased: 1.637
batch start
#iterations: 246
currently lose_sum: 518.9408927261829
time_elpased: 1.635
batch start
#iterations: 247
currently lose_sum: 519.4948831498623
time_elpased: 1.63
batch start
#iterations: 248
currently lose_sum: 519.0090842545033
time_elpased: 1.623
batch start
#iterations: 249
currently lose_sum: 519.1107675433159
time_elpased: 1.627
batch start
#iterations: 250
currently lose_sum: 519.3215978443623
time_elpased: 1.642
batch start
#iterations: 251
currently lose_sum: 521.1918870210648
time_elpased: 1.625
batch start
#iterations: 252
currently lose_sum: 520.3206067979336
time_elpased: 1.629
batch start
#iterations: 253
currently lose_sum: 517.7069675028324
time_elpased: 1.634
batch start
#iterations: 254
currently lose_sum: 520.0668849349022
time_elpased: 1.622
batch start
#iterations: 255
currently lose_sum: 518.9640946984291
time_elpased: 1.62
batch start
#iterations: 256
currently lose_sum: 517.8226245045662
time_elpased: 1.638
batch start
#iterations: 257
currently lose_sum: 517.7776833176613
time_elpased: 1.62
batch start
#iterations: 258
currently lose_sum: 520.9008327126503
time_elpased: 1.634
batch start
#iterations: 259
currently lose_sum: 519.285130918026
time_elpased: 1.634
start validation test
0.600206185567
0.927996422182
0.214581178904
0.34856374937
0
validation finish
batch start
#iterations: 260
currently lose_sum: 521.2397749722004
time_elpased: 1.638
batch start
#iterations: 261
currently lose_sum: 520.4998372495174
time_elpased: 1.639
batch start
#iterations: 262
currently lose_sum: 518.9362671971321
time_elpased: 1.631
batch start
#iterations: 263
currently lose_sum: 518.6925513744354
time_elpased: 1.658
batch start
#iterations: 264
currently lose_sum: 518.7694769203663
time_elpased: 1.623
batch start
#iterations: 265
currently lose_sum: 519.3535716533661
time_elpased: 1.629
batch start
#iterations: 266
currently lose_sum: 518.7858457267284
time_elpased: 1.647
batch start
#iterations: 267
currently lose_sum: 518.5945287942886
time_elpased: 1.633
batch start
#iterations: 268
currently lose_sum: 518.3744523227215
time_elpased: 1.636
batch start
#iterations: 269
currently lose_sum: 518.7131028473377
time_elpased: 1.622
batch start
#iterations: 270
currently lose_sum: 518.5028867423534
time_elpased: 1.633
batch start
#iterations: 271
currently lose_sum: 517.2614801228046
time_elpased: 1.626
batch start
#iterations: 272
currently lose_sum: 519.3236745595932
time_elpased: 1.619
batch start
#iterations: 273
currently lose_sum: 517.4708578884602
time_elpased: 1.626
batch start
#iterations: 274
currently lose_sum: 518.6593842804432
time_elpased: 1.623
batch start
#iterations: 275
currently lose_sum: 518.0449510812759
time_elpased: 1.63
batch start
#iterations: 276
currently lose_sum: 519.0432848632336
time_elpased: 1.626
batch start
#iterations: 277
currently lose_sum: 520.8554391562939
time_elpased: 1.616
batch start
#iterations: 278
currently lose_sum: 520.2701159715652
time_elpased: 1.64
batch start
#iterations: 279
currently lose_sum: 521.1256506741047
time_elpased: 1.629
start validation test
0.580103092784
0.944574095683
0.167425025853
0.284434293746
0
validation finish
batch start
#iterations: 280
currently lose_sum: 518.1086623370647
time_elpased: 1.63
batch start
#iterations: 281
currently lose_sum: 517.0960753262043
time_elpased: 1.629
batch start
#iterations: 282
currently lose_sum: 519.8723155260086
time_elpased: 1.63
batch start
#iterations: 283
currently lose_sum: 518.2185726463795
time_elpased: 1.611
batch start
#iterations: 284
currently lose_sum: 519.2404848635197
time_elpased: 1.634
batch start
#iterations: 285
currently lose_sum: 520.5241341888905
time_elpased: 1.628
batch start
#iterations: 286
currently lose_sum: 522.3341152071953
time_elpased: 1.649
batch start
#iterations: 287
currently lose_sum: 519.5047733783722
time_elpased: 1.626
batch start
#iterations: 288
currently lose_sum: 519.8559584021568
time_elpased: 1.617
batch start
#iterations: 289
currently lose_sum: 519.5628263056278
time_elpased: 1.627
batch start
#iterations: 290
currently lose_sum: 518.5886354446411
time_elpased: 1.633
batch start
#iterations: 291
currently lose_sum: 519.3936964571476
time_elpased: 1.636
batch start
#iterations: 292
currently lose_sum: 519.2813718020916
time_elpased: 1.629
batch start
#iterations: 293
currently lose_sum: 520.2132995724678
time_elpased: 1.628
batch start
#iterations: 294
currently lose_sum: 520.3057863712311
time_elpased: 1.64
batch start
#iterations: 295
currently lose_sum: 519.5023861527443
time_elpased: 1.629
batch start
#iterations: 296
currently lose_sum: 518.3570178151131
time_elpased: 1.624
batch start
#iterations: 297
currently lose_sum: 519.9809331297874
time_elpased: 1.629
batch start
#iterations: 298
currently lose_sum: 517.9620853960514
time_elpased: 1.633
batch start
#iterations: 299
currently lose_sum: 519.4952000379562
time_elpased: 1.632
start validation test
0.595154639175
0.936958614052
0.201344364012
0.331460674157
0
validation finish
batch start
#iterations: 300
currently lose_sum: 518.2094992995262
time_elpased: 1.657
batch start
#iterations: 301
currently lose_sum: 518.9315726161003
time_elpased: 1.639
batch start
#iterations: 302
currently lose_sum: 520.878252863884
time_elpased: 1.624
batch start
#iterations: 303
currently lose_sum: 522.2174577116966
time_elpased: 1.613
batch start
#iterations: 304
currently lose_sum: 516.3275846540928
time_elpased: 1.63
batch start
#iterations: 305
currently lose_sum: 519.5522053539753
time_elpased: 1.638
batch start
#iterations: 306
currently lose_sum: 519.1287179291248
time_elpased: 1.623
batch start
#iterations: 307
currently lose_sum: 520.3894284963608
time_elpased: 1.634
batch start
#iterations: 308
currently lose_sum: 518.2482713460922
time_elpased: 1.633
batch start
#iterations: 309
currently lose_sum: 519.1242988407612
time_elpased: 1.628
batch start
#iterations: 310
currently lose_sum: 518.336296826601
time_elpased: 1.632
batch start
#iterations: 311
currently lose_sum: 517.6449280977249
time_elpased: 1.627
batch start
#iterations: 312
currently lose_sum: 520.3810117542744
time_elpased: 1.629
batch start
#iterations: 313
currently lose_sum: 521.1454408466816
time_elpased: 1.616
batch start
#iterations: 314
currently lose_sum: 518.0387779772282
time_elpased: 1.622
batch start
#iterations: 315
currently lose_sum: 518.8203465044498
time_elpased: 1.625
batch start
#iterations: 316
currently lose_sum: 518.9783701002598
time_elpased: 1.61
batch start
#iterations: 317
currently lose_sum: 519.219324439764
time_elpased: 1.639
batch start
#iterations: 318
currently lose_sum: 520.6565254926682
time_elpased: 1.633
batch start
#iterations: 319
currently lose_sum: 519.5714968144894
time_elpased: 1.628
start validation test
0.594226804124
0.933461909354
0.200206825233
0.32970027248
0
validation finish
batch start
#iterations: 320
currently lose_sum: 519.7723500132561
time_elpased: 1.655
batch start
#iterations: 321
currently lose_sum: 521.0665712952614
time_elpased: 1.629
batch start
#iterations: 322
currently lose_sum: 517.7930291593075
time_elpased: 1.628
batch start
#iterations: 323
currently lose_sum: 516.754819303751
time_elpased: 1.631
batch start
#iterations: 324
currently lose_sum: 520.9519811570644
time_elpased: 1.64
batch start
#iterations: 325
currently lose_sum: 516.67850664258
time_elpased: 1.625
batch start
#iterations: 326
currently lose_sum: 517.5096237659454
time_elpased: 1.639
batch start
#iterations: 327
currently lose_sum: 519.8075989484787
time_elpased: 1.629
batch start
#iterations: 328
currently lose_sum: 520.1200996339321
time_elpased: 1.627
batch start
#iterations: 329
currently lose_sum: 519.7733681499958
time_elpased: 1.628
batch start
#iterations: 330
currently lose_sum: 520.6571327149868
time_elpased: 1.64
batch start
#iterations: 331
currently lose_sum: 521.614524513483
time_elpased: 1.635
batch start
#iterations: 332
currently lose_sum: 519.6528109014034
time_elpased: 1.631
batch start
#iterations: 333
currently lose_sum: 518.5972636044025
time_elpased: 1.627
batch start
#iterations: 334
currently lose_sum: 517.6744491755962
time_elpased: 1.637
batch start
#iterations: 335
currently lose_sum: 518.4458823502064
time_elpased: 1.632
batch start
#iterations: 336
currently lose_sum: 520.3764745295048
time_elpased: 1.636
batch start
#iterations: 337
currently lose_sum: 520.552546441555
time_elpased: 1.629
batch start
#iterations: 338
currently lose_sum: 519.2912856042385
time_elpased: 1.633
batch start
#iterations: 339
currently lose_sum: 519.1391524672508
time_elpased: 1.626
start validation test
0.585670103093
0.93729903537
0.180868665977
0.303224687933
0
validation finish
batch start
#iterations: 340
currently lose_sum: 519.229303330183
time_elpased: 1.656
batch start
#iterations: 341
currently lose_sum: 517.9513537287712
time_elpased: 1.628
batch start
#iterations: 342
currently lose_sum: 520.7941589951515
time_elpased: 1.631
batch start
#iterations: 343
currently lose_sum: 520.7554805874825
time_elpased: 1.64
batch start
#iterations: 344
currently lose_sum: 518.7991584837437
time_elpased: 1.626
batch start
#iterations: 345
currently lose_sum: 520.0887161791325
time_elpased: 1.626
batch start
#iterations: 346
currently lose_sum: 516.7629002630711
time_elpased: 1.639
batch start
#iterations: 347
currently lose_sum: 518.7550917565823
time_elpased: 1.626
batch start
#iterations: 348
currently lose_sum: 519.4056282043457
time_elpased: 1.624
batch start
#iterations: 349
currently lose_sum: 519.6765503287315
time_elpased: 1.628
batch start
#iterations: 350
currently lose_sum: 518.740321457386
time_elpased: 1.63
batch start
#iterations: 351
currently lose_sum: 518.2445203661919
time_elpased: 1.637
batch start
#iterations: 352
currently lose_sum: 519.3019554615021
time_elpased: 1.63
batch start
#iterations: 353
currently lose_sum: 519.7567810714245
time_elpased: 1.634
batch start
#iterations: 354
currently lose_sum: 518.4852394759655
time_elpased: 1.634
batch start
#iterations: 355
currently lose_sum: 520.2028723061085
time_elpased: 1.63
batch start
#iterations: 356
currently lose_sum: 519.1893258690834
time_elpased: 1.628
batch start
#iterations: 357
currently lose_sum: 518.5355474352837
time_elpased: 1.643
batch start
#iterations: 358
currently lose_sum: 520.3498687446117
time_elpased: 1.629
batch start
#iterations: 359
currently lose_sum: 518.2362695634365
time_elpased: 1.626
start validation test
0.574587628866
0.938157081014
0.156876938987
0.268804819704
0
validation finish
batch start
#iterations: 360
currently lose_sum: 520.4823942184448
time_elpased: 1.637
batch start
#iterations: 361
currently lose_sum: 517.1703705191612
time_elpased: 1.643
batch start
#iterations: 362
currently lose_sum: 517.4020997881889
time_elpased: 1.626
batch start
#iterations: 363
currently lose_sum: 519.5079502761364
time_elpased: 1.641
batch start
#iterations: 364
currently lose_sum: 519.2001921832561
time_elpased: 1.635
batch start
#iterations: 365
currently lose_sum: 519.2534277141094
time_elpased: 1.641
batch start
#iterations: 366
currently lose_sum: 518.8471940159798
time_elpased: 1.629
batch start
#iterations: 367
currently lose_sum: 519.8953835964203
time_elpased: 1.619
batch start
#iterations: 368
currently lose_sum: 517.541328728199
time_elpased: 1.631
batch start
#iterations: 369
currently lose_sum: 517.854523152113
time_elpased: 1.627
batch start
#iterations: 370
currently lose_sum: 518.681627124548
time_elpased: 1.632
batch start
#iterations: 371
currently lose_sum: 519.9555535018444
time_elpased: 1.638
batch start
#iterations: 372
currently lose_sum: 519.1754622161388
time_elpased: 1.625
batch start
#iterations: 373
currently lose_sum: 518.5984694957733
time_elpased: 1.648
batch start
#iterations: 374
currently lose_sum: 520.1987941861153
time_elpased: 1.632
batch start
#iterations: 375
currently lose_sum: 520.1979228258133
time_elpased: 1.648
batch start
#iterations: 376
currently lose_sum: 519.3560281693935
time_elpased: 1.626
batch start
#iterations: 377
currently lose_sum: 518.7000464200974
time_elpased: 1.635
batch start
#iterations: 378
currently lose_sum: 519.3811920881271
time_elpased: 1.625
batch start
#iterations: 379
currently lose_sum: 517.5353283882141
time_elpased: 1.614
start validation test
0.587680412371
0.940897097625
0.184384694933
0.308344141807
0
validation finish
batch start
#iterations: 380
currently lose_sum: 520.3913298249245
time_elpased: 1.648
batch start
#iterations: 381
currently lose_sum: 517.6310124099255
time_elpased: 1.622
batch start
#iterations: 382
currently lose_sum: 520.4479709863663
time_elpased: 1.634
batch start
#iterations: 383
currently lose_sum: 516.6195829808712
time_elpased: 1.633
batch start
#iterations: 384
currently lose_sum: 516.9237635731697
time_elpased: 1.621
batch start
#iterations: 385
currently lose_sum: 517.81133633852
time_elpased: 1.645
batch start
#iterations: 386
currently lose_sum: 518.9187021255493
time_elpased: 1.628
batch start
#iterations: 387
currently lose_sum: 518.4357485473156
time_elpased: 1.621
batch start
#iterations: 388
currently lose_sum: 518.6967707872391
time_elpased: 1.642
batch start
#iterations: 389
currently lose_sum: 518.4304627478123
time_elpased: 1.625
batch start
#iterations: 390
currently lose_sum: 519.2873699963093
time_elpased: 1.634
batch start
#iterations: 391
currently lose_sum: 518.6381153464317
time_elpased: 1.639
batch start
#iterations: 392
currently lose_sum: 519.5308220088482
time_elpased: 1.637
batch start
#iterations: 393
currently lose_sum: 518.3107077777386
time_elpased: 1.633
batch start
#iterations: 394
currently lose_sum: 519.3735056817532
time_elpased: 1.642
batch start
#iterations: 395
currently lose_sum: 517.0420630276203
time_elpased: 1.621
batch start
#iterations: 396
currently lose_sum: 518.6264844834805
time_elpased: 1.639
batch start
#iterations: 397
currently lose_sum: 519.9685436785221
time_elpased: 1.648
batch start
#iterations: 398
currently lose_sum: 518.934443384409
time_elpased: 1.643
batch start
#iterations: 399
currently lose_sum: 518.3306792676449
time_elpased: 1.621
start validation test
0.57087628866
0.942725477288
0.148086866598
0.255965680579
0
validation finish
acc: 0.612
pre: 0.925
rec: 0.252
F1: 0.396
auc: 0.000
