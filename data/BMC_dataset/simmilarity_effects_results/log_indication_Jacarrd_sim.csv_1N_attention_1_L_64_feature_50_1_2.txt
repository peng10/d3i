start to construct graph
graph construct over
58300
epochs start
batch start
#iterations: 0
currently lose_sum: 394.2593042254448
time_elpased: 1.241
batch start
#iterations: 1
currently lose_sum: 385.8926174044609
time_elpased: 1.191
batch start
#iterations: 2
currently lose_sum: 381.04487866163254
time_elpased: 1.188
batch start
#iterations: 3
currently lose_sum: 378.31245589256287
time_elpased: 1.192
batch start
#iterations: 4
currently lose_sum: 376.00595766305923
time_elpased: 1.205
batch start
#iterations: 5
currently lose_sum: 374.7157136797905
time_elpased: 1.193
batch start
#iterations: 6
currently lose_sum: 372.44296783208847
time_elpased: 1.196
batch start
#iterations: 7
currently lose_sum: 371.9570950269699
time_elpased: 1.189
batch start
#iterations: 8
currently lose_sum: 370.1136485338211
time_elpased: 1.203
batch start
#iterations: 9
currently lose_sum: 371.382377743721
time_elpased: 1.188
batch start
#iterations: 10
currently lose_sum: 367.91126346588135
time_elpased: 1.185
batch start
#iterations: 11
currently lose_sum: 368.28920590877533
time_elpased: 1.19
batch start
#iterations: 12
currently lose_sum: 368.15916270017624
time_elpased: 1.186
batch start
#iterations: 13
currently lose_sum: 370.1111432313919
time_elpased: 1.198
batch start
#iterations: 14
currently lose_sum: 365.1418644785881
time_elpased: 1.187
batch start
#iterations: 15
currently lose_sum: 366.6278626322746
time_elpased: 1.181
batch start
#iterations: 16
currently lose_sum: 365.3337261080742
time_elpased: 1.194
batch start
#iterations: 17
currently lose_sum: 366.36355781555176
time_elpased: 1.205
batch start
#iterations: 18
currently lose_sum: 364.0216734409332
time_elpased: 1.187
batch start
#iterations: 19
currently lose_sum: 365.4042646288872
time_elpased: 1.188
start validation test
0.781288659794
0.843698543382
0.688831437435
0.758440079704
0
validation finish
batch start
#iterations: 20
currently lose_sum: 364.1804429292679
time_elpased: 1.196
batch start
#iterations: 21
currently lose_sum: 364.3016278743744
time_elpased: 1.191
batch start
#iterations: 22
currently lose_sum: 364.9057064652443
time_elpased: 1.192
batch start
#iterations: 23
currently lose_sum: 364.7366518378258
time_elpased: 1.18
batch start
#iterations: 24
currently lose_sum: 364.1835985779762
time_elpased: 1.182
batch start
#iterations: 25
currently lose_sum: 363.70553147792816
time_elpased: 1.189
batch start
#iterations: 26
currently lose_sum: 362.867890894413
time_elpased: 1.207
batch start
#iterations: 27
currently lose_sum: 363.47029960155487
time_elpased: 1.193
batch start
#iterations: 28
currently lose_sum: 361.47359797358513
time_elpased: 1.188
batch start
#iterations: 29
currently lose_sum: 363.33533865213394
time_elpased: 1.188
batch start
#iterations: 30
currently lose_sum: 363.08422791957855
time_elpased: 1.189
batch start
#iterations: 31
currently lose_sum: 361.68359607458115
time_elpased: 1.182
batch start
#iterations: 32
currently lose_sum: 363.3172040581703
time_elpased: 1.193
batch start
#iterations: 33
currently lose_sum: 362.9714394211769
time_elpased: 1.18
batch start
#iterations: 34
currently lose_sum: 362.9459408521652
time_elpased: 1.183
batch start
#iterations: 35
currently lose_sum: 362.7726096510887
time_elpased: 1.196
batch start
#iterations: 36
currently lose_sum: 361.78146490454674
time_elpased: 1.196
batch start
#iterations: 37
currently lose_sum: 361.0139563679695
time_elpased: 1.2
batch start
#iterations: 38
currently lose_sum: 362.76198065280914
time_elpased: 1.201
batch start
#iterations: 39
currently lose_sum: 362.16437685489655
time_elpased: 1.19
start validation test
0.788711340206
0.832121139859
0.721716649431
0.772996621809
0
validation finish
batch start
#iterations: 40
currently lose_sum: 360.8310626745224
time_elpased: 1.195
batch start
#iterations: 41
currently lose_sum: 361.4166648387909
time_elpased: 1.187
batch start
#iterations: 42
currently lose_sum: 360.05391097068787
time_elpased: 1.189
batch start
#iterations: 43
currently lose_sum: 360.55682215094566
time_elpased: 1.2
batch start
#iterations: 44
currently lose_sum: 361.1553764343262
time_elpased: 1.185
batch start
#iterations: 45
currently lose_sum: 359.6463384628296
time_elpased: 1.199
batch start
#iterations: 46
currently lose_sum: 359.99463817477226
time_elpased: 1.194
batch start
#iterations: 47
currently lose_sum: 360.8403906226158
time_elpased: 1.184
batch start
#iterations: 48
currently lose_sum: 360.2844553589821
time_elpased: 1.183
batch start
#iterations: 49
currently lose_sum: 360.7268477678299
time_elpased: 1.187
batch start
#iterations: 50
currently lose_sum: 358.8632813692093
time_elpased: 1.182
batch start
#iterations: 51
currently lose_sum: 360.40825468301773
time_elpased: 1.2
batch start
#iterations: 52
currently lose_sum: 360.27679455280304
time_elpased: 1.201
batch start
#iterations: 53
currently lose_sum: 361.1835085749626
time_elpased: 1.187
batch start
#iterations: 54
currently lose_sum: 360.33672004938126
time_elpased: 1.184
batch start
#iterations: 55
currently lose_sum: 359.51921010017395
time_elpased: 1.215
batch start
#iterations: 56
currently lose_sum: 361.2163333892822
time_elpased: 1.195
batch start
#iterations: 57
currently lose_sum: 360.36949104070663
time_elpased: 1.188
batch start
#iterations: 58
currently lose_sum: 359.0365422964096
time_elpased: 1.193
batch start
#iterations: 59
currently lose_sum: 359.92113506793976
time_elpased: 1.189
start validation test
0.799381443299
0.806557724958
0.786039296794
0.796166334974
0
validation finish
batch start
#iterations: 60
currently lose_sum: 360.1174131631851
time_elpased: 1.201
batch start
#iterations: 61
currently lose_sum: 359.46699011325836
time_elpased: 1.201
batch start
#iterations: 62
currently lose_sum: 359.23430716991425
time_elpased: 1.201
batch start
#iterations: 63
currently lose_sum: 358.91832131147385
time_elpased: 1.182
batch start
#iterations: 64
currently lose_sum: 360.11643451452255
time_elpased: 1.204
batch start
#iterations: 65
currently lose_sum: 360.40244656801224
time_elpased: 1.204
batch start
#iterations: 66
currently lose_sum: 359.6689782142639
time_elpased: 1.192
batch start
#iterations: 67
currently lose_sum: 359.6826934814453
time_elpased: 1.187
batch start
#iterations: 68
currently lose_sum: 359.9050611257553
time_elpased: 1.192
batch start
#iterations: 69
currently lose_sum: 359.6098300218582
time_elpased: 1.202
batch start
#iterations: 70
currently lose_sum: 359.2510552406311
time_elpased: 1.175
batch start
#iterations: 71
currently lose_sum: 359.620054602623
time_elpased: 1.205
batch start
#iterations: 72
currently lose_sum: 358.861458837986
time_elpased: 1.199
batch start
#iterations: 73
currently lose_sum: 358.20555263757706
time_elpased: 1.193
batch start
#iterations: 74
currently lose_sum: 358.26922103762627
time_elpased: 1.191
batch start
#iterations: 75
currently lose_sum: 358.24691781401634
time_elpased: 1.2
batch start
#iterations: 76
currently lose_sum: 359.04679921269417
time_elpased: 1.206
batch start
#iterations: 77
currently lose_sum: 358.8009786307812
time_elpased: 1.191
batch start
#iterations: 78
currently lose_sum: 359.52455139160156
time_elpased: 1.184
batch start
#iterations: 79
currently lose_sum: 359.0346023440361
time_elpased: 1.21
start validation test
0.802783505155
0.801672517035
0.802998965874
0.802335193222
0
validation finish
batch start
#iterations: 80
currently lose_sum: 357.7391160726547
time_elpased: 1.191
batch start
#iterations: 81
currently lose_sum: 360.1057839393616
time_elpased: 1.195
batch start
#iterations: 82
currently lose_sum: 359.41349613666534
time_elpased: 1.193
batch start
#iterations: 83
currently lose_sum: 358.7579094171524
time_elpased: 1.179
batch start
#iterations: 84
currently lose_sum: 359.7171601653099
time_elpased: 1.195
batch start
#iterations: 85
currently lose_sum: 359.1933379769325
time_elpased: 1.187
batch start
#iterations: 86
currently lose_sum: 359.34473890066147
time_elpased: 1.202
batch start
#iterations: 87
currently lose_sum: 358.2880339026451
time_elpased: 1.189
batch start
#iterations: 88
currently lose_sum: 358.32297599315643
time_elpased: 1.196
batch start
#iterations: 89
currently lose_sum: 358.2225412726402
time_elpased: 1.195
batch start
#iterations: 90
currently lose_sum: 359.70842176675797
time_elpased: 1.19
batch start
#iterations: 91
currently lose_sum: 358.0912001132965
time_elpased: 1.197
batch start
#iterations: 92
currently lose_sum: 357.4747359752655
time_elpased: 1.191
batch start
#iterations: 93
currently lose_sum: 360.1122513413429
time_elpased: 1.199
batch start
#iterations: 94
currently lose_sum: 357.9927563369274
time_elpased: 1.184
batch start
#iterations: 95
currently lose_sum: 357.9568620920181
time_elpased: 1.207
batch start
#iterations: 96
currently lose_sum: 359.54883274435997
time_elpased: 1.187
batch start
#iterations: 97
currently lose_sum: 358.4028602838516
time_elpased: 1.19
batch start
#iterations: 98
currently lose_sum: 358.8161842226982
time_elpased: 1.183
batch start
#iterations: 99
currently lose_sum: 358.12448132038116
time_elpased: 1.189
start validation test
0.799226804124
0.811926109971
0.777249224405
0.794209330586
0
validation finish
batch start
#iterations: 100
currently lose_sum: 358.08017522096634
time_elpased: 1.209
batch start
#iterations: 101
currently lose_sum: 357.72156006097794
time_elpased: 1.191
batch start
#iterations: 102
currently lose_sum: 358.0496690273285
time_elpased: 1.196
batch start
#iterations: 103
currently lose_sum: 357.9743763804436
time_elpased: 1.188
batch start
#iterations: 104
currently lose_sum: 358.8104321360588
time_elpased: 1.2
batch start
#iterations: 105
currently lose_sum: 357.3959165215492
time_elpased: 1.179
batch start
#iterations: 106
currently lose_sum: 358.29559153318405
time_elpased: 1.216
batch start
#iterations: 107
currently lose_sum: 357.89822912216187
time_elpased: 1.192
batch start
#iterations: 108
currently lose_sum: 358.3514305651188
time_elpased: 1.207
batch start
#iterations: 109
currently lose_sum: 357.8803606033325
time_elpased: 1.192
batch start
#iterations: 110
currently lose_sum: 359.4312050938606
time_elpased: 1.186
batch start
#iterations: 111
currently lose_sum: 357.25431764125824
time_elpased: 1.191
batch start
#iterations: 112
currently lose_sum: 357.8931083083153
time_elpased: 1.208
batch start
#iterations: 113
currently lose_sum: 357.63189405202866
time_elpased: 1.194
batch start
#iterations: 114
currently lose_sum: 356.7325306534767
time_elpased: 1.193
batch start
#iterations: 115
currently lose_sum: 357.11674177646637
time_elpased: 1.205
batch start
#iterations: 116
currently lose_sum: 358.7699457705021
time_elpased: 1.205
batch start
#iterations: 117
currently lose_sum: 357.32140320539474
time_elpased: 1.194
batch start
#iterations: 118
currently lose_sum: 356.1723269224167
time_elpased: 1.204
batch start
#iterations: 119
currently lose_sum: 356.9470559358597
time_elpased: 1.209
start validation test
0.798195876289
0.813078011098
0.772802481903
0.792428821377
0
validation finish
batch start
#iterations: 120
currently lose_sum: 357.8142644762993
time_elpased: 1.205
batch start
#iterations: 121
currently lose_sum: 356.868059694767
time_elpased: 1.192
batch start
#iterations: 122
currently lose_sum: 355.07032537460327
time_elpased: 1.215
batch start
#iterations: 123
currently lose_sum: 357.53371024131775
time_elpased: 1.193
batch start
#iterations: 124
currently lose_sum: 355.84446001052856
time_elpased: 1.199
batch start
#iterations: 125
currently lose_sum: 357.46839690208435
time_elpased: 1.195
batch start
#iterations: 126
currently lose_sum: 358.9942951798439
time_elpased: 1.2
batch start
#iterations: 127
currently lose_sum: 357.3118149936199
time_elpased: 1.198
batch start
#iterations: 128
currently lose_sum: 357.0798416733742
time_elpased: 1.204
batch start
#iterations: 129
currently lose_sum: 358.62515050172806
time_elpased: 1.193
batch start
#iterations: 130
currently lose_sum: 357.1137451529503
time_elpased: 1.185
batch start
#iterations: 131
currently lose_sum: 356.2322336435318
time_elpased: 1.196
batch start
#iterations: 132
currently lose_sum: 357.6071593761444
time_elpased: 1.205
batch start
#iterations: 133
currently lose_sum: 356.7772944867611
time_elpased: 1.208
batch start
#iterations: 134
currently lose_sum: 357.67744302749634
time_elpased: 1.193
batch start
#iterations: 135
currently lose_sum: 357.5264200568199
time_elpased: 1.195
batch start
#iterations: 136
currently lose_sum: 359.7158742547035
time_elpased: 1.194
batch start
#iterations: 137
currently lose_sum: 357.43811017274857
time_elpased: 1.195
batch start
#iterations: 138
currently lose_sum: 358.1876252889633
time_elpased: 1.2
batch start
#iterations: 139
currently lose_sum: 356.99873346090317
time_elpased: 1.196
start validation test
0.796804123711
0.800272593835
0.789348500517
0.794773011245
0
validation finish
batch start
#iterations: 140
currently lose_sum: 357.52452421188354
time_elpased: 1.212
batch start
#iterations: 141
currently lose_sum: 358.6905522942543
time_elpased: 1.204
batch start
#iterations: 142
currently lose_sum: 358.45642352104187
time_elpased: 1.19
batch start
#iterations: 143
currently lose_sum: 357.2048308253288
time_elpased: 1.188
batch start
#iterations: 144
currently lose_sum: 357.5682557821274
time_elpased: 1.199
batch start
#iterations: 145
currently lose_sum: 357.0785536766052
time_elpased: 1.216
batch start
#iterations: 146
currently lose_sum: 357.0613445043564
time_elpased: 1.199
batch start
#iterations: 147
currently lose_sum: 357.01919066905975
time_elpased: 1.192
batch start
#iterations: 148
currently lose_sum: 357.57737535238266
time_elpased: 1.193
batch start
#iterations: 149
currently lose_sum: 358.0088971853256
time_elpased: 1.191
batch start
#iterations: 150
currently lose_sum: 356.58735567331314
time_elpased: 1.188
batch start
#iterations: 151
currently lose_sum: 357.12559723854065
time_elpased: 1.198
batch start
#iterations: 152
currently lose_sum: 357.09864205121994
time_elpased: 1.207
batch start
#iterations: 153
currently lose_sum: 358.45902568101883
time_elpased: 1.197
batch start
#iterations: 154
currently lose_sum: 357.36031806468964
time_elpased: 1.205
batch start
#iterations: 155
currently lose_sum: 357.54127049446106
time_elpased: 1.207
batch start
#iterations: 156
currently lose_sum: 357.13378381729126
time_elpased: 1.202
batch start
#iterations: 157
currently lose_sum: 357.0328477323055
time_elpased: 1.204
batch start
#iterations: 158
currently lose_sum: 357.5180643796921
time_elpased: 1.205
batch start
#iterations: 159
currently lose_sum: 358.7910541296005
time_elpased: 1.194
start validation test
0.79824742268
0.787512487512
0.815201654602
0.801117886179
0
validation finish
batch start
#iterations: 160
currently lose_sum: 357.25067752599716
time_elpased: 1.212
batch start
#iterations: 161
currently lose_sum: 356.2814217209816
time_elpased: 1.204
batch start
#iterations: 162
currently lose_sum: 357.0915479063988
time_elpased: 1.187
batch start
#iterations: 163
currently lose_sum: 356.8997635245323
time_elpased: 1.188
batch start
#iterations: 164
currently lose_sum: 356.58755511045456
time_elpased: 1.199
batch start
#iterations: 165
currently lose_sum: 355.5441057384014
time_elpased: 1.197
batch start
#iterations: 166
currently lose_sum: 356.2172022163868
time_elpased: 1.202
batch start
#iterations: 167
currently lose_sum: 357.73194229602814
time_elpased: 1.193
batch start
#iterations: 168
currently lose_sum: 356.7945823073387
time_elpased: 1.2
batch start
#iterations: 169
currently lose_sum: 356.73828160762787
time_elpased: 1.19
batch start
#iterations: 170
currently lose_sum: 357.3677415251732
time_elpased: 1.214
batch start
#iterations: 171
currently lose_sum: 356.10707783699036
time_elpased: 1.188
batch start
#iterations: 172
currently lose_sum: 357.48457223176956
time_elpased: 1.189
batch start
#iterations: 173
currently lose_sum: 357.96531319618225
time_elpased: 1.185
batch start
#iterations: 174
currently lose_sum: 355.8023804426193
time_elpased: 1.208
batch start
#iterations: 175
currently lose_sum: 358.09165769815445
time_elpased: 1.202
batch start
#iterations: 176
currently lose_sum: 357.035224378109
time_elpased: 1.204
batch start
#iterations: 177
currently lose_sum: 356.49609196186066
time_elpased: 1.19
batch start
#iterations: 178
currently lose_sum: 357.9522685408592
time_elpased: 1.203
batch start
#iterations: 179
currently lose_sum: 356.4324960112572
time_elpased: 1.195
start validation test
0.796546391753
0.789889575524
0.806308169597
0.798014431196
0
validation finish
batch start
#iterations: 180
currently lose_sum: 357.526608645916
time_elpased: 1.211
batch start
#iterations: 181
currently lose_sum: 355.3659955263138
time_elpased: 1.19
batch start
#iterations: 182
currently lose_sum: 357.3244428038597
time_elpased: 1.2
batch start
#iterations: 183
currently lose_sum: 356.55867010354996
time_elpased: 1.2
batch start
#iterations: 184
currently lose_sum: 356.0817851424217
time_elpased: 1.199
batch start
#iterations: 185
currently lose_sum: 356.9004027247429
time_elpased: 1.193
batch start
#iterations: 186
currently lose_sum: 356.7586576938629
time_elpased: 1.19
batch start
#iterations: 187
currently lose_sum: 355.39327216148376
time_elpased: 1.198
batch start
#iterations: 188
currently lose_sum: 357.2693285346031
time_elpased: 1.188
batch start
#iterations: 189
currently lose_sum: 357.2339817285538
time_elpased: 1.207
batch start
#iterations: 190
currently lose_sum: 355.7008525133133
time_elpased: 1.193
batch start
#iterations: 191
currently lose_sum: 356.442926466465
time_elpased: 1.192
batch start
#iterations: 192
currently lose_sum: 357.4120032787323
time_elpased: 1.211
batch start
#iterations: 193
currently lose_sum: 357.0728036761284
time_elpased: 1.195
batch start
#iterations: 194
currently lose_sum: 356.2413858771324
time_elpased: 1.203
batch start
#iterations: 195
currently lose_sum: 356.72956812381744
time_elpased: 1.191
batch start
#iterations: 196
currently lose_sum: 358.36276483535767
time_elpased: 1.189
batch start
#iterations: 197
currently lose_sum: 356.6289799809456
time_elpased: 1.202
batch start
#iterations: 198
currently lose_sum: 357.25133258104324
time_elpased: 1.207
batch start
#iterations: 199
currently lose_sum: 355.9184031486511
time_elpased: 1.207
start validation test
0.79587628866
0.815818584071
0.762668045502
0.78834847675
0
validation finish
batch start
#iterations: 200
currently lose_sum: 357.9100704193115
time_elpased: 1.205
batch start
#iterations: 201
currently lose_sum: 355.8259045481682
time_elpased: 1.192
batch start
#iterations: 202
currently lose_sum: 355.4619069695473
time_elpased: 1.193
batch start
#iterations: 203
currently lose_sum: 355.4412200450897
time_elpased: 1.208
batch start
#iterations: 204
currently lose_sum: 356.9235835969448
time_elpased: 1.182
batch start
#iterations: 205
currently lose_sum: 354.9770916700363
time_elpased: 1.183
batch start
#iterations: 206
currently lose_sum: 357.0058089494705
time_elpased: 1.191
batch start
#iterations: 207
currently lose_sum: 357.92488676309586
time_elpased: 1.202
batch start
#iterations: 208
currently lose_sum: 356.8079506754875
time_elpased: 1.211
batch start
#iterations: 209
currently lose_sum: 355.225149333477
time_elpased: 1.193
batch start
#iterations: 210
currently lose_sum: 356.0149339437485
time_elpased: 1.202
batch start
#iterations: 211
currently lose_sum: 356.37923008203506
time_elpased: 1.202
batch start
#iterations: 212
currently lose_sum: 356.7808060646057
time_elpased: 1.187
batch start
#iterations: 213
currently lose_sum: 357.4486608207226
time_elpased: 1.194
batch start
#iterations: 214
currently lose_sum: 357.06119042634964
time_elpased: 1.211
batch start
#iterations: 215
currently lose_sum: 355.60632199048996
time_elpased: 1.197
batch start
#iterations: 216
currently lose_sum: 356.76273399591446
time_elpased: 1.214
batch start
#iterations: 217
currently lose_sum: 356.58029651641846
time_elpased: 1.213
batch start
#iterations: 218
currently lose_sum: 356.75355887413025
time_elpased: 1.193
batch start
#iterations: 219
currently lose_sum: 357.5274249613285
time_elpased: 1.187
start validation test
0.796391752577
0.800925925926
0.787176835574
0.793991863982
0
validation finish
batch start
#iterations: 220
currently lose_sum: 355.0013058781624
time_elpased: 1.208
batch start
#iterations: 221
currently lose_sum: 356.5865198671818
time_elpased: 1.246
batch start
#iterations: 222
currently lose_sum: 357.31510680913925
time_elpased: 1.184
batch start
#iterations: 223
currently lose_sum: 356.4783208966255
time_elpased: 1.194
batch start
#iterations: 224
currently lose_sum: 356.34844386577606
time_elpased: 1.206
batch start
#iterations: 225
currently lose_sum: 356.6412691473961
time_elpased: 1.188
batch start
#iterations: 226
currently lose_sum: 356.28641882538795
time_elpased: 1.194
batch start
#iterations: 227
currently lose_sum: 357.20708924531937
time_elpased: 1.199
batch start
#iterations: 228
currently lose_sum: 355.8236865401268
time_elpased: 1.184
batch start
#iterations: 229
currently lose_sum: 356.3057146668434
time_elpased: 1.187
batch start
#iterations: 230
currently lose_sum: 356.56076723337173
time_elpased: 1.204
batch start
#iterations: 231
currently lose_sum: 356.76082319021225
time_elpased: 1.201
batch start
#iterations: 232
currently lose_sum: 356.6626874804497
time_elpased: 1.204
batch start
#iterations: 233
currently lose_sum: 355.9441028833389
time_elpased: 1.197
batch start
#iterations: 234
currently lose_sum: 356.35866755247116
time_elpased: 1.196
batch start
#iterations: 235
currently lose_sum: 355.2616443037987
time_elpased: 1.199
batch start
#iterations: 236
currently lose_sum: 356.0211571455002
time_elpased: 1.195
batch start
#iterations: 237
currently lose_sum: 355.7481843829155
time_elpased: 1.221
batch start
#iterations: 238
currently lose_sum: 356.0303210616112
time_elpased: 1.191
batch start
#iterations: 239
currently lose_sum: 354.5084228515625
time_elpased: 1.19
start validation test
0.797010309278
0.802767800549
0.785832471562
0.794209866221
0
validation finish
batch start
#iterations: 240
currently lose_sum: 355.50296753644943
time_elpased: 1.21
batch start
#iterations: 241
currently lose_sum: 355.7760250866413
time_elpased: 1.199
batch start
#iterations: 242
currently lose_sum: 354.38208201527596
time_elpased: 1.184
batch start
#iterations: 243
currently lose_sum: 356.51847392320633
time_elpased: 1.192
batch start
#iterations: 244
currently lose_sum: 356.0160913467407
time_elpased: 1.19
batch start
#iterations: 245
currently lose_sum: 355.21331945061684
time_elpased: 1.2
batch start
#iterations: 246
currently lose_sum: 356.9661632180214
time_elpased: 1.213
batch start
#iterations: 247
currently lose_sum: 357.46802920103073
time_elpased: 1.189
batch start
#iterations: 248
currently lose_sum: 358.105206400156
time_elpased: 1.206
batch start
#iterations: 249
currently lose_sum: 355.9496448338032
time_elpased: 1.192
batch start
#iterations: 250
currently lose_sum: 356.72017896175385
time_elpased: 1.19
batch start
#iterations: 251
currently lose_sum: 357.87487453222275
time_elpased: 1.181
batch start
#iterations: 252
currently lose_sum: 356.1418101787567
time_elpased: 1.193
batch start
#iterations: 253
currently lose_sum: 355.1038398742676
time_elpased: 1.191
batch start
#iterations: 254
currently lose_sum: 355.7696874141693
time_elpased: 1.206
batch start
#iterations: 255
currently lose_sum: 356.2548849582672
time_elpased: 1.18
batch start
#iterations: 256
currently lose_sum: 355.74044156074524
time_elpased: 1.207
batch start
#iterations: 257
currently lose_sum: 357.95573365688324
time_elpased: 1.188
batch start
#iterations: 258
currently lose_sum: 358.6950260400772
time_elpased: 1.184
batch start
#iterations: 259
currently lose_sum: 355.76091611385345
time_elpased: 1.197
start validation test
0.795927835052
0.796613690662
0.793071354705
0.794838575944
0
validation finish
batch start
#iterations: 260
currently lose_sum: 355.0891892015934
time_elpased: 1.2
batch start
#iterations: 261
currently lose_sum: 355.3558244109154
time_elpased: 1.203
batch start
#iterations: 262
currently lose_sum: 357.2871991991997
time_elpased: 1.191
batch start
#iterations: 263
currently lose_sum: 358.18901962041855
time_elpased: 1.206
batch start
#iterations: 264
currently lose_sum: 355.4552454352379
time_elpased: 1.195
batch start
#iterations: 265
currently lose_sum: 355.53179264068604
time_elpased: 1.202
batch start
#iterations: 266
currently lose_sum: 357.0808650255203
time_elpased: 1.2
batch start
#iterations: 267
currently lose_sum: 357.12278014421463
time_elpased: 1.193
batch start
#iterations: 268
currently lose_sum: 356.0838590860367
time_elpased: 1.197
batch start
#iterations: 269
currently lose_sum: 356.9178857803345
time_elpased: 1.216
batch start
#iterations: 270
currently lose_sum: 354.89218601584435
time_elpased: 1.194
batch start
#iterations: 271
currently lose_sum: 356.1919628381729
time_elpased: 1.185
batch start
#iterations: 272
currently lose_sum: 356.25010895729065
time_elpased: 1.188
batch start
#iterations: 273
currently lose_sum: 356.1772475242615
time_elpased: 1.197
batch start
#iterations: 274
currently lose_sum: 355.9997155070305
time_elpased: 1.213
batch start
#iterations: 275
currently lose_sum: 357.1981312036514
time_elpased: 1.198
batch start
#iterations: 276
currently lose_sum: 355.126830637455
time_elpased: 1.205
batch start
#iterations: 277
currently lose_sum: 355.67267286777496
time_elpased: 1.188
batch start
#iterations: 278
currently lose_sum: 355.9369286596775
time_elpased: 1.213
batch start
#iterations: 279
currently lose_sum: 355.6465903520584
time_elpased: 1.197
start validation test
0.795515463918
0.801905770249
0.783247156153
0.792466649228
0
validation finish
batch start
#iterations: 280
currently lose_sum: 355.57847648859024
time_elpased: 1.204
batch start
#iterations: 281
currently lose_sum: 357.73388636112213
time_elpased: 1.208
batch start
#iterations: 282
currently lose_sum: 355.3430904150009
time_elpased: 1.204
batch start
#iterations: 283
currently lose_sum: 355.47853434085846
time_elpased: 1.206
batch start
#iterations: 284
currently lose_sum: 356.3007821440697
time_elpased: 1.209
batch start
#iterations: 285
currently lose_sum: 353.7837710380554
time_elpased: 1.246
batch start
#iterations: 286
currently lose_sum: 355.82919722795486
time_elpased: 1.199
batch start
#iterations: 287
currently lose_sum: 355.1345024704933
time_elpased: 1.208
batch start
#iterations: 288
currently lose_sum: 356.08516961336136
time_elpased: 1.205
batch start
#iterations: 289
currently lose_sum: 355.65364557504654
time_elpased: 1.199
batch start
#iterations: 290
currently lose_sum: 354.64418864250183
time_elpased: 1.198
batch start
#iterations: 291
currently lose_sum: 356.1197701692581
time_elpased: 1.196
batch start
#iterations: 292
currently lose_sum: 356.23061287403107
time_elpased: 1.201
batch start
#iterations: 293
currently lose_sum: 356.2392798066139
time_elpased: 1.204
batch start
#iterations: 294
currently lose_sum: 354.02891540527344
time_elpased: 1.189
batch start
#iterations: 295
currently lose_sum: 355.7019386291504
time_elpased: 1.194
batch start
#iterations: 296
currently lose_sum: 355.04719334840775
time_elpased: 1.208
batch start
#iterations: 297
currently lose_sum: 355.1774759888649
time_elpased: 1.191
batch start
#iterations: 298
currently lose_sum: 354.23396039009094
time_elpased: 1.208
batch start
#iterations: 299
currently lose_sum: 354.81046828627586
time_elpased: 1.21
start validation test
0.796082474227
0.795144628099
0.795966907963
0.795555555556
0
validation finish
batch start
#iterations: 300
currently lose_sum: 355.05797415971756
time_elpased: 1.241
batch start
#iterations: 301
currently lose_sum: 356.1253686249256
time_elpased: 1.2
batch start
#iterations: 302
currently lose_sum: 357.05862230062485
time_elpased: 1.202
batch start
#iterations: 303
currently lose_sum: 356.79791367053986
time_elpased: 1.19
batch start
#iterations: 304
currently lose_sum: 356.24924182891846
time_elpased: 1.195
batch start
#iterations: 305
currently lose_sum: 356.6004331111908
time_elpased: 1.199
batch start
#iterations: 306
currently lose_sum: 353.96694165468216
time_elpased: 1.199
batch start
#iterations: 307
currently lose_sum: 355.28915882110596
time_elpased: 1.195
batch start
#iterations: 308
currently lose_sum: 354.63215869665146
time_elpased: 1.193
batch start
#iterations: 309
currently lose_sum: 355.7011115550995
time_elpased: 1.19
batch start
#iterations: 310
currently lose_sum: 354.7994892001152
time_elpased: 1.199
batch start
#iterations: 311
currently lose_sum: 356.47342470288277
time_elpased: 1.185
batch start
#iterations: 312
currently lose_sum: 353.15928944945335
time_elpased: 1.213
batch start
#iterations: 313
currently lose_sum: 355.2122617959976
time_elpased: 1.206
batch start
#iterations: 314
currently lose_sum: 357.20303267240524
time_elpased: 1.183
batch start
#iterations: 315
currently lose_sum: 356.3975206911564
time_elpased: 1.212
batch start
#iterations: 316
currently lose_sum: 354.3162981271744
time_elpased: 1.188
batch start
#iterations: 317
currently lose_sum: 357.3170652985573
time_elpased: 1.202
batch start
#iterations: 318
currently lose_sum: 357.0251241326332
time_elpased: 1.182
batch start
#iterations: 319
currently lose_sum: 356.5217981338501
time_elpased: 1.206
start validation test
0.795360824742
0.78975193168
0.803309203723
0.796472880139
0
validation finish
batch start
#iterations: 320
currently lose_sum: 355.14015167951584
time_elpased: 1.214
batch start
#iterations: 321
currently lose_sum: 356.3921482563019
time_elpased: 1.194
batch start
#iterations: 322
currently lose_sum: 355.49756157398224
time_elpased: 1.225
batch start
#iterations: 323
currently lose_sum: 356.48276978731155
time_elpased: 1.2
batch start
#iterations: 324
currently lose_sum: 355.8930673599243
time_elpased: 1.186
batch start
#iterations: 325
currently lose_sum: 355.93713676929474
time_elpased: 1.199
batch start
#iterations: 326
currently lose_sum: 355.8065968453884
time_elpased: 1.207
batch start
#iterations: 327
currently lose_sum: 354.78565526008606
time_elpased: 1.206
batch start
#iterations: 328
currently lose_sum: 355.0586295723915
time_elpased: 1.191
batch start
#iterations: 329
currently lose_sum: 355.33267718553543
time_elpased: 1.204
batch start
#iterations: 330
currently lose_sum: 356.1182758808136
time_elpased: 1.213
batch start
#iterations: 331
currently lose_sum: 354.4368434548378
time_elpased: 1.187
batch start
#iterations: 332
currently lose_sum: 355.29888385534286
time_elpased: 1.209
batch start
#iterations: 333
currently lose_sum: 355.38371500372887
time_elpased: 1.187
batch start
#iterations: 334
currently lose_sum: 355.6374906897545
time_elpased: 1.199
batch start
#iterations: 335
currently lose_sum: 355.8131588101387
time_elpased: 1.207
batch start
#iterations: 336
currently lose_sum: 356.6258852481842
time_elpased: 1.195
batch start
#iterations: 337
currently lose_sum: 353.912266433239
time_elpased: 1.202
batch start
#iterations: 338
currently lose_sum: 355.9073193669319
time_elpased: 1.191
batch start
#iterations: 339
currently lose_sum: 354.3838634490967
time_elpased: 1.195
start validation test
0.79618556701
0.797646323683
0.792037228542
0.794831880448
0
validation finish
batch start
#iterations: 340
currently lose_sum: 354.83076548576355
time_elpased: 1.197
batch start
#iterations: 341
currently lose_sum: 356.12435203790665
time_elpased: 1.182
batch start
#iterations: 342
currently lose_sum: 354.30032616853714
time_elpased: 1.178
batch start
#iterations: 343
currently lose_sum: 355.27231118083
time_elpased: 1.196
batch start
#iterations: 344
currently lose_sum: 354.3769226670265
time_elpased: 1.184
batch start
#iterations: 345
currently lose_sum: 354.941440731287
time_elpased: 1.197
batch start
#iterations: 346
currently lose_sum: 355.4898733496666
time_elpased: 1.213
batch start
#iterations: 347
currently lose_sum: 356.5251941084862
time_elpased: 1.201
batch start
#iterations: 348
currently lose_sum: 353.46652179956436
time_elpased: 1.218
batch start
#iterations: 349
currently lose_sum: 354.3895459473133
time_elpased: 1.196
batch start
#iterations: 350
currently lose_sum: 355.84181544184685
time_elpased: 1.201
batch start
#iterations: 351
currently lose_sum: 355.3766449689865
time_elpased: 1.193
batch start
#iterations: 352
currently lose_sum: 357.94361197948456
time_elpased: 1.208
batch start
#iterations: 353
currently lose_sum: 354.5047581791878
time_elpased: 1.206
batch start
#iterations: 354
currently lose_sum: 354.58465799689293
time_elpased: 1.182
batch start
#iterations: 355
currently lose_sum: 354.5160099864006
time_elpased: 1.201
batch start
#iterations: 356
currently lose_sum: 356.0397845506668
time_elpased: 1.2
batch start
#iterations: 357
currently lose_sum: 354.99624609947205
time_elpased: 1.186
batch start
#iterations: 358
currently lose_sum: 354.12221363186836
time_elpased: 1.194
batch start
#iterations: 359
currently lose_sum: 354.0314047932625
time_elpased: 1.193
start validation test
0.792680412371
0.797827462561
0.782316442606
0.789995822891
0
validation finish
batch start
#iterations: 360
currently lose_sum: 356.055867433548
time_elpased: 1.197
batch start
#iterations: 361
currently lose_sum: 355.08931481838226
time_elpased: 1.184
batch start
#iterations: 362
currently lose_sum: 353.8339142203331
time_elpased: 1.197
batch start
#iterations: 363
currently lose_sum: 357.59556716680527
time_elpased: 1.215
batch start
#iterations: 364
currently lose_sum: 353.82574689388275
time_elpased: 1.191
batch start
#iterations: 365
currently lose_sum: 354.6480706334114
time_elpased: 1.206
batch start
#iterations: 366
currently lose_sum: 356.0685525238514
time_elpased: 1.198
batch start
#iterations: 367
currently lose_sum: 356.52093613147736
time_elpased: 1.194
batch start
#iterations: 368
currently lose_sum: 353.92913618683815
time_elpased: 1.194
batch start
#iterations: 369
currently lose_sum: 353.68035638332367
time_elpased: 1.186
batch start
#iterations: 370
currently lose_sum: 354.303843408823
time_elpased: 1.22
batch start
#iterations: 371
currently lose_sum: 355.87399011850357
time_elpased: 1.189
batch start
#iterations: 372
currently lose_sum: 355.8464978337288
time_elpased: 1.202
batch start
#iterations: 373
currently lose_sum: 355.12311404943466
time_elpased: 1.192
batch start
#iterations: 374
currently lose_sum: 355.8581758737564
time_elpased: 1.2
batch start
#iterations: 375
currently lose_sum: 354.18737614154816
time_elpased: 1.201
batch start
#iterations: 376
currently lose_sum: 355.57923460006714
time_elpased: 1.212
batch start
#iterations: 377
currently lose_sum: 355.4043188095093
time_elpased: 1.219
batch start
#iterations: 378
currently lose_sum: 354.6948492825031
time_elpased: 1.207
batch start
#iterations: 379
currently lose_sum: 355.1178829073906
time_elpased: 1.195
start validation test
0.794020618557
0.792293426746
0.795243019648
0.793765483072
0
validation finish
batch start
#iterations: 380
currently lose_sum: 356.5415616929531
time_elpased: 1.196
batch start
#iterations: 381
currently lose_sum: 356.41450250148773
time_elpased: 1.206
batch start
#iterations: 382
currently lose_sum: 355.6130599081516
time_elpased: 1.198
batch start
#iterations: 383
currently lose_sum: 355.05471163988113
time_elpased: 1.196
batch start
#iterations: 384
currently lose_sum: 355.3986927270889
time_elpased: 1.186
batch start
#iterations: 385
currently lose_sum: 354.20835095644
time_elpased: 1.203
batch start
#iterations: 386
currently lose_sum: 355.5549684166908
time_elpased: 1.193
batch start
#iterations: 387
currently lose_sum: 354.0840247273445
time_elpased: 1.196
batch start
#iterations: 388
currently lose_sum: 354.2409490644932
time_elpased: 1.198
batch start
#iterations: 389
currently lose_sum: 354.3109090924263
time_elpased: 1.208
batch start
#iterations: 390
currently lose_sum: 354.22126537561417
time_elpased: 1.21
batch start
#iterations: 391
currently lose_sum: 354.0606824159622
time_elpased: 1.203
batch start
#iterations: 392
currently lose_sum: 354.6986834406853
time_elpased: 1.193
batch start
#iterations: 393
currently lose_sum: 354.9562944173813
time_elpased: 1.196
batch start
#iterations: 394
currently lose_sum: 355.72291895747185
time_elpased: 1.192
batch start
#iterations: 395
currently lose_sum: 355.23230254650116
time_elpased: 1.203
batch start
#iterations: 396
currently lose_sum: 355.71352791786194
time_elpased: 1.205
batch start
#iterations: 397
currently lose_sum: 352.9533636868
time_elpased: 1.194
batch start
#iterations: 398
currently lose_sum: 354.9486406445503
time_elpased: 1.208
batch start
#iterations: 399
currently lose_sum: 353.8610235452652
time_elpased: 1.207
start validation test
0.795567010309
0.785828823412
0.810858324716
0.798147394137
0
validation finish
acc: 0.804
pre: 0.806
rec: 0.805
F1: 0.805
auc: 0.000
