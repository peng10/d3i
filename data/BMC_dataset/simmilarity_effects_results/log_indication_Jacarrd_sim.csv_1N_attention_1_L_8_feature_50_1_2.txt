start to construct graph
graph construct over
58300
epochs start
batch start
#iterations: 0
currently lose_sum: 393.891638815403
time_elpased: 1.118
batch start
#iterations: 1
currently lose_sum: 385.60643780231476
time_elpased: 1.067
batch start
#iterations: 2
currently lose_sum: 380.7893664240837
time_elpased: 1.071
batch start
#iterations: 3
currently lose_sum: 378.1234787106514
time_elpased: 1.063
batch start
#iterations: 4
currently lose_sum: 375.7938398718834
time_elpased: 1.067
batch start
#iterations: 5
currently lose_sum: 374.4551957845688
time_elpased: 1.085
batch start
#iterations: 6
currently lose_sum: 372.0129327774048
time_elpased: 1.071
batch start
#iterations: 7
currently lose_sum: 371.47331112623215
time_elpased: 1.055
batch start
#iterations: 8
currently lose_sum: 369.7101539969444
time_elpased: 1.07
batch start
#iterations: 9
currently lose_sum: 371.03173434734344
time_elpased: 1.073
batch start
#iterations: 10
currently lose_sum: 367.4401440024376
time_elpased: 1.063
batch start
#iterations: 11
currently lose_sum: 367.62699538469315
time_elpased: 1.079
batch start
#iterations: 12
currently lose_sum: 367.58534014225006
time_elpased: 1.072
batch start
#iterations: 13
currently lose_sum: 369.61438196897507
time_elpased: 1.07
batch start
#iterations: 14
currently lose_sum: 364.3216500580311
time_elpased: 1.064
batch start
#iterations: 15
currently lose_sum: 365.93172496557236
time_elpased: 1.058
batch start
#iterations: 16
currently lose_sum: 364.7308500409126
time_elpased: 1.074
batch start
#iterations: 17
currently lose_sum: 365.7638311982155
time_elpased: 1.072
batch start
#iterations: 18
currently lose_sum: 363.48094314336777
time_elpased: 1.059
batch start
#iterations: 19
currently lose_sum: 364.89291483163834
time_elpased: 1.072
start validation test
0.78118556701
0.843397898468
0.688934850052
0.758381239684
0
validation finish
batch start
#iterations: 20
currently lose_sum: 363.7786335349083
time_elpased: 1.082
batch start
#iterations: 21
currently lose_sum: 363.75348395109177
time_elpased: 1.061
batch start
#iterations: 22
currently lose_sum: 364.58972030878067
time_elpased: 1.073
batch start
#iterations: 23
currently lose_sum: 364.27824223041534
time_elpased: 1.068
batch start
#iterations: 24
currently lose_sum: 363.68020510673523
time_elpased: 1.065
batch start
#iterations: 25
currently lose_sum: 363.20966136455536
time_elpased: 1.075
batch start
#iterations: 26
currently lose_sum: 362.4527872800827
time_elpased: 1.078
batch start
#iterations: 27
currently lose_sum: 363.028955578804
time_elpased: 1.066
batch start
#iterations: 28
currently lose_sum: 361.10902962088585
time_elpased: 1.088
batch start
#iterations: 29
currently lose_sum: 362.83331286907196
time_elpased: 1.063
batch start
#iterations: 30
currently lose_sum: 362.8339440226555
time_elpased: 1.061
batch start
#iterations: 31
currently lose_sum: 361.3972609639168
time_elpased: 1.061
batch start
#iterations: 32
currently lose_sum: 362.8285663127899
time_elpased: 1.065
batch start
#iterations: 33
currently lose_sum: 362.5650914311409
time_elpased: 1.074
batch start
#iterations: 34
currently lose_sum: 362.592098236084
time_elpased: 1.075
batch start
#iterations: 35
currently lose_sum: 362.66458600759506
time_elpased: 1.06
batch start
#iterations: 36
currently lose_sum: 361.5419090092182
time_elpased: 1.087
batch start
#iterations: 37
currently lose_sum: 360.6670033931732
time_elpased: 1.071
batch start
#iterations: 38
currently lose_sum: 362.3892982006073
time_elpased: 1.071
batch start
#iterations: 39
currently lose_sum: 361.902538895607
time_elpased: 1.076
start validation test
0.789381443299
0.835415665545
0.719131334023
0.772924308103
0
validation finish
batch start
#iterations: 40
currently lose_sum: 360.68995279073715
time_elpased: 1.078
batch start
#iterations: 41
currently lose_sum: 361.1612951159477
time_elpased: 1.069
batch start
#iterations: 42
currently lose_sum: 359.7096599340439
time_elpased: 1.076
batch start
#iterations: 43
currently lose_sum: 360.2498854994774
time_elpased: 1.063
batch start
#iterations: 44
currently lose_sum: 360.61703085899353
time_elpased: 1.057
batch start
#iterations: 45
currently lose_sum: 359.2514125108719
time_elpased: 1.072
batch start
#iterations: 46
currently lose_sum: 359.8165393471718
time_elpased: 1.064
batch start
#iterations: 47
currently lose_sum: 360.62320417165756
time_elpased: 1.071
batch start
#iterations: 48
currently lose_sum: 359.91351932287216
time_elpased: 1.072
batch start
#iterations: 49
currently lose_sum: 360.18153315782547
time_elpased: 1.063
batch start
#iterations: 50
currently lose_sum: 358.4326068162918
time_elpased: 1.081
batch start
#iterations: 51
currently lose_sum: 360.0055801272392
time_elpased: 1.078
batch start
#iterations: 52
currently lose_sum: 359.97059375047684
time_elpased: 1.059
batch start
#iterations: 53
currently lose_sum: 360.92845779657364
time_elpased: 1.067
batch start
#iterations: 54
currently lose_sum: 359.9386736154556
time_elpased: 1.08
batch start
#iterations: 55
currently lose_sum: 359.15253734588623
time_elpased: 1.072
batch start
#iterations: 56
currently lose_sum: 360.63621163368225
time_elpased: 1.067
batch start
#iterations: 57
currently lose_sum: 359.85676270723343
time_elpased: 1.065
batch start
#iterations: 58
currently lose_sum: 358.73541951179504
time_elpased: 1.062
batch start
#iterations: 59
currently lose_sum: 359.91006803512573
time_elpased: 1.071
start validation test
0.799948453608
0.807239146587
0.78645294726
0.796710491855
0
validation finish
batch start
#iterations: 60
currently lose_sum: 359.9285418987274
time_elpased: 1.072
batch start
#iterations: 61
currently lose_sum: 359.08531790971756
time_elpased: 1.076
batch start
#iterations: 62
currently lose_sum: 358.83983170986176
time_elpased: 1.058
batch start
#iterations: 63
currently lose_sum: 358.5122643709183
time_elpased: 1.065
batch start
#iterations: 64
currently lose_sum: 360.07295191287994
time_elpased: 1.059
batch start
#iterations: 65
currently lose_sum: 360.0145572423935
time_elpased: 1.073
batch start
#iterations: 66
currently lose_sum: 359.3985491991043
time_elpased: 1.059
batch start
#iterations: 67
currently lose_sum: 359.1375336050987
time_elpased: 1.067
batch start
#iterations: 68
currently lose_sum: 359.5398955345154
time_elpased: 1.058
batch start
#iterations: 69
currently lose_sum: 359.08413368463516
time_elpased: 1.072
batch start
#iterations: 70
currently lose_sum: 358.8917714357376
time_elpased: 1.073
batch start
#iterations: 71
currently lose_sum: 359.11757731437683
time_elpased: 1.073
batch start
#iterations: 72
currently lose_sum: 358.6902984380722
time_elpased: 1.064
batch start
#iterations: 73
currently lose_sum: 357.86717307567596
time_elpased: 1.072
batch start
#iterations: 74
currently lose_sum: 357.8394402563572
time_elpased: 1.081
batch start
#iterations: 75
currently lose_sum: 357.79007464647293
time_elpased: 1.061
batch start
#iterations: 76
currently lose_sum: 358.7753522694111
time_elpased: 1.076
batch start
#iterations: 77
currently lose_sum: 358.5380831360817
time_elpased: 1.072
batch start
#iterations: 78
currently lose_sum: 359.25198209285736
time_elpased: 1.069
batch start
#iterations: 79
currently lose_sum: 358.68456095457077
time_elpased: 1.083
start validation test
0.801597938144
0.798054275474
0.805894519131
0.8019552354
0
validation finish
batch start
#iterations: 80
currently lose_sum: 357.37013906240463
time_elpased: 1.074
batch start
#iterations: 81
currently lose_sum: 359.77518832683563
time_elpased: 1.066
batch start
#iterations: 82
currently lose_sum: 358.7714778780937
time_elpased: 1.087
batch start
#iterations: 83
currently lose_sum: 358.659790456295
time_elpased: 1.06
batch start
#iterations: 84
currently lose_sum: 359.4161448478699
time_elpased: 1.071
batch start
#iterations: 85
currently lose_sum: 358.75348007678986
time_elpased: 1.069
batch start
#iterations: 86
currently lose_sum: 358.9413214325905
time_elpased: 1.055
batch start
#iterations: 87
currently lose_sum: 358.1660280227661
time_elpased: 1.074
batch start
#iterations: 88
currently lose_sum: 357.9426797032356
time_elpased: 1.07
batch start
#iterations: 89
currently lose_sum: 357.91674345731735
time_elpased: 1.071
batch start
#iterations: 90
currently lose_sum: 359.45963674783707
time_elpased: 1.072
batch start
#iterations: 91
currently lose_sum: 358.0140171647072
time_elpased: 1.076
batch start
#iterations: 92
currently lose_sum: 357.24282443523407
time_elpased: 1.065
batch start
#iterations: 93
currently lose_sum: 360.0938224196434
time_elpased: 1.072
batch start
#iterations: 94
currently lose_sum: 358.06388768553734
time_elpased: 1.07
batch start
#iterations: 95
currently lose_sum: 357.8659151792526
time_elpased: 1.061
batch start
#iterations: 96
currently lose_sum: 359.365721732378
time_elpased: 1.067
batch start
#iterations: 97
currently lose_sum: 358.0624466538429
time_elpased: 1.067
batch start
#iterations: 98
currently lose_sum: 358.5813383758068
time_elpased: 1.067
batch start
#iterations: 99
currently lose_sum: 357.80427902936935
time_elpased: 1.077
start validation test
0.800721649485
0.814955502496
0.776525336091
0.795276424486
0
validation finish
batch start
#iterations: 100
currently lose_sum: 357.8155178427696
time_elpased: 1.079
batch start
#iterations: 101
currently lose_sum: 357.28859907388687
time_elpased: 1.069
batch start
#iterations: 102
currently lose_sum: 357.8117345571518
time_elpased: 1.067
batch start
#iterations: 103
currently lose_sum: 357.76157534122467
time_elpased: 1.069
batch start
#iterations: 104
currently lose_sum: 358.44068616628647
time_elpased: 1.071
batch start
#iterations: 105
currently lose_sum: 357.2682881951332
time_elpased: 1.083
batch start
#iterations: 106
currently lose_sum: 358.3748849630356
time_elpased: 1.06
batch start
#iterations: 107
currently lose_sum: 357.76366913318634
time_elpased: 1.06
batch start
#iterations: 108
currently lose_sum: 358.15102848410606
time_elpased: 1.075
batch start
#iterations: 109
currently lose_sum: 357.6157324016094
time_elpased: 1.085
batch start
#iterations: 110
currently lose_sum: 359.3283897638321
time_elpased: 1.081
batch start
#iterations: 111
currently lose_sum: 357.16950356960297
time_elpased: 1.069
batch start
#iterations: 112
currently lose_sum: 357.65061938762665
time_elpased: 1.059
batch start
#iterations: 113
currently lose_sum: 357.77189695835114
time_elpased: 1.069
batch start
#iterations: 114
currently lose_sum: 356.67674148082733
time_elpased: 1.074
batch start
#iterations: 115
currently lose_sum: 356.9678974747658
time_elpased: 1.059
batch start
#iterations: 116
currently lose_sum: 358.7148446440697
time_elpased: 1.063
batch start
#iterations: 117
currently lose_sum: 357.2199862599373
time_elpased: 1.076
batch start
#iterations: 118
currently lose_sum: 356.2264649271965
time_elpased: 1.065
batch start
#iterations: 119
currently lose_sum: 356.96015936136246
time_elpased: 1.086
start validation test
0.800515463918
0.817495073352
0.772182006205
0.794192724952
0
validation finish
batch start
#iterations: 120
currently lose_sum: 357.77665919065475
time_elpased: 1.077
batch start
#iterations: 121
currently lose_sum: 356.8256841301918
time_elpased: 1.069
batch start
#iterations: 122
currently lose_sum: 355.29306465387344
time_elpased: 1.071
batch start
#iterations: 123
currently lose_sum: 357.4090100824833
time_elpased: 1.068
batch start
#iterations: 124
currently lose_sum: 355.966434776783
time_elpased: 1.074
batch start
#iterations: 125
currently lose_sum: 357.25821232795715
time_elpased: 1.069
batch start
#iterations: 126
currently lose_sum: 359.03916895389557
time_elpased: 1.072
batch start
#iterations: 127
currently lose_sum: 357.18018248677254
time_elpased: 1.072
batch start
#iterations: 128
currently lose_sum: 356.83980709314346
time_elpased: 1.078
batch start
#iterations: 129
currently lose_sum: 358.70326232910156
time_elpased: 1.065
batch start
#iterations: 130
currently lose_sum: 357.15749007463455
time_elpased: 1.067
batch start
#iterations: 131
currently lose_sum: 356.3692868947983
time_elpased: 1.066
batch start
#iterations: 132
currently lose_sum: 357.571076631546
time_elpased: 1.064
batch start
#iterations: 133
currently lose_sum: 356.8835350871086
time_elpased: 1.082
batch start
#iterations: 134
currently lose_sum: 357.74714624881744
time_elpased: 1.06
batch start
#iterations: 135
currently lose_sum: 357.56797486543655
time_elpased: 1.073
batch start
#iterations: 136
currently lose_sum: 359.6684555411339
time_elpased: 1.086
batch start
#iterations: 137
currently lose_sum: 357.42168414592743
time_elpased: 1.078
batch start
#iterations: 138
currently lose_sum: 357.9514048695564
time_elpased: 1.074
batch start
#iterations: 139
currently lose_sum: 357.0659973025322
time_elpased: 1.077
start validation test
0.798144329897
0.80554375531
0.784384694933
0.794823430787
0
validation finish
batch start
#iterations: 140
currently lose_sum: 357.76663160324097
time_elpased: 1.065
batch start
#iterations: 141
currently lose_sum: 358.8767243027687
time_elpased: 1.071
batch start
#iterations: 142
currently lose_sum: 358.3671959042549
time_elpased: 1.065
batch start
#iterations: 143
currently lose_sum: 357.19611525535583
time_elpased: 1.076
batch start
#iterations: 144
currently lose_sum: 357.60184240341187
time_elpased: 1.066
batch start
#iterations: 145
currently lose_sum: 357.05823200941086
time_elpased: 1.075
batch start
#iterations: 146
currently lose_sum: 356.662422478199
time_elpased: 1.073
batch start
#iterations: 147
currently lose_sum: 357.12494844198227
time_elpased: 1.075
batch start
#iterations: 148
currently lose_sum: 357.5436706542969
time_elpased: 1.071
batch start
#iterations: 149
currently lose_sum: 357.8046548962593
time_elpased: 1.079
batch start
#iterations: 150
currently lose_sum: 356.679950773716
time_elpased: 1.071
batch start
#iterations: 151
currently lose_sum: 357.12672317028046
time_elpased: 1.07
batch start
#iterations: 152
currently lose_sum: 357.0099808573723
time_elpased: 1.067
batch start
#iterations: 153
currently lose_sum: 358.6367951333523
time_elpased: 1.071
batch start
#iterations: 154
currently lose_sum: 357.3625490665436
time_elpased: 1.079
batch start
#iterations: 155
currently lose_sum: 357.7938639521599
time_elpased: 1.063
batch start
#iterations: 156
currently lose_sum: 357.26522094011307
time_elpased: 1.07
batch start
#iterations: 157
currently lose_sum: 357.1473018527031
time_elpased: 1.078
batch start
#iterations: 158
currently lose_sum: 357.90317994356155
time_elpased: 1.067
batch start
#iterations: 159
currently lose_sum: 358.9160547852516
time_elpased: 1.074
start validation test
0.80118556701
0.792905371359
0.813650465357
0.803143979993
0
validation finish
batch start
#iterations: 160
currently lose_sum: 357.1272609233856
time_elpased: 1.075
batch start
#iterations: 161
currently lose_sum: 356.4619369804859
time_elpased: 1.07
batch start
#iterations: 162
currently lose_sum: 357.2782726287842
time_elpased: 1.066
batch start
#iterations: 163
currently lose_sum: 356.94311743974686
time_elpased: 1.069
batch start
#iterations: 164
currently lose_sum: 356.8972960114479
time_elpased: 1.078
batch start
#iterations: 165
currently lose_sum: 355.5723043680191
time_elpased: 1.071
batch start
#iterations: 166
currently lose_sum: 356.2812815606594
time_elpased: 1.077
batch start
#iterations: 167
currently lose_sum: 357.4979692697525
time_elpased: 1.071
batch start
#iterations: 168
currently lose_sum: 356.79069596529007
time_elpased: 1.071
batch start
#iterations: 169
currently lose_sum: 356.98812061548233
time_elpased: 1.078
batch start
#iterations: 170
currently lose_sum: 357.79430758953094
time_elpased: 1.075
batch start
#iterations: 171
currently lose_sum: 356.4240909218788
time_elpased: 1.069
batch start
#iterations: 172
currently lose_sum: 357.2627167105675
time_elpased: 1.074
batch start
#iterations: 173
currently lose_sum: 358.31336003541946
time_elpased: 1.082
batch start
#iterations: 174
currently lose_sum: 355.81500416994095
time_elpased: 1.076
batch start
#iterations: 175
currently lose_sum: 358.3433846235275
time_elpased: 1.072
batch start
#iterations: 176
currently lose_sum: 357.238406509161
time_elpased: 1.067
batch start
#iterations: 177
currently lose_sum: 356.4998363852501
time_elpased: 1.065
batch start
#iterations: 178
currently lose_sum: 357.9356316924095
time_elpased: 1.072
batch start
#iterations: 179
currently lose_sum: 356.9054065942764
time_elpased: 1.064
start validation test
0.800257731959
0.80267418782
0.79462254395
0.798628072546
0
validation finish
batch start
#iterations: 180
currently lose_sum: 357.6615818142891
time_elpased: 1.075
batch start
#iterations: 181
currently lose_sum: 355.31436479091644
time_elpased: 1.075
batch start
#iterations: 182
currently lose_sum: 357.32158321142197
time_elpased: 1.075
batch start
#iterations: 183
currently lose_sum: 357.0238580107689
time_elpased: 1.075
batch start
#iterations: 184
currently lose_sum: 356.30508503317833
time_elpased: 1.078
batch start
#iterations: 185
currently lose_sum: 356.9475809633732
time_elpased: 1.078
batch start
#iterations: 186
currently lose_sum: 356.85382664203644
time_elpased: 1.067
batch start
#iterations: 187
currently lose_sum: 355.81116247177124
time_elpased: 1.069
batch start
#iterations: 188
currently lose_sum: 357.67738339304924
time_elpased: 1.07
batch start
#iterations: 189
currently lose_sum: 357.36200469732285
time_elpased: 1.076
batch start
#iterations: 190
currently lose_sum: 355.63657742738724
time_elpased: 1.06
batch start
#iterations: 191
currently lose_sum: 356.4355962276459
time_elpased: 1.078
batch start
#iterations: 192
currently lose_sum: 357.5411574244499
time_elpased: 1.086
batch start
#iterations: 193
currently lose_sum: 357.4008705019951
time_elpased: 1.067
batch start
#iterations: 194
currently lose_sum: 356.4491472542286
time_elpased: 1.071
batch start
#iterations: 195
currently lose_sum: 356.9187387228012
time_elpased: 1.104
batch start
#iterations: 196
currently lose_sum: 358.43670415878296
time_elpased: 1.062
batch start
#iterations: 197
currently lose_sum: 356.89569902420044
time_elpased: 1.071
batch start
#iterations: 198
currently lose_sum: 357.56822270154953
time_elpased: 1.081
batch start
#iterations: 199
currently lose_sum: 356.1371532380581
time_elpased: 1.097
start validation test
0.798041237113
0.824531708418
0.75563598759
0.788581912368
0
validation finish
batch start
#iterations: 200
currently lose_sum: 358.39093375205994
time_elpased: 1.086
batch start
#iterations: 201
currently lose_sum: 355.72975039482117
time_elpased: 1.066
batch start
#iterations: 202
currently lose_sum: 355.5525820851326
time_elpased: 1.086
batch start
#iterations: 203
currently lose_sum: 355.5377061665058
time_elpased: 1.064
batch start
#iterations: 204
currently lose_sum: 357.2536197900772
time_elpased: 1.072
batch start
#iterations: 205
currently lose_sum: 355.2980343103409
time_elpased: 1.061
batch start
#iterations: 206
currently lose_sum: 357.2274693250656
time_elpased: 1.073
batch start
#iterations: 207
currently lose_sum: 358.0509914755821
time_elpased: 1.07
batch start
#iterations: 208
currently lose_sum: 357.0891543626785
time_elpased: 1.076
batch start
#iterations: 209
currently lose_sum: 355.6402864456177
time_elpased: 1.091
batch start
#iterations: 210
currently lose_sum: 356.2821502685547
time_elpased: 1.07
batch start
#iterations: 211
currently lose_sum: 356.4289175271988
time_elpased: 1.073
batch start
#iterations: 212
currently lose_sum: 356.8088707923889
time_elpased: 1.083
batch start
#iterations: 213
currently lose_sum: 357.4961541593075
time_elpased: 1.076
batch start
#iterations: 214
currently lose_sum: 357.17590445280075
time_elpased: 1.079
batch start
#iterations: 215
currently lose_sum: 355.63728058338165
time_elpased: 1.067
batch start
#iterations: 216
currently lose_sum: 356.8628039956093
time_elpased: 1.076
batch start
#iterations: 217
currently lose_sum: 357.0597599744797
time_elpased: 1.078
batch start
#iterations: 218
currently lose_sum: 356.9329332113266
time_elpased: 1.074
batch start
#iterations: 219
currently lose_sum: 358.03409069776535
time_elpased: 1.073
start validation test
0.800515463918
0.811693895099
0.780972078594
0.796036681775
0
validation finish
batch start
#iterations: 220
currently lose_sum: 355.17485749721527
time_elpased: 1.083
batch start
#iterations: 221
currently lose_sum: 356.6443005800247
time_elpased: 1.087
batch start
#iterations: 222
currently lose_sum: 357.6511726975441
time_elpased: 1.069
batch start
#iterations: 223
currently lose_sum: 356.8417963385582
time_elpased: 1.091
batch start
#iterations: 224
currently lose_sum: 356.67813342809677
time_elpased: 1.071
batch start
#iterations: 225
currently lose_sum: 356.69863736629486
time_elpased: 1.06
batch start
#iterations: 226
currently lose_sum: 356.21742352843285
time_elpased: 1.073
batch start
#iterations: 227
currently lose_sum: 357.49660205841064
time_elpased: 1.079
batch start
#iterations: 228
currently lose_sum: 356.40161284804344
time_elpased: 1.067
batch start
#iterations: 229
currently lose_sum: 356.5736926794052
time_elpased: 1.067
batch start
#iterations: 230
currently lose_sum: 356.84740084409714
time_elpased: 1.081
batch start
#iterations: 231
currently lose_sum: 356.75931575894356
time_elpased: 1.068
batch start
#iterations: 232
currently lose_sum: 357.0101742744446
time_elpased: 1.074
batch start
#iterations: 233
currently lose_sum: 356.1399411559105
time_elpased: 1.074
batch start
#iterations: 234
currently lose_sum: 356.8221130669117
time_elpased: 1.075
batch start
#iterations: 235
currently lose_sum: 355.45418721437454
time_elpased: 1.07
batch start
#iterations: 236
currently lose_sum: 356.29855155944824
time_elpased: 1.069
batch start
#iterations: 237
currently lose_sum: 356.2321302294731
time_elpased: 1.069
batch start
#iterations: 238
currently lose_sum: 356.3514366745949
time_elpased: 1.071
batch start
#iterations: 239
currently lose_sum: 354.8874780535698
time_elpased: 1.076
start validation test
0.800670103093
0.811955703688
0.780972078594
0.796162563913
0
validation finish
batch start
#iterations: 240
currently lose_sum: 355.8768841922283
time_elpased: 1.088
batch start
#iterations: 241
currently lose_sum: 356.04722517728806
time_elpased: 1.063
batch start
#iterations: 242
currently lose_sum: 354.5563616156578
time_elpased: 1.073
batch start
#iterations: 243
currently lose_sum: 356.6993297934532
time_elpased: 1.075
batch start
#iterations: 244
currently lose_sum: 356.78196626901627
time_elpased: 1.07
batch start
#iterations: 245
currently lose_sum: 355.6001474559307
time_elpased: 1.088
batch start
#iterations: 246
currently lose_sum: 356.80897122621536
time_elpased: 1.074
batch start
#iterations: 247
currently lose_sum: 357.4789347052574
time_elpased: 1.064
batch start
#iterations: 248
currently lose_sum: 358.20225524902344
time_elpased: 1.071
batch start
#iterations: 249
currently lose_sum: 356.2164333462715
time_elpased: 1.078
batch start
#iterations: 250
currently lose_sum: 357.089276432991
time_elpased: 1.092
batch start
#iterations: 251
currently lose_sum: 358.0439596772194
time_elpased: 1.094
batch start
#iterations: 252
currently lose_sum: 356.4924164414406
time_elpased: 1.071
batch start
#iterations: 253
currently lose_sum: 355.11016207933426
time_elpased: 1.079
batch start
#iterations: 254
currently lose_sum: 356.18148082494736
time_elpased: 1.088
batch start
#iterations: 255
currently lose_sum: 356.7782975435257
time_elpased: 1.072
batch start
#iterations: 256
currently lose_sum: 356.23153257369995
time_elpased: 1.067
batch start
#iterations: 257
currently lose_sum: 358.27472162246704
time_elpased: 1.086
batch start
#iterations: 258
currently lose_sum: 358.5138649344444
time_elpased: 1.09
batch start
#iterations: 259
currently lose_sum: 355.8730046451092
time_elpased: 1.079
start validation test
0.800567010309
0.807093700371
0.788314374354
0.797593512948
0
validation finish
batch start
#iterations: 260
currently lose_sum: 355.69918793439865
time_elpased: 1.083
batch start
#iterations: 261
currently lose_sum: 355.8212884068489
time_elpased: 1.068
batch start
#iterations: 262
currently lose_sum: 357.52914905548096
time_elpased: 1.068
batch start
#iterations: 263
currently lose_sum: 358.37606048583984
time_elpased: 1.064
batch start
#iterations: 264
currently lose_sum: 356.12178003787994
time_elpased: 1.072
batch start
#iterations: 265
currently lose_sum: 355.9010987877846
time_elpased: 1.068
batch start
#iterations: 266
currently lose_sum: 357.0976966023445
time_elpased: 1.081
batch start
#iterations: 267
currently lose_sum: 357.62519973516464
time_elpased: 1.079
batch start
#iterations: 268
currently lose_sum: 356.3757736682892
time_elpased: 1.065
batch start
#iterations: 269
currently lose_sum: 357.55326449871063
time_elpased: 1.075
batch start
#iterations: 270
currently lose_sum: 354.96235674619675
time_elpased: 1.073
batch start
#iterations: 271
currently lose_sum: 356.5920324921608
time_elpased: 1.069
batch start
#iterations: 272
currently lose_sum: 357.0191893875599
time_elpased: 1.07
batch start
#iterations: 273
currently lose_sum: 356.4345569014549
time_elpased: 1.071
batch start
#iterations: 274
currently lose_sum: 356.58319240808487
time_elpased: 1.08
batch start
#iterations: 275
currently lose_sum: 357.70419120788574
time_elpased: 1.081
batch start
#iterations: 276
currently lose_sum: 355.505142390728
time_elpased: 1.083
batch start
#iterations: 277
currently lose_sum: 356.1981313228607
time_elpased: 1.067
batch start
#iterations: 278
currently lose_sum: 356.5070774257183
time_elpased: 1.064
batch start
#iterations: 279
currently lose_sum: 355.5237966179848
time_elpased: 1.072
start validation test
0.799587628866
0.813149913345
0.776318510858
0.79430748069
0
validation finish
batch start
#iterations: 280
currently lose_sum: 355.82403296232224
time_elpased: 1.076
batch start
#iterations: 281
currently lose_sum: 358.1191197037697
time_elpased: 1.068
batch start
#iterations: 282
currently lose_sum: 356.13068801164627
time_elpased: 1.06
batch start
#iterations: 283
currently lose_sum: 355.663074195385
time_elpased: 1.069
batch start
#iterations: 284
currently lose_sum: 356.51154178380966
time_elpased: 1.075
batch start
#iterations: 285
currently lose_sum: 354.68390703201294
time_elpased: 1.063
batch start
#iterations: 286
currently lose_sum: 356.21499878168106
time_elpased: 1.065
batch start
#iterations: 287
currently lose_sum: 355.3923261165619
time_elpased: 1.079
batch start
#iterations: 288
currently lose_sum: 356.4827849268913
time_elpased: 1.057
batch start
#iterations: 289
currently lose_sum: 356.00703263282776
time_elpased: 1.077
batch start
#iterations: 290
currently lose_sum: 355.467023730278
time_elpased: 1.069
batch start
#iterations: 291
currently lose_sum: 356.48699283599854
time_elpased: 1.084
batch start
#iterations: 292
currently lose_sum: 356.39778858423233
time_elpased: 1.065
batch start
#iterations: 293
currently lose_sum: 356.8253848850727
time_elpased: 1.069
batch start
#iterations: 294
currently lose_sum: 354.8528963327408
time_elpased: 1.076
batch start
#iterations: 295
currently lose_sum: 356.123419046402
time_elpased: 1.073
batch start
#iterations: 296
currently lose_sum: 355.72157126665115
time_elpased: 1.067
batch start
#iterations: 297
currently lose_sum: 355.65587317943573
time_elpased: 1.071
batch start
#iterations: 298
currently lose_sum: 354.65792709589005
time_elpased: 1.063
batch start
#iterations: 299
currently lose_sum: 355.1513468027115
time_elpased: 1.065
start validation test
0.801134020619
0.801139896373
0.799482936918
0.800310559006
0
validation finish
batch start
#iterations: 300
currently lose_sum: 355.33826196193695
time_elpased: 1.093
batch start
#iterations: 301
currently lose_sum: 356.2141039967537
time_elpased: 1.072
batch start
#iterations: 302
currently lose_sum: 357.6184206008911
time_elpased: 1.065
batch start
#iterations: 303
currently lose_sum: 357.22717958688736
time_elpased: 1.07
batch start
#iterations: 304
currently lose_sum: 356.7139005661011
time_elpased: 1.082
batch start
#iterations: 305
currently lose_sum: 356.719541490078
time_elpased: 1.08
batch start
#iterations: 306
currently lose_sum: 354.54905819892883
time_elpased: 1.076
batch start
#iterations: 307
currently lose_sum: 355.87248089909554
time_elpased: 1.063
batch start
#iterations: 308
currently lose_sum: 355.0361967384815
time_elpased: 1.071
batch start
#iterations: 309
currently lose_sum: 355.89676427841187
time_elpased: 1.08
batch start
#iterations: 310
currently lose_sum: 355.41416585445404
time_elpased: 1.074
batch start
#iterations: 311
currently lose_sum: 357.3393408060074
time_elpased: 1.069
batch start
#iterations: 312
currently lose_sum: 353.5211380124092
time_elpased: 1.077
batch start
#iterations: 313
currently lose_sum: 355.76588892936707
time_elpased: 1.081
batch start
#iterations: 314
currently lose_sum: 357.68901884555817
time_elpased: 1.082
batch start
#iterations: 315
currently lose_sum: 356.82216441631317
time_elpased: 1.088
batch start
#iterations: 316
currently lose_sum: 355.18742763996124
time_elpased: 1.074
batch start
#iterations: 317
currently lose_sum: 357.67490977048874
time_elpased: 1.072
batch start
#iterations: 318
currently lose_sum: 357.63747811317444
time_elpased: 1.065
batch start
#iterations: 319
currently lose_sum: 356.7841500043869
time_elpased: 1.067
start validation test
0.800206185567
0.803160318125
0.793691830403
0.798398002705
0
validation finish
batch start
#iterations: 320
currently lose_sum: 355.8057140111923
time_elpased: 1.078
batch start
#iterations: 321
currently lose_sum: 357.20042592287064
time_elpased: 1.085
batch start
#iterations: 322
currently lose_sum: 355.99731492996216
time_elpased: 1.072
batch start
#iterations: 323
currently lose_sum: 357.0133131146431
time_elpased: 1.064
batch start
#iterations: 324
currently lose_sum: 356.8580273091793
time_elpased: 1.069
batch start
#iterations: 325
currently lose_sum: 356.2191841006279
time_elpased: 1.077
batch start
#iterations: 326
currently lose_sum: 356.1628956794739
time_elpased: 1.071
batch start
#iterations: 327
currently lose_sum: 355.5663672685623
time_elpased: 1.073
batch start
#iterations: 328
currently lose_sum: 355.48329335451126
time_elpased: 1.065
batch start
#iterations: 329
currently lose_sum: 355.9496097564697
time_elpased: 1.082
batch start
#iterations: 330
currently lose_sum: 357.2152800858021
time_elpased: 1.084
batch start
#iterations: 331
currently lose_sum: 355.34316098690033
time_elpased: 1.087
batch start
#iterations: 332
currently lose_sum: 355.7118007540703
time_elpased: 1.079
batch start
#iterations: 333
currently lose_sum: 356.02373147010803
time_elpased: 1.062
batch start
#iterations: 334
currently lose_sum: 356.0848028063774
time_elpased: 1.073
batch start
#iterations: 335
currently lose_sum: 355.99553138017654
time_elpased: 1.068
batch start
#iterations: 336
currently lose_sum: 357.34909278154373
time_elpased: 1.071
batch start
#iterations: 337
currently lose_sum: 354.6032382249832
time_elpased: 1.066
batch start
#iterations: 338
currently lose_sum: 356.69137078523636
time_elpased: 1.078
batch start
#iterations: 339
currently lose_sum: 355.2387447953224
time_elpased: 1.069
start validation test
0.801134020619
0.810935159426
0.783764219235
0.797118216239
0
validation finish
batch start
#iterations: 340
currently lose_sum: 355.61409842967987
time_elpased: 1.092
batch start
#iterations: 341
currently lose_sum: 356.8993276953697
time_elpased: 1.078
batch start
#iterations: 342
currently lose_sum: 354.6614316999912
time_elpased: 1.071
batch start
#iterations: 343
currently lose_sum: 355.5145686864853
time_elpased: 1.08
batch start
#iterations: 344
currently lose_sum: 354.96530014276505
time_elpased: 1.076
batch start
#iterations: 345
currently lose_sum: 355.72387313842773
time_elpased: 1.082
batch start
#iterations: 346
currently lose_sum: 355.8679000735283
time_elpased: 1.081
batch start
#iterations: 347
currently lose_sum: 357.0131428837776
time_elpased: 1.072
batch start
#iterations: 348
currently lose_sum: 354.55501264333725
time_elpased: 1.064
batch start
#iterations: 349
currently lose_sum: 355.2929896712303
time_elpased: 1.071
batch start
#iterations: 350
currently lose_sum: 356.20533949136734
time_elpased: 1.08
batch start
#iterations: 351
currently lose_sum: 356.2026730775833
time_elpased: 1.08
batch start
#iterations: 352
currently lose_sum: 358.4287192225456
time_elpased: 1.089
batch start
#iterations: 353
currently lose_sum: 355.6001842021942
time_elpased: 1.073
batch start
#iterations: 354
currently lose_sum: 355.1204027235508
time_elpased: 1.087
batch start
#iterations: 355
currently lose_sum: 354.99379044771194
time_elpased: 1.084
batch start
#iterations: 356
currently lose_sum: 356.7049380540848
time_elpased: 1.096
batch start
#iterations: 357
currently lose_sum: 355.93300354480743
time_elpased: 1.075
batch start
#iterations: 358
currently lose_sum: 355.0669788122177
time_elpased: 1.078
batch start
#iterations: 359
currently lose_sum: 354.6205946803093
time_elpased: 1.072
start validation test
0.799226804124
0.813961074263
0.774146845915
0.793554884189
0
validation finish
batch start
#iterations: 360
currently lose_sum: 356.7708931863308
time_elpased: 1.086
batch start
#iterations: 361
currently lose_sum: 355.54270774126053
time_elpased: 1.085
batch start
#iterations: 362
currently lose_sum: 355.22028362751007
time_elpased: 1.078
batch start
#iterations: 363
currently lose_sum: 357.83082342147827
time_elpased: 1.066
batch start
#iterations: 364
currently lose_sum: 354.96707731485367
time_elpased: 1.069
batch start
#iterations: 365
currently lose_sum: 355.2974811196327
time_elpased: 1.082
batch start
#iterations: 366
currently lose_sum: 356.37551552057266
time_elpased: 1.086
batch start
#iterations: 367
currently lose_sum: 356.7019665837288
time_elpased: 1.071
batch start
#iterations: 368
currently lose_sum: 354.6190212368965
time_elpased: 1.079
batch start
#iterations: 369
currently lose_sum: 354.19913363456726
time_elpased: 1.08
batch start
#iterations: 370
currently lose_sum: 355.3907664716244
time_elpased: 1.062
batch start
#iterations: 371
currently lose_sum: 356.39509254693985
time_elpased: 1.085
batch start
#iterations: 372
currently lose_sum: 356.9894292652607
time_elpased: 1.067
batch start
#iterations: 373
currently lose_sum: 355.8635553121567
time_elpased: 1.075
batch start
#iterations: 374
currently lose_sum: 356.28323698043823
time_elpased: 1.069
batch start
#iterations: 375
currently lose_sum: 354.8416620492935
time_elpased: 1.074
batch start
#iterations: 376
currently lose_sum: 356.0565333366394
time_elpased: 1.084
batch start
#iterations: 377
currently lose_sum: 356.12045377492905
time_elpased: 1.079
batch start
#iterations: 378
currently lose_sum: 355.639806330204
time_elpased: 1.083
batch start
#iterations: 379
currently lose_sum: 355.791883289814
time_elpased: 1.066
start validation test
0.798865979381
0.805573214664
0.786246122027
0.795792338288
0
validation finish
batch start
#iterations: 380
currently lose_sum: 356.88469713926315
time_elpased: 1.076
batch start
#iterations: 381
currently lose_sum: 356.70484721660614
time_elpased: 1.076
batch start
#iterations: 382
currently lose_sum: 356.0253067314625
time_elpased: 1.069
batch start
#iterations: 383
currently lose_sum: 355.92477428913116
time_elpased: 1.077
batch start
#iterations: 384
currently lose_sum: 355.7606471776962
time_elpased: 1.083
batch start
#iterations: 385
currently lose_sum: 355.03319534659386
time_elpased: 1.083
batch start
#iterations: 386
currently lose_sum: 356.39372420310974
time_elpased: 1.065
batch start
#iterations: 387
currently lose_sum: 354.90649342536926
time_elpased: 1.076
batch start
#iterations: 388
currently lose_sum: 355.3566711843014
time_elpased: 1.069
batch start
#iterations: 389
currently lose_sum: 355.59167405962944
time_elpased: 1.071
batch start
#iterations: 390
currently lose_sum: 355.1202062666416
time_elpased: 1.07
batch start
#iterations: 391
currently lose_sum: 355.04300582408905
time_elpased: 1.088
batch start
#iterations: 392
currently lose_sum: 355.37038922309875
time_elpased: 1.075
batch start
#iterations: 393
currently lose_sum: 356.31022053956985
time_elpased: 1.073
batch start
#iterations: 394
currently lose_sum: 356.5719390511513
time_elpased: 1.072
batch start
#iterations: 395
currently lose_sum: 355.84487548470497
time_elpased: 1.068
batch start
#iterations: 396
currently lose_sum: 356.64859479665756
time_elpased: 1.084
batch start
#iterations: 397
currently lose_sum: 354.1852695941925
time_elpased: 1.083
batch start
#iterations: 398
currently lose_sum: 356.25301492214203
time_elpased: 1.072
batch start
#iterations: 399
currently lose_sum: 354.48603969812393
time_elpased: 1.068
start validation test
0.799381443299
0.801565762004
0.794105480869
0.797818181818
0
validation finish
acc: 0.803
pre: 0.799
rec: 0.815
F1: 0.807
auc: 0.000
