start to construct graph
graph construct over
58302
epochs start
batch start
#iterations: 0
currently lose_sum: 393.85441356897354
time_elpased: 1.109
batch start
#iterations: 1
currently lose_sum: 384.1471173763275
time_elpased: 1.061
batch start
#iterations: 2
currently lose_sum: 380.7797839641571
time_elpased: 1.071
batch start
#iterations: 3
currently lose_sum: 375.4550476074219
time_elpased: 1.076
batch start
#iterations: 4
currently lose_sum: 374.72227931022644
time_elpased: 1.059
batch start
#iterations: 5
currently lose_sum: 373.7579575777054
time_elpased: 1.06
batch start
#iterations: 6
currently lose_sum: 372.97477972507477
time_elpased: 1.062
batch start
#iterations: 7
currently lose_sum: 370.7350800037384
time_elpased: 1.064
batch start
#iterations: 8
currently lose_sum: 369.16360288858414
time_elpased: 1.075
batch start
#iterations: 9
currently lose_sum: 369.13225120306015
time_elpased: 1.065
batch start
#iterations: 10
currently lose_sum: 368.167185485363
time_elpased: 1.065
batch start
#iterations: 11
currently lose_sum: 366.06223940849304
time_elpased: 1.06
batch start
#iterations: 12
currently lose_sum: 366.3856418132782
time_elpased: 1.074
batch start
#iterations: 13
currently lose_sum: 365.45984357595444
time_elpased: 1.076
batch start
#iterations: 14
currently lose_sum: 365.23758363723755
time_elpased: 1.075
batch start
#iterations: 15
currently lose_sum: 364.0255138874054
time_elpased: 1.065
batch start
#iterations: 16
currently lose_sum: 366.2373461127281
time_elpased: 1.069
batch start
#iterations: 17
currently lose_sum: 364.34298634529114
time_elpased: 1.082
batch start
#iterations: 18
currently lose_sum: 364.06531858444214
time_elpased: 1.063
batch start
#iterations: 19
currently lose_sum: 362.7119438648224
time_elpased: 1.065
start validation test
0.798144329897
0.80109132091
0.796825396825
0.798952664545
0
validation finish
batch start
#iterations: 20
currently lose_sum: 362.4432135820389
time_elpased: 1.092
batch start
#iterations: 21
currently lose_sum: 362.71041882038116
time_elpased: 1.064
batch start
#iterations: 22
currently lose_sum: 363.39659041166306
time_elpased: 1.071
batch start
#iterations: 23
currently lose_sum: 360.83843809366226
time_elpased: 1.064
batch start
#iterations: 24
currently lose_sum: 362.73051595687866
time_elpased: 1.067
batch start
#iterations: 25
currently lose_sum: 362.6544740796089
time_elpased: 1.062
batch start
#iterations: 26
currently lose_sum: 361.46458595991135
time_elpased: 1.069
batch start
#iterations: 27
currently lose_sum: 363.4545484781265
time_elpased: 1.071
batch start
#iterations: 28
currently lose_sum: 361.4815359413624
time_elpased: 1.064
batch start
#iterations: 29
currently lose_sum: 362.48220258951187
time_elpased: 1.073
batch start
#iterations: 30
currently lose_sum: 361.6010035276413
time_elpased: 1.056
batch start
#iterations: 31
currently lose_sum: 361.3103922009468
time_elpased: 1.066
batch start
#iterations: 32
currently lose_sum: 360.51894932985306
time_elpased: 1.084
batch start
#iterations: 33
currently lose_sum: 360.7922827601433
time_elpased: 1.07
batch start
#iterations: 34
currently lose_sum: 360.85492873191833
time_elpased: 1.067
batch start
#iterations: 35
currently lose_sum: 360.7651551961899
time_elpased: 1.075
batch start
#iterations: 36
currently lose_sum: 361.27226608991623
time_elpased: 1.067
batch start
#iterations: 37
currently lose_sum: 359.01265501976013
time_elpased: 1.062
batch start
#iterations: 38
currently lose_sum: 359.13014829158783
time_elpased: 1.064
batch start
#iterations: 39
currently lose_sum: 360.25504326820374
time_elpased: 1.072
start validation test
0.805051546392
0.822047583163
0.781976446493
0.80151149365
0
validation finish
batch start
#iterations: 40
currently lose_sum: 359.48547530174255
time_elpased: 1.083
batch start
#iterations: 41
currently lose_sum: 358.51073211431503
time_elpased: 1.073
batch start
#iterations: 42
currently lose_sum: 359.8117279410362
time_elpased: 1.079
batch start
#iterations: 43
currently lose_sum: 358.55617785453796
time_elpased: 1.074
batch start
#iterations: 44
currently lose_sum: 358.40192836523056
time_elpased: 1.063
batch start
#iterations: 45
currently lose_sum: 358.33270341157913
time_elpased: 1.069
batch start
#iterations: 46
currently lose_sum: 360.9567787051201
time_elpased: 1.069
batch start
#iterations: 47
currently lose_sum: 359.31980526447296
time_elpased: 1.064
batch start
#iterations: 48
currently lose_sum: 358.3813996911049
time_elpased: 1.06
batch start
#iterations: 49
currently lose_sum: 358.3860378265381
time_elpased: 1.062
batch start
#iterations: 50
currently lose_sum: 361.59034234285355
time_elpased: 1.063
batch start
#iterations: 51
currently lose_sum: 359.62088537216187
time_elpased: 1.058
batch start
#iterations: 52
currently lose_sum: 359.09319269657135
time_elpased: 1.065
batch start
#iterations: 53
currently lose_sum: 359.8126456141472
time_elpased: 1.073
batch start
#iterations: 54
currently lose_sum: 358.14860051870346
time_elpased: 1.066
batch start
#iterations: 55
currently lose_sum: 357.8542380928993
time_elpased: 1.073
batch start
#iterations: 56
currently lose_sum: 359.3983400464058
time_elpased: 1.072
batch start
#iterations: 57
currently lose_sum: 358.671351313591
time_elpased: 1.071
batch start
#iterations: 58
currently lose_sum: 357.4668538272381
time_elpased: 1.065
batch start
#iterations: 59
currently lose_sum: 359.8220799565315
time_elpased: 1.072
start validation test
0.802268041237
0.830306406685
0.763133640553
0.79530416222
0
validation finish
batch start
#iterations: 60
currently lose_sum: 358.13395673036575
time_elpased: 1.067
batch start
#iterations: 61
currently lose_sum: 358.35857450962067
time_elpased: 1.069
batch start
#iterations: 62
currently lose_sum: 358.1327889561653
time_elpased: 1.078
batch start
#iterations: 63
currently lose_sum: 359.4633560180664
time_elpased: 1.058
batch start
#iterations: 64
currently lose_sum: 358.60156589746475
time_elpased: 1.074
batch start
#iterations: 65
currently lose_sum: 358.01848888397217
time_elpased: 1.06
batch start
#iterations: 66
currently lose_sum: 359.29764673113823
time_elpased: 1.063
batch start
#iterations: 67
currently lose_sum: 359.60707914829254
time_elpased: 1.069
batch start
#iterations: 68
currently lose_sum: 357.82932913303375
time_elpased: 1.084
batch start
#iterations: 69
currently lose_sum: 358.314040184021
time_elpased: 1.069
batch start
#iterations: 70
currently lose_sum: 357.9839615225792
time_elpased: 1.067
batch start
#iterations: 71
currently lose_sum: 358.08914336562157
time_elpased: 1.073
batch start
#iterations: 72
currently lose_sum: 357.2166251540184
time_elpased: 1.081
batch start
#iterations: 73
currently lose_sum: 358.4800048470497
time_elpased: 1.068
batch start
#iterations: 74
currently lose_sum: 358.97159773111343
time_elpased: 1.059
batch start
#iterations: 75
currently lose_sum: 357.1856897473335
time_elpased: 1.068
batch start
#iterations: 76
currently lose_sum: 358.4691346883774
time_elpased: 1.07
batch start
#iterations: 77
currently lose_sum: 357.3531914949417
time_elpased: 1.062
batch start
#iterations: 78
currently lose_sum: 357.3155245780945
time_elpased: 1.08
batch start
#iterations: 79
currently lose_sum: 356.3888000845909
time_elpased: 1.086
start validation test
0.805051546392
0.811711993331
0.797747055812
0.804668939159
0
validation finish
batch start
#iterations: 80
currently lose_sum: 356.45787382125854
time_elpased: 1.08
batch start
#iterations: 81
currently lose_sum: 358.75824093818665
time_elpased: 1.067
batch start
#iterations: 82
currently lose_sum: 357.9205851852894
time_elpased: 1.065
batch start
#iterations: 83
currently lose_sum: 357.4560634493828
time_elpased: 1.077
batch start
#iterations: 84
currently lose_sum: 358.2101898789406
time_elpased: 1.07
batch start
#iterations: 85
currently lose_sum: 357.90332156419754
time_elpased: 1.06
batch start
#iterations: 86
currently lose_sum: 358.08949786424637
time_elpased: 1.075
batch start
#iterations: 87
currently lose_sum: 358.5923453569412
time_elpased: 1.084
batch start
#iterations: 88
currently lose_sum: 355.7232612967491
time_elpased: 1.068
batch start
#iterations: 89
currently lose_sum: 357.05478370189667
time_elpased: 1.062
batch start
#iterations: 90
currently lose_sum: 356.5866850614548
time_elpased: 1.073
batch start
#iterations: 91
currently lose_sum: 356.69680231809616
time_elpased: 1.072
batch start
#iterations: 92
currently lose_sum: 356.8332082629204
time_elpased: 1.098
batch start
#iterations: 93
currently lose_sum: 359.2082100510597
time_elpased: 1.064
batch start
#iterations: 94
currently lose_sum: 358.44672507047653
time_elpased: 1.069
batch start
#iterations: 95
currently lose_sum: 355.12327241897583
time_elpased: 1.059
batch start
#iterations: 96
currently lose_sum: 355.90874153375626
time_elpased: 1.08
batch start
#iterations: 97
currently lose_sum: 356.97171652317047
time_elpased: 1.064
batch start
#iterations: 98
currently lose_sum: 355.646952688694
time_elpased: 1.065
batch start
#iterations: 99
currently lose_sum: 358.4839434027672
time_elpased: 1.062
start validation test
0.801546391753
0.827483113719
0.765284178187
0.795169184933
0
validation finish
batch start
#iterations: 100
currently lose_sum: 357.9420527815819
time_elpased: 1.075
batch start
#iterations: 101
currently lose_sum: 359.6402496099472
time_elpased: 1.074
batch start
#iterations: 102
currently lose_sum: 355.5049738883972
time_elpased: 1.072
batch start
#iterations: 103
currently lose_sum: 357.6564490199089
time_elpased: 1.075
batch start
#iterations: 104
currently lose_sum: 358.06946498155594
time_elpased: 1.078
batch start
#iterations: 105
currently lose_sum: 356.7088785171509
time_elpased: 1.072
batch start
#iterations: 106
currently lose_sum: 357.30915904045105
time_elpased: 1.071
batch start
#iterations: 107
currently lose_sum: 355.1204812824726
time_elpased: 1.062
batch start
#iterations: 108
currently lose_sum: 356.8838447332382
time_elpased: 1.078
batch start
#iterations: 109
currently lose_sum: 356.72641611099243
time_elpased: 1.074
batch start
#iterations: 110
currently lose_sum: 356.5913795232773
time_elpased: 1.065
batch start
#iterations: 111
currently lose_sum: 356.30226999521255
time_elpased: 1.073
batch start
#iterations: 112
currently lose_sum: 355.98179465532303
time_elpased: 1.061
batch start
#iterations: 113
currently lose_sum: 357.14700669050217
time_elpased: 1.062
batch start
#iterations: 114
currently lose_sum: 358.0746573805809
time_elpased: 1.068
batch start
#iterations: 115
currently lose_sum: 356.02615281939507
time_elpased: 1.069
batch start
#iterations: 116
currently lose_sum: 356.1449726819992
time_elpased: 1.069
batch start
#iterations: 117
currently lose_sum: 356.5052892565727
time_elpased: 1.07
batch start
#iterations: 118
currently lose_sum: 355.81075966358185
time_elpased: 1.066
batch start
#iterations: 119
currently lose_sum: 356.1501356959343
time_elpased: 1.064
start validation test
0.804484536082
0.828565140845
0.771121351767
0.798811860181
0
validation finish
batch start
#iterations: 120
currently lose_sum: 356.0632806420326
time_elpased: 1.087
batch start
#iterations: 121
currently lose_sum: 356.1495554447174
time_elpased: 1.074
batch start
#iterations: 122
currently lose_sum: 356.86521446704865
time_elpased: 1.063
batch start
#iterations: 123
currently lose_sum: 354.457102894783
time_elpased: 1.073
batch start
#iterations: 124
currently lose_sum: 355.09105587005615
time_elpased: 1.071
batch start
#iterations: 125
currently lose_sum: 356.3365626037121
time_elpased: 1.074
batch start
#iterations: 126
currently lose_sum: 356.6129866242409
time_elpased: 1.064
batch start
#iterations: 127
currently lose_sum: 356.96535789966583
time_elpased: 1.08
batch start
#iterations: 128
currently lose_sum: 358.34434205293655
time_elpased: 1.078
batch start
#iterations: 129
currently lose_sum: 355.0968808531761
time_elpased: 1.089
batch start
#iterations: 130
currently lose_sum: 355.98465982079506
time_elpased: 1.076
batch start
#iterations: 131
currently lose_sum: 357.0586552619934
time_elpased: 1.078
batch start
#iterations: 132
currently lose_sum: 356.4525839686394
time_elpased: 1.07
batch start
#iterations: 133
currently lose_sum: 358.09531182050705
time_elpased: 1.068
batch start
#iterations: 134
currently lose_sum: 355.9052039682865
time_elpased: 1.077
batch start
#iterations: 135
currently lose_sum: 357.4481760263443
time_elpased: 1.071
batch start
#iterations: 136
currently lose_sum: 356.6372489333153
time_elpased: 1.081
batch start
#iterations: 137
currently lose_sum: 355.95032292604446
time_elpased: 1.064
batch start
#iterations: 138
currently lose_sum: 357.73639130592346
time_elpased: 1.065
batch start
#iterations: 139
currently lose_sum: 354.7995461821556
time_elpased: 1.068
start validation test
0.80087628866
0.823574561404
0.76917562724
0.79544612126
0
validation finish
batch start
#iterations: 140
currently lose_sum: 355.44985288381577
time_elpased: 1.078
batch start
#iterations: 141
currently lose_sum: 356.176703363657
time_elpased: 1.07
batch start
#iterations: 142
currently lose_sum: 354.07278311252594
time_elpased: 1.092
batch start
#iterations: 143
currently lose_sum: 354.69759598374367
time_elpased: 1.085
batch start
#iterations: 144
currently lose_sum: 355.4386450648308
time_elpased: 1.076
batch start
#iterations: 145
currently lose_sum: 357.0737521648407
time_elpased: 1.071
batch start
#iterations: 146
currently lose_sum: 353.3701186180115
time_elpased: 1.073
batch start
#iterations: 147
currently lose_sum: 357.0693881511688
time_elpased: 1.06
batch start
#iterations: 148
currently lose_sum: 356.51291847229004
time_elpased: 1.077
batch start
#iterations: 149
currently lose_sum: 355.3416859805584
time_elpased: 1.073
batch start
#iterations: 150
currently lose_sum: 356.32768601179123
time_elpased: 1.075
batch start
#iterations: 151
currently lose_sum: 356.2521806359291
time_elpased: 1.072
batch start
#iterations: 152
currently lose_sum: 354.78821447491646
time_elpased: 1.069
batch start
#iterations: 153
currently lose_sum: 356.2216281890869
time_elpased: 1.072
batch start
#iterations: 154
currently lose_sum: 355.4867444038391
time_elpased: 1.078
batch start
#iterations: 155
currently lose_sum: 355.9637091755867
time_elpased: 1.118
batch start
#iterations: 156
currently lose_sum: 357.1201252937317
time_elpased: 1.071
batch start
#iterations: 157
currently lose_sum: 357.1422982811928
time_elpased: 1.065
batch start
#iterations: 158
currently lose_sum: 356.2595358490944
time_elpased: 1.075
batch start
#iterations: 159
currently lose_sum: 355.1549277603626
time_elpased: 1.064
start validation test
0.805670103093
0.809243784174
0.803277009729
0.806249357591
0
validation finish
batch start
#iterations: 160
currently lose_sum: 356.5612261891365
time_elpased: 1.08
batch start
#iterations: 161
currently lose_sum: 356.61568534374237
time_elpased: 1.082
batch start
#iterations: 162
currently lose_sum: 356.53232476115227
time_elpased: 1.072
batch start
#iterations: 163
currently lose_sum: 355.2787663936615
time_elpased: 1.075
batch start
#iterations: 164
currently lose_sum: 356.59745305776596
time_elpased: 1.078
batch start
#iterations: 165
currently lose_sum: 355.17806124687195
time_elpased: 1.074
batch start
#iterations: 166
currently lose_sum: 355.4146400690079
time_elpased: 1.062
batch start
#iterations: 167
currently lose_sum: 356.5845468044281
time_elpased: 1.081
batch start
#iterations: 168
currently lose_sum: 356.1653907597065
time_elpased: 1.08
batch start
#iterations: 169
currently lose_sum: 355.1348359286785
time_elpased: 1.08
batch start
#iterations: 170
currently lose_sum: 355.5291270017624
time_elpased: 1.078
batch start
#iterations: 171
currently lose_sum: 354.6742045879364
time_elpased: 1.079
batch start
#iterations: 172
currently lose_sum: 356.3032176196575
time_elpased: 1.073
batch start
#iterations: 173
currently lose_sum: 356.60480242967606
time_elpased: 1.057
batch start
#iterations: 174
currently lose_sum: 355.82519233226776
time_elpased: 1.079
batch start
#iterations: 175
currently lose_sum: 355.5713637173176
time_elpased: 1.07
batch start
#iterations: 176
currently lose_sum: 355.7630126476288
time_elpased: 1.065
batch start
#iterations: 177
currently lose_sum: 355.8843140602112
time_elpased: 1.068
batch start
#iterations: 178
currently lose_sum: 354.98067730665207
time_elpased: 1.086
batch start
#iterations: 179
currently lose_sum: 354.9549148082733
time_elpased: 1.083
start validation test
0.801597938144
0.829398663697
0.762724014337
0.794665244065
0
validation finish
batch start
#iterations: 180
currently lose_sum: 355.96827840805054
time_elpased: 1.074
batch start
#iterations: 181
currently lose_sum: 355.53235989809036
time_elpased: 1.068
batch start
#iterations: 182
currently lose_sum: 357.6357555985451
time_elpased: 1.083
batch start
#iterations: 183
currently lose_sum: 355.6903578042984
time_elpased: 1.076
batch start
#iterations: 184
currently lose_sum: 355.84807246923447
time_elpased: 1.081
batch start
#iterations: 185
currently lose_sum: 354.03844237327576
time_elpased: 1.076
batch start
#iterations: 186
currently lose_sum: 356.6418060362339
time_elpased: 1.074
batch start
#iterations: 187
currently lose_sum: 355.30986458063126
time_elpased: 1.081
batch start
#iterations: 188
currently lose_sum: 355.3801132440567
time_elpased: 1.068
batch start
#iterations: 189
currently lose_sum: 356.5906259417534
time_elpased: 1.083
batch start
#iterations: 190
currently lose_sum: 356.124206840992
time_elpased: 1.076
batch start
#iterations: 191
currently lose_sum: 356.90744054317474
time_elpased: 1.074
batch start
#iterations: 192
currently lose_sum: 356.30406364798546
time_elpased: 1.075
batch start
#iterations: 193
currently lose_sum: 355.403662443161
time_elpased: 1.083
batch start
#iterations: 194
currently lose_sum: 354.179585903883
time_elpased: 1.062
batch start
#iterations: 195
currently lose_sum: 354.32206785678864
time_elpased: 1.071
batch start
#iterations: 196
currently lose_sum: 356.02416384220123
time_elpased: 1.067
batch start
#iterations: 197
currently lose_sum: 357.61226493120193
time_elpased: 1.07
batch start
#iterations: 198
currently lose_sum: 356.1443599462509
time_elpased: 1.073
batch start
#iterations: 199
currently lose_sum: 356.28435429930687
time_elpased: 1.073
start validation test
0.804175257732
0.810276679842
0.797747055812
0.803963052789
0
validation finish
batch start
#iterations: 200
currently lose_sum: 354.45110723376274
time_elpased: 1.081
batch start
#iterations: 201
currently lose_sum: 356.2226867079735
time_elpased: 1.079
batch start
#iterations: 202
currently lose_sum: 356.5657908022404
time_elpased: 1.074
batch start
#iterations: 203
currently lose_sum: 353.80163899064064
time_elpased: 1.064
batch start
#iterations: 204
currently lose_sum: 355.5174415707588
time_elpased: 1.069
batch start
#iterations: 205
currently lose_sum: 355.32099437713623
time_elpased: 1.071
batch start
#iterations: 206
currently lose_sum: 357.0290532708168
time_elpased: 1.069
batch start
#iterations: 207
currently lose_sum: 354.6724007129669
time_elpased: 1.069
batch start
#iterations: 208
currently lose_sum: 356.4514721632004
time_elpased: 1.077
batch start
#iterations: 209
currently lose_sum: 352.48693466186523
time_elpased: 1.07
batch start
#iterations: 210
currently lose_sum: 355.49146646261215
time_elpased: 1.058
batch start
#iterations: 211
currently lose_sum: 354.088869869709
time_elpased: 1.07
batch start
#iterations: 212
currently lose_sum: 354.31804689764977
time_elpased: 1.071
batch start
#iterations: 213
currently lose_sum: 354.6127550005913
time_elpased: 1.067
batch start
#iterations: 214
currently lose_sum: 355.7081509232521
time_elpased: 1.065
batch start
#iterations: 215
currently lose_sum: 354.32391488552094
time_elpased: 1.083
batch start
#iterations: 216
currently lose_sum: 355.2128935456276
time_elpased: 1.074
batch start
#iterations: 217
currently lose_sum: 355.84702774882317
time_elpased: 1.07
batch start
#iterations: 218
currently lose_sum: 354.41406840085983
time_elpased: 1.063
batch start
#iterations: 219
currently lose_sum: 357.1596569418907
time_elpased: 1.073
start validation test
0.804845360825
0.815181866104
0.791807475678
0.803324675325
0
validation finish
batch start
#iterations: 220
currently lose_sum: 355.2095050215721
time_elpased: 1.085
batch start
#iterations: 221
currently lose_sum: 354.9094343185425
time_elpased: 1.07
batch start
#iterations: 222
currently lose_sum: 354.2104142308235
time_elpased: 1.078
batch start
#iterations: 223
currently lose_sum: 355.0995084643364
time_elpased: 1.072
batch start
#iterations: 224
currently lose_sum: 352.68074598908424
time_elpased: 1.065
batch start
#iterations: 225
currently lose_sum: 355.3017401099205
time_elpased: 1.072
batch start
#iterations: 226
currently lose_sum: 355.8574290275574
time_elpased: 1.077
batch start
#iterations: 227
currently lose_sum: 354.4645875990391
time_elpased: 1.072
batch start
#iterations: 228
currently lose_sum: 354.2745749950409
time_elpased: 1.067
batch start
#iterations: 229
currently lose_sum: 354.0207859277725
time_elpased: 1.069
batch start
#iterations: 230
currently lose_sum: 355.0669568181038
time_elpased: 1.073
batch start
#iterations: 231
currently lose_sum: 355.82122004032135
time_elpased: 1.06
batch start
#iterations: 232
currently lose_sum: 356.4346480369568
time_elpased: 1.072
batch start
#iterations: 233
currently lose_sum: 354.01306104660034
time_elpased: 1.076
batch start
#iterations: 234
currently lose_sum: 352.7391929924488
time_elpased: 1.071
batch start
#iterations: 235
currently lose_sum: 355.4842247068882
time_elpased: 1.082
batch start
#iterations: 236
currently lose_sum: 354.6585125029087
time_elpased: 1.074
batch start
#iterations: 237
currently lose_sum: 355.3236356973648
time_elpased: 1.073
batch start
#iterations: 238
currently lose_sum: 355.0206826329231
time_elpased: 1.066
batch start
#iterations: 239
currently lose_sum: 354.54418101906776
time_elpased: 1.08
start validation test
0.801494845361
0.816324347454
0.781464413722
0.798514100351
0
validation finish
batch start
#iterations: 240
currently lose_sum: 355.8990875482559
time_elpased: 1.073
batch start
#iterations: 241
currently lose_sum: 353.9605715870857
time_elpased: 1.071
batch start
#iterations: 242
currently lose_sum: 352.83213859796524
time_elpased: 1.069
batch start
#iterations: 243
currently lose_sum: 357.15518230199814
time_elpased: 1.066
batch start
#iterations: 244
currently lose_sum: 356.4011578261852
time_elpased: 1.078
batch start
#iterations: 245
currently lose_sum: 356.2466621994972
time_elpased: 1.071
batch start
#iterations: 246
currently lose_sum: 354.82274278998375
time_elpased: 1.065
batch start
#iterations: 247
currently lose_sum: 355.12852239608765
time_elpased: 1.074
batch start
#iterations: 248
currently lose_sum: 355.5449511408806
time_elpased: 1.076
batch start
#iterations: 249
currently lose_sum: 356.77255350351334
time_elpased: 1.065
batch start
#iterations: 250
currently lose_sum: 355.32482478022575
time_elpased: 1.072
batch start
#iterations: 251
currently lose_sum: 355.38578191399574
time_elpased: 1.072
batch start
#iterations: 252
currently lose_sum: 354.4309051632881
time_elpased: 1.07
batch start
#iterations: 253
currently lose_sum: 355.19131579995155
time_elpased: 1.062
batch start
#iterations: 254
currently lose_sum: 354.5904627740383
time_elpased: 1.068
batch start
#iterations: 255
currently lose_sum: 356.614265114069
time_elpased: 1.066
batch start
#iterations: 256
currently lose_sum: 355.78889912366867
time_elpased: 1.071
batch start
#iterations: 257
currently lose_sum: 354.885149538517
time_elpased: 1.073
batch start
#iterations: 258
currently lose_sum: 356.0044791698456
time_elpased: 1.082
batch start
#iterations: 259
currently lose_sum: 356.1074329614639
time_elpased: 1.066
start validation test
0.801701030928
0.822260945328
0.773169482847
0.796959940888
0
validation finish
batch start
#iterations: 260
currently lose_sum: 354.1519141495228
time_elpased: 1.085
batch start
#iterations: 261
currently lose_sum: 356.10209146142006
time_elpased: 1.07
batch start
#iterations: 262
currently lose_sum: 354.626635491848
time_elpased: 1.063
batch start
#iterations: 263
currently lose_sum: 354.7426943182945
time_elpased: 1.065
batch start
#iterations: 264
currently lose_sum: 354.6373254954815
time_elpased: 1.07
batch start
#iterations: 265
currently lose_sum: 354.89004868268967
time_elpased: 1.065
batch start
#iterations: 266
currently lose_sum: 353.8123848736286
time_elpased: 1.06
batch start
#iterations: 267
currently lose_sum: 354.81654688715935
time_elpased: 1.072
batch start
#iterations: 268
currently lose_sum: 353.3761645555496
time_elpased: 1.07
batch start
#iterations: 269
currently lose_sum: 354.43764141201973
time_elpased: 1.071
batch start
#iterations: 270
currently lose_sum: 353.57073575258255
time_elpased: 1.075
batch start
#iterations: 271
currently lose_sum: 355.95362025499344
time_elpased: 1.07
batch start
#iterations: 272
currently lose_sum: 354.0474024415016
time_elpased: 1.068
batch start
#iterations: 273
currently lose_sum: 353.72106099128723
time_elpased: 1.072
batch start
#iterations: 274
currently lose_sum: 354.37934494018555
time_elpased: 1.077
batch start
#iterations: 275
currently lose_sum: 355.40736401081085
time_elpased: 1.092
batch start
#iterations: 276
currently lose_sum: 353.3249891400337
time_elpased: 1.08
batch start
#iterations: 277
currently lose_sum: 355.1103020310402
time_elpased: 1.064
batch start
#iterations: 278
currently lose_sum: 354.7300534248352
time_elpased: 1.073
batch start
#iterations: 279
currently lose_sum: 354.6027747988701
time_elpased: 1.08
start validation test
0.805257731959
0.814278215223
0.794265232975
0.804147226542
0
validation finish
batch start
#iterations: 280
currently lose_sum: 353.72728061676025
time_elpased: 1.069
batch start
#iterations: 281
currently lose_sum: 354.0698384642601
time_elpased: 1.076
batch start
#iterations: 282
currently lose_sum: 354.4766172170639
time_elpased: 1.075
batch start
#iterations: 283
currently lose_sum: 352.97806468605995
time_elpased: 1.073
batch start
#iterations: 284
currently lose_sum: 354.6970596909523
time_elpased: 1.092
batch start
#iterations: 285
currently lose_sum: 354.4200001358986
time_elpased: 1.079
batch start
#iterations: 286
currently lose_sum: 355.3203499317169
time_elpased: 1.07
batch start
#iterations: 287
currently lose_sum: 356.54944294691086
time_elpased: 1.069
batch start
#iterations: 288
currently lose_sum: 355.09450590610504
time_elpased: 1.065
batch start
#iterations: 289
currently lose_sum: 352.92537701129913
time_elpased: 1.073
batch start
#iterations: 290
currently lose_sum: 353.30013063549995
time_elpased: 1.078
batch start
#iterations: 291
currently lose_sum: 354.7751958370209
time_elpased: 1.069
batch start
#iterations: 292
currently lose_sum: 355.0219039916992
time_elpased: 1.072
batch start
#iterations: 293
currently lose_sum: 354.44455832242966
time_elpased: 1.071
batch start
#iterations: 294
currently lose_sum: 354.02994120121
time_elpased: 1.081
batch start
#iterations: 295
currently lose_sum: 353.9803133904934
time_elpased: 1.075
batch start
#iterations: 296
currently lose_sum: 353.1837834715843
time_elpased: 1.085
batch start
#iterations: 297
currently lose_sum: 355.5470086336136
time_elpased: 1.073
batch start
#iterations: 298
currently lose_sum: 356.39057153463364
time_elpased: 1.073
batch start
#iterations: 299
currently lose_sum: 354.36784541606903
time_elpased: 1.073
start validation test
0.804278350515
0.816974718504
0.787608806964
0.80202304604
0
validation finish
batch start
#iterations: 300
currently lose_sum: 354.1628540754318
time_elpased: 1.092
batch start
#iterations: 301
currently lose_sum: 354.64318826794624
time_elpased: 1.064
batch start
#iterations: 302
currently lose_sum: 353.86085200309753
time_elpased: 1.063
batch start
#iterations: 303
currently lose_sum: 353.6873183250427
time_elpased: 1.067
batch start
#iterations: 304
currently lose_sum: 354.5846676826477
time_elpased: 1.07
batch start
#iterations: 305
currently lose_sum: 355.328700363636
time_elpased: 1.078
batch start
#iterations: 306
currently lose_sum: 355.3847226500511
time_elpased: 1.065
batch start
#iterations: 307
currently lose_sum: 355.4478831887245
time_elpased: 1.076
batch start
#iterations: 308
currently lose_sum: 354.17151975631714
time_elpased: 1.073
batch start
#iterations: 309
currently lose_sum: 354.0678877234459
time_elpased: 1.079
batch start
#iterations: 310
currently lose_sum: 355.1419215798378
time_elpased: 1.062
batch start
#iterations: 311
currently lose_sum: 356.5474157333374
time_elpased: 1.072
batch start
#iterations: 312
currently lose_sum: 354.3711647391319
time_elpased: 1.082
batch start
#iterations: 313
currently lose_sum: 354.2634350657463
time_elpased: 1.08
batch start
#iterations: 314
currently lose_sum: 353.83984714746475
time_elpased: 1.064
batch start
#iterations: 315
currently lose_sum: 354.5807678103447
time_elpased: 1.075
batch start
#iterations: 316
currently lose_sum: 354.1510662138462
time_elpased: 1.077
batch start
#iterations: 317
currently lose_sum: 354.5790293812752
time_elpased: 1.065
batch start
#iterations: 318
currently lose_sum: 355.60622105002403
time_elpased: 1.072
batch start
#iterations: 319
currently lose_sum: 353.58647441864014
time_elpased: 1.068
start validation test
0.805
0.815373260228
0.791909882232
0.803470310146
0
validation finish
batch start
#iterations: 320
currently lose_sum: 354.4367123246193
time_elpased: 1.078
batch start
#iterations: 321
currently lose_sum: 354.9060914218426
time_elpased: 1.071
batch start
#iterations: 322
currently lose_sum: 354.3501461148262
time_elpased: 1.08
batch start
#iterations: 323
currently lose_sum: 355.149586468935
time_elpased: 1.101
batch start
#iterations: 324
currently lose_sum: 352.70608854293823
time_elpased: 1.085
batch start
#iterations: 325
currently lose_sum: 353.31531649827957
time_elpased: 1.083
batch start
#iterations: 326
currently lose_sum: 354.08011853694916
time_elpased: 1.079
batch start
#iterations: 327
currently lose_sum: 354.45989829301834
time_elpased: 1.081
batch start
#iterations: 328
currently lose_sum: 356.09604918956757
time_elpased: 1.071
batch start
#iterations: 329
currently lose_sum: 356.2845353484154
time_elpased: 1.091
batch start
#iterations: 330
currently lose_sum: 353.3915002346039
time_elpased: 1.093
batch start
#iterations: 331
currently lose_sum: 356.46478524804115
time_elpased: 1.065
batch start
#iterations: 332
currently lose_sum: 354.18002861738205
time_elpased: 1.066
batch start
#iterations: 333
currently lose_sum: 355.62386283278465
time_elpased: 1.086
batch start
#iterations: 334
currently lose_sum: 353.7309030890465
time_elpased: 1.066
batch start
#iterations: 335
currently lose_sum: 353.55769351124763
time_elpased: 1.076
batch start
#iterations: 336
currently lose_sum: 353.3335492312908
time_elpased: 1.073
batch start
#iterations: 337
currently lose_sum: 355.20063561201096
time_elpased: 1.067
batch start
#iterations: 338
currently lose_sum: 353.66339260339737
time_elpased: 1.071
batch start
#iterations: 339
currently lose_sum: 354.2622163593769
time_elpased: 1.076
start validation test
0.805670103093
0.814170422388
0.795494111623
0.804723920025
0
validation finish
batch start
#iterations: 340
currently lose_sum: 353.7860514521599
time_elpased: 1.094
batch start
#iterations: 341
currently lose_sum: 353.6307981610298
time_elpased: 1.073
batch start
#iterations: 342
currently lose_sum: 355.06681874394417
time_elpased: 1.078
batch start
#iterations: 343
currently lose_sum: 354.1038980484009
time_elpased: 1.061
batch start
#iterations: 344
currently lose_sum: 354.2847384214401
time_elpased: 1.067
batch start
#iterations: 345
currently lose_sum: 354.9506753087044
time_elpased: 1.074
batch start
#iterations: 346
currently lose_sum: 354.59917825460434
time_elpased: 1.071
batch start
#iterations: 347
currently lose_sum: 354.65719521045685
time_elpased: 1.075
batch start
#iterations: 348
currently lose_sum: 354.55832517147064
time_elpased: 1.074
batch start
#iterations: 349
currently lose_sum: 355.3465111851692
time_elpased: 1.064
batch start
#iterations: 350
currently lose_sum: 354.4038249850273
time_elpased: 1.071
batch start
#iterations: 351
currently lose_sum: 354.4861092567444
time_elpased: 1.074
batch start
#iterations: 352
currently lose_sum: 352.99018013477325
time_elpased: 1.081
batch start
#iterations: 353
currently lose_sum: 354.7019282579422
time_elpased: 1.069
batch start
#iterations: 354
currently lose_sum: 353.75202345848083
time_elpased: 1.057
batch start
#iterations: 355
currently lose_sum: 352.46862012147903
time_elpased: 1.083
batch start
#iterations: 356
currently lose_sum: 354.6643496155739
time_elpased: 1.069
batch start
#iterations: 357
currently lose_sum: 353.8210714459419
time_elpased: 1.072
batch start
#iterations: 358
currently lose_sum: 353.86803182959557
time_elpased: 1.071
batch start
#iterations: 359
currently lose_sum: 354.0843918323517
time_elpased: 1.064
start validation test
0.804072164948
0.814093111439
0.791500256016
0.802637727816
0
validation finish
batch start
#iterations: 360
currently lose_sum: 355.75982797145844
time_elpased: 1.081
batch start
#iterations: 361
currently lose_sum: 354.01357209682465
time_elpased: 1.069
batch start
#iterations: 362
currently lose_sum: 353.9020290374756
time_elpased: 1.075
batch start
#iterations: 363
currently lose_sum: 354.5847414135933
time_elpased: 1.089
batch start
#iterations: 364
currently lose_sum: 355.9060695171356
time_elpased: 1.077
batch start
#iterations: 365
currently lose_sum: 354.1708284318447
time_elpased: 1.08
batch start
#iterations: 366
currently lose_sum: 353.8049551844597
time_elpased: 1.081
batch start
#iterations: 367
currently lose_sum: 355.15119782090187
time_elpased: 1.072
batch start
#iterations: 368
currently lose_sum: 354.5000231564045
time_elpased: 1.071
batch start
#iterations: 369
currently lose_sum: 356.09853488206863
time_elpased: 1.067
batch start
#iterations: 370
currently lose_sum: 355.1393647789955
time_elpased: 1.068
batch start
#iterations: 371
currently lose_sum: 354.90932339429855
time_elpased: 1.072
batch start
#iterations: 372
currently lose_sum: 354.61374229192734
time_elpased: 1.078
batch start
#iterations: 373
currently lose_sum: 353.7164953649044
time_elpased: 1.076
batch start
#iterations: 374
currently lose_sum: 355.1557250022888
time_elpased: 1.078
batch start
#iterations: 375
currently lose_sum: 356.29900896549225
time_elpased: 1.067
batch start
#iterations: 376
currently lose_sum: 354.36212891340256
time_elpased: 1.068
batch start
#iterations: 377
currently lose_sum: 353.21332371234894
time_elpased: 1.068
batch start
#iterations: 378
currently lose_sum: 355.71294617652893
time_elpased: 1.072
batch start
#iterations: 379
currently lose_sum: 354.05694884061813
time_elpased: 1.071
start validation test
0.804020618557
0.820212651702
0.782078853047
0.800691968966
0
validation finish
batch start
#iterations: 380
currently lose_sum: 355.3817866742611
time_elpased: 1.096
batch start
#iterations: 381
currently lose_sum: 354.2638426423073
time_elpased: 1.063
batch start
#iterations: 382
currently lose_sum: 354.104045599699
time_elpased: 1.083
batch start
#iterations: 383
currently lose_sum: 352.4155153930187
time_elpased: 1.08
batch start
#iterations: 384
currently lose_sum: 354.22604781389236
time_elpased: 1.07
batch start
#iterations: 385
currently lose_sum: 352.7447436451912
time_elpased: 1.065
batch start
#iterations: 386
currently lose_sum: 354.94982039928436
time_elpased: 1.077
batch start
#iterations: 387
currently lose_sum: 353.9485781788826
time_elpased: 1.068
batch start
#iterations: 388
currently lose_sum: 354.1396315097809
time_elpased: 1.062
batch start
#iterations: 389
currently lose_sum: 354.00724214315414
time_elpased: 1.068
batch start
#iterations: 390
currently lose_sum: 353.2519090771675
time_elpased: 1.073
batch start
#iterations: 391
currently lose_sum: 352.3150263428688
time_elpased: 1.07
batch start
#iterations: 392
currently lose_sum: 354.90883296728134
time_elpased: 1.076
batch start
#iterations: 393
currently lose_sum: 355.6530061364174
time_elpased: 1.081
batch start
#iterations: 394
currently lose_sum: 353.11095306277275
time_elpased: 1.065
batch start
#iterations: 395
currently lose_sum: 354.3958334326744
time_elpased: 1.085
batch start
#iterations: 396
currently lose_sum: 354.05444717407227
time_elpased: 1.068
batch start
#iterations: 397
currently lose_sum: 354.2884451150894
time_elpased: 1.077
batch start
#iterations: 398
currently lose_sum: 354.83002346754074
time_elpased: 1.069
batch start
#iterations: 399
currently lose_sum: 355.7223421931267
time_elpased: 1.072
start validation test
0.805515463918
0.812604340568
0.797542242704
0.805002842524
0
validation finish
acc: 0.808
pre: 0.809
rec: 0.806
F1: 0.807
auc: 0.000
