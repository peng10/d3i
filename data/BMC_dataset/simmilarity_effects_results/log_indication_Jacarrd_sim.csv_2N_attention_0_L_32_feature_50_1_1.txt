start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 548.8418660461903
time_elpased: 1.736
batch start
#iterations: 1
currently lose_sum: 536.0380386710167
time_elpased: 1.72
batch start
#iterations: 2
currently lose_sum: 532.9644967913628
time_elpased: 1.717
batch start
#iterations: 3
currently lose_sum: 528.7001940310001
time_elpased: 1.727
batch start
#iterations: 4
currently lose_sum: 528.2841877043247
time_elpased: 1.715
batch start
#iterations: 5
currently lose_sum: 526.4185214340687
time_elpased: 1.736
batch start
#iterations: 6
currently lose_sum: 523.9044375121593
time_elpased: 1.723
batch start
#iterations: 7
currently lose_sum: 524.6922896802425
time_elpased: 1.751
batch start
#iterations: 8
currently lose_sum: 522.3268720805645
time_elpased: 1.728
batch start
#iterations: 9
currently lose_sum: 522.5246651172638
time_elpased: 1.761
batch start
#iterations: 10
currently lose_sum: 523.3364515006542
time_elpased: 1.721
batch start
#iterations: 11
currently lose_sum: 524.8865323662758
time_elpased: 1.718
batch start
#iterations: 12
currently lose_sum: 520.6711623072624
time_elpased: 1.718
batch start
#iterations: 13
currently lose_sum: 521.0209712982178
time_elpased: 1.726
batch start
#iterations: 14
currently lose_sum: 519.0817385315895
time_elpased: 1.715
batch start
#iterations: 15
currently lose_sum: 520.5297789871693
time_elpased: 1.761
batch start
#iterations: 16
currently lose_sum: 521.4630844295025
time_elpased: 1.801
batch start
#iterations: 17
currently lose_sum: 518.764733672142
time_elpased: 1.78
batch start
#iterations: 18
currently lose_sum: 519.6833595633507
time_elpased: 1.754
batch start
#iterations: 19
currently lose_sum: 517.9067752957344
time_elpased: 1.722
start validation test
0.580824742268
0.990588235294
0.171836734694
0.292869565217
0
validation finish
batch start
#iterations: 20
currently lose_sum: 518.5965601801872
time_elpased: 1.78
batch start
#iterations: 21
currently lose_sum: 520.9301216304302
time_elpased: 1.821
batch start
#iterations: 22
currently lose_sum: 520.0796940624714
time_elpased: 1.741
batch start
#iterations: 23
currently lose_sum: 518.4039404392242
time_elpased: 1.768
batch start
#iterations: 24
currently lose_sum: 517.789915561676
time_elpased: 1.75
batch start
#iterations: 25
currently lose_sum: 518.3837027847767
time_elpased: 1.755
batch start
#iterations: 26
currently lose_sum: 519.1606898605824
time_elpased: 1.775
batch start
#iterations: 27
currently lose_sum: 518.0523771345615
time_elpased: 1.761
batch start
#iterations: 28
currently lose_sum: 517.9162958562374
time_elpased: 1.728
batch start
#iterations: 29
currently lose_sum: 518.1685625910759
time_elpased: 1.714
batch start
#iterations: 30
currently lose_sum: 516.546951174736
time_elpased: 1.808
batch start
#iterations: 31
currently lose_sum: 517.9535681307316
time_elpased: 1.761
batch start
#iterations: 32
currently lose_sum: 518.8211422860622
time_elpased: 1.752
batch start
#iterations: 33
currently lose_sum: 515.6493681073189
time_elpased: 1.75
batch start
#iterations: 34
currently lose_sum: 517.6242729127407
time_elpased: 1.753
batch start
#iterations: 35
currently lose_sum: 517.1639486849308
time_elpased: 1.764
batch start
#iterations: 36
currently lose_sum: 517.2465986907482
time_elpased: 1.726
batch start
#iterations: 37
currently lose_sum: 517.5706760585308
time_elpased: 1.761
batch start
#iterations: 38
currently lose_sum: 517.723701864481
time_elpased: 1.751
batch start
#iterations: 39
currently lose_sum: 519.076318025589
time_elpased: 1.738
start validation test
0.579896907216
0.994604316547
0.169285714286
0.289326822463
0
validation finish
batch start
#iterations: 40
currently lose_sum: 518.1228109002113
time_elpased: 1.735
batch start
#iterations: 41
currently lose_sum: 516.8160196244717
time_elpased: 1.731
batch start
#iterations: 42
currently lose_sum: 516.2538430988789
time_elpased: 1.718
batch start
#iterations: 43
currently lose_sum: 516.9848273396492
time_elpased: 1.717
batch start
#iterations: 44
currently lose_sum: 516.4883718490601
time_elpased: 1.7
batch start
#iterations: 45
currently lose_sum: 518.3109914362431
time_elpased: 1.691
batch start
#iterations: 46
currently lose_sum: 516.6735434532166
time_elpased: 1.695
batch start
#iterations: 47
currently lose_sum: 518.6574591696262
time_elpased: 1.695
batch start
#iterations: 48
currently lose_sum: 515.6625468432903
time_elpased: 1.693
batch start
#iterations: 49
currently lose_sum: 516.7607165873051
time_elpased: 1.682
batch start
#iterations: 50
currently lose_sum: 518.462573736906
time_elpased: 1.685
batch start
#iterations: 51
currently lose_sum: 517.2810313403606
time_elpased: 1.695
batch start
#iterations: 52
currently lose_sum: 517.3543645739555
time_elpased: 1.688
batch start
#iterations: 53
currently lose_sum: 517.1682102382183
time_elpased: 1.686
batch start
#iterations: 54
currently lose_sum: 516.597020059824
time_elpased: 1.684
batch start
#iterations: 55
currently lose_sum: 517.0591343939304
time_elpased: 1.719
batch start
#iterations: 56
currently lose_sum: 517.6463368535042
time_elpased: 1.689
batch start
#iterations: 57
currently lose_sum: 515.7036476135254
time_elpased: 1.707
batch start
#iterations: 58
currently lose_sum: 515.0692578554153
time_elpased: 1.727
batch start
#iterations: 59
currently lose_sum: 515.782594025135
time_elpased: 1.677
start validation test
0.59381443299
0.995867768595
0.196734693878
0.328561690525
0
validation finish
batch start
#iterations: 60
currently lose_sum: 516.2795251309872
time_elpased: 1.734
batch start
#iterations: 61
currently lose_sum: 516.8421393930912
time_elpased: 1.701
batch start
#iterations: 62
currently lose_sum: 518.5317950844765
time_elpased: 1.725
batch start
#iterations: 63
currently lose_sum: 516.3997145593166
time_elpased: 1.682
batch start
#iterations: 64
currently lose_sum: 515.565143674612
time_elpased: 1.693
batch start
#iterations: 65
currently lose_sum: 516.9831394553185
time_elpased: 1.708
batch start
#iterations: 66
currently lose_sum: 516.45353525877
time_elpased: 1.696
batch start
#iterations: 67
currently lose_sum: 515.1971142292023
time_elpased: 1.713
batch start
#iterations: 68
currently lose_sum: 518.3491790294647
time_elpased: 1.696
batch start
#iterations: 69
currently lose_sum: 517.1469275355339
time_elpased: 1.7
batch start
#iterations: 70
currently lose_sum: 515.8847911059856
time_elpased: 1.694
batch start
#iterations: 71
currently lose_sum: 516.0822435617447
time_elpased: 1.697
batch start
#iterations: 72
currently lose_sum: 517.2220205068588
time_elpased: 1.688
batch start
#iterations: 73
currently lose_sum: 516.2129633128643
time_elpased: 1.692
batch start
#iterations: 74
currently lose_sum: 516.5912788808346
time_elpased: 1.698
batch start
#iterations: 75
currently lose_sum: 516.9287885427475
time_elpased: 1.702
batch start
#iterations: 76
currently lose_sum: 516.8887477815151
time_elpased: 1.69
batch start
#iterations: 77
currently lose_sum: 516.5165131986141
time_elpased: 1.701
batch start
#iterations: 78
currently lose_sum: 515.1518624424934
time_elpased: 1.713
batch start
#iterations: 79
currently lose_sum: 517.7401821017265
time_elpased: 1.712
start validation test
0.613195876289
0.994827586207
0.235510204082
0.380858085809
0
validation finish
batch start
#iterations: 80
currently lose_sum: 516.0681333839893
time_elpased: 1.693
batch start
#iterations: 81
currently lose_sum: 516.6479851603508
time_elpased: 1.678
batch start
#iterations: 82
currently lose_sum: 515.2398158609867
time_elpased: 1.688
batch start
#iterations: 83
currently lose_sum: 516.118573397398
time_elpased: 1.684
batch start
#iterations: 84
currently lose_sum: 515.8540263772011
time_elpased: 1.718
batch start
#iterations: 85
currently lose_sum: 515.774167984724
time_elpased: 1.707
batch start
#iterations: 86
currently lose_sum: 515.4835467338562
time_elpased: 1.687
batch start
#iterations: 87
currently lose_sum: 514.2023844718933
time_elpased: 1.691
batch start
#iterations: 88
currently lose_sum: 515.3415589630604
time_elpased: 1.731
batch start
#iterations: 89
currently lose_sum: 516.8911681771278
time_elpased: 1.688
batch start
#iterations: 90
currently lose_sum: 515.0129015743732
time_elpased: 1.713
batch start
#iterations: 91
currently lose_sum: 514.1638344526291
time_elpased: 1.714
batch start
#iterations: 92
currently lose_sum: 516.172857940197
time_elpased: 1.714
batch start
#iterations: 93
currently lose_sum: 516.0480388700962
time_elpased: 1.67
batch start
#iterations: 94
currently lose_sum: 514.645956069231
time_elpased: 1.704
batch start
#iterations: 95
currently lose_sum: 516.1294536888599
time_elpased: 1.674
batch start
#iterations: 96
currently lose_sum: 517.1772920489311
time_elpased: 1.73
batch start
#iterations: 97
currently lose_sum: 515.0904413461685
time_elpased: 1.705
batch start
#iterations: 98
currently lose_sum: 516.0294120013714
time_elpased: 1.711
batch start
#iterations: 99
currently lose_sum: 516.1546034514904
time_elpased: 1.691
start validation test
0.579742268041
0.996383363472
0.168673469388
0.288506850511
0
validation finish
batch start
#iterations: 100
currently lose_sum: 516.5237615704536
time_elpased: 1.716
batch start
#iterations: 101
currently lose_sum: 515.8475511372089
time_elpased: 1.692
batch start
#iterations: 102
currently lose_sum: 513.6756246685982
time_elpased: 1.666
batch start
#iterations: 103
currently lose_sum: 514.803130954504
time_elpased: 1.7
batch start
#iterations: 104
currently lose_sum: 516.5518268346786
time_elpased: 1.706
batch start
#iterations: 105
currently lose_sum: 515.5675567984581
time_elpased: 1.695
batch start
#iterations: 106
currently lose_sum: 517.0088658630848
time_elpased: 1.701
batch start
#iterations: 107
currently lose_sum: 517.810346275568
time_elpased: 1.694
batch start
#iterations: 108
currently lose_sum: 515.2091386020184
time_elpased: 1.706
batch start
#iterations: 109
currently lose_sum: 516.1985519826412
time_elpased: 1.695
batch start
#iterations: 110
currently lose_sum: 515.5763986110687
time_elpased: 1.707
batch start
#iterations: 111
currently lose_sum: 518.3759916722775
time_elpased: 1.696
batch start
#iterations: 112
currently lose_sum: 515.7541044354439
time_elpased: 1.719
batch start
#iterations: 113
currently lose_sum: 515.2048932015896
time_elpased: 1.674
batch start
#iterations: 114
currently lose_sum: 515.4093053340912
time_elpased: 1.694
batch start
#iterations: 115
currently lose_sum: 515.7488099336624
time_elpased: 1.811
batch start
#iterations: 116
currently lose_sum: 514.9737693965435
time_elpased: 1.819
batch start
#iterations: 117
currently lose_sum: 514.8263337314129
time_elpased: 1.807
batch start
#iterations: 118
currently lose_sum: 516.4110347032547
time_elpased: 1.81
batch start
#iterations: 119
currently lose_sum: 516.222689896822
time_elpased: 1.799
start validation test
0.599278350515
0.998033431662
0.207142857143
0.34307926314
0
validation finish
batch start
#iterations: 120
currently lose_sum: 515.6912952959538
time_elpased: 1.727
batch start
#iterations: 121
currently lose_sum: 515.5106721818447
time_elpased: 1.698
batch start
#iterations: 122
currently lose_sum: 514.7105076014996
time_elpased: 1.69
batch start
#iterations: 123
currently lose_sum: 515.2109304070473
time_elpased: 1.712
batch start
#iterations: 124
currently lose_sum: 516.2293530702591
time_elpased: 1.695
batch start
#iterations: 125
currently lose_sum: 517.3643665611744
time_elpased: 1.699
batch start
#iterations: 126
currently lose_sum: 515.0463192164898
time_elpased: 1.718
batch start
#iterations: 127
currently lose_sum: 518.0794195830822
time_elpased: 1.724
batch start
#iterations: 128
currently lose_sum: 516.3789559602737
time_elpased: 1.698
batch start
#iterations: 129
currently lose_sum: 516.4607172906399
time_elpased: 1.705
batch start
#iterations: 130
currently lose_sum: 515.525191962719
time_elpased: 1.691
batch start
#iterations: 131
currently lose_sum: 515.6584369838238
time_elpased: 1.687
batch start
#iterations: 132
currently lose_sum: 515.1122748553753
time_elpased: 1.717
batch start
#iterations: 133
currently lose_sum: 518.225516885519
time_elpased: 1.685
batch start
#iterations: 134
currently lose_sum: 516.1147367656231
time_elpased: 1.705
batch start
#iterations: 135
currently lose_sum: 513.8390908837318
time_elpased: 1.71
batch start
#iterations: 136
currently lose_sum: 513.8980199098587
time_elpased: 1.706
batch start
#iterations: 137
currently lose_sum: 514.7584499418736
time_elpased: 1.698
batch start
#iterations: 138
currently lose_sum: 516.422186434269
time_elpased: 1.719
batch start
#iterations: 139
currently lose_sum: 516.1332578957081
time_elpased: 1.704
start validation test
0.589072164948
0.998908296943
0.186734693878
0.314649243466
0
validation finish
batch start
#iterations: 140
currently lose_sum: 512.9981657862663
time_elpased: 1.734
batch start
#iterations: 141
currently lose_sum: 515.0638569891453
time_elpased: 1.704
batch start
#iterations: 142
currently lose_sum: 514.8711405992508
time_elpased: 1.712
batch start
#iterations: 143
currently lose_sum: 516.2430154383183
time_elpased: 1.699
batch start
#iterations: 144
currently lose_sum: 516.1358847618103
time_elpased: 1.697
batch start
#iterations: 145
currently lose_sum: 514.5404163300991
time_elpased: 1.707
batch start
#iterations: 146
currently lose_sum: 517.5166071355343
time_elpased: 1.725
batch start
#iterations: 147
currently lose_sum: 517.3845657706261
time_elpased: 1.666
batch start
#iterations: 148
currently lose_sum: 514.3732922971249
time_elpased: 1.703
batch start
#iterations: 149
currently lose_sum: 517.5777528882027
time_elpased: 1.692
batch start
#iterations: 150
currently lose_sum: 514.3215864896774
time_elpased: 1.693
batch start
#iterations: 151
currently lose_sum: 513.7315545976162
time_elpased: 1.693
batch start
#iterations: 152
currently lose_sum: 514.6900590360165
time_elpased: 1.694
batch start
#iterations: 153
currently lose_sum: 514.1742312014103
time_elpased: 1.684
batch start
#iterations: 154
currently lose_sum: 518.670467287302
time_elpased: 1.694
batch start
#iterations: 155
currently lose_sum: 514.214345484972
time_elpased: 1.695
batch start
#iterations: 156
currently lose_sum: 516.640556961298
time_elpased: 1.724
batch start
#iterations: 157
currently lose_sum: 515.7142177820206
time_elpased: 1.704
batch start
#iterations: 158
currently lose_sum: 513.7578801214695
time_elpased: 1.716
batch start
#iterations: 159
currently lose_sum: 516.7661593556404
time_elpased: 1.729
start validation test
0.587835051546
0.998342541436
0.184387755102
0.3112833764
0
validation finish
batch start
#iterations: 160
currently lose_sum: 516.8411994576454
time_elpased: 1.701
batch start
#iterations: 161
currently lose_sum: 516.4983158409595
time_elpased: 1.707
batch start
#iterations: 162
currently lose_sum: 514.1771664917469
time_elpased: 1.691
batch start
#iterations: 163
currently lose_sum: 513.1595163941383
time_elpased: 1.71
batch start
#iterations: 164
currently lose_sum: 517.1229159235954
time_elpased: 1.697
batch start
#iterations: 165
currently lose_sum: 516.9915193021297
time_elpased: 1.708
batch start
#iterations: 166
currently lose_sum: 517.5529699623585
time_elpased: 1.709
batch start
#iterations: 167
currently lose_sum: 515.8184667229652
time_elpased: 1.672
batch start
#iterations: 168
currently lose_sum: 515.9922990500927
time_elpased: 1.69
batch start
#iterations: 169
currently lose_sum: 517.0869239270687
time_elpased: 1.677
batch start
#iterations: 170
currently lose_sum: 515.4222850501537
time_elpased: 1.718
batch start
#iterations: 171
currently lose_sum: 514.2835727632046
time_elpased: 1.716
batch start
#iterations: 172
currently lose_sum: 518.0648145377636
time_elpased: 1.693
batch start
#iterations: 173
currently lose_sum: 516.6384547948837
time_elpased: 1.696
batch start
#iterations: 174
currently lose_sum: 516.4650004506111
time_elpased: 1.72
batch start
#iterations: 175
currently lose_sum: 515.1489609777927
time_elpased: 1.69
batch start
#iterations: 176
currently lose_sum: 516.0527785122395
time_elpased: 1.704
batch start
#iterations: 177
currently lose_sum: 514.3426150381565
time_elpased: 1.73
batch start
#iterations: 178
currently lose_sum: 514.9999122917652
time_elpased: 1.71
batch start
#iterations: 179
currently lose_sum: 516.6965016424656
time_elpased: 1.692
start validation test
0.599381443299
0.998525073746
0.207244897959
0.343248267703
0
validation finish
batch start
#iterations: 180
currently lose_sum: 516.3563669323921
time_elpased: 1.724
batch start
#iterations: 181
currently lose_sum: 516.6721306741238
time_elpased: 1.75
batch start
#iterations: 182
currently lose_sum: 515.232028901577
time_elpased: 1.726
batch start
#iterations: 183
currently lose_sum: 516.7770746648312
time_elpased: 1.713
batch start
#iterations: 184
currently lose_sum: 515.0210008621216
time_elpased: 1.718
batch start
#iterations: 185
currently lose_sum: 514.9558709561825
time_elpased: 1.725
batch start
#iterations: 186
currently lose_sum: 517.2873918712139
time_elpased: 1.719
batch start
#iterations: 187
currently lose_sum: 515.0758941173553
time_elpased: 1.693
batch start
#iterations: 188
currently lose_sum: 515.3475770354271
time_elpased: 1.713
batch start
#iterations: 189
currently lose_sum: 516.6851417124271
time_elpased: 1.699
batch start
#iterations: 190
currently lose_sum: 514.6503429710865
time_elpased: 1.743
batch start
#iterations: 191
currently lose_sum: 516.9233979582787
time_elpased: 1.687
batch start
#iterations: 192
currently lose_sum: 515.0570766627789
time_elpased: 1.693
batch start
#iterations: 193
currently lose_sum: 515.0165477693081
time_elpased: 1.723
batch start
#iterations: 194
currently lose_sum: 516.9671737551689
time_elpased: 1.692
batch start
#iterations: 195
currently lose_sum: 515.2526898980141
time_elpased: 1.68
batch start
#iterations: 196
currently lose_sum: 515.0051630735397
time_elpased: 1.695
batch start
#iterations: 197
currently lose_sum: 516.3489942252636
time_elpased: 1.702
batch start
#iterations: 198
currently lose_sum: 515.4913254678249
time_elpased: 1.701
batch start
#iterations: 199
currently lose_sum: 513.9707719087601
time_elpased: 1.692
start validation test
0.590412371134
0.998923573735
0.189387755102
0.318407960199
0
validation finish
batch start
#iterations: 200
currently lose_sum: 515.7848369479179
time_elpased: 1.753
batch start
#iterations: 201
currently lose_sum: 515.1105131208897
time_elpased: 1.679
batch start
#iterations: 202
currently lose_sum: 515.5902599394321
time_elpased: 1.696
batch start
#iterations: 203
currently lose_sum: 514.7764208614826
time_elpased: 1.694
batch start
#iterations: 204
currently lose_sum: 515.4549175798893
time_elpased: 1.715
batch start
#iterations: 205
currently lose_sum: 514.7264761328697
time_elpased: 1.696
batch start
#iterations: 206
currently lose_sum: 518.2491029500961
time_elpased: 1.713
batch start
#iterations: 207
currently lose_sum: 516.8729038238525
time_elpased: 1.691
batch start
#iterations: 208
currently lose_sum: 514.6639274060726
time_elpased: 1.699
batch start
#iterations: 209
currently lose_sum: 514.7222377061844
time_elpased: 1.734
batch start
#iterations: 210
currently lose_sum: 515.5965559482574
time_elpased: 1.716
batch start
#iterations: 211
currently lose_sum: 516.179425239563
time_elpased: 1.704
batch start
#iterations: 212
currently lose_sum: 515.8579018712044
time_elpased: 1.717
batch start
#iterations: 213
currently lose_sum: 514.3431106805801
time_elpased: 1.715
batch start
#iterations: 214
currently lose_sum: 515.6494706869125
time_elpased: 1.721
batch start
#iterations: 215
currently lose_sum: 516.0227860808372
time_elpased: 1.727
batch start
#iterations: 216
currently lose_sum: 515.7291496098042
time_elpased: 1.684
batch start
#iterations: 217
currently lose_sum: 516.1584695577621
time_elpased: 1.709
batch start
#iterations: 218
currently lose_sum: 515.5805840194225
time_elpased: 1.734
batch start
#iterations: 219
currently lose_sum: 516.7424330115318
time_elpased: 1.705
start validation test
0.600721649485
0.99806013579
0.21
0.346990389479
0
validation finish
batch start
#iterations: 220
currently lose_sum: 514.7778926491737
time_elpased: 1.723
batch start
#iterations: 221
currently lose_sum: 517.7007985711098
time_elpased: 1.715
batch start
#iterations: 222
currently lose_sum: 514.5686056613922
time_elpased: 1.731
batch start
#iterations: 223
currently lose_sum: 514.7534156441689
time_elpased: 1.688
batch start
#iterations: 224
currently lose_sum: 517.4013833105564
time_elpased: 1.712
batch start
#iterations: 225
currently lose_sum: 517.0105951726437
time_elpased: 1.702
batch start
#iterations: 226
currently lose_sum: 516.0849174559116
time_elpased: 1.672
batch start
#iterations: 227
currently lose_sum: 515.9883081018925
time_elpased: 1.693
batch start
#iterations: 228
currently lose_sum: 513.1773944199085
time_elpased: 1.683
batch start
#iterations: 229
currently lose_sum: 517.1866402924061
time_elpased: 1.685
batch start
#iterations: 230
currently lose_sum: 516.7455445230007
time_elpased: 1.725
batch start
#iterations: 231
currently lose_sum: 516.992348998785
time_elpased: 1.691
batch start
#iterations: 232
currently lose_sum: 512.7737326025963
time_elpased: 1.734
batch start
#iterations: 233
currently lose_sum: 513.3664736151695
time_elpased: 1.69
batch start
#iterations: 234
currently lose_sum: 515.1098369956017
time_elpased: 1.717
batch start
#iterations: 235
currently lose_sum: 515.8140246868134
time_elpased: 1.703
batch start
#iterations: 236
currently lose_sum: 515.6692177653313
time_elpased: 1.719
batch start
#iterations: 237
currently lose_sum: 516.5326451957226
time_elpased: 1.716
batch start
#iterations: 238
currently lose_sum: 514.2060702741146
time_elpased: 1.688
batch start
#iterations: 239
currently lose_sum: 513.4049691855907
time_elpased: 1.721
start validation test
0.583453608247
0.999418942475
0.175510204082
0.298585192258
0
validation finish
batch start
#iterations: 240
currently lose_sum: 513.3545386791229
time_elpased: 1.685
batch start
#iterations: 241
currently lose_sum: 514.3408851325512
time_elpased: 1.696
batch start
#iterations: 242
currently lose_sum: 513.1382526755333
time_elpased: 1.694
batch start
#iterations: 243
currently lose_sum: 515.3004127442837
time_elpased: 1.726
batch start
#iterations: 244
currently lose_sum: 515.7686750590801
time_elpased: 1.704
batch start
#iterations: 245
currently lose_sum: 515.5940596163273
time_elpased: 1.687
batch start
#iterations: 246
currently lose_sum: 515.7497898936272
time_elpased: 1.712
batch start
#iterations: 247
currently lose_sum: 515.8567388653755
time_elpased: 1.674
batch start
#iterations: 248
currently lose_sum: 515.9468051791191
time_elpased: 1.703
batch start
#iterations: 249
currently lose_sum: 516.1312790811062
time_elpased: 1.684
batch start
#iterations: 250
currently lose_sum: 514.6660839319229
time_elpased: 1.711
batch start
#iterations: 251
currently lose_sum: 515.6920279860497
time_elpased: 1.707
batch start
#iterations: 252
currently lose_sum: 515.654617369175
time_elpased: 1.694
batch start
#iterations: 253
currently lose_sum: 513.4379238784313
time_elpased: 1.758
batch start
#iterations: 254
currently lose_sum: 517.9068632721901
time_elpased: 1.693
batch start
#iterations: 255
currently lose_sum: 515.8708179295063
time_elpased: 1.71
batch start
#iterations: 256
currently lose_sum: 515.1632207334042
time_elpased: 1.698
batch start
#iterations: 257
currently lose_sum: 513.864402025938
time_elpased: 1.71
batch start
#iterations: 258
currently lose_sum: 515.3427452147007
time_elpased: 1.697
batch start
#iterations: 259
currently lose_sum: 516.4942741692066
time_elpased: 1.715
start validation test
0.59793814433
0.999001996008
0.204285714286
0.339207048458
0
validation finish
batch start
#iterations: 260
currently lose_sum: 517.3841553330421
time_elpased: 1.735
batch start
#iterations: 261
currently lose_sum: 517.27238458395
time_elpased: 1.833
batch start
#iterations: 262
currently lose_sum: 515.6980086565018
time_elpased: 1.847
batch start
#iterations: 263
currently lose_sum: 515.9337636232376
time_elpased: 1.874
batch start
#iterations: 264
currently lose_sum: 515.0302471220493
time_elpased: 1.861
batch start
#iterations: 265
currently lose_sum: 514.8617060184479
time_elpased: 1.861
batch start
#iterations: 266
currently lose_sum: 515.588535785675
time_elpased: 1.741
batch start
#iterations: 267
currently lose_sum: 514.9157100915909
time_elpased: 1.732
batch start
#iterations: 268
currently lose_sum: 516.8435755074024
time_elpased: 1.721
batch start
#iterations: 269
currently lose_sum: 516.0018374621868
time_elpased: 1.766
batch start
#iterations: 270
currently lose_sum: 515.2590435147285
time_elpased: 1.805
batch start
#iterations: 271
currently lose_sum: 512.0587874352932
time_elpased: 1.843
batch start
#iterations: 272
currently lose_sum: 515.9074802100658
time_elpased: 1.834
batch start
#iterations: 273
currently lose_sum: 514.6431498229504
time_elpased: 1.867
batch start
#iterations: 274
currently lose_sum: 515.9121912419796
time_elpased: 1.807
batch start
#iterations: 275
currently lose_sum: 515.972573786974
time_elpased: 1.831
batch start
#iterations: 276
currently lose_sum: 513.7414235770702
time_elpased: 1.878
batch start
#iterations: 277
currently lose_sum: 517.3897526562214
time_elpased: 1.795
batch start
#iterations: 278
currently lose_sum: 516.0744002759457
time_elpased: 1.789
batch start
#iterations: 279
currently lose_sum: 515.3191166520119
time_elpased: 1.847
start validation test
0.591082474227
0.998931052913
0.190714285714
0.320281038471
0
validation finish
batch start
#iterations: 280
currently lose_sum: 514.4045445024967
time_elpased: 1.8
batch start
#iterations: 281
currently lose_sum: 515.0651304721832
time_elpased: 1.862
batch start
#iterations: 282
currently lose_sum: 514.8507915139198
time_elpased: 1.828
batch start
#iterations: 283
currently lose_sum: 514.6079099774361
time_elpased: 1.821
batch start
#iterations: 284
currently lose_sum: 516.1596242785454
time_elpased: 1.872
batch start
#iterations: 285
currently lose_sum: 515.0213310718536
time_elpased: 1.785
batch start
#iterations: 286
currently lose_sum: 515.5777262747288
time_elpased: 1.816
batch start
#iterations: 287
currently lose_sum: 515.8064348995686
time_elpased: 1.798
batch start
#iterations: 288
currently lose_sum: 516.204209536314
time_elpased: 1.726
batch start
#iterations: 289
currently lose_sum: 515.7827985286713
time_elpased: 1.793
batch start
#iterations: 290
currently lose_sum: 513.6639859378338
time_elpased: 1.802
batch start
#iterations: 291
currently lose_sum: 516.8939767181873
time_elpased: 1.85
batch start
#iterations: 292
currently lose_sum: 515.2421781122684
time_elpased: 1.784
batch start
#iterations: 293
currently lose_sum: 514.7791094183922
time_elpased: 1.832
batch start
#iterations: 294
currently lose_sum: 514.9085184633732
time_elpased: 1.799
batch start
#iterations: 295
currently lose_sum: 516.1840214729309
time_elpased: 1.804
batch start
#iterations: 296
currently lose_sum: 512.5054036080837
time_elpased: 1.773
batch start
#iterations: 297
currently lose_sum: 515.525454044342
time_elpased: 1.764
batch start
#iterations: 298
currently lose_sum: 514.7057606875896
time_elpased: 1.781
batch start
#iterations: 299
currently lose_sum: 514.3265816271305
time_elpased: 1.808
start validation test
0.607886597938
0.998182644253
0.224183673469
0.36613615532
0
validation finish
batch start
#iterations: 300
currently lose_sum: 514.5050966441631
time_elpased: 1.752
batch start
#iterations: 301
currently lose_sum: 514.9132672846317
time_elpased: 1.735
batch start
#iterations: 302
currently lose_sum: 516.2037221491337
time_elpased: 1.75
batch start
#iterations: 303
currently lose_sum: 514.6334228217602
time_elpased: 1.832
batch start
#iterations: 304
currently lose_sum: 515.5037732124329
time_elpased: 1.859
batch start
#iterations: 305
currently lose_sum: 517.1073069274426
time_elpased: 1.849
batch start
#iterations: 306
currently lose_sum: 514.5193892717361
time_elpased: 1.848
batch start
#iterations: 307
currently lose_sum: 518.1512771546841
time_elpased: 1.834
batch start
#iterations: 308
currently lose_sum: 515.2626592516899
time_elpased: 1.851
batch start
#iterations: 309
currently lose_sum: 516.1402469575405
time_elpased: 1.839
batch start
#iterations: 310
currently lose_sum: 514.310178488493
time_elpased: 1.858
batch start
#iterations: 311
currently lose_sum: 513.621200799942
time_elpased: 1.851
batch start
#iterations: 312
currently lose_sum: 514.3884327411652
time_elpased: 1.803
batch start
#iterations: 313
currently lose_sum: 515.7636901140213
time_elpased: 1.745
batch start
#iterations: 314
currently lose_sum: 514.3899520635605
time_elpased: 1.798
batch start
#iterations: 315
currently lose_sum: 515.7788588702679
time_elpased: 1.869
batch start
#iterations: 316
currently lose_sum: 515.2085303068161
time_elpased: 1.807
batch start
#iterations: 317
currently lose_sum: 514.4182319045067
time_elpased: 1.787
batch start
#iterations: 318
currently lose_sum: 517.3393794000149
time_elpased: 1.752
batch start
#iterations: 319
currently lose_sum: 515.9179572165012
time_elpased: 1.872
start validation test
0.594742268041
0.998456790123
0.19806122449
0.330551771117
0
validation finish
batch start
#iterations: 320
currently lose_sum: 515.2083548009396
time_elpased: 1.843
batch start
#iterations: 321
currently lose_sum: 514.4918634295464
time_elpased: 1.755
batch start
#iterations: 322
currently lose_sum: 513.9525375664234
time_elpased: 1.755
batch start
#iterations: 323
currently lose_sum: 514.5368283390999
time_elpased: 1.75
batch start
#iterations: 324
currently lose_sum: 517.6095618903637
time_elpased: 1.832
batch start
#iterations: 325
currently lose_sum: 516.5378391742706
time_elpased: 1.844
batch start
#iterations: 326
currently lose_sum: 516.1296801269054
time_elpased: 1.849
batch start
#iterations: 327
currently lose_sum: 515.6651919186115
time_elpased: 1.881
batch start
#iterations: 328
currently lose_sum: 514.8556133210659
time_elpased: 1.866
batch start
#iterations: 329
currently lose_sum: 515.153578877449
time_elpased: 1.829
batch start
#iterations: 330
currently lose_sum: 516.5807636082172
time_elpased: 1.9
batch start
#iterations: 331
currently lose_sum: 516.7391458749771
time_elpased: 1.871
batch start
#iterations: 332
currently lose_sum: 514.8807029724121
time_elpased: 1.892
batch start
#iterations: 333
currently lose_sum: 515.9508572518826
time_elpased: 1.858
batch start
#iterations: 334
currently lose_sum: 515.8153882622719
time_elpased: 1.834
batch start
#iterations: 335
currently lose_sum: 513.5931725502014
time_elpased: 1.839
batch start
#iterations: 336
currently lose_sum: 516.1330146193504
time_elpased: 1.863
batch start
#iterations: 337
currently lose_sum: 514.8213922679424
time_elpased: 1.772
batch start
#iterations: 338
currently lose_sum: 516.6701793074608
time_elpased: 1.79
batch start
#iterations: 339
currently lose_sum: 514.8601891696453
time_elpased: 1.891
start validation test
0.60087628866
0.998545807077
0.210204081633
0.347298322515
0
validation finish
batch start
#iterations: 340
currently lose_sum: 515.2294739186764
time_elpased: 1.866
batch start
#iterations: 341
currently lose_sum: 515.6876707971096
time_elpased: 1.854
batch start
#iterations: 342
currently lose_sum: 514.7451635301113
time_elpased: 1.865
batch start
#iterations: 343
currently lose_sum: 515.5364562571049
time_elpased: 1.865
batch start
#iterations: 344
currently lose_sum: 515.8674151599407
time_elpased: 1.836
batch start
#iterations: 345
currently lose_sum: 515.978617399931
time_elpased: 1.762
batch start
#iterations: 346
currently lose_sum: 514.3817893266678
time_elpased: 1.786
batch start
#iterations: 347
currently lose_sum: 514.7461384534836
time_elpased: 1.754
batch start
#iterations: 348
currently lose_sum: 515.5734805464745
time_elpased: 1.83
batch start
#iterations: 349
currently lose_sum: 515.7615361213684
time_elpased: 1.853
batch start
#iterations: 350
currently lose_sum: 514.8982131481171
time_elpased: 1.811
batch start
#iterations: 351
currently lose_sum: 515.3669580221176
time_elpased: 1.843
batch start
#iterations: 352
currently lose_sum: 515.4818642139435
time_elpased: 1.881
batch start
#iterations: 353
currently lose_sum: 516.9918341040611
time_elpased: 1.795
batch start
#iterations: 354
currently lose_sum: 513.9497928619385
time_elpased: 1.836
batch start
#iterations: 355
currently lose_sum: 515.5442541539669
time_elpased: 1.812
batch start
#iterations: 356
currently lose_sum: 515.1244468390942
time_elpased: 1.84
batch start
#iterations: 357
currently lose_sum: 515.1423290669918
time_elpased: 1.801
batch start
#iterations: 358
currently lose_sum: 515.5720694959164
time_elpased: 1.868
batch start
#iterations: 359
currently lose_sum: 515.7589195668697
time_elpased: 1.84
start validation test
0.590515463918
0.998924731183
0.189591836735
0.318696397942
0
validation finish
batch start
#iterations: 360
currently lose_sum: 516.2322257459164
time_elpased: 1.863
batch start
#iterations: 361
currently lose_sum: 512.939331382513
time_elpased: 1.845
batch start
#iterations: 362
currently lose_sum: 516.1945304274559
time_elpased: 1.848
batch start
#iterations: 363
currently lose_sum: 514.6692532598972
time_elpased: 1.77
batch start
#iterations: 364
currently lose_sum: 515.357270270586
time_elpased: 1.83
batch start
#iterations: 365
currently lose_sum: 516.1624177098274
time_elpased: 1.842
batch start
#iterations: 366
currently lose_sum: 516.9560906291008
time_elpased: 1.803
batch start
#iterations: 367
currently lose_sum: 515.8030636608601
time_elpased: 1.83
batch start
#iterations: 368
currently lose_sum: 513.3167839050293
time_elpased: 1.821
batch start
#iterations: 369
currently lose_sum: 514.325526714325
time_elpased: 1.836
batch start
#iterations: 370
currently lose_sum: 513.8290489315987
time_elpased: 1.784
batch start
#iterations: 371
currently lose_sum: 515.8943266272545
time_elpased: 1.851
batch start
#iterations: 372
currently lose_sum: 515.1409623026848
time_elpased: 1.803
batch start
#iterations: 373
currently lose_sum: 516.6866380870342
time_elpased: 1.838
batch start
#iterations: 374
currently lose_sum: 516.5004063844681
time_elpased: 1.842
batch start
#iterations: 375
currently lose_sum: 515.3032832443714
time_elpased: 1.807
batch start
#iterations: 376
currently lose_sum: 515.5733037292957
time_elpased: 1.8
batch start
#iterations: 377
currently lose_sum: 515.8489374220371
time_elpased: 1.853
batch start
#iterations: 378
currently lose_sum: 515.1746149361134
time_elpased: 1.808
batch start
#iterations: 379
currently lose_sum: 514.4558035731316
time_elpased: 1.797
start validation test
0.594432989691
0.998966942149
0.197346938776
0.329584185412
0
validation finish
batch start
#iterations: 380
currently lose_sum: 515.8848679363728
time_elpased: 1.833
batch start
#iterations: 381
currently lose_sum: 514.5857638120651
time_elpased: 1.8
batch start
#iterations: 382
currently lose_sum: 515.7657091915607
time_elpased: 1.811
batch start
#iterations: 383
currently lose_sum: 513.2922362089157
time_elpased: 1.824
batch start
#iterations: 384
currently lose_sum: 514.8736493587494
time_elpased: 1.86
batch start
#iterations: 385
currently lose_sum: 513.3471238017082
time_elpased: 1.857
batch start
#iterations: 386
currently lose_sum: 514.6084574460983
time_elpased: 1.832
batch start
#iterations: 387
currently lose_sum: 515.1102369725704
time_elpased: 1.841
batch start
#iterations: 388
currently lose_sum: 515.3199676573277
time_elpased: 1.842
batch start
#iterations: 389
currently lose_sum: 513.8069902956486
time_elpased: 1.828
batch start
#iterations: 390
currently lose_sum: 513.0791391432285
time_elpased: 1.811
batch start
#iterations: 391
currently lose_sum: 515.0704037845135
time_elpased: 1.868
batch start
#iterations: 392
currently lose_sum: 515.1007389426231
time_elpased: 1.856
batch start
#iterations: 393
currently lose_sum: 515.466866761446
time_elpased: 1.881
batch start
#iterations: 394
currently lose_sum: 514.1796435117722
time_elpased: 1.815
batch start
#iterations: 395
currently lose_sum: 514.096199721098
time_elpased: 1.835
batch start
#iterations: 396
currently lose_sum: 514.7198522984982
time_elpased: 1.755
batch start
#iterations: 397
currently lose_sum: 516.2463541030884
time_elpased: 1.815
batch start
#iterations: 398
currently lose_sum: 514.5219285190105
time_elpased: 1.862
batch start
#iterations: 399
currently lose_sum: 516.2885094583035
time_elpased: 1.861
start validation test
0.591391752577
0.998934469899
0.191326530612
0.321144129485
0
validation finish
acc: 0.621
pre: 0.996
rec: 0.234
F1: 0.379
auc: 0.000
