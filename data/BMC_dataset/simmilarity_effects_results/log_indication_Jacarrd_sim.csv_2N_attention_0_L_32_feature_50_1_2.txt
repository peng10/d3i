start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 548.2459935545921
time_elpased: 1.796
batch start
#iterations: 1
currently lose_sum: 536.835627257824
time_elpased: 1.753
batch start
#iterations: 2
currently lose_sum: 533.1156340241432
time_elpased: 1.753
batch start
#iterations: 3
currently lose_sum: 530.069776147604
time_elpased: 1.776
batch start
#iterations: 4
currently lose_sum: 528.6573236882687
time_elpased: 1.764
batch start
#iterations: 5
currently lose_sum: 527.91341817379
time_elpased: 1.729
batch start
#iterations: 6
currently lose_sum: 524.3693622946739
time_elpased: 1.725
batch start
#iterations: 7
currently lose_sum: 525.45509532094
time_elpased: 1.749
batch start
#iterations: 8
currently lose_sum: 523.4469316005707
time_elpased: 1.732
batch start
#iterations: 9
currently lose_sum: 523.4834048748016
time_elpased: 1.729
batch start
#iterations: 10
currently lose_sum: 522.4025265276432
time_elpased: 1.718
batch start
#iterations: 11
currently lose_sum: 523.3405646085739
time_elpased: 1.719
batch start
#iterations: 12
currently lose_sum: 520.8184904456139
time_elpased: 1.712
batch start
#iterations: 13
currently lose_sum: 519.9026204943657
time_elpased: 1.727
batch start
#iterations: 14
currently lose_sum: 520.7448422908783
time_elpased: 1.699
batch start
#iterations: 15
currently lose_sum: 519.9761784076691
time_elpased: 1.713
batch start
#iterations: 16
currently lose_sum: 520.7471065223217
time_elpased: 1.812
batch start
#iterations: 17
currently lose_sum: 519.9252089262009
time_elpased: 1.723
batch start
#iterations: 18
currently lose_sum: 519.8383019268513
time_elpased: 1.715
batch start
#iterations: 19
currently lose_sum: 521.0202179849148
time_elpased: 1.732
start validation test
0.583659793814
0.991363355953
0.166184074457
0.284651492339
0
validation finish
batch start
#iterations: 20
currently lose_sum: 519.4554923772812
time_elpased: 1.721
batch start
#iterations: 21
currently lose_sum: 521.6508201658726
time_elpased: 1.746
batch start
#iterations: 22
currently lose_sum: 519.1483907699585
time_elpased: 1.717
batch start
#iterations: 23
currently lose_sum: 519.2809314727783
time_elpased: 1.702
batch start
#iterations: 24
currently lose_sum: 517.3398548364639
time_elpased: 1.731
batch start
#iterations: 25
currently lose_sum: 519.8975913822651
time_elpased: 1.716
batch start
#iterations: 26
currently lose_sum: 519.2609937489033
time_elpased: 1.725
batch start
#iterations: 27
currently lose_sum: 518.5546589791775
time_elpased: 1.734
batch start
#iterations: 28
currently lose_sum: 517.4978242218494
time_elpased: 1.749
batch start
#iterations: 29
currently lose_sum: 517.5500313639641
time_elpased: 1.736
batch start
#iterations: 30
currently lose_sum: 517.3574657440186
time_elpased: 1.721
batch start
#iterations: 31
currently lose_sum: 516.0588067173958
time_elpased: 1.712
batch start
#iterations: 32
currently lose_sum: 518.9612451195717
time_elpased: 1.743
batch start
#iterations: 33
currently lose_sum: 516.6803876757622
time_elpased: 1.711
batch start
#iterations: 34
currently lose_sum: 518.6377021968365
time_elpased: 1.705
batch start
#iterations: 35
currently lose_sum: 515.3687945306301
time_elpased: 1.718
batch start
#iterations: 36
currently lose_sum: 518.3064634203911
time_elpased: 1.703
batch start
#iterations: 37
currently lose_sum: 517.061083137989
time_elpased: 1.711
batch start
#iterations: 38
currently lose_sum: 517.9001850485802
time_elpased: 1.73
batch start
#iterations: 39
currently lose_sum: 519.1697233617306
time_elpased: 1.744
start validation test
0.585103092784
0.996325780772
0.168252326784
0.287888171282
0
validation finish
batch start
#iterations: 40
currently lose_sum: 519.2148159146309
time_elpased: 1.726
batch start
#iterations: 41
currently lose_sum: 519.5014537870884
time_elpased: 1.725
batch start
#iterations: 42
currently lose_sum: 517.7261752784252
time_elpased: 1.731
batch start
#iterations: 43
currently lose_sum: 517.3873715102673
time_elpased: 1.718
batch start
#iterations: 44
currently lose_sum: 517.0131773352623
time_elpased: 1.748
batch start
#iterations: 45
currently lose_sum: 516.9766060709953
time_elpased: 1.742
batch start
#iterations: 46
currently lose_sum: 516.4585125148296
time_elpased: 1.712
batch start
#iterations: 47
currently lose_sum: 517.3963276743889
time_elpased: 1.712
batch start
#iterations: 48
currently lose_sum: 516.1557224690914
time_elpased: 1.734
batch start
#iterations: 49
currently lose_sum: 516.5074765980244
time_elpased: 1.711
batch start
#iterations: 50
currently lose_sum: 518.4384336173534
time_elpased: 1.731
batch start
#iterations: 51
currently lose_sum: 516.9461068511009
time_elpased: 1.699
batch start
#iterations: 52
currently lose_sum: 516.3521105051041
time_elpased: 1.713
batch start
#iterations: 53
currently lose_sum: 518.4821584820747
time_elpased: 1.723
batch start
#iterations: 54
currently lose_sum: 516.2834871411324
time_elpased: 1.719
batch start
#iterations: 55
currently lose_sum: 517.8555567860603
time_elpased: 1.733
batch start
#iterations: 56
currently lose_sum: 517.281460672617
time_elpased: 1.716
batch start
#iterations: 57
currently lose_sum: 516.0495558381081
time_elpased: 1.748
batch start
#iterations: 58
currently lose_sum: 518.146527081728
time_elpased: 1.741
batch start
#iterations: 59
currently lose_sum: 515.667947947979
time_elpased: 1.711
start validation test
0.599278350515
0.997899159664
0.196483971044
0.328322101261
0
validation finish
batch start
#iterations: 60
currently lose_sum: 517.7753544151783
time_elpased: 1.698
batch start
#iterations: 61
currently lose_sum: 517.9101748764515
time_elpased: 1.72
batch start
#iterations: 62
currently lose_sum: 519.3446485698223
time_elpased: 1.721
batch start
#iterations: 63
currently lose_sum: 518.1691389679909
time_elpased: 1.724
batch start
#iterations: 64
currently lose_sum: 518.341780513525
time_elpased: 1.724
batch start
#iterations: 65
currently lose_sum: 518.4700897336006
time_elpased: 1.688
batch start
#iterations: 66
currently lose_sum: 516.9811809659004
time_elpased: 1.712
batch start
#iterations: 67
currently lose_sum: 514.9013620913029
time_elpased: 1.733
batch start
#iterations: 68
currently lose_sum: 519.2936975955963
time_elpased: 1.729
batch start
#iterations: 69
currently lose_sum: 517.5305250883102
time_elpased: 1.726
batch start
#iterations: 70
currently lose_sum: 517.6827536821365
time_elpased: 1.738
batch start
#iterations: 71
currently lose_sum: 517.1426218450069
time_elpased: 1.724
batch start
#iterations: 72
currently lose_sum: 517.009087651968
time_elpased: 1.701
batch start
#iterations: 73
currently lose_sum: 515.4206766784191
time_elpased: 1.713
batch start
#iterations: 74
currently lose_sum: 517.1350081861019
time_elpased: 1.708
batch start
#iterations: 75
currently lose_sum: 516.9018420875072
time_elpased: 1.741
batch start
#iterations: 76
currently lose_sum: 518.0874002277851
time_elpased: 1.711
batch start
#iterations: 77
currently lose_sum: 517.1023162007332
time_elpased: 1.749
batch start
#iterations: 78
currently lose_sum: 517.6373096108437
time_elpased: 1.733
batch start
#iterations: 79
currently lose_sum: 518.1774741113186
time_elpased: 1.722
start validation test
0.621958762887
0.997444633731
0.242192347466
0.389748710268
0
validation finish
batch start
#iterations: 80
currently lose_sum: 515.9845629930496
time_elpased: 1.72
batch start
#iterations: 81
currently lose_sum: 518.0691073536873
time_elpased: 1.727
batch start
#iterations: 82
currently lose_sum: 516.6582389771938
time_elpased: 1.754
batch start
#iterations: 83
currently lose_sum: 517.4540040791035
time_elpased: 1.721
batch start
#iterations: 84
currently lose_sum: 518.3797106444836
time_elpased: 1.699
batch start
#iterations: 85
currently lose_sum: 515.4448921382427
time_elpased: 1.696
batch start
#iterations: 86
currently lose_sum: 516.800412774086
time_elpased: 1.696
batch start
#iterations: 87
currently lose_sum: 515.9344010651112
time_elpased: 1.72
batch start
#iterations: 88
currently lose_sum: 517.5560014545918
time_elpased: 1.711
batch start
#iterations: 89
currently lose_sum: 516.0288681983948
time_elpased: 1.715
batch start
#iterations: 90
currently lose_sum: 517.9986472725868
time_elpased: 1.695
batch start
#iterations: 91
currently lose_sum: 515.6641437411308
time_elpased: 1.738
batch start
#iterations: 92
currently lose_sum: 518.0063607394695
time_elpased: 1.722
batch start
#iterations: 93
currently lose_sum: 517.254864513874
time_elpased: 1.711
batch start
#iterations: 94
currently lose_sum: 516.6710433959961
time_elpased: 1.716
batch start
#iterations: 95
currently lose_sum: 515.4646609127522
time_elpased: 1.707
batch start
#iterations: 96
currently lose_sum: 518.3826920092106
time_elpased: 1.685
batch start
#iterations: 97
currently lose_sum: 515.738219588995
time_elpased: 1.73
batch start
#iterations: 98
currently lose_sum: 517.4906204342842
time_elpased: 1.712
batch start
#iterations: 99
currently lose_sum: 517.0976573526859
time_elpased: 1.705
start validation test
0.592113402062
0.998864281658
0.181902792141
0.307759601085
0
validation finish
batch start
#iterations: 100
currently lose_sum: 517.5347131490707
time_elpased: 1.694
batch start
#iterations: 101
currently lose_sum: 516.3124778270721
time_elpased: 1.711
batch start
#iterations: 102
currently lose_sum: 515.2864218354225
time_elpased: 1.725
batch start
#iterations: 103
currently lose_sum: 517.0035918056965
time_elpased: 1.698
batch start
#iterations: 104
currently lose_sum: 516.4954990148544
time_elpased: 1.722
batch start
#iterations: 105
currently lose_sum: 516.5985036790371
time_elpased: 1.708
batch start
#iterations: 106
currently lose_sum: 517.6765291690826
time_elpased: 1.693
batch start
#iterations: 107
currently lose_sum: 516.8762917220592
time_elpased: 1.721
batch start
#iterations: 108
currently lose_sum: 514.9095014333725
time_elpased: 1.715
batch start
#iterations: 109
currently lose_sum: 516.9720768332481
time_elpased: 1.713
batch start
#iterations: 110
currently lose_sum: 516.745441108942
time_elpased: 1.723
batch start
#iterations: 111
currently lose_sum: 519.2244041264057
time_elpased: 1.709
batch start
#iterations: 112
currently lose_sum: 515.0737532377243
time_elpased: 1.717
batch start
#iterations: 113
currently lose_sum: 517.1421076953411
time_elpased: 1.726
batch start
#iterations: 114
currently lose_sum: 515.79000633955
time_elpased: 1.73
batch start
#iterations: 115
currently lose_sum: 516.946820139885
time_elpased: 1.713
batch start
#iterations: 116
currently lose_sum: 514.9265970885754
time_elpased: 1.725
batch start
#iterations: 117
currently lose_sum: 517.1878592371941
time_elpased: 1.719
batch start
#iterations: 118
currently lose_sum: 516.1785842478275
time_elpased: 1.705
batch start
#iterations: 119
currently lose_sum: 516.0229933261871
time_elpased: 1.737
start validation test
0.603402061856
0.997983870968
0.204756980352
0.339797494423
0
validation finish
batch start
#iterations: 120
currently lose_sum: 516.7995009720325
time_elpased: 1.705
batch start
#iterations: 121
currently lose_sum: 515.5521875619888
time_elpased: 1.709
batch start
#iterations: 122
currently lose_sum: 514.6122309863567
time_elpased: 1.696
batch start
#iterations: 123
currently lose_sum: 515.1928640305996
time_elpased: 1.724
batch start
#iterations: 124
currently lose_sum: 515.6250895559788
time_elpased: 1.702
batch start
#iterations: 125
currently lose_sum: 516.387034446001
time_elpased: 1.715
batch start
#iterations: 126
currently lose_sum: 515.7177030444145
time_elpased: 1.729
batch start
#iterations: 127
currently lose_sum: 517.3339881300926
time_elpased: 1.709
batch start
#iterations: 128
currently lose_sum: 515.0158346295357
time_elpased: 1.734
batch start
#iterations: 129
currently lose_sum: 516.3180463016033
time_elpased: 1.711
batch start
#iterations: 130
currently lose_sum: 516.5274149477482
time_elpased: 1.72
batch start
#iterations: 131
currently lose_sum: 516.4022437930107
time_elpased: 1.693
batch start
#iterations: 132
currently lose_sum: 518.1035477817059
time_elpased: 1.714
batch start
#iterations: 133
currently lose_sum: 516.6962092220783
time_elpased: 1.715
batch start
#iterations: 134
currently lose_sum: 516.6779481172562
time_elpased: 1.677
batch start
#iterations: 135
currently lose_sum: 514.1270066797733
time_elpased: 1.708
batch start
#iterations: 136
currently lose_sum: 515.7786029279232
time_elpased: 1.718
batch start
#iterations: 137
currently lose_sum: 515.1619167625904
time_elpased: 1.714
batch start
#iterations: 138
currently lose_sum: 516.6852824687958
time_elpased: 1.706
batch start
#iterations: 139
currently lose_sum: 517.3534292578697
time_elpased: 1.701
start validation test
0.589175257732
0.999412455934
0.175904860393
0.299155821316
0
validation finish
batch start
#iterations: 140
currently lose_sum: 513.3686538338661
time_elpased: 1.717
batch start
#iterations: 141
currently lose_sum: 517.7427526712418
time_elpased: 1.742
batch start
#iterations: 142
currently lose_sum: 516.0107492506504
time_elpased: 1.745
batch start
#iterations: 143
currently lose_sum: 516.6898323893547
time_elpased: 1.697
batch start
#iterations: 144
currently lose_sum: 517.4774443209171
time_elpased: 1.731
batch start
#iterations: 145
currently lose_sum: 517.4325338900089
time_elpased: 1.729
batch start
#iterations: 146
currently lose_sum: 518.661891490221
time_elpased: 1.753
batch start
#iterations: 147
currently lose_sum: 516.2663732767105
time_elpased: 1.723
batch start
#iterations: 148
currently lose_sum: 514.7247138023376
time_elpased: 1.719
batch start
#iterations: 149
currently lose_sum: 517.7089659571648
time_elpased: 1.731
batch start
#iterations: 150
currently lose_sum: 515.547421336174
time_elpased: 1.699
batch start
#iterations: 151
currently lose_sum: 515.9021881520748
time_elpased: 1.721
batch start
#iterations: 152
currently lose_sum: 516.5569994449615
time_elpased: 1.718
batch start
#iterations: 153
currently lose_sum: 513.4704990684986
time_elpased: 1.721
batch start
#iterations: 154
currently lose_sum: 518.8974303603172
time_elpased: 1.734
batch start
#iterations: 155
currently lose_sum: 515.0850815176964
time_elpased: 1.723
batch start
#iterations: 156
currently lose_sum: 516.934201747179
time_elpased: 1.724
batch start
#iterations: 157
currently lose_sum: 515.8958041071892
time_elpased: 1.749
batch start
#iterations: 158
currently lose_sum: 514.5976224839687
time_elpased: 1.74
batch start
#iterations: 159
currently lose_sum: 516.5184110105038
time_elpased: 1.749
start validation test
0.590412371134
0.999420625724
0.178386763185
0.302737802738
0
validation finish
batch start
#iterations: 160
currently lose_sum: 516.728356808424
time_elpased: 1.759
batch start
#iterations: 161
currently lose_sum: 516.7128531932831
time_elpased: 1.749
batch start
#iterations: 162
currently lose_sum: 515.6439523398876
time_elpased: 1.741
batch start
#iterations: 163
currently lose_sum: 513.5904544591904
time_elpased: 1.744
batch start
#iterations: 164
currently lose_sum: 518.3662556707859
time_elpased: 1.755
batch start
#iterations: 165
currently lose_sum: 515.3078563213348
time_elpased: 1.742
batch start
#iterations: 166
currently lose_sum: 515.5480800271034
time_elpased: 1.796
batch start
#iterations: 167
currently lose_sum: 517.2155588269234
time_elpased: 1.754
batch start
#iterations: 168
currently lose_sum: 516.4465908110142
time_elpased: 1.736
batch start
#iterations: 169
currently lose_sum: 518.196358025074
time_elpased: 1.811
batch start
#iterations: 170
currently lose_sum: 515.5820694565773
time_elpased: 1.779
batch start
#iterations: 171
currently lose_sum: 515.3389803469181
time_elpased: 1.75
batch start
#iterations: 172
currently lose_sum: 518.361787289381
time_elpased: 1.753
batch start
#iterations: 173
currently lose_sum: 516.383535951376
time_elpased: 1.827
batch start
#iterations: 174
currently lose_sum: 517.2182256281376
time_elpased: 1.837
batch start
#iterations: 175
currently lose_sum: 516.8747876882553
time_elpased: 1.793
batch start
#iterations: 176
currently lose_sum: 516.0897751152515
time_elpased: 1.783
batch start
#iterations: 177
currently lose_sum: 516.6861275732517
time_elpased: 1.796
batch start
#iterations: 178
currently lose_sum: 515.1532725989819
time_elpased: 1.858
batch start
#iterations: 179
currently lose_sum: 516.3519231677055
time_elpased: 1.789
start validation test
0.606649484536
0.99804592086
0.211271975181
0.348724076129
0
validation finish
batch start
#iterations: 180
currently lose_sum: 516.139716565609
time_elpased: 1.829
batch start
#iterations: 181
currently lose_sum: 517.8362137377262
time_elpased: 1.807
batch start
#iterations: 182
currently lose_sum: 514.7156090736389
time_elpased: 1.817
batch start
#iterations: 183
currently lose_sum: 516.8904395401478
time_elpased: 1.791
batch start
#iterations: 184
currently lose_sum: 516.1527655720711
time_elpased: 1.836
batch start
#iterations: 185
currently lose_sum: 514.5166060626507
time_elpased: 1.829
batch start
#iterations: 186
currently lose_sum: 517.9396142959595
time_elpased: 1.82
batch start
#iterations: 187
currently lose_sum: 515.2326566576958
time_elpased: 1.81
batch start
#iterations: 188
currently lose_sum: 514.9607748091221
time_elpased: 1.835
batch start
#iterations: 189
currently lose_sum: 515.4949401021004
time_elpased: 1.847
batch start
#iterations: 190
currently lose_sum: 513.930902838707
time_elpased: 1.881
batch start
#iterations: 191
currently lose_sum: 517.3533450067043
time_elpased: 1.853
batch start
#iterations: 192
currently lose_sum: 516.3268104791641
time_elpased: 1.784
batch start
#iterations: 193
currently lose_sum: 516.2820620536804
time_elpased: 1.797
batch start
#iterations: 194
currently lose_sum: 516.1242209076881
time_elpased: 1.815
batch start
#iterations: 195
currently lose_sum: 516.6438644826412
time_elpased: 1.781
batch start
#iterations: 196
currently lose_sum: 514.9431549608707
time_elpased: 1.76
batch start
#iterations: 197
currently lose_sum: 517.3594322502613
time_elpased: 1.784
batch start
#iterations: 198
currently lose_sum: 516.3749840557575
time_elpased: 1.822
batch start
#iterations: 199
currently lose_sum: 515.7837391495705
time_elpased: 1.749
start validation test
0.602680412371
0.999490835031
0.202998965874
0.337459171394
0
validation finish
batch start
#iterations: 200
currently lose_sum: 518.2384008467197
time_elpased: 1.82
batch start
#iterations: 201
currently lose_sum: 515.9191016256809
time_elpased: 1.798
batch start
#iterations: 202
currently lose_sum: 516.3900582492352
time_elpased: 1.816
batch start
#iterations: 203
currently lose_sum: 515.0978425741196
time_elpased: 1.761
batch start
#iterations: 204
currently lose_sum: 516.4766134917736
time_elpased: 1.759
batch start
#iterations: 205
currently lose_sum: 517.5235370695591
time_elpased: 1.753
batch start
#iterations: 206
currently lose_sum: 516.7414600253105
time_elpased: 1.771
batch start
#iterations: 207
currently lose_sum: 514.8358294069767
time_elpased: 1.841
batch start
#iterations: 208
currently lose_sum: 515.8805641531944
time_elpased: 1.816
batch start
#iterations: 209
currently lose_sum: 515.4208652675152
time_elpased: 1.791
batch start
#iterations: 210
currently lose_sum: 517.4396732449532
time_elpased: 1.762
batch start
#iterations: 211
currently lose_sum: 517.0815536081791
time_elpased: 1.748
batch start
#iterations: 212
currently lose_sum: 516.1944254338741
time_elpased: 1.77
batch start
#iterations: 213
currently lose_sum: 515.0288478732109
time_elpased: 1.768
batch start
#iterations: 214
currently lose_sum: 517.3315545320511
time_elpased: 1.772
batch start
#iterations: 215
currently lose_sum: 516.6941155791283
time_elpased: 1.822
batch start
#iterations: 216
currently lose_sum: 516.3484418392181
time_elpased: 1.856
batch start
#iterations: 217
currently lose_sum: 515.4121619164944
time_elpased: 1.767
batch start
#iterations: 218
currently lose_sum: 517.6541223824024
time_elpased: 1.753
batch start
#iterations: 219
currently lose_sum: 516.927619189024
time_elpased: 1.763
start validation test
0.599072164948
0.998945147679
0.195863495346
0.327511672142
0
validation finish
batch start
#iterations: 220
currently lose_sum: 514.7172378003597
time_elpased: 1.83
batch start
#iterations: 221
currently lose_sum: 517.3579153120518
time_elpased: 1.793
batch start
#iterations: 222
currently lose_sum: 515.3302490711212
time_elpased: 1.829
batch start
#iterations: 223
currently lose_sum: 516.9639567136765
time_elpased: 1.87
batch start
#iterations: 224
currently lose_sum: 515.216896802187
time_elpased: 1.89
batch start
#iterations: 225
currently lose_sum: 518.1600939035416
time_elpased: 1.851
batch start
#iterations: 226
currently lose_sum: 517.0881621539593
time_elpased: 1.825
batch start
#iterations: 227
currently lose_sum: 514.3735399842262
time_elpased: 1.842
batch start
#iterations: 228
currently lose_sum: 514.0828107893467
time_elpased: 1.825
batch start
#iterations: 229
currently lose_sum: 515.7508588731289
time_elpased: 1.806
batch start
#iterations: 230
currently lose_sum: 517.704211473465
time_elpased: 1.824
batch start
#iterations: 231
currently lose_sum: 518.6114016473293
time_elpased: 1.832
batch start
#iterations: 232
currently lose_sum: 515.4065312743187
time_elpased: 1.787
batch start
#iterations: 233
currently lose_sum: 514.2583862841129
time_elpased: 1.788
batch start
#iterations: 234
currently lose_sum: 514.550183057785
time_elpased: 1.798
batch start
#iterations: 235
currently lose_sum: 515.6300976872444
time_elpased: 1.805
batch start
#iterations: 236
currently lose_sum: 514.7215483486652
time_elpased: 1.798
batch start
#iterations: 237
currently lose_sum: 515.9225931167603
time_elpased: 1.845
batch start
#iterations: 238
currently lose_sum: 514.4237806499004
time_elpased: 1.888
batch start
#iterations: 239
currently lose_sum: 514.895750939846
time_elpased: 1.817
start validation test
0.587164948454
1.0
0.171768355739
0.293178007237
0
validation finish
batch start
#iterations: 240
currently lose_sum: 514.4367117881775
time_elpased: 1.806
batch start
#iterations: 241
currently lose_sum: 517.452047586441
time_elpased: 1.783
batch start
#iterations: 242
currently lose_sum: 514.6578760147095
time_elpased: 1.776
batch start
#iterations: 243
currently lose_sum: 514.2040111124516
time_elpased: 1.786
batch start
#iterations: 244
currently lose_sum: 515.5148449242115
time_elpased: 1.786
batch start
#iterations: 245
currently lose_sum: 516.1369310617447
time_elpased: 1.82
batch start
#iterations: 246
currently lose_sum: 515.5385883450508
time_elpased: 1.81
batch start
#iterations: 247
currently lose_sum: 516.2818796932697
time_elpased: 1.79
batch start
#iterations: 248
currently lose_sum: 515.3849975764751
time_elpased: 1.777
batch start
#iterations: 249
currently lose_sum: 516.1711636185646
time_elpased: 1.785
batch start
#iterations: 250
currently lose_sum: 515.5028120279312
time_elpased: 1.803
batch start
#iterations: 251
currently lose_sum: 517.2013882398605
time_elpased: 1.884
batch start
#iterations: 252
currently lose_sum: 516.3122954070568
time_elpased: 1.829
batch start
#iterations: 253
currently lose_sum: 515.3455554246902
time_elpased: 1.781
batch start
#iterations: 254
currently lose_sum: 516.8971692621708
time_elpased: 1.813
batch start
#iterations: 255
currently lose_sum: 514.83716532588
time_elpased: 1.805
batch start
#iterations: 256
currently lose_sum: 515.0026360452175
time_elpased: 1.787
batch start
#iterations: 257
currently lose_sum: 513.8526602983475
time_elpased: 1.836
batch start
#iterations: 258
currently lose_sum: 516.8527954518795
time_elpased: 1.763
batch start
#iterations: 259
currently lose_sum: 516.7624822258949
time_elpased: 1.779
start validation test
0.605515463918
0.998024691358
0.208996897622
0.345617785378
0
validation finish
batch start
#iterations: 260
currently lose_sum: 518.513791769743
time_elpased: 1.849
batch start
#iterations: 261
currently lose_sum: 517.4546203613281
time_elpased: 1.808
batch start
#iterations: 262
currently lose_sum: 515.8119204938412
time_elpased: 1.829
batch start
#iterations: 263
currently lose_sum: 515.7230626344681
time_elpased: 1.83
batch start
#iterations: 264
currently lose_sum: 516.3609170019627
time_elpased: 1.794
batch start
#iterations: 265
currently lose_sum: 516.6995380520821
time_elpased: 1.772
batch start
#iterations: 266
currently lose_sum: 514.9053225517273
time_elpased: 1.776
batch start
#iterations: 267
currently lose_sum: 514.1861662864685
time_elpased: 1.804
batch start
#iterations: 268
currently lose_sum: 515.5385959744453
time_elpased: 1.798
batch start
#iterations: 269
currently lose_sum: 516.0251320004463
time_elpased: 1.815
batch start
#iterations: 270
currently lose_sum: 514.7438865602016
time_elpased: 1.807
batch start
#iterations: 271
currently lose_sum: 513.9985624551773
time_elpased: 1.801
batch start
#iterations: 272
currently lose_sum: 516.3677162528038
time_elpased: 1.846
batch start
#iterations: 273
currently lose_sum: 513.9160859882832
time_elpased: 1.771
batch start
#iterations: 274
currently lose_sum: 515.4597766399384
time_elpased: 1.749
batch start
#iterations: 275
currently lose_sum: 515.1036382317543
time_elpased: 1.801
batch start
#iterations: 276
currently lose_sum: 515.8009175956249
time_elpased: 1.78
batch start
#iterations: 277
currently lose_sum: 515.802334010601
time_elpased: 1.794
batch start
#iterations: 278
currently lose_sum: 515.4748637080193
time_elpased: 1.777
batch start
#iterations: 279
currently lose_sum: 517.7664174735546
time_elpased: 1.785
start validation test
0.596134020619
0.99891245242
0.189968976215
0.319228429924
0
validation finish
batch start
#iterations: 280
currently lose_sum: 513.9597697257996
time_elpased: 1.796
batch start
#iterations: 281
currently lose_sum: 514.2198838293552
time_elpased: 1.832
batch start
#iterations: 282
currently lose_sum: 516.8071058094501
time_elpased: 1.803
batch start
#iterations: 283
currently lose_sum: 515.3387718498707
time_elpased: 1.787
batch start
#iterations: 284
currently lose_sum: 516.6868853271008
time_elpased: 1.794
batch start
#iterations: 285
currently lose_sum: 515.6255353093147
time_elpased: 1.803
batch start
#iterations: 286
currently lose_sum: 517.3790696561337
time_elpased: 1.803
batch start
#iterations: 287
currently lose_sum: 516.2911259531975
time_elpased: 1.793
batch start
#iterations: 288
currently lose_sum: 516.3804916143417
time_elpased: 1.805
batch start
#iterations: 289
currently lose_sum: 516.3005331158638
time_elpased: 1.792
batch start
#iterations: 290
currently lose_sum: 514.7141601741314
time_elpased: 1.803
batch start
#iterations: 291
currently lose_sum: 516.1106326580048
time_elpased: 1.832
batch start
#iterations: 292
currently lose_sum: 516.42819917202
time_elpased: 1.805
batch start
#iterations: 293
currently lose_sum: 516.3475322127342
time_elpased: 1.779
batch start
#iterations: 294
currently lose_sum: 517.4250556528568
time_elpased: 1.781
batch start
#iterations: 295
currently lose_sum: 515.7189242243767
time_elpased: 1.795
batch start
#iterations: 296
currently lose_sum: 514.1508310735226
time_elpased: 1.787
batch start
#iterations: 297
currently lose_sum: 516.8797309696674
time_elpased: 1.806
batch start
#iterations: 298
currently lose_sum: 515.2509515285492
time_elpased: 1.802
batch start
#iterations: 299
currently lose_sum: 516.1583636105061
time_elpased: 1.795
start validation test
0.607628865979
0.999030067895
0.213029989659
0.351176270031
0
validation finish
batch start
#iterations: 300
currently lose_sum: 515.4002951383591
time_elpased: 1.801
batch start
#iterations: 301
currently lose_sum: 515.4418902099133
time_elpased: 1.817
batch start
#iterations: 302
currently lose_sum: 518.4420721828938
time_elpased: 1.81
batch start
#iterations: 303
currently lose_sum: 518.9720640778542
time_elpased: 1.791
batch start
#iterations: 304
currently lose_sum: 512.014163672924
time_elpased: 1.796
batch start
#iterations: 305
currently lose_sum: 516.8967458307743
time_elpased: 1.777
batch start
#iterations: 306
currently lose_sum: 515.3868754208088
time_elpased: 1.766
batch start
#iterations: 307
currently lose_sum: 515.3734401464462
time_elpased: 1.771
batch start
#iterations: 308
currently lose_sum: 515.8290059566498
time_elpased: 1.796
batch start
#iterations: 309
currently lose_sum: 516.5366234481335
time_elpased: 1.785
batch start
#iterations: 310
currently lose_sum: 515.5714421868324
time_elpased: 1.786
batch start
#iterations: 311
currently lose_sum: 513.9135192334652
time_elpased: 1.784
batch start
#iterations: 312
currently lose_sum: 516.0099836587906
time_elpased: 1.762
batch start
#iterations: 313
currently lose_sum: 517.509360074997
time_elpased: 1.772
batch start
#iterations: 314
currently lose_sum: 514.9051019847393
time_elpased: 1.798
batch start
#iterations: 315
currently lose_sum: 515.2801399827003
time_elpased: 1.804
batch start
#iterations: 316
currently lose_sum: 515.557778596878
time_elpased: 1.813
batch start
#iterations: 317
currently lose_sum: 515.9043174684048
time_elpased: 1.768
batch start
#iterations: 318
currently lose_sum: 518.1167880892754
time_elpased: 1.771
batch start
#iterations: 319
currently lose_sum: 515.1279589533806
time_elpased: 1.783
start validation test
0.600824742268
0.99896373057
0.199379524302
0.332413793103
0
validation finish
batch start
#iterations: 320
currently lose_sum: 516.3095811605453
time_elpased: 1.763
batch start
#iterations: 321
currently lose_sum: 517.9264542460442
time_elpased: 1.775
batch start
#iterations: 322
currently lose_sum: 514.9362315237522
time_elpased: 1.808
batch start
#iterations: 323
currently lose_sum: 514.1584626734257
time_elpased: 1.781
batch start
#iterations: 324
currently lose_sum: 518.7090238630772
time_elpased: 1.773
batch start
#iterations: 325
currently lose_sum: 514.606377273798
time_elpased: 1.785
batch start
#iterations: 326
currently lose_sum: 514.9652192890644
time_elpased: 1.77
batch start
#iterations: 327
currently lose_sum: 516.0937886238098
time_elpased: 1.806
batch start
#iterations: 328
currently lose_sum: 515.852393746376
time_elpased: 1.807
batch start
#iterations: 329
currently lose_sum: 515.4610944390297
time_elpased: 1.767
batch start
#iterations: 330
currently lose_sum: 518.2431734800339
time_elpased: 1.821
batch start
#iterations: 331
currently lose_sum: 518.3578348755836
time_elpased: 1.792
batch start
#iterations: 332
currently lose_sum: 517.6464447081089
time_elpased: 1.792
batch start
#iterations: 333
currently lose_sum: 515.4705294966698
time_elpased: 1.785
batch start
#iterations: 334
currently lose_sum: 515.386213093996
time_elpased: 1.788
batch start
#iterations: 335
currently lose_sum: 515.4917541146278
time_elpased: 1.807
batch start
#iterations: 336
currently lose_sum: 517.5619678199291
time_elpased: 1.822
batch start
#iterations: 337
currently lose_sum: 516.3844068050385
time_elpased: 1.874
batch start
#iterations: 338
currently lose_sum: 515.7407836019993
time_elpased: 1.827
batch start
#iterations: 339
currently lose_sum: 516.0911108851433
time_elpased: 1.788
start validation test
0.604329896907
0.998998998999
0.206411582213
0.342132327734
0
validation finish
batch start
#iterations: 340
currently lose_sum: 516.3266658782959
time_elpased: 1.755
batch start
#iterations: 341
currently lose_sum: 516.8960857391357
time_elpased: 1.795
batch start
#iterations: 342
currently lose_sum: 517.9744904637337
time_elpased: 1.787
batch start
#iterations: 343
currently lose_sum: 517.0692682564259
time_elpased: 1.777
batch start
#iterations: 344
currently lose_sum: 517.1447831988335
time_elpased: 1.789
batch start
#iterations: 345
currently lose_sum: 516.9762605428696
time_elpased: 1.77
batch start
#iterations: 346
currently lose_sum: 513.6694769561291
time_elpased: 1.775
batch start
#iterations: 347
currently lose_sum: 515.7997251451015
time_elpased: 1.803
batch start
#iterations: 348
currently lose_sum: 515.0153590142727
time_elpased: 1.791
batch start
#iterations: 349
currently lose_sum: 516.1343878805637
time_elpased: 1.777
batch start
#iterations: 350
currently lose_sum: 516.4904962480068
time_elpased: 1.78
batch start
#iterations: 351
currently lose_sum: 515.0956054031849
time_elpased: 1.757
batch start
#iterations: 352
currently lose_sum: 515.5985690951347
time_elpased: 1.818
batch start
#iterations: 353
currently lose_sum: 516.9006260931492
time_elpased: 1.816
batch start
#iterations: 354
currently lose_sum: 514.8152927160263
time_elpased: 1.847
batch start
#iterations: 355
currently lose_sum: 515.8053596615791
time_elpased: 1.856
batch start
#iterations: 356
currently lose_sum: 516.4928461611271
time_elpased: 1.867
batch start
#iterations: 357
currently lose_sum: 515.7067335247993
time_elpased: 1.863
batch start
#iterations: 358
currently lose_sum: 515.5347555279732
time_elpased: 1.808
batch start
#iterations: 359
currently lose_sum: 515.309799015522
time_elpased: 1.803
start validation test
0.591288659794
0.999426276535
0.180144777663
0.305265924823
0
validation finish
batch start
#iterations: 360
currently lose_sum: 516.8606812953949
time_elpased: 1.82
batch start
#iterations: 361
currently lose_sum: 514.2145332992077
time_elpased: 1.82
batch start
#iterations: 362
currently lose_sum: 513.7565915286541
time_elpased: 1.777
batch start
#iterations: 363
currently lose_sum: 516.2217075526714
time_elpased: 1.782
batch start
#iterations: 364
currently lose_sum: 516.6505051851273
time_elpased: 1.767
batch start
#iterations: 365
currently lose_sum: 515.1318306624889
time_elpased: 1.793
batch start
#iterations: 366
currently lose_sum: 515.065982401371
time_elpased: 1.814
batch start
#iterations: 367
currently lose_sum: 517.1632922291756
time_elpased: 1.774
batch start
#iterations: 368
currently lose_sum: 513.3821219801903
time_elpased: 1.782
batch start
#iterations: 369
currently lose_sum: 515.2740494608879
time_elpased: 1.773
batch start
#iterations: 370
currently lose_sum: 514.2904290258884
time_elpased: 1.811
batch start
#iterations: 371
currently lose_sum: 517.1527967453003
time_elpased: 1.819
batch start
#iterations: 372
currently lose_sum: 515.7628050148487
time_elpased: 1.859
batch start
#iterations: 373
currently lose_sum: 516.3442586362362
time_elpased: 1.8
batch start
#iterations: 374
currently lose_sum: 517.6258416175842
time_elpased: 1.809
batch start
#iterations: 375
currently lose_sum: 517.6872952878475
time_elpased: 1.776
batch start
#iterations: 376
currently lose_sum: 515.9328477680683
time_elpased: 1.787
batch start
#iterations: 377
currently lose_sum: 514.8836549222469
time_elpased: 1.794
batch start
#iterations: 378
currently lose_sum: 515.4249726831913
time_elpased: 1.77
batch start
#iterations: 379
currently lose_sum: 513.6239261329174
time_elpased: 1.804
start validation test
0.599329896907
0.998947922146
0.196380558428
0.328234379051
0
validation finish
batch start
#iterations: 380
currently lose_sum: 516.9667540192604
time_elpased: 1.814
batch start
#iterations: 381
currently lose_sum: 515.5760021209717
time_elpased: 1.789
batch start
#iterations: 382
currently lose_sum: 516.711464047432
time_elpased: 1.774
batch start
#iterations: 383
currently lose_sum: 514.2892796993256
time_elpased: 1.774
batch start
#iterations: 384
currently lose_sum: 514.2196245193481
time_elpased: 1.786
batch start
#iterations: 385
currently lose_sum: 514.4543623328209
time_elpased: 1.799
batch start
#iterations: 386
currently lose_sum: 515.2787820100784
time_elpased: 1.809
batch start
#iterations: 387
currently lose_sum: 515.8186482191086
time_elpased: 1.806
batch start
#iterations: 388
currently lose_sum: 515.8074842393398
time_elpased: 1.772
batch start
#iterations: 389
currently lose_sum: 515.89996945858
time_elpased: 1.798
batch start
#iterations: 390
currently lose_sum: 516.1432630121708
time_elpased: 1.797
batch start
#iterations: 391
currently lose_sum: 516.1581000387669
time_elpased: 1.8
batch start
#iterations: 392
currently lose_sum: 515.3974335193634
time_elpased: 1.799
batch start
#iterations: 393
currently lose_sum: 515.0846145749092
time_elpased: 1.78
batch start
#iterations: 394
currently lose_sum: 515.9820215404034
time_elpased: 1.803
batch start
#iterations: 395
currently lose_sum: 514.7277497649193
time_elpased: 1.792
batch start
#iterations: 396
currently lose_sum: 516.4421270787716
time_elpased: 1.891
batch start
#iterations: 397
currently lose_sum: 517.1303438842297
time_elpased: 1.811
batch start
#iterations: 398
currently lose_sum: 515.5145067870617
time_elpased: 1.776
batch start
#iterations: 399
currently lose_sum: 514.8250792324543
time_elpased: 1.778
start validation test
0.594690721649
0.999447208402
0.186970010341
0.315010018294
0
validation finish
acc: 0.617
pre: 0.997
rec: 0.242
F1: 0.390
auc: 0.000
