start to construct graph
graph construct over
87453
epochs start
batch start
#iterations: 0
currently lose_sum: 547.084281206131
time_elpased: 1.752
batch start
#iterations: 1
currently lose_sum: 537.9264514148235
time_elpased: 1.712
batch start
#iterations: 2
currently lose_sum: 532.1618024408817
time_elpased: 1.717
batch start
#iterations: 3
currently lose_sum: 529.9030633568764
time_elpased: 1.761
batch start
#iterations: 4
currently lose_sum: 526.4515703320503
time_elpased: 1.775
batch start
#iterations: 5
currently lose_sum: 525.7454637885094
time_elpased: 1.72
batch start
#iterations: 6
currently lose_sum: 525.444606423378
time_elpased: 1.729
batch start
#iterations: 7
currently lose_sum: 523.9742235839367
time_elpased: 1.766
batch start
#iterations: 8
currently lose_sum: 524.165521979332
time_elpased: 1.719
batch start
#iterations: 9
currently lose_sum: 521.3137535750866
time_elpased: 1.731
batch start
#iterations: 10
currently lose_sum: 520.7181299626827
time_elpased: 1.739
batch start
#iterations: 11
currently lose_sum: 520.8951786756516
time_elpased: 1.765
batch start
#iterations: 12
currently lose_sum: 523.7157818078995
time_elpased: 1.727
batch start
#iterations: 13
currently lose_sum: 520.0441391766071
time_elpased: 1.719
batch start
#iterations: 14
currently lose_sum: 520.6783508062363
time_elpased: 1.717
batch start
#iterations: 15
currently lose_sum: 519.2529223263264
time_elpased: 1.711
batch start
#iterations: 16
currently lose_sum: 519.5684143900871
time_elpased: 1.71
batch start
#iterations: 17
currently lose_sum: 520.7973129153252
time_elpased: 1.743
batch start
#iterations: 18
currently lose_sum: 520.0441073477268
time_elpased: 1.737
batch start
#iterations: 19
currently lose_sum: 518.7951184511185
time_elpased: 1.692
start validation test
0.585
0.99196326062
0.176958525346
0.300338924133
0
validation finish
batch start
#iterations: 20
currently lose_sum: 520.5502057671547
time_elpased: 1.745
batch start
#iterations: 21
currently lose_sum: 518.0820814371109
time_elpased: 1.713
batch start
#iterations: 22
currently lose_sum: 519.8688154816628
time_elpased: 1.723
batch start
#iterations: 23
currently lose_sum: 518.8906995654106
time_elpased: 1.703
batch start
#iterations: 24
currently lose_sum: 518.901675760746
time_elpased: 1.745
batch start
#iterations: 25
currently lose_sum: 517.2457013726234
time_elpased: 1.713
batch start
#iterations: 26
currently lose_sum: 517.8842481374741
time_elpased: 1.713
batch start
#iterations: 27
currently lose_sum: 519.045205950737
time_elpased: 1.728
batch start
#iterations: 28
currently lose_sum: 517.4969658851624
time_elpased: 1.696
batch start
#iterations: 29
currently lose_sum: 516.5237695872784
time_elpased: 1.744
batch start
#iterations: 30
currently lose_sum: 517.5294845998287
time_elpased: 1.716
batch start
#iterations: 31
currently lose_sum: 518.1073272526264
time_elpased: 1.723
batch start
#iterations: 32
currently lose_sum: 518.305560529232
time_elpased: 1.727
batch start
#iterations: 33
currently lose_sum: 517.0250027179718
time_elpased: 1.711
batch start
#iterations: 34
currently lose_sum: 516.6371263563633
time_elpased: 1.697
batch start
#iterations: 35
currently lose_sum: 519.5936902463436
time_elpased: 1.742
batch start
#iterations: 36
currently lose_sum: 518.563655257225
time_elpased: 1.706
batch start
#iterations: 37
currently lose_sum: 518.4841310679913
time_elpased: 1.759
batch start
#iterations: 38
currently lose_sum: 517.9379227459431
time_elpased: 1.863
batch start
#iterations: 39
currently lose_sum: 518.4190230965614
time_elpased: 1.772
start validation test
0.599329896907
0.996015936255
0.204813108039
0.339760468869
0
validation finish
batch start
#iterations: 40
currently lose_sum: 517.4213215410709
time_elpased: 1.724
batch start
#iterations: 41
currently lose_sum: 518.6852142810822
time_elpased: 1.711
batch start
#iterations: 42
currently lose_sum: 518.129079580307
time_elpased: 1.711
batch start
#iterations: 43
currently lose_sum: 518.5047391951084
time_elpased: 1.725
batch start
#iterations: 44
currently lose_sum: 516.1371910274029
time_elpased: 1.707
batch start
#iterations: 45
currently lose_sum: 515.7317822873592
time_elpased: 1.694
batch start
#iterations: 46
currently lose_sum: 520.0009042918682
time_elpased: 1.702
batch start
#iterations: 47
currently lose_sum: 517.1001064777374
time_elpased: 1.707
batch start
#iterations: 48
currently lose_sum: 515.6140081882477
time_elpased: 1.695
batch start
#iterations: 49
currently lose_sum: 517.1473508179188
time_elpased: 1.722
batch start
#iterations: 50
currently lose_sum: 516.791350454092
time_elpased: 1.746
batch start
#iterations: 51
currently lose_sum: 518.7785956263542
time_elpased: 1.76
batch start
#iterations: 52
currently lose_sum: 519.1854934692383
time_elpased: 1.738
batch start
#iterations: 53
currently lose_sum: 517.3556602597237
time_elpased: 1.714
batch start
#iterations: 54
currently lose_sum: 516.6984733641148
time_elpased: 1.722
batch start
#iterations: 55
currently lose_sum: 515.4373622238636
time_elpased: 1.716
batch start
#iterations: 56
currently lose_sum: 516.1183653175831
time_elpased: 1.713
batch start
#iterations: 57
currently lose_sum: 515.6230708062649
time_elpased: 1.682
batch start
#iterations: 58
currently lose_sum: 517.3866246342659
time_elpased: 1.694
batch start
#iterations: 59
currently lose_sum: 516.7661149799824
time_elpased: 1.708
start validation test
0.581030927835
0.998781230957
0.167844342038
0.287392600386
0
validation finish
batch start
#iterations: 60
currently lose_sum: 517.5745460391045
time_elpased: 1.729
batch start
#iterations: 61
currently lose_sum: 516.7149553000927
time_elpased: 1.718
batch start
#iterations: 62
currently lose_sum: 515.8502151668072
time_elpased: 1.714
batch start
#iterations: 63
currently lose_sum: 519.2455817461014
time_elpased: 1.721
batch start
#iterations: 64
currently lose_sum: 517.4586364626884
time_elpased: 1.667
batch start
#iterations: 65
currently lose_sum: 518.1757566034794
time_elpased: 1.674
batch start
#iterations: 66
currently lose_sum: 515.8818928897381
time_elpased: 1.714
batch start
#iterations: 67
currently lose_sum: 515.4315249621868
time_elpased: 1.701
batch start
#iterations: 68
currently lose_sum: 514.751881569624
time_elpased: 1.705
batch start
#iterations: 69
currently lose_sum: 516.3078695237637
time_elpased: 1.706
batch start
#iterations: 70
currently lose_sum: 517.1053425073624
time_elpased: 1.713
batch start
#iterations: 71
currently lose_sum: 516.7333737909794
time_elpased: 1.703
batch start
#iterations: 72
currently lose_sum: 514.2486763298512
time_elpased: 1.697
batch start
#iterations: 73
currently lose_sum: 516.7974876761436
time_elpased: 1.731
batch start
#iterations: 74
currently lose_sum: 517.4465847313404
time_elpased: 1.722
batch start
#iterations: 75
currently lose_sum: 516.414104193449
time_elpased: 1.708
batch start
#iterations: 76
currently lose_sum: 516.8814336359501
time_elpased: 1.717
batch start
#iterations: 77
currently lose_sum: 516.6360023915768
time_elpased: 1.698
batch start
#iterations: 78
currently lose_sum: 515.5093051195145
time_elpased: 1.701
batch start
#iterations: 79
currently lose_sum: 516.7326930761337
time_elpased: 1.7
start validation test
0.600618556701
0.997042878265
0.207168458781
0.34305579108
0
validation finish
batch start
#iterations: 80
currently lose_sum: 515.933629989624
time_elpased: 1.743
batch start
#iterations: 81
currently lose_sum: 518.0015632212162
time_elpased: 1.702
batch start
#iterations: 82
currently lose_sum: 517.1039426326752
time_elpased: 1.715
batch start
#iterations: 83
currently lose_sum: 514.6199774742126
time_elpased: 1.702
batch start
#iterations: 84
currently lose_sum: 515.3420482575893
time_elpased: 1.736
batch start
#iterations: 85
currently lose_sum: 518.516282171011
time_elpased: 1.723
batch start
#iterations: 86
currently lose_sum: 517.5352793633938
time_elpased: 1.695
batch start
#iterations: 87
currently lose_sum: 517.9227959811687
time_elpased: 1.752
batch start
#iterations: 88
currently lose_sum: 516.5932504236698
time_elpased: 1.715
batch start
#iterations: 89
currently lose_sum: 515.7504433393478
time_elpased: 1.69
batch start
#iterations: 90
currently lose_sum: 516.6919417083263
time_elpased: 1.703
batch start
#iterations: 91
currently lose_sum: 514.8597627580166
time_elpased: 1.692
batch start
#iterations: 92
currently lose_sum: 514.838149279356
time_elpased: 1.695
batch start
#iterations: 93
currently lose_sum: 513.7747150361538
time_elpased: 1.728
batch start
#iterations: 94
currently lose_sum: 516.7706862688065
time_elpased: 1.706
batch start
#iterations: 95
currently lose_sum: 515.195571988821
time_elpased: 1.682
batch start
#iterations: 96
currently lose_sum: 516.8389586210251
time_elpased: 1.681
batch start
#iterations: 97
currently lose_sum: 518.3801474571228
time_elpased: 1.697
batch start
#iterations: 98
currently lose_sum: 514.207868874073
time_elpased: 1.699
batch start
#iterations: 99
currently lose_sum: 516.67336332798
time_elpased: 1.708
start validation test
0.583298969072
0.998221695317
0.172452636969
0.294097100943
0
validation finish
batch start
#iterations: 100
currently lose_sum: 516.0304402410984
time_elpased: 1.719
batch start
#iterations: 101
currently lose_sum: 515.8426526486874
time_elpased: 1.717
batch start
#iterations: 102
currently lose_sum: 517.5374938845634
time_elpased: 1.71
batch start
#iterations: 103
currently lose_sum: 514.0241580605507
time_elpased: 1.719
batch start
#iterations: 104
currently lose_sum: 514.7618326544762
time_elpased: 1.725
batch start
#iterations: 105
currently lose_sum: 515.2087432444096
time_elpased: 1.709
batch start
#iterations: 106
currently lose_sum: 516.9196340143681
time_elpased: 1.742
batch start
#iterations: 107
currently lose_sum: 516.3941894471645
time_elpased: 1.735
batch start
#iterations: 108
currently lose_sum: 515.1905167400837
time_elpased: 1.738
batch start
#iterations: 109
currently lose_sum: 517.8506798148155
time_elpased: 1.706
batch start
#iterations: 110
currently lose_sum: 516.0775097310543
time_elpased: 1.721
batch start
#iterations: 111
currently lose_sum: 517.4034015536308
time_elpased: 1.708
batch start
#iterations: 112
currently lose_sum: 514.7541126012802
time_elpased: 1.714
batch start
#iterations: 113
currently lose_sum: 516.2610559463501
time_elpased: 1.696
batch start
#iterations: 114
currently lose_sum: 516.8042524755001
time_elpased: 1.706
batch start
#iterations: 115
currently lose_sum: 517.1558612883091
time_elpased: 1.759
batch start
#iterations: 116
currently lose_sum: 516.2285375595093
time_elpased: 1.706
batch start
#iterations: 117
currently lose_sum: 515.938771635294
time_elpased: 1.708
batch start
#iterations: 118
currently lose_sum: 516.8978741168976
time_elpased: 1.734
batch start
#iterations: 119
currently lose_sum: 514.2103216350079
time_elpased: 1.733
start validation test
0.582989690722
1.0
0.171530977983
0.292832167832
0
validation finish
batch start
#iterations: 120
currently lose_sum: 516.5121116638184
time_elpased: 1.749
batch start
#iterations: 121
currently lose_sum: 516.416622877121
time_elpased: 1.721
batch start
#iterations: 122
currently lose_sum: 517.635957211256
time_elpased: 1.732
batch start
#iterations: 123
currently lose_sum: 518.8361869156361
time_elpased: 1.709
batch start
#iterations: 124
currently lose_sum: 516.242993324995
time_elpased: 1.733
batch start
#iterations: 125
currently lose_sum: 513.96142411232
time_elpased: 1.699
batch start
#iterations: 126
currently lose_sum: 513.8747821450233
time_elpased: 1.746
batch start
#iterations: 127
currently lose_sum: 515.6063447594643
time_elpased: 1.703
batch start
#iterations: 128
currently lose_sum: 516.7700840234756
time_elpased: 1.719
batch start
#iterations: 129
currently lose_sum: 515.8959153592587
time_elpased: 1.699
batch start
#iterations: 130
currently lose_sum: 514.69772580266
time_elpased: 1.709
batch start
#iterations: 131
currently lose_sum: 515.901238232851
time_elpased: 1.74
batch start
#iterations: 132
currently lose_sum: 515.1487134993076
time_elpased: 1.696
batch start
#iterations: 133
currently lose_sum: 515.3904947042465
time_elpased: 1.717
batch start
#iterations: 134
currently lose_sum: 515.1183279454708
time_elpased: 1.698
batch start
#iterations: 135
currently lose_sum: 513.9995597600937
time_elpased: 1.714
batch start
#iterations: 136
currently lose_sum: 517.12216848135
time_elpased: 1.694
batch start
#iterations: 137
currently lose_sum: 516.5578734576702
time_elpased: 1.723
batch start
#iterations: 138
currently lose_sum: 517.3510599732399
time_elpased: 1.739
batch start
#iterations: 139
currently lose_sum: 516.3226535022259
time_elpased: 1.706
start validation test
0.606804123711
0.998135198135
0.219252432156
0.359529806885
0
validation finish
batch start
#iterations: 140
currently lose_sum: 515.4641283452511
time_elpased: 1.724
batch start
#iterations: 141
currently lose_sum: 514.547419577837
time_elpased: 1.738
batch start
#iterations: 142
currently lose_sum: 515.6151230335236
time_elpased: 1.743
batch start
#iterations: 143
currently lose_sum: 515.3801355659962
time_elpased: 1.721
batch start
#iterations: 144
currently lose_sum: 516.0905673801899
time_elpased: 1.732
batch start
#iterations: 145
currently lose_sum: 515.926877617836
time_elpased: 1.739
batch start
#iterations: 146
currently lose_sum: 516.2814290821552
time_elpased: 1.697
batch start
#iterations: 147
currently lose_sum: 515.528860270977
time_elpased: 1.727
batch start
#iterations: 148
currently lose_sum: 515.0005417466164
time_elpased: 1.694
batch start
#iterations: 149
currently lose_sum: 517.1359705626965
time_elpased: 1.709
batch start
#iterations: 150
currently lose_sum: 515.8580290675163
time_elpased: 1.729
batch start
#iterations: 151
currently lose_sum: 514.1402242183685
time_elpased: 1.731
batch start
#iterations: 152
currently lose_sum: 515.156379699707
time_elpased: 1.707
batch start
#iterations: 153
currently lose_sum: 512.8971064388752
time_elpased: 1.723
batch start
#iterations: 154
currently lose_sum: 516.5795109272003
time_elpased: 1.72
batch start
#iterations: 155
currently lose_sum: 516.9489655792713
time_elpased: 1.708
batch start
#iterations: 156
currently lose_sum: 516.0359815657139
time_elpased: 1.734
batch start
#iterations: 157
currently lose_sum: 515.0475756525993
time_elpased: 1.73
batch start
#iterations: 158
currently lose_sum: 516.2158632576466
time_elpased: 1.687
batch start
#iterations: 159
currently lose_sum: 514.50344145298
time_elpased: 1.692
start validation test
0.593711340206
0.999469496021
0.192933947773
0.323433476395
0
validation finish
batch start
#iterations: 160
currently lose_sum: 515.9103218019009
time_elpased: 1.723
batch start
#iterations: 161
currently lose_sum: 516.5293229520321
time_elpased: 1.722
batch start
#iterations: 162
currently lose_sum: 514.6451413631439
time_elpased: 1.725
batch start
#iterations: 163
currently lose_sum: 515.9732346534729
time_elpased: 1.707
batch start
#iterations: 164
currently lose_sum: 515.5934297144413
time_elpased: 1.703
batch start
#iterations: 165
currently lose_sum: 514.7303592264652
time_elpased: 1.712
batch start
#iterations: 166
currently lose_sum: 515.1392809450626
time_elpased: 1.702
batch start
#iterations: 167
currently lose_sum: 514.9115334153175
time_elpased: 1.72
batch start
#iterations: 168
currently lose_sum: 516.4196147024632
time_elpased: 1.714
batch start
#iterations: 169
currently lose_sum: 515.1039430201054
time_elpased: 1.733
batch start
#iterations: 170
currently lose_sum: 515.8431774377823
time_elpased: 1.743
batch start
#iterations: 171
currently lose_sum: 515.7576479315758
time_elpased: 1.72
batch start
#iterations: 172
currently lose_sum: 514.8517455160618
time_elpased: 1.71
batch start
#iterations: 173
currently lose_sum: 513.6590066850185
time_elpased: 1.691
batch start
#iterations: 174
currently lose_sum: 515.8911716043949
time_elpased: 1.709
batch start
#iterations: 175
currently lose_sum: 514.1496057212353
time_elpased: 1.704
batch start
#iterations: 176
currently lose_sum: 515.5866277217865
time_elpased: 1.688
batch start
#iterations: 177
currently lose_sum: 516.109834253788
time_elpased: 1.726
batch start
#iterations: 178
currently lose_sum: 515.3328306376934
time_elpased: 1.721
batch start
#iterations: 179
currently lose_sum: 517.7864694297314
time_elpased: 1.713
start validation test
0.601391752577
0.998527968597
0.20839733743
0.344827586207
0
validation finish
batch start
#iterations: 180
currently lose_sum: 515.8634940087795
time_elpased: 1.711
batch start
#iterations: 181
currently lose_sum: 516.5771382451057
time_elpased: 1.737
batch start
#iterations: 182
currently lose_sum: 515.2221638262272
time_elpased: 1.717
batch start
#iterations: 183
currently lose_sum: 515.1169777810574
time_elpased: 1.714
batch start
#iterations: 184
currently lose_sum: 515.3873531222343
time_elpased: 1.704
batch start
#iterations: 185
currently lose_sum: 515.6458000838757
time_elpased: 1.717
batch start
#iterations: 186
currently lose_sum: 514.1833863854408
time_elpased: 1.702
batch start
#iterations: 187
currently lose_sum: 517.8980104923248
time_elpased: 1.769
batch start
#iterations: 188
currently lose_sum: 515.7906140387058
time_elpased: 1.712
batch start
#iterations: 189
currently lose_sum: 516.2460552453995
time_elpased: 1.724
batch start
#iterations: 190
currently lose_sum: 515.9829221069813
time_elpased: 1.706
batch start
#iterations: 191
currently lose_sum: 515.8321533203125
time_elpased: 1.732
batch start
#iterations: 192
currently lose_sum: 514.7341013252735
time_elpased: 1.689
batch start
#iterations: 193
currently lose_sum: 514.9052349030972
time_elpased: 1.711
batch start
#iterations: 194
currently lose_sum: 516.5209629833698
time_elpased: 1.721
batch start
#iterations: 195
currently lose_sum: 514.7157096266747
time_elpased: 1.728
batch start
#iterations: 196
currently lose_sum: 517.1973042786121
time_elpased: 1.732
batch start
#iterations: 197
currently lose_sum: 516.3776084780693
time_elpased: 1.709
batch start
#iterations: 198
currently lose_sum: 515.6074154675007
time_elpased: 1.716
batch start
#iterations: 199
currently lose_sum: 517.2680645883083
time_elpased: 1.723
start validation test
0.597164948454
0.999487704918
0.199795186892
0.333020397713
0
validation finish
batch start
#iterations: 200
currently lose_sum: 512.3806590735912
time_elpased: 1.74
batch start
#iterations: 201
currently lose_sum: 515.8439619243145
time_elpased: 1.71
batch start
#iterations: 202
currently lose_sum: 513.6514413356781
time_elpased: 1.718
batch start
#iterations: 203
currently lose_sum: 516.1901895105839
time_elpased: 1.719
batch start
#iterations: 204
currently lose_sum: 515.8667979836464
time_elpased: 1.706
batch start
#iterations: 205
currently lose_sum: 514.8843855857849
time_elpased: 1.728
batch start
#iterations: 206
currently lose_sum: 512.8352685570717
time_elpased: 1.717
batch start
#iterations: 207
currently lose_sum: 514.0624870955944
time_elpased: 1.765
batch start
#iterations: 208
currently lose_sum: 515.8630718588829
time_elpased: 1.726
batch start
#iterations: 209
currently lose_sum: 516.1412655711174
time_elpased: 1.723
batch start
#iterations: 210
currently lose_sum: 515.1289444565773
time_elpased: 1.72
batch start
#iterations: 211
currently lose_sum: 515.1998106241226
time_elpased: 1.716
batch start
#iterations: 212
currently lose_sum: 515.0076087117195
time_elpased: 1.734
batch start
#iterations: 213
currently lose_sum: 516.858640640974
time_elpased: 1.721
batch start
#iterations: 214
currently lose_sum: 517.2648804187775
time_elpased: 1.739
batch start
#iterations: 215
currently lose_sum: 515.5025645196438
time_elpased: 1.737
batch start
#iterations: 216
currently lose_sum: 516.456340432167
time_elpased: 1.739
batch start
#iterations: 217
currently lose_sum: 514.7824455797672
time_elpased: 1.734
batch start
#iterations: 218
currently lose_sum: 516.8790796995163
time_elpased: 1.73
batch start
#iterations: 219
currently lose_sum: 515.4622498452663
time_elpased: 1.713
start validation test
0.585463917526
0.999420289855
0.17654889913
0.300087032202
0
validation finish
batch start
#iterations: 220
currently lose_sum: 516.1997669935226
time_elpased: 1.724
batch start
#iterations: 221
currently lose_sum: 516.9496854543686
time_elpased: 1.725
batch start
#iterations: 222
currently lose_sum: 515.4708543717861
time_elpased: 1.7
batch start
#iterations: 223
currently lose_sum: 511.84728798270226
time_elpased: 1.702
batch start
#iterations: 224
currently lose_sum: 517.6068295538425
time_elpased: 1.72
batch start
#iterations: 225
currently lose_sum: 515.8055077195168
time_elpased: 1.735
batch start
#iterations: 226
currently lose_sum: 516.7138819992542
time_elpased: 1.719
batch start
#iterations: 227
currently lose_sum: 513.4319396615028
time_elpased: 1.726
batch start
#iterations: 228
currently lose_sum: 514.9792450368404
time_elpased: 1.753
batch start
#iterations: 229
currently lose_sum: 516.4886503815651
time_elpased: 1.713
batch start
#iterations: 230
currently lose_sum: 515.3749756217003
time_elpased: 1.735
batch start
#iterations: 231
currently lose_sum: 519.4708896875381
time_elpased: 1.71
batch start
#iterations: 232
currently lose_sum: 515.9067153036594
time_elpased: 1.736
batch start
#iterations: 233
currently lose_sum: 515.144769191742
time_elpased: 1.726
batch start
#iterations: 234
currently lose_sum: 515.0985246300697
time_elpased: 1.732
batch start
#iterations: 235
currently lose_sum: 515.787459552288
time_elpased: 1.707
batch start
#iterations: 236
currently lose_sum: 514.1315514743328
time_elpased: 1.734
batch start
#iterations: 237
currently lose_sum: 514.5236685574055
time_elpased: 1.741
batch start
#iterations: 238
currently lose_sum: 514.2902389764786
time_elpased: 1.727
batch start
#iterations: 239
currently lose_sum: 518.9673829376698
time_elpased: 1.737
start validation test
0.596597938144
0.998457583548
0.198873527906
0.331682322801
0
validation finish
batch start
#iterations: 240
currently lose_sum: 516.2366624176502
time_elpased: 1.74
batch start
#iterations: 241
currently lose_sum: 515.9224472939968
time_elpased: 1.74
batch start
#iterations: 242
currently lose_sum: 514.9896829426289
time_elpased: 1.754
batch start
#iterations: 243
currently lose_sum: 515.0786390304565
time_elpased: 1.763
batch start
#iterations: 244
currently lose_sum: 517.6705449819565
time_elpased: 1.77
batch start
#iterations: 245
currently lose_sum: 515.2789026498795
time_elpased: 1.75
batch start
#iterations: 246
currently lose_sum: 515.7941414117813
time_elpased: 1.747
batch start
#iterations: 247
currently lose_sum: 516.6835969686508
time_elpased: 1.727
batch start
#iterations: 248
currently lose_sum: 513.905431330204
time_elpased: 1.764
batch start
#iterations: 249
currently lose_sum: 516.610642105341
time_elpased: 1.754
batch start
#iterations: 250
currently lose_sum: 516.1023471057415
time_elpased: 1.744
batch start
#iterations: 251
currently lose_sum: 515.0910741686821
time_elpased: 1.738
batch start
#iterations: 252
currently lose_sum: 515.51136302948
time_elpased: 1.728
batch start
#iterations: 253
currently lose_sum: 514.7938372194767
time_elpased: 1.75
batch start
#iterations: 254
currently lose_sum: 517.1745518743992
time_elpased: 1.748
batch start
#iterations: 255
currently lose_sum: 514.8281825482845
time_elpased: 1.75
batch start
#iterations: 256
currently lose_sum: 516.0524708926678
time_elpased: 1.742
batch start
#iterations: 257
currently lose_sum: 514.091068893671
time_elpased: 1.725
batch start
#iterations: 258
currently lose_sum: 513.3418279588223
time_elpased: 1.735
batch start
#iterations: 259
currently lose_sum: 515.9595884084702
time_elpased: 1.743
start validation test
0.599329896907
0.999498495486
0.204096262161
0.338974402585
0
validation finish
batch start
#iterations: 260
currently lose_sum: 515.6499513685703
time_elpased: 1.733
batch start
#iterations: 261
currently lose_sum: 515.5590376853943
time_elpased: 1.733
batch start
#iterations: 262
currently lose_sum: 515.8586977422237
time_elpased: 1.73
batch start
#iterations: 263
currently lose_sum: 515.9425592422485
time_elpased: 1.726
batch start
#iterations: 264
currently lose_sum: 516.2733430564404
time_elpased: 1.748
batch start
#iterations: 265
currently lose_sum: 517.3784593939781
time_elpased: 1.875
batch start
#iterations: 266
currently lose_sum: 516.1308606564999
time_elpased: 1.789
batch start
#iterations: 267
currently lose_sum: 515.5091288685799
time_elpased: 1.831
batch start
#iterations: 268
currently lose_sum: 512.9967911541462
time_elpased: 1.804
batch start
#iterations: 269
currently lose_sum: 514.1171015501022
time_elpased: 1.796
batch start
#iterations: 270
currently lose_sum: 516.1860797405243
time_elpased: 1.76
batch start
#iterations: 271
currently lose_sum: 517.6063753962517
time_elpased: 1.802
batch start
#iterations: 272
currently lose_sum: 514.9710108935833
time_elpased: 1.736
batch start
#iterations: 273
currently lose_sum: 516.4153856933117
time_elpased: 1.756
batch start
#iterations: 274
currently lose_sum: 514.949793189764
time_elpased: 1.746
batch start
#iterations: 275
currently lose_sum: 514.4527122974396
time_elpased: 1.775
batch start
#iterations: 276
currently lose_sum: 516.666001200676
time_elpased: 1.743
batch start
#iterations: 277
currently lose_sum: 514.4352725744247
time_elpased: 1.768
batch start
#iterations: 278
currently lose_sum: 513.5714735686779
time_elpased: 1.79
batch start
#iterations: 279
currently lose_sum: 514.0190432071686
time_elpased: 1.73
start validation test
0.597268041237
0.998977505112
0.200102406554
0.333418650286
0
validation finish
batch start
#iterations: 280
currently lose_sum: 516.7885412573814
time_elpased: 1.788
batch start
#iterations: 281
currently lose_sum: 515.3307170569897
time_elpased: 1.777
batch start
#iterations: 282
currently lose_sum: 516.916920363903
time_elpased: 1.746
batch start
#iterations: 283
currently lose_sum: 515.3802192211151
time_elpased: 1.747
batch start
#iterations: 284
currently lose_sum: 514.390891969204
time_elpased: 1.795
batch start
#iterations: 285
currently lose_sum: 514.0794963538647
time_elpased: 1.759
batch start
#iterations: 286
currently lose_sum: 515.2626892626286
time_elpased: 1.781
batch start
#iterations: 287
currently lose_sum: 515.8287139832973
time_elpased: 1.751
batch start
#iterations: 288
currently lose_sum: 517.7683218717575
time_elpased: 1.755
batch start
#iterations: 289
currently lose_sum: 517.6483718454838
time_elpased: 1.777
batch start
#iterations: 290
currently lose_sum: 514.0502927601337
time_elpased: 1.739
batch start
#iterations: 291
currently lose_sum: 513.5052004754543
time_elpased: 1.768
batch start
#iterations: 292
currently lose_sum: 514.8796508014202
time_elpased: 1.744
batch start
#iterations: 293
currently lose_sum: 516.6412603855133
time_elpased: 1.775
batch start
#iterations: 294
currently lose_sum: 514.8842680454254
time_elpased: 1.747
batch start
#iterations: 295
currently lose_sum: 514.6816448271275
time_elpased: 1.766
batch start
#iterations: 296
currently lose_sum: 515.8394593000412
time_elpased: 1.742
batch start
#iterations: 297
currently lose_sum: 514.1938964426517
time_elpased: 1.792
batch start
#iterations: 298
currently lose_sum: 515.598936021328
time_elpased: 1.759
batch start
#iterations: 299
currently lose_sum: 517.1342731118202
time_elpased: 1.726
start validation test
0.593556701031
1.0
0.192524321557
0.322885358523
0
validation finish
batch start
#iterations: 300
currently lose_sum: 515.5800258517265
time_elpased: 1.763
batch start
#iterations: 301
currently lose_sum: 515.4975962936878
time_elpased: 1.827
batch start
#iterations: 302
currently lose_sum: 516.8692807257175
time_elpased: 1.744
batch start
#iterations: 303
currently lose_sum: 515.2896684706211
time_elpased: 1.743
batch start
#iterations: 304
currently lose_sum: 513.5819354355335
time_elpased: 1.755
batch start
#iterations: 305
currently lose_sum: 514.7759765088558
time_elpased: 1.733
batch start
#iterations: 306
currently lose_sum: 515.8716548979282
time_elpased: 1.777
batch start
#iterations: 307
currently lose_sum: 513.9597352147102
time_elpased: 1.746
batch start
#iterations: 308
currently lose_sum: 515.9455234706402
time_elpased: 1.738
batch start
#iterations: 309
currently lose_sum: 515.2751207053661
time_elpased: 1.742
batch start
#iterations: 310
currently lose_sum: 517.3433064520359
time_elpased: 1.742
batch start
#iterations: 311
currently lose_sum: 514.5912929773331
time_elpased: 1.733
batch start
#iterations: 312
currently lose_sum: 512.8806927204132
time_elpased: 1.737
batch start
#iterations: 313
currently lose_sum: 515.1872422993183
time_elpased: 1.727
batch start
#iterations: 314
currently lose_sum: 517.1273504197598
time_elpased: 1.712
batch start
#iterations: 315
currently lose_sum: 515.3799113333225
time_elpased: 1.724
batch start
#iterations: 316
currently lose_sum: 515.0212962329388
time_elpased: 1.764
batch start
#iterations: 317
currently lose_sum: 515.1913042068481
time_elpased: 1.734
batch start
#iterations: 318
currently lose_sum: 513.6991676390171
time_elpased: 1.719
batch start
#iterations: 319
currently lose_sum: 514.0141113102436
time_elpased: 1.746
start validation test
0.594175257732
1.0
0.193753200205
0.324611821223
0
validation finish
batch start
#iterations: 320
currently lose_sum: 517.5947819948196
time_elpased: 1.73
batch start
#iterations: 321
currently lose_sum: 514.6080046594143
time_elpased: 1.742
batch start
#iterations: 322
currently lose_sum: 512.2525307238102
time_elpased: 1.729
batch start
#iterations: 323
currently lose_sum: 516.2796568274498
time_elpased: 1.734
batch start
#iterations: 324
currently lose_sum: 514.6715240478516
time_elpased: 1.733
batch start
#iterations: 325
currently lose_sum: 516.1518288850784
time_elpased: 1.7
batch start
#iterations: 326
currently lose_sum: 515.1522222161293
time_elpased: 1.72
batch start
#iterations: 327
currently lose_sum: 514.8210662603378
time_elpased: 1.714
batch start
#iterations: 328
currently lose_sum: 516.6897540688515
time_elpased: 1.728
batch start
#iterations: 329
currently lose_sum: 514.6587786972523
time_elpased: 1.723
batch start
#iterations: 330
currently lose_sum: 517.0636500418186
time_elpased: 1.726
batch start
#iterations: 331
currently lose_sum: 514.6222355663776
time_elpased: 1.753
batch start
#iterations: 332
currently lose_sum: 515.8287124633789
time_elpased: 1.794
batch start
#iterations: 333
currently lose_sum: 514.8021509945393
time_elpased: 1.812
batch start
#iterations: 334
currently lose_sum: 514.0650928914547
time_elpased: 1.742
batch start
#iterations: 335
currently lose_sum: 513.9821435213089
time_elpased: 1.795
batch start
#iterations: 336
currently lose_sum: 515.8551236391068
time_elpased: 1.754
batch start
#iterations: 337
currently lose_sum: 515.2990380823612
time_elpased: 1.747
batch start
#iterations: 338
currently lose_sum: 515.4282248616219
time_elpased: 1.733
batch start
#iterations: 339
currently lose_sum: 516.4410669803619
time_elpased: 1.752
start validation test
0.595927835052
0.999481327801
0.197337429595
0.329598905328
0
validation finish
batch start
#iterations: 340
currently lose_sum: 516.3805324733257
time_elpased: 1.748
batch start
#iterations: 341
currently lose_sum: 514.4749634563923
time_elpased: 1.807
batch start
#iterations: 342
currently lose_sum: 514.2809745371342
time_elpased: 1.781
batch start
#iterations: 343
currently lose_sum: 515.5129852294922
time_elpased: 1.749
batch start
#iterations: 344
currently lose_sum: 517.4792482852936
time_elpased: 1.766
batch start
#iterations: 345
currently lose_sum: 517.4282141327858
time_elpased: 1.787
batch start
#iterations: 346
currently lose_sum: 512.2938120663166
time_elpased: 1.797
batch start
#iterations: 347
currently lose_sum: 515.1026675999165
time_elpased: 1.796
batch start
#iterations: 348
currently lose_sum: 515.0548679530621
time_elpased: 1.789
batch start
#iterations: 349
currently lose_sum: 515.3630508482456
time_elpased: 1.732
batch start
#iterations: 350
currently lose_sum: 516.0305798947811
time_elpased: 1.794
batch start
#iterations: 351
currently lose_sum: 516.2432709932327
time_elpased: 1.769
batch start
#iterations: 352
currently lose_sum: 517.1209102272987
time_elpased: 1.762
batch start
#iterations: 353
currently lose_sum: 516.3569287061691
time_elpased: 1.841
batch start
#iterations: 354
currently lose_sum: 517.8346562981606
time_elpased: 1.784
batch start
#iterations: 355
currently lose_sum: 516.280691832304
time_elpased: 1.761
batch start
#iterations: 356
currently lose_sum: 514.5616295039654
time_elpased: 1.779
batch start
#iterations: 357
currently lose_sum: 514.6795068383217
time_elpased: 1.77
batch start
#iterations: 358
currently lose_sum: 514.1531569063663
time_elpased: 1.757
batch start
#iterations: 359
currently lose_sum: 515.3810601532459
time_elpased: 1.803
start validation test
0.594484536082
0.999473684211
0.194470046083
0.32558936991
0
validation finish
batch start
#iterations: 360
currently lose_sum: 515.8439383804798
time_elpased: 1.809
batch start
#iterations: 361
currently lose_sum: 516.3932999968529
time_elpased: 1.764
batch start
#iterations: 362
currently lose_sum: 515.3914828002453
time_elpased: 1.824
batch start
#iterations: 363
currently lose_sum: 514.5119041502476
time_elpased: 1.778
batch start
#iterations: 364
currently lose_sum: 516.0554339885712
time_elpased: 1.838
batch start
#iterations: 365
currently lose_sum: 514.0913035273552
time_elpased: 1.78
batch start
#iterations: 366
currently lose_sum: 515.8416755199432
time_elpased: 1.797
batch start
#iterations: 367
currently lose_sum: 518.1378837227821
time_elpased: 1.793
batch start
#iterations: 368
currently lose_sum: 514.5544981956482
time_elpased: 1.746
batch start
#iterations: 369
currently lose_sum: 514.649878770113
time_elpased: 1.73
batch start
#iterations: 370
currently lose_sum: 516.878280133009
time_elpased: 1.781
batch start
#iterations: 371
currently lose_sum: 513.1174193024635
time_elpased: 1.758
batch start
#iterations: 372
currently lose_sum: 517.0669512450695
time_elpased: 1.726
batch start
#iterations: 373
currently lose_sum: 517.5809719860554
time_elpased: 1.756
batch start
#iterations: 374
currently lose_sum: 514.2017138898373
time_elpased: 1.757
batch start
#iterations: 375
currently lose_sum: 516.460741430521
time_elpased: 1.758
batch start
#iterations: 376
currently lose_sum: 514.9782668948174
time_elpased: 1.747
batch start
#iterations: 377
currently lose_sum: 514.9885962605476
time_elpased: 1.738
batch start
#iterations: 378
currently lose_sum: 515.1053194701672
time_elpased: 1.788
batch start
#iterations: 379
currently lose_sum: 514.191874653101
time_elpased: 1.772
start validation test
0.596701030928
0.999485331961
0.198873527906
0.331738981893
0
validation finish
batch start
#iterations: 380
currently lose_sum: 515.3469198942184
time_elpased: 1.767
batch start
#iterations: 381
currently lose_sum: 514.5887446701527
time_elpased: 1.723
batch start
#iterations: 382
currently lose_sum: 512.925439029932
time_elpased: 1.811
batch start
#iterations: 383
currently lose_sum: 514.1290531754494
time_elpased: 1.754
batch start
#iterations: 384
currently lose_sum: 515.7493409216404
time_elpased: 1.719
batch start
#iterations: 385
currently lose_sum: 516.1933492422104
time_elpased: 1.744
batch start
#iterations: 386
currently lose_sum: 518.1990456283092
time_elpased: 1.747
batch start
#iterations: 387
currently lose_sum: 514.437590688467
time_elpased: 1.759
batch start
#iterations: 388
currently lose_sum: 514.8900274634361
time_elpased: 1.756
batch start
#iterations: 389
currently lose_sum: 515.336760699749
time_elpased: 1.757
batch start
#iterations: 390
currently lose_sum: 516.0530769228935
time_elpased: 1.764
batch start
#iterations: 391
currently lose_sum: 514.0080847144127
time_elpased: 1.733
batch start
#iterations: 392
currently lose_sum: 515.3874705731869
time_elpased: 1.759
batch start
#iterations: 393
currently lose_sum: 516.550154864788
time_elpased: 1.736
batch start
#iterations: 394
currently lose_sum: 514.6557568907738
time_elpased: 1.738
batch start
#iterations: 395
currently lose_sum: 516.1234535574913
time_elpased: 1.751
batch start
#iterations: 396
currently lose_sum: 514.4143556654453
time_elpased: 1.76
batch start
#iterations: 397
currently lose_sum: 516.1335716247559
time_elpased: 1.741
batch start
#iterations: 398
currently lose_sum: 514.06641715765
time_elpased: 1.748
batch start
#iterations: 399
currently lose_sum: 516.6051371991634
time_elpased: 1.749
start validation test
0.594845360825
0.999475616151
0.195186891961
0.326593557231
0
validation finish
acc: 0.613
pre: 0.996
rec: 0.224
F1: 0.365
auc: 0.000
