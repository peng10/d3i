start to construct graph
graph construct over
87453
epochs start
batch start
#iterations: 0
currently lose_sum: 544.9294408559799
time_elpased: 2.65
batch start
#iterations: 1
currently lose_sum: 530.640938937664
time_elpased: 2.634
batch start
#iterations: 2
currently lose_sum: 523.3050675690174
time_elpased: 2.651
batch start
#iterations: 3
currently lose_sum: 520.300178527832
time_elpased: 2.636
batch start
#iterations: 4
currently lose_sum: 514.9856793284416
time_elpased: 2.623
batch start
#iterations: 5
currently lose_sum: 514.1994662284851
time_elpased: 2.649
batch start
#iterations: 6
currently lose_sum: 511.95315432548523
time_elpased: 2.645
batch start
#iterations: 7
currently lose_sum: 509.07072219252586
time_elpased: 2.632
batch start
#iterations: 8
currently lose_sum: 510.2312034070492
time_elpased: 2.668
batch start
#iterations: 9
currently lose_sum: 507.61019733548164
time_elpased: 2.66
batch start
#iterations: 10
currently lose_sum: 506.7698780596256
time_elpased: 2.657
batch start
#iterations: 11
currently lose_sum: 507.85758224129677
time_elpased: 2.669
batch start
#iterations: 12
currently lose_sum: 507.10224866867065
time_elpased: 2.648
batch start
#iterations: 13
currently lose_sum: 504.3939871788025
time_elpased: 2.643
batch start
#iterations: 14
currently lose_sum: 505.6484824717045
time_elpased: 2.672
batch start
#iterations: 15
currently lose_sum: 504.25185638666153
time_elpased: 2.66
batch start
#iterations: 16
currently lose_sum: 506.52347099781036
time_elpased: 2.665
batch start
#iterations: 17
currently lose_sum: 503.7099425494671
time_elpased: 2.658
batch start
#iterations: 18
currently lose_sum: 503.68341705203056
time_elpased: 2.654
batch start
#iterations: 19
currently lose_sum: 502.7710320353508
time_elpased: 2.644
start validation test
0.588865979381
0.902626811594
0.204326430182
0.333221869253
0
validation finish
batch start
#iterations: 20
currently lose_sum: 504.6293214857578
time_elpased: 2.656
batch start
#iterations: 21
currently lose_sum: 503.60730680823326
time_elpased: 2.642
batch start
#iterations: 22
currently lose_sum: 503.16564506292343
time_elpased: 2.638
batch start
#iterations: 23
currently lose_sum: 499.8602413535118
time_elpased: 2.656
batch start
#iterations: 24
currently lose_sum: 502.751308709383
time_elpased: 2.639
batch start
#iterations: 25
currently lose_sum: 500.5677432715893
time_elpased: 2.628
batch start
#iterations: 26
currently lose_sum: 499.35683768987656
time_elpased: 2.651
batch start
#iterations: 27
currently lose_sum: 502.3069752752781
time_elpased: 2.648
batch start
#iterations: 28
currently lose_sum: 500.4647531211376
time_elpased: 2.637
batch start
#iterations: 29
currently lose_sum: 497.92822816967964
time_elpased: 2.658
batch start
#iterations: 30
currently lose_sum: 498.85182493925095
time_elpased: 2.657
batch start
#iterations: 31
currently lose_sum: 500.84170135855675
time_elpased: 2.645
batch start
#iterations: 32
currently lose_sum: 500.29410967230797
time_elpased: 2.675
batch start
#iterations: 33
currently lose_sum: 500.70136657357216
time_elpased: 2.655
batch start
#iterations: 34
currently lose_sum: 500.48861196637154
time_elpased: 2.633
batch start
#iterations: 35
currently lose_sum: 500.1140852868557
time_elpased: 2.633
batch start
#iterations: 36
currently lose_sum: 500.1549842059612
time_elpased: 2.64
batch start
#iterations: 37
currently lose_sum: 496.71383333206177
time_elpased: 2.635
batch start
#iterations: 38
currently lose_sum: 500.6237487792969
time_elpased: 2.648
batch start
#iterations: 39
currently lose_sum: 498.5367647111416
time_elpased: 2.634
start validation test
0.584072164948
0.876620473849
0.201045724831
0.327078642315
0
validation finish
batch start
#iterations: 40
currently lose_sum: 498.77713826298714
time_elpased: 2.625
batch start
#iterations: 41
currently lose_sum: 499.63961082696915
time_elpased: 2.64
batch start
#iterations: 42
currently lose_sum: 500.7777951359749
time_elpased: 2.645
batch start
#iterations: 43
currently lose_sum: 500.48272812366486
time_elpased: 2.639
batch start
#iterations: 44
currently lose_sum: 498.03836727142334
time_elpased: 2.654
batch start
#iterations: 45
currently lose_sum: 498.5858664512634
time_elpased: 2.654
batch start
#iterations: 46
currently lose_sum: 502.02191185951233
time_elpased: 2.636
batch start
#iterations: 47
currently lose_sum: 498.9964416027069
time_elpased: 2.661
batch start
#iterations: 48
currently lose_sum: 496.17204573750496
time_elpased: 2.654
batch start
#iterations: 49
currently lose_sum: 497.0815227329731
time_elpased: 2.639
batch start
#iterations: 50
currently lose_sum: 494.9033225774765
time_elpased: 2.647
batch start
#iterations: 51
currently lose_sum: 496.6161479949951
time_elpased: 2.629
batch start
#iterations: 52
currently lose_sum: 499.5510429441929
time_elpased: 2.637
batch start
#iterations: 53
currently lose_sum: 498.3520040214062
time_elpased: 2.652
batch start
#iterations: 54
currently lose_sum: 497.5208440423012
time_elpased: 2.638
batch start
#iterations: 55
currently lose_sum: 496.5581855773926
time_elpased: 2.635
batch start
#iterations: 56
currently lose_sum: 497.24101027846336
time_elpased: 2.654
batch start
#iterations: 57
currently lose_sum: 497.9155941605568
time_elpased: 2.651
batch start
#iterations: 58
currently lose_sum: 496.3916721045971
time_elpased: 2.636
batch start
#iterations: 59
currently lose_sum: 496.0258475244045
time_elpased: 2.655
start validation test
0.578092783505
0.910088865656
0.178490875538
0.298448615754
0
validation finish
batch start
#iterations: 60
currently lose_sum: 498.0499886870384
time_elpased: 2.658
batch start
#iterations: 61
currently lose_sum: 494.66360530257225
time_elpased: 2.647
batch start
#iterations: 62
currently lose_sum: 497.3379430472851
time_elpased: 2.657
batch start
#iterations: 63
currently lose_sum: 497.6875596344471
time_elpased: 2.655
batch start
#iterations: 64
currently lose_sum: 494.50605660676956
time_elpased: 2.637
batch start
#iterations: 65
currently lose_sum: 496.4133251905441
time_elpased: 2.656
batch start
#iterations: 66
currently lose_sum: 497.56898441910744
time_elpased: 2.648
batch start
#iterations: 67
currently lose_sum: 494.6070463657379
time_elpased: 2.638
batch start
#iterations: 68
currently lose_sum: 495.30064553022385
time_elpased: 2.65
batch start
#iterations: 69
currently lose_sum: 496.73403164744377
time_elpased: 2.65
batch start
#iterations: 70
currently lose_sum: 497.3195413649082
time_elpased: 2.637
batch start
#iterations: 71
currently lose_sum: 498.45829743146896
time_elpased: 2.648
batch start
#iterations: 72
currently lose_sum: 495.51886039972305
time_elpased: 2.63
batch start
#iterations: 73
currently lose_sum: 493.4900404512882
time_elpased: 2.625
batch start
#iterations: 74
currently lose_sum: 494.2194355428219
time_elpased: 2.642
batch start
#iterations: 75
currently lose_sum: 495.5071555376053
time_elpased: 2.64
batch start
#iterations: 76
currently lose_sum: 496.1945731341839
time_elpased: 2.632
batch start
#iterations: 77
currently lose_sum: 495.5023847222328
time_elpased: 2.637
batch start
#iterations: 78
currently lose_sum: 495.61775264143944
time_elpased: 2.655
batch start
#iterations: 79
currently lose_sum: 494.9505380690098
time_elpased: 2.636
start validation test
0.617113402062
0.895578231293
0.269940537216
0.414841657476
0
validation finish
batch start
#iterations: 80
currently lose_sum: 496.6282038092613
time_elpased: 2.648
batch start
#iterations: 81
currently lose_sum: 496.075047314167
time_elpased: 2.631
batch start
#iterations: 82
currently lose_sum: 494.74710193276405
time_elpased: 2.627
batch start
#iterations: 83
currently lose_sum: 494.736322671175
time_elpased: 2.651
batch start
#iterations: 84
currently lose_sum: 495.5393907427788
time_elpased: 2.637
batch start
#iterations: 85
currently lose_sum: 495.13855442404747
time_elpased: 2.63
batch start
#iterations: 86
currently lose_sum: 495.64093416929245
time_elpased: 2.643
batch start
#iterations: 87
currently lose_sum: 494.5746725797653
time_elpased: 2.636
batch start
#iterations: 88
currently lose_sum: 496.00499898195267
time_elpased: 2.638
batch start
#iterations: 89
currently lose_sum: 494.1446335911751
time_elpased: 2.648
batch start
#iterations: 90
currently lose_sum: 494.93235728144646
time_elpased: 2.645
batch start
#iterations: 91
currently lose_sum: 494.8897243440151
time_elpased: 2.644
batch start
#iterations: 92
currently lose_sum: 493.53966718912125
time_elpased: 2.649
batch start
#iterations: 93
currently lose_sum: 495.36988121271133
time_elpased: 2.649
batch start
#iterations: 94
currently lose_sum: 494.19507971405983
time_elpased: 2.652
batch start
#iterations: 95
currently lose_sum: 495.176571726799
time_elpased: 2.642
batch start
#iterations: 96
currently lose_sum: 493.24312075972557
time_elpased: 2.642
batch start
#iterations: 97
currently lose_sum: 494.38937214016914
time_elpased: 2.652
batch start
#iterations: 98
currently lose_sum: 491.83604526519775
time_elpased: 2.642
batch start
#iterations: 99
currently lose_sum: 496.6684547662735
time_elpased: 2.656
start validation test
0.589587628866
0.896811337467
0.207607135534
0.337162837163
0
validation finish
batch start
#iterations: 100
currently lose_sum: 494.85637348890305
time_elpased: 2.645
batch start
#iterations: 101
currently lose_sum: 492.43889832496643
time_elpased: 2.661
batch start
#iterations: 102
currently lose_sum: 496.84310588240623
time_elpased: 2.671
batch start
#iterations: 103
currently lose_sum: 493.383890748024
time_elpased: 2.678
batch start
#iterations: 104
currently lose_sum: 492.70225155353546
time_elpased: 2.669
batch start
#iterations: 105
currently lose_sum: 492.3003752231598
time_elpased: 2.657
batch start
#iterations: 106
currently lose_sum: 494.3287664949894
time_elpased: 2.654
batch start
#iterations: 107
currently lose_sum: 494.1942145228386
time_elpased: 2.67
batch start
#iterations: 108
currently lose_sum: 492.46608301997185
time_elpased: 2.652
batch start
#iterations: 109
currently lose_sum: 496.5062708258629
time_elpased: 2.652
batch start
#iterations: 110
currently lose_sum: 494.32398653030396
time_elpased: 2.678
batch start
#iterations: 111
currently lose_sum: 495.53953725099564
time_elpased: 2.663
batch start
#iterations: 112
currently lose_sum: 494.04861319065094
time_elpased: 2.637
batch start
#iterations: 113
currently lose_sum: 494.5783260166645
time_elpased: 2.656
batch start
#iterations: 114
currently lose_sum: 494.31862074136734
time_elpased: 2.65
batch start
#iterations: 115
currently lose_sum: 491.5270081460476
time_elpased: 2.644
batch start
#iterations: 116
currently lose_sum: 495.7048256099224
time_elpased: 2.652
batch start
#iterations: 117
currently lose_sum: 492.8635908961296
time_elpased: 2.653
batch start
#iterations: 118
currently lose_sum: 494.1210357248783
time_elpased: 2.657
batch start
#iterations: 119
currently lose_sum: 491.4949878156185
time_elpased: 2.671
start validation test
0.572989690722
0.906077348066
0.168136149272
0.283638879281
0
validation finish
batch start
#iterations: 120
currently lose_sum: 494.92557486891747
time_elpased: 2.672
batch start
#iterations: 121
currently lose_sum: 494.29721516370773
time_elpased: 2.655
batch start
#iterations: 122
currently lose_sum: 495.9021266102791
time_elpased: 2.663
batch start
#iterations: 123
currently lose_sum: 494.20825040340424
time_elpased: 2.66
batch start
#iterations: 124
currently lose_sum: 493.3296692073345
time_elpased: 2.659
batch start
#iterations: 125
currently lose_sum: 492.17972135543823
time_elpased: 2.66
batch start
#iterations: 126
currently lose_sum: 491.8897182047367
time_elpased: 2.656
batch start
#iterations: 127
currently lose_sum: 493.6839155256748
time_elpased: 2.629
batch start
#iterations: 128
currently lose_sum: 494.0413243174553
time_elpased: 2.654
batch start
#iterations: 129
currently lose_sum: 491.98091572523117
time_elpased: 2.668
batch start
#iterations: 130
currently lose_sum: 493.8970271348953
time_elpased: 2.629
batch start
#iterations: 131
currently lose_sum: 494.3862712085247
time_elpased: 2.656
batch start
#iterations: 132
currently lose_sum: 493.2264195084572
time_elpased: 2.648
batch start
#iterations: 133
currently lose_sum: 493.93120914697647
time_elpased: 2.65
batch start
#iterations: 134
currently lose_sum: 493.26190638542175
time_elpased: 2.658
batch start
#iterations: 135
currently lose_sum: 493.36783069372177
time_elpased: 2.666
batch start
#iterations: 136
currently lose_sum: 493.55544647574425
time_elpased: 2.652
batch start
#iterations: 137
currently lose_sum: 491.54516926407814
time_elpased: 2.673
batch start
#iterations: 138
currently lose_sum: 494.3290514051914
time_elpased: 2.67
batch start
#iterations: 139
currently lose_sum: 494.6283350586891
time_elpased: 2.658
start validation test
0.612113402062
0.894513274336
0.259073200738
0.401780745687
0
validation finish
batch start
#iterations: 140
currently lose_sum: 492.1142058670521
time_elpased: 2.654
batch start
#iterations: 141
currently lose_sum: 492.3961314857006
time_elpased: 2.665
batch start
#iterations: 142
currently lose_sum: 494.05604910850525
time_elpased: 2.674
batch start
#iterations: 143
currently lose_sum: 492.397586196661
time_elpased: 2.665
batch start
#iterations: 144
currently lose_sum: 493.61027678847313
time_elpased: 2.656
batch start
#iterations: 145
currently lose_sum: 494.03169399499893
time_elpased: 2.646
batch start
#iterations: 146
currently lose_sum: 493.25968688726425
time_elpased: 2.678
batch start
#iterations: 147
currently lose_sum: 491.1069395840168
time_elpased: 2.68
batch start
#iterations: 148
currently lose_sum: 491.7079913318157
time_elpased: 2.657
batch start
#iterations: 149
currently lose_sum: 492.4554546177387
time_elpased: 2.674
batch start
#iterations: 150
currently lose_sum: 493.3416814804077
time_elpased: 2.688
batch start
#iterations: 151
currently lose_sum: 491.46085864305496
time_elpased: 2.663
batch start
#iterations: 152
currently lose_sum: 491.6662550866604
time_elpased: 2.677
batch start
#iterations: 153
currently lose_sum: 490.76830050349236
time_elpased: 2.661
batch start
#iterations: 154
currently lose_sum: 493.16431003808975
time_elpased: 2.665
batch start
#iterations: 155
currently lose_sum: 493.3760145306587
time_elpased: 2.66
batch start
#iterations: 156
currently lose_sum: 492.5892505645752
time_elpased: 2.667
batch start
#iterations: 157
currently lose_sum: 491.2054927647114
time_elpased: 2.647
batch start
#iterations: 158
currently lose_sum: 492.25508522987366
time_elpased: 2.677
batch start
#iterations: 159
currently lose_sum: 492.39918354153633
time_elpased: 2.663
start validation test
0.582268041237
0.891366223909
0.192638917367
0.316809981453
0
validation finish
batch start
#iterations: 160
currently lose_sum: 493.3365079462528
time_elpased: 2.655
batch start
#iterations: 161
currently lose_sum: 493.46425956487656
time_elpased: 2.675
batch start
#iterations: 162
currently lose_sum: 491.1497143805027
time_elpased: 2.673
batch start
#iterations: 163
currently lose_sum: 492.8023071885109
time_elpased: 2.675
batch start
#iterations: 164
currently lose_sum: 493.25036868453026
time_elpased: 2.676
batch start
#iterations: 165
currently lose_sum: 494.19237491488457
time_elpased: 2.686
batch start
#iterations: 166
currently lose_sum: 492.173956155777
time_elpased: 2.671
batch start
#iterations: 167
currently lose_sum: 492.8405242264271
time_elpased: 2.65
batch start
#iterations: 168
currently lose_sum: 491.6693180501461
time_elpased: 2.673
batch start
#iterations: 169
currently lose_sum: 490.49708864092827
time_elpased: 2.666
batch start
#iterations: 170
currently lose_sum: 493.08410266041756
time_elpased: 2.661
batch start
#iterations: 171
currently lose_sum: 492.9361741244793
time_elpased: 2.659
batch start
#iterations: 172
currently lose_sum: 492.7233344912529
time_elpased: 2.671
batch start
#iterations: 173
currently lose_sum: 491.72491267323494
time_elpased: 2.667
batch start
#iterations: 174
currently lose_sum: 493.72921273112297
time_elpased: 2.667
batch start
#iterations: 175
currently lose_sum: 493.1995414197445
time_elpased: 2.667
batch start
#iterations: 176
currently lose_sum: 493.5657579898834
time_elpased: 2.662
batch start
#iterations: 177
currently lose_sum: 492.51325330138206
time_elpased: 2.666
batch start
#iterations: 178
currently lose_sum: 492.24594590067863
time_elpased: 2.666
batch start
#iterations: 179
currently lose_sum: 493.4866074323654
time_elpased: 2.658
start validation test
0.600360824742
0.890973036342
0.233750256305
0.37034029075
0
validation finish
batch start
#iterations: 180
currently lose_sum: 492.4927134811878
time_elpased: 2.64
batch start
#iterations: 181
currently lose_sum: 493.7995513677597
time_elpased: 2.635
batch start
#iterations: 182
currently lose_sum: 491.7657095193863
time_elpased: 2.63
batch start
#iterations: 183
currently lose_sum: 493.23256236314774
time_elpased: 2.638
batch start
#iterations: 184
currently lose_sum: 491.63911083340645
time_elpased: 2.648
batch start
#iterations: 185
currently lose_sum: 490.76640328764915
time_elpased: 2.641
batch start
#iterations: 186
currently lose_sum: 491.13496404886246
time_elpased: 2.665
batch start
#iterations: 187
currently lose_sum: 494.5006077885628
time_elpased: 2.654
batch start
#iterations: 188
currently lose_sum: 491.25973999500275
time_elpased: 2.652
batch start
#iterations: 189
currently lose_sum: 492.2982425689697
time_elpased: 2.671
batch start
#iterations: 190
currently lose_sum: 491.984786182642
time_elpased: 2.7
batch start
#iterations: 191
currently lose_sum: 490.3205148279667
time_elpased: 2.667
batch start
#iterations: 192
currently lose_sum: 491.90011498332024
time_elpased: 2.658
batch start
#iterations: 193
currently lose_sum: 491.8502393066883
time_elpased: 2.64
batch start
#iterations: 194
currently lose_sum: 493.9281362593174
time_elpased: 2.639
batch start
#iterations: 195
currently lose_sum: 490.6289239823818
time_elpased: 2.653
batch start
#iterations: 196
currently lose_sum: 492.34875550866127
time_elpased: 2.636
batch start
#iterations: 197
currently lose_sum: 491.44836416840553
time_elpased: 2.626
batch start
#iterations: 198
currently lose_sum: 492.8725580275059
time_elpased: 2.652
batch start
#iterations: 199
currently lose_sum: 493.405459433794
time_elpased: 2.651
start validation test
0.596288659794
0.895148026316
0.223190485954
0.357295256852
0
validation finish
batch start
#iterations: 200
currently lose_sum: 487.0982559621334
time_elpased: 2.636
batch start
#iterations: 201
currently lose_sum: 492.3435142338276
time_elpased: 2.662
batch start
#iterations: 202
currently lose_sum: 492.1045447587967
time_elpased: 2.657
batch start
#iterations: 203
currently lose_sum: 491.79361736774445
time_elpased: 2.654
batch start
#iterations: 204
currently lose_sum: 492.9535731077194
time_elpased: 2.663
batch start
#iterations: 205
currently lose_sum: 491.13945588469505
time_elpased: 2.655
batch start
#iterations: 206
currently lose_sum: 489.27565854787827
time_elpased: 2.666
batch start
#iterations: 207
currently lose_sum: 490.83117374777794
time_elpased: 2.663
batch start
#iterations: 208
currently lose_sum: 492.8531704246998
time_elpased: 2.655
batch start
#iterations: 209
currently lose_sum: 492.25468519330025
time_elpased: 2.667
batch start
#iterations: 210
currently lose_sum: 491.6963320672512
time_elpased: 2.677
batch start
#iterations: 211
currently lose_sum: 491.5504486858845
time_elpased: 2.652
batch start
#iterations: 212
currently lose_sum: 490.3989024758339
time_elpased: 2.627
batch start
#iterations: 213
currently lose_sum: 491.9529213607311
time_elpased: 2.655
batch start
#iterations: 214
currently lose_sum: 491.1516873538494
time_elpased: 2.642
batch start
#iterations: 215
currently lose_sum: 492.01611828804016
time_elpased: 2.639
batch start
#iterations: 216
currently lose_sum: 492.717486590147
time_elpased: 2.665
batch start
#iterations: 217
currently lose_sum: 490.9382976591587
time_elpased: 2.664
batch start
#iterations: 218
currently lose_sum: 492.16204008460045
time_elpased: 2.651
batch start
#iterations: 219
currently lose_sum: 493.83582615852356
time_elpased: 2.658
start validation test
0.582268041237
0.892483349191
0.192331351241
0.316464237517
0
validation finish
batch start
#iterations: 220
currently lose_sum: 490.42956551909447
time_elpased: 2.655
batch start
#iterations: 221
currently lose_sum: 492.6178533434868
time_elpased: 2.662
batch start
#iterations: 222
currently lose_sum: 491.1971275806427
time_elpased: 2.663
batch start
#iterations: 223
currently lose_sum: 492.18856516480446
time_elpased: 2.653
batch start
#iterations: 224
currently lose_sum: 492.8567191064358
time_elpased: 2.667
batch start
#iterations: 225
currently lose_sum: 491.75885862112045
time_elpased: 2.677
batch start
#iterations: 226
currently lose_sum: 491.12142956256866
time_elpased: 2.664
batch start
#iterations: 227
currently lose_sum: 492.28794249892235
time_elpased: 2.65
batch start
#iterations: 228
currently lose_sum: 489.87004286050797
time_elpased: 2.666
batch start
#iterations: 229
currently lose_sum: 490.0888883471489
time_elpased: 2.645
batch start
#iterations: 230
currently lose_sum: 490.52597257494926
time_elpased: 2.656
batch start
#iterations: 231
currently lose_sum: 491.1247628927231
time_elpased: 2.652
batch start
#iterations: 232
currently lose_sum: 492.59731462597847
time_elpased: 2.668
batch start
#iterations: 233
currently lose_sum: 490.65356066823006
time_elpased: 2.647
batch start
#iterations: 234
currently lose_sum: 492.9890629053116
time_elpased: 2.674
batch start
#iterations: 235
currently lose_sum: 489.07826551795006
time_elpased: 2.668
batch start
#iterations: 236
currently lose_sum: 486.9086689054966
time_elpased: 2.65
batch start
#iterations: 237
currently lose_sum: 488.57406306266785
time_elpased: 2.665
batch start
#iterations: 238
currently lose_sum: 487.0897890627384
time_elpased: 2.667
batch start
#iterations: 239
currently lose_sum: 491.64948442578316
time_elpased: 2.656
start validation test
0.620773195876
0.876295133438
0.286139019889
0.431408918773
0
validation finish
batch start
#iterations: 240
currently lose_sum: 490.0199311077595
time_elpased: 2.665
batch start
#iterations: 241
currently lose_sum: 492.03802132606506
time_elpased: 2.663
batch start
#iterations: 242
currently lose_sum: 490.862532556057
time_elpased: 2.641
batch start
#iterations: 243
currently lose_sum: 489.9980817735195
time_elpased: 2.652
batch start
#iterations: 244
currently lose_sum: 491.6103313267231
time_elpased: 2.667
batch start
#iterations: 245
currently lose_sum: 490.0214334130287
time_elpased: 2.641
batch start
#iterations: 246
currently lose_sum: 492.3426086306572
time_elpased: 2.661
batch start
#iterations: 247
currently lose_sum: 494.1277194619179
time_elpased: 2.682
batch start
#iterations: 248
currently lose_sum: 490.62795212864876
time_elpased: 2.661
batch start
#iterations: 249
currently lose_sum: 491.1975291669369
time_elpased: 2.66
batch start
#iterations: 250
currently lose_sum: 492.29265984892845
time_elpased: 2.667
batch start
#iterations: 251
currently lose_sum: 489.9277366697788
time_elpased: 2.653
batch start
#iterations: 252
currently lose_sum: 488.79630985856056
time_elpased: 2.685
batch start
#iterations: 253
currently lose_sum: 488.65909495949745
time_elpased: 2.661
batch start
#iterations: 254
currently lose_sum: 489.2007824778557
time_elpased: 2.666
batch start
#iterations: 255
currently lose_sum: 489.210631608963
time_elpased: 2.655
batch start
#iterations: 256
currently lose_sum: 491.04838874936104
time_elpased: 2.658
batch start
#iterations: 257
currently lose_sum: 489.3280000090599
time_elpased: 2.647
batch start
#iterations: 258
currently lose_sum: 488.2198737859726
time_elpased: 2.661
batch start
#iterations: 259
currently lose_sum: 493.3647338449955
time_elpased: 2.667
start validation test
0.600670103093
0.886407393146
0.236005741234
0.372763339001
0
validation finish
batch start
#iterations: 260
currently lose_sum: 487.85187324881554
time_elpased: 2.667
batch start
#iterations: 261
currently lose_sum: 490.91677045822144
time_elpased: 2.647
batch start
#iterations: 262
currently lose_sum: 492.3785763978958
time_elpased: 2.667
batch start
#iterations: 263
currently lose_sum: 491.8173298537731
time_elpased: 2.675
batch start
#iterations: 264
currently lose_sum: 493.56319880485535
time_elpased: 2.681
batch start
#iterations: 265
currently lose_sum: 490.6097156703472
time_elpased: 2.668
batch start
#iterations: 266
currently lose_sum: 490.32227250933647
time_elpased: 2.661
batch start
#iterations: 267
currently lose_sum: 491.41425743699074
time_elpased: 2.661
batch start
#iterations: 268
currently lose_sum: 488.7074701189995
time_elpased: 2.673
batch start
#iterations: 269
currently lose_sum: 491.619524538517
time_elpased: 2.661
batch start
#iterations: 270
currently lose_sum: 492.5193178355694
time_elpased: 2.661
batch start
#iterations: 271
currently lose_sum: 491.13421055674553
time_elpased: 2.678
batch start
#iterations: 272
currently lose_sum: 491.23493722081184
time_elpased: 2.663
batch start
#iterations: 273
currently lose_sum: 489.9595210850239
time_elpased: 2.661
batch start
#iterations: 274
currently lose_sum: 491.6742354631424
time_elpased: 2.667
batch start
#iterations: 275
currently lose_sum: 489.2762054800987
time_elpased: 2.661
batch start
#iterations: 276
currently lose_sum: 493.03286507725716
time_elpased: 2.652
batch start
#iterations: 277
currently lose_sum: 492.5233665108681
time_elpased: 2.664
batch start
#iterations: 278
currently lose_sum: 490.5961372256279
time_elpased: 2.657
batch start
#iterations: 279
currently lose_sum: 489.50755059719086
time_elpased: 2.648
start validation test
0.607113402062
0.87962962963
0.253229444331
0.393249482566
0
validation finish
batch start
#iterations: 280
currently lose_sum: 491.4938461482525
time_elpased: 2.663
batch start
#iterations: 281
currently lose_sum: 489.29288616776466
time_elpased: 2.651
batch start
#iterations: 282
currently lose_sum: 489.6172932982445
time_elpased: 2.646
batch start
#iterations: 283
currently lose_sum: 491.1575625538826
time_elpased: 2.652
batch start
#iterations: 284
currently lose_sum: 490.4376911818981
time_elpased: 2.663
batch start
#iterations: 285
currently lose_sum: 489.57224974036217
time_elpased: 2.632
batch start
#iterations: 286
currently lose_sum: 489.33141154050827
time_elpased: 2.651
batch start
#iterations: 287
currently lose_sum: 490.46240171790123
time_elpased: 2.633
batch start
#iterations: 288
currently lose_sum: 490.2657581567764
time_elpased: 2.626
batch start
#iterations: 289
currently lose_sum: 492.8997136950493
time_elpased: 2.648
batch start
#iterations: 290
currently lose_sum: 490.8951978087425
time_elpased: 2.663
batch start
#iterations: 291
currently lose_sum: 488.40737971663475
time_elpased: 2.641
batch start
#iterations: 292
currently lose_sum: 489.66341841220856
time_elpased: 2.658
batch start
#iterations: 293
currently lose_sum: 493.04626590013504
time_elpased: 2.652
batch start
#iterations: 294
currently lose_sum: 488.9590517282486
time_elpased: 2.657
batch start
#iterations: 295
currently lose_sum: 491.3494146168232
time_elpased: 2.668
batch start
#iterations: 296
currently lose_sum: 489.9116135239601
time_elpased: 2.647
batch start
#iterations: 297
currently lose_sum: 489.3054954111576
time_elpased: 2.659
batch start
#iterations: 298
currently lose_sum: 490.76300647854805
time_elpased: 2.661
batch start
#iterations: 299
currently lose_sum: 490.7428817451
time_elpased: 2.654
start validation test
0.596597938144
0.881027667984
0.228521632151
0.362911103875
0
validation finish
batch start
#iterations: 300
currently lose_sum: 490.03303545713425
time_elpased: 2.644
batch start
#iterations: 301
currently lose_sum: 490.78183525800705
time_elpased: 2.671
batch start
#iterations: 302
currently lose_sum: 490.4241901934147
time_elpased: 2.637
batch start
#iterations: 303
currently lose_sum: 492.210905790329
time_elpased: 2.648
batch start
#iterations: 304
currently lose_sum: 489.98068937659264
time_elpased: 2.663
batch start
#iterations: 305
currently lose_sum: 491.92281064391136
time_elpased: 2.658
batch start
#iterations: 306
currently lose_sum: 489.68061527609825
time_elpased: 2.649
batch start
#iterations: 307
currently lose_sum: 488.749603331089
time_elpased: 2.67
batch start
#iterations: 308
currently lose_sum: 490.4587844312191
time_elpased: 2.658
batch start
#iterations: 309
currently lose_sum: 490.16542062163353
time_elpased: 2.671
batch start
#iterations: 310
currently lose_sum: 490.6783242225647
time_elpased: 2.664
batch start
#iterations: 311
currently lose_sum: 490.60311010479927
time_elpased: 2.67
batch start
#iterations: 312
currently lose_sum: 489.4211597144604
time_elpased: 2.658
batch start
#iterations: 313
currently lose_sum: 490.9207875728607
time_elpased: 2.667
batch start
#iterations: 314
currently lose_sum: 491.2644236087799
time_elpased: 2.67
batch start
#iterations: 315
currently lose_sum: 492.05915439128876
time_elpased: 2.657
batch start
#iterations: 316
currently lose_sum: 491.7590517401695
time_elpased: 2.668
batch start
#iterations: 317
currently lose_sum: 489.6666709780693
time_elpased: 2.664
batch start
#iterations: 318
currently lose_sum: 487.79599821567535
time_elpased: 2.648
batch start
#iterations: 319
currently lose_sum: 488.95358124375343
time_elpased: 2.667
start validation test
0.591288659794
0.882279011311
0.215911420956
0.346923647146
0
validation finish
batch start
#iterations: 320
currently lose_sum: 492.10682955384254
time_elpased: 2.645
batch start
#iterations: 321
currently lose_sum: 488.10800218582153
time_elpased: 2.63
batch start
#iterations: 322
currently lose_sum: 489.51709219813347
time_elpased: 2.654
batch start
#iterations: 323
currently lose_sum: 490.1088998913765
time_elpased: 2.648
batch start
#iterations: 324
currently lose_sum: 487.6323430240154
time_elpased: 2.652
batch start
#iterations: 325
currently lose_sum: 492.1733422279358
time_elpased: 2.662
batch start
#iterations: 326
currently lose_sum: 489.86129173636436
time_elpased: 2.656
batch start
#iterations: 327
currently lose_sum: 490.97698482871056
time_elpased: 2.659
batch start
#iterations: 328
currently lose_sum: 489.88890209794044
time_elpased: 2.673
batch start
#iterations: 329
currently lose_sum: 488.25749054551125
time_elpased: 2.66
batch start
#iterations: 330
currently lose_sum: 491.0092599093914
time_elpased: 2.643
batch start
#iterations: 331
currently lose_sum: 489.6279591023922
time_elpased: 2.666
batch start
#iterations: 332
currently lose_sum: 489.0770660042763
time_elpased: 2.652
batch start
#iterations: 333
currently lose_sum: 489.74566543102264
time_elpased: 2.648
batch start
#iterations: 334
currently lose_sum: 489.46271631121635
time_elpased: 2.671
batch start
#iterations: 335
currently lose_sum: 489.03804966807365
time_elpased: 2.663
batch start
#iterations: 336
currently lose_sum: 487.9116169810295
time_elpased: 2.653
batch start
#iterations: 337
currently lose_sum: 491.39728927612305
time_elpased: 2.661
batch start
#iterations: 338
currently lose_sum: 492.0757456421852
time_elpased: 2.673
batch start
#iterations: 339
currently lose_sum: 490.1264946758747
time_elpased: 2.643
start validation test
0.591391752577
0.879833679834
0.216936641378
0.348054938729
0
validation finish
batch start
#iterations: 340
currently lose_sum: 489.92751118540764
time_elpased: 2.663
batch start
#iterations: 341
currently lose_sum: 491.2271139025688
time_elpased: 2.667
batch start
#iterations: 342
currently lose_sum: 490.16267961263657
time_elpased: 2.653
batch start
#iterations: 343
currently lose_sum: 490.84490832686424
time_elpased: 2.679
batch start
#iterations: 344
currently lose_sum: 491.0158591866493
time_elpased: 2.713
batch start
#iterations: 345
currently lose_sum: 491.5180866420269
time_elpased: 2.676
batch start
#iterations: 346
currently lose_sum: 487.6815776228905
time_elpased: 2.665
batch start
#iterations: 347
currently lose_sum: 490.42092654109
time_elpased: 2.633
batch start
#iterations: 348
currently lose_sum: 491.53238674998283
time_elpased: 2.601
batch start
#iterations: 349
currently lose_sum: 488.6588185727596
time_elpased: 2.644
batch start
#iterations: 350
currently lose_sum: 488.5863155424595
time_elpased: 2.644
batch start
#iterations: 351
currently lose_sum: 490.68848928809166
time_elpased: 2.641
batch start
#iterations: 352
currently lose_sum: 491.65991672873497
time_elpased: 2.667
batch start
#iterations: 353
currently lose_sum: 489.11648032069206
time_elpased: 2.667
batch start
#iterations: 354
currently lose_sum: 491.1573143899441
time_elpased: 2.664
batch start
#iterations: 355
currently lose_sum: 490.8415792286396
time_elpased: 2.678
batch start
#iterations: 356
currently lose_sum: 491.41492787003517
time_elpased: 2.666
batch start
#iterations: 357
currently lose_sum: 489.04235088825226
time_elpased: 2.653
batch start
#iterations: 358
currently lose_sum: 490.50724548101425
time_elpased: 2.661
batch start
#iterations: 359
currently lose_sum: 487.3214558362961
time_elpased: 2.661
start validation test
0.605206185567
0.87966654585
0.248820996514
0.387916566771
0
validation finish
batch start
#iterations: 360
currently lose_sum: 488.1530611217022
time_elpased: 2.648
batch start
#iterations: 361
currently lose_sum: 488.4845654666424
time_elpased: 2.669
batch start
#iterations: 362
currently lose_sum: 488.78733560442924
time_elpased: 2.679
batch start
#iterations: 363
currently lose_sum: 489.1643818318844
time_elpased: 2.669
batch start
#iterations: 364
currently lose_sum: 488.08520871400833
time_elpased: 2.672
batch start
#iterations: 365
currently lose_sum: 487.93535274267197
time_elpased: 2.673
batch start
#iterations: 366
currently lose_sum: 488.4097853600979
time_elpased: 2.678
batch start
#iterations: 367
currently lose_sum: 492.7003688812256
time_elpased: 2.659
batch start
#iterations: 368
currently lose_sum: 489.69974422454834
time_elpased: 2.684
batch start
#iterations: 369
currently lose_sum: 487.7503964304924
time_elpased: 2.658
batch start
#iterations: 370
currently lose_sum: 490.03071945905685
time_elpased: 2.669
batch start
#iterations: 371
currently lose_sum: 488.1865833699703
time_elpased: 2.676
batch start
#iterations: 372
currently lose_sum: 490.22579795122147
time_elpased: 2.655
batch start
#iterations: 373
currently lose_sum: 492.3459563553333
time_elpased: 2.677
batch start
#iterations: 374
currently lose_sum: 488.3611496090889
time_elpased: 2.678
batch start
#iterations: 375
currently lose_sum: 492.9184768497944
time_elpased: 2.648
batch start
#iterations: 376
currently lose_sum: 488.3691499531269
time_elpased: 2.658
batch start
#iterations: 377
currently lose_sum: 489.84674620628357
time_elpased: 2.653
batch start
#iterations: 378
currently lose_sum: 491.2131857872009
time_elpased: 2.681
batch start
#iterations: 379
currently lose_sum: 486.3409286439419
time_elpased: 2.685
start validation test
0.607989690722
0.877680140598
0.255997539471
0.396380665132
0
validation finish
batch start
#iterations: 380
currently lose_sum: 490.1402167379856
time_elpased: 2.654
batch start
#iterations: 381
currently lose_sum: 488.55076363682747
time_elpased: 2.666
batch start
#iterations: 382
currently lose_sum: 489.9080808162689
time_elpased: 2.653
batch start
#iterations: 383
currently lose_sum: 490.61652716994286
time_elpased: 2.663
batch start
#iterations: 384
currently lose_sum: 489.74162420630455
time_elpased: 2.65
batch start
#iterations: 385
currently lose_sum: 489.47732120752335
time_elpased: 2.648
batch start
#iterations: 386
currently lose_sum: 491.80815011262894
time_elpased: 2.67
batch start
#iterations: 387
currently lose_sum: 489.70607447624207
time_elpased: 2.671
batch start
#iterations: 388
currently lose_sum: 491.01488867402077
time_elpased: 2.669
batch start
#iterations: 389
currently lose_sum: 489.16195210814476
time_elpased: 2.685
batch start
#iterations: 390
currently lose_sum: 489.8891055583954
time_elpased: 2.665
batch start
#iterations: 391
currently lose_sum: 488.4842612147331
time_elpased: 2.657
batch start
#iterations: 392
currently lose_sum: 488.5571138262749
time_elpased: 2.66
batch start
#iterations: 393
currently lose_sum: 489.61426872015
time_elpased: 2.672
batch start
#iterations: 394
currently lose_sum: 489.2488212287426
time_elpased: 2.658
batch start
#iterations: 395
currently lose_sum: 491.7951982319355
time_elpased: 2.666
batch start
#iterations: 396
currently lose_sum: 490.00827902555466
time_elpased: 2.663
batch start
#iterations: 397
currently lose_sum: 488.93300941586494
time_elpased: 2.651
batch start
#iterations: 398
currently lose_sum: 489.2706610560417
time_elpased: 2.675
batch start
#iterations: 399
currently lose_sum: 488.34680113196373
time_elpased: 2.657
start validation test
0.603762886598
0.880102979036
0.245335247078
0.383708811032
0
validation finish
acc: 0.619
pre: 0.868
rec: 0.287
F1: 0.432
auc: 0.000
