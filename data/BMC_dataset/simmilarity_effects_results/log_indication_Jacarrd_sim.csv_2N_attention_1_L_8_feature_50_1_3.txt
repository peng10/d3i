start to construct graph
graph construct over
87453
epochs start
batch start
#iterations: 0
currently lose_sum: 543.7621940970421
time_elpased: 1.668
batch start
#iterations: 1
currently lose_sum: 531.1309999227524
time_elpased: 1.596
batch start
#iterations: 2
currently lose_sum: 523.8860273063183
time_elpased: 1.603
batch start
#iterations: 3
currently lose_sum: 520.4710516035557
time_elpased: 1.607
batch start
#iterations: 4
currently lose_sum: 515.9733867049217
time_elpased: 1.617
batch start
#iterations: 5
currently lose_sum: 513.8791973292828
time_elpased: 1.6
batch start
#iterations: 6
currently lose_sum: 513.484944999218
time_elpased: 1.607
batch start
#iterations: 7
currently lose_sum: 511.98733907938004
time_elpased: 1.611
batch start
#iterations: 8
currently lose_sum: 510.72342854738235
time_elpased: 1.609
batch start
#iterations: 9
currently lose_sum: 507.71079328656197
time_elpased: 1.602
batch start
#iterations: 10
currently lose_sum: 507.6145522892475
time_elpased: 1.608
batch start
#iterations: 11
currently lose_sum: 506.244271248579
time_elpased: 1.601
batch start
#iterations: 12
currently lose_sum: 508.87602016329765
time_elpased: 1.592
batch start
#iterations: 13
currently lose_sum: 505.16193053126335
time_elpased: 1.605
batch start
#iterations: 14
currently lose_sum: 505.57726085186005
time_elpased: 1.607
batch start
#iterations: 15
currently lose_sum: 504.7228674888611
time_elpased: 1.59
batch start
#iterations: 16
currently lose_sum: 504.9844337403774
time_elpased: 1.621
batch start
#iterations: 17
currently lose_sum: 504.9717792570591
time_elpased: 1.603
batch start
#iterations: 18
currently lose_sum: 505.37201058864594
time_elpased: 1.605
batch start
#iterations: 19
currently lose_sum: 503.7685650885105
time_elpased: 1.604
start validation test
0.613144329897
0.910909090909
0.256528417819
0.40031961646
0
validation finish
batch start
#iterations: 20
currently lose_sum: 504.62538784742355
time_elpased: 1.601
batch start
#iterations: 21
currently lose_sum: 502.6284500360489
time_elpased: 1.609
batch start
#iterations: 22
currently lose_sum: 504.7531936466694
time_elpased: 1.629
batch start
#iterations: 23
currently lose_sum: 502.55177357792854
time_elpased: 1.599
batch start
#iterations: 24
currently lose_sum: 502.37926557660103
time_elpased: 1.61
batch start
#iterations: 25
currently lose_sum: 500.7989640533924
time_elpased: 1.601
batch start
#iterations: 26
currently lose_sum: 500.97272381186485
time_elpased: 1.59
batch start
#iterations: 27
currently lose_sum: 502.4634059369564
time_elpased: 1.6
batch start
#iterations: 28
currently lose_sum: 500.0428987443447
time_elpased: 1.596
batch start
#iterations: 29
currently lose_sum: 499.6773648262024
time_elpased: 1.605
batch start
#iterations: 30
currently lose_sum: 500.1001011133194
time_elpased: 1.613
batch start
#iterations: 31
currently lose_sum: 501.0641243159771
time_elpased: 1.598
batch start
#iterations: 32
currently lose_sum: 501.66845816373825
time_elpased: 1.616
batch start
#iterations: 33
currently lose_sum: 499.32702246308327
time_elpased: 1.608
batch start
#iterations: 34
currently lose_sum: 498.3675052523613
time_elpased: 1.605
batch start
#iterations: 35
currently lose_sum: 502.1737384200096
time_elpased: 1.591
batch start
#iterations: 36
currently lose_sum: 500.68533688783646
time_elpased: 1.603
batch start
#iterations: 37
currently lose_sum: 500.08803433179855
time_elpased: 1.597
batch start
#iterations: 38
currently lose_sum: 499.715308457613
time_elpased: 1.594
batch start
#iterations: 39
currently lose_sum: 498.72381123900414
time_elpased: 1.607
start validation test
0.609896907216
0.903415350716
0.251920122888
0.39397821909
0
validation finish
batch start
#iterations: 40
currently lose_sum: 499.0985396504402
time_elpased: 1.619
batch start
#iterations: 41
currently lose_sum: 500.7320232093334
time_elpased: 1.611
batch start
#iterations: 42
currently lose_sum: 499.0235213637352
time_elpased: 1.613
batch start
#iterations: 43
currently lose_sum: 500.4425922036171
time_elpased: 1.608
batch start
#iterations: 44
currently lose_sum: 496.2263621389866
time_elpased: 1.592
batch start
#iterations: 45
currently lose_sum: 496.4160090684891
time_elpased: 1.596
batch start
#iterations: 46
currently lose_sum: 500.8311188817024
time_elpased: 1.607
batch start
#iterations: 47
currently lose_sum: 499.3352142572403
time_elpased: 1.605
batch start
#iterations: 48
currently lose_sum: 496.9964938759804
time_elpased: 1.601
batch start
#iterations: 49
currently lose_sum: 498.35173374414444
time_elpased: 1.595
batch start
#iterations: 50
currently lose_sum: 498.2552953362465
time_elpased: 1.607
batch start
#iterations: 51
currently lose_sum: 500.5357596874237
time_elpased: 1.602
batch start
#iterations: 52
currently lose_sum: 500.73855397105217
time_elpased: 1.608
batch start
#iterations: 53
currently lose_sum: 499.1266728043556
time_elpased: 1.609
batch start
#iterations: 54
currently lose_sum: 496.8735267519951
time_elpased: 1.599
batch start
#iterations: 55
currently lose_sum: 497.176572650671
time_elpased: 1.618
batch start
#iterations: 56
currently lose_sum: 496.7763747870922
time_elpased: 1.602
batch start
#iterations: 57
currently lose_sum: 496.7410967051983
time_elpased: 1.606
batch start
#iterations: 58
currently lose_sum: 496.764815479517
time_elpased: 1.598
batch start
#iterations: 59
currently lose_sum: 496.63104870915413
time_elpased: 1.609
start validation test
0.562010309278
0.927223719677
0.140911418331
0.244643968353
0
validation finish
batch start
#iterations: 60
currently lose_sum: 498.5061949789524
time_elpased: 1.628
batch start
#iterations: 61
currently lose_sum: 497.2530971765518
time_elpased: 1.599
batch start
#iterations: 62
currently lose_sum: 496.44167843461037
time_elpased: 1.605
batch start
#iterations: 63
currently lose_sum: 499.27433055639267
time_elpased: 1.596
batch start
#iterations: 64
currently lose_sum: 496.7193413078785
time_elpased: 1.603
batch start
#iterations: 65
currently lose_sum: 498.3187134861946
time_elpased: 1.613
batch start
#iterations: 66
currently lose_sum: 495.8108808994293
time_elpased: 1.618
batch start
#iterations: 67
currently lose_sum: 495.48792347311974
time_elpased: 1.596
batch start
#iterations: 68
currently lose_sum: 494.1871815621853
time_elpased: 1.607
batch start
#iterations: 69
currently lose_sum: 496.15942445397377
time_elpased: 1.601
batch start
#iterations: 70
currently lose_sum: 497.68895453214645
time_elpased: 1.606
batch start
#iterations: 71
currently lose_sum: 496.789999127388
time_elpased: 1.614
batch start
#iterations: 72
currently lose_sum: 494.69216296076775
time_elpased: 1.597
batch start
#iterations: 73
currently lose_sum: 497.6742040514946
time_elpased: 1.612
batch start
#iterations: 74
currently lose_sum: 496.593994140625
time_elpased: 1.597
batch start
#iterations: 75
currently lose_sum: 496.02286756038666
time_elpased: 1.61
batch start
#iterations: 76
currently lose_sum: 496.9998771548271
time_elpased: 1.61
batch start
#iterations: 77
currently lose_sum: 495.6050127148628
time_elpased: 1.595
batch start
#iterations: 78
currently lose_sum: 495.51808056235313
time_elpased: 1.605
batch start
#iterations: 79
currently lose_sum: 495.3795637190342
time_elpased: 1.616
start validation test
0.60324742268
0.917946645109
0.232565284178
0.371108750715
0
validation finish
batch start
#iterations: 80
currently lose_sum: 496.51297786831856
time_elpased: 1.599
batch start
#iterations: 81
currently lose_sum: 496.7829632163048
time_elpased: 1.618
batch start
#iterations: 82
currently lose_sum: 496.777768522501
time_elpased: 1.599
batch start
#iterations: 83
currently lose_sum: 493.62565901875496
time_elpased: 1.608
batch start
#iterations: 84
currently lose_sum: 495.5195261836052
time_elpased: 1.599
batch start
#iterations: 85
currently lose_sum: 498.2945447266102
time_elpased: 1.595
batch start
#iterations: 86
currently lose_sum: 497.04907673597336
time_elpased: 1.596
batch start
#iterations: 87
currently lose_sum: 496.53408285975456
time_elpased: 1.604
batch start
#iterations: 88
currently lose_sum: 496.79947459697723
time_elpased: 1.609
batch start
#iterations: 89
currently lose_sum: 494.31913751363754
time_elpased: 1.612
batch start
#iterations: 90
currently lose_sum: 495.4504318833351
time_elpased: 1.619
batch start
#iterations: 91
currently lose_sum: 495.2370268404484
time_elpased: 1.608
batch start
#iterations: 92
currently lose_sum: 494.47307446599007
time_elpased: 1.606
batch start
#iterations: 93
currently lose_sum: 493.4383243918419
time_elpased: 1.615
batch start
#iterations: 94
currently lose_sum: 495.43672251701355
time_elpased: 1.602
batch start
#iterations: 95
currently lose_sum: 493.7233175933361
time_elpased: 1.616
batch start
#iterations: 96
currently lose_sum: 494.8201758861542
time_elpased: 1.606
batch start
#iterations: 97
currently lose_sum: 497.4806382358074
time_elpased: 1.593
batch start
#iterations: 98
currently lose_sum: 494.16975194215775
time_elpased: 1.614
batch start
#iterations: 99
currently lose_sum: 496.33677965402603
time_elpased: 1.603
start validation test
0.599948453608
0.926382978723
0.2229390681
0.359389186958
0
validation finish
batch start
#iterations: 100
currently lose_sum: 494.2086168527603
time_elpased: 1.608
batch start
#iterations: 101
currently lose_sum: 494.07670801877975
time_elpased: 1.602
batch start
#iterations: 102
currently lose_sum: 496.8648772239685
time_elpased: 1.6
batch start
#iterations: 103
currently lose_sum: 494.24132934212685
time_elpased: 1.613
batch start
#iterations: 104
currently lose_sum: 492.47351372241974
time_elpased: 1.608
batch start
#iterations: 105
currently lose_sum: 494.0244500041008
time_elpased: 1.6
batch start
#iterations: 106
currently lose_sum: 495.2818347811699
time_elpased: 1.626
batch start
#iterations: 107
currently lose_sum: 495.57420951128006
time_elpased: 1.607
batch start
#iterations: 108
currently lose_sum: 494.46344968676567
time_elpased: 1.615
batch start
#iterations: 109
currently lose_sum: 496.2355152666569
time_elpased: 1.607
batch start
#iterations: 110
currently lose_sum: 494.74817955493927
time_elpased: 1.631
batch start
#iterations: 111
currently lose_sum: 495.30170583724976
time_elpased: 1.605
batch start
#iterations: 112
currently lose_sum: 493.03187054395676
time_elpased: 1.593
batch start
#iterations: 113
currently lose_sum: 495.4123701751232
time_elpased: 1.614
batch start
#iterations: 114
currently lose_sum: 495.90644931793213
time_elpased: 1.599
batch start
#iterations: 115
currently lose_sum: 495.0113101899624
time_elpased: 1.617
batch start
#iterations: 116
currently lose_sum: 495.58704951405525
time_elpased: 1.608
batch start
#iterations: 117
currently lose_sum: 494.92791071534157
time_elpased: 1.603
batch start
#iterations: 118
currently lose_sum: 495.9284816086292
time_elpased: 1.617
batch start
#iterations: 119
currently lose_sum: 493.07933923602104
time_elpased: 1.669
start validation test
0.585206185567
0.92736318408
0.190885816692
0.316602972399
0
validation finish
batch start
#iterations: 120
currently lose_sum: 495.50432708859444
time_elpased: 1.621
batch start
#iterations: 121
currently lose_sum: 494.89969408512115
time_elpased: 1.609
batch start
#iterations: 122
currently lose_sum: 495.86562740802765
time_elpased: 1.605
batch start
#iterations: 123
currently lose_sum: 497.17107915878296
time_elpased: 1.617
batch start
#iterations: 124
currently lose_sum: 495.5883229970932
time_elpased: 1.591
batch start
#iterations: 125
currently lose_sum: 493.37599235773087
time_elpased: 1.617
batch start
#iterations: 126
currently lose_sum: 492.2263669669628
time_elpased: 1.602
batch start
#iterations: 127
currently lose_sum: 494.22409799695015
time_elpased: 1.598
batch start
#iterations: 128
currently lose_sum: 493.81929221749306
time_elpased: 1.602
batch start
#iterations: 129
currently lose_sum: 495.1070505976677
time_elpased: 1.615
batch start
#iterations: 130
currently lose_sum: 493.26507249474525
time_elpased: 1.597
batch start
#iterations: 131
currently lose_sum: 493.4638519883156
time_elpased: 1.61
batch start
#iterations: 132
currently lose_sum: 493.7551011145115
time_elpased: 1.606
batch start
#iterations: 133
currently lose_sum: 494.0828215479851
time_elpased: 1.614
batch start
#iterations: 134
currently lose_sum: 493.066845446825
time_elpased: 1.61
batch start
#iterations: 135
currently lose_sum: 492.05928775668144
time_elpased: 1.608
batch start
#iterations: 136
currently lose_sum: 495.08348193764687
time_elpased: 1.609
batch start
#iterations: 137
currently lose_sum: 494.1190659701824
time_elpased: 1.604
batch start
#iterations: 138
currently lose_sum: 496.05760249495506
time_elpased: 1.612
batch start
#iterations: 139
currently lose_sum: 495.76729533076286
time_elpased: 1.602
start validation test
0.638402061856
0.898781902552
0.317357910906
0.469083478392
0
validation finish
batch start
#iterations: 140
currently lose_sum: 493.7428046762943
time_elpased: 1.637
batch start
#iterations: 141
currently lose_sum: 492.8148173689842
time_elpased: 1.613
batch start
#iterations: 142
currently lose_sum: 492.4784425199032
time_elpased: 1.601
batch start
#iterations: 143
currently lose_sum: 494.12568202614784
time_elpased: 1.597
batch start
#iterations: 144
currently lose_sum: 494.2783496081829
time_elpased: 1.602
batch start
#iterations: 145
currently lose_sum: 494.37671199440956
time_elpased: 1.623
batch start
#iterations: 146
currently lose_sum: 493.7742428779602
time_elpased: 1.607
batch start
#iterations: 147
currently lose_sum: 494.0071985423565
time_elpased: 1.619
batch start
#iterations: 148
currently lose_sum: 493.09413146972656
time_elpased: 1.621
batch start
#iterations: 149
currently lose_sum: 496.1562597453594
time_elpased: 1.598
batch start
#iterations: 150
currently lose_sum: 494.61972361803055
time_elpased: 1.607
batch start
#iterations: 151
currently lose_sum: 493.3972214758396
time_elpased: 1.607
batch start
#iterations: 152
currently lose_sum: 493.25655776262283
time_elpased: 1.606
batch start
#iterations: 153
currently lose_sum: 493.10948061943054
time_elpased: 1.613
batch start
#iterations: 154
currently lose_sum: 493.9567172527313
time_elpased: 1.595
batch start
#iterations: 155
currently lose_sum: 496.19666826725006
time_elpased: 1.593
batch start
#iterations: 156
currently lose_sum: 493.5950938463211
time_elpased: 1.593
batch start
#iterations: 157
currently lose_sum: 493.6142366826534
time_elpased: 1.605
batch start
#iterations: 158
currently lose_sum: 494.798138409853
time_elpased: 1.614
batch start
#iterations: 159
currently lose_sum: 493.0891191959381
time_elpased: 1.607
start validation test
0.609948453608
0.907037037037
0.250793650794
0.392940232651
0
validation finish
batch start
#iterations: 160
currently lose_sum: 494.6687906086445
time_elpased: 1.637
batch start
#iterations: 161
currently lose_sum: 494.1332628726959
time_elpased: 1.601
batch start
#iterations: 162
currently lose_sum: 493.50931641459465
time_elpased: 1.612
batch start
#iterations: 163
currently lose_sum: 494.3536399304867
time_elpased: 1.599
batch start
#iterations: 164
currently lose_sum: 494.17385244369507
time_elpased: 1.627
batch start
#iterations: 165
currently lose_sum: 492.15051808953285
time_elpased: 1.597
batch start
#iterations: 166
currently lose_sum: 493.73441022634506
time_elpased: 1.625
batch start
#iterations: 167
currently lose_sum: 491.91063192486763
time_elpased: 1.598
batch start
#iterations: 168
currently lose_sum: 494.32118514180183
time_elpased: 1.606
batch start
#iterations: 169
currently lose_sum: 492.21842071413994
time_elpased: 1.597
batch start
#iterations: 170
currently lose_sum: 493.79548412561417
time_elpased: 1.618
batch start
#iterations: 171
currently lose_sum: 494.48500394821167
time_elpased: 1.613
batch start
#iterations: 172
currently lose_sum: 492.8395809829235
time_elpased: 1.601
batch start
#iterations: 173
currently lose_sum: 492.47300603985786
time_elpased: 1.601
batch start
#iterations: 174
currently lose_sum: 493.30821004509926
time_elpased: 1.618
batch start
#iterations: 175
currently lose_sum: 492.17714112997055
time_elpased: 1.607
batch start
#iterations: 176
currently lose_sum: 494.35220381617546
time_elpased: 1.597
batch start
#iterations: 177
currently lose_sum: 494.22546523809433
time_elpased: 1.615
batch start
#iterations: 178
currently lose_sum: 492.9581327140331
time_elpased: 1.609
batch start
#iterations: 179
currently lose_sum: 495.4329636991024
time_elpased: 1.611
start validation test
0.614742268041
0.90491339696
0.26216077829
0.406542798158
0
validation finish
batch start
#iterations: 180
currently lose_sum: 494.1103246510029
time_elpased: 1.643
batch start
#iterations: 181
currently lose_sum: 494.73432832956314
time_elpased: 1.595
batch start
#iterations: 182
currently lose_sum: 491.9828144311905
time_elpased: 1.607
batch start
#iterations: 183
currently lose_sum: 494.24884390830994
time_elpased: 1.606
batch start
#iterations: 184
currently lose_sum: 492.7923675477505
time_elpased: 1.604
batch start
#iterations: 185
currently lose_sum: 493.5935142338276
time_elpased: 1.609
batch start
#iterations: 186
currently lose_sum: 492.23783335089684
time_elpased: 1.609
batch start
#iterations: 187
currently lose_sum: 494.98950958251953
time_elpased: 1.616
batch start
#iterations: 188
currently lose_sum: 492.9696595966816
time_elpased: 1.599
batch start
#iterations: 189
currently lose_sum: 493.3015775978565
time_elpased: 1.617
batch start
#iterations: 190
currently lose_sum: 494.428641051054
time_elpased: 1.593
batch start
#iterations: 191
currently lose_sum: 492.84022054076195
time_elpased: 1.618
batch start
#iterations: 192
currently lose_sum: 492.9730616211891
time_elpased: 1.611
batch start
#iterations: 193
currently lose_sum: 492.84146985411644
time_elpased: 1.602
batch start
#iterations: 194
currently lose_sum: 494.90580931305885
time_elpased: 1.602
batch start
#iterations: 195
currently lose_sum: 492.09443312883377
time_elpased: 1.601
batch start
#iterations: 196
currently lose_sum: 494.21477353572845
time_elpased: 1.602
batch start
#iterations: 197
currently lose_sum: 494.74990925192833
time_elpased: 1.626
batch start
#iterations: 198
currently lose_sum: 493.5863533616066
time_elpased: 1.599
batch start
#iterations: 199
currently lose_sum: 495.23016715049744
time_elpased: 1.602
start validation test
0.603762886598
0.913285600636
0.235125448029
0.37397182181
0
validation finish
batch start
#iterations: 200
currently lose_sum: 490.4268756210804
time_elpased: 1.623
batch start
#iterations: 201
currently lose_sum: 493.57204207777977
time_elpased: 1.62
batch start
#iterations: 202
currently lose_sum: 490.50125059485435
time_elpased: 1.594
batch start
#iterations: 203
currently lose_sum: 493.25412344932556
time_elpased: 1.602
batch start
#iterations: 204
currently lose_sum: 493.6377643048763
time_elpased: 1.607
batch start
#iterations: 205
currently lose_sum: 493.77020156383514
time_elpased: 1.603
batch start
#iterations: 206
currently lose_sum: 490.657889932394
time_elpased: 1.596
batch start
#iterations: 207
currently lose_sum: 493.26770040392876
time_elpased: 1.61
batch start
#iterations: 208
currently lose_sum: 493.65936693549156
time_elpased: 1.61
batch start
#iterations: 209
currently lose_sum: 493.6945213377476
time_elpased: 1.609
batch start
#iterations: 210
currently lose_sum: 492.6643380224705
time_elpased: 1.606
batch start
#iterations: 211
currently lose_sum: 492.0847792327404
time_elpased: 1.614
batch start
#iterations: 212
currently lose_sum: 492.5687371492386
time_elpased: 1.6
batch start
#iterations: 213
currently lose_sum: 494.61599335074425
time_elpased: 1.599
batch start
#iterations: 214
currently lose_sum: 495.147863060236
time_elpased: 1.608
batch start
#iterations: 215
currently lose_sum: 493.1072078049183
time_elpased: 1.601
batch start
#iterations: 216
currently lose_sum: 494.8654138445854
time_elpased: 1.604
batch start
#iterations: 217
currently lose_sum: 492.5774436593056
time_elpased: 1.611
batch start
#iterations: 218
currently lose_sum: 494.89108046889305
time_elpased: 1.61
batch start
#iterations: 219
currently lose_sum: 493.91656133532524
time_elpased: 1.601
start validation test
0.582525773196
0.927618069815
0.185048643113
0.308546059933
0
validation finish
batch start
#iterations: 220
currently lose_sum: 493.30375468730927
time_elpased: 1.622
batch start
#iterations: 221
currently lose_sum: 495.22536185383797
time_elpased: 1.603
batch start
#iterations: 222
currently lose_sum: 493.6151358783245
time_elpased: 1.617
batch start
#iterations: 223
currently lose_sum: 489.5277095735073
time_elpased: 1.598
batch start
#iterations: 224
currently lose_sum: 494.1663444638252
time_elpased: 1.605
batch start
#iterations: 225
currently lose_sum: 493.78242337703705
time_elpased: 1.617
batch start
#iterations: 226
currently lose_sum: 493.4985603392124
time_elpased: 1.607
batch start
#iterations: 227
currently lose_sum: 490.73652398586273
time_elpased: 1.607
batch start
#iterations: 228
currently lose_sum: 492.3438518345356
time_elpased: 1.616
batch start
#iterations: 229
currently lose_sum: 493.59067022800446
time_elpased: 1.647
batch start
#iterations: 230
currently lose_sum: 492.8240089714527
time_elpased: 1.616
batch start
#iterations: 231
currently lose_sum: 496.85537961125374
time_elpased: 1.6
batch start
#iterations: 232
currently lose_sum: 494.28402096033096
time_elpased: 1.598
batch start
#iterations: 233
currently lose_sum: 493.7104252278805
time_elpased: 1.603
batch start
#iterations: 234
currently lose_sum: 492.7314504683018
time_elpased: 1.613
batch start
#iterations: 235
currently lose_sum: 493.2675911784172
time_elpased: 1.601
batch start
#iterations: 236
currently lose_sum: 491.80979642271996
time_elpased: 1.606
batch start
#iterations: 237
currently lose_sum: 491.9286524951458
time_elpased: 1.605
batch start
#iterations: 238
currently lose_sum: 491.3727448284626
time_elpased: 1.609
batch start
#iterations: 239
currently lose_sum: 495.9996745288372
time_elpased: 1.613
start validation test
0.607886597938
0.911517925248
0.244751664107
0.38588843142
0
validation finish
batch start
#iterations: 240
currently lose_sum: 493.91559341549873
time_elpased: 1.631
batch start
#iterations: 241
currently lose_sum: 493.9184482097626
time_elpased: 1.606
batch start
#iterations: 242
currently lose_sum: 493.2035640478134
time_elpased: 1.608
batch start
#iterations: 243
currently lose_sum: 493.4061167240143
time_elpased: 1.596
batch start
#iterations: 244
currently lose_sum: 495.9516777396202
time_elpased: 1.598
batch start
#iterations: 245
currently lose_sum: 493.0439233481884
time_elpased: 1.593
batch start
#iterations: 246
currently lose_sum: 493.3882620334625
time_elpased: 1.601
batch start
#iterations: 247
currently lose_sum: 493.7040367424488
time_elpased: 1.614
batch start
#iterations: 248
currently lose_sum: 491.0481380224228
time_elpased: 1.618
batch start
#iterations: 249
currently lose_sum: 493.94003638625145
time_elpased: 1.613
batch start
#iterations: 250
currently lose_sum: 492.9595682621002
time_elpased: 1.606
batch start
#iterations: 251
currently lose_sum: 491.9920044541359
time_elpased: 1.613
batch start
#iterations: 252
currently lose_sum: 494.49816876649857
time_elpased: 1.61
batch start
#iterations: 253
currently lose_sum: 491.6153861284256
time_elpased: 1.608
batch start
#iterations: 254
currently lose_sum: 495.6898815333843
time_elpased: 1.61
batch start
#iterations: 255
currently lose_sum: 492.97591921687126
time_elpased: 1.598
batch start
#iterations: 256
currently lose_sum: 493.0961675643921
time_elpased: 1.598
batch start
#iterations: 257
currently lose_sum: 491.75435599684715
time_elpased: 1.602
batch start
#iterations: 258
currently lose_sum: 490.4718790650368
time_elpased: 1.595
batch start
#iterations: 259
currently lose_sum: 493.4796614050865
time_elpased: 1.595
start validation test
0.625618556701
0.912326961107
0.283461341526
0.432533791702
0
validation finish
batch start
#iterations: 260
currently lose_sum: 492.2615513205528
time_elpased: 1.615
batch start
#iterations: 261
currently lose_sum: 492.64202719926834
time_elpased: 1.602
batch start
#iterations: 262
currently lose_sum: 493.4823246002197
time_elpased: 1.615
batch start
#iterations: 263
currently lose_sum: 492.639486014843
time_elpased: 1.623
batch start
#iterations: 264
currently lose_sum: 493.28351205587387
time_elpased: 1.621
batch start
#iterations: 265
currently lose_sum: 493.22702288627625
time_elpased: 1.605
batch start
#iterations: 266
currently lose_sum: 493.50061613321304
time_elpased: 1.61
batch start
#iterations: 267
currently lose_sum: 493.10648825764656
time_elpased: 1.59
batch start
#iterations: 268
currently lose_sum: 491.1449810266495
time_elpased: 1.601
batch start
#iterations: 269
currently lose_sum: 491.38218200206757
time_elpased: 1.605
batch start
#iterations: 270
currently lose_sum: 494.03872203826904
time_elpased: 1.603
batch start
#iterations: 271
currently lose_sum: 494.28550803661346
time_elpased: 1.628
batch start
#iterations: 272
currently lose_sum: 492.8397982418537
time_elpased: 1.598
batch start
#iterations: 273
currently lose_sum: 493.407711148262
time_elpased: 1.61
batch start
#iterations: 274
currently lose_sum: 493.15293803811073
time_elpased: 1.615
batch start
#iterations: 275
currently lose_sum: 491.7361536324024
time_elpased: 1.604
batch start
#iterations: 276
currently lose_sum: 493.5688208937645
time_elpased: 1.612
batch start
#iterations: 277
currently lose_sum: 491.7424730360508
time_elpased: 1.624
batch start
#iterations: 278
currently lose_sum: 491.40391620993614
time_elpased: 1.608
batch start
#iterations: 279
currently lose_sum: 491.26191687583923
time_elpased: 1.607
start validation test
0.626494845361
0.916639100232
0.283768561188
0.433375039099
0
validation finish
batch start
#iterations: 280
currently lose_sum: 494.8488303422928
time_elpased: 1.606
batch start
#iterations: 281
currently lose_sum: 493.38728526234627
time_elpased: 1.612
batch start
#iterations: 282
currently lose_sum: 495.23413771390915
time_elpased: 1.604
batch start
#iterations: 283
currently lose_sum: 493.1604501605034
time_elpased: 1.622
batch start
#iterations: 284
currently lose_sum: 492.0674174129963
time_elpased: 1.6
batch start
#iterations: 285
currently lose_sum: 491.24677217006683
time_elpased: 1.604
batch start
#iterations: 286
currently lose_sum: 492.8805675804615
time_elpased: 1.594
batch start
#iterations: 287
currently lose_sum: 493.9958734214306
time_elpased: 1.644
batch start
#iterations: 288
currently lose_sum: 494.5611059963703
time_elpased: 1.602
batch start
#iterations: 289
currently lose_sum: 493.630823045969
time_elpased: 1.622
batch start
#iterations: 290
currently lose_sum: 492.120958507061
time_elpased: 1.607
batch start
#iterations: 291
currently lose_sum: 490.5462253987789
time_elpased: 1.599
batch start
#iterations: 292
currently lose_sum: 491.04987850785255
time_elpased: 1.599
batch start
#iterations: 293
currently lose_sum: 493.8586387038231
time_elpased: 1.601
batch start
#iterations: 294
currently lose_sum: 492.5797226727009
time_elpased: 1.608
batch start
#iterations: 295
currently lose_sum: 492.2869553565979
time_elpased: 1.597
batch start
#iterations: 296
currently lose_sum: 492.04423117637634
time_elpased: 1.608
batch start
#iterations: 297
currently lose_sum: 489.64277145266533
time_elpased: 1.613
batch start
#iterations: 298
currently lose_sum: 492.6995322406292
time_elpased: 1.599
batch start
#iterations: 299
currently lose_sum: 495.09191331267357
time_elpased: 1.604
start validation test
0.616701030928
0.907025515554
0.26574500768
0.411056549976
0
validation finish
batch start
#iterations: 300
currently lose_sum: 493.7776455283165
time_elpased: 1.613
batch start
#iterations: 301
currently lose_sum: 491.3912209570408
time_elpased: 1.61
batch start
#iterations: 302
currently lose_sum: 493.4360511302948
time_elpased: 1.614
batch start
#iterations: 303
currently lose_sum: 492.93676033616066
time_elpased: 1.617
batch start
#iterations: 304
currently lose_sum: 491.1003895998001
time_elpased: 1.607
batch start
#iterations: 305
currently lose_sum: 492.7829628288746
time_elpased: 1.602
batch start
#iterations: 306
currently lose_sum: 492.1810949444771
time_elpased: 1.606
batch start
#iterations: 307
currently lose_sum: 492.18279471993446
time_elpased: 1.604
batch start
#iterations: 308
currently lose_sum: 493.5337402522564
time_elpased: 1.612
batch start
#iterations: 309
currently lose_sum: 491.6061005592346
time_elpased: 1.6
batch start
#iterations: 310
currently lose_sum: 493.58943006396294
time_elpased: 1.606
batch start
#iterations: 311
currently lose_sum: 492.4719668030739
time_elpased: 1.597
batch start
#iterations: 312
currently lose_sum: 490.97392055392265
time_elpased: 1.605
batch start
#iterations: 313
currently lose_sum: 491.98355746269226
time_elpased: 1.608
batch start
#iterations: 314
currently lose_sum: 494.0022704601288
time_elpased: 1.609
batch start
#iterations: 315
currently lose_sum: 491.80170434713364
time_elpased: 1.617
batch start
#iterations: 316
currently lose_sum: 492.4446210861206
time_elpased: 1.607
batch start
#iterations: 317
currently lose_sum: 493.02484199404716
time_elpased: 1.606
batch start
#iterations: 318
currently lose_sum: 491.521316409111
time_elpased: 1.607
batch start
#iterations: 319
currently lose_sum: 490.58424589037895
time_elpased: 1.603
start validation test
0.619948453608
0.91384083045
0.270455709165
0.41738443303
0
validation finish
batch start
#iterations: 320
currently lose_sum: 496.43317541480064
time_elpased: 1.617
batch start
#iterations: 321
currently lose_sum: 492.79441344738007
time_elpased: 1.61
batch start
#iterations: 322
currently lose_sum: 488.6696500182152
time_elpased: 1.607
batch start
#iterations: 323
currently lose_sum: 494.3764232993126
time_elpased: 1.625
batch start
#iterations: 324
currently lose_sum: 492.19090658426285
time_elpased: 1.602
batch start
#iterations: 325
currently lose_sum: 491.79023134708405
time_elpased: 1.601
batch start
#iterations: 326
currently lose_sum: 491.9577907919884
time_elpased: 1.605
batch start
#iterations: 327
currently lose_sum: 492.43474251031876
time_elpased: 1.612
batch start
#iterations: 328
currently lose_sum: 492.54014268517494
time_elpased: 1.623
batch start
#iterations: 329
currently lose_sum: 490.8212009072304
time_elpased: 1.612
batch start
#iterations: 330
currently lose_sum: 493.3544597029686
time_elpased: 1.609
batch start
#iterations: 331
currently lose_sum: 491.8555198609829
time_elpased: 1.601
batch start
#iterations: 332
currently lose_sum: 492.69139659404755
time_elpased: 1.606
batch start
#iterations: 333
currently lose_sum: 491.2933988571167
time_elpased: 1.609
batch start
#iterations: 334
currently lose_sum: 490.90857830643654
time_elpased: 1.619
batch start
#iterations: 335
currently lose_sum: 491.658798545599
time_elpased: 1.621
batch start
#iterations: 336
currently lose_sum: 492.9474828541279
time_elpased: 1.602
batch start
#iterations: 337
currently lose_sum: 492.1334581375122
time_elpased: 1.624
batch start
#iterations: 338
currently lose_sum: 493.05608227849007
time_elpased: 1.611
batch start
#iterations: 339
currently lose_sum: 494.0395717024803
time_elpased: 1.611
start validation test
0.608762886598
0.916188289323
0.245161290323
0.386815317499
0
validation finish
batch start
#iterations: 340
currently lose_sum: 493.9045931696892
time_elpased: 1.651
batch start
#iterations: 341
currently lose_sum: 491.64280876517296
time_elpased: 1.613
batch start
#iterations: 342
currently lose_sum: 492.14004749059677
time_elpased: 1.591
batch start
#iterations: 343
currently lose_sum: 493.567113250494
time_elpased: 1.613
batch start
#iterations: 344
currently lose_sum: 494.9073379933834
time_elpased: 1.611
batch start
#iterations: 345
currently lose_sum: 494.9140762388706
time_elpased: 1.628
batch start
#iterations: 346
currently lose_sum: 489.25104010105133
time_elpased: 1.619
batch start
#iterations: 347
currently lose_sum: 491.99915969371796
time_elpased: 1.608
batch start
#iterations: 348
currently lose_sum: 492.22831133008003
time_elpased: 1.628
batch start
#iterations: 349
currently lose_sum: 492.55137944221497
time_elpased: 1.603
batch start
#iterations: 350
currently lose_sum: 492.74913412332535
time_elpased: 1.606
batch start
#iterations: 351
currently lose_sum: 493.1103337407112
time_elpased: 1.612
batch start
#iterations: 352
currently lose_sum: 492.8290286064148
time_elpased: 1.633
batch start
#iterations: 353
currently lose_sum: 492.421404004097
time_elpased: 1.604
batch start
#iterations: 354
currently lose_sum: 496.17566907405853
time_elpased: 1.605
batch start
#iterations: 355
currently lose_sum: 493.06096732616425
time_elpased: 1.603
batch start
#iterations: 356
currently lose_sum: 491.421520203352
time_elpased: 1.623
batch start
#iterations: 357
currently lose_sum: 491.36414071917534
time_elpased: 1.598
batch start
#iterations: 358
currently lose_sum: 492.0884413421154
time_elpased: 1.602
batch start
#iterations: 359
currently lose_sum: 492.74672189354897
time_elpased: 1.623
start validation test
0.620463917526
0.912148249828
0.272196620584
0.419275968136
0
validation finish
batch start
#iterations: 360
currently lose_sum: 493.11189886927605
time_elpased: 1.619
batch start
#iterations: 361
currently lose_sum: 494.2393308579922
time_elpased: 1.6
batch start
#iterations: 362
currently lose_sum: 493.4967153966427
time_elpased: 1.616
batch start
#iterations: 363
currently lose_sum: 491.3545519411564
time_elpased: 1.607
batch start
#iterations: 364
currently lose_sum: 492.0590408742428
time_elpased: 1.618
batch start
#iterations: 365
currently lose_sum: 490.27330765128136
time_elpased: 1.609
batch start
#iterations: 366
currently lose_sum: 492.1974277794361
time_elpased: 1.603
batch start
#iterations: 367
currently lose_sum: 495.64454820752144
time_elpased: 1.613
batch start
#iterations: 368
currently lose_sum: 492.4867045879364
time_elpased: 1.608
batch start
#iterations: 369
currently lose_sum: 492.82975679636
time_elpased: 1.618
batch start
#iterations: 370
currently lose_sum: 493.79990515112877
time_elpased: 1.618
batch start
#iterations: 371
currently lose_sum: 491.18110194802284
time_elpased: 1.623
batch start
#iterations: 372
currently lose_sum: 493.9849652349949
time_elpased: 1.62
batch start
#iterations: 373
currently lose_sum: 494.4436128437519
time_elpased: 1.609
batch start
#iterations: 374
currently lose_sum: 492.25873520970345
time_elpased: 1.614
batch start
#iterations: 375
currently lose_sum: 492.34130892157555
time_elpased: 1.633
batch start
#iterations: 376
currently lose_sum: 491.86381819844246
time_elpased: 1.615
batch start
#iterations: 377
currently lose_sum: 491.2406922876835
time_elpased: 1.612
batch start
#iterations: 378
currently lose_sum: 493.8214050233364
time_elpased: 1.605
batch start
#iterations: 379
currently lose_sum: 490.04508197307587
time_elpased: 1.62
start validation test
0.618505154639
0.912997903564
0.267588325653
0.413875029698
0
validation finish
batch start
#iterations: 380
currently lose_sum: 491.1597001850605
time_elpased: 1.626
batch start
#iterations: 381
currently lose_sum: 490.40105390548706
time_elpased: 1.616
batch start
#iterations: 382
currently lose_sum: 490.96666502952576
time_elpased: 1.625
batch start
#iterations: 383
currently lose_sum: 490.6834661066532
time_elpased: 1.603
batch start
#iterations: 384
currently lose_sum: 491.9629017114639
time_elpased: 1.626
batch start
#iterations: 385
currently lose_sum: 493.11325630545616
time_elpased: 1.598
batch start
#iterations: 386
currently lose_sum: 495.16674000024796
time_elpased: 1.608
batch start
#iterations: 387
currently lose_sum: 492.0824179947376
time_elpased: 1.597
batch start
#iterations: 388
currently lose_sum: 491.25091940164566
time_elpased: 1.615
batch start
#iterations: 389
currently lose_sum: 493.3974544107914
time_elpased: 1.633
batch start
#iterations: 390
currently lose_sum: 493.09181290864944
time_elpased: 1.613
batch start
#iterations: 391
currently lose_sum: 490.87776908278465
time_elpased: 1.607
batch start
#iterations: 392
currently lose_sum: 494.2416625022888
time_elpased: 1.606
batch start
#iterations: 393
currently lose_sum: 494.60578522086143
time_elpased: 1.599
batch start
#iterations: 394
currently lose_sum: 490.66395565867424
time_elpased: 1.613
batch start
#iterations: 395
currently lose_sum: 492.6873903274536
time_elpased: 1.602
batch start
#iterations: 396
currently lose_sum: 490.7935298681259
time_elpased: 1.611
batch start
#iterations: 397
currently lose_sum: 492.77228730916977
time_elpased: 1.612
batch start
#iterations: 398
currently lose_sum: 491.3762985467911
time_elpased: 1.607
batch start
#iterations: 399
currently lose_sum: 493.24392119050026
time_elpased: 1.612
start validation test
0.609381443299
0.909704008992
0.248643113159
0.390542062088
0
validation finish
acc: 0.646
pre: 0.905
rec: 0.323
F1: 0.476
auc: 0.000
