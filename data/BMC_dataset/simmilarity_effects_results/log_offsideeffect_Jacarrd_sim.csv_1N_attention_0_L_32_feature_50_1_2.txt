start to construct graph
graph construct over
58300
epochs start
batch start
#iterations: 0
currently lose_sum: 401.502217233181
time_elpased: 1.237
batch start
#iterations: 1
currently lose_sum: 397.89602464437485
time_elpased: 1.161
batch start
#iterations: 2
currently lose_sum: 394.9796788096428
time_elpased: 1.156
batch start
#iterations: 3
currently lose_sum: 393.9259268641472
time_elpased: 1.178
batch start
#iterations: 4
currently lose_sum: 392.9534116387367
time_elpased: 1.157
batch start
#iterations: 5
currently lose_sum: 392.23138523101807
time_elpased: 1.179
batch start
#iterations: 6
currently lose_sum: 391.79339838027954
time_elpased: 1.189
batch start
#iterations: 7
currently lose_sum: 391.1579583287239
time_elpased: 1.172
batch start
#iterations: 8
currently lose_sum: 390.7694536447525
time_elpased: 1.171
batch start
#iterations: 9
currently lose_sum: 391.27797961235046
time_elpased: 1.159
batch start
#iterations: 10
currently lose_sum: 389.34016168117523
time_elpased: 1.185
batch start
#iterations: 11
currently lose_sum: 388.43967390060425
time_elpased: 1.17
batch start
#iterations: 12
currently lose_sum: 389.148822247982
time_elpased: 1.176
batch start
#iterations: 13
currently lose_sum: 388.3722279071808
time_elpased: 1.169
batch start
#iterations: 14
currently lose_sum: 385.4358880519867
time_elpased: 1.165
batch start
#iterations: 15
currently lose_sum: 387.05646258592606
time_elpased: 1.172
batch start
#iterations: 16
currently lose_sum: 386.1565253138542
time_elpased: 1.168
batch start
#iterations: 17
currently lose_sum: 386.79342955350876
time_elpased: 1.165
batch start
#iterations: 18
currently lose_sum: 384.692631483078
time_elpased: 1.161
batch start
#iterations: 19
currently lose_sum: 386.38878083229065
time_elpased: 1.163
start validation test
0.642113402062
0.92756349953
0.305894519131
0.460066879229
0
validation finish
batch start
#iterations: 20
currently lose_sum: 384.93541330099106
time_elpased: 1.191
batch start
#iterations: 21
currently lose_sum: 384.8610653281212
time_elpased: 1.168
batch start
#iterations: 22
currently lose_sum: 385.640476167202
time_elpased: 1.183
batch start
#iterations: 23
currently lose_sum: 385.93945640325546
time_elpased: 1.164
batch start
#iterations: 24
currently lose_sum: 384.1837916970253
time_elpased: 1.172
batch start
#iterations: 25
currently lose_sum: 384.2387855052948
time_elpased: 1.178
batch start
#iterations: 26
currently lose_sum: 384.1013993024826
time_elpased: 1.182
batch start
#iterations: 27
currently lose_sum: 383.39988619089127
time_elpased: 1.171
batch start
#iterations: 28
currently lose_sum: 383.3916622400284
time_elpased: 1.163
batch start
#iterations: 29
currently lose_sum: 383.4776357412338
time_elpased: 1.173
batch start
#iterations: 30
currently lose_sum: 382.7840408682823
time_elpased: 1.163
batch start
#iterations: 31
currently lose_sum: 383.40110778808594
time_elpased: 1.16
batch start
#iterations: 32
currently lose_sum: 382.99431627988815
time_elpased: 1.168
batch start
#iterations: 33
currently lose_sum: 383.553435087204
time_elpased: 1.152
batch start
#iterations: 34
currently lose_sum: 382.78534388542175
time_elpased: 1.171
batch start
#iterations: 35
currently lose_sum: 383.7321447134018
time_elpased: 1.17
batch start
#iterations: 36
currently lose_sum: 382.72933715581894
time_elpased: 1.163
batch start
#iterations: 37
currently lose_sum: 381.08884221315384
time_elpased: 1.165
batch start
#iterations: 38
currently lose_sum: 382.1216601729393
time_elpased: 1.167
batch start
#iterations: 39
currently lose_sum: 381.63947266340256
time_elpased: 1.183
start validation test
0.749690721649
0.857865001487
0.596690796277
0.703830202488
0
validation finish
batch start
#iterations: 40
currently lose_sum: 381.702147603035
time_elpased: 1.195
batch start
#iterations: 41
currently lose_sum: 380.4496691226959
time_elpased: 1.155
batch start
#iterations: 42
currently lose_sum: 380.22540056705475
time_elpased: 1.162
batch start
#iterations: 43
currently lose_sum: 382.05107283592224
time_elpased: 1.168
batch start
#iterations: 44
currently lose_sum: 382.0637767314911
time_elpased: 1.15
batch start
#iterations: 45
currently lose_sum: 381.14155435562134
time_elpased: 1.144
batch start
#iterations: 46
currently lose_sum: 381.1793290376663
time_elpased: 1.148
batch start
#iterations: 47
currently lose_sum: 381.1790973544121
time_elpased: 1.179
batch start
#iterations: 48
currently lose_sum: 379.42432421445847
time_elpased: 1.16
batch start
#iterations: 49
currently lose_sum: 380.6872398853302
time_elpased: 1.141
batch start
#iterations: 50
currently lose_sum: 380.00074660778046
time_elpased: 1.168
batch start
#iterations: 51
currently lose_sum: 380.3081250190735
time_elpased: 1.169
batch start
#iterations: 52
currently lose_sum: 380.81417578458786
time_elpased: 1.193
batch start
#iterations: 53
currently lose_sum: 379.7812610864639
time_elpased: 1.157
batch start
#iterations: 54
currently lose_sum: 380.2553636431694
time_elpased: 1.164
batch start
#iterations: 55
currently lose_sum: 379.9996299147606
time_elpased: 1.142
batch start
#iterations: 56
currently lose_sum: 380.2206346988678
time_elpased: 1.155
batch start
#iterations: 57
currently lose_sum: 380.015467941761
time_elpased: 1.157
batch start
#iterations: 58
currently lose_sum: 379.60307747125626
time_elpased: 1.156
batch start
#iterations: 59
currently lose_sum: 380.3747076392174
time_elpased: 1.173
start validation test
0.782113402062
0.826045285731
0.713029989659
0.765388244436
0
validation finish
batch start
#iterations: 60
currently lose_sum: 379.40408831834793
time_elpased: 1.188
batch start
#iterations: 61
currently lose_sum: 379.08263432979584
time_elpased: 1.154
batch start
#iterations: 62
currently lose_sum: 379.6374263763428
time_elpased: 1.163
batch start
#iterations: 63
currently lose_sum: 378.05747133493423
time_elpased: 1.162
batch start
#iterations: 64
currently lose_sum: 379.32857912778854
time_elpased: 1.178
batch start
#iterations: 65
currently lose_sum: 379.7932304739952
time_elpased: 1.194
batch start
#iterations: 66
currently lose_sum: 379.07507079839706
time_elpased: 1.162
batch start
#iterations: 67
currently lose_sum: 377.99379682540894
time_elpased: 1.168
batch start
#iterations: 68
currently lose_sum: 379.43535482883453
time_elpased: 1.175
batch start
#iterations: 69
currently lose_sum: 377.9350041747093
time_elpased: 1.172
batch start
#iterations: 70
currently lose_sum: 379.14751982688904
time_elpased: 1.173
batch start
#iterations: 71
currently lose_sum: 378.6128557920456
time_elpased: 1.198
batch start
#iterations: 72
currently lose_sum: 378.8998147845268
time_elpased: 1.168
batch start
#iterations: 73
currently lose_sum: 378.2968913912773
time_elpased: 1.17
batch start
#iterations: 74
currently lose_sum: 378.51397466659546
time_elpased: 1.165
batch start
#iterations: 75
currently lose_sum: 376.48107743263245
time_elpased: 1.171
batch start
#iterations: 76
currently lose_sum: 377.5062661767006
time_elpased: 1.166
batch start
#iterations: 77
currently lose_sum: 378.6563872694969
time_elpased: 1.173
batch start
#iterations: 78
currently lose_sum: 378.5428804755211
time_elpased: 1.141
batch start
#iterations: 79
currently lose_sum: 377.83090257644653
time_elpased: 1.157
start validation test
0.760360824742
0.717982113398
0.855118924509
0.780572992873
0
validation finish
batch start
#iterations: 80
currently lose_sum: 376.6264687180519
time_elpased: 1.198
batch start
#iterations: 81
currently lose_sum: 378.81380236148834
time_elpased: 1.177
batch start
#iterations: 82
currently lose_sum: 378.03374606370926
time_elpased: 1.165
batch start
#iterations: 83
currently lose_sum: 377.04727840423584
time_elpased: 1.175
batch start
#iterations: 84
currently lose_sum: 378.65518379211426
time_elpased: 1.182
batch start
#iterations: 85
currently lose_sum: 378.1734348535538
time_elpased: 1.158
batch start
#iterations: 86
currently lose_sum: 377.9558379650116
time_elpased: 1.153
batch start
#iterations: 87
currently lose_sum: 377.7403923869133
time_elpased: 1.173
batch start
#iterations: 88
currently lose_sum: 377.35625356435776
time_elpased: 1.163
batch start
#iterations: 89
currently lose_sum: 377.4944856762886
time_elpased: 1.186
batch start
#iterations: 90
currently lose_sum: 378.33293825387955
time_elpased: 1.154
batch start
#iterations: 91
currently lose_sum: 377.15483140945435
time_elpased: 1.163
batch start
#iterations: 92
currently lose_sum: 377.2561831474304
time_elpased: 1.171
batch start
#iterations: 93
currently lose_sum: 377.76503360271454
time_elpased: 1.142
batch start
#iterations: 94
currently lose_sum: 378.2004904150963
time_elpased: 1.163
batch start
#iterations: 95
currently lose_sum: 376.90197217464447
time_elpased: 1.151
batch start
#iterations: 96
currently lose_sum: 377.44162315130234
time_elpased: 1.152
batch start
#iterations: 97
currently lose_sum: 377.3386442065239
time_elpased: 1.189
batch start
#iterations: 98
currently lose_sum: 377.13728404045105
time_elpased: 1.17
batch start
#iterations: 99
currently lose_sum: 377.2163212299347
time_elpased: 1.165
start validation test
0.780824742268
0.870994248151
0.657704239917
0.749469714824
0
validation finish
batch start
#iterations: 100
currently lose_sum: 377.269468665123
time_elpased: 1.17
batch start
#iterations: 101
currently lose_sum: 376.1241776943207
time_elpased: 1.166
batch start
#iterations: 102
currently lose_sum: 376.6883094906807
time_elpased: 1.16
batch start
#iterations: 103
currently lose_sum: 376.6498682498932
time_elpased: 1.165
batch start
#iterations: 104
currently lose_sum: 377.21859461069107
time_elpased: 1.168
batch start
#iterations: 105
currently lose_sum: 376.1765823960304
time_elpased: 1.152
batch start
#iterations: 106
currently lose_sum: 377.29218393564224
time_elpased: 1.166
batch start
#iterations: 107
currently lose_sum: 376.4353483915329
time_elpased: 1.158
batch start
#iterations: 108
currently lose_sum: 376.5955645442009
time_elpased: 1.17
batch start
#iterations: 109
currently lose_sum: 375.3412407040596
time_elpased: 1.161
batch start
#iterations: 110
currently lose_sum: 377.399990439415
time_elpased: 1.199
batch start
#iterations: 111
currently lose_sum: 375.8577943444252
time_elpased: 1.224
batch start
#iterations: 112
currently lose_sum: 375.8979284763336
time_elpased: 1.214
batch start
#iterations: 113
currently lose_sum: 377.40262508392334
time_elpased: 1.191
batch start
#iterations: 114
currently lose_sum: 376.107934653759
time_elpased: 1.167
batch start
#iterations: 115
currently lose_sum: 376.27909129858017
time_elpased: 1.163
batch start
#iterations: 116
currently lose_sum: 376.17008423805237
time_elpased: 1.188
batch start
#iterations: 117
currently lose_sum: 375.6540519595146
time_elpased: 1.158
batch start
#iterations: 118
currently lose_sum: 374.894227206707
time_elpased: 1.176
batch start
#iterations: 119
currently lose_sum: 376.4837518930435
time_elpased: 1.151
start validation test
0.797268041237
0.854267012474
0.715305067218
0.778634547194
0
validation finish
batch start
#iterations: 120
currently lose_sum: 376.6982831954956
time_elpased: 1.196
batch start
#iterations: 121
currently lose_sum: 375.7711881995201
time_elpased: 1.151
batch start
#iterations: 122
currently lose_sum: 374.4967737197876
time_elpased: 1.153
batch start
#iterations: 123
currently lose_sum: 374.55196607112885
time_elpased: 1.17
batch start
#iterations: 124
currently lose_sum: 375.0934907197952
time_elpased: 1.158
batch start
#iterations: 125
currently lose_sum: 375.55238050222397
time_elpased: 1.158
batch start
#iterations: 126
currently lose_sum: 377.0811478495598
time_elpased: 1.151
batch start
#iterations: 127
currently lose_sum: 376.1128753423691
time_elpased: 1.158
batch start
#iterations: 128
currently lose_sum: 374.9019128084183
time_elpased: 1.177
batch start
#iterations: 129
currently lose_sum: 375.731380879879
time_elpased: 1.172
batch start
#iterations: 130
currently lose_sum: 374.8336720466614
time_elpased: 1.176
batch start
#iterations: 131
currently lose_sum: 376.00986260175705
time_elpased: 1.183
batch start
#iterations: 132
currently lose_sum: 376.7789480686188
time_elpased: 1.171
batch start
#iterations: 133
currently lose_sum: 375.16628098487854
time_elpased: 1.18
batch start
#iterations: 134
currently lose_sum: 376.28063929080963
time_elpased: 1.169
batch start
#iterations: 135
currently lose_sum: 375.8333648443222
time_elpased: 1.161
batch start
#iterations: 136
currently lose_sum: 376.9121451973915
time_elpased: 1.159
batch start
#iterations: 137
currently lose_sum: 374.7556876540184
time_elpased: 1.145
batch start
#iterations: 138
currently lose_sum: 377.414036154747
time_elpased: 1.152
batch start
#iterations: 139
currently lose_sum: 375.349491417408
time_elpased: 1.163
start validation test
0.800463917526
0.831864484377
0.751602895553
0.789699570815
0
validation finish
batch start
#iterations: 140
currently lose_sum: 375.44614338874817
time_elpased: 1.168
batch start
#iterations: 141
currently lose_sum: 376.7901003956795
time_elpased: 1.164
batch start
#iterations: 142
currently lose_sum: 375.81897592544556
time_elpased: 1.189
batch start
#iterations: 143
currently lose_sum: 376.19018042087555
time_elpased: 1.17
batch start
#iterations: 144
currently lose_sum: 375.64329504966736
time_elpased: 1.171
batch start
#iterations: 145
currently lose_sum: 374.98141008615494
time_elpased: 1.168
batch start
#iterations: 146
currently lose_sum: 374.68527203798294
time_elpased: 1.164
batch start
#iterations: 147
currently lose_sum: 375.5456929206848
time_elpased: 1.154
batch start
#iterations: 148
currently lose_sum: 375.1689547300339
time_elpased: 1.174
batch start
#iterations: 149
currently lose_sum: 375.90360993146896
time_elpased: 1.179
batch start
#iterations: 150
currently lose_sum: 374.84559988975525
time_elpased: 1.175
batch start
#iterations: 151
currently lose_sum: 375.323977291584
time_elpased: 1.142
batch start
#iterations: 152
currently lose_sum: 374.4119668006897
time_elpased: 1.16
batch start
#iterations: 153
currently lose_sum: 376.0400573015213
time_elpased: 1.195
batch start
#iterations: 154
currently lose_sum: 374.83046305179596
time_elpased: 1.171
batch start
#iterations: 155
currently lose_sum: 375.33760410547256
time_elpased: 1.169
batch start
#iterations: 156
currently lose_sum: 375.5871281027794
time_elpased: 1.152
batch start
#iterations: 157
currently lose_sum: 374.9410356283188
time_elpased: 1.152
batch start
#iterations: 158
currently lose_sum: 375.0456044077873
time_elpased: 1.151
batch start
#iterations: 159
currently lose_sum: 376.69731920957565
time_elpased: 1.167
start validation test
0.764278350515
0.918679152292
0.578283350569
0.709779780415
0
validation finish
batch start
#iterations: 160
currently lose_sum: 375.2438155412674
time_elpased: 1.178
batch start
#iterations: 161
currently lose_sum: 375.50163900852203
time_elpased: 1.174
batch start
#iterations: 162
currently lose_sum: 375.5113347172737
time_elpased: 1.178
batch start
#iterations: 163
currently lose_sum: 375.58984220027924
time_elpased: 1.251
batch start
#iterations: 164
currently lose_sum: 374.5107674598694
time_elpased: 1.223
batch start
#iterations: 165
currently lose_sum: 373.75064891576767
time_elpased: 1.26
batch start
#iterations: 166
currently lose_sum: 374.39386534690857
time_elpased: 1.233
batch start
#iterations: 167
currently lose_sum: 374.60765051841736
time_elpased: 1.256
batch start
#iterations: 168
currently lose_sum: 374.79278695583344
time_elpased: 1.236
batch start
#iterations: 169
currently lose_sum: 373.469805598259
time_elpased: 1.2
batch start
#iterations: 170
currently lose_sum: 374.4915264248848
time_elpased: 1.248
batch start
#iterations: 171
currently lose_sum: 375.0666140317917
time_elpased: 1.207
batch start
#iterations: 172
currently lose_sum: 374.98477268218994
time_elpased: 1.257
batch start
#iterations: 173
currently lose_sum: 375.10119765996933
time_elpased: 1.249
batch start
#iterations: 174
currently lose_sum: 374.25605821609497
time_elpased: 1.178
batch start
#iterations: 175
currently lose_sum: 375.8286755681038
time_elpased: 1.257
batch start
#iterations: 176
currently lose_sum: 374.52680015563965
time_elpased: 1.21
batch start
#iterations: 177
currently lose_sum: 373.5938888192177
time_elpased: 1.215
batch start
#iterations: 178
currently lose_sum: 376.11488765478134
time_elpased: 1.211
batch start
#iterations: 179
currently lose_sum: 375.06888258457184
time_elpased: 1.174
start validation test
0.797628865979
0.874543557642
0.693485005171
0.773560964356
0
validation finish
batch start
#iterations: 180
currently lose_sum: 374.87963730096817
time_elpased: 1.203
batch start
#iterations: 181
currently lose_sum: 374.48202681541443
time_elpased: 1.197
batch start
#iterations: 182
currently lose_sum: 374.404033601284
time_elpased: 1.193
batch start
#iterations: 183
currently lose_sum: 374.9194305539131
time_elpased: 1.182
batch start
#iterations: 184
currently lose_sum: 374.4387810230255
time_elpased: 1.245
batch start
#iterations: 185
currently lose_sum: 375.03643983602524
time_elpased: 1.188
batch start
#iterations: 186
currently lose_sum: 373.88406014442444
time_elpased: 1.193
batch start
#iterations: 187
currently lose_sum: 372.74017333984375
time_elpased: 1.24
batch start
#iterations: 188
currently lose_sum: 375.45457047224045
time_elpased: 1.235
batch start
#iterations: 189
currently lose_sum: 375.41983461380005
time_elpased: 1.247
batch start
#iterations: 190
currently lose_sum: 372.7342081665993
time_elpased: 1.214
batch start
#iterations: 191
currently lose_sum: 374.47153371572495
time_elpased: 1.207
batch start
#iterations: 192
currently lose_sum: 375.4888565540314
time_elpased: 1.187
batch start
#iterations: 193
currently lose_sum: 375.3031054735184
time_elpased: 1.176
batch start
#iterations: 194
currently lose_sum: 373.74154049158096
time_elpased: 1.209
batch start
#iterations: 195
currently lose_sum: 374.2869790792465
time_elpased: 1.216
batch start
#iterations: 196
currently lose_sum: 376.5026620030403
time_elpased: 1.206
batch start
#iterations: 197
currently lose_sum: 373.84407037496567
time_elpased: 1.204
batch start
#iterations: 198
currently lose_sum: 374.87279284000397
time_elpased: 1.232
batch start
#iterations: 199
currently lose_sum: 373.2921930551529
time_elpased: 1.221
start validation test
0.795051546392
0.893993910877
0.668045501551
0.764678030303
0
validation finish
batch start
#iterations: 200
currently lose_sum: 374.79808312654495
time_elpased: 1.212
batch start
#iterations: 201
currently lose_sum: 373.4538838863373
time_elpased: 1.194
batch start
#iterations: 202
currently lose_sum: 373.46113926172256
time_elpased: 1.186
batch start
#iterations: 203
currently lose_sum: 372.412233710289
time_elpased: 1.168
batch start
#iterations: 204
currently lose_sum: 375.0469896197319
time_elpased: 1.199
batch start
#iterations: 205
currently lose_sum: 373.5179533958435
time_elpased: 1.191
batch start
#iterations: 206
currently lose_sum: 373.6852636933327
time_elpased: 1.193
batch start
#iterations: 207
currently lose_sum: 374.48408526182175
time_elpased: 1.158
batch start
#iterations: 208
currently lose_sum: 374.13166213035583
time_elpased: 1.189
batch start
#iterations: 209
currently lose_sum: 374.6991692185402
time_elpased: 1.187
batch start
#iterations: 210
currently lose_sum: 373.8415969610214
time_elpased: 1.184
batch start
#iterations: 211
currently lose_sum: 373.5184762477875
time_elpased: 1.159
batch start
#iterations: 212
currently lose_sum: 375.155478477478
time_elpased: 1.195
batch start
#iterations: 213
currently lose_sum: 374.4189142584801
time_elpased: 1.16
batch start
#iterations: 214
currently lose_sum: 374.6675668954849
time_elpased: 1.183
batch start
#iterations: 215
currently lose_sum: 374.38831037282944
time_elpased: 1.154
batch start
#iterations: 216
currently lose_sum: 374.1777712702751
time_elpased: 1.195
batch start
#iterations: 217
currently lose_sum: 373.7680410146713
time_elpased: 1.167
batch start
#iterations: 218
currently lose_sum: 374.06874841451645
time_elpased: 1.232
batch start
#iterations: 219
currently lose_sum: 375.3275827765465
time_elpased: 1.179
start validation test
0.804845360825
0.852250957854
0.736091003102
0.789923426923
0
validation finish
batch start
#iterations: 220
currently lose_sum: 373.3277457356453
time_elpased: 1.202
batch start
#iterations: 221
currently lose_sum: 374.45664060115814
time_elpased: 1.199
batch start
#iterations: 222
currently lose_sum: 374.1113242506981
time_elpased: 1.179
batch start
#iterations: 223
currently lose_sum: 374.17477864027023
time_elpased: 1.158
batch start
#iterations: 224
currently lose_sum: 374.81152737140656
time_elpased: 1.195
batch start
#iterations: 225
currently lose_sum: 373.5910664796829
time_elpased: 1.168
batch start
#iterations: 226
currently lose_sum: 374.3278303742409
time_elpased: 1.15
batch start
#iterations: 227
currently lose_sum: 374.1227604150772
time_elpased: 1.167
batch start
#iterations: 228
currently lose_sum: 374.07355147600174
time_elpased: 1.222
batch start
#iterations: 229
currently lose_sum: 372.96874845027924
time_elpased: 1.2
batch start
#iterations: 230
currently lose_sum: 373.80892848968506
time_elpased: 1.179
batch start
#iterations: 231
currently lose_sum: 374.33903378248215
time_elpased: 1.229
batch start
#iterations: 232
currently lose_sum: 374.18408530950546
time_elpased: 1.221
batch start
#iterations: 233
currently lose_sum: 374.17537039518356
time_elpased: 1.244
batch start
#iterations: 234
currently lose_sum: 373.83787375688553
time_elpased: 1.197
batch start
#iterations: 235
currently lose_sum: 374.0427911877632
time_elpased: 1.198
batch start
#iterations: 236
currently lose_sum: 373.40309751033783
time_elpased: 1.188
batch start
#iterations: 237
currently lose_sum: 373.1830582022667
time_elpased: 1.173
batch start
#iterations: 238
currently lose_sum: 374.14109390974045
time_elpased: 1.182
batch start
#iterations: 239
currently lose_sum: 373.57633924484253
time_elpased: 1.163
start validation test
0.783298969072
0.919300398895
0.619648397104
0.74030145787
0
validation finish
batch start
#iterations: 240
currently lose_sum: 372.6596794128418
time_elpased: 1.189
batch start
#iterations: 241
currently lose_sum: 373.2054558992386
time_elpased: 1.177
batch start
#iterations: 242
currently lose_sum: 372.0549250841141
time_elpased: 1.197
batch start
#iterations: 243
currently lose_sum: 374.42657428979874
time_elpased: 1.204
batch start
#iterations: 244
currently lose_sum: 374.4314522743225
time_elpased: 1.179
batch start
#iterations: 245
currently lose_sum: 373.7798247933388
time_elpased: 1.197
batch start
#iterations: 246
currently lose_sum: 374.0436520576477
time_elpased: 1.221
batch start
#iterations: 247
currently lose_sum: 374.48955619335175
time_elpased: 1.233
batch start
#iterations: 248
currently lose_sum: 374.7069448828697
time_elpased: 1.2
batch start
#iterations: 249
currently lose_sum: 373.612045109272
time_elpased: 1.186
batch start
#iterations: 250
currently lose_sum: 374.1133515238762
time_elpased: 1.202
batch start
#iterations: 251
currently lose_sum: 374.7445700764656
time_elpased: 1.291
batch start
#iterations: 252
currently lose_sum: 374.9088090658188
time_elpased: 1.2
batch start
#iterations: 253
currently lose_sum: 373.07243794202805
time_elpased: 1.23
batch start
#iterations: 254
currently lose_sum: 374.33324736356735
time_elpased: 1.249
batch start
#iterations: 255
currently lose_sum: 374.4770541191101
time_elpased: 1.232
batch start
#iterations: 256
currently lose_sum: 373.76255279779434
time_elpased: 1.275
batch start
#iterations: 257
currently lose_sum: 374.15899008512497
time_elpased: 1.18
batch start
#iterations: 258
currently lose_sum: 374.49729377031326
time_elpased: 1.178
batch start
#iterations: 259
currently lose_sum: 373.38990169763565
time_elpased: 1.183
start validation test
0.758041237113
0.940198159943
0.549534643226
0.693643127529
0
validation finish
batch start
#iterations: 260
currently lose_sum: 373.50012868642807
time_elpased: 1.215
batch start
#iterations: 261
currently lose_sum: 372.72293895483017
time_elpased: 1.231
batch start
#iterations: 262
currently lose_sum: 374.1838638782501
time_elpased: 1.21
batch start
#iterations: 263
currently lose_sum: 376.08051043748856
time_elpased: 1.218
batch start
#iterations: 264
currently lose_sum: 374.0511306524277
time_elpased: 1.285
batch start
#iterations: 265
currently lose_sum: 373.5590053796768
time_elpased: 1.217
batch start
#iterations: 266
currently lose_sum: 374.0088521242142
time_elpased: 1.208
batch start
#iterations: 267
currently lose_sum: 373.6496294140816
time_elpased: 1.213
batch start
#iterations: 268
currently lose_sum: 373.8548371195793
time_elpased: 1.223
batch start
#iterations: 269
currently lose_sum: 374.1084802746773
time_elpased: 1.21
batch start
#iterations: 270
currently lose_sum: 372.89910942316055
time_elpased: 1.246
batch start
#iterations: 271
currently lose_sum: 373.65601086616516
time_elpased: 1.246
batch start
#iterations: 272
currently lose_sum: 374.27678430080414
time_elpased: 1.217
batch start
#iterations: 273
currently lose_sum: 374.2107227444649
time_elpased: 1.228
batch start
#iterations: 274
currently lose_sum: 374.63369649648666
time_elpased: 1.217
batch start
#iterations: 275
currently lose_sum: 374.4091693162918
time_elpased: 1.262
batch start
#iterations: 276
currently lose_sum: 372.43697756528854
time_elpased: 1.18
batch start
#iterations: 277
currently lose_sum: 374.0636959671974
time_elpased: 1.218
batch start
#iterations: 278
currently lose_sum: 373.15991312265396
time_elpased: 1.198
batch start
#iterations: 279
currently lose_sum: 373.0864210128784
time_elpased: 1.186
start validation test
0.797422680412
0.902073409919
0.665873836608
0.766182770109
0
validation finish
batch start
#iterations: 280
currently lose_sum: 372.661537528038
time_elpased: 1.212
batch start
#iterations: 281
currently lose_sum: 374.57462042570114
time_elpased: 1.169
batch start
#iterations: 282
currently lose_sum: 374.4188812971115
time_elpased: 1.181
batch start
#iterations: 283
currently lose_sum: 373.47736287117004
time_elpased: 1.179
batch start
#iterations: 284
currently lose_sum: 374.6784898042679
time_elpased: 1.178
batch start
#iterations: 285
currently lose_sum: 372.7132561802864
time_elpased: 1.169
batch start
#iterations: 286
currently lose_sum: 373.52699464559555
time_elpased: 1.165
batch start
#iterations: 287
currently lose_sum: 372.3803340792656
time_elpased: 1.175
batch start
#iterations: 288
currently lose_sum: 374.0228605270386
time_elpased: 1.175
batch start
#iterations: 289
currently lose_sum: 373.54202377796173
time_elpased: 1.185
batch start
#iterations: 290
currently lose_sum: 372.9251541495323
time_elpased: 1.179
batch start
#iterations: 291
currently lose_sum: 372.8858207464218
time_elpased: 1.182
batch start
#iterations: 292
currently lose_sum: 373.71775209903717
time_elpased: 1.197
batch start
#iterations: 293
currently lose_sum: 374.098259806633
time_elpased: 1.227
batch start
#iterations: 294
currently lose_sum: 373.3282144665718
time_elpased: 1.267
batch start
#iterations: 295
currently lose_sum: 372.90614026784897
time_elpased: 1.233
batch start
#iterations: 296
currently lose_sum: 372.67170840501785
time_elpased: 1.211
batch start
#iterations: 297
currently lose_sum: 373.26173543930054
time_elpased: 1.182
batch start
#iterations: 298
currently lose_sum: 373.2096470594406
time_elpased: 1.193
batch start
#iterations: 299
currently lose_sum: 372.27868938446045
time_elpased: 1.198
start validation test
0.779432989691
0.92806098142
0.604343329886
0.732009770151
0
validation finish
batch start
#iterations: 300
currently lose_sum: 372.4534137248993
time_elpased: 1.235
batch start
#iterations: 301
currently lose_sum: 373.52186411619186
time_elpased: 1.203
batch start
#iterations: 302
currently lose_sum: 374.1098133325577
time_elpased: 1.221
batch start
#iterations: 303
currently lose_sum: 374.79999190568924
time_elpased: 1.256
batch start
#iterations: 304
currently lose_sum: 373.48745292425156
time_elpased: 1.193
batch start
#iterations: 305
currently lose_sum: 374.70486426353455
time_elpased: 1.203
batch start
#iterations: 306
currently lose_sum: 372.3000245690346
time_elpased: 1.195
batch start
#iterations: 307
currently lose_sum: 372.13019585609436
time_elpased: 1.203
batch start
#iterations: 308
currently lose_sum: 372.7646336555481
time_elpased: 1.248
batch start
#iterations: 309
currently lose_sum: 373.37214291095734
time_elpased: 1.185
batch start
#iterations: 310
currently lose_sum: 371.699287712574
time_elpased: 1.227
batch start
#iterations: 311
currently lose_sum: 374.29060101509094
time_elpased: 1.225
batch start
#iterations: 312
currently lose_sum: 372.1049289703369
time_elpased: 1.221
batch start
#iterations: 313
currently lose_sum: 372.1071540117264
time_elpased: 1.29
batch start
#iterations: 314
currently lose_sum: 373.4033770561218
time_elpased: 1.195
batch start
#iterations: 315
currently lose_sum: 374.0002906322479
time_elpased: 1.193
batch start
#iterations: 316
currently lose_sum: 373.1519720554352
time_elpased: 1.174
batch start
#iterations: 317
currently lose_sum: 373.395083963871
time_elpased: 1.187
batch start
#iterations: 318
currently lose_sum: 373.7651397585869
time_elpased: 1.177
batch start
#iterations: 319
currently lose_sum: 372.6489460468292
time_elpased: 1.181
start validation test
0.787886597938
0.914861837192
0.633402275078
0.74854873205
0
validation finish
batch start
#iterations: 320
currently lose_sum: 373.1459106206894
time_elpased: 1.25
batch start
#iterations: 321
currently lose_sum: 373.4238564968109
time_elpased: 1.25
batch start
#iterations: 322
currently lose_sum: 374.2754102945328
time_elpased: 1.266
batch start
#iterations: 323
currently lose_sum: 374.1970261335373
time_elpased: 1.214
batch start
#iterations: 324
currently lose_sum: 372.6128950715065
time_elpased: 1.205
batch start
#iterations: 325
currently lose_sum: 373.65501165390015
time_elpased: 1.178
batch start
#iterations: 326
currently lose_sum: 373.99702471494675
time_elpased: 1.193
batch start
#iterations: 327
currently lose_sum: 372.80265778303146
time_elpased: 1.183
batch start
#iterations: 328
currently lose_sum: 372.8580057621002
time_elpased: 1.193
batch start
#iterations: 329
currently lose_sum: 372.1414169073105
time_elpased: 1.184
batch start
#iterations: 330
currently lose_sum: 373.24015843868256
time_elpased: 1.191
batch start
#iterations: 331
currently lose_sum: 373.1025807261467
time_elpased: 1.225
batch start
#iterations: 332
currently lose_sum: 373.3983466029167
time_elpased: 1.197
batch start
#iterations: 333
currently lose_sum: 373.41992980241776
time_elpased: 1.205
batch start
#iterations: 334
currently lose_sum: 373.2228825688362
time_elpased: 1.206
batch start
#iterations: 335
currently lose_sum: 372.52269208431244
time_elpased: 1.185
batch start
#iterations: 336
currently lose_sum: 374.15829116106033
time_elpased: 1.219
batch start
#iterations: 337
currently lose_sum: 372.02877604961395
time_elpased: 1.19
batch start
#iterations: 338
currently lose_sum: 373.61902409791946
time_elpased: 1.183
batch start
#iterations: 339
currently lose_sum: 373.1885362267494
time_elpased: 1.201
start validation test
0.801030927835
0.896424672489
0.679317476732
0.772914460525
0
validation finish
batch start
#iterations: 340
currently lose_sum: 373.49201768636703
time_elpased: 1.206
batch start
#iterations: 341
currently lose_sum: 373.91875010728836
time_elpased: 1.192
batch start
#iterations: 342
currently lose_sum: 373.44609874486923
time_elpased: 1.202
batch start
#iterations: 343
currently lose_sum: 372.6586443781853
time_elpased: 1.199
batch start
#iterations: 344
currently lose_sum: 372.42351830005646
time_elpased: 1.166
batch start
#iterations: 345
currently lose_sum: 372.9504490494728
time_elpased: 1.168
batch start
#iterations: 346
currently lose_sum: 373.3325293660164
time_elpased: 1.195
batch start
#iterations: 347
currently lose_sum: 374.090860247612
time_elpased: 1.221
batch start
#iterations: 348
currently lose_sum: 371.39945244789124
time_elpased: 1.172
batch start
#iterations: 349
currently lose_sum: 373.3489717245102
time_elpased: 1.194
batch start
#iterations: 350
currently lose_sum: 373.50731563568115
time_elpased: 1.189
batch start
#iterations: 351
currently lose_sum: 373.34301537275314
time_elpased: 1.171
batch start
#iterations: 352
currently lose_sum: 373.8750605583191
time_elpased: 1.182
batch start
#iterations: 353
currently lose_sum: 373.09458404779434
time_elpased: 1.202
batch start
#iterations: 354
currently lose_sum: 372.46171379089355
time_elpased: 1.202
batch start
#iterations: 355
currently lose_sum: 372.3644337654114
time_elpased: 1.201
batch start
#iterations: 356
currently lose_sum: 372.0697479248047
time_elpased: 1.184
batch start
#iterations: 357
currently lose_sum: 373.1698840856552
time_elpased: 1.194
batch start
#iterations: 358
currently lose_sum: 372.7782893180847
time_elpased: 1.199
batch start
#iterations: 359
currently lose_sum: 371.2707681655884
time_elpased: 1.21
start validation test
0.802216494845
0.885219918109
0.693071354705
0.77744910388
0
validation finish
batch start
#iterations: 360
currently lose_sum: 373.37549364566803
time_elpased: 1.221
batch start
#iterations: 361
currently lose_sum: 373.2156026363373
time_elpased: 1.162
batch start
#iterations: 362
currently lose_sum: 372.80641877651215
time_elpased: 1.186
batch start
#iterations: 363
currently lose_sum: 374.53830885887146
time_elpased: 1.183
batch start
#iterations: 364
currently lose_sum: 372.1280171275139
time_elpased: 1.173
batch start
#iterations: 365
currently lose_sum: 372.9726701974869
time_elpased: 1.213
batch start
#iterations: 366
currently lose_sum: 373.7700282931328
time_elpased: 1.166
batch start
#iterations: 367
currently lose_sum: 373.75006598234177
time_elpased: 1.166
batch start
#iterations: 368
currently lose_sum: 372.49835073947906
time_elpased: 1.183
batch start
#iterations: 369
currently lose_sum: 371.45079958438873
time_elpased: 1.172
batch start
#iterations: 370
currently lose_sum: 372.3563694357872
time_elpased: 1.19
batch start
#iterations: 371
currently lose_sum: 372.8351065516472
time_elpased: 1.213
batch start
#iterations: 372
currently lose_sum: 372.35164880752563
time_elpased: 1.198
batch start
#iterations: 373
currently lose_sum: 373.40258544683456
time_elpased: 1.185
batch start
#iterations: 374
currently lose_sum: 373.11305916309357
time_elpased: 1.191
batch start
#iterations: 375
currently lose_sum: 371.507629096508
time_elpased: 1.19
batch start
#iterations: 376
currently lose_sum: 372.5423273444176
time_elpased: 1.192
batch start
#iterations: 377
currently lose_sum: 373.1647938489914
time_elpased: 1.191
batch start
#iterations: 378
currently lose_sum: 372.77887773513794
time_elpased: 1.163
batch start
#iterations: 379
currently lose_sum: 373.0895317196846
time_elpased: 1.197
start validation test
0.810567010309
0.863025311857
0.736918304033
0.795001952362
0
validation finish
batch start
#iterations: 380
currently lose_sum: 374.2507481575012
time_elpased: 1.187
batch start
#iterations: 381
currently lose_sum: 373.8594628572464
time_elpased: 1.179
batch start
#iterations: 382
currently lose_sum: 373.4066025018692
time_elpased: 1.225
batch start
#iterations: 383
currently lose_sum: 372.91991394758224
time_elpased: 1.203
batch start
#iterations: 384
currently lose_sum: 372.47005945444107
time_elpased: 1.19
batch start
#iterations: 385
currently lose_sum: 372.3253016471863
time_elpased: 1.194
batch start
#iterations: 386
currently lose_sum: 373.3514550924301
time_elpased: 1.196
batch start
#iterations: 387
currently lose_sum: 372.2619897723198
time_elpased: 1.184
batch start
#iterations: 388
currently lose_sum: 372.3619011044502
time_elpased: 1.18
batch start
#iterations: 389
currently lose_sum: 373.0613390803337
time_elpased: 1.196
batch start
#iterations: 390
currently lose_sum: 371.6796563863754
time_elpased: 1.18
batch start
#iterations: 391
currently lose_sum: 372.32787293195724
time_elpased: 1.166
batch start
#iterations: 392
currently lose_sum: 371.4995172023773
time_elpased: 1.173
batch start
#iterations: 393
currently lose_sum: 373.3699113726616
time_elpased: 1.19
batch start
#iterations: 394
currently lose_sum: 372.945576608181
time_elpased: 1.187
batch start
#iterations: 395
currently lose_sum: 372.95073771476746
time_elpased: 1.163
batch start
#iterations: 396
currently lose_sum: 372.919526219368
time_elpased: 1.179
batch start
#iterations: 397
currently lose_sum: 372.14033639431
time_elpased: 1.155
batch start
#iterations: 398
currently lose_sum: 372.8355718255043
time_elpased: 1.173
batch start
#iterations: 399
currently lose_sum: 371.38260543346405
time_elpased: 1.184
start validation test
0.80118556701
0.897442909887
0.678697001034
0.772890537596
0
validation finish
acc: 0.809
pre: 0.864
rec: 0.737
F1: 0.795
auc: 0.000
