start to construct graph
graph construct over
58302
epochs start
batch start
#iterations: 0
currently lose_sum: 401.34337735176086
time_elpased: 1.194
batch start
#iterations: 1
currently lose_sum: 397.03769969940186
time_elpased: 1.143
batch start
#iterations: 2
currently lose_sum: 396.17826437950134
time_elpased: 1.147
batch start
#iterations: 3
currently lose_sum: 394.7461771965027
time_elpased: 1.152
batch start
#iterations: 4
currently lose_sum: 394.07690316438675
time_elpased: 1.178
batch start
#iterations: 5
currently lose_sum: 392.3739097714424
time_elpased: 1.2
batch start
#iterations: 6
currently lose_sum: 392.87279719114304
time_elpased: 1.171
batch start
#iterations: 7
currently lose_sum: 390.3882133960724
time_elpased: 1.146
batch start
#iterations: 8
currently lose_sum: 391.18284636735916
time_elpased: 1.16
batch start
#iterations: 9
currently lose_sum: 390.3954603075981
time_elpased: 1.166
batch start
#iterations: 10
currently lose_sum: 389.97023141384125
time_elpased: 1.156
batch start
#iterations: 11
currently lose_sum: 388.8409830927849
time_elpased: 1.153
batch start
#iterations: 12
currently lose_sum: 388.2811096906662
time_elpased: 1.165
batch start
#iterations: 13
currently lose_sum: 388.5189759731293
time_elpased: 1.166
batch start
#iterations: 14
currently lose_sum: 388.8725691437721
time_elpased: 1.16
batch start
#iterations: 15
currently lose_sum: 387.57755529880524
time_elpased: 1.147
batch start
#iterations: 16
currently lose_sum: 387.22101080417633
time_elpased: 1.139
batch start
#iterations: 17
currently lose_sum: 386.3423852324486
time_elpased: 1.141
batch start
#iterations: 18
currently lose_sum: 385.9177971482277
time_elpased: 1.163
batch start
#iterations: 19
currently lose_sum: 385.73941028118134
time_elpased: 1.152
start validation test
0.738917525773
0.709236947791
0.814742669674
0.758337706952
0
validation finish
batch start
#iterations: 20
currently lose_sum: 384.3751436471939
time_elpased: 1.141
batch start
#iterations: 21
currently lose_sum: 386.3927466273308
time_elpased: 1.155
batch start
#iterations: 22
currently lose_sum: 384.6462156176567
time_elpased: 1.141
batch start
#iterations: 23
currently lose_sum: 385.0524848103523
time_elpased: 1.137
batch start
#iterations: 24
currently lose_sum: 383.4629691839218
time_elpased: 1.151
batch start
#iterations: 25
currently lose_sum: 384.74453353881836
time_elpased: 1.134
batch start
#iterations: 26
currently lose_sum: 383.3197420835495
time_elpased: 1.152
batch start
#iterations: 27
currently lose_sum: 383.4997090101242
time_elpased: 1.142
batch start
#iterations: 28
currently lose_sum: 383.55237317085266
time_elpased: 1.15
batch start
#iterations: 29
currently lose_sum: 384.0897213816643
time_elpased: 1.146
batch start
#iterations: 30
currently lose_sum: 382.74379074573517
time_elpased: 1.14
batch start
#iterations: 31
currently lose_sum: 383.1317679286003
time_elpased: 1.14
batch start
#iterations: 32
currently lose_sum: 382.7437843680382
time_elpased: 1.165
batch start
#iterations: 33
currently lose_sum: 382.16871869564056
time_elpased: 1.152
batch start
#iterations: 34
currently lose_sum: 382.1479399204254
time_elpased: 1.138
batch start
#iterations: 35
currently lose_sum: 380.6413651108742
time_elpased: 1.142
batch start
#iterations: 36
currently lose_sum: 382.6235527396202
time_elpased: 1.148
batch start
#iterations: 37
currently lose_sum: 381.7549312710762
time_elpased: 1.148
batch start
#iterations: 38
currently lose_sum: 382.3571548461914
time_elpased: 1.163
batch start
#iterations: 39
currently lose_sum: 382.3460240960121
time_elpased: 1.159
start validation test
0.758917525773
0.725224026262
0.838015173262
0.777550535077
0
validation finish
batch start
#iterations: 40
currently lose_sum: 380.9749897122383
time_elpased: 1.183
batch start
#iterations: 41
currently lose_sum: 382.0292541384697
time_elpased: 1.135
batch start
#iterations: 42
currently lose_sum: 380.81627839803696
time_elpased: 1.148
batch start
#iterations: 43
currently lose_sum: 381.04827481508255
time_elpased: 1.14
batch start
#iterations: 44
currently lose_sum: 380.16627180576324
time_elpased: 1.127
batch start
#iterations: 45
currently lose_sum: 381.10225319862366
time_elpased: 1.148
batch start
#iterations: 46
currently lose_sum: 380.42609137296677
time_elpased: 1.147
batch start
#iterations: 47
currently lose_sum: 380.8089289665222
time_elpased: 1.154
batch start
#iterations: 48
currently lose_sum: 381.6310805082321
time_elpased: 1.177
batch start
#iterations: 49
currently lose_sum: 380.92359787225723
time_elpased: 1.151
batch start
#iterations: 50
currently lose_sum: 380.4710865020752
time_elpased: 1.143
batch start
#iterations: 51
currently lose_sum: 379.83416533470154
time_elpased: 1.157
batch start
#iterations: 52
currently lose_sum: 379.8155429959297
time_elpased: 1.161
batch start
#iterations: 53
currently lose_sum: 380.1685242652893
time_elpased: 1.154
batch start
#iterations: 54
currently lose_sum: 379.9186398386955
time_elpased: 1.124
batch start
#iterations: 55
currently lose_sum: 379.49297004938126
time_elpased: 1.164
batch start
#iterations: 56
currently lose_sum: 380.3274031281471
time_elpased: 1.143
batch start
#iterations: 57
currently lose_sum: 378.848321557045
time_elpased: 1.146
batch start
#iterations: 58
currently lose_sum: 380.0083392858505
time_elpased: 1.137
batch start
#iterations: 59
currently lose_sum: 380.0303404331207
time_elpased: 1.135
start validation test
0.722783505155
0.938476953908
0.480110723806
0.635241454151
0
validation finish
batch start
#iterations: 60
currently lose_sum: 379.158179461956
time_elpased: 1.165
batch start
#iterations: 61
currently lose_sum: 380.5410839319229
time_elpased: 1.143
batch start
#iterations: 62
currently lose_sum: 379.17358630895615
time_elpased: 1.144
batch start
#iterations: 63
currently lose_sum: 380.2445647120476
time_elpased: 1.13
batch start
#iterations: 64
currently lose_sum: 379.5294305086136
time_elpased: 1.145
batch start
#iterations: 65
currently lose_sum: 378.22661286592484
time_elpased: 1.134
batch start
#iterations: 66
currently lose_sum: 378.51782447099686
time_elpased: 1.151
batch start
#iterations: 67
currently lose_sum: 378.58467614650726
time_elpased: 1.16
batch start
#iterations: 68
currently lose_sum: 378.0984069108963
time_elpased: 1.164
batch start
#iterations: 69
currently lose_sum: 377.9899559020996
time_elpased: 1.159
batch start
#iterations: 70
currently lose_sum: 378.8309054374695
time_elpased: 1.125
batch start
#iterations: 71
currently lose_sum: 379.4737437963486
time_elpased: 1.142
batch start
#iterations: 72
currently lose_sum: 377.42841798067093
time_elpased: 1.133
batch start
#iterations: 73
currently lose_sum: 378.27191734313965
time_elpased: 1.133
batch start
#iterations: 74
currently lose_sum: 378.7036494612694
time_elpased: 1.155
batch start
#iterations: 75
currently lose_sum: 378.9134911894798
time_elpased: 1.156
batch start
#iterations: 76
currently lose_sum: 378.4750049710274
time_elpased: 1.15
batch start
#iterations: 77
currently lose_sum: 378.15161007642746
time_elpased: 1.128
batch start
#iterations: 78
currently lose_sum: 379.1275441646576
time_elpased: 1.137
batch start
#iterations: 79
currently lose_sum: 376.63160741329193
time_elpased: 1.143
start validation test
0.783298969072
0.865516332982
0.673672339553
0.75763864868
0
validation finish
batch start
#iterations: 80
currently lose_sum: 379.0577994585037
time_elpased: 1.17
batch start
#iterations: 81
currently lose_sum: 379.55706775188446
time_elpased: 1.157
batch start
#iterations: 82
currently lose_sum: 376.8097525238991
time_elpased: 1.145
batch start
#iterations: 83
currently lose_sum: 376.0548824071884
time_elpased: 1.172
batch start
#iterations: 84
currently lose_sum: 377.61233991384506
time_elpased: 1.144
batch start
#iterations: 85
currently lose_sum: 377.4884665608406
time_elpased: 1.147
batch start
#iterations: 86
currently lose_sum: 377.4455918073654
time_elpased: 1.151
batch start
#iterations: 87
currently lose_sum: 377.9039743542671
time_elpased: 1.167
batch start
#iterations: 88
currently lose_sum: 377.38913333415985
time_elpased: 1.171
batch start
#iterations: 89
currently lose_sum: 377.7846412062645
time_elpased: 1.137
batch start
#iterations: 90
currently lose_sum: 377.85426861047745
time_elpased: 1.171
batch start
#iterations: 91
currently lose_sum: 375.99040311574936
time_elpased: 1.134
batch start
#iterations: 92
currently lose_sum: 378.94481617212296
time_elpased: 1.155
batch start
#iterations: 93
currently lose_sum: 377.3907780647278
time_elpased: 1.152
batch start
#iterations: 94
currently lose_sum: 379.5140332579613
time_elpased: 1.174
batch start
#iterations: 95
currently lose_sum: 376.97480046749115
time_elpased: 1.148
batch start
#iterations: 96
currently lose_sum: 378.46872186660767
time_elpased: 1.166
batch start
#iterations: 97
currently lose_sum: 376.78050088882446
time_elpased: 1.153
batch start
#iterations: 98
currently lose_sum: 377.0593252182007
time_elpased: 1.156
batch start
#iterations: 99
currently lose_sum: 376.31034326553345
time_elpased: 1.163
start validation test
0.771701030928
0.908421537046
0.60713553414
0.727831377128
0
validation finish
batch start
#iterations: 100
currently lose_sum: 377.3843261003494
time_elpased: 1.183
batch start
#iterations: 101
currently lose_sum: 377.91871827840805
time_elpased: 1.134
batch start
#iterations: 102
currently lose_sum: 376.68548876047134
time_elpased: 1.137
batch start
#iterations: 103
currently lose_sum: 376.78187084198
time_elpased: 1.14
batch start
#iterations: 104
currently lose_sum: 376.638523042202
time_elpased: 1.154
batch start
#iterations: 105
currently lose_sum: 378.23139905929565
time_elpased: 1.151
batch start
#iterations: 106
currently lose_sum: 376.4923143982887
time_elpased: 1.13
batch start
#iterations: 107
currently lose_sum: 377.52015429735184
time_elpased: 1.134
batch start
#iterations: 108
currently lose_sum: 377.5704457163811
time_elpased: 1.139
batch start
#iterations: 109
currently lose_sum: 375.7113935947418
time_elpased: 1.145
batch start
#iterations: 110
currently lose_sum: 377.27477341890335
time_elpased: 1.17
batch start
#iterations: 111
currently lose_sum: 376.3926215171814
time_elpased: 1.133
batch start
#iterations: 112
currently lose_sum: 376.7483947277069
time_elpased: 1.156
batch start
#iterations: 113
currently lose_sum: 377.06856352090836
time_elpased: 1.141
batch start
#iterations: 114
currently lose_sum: 376.1914608478546
time_elpased: 1.145
batch start
#iterations: 115
currently lose_sum: 375.9632135629654
time_elpased: 1.166
batch start
#iterations: 116
currently lose_sum: 377.14564311504364
time_elpased: 1.138
batch start
#iterations: 117
currently lose_sum: 376.65897619724274
time_elpased: 1.156
batch start
#iterations: 118
currently lose_sum: 375.85334116220474
time_elpased: 1.152
batch start
#iterations: 119
currently lose_sum: 377.03111535310745
time_elpased: 1.161
start validation test
0.734845360825
0.952493129172
0.497436948944
0.653556034483
0
validation finish
batch start
#iterations: 120
currently lose_sum: 376.12172281742096
time_elpased: 1.178
batch start
#iterations: 121
currently lose_sum: 376.73781460523605
time_elpased: 1.146
batch start
#iterations: 122
currently lose_sum: 376.2087813615799
time_elpased: 1.146
batch start
#iterations: 123
currently lose_sum: 375.91076946258545
time_elpased: 1.137
batch start
#iterations: 124
currently lose_sum: 375.39766877889633
time_elpased: 1.136
batch start
#iterations: 125
currently lose_sum: 375.74928522109985
time_elpased: 1.134
batch start
#iterations: 126
currently lose_sum: 375.0200651884079
time_elpased: 1.155
batch start
#iterations: 127
currently lose_sum: 374.98172003030777
time_elpased: 1.129
batch start
#iterations: 128
currently lose_sum: 376.47569221258163
time_elpased: 1.176
batch start
#iterations: 129
currently lose_sum: 376.218501329422
time_elpased: 1.157
batch start
#iterations: 130
currently lose_sum: 374.8931515812874
time_elpased: 1.138
batch start
#iterations: 131
currently lose_sum: 376.2423467040062
time_elpased: 1.144
batch start
#iterations: 132
currently lose_sum: 375.50967729091644
time_elpased: 1.142
batch start
#iterations: 133
currently lose_sum: 375.5238605737686
time_elpased: 1.172
batch start
#iterations: 134
currently lose_sum: 375.7123218178749
time_elpased: 1.158
batch start
#iterations: 135
currently lose_sum: 375.5472283959389
time_elpased: 1.132
batch start
#iterations: 136
currently lose_sum: 375.43520951271057
time_elpased: 1.148
batch start
#iterations: 137
currently lose_sum: 376.65970182418823
time_elpased: 1.132
batch start
#iterations: 138
currently lose_sum: 375.97204053401947
time_elpased: 1.14
batch start
#iterations: 139
currently lose_sum: 375.838859975338
time_elpased: 1.144
start validation test
0.73118556701
0.956731736768
0.487389788805
0.645792297765
0
validation finish
batch start
#iterations: 140
currently lose_sum: 376.04356265068054
time_elpased: 1.165
batch start
#iterations: 141
currently lose_sum: 376.1666225194931
time_elpased: 1.145
batch start
#iterations: 142
currently lose_sum: 375.83943140506744
time_elpased: 1.139
batch start
#iterations: 143
currently lose_sum: 375.99526530504227
time_elpased: 1.154
batch start
#iterations: 144
currently lose_sum: 377.0180891752243
time_elpased: 1.151
batch start
#iterations: 145
currently lose_sum: 375.9011372923851
time_elpased: 1.169
batch start
#iterations: 146
currently lose_sum: 375.9418403506279
time_elpased: 1.133
batch start
#iterations: 147
currently lose_sum: 376.1282298564911
time_elpased: 1.132
batch start
#iterations: 148
currently lose_sum: 376.3883274793625
time_elpased: 1.145
batch start
#iterations: 149
currently lose_sum: 375.26587051153183
time_elpased: 1.146
batch start
#iterations: 150
currently lose_sum: 375.3723168373108
time_elpased: 1.144
batch start
#iterations: 151
currently lose_sum: 375.128582239151
time_elpased: 1.152
batch start
#iterations: 152
currently lose_sum: 375.67875427007675
time_elpased: 1.142
batch start
#iterations: 153
currently lose_sum: 375.97766464948654
time_elpased: 1.154
batch start
#iterations: 154
currently lose_sum: 374.78232139348984
time_elpased: 1.136
batch start
#iterations: 155
currently lose_sum: 376.41286474466324
time_elpased: 1.144
batch start
#iterations: 156
currently lose_sum: 375.5853418111801
time_elpased: 1.148
batch start
#iterations: 157
currently lose_sum: 374.3996097445488
time_elpased: 1.124
batch start
#iterations: 158
currently lose_sum: 374.14566308259964
time_elpased: 1.145
batch start
#iterations: 159
currently lose_sum: 374.740768969059
time_elpased: 1.164
start validation test
0.801701030928
0.847511471938
0.738466270248
0.789240124911
0
validation finish
batch start
#iterations: 160
currently lose_sum: 376.88756424188614
time_elpased: 1.186
batch start
#iterations: 161
currently lose_sum: 375.58080047369003
time_elpased: 1.147
batch start
#iterations: 162
currently lose_sum: 374.6274352669716
time_elpased: 1.159
batch start
#iterations: 163
currently lose_sum: 376.84363251924515
time_elpased: 1.168
batch start
#iterations: 164
currently lose_sum: 375.3849436044693
time_elpased: 1.152
batch start
#iterations: 165
currently lose_sum: 375.6588870882988
time_elpased: 1.15
batch start
#iterations: 166
currently lose_sum: 375.4849918484688
time_elpased: 1.14
batch start
#iterations: 167
currently lose_sum: 374.7471947669983
time_elpased: 1.163
batch start
#iterations: 168
currently lose_sum: 375.0322126150131
time_elpased: 1.143
batch start
#iterations: 169
currently lose_sum: 375.1812190413475
time_elpased: 1.168
batch start
#iterations: 170
currently lose_sum: 374.6603181362152
time_elpased: 1.156
batch start
#iterations: 171
currently lose_sum: 375.4218726158142
time_elpased: 1.176
batch start
#iterations: 172
currently lose_sum: 374.5582585334778
time_elpased: 1.148
batch start
#iterations: 173
currently lose_sum: 374.991079390049
time_elpased: 1.144
batch start
#iterations: 174
currently lose_sum: 374.8845751285553
time_elpased: 1.143
batch start
#iterations: 175
currently lose_sum: 374.45969289541245
time_elpased: 1.181
batch start
#iterations: 176
currently lose_sum: 375.16879361867905
time_elpased: 1.172
batch start
#iterations: 177
currently lose_sum: 375.53659266233444
time_elpased: 1.162
batch start
#iterations: 178
currently lose_sum: 375.1436330676079
time_elpased: 1.169
batch start
#iterations: 179
currently lose_sum: 374.8637196421623
time_elpased: 1.169
start validation test
0.797268041237
0.88544563634
0.68546237441
0.772724646056
0
validation finish
batch start
#iterations: 180
currently lose_sum: 373.9089099764824
time_elpased: 1.183
batch start
#iterations: 181
currently lose_sum: 374.061271071434
time_elpased: 1.155
batch start
#iterations: 182
currently lose_sum: 374.51900029182434
time_elpased: 1.135
batch start
#iterations: 183
currently lose_sum: 375.17481261491776
time_elpased: 1.156
batch start
#iterations: 184
currently lose_sum: 374.02788281440735
time_elpased: 1.148
batch start
#iterations: 185
currently lose_sum: 374.09663158655167
time_elpased: 1.129
batch start
#iterations: 186
currently lose_sum: 374.45127034187317
time_elpased: 1.157
batch start
#iterations: 187
currently lose_sum: 374.89280807971954
time_elpased: 1.156
batch start
#iterations: 188
currently lose_sum: 373.3258084654808
time_elpased: 1.149
batch start
#iterations: 189
currently lose_sum: 373.55256432294846
time_elpased: 1.151
batch start
#iterations: 190
currently lose_sum: 374.1701912879944
time_elpased: 1.14
batch start
#iterations: 191
currently lose_sum: 373.3984586596489
time_elpased: 1.17
batch start
#iterations: 192
currently lose_sum: 375.4001615047455
time_elpased: 1.126
batch start
#iterations: 193
currently lose_sum: 373.19092828035355
time_elpased: 1.175
batch start
#iterations: 194
currently lose_sum: 373.83628636598587
time_elpased: 1.146
batch start
#iterations: 195
currently lose_sum: 373.4367774128914
time_elpased: 1.151
batch start
#iterations: 196
currently lose_sum: 376.4477353692055
time_elpased: 1.191
batch start
#iterations: 197
currently lose_sum: 374.6972813010216
time_elpased: 1.171
batch start
#iterations: 198
currently lose_sum: 374.5931494832039
time_elpased: 1.198
batch start
#iterations: 199
currently lose_sum: 374.90141797065735
time_elpased: 1.165
start validation test
0.794690721649
0.888828998787
0.676235390609
0.768093158661
0
validation finish
batch start
#iterations: 200
currently lose_sum: 375.61944007873535
time_elpased: 1.197
batch start
#iterations: 201
currently lose_sum: 373.4277848005295
time_elpased: 1.189
batch start
#iterations: 202
currently lose_sum: 373.9151816368103
time_elpased: 1.174
batch start
#iterations: 203
currently lose_sum: 374.8725925087929
time_elpased: 1.225
batch start
#iterations: 204
currently lose_sum: 375.2584304213524
time_elpased: 1.188
batch start
#iterations: 205
currently lose_sum: 373.792597591877
time_elpased: 1.234
batch start
#iterations: 206
currently lose_sum: 374.3928270339966
time_elpased: 1.2
batch start
#iterations: 207
currently lose_sum: 373.3280748128891
time_elpased: 1.174
batch start
#iterations: 208
currently lose_sum: 375.68193155527115
time_elpased: 1.213
batch start
#iterations: 209
currently lose_sum: 372.7773734331131
time_elpased: 1.164
batch start
#iterations: 210
currently lose_sum: 374.3913629055023
time_elpased: 1.234
batch start
#iterations: 211
currently lose_sum: 374.22857612371445
time_elpased: 1.194
batch start
#iterations: 212
currently lose_sum: 373.20152604579926
time_elpased: 1.194
batch start
#iterations: 213
currently lose_sum: 374.13949704170227
time_elpased: 1.256
batch start
#iterations: 214
currently lose_sum: 373.64495611190796
time_elpased: 1.173
batch start
#iterations: 215
currently lose_sum: 374.47552758455276
time_elpased: 1.192
batch start
#iterations: 216
currently lose_sum: 373.88180434703827
time_elpased: 1.216
batch start
#iterations: 217
currently lose_sum: 375.31666004657745
time_elpased: 1.18
batch start
#iterations: 218
currently lose_sum: 373.94640904664993
time_elpased: 1.203
batch start
#iterations: 219
currently lose_sum: 373.6991419196129
time_elpased: 1.218
start validation test
0.759639175258
0.944628820961
0.554439204429
0.698753149428
0
validation finish
batch start
#iterations: 220
currently lose_sum: 375.0016593337059
time_elpased: 1.208
batch start
#iterations: 221
currently lose_sum: 374.7394869327545
time_elpased: 1.212
batch start
#iterations: 222
currently lose_sum: 373.70469480752945
time_elpased: 1.207
batch start
#iterations: 223
currently lose_sum: 372.40779465436935
time_elpased: 1.189
batch start
#iterations: 224
currently lose_sum: 374.2282890677452
time_elpased: 1.202
batch start
#iterations: 225
currently lose_sum: 375.8060954809189
time_elpased: 1.182
batch start
#iterations: 226
currently lose_sum: 373.69903165102005
time_elpased: 1.259
batch start
#iterations: 227
currently lose_sum: 374.1654931306839
time_elpased: 1.199
batch start
#iterations: 228
currently lose_sum: 374.68278658390045
time_elpased: 1.192
batch start
#iterations: 229
currently lose_sum: 373.99248456954956
time_elpased: 1.201
batch start
#iterations: 230
currently lose_sum: 374.3926852941513
time_elpased: 1.188
batch start
#iterations: 231
currently lose_sum: 373.80663150548935
time_elpased: 1.238
batch start
#iterations: 232
currently lose_sum: 375.11745595932007
time_elpased: 1.222
batch start
#iterations: 233
currently lose_sum: 373.6549225449562
time_elpased: 1.202
batch start
#iterations: 234
currently lose_sum: 373.60308665037155
time_elpased: 1.23
batch start
#iterations: 235
currently lose_sum: 373.91670191287994
time_elpased: 1.207
batch start
#iterations: 236
currently lose_sum: 373.78786647319794
time_elpased: 1.243
batch start
#iterations: 237
currently lose_sum: 373.7366440296173
time_elpased: 1.204
batch start
#iterations: 238
currently lose_sum: 375.2644607424736
time_elpased: 1.21
batch start
#iterations: 239
currently lose_sum: 376.5722869038582
time_elpased: 1.227
start validation test
0.723969072165
0.970079076726
0.465347549723
0.628975265018
0
validation finish
batch start
#iterations: 240
currently lose_sum: 373.82984560728073
time_elpased: 1.203
batch start
#iterations: 241
currently lose_sum: 373.2141143679619
time_elpased: 1.167
batch start
#iterations: 242
currently lose_sum: 373.3296062350273
time_elpased: 1.188
batch start
#iterations: 243
currently lose_sum: 375.0028412938118
time_elpased: 1.139
batch start
#iterations: 244
currently lose_sum: 374.09253787994385
time_elpased: 1.182
batch start
#iterations: 245
currently lose_sum: 374.2081216573715
time_elpased: 1.176
batch start
#iterations: 246
currently lose_sum: 373.7590720653534
time_elpased: 1.157
batch start
#iterations: 247
currently lose_sum: 374.72212040424347
time_elpased: 1.168
batch start
#iterations: 248
currently lose_sum: 374.6066247224808
time_elpased: 1.162
batch start
#iterations: 249
currently lose_sum: 373.45827877521515
time_elpased: 1.154
batch start
#iterations: 250
currently lose_sum: 374.05921745300293
time_elpased: 1.165
batch start
#iterations: 251
currently lose_sum: 373.80471193790436
time_elpased: 1.165
batch start
#iterations: 252
currently lose_sum: 376.16544675827026
time_elpased: 1.175
batch start
#iterations: 253
currently lose_sum: 373.9669662117958
time_elpased: 1.153
batch start
#iterations: 254
currently lose_sum: 373.8714951276779
time_elpased: 1.187
batch start
#iterations: 255
currently lose_sum: 374.2529062628746
time_elpased: 1.149
batch start
#iterations: 256
currently lose_sum: 374.9549334049225
time_elpased: 1.171
batch start
#iterations: 257
currently lose_sum: 374.8031190633774
time_elpased: 1.15
batch start
#iterations: 258
currently lose_sum: 373.4710646867752
time_elpased: 1.166
batch start
#iterations: 259
currently lose_sum: 373.0083275437355
time_elpased: 1.149
start validation test
0.798402061856
0.89431772169
0.679311051876
0.772126085183
0
validation finish
batch start
#iterations: 260
currently lose_sum: 374.1666023135185
time_elpased: 1.179
batch start
#iterations: 261
currently lose_sum: 373.31399804353714
time_elpased: 1.163
batch start
#iterations: 262
currently lose_sum: 374.0035103559494
time_elpased: 1.179
batch start
#iterations: 263
currently lose_sum: 374.3033574819565
time_elpased: 1.158
batch start
#iterations: 264
currently lose_sum: 373.3622615337372
time_elpased: 1.177
batch start
#iterations: 265
currently lose_sum: 374.2320110797882
time_elpased: 1.169
batch start
#iterations: 266
currently lose_sum: 373.41597348451614
time_elpased: 1.186
batch start
#iterations: 267
currently lose_sum: 374.840751349926
time_elpased: 1.162
batch start
#iterations: 268
currently lose_sum: 372.9999405145645
time_elpased: 1.147
batch start
#iterations: 269
currently lose_sum: 373.8315899968147
time_elpased: 1.197
batch start
#iterations: 270
currently lose_sum: 373.5897750854492
time_elpased: 1.173
batch start
#iterations: 271
currently lose_sum: 373.0742175579071
time_elpased: 1.173
batch start
#iterations: 272
currently lose_sum: 373.28723883628845
time_elpased: 1.188
batch start
#iterations: 273
currently lose_sum: 373.7902890443802
time_elpased: 1.173
batch start
#iterations: 274
currently lose_sum: 373.86143732070923
time_elpased: 1.173
batch start
#iterations: 275
currently lose_sum: 373.362036883831
time_elpased: 1.196
batch start
#iterations: 276
currently lose_sum: 375.2509218454361
time_elpased: 1.204
batch start
#iterations: 277
currently lose_sum: 373.7617439031601
time_elpased: 1.187
batch start
#iterations: 278
currently lose_sum: 375.0023225545883
time_elpased: 1.168
batch start
#iterations: 279
currently lose_sum: 373.77857822179794
time_elpased: 1.225
start validation test
0.79824742268
0.893849473968
0.679413573918
0.772017707363
0
validation finish
batch start
#iterations: 280
currently lose_sum: 372.962385058403
time_elpased: 1.204
batch start
#iterations: 281
currently lose_sum: 373.3742670416832
time_elpased: 1.186
batch start
#iterations: 282
currently lose_sum: 374.8519116640091
time_elpased: 1.176
batch start
#iterations: 283
currently lose_sum: 375.3051958680153
time_elpased: 1.173
batch start
#iterations: 284
currently lose_sum: 373.6715648174286
time_elpased: 1.217
batch start
#iterations: 285
currently lose_sum: 373.5949640274048
time_elpased: 1.176
batch start
#iterations: 286
currently lose_sum: 372.841570854187
time_elpased: 1.184
batch start
#iterations: 287
currently lose_sum: 373.0157470703125
time_elpased: 1.195
batch start
#iterations: 288
currently lose_sum: 375.0164577960968
time_elpased: 1.194
batch start
#iterations: 289
currently lose_sum: 372.19482934474945
time_elpased: 1.182
batch start
#iterations: 290
currently lose_sum: 373.8518804907799
time_elpased: 1.171
batch start
#iterations: 291
currently lose_sum: 372.33830016851425
time_elpased: 1.249
batch start
#iterations: 292
currently lose_sum: 373.0088024735451
time_elpased: 1.192
batch start
#iterations: 293
currently lose_sum: 373.6511068344116
time_elpased: 1.191
batch start
#iterations: 294
currently lose_sum: 373.71330320835114
time_elpased: 1.203
batch start
#iterations: 295
currently lose_sum: 373.55493903160095
time_elpased: 1.188
batch start
#iterations: 296
currently lose_sum: 373.8428524136543
time_elpased: 1.225
batch start
#iterations: 297
currently lose_sum: 373.47551226615906
time_elpased: 1.215
batch start
#iterations: 298
currently lose_sum: 374.2785612940788
time_elpased: 1.224
batch start
#iterations: 299
currently lose_sum: 374.3598849773407
time_elpased: 1.24
start validation test
0.784639175258
0.918996092576
0.626922288292
0.745368113116
0
validation finish
batch start
#iterations: 300
currently lose_sum: 373.9823567867279
time_elpased: 1.224
batch start
#iterations: 301
currently lose_sum: 372.60781490802765
time_elpased: 1.189
batch start
#iterations: 302
currently lose_sum: 372.20621514320374
time_elpased: 1.2
batch start
#iterations: 303
currently lose_sum: 372.0098205804825
time_elpased: 1.175
batch start
#iterations: 304
currently lose_sum: 373.52673268318176
time_elpased: 1.212
batch start
#iterations: 305
currently lose_sum: 373.35829108953476
time_elpased: 1.182
batch start
#iterations: 306
currently lose_sum: 373.17777609825134
time_elpased: 1.19
batch start
#iterations: 307
currently lose_sum: 374.35047030448914
time_elpased: 1.166
batch start
#iterations: 308
currently lose_sum: 374.84847021102905
time_elpased: 1.214
batch start
#iterations: 309
currently lose_sum: 373.4772569537163
time_elpased: 1.206
batch start
#iterations: 310
currently lose_sum: 374.51467740535736
time_elpased: 1.2
batch start
#iterations: 311
currently lose_sum: 372.777920126915
time_elpased: 1.169
batch start
#iterations: 312
currently lose_sum: 373.25214755535126
time_elpased: 1.172
batch start
#iterations: 313
currently lose_sum: 374.7854000926018
time_elpased: 1.171
batch start
#iterations: 314
currently lose_sum: 372.5760347247124
time_elpased: 1.191
batch start
#iterations: 315
currently lose_sum: 372.1356711387634
time_elpased: 1.187
batch start
#iterations: 316
currently lose_sum: 375.2564794421196
time_elpased: 1.19
batch start
#iterations: 317
currently lose_sum: 372.20982110500336
time_elpased: 1.196
batch start
#iterations: 318
currently lose_sum: 371.79090970754623
time_elpased: 1.191
batch start
#iterations: 319
currently lose_sum: 372.99046355485916
time_elpased: 1.199
start validation test
0.766804123711
0.940978077572
0.572072995694
0.71155317521
0
validation finish
batch start
#iterations: 320
currently lose_sum: 372.2281896471977
time_elpased: 1.189
batch start
#iterations: 321
currently lose_sum: 374.8084707856178
time_elpased: 1.18
batch start
#iterations: 322
currently lose_sum: 374.0266416668892
time_elpased: 1.198
batch start
#iterations: 323
currently lose_sum: 371.6009339094162
time_elpased: 1.208
batch start
#iterations: 324
currently lose_sum: 373.07702845335007
time_elpased: 1.18
batch start
#iterations: 325
currently lose_sum: 373.2247511744499
time_elpased: 1.156
batch start
#iterations: 326
currently lose_sum: 374.045703291893
time_elpased: 1.163
batch start
#iterations: 327
currently lose_sum: 372.7207159399986
time_elpased: 1.218
batch start
#iterations: 328
currently lose_sum: 373.5113826394081
time_elpased: 1.143
batch start
#iterations: 329
currently lose_sum: 373.9748885035515
time_elpased: 1.188
batch start
#iterations: 330
currently lose_sum: 373.42566150426865
time_elpased: 1.166
batch start
#iterations: 331
currently lose_sum: 373.4360700249672
time_elpased: 1.163
batch start
#iterations: 332
currently lose_sum: 373.7623668909073
time_elpased: 1.147
batch start
#iterations: 333
currently lose_sum: 373.04955101013184
time_elpased: 1.169
batch start
#iterations: 334
currently lose_sum: 372.2079418897629
time_elpased: 1.182
batch start
#iterations: 335
currently lose_sum: 372.38900500535965
time_elpased: 1.179
batch start
#iterations: 336
currently lose_sum: 372.8400084376335
time_elpased: 1.191
batch start
#iterations: 337
currently lose_sum: 373.1725222468376
time_elpased: 1.179
batch start
#iterations: 338
currently lose_sum: 373.9788326025009
time_elpased: 1.165
batch start
#iterations: 339
currently lose_sum: 374.89436596632004
time_elpased: 1.199
start validation test
0.765051546392
0.94963655244
0.562538445766
0.70654133402
0
validation finish
batch start
#iterations: 340
currently lose_sum: 374.14412277936935
time_elpased: 1.193
batch start
#iterations: 341
currently lose_sum: 372.0074459910393
time_elpased: 1.196
batch start
#iterations: 342
currently lose_sum: 371.5310556292534
time_elpased: 1.201
batch start
#iterations: 343
currently lose_sum: 373.8636817932129
time_elpased: 1.215
batch start
#iterations: 344
currently lose_sum: 374.2294414639473
time_elpased: 1.212
batch start
#iterations: 345
currently lose_sum: 372.8819870352745
time_elpased: 1.193
batch start
#iterations: 346
currently lose_sum: 372.3284475207329
time_elpased: 1.206
batch start
#iterations: 347
currently lose_sum: 373.11799770593643
time_elpased: 1.22
batch start
#iterations: 348
currently lose_sum: 373.06916666030884
time_elpased: 1.182
batch start
#iterations: 349
currently lose_sum: 373.32058107852936
time_elpased: 1.236
batch start
#iterations: 350
currently lose_sum: 372.5616167187691
time_elpased: 1.221
batch start
#iterations: 351
currently lose_sum: 372.90586346387863
time_elpased: 1.228
batch start
#iterations: 352
currently lose_sum: 372.87579476833344
time_elpased: 1.248
batch start
#iterations: 353
currently lose_sum: 372.1115642786026
time_elpased: 1.197
batch start
#iterations: 354
currently lose_sum: 371.55281645059586
time_elpased: 1.217
batch start
#iterations: 355
currently lose_sum: 373.17862087488174
time_elpased: 1.2
batch start
#iterations: 356
currently lose_sum: 374.63259637355804
time_elpased: 1.231
batch start
#iterations: 357
currently lose_sum: 373.2418319582939
time_elpased: 1.223
batch start
#iterations: 358
currently lose_sum: 373.00985741615295
time_elpased: 1.211
batch start
#iterations: 359
currently lose_sum: 373.90879702568054
time_elpased: 1.255
start validation test
0.805257731959
0.877940804452
0.711605495181
0.786070215176
0
validation finish
batch start
#iterations: 360
currently lose_sum: 371.9111238718033
time_elpased: 1.273
batch start
#iterations: 361
currently lose_sum: 374.0758630633354
time_elpased: 1.2
batch start
#iterations: 362
currently lose_sum: 373.49257522821426
time_elpased: 1.249
batch start
#iterations: 363
currently lose_sum: 372.76767295598984
time_elpased: 1.237
batch start
#iterations: 364
currently lose_sum: 372.7870253920555
time_elpased: 1.247
batch start
#iterations: 365
currently lose_sum: 372.203565120697
time_elpased: 1.223
batch start
#iterations: 366
currently lose_sum: 373.399589240551
time_elpased: 1.209
batch start
#iterations: 367
currently lose_sum: 373.5477948784828
time_elpased: 1.172
batch start
#iterations: 368
currently lose_sum: 373.8064840435982
time_elpased: 1.242
batch start
#iterations: 369
currently lose_sum: 373.50423407554626
time_elpased: 1.232
batch start
#iterations: 370
currently lose_sum: 374.2069499492645
time_elpased: 1.238
batch start
#iterations: 371
currently lose_sum: 373.9151667356491
time_elpased: 1.209
batch start
#iterations: 372
currently lose_sum: 372.6142739057541
time_elpased: 1.19
batch start
#iterations: 373
currently lose_sum: 373.99445897340775
time_elpased: 1.245
batch start
#iterations: 374
currently lose_sum: 373.7201445698738
time_elpased: 1.24
batch start
#iterations: 375
currently lose_sum: 373.2177754044533
time_elpased: 1.267
batch start
#iterations: 376
currently lose_sum: 373.3915203809738
time_elpased: 1.227
batch start
#iterations: 377
currently lose_sum: 373.91594582796097
time_elpased: 1.237
batch start
#iterations: 378
currently lose_sum: 372.6234701871872
time_elpased: 1.253
batch start
#iterations: 379
currently lose_sum: 373.0137155056
time_elpased: 1.201
start validation test
0.805927835052
0.870468885315
0.721345089194
0.788921903908
0
validation finish
batch start
#iterations: 380
currently lose_sum: 373.3196830749512
time_elpased: 1.249
batch start
#iterations: 381
currently lose_sum: 372.76789873838425
time_elpased: 1.276
batch start
#iterations: 382
currently lose_sum: 373.4796487092972
time_elpased: 1.173
batch start
#iterations: 383
currently lose_sum: 372.84378784894943
time_elpased: 1.269
batch start
#iterations: 384
currently lose_sum: 373.12497967481613
time_elpased: 1.219
batch start
#iterations: 385
currently lose_sum: 371.55363541841507
time_elpased: 1.263
batch start
#iterations: 386
currently lose_sum: 373.07182371616364
time_elpased: 1.238
batch start
#iterations: 387
currently lose_sum: 373.90532487630844
time_elpased: 1.226
batch start
#iterations: 388
currently lose_sum: 372.9842035770416
time_elpased: 1.207
batch start
#iterations: 389
currently lose_sum: 373.34635972976685
time_elpased: 1.186
batch start
#iterations: 390
currently lose_sum: 374.55790996551514
time_elpased: 1.215
batch start
#iterations: 391
currently lose_sum: 372.9359807372093
time_elpased: 1.267
batch start
#iterations: 392
currently lose_sum: 373.04083228111267
time_elpased: 1.187
batch start
#iterations: 393
currently lose_sum: 371.7188449501991
time_elpased: 1.18
batch start
#iterations: 394
currently lose_sum: 372.1917181611061
time_elpased: 1.176
batch start
#iterations: 395
currently lose_sum: 372.568603515625
time_elpased: 1.163
batch start
#iterations: 396
currently lose_sum: 372.1951604485512
time_elpased: 1.153
batch start
#iterations: 397
currently lose_sum: 373.2519636154175
time_elpased: 1.173
batch start
#iterations: 398
currently lose_sum: 373.50895887613297
time_elpased: 1.175
batch start
#iterations: 399
currently lose_sum: 373.011699616909
time_elpased: 1.169
start validation test
0.801082474227
0.897397869759
0.682386713143
0.775260613826
0
validation finish
acc: 0.802
pre: 0.848
rec: 0.738
F1: 0.789
auc: 0.000
