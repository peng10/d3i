start to construct graph
graph construct over
58300
epochs start
batch start
#iterations: 0
currently lose_sum: 400.7848833799362
time_elpased: 1.149
batch start
#iterations: 1
currently lose_sum: 396.1737217903137
time_elpased: 1.09
batch start
#iterations: 2
currently lose_sum: 392.0131248831749
time_elpased: 1.093
batch start
#iterations: 3
currently lose_sum: 388.7285469174385
time_elpased: 1.102
batch start
#iterations: 4
currently lose_sum: 386.2477703690529
time_elpased: 1.104
batch start
#iterations: 5
currently lose_sum: 384.43152898550034
time_elpased: 1.09
batch start
#iterations: 6
currently lose_sum: 383.35679256916046
time_elpased: 1.087
batch start
#iterations: 7
currently lose_sum: 382.850255548954
time_elpased: 1.094
batch start
#iterations: 8
currently lose_sum: 380.56928676366806
time_elpased: 1.088
batch start
#iterations: 9
currently lose_sum: 378.82983285188675
time_elpased: 1.082
batch start
#iterations: 10
currently lose_sum: 378.46618616580963
time_elpased: 1.084
batch start
#iterations: 11
currently lose_sum: 378.18471002578735
time_elpased: 1.094
batch start
#iterations: 12
currently lose_sum: 377.12350565195084
time_elpased: 1.089
batch start
#iterations: 13
currently lose_sum: 376.5634549856186
time_elpased: 1.093
batch start
#iterations: 14
currently lose_sum: 375.25311845541
time_elpased: 1.093
batch start
#iterations: 15
currently lose_sum: 375.33873730897903
time_elpased: 1.098
batch start
#iterations: 16
currently lose_sum: 374.80514615774155
time_elpased: 1.091
batch start
#iterations: 17
currently lose_sum: 374.24247646331787
time_elpased: 1.084
batch start
#iterations: 18
currently lose_sum: 373.6392566561699
time_elpased: 1.097
batch start
#iterations: 19
currently lose_sum: 374.0007929801941
time_elpased: 1.092
start validation test
0.719432989691
0.868052035817
0.524285714286
0.653731153381
0
validation finish
batch start
#iterations: 20
currently lose_sum: 373.58175027370453
time_elpased: 1.095
batch start
#iterations: 21
currently lose_sum: 372.6086603999138
time_elpased: 1.099
batch start
#iterations: 22
currently lose_sum: 372.1006889343262
time_elpased: 1.086
batch start
#iterations: 23
currently lose_sum: 372.1841432452202
time_elpased: 1.09
batch start
#iterations: 24
currently lose_sum: 371.9323214292526
time_elpased: 1.091
batch start
#iterations: 25
currently lose_sum: 370.3288666009903
time_elpased: 1.092
batch start
#iterations: 26
currently lose_sum: 370.84475499391556
time_elpased: 1.091
batch start
#iterations: 27
currently lose_sum: 370.25579965114594
time_elpased: 1.078
batch start
#iterations: 28
currently lose_sum: 370.6845986843109
time_elpased: 1.088
batch start
#iterations: 29
currently lose_sum: 370.4518418312073
time_elpased: 1.107
batch start
#iterations: 30
currently lose_sum: 369.7212772369385
time_elpased: 1.078
batch start
#iterations: 31
currently lose_sum: 370.78949958086014
time_elpased: 1.078
batch start
#iterations: 32
currently lose_sum: 368.0896002650261
time_elpased: 1.083
batch start
#iterations: 33
currently lose_sum: 369.66302531957626
time_elpased: 1.076
batch start
#iterations: 34
currently lose_sum: 369.14736688137054
time_elpased: 1.087
batch start
#iterations: 35
currently lose_sum: 368.6050306558609
time_elpased: 1.098
batch start
#iterations: 36
currently lose_sum: 368.80043160915375
time_elpased: 1.1
batch start
#iterations: 37
currently lose_sum: 367.99431627988815
time_elpased: 1.09
batch start
#iterations: 38
currently lose_sum: 367.18397253751755
time_elpased: 1.099
batch start
#iterations: 39
currently lose_sum: 368.5500813126564
time_elpased: 1.09
start validation test
0.783917525773
0.796280642434
0.768979591837
0.782392026578
0
validation finish
batch start
#iterations: 40
currently lose_sum: 368.39850717782974
time_elpased: 1.095
batch start
#iterations: 41
currently lose_sum: 368.09262001514435
time_elpased: 1.08
batch start
#iterations: 42
currently lose_sum: 367.51428920030594
time_elpased: 1.085
batch start
#iterations: 43
currently lose_sum: 366.8300772309303
time_elpased: 1.091
batch start
#iterations: 44
currently lose_sum: 366.35688561201096
time_elpased: 1.089
batch start
#iterations: 45
currently lose_sum: 367.1465417742729
time_elpased: 1.083
batch start
#iterations: 46
currently lose_sum: 367.0007236003876
time_elpased: 1.091
batch start
#iterations: 47
currently lose_sum: 368.0600840449333
time_elpased: 1.095
batch start
#iterations: 48
currently lose_sum: 367.61492854356766
time_elpased: 1.096
batch start
#iterations: 49
currently lose_sum: 365.92659085989
time_elpased: 1.084
batch start
#iterations: 50
currently lose_sum: 367.97774428129196
time_elpased: 1.094
batch start
#iterations: 51
currently lose_sum: 365.48548609018326
time_elpased: 1.084
batch start
#iterations: 52
currently lose_sum: 365.2063698172569
time_elpased: 1.082
batch start
#iterations: 53
currently lose_sum: 365.30417740345
time_elpased: 1.084
batch start
#iterations: 54
currently lose_sum: 366.7281868457794
time_elpased: 1.088
batch start
#iterations: 55
currently lose_sum: 366.263592839241
time_elpased: 1.099
batch start
#iterations: 56
currently lose_sum: 366.69176441431046
time_elpased: 1.085
batch start
#iterations: 57
currently lose_sum: 364.7393029332161
time_elpased: 1.1
batch start
#iterations: 58
currently lose_sum: 365.06108754873276
time_elpased: 1.101
batch start
#iterations: 59
currently lose_sum: 365.68383395671844
time_elpased: 1.087
start validation test
0.793298969072
0.796983996717
0.792755102041
0.794863924698
0
validation finish
batch start
#iterations: 60
currently lose_sum: 366.0836223959923
time_elpased: 1.09
batch start
#iterations: 61
currently lose_sum: 365.54626190662384
time_elpased: 1.101
batch start
#iterations: 62
currently lose_sum: 366.87393736839294
time_elpased: 1.139
batch start
#iterations: 63
currently lose_sum: 364.36695832014084
time_elpased: 1.104
batch start
#iterations: 64
currently lose_sum: 365.35445362329483
time_elpased: 1.095
batch start
#iterations: 65
currently lose_sum: 363.8491696715355
time_elpased: 1.108
batch start
#iterations: 66
currently lose_sum: 364.3707240819931
time_elpased: 1.099
batch start
#iterations: 67
currently lose_sum: 365.322787463665
time_elpased: 1.089
batch start
#iterations: 68
currently lose_sum: 366.2331181168556
time_elpased: 1.097
batch start
#iterations: 69
currently lose_sum: 365.42888128757477
time_elpased: 1.089
batch start
#iterations: 70
currently lose_sum: 364.365920484066
time_elpased: 1.095
batch start
#iterations: 71
currently lose_sum: 365.2442868947983
time_elpased: 1.103
batch start
#iterations: 72
currently lose_sum: 364.3048235177994
time_elpased: 1.087
batch start
#iterations: 73
currently lose_sum: 364.7169721722603
time_elpased: 1.091
batch start
#iterations: 74
currently lose_sum: 364.49409180879593
time_elpased: 1.099
batch start
#iterations: 75
currently lose_sum: 363.18338561058044
time_elpased: 1.086
batch start
#iterations: 76
currently lose_sum: 362.63480204343796
time_elpased: 1.097
batch start
#iterations: 77
currently lose_sum: 364.18120431900024
time_elpased: 1.091
batch start
#iterations: 78
currently lose_sum: 363.3537934422493
time_elpased: 1.085
batch start
#iterations: 79
currently lose_sum: 364.0014299750328
time_elpased: 1.095
start validation test
0.790412371134
0.767444029851
0.839489795918
0.801851851852
0
validation finish
batch start
#iterations: 80
currently lose_sum: 362.51041889190674
time_elpased: 1.095
batch start
#iterations: 81
currently lose_sum: 363.9977566599846
time_elpased: 1.083
batch start
#iterations: 82
currently lose_sum: 363.47327810525894
time_elpased: 1.097
batch start
#iterations: 83
currently lose_sum: 362.8037601709366
time_elpased: 1.088
batch start
#iterations: 84
currently lose_sum: 364.53783202171326
time_elpased: 1.095
batch start
#iterations: 85
currently lose_sum: 362.62811464071274
time_elpased: 1.088
batch start
#iterations: 86
currently lose_sum: 362.2502484321594
time_elpased: 1.087
batch start
#iterations: 87
currently lose_sum: 363.63528656959534
time_elpased: 1.089
batch start
#iterations: 88
currently lose_sum: 361.77876710891724
time_elpased: 1.096
batch start
#iterations: 89
currently lose_sum: 363.21991616487503
time_elpased: 1.081
batch start
#iterations: 90
currently lose_sum: 362.0500079393387
time_elpased: 1.103
batch start
#iterations: 91
currently lose_sum: 361.7089613080025
time_elpased: 1.098
batch start
#iterations: 92
currently lose_sum: 363.1023476719856
time_elpased: 1.096
batch start
#iterations: 93
currently lose_sum: 362.30564564466476
time_elpased: 1.091
batch start
#iterations: 94
currently lose_sum: 362.39542031288147
time_elpased: 1.09
batch start
#iterations: 95
currently lose_sum: 362.8302513360977
time_elpased: 1.084
batch start
#iterations: 96
currently lose_sum: 361.1877720952034
time_elpased: 1.091
batch start
#iterations: 97
currently lose_sum: 362.28350096940994
time_elpased: 1.09
batch start
#iterations: 98
currently lose_sum: 362.1673316061497
time_elpased: 1.094
batch start
#iterations: 99
currently lose_sum: 362.96907514333725
time_elpased: 1.092
start validation test
0.798092783505
0.813960934998
0.778163265306
0.795659658824
0
validation finish
batch start
#iterations: 100
currently lose_sum: 361.22768700122833
time_elpased: 1.101
batch start
#iterations: 101
currently lose_sum: 360.90157729387283
time_elpased: 1.094
batch start
#iterations: 102
currently lose_sum: 361.30376026034355
time_elpased: 1.097
batch start
#iterations: 103
currently lose_sum: 362.2659057676792
time_elpased: 1.087
batch start
#iterations: 104
currently lose_sum: 362.06872004270554
time_elpased: 1.091
batch start
#iterations: 105
currently lose_sum: 362.0264554619789
time_elpased: 1.107
batch start
#iterations: 106
currently lose_sum: 363.1284855604172
time_elpased: 1.091
batch start
#iterations: 107
currently lose_sum: 361.891075193882
time_elpased: 1.089
batch start
#iterations: 108
currently lose_sum: 361.63725423812866
time_elpased: 1.095
batch start
#iterations: 109
currently lose_sum: 360.7932702898979
time_elpased: 1.09
batch start
#iterations: 110
currently lose_sum: 362.0960066318512
time_elpased: 1.095
batch start
#iterations: 111
currently lose_sum: 361.8094435930252
time_elpased: 1.103
batch start
#iterations: 112
currently lose_sum: 360.11999040842056
time_elpased: 1.088
batch start
#iterations: 113
currently lose_sum: 361.494605243206
time_elpased: 1.104
batch start
#iterations: 114
currently lose_sum: 361.0254068374634
time_elpased: 1.086
batch start
#iterations: 115
currently lose_sum: 362.4464349746704
time_elpased: 1.092
batch start
#iterations: 116
currently lose_sum: 360.50531256198883
time_elpased: 1.105
batch start
#iterations: 117
currently lose_sum: 361.75460439920425
time_elpased: 1.091
batch start
#iterations: 118
currently lose_sum: 359.8245415687561
time_elpased: 1.082
batch start
#iterations: 119
currently lose_sum: 360.5183280110359
time_elpased: 1.09
start validation test
0.790154639175
0.770977201778
0.831632653061
0.800157086054
0
validation finish
batch start
#iterations: 120
currently lose_sum: 360.9739734530449
time_elpased: 1.1
batch start
#iterations: 121
currently lose_sum: 361.28461569547653
time_elpased: 1.094
batch start
#iterations: 122
currently lose_sum: 360.2368407845497
time_elpased: 1.087
batch start
#iterations: 123
currently lose_sum: 358.98566007614136
time_elpased: 1.089
batch start
#iterations: 124
currently lose_sum: 361.6974774003029
time_elpased: 1.09
batch start
#iterations: 125
currently lose_sum: 360.47325789928436
time_elpased: 1.089
batch start
#iterations: 126
currently lose_sum: 361.1723583340645
time_elpased: 1.089
batch start
#iterations: 127
currently lose_sum: 359.5314858555794
time_elpased: 1.093
batch start
#iterations: 128
currently lose_sum: 359.92746007442474
time_elpased: 1.092
batch start
#iterations: 129
currently lose_sum: 362.4820969104767
time_elpased: 1.101
batch start
#iterations: 130
currently lose_sum: 359.63119173049927
time_elpased: 1.096
batch start
#iterations: 131
currently lose_sum: 359.8654163777828
time_elpased: 1.092
batch start
#iterations: 132
currently lose_sum: 359.3526765704155
time_elpased: 1.09
batch start
#iterations: 133
currently lose_sum: 359.91656798124313
time_elpased: 1.092
batch start
#iterations: 134
currently lose_sum: 359.84159302711487
time_elpased: 1.098
batch start
#iterations: 135
currently lose_sum: 361.71170580387115
time_elpased: 1.102
batch start
#iterations: 136
currently lose_sum: 360.67032527923584
time_elpased: 1.085
batch start
#iterations: 137
currently lose_sum: 359.20813339948654
time_elpased: 1.091
batch start
#iterations: 138
currently lose_sum: 361.685515999794
time_elpased: 1.086
batch start
#iterations: 139
currently lose_sum: 359.85096645355225
time_elpased: 1.096
start validation test
0.800154639175
0.790371605059
0.822551020408
0.806140307015
0
validation finish
batch start
#iterations: 140
currently lose_sum: 358.83603543043137
time_elpased: 1.095
batch start
#iterations: 141
currently lose_sum: 360.5786978006363
time_elpased: 1.103
batch start
#iterations: 142
currently lose_sum: 361.3041949868202
time_elpased: 1.092
batch start
#iterations: 143
currently lose_sum: 359.776614010334
time_elpased: 1.09
batch start
#iterations: 144
currently lose_sum: 359.5790753364563
time_elpased: 1.102
batch start
#iterations: 145
currently lose_sum: 361.1756191253662
time_elpased: 1.098
batch start
#iterations: 146
currently lose_sum: 360.4806761145592
time_elpased: 1.095
batch start
#iterations: 147
currently lose_sum: 360.49379658699036
time_elpased: 1.095
batch start
#iterations: 148
currently lose_sum: 361.58670687675476
time_elpased: 1.109
batch start
#iterations: 149
currently lose_sum: 359.40533900260925
time_elpased: 1.088
batch start
#iterations: 150
currently lose_sum: 357.8376145660877
time_elpased: 1.098
batch start
#iterations: 151
currently lose_sum: 360.44520729780197
time_elpased: 1.094
batch start
#iterations: 152
currently lose_sum: 360.10838705301285
time_elpased: 1.091
batch start
#iterations: 153
currently lose_sum: 360.1611891388893
time_elpased: 1.092
batch start
#iterations: 154
currently lose_sum: 359.50685757398605
time_elpased: 1.094
batch start
#iterations: 155
currently lose_sum: 360.1045960187912
time_elpased: 1.099
batch start
#iterations: 156
currently lose_sum: 361.3324431180954
time_elpased: 1.1
batch start
#iterations: 157
currently lose_sum: 359.46695056557655
time_elpased: 1.113
batch start
#iterations: 158
currently lose_sum: 358.0954136252403
time_elpased: 1.107
batch start
#iterations: 159
currently lose_sum: 360.6890268623829
time_elpased: 1.1
start validation test
0.800979381443
0.823792389052
0.770918367347
0.796478836118
0
validation finish
batch start
#iterations: 160
currently lose_sum: 360.3863579630852
time_elpased: 1.104
batch start
#iterations: 161
currently lose_sum: 360.3573177456856
time_elpased: 1.085
batch start
#iterations: 162
currently lose_sum: 360.30344331264496
time_elpased: 1.089
batch start
#iterations: 163
currently lose_sum: 359.89813005924225
time_elpased: 1.094
batch start
#iterations: 164
currently lose_sum: 359.87861001491547
time_elpased: 1.081
batch start
#iterations: 165
currently lose_sum: 360.77843248844147
time_elpased: 1.088
batch start
#iterations: 166
currently lose_sum: 359.9259443283081
time_elpased: 1.086
batch start
#iterations: 167
currently lose_sum: 358.73274260759354
time_elpased: 1.103
batch start
#iterations: 168
currently lose_sum: 359.1331576704979
time_elpased: 1.088
batch start
#iterations: 169
currently lose_sum: 359.9571101665497
time_elpased: 1.092
batch start
#iterations: 170
currently lose_sum: 357.41825264692307
time_elpased: 1.086
batch start
#iterations: 171
currently lose_sum: 359.0072876214981
time_elpased: 1.095
batch start
#iterations: 172
currently lose_sum: 360.0042870044708
time_elpased: 1.103
batch start
#iterations: 173
currently lose_sum: 359.3771574795246
time_elpased: 1.093
batch start
#iterations: 174
currently lose_sum: 359.9700554013252
time_elpased: 1.092
batch start
#iterations: 175
currently lose_sum: 358.58354783058167
time_elpased: 1.09
batch start
#iterations: 176
currently lose_sum: 359.2164282798767
time_elpased: 1.086
batch start
#iterations: 177
currently lose_sum: 359.9752268195152
time_elpased: 1.099
batch start
#iterations: 178
currently lose_sum: 359.0173954963684
time_elpased: 1.089
batch start
#iterations: 179
currently lose_sum: 359.68494260311127
time_elpased: 1.1
start validation test
0.801907216495
0.803155216285
0.805204081633
0.804178343949
0
validation finish
batch start
#iterations: 180
currently lose_sum: 360.4966898560524
time_elpased: 1.103
batch start
#iterations: 181
currently lose_sum: 359.06480544805527
time_elpased: 1.095
batch start
#iterations: 182
currently lose_sum: 360.0158176422119
time_elpased: 1.108
batch start
#iterations: 183
currently lose_sum: 358.11980777978897
time_elpased: 1.099
batch start
#iterations: 184
currently lose_sum: 358.4544499516487
time_elpased: 1.106
batch start
#iterations: 185
currently lose_sum: 359.93003284931183
time_elpased: 1.095
batch start
#iterations: 186
currently lose_sum: 356.3630141019821
time_elpased: 1.089
batch start
#iterations: 187
currently lose_sum: 359.8347323536873
time_elpased: 1.087
batch start
#iterations: 188
currently lose_sum: 359.31401294469833
time_elpased: 1.092
batch start
#iterations: 189
currently lose_sum: 358.9943755865097
time_elpased: 1.105
batch start
#iterations: 190
currently lose_sum: 358.4736538231373
time_elpased: 1.082
batch start
#iterations: 191
currently lose_sum: 358.2759765088558
time_elpased: 1.096
batch start
#iterations: 192
currently lose_sum: 359.6643167734146
time_elpased: 1.086
batch start
#iterations: 193
currently lose_sum: 358.5443810224533
time_elpased: 1.097
batch start
#iterations: 194
currently lose_sum: 357.56105691194534
time_elpased: 1.101
batch start
#iterations: 195
currently lose_sum: 358.2728206515312
time_elpased: 1.092
batch start
#iterations: 196
currently lose_sum: 359.02231615781784
time_elpased: 1.101
batch start
#iterations: 197
currently lose_sum: 358.7156839966774
time_elpased: 1.103
batch start
#iterations: 198
currently lose_sum: 359.007273375988
time_elpased: 1.103
batch start
#iterations: 199
currently lose_sum: 359.2042046189308
time_elpased: 1.09
start validation test
0.796494845361
0.808194649252
0.782959183673
0.795376801078
0
validation finish
batch start
#iterations: 200
currently lose_sum: 357.520332634449
time_elpased: 1.106
batch start
#iterations: 201
currently lose_sum: 358.6887419819832
time_elpased: 1.101
batch start
#iterations: 202
currently lose_sum: 358.2829062342644
time_elpased: 1.106
batch start
#iterations: 203
currently lose_sum: 359.64562568068504
time_elpased: 1.091
batch start
#iterations: 204
currently lose_sum: 357.7715095281601
time_elpased: 1.091
batch start
#iterations: 205
currently lose_sum: 358.1636491715908
time_elpased: 1.094
batch start
#iterations: 206
currently lose_sum: 358.9089549481869
time_elpased: 1.102
batch start
#iterations: 207
currently lose_sum: 359.18074357509613
time_elpased: 1.09
batch start
#iterations: 208
currently lose_sum: 358.6663349866867
time_elpased: 1.1
batch start
#iterations: 209
currently lose_sum: 358.8900158405304
time_elpased: 1.131
batch start
#iterations: 210
currently lose_sum: 358.0343570113182
time_elpased: 1.085
batch start
#iterations: 211
currently lose_sum: 357.01870253682137
time_elpased: 1.093
batch start
#iterations: 212
currently lose_sum: 358.01670730113983
time_elpased: 1.107
batch start
#iterations: 213
currently lose_sum: 356.5708666443825
time_elpased: 1.097
batch start
#iterations: 214
currently lose_sum: 358.61545902490616
time_elpased: 1.097
batch start
#iterations: 215
currently lose_sum: 359.59095925092697
time_elpased: 1.097
batch start
#iterations: 216
currently lose_sum: 358.28809809684753
time_elpased: 1.091
batch start
#iterations: 217
currently lose_sum: 357.9763733148575
time_elpased: 1.091
batch start
#iterations: 218
currently lose_sum: 359.074225127697
time_elpased: 1.091
batch start
#iterations: 219
currently lose_sum: 358.60847467184067
time_elpased: 1.092
start validation test
0.798144329897
0.802861848878
0.795816326531
0.79932356257
0
validation finish
batch start
#iterations: 220
currently lose_sum: 358.4982236623764
time_elpased: 1.106
batch start
#iterations: 221
currently lose_sum: 358.2874485254288
time_elpased: 1.099
batch start
#iterations: 222
currently lose_sum: 356.5863966345787
time_elpased: 1.086
batch start
#iterations: 223
currently lose_sum: 358.64316976070404
time_elpased: 1.093
batch start
#iterations: 224
currently lose_sum: 359.77092200517654
time_elpased: 1.099
batch start
#iterations: 225
currently lose_sum: 357.8577992320061
time_elpased: 1.097
batch start
#iterations: 226
currently lose_sum: 359.1309313774109
time_elpased: 1.097
batch start
#iterations: 227
currently lose_sum: 358.12906420230865
time_elpased: 1.102
batch start
#iterations: 228
currently lose_sum: 357.9100706577301
time_elpased: 1.106
batch start
#iterations: 229
currently lose_sum: 358.80562311410904
time_elpased: 1.097
batch start
#iterations: 230
currently lose_sum: 357.6612903177738
time_elpased: 1.101
batch start
#iterations: 231
currently lose_sum: 357.44573533535004
time_elpased: 1.098
batch start
#iterations: 232
currently lose_sum: 358.69047498703003
time_elpased: 1.089
batch start
#iterations: 233
currently lose_sum: 358.40528017282486
time_elpased: 1.084
batch start
#iterations: 234
currently lose_sum: 357.632303416729
time_elpased: 1.103
batch start
#iterations: 235
currently lose_sum: 358.6676276922226
time_elpased: 1.102
batch start
#iterations: 236
currently lose_sum: 357.6094859242439
time_elpased: 1.094
batch start
#iterations: 237
currently lose_sum: 358.3088992238045
time_elpased: 1.094
batch start
#iterations: 238
currently lose_sum: 356.4500760436058
time_elpased: 1.096
batch start
#iterations: 239
currently lose_sum: 358.3690645992756
time_elpased: 1.093
start validation test
0.800051546392
0.819604879629
0.774693877551
0.796516812674
0
validation finish
batch start
#iterations: 240
currently lose_sum: 356.9994662106037
time_elpased: 1.097
batch start
#iterations: 241
currently lose_sum: 358.2524424791336
time_elpased: 1.098
batch start
#iterations: 242
currently lose_sum: 358.1234582066536
time_elpased: 1.096
batch start
#iterations: 243
currently lose_sum: 357.2448963522911
time_elpased: 1.1
batch start
#iterations: 244
currently lose_sum: 357.47051152586937
time_elpased: 1.084
batch start
#iterations: 245
currently lose_sum: 358.34731048345566
time_elpased: 1.095
batch start
#iterations: 246
currently lose_sum: 358.0854241549969
time_elpased: 1.105
batch start
#iterations: 247
currently lose_sum: 357.803259909153
time_elpased: 1.094
batch start
#iterations: 248
currently lose_sum: 358.95372036099434
time_elpased: 1.082
batch start
#iterations: 249
currently lose_sum: 357.88779175281525
time_elpased: 1.095
batch start
#iterations: 250
currently lose_sum: 357.76625776290894
time_elpased: 1.084
batch start
#iterations: 251
currently lose_sum: 359.238118827343
time_elpased: 1.079
batch start
#iterations: 252
currently lose_sum: 357.8885221481323
time_elpased: 1.1
batch start
#iterations: 253
currently lose_sum: 358.4755650162697
time_elpased: 1.1
batch start
#iterations: 254
currently lose_sum: 358.26198095083237
time_elpased: 1.088
batch start
#iterations: 255
currently lose_sum: 356.6600465774536
time_elpased: 1.095
batch start
#iterations: 256
currently lose_sum: 357.95784705877304
time_elpased: 1.102
batch start
#iterations: 257
currently lose_sum: 358.20793575048447
time_elpased: 1.088
batch start
#iterations: 258
currently lose_sum: 357.00545313954353
time_elpased: 1.094
batch start
#iterations: 259
currently lose_sum: 356.08846390247345
time_elpased: 1.103
start validation test
0.801443298969
0.810827759197
0.791632653061
0.801115241636
0
validation finish
batch start
#iterations: 260
currently lose_sum: 356.64311999082565
time_elpased: 1.105
batch start
#iterations: 261
currently lose_sum: 357.8718423843384
time_elpased: 1.094
batch start
#iterations: 262
currently lose_sum: 358.5765942335129
time_elpased: 1.085
batch start
#iterations: 263
currently lose_sum: 356.88846242427826
time_elpased: 1.107
batch start
#iterations: 264
currently lose_sum: 357.50853791832924
time_elpased: 1.095
batch start
#iterations: 265
currently lose_sum: 357.86156606674194
time_elpased: 1.115
batch start
#iterations: 266
currently lose_sum: 358.62677147984505
time_elpased: 1.108
batch start
#iterations: 267
currently lose_sum: 356.48879596590996
time_elpased: 1.091
batch start
#iterations: 268
currently lose_sum: 357.5089208483696
time_elpased: 1.093
batch start
#iterations: 269
currently lose_sum: 357.31179505586624
time_elpased: 1.095
batch start
#iterations: 270
currently lose_sum: 358.18378990888596
time_elpased: 1.09
batch start
#iterations: 271
currently lose_sum: 357.9623852968216
time_elpased: 1.095
batch start
#iterations: 272
currently lose_sum: 356.9492581188679
time_elpased: 1.091
batch start
#iterations: 273
currently lose_sum: 357.72192001342773
time_elpased: 1.093
batch start
#iterations: 274
currently lose_sum: 357.9555498957634
time_elpased: 1.107
batch start
#iterations: 275
currently lose_sum: 357.7232769727707
time_elpased: 1.102
batch start
#iterations: 276
currently lose_sum: 357.4877931177616
time_elpased: 1.101
batch start
#iterations: 277
currently lose_sum: 358.45670706033707
time_elpased: 1.085
batch start
#iterations: 278
currently lose_sum: 355.9363062977791
time_elpased: 1.087
batch start
#iterations: 279
currently lose_sum: 357.3370822072029
time_elpased: 1.094
start validation test
0.803195876289
0.806706316653
0.802755102041
0.804725859247
0
validation finish
batch start
#iterations: 280
currently lose_sum: 356.9282642900944
time_elpased: 1.101
batch start
#iterations: 281
currently lose_sum: 357.4364303946495
time_elpased: 1.099
batch start
#iterations: 282
currently lose_sum: 357.8746168911457
time_elpased: 1.089
batch start
#iterations: 283
currently lose_sum: 358.15773701667786
time_elpased: 1.084
batch start
#iterations: 284
currently lose_sum: 357.96430146694183
time_elpased: 1.089
batch start
#iterations: 285
currently lose_sum: 357.5065450668335
time_elpased: 1.103
batch start
#iterations: 286
currently lose_sum: 356.89277017116547
time_elpased: 1.099
batch start
#iterations: 287
currently lose_sum: 358.02687042951584
time_elpased: 1.09
batch start
#iterations: 288
currently lose_sum: 356.3605897128582
time_elpased: 1.104
batch start
#iterations: 289
currently lose_sum: 357.2150926887989
time_elpased: 1.108
batch start
#iterations: 290
currently lose_sum: 357.3756159543991
time_elpased: 1.1
batch start
#iterations: 291
currently lose_sum: 356.7345259785652
time_elpased: 1.093
batch start
#iterations: 292
currently lose_sum: 357.7171781659126
time_elpased: 1.108
batch start
#iterations: 293
currently lose_sum: 357.10270994901657
time_elpased: 1.098
batch start
#iterations: 294
currently lose_sum: 357.2943421304226
time_elpased: 1.092
batch start
#iterations: 295
currently lose_sum: 356.6653776168823
time_elpased: 1.1
batch start
#iterations: 296
currently lose_sum: 356.22513818740845
time_elpased: 1.106
batch start
#iterations: 297
currently lose_sum: 357.27028834819794
time_elpased: 1.094
batch start
#iterations: 298
currently lose_sum: 356.7740923166275
time_elpased: 1.1
batch start
#iterations: 299
currently lose_sum: 357.0252076089382
time_elpased: 1.105
start validation test
0.802835051546
0.817650186071
0.784693877551
0.800833116376
0
validation finish
batch start
#iterations: 300
currently lose_sum: 357.9831541776657
time_elpased: 1.101
batch start
#iterations: 301
currently lose_sum: 356.0387792289257
time_elpased: 1.094
batch start
#iterations: 302
currently lose_sum: 357.4344853758812
time_elpased: 1.103
batch start
#iterations: 303
currently lose_sum: 356.96172565221786
time_elpased: 1.096
batch start
#iterations: 304
currently lose_sum: 358.0979046821594
time_elpased: 1.095
batch start
#iterations: 305
currently lose_sum: 357.1502568125725
time_elpased: 1.098
batch start
#iterations: 306
currently lose_sum: 356.7520245909691
time_elpased: 1.098
batch start
#iterations: 307
currently lose_sum: 356.03796803951263
time_elpased: 1.1
batch start
#iterations: 308
currently lose_sum: 355.7366843223572
time_elpased: 1.096
batch start
#iterations: 309
currently lose_sum: 356.54030814766884
time_elpased: 1.09
batch start
#iterations: 310
currently lose_sum: 357.1167555451393
time_elpased: 1.107
batch start
#iterations: 311
currently lose_sum: 357.46438351273537
time_elpased: 1.089
batch start
#iterations: 312
currently lose_sum: 356.110270857811
time_elpased: 1.094
batch start
#iterations: 313
currently lose_sum: 357.5760505795479
time_elpased: 1.093
batch start
#iterations: 314
currently lose_sum: 355.5138521194458
time_elpased: 1.12
batch start
#iterations: 315
currently lose_sum: 356.66855719685555
time_elpased: 1.096
batch start
#iterations: 316
currently lose_sum: 356.6701025366783
time_elpased: 1.087
batch start
#iterations: 317
currently lose_sum: 357.02495020627975
time_elpased: 1.097
batch start
#iterations: 318
currently lose_sum: 357.19003772735596
time_elpased: 1.108
batch start
#iterations: 319
currently lose_sum: 358.18811225891113
time_elpased: 1.104
start validation test
0.805824742268
0.817961420892
0.791836734694
0.804687094934
0
validation finish
batch start
#iterations: 320
currently lose_sum: 358.54062855243683
time_elpased: 1.1
batch start
#iterations: 321
currently lose_sum: 358.7880156636238
time_elpased: 1.106
batch start
#iterations: 322
currently lose_sum: 356.43276542425156
time_elpased: 1.101
batch start
#iterations: 323
currently lose_sum: 356.2147977948189
time_elpased: 1.099
batch start
#iterations: 324
currently lose_sum: 357.6381888985634
time_elpased: 1.104
batch start
#iterations: 325
currently lose_sum: 355.3954356312752
time_elpased: 1.097
batch start
#iterations: 326
currently lose_sum: 357.34597277641296
time_elpased: 1.099
batch start
#iterations: 327
currently lose_sum: 358.1114898324013
time_elpased: 1.086
batch start
#iterations: 328
currently lose_sum: 357.50377213954926
time_elpased: 1.102
batch start
#iterations: 329
currently lose_sum: 356.09894636273384
time_elpased: 1.091
batch start
#iterations: 330
currently lose_sum: 358.4017369747162
time_elpased: 1.094
batch start
#iterations: 331
currently lose_sum: 357.85361900925636
time_elpased: 1.108
batch start
#iterations: 332
currently lose_sum: 356.89389193058014
time_elpased: 1.092
batch start
#iterations: 333
currently lose_sum: 353.9930583536625
time_elpased: 1.095
batch start
#iterations: 334
currently lose_sum: 357.0219232439995
time_elpased: 1.089
batch start
#iterations: 335
currently lose_sum: 355.79551085829735
time_elpased: 1.099
batch start
#iterations: 336
currently lose_sum: 354.84918636083603
time_elpased: 1.092
batch start
#iterations: 337
currently lose_sum: 356.60091453790665
time_elpased: 1.104
batch start
#iterations: 338
currently lose_sum: 355.4698932170868
time_elpased: 1.111
batch start
#iterations: 339
currently lose_sum: 356.3322094678879
time_elpased: 1.097
start validation test
0.803608247423
0.815064169998
0.790612244898
0.802652025277
0
validation finish
batch start
#iterations: 340
currently lose_sum: 356.9987326860428
time_elpased: 1.106
batch start
#iterations: 341
currently lose_sum: 357.28817373514175
time_elpased: 1.088
batch start
#iterations: 342
currently lose_sum: 356.4807648360729
time_elpased: 1.092
batch start
#iterations: 343
currently lose_sum: 357.48817828297615
time_elpased: 1.094
batch start
#iterations: 344
currently lose_sum: 355.5042399466038
time_elpased: 1.115
batch start
#iterations: 345
currently lose_sum: 357.6268656849861
time_elpased: 1.091
batch start
#iterations: 346
currently lose_sum: 355.9673558473587
time_elpased: 1.104
batch start
#iterations: 347
currently lose_sum: 353.78737038373947
time_elpased: 1.086
batch start
#iterations: 348
currently lose_sum: 356.7932215332985
time_elpased: 1.098
batch start
#iterations: 349
currently lose_sum: 357.37982681393623
time_elpased: 1.087
batch start
#iterations: 350
currently lose_sum: 358.02337539196014
time_elpased: 1.093
batch start
#iterations: 351
currently lose_sum: 355.5113099217415
time_elpased: 1.099
batch start
#iterations: 352
currently lose_sum: 355.93155270814896
time_elpased: 1.09
batch start
#iterations: 353
currently lose_sum: 357.2720932364464
time_elpased: 1.091
batch start
#iterations: 354
currently lose_sum: 355.29037088155746
time_elpased: 1.095
batch start
#iterations: 355
currently lose_sum: 356.2798649072647
time_elpased: 1.095
batch start
#iterations: 356
currently lose_sum: 356.8841015100479
time_elpased: 1.086
batch start
#iterations: 357
currently lose_sum: 357.54953414201736
time_elpased: 1.091
batch start
#iterations: 358
currently lose_sum: 358.20125418901443
time_elpased: 1.096
batch start
#iterations: 359
currently lose_sum: 356.47724705934525
time_elpased: 1.098
start validation test
0.799639175258
0.796807549443
0.809897959184
0.803299428167
0
validation finish
batch start
#iterations: 360
currently lose_sum: 356.64870193600655
time_elpased: 1.109
batch start
#iterations: 361
currently lose_sum: 356.5543941259384
time_elpased: 1.095
batch start
#iterations: 362
currently lose_sum: 357.4531200528145
time_elpased: 1.092
batch start
#iterations: 363
currently lose_sum: 356.946416079998
time_elpased: 1.094
batch start
#iterations: 364
currently lose_sum: 356.80233255028725
time_elpased: 1.104
batch start
#iterations: 365
currently lose_sum: 358.42478734254837
time_elpased: 1.096
batch start
#iterations: 366
currently lose_sum: 357.52790236473083
time_elpased: 1.1
batch start
#iterations: 367
currently lose_sum: 357.7720683813095
time_elpased: 1.087
batch start
#iterations: 368
currently lose_sum: 356.8532502055168
time_elpased: 1.094
batch start
#iterations: 369
currently lose_sum: 354.8595412373543
time_elpased: 1.104
batch start
#iterations: 370
currently lose_sum: 356.70787900686264
time_elpased: 1.098
batch start
#iterations: 371
currently lose_sum: 356.71250385046005
time_elpased: 1.092
batch start
#iterations: 372
currently lose_sum: 354.2733711004257
time_elpased: 1.094
batch start
#iterations: 373
currently lose_sum: 356.63611727952957
time_elpased: 1.097
batch start
#iterations: 374
currently lose_sum: 355.65639171004295
time_elpased: 1.107
batch start
#iterations: 375
currently lose_sum: 357.8058613538742
time_elpased: 1.103
batch start
#iterations: 376
currently lose_sum: 356.08957129716873
time_elpased: 1.101
batch start
#iterations: 377
currently lose_sum: 357.093337059021
time_elpased: 1.103
batch start
#iterations: 378
currently lose_sum: 355.7921315431595
time_elpased: 1.086
batch start
#iterations: 379
currently lose_sum: 356.53128829598427
time_elpased: 1.096
start validation test
0.804690721649
0.807782898105
0.804897959184
0.806337848198
0
validation finish
batch start
#iterations: 380
currently lose_sum: 355.6358504295349
time_elpased: 1.097
batch start
#iterations: 381
currently lose_sum: 354.48828661441803
time_elpased: 1.089
batch start
#iterations: 382
currently lose_sum: 355.74719047546387
time_elpased: 1.1
batch start
#iterations: 383
currently lose_sum: 354.89399921894073
time_elpased: 1.093
batch start
#iterations: 384
currently lose_sum: 355.6903594136238
time_elpased: 1.106
batch start
#iterations: 385
currently lose_sum: 356.6387676000595
time_elpased: 1.098
batch start
#iterations: 386
currently lose_sum: 356.1368911266327
time_elpased: 1.102
batch start
#iterations: 387
currently lose_sum: 356.906018525362
time_elpased: 1.11
batch start
#iterations: 388
currently lose_sum: 354.8366829454899
time_elpased: 1.103
batch start
#iterations: 389
currently lose_sum: 355.88925713300705
time_elpased: 1.101
batch start
#iterations: 390
currently lose_sum: 358.23443150520325
time_elpased: 1.105
batch start
#iterations: 391
currently lose_sum: 355.9360002577305
time_elpased: 1.104
batch start
#iterations: 392
currently lose_sum: 355.24178245663643
time_elpased: 1.093
batch start
#iterations: 393
currently lose_sum: 356.58398139476776
time_elpased: 1.105
batch start
#iterations: 394
currently lose_sum: 356.35047459602356
time_elpased: 1.102
batch start
#iterations: 395
currently lose_sum: 356.24243664741516
time_elpased: 1.093
batch start
#iterations: 396
currently lose_sum: 355.3755810856819
time_elpased: 1.101
batch start
#iterations: 397
currently lose_sum: 357.25825464725494
time_elpased: 1.102
batch start
#iterations: 398
currently lose_sum: 356.0441520810127
time_elpased: 1.102
batch start
#iterations: 399
currently lose_sum: 355.98226231336594
time_elpased: 1.097
start validation test
0.803092783505
0.809459739184
0.79806122449
0.80372006988
0
validation finish
acc: 0.807
pre: 0.806
rec: 0.805
F1: 0.805
auc: 0.000
