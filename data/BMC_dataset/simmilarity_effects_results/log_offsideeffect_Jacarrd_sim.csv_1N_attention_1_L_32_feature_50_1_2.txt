start to construct graph
graph construct over
58300
epochs start
batch start
#iterations: 0
currently lose_sum: 401.01810669898987
time_elpased: 1.775
batch start
#iterations: 1
currently lose_sum: 396.2191666960716
time_elpased: 1.741
batch start
#iterations: 2
currently lose_sum: 391.65706795454025
time_elpased: 1.767
batch start
#iterations: 3
currently lose_sum: 389.5352791547775
time_elpased: 1.759
batch start
#iterations: 4
currently lose_sum: 386.80180537700653
time_elpased: 1.762
batch start
#iterations: 5
currently lose_sum: 385.15590035915375
time_elpased: 1.769
batch start
#iterations: 6
currently lose_sum: 383.5349078178406
time_elpased: 1.753
batch start
#iterations: 7
currently lose_sum: 382.2467179298401
time_elpased: 1.768
batch start
#iterations: 8
currently lose_sum: 381.4092535972595
time_elpased: 1.754
batch start
#iterations: 9
currently lose_sum: 381.95896154642105
time_elpased: 1.773
batch start
#iterations: 10
currently lose_sum: 377.59762984514236
time_elpased: 1.758
batch start
#iterations: 11
currently lose_sum: 378.1562389135361
time_elpased: 1.764
batch start
#iterations: 12
currently lose_sum: 377.8457703590393
time_elpased: 1.765
batch start
#iterations: 13
currently lose_sum: 377.9547963142395
time_elpased: 1.754
batch start
#iterations: 14
currently lose_sum: 373.6732546687126
time_elpased: 1.766
batch start
#iterations: 15
currently lose_sum: 376.1460288167
time_elpased: 1.762
batch start
#iterations: 16
currently lose_sum: 374.5260995030403
time_elpased: 1.78
batch start
#iterations: 17
currently lose_sum: 375.6167362332344
time_elpased: 1.761
batch start
#iterations: 18
currently lose_sum: 372.6996749639511
time_elpased: 1.763
batch start
#iterations: 19
currently lose_sum: 374.31731683015823
time_elpased: 1.762
start validation test
0.697577319588
0.871749755621
0.461116856256
0.603178897531
0
validation finish
batch start
#iterations: 20
currently lose_sum: 373.15036392211914
time_elpased: 1.761
batch start
#iterations: 21
currently lose_sum: 372.17959755659103
time_elpased: 1.77
batch start
#iterations: 22
currently lose_sum: 373.6535020470619
time_elpased: 1.763
batch start
#iterations: 23
currently lose_sum: 373.9315185546875
time_elpased: 1.765
batch start
#iterations: 24
currently lose_sum: 372.54840463399887
time_elpased: 1.756
batch start
#iterations: 25
currently lose_sum: 371.59230929613113
time_elpased: 1.767
batch start
#iterations: 26
currently lose_sum: 372.4615515470505
time_elpased: 1.76
batch start
#iterations: 27
currently lose_sum: 371.5929938554764
time_elpased: 1.774
batch start
#iterations: 28
currently lose_sum: 370.11796194314957
time_elpased: 1.766
batch start
#iterations: 29
currently lose_sum: 371.2328827381134
time_elpased: 1.766
batch start
#iterations: 30
currently lose_sum: 371.62413197755814
time_elpased: 1.757
batch start
#iterations: 31
currently lose_sum: 371.56111896038055
time_elpased: 1.758
batch start
#iterations: 32
currently lose_sum: 370.61462169885635
time_elpased: 1.769
batch start
#iterations: 33
currently lose_sum: 371.39792132377625
time_elpased: 1.756
batch start
#iterations: 34
currently lose_sum: 370.88200813531876
time_elpased: 1.77
batch start
#iterations: 35
currently lose_sum: 371.23316234350204
time_elpased: 1.763
batch start
#iterations: 36
currently lose_sum: 370.05617052316666
time_elpased: 1.772
batch start
#iterations: 37
currently lose_sum: 368.1798983812332
time_elpased: 1.757
batch start
#iterations: 38
currently lose_sum: 370.66416680812836
time_elpased: 1.773
batch start
#iterations: 39
currently lose_sum: 369.70054548978806
time_elpased: 1.764
start validation test
0.784278350515
0.792783174976
0.767942088935
0.780164941955
0
validation finish
batch start
#iterations: 40
currently lose_sum: 369.84138518571854
time_elpased: 1.763
batch start
#iterations: 41
currently lose_sum: 368.65902572870255
time_elpased: 1.759
batch start
#iterations: 42
currently lose_sum: 368.1588099002838
time_elpased: 1.76
batch start
#iterations: 43
currently lose_sum: 369.1538915634155
time_elpased: 1.771
batch start
#iterations: 44
currently lose_sum: 368.85590475797653
time_elpased: 1.762
batch start
#iterations: 45
currently lose_sum: 367.7923432588577
time_elpased: 1.781
batch start
#iterations: 46
currently lose_sum: 368.08351093530655
time_elpased: 1.76
batch start
#iterations: 47
currently lose_sum: 368.0487852692604
time_elpased: 1.776
batch start
#iterations: 48
currently lose_sum: 367.1237320303917
time_elpased: 1.773
batch start
#iterations: 49
currently lose_sum: 367.37652093172073
time_elpased: 1.767
batch start
#iterations: 50
currently lose_sum: 367.28505647182465
time_elpased: 1.769
batch start
#iterations: 51
currently lose_sum: 367.52882742881775
time_elpased: 1.759
batch start
#iterations: 52
currently lose_sum: 367.6865913271904
time_elpased: 1.776
batch start
#iterations: 53
currently lose_sum: 367.62881380319595
time_elpased: 1.755
batch start
#iterations: 54
currently lose_sum: 367.85064417123795
time_elpased: 1.766
batch start
#iterations: 55
currently lose_sum: 366.9545279741287
time_elpased: 1.759
batch start
#iterations: 56
currently lose_sum: 367.5162283182144
time_elpased: 1.76
batch start
#iterations: 57
currently lose_sum: 367.47785890102386
time_elpased: 1.764
batch start
#iterations: 58
currently lose_sum: 366.8910853564739
time_elpased: 1.761
batch start
#iterations: 59
currently lose_sum: 368.1463184952736
time_elpased: 1.771
start validation test
0.784381443299
0.823182942632
0.722647362978
0.769645905612
0
validation finish
batch start
#iterations: 60
currently lose_sum: 366.90574914216995
time_elpased: 1.765
batch start
#iterations: 61
currently lose_sum: 366.44319754838943
time_elpased: 1.768
batch start
#iterations: 62
currently lose_sum: 366.1345462799072
time_elpased: 1.759
batch start
#iterations: 63
currently lose_sum: 365.6883454322815
time_elpased: 1.775
batch start
#iterations: 64
currently lose_sum: 367.21833819150925
time_elpased: 1.77
batch start
#iterations: 65
currently lose_sum: 365.87973779439926
time_elpased: 1.764
batch start
#iterations: 66
currently lose_sum: 366.27803498506546
time_elpased: 1.768
batch start
#iterations: 67
currently lose_sum: 365.9671049416065
time_elpased: 1.771
batch start
#iterations: 68
currently lose_sum: 366.4232522249222
time_elpased: 1.764
batch start
#iterations: 69
currently lose_sum: 364.72231459617615
time_elpased: 1.76
batch start
#iterations: 70
currently lose_sum: 365.67409068346024
time_elpased: 1.761
batch start
#iterations: 71
currently lose_sum: 366.0852603316307
time_elpased: 1.761
batch start
#iterations: 72
currently lose_sum: 365.7708419561386
time_elpased: 1.774
batch start
#iterations: 73
currently lose_sum: 364.9159606099129
time_elpased: 1.772
batch start
#iterations: 74
currently lose_sum: 364.1778561472893
time_elpased: 1.767
batch start
#iterations: 75
currently lose_sum: 363.9752239584923
time_elpased: 1.757
batch start
#iterations: 76
currently lose_sum: 364.8370133638382
time_elpased: 1.767
batch start
#iterations: 77
currently lose_sum: 365.68332105875015
time_elpased: 1.762
batch start
#iterations: 78
currently lose_sum: 365.725672185421
time_elpased: 1.762
batch start
#iterations: 79
currently lose_sum: 364.7887904047966
time_elpased: 1.769
start validation test
0.787216494845
0.762604245641
0.832161323681
0.795865888636
0
validation finish
batch start
#iterations: 80
currently lose_sum: 364.33092230558395
time_elpased: 1.756
batch start
#iterations: 81
currently lose_sum: 366.29390597343445
time_elpased: 1.774
batch start
#iterations: 82
currently lose_sum: 364.99239033460617
time_elpased: 1.757
batch start
#iterations: 83
currently lose_sum: 365.1007659435272
time_elpased: 1.768
batch start
#iterations: 84
currently lose_sum: 366.2169135212898
time_elpased: 1.768
batch start
#iterations: 85
currently lose_sum: 365.4851019978523
time_elpased: 1.776
batch start
#iterations: 86
currently lose_sum: 365.23270201683044
time_elpased: 1.761
batch start
#iterations: 87
currently lose_sum: 364.54774272441864
time_elpased: 1.768
batch start
#iterations: 88
currently lose_sum: 364.01318085193634
time_elpased: 1.77
batch start
#iterations: 89
currently lose_sum: 363.90282744169235
time_elpased: 1.76
batch start
#iterations: 90
currently lose_sum: 365.8101817369461
time_elpased: 1.77
batch start
#iterations: 91
currently lose_sum: 364.19924890995026
time_elpased: 1.764
batch start
#iterations: 92
currently lose_sum: 362.8616336584091
time_elpased: 1.768
batch start
#iterations: 93
currently lose_sum: 365.2700127363205
time_elpased: 1.767
batch start
#iterations: 94
currently lose_sum: 364.713974237442
time_elpased: 1.769
batch start
#iterations: 95
currently lose_sum: 364.10015511512756
time_elpased: 1.768
batch start
#iterations: 96
currently lose_sum: 364.1114425063133
time_elpased: 1.767
batch start
#iterations: 97
currently lose_sum: 363.9689719080925
time_elpased: 1.775
batch start
#iterations: 98
currently lose_sum: 364.4250678420067
time_elpased: 1.761
batch start
#iterations: 99
currently lose_sum: 363.2961230278015
time_elpased: 1.765
start validation test
0.797216494845
0.826949384405
0.750155118925
0.786682572389
0
validation finish
batch start
#iterations: 100
currently lose_sum: 363.61309736967087
time_elpased: 1.759
batch start
#iterations: 101
currently lose_sum: 362.7735293507576
time_elpased: 1.771
batch start
#iterations: 102
currently lose_sum: 363.41521233320236
time_elpased: 1.768
batch start
#iterations: 103
currently lose_sum: 363.16311222314835
time_elpased: 1.775
batch start
#iterations: 104
currently lose_sum: 363.47691810131073
time_elpased: 1.766
batch start
#iterations: 105
currently lose_sum: 363.58736366033554
time_elpased: 1.767
batch start
#iterations: 106
currently lose_sum: 363.899261534214
time_elpased: 1.768
batch start
#iterations: 107
currently lose_sum: 363.20862102508545
time_elpased: 1.769
batch start
#iterations: 108
currently lose_sum: 363.5710577368736
time_elpased: 1.775
batch start
#iterations: 109
currently lose_sum: 362.6367275714874
time_elpased: 1.771
batch start
#iterations: 110
currently lose_sum: 364.1641495227814
time_elpased: 1.774
batch start
#iterations: 111
currently lose_sum: 362.7845317721367
time_elpased: 1.765
batch start
#iterations: 112
currently lose_sum: 362.81939047574997
time_elpased: 1.764
batch start
#iterations: 113
currently lose_sum: 363.1853108406067
time_elpased: 1.767
batch start
#iterations: 114
currently lose_sum: 362.5596430301666
time_elpased: 1.762
batch start
#iterations: 115
currently lose_sum: 362.36073994636536
time_elpased: 1.761
batch start
#iterations: 116
currently lose_sum: 362.94646060466766
time_elpased: 1.754
batch start
#iterations: 117
currently lose_sum: 362.1593049764633
time_elpased: 1.765
batch start
#iterations: 118
currently lose_sum: 361.82628709077835
time_elpased: 1.761
batch start
#iterations: 119
currently lose_sum: 362.4200139641762
time_elpased: 1.77
start validation test
0.797371134021
0.782960260329
0.821096173733
0.801574882641
0
validation finish
batch start
#iterations: 120
currently lose_sum: 362.96269631385803
time_elpased: 1.758
batch start
#iterations: 121
currently lose_sum: 362.9294135570526
time_elpased: 1.762
batch start
#iterations: 122
currently lose_sum: 359.83512458205223
time_elpased: 1.762
batch start
#iterations: 123
currently lose_sum: 362.14349615573883
time_elpased: 1.753
batch start
#iterations: 124
currently lose_sum: 361.3072877526283
time_elpased: 1.765
batch start
#iterations: 125
currently lose_sum: 362.7209315299988
time_elpased: 1.763
batch start
#iterations: 126
currently lose_sum: 363.48826187849045
time_elpased: 1.775
batch start
#iterations: 127
currently lose_sum: 362.46487134695053
time_elpased: 1.761
batch start
#iterations: 128
currently lose_sum: 361.8735668063164
time_elpased: 1.78
batch start
#iterations: 129
currently lose_sum: 363.1446325778961
time_elpased: 1.772
batch start
#iterations: 130
currently lose_sum: 362.3173585534096
time_elpased: 1.774
batch start
#iterations: 131
currently lose_sum: 361.39570140838623
time_elpased: 1.766
batch start
#iterations: 132
currently lose_sum: 362.2969380021095
time_elpased: 1.765
batch start
#iterations: 133
currently lose_sum: 362.07067608833313
time_elpased: 1.766
batch start
#iterations: 134
currently lose_sum: 362.00762689113617
time_elpased: 1.769
batch start
#iterations: 135
currently lose_sum: 362.06460481882095
time_elpased: 1.77
batch start
#iterations: 136
currently lose_sum: 363.73335617780685
time_elpased: 1.765
batch start
#iterations: 137
currently lose_sum: 361.4874637722969
time_elpased: 1.78
batch start
#iterations: 138
currently lose_sum: 363.24596577882767
time_elpased: 1.769
batch start
#iterations: 139
currently lose_sum: 361.44783544540405
time_elpased: 1.768
start validation test
0.799381443299
0.786607142857
0.819958634953
0.802936708861
0
validation finish
batch start
#iterations: 140
currently lose_sum: 362.33224868774414
time_elpased: 1.758
batch start
#iterations: 141
currently lose_sum: 363.26069861650467
time_elpased: 1.776
batch start
#iterations: 142
currently lose_sum: 362.77939707040787
time_elpased: 1.77
batch start
#iterations: 143
currently lose_sum: 362.1382702589035
time_elpased: 1.771
batch start
#iterations: 144
currently lose_sum: 362.0915595293045
time_elpased: 1.766
batch start
#iterations: 145
currently lose_sum: 362.0284255743027
time_elpased: 1.767
batch start
#iterations: 146
currently lose_sum: 360.75872853398323
time_elpased: 1.777
batch start
#iterations: 147
currently lose_sum: 361.62175846099854
time_elpased: 1.769
batch start
#iterations: 148
currently lose_sum: 361.5282304286957
time_elpased: 1.767
batch start
#iterations: 149
currently lose_sum: 362.3251676559448
time_elpased: 1.748
batch start
#iterations: 150
currently lose_sum: 361.5431841611862
time_elpased: 1.764
batch start
#iterations: 151
currently lose_sum: 361.2241808772087
time_elpased: 1.767
batch start
#iterations: 152
currently lose_sum: 360.4687929749489
time_elpased: 1.762
batch start
#iterations: 153
currently lose_sum: 363.4689476490021
time_elpased: 1.77
batch start
#iterations: 154
currently lose_sum: 361.34245443344116
time_elpased: 1.753
batch start
#iterations: 155
currently lose_sum: 361.97003930807114
time_elpased: 1.775
batch start
#iterations: 156
currently lose_sum: 361.3774291872978
time_elpased: 1.752
batch start
#iterations: 157
currently lose_sum: 361.05322217941284
time_elpased: 1.763
batch start
#iterations: 158
currently lose_sum: 361.3997151851654
time_elpased: 1.765
batch start
#iterations: 159
currently lose_sum: 363.0776206254959
time_elpased: 1.768
start validation test
0.800773195876
0.803133159269
0.795243019648
0.799168615225
0
validation finish
batch start
#iterations: 160
currently lose_sum: 361.492262840271
time_elpased: 1.769
batch start
#iterations: 161
currently lose_sum: 360.9995074868202
time_elpased: 1.769
batch start
#iterations: 162
currently lose_sum: 361.4784395098686
time_elpased: 1.775
batch start
#iterations: 163
currently lose_sum: 361.4935097694397
time_elpased: 1.768
batch start
#iterations: 164
currently lose_sum: 360.6342312693596
time_elpased: 1.772
batch start
#iterations: 165
currently lose_sum: 359.35404974222183
time_elpased: 1.768
batch start
#iterations: 166
currently lose_sum: 360.68981975317
time_elpased: 1.774
batch start
#iterations: 167
currently lose_sum: 361.8037391901016
time_elpased: 1.773
batch start
#iterations: 168
currently lose_sum: 360.7239478826523
time_elpased: 1.766
batch start
#iterations: 169
currently lose_sum: 360.22015357017517
time_elpased: 1.772
batch start
#iterations: 170
currently lose_sum: 362.07391810417175
time_elpased: 1.774
batch start
#iterations: 171
currently lose_sum: 360.3966871500015
time_elpased: 1.775
batch start
#iterations: 172
currently lose_sum: 360.14282488822937
time_elpased: 1.763
batch start
#iterations: 173
currently lose_sum: 361.60666459798813
time_elpased: 1.781
batch start
#iterations: 174
currently lose_sum: 359.79377976059914
time_elpased: 1.77
batch start
#iterations: 175
currently lose_sum: 361.8521674275398
time_elpased: 1.772
batch start
#iterations: 176
currently lose_sum: 361.1299047470093
time_elpased: 1.767
batch start
#iterations: 177
currently lose_sum: 360.1229475736618
time_elpased: 1.757
batch start
#iterations: 178
currently lose_sum: 361.46643006801605
time_elpased: 1.77
batch start
#iterations: 179
currently lose_sum: 361.4280570745468
time_elpased: 1.771
start validation test
0.800360824742
0.811231611726
0.781282316443
0.795975346363
0
validation finish
batch start
#iterations: 180
currently lose_sum: 361.4128956794739
time_elpased: 1.763
batch start
#iterations: 181
currently lose_sum: 358.96377444267273
time_elpased: 1.765
batch start
#iterations: 182
currently lose_sum: 360.3217411339283
time_elpased: 1.762
batch start
#iterations: 183
currently lose_sum: 361.1998297572136
time_elpased: 1.755
batch start
#iterations: 184
currently lose_sum: 360.35730266571045
time_elpased: 1.773
batch start
#iterations: 185
currently lose_sum: 360.368351995945
time_elpased: 1.753
batch start
#iterations: 186
currently lose_sum: 360.5606644451618
time_elpased: 1.759
batch start
#iterations: 187
currently lose_sum: 359.5505946278572
time_elpased: 1.76
batch start
#iterations: 188
currently lose_sum: 361.06900280714035
time_elpased: 1.765
batch start
#iterations: 189
currently lose_sum: 361.06003826856613
time_elpased: 1.77
batch start
#iterations: 190
currently lose_sum: 359.26118195056915
time_elpased: 1.782
batch start
#iterations: 191
currently lose_sum: 360.0178008675575
time_elpased: 1.772
batch start
#iterations: 192
currently lose_sum: 361.3156603574753
time_elpased: 1.771
batch start
#iterations: 193
currently lose_sum: 360.90388733148575
time_elpased: 1.785
batch start
#iterations: 194
currently lose_sum: 360.0014804005623
time_elpased: 1.763
batch start
#iterations: 195
currently lose_sum: 360.3403388261795
time_elpased: 1.78
batch start
#iterations: 196
currently lose_sum: 362.155720949173
time_elpased: 1.76
batch start
#iterations: 197
currently lose_sum: 360.55677914619446
time_elpased: 1.769
batch start
#iterations: 198
currently lose_sum: 360.9406889677048
time_elpased: 1.778
batch start
#iterations: 199
currently lose_sum: 360.48687279224396
time_elpased: 1.767
start validation test
0.803402061856
0.803545511093
0.801551189245
0.802547111203
0
validation finish
batch start
#iterations: 200
currently lose_sum: 360.7717584371567
time_elpased: 1.773
batch start
#iterations: 201
currently lose_sum: 358.861511528492
time_elpased: 1.769
batch start
#iterations: 202
currently lose_sum: 359.2383915781975
time_elpased: 1.763
batch start
#iterations: 203
currently lose_sum: 357.9772920012474
time_elpased: 1.772
batch start
#iterations: 204
currently lose_sum: 361.1443147659302
time_elpased: 1.779
batch start
#iterations: 205
currently lose_sum: 358.9014542400837
time_elpased: 1.763
batch start
#iterations: 206
currently lose_sum: 361.0966096520424
time_elpased: 1.771
batch start
#iterations: 207
currently lose_sum: 361.898717045784
time_elpased: 1.772
batch start
#iterations: 208
currently lose_sum: 360.06020230054855
time_elpased: 1.763
batch start
#iterations: 209
currently lose_sum: 359.89181488752365
time_elpased: 1.768
batch start
#iterations: 210
currently lose_sum: 359.41831481456757
time_elpased: 1.772
batch start
#iterations: 211
currently lose_sum: 360.04365307092667
time_elpased: 1.771
batch start
#iterations: 212
currently lose_sum: 360.03456699848175
time_elpased: 1.764
batch start
#iterations: 213
currently lose_sum: 360.42239159345627
time_elpased: 1.783
batch start
#iterations: 214
currently lose_sum: 360.3294471502304
time_elpased: 1.769
batch start
#iterations: 215
currently lose_sum: 359.1845139861107
time_elpased: 1.774
batch start
#iterations: 216
currently lose_sum: 360.51314955949783
time_elpased: 1.769
batch start
#iterations: 217
currently lose_sum: 360.0047445297241
time_elpased: 1.77
batch start
#iterations: 218
currently lose_sum: 360.1599544286728
time_elpased: 1.774
batch start
#iterations: 219
currently lose_sum: 360.65121257305145
time_elpased: 1.766
start validation test
0.801288659794
0.800952282372
0.800206825233
0.80057938027
0
validation finish
batch start
#iterations: 220
currently lose_sum: 359.06888848543167
time_elpased: 1.774
batch start
#iterations: 221
currently lose_sum: 359.8705619573593
time_elpased: 1.756
batch start
#iterations: 222
currently lose_sum: 360.0702695250511
time_elpased: 1.782
batch start
#iterations: 223
currently lose_sum: 361.0388182401657
time_elpased: 1.754
batch start
#iterations: 224
currently lose_sum: 360.68110781908035
time_elpased: 1.775
batch start
#iterations: 225
currently lose_sum: 359.52514666318893
time_elpased: 1.772
batch start
#iterations: 226
currently lose_sum: 359.19810128211975
time_elpased: 1.768
batch start
#iterations: 227
currently lose_sum: 360.3010941147804
time_elpased: 1.769
batch start
#iterations: 228
currently lose_sum: 360.1860341131687
time_elpased: 1.764
batch start
#iterations: 229
currently lose_sum: 359.3411223292351
time_elpased: 1.776
batch start
#iterations: 230
currently lose_sum: 359.69146955013275
time_elpased: 1.764
batch start
#iterations: 231
currently lose_sum: 360.14773058891296
time_elpased: 1.783
batch start
#iterations: 232
currently lose_sum: 360.33298152685165
time_elpased: 1.766
batch start
#iterations: 233
currently lose_sum: 359.5263724923134
time_elpased: 1.767
batch start
#iterations: 234
currently lose_sum: 360.1855140924454
time_elpased: 1.759
batch start
#iterations: 235
currently lose_sum: 358.3802490234375
time_elpased: 1.776
batch start
#iterations: 236
currently lose_sum: 359.49349904060364
time_elpased: 1.771
batch start
#iterations: 237
currently lose_sum: 359.61411559581757
time_elpased: 1.763
batch start
#iterations: 238
currently lose_sum: 359.82877510786057
time_elpased: 1.773
batch start
#iterations: 239
currently lose_sum: 357.8835263848305
time_elpased: 1.766
start validation test
0.79293814433
0.815633724176
0.755325749741
0.784322147651
0
validation finish
batch start
#iterations: 240
currently lose_sum: 358.2974611520767
time_elpased: 1.768
batch start
#iterations: 241
currently lose_sum: 358.6804183125496
time_elpased: 1.754
batch start
#iterations: 242
currently lose_sum: 357.74271699786186
time_elpased: 1.772
batch start
#iterations: 243
currently lose_sum: 359.3670751452446
time_elpased: 1.76
batch start
#iterations: 244
currently lose_sum: 359.9820554256439
time_elpased: 1.766
batch start
#iterations: 245
currently lose_sum: 358.922665476799
time_elpased: 1.753
batch start
#iterations: 246
currently lose_sum: 359.4030126929283
time_elpased: 1.755
batch start
#iterations: 247
currently lose_sum: 359.99926191568375
time_elpased: 1.758
batch start
#iterations: 248
currently lose_sum: 360.7003690004349
time_elpased: 1.763
batch start
#iterations: 249
currently lose_sum: 357.9898213148117
time_elpased: 1.767
batch start
#iterations: 250
currently lose_sum: 360.13228499889374
time_elpased: 1.757
batch start
#iterations: 251
currently lose_sum: 360.4849488735199
time_elpased: 1.778
batch start
#iterations: 252
currently lose_sum: 358.7112033367157
time_elpased: 1.763
batch start
#iterations: 253
currently lose_sum: 357.6954314112663
time_elpased: 1.782
batch start
#iterations: 254
currently lose_sum: 359.44949346780777
time_elpased: 1.761
batch start
#iterations: 255
currently lose_sum: 359.4500551223755
time_elpased: 1.768
batch start
#iterations: 256
currently lose_sum: 359.4988144338131
time_elpased: 1.766
batch start
#iterations: 257
currently lose_sum: 360.2835928797722
time_elpased: 1.764
batch start
#iterations: 258
currently lose_sum: 360.6143845319748
time_elpased: 1.77
batch start
#iterations: 259
currently lose_sum: 358.5561102628708
time_elpased: 1.761
start validation test
0.801237113402
0.808140767437
0.78841778697
0.798157453936
0
validation finish
batch start
#iterations: 260
currently lose_sum: 358.80137026309967
time_elpased: 1.773
batch start
#iterations: 261
currently lose_sum: 358.10109281539917
time_elpased: 1.756
batch start
#iterations: 262
currently lose_sum: 359.62866711616516
time_elpased: 1.769
batch start
#iterations: 263
currently lose_sum: 360.61235988140106
time_elpased: 1.768
batch start
#iterations: 264
currently lose_sum: 359.1674880385399
time_elpased: 1.762
batch start
#iterations: 265
currently lose_sum: 358.5824432373047
time_elpased: 1.774
batch start
#iterations: 266
currently lose_sum: 359.6833795905113
time_elpased: 1.761
batch start
#iterations: 267
currently lose_sum: 358.8107013106346
time_elpased: 1.762
batch start
#iterations: 268
currently lose_sum: 359.44062596559525
time_elpased: 1.754
batch start
#iterations: 269
currently lose_sum: 360.4931417107582
time_elpased: 1.779
batch start
#iterations: 270
currently lose_sum: 357.8781588077545
time_elpased: 1.768
batch start
#iterations: 271
currently lose_sum: 358.53697204589844
time_elpased: 1.772
batch start
#iterations: 272
currently lose_sum: 358.64069533348083
time_elpased: 1.768
batch start
#iterations: 273
currently lose_sum: 358.84057492017746
time_elpased: 1.771
batch start
#iterations: 274
currently lose_sum: 360.04510921239853
time_elpased: 1.773
batch start
#iterations: 275
currently lose_sum: 359.2112374007702
time_elpased: 1.755
batch start
#iterations: 276
currently lose_sum: 357.7256170511246
time_elpased: 1.766
batch start
#iterations: 277
currently lose_sum: 359.30342996120453
time_elpased: 1.743
batch start
#iterations: 278
currently lose_sum: 358.75562566518784
time_elpased: 1.775
batch start
#iterations: 279
currently lose_sum: 358.2363922595978
time_elpased: 1.767
start validation test
0.803505154639
0.796938361719
0.812926577042
0.804853076687
0
validation finish
batch start
#iterations: 280
currently lose_sum: 358.56368440389633
time_elpased: 1.768
batch start
#iterations: 281
currently lose_sum: 360.5441778898239
time_elpased: 1.775
batch start
#iterations: 282
currently lose_sum: 358.85997182130814
time_elpased: 1.773
batch start
#iterations: 283
currently lose_sum: 357.88018107414246
time_elpased: 1.768
batch start
#iterations: 284
currently lose_sum: 359.35713881254196
time_elpased: 1.77
batch start
#iterations: 285
currently lose_sum: 356.66752603650093
time_elpased: 1.765
batch start
#iterations: 286
currently lose_sum: 358.29574716091156
time_elpased: 1.762
batch start
#iterations: 287
currently lose_sum: 357.7115566134453
time_elpased: 1.78
batch start
#iterations: 288
currently lose_sum: 358.9374496936798
time_elpased: 1.765
batch start
#iterations: 289
currently lose_sum: 358.4015189409256
time_elpased: 1.766
batch start
#iterations: 290
currently lose_sum: 358.1420545578003
time_elpased: 1.764
batch start
#iterations: 291
currently lose_sum: 358.51584815979004
time_elpased: 1.773
batch start
#iterations: 292
currently lose_sum: 359.12410551309586
time_elpased: 1.77
batch start
#iterations: 293
currently lose_sum: 358.89126032590866
time_elpased: 1.777
batch start
#iterations: 294
currently lose_sum: 357.928138256073
time_elpased: 1.773
batch start
#iterations: 295
currently lose_sum: 358.49611061811447
time_elpased: 1.773
batch start
#iterations: 296
currently lose_sum: 357.5341246724129
time_elpased: 1.784
batch start
#iterations: 297
currently lose_sum: 357.8719515800476
time_elpased: 1.764
batch start
#iterations: 298
currently lose_sum: 357.059137403965
time_elpased: 1.766
batch start
#iterations: 299
currently lose_sum: 357.278961956501
time_elpased: 1.767
start validation test
0.79824742268
0.809329320722
0.778697001034
0.793717718984
0
validation finish
batch start
#iterations: 300
currently lose_sum: 358.1363712847233
time_elpased: 1.76
batch start
#iterations: 301
currently lose_sum: 358.8139226436615
time_elpased: 1.763
batch start
#iterations: 302
currently lose_sum: 360.097354888916
time_elpased: 1.763
batch start
#iterations: 303
currently lose_sum: 359.6024290919304
time_elpased: 1.768
batch start
#iterations: 304
currently lose_sum: 359.4514172077179
time_elpased: 1.785
batch start
#iterations: 305
currently lose_sum: 358.41133493185043
time_elpased: 1.788
batch start
#iterations: 306
currently lose_sum: 357.0655239224434
time_elpased: 1.776
batch start
#iterations: 307
currently lose_sum: 357.9134814143181
time_elpased: 1.786
batch start
#iterations: 308
currently lose_sum: 357.4435376524925
time_elpased: 1.769
batch start
#iterations: 309
currently lose_sum: 358.36026614904404
time_elpased: 1.778
batch start
#iterations: 310
currently lose_sum: 357.24374228715897
time_elpased: 1.77
batch start
#iterations: 311
currently lose_sum: 359.3030233979225
time_elpased: 1.77
batch start
#iterations: 312
currently lose_sum: 355.80830532312393
time_elpased: 1.775
batch start
#iterations: 313
currently lose_sum: 357.62902790308
time_elpased: 1.769
batch start
#iterations: 314
currently lose_sum: 359.3661944270134
time_elpased: 1.772
batch start
#iterations: 315
currently lose_sum: 358.7653218507767
time_elpased: 1.765
batch start
#iterations: 316
currently lose_sum: 357.1660258769989
time_elpased: 1.779
batch start
#iterations: 317
currently lose_sum: 359.7888095974922
time_elpased: 1.768
batch start
#iterations: 318
currently lose_sum: 359.1497046947479
time_elpased: 1.773
batch start
#iterations: 319
currently lose_sum: 358.12611305713654
time_elpased: 1.775
start validation test
0.800773195876
0.800808373925
0.799069286453
0.799937884984
0
validation finish
batch start
#iterations: 320
currently lose_sum: 357.57355612516403
time_elpased: 1.776
batch start
#iterations: 321
currently lose_sum: 358.9625408053398
time_elpased: 1.778
batch start
#iterations: 322
currently lose_sum: 358.39724165201187
time_elpased: 1.772
batch start
#iterations: 323
currently lose_sum: 359.7920921444893
time_elpased: 1.777
batch start
#iterations: 324
currently lose_sum: 359.02639240026474
time_elpased: 1.778
batch start
#iterations: 325
currently lose_sum: 358.40447092056274
time_elpased: 1.786
batch start
#iterations: 326
currently lose_sum: 358.51019340753555
time_elpased: 1.776
batch start
#iterations: 327
currently lose_sum: 357.8281860947609
time_elpased: 1.788
batch start
#iterations: 328
currently lose_sum: 357.1365679204464
time_elpased: 1.777
batch start
#iterations: 329
currently lose_sum: 357.5090796351433
time_elpased: 1.786
batch start
#iterations: 330
currently lose_sum: 359.0893900990486
time_elpased: 1.772
batch start
#iterations: 331
currently lose_sum: 357.45323318243027
time_elpased: 1.771
batch start
#iterations: 332
currently lose_sum: 358.4111454486847
time_elpased: 1.778
batch start
#iterations: 333
currently lose_sum: 357.98514354228973
time_elpased: 1.774
batch start
#iterations: 334
currently lose_sum: 357.6836870908737
time_elpased: 1.784
batch start
#iterations: 335
currently lose_sum: 357.62330746650696
time_elpased: 1.779
batch start
#iterations: 336
currently lose_sum: 359.124431848526
time_elpased: 1.789
batch start
#iterations: 337
currently lose_sum: 356.57499039173126
time_elpased: 1.785
batch start
#iterations: 338
currently lose_sum: 358.76766937971115
time_elpased: 1.779
batch start
#iterations: 339
currently lose_sum: 357.939149916172
time_elpased: 1.782
start validation test
0.802010309278
0.807469142315
0.79152016546
0.799415113061
0
validation finish
batch start
#iterations: 340
currently lose_sum: 357.82772678136826
time_elpased: 1.79
batch start
#iterations: 341
currently lose_sum: 359.41553395986557
time_elpased: 1.783
batch start
#iterations: 342
currently lose_sum: 357.2753967642784
time_elpased: 1.779
batch start
#iterations: 343
currently lose_sum: 357.11191511154175
time_elpased: 1.781
batch start
#iterations: 344
currently lose_sum: 356.5759184360504
time_elpased: 1.768
batch start
#iterations: 345
currently lose_sum: 357.7949330210686
time_elpased: 1.783
batch start
#iterations: 346
currently lose_sum: 357.9088716506958
time_elpased: 1.771
batch start
#iterations: 347
currently lose_sum: 358.30305528640747
time_elpased: 1.791
batch start
#iterations: 348
currently lose_sum: 356.20898485183716
time_elpased: 1.778
batch start
#iterations: 349
currently lose_sum: 357.4578593671322
time_elpased: 1.775
batch start
#iterations: 350
currently lose_sum: 358.8063774704933
time_elpased: 1.775
batch start
#iterations: 351
currently lose_sum: 358.2034882903099
time_elpased: 1.777
batch start
#iterations: 352
currently lose_sum: 359.3111079931259
time_elpased: 1.78
batch start
#iterations: 353
currently lose_sum: 357.7159655690193
time_elpased: 1.776
batch start
#iterations: 354
currently lose_sum: 356.74656915664673
time_elpased: 1.784
batch start
#iterations: 355
currently lose_sum: 356.98878532648087
time_elpased: 1.773
batch start
#iterations: 356
currently lose_sum: 358.32183891534805
time_elpased: 1.787
batch start
#iterations: 357
currently lose_sum: 357.7803046107292
time_elpased: 1.765
batch start
#iterations: 358
currently lose_sum: 356.7549642920494
time_elpased: 1.773
batch start
#iterations: 359
currently lose_sum: 356.2655317187309
time_elpased: 1.776
start validation test
0.801134020619
0.794785960641
0.810237849018
0.802437525604
0
validation finish
batch start
#iterations: 360
currently lose_sum: 358.5820733010769
time_elpased: 1.772
batch start
#iterations: 361
currently lose_sum: 357.7211859822273
time_elpased: 1.757
batch start
#iterations: 362
currently lose_sum: 356.5796808004379
time_elpased: 1.76
batch start
#iterations: 363
currently lose_sum: 359.3394013643265
time_elpased: 1.771
batch start
#iterations: 364
currently lose_sum: 356.5038878917694
time_elpased: 1.758
batch start
#iterations: 365
currently lose_sum: 357.53252607584
time_elpased: 1.771
batch start
#iterations: 366
currently lose_sum: 357.98643308877945
time_elpased: 1.756
batch start
#iterations: 367
currently lose_sum: 359.081311583519
time_elpased: 1.773
batch start
#iterations: 368
currently lose_sum: 357.2250561118126
time_elpased: 1.77
batch start
#iterations: 369
currently lose_sum: 355.7994017601013
time_elpased: 1.778
batch start
#iterations: 370
currently lose_sum: 357.2586433291435
time_elpased: 1.764
batch start
#iterations: 371
currently lose_sum: 358.1413580775261
time_elpased: 1.769
batch start
#iterations: 372
currently lose_sum: 358.3999073803425
time_elpased: 1.777
batch start
#iterations: 373
currently lose_sum: 358.05752712488174
time_elpased: 1.772
batch start
#iterations: 374
currently lose_sum: 357.7794353365898
time_elpased: 1.79
batch start
#iterations: 375
currently lose_sum: 355.9455876350403
time_elpased: 1.779
batch start
#iterations: 376
currently lose_sum: 357.6833384037018
time_elpased: 1.789
batch start
#iterations: 377
currently lose_sum: 358.21052157878876
time_elpased: 1.777
batch start
#iterations: 378
currently lose_sum: 357.5385971367359
time_elpased: 1.788
batch start
#iterations: 379
currently lose_sum: 357.5724025964737
time_elpased: 1.78
start validation test
0.800309278351
0.796743805038
0.804653567735
0.800679152089
0
validation finish
batch start
#iterations: 380
currently lose_sum: 358.2680532336235
time_elpased: 1.771
batch start
#iterations: 381
currently lose_sum: 358.1414785385132
time_elpased: 1.778
batch start
#iterations: 382
currently lose_sum: 357.34840044379234
time_elpased: 1.773
batch start
#iterations: 383
currently lose_sum: 357.8825623393059
time_elpased: 1.777
batch start
#iterations: 384
currently lose_sum: 357.5399438738823
time_elpased: 1.777
batch start
#iterations: 385
currently lose_sum: 357.23586773872375
time_elpased: 1.785
batch start
#iterations: 386
currently lose_sum: 357.6364452242851
time_elpased: 1.766
batch start
#iterations: 387
currently lose_sum: 357.01621040701866
time_elpased: 1.779
batch start
#iterations: 388
currently lose_sum: 357.5458224117756
time_elpased: 1.775
batch start
#iterations: 389
currently lose_sum: 357.0681081414223
time_elpased: 1.786
batch start
#iterations: 390
currently lose_sum: 356.5320625901222
time_elpased: 1.776
batch start
#iterations: 391
currently lose_sum: 356.7172284722328
time_elpased: 1.772
batch start
#iterations: 392
currently lose_sum: 357.2137757539749
time_elpased: 1.769
batch start
#iterations: 393
currently lose_sum: 357.9207230210304
time_elpased: 1.765
batch start
#iterations: 394
currently lose_sum: 358.29357796907425
time_elpased: 1.781
batch start
#iterations: 395
currently lose_sum: 358.4179172515869
time_elpased: 1.774
batch start
#iterations: 396
currently lose_sum: 358.1343961954117
time_elpased: 1.782
batch start
#iterations: 397
currently lose_sum: 356.16499984264374
time_elpased: 1.775
batch start
#iterations: 398
currently lose_sum: 358.3490977883339
time_elpased: 1.778
batch start
#iterations: 399
currently lose_sum: 355.9998768866062
time_elpased: 1.776
start validation test
0.796649484536
0.802813921506
0.784798345398
0.793703916749
0
validation finish
acc: 0.803
pre: 0.801
rec: 0.811
F1: 0.806
auc: 0.000
