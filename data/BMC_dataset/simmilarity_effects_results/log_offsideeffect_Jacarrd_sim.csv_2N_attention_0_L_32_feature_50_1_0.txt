start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 552.4353635907173
time_elpased: 2.127
batch start
#iterations: 1
currently lose_sum: 545.75584846735
time_elpased: 2.12
batch start
#iterations: 2
currently lose_sum: 544.7529355287552
time_elpased: 2.117
batch start
#iterations: 3
currently lose_sum: 542.4936588406563
time_elpased: 2.129
batch start
#iterations: 4
currently lose_sum: 540.6447048187256
time_elpased: 2.116
batch start
#iterations: 5
currently lose_sum: 539.2799903154373
time_elpased: 2.114
batch start
#iterations: 6
currently lose_sum: 537.2334243655205
time_elpased: 2.108
batch start
#iterations: 7
currently lose_sum: 536.9161819815636
time_elpased: 2.105
batch start
#iterations: 8
currently lose_sum: 537.3766298592091
time_elpased: 2.123
batch start
#iterations: 9
currently lose_sum: 536.4405407011509
time_elpased: 2.126
batch start
#iterations: 10
currently lose_sum: 536.3151007294655
time_elpased: 2.129
batch start
#iterations: 11
currently lose_sum: 537.3810496926308
time_elpased: 2.134
batch start
#iterations: 12
currently lose_sum: 535.2934927940369
time_elpased: 2.135
batch start
#iterations: 13
currently lose_sum: 533.7655093073845
time_elpased: 2.128
batch start
#iterations: 14
currently lose_sum: 533.5404255986214
time_elpased: 2.134
batch start
#iterations: 15
currently lose_sum: 532.1040217280388
time_elpased: 2.125
batch start
#iterations: 16
currently lose_sum: 532.5924890339375
time_elpased: 2.144
batch start
#iterations: 17
currently lose_sum: 532.1286371946335
time_elpased: 2.116
batch start
#iterations: 18
currently lose_sum: 531.0863280594349
time_elpased: 2.142
batch start
#iterations: 19
currently lose_sum: 531.3138669729233
time_elpased: 2.11
start validation test
0.576701030928
0.977225672878
0.147576862949
0.256428830134
0
validation finish
batch start
#iterations: 20
currently lose_sum: 530.4020629823208
time_elpased: 2.114
batch start
#iterations: 21
currently lose_sum: 530.7138096690178
time_elpased: 2.117
batch start
#iterations: 22
currently lose_sum: 529.8561999201775
time_elpased: 2.132
batch start
#iterations: 23
currently lose_sum: 530.6832224428654
time_elpased: 2.133
batch start
#iterations: 24
currently lose_sum: 528.4588743150234
time_elpased: 2.135
batch start
#iterations: 25
currently lose_sum: 528.6147485673428
time_elpased: 2.14
batch start
#iterations: 26
currently lose_sum: 529.0705127418041
time_elpased: 2.116
batch start
#iterations: 27
currently lose_sum: 529.4967207610607
time_elpased: 2.131
batch start
#iterations: 28
currently lose_sum: 527.5602477490902
time_elpased: 2.132
batch start
#iterations: 29
currently lose_sum: 526.2795761525631
time_elpased: 2.138
batch start
#iterations: 30
currently lose_sum: 527.112307459116
time_elpased: 2.12
batch start
#iterations: 31
currently lose_sum: 528.4718110263348
time_elpased: 2.13
batch start
#iterations: 32
currently lose_sum: 528.5646364688873
time_elpased: 2.107
batch start
#iterations: 33
currently lose_sum: 526.8233387768269
time_elpased: 2.122
batch start
#iterations: 34
currently lose_sum: 526.6132795214653
time_elpased: 2.108
batch start
#iterations: 35
currently lose_sum: 525.5279431343079
time_elpased: 2.127
batch start
#iterations: 36
currently lose_sum: 525.694821357727
time_elpased: 2.122
batch start
#iterations: 37
currently lose_sum: 523.8311623632908
time_elpased: 2.11
batch start
#iterations: 38
currently lose_sum: 525.5908890366554
time_elpased: 2.131
batch start
#iterations: 39
currently lose_sum: 525.9670173823833
time_elpased: 2.103
start validation test
0.581804123711
0.986220472441
0.156644085461
0.270348052882
0
validation finish
batch start
#iterations: 40
currently lose_sum: 526.6270981431007
time_elpased: 2.128
batch start
#iterations: 41
currently lose_sum: 525.0044283866882
time_elpased: 2.107
batch start
#iterations: 42
currently lose_sum: 524.7332291603088
time_elpased: 2.126
batch start
#iterations: 43
currently lose_sum: 523.2426212728024
time_elpased: 2.117
batch start
#iterations: 44
currently lose_sum: 526.0614453852177
time_elpased: 2.132
batch start
#iterations: 45
currently lose_sum: 525.9879712760448
time_elpased: 2.101
batch start
#iterations: 46
currently lose_sum: 523.0895918607712
time_elpased: 2.131
batch start
#iterations: 47
currently lose_sum: 525.2177315056324
time_elpased: 2.107
batch start
#iterations: 48
currently lose_sum: 524.6538364887238
time_elpased: 2.123
batch start
#iterations: 49
currently lose_sum: 520.9506570398808
time_elpased: 2.115
batch start
#iterations: 50
currently lose_sum: 524.6700690090656
time_elpased: 2.106
batch start
#iterations: 51
currently lose_sum: 524.3360023796558
time_elpased: 2.109
batch start
#iterations: 52
currently lose_sum: 523.3149930238724
time_elpased: 2.122
batch start
#iterations: 53
currently lose_sum: 524.9570956528187
time_elpased: 2.13
batch start
#iterations: 54
currently lose_sum: 522.5797077417374
time_elpased: 2.122
batch start
#iterations: 55
currently lose_sum: 524.6404741704464
time_elpased: 2.116
batch start
#iterations: 56
currently lose_sum: 524.0898104012012
time_elpased: 2.111
batch start
#iterations: 57
currently lose_sum: 522.7551110386848
time_elpased: 2.117
batch start
#iterations: 58
currently lose_sum: 524.1374818682671
time_elpased: 2.106
batch start
#iterations: 59
currently lose_sum: 522.4401746094227
time_elpased: 2.111
start validation test
0.570979381443
0.995327102804
0.133194372069
0.23494806508
0
validation finish
batch start
#iterations: 60
currently lose_sum: 522.2155088484287
time_elpased: 2.111
batch start
#iterations: 61
currently lose_sum: 521.0396708250046
time_elpased: 2.134
batch start
#iterations: 62
currently lose_sum: 526.0524497032166
time_elpased: 2.105
batch start
#iterations: 63
currently lose_sum: 524.5479011237621
time_elpased: 2.126
batch start
#iterations: 64
currently lose_sum: 522.0518884956837
time_elpased: 2.103
batch start
#iterations: 65
currently lose_sum: 522.6741801202297
time_elpased: 2.11
batch start
#iterations: 66
currently lose_sum: 522.4380509257317
time_elpased: 2.117
batch start
#iterations: 67
currently lose_sum: 521.2218999564648
time_elpased: 2.117
batch start
#iterations: 68
currently lose_sum: 522.2275341749191
time_elpased: 2.134
batch start
#iterations: 69
currently lose_sum: 524.7199501395226
time_elpased: 2.108
batch start
#iterations: 70
currently lose_sum: 520.4161967635155
time_elpased: 2.131
batch start
#iterations: 71
currently lose_sum: 523.2693466246128
time_elpased: 2.128
batch start
#iterations: 72
currently lose_sum: 523.7146733999252
time_elpased: 2.131
batch start
#iterations: 73
currently lose_sum: 521.0915775597095
time_elpased: 2.124
batch start
#iterations: 74
currently lose_sum: 522.2956345677376
time_elpased: 2.137
batch start
#iterations: 75
currently lose_sum: 520.5600936710835
time_elpased: 2.119
batch start
#iterations: 76
currently lose_sum: 521.3326191604137
time_elpased: 2.132
batch start
#iterations: 77
currently lose_sum: 522.3723888397217
time_elpased: 2.127
batch start
#iterations: 78
currently lose_sum: 521.7498970329762
time_elpased: 2.126
batch start
#iterations: 79
currently lose_sum: 523.3500330746174
time_elpased: 2.123
start validation test
0.560103092784
0.999059266228
0.110682647212
0.199286920623
0
validation finish
batch start
#iterations: 80
currently lose_sum: 521.6782584488392
time_elpased: 2.122
batch start
#iterations: 81
currently lose_sum: 520.406132131815
time_elpased: 2.13
batch start
#iterations: 82
currently lose_sum: 522.4110816121101
time_elpased: 2.112
batch start
#iterations: 83
currently lose_sum: 521.056925714016
time_elpased: 2.118
batch start
#iterations: 84
currently lose_sum: 521.5851732194424
time_elpased: 2.123
batch start
#iterations: 85
currently lose_sum: 519.6866925060749
time_elpased: 2.15
batch start
#iterations: 86
currently lose_sum: 520.9984695017338
time_elpased: 2.109
batch start
#iterations: 87
currently lose_sum: 519.6668649017811
time_elpased: 2.128
batch start
#iterations: 88
currently lose_sum: 521.6800029873848
time_elpased: 2.121
batch start
#iterations: 89
currently lose_sum: 522.2910265624523
time_elpased: 2.138
batch start
#iterations: 90
currently lose_sum: 521.3867461383343
time_elpased: 2.119
batch start
#iterations: 91
currently lose_sum: 519.1884596645832
time_elpased: 2.128
batch start
#iterations: 92
currently lose_sum: 520.2222248613834
time_elpased: 2.1
batch start
#iterations: 93
currently lose_sum: 521.7419407367706
time_elpased: 2.138
batch start
#iterations: 94
currently lose_sum: 519.9794290065765
time_elpased: 2.122
batch start
#iterations: 95
currently lose_sum: 520.128368973732
time_elpased: 2.125
batch start
#iterations: 96
currently lose_sum: 522.0284530818462
time_elpased: 2.125
batch start
#iterations: 97
currently lose_sum: 518.2143879532814
time_elpased: 2.117
batch start
#iterations: 98
currently lose_sum: 520.8120248317719
time_elpased: 2.131
batch start
#iterations: 99
currently lose_sum: 520.9394833147526
time_elpased: 2.128
start validation test
0.608762886598
0.994573260977
0.210109431996
0.346928239546
0
validation finish
batch start
#iterations: 100
currently lose_sum: 520.6816286742687
time_elpased: 2.135
batch start
#iterations: 101
currently lose_sum: 521.4202232062817
time_elpased: 2.131
batch start
#iterations: 102
currently lose_sum: 518.5768849849701
time_elpased: 2.149
batch start
#iterations: 103
currently lose_sum: 521.52359983325
time_elpased: 2.12
batch start
#iterations: 104
currently lose_sum: 520.2042141258717
time_elpased: 2.144
batch start
#iterations: 105
currently lose_sum: 519.9140639603138
time_elpased: 2.127
batch start
#iterations: 106
currently lose_sum: 521.3143910765648
time_elpased: 2.143
batch start
#iterations: 107
currently lose_sum: 519.465593367815
time_elpased: 2.127
batch start
#iterations: 108
currently lose_sum: 517.9072240889072
time_elpased: 2.116
batch start
#iterations: 109
currently lose_sum: 520.5411669313908
time_elpased: 2.161
batch start
#iterations: 110
currently lose_sum: 522.0247403681278
time_elpased: 2.129
batch start
#iterations: 111
currently lose_sum: 522.8721049726009
time_elpased: 2.138
batch start
#iterations: 112
currently lose_sum: 518.4415244162083
time_elpased: 2.131
batch start
#iterations: 113
currently lose_sum: 520.4548071622849
time_elpased: 2.136
batch start
#iterations: 114
currently lose_sum: 520.4740611612797
time_elpased: 2.123
batch start
#iterations: 115
currently lose_sum: 519.327886402607
time_elpased: 2.127
batch start
#iterations: 116
currently lose_sum: 518.8631660342216
time_elpased: 2.114
batch start
#iterations: 117
currently lose_sum: 521.0084663331509
time_elpased: 2.136
batch start
#iterations: 118
currently lose_sum: 518.7881450653076
time_elpased: 2.124
batch start
#iterations: 119
currently lose_sum: 519.048463344574
time_elpased: 2.13
start validation test
0.631907216495
0.992771084337
0.257634184471
0.409102192801
0
validation finish
batch start
#iterations: 120
currently lose_sum: 520.3194435536861
time_elpased: 2.128
batch start
#iterations: 121
currently lose_sum: 519.1179564893246
time_elpased: 2.13
batch start
#iterations: 122
currently lose_sum: 518.6920626163483
time_elpased: 2.149
batch start
#iterations: 123
currently lose_sum: 518.9779284894466
time_elpased: 2.158
batch start
#iterations: 124
currently lose_sum: 519.2978625893593
time_elpased: 2.141
batch start
#iterations: 125
currently lose_sum: 520.257309794426
time_elpased: 2.122
batch start
#iterations: 126
currently lose_sum: 520.0693063437939
time_elpased: 2.141
batch start
#iterations: 127
currently lose_sum: 519.2533091902733
time_elpased: 2.109
batch start
#iterations: 128
currently lose_sum: 518.9449555575848
time_elpased: 2.13
batch start
#iterations: 129
currently lose_sum: 519.4665339291096
time_elpased: 2.129
batch start
#iterations: 130
currently lose_sum: 518.7696722447872
time_elpased: 2.147
batch start
#iterations: 131
currently lose_sum: 520.1886988282204
time_elpased: 2.126
batch start
#iterations: 132
currently lose_sum: 519.6328577697277
time_elpased: 2.131
batch start
#iterations: 133
currently lose_sum: 521.0073415338993
time_elpased: 2.132
batch start
#iterations: 134
currently lose_sum: 518.3847441375256
time_elpased: 2.133
batch start
#iterations: 135
currently lose_sum: 517.6974674463272
time_elpased: 2.142
batch start
#iterations: 136
currently lose_sum: 518.7389286160469
time_elpased: 2.134
batch start
#iterations: 137
currently lose_sum: 518.441698461771
time_elpased: 2.148
batch start
#iterations: 138
currently lose_sum: 517.9278048276901
time_elpased: 2.145
batch start
#iterations: 139
currently lose_sum: 519.4170898199081
time_elpased: 2.167
start validation test
0.570670103093
1.0
0.131943720688
0.233127704631
0
validation finish
batch start
#iterations: 140
currently lose_sum: 516.728104531765
time_elpased: 2.147
batch start
#iterations: 141
currently lose_sum: 518.6077933907509
time_elpased: 2.182
batch start
#iterations: 142
currently lose_sum: 517.5087278485298
time_elpased: 2.157
batch start
#iterations: 143
currently lose_sum: 519.4252599477768
time_elpased: 2.165
batch start
#iterations: 144
currently lose_sum: 518.3430312275887
time_elpased: 2.158
batch start
#iterations: 145
currently lose_sum: 520.7969211935997
time_elpased: 2.142
batch start
#iterations: 146
currently lose_sum: 520.6670487821102
time_elpased: 2.157
batch start
#iterations: 147
currently lose_sum: 519.5519169270992
time_elpased: 2.146
batch start
#iterations: 148
currently lose_sum: 517.3582173585892
time_elpased: 2.147
batch start
#iterations: 149
currently lose_sum: 520.2320535778999
time_elpased: 2.148
batch start
#iterations: 150
currently lose_sum: 517.4311845600605
time_elpased: 2.156
batch start
#iterations: 151
currently lose_sum: 517.1351628303528
time_elpased: 2.15
batch start
#iterations: 152
currently lose_sum: 518.1178362667561
time_elpased: 2.15
batch start
#iterations: 153
currently lose_sum: 516.2652704715729
time_elpased: 2.155
batch start
#iterations: 154
currently lose_sum: 520.5910396873951
time_elpased: 2.17
batch start
#iterations: 155
currently lose_sum: 516.0380192101002
time_elpased: 2.143
batch start
#iterations: 156
currently lose_sum: 518.6544973254204
time_elpased: 2.166
batch start
#iterations: 157
currently lose_sum: 517.0172794759274
time_elpased: 2.145
batch start
#iterations: 158
currently lose_sum: 517.4193092286587
time_elpased: 2.139
batch start
#iterations: 159
currently lose_sum: 518.7234780490398
time_elpased: 2.146
start validation test
0.591907216495
0.999404761905
0.174986972381
0.297827050998
0
validation finish
batch start
#iterations: 160
currently lose_sum: 518.7360374331474
time_elpased: 2.147
batch start
#iterations: 161
currently lose_sum: 517.3327095806599
time_elpased: 2.146
batch start
#iterations: 162
currently lose_sum: 517.6149377524853
time_elpased: 2.152
batch start
#iterations: 163
currently lose_sum: 518.0035095512867
time_elpased: 2.155
batch start
#iterations: 164
currently lose_sum: 518.8465919196606
time_elpased: 2.163
batch start
#iterations: 165
currently lose_sum: 519.8916952610016
time_elpased: 2.174
batch start
#iterations: 166
currently lose_sum: 518.8133939504623
time_elpased: 2.153
batch start
#iterations: 167
currently lose_sum: 518.4132731556892
time_elpased: 2.16
batch start
#iterations: 168
currently lose_sum: 517.9741195440292
time_elpased: 2.155
batch start
#iterations: 169
currently lose_sum: 518.0428441166878
time_elpased: 2.159
batch start
#iterations: 170
currently lose_sum: 517.9742447733879
time_elpased: 2.16
batch start
#iterations: 171
currently lose_sum: 516.5401777625084
time_elpased: 2.15
batch start
#iterations: 172
currently lose_sum: 520.3269335329533
time_elpased: 2.166
batch start
#iterations: 173
currently lose_sum: 518.7108738720417
time_elpased: 2.139
batch start
#iterations: 174
currently lose_sum: 519.1607240736485
time_elpased: 2.166
batch start
#iterations: 175
currently lose_sum: 517.5768825411797
time_elpased: 2.135
batch start
#iterations: 176
currently lose_sum: 517.8791390955448
time_elpased: 2.177
batch start
#iterations: 177
currently lose_sum: 518.9297891259193
time_elpased: 2.144
batch start
#iterations: 178
currently lose_sum: 519.6090020537376
time_elpased: 2.176
batch start
#iterations: 179
currently lose_sum: 519.7424789369106
time_elpased: 2.174
start validation test
0.588402061856
0.999379652605
0.16789994789
0.287498884626
0
validation finish
batch start
#iterations: 180
currently lose_sum: 519.0658012926579
time_elpased: 2.158
batch start
#iterations: 181
currently lose_sum: 520.164756834507
time_elpased: 2.17
batch start
#iterations: 182
currently lose_sum: 517.9716609716415
time_elpased: 2.139
batch start
#iterations: 183
currently lose_sum: 518.6726348698139
time_elpased: 2.15
batch start
#iterations: 184
currently lose_sum: 517.453216701746
time_elpased: 2.138
batch start
#iterations: 185
currently lose_sum: 519.2607736587524
time_elpased: 2.142
batch start
#iterations: 186
currently lose_sum: 518.819173425436
time_elpased: 2.123
batch start
#iterations: 187
currently lose_sum: 519.0685058236122
time_elpased: 2.15
batch start
#iterations: 188
currently lose_sum: 517.3702812194824
time_elpased: 2.158
batch start
#iterations: 189
currently lose_sum: 518.1021767556667
time_elpased: 2.172
batch start
#iterations: 190
currently lose_sum: 517.4537578225136
time_elpased: 2.148
batch start
#iterations: 191
currently lose_sum: 518.7726576328278
time_elpased: 2.152
batch start
#iterations: 192
currently lose_sum: 515.7782077491283
time_elpased: 2.154
batch start
#iterations: 193
currently lose_sum: 518.3092590868473
time_elpased: 2.142
batch start
#iterations: 194
currently lose_sum: 519.2841467857361
time_elpased: 2.155
batch start
#iterations: 195
currently lose_sum: 521.0843982994556
time_elpased: 2.157
batch start
#iterations: 196
currently lose_sum: 515.2634441256523
time_elpased: 2.169
batch start
#iterations: 197
currently lose_sum: 519.2840381264687
time_elpased: 2.194
batch start
#iterations: 198
currently lose_sum: 516.8758453726768
time_elpased: 2.171
batch start
#iterations: 199
currently lose_sum: 517.6479862034321
time_elpased: 2.169
start validation test
0.593402061856
0.998831092928
0.178113600834
0.302317353618
0
validation finish
batch start
#iterations: 200
currently lose_sum: 518.0000755488873
time_elpased: 2.166
batch start
#iterations: 201
currently lose_sum: 519.3791201412678
time_elpased: 2.166
batch start
#iterations: 202
currently lose_sum: 517.1622240841389
time_elpased: 2.154
batch start
#iterations: 203
currently lose_sum: 516.9519864022732
time_elpased: 2.159
batch start
#iterations: 204
currently lose_sum: 516.6577617228031
time_elpased: 2.145
batch start
#iterations: 205
currently lose_sum: 517.9011285007
time_elpased: 2.159
batch start
#iterations: 206
currently lose_sum: 520.5140297114849
time_elpased: 2.159
batch start
#iterations: 207
currently lose_sum: 517.1944686174393
time_elpased: 2.176
batch start
#iterations: 208
currently lose_sum: 516.1065648198128
time_elpased: 2.142
batch start
#iterations: 209
currently lose_sum: 517.8011077642441
time_elpased: 2.167
batch start
#iterations: 210
currently lose_sum: 516.9978566467762
time_elpased: 2.142
batch start
#iterations: 211
currently lose_sum: 518.1756226718426
time_elpased: 2.175
batch start
#iterations: 212
currently lose_sum: 517.4778332412243
time_elpased: 2.136
batch start
#iterations: 213
currently lose_sum: 517.5239354670048
time_elpased: 2.15
batch start
#iterations: 214
currently lose_sum: 518.9427326619625
time_elpased: 2.139
batch start
#iterations: 215
currently lose_sum: 517.3764299154282
time_elpased: 2.154
batch start
#iterations: 216
currently lose_sum: 518.3313727676868
time_elpased: 2.161
batch start
#iterations: 217
currently lose_sum: 517.2238402366638
time_elpased: 2.161
batch start
#iterations: 218
currently lose_sum: 516.8239071667194
time_elpased: 2.178
batch start
#iterations: 219
currently lose_sum: 518.9942234456539
time_elpased: 2.172
start validation test
0.565206185567
1.0
0.120896300156
0.215713621571
0
validation finish
batch start
#iterations: 220
currently lose_sum: 517.8962725102901
time_elpased: 2.163
batch start
#iterations: 221
currently lose_sum: 518.172616481781
time_elpased: 2.158
batch start
#iterations: 222
currently lose_sum: 516.1303124129772
time_elpased: 2.163
batch start
#iterations: 223
currently lose_sum: 517.9854088127613
time_elpased: 2.158
batch start
#iterations: 224
currently lose_sum: 517.9036037325859
time_elpased: 2.144
batch start
#iterations: 225
currently lose_sum: 520.3623098731041
time_elpased: 2.156
batch start
#iterations: 226
currently lose_sum: 517.422465711832
time_elpased: 2.148
batch start
#iterations: 227
currently lose_sum: 517.1930802762508
time_elpased: 2.178
batch start
#iterations: 228
currently lose_sum: 515.6109072864056
time_elpased: 2.162
batch start
#iterations: 229
currently lose_sum: 518.0811201930046
time_elpased: 2.165
batch start
#iterations: 230
currently lose_sum: 518.8766134381294
time_elpased: 2.167
batch start
#iterations: 231
currently lose_sum: 519.8936458826065
time_elpased: 2.182
batch start
#iterations: 232
currently lose_sum: 517.2945961654186
time_elpased: 2.132
batch start
#iterations: 233
currently lose_sum: 514.8625950217247
time_elpased: 2.143
batch start
#iterations: 234
currently lose_sum: 516.3428801894188
time_elpased: 2.134
batch start
#iterations: 235
currently lose_sum: 516.6791830956936
time_elpased: 2.125
batch start
#iterations: 236
currently lose_sum: 516.625952154398
time_elpased: 2.116
batch start
#iterations: 237
currently lose_sum: 517.9719732701778
time_elpased: 2.135
batch start
#iterations: 238
currently lose_sum: 516.2019270658493
time_elpased: 2.136
batch start
#iterations: 239
currently lose_sum: 516.2078308165073
time_elpased: 2.118
start validation test
0.554020618557
1.0
0.0982803543512
0.178971341811
0
validation finish
batch start
#iterations: 240
currently lose_sum: 517.3008863925934
time_elpased: 2.102
batch start
#iterations: 241
currently lose_sum: 515.8512727022171
time_elpased: 2.109
batch start
#iterations: 242
currently lose_sum: 517.2674166560173
time_elpased: 2.121
batch start
#iterations: 243
currently lose_sum: 515.8408073484898
time_elpased: 2.115
batch start
#iterations: 244
currently lose_sum: 517.2486284375191
time_elpased: 2.108
batch start
#iterations: 245
currently lose_sum: 516.0469178557396
time_elpased: 2.12
batch start
#iterations: 246
currently lose_sum: 516.4734708964825
time_elpased: 2.122
batch start
#iterations: 247
currently lose_sum: 517.0602569282055
time_elpased: 2.088
batch start
#iterations: 248
currently lose_sum: 516.4286705255508
time_elpased: 2.134
batch start
#iterations: 249
currently lose_sum: 517.2390024662018
time_elpased: 2.102
batch start
#iterations: 250
currently lose_sum: 516.5420786142349
time_elpased: 2.12
batch start
#iterations: 251
currently lose_sum: 518.4838807880878
time_elpased: 2.105
batch start
#iterations: 252
currently lose_sum: 519.3036303818226
time_elpased: 2.094
batch start
#iterations: 253
currently lose_sum: 516.2582820653915
time_elpased: 2.108
batch start
#iterations: 254
currently lose_sum: 519.1829528808594
time_elpased: 2.101
batch start
#iterations: 255
currently lose_sum: 516.0918342471123
time_elpased: 2.115
batch start
#iterations: 256
currently lose_sum: 519.0014142990112
time_elpased: 2.114
batch start
#iterations: 257
currently lose_sum: 515.6179354786873
time_elpased: 2.108
batch start
#iterations: 258
currently lose_sum: 515.7951042354107
time_elpased: 2.119
batch start
#iterations: 259
currently lose_sum: 518.1594167649746
time_elpased: 2.108
start validation test
0.596134020619
0.998301245753
0.183741532048
0.310360003521
0
validation finish
batch start
#iterations: 260
currently lose_sum: 518.870686262846
time_elpased: 2.115
batch start
#iterations: 261
currently lose_sum: 517.9880720674992
time_elpased: 2.141
batch start
#iterations: 262
currently lose_sum: 516.9486160576344
time_elpased: 2.123
batch start
#iterations: 263
currently lose_sum: 516.2367331981659
time_elpased: 2.122
batch start
#iterations: 264
currently lose_sum: 516.5317514240742
time_elpased: 2.109
batch start
#iterations: 265
currently lose_sum: 518.1681796610355
time_elpased: 2.111
batch start
#iterations: 266
currently lose_sum: 517.0777696669102
time_elpased: 2.115
batch start
#iterations: 267
currently lose_sum: 515.7931114435196
time_elpased: 2.136
batch start
#iterations: 268
currently lose_sum: 516.7235091924667
time_elpased: 2.115
batch start
#iterations: 269
currently lose_sum: 515.6415889263153
time_elpased: 2.132
batch start
#iterations: 270
currently lose_sum: 518.3076647818089
time_elpased: 2.123
batch start
#iterations: 271
currently lose_sum: 515.1533851623535
time_elpased: 2.141
batch start
#iterations: 272
currently lose_sum: 516.908125191927
time_elpased: 2.145
batch start
#iterations: 273
currently lose_sum: 517.091630011797
time_elpased: 2.137
batch start
#iterations: 274
currently lose_sum: 517.6068098545074
time_elpased: 2.137
batch start
#iterations: 275
currently lose_sum: 516.5227487385273
time_elpased: 2.132
batch start
#iterations: 276
currently lose_sum: 515.9150519669056
time_elpased: 2.15
batch start
#iterations: 277
currently lose_sum: 517.4596524238586
time_elpased: 2.143
batch start
#iterations: 278
currently lose_sum: 515.8434874117374
time_elpased: 2.147
batch start
#iterations: 279
currently lose_sum: 517.5329085886478
time_elpased: 2.121
start validation test
0.59675257732
0.998873873874
0.18488796248
0.312021809867
0
validation finish
batch start
#iterations: 280
currently lose_sum: 515.9675362110138
time_elpased: 2.125
batch start
#iterations: 281
currently lose_sum: 516.2357611954212
time_elpased: 2.128
batch start
#iterations: 282
currently lose_sum: 516.6589467525482
time_elpased: 2.135
batch start
#iterations: 283
currently lose_sum: 517.3172748684883
time_elpased: 2.12
batch start
#iterations: 284
currently lose_sum: 516.4671611785889
time_elpased: 2.148
batch start
#iterations: 285
currently lose_sum: 518.0903848707676
time_elpased: 2.149
batch start
#iterations: 286
currently lose_sum: 516.9790728092194
time_elpased: 2.148
batch start
#iterations: 287
currently lose_sum: 518.5020052492619
time_elpased: 2.144
batch start
#iterations: 288
currently lose_sum: 515.9782911241055
time_elpased: 2.137
batch start
#iterations: 289
currently lose_sum: 516.2411500513554
time_elpased: 2.14
batch start
#iterations: 290
currently lose_sum: 514.5452414155006
time_elpased: 2.128
batch start
#iterations: 291
currently lose_sum: 517.7537690401077
time_elpased: 2.161
batch start
#iterations: 292
currently lose_sum: 516.6274385750294
time_elpased: 2.138
batch start
#iterations: 293
currently lose_sum: 517.4316365718842
time_elpased: 2.153
batch start
#iterations: 294
currently lose_sum: 518.2964775562286
time_elpased: 2.147
batch start
#iterations: 295
currently lose_sum: 517.3002399504185
time_elpased: 2.153
batch start
#iterations: 296
currently lose_sum: 516.1960240602493
time_elpased: 2.17
batch start
#iterations: 297
currently lose_sum: 516.7249334454536
time_elpased: 2.16
batch start
#iterations: 298
currently lose_sum: 517.4541707038879
time_elpased: 2.145
batch start
#iterations: 299
currently lose_sum: 518.0959341824055
time_elpased: 2.141
start validation test
0.615309278351
0.99766573296
0.222720166754
0.364147567522
0
validation finish
batch start
#iterations: 300
currently lose_sum: 517.1489487886429
time_elpased: 2.144
batch start
#iterations: 301
currently lose_sum: 515.9140816926956
time_elpased: 2.116
batch start
#iterations: 302
currently lose_sum: 517.7017302811146
time_elpased: 2.152
batch start
#iterations: 303
currently lose_sum: 517.8707768321037
time_elpased: 2.142
batch start
#iterations: 304
currently lose_sum: 515.5252223610878
time_elpased: 2.148
batch start
#iterations: 305
currently lose_sum: 518.2562719285488
time_elpased: 2.144
batch start
#iterations: 306
currently lose_sum: 517.4264595210552
time_elpased: 2.161
batch start
#iterations: 307
currently lose_sum: 518.3104800283909
time_elpased: 2.157
batch start
#iterations: 308
currently lose_sum: 517.0697646439075
time_elpased: 2.131
batch start
#iterations: 309
currently lose_sum: 516.6061089336872
time_elpased: 2.154
batch start
#iterations: 310
currently lose_sum: 515.0691283941269
time_elpased: 2.145
batch start
#iterations: 311
currently lose_sum: 516.3203780651093
time_elpased: 2.156
batch start
#iterations: 312
currently lose_sum: 517.6999753415585
time_elpased: 2.148
batch start
#iterations: 313
currently lose_sum: 517.630834966898
time_elpased: 2.168
batch start
#iterations: 314
currently lose_sum: 515.8989067077637
time_elpased: 2.151
batch start
#iterations: 315
currently lose_sum: 517.8473455905914
time_elpased: 2.165
batch start
#iterations: 316
currently lose_sum: 517.5203738808632
time_elpased: 2.166
batch start
#iterations: 317
currently lose_sum: 515.8081090450287
time_elpased: 2.173
batch start
#iterations: 318
currently lose_sum: 518.5885506868362
time_elpased: 2.16
batch start
#iterations: 319
currently lose_sum: 515.9016347229481
time_elpased: 2.151
start validation test
0.559896907216
1.0
0.11016154247
0.198460383027
0
validation finish
batch start
#iterations: 320
currently lose_sum: 517.5236378908157
time_elpased: 2.164
batch start
#iterations: 321
currently lose_sum: 517.9307263195515
time_elpased: 2.142
batch start
#iterations: 322
currently lose_sum: 515.5880056321621
time_elpased: 2.159
batch start
#iterations: 323
currently lose_sum: 515.7737022042274
time_elpased: 2.143
batch start
#iterations: 324
currently lose_sum: 518.7201193571091
time_elpased: 2.155
batch start
#iterations: 325
currently lose_sum: 516.9567007124424
time_elpased: 2.13
batch start
#iterations: 326
currently lose_sum: 517.5951049029827
time_elpased: 2.166
batch start
#iterations: 327
currently lose_sum: 517.4591093957424
time_elpased: 2.156
batch start
#iterations: 328
currently lose_sum: 517.5072576999664
time_elpased: 2.158
batch start
#iterations: 329
currently lose_sum: 515.8463317155838
time_elpased: 2.158
batch start
#iterations: 330
currently lose_sum: 518.3889226019382
time_elpased: 2.158
batch start
#iterations: 331
currently lose_sum: 518.4861541986465
time_elpased: 2.162
batch start
#iterations: 332
currently lose_sum: 517.8169630169868
time_elpased: 2.15
batch start
#iterations: 333
currently lose_sum: 517.2606999576092
time_elpased: 2.177
batch start
#iterations: 334
currently lose_sum: 515.8639222681522
time_elpased: 2.159
batch start
#iterations: 335
currently lose_sum: 516.4328883290291
time_elpased: 2.178
batch start
#iterations: 336
currently lose_sum: 517.8515876233578
time_elpased: 2.17
batch start
#iterations: 337
currently lose_sum: 516.7799685299397
time_elpased: 2.175
batch start
#iterations: 338
currently lose_sum: 514.841393828392
time_elpased: 2.145
batch start
#iterations: 339
currently lose_sum: 518.2471035718918
time_elpased: 2.151
start validation test
0.59175257732
0.999403697078
0.174674309536
0.297374024131
0
validation finish
batch start
#iterations: 340
currently lose_sum: 515.156013250351
time_elpased: 2.144
batch start
#iterations: 341
currently lose_sum: 516.6327131688595
time_elpased: 2.16
batch start
#iterations: 342
currently lose_sum: 518.6179953813553
time_elpased: 2.157
batch start
#iterations: 343
currently lose_sum: 517.942773848772
time_elpased: 2.155
batch start
#iterations: 344
currently lose_sum: 518.7336999475956
time_elpased: 2.159
batch start
#iterations: 345
currently lose_sum: 518.0000438690186
time_elpased: 2.152
batch start
#iterations: 346
currently lose_sum: 516.9615781903267
time_elpased: 2.155
batch start
#iterations: 347
currently lose_sum: 517.2540679574013
time_elpased: 2.15
batch start
#iterations: 348
currently lose_sum: 517.6388276517391
time_elpased: 2.171
batch start
#iterations: 349
currently lose_sum: 518.7250550687313
time_elpased: 2.165
batch start
#iterations: 350
currently lose_sum: 517.433690071106
time_elpased: 2.164
batch start
#iterations: 351
currently lose_sum: 517.2970423102379
time_elpased: 2.153
batch start
#iterations: 352
currently lose_sum: 516.9658250808716
time_elpased: 2.156
batch start
#iterations: 353
currently lose_sum: 518.2900370955467
time_elpased: 2.147
batch start
#iterations: 354
currently lose_sum: 517.2908437550068
time_elpased: 2.175
batch start
#iterations: 355
currently lose_sum: 516.9162468016148
time_elpased: 2.182
batch start
#iterations: 356
currently lose_sum: 515.6283695697784
time_elpased: 2.158
batch start
#iterations: 357
currently lose_sum: 515.4836097359657
time_elpased: 2.174
batch start
#iterations: 358
currently lose_sum: 517.9689806997776
time_elpased: 2.164
batch start
#iterations: 359
currently lose_sum: 515.8612426221371
time_elpased: 2.172
start validation test
0.598350515464
0.99944598338
0.188014590933
0.31649122807
0
validation finish
batch start
#iterations: 360
currently lose_sum: 516.8027713894844
time_elpased: 2.139
batch start
#iterations: 361
currently lose_sum: 515.5967930555344
time_elpased: 2.15
batch start
#iterations: 362
currently lose_sum: 516.7618534862995
time_elpased: 2.145
batch start
#iterations: 363
currently lose_sum: 517.1454066336155
time_elpased: 2.149
batch start
#iterations: 364
currently lose_sum: 517.308328807354
time_elpased: 2.15
batch start
#iterations: 365
currently lose_sum: 516.8234322965145
time_elpased: 2.151
batch start
#iterations: 366
currently lose_sum: 517.3153896331787
time_elpased: 2.157
batch start
#iterations: 367
currently lose_sum: 517.0193695425987
time_elpased: 2.161
batch start
#iterations: 368
currently lose_sum: 515.5976963341236
time_elpased: 2.184
batch start
#iterations: 369
currently lose_sum: 515.1575003266335
time_elpased: 2.19
batch start
#iterations: 370
currently lose_sum: 515.2371867895126
time_elpased: 2.207
batch start
#iterations: 371
currently lose_sum: 517.0611792802811
time_elpased: 2.195
batch start
#iterations: 372
currently lose_sum: 515.7214550375938
time_elpased: 2.18
batch start
#iterations: 373
currently lose_sum: 516.4387728273869
time_elpased: 2.177
batch start
#iterations: 374
currently lose_sum: 517.8730107545853
time_elpased: 2.171
batch start
#iterations: 375
currently lose_sum: 518.0237413048744
time_elpased: 2.178
batch start
#iterations: 376
currently lose_sum: 516.7844981253147
time_elpased: 2.185
batch start
#iterations: 377
currently lose_sum: 515.5181981623173
time_elpased: 2.182
batch start
#iterations: 378
currently lose_sum: 518.2913235723972
time_elpased: 2.17
batch start
#iterations: 379
currently lose_sum: 516.5485059916973
time_elpased: 2.146
start validation test
0.603762886598
0.999476439791
0.198957790516
0.331855714907
0
validation finish
batch start
#iterations: 380
currently lose_sum: 516.9219984710217
time_elpased: 2.136
batch start
#iterations: 381
currently lose_sum: 515.5887839496136
time_elpased: 2.158
batch start
#iterations: 382
currently lose_sum: 516.6651705801487
time_elpased: 2.153
batch start
#iterations: 383
currently lose_sum: 514.5915805995464
time_elpased: 2.161
batch start
#iterations: 384
currently lose_sum: 514.7024803161621
time_elpased: 2.17
batch start
#iterations: 385
currently lose_sum: 515.8303127288818
time_elpased: 2.17
batch start
#iterations: 386
currently lose_sum: 516.8084174394608
time_elpased: 2.162
batch start
#iterations: 387
currently lose_sum: 515.5580899119377
time_elpased: 2.168
batch start
#iterations: 388
currently lose_sum: 516.126621067524
time_elpased: 2.162
batch start
#iterations: 389
currently lose_sum: 517.9998978376389
time_elpased: 2.153
batch start
#iterations: 390
currently lose_sum: 516.2408158183098
time_elpased: 2.157
batch start
#iterations: 391
currently lose_sum: 517.3601371943951
time_elpased: 2.162
batch start
#iterations: 392
currently lose_sum: 516.4890076518059
time_elpased: 2.167
batch start
#iterations: 393
currently lose_sum: 515.4734690785408
time_elpased: 2.172
batch start
#iterations: 394
currently lose_sum: 515.543377250433
time_elpased: 2.156
batch start
#iterations: 395
currently lose_sum: 516.1310702562332
time_elpased: 2.181
batch start
#iterations: 396
currently lose_sum: 516.2293668687344
time_elpased: 2.166
batch start
#iterations: 397
currently lose_sum: 517.6845923960209
time_elpased: 2.164
batch start
#iterations: 398
currently lose_sum: 516.0693327188492
time_elpased: 2.161
batch start
#iterations: 399
currently lose_sum: 515.6858155429363
time_elpased: 2.166
start validation test
0.593659793814
0.999416569428
0.178530484627
0.302944557432
0
validation finish
acc: 0.628
pre: 0.993
rec: 0.261
F1: 0.413
auc: 0.000
