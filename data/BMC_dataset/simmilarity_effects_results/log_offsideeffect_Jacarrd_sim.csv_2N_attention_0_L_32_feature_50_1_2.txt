start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 551.6220624446869
time_elpased: 1.815
batch start
#iterations: 1
currently lose_sum: 545.3372923731804
time_elpased: 1.709
batch start
#iterations: 2
currently lose_sum: 544.4822097420692
time_elpased: 1.745
batch start
#iterations: 3
currently lose_sum: 541.6515787243843
time_elpased: 1.716
batch start
#iterations: 4
currently lose_sum: 540.1705757081509
time_elpased: 1.789
batch start
#iterations: 5
currently lose_sum: 539.430370092392
time_elpased: 1.736
batch start
#iterations: 6
currently lose_sum: 536.6991739869118
time_elpased: 1.706
batch start
#iterations: 7
currently lose_sum: 537.0614991188049
time_elpased: 1.708
batch start
#iterations: 8
currently lose_sum: 536.513096511364
time_elpased: 1.698
batch start
#iterations: 9
currently lose_sum: 535.190430611372
time_elpased: 1.75
batch start
#iterations: 10
currently lose_sum: 534.3210738003254
time_elpased: 1.747
batch start
#iterations: 11
currently lose_sum: 536.3090610802174
time_elpased: 1.79
batch start
#iterations: 12
currently lose_sum: 533.2920427918434
time_elpased: 1.726
batch start
#iterations: 13
currently lose_sum: 532.5217146873474
time_elpased: 1.736
batch start
#iterations: 14
currently lose_sum: 532.2914672195911
time_elpased: 1.733
batch start
#iterations: 15
currently lose_sum: 530.8184505999088
time_elpased: 1.701
batch start
#iterations: 16
currently lose_sum: 531.9243701696396
time_elpased: 1.73
batch start
#iterations: 17
currently lose_sum: 530.7399522960186
time_elpased: 1.712
batch start
#iterations: 18
currently lose_sum: 529.9188190102577
time_elpased: 1.72
batch start
#iterations: 19
currently lose_sum: 530.7653037011623
time_elpased: 1.717
start validation test
0.558762886598
0.972742759796
0.118097207859
0.210623386204
0
validation finish
batch start
#iterations: 20
currently lose_sum: 529.4162468910217
time_elpased: 1.725
batch start
#iterations: 21
currently lose_sum: 530.9423078596592
time_elpased: 1.718
batch start
#iterations: 22
currently lose_sum: 528.5363939404488
time_elpased: 1.71
batch start
#iterations: 23
currently lose_sum: 528.8275838196278
time_elpased: 1.71
batch start
#iterations: 24
currently lose_sum: 527.7362173199654
time_elpased: 1.721
batch start
#iterations: 25
currently lose_sum: 528.9642102718353
time_elpased: 1.706
batch start
#iterations: 26
currently lose_sum: 529.1403213441372
time_elpased: 1.722
batch start
#iterations: 27
currently lose_sum: 528.3535399436951
time_elpased: 1.75
batch start
#iterations: 28
currently lose_sum: 527.0325636863708
time_elpased: 1.698
batch start
#iterations: 29
currently lose_sum: 526.8208546340466
time_elpased: 1.714
batch start
#iterations: 30
currently lose_sum: 527.0583827793598
time_elpased: 1.718
batch start
#iterations: 31
currently lose_sum: 525.852808624506
time_elpased: 1.716
batch start
#iterations: 32
currently lose_sum: 528.1111851334572
time_elpased: 1.703
batch start
#iterations: 33
currently lose_sum: 525.6211661994457
time_elpased: 1.707
batch start
#iterations: 34
currently lose_sum: 527.590728521347
time_elpased: 1.721
batch start
#iterations: 35
currently lose_sum: 523.1328297555447
time_elpased: 1.721
batch start
#iterations: 36
currently lose_sum: 526.5962455868721
time_elpased: 1.727
batch start
#iterations: 37
currently lose_sum: 524.0103272199631
time_elpased: 1.699
batch start
#iterations: 38
currently lose_sum: 525.121144682169
time_elpased: 1.724
batch start
#iterations: 39
currently lose_sum: 526.0537962913513
time_elpased: 1.699
start validation test
0.562886597938
0.983739837398
0.12512926577
0.222018348624
0
validation finish
batch start
#iterations: 40
currently lose_sum: 527.3076225221157
time_elpased: 1.703
batch start
#iterations: 41
currently lose_sum: 526.2639480829239
time_elpased: 1.697
batch start
#iterations: 42
currently lose_sum: 524.373073130846
time_elpased: 1.693
batch start
#iterations: 43
currently lose_sum: 524.4885857999325
time_elpased: 1.713
batch start
#iterations: 44
currently lose_sum: 524.0962334275246
time_elpased: 1.681
batch start
#iterations: 45
currently lose_sum: 523.6855591535568
time_elpased: 1.712
batch start
#iterations: 46
currently lose_sum: 522.7267936766148
time_elpased: 1.71
batch start
#iterations: 47
currently lose_sum: 523.8617174327374
time_elpased: 1.693
batch start
#iterations: 48
currently lose_sum: 522.9389798343182
time_elpased: 1.704
batch start
#iterations: 49
currently lose_sum: 523.0544629096985
time_elpased: 1.697
batch start
#iterations: 50
currently lose_sum: 524.6487064659595
time_elpased: 1.682
batch start
#iterations: 51
currently lose_sum: 523.0107284784317
time_elpased: 1.686
batch start
#iterations: 52
currently lose_sum: 523.3831753432751
time_elpased: 1.679
batch start
#iterations: 53
currently lose_sum: 525.2782387137413
time_elpased: 1.701
batch start
#iterations: 54
currently lose_sum: 522.0410757064819
time_elpased: 1.696
batch start
#iterations: 55
currently lose_sum: 523.9284910559654
time_elpased: 1.683
batch start
#iterations: 56
currently lose_sum: 523.5521262288094
time_elpased: 1.703
batch start
#iterations: 57
currently lose_sum: 521.7616687715054
time_elpased: 1.675
batch start
#iterations: 58
currently lose_sum: 524.1381193697453
time_elpased: 1.697
batch start
#iterations: 59
currently lose_sum: 521.2663584649563
time_elpased: 1.699
start validation test
0.570463917526
0.98689002185
0.14012409514
0.245404328534
0
validation finish
batch start
#iterations: 60
currently lose_sum: 523.4411995410919
time_elpased: 1.714
batch start
#iterations: 61
currently lose_sum: 522.7826605141163
time_elpased: 1.7
batch start
#iterations: 62
currently lose_sum: 525.717125415802
time_elpased: 1.702
batch start
#iterations: 63
currently lose_sum: 523.9671356976032
time_elpased: 1.664
batch start
#iterations: 64
currently lose_sum: 523.3953022360802
time_elpased: 1.662
batch start
#iterations: 65
currently lose_sum: 523.8438732624054
time_elpased: 1.685
batch start
#iterations: 66
currently lose_sum: 522.1813359558582
time_elpased: 1.683
batch start
#iterations: 67
currently lose_sum: 520.6973318755627
time_elpased: 1.686
batch start
#iterations: 68
currently lose_sum: 524.2583807110786
time_elpased: 1.698
batch start
#iterations: 69
currently lose_sum: 523.0227942764759
time_elpased: 1.726
batch start
#iterations: 70
currently lose_sum: 522.742787539959
time_elpased: 1.705
batch start
#iterations: 71
currently lose_sum: 522.0989386439323
time_elpased: 1.694
batch start
#iterations: 72
currently lose_sum: 521.8754943013191
time_elpased: 1.654
batch start
#iterations: 73
currently lose_sum: 520.7665827870369
time_elpased: 1.698
batch start
#iterations: 74
currently lose_sum: 520.5076069235802
time_elpased: 1.661
batch start
#iterations: 75
currently lose_sum: 521.4058107733727
time_elpased: 1.654
batch start
#iterations: 76
currently lose_sum: 522.5290480256081
time_elpased: 1.7
batch start
#iterations: 77
currently lose_sum: 521.959330022335
time_elpased: 1.682
batch start
#iterations: 78
currently lose_sum: 521.3326701819897
time_elpased: 1.693
batch start
#iterations: 79
currently lose_sum: 522.410660892725
time_elpased: 1.69
start validation test
0.575051546392
0.997210599721
0.147880041365
0.257564841499
0
validation finish
batch start
#iterations: 80
currently lose_sum: 520.6244619488716
time_elpased: 1.7
batch start
#iterations: 81
currently lose_sum: 521.9477735161781
time_elpased: 1.684
batch start
#iterations: 82
currently lose_sum: 521.6715335845947
time_elpased: 1.663
batch start
#iterations: 83
currently lose_sum: 522.2364653944969
time_elpased: 1.699
batch start
#iterations: 84
currently lose_sum: 522.3939287662506
time_elpased: 1.695
batch start
#iterations: 85
currently lose_sum: 519.8896324634552
time_elpased: 1.692
batch start
#iterations: 86
currently lose_sum: 520.551091760397
time_elpased: 1.676
batch start
#iterations: 87
currently lose_sum: 519.1817527413368
time_elpased: 1.673
batch start
#iterations: 88
currently lose_sum: 521.1018538177013
time_elpased: 1.7
batch start
#iterations: 89
currently lose_sum: 520.6270727217197
time_elpased: 1.692
batch start
#iterations: 90
currently lose_sum: 522.2429370880127
time_elpased: 1.701
batch start
#iterations: 91
currently lose_sum: 519.3738899230957
time_elpased: 1.681
batch start
#iterations: 92
currently lose_sum: 521.8827906847
time_elpased: 1.703
batch start
#iterations: 93
currently lose_sum: 521.099381417036
time_elpased: 1.705
batch start
#iterations: 94
currently lose_sum: 519.6821017861366
time_elpased: 1.679
batch start
#iterations: 95
currently lose_sum: 519.5237523019314
time_elpased: 1.673
batch start
#iterations: 96
currently lose_sum: 522.0071256160736
time_elpased: 1.708
batch start
#iterations: 97
currently lose_sum: 519.6136056184769
time_elpased: 1.689
batch start
#iterations: 98
currently lose_sum: 520.9239656329155
time_elpased: 1.665
batch start
#iterations: 99
currently lose_sum: 520.7047314941883
time_elpased: 1.68
start validation test
0.60587628866
0.993177387914
0.210754912099
0.347722231701
0
validation finish
batch start
#iterations: 100
currently lose_sum: 521.2268689274788
time_elpased: 1.731
batch start
#iterations: 101
currently lose_sum: 519.6939927339554
time_elpased: 1.715
batch start
#iterations: 102
currently lose_sum: 518.7162664830685
time_elpased: 1.672
batch start
#iterations: 103
currently lose_sum: 520.2076931297779
time_elpased: 1.67
batch start
#iterations: 104
currently lose_sum: 520.279017329216
time_elpased: 1.69
batch start
#iterations: 105
currently lose_sum: 520.3007712364197
time_elpased: 1.694
batch start
#iterations: 106
currently lose_sum: 520.9027324020863
time_elpased: 1.708
batch start
#iterations: 107
currently lose_sum: 520.5142376720905
time_elpased: 1.699
batch start
#iterations: 108
currently lose_sum: 518.0818736255169
time_elpased: 1.683
batch start
#iterations: 109
currently lose_sum: 520.4106380939484
time_elpased: 1.695
batch start
#iterations: 110
currently lose_sum: 519.6147530674934
time_elpased: 1.709
batch start
#iterations: 111
currently lose_sum: 522.5784456431866
time_elpased: 1.707
batch start
#iterations: 112
currently lose_sum: 518.8106065392494
time_elpased: 1.685
batch start
#iterations: 113
currently lose_sum: 520.1335812211037
time_elpased: 1.693
batch start
#iterations: 114
currently lose_sum: 519.411778897047
time_elpased: 1.766
batch start
#iterations: 115
currently lose_sum: 519.8086534440517
time_elpased: 1.8
batch start
#iterations: 116
currently lose_sum: 518.1691370308399
time_elpased: 1.712
batch start
#iterations: 117
currently lose_sum: 519.705033570528
time_elpased: 1.701
batch start
#iterations: 118
currently lose_sum: 518.6754265129566
time_elpased: 1.712
batch start
#iterations: 119
currently lose_sum: 518.7404237091541
time_elpased: 1.693
start validation test
0.647731958763
0.985949280329
0.297518097208
0.457102001907
0
validation finish
batch start
#iterations: 120
currently lose_sum: 520.0906144082546
time_elpased: 1.702
batch start
#iterations: 121
currently lose_sum: 518.6369146704674
time_elpased: 1.69
batch start
#iterations: 122
currently lose_sum: 517.7911107242107
time_elpased: 1.718
batch start
#iterations: 123
currently lose_sum: 517.9678193330765
time_elpased: 1.678
batch start
#iterations: 124
currently lose_sum: 518.3549817204475
time_elpased: 1.689
batch start
#iterations: 125
currently lose_sum: 519.5703883171082
time_elpased: 1.729
batch start
#iterations: 126
currently lose_sum: 518.3667578995228
time_elpased: 1.702
batch start
#iterations: 127
currently lose_sum: 520.1420767307281
time_elpased: 1.718
batch start
#iterations: 128
currently lose_sum: 517.4665673077106
time_elpased: 1.671
batch start
#iterations: 129
currently lose_sum: 518.6588421165943
time_elpased: 1.69
batch start
#iterations: 130
currently lose_sum: 519.0779941082001
time_elpased: 1.714
batch start
#iterations: 131
currently lose_sum: 519.3612631559372
time_elpased: 1.693
batch start
#iterations: 132
currently lose_sum: 520.4865063428879
time_elpased: 1.704
batch start
#iterations: 133
currently lose_sum: 519.2651870548725
time_elpased: 1.686
batch start
#iterations: 134
currently lose_sum: 519.5500692725182
time_elpased: 1.695
batch start
#iterations: 135
currently lose_sum: 516.4679417908192
time_elpased: 1.681
batch start
#iterations: 136
currently lose_sum: 518.0654191076756
time_elpased: 1.681
batch start
#iterations: 137
currently lose_sum: 517.9976135194302
time_elpased: 1.671
batch start
#iterations: 138
currently lose_sum: 519.0718899965286
time_elpased: 1.714
batch start
#iterations: 139
currently lose_sum: 519.9183261692524
time_elpased: 1.688
start validation test
0.565721649485
1.0
0.128748707342
0.228126431516
0
validation finish
batch start
#iterations: 140
currently lose_sum: 515.7356830537319
time_elpased: 1.702
batch start
#iterations: 141
currently lose_sum: 520.1547066271305
time_elpased: 1.674
batch start
#iterations: 142
currently lose_sum: 518.530390560627
time_elpased: 1.679
batch start
#iterations: 143
currently lose_sum: 519.0263513326645
time_elpased: 1.696
batch start
#iterations: 144
currently lose_sum: 519.8757459819317
time_elpased: 1.713
batch start
#iterations: 145
currently lose_sum: 520.1883336603642
time_elpased: 1.695
batch start
#iterations: 146
currently lose_sum: 520.6984555125237
time_elpased: 1.698
batch start
#iterations: 147
currently lose_sum: 518.1988891661167
time_elpased: 1.688
batch start
#iterations: 148
currently lose_sum: 517.3882923722267
time_elpased: 1.68
batch start
#iterations: 149
currently lose_sum: 519.6459244787693
time_elpased: 1.716
batch start
#iterations: 150
currently lose_sum: 517.5076834261417
time_elpased: 1.677
batch start
#iterations: 151
currently lose_sum: 518.1494505107403
time_elpased: 1.697
batch start
#iterations: 152
currently lose_sum: 518.8469274640083
time_elpased: 1.703
batch start
#iterations: 153
currently lose_sum: 516.0391755700111
time_elpased: 1.687
batch start
#iterations: 154
currently lose_sum: 521.3695731759071
time_elpased: 1.69
batch start
#iterations: 155
currently lose_sum: 517.1836439371109
time_elpased: 1.704
batch start
#iterations: 156
currently lose_sum: 519.4996042251587
time_elpased: 1.697
batch start
#iterations: 157
currently lose_sum: 518.0714847743511
time_elpased: 1.705
batch start
#iterations: 158
currently lose_sum: 517.1869763433933
time_elpased: 1.696
batch start
#iterations: 159
currently lose_sum: 518.3931476473808
time_elpased: 1.725
start validation test
0.575927835052
0.999307958478
0.149327817994
0.259829059829
0
validation finish
batch start
#iterations: 160
currently lose_sum: 518.8788527846336
time_elpased: 1.723
batch start
#iterations: 161
currently lose_sum: 518.9502094686031
time_elpased: 1.676
batch start
#iterations: 162
currently lose_sum: 517.4397571086884
time_elpased: 1.697
batch start
#iterations: 163
currently lose_sum: 515.8613465130329
time_elpased: 1.697
batch start
#iterations: 164
currently lose_sum: 520.4182142913342
time_elpased: 1.712
batch start
#iterations: 165
currently lose_sum: 517.483464539051
time_elpased: 1.707
batch start
#iterations: 166
currently lose_sum: 517.6595881581306
time_elpased: 1.669
batch start
#iterations: 167
currently lose_sum: 519.4848407506943
time_elpased: 1.686
batch start
#iterations: 168
currently lose_sum: 518.5906089246273
time_elpased: 1.699
batch start
#iterations: 169
currently lose_sum: 520.0369924604893
time_elpased: 1.678
batch start
#iterations: 170
currently lose_sum: 518.3399533629417
time_elpased: 1.703
batch start
#iterations: 171
currently lose_sum: 517.2591178119183
time_elpased: 1.701
batch start
#iterations: 172
currently lose_sum: 520.3758750259876
time_elpased: 1.693
batch start
#iterations: 173
currently lose_sum: 518.18431442976
time_elpased: 1.669
batch start
#iterations: 174
currently lose_sum: 519.1876731812954
time_elpased: 1.703
batch start
#iterations: 175
currently lose_sum: 518.4698642790318
time_elpased: 1.688
batch start
#iterations: 176
currently lose_sum: 517.9562528729439
time_elpased: 1.684
batch start
#iterations: 177
currently lose_sum: 518.7120425999165
time_elpased: 1.701
batch start
#iterations: 178
currently lose_sum: 516.9906583428383
time_elpased: 1.693
batch start
#iterations: 179
currently lose_sum: 518.1141918599606
time_elpased: 1.697
start validation test
0.604381443299
0.997506234414
0.206825232678
0.3426124197
0
validation finish
batch start
#iterations: 180
currently lose_sum: 517.6267584562302
time_elpased: 1.717
batch start
#iterations: 181
currently lose_sum: 519.5467296242714
time_elpased: 1.681
batch start
#iterations: 182
currently lose_sum: 516.8837438225746
time_elpased: 1.688
batch start
#iterations: 183
currently lose_sum: 518.6926874518394
time_elpased: 1.69
batch start
#iterations: 184
currently lose_sum: 518.0672597289085
time_elpased: 1.675
batch start
#iterations: 185
currently lose_sum: 516.165928542614
time_elpased: 1.675
batch start
#iterations: 186
currently lose_sum: 519.821194678545
time_elpased: 1.705
batch start
#iterations: 187
currently lose_sum: 516.5994573235512
time_elpased: 1.671
batch start
#iterations: 188
currently lose_sum: 516.3122390210629
time_elpased: 1.692
batch start
#iterations: 189
currently lose_sum: 517.2805407345295
time_elpased: 1.67
batch start
#iterations: 190
currently lose_sum: 515.351520717144
time_elpased: 1.688
batch start
#iterations: 191
currently lose_sum: 518.5524660944939
time_elpased: 1.678
batch start
#iterations: 192
currently lose_sum: 517.9686754643917
time_elpased: 1.668
batch start
#iterations: 193
currently lose_sum: 518.03229829669
time_elpased: 1.724
batch start
#iterations: 194
currently lose_sum: 517.8408785760403
time_elpased: 1.709
batch start
#iterations: 195
currently lose_sum: 518.1935953497887
time_elpased: 1.695
batch start
#iterations: 196
currently lose_sum: 516.301926612854
time_elpased: 1.671
batch start
#iterations: 197
currently lose_sum: 519.1467187702656
time_elpased: 1.703
batch start
#iterations: 198
currently lose_sum: 517.8472293317318
time_elpased: 1.714
batch start
#iterations: 199
currently lose_sum: 517.2058538794518
time_elpased: 1.713
start validation test
0.604587628866
0.998006975585
0.207135470527
0.343067568725
0
validation finish
batch start
#iterations: 200
currently lose_sum: 519.6544326841831
time_elpased: 1.696
batch start
#iterations: 201
currently lose_sum: 517.2750352025032
time_elpased: 1.704
batch start
#iterations: 202
currently lose_sum: 517.8314246237278
time_elpased: 1.681
batch start
#iterations: 203
currently lose_sum: 516.613932877779
time_elpased: 1.707
batch start
#iterations: 204
currently lose_sum: 517.9460742175579
time_elpased: 1.725
batch start
#iterations: 205
currently lose_sum: 518.5920795798302
time_elpased: 1.7
batch start
#iterations: 206
currently lose_sum: 518.5473403930664
time_elpased: 1.678
batch start
#iterations: 207
currently lose_sum: 516.6089541614056
time_elpased: 1.711
batch start
#iterations: 208
currently lose_sum: 517.1714108884335
time_elpased: 1.685
batch start
#iterations: 209
currently lose_sum: 516.7935956120491
time_elpased: 1.705
batch start
#iterations: 210
currently lose_sum: 518.9260229170322
time_elpased: 1.684
batch start
#iterations: 211
currently lose_sum: 518.1591044962406
time_elpased: 1.707
batch start
#iterations: 212
currently lose_sum: 517.6048613786697
time_elpased: 1.717
batch start
#iterations: 213
currently lose_sum: 516.650469660759
time_elpased: 1.69
batch start
#iterations: 214
currently lose_sum: 518.3588936328888
time_elpased: 1.689
batch start
#iterations: 215
currently lose_sum: 518.0383334457874
time_elpased: 1.697
batch start
#iterations: 216
currently lose_sum: 517.7450540363789
time_elpased: 1.713
batch start
#iterations: 217
currently lose_sum: 517.0540791749954
time_elpased: 1.708
batch start
#iterations: 218
currently lose_sum: 518.6102495193481
time_elpased: 1.715
batch start
#iterations: 219
currently lose_sum: 518.7248931229115
time_elpased: 1.709
start validation test
0.566443298969
1.0
0.130196483971
0.230396193613
0
validation finish
batch start
#iterations: 220
currently lose_sum: 516.0540755391121
time_elpased: 1.704
batch start
#iterations: 221
currently lose_sum: 518.3926995098591
time_elpased: 1.698
batch start
#iterations: 222
currently lose_sum: 516.7963481247425
time_elpased: 1.723
batch start
#iterations: 223
currently lose_sum: 518.061176866293
time_elpased: 1.739
batch start
#iterations: 224
currently lose_sum: 516.3898639380932
time_elpased: 1.709
batch start
#iterations: 225
currently lose_sum: 519.4042084515095
time_elpased: 1.719
batch start
#iterations: 226
currently lose_sum: 518.3210441470146
time_elpased: 1.743
batch start
#iterations: 227
currently lose_sum: 515.5132988393307
time_elpased: 1.715
batch start
#iterations: 228
currently lose_sum: 515.2276521921158
time_elpased: 1.728
batch start
#iterations: 229
currently lose_sum: 517.0008058845997
time_elpased: 1.727
batch start
#iterations: 230
currently lose_sum: 519.0768355131149
time_elpased: 1.721
batch start
#iterations: 231
currently lose_sum: 520.1117883622646
time_elpased: 1.692
batch start
#iterations: 232
currently lose_sum: 516.5163928568363
time_elpased: 1.695
batch start
#iterations: 233
currently lose_sum: 515.2736351191998
time_elpased: 1.758
batch start
#iterations: 234
currently lose_sum: 515.8432989418507
time_elpased: 1.698
batch start
#iterations: 235
currently lose_sum: 516.5504302680492
time_elpased: 1.708
batch start
#iterations: 236
currently lose_sum: 516.0757974982262
time_elpased: 1.718
batch start
#iterations: 237
currently lose_sum: 517.1898682713509
time_elpased: 1.723
batch start
#iterations: 238
currently lose_sum: 515.6921580433846
time_elpased: 1.713
batch start
#iterations: 239
currently lose_sum: 515.9280851483345
time_elpased: 1.719
start validation test
0.552010309278
1.0
0.101240951396
0.183867029768
0
validation finish
batch start
#iterations: 240
currently lose_sum: 515.8038514554501
time_elpased: 1.718
batch start
#iterations: 241
currently lose_sum: 518.4423463046551
time_elpased: 1.717
batch start
#iterations: 242
currently lose_sum: 515.2681428492069
time_elpased: 1.744
batch start
#iterations: 243
currently lose_sum: 515.5065386593342
time_elpased: 1.711
batch start
#iterations: 244
currently lose_sum: 516.5381442606449
time_elpased: 1.733
batch start
#iterations: 245
currently lose_sum: 517.2644399702549
time_elpased: 1.695
batch start
#iterations: 246
currently lose_sum: 516.7984902858734
time_elpased: 1.734
batch start
#iterations: 247
currently lose_sum: 517.1482380926609
time_elpased: 1.711
batch start
#iterations: 248
currently lose_sum: 516.6031098663807
time_elpased: 1.751
batch start
#iterations: 249
currently lose_sum: 517.7700998187065
time_elpased: 1.712
batch start
#iterations: 250
currently lose_sum: 516.3233918845654
time_elpased: 1.707
batch start
#iterations: 251
currently lose_sum: 518.1563909351826
time_elpased: 1.733
batch start
#iterations: 252
currently lose_sum: 517.4410670101643
time_elpased: 1.71
batch start
#iterations: 253
currently lose_sum: 516.5046562254429
time_elpased: 1.716
batch start
#iterations: 254
currently lose_sum: 517.9096532166004
time_elpased: 1.697
batch start
#iterations: 255
currently lose_sum: 515.785047441721
time_elpased: 1.726
batch start
#iterations: 256
currently lose_sum: 516.0667897164822
time_elpased: 1.718
batch start
#iterations: 257
currently lose_sum: 515.1400365829468
time_elpased: 1.709
batch start
#iterations: 258
currently lose_sum: 517.8972033858299
time_elpased: 1.808
batch start
#iterations: 259
currently lose_sum: 517.7783044874668
time_elpased: 1.799
start validation test
0.589484536082
0.998830409357
0.176628748707
0.300175746924
0
validation finish
batch start
#iterations: 260
currently lose_sum: 519.1933488845825
time_elpased: 1.734
batch start
#iterations: 261
currently lose_sum: 518.586946606636
time_elpased: 1.702
batch start
#iterations: 262
currently lose_sum: 516.8396300971508
time_elpased: 1.705
batch start
#iterations: 263
currently lose_sum: 516.8344224989414
time_elpased: 1.736
batch start
#iterations: 264
currently lose_sum: 517.2619932293892
time_elpased: 1.712
batch start
#iterations: 265
currently lose_sum: 517.9020852148533
time_elpased: 1.716
batch start
#iterations: 266
currently lose_sum: 515.7753580212593
time_elpased: 1.711
batch start
#iterations: 267
currently lose_sum: 515.1836223900318
time_elpased: 1.755
batch start
#iterations: 268
currently lose_sum: 516.6502908468246
time_elpased: 1.73
batch start
#iterations: 269
currently lose_sum: 517.0786610543728
time_elpased: 1.769
batch start
#iterations: 270
currently lose_sum: 515.4733574986458
time_elpased: 1.741
batch start
#iterations: 271
currently lose_sum: 514.9769957959652
time_elpased: 1.735
batch start
#iterations: 272
currently lose_sum: 517.1631481647491
time_elpased: 1.75
batch start
#iterations: 273
currently lose_sum: 514.8155928254128
time_elpased: 1.706
batch start
#iterations: 274
currently lose_sum: 516.4733504652977
time_elpased: 1.708
batch start
#iterations: 275
currently lose_sum: 515.8989164531231
time_elpased: 1.714
batch start
#iterations: 276
currently lose_sum: 517.0068708658218
time_elpased: 1.715
batch start
#iterations: 277
currently lose_sum: 516.7032340466976
time_elpased: 1.698
batch start
#iterations: 278
currently lose_sum: 516.6504473984241
time_elpased: 1.702
batch start
#iterations: 279
currently lose_sum: 518.7176198959351
time_elpased: 1.728
start validation test
0.578969072165
1.0
0.155325749741
0.268886501969
0
validation finish
batch start
#iterations: 280
currently lose_sum: 514.7112902402878
time_elpased: 1.822
batch start
#iterations: 281
currently lose_sum: 515.0308701694012
time_elpased: 1.765
batch start
#iterations: 282
currently lose_sum: 517.8117528557777
time_elpased: 1.736
batch start
#iterations: 283
currently lose_sum: 516.2304662466049
time_elpased: 1.712
batch start
#iterations: 284
currently lose_sum: 517.4891709685326
time_elpased: 1.693
batch start
#iterations: 285
currently lose_sum: 516.6363234519958
time_elpased: 1.744
batch start
#iterations: 286
currently lose_sum: 518.1017120778561
time_elpased: 1.729
batch start
#iterations: 287
currently lose_sum: 517.0598866045475
time_elpased: 1.841
batch start
#iterations: 288
currently lose_sum: 516.9903679788113
time_elpased: 1.824
batch start
#iterations: 289
currently lose_sum: 517.1815376877785
time_elpased: 1.798
batch start
#iterations: 290
currently lose_sum: 515.49092066288
time_elpased: 1.715
batch start
#iterations: 291
currently lose_sum: 517.0889947414398
time_elpased: 1.783
batch start
#iterations: 292
currently lose_sum: 517.0820592343807
time_elpased: 1.765
batch start
#iterations: 293
currently lose_sum: 517.1108787953854
time_elpased: 1.722
batch start
#iterations: 294
currently lose_sum: 518.0870467722416
time_elpased: 1.74
batch start
#iterations: 295
currently lose_sum: 516.3798069059849
time_elpased: 1.743
batch start
#iterations: 296
currently lose_sum: 514.7765508890152
time_elpased: 1.821
batch start
#iterations: 297
currently lose_sum: 517.8044428229332
time_elpased: 1.719
batch start
#iterations: 298
currently lose_sum: 515.9552034437656
time_elpased: 1.709
batch start
#iterations: 299
currently lose_sum: 517.0293461084366
time_elpased: 1.765
start validation test
0.617628865979
0.996910856134
0.23360910031
0.378518766756
0
validation finish
batch start
#iterations: 300
currently lose_sum: 516.0832516551018
time_elpased: 1.756
batch start
#iterations: 301
currently lose_sum: 516.4762021303177
time_elpased: 1.794
batch start
#iterations: 302
currently lose_sum: 519.039221227169
time_elpased: 1.739
batch start
#iterations: 303
currently lose_sum: 519.3609760701656
time_elpased: 1.746
batch start
#iterations: 304
currently lose_sum: 512.8469715416431
time_elpased: 1.749
batch start
#iterations: 305
currently lose_sum: 517.7472460865974
time_elpased: 1.749
batch start
#iterations: 306
currently lose_sum: 516.2178572118282
time_elpased: 1.791
batch start
#iterations: 307
currently lose_sum: 516.357085108757
time_elpased: 1.807
batch start
#iterations: 308
currently lose_sum: 516.5726220011711
time_elpased: 1.774
batch start
#iterations: 309
currently lose_sum: 517.2188030481339
time_elpased: 1.809
batch start
#iterations: 310
currently lose_sum: 516.2106794118881
time_elpased: 1.768
batch start
#iterations: 311
currently lose_sum: 514.6242295503616
time_elpased: 1.828
batch start
#iterations: 312
currently lose_sum: 516.8139270544052
time_elpased: 1.794
batch start
#iterations: 313
currently lose_sum: 518.3052378892899
time_elpased: 1.715
batch start
#iterations: 314
currently lose_sum: 515.6155396997929
time_elpased: 1.714
batch start
#iterations: 315
currently lose_sum: 515.9216068983078
time_elpased: 1.732
batch start
#iterations: 316
currently lose_sum: 516.2627713084221
time_elpased: 1.716
batch start
#iterations: 317
currently lose_sum: 516.425436347723
time_elpased: 1.735
batch start
#iterations: 318
currently lose_sum: 519.0986505746841
time_elpased: 1.736
batch start
#iterations: 319
currently lose_sum: 515.7891911566257
time_elpased: 1.734
start validation test
0.557113402062
1.0
0.111478800414
0.20059545962
0
validation finish
batch start
#iterations: 320
currently lose_sum: 516.9353726804256
time_elpased: 1.847
batch start
#iterations: 321
currently lose_sum: 518.4275816380978
time_elpased: 1.827
batch start
#iterations: 322
currently lose_sum: 515.6905343234539
time_elpased: 1.865
batch start
#iterations: 323
currently lose_sum: 514.9322499334812
time_elpased: 1.816
batch start
#iterations: 324
currently lose_sum: 519.6367629170418
time_elpased: 1.801
batch start
#iterations: 325
currently lose_sum: 515.3214910328388
time_elpased: 1.796
batch start
#iterations: 326
currently lose_sum: 515.5118436813354
time_elpased: 1.813
batch start
#iterations: 327
currently lose_sum: 516.812553614378
time_elpased: 1.75
batch start
#iterations: 328
currently lose_sum: 516.499561548233
time_elpased: 1.827
batch start
#iterations: 329
currently lose_sum: 516.0606454610825
time_elpased: 1.824
batch start
#iterations: 330
currently lose_sum: 518.8710979819298
time_elpased: 1.795
batch start
#iterations: 331
currently lose_sum: 519.1369012892246
time_elpased: 1.77
batch start
#iterations: 332
currently lose_sum: 518.3897067904472
time_elpased: 1.788
batch start
#iterations: 333
currently lose_sum: 515.9513530433178
time_elpased: 1.76
batch start
#iterations: 334
currently lose_sum: 515.6916121840477
time_elpased: 1.765
batch start
#iterations: 335
currently lose_sum: 516.1888166964054
time_elpased: 1.766
batch start
#iterations: 336
currently lose_sum: 517.8863807320595
time_elpased: 1.739
batch start
#iterations: 337
currently lose_sum: 516.8974259793758
time_elpased: 1.809
batch start
#iterations: 338
currently lose_sum: 516.3492060601711
time_elpased: 1.827
batch start
#iterations: 339
currently lose_sum: 516.4738928973675
time_elpased: 1.731
start validation test
0.605206185567
0.998512642538
0.208273009307
0.344656455891
0
validation finish
batch start
#iterations: 340
currently lose_sum: 516.9598942101002
time_elpased: 1.77
batch start
#iterations: 341
currently lose_sum: 517.3326465189457
time_elpased: 1.845
batch start
#iterations: 342
currently lose_sum: 518.525298833847
time_elpased: 1.854
batch start
#iterations: 343
currently lose_sum: 517.5317721068859
time_elpased: 1.852
batch start
#iterations: 344
currently lose_sum: 517.5611129701138
time_elpased: 1.852
batch start
#iterations: 345
currently lose_sum: 517.890388160944
time_elpased: 1.876
batch start
#iterations: 346
currently lose_sum: 514.2420037686825
time_elpased: 1.746
batch start
#iterations: 347
currently lose_sum: 516.3796671628952
time_elpased: 1.735
batch start
#iterations: 348
currently lose_sum: 515.6932578086853
time_elpased: 1.78
batch start
#iterations: 349
currently lose_sum: 516.676699668169
time_elpased: 1.792
batch start
#iterations: 350
currently lose_sum: 516.8944048583508
time_elpased: 1.838
batch start
#iterations: 351
currently lose_sum: 515.8387252390385
time_elpased: 1.798
batch start
#iterations: 352
currently lose_sum: 516.1068796217442
time_elpased: 1.792
batch start
#iterations: 353
currently lose_sum: 517.5997079312801
time_elpased: 1.782
batch start
#iterations: 354
currently lose_sum: 515.5065747201443
time_elpased: 1.745
batch start
#iterations: 355
currently lose_sum: 516.3705240488052
time_elpased: 1.757
batch start
#iterations: 356
currently lose_sum: 516.9677534997463
time_elpased: 1.755
batch start
#iterations: 357
currently lose_sum: 516.1235898435116
time_elpased: 1.778
batch start
#iterations: 358
currently lose_sum: 516.0102350115776
time_elpased: 1.753
batch start
#iterations: 359
currently lose_sum: 515.9225448668003
time_elpased: 1.85
start validation test
0.581391752577
0.999355254674
0.160289555326
0.276267712325
0
validation finish
batch start
#iterations: 360
currently lose_sum: 517.6285201609135
time_elpased: 1.818
batch start
#iterations: 361
currently lose_sum: 514.7809138000011
time_elpased: 1.873
batch start
#iterations: 362
currently lose_sum: 514.1686827242374
time_elpased: 1.803
batch start
#iterations: 363
currently lose_sum: 516.6783351004124
time_elpased: 1.765
batch start
#iterations: 364
currently lose_sum: 517.1139296293259
time_elpased: 1.791
batch start
#iterations: 365
currently lose_sum: 515.5870295763016
time_elpased: 1.812
batch start
#iterations: 366
currently lose_sum: 515.7248027026653
time_elpased: 1.835
batch start
#iterations: 367
currently lose_sum: 517.5912292897701
time_elpased: 1.836
batch start
#iterations: 368
currently lose_sum: 513.8545975983143
time_elpased: 1.829
batch start
#iterations: 369
currently lose_sum: 515.5181646347046
time_elpased: 1.725
batch start
#iterations: 370
currently lose_sum: 514.7960934937
time_elpased: 1.765
batch start
#iterations: 371
currently lose_sum: 517.5888326764107
time_elpased: 1.77
batch start
#iterations: 372
currently lose_sum: 516.233823210001
time_elpased: 1.798
batch start
#iterations: 373
currently lose_sum: 516.7502528727055
time_elpased: 1.852
batch start
#iterations: 374
currently lose_sum: 517.8981837928295
time_elpased: 1.818
batch start
#iterations: 375
currently lose_sum: 518.1502161622047
time_elpased: 1.744
batch start
#iterations: 376
currently lose_sum: 516.4018077552319
time_elpased: 1.721
batch start
#iterations: 377
currently lose_sum: 515.6022190749645
time_elpased: 1.735
batch start
#iterations: 378
currently lose_sum: 515.7525590658188
time_elpased: 1.738
batch start
#iterations: 379
currently lose_sum: 514.0049639344215
time_elpased: 1.786
start validation test
0.591082474227
0.999424956872
0.179731127198
0.304671750373
0
validation finish
batch start
#iterations: 380
currently lose_sum: 517.3363327085972
time_elpased: 1.76
batch start
#iterations: 381
currently lose_sum: 515.8749061226845
time_elpased: 1.818
batch start
#iterations: 382
currently lose_sum: 517.1799115240574
time_elpased: 1.879
batch start
#iterations: 383
currently lose_sum: 514.7998838126659
time_elpased: 1.849
batch start
#iterations: 384
currently lose_sum: 514.6169980168343
time_elpased: 1.763
batch start
#iterations: 385
currently lose_sum: 514.7897185087204
time_elpased: 1.762
batch start
#iterations: 386
currently lose_sum: 515.5969382226467
time_elpased: 1.799
batch start
#iterations: 387
currently lose_sum: 516.1717528998852
time_elpased: 1.774
batch start
#iterations: 388
currently lose_sum: 516.4186736345291
time_elpased: 1.788
batch start
#iterations: 389
currently lose_sum: 516.135277479887
time_elpased: 1.791
batch start
#iterations: 390
currently lose_sum: 516.8814876675606
time_elpased: 1.848
batch start
#iterations: 391
currently lose_sum: 516.5144519209862
time_elpased: 1.799
batch start
#iterations: 392
currently lose_sum: 515.6713256537914
time_elpased: 1.798
batch start
#iterations: 393
currently lose_sum: 515.3636619150639
time_elpased: 1.831
batch start
#iterations: 394
currently lose_sum: 516.4312853515148
time_elpased: 1.864
batch start
#iterations: 395
currently lose_sum: 514.9218743741512
time_elpased: 1.888
batch start
#iterations: 396
currently lose_sum: 516.8321578502655
time_elpased: 1.835
batch start
#iterations: 397
currently lose_sum: 517.5648694038391
time_elpased: 1.795
batch start
#iterations: 398
currently lose_sum: 515.9127712249756
time_elpased: 1.823
batch start
#iterations: 399
currently lose_sum: 515.4981424510479
time_elpased: 1.852
start validation test
0.588556701031
0.999408284024
0.174663908997
0.29735915493
0
validation finish
acc: 0.643
pre: 0.990
rec: 0.296
F1: 0.455
auc: 0.000
