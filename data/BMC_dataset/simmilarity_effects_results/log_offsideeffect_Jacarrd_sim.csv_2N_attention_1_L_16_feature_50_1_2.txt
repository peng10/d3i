start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 550.7428758144379
time_elpased: 1.699
batch start
#iterations: 1
currently lose_sum: 542.0693780779839
time_elpased: 1.627
batch start
#iterations: 2
currently lose_sum: 538.4245726466179
time_elpased: 1.628
batch start
#iterations: 3
currently lose_sum: 533.4711410701275
time_elpased: 1.622
batch start
#iterations: 4
currently lose_sum: 531.1156292557716
time_elpased: 1.629
batch start
#iterations: 5
currently lose_sum: 528.934842646122
time_elpased: 1.66
batch start
#iterations: 6
currently lose_sum: 524.4212085604668
time_elpased: 1.633
batch start
#iterations: 7
currently lose_sum: 524.653599768877
time_elpased: 1.629
batch start
#iterations: 8
currently lose_sum: 522.9728957116604
time_elpased: 1.627
batch start
#iterations: 9
currently lose_sum: 521.0858538150787
time_elpased: 1.614
batch start
#iterations: 10
currently lose_sum: 519.5924670100212
time_elpased: 1.635
batch start
#iterations: 11
currently lose_sum: 521.3340402841568
time_elpased: 1.633
batch start
#iterations: 12
currently lose_sum: 518.4745078086853
time_elpased: 1.626
batch start
#iterations: 13
currently lose_sum: 517.266395509243
time_elpased: 1.619
batch start
#iterations: 14
currently lose_sum: 516.5434165596962
time_elpased: 1.616
batch start
#iterations: 15
currently lose_sum: 516.5968775749207
time_elpased: 1.633
batch start
#iterations: 16
currently lose_sum: 517.4047883749008
time_elpased: 1.639
batch start
#iterations: 17
currently lose_sum: 515.8708849847317
time_elpased: 1.618
batch start
#iterations: 18
currently lose_sum: 514.2375425696373
time_elpased: 1.615
batch start
#iterations: 19
currently lose_sum: 515.4021766483784
time_elpased: 1.619
start validation test
0.606649484536
0.923908523909
0.229782833506
0.368033126294
0
validation finish
batch start
#iterations: 20
currently lose_sum: 514.9110850989819
time_elpased: 1.614
batch start
#iterations: 21
currently lose_sum: 515.6761740148067
time_elpased: 1.632
batch start
#iterations: 22
currently lose_sum: 512.8028745651245
time_elpased: 1.614
batch start
#iterations: 23
currently lose_sum: 513.8003121316433
time_elpased: 1.628
batch start
#iterations: 24
currently lose_sum: 512.2331843078136
time_elpased: 1.636
batch start
#iterations: 25
currently lose_sum: 514.1210268735886
time_elpased: 1.615
batch start
#iterations: 26
currently lose_sum: 513.1076063811779
time_elpased: 1.62
batch start
#iterations: 27
currently lose_sum: 512.649917781353
time_elpased: 1.63
batch start
#iterations: 28
currently lose_sum: 510.8552752137184
time_elpased: 1.638
batch start
#iterations: 29
currently lose_sum: 510.7168832719326
time_elpased: 1.632
batch start
#iterations: 30
currently lose_sum: 511.1979857683182
time_elpased: 1.626
batch start
#iterations: 31
currently lose_sum: 509.5179952979088
time_elpased: 1.637
batch start
#iterations: 32
currently lose_sum: 511.9563868045807
time_elpased: 1.636
batch start
#iterations: 33
currently lose_sum: 509.7084546685219
time_elpased: 1.624
batch start
#iterations: 34
currently lose_sum: 512.2346141338348
time_elpased: 1.621
batch start
#iterations: 35
currently lose_sum: 507.2778113782406
time_elpased: 1.631
batch start
#iterations: 36
currently lose_sum: 510.1737165749073
time_elpased: 1.618
batch start
#iterations: 37
currently lose_sum: 508.10060849785805
time_elpased: 1.623
batch start
#iterations: 38
currently lose_sum: 508.858937561512
time_elpased: 1.633
batch start
#iterations: 39
currently lose_sum: 509.68317997455597
time_elpased: 1.626
start validation test
0.531443298969
0.955974842767
0.0628748707342
0.117989520668
0
validation finish
batch start
#iterations: 40
currently lose_sum: 511.53717029094696
time_elpased: 1.64
batch start
#iterations: 41
currently lose_sum: 509.79504480957985
time_elpased: 1.622
batch start
#iterations: 42
currently lose_sum: 507.76046484708786
time_elpased: 1.617
batch start
#iterations: 43
currently lose_sum: 508.838432520628
time_elpased: 1.629
batch start
#iterations: 44
currently lose_sum: 507.6283301413059
time_elpased: 1.638
batch start
#iterations: 45
currently lose_sum: 507.3046970665455
time_elpased: 1.617
batch start
#iterations: 46
currently lose_sum: 505.9716928899288
time_elpased: 1.634
batch start
#iterations: 47
currently lose_sum: 507.38773143291473
time_elpased: 1.629
batch start
#iterations: 48
currently lose_sum: 506.80790039896965
time_elpased: 1.631
batch start
#iterations: 49
currently lose_sum: 506.774727165699
time_elpased: 1.629
batch start
#iterations: 50
currently lose_sum: 507.5025757551193
time_elpased: 1.626
batch start
#iterations: 51
currently lose_sum: 506.0949223637581
time_elpased: 1.638
batch start
#iterations: 52
currently lose_sum: 506.4797957241535
time_elpased: 1.63
batch start
#iterations: 53
currently lose_sum: 509.0024820268154
time_elpased: 1.626
batch start
#iterations: 54
currently lose_sum: 505.54145991802216
time_elpased: 1.623
batch start
#iterations: 55
currently lose_sum: 508.4163555800915
time_elpased: 1.632
batch start
#iterations: 56
currently lose_sum: 506.5535794198513
time_elpased: 1.617
batch start
#iterations: 57
currently lose_sum: 506.0180007517338
time_elpased: 1.645
batch start
#iterations: 58
currently lose_sum: 507.08051270246506
time_elpased: 1.634
batch start
#iterations: 59
currently lose_sum: 504.8915790915489
time_elpased: 1.624
start validation test
0.617731958763
0.926248108926
0.253257497415
0.397758648693
0
validation finish
batch start
#iterations: 60
currently lose_sum: 507.0553583204746
time_elpased: 1.652
batch start
#iterations: 61
currently lose_sum: 507.3583731651306
time_elpased: 1.626
batch start
#iterations: 62
currently lose_sum: 508.86692467331886
time_elpased: 1.635
batch start
#iterations: 63
currently lose_sum: 507.2136241495609
time_elpased: 1.639
batch start
#iterations: 64
currently lose_sum: 506.5380375981331
time_elpased: 1.631
batch start
#iterations: 65
currently lose_sum: 505.3983477950096
time_elpased: 1.628
batch start
#iterations: 66
currently lose_sum: 505.51748663187027
time_elpased: 1.641
batch start
#iterations: 67
currently lose_sum: 504.03171196579933
time_elpased: 1.625
batch start
#iterations: 68
currently lose_sum: 506.77876737713814
time_elpased: 1.648
batch start
#iterations: 69
currently lose_sum: 505.2559390068054
time_elpased: 1.627
batch start
#iterations: 70
currently lose_sum: 505.48464757204056
time_elpased: 1.633
batch start
#iterations: 71
currently lose_sum: 505.52470630407333
time_elpased: 1.629
batch start
#iterations: 72
currently lose_sum: 505.6486876606941
time_elpased: 1.636
batch start
#iterations: 73
currently lose_sum: 503.84557062387466
time_elpased: 1.625
batch start
#iterations: 74
currently lose_sum: 505.09520399570465
time_elpased: 1.635
batch start
#iterations: 75
currently lose_sum: 503.56601136922836
time_elpased: 1.634
batch start
#iterations: 76
currently lose_sum: 505.5674612224102
time_elpased: 1.631
batch start
#iterations: 77
currently lose_sum: 504.2806514799595
time_elpased: 1.617
batch start
#iterations: 78
currently lose_sum: 504.51528149843216
time_elpased: 1.625
batch start
#iterations: 79
currently lose_sum: 506.5456075370312
time_elpased: 1.64
start validation test
0.645824742268
0.892566619916
0.329058945191
0.480846241028
0
validation finish
batch start
#iterations: 80
currently lose_sum: 502.65165519714355
time_elpased: 1.659
batch start
#iterations: 81
currently lose_sum: 503.4150200486183
time_elpased: 1.631
batch start
#iterations: 82
currently lose_sum: 503.9356387555599
time_elpased: 1.633
batch start
#iterations: 83
currently lose_sum: 504.33019906282425
time_elpased: 1.638
batch start
#iterations: 84
currently lose_sum: 504.8401801586151
time_elpased: 1.627
batch start
#iterations: 85
currently lose_sum: 502.54558500647545
time_elpased: 1.628
batch start
#iterations: 86
currently lose_sum: 502.4943891763687
time_elpased: 1.633
batch start
#iterations: 87
currently lose_sum: 502.3776694238186
time_elpased: 1.649
batch start
#iterations: 88
currently lose_sum: 503.22110962867737
time_elpased: 1.617
batch start
#iterations: 89
currently lose_sum: 502.39110401272774
time_elpased: 1.637
batch start
#iterations: 90
currently lose_sum: 505.2943064570427
time_elpased: 1.624
batch start
#iterations: 91
currently lose_sum: 501.7444071471691
time_elpased: 1.627
batch start
#iterations: 92
currently lose_sum: 503.51633393764496
time_elpased: 1.63
batch start
#iterations: 93
currently lose_sum: 502.64168402552605
time_elpased: 1.634
batch start
#iterations: 94
currently lose_sum: 502.3257288336754
time_elpased: 1.628
batch start
#iterations: 95
currently lose_sum: 502.88936147093773
time_elpased: 1.622
batch start
#iterations: 96
currently lose_sum: 503.8642162978649
time_elpased: 1.626
batch start
#iterations: 97
currently lose_sum: 502.43317353725433
time_elpased: 1.652
batch start
#iterations: 98
currently lose_sum: 502.32730308175087
time_elpased: 1.628
batch start
#iterations: 99
currently lose_sum: 501.9306048154831
time_elpased: 1.627
start validation test
0.656494845361
0.892838473602
0.353257497415
0.50622406639
0
validation finish
batch start
#iterations: 100
currently lose_sum: 502.53175964951515
time_elpased: 1.639
batch start
#iterations: 101
currently lose_sum: 501.98063710331917
time_elpased: 1.621
batch start
#iterations: 102
currently lose_sum: 500.6998811364174
time_elpased: 1.631
batch start
#iterations: 103
currently lose_sum: 501.60359212756157
time_elpased: 1.64
batch start
#iterations: 104
currently lose_sum: 502.7255645096302
time_elpased: 1.629
batch start
#iterations: 105
currently lose_sum: 501.148196965456
time_elpased: 1.646
batch start
#iterations: 106
currently lose_sum: 502.42239716649055
time_elpased: 1.637
batch start
#iterations: 107
currently lose_sum: 502.78972816467285
time_elpased: 1.628
batch start
#iterations: 108
currently lose_sum: 500.6269017159939
time_elpased: 1.636
batch start
#iterations: 109
currently lose_sum: 502.121923238039
time_elpased: 1.634
batch start
#iterations: 110
currently lose_sum: 502.00810703635216
time_elpased: 1.632
batch start
#iterations: 111
currently lose_sum: 504.2145695090294
time_elpased: 1.645
batch start
#iterations: 112
currently lose_sum: 500.73684671521187
time_elpased: 1.632
batch start
#iterations: 113
currently lose_sum: 500.8974235057831
time_elpased: 1.623
batch start
#iterations: 114
currently lose_sum: 500.8764134645462
time_elpased: 1.633
batch start
#iterations: 115
currently lose_sum: 500.6305718123913
time_elpased: 1.625
batch start
#iterations: 116
currently lose_sum: 500.8313177525997
time_elpased: 1.63
batch start
#iterations: 117
currently lose_sum: 502.32355070114136
time_elpased: 1.632
batch start
#iterations: 118
currently lose_sum: 500.6485104262829
time_elpased: 1.633
batch start
#iterations: 119
currently lose_sum: 500.4768929183483
time_elpased: 1.63
start validation test
0.731855670103
0.887309292649
0.529265770424
0.663039253789
0
validation finish
batch start
#iterations: 120
currently lose_sum: 502.5303393006325
time_elpased: 1.65
batch start
#iterations: 121
currently lose_sum: 499.4091811776161
time_elpased: 1.628
batch start
#iterations: 122
currently lose_sum: 499.3710094988346
time_elpased: 1.642
batch start
#iterations: 123
currently lose_sum: 498.8819692134857
time_elpased: 1.633
batch start
#iterations: 124
currently lose_sum: 498.5488435328007
time_elpased: 1.627
batch start
#iterations: 125
currently lose_sum: 499.61500188708305
time_elpased: 1.63
batch start
#iterations: 126
currently lose_sum: 499.1222728192806
time_elpased: 1.628
batch start
#iterations: 127
currently lose_sum: 502.30686780810356
time_elpased: 1.655
batch start
#iterations: 128
currently lose_sum: 498.93325927853584
time_elpased: 1.634
batch start
#iterations: 129
currently lose_sum: 499.3418305814266
time_elpased: 1.637
batch start
#iterations: 130
currently lose_sum: 499.0677310824394
time_elpased: 1.623
batch start
#iterations: 131
currently lose_sum: 499.6692484617233
time_elpased: 1.64
batch start
#iterations: 132
currently lose_sum: 502.0667916238308
time_elpased: 1.634
batch start
#iterations: 133
currently lose_sum: 500.3524295389652
time_elpased: 1.637
batch start
#iterations: 134
currently lose_sum: 501.0618965923786
time_elpased: 1.653
batch start
#iterations: 135
currently lose_sum: 498.3763818740845
time_elpased: 1.618
batch start
#iterations: 136
currently lose_sum: 499.6361363530159
time_elpased: 1.639
batch start
#iterations: 137
currently lose_sum: 498.9709931612015
time_elpased: 1.624
batch start
#iterations: 138
currently lose_sum: 499.7278263270855
time_elpased: 1.638
batch start
#iterations: 139
currently lose_sum: 500.5283264219761
time_elpased: 1.645
start validation test
0.609329896907
0.909197651663
0.240227507756
0.380040899796
0
validation finish
batch start
#iterations: 140
currently lose_sum: 497.26159489154816
time_elpased: 1.648
batch start
#iterations: 141
currently lose_sum: 500.6997858583927
time_elpased: 1.64
batch start
#iterations: 142
currently lose_sum: 499.5625650584698
time_elpased: 1.631
batch start
#iterations: 143
currently lose_sum: 500.5638580620289
time_elpased: 1.631
batch start
#iterations: 144
currently lose_sum: 499.87092635035515
time_elpased: 1.631
batch start
#iterations: 145
currently lose_sum: 501.739370316267
time_elpased: 1.643
batch start
#iterations: 146
currently lose_sum: 501.89253681898117
time_elpased: 1.632
batch start
#iterations: 147
currently lose_sum: 500.0531653165817
time_elpased: 1.639
batch start
#iterations: 148
currently lose_sum: 498.03452348709106
time_elpased: 1.636
batch start
#iterations: 149
currently lose_sum: 501.7378779351711
time_elpased: 1.632
batch start
#iterations: 150
currently lose_sum: 499.08925235271454
time_elpased: 1.649
batch start
#iterations: 151
currently lose_sum: 499.8688953220844
time_elpased: 1.657
batch start
#iterations: 152
currently lose_sum: 499.79271867871284
time_elpased: 1.634
batch start
#iterations: 153
currently lose_sum: 496.23376601934433
time_elpased: 1.647
batch start
#iterations: 154
currently lose_sum: 503.0917579829693
time_elpased: 1.639
batch start
#iterations: 155
currently lose_sum: 499.3725381195545
time_elpased: 1.633
batch start
#iterations: 156
currently lose_sum: 499.63748148083687
time_elpased: 1.632
batch start
#iterations: 157
currently lose_sum: 499.4655909836292
time_elpased: 1.644
batch start
#iterations: 158
currently lose_sum: 498.75680431723595
time_elpased: 1.624
batch start
#iterations: 159
currently lose_sum: 498.87744760513306
time_elpased: 1.641
start validation test
0.536649484536
0.931558935361
0.0760082730093
0.140548809638
0
validation finish
batch start
#iterations: 160
currently lose_sum: 500.02422815561295
time_elpased: 1.647
batch start
#iterations: 161
currently lose_sum: 499.61413422226906
time_elpased: 1.627
batch start
#iterations: 162
currently lose_sum: 498.2056065797806
time_elpased: 1.64
batch start
#iterations: 163
currently lose_sum: 497.276552259922
time_elpased: 1.639
batch start
#iterations: 164
currently lose_sum: 500.33223259449005
time_elpased: 1.62
batch start
#iterations: 165
currently lose_sum: 498.8049916625023
time_elpased: 1.644
batch start
#iterations: 166
currently lose_sum: 499.5670918226242
time_elpased: 1.623
batch start
#iterations: 167
currently lose_sum: 500.1189438700676
time_elpased: 1.639
batch start
#iterations: 168
currently lose_sum: 499.1150952577591
time_elpased: 1.647
batch start
#iterations: 169
currently lose_sum: 501.01058995723724
time_elpased: 1.627
batch start
#iterations: 170
currently lose_sum: 499.8327170610428
time_elpased: 1.644
batch start
#iterations: 171
currently lose_sum: 498.04796692728996
time_elpased: 1.634
batch start
#iterations: 172
currently lose_sum: 499.9702403843403
time_elpased: 1.637
batch start
#iterations: 173
currently lose_sum: 499.3907180130482
time_elpased: 1.633
batch start
#iterations: 174
currently lose_sum: 500.2776702642441
time_elpased: 1.624
batch start
#iterations: 175
currently lose_sum: 499.7793770432472
time_elpased: 1.647
batch start
#iterations: 176
currently lose_sum: 498.45960143208504
time_elpased: 1.634
batch start
#iterations: 177
currently lose_sum: 499.5653660297394
time_elpased: 1.638
batch start
#iterations: 178
currently lose_sum: 498.8158685863018
time_elpased: 1.628
batch start
#iterations: 179
currently lose_sum: 498.2968344986439
time_elpased: 1.629
start validation test
0.592010309278
0.922891566265
0.19803516029
0.326096211154
0
validation finish
batch start
#iterations: 180
currently lose_sum: 498.61727303266525
time_elpased: 1.662
batch start
#iterations: 181
currently lose_sum: 500.61593955755234
time_elpased: 1.63
batch start
#iterations: 182
currently lose_sum: 497.44964879751205
time_elpased: 1.634
batch start
#iterations: 183
currently lose_sum: 499.12624856829643
time_elpased: 1.651
batch start
#iterations: 184
currently lose_sum: 497.4440093636513
time_elpased: 1.643
batch start
#iterations: 185
currently lose_sum: 496.63220623135567
time_elpased: 1.651
batch start
#iterations: 186
currently lose_sum: 500.3976050019264
time_elpased: 1.627
batch start
#iterations: 187
currently lose_sum: 498.6735773384571
time_elpased: 1.636
batch start
#iterations: 188
currently lose_sum: 495.7027714252472
time_elpased: 1.615
batch start
#iterations: 189
currently lose_sum: 497.19809463620186
time_elpased: 1.627
batch start
#iterations: 190
currently lose_sum: 497.4231915771961
time_elpased: 1.623
batch start
#iterations: 191
currently lose_sum: 498.6704111993313
time_elpased: 1.643
batch start
#iterations: 192
currently lose_sum: 498.0284865796566
time_elpased: 1.613
batch start
#iterations: 193
currently lose_sum: 497.9724951684475
time_elpased: 1.625
batch start
#iterations: 194
currently lose_sum: 498.9452337920666
time_elpased: 1.652
batch start
#iterations: 195
currently lose_sum: 498.7669461071491
time_elpased: 1.623
batch start
#iterations: 196
currently lose_sum: 497.13966116309166
time_elpased: 1.63
batch start
#iterations: 197
currently lose_sum: 500.12565764784813
time_elpased: 1.631
batch start
#iterations: 198
currently lose_sum: 497.51465651392937
time_elpased: 1.629
batch start
#iterations: 199
currently lose_sum: 498.2407393157482
time_elpased: 1.633
start validation test
0.647680412371
0.897392767031
0.331023784902
0.483644330286
0
validation finish
batch start
#iterations: 200
currently lose_sum: 499.2462828755379
time_elpased: 1.632
batch start
#iterations: 201
currently lose_sum: 497.53516989946365
time_elpased: 1.627
batch start
#iterations: 202
currently lose_sum: 497.38638186454773
time_elpased: 1.635
batch start
#iterations: 203
currently lose_sum: 496.8175047338009
time_elpased: 1.637
batch start
#iterations: 204
currently lose_sum: 497.93075600266457
time_elpased: 1.636
batch start
#iterations: 205
currently lose_sum: 498.8146890103817
time_elpased: 1.636
batch start
#iterations: 206
currently lose_sum: 499.4122254550457
time_elpased: 1.641
batch start
#iterations: 207
currently lose_sum: 496.39319425821304
time_elpased: 1.646
batch start
#iterations: 208
currently lose_sum: 497.73691922426224
time_elpased: 1.639
batch start
#iterations: 209
currently lose_sum: 496.43712720274925
time_elpased: 1.643
batch start
#iterations: 210
currently lose_sum: 498.31013986468315
time_elpased: 1.626
batch start
#iterations: 211
currently lose_sum: 498.29538810253143
time_elpased: 1.652
batch start
#iterations: 212
currently lose_sum: 497.83077958226204
time_elpased: 1.627
batch start
#iterations: 213
currently lose_sum: 497.457694709301
time_elpased: 1.631
batch start
#iterations: 214
currently lose_sum: 497.770300000906
time_elpased: 1.628
batch start
#iterations: 215
currently lose_sum: 497.6040131151676
time_elpased: 1.62
batch start
#iterations: 216
currently lose_sum: 498.19329392910004
time_elpased: 1.623
batch start
#iterations: 217
currently lose_sum: 496.72897493839264
time_elpased: 1.622
batch start
#iterations: 218
currently lose_sum: 498.15454253554344
time_elpased: 1.64
batch start
#iterations: 219
currently lose_sum: 498.9817036688328
time_elpased: 1.643
start validation test
0.524845360825
0.924812030075
0.0508790072389
0.0964516761419
0
validation finish
batch start
#iterations: 220
currently lose_sum: 496.0604463815689
time_elpased: 1.645
batch start
#iterations: 221
currently lose_sum: 499.2868977189064
time_elpased: 1.653
batch start
#iterations: 222
currently lose_sum: 496.8887300491333
time_elpased: 1.642
batch start
#iterations: 223
currently lose_sum: 498.6707915663719
time_elpased: 1.631
batch start
#iterations: 224
currently lose_sum: 496.8179275691509
time_elpased: 1.619
batch start
#iterations: 225
currently lose_sum: 500.90935134887695
time_elpased: 1.629
batch start
#iterations: 226
currently lose_sum: 497.6402787268162
time_elpased: 1.633
batch start
#iterations: 227
currently lose_sum: 495.2484105825424
time_elpased: 1.624
batch start
#iterations: 228
currently lose_sum: 496.38766074180603
time_elpased: 1.657
batch start
#iterations: 229
currently lose_sum: 495.34114694595337
time_elpased: 1.639
batch start
#iterations: 230
currently lose_sum: 499.5252303183079
time_elpased: 1.657
batch start
#iterations: 231
currently lose_sum: 500.6076929271221
time_elpased: 1.635
batch start
#iterations: 232
currently lose_sum: 497.1605429649353
time_elpased: 1.633
batch start
#iterations: 233
currently lose_sum: 495.7566137313843
time_elpased: 1.64
batch start
#iterations: 234
currently lose_sum: 496.68950766324997
time_elpased: 1.626
batch start
#iterations: 235
currently lose_sum: 496.22762128710747
time_elpased: 1.64
batch start
#iterations: 236
currently lose_sum: 495.9395872950554
time_elpased: 1.636
batch start
#iterations: 237
currently lose_sum: 497.9399243593216
time_elpased: 1.633
batch start
#iterations: 238
currently lose_sum: 495.367310076952
time_elpased: 1.633
batch start
#iterations: 239
currently lose_sum: 496.82212895154953
time_elpased: 1.62
start validation test
0.513144329897
0.934362934363
0.0250258531541
0.0487460972908
0
validation finish
batch start
#iterations: 240
currently lose_sum: 495.06114730238914
time_elpased: 1.63
batch start
#iterations: 241
currently lose_sum: 498.67364659905434
time_elpased: 1.636
batch start
#iterations: 242
currently lose_sum: 495.58236369490623
time_elpased: 1.641
batch start
#iterations: 243
currently lose_sum: 495.175145983696
time_elpased: 1.639
batch start
#iterations: 244
currently lose_sum: 497.2227250635624
time_elpased: 1.634
batch start
#iterations: 245
currently lose_sum: 495.57893058657646
time_elpased: 1.636
batch start
#iterations: 246
currently lose_sum: 495.71271428465843
time_elpased: 1.635
batch start
#iterations: 247
currently lose_sum: 496.08705070614815
time_elpased: 1.631
batch start
#iterations: 248
currently lose_sum: 495.95255383849144
time_elpased: 1.641
batch start
#iterations: 249
currently lose_sum: 494.9880884587765
time_elpased: 1.638
batch start
#iterations: 250
currently lose_sum: 496.29334929585457
time_elpased: 1.634
batch start
#iterations: 251
currently lose_sum: 498.5931662619114
time_elpased: 1.631
batch start
#iterations: 252
currently lose_sum: 497.2512880861759
time_elpased: 1.635
batch start
#iterations: 253
currently lose_sum: 496.114238679409
time_elpased: 1.634
batch start
#iterations: 254
currently lose_sum: 498.25142538547516
time_elpased: 1.647
batch start
#iterations: 255
currently lose_sum: 496.4215834438801
time_elpased: 1.633
batch start
#iterations: 256
currently lose_sum: 495.81828913092613
time_elpased: 1.633
batch start
#iterations: 257
currently lose_sum: 495.64117655158043
time_elpased: 1.643
batch start
#iterations: 258
currently lose_sum: 497.72296434640884
time_elpased: 1.628
batch start
#iterations: 259
currently lose_sum: 497.97269490361214
time_elpased: 1.646
start validation test
0.589381443299
0.903409090909
0.197311271975
0.323883890681
0
validation finish
batch start
#iterations: 260
currently lose_sum: 498.277704924345
time_elpased: 1.656
batch start
#iterations: 261
currently lose_sum: 497.48959720134735
time_elpased: 1.632
batch start
#iterations: 262
currently lose_sum: 495.6564441919327
time_elpased: 1.631
batch start
#iterations: 263
currently lose_sum: 496.0999513864517
time_elpased: 1.631
batch start
#iterations: 264
currently lose_sum: 496.9601222872734
time_elpased: 1.627
batch start
#iterations: 265
currently lose_sum: 497.0787192285061
time_elpased: 1.639
batch start
#iterations: 266
currently lose_sum: 496.1290633380413
time_elpased: 1.626
batch start
#iterations: 267
currently lose_sum: 494.66921961307526
time_elpased: 1.635
batch start
#iterations: 268
currently lose_sum: 497.0623179972172
time_elpased: 1.64
batch start
#iterations: 269
currently lose_sum: 496.9031202197075
time_elpased: 1.632
batch start
#iterations: 270
currently lose_sum: 495.56094965338707
time_elpased: 1.643
batch start
#iterations: 271
currently lose_sum: 494.4997932314873
time_elpased: 1.657
batch start
#iterations: 272
currently lose_sum: 495.6849787533283
time_elpased: 1.642
batch start
#iterations: 273
currently lose_sum: 493.7287861406803
time_elpased: 1.63
batch start
#iterations: 274
currently lose_sum: 497.14297285676
time_elpased: 1.635
batch start
#iterations: 275
currently lose_sum: 495.3554082810879
time_elpased: 1.638
batch start
#iterations: 276
currently lose_sum: 496.1691208779812
time_elpased: 1.621
batch start
#iterations: 277
currently lose_sum: 497.42414578795433
time_elpased: 1.646
batch start
#iterations: 278
currently lose_sum: 497.7803674042225
time_elpased: 1.646
batch start
#iterations: 279
currently lose_sum: 497.40151235461235
time_elpased: 1.642
start validation test
0.554278350515
0.914170040486
0.116752843847
0.207060981201
0
validation finish
batch start
#iterations: 280
currently lose_sum: 494.3888897895813
time_elpased: 1.632
batch start
#iterations: 281
currently lose_sum: 494.1077944934368
time_elpased: 1.667
batch start
#iterations: 282
currently lose_sum: 497.6242704093456
time_elpased: 1.652
batch start
#iterations: 283
currently lose_sum: 495.69464018940926
time_elpased: 1.65
batch start
#iterations: 284
currently lose_sum: 497.85712119936943
time_elpased: 1.625
batch start
#iterations: 285
currently lose_sum: 497.4828502833843
time_elpased: 1.657
batch start
#iterations: 286
currently lose_sum: 499.09187257289886
time_elpased: 1.643
batch start
#iterations: 287
currently lose_sum: 497.0665099620819
time_elpased: 1.63
batch start
#iterations: 288
currently lose_sum: 497.88556054234505
time_elpased: 1.656
batch start
#iterations: 289
currently lose_sum: 496.37934109568596
time_elpased: 1.637
batch start
#iterations: 290
currently lose_sum: 495.2169599235058
time_elpased: 1.645
batch start
#iterations: 291
currently lose_sum: 496.43556010723114
time_elpased: 1.651
batch start
#iterations: 292
currently lose_sum: 495.3873378932476
time_elpased: 1.644
batch start
#iterations: 293
currently lose_sum: 496.91634061932564
time_elpased: 1.644
batch start
#iterations: 294
currently lose_sum: 496.4065280556679
time_elpased: 1.632
batch start
#iterations: 295
currently lose_sum: 496.3604751229286
time_elpased: 1.634
batch start
#iterations: 296
currently lose_sum: 494.91121742129326
time_elpased: 1.64
batch start
#iterations: 297
currently lose_sum: 496.9230618774891
time_elpased: 1.639
batch start
#iterations: 298
currently lose_sum: 494.6788054704666
time_elpased: 1.648
batch start
#iterations: 299
currently lose_sum: 497.3038758933544
time_elpased: 1.622
start validation test
0.638505154639
0.894330661917
0.31158221303
0.462152005522
0
validation finish
batch start
#iterations: 300
currently lose_sum: 495.4474779367447
time_elpased: 1.627
batch start
#iterations: 301
currently lose_sum: 496.1182773709297
time_elpased: 1.636
batch start
#iterations: 302
currently lose_sum: 497.6516345143318
time_elpased: 1.635
batch start
#iterations: 303
currently lose_sum: 497.6708709895611
time_elpased: 1.623
batch start
#iterations: 304
currently lose_sum: 493.25463303923607
time_elpased: 1.633
batch start
#iterations: 305
currently lose_sum: 497.55606457591057
time_elpased: 1.643
batch start
#iterations: 306
currently lose_sum: 494.8168443441391
time_elpased: 1.646
batch start
#iterations: 307
currently lose_sum: 495.7940818965435
time_elpased: 1.632
batch start
#iterations: 308
currently lose_sum: 494.8993002176285
time_elpased: 1.633
batch start
#iterations: 309
currently lose_sum: 494.60270074009895
time_elpased: 1.628
batch start
#iterations: 310
currently lose_sum: 496.05089044570923
time_elpased: 1.632
batch start
#iterations: 311
currently lose_sum: 494.2923977673054
time_elpased: 1.638
batch start
#iterations: 312
currently lose_sum: 496.2579261660576
time_elpased: 1.622
batch start
#iterations: 313
currently lose_sum: 497.618517190218
time_elpased: 1.635
batch start
#iterations: 314
currently lose_sum: 494.2204339802265
time_elpased: 1.621
batch start
#iterations: 315
currently lose_sum: 496.0431536436081
time_elpased: 1.659
batch start
#iterations: 316
currently lose_sum: 496.0636084675789
time_elpased: 1.637
batch start
#iterations: 317
currently lose_sum: 496.0990506708622
time_elpased: 1.629
batch start
#iterations: 318
currently lose_sum: 498.3899976015091
time_elpased: 1.645
batch start
#iterations: 319
currently lose_sum: 494.6633914709091
time_elpased: 1.638
start validation test
0.530567010309
0.924585218703
0.0633919338159
0.118648988677
0
validation finish
batch start
#iterations: 320
currently lose_sum: 496.47520646452904
time_elpased: 1.639
batch start
#iterations: 321
currently lose_sum: 497.93068289756775
time_elpased: 1.655
batch start
#iterations: 322
currently lose_sum: 494.4506279528141
time_elpased: 1.646
batch start
#iterations: 323
currently lose_sum: 494.0502439439297
time_elpased: 1.638
batch start
#iterations: 324
currently lose_sum: 497.0539344251156
time_elpased: 1.622
batch start
#iterations: 325
currently lose_sum: 493.64529824256897
time_elpased: 1.648
batch start
#iterations: 326
currently lose_sum: 493.3490971326828
time_elpased: 1.637
batch start
#iterations: 327
currently lose_sum: 495.9103275537491
time_elpased: 1.632
batch start
#iterations: 328
currently lose_sum: 496.59083208441734
time_elpased: 1.669
batch start
#iterations: 329
currently lose_sum: 495.08824041485786
time_elpased: 1.652
batch start
#iterations: 330
currently lose_sum: 498.46658131480217
time_elpased: 1.632
batch start
#iterations: 331
currently lose_sum: 497.8819103837013
time_elpased: 1.649
batch start
#iterations: 332
currently lose_sum: 496.6034666001797
time_elpased: 1.645
batch start
#iterations: 333
currently lose_sum: 494.53924801945686
time_elpased: 1.636
batch start
#iterations: 334
currently lose_sum: 493.5413134396076
time_elpased: 1.638
batch start
#iterations: 335
currently lose_sum: 494.473327934742
time_elpased: 1.636
batch start
#iterations: 336
currently lose_sum: 496.9104679822922
time_elpased: 1.652
batch start
#iterations: 337
currently lose_sum: 495.01082971692085
time_elpased: 1.631
batch start
#iterations: 338
currently lose_sum: 495.7231213450432
time_elpased: 1.648
batch start
#iterations: 339
currently lose_sum: 496.45197156071663
time_elpased: 1.651
start validation test
0.620979381443
0.899069927661
0.269906928645
0.415175375805
0
validation finish
batch start
#iterations: 340
currently lose_sum: 496.9381023347378
time_elpased: 1.648
batch start
#iterations: 341
currently lose_sum: 495.83712512254715
time_elpased: 1.622
batch start
#iterations: 342
currently lose_sum: 496.9783200621605
time_elpased: 1.642
batch start
#iterations: 343
currently lose_sum: 496.7931734621525
time_elpased: 1.649
batch start
#iterations: 344
currently lose_sum: 494.4963960945606
time_elpased: 1.648
batch start
#iterations: 345
currently lose_sum: 494.874135017395
time_elpased: 1.648
batch start
#iterations: 346
currently lose_sum: 493.2788806259632
time_elpased: 1.637
batch start
#iterations: 347
currently lose_sum: 495.2145086526871
time_elpased: 1.636
batch start
#iterations: 348
currently lose_sum: 496.32120871543884
time_elpased: 1.633
batch start
#iterations: 349
currently lose_sum: 495.8321085572243
time_elpased: 1.629
batch start
#iterations: 350
currently lose_sum: 496.18251368403435
time_elpased: 1.64
batch start
#iterations: 351
currently lose_sum: 494.7785576283932
time_elpased: 1.636
batch start
#iterations: 352
currently lose_sum: 496.18750151991844
time_elpased: 1.635
batch start
#iterations: 353
currently lose_sum: 496.5633149445057
time_elpased: 1.631
batch start
#iterations: 354
currently lose_sum: 494.7883749604225
time_elpased: 1.645
batch start
#iterations: 355
currently lose_sum: 496.3826299905777
time_elpased: 1.638
batch start
#iterations: 356
currently lose_sum: 496.43559578061104
time_elpased: 1.647
batch start
#iterations: 357
currently lose_sum: 494.90632966160774
time_elpased: 1.642
batch start
#iterations: 358
currently lose_sum: 496.1806386113167
time_elpased: 1.655
batch start
#iterations: 359
currently lose_sum: 495.11290004849434
time_elpased: 1.644
start validation test
0.563402061856
0.923131170663
0.135367114788
0.236111111111
0
validation finish
batch start
#iterations: 360
currently lose_sum: 497.00292831659317
time_elpased: 1.635
batch start
#iterations: 361
currently lose_sum: 493.7259630858898
time_elpased: 1.653
batch start
#iterations: 362
currently lose_sum: 493.89658680558205
time_elpased: 1.648
batch start
#iterations: 363
currently lose_sum: 495.55981543660164
time_elpased: 1.633
batch start
#iterations: 364
currently lose_sum: 496.28461998701096
time_elpased: 1.629
batch start
#iterations: 365
currently lose_sum: 495.0639491677284
time_elpased: 1.64
batch start
#iterations: 366
currently lose_sum: 495.5705607533455
time_elpased: 1.636
batch start
#iterations: 367
currently lose_sum: 497.3401316702366
time_elpased: 1.637
batch start
#iterations: 368
currently lose_sum: 492.4420481622219
time_elpased: 1.645
batch start
#iterations: 369
currently lose_sum: 495.09040850400925
time_elpased: 1.634
batch start
#iterations: 370
currently lose_sum: 494.21894785761833
time_elpased: 1.634
batch start
#iterations: 371
currently lose_sum: 495.8720354437828
time_elpased: 1.627
batch start
#iterations: 372
currently lose_sum: 495.50045678019524
time_elpased: 1.639
batch start
#iterations: 373
currently lose_sum: 495.5350425541401
time_elpased: 1.635
batch start
#iterations: 374
currently lose_sum: 497.01488083601
time_elpased: 1.633
batch start
#iterations: 375
currently lose_sum: 496.6872323155403
time_elpased: 1.635
batch start
#iterations: 376
currently lose_sum: 496.8110436499119
time_elpased: 1.638
batch start
#iterations: 377
currently lose_sum: 494.37090733647346
time_elpased: 1.632
batch start
#iterations: 378
currently lose_sum: 495.8360116779804
time_elpased: 1.625
batch start
#iterations: 379
currently lose_sum: 493.460652589798
time_elpased: 1.642
start validation test
0.588917525773
0.895104895105
0.198552223371
0.325010579771
0
validation finish
batch start
#iterations: 380
currently lose_sum: 496.0414932370186
time_elpased: 1.637
batch start
#iterations: 381
currently lose_sum: 494.71321403980255
time_elpased: 1.656
batch start
#iterations: 382
currently lose_sum: 496.36069789528847
time_elpased: 1.646
batch start
#iterations: 383
currently lose_sum: 493.2627047896385
time_elpased: 1.644
batch start
#iterations: 384
currently lose_sum: 493.19266736507416
time_elpased: 1.64
batch start
#iterations: 385
currently lose_sum: 493.8792998492718
time_elpased: 1.639
batch start
#iterations: 386
currently lose_sum: 494.5267668366432
time_elpased: 1.639
batch start
#iterations: 387
currently lose_sum: 494.4078296124935
time_elpased: 1.64
batch start
#iterations: 388
currently lose_sum: 495.33636835217476
time_elpased: 1.654
batch start
#iterations: 389
currently lose_sum: 495.916196256876
time_elpased: 1.633
batch start
#iterations: 390
currently lose_sum: 496.06564995646477
time_elpased: 1.631
batch start
#iterations: 391
currently lose_sum: 495.4224979877472
time_elpased: 1.637
batch start
#iterations: 392
currently lose_sum: 494.82923343777657
time_elpased: 1.628
batch start
#iterations: 393
currently lose_sum: 494.1198562979698
time_elpased: 1.629
batch start
#iterations: 394
currently lose_sum: 495.9191215634346
time_elpased: 1.636
batch start
#iterations: 395
currently lose_sum: 493.9296436905861
time_elpased: 1.646
batch start
#iterations: 396
currently lose_sum: 496.5603141784668
time_elpased: 1.656
batch start
#iterations: 397
currently lose_sum: 495.35301983356476
time_elpased: 1.647
batch start
#iterations: 398
currently lose_sum: 495.5340870320797
time_elpased: 1.63
batch start
#iterations: 399
currently lose_sum: 493.93154963850975
time_elpased: 1.632
start validation test
0.591237113402
0.911542100284
0.199276111686
0.327053632043
0
validation finish
acc: 0.728
pre: 0.887
rec: 0.530
F1: 0.663
auc: 0.000
