start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 551.3682066202164
time_elpased: 2.636
batch start
#iterations: 1
currently lose_sum: 541.8986072540283
time_elpased: 2.652
batch start
#iterations: 2
currently lose_sum: 538.3643164038658
time_elpased: 2.648
batch start
#iterations: 3
currently lose_sum: 533.2799057066441
time_elpased: 2.646
batch start
#iterations: 4
currently lose_sum: 530.9492569565773
time_elpased: 2.656
batch start
#iterations: 5
currently lose_sum: 528.2774581313133
time_elpased: 2.649
batch start
#iterations: 6
currently lose_sum: 524.1671150624752
time_elpased: 2.636
batch start
#iterations: 7
currently lose_sum: 523.6887136101723
time_elpased: 2.656
batch start
#iterations: 8
currently lose_sum: 522.2447203695774
time_elpased: 2.659
batch start
#iterations: 9
currently lose_sum: 521.3896256685257
time_elpased: 2.656
batch start
#iterations: 10
currently lose_sum: 520.1167563199997
time_elpased: 2.658
batch start
#iterations: 11
currently lose_sum: 521.6409076154232
time_elpased: 2.663
batch start
#iterations: 12
currently lose_sum: 517.3035174012184
time_elpased: 2.648
batch start
#iterations: 13
currently lose_sum: 517.1711887717247
time_elpased: 2.666
batch start
#iterations: 14
currently lose_sum: 516.0891976058483
time_elpased: 2.662
batch start
#iterations: 15
currently lose_sum: 514.8333972990513
time_elpased: 2.661
batch start
#iterations: 16
currently lose_sum: 517.1713119447231
time_elpased: 2.677
batch start
#iterations: 17
currently lose_sum: 514.106162250042
time_elpased: 2.675
batch start
#iterations: 18
currently lose_sum: 513.8856237828732
time_elpased: 2.657
batch start
#iterations: 19
currently lose_sum: 511.9486512839794
time_elpased: 2.675
start validation test
0.544175257732
0.918635170604
0.107142857143
0.191903499954
0
validation finish
batch start
#iterations: 20
currently lose_sum: 513.3217997252941
time_elpased: 2.652
batch start
#iterations: 21
currently lose_sum: 514.7559512853622
time_elpased: 2.653
batch start
#iterations: 22
currently lose_sum: 512.8701420724392
time_elpased: 2.643
batch start
#iterations: 23
currently lose_sum: 511.66281566023827
time_elpased: 2.658
batch start
#iterations: 24
currently lose_sum: 509.9923972785473
time_elpased: 2.649
batch start
#iterations: 25
currently lose_sum: 511.69600865244865
time_elpased: 2.654
batch start
#iterations: 26
currently lose_sum: 511.9792972803116
time_elpased: 2.658
batch start
#iterations: 27
currently lose_sum: 511.7473914027214
time_elpased: 2.659
batch start
#iterations: 28
currently lose_sum: 510.54877054691315
time_elpased: 2.661
batch start
#iterations: 29
currently lose_sum: 511.1629759669304
time_elpased: 2.66
batch start
#iterations: 30
currently lose_sum: 509.08548280596733
time_elpased: 2.664
batch start
#iterations: 31
currently lose_sum: 510.2908702790737
time_elpased: 2.664
batch start
#iterations: 32
currently lose_sum: 510.8687339127064
time_elpased: 2.678
batch start
#iterations: 33
currently lose_sum: 507.55142733454704
time_elpased: 2.668
batch start
#iterations: 34
currently lose_sum: 509.09574684500694
time_elpased: 2.658
batch start
#iterations: 35
currently lose_sum: 506.4963036775589
time_elpased: 2.663
batch start
#iterations: 36
currently lose_sum: 508.01898950338364
time_elpased: 2.676
batch start
#iterations: 37
currently lose_sum: 507.1465195417404
time_elpased: 2.675
batch start
#iterations: 38
currently lose_sum: 507.95532912015915
time_elpased: 2.661
batch start
#iterations: 39
currently lose_sum: 508.653701543808
time_elpased: 2.62
start validation test
0.58912371134
0.89164882227
0.212448979592
0.343139678616
0
validation finish
batch start
#iterations: 40
currently lose_sum: 507.31547129154205
time_elpased: 2.642
batch start
#iterations: 41
currently lose_sum: 506.43946328759193
time_elpased: 2.646
batch start
#iterations: 42
currently lose_sum: 505.3953790962696
time_elpased: 2.64
batch start
#iterations: 43
currently lose_sum: 506.39093309640884
time_elpased: 2.651
batch start
#iterations: 44
currently lose_sum: 504.86897042393684
time_elpased: 2.654
batch start
#iterations: 45
currently lose_sum: 505.98123574256897
time_elpased: 2.653
batch start
#iterations: 46
currently lose_sum: 504.47720512747765
time_elpased: 2.663
batch start
#iterations: 47
currently lose_sum: 506.9954568743706
time_elpased: 2.652
batch start
#iterations: 48
currently lose_sum: 503.1988706290722
time_elpased: 2.645
batch start
#iterations: 49
currently lose_sum: 504.2169951200485
time_elpased: 2.633
batch start
#iterations: 50
currently lose_sum: 506.56886371970177
time_elpased: 2.646
batch start
#iterations: 51
currently lose_sum: 504.94634544849396
time_elpased: 2.64
batch start
#iterations: 52
currently lose_sum: 504.57498320937157
time_elpased: 2.656
batch start
#iterations: 53
currently lose_sum: 505.17023968696594
time_elpased: 2.652
batch start
#iterations: 54
currently lose_sum: 502.8949741721153
time_elpased: 2.643
batch start
#iterations: 55
currently lose_sum: 504.8999318778515
time_elpased: 2.652
batch start
#iterations: 56
currently lose_sum: 504.9382124245167
time_elpased: 2.647
batch start
#iterations: 57
currently lose_sum: 503.85286039114
time_elpased: 2.637
batch start
#iterations: 58
currently lose_sum: 503.1507035791874
time_elpased: 2.65
batch start
#iterations: 59
currently lose_sum: 501.74567142128944
time_elpased: 2.645
start validation test
0.607783505155
0.916381603953
0.246020408163
0.387901214705
0
validation finish
batch start
#iterations: 60
currently lose_sum: 504.36004784703255
time_elpased: 2.638
batch start
#iterations: 61
currently lose_sum: 502.68861147761345
time_elpased: 2.654
batch start
#iterations: 62
currently lose_sum: 505.571510463953
time_elpased: 2.646
batch start
#iterations: 63
currently lose_sum: 501.9604504108429
time_elpased: 2.641
batch start
#iterations: 64
currently lose_sum: 503.2694030404091
time_elpased: 2.664
batch start
#iterations: 65
currently lose_sum: 503.2902230024338
time_elpased: 2.656
batch start
#iterations: 66
currently lose_sum: 501.55690720677376
time_elpased: 2.646
batch start
#iterations: 67
currently lose_sum: 502.02762296795845
time_elpased: 2.66
batch start
#iterations: 68
currently lose_sum: 504.46036276221275
time_elpased: 2.641
batch start
#iterations: 69
currently lose_sum: 502.39612889289856
time_elpased: 2.651
batch start
#iterations: 70
currently lose_sum: 502.22812044620514
time_elpased: 2.631
batch start
#iterations: 71
currently lose_sum: 502.4208292365074
time_elpased: 2.655
batch start
#iterations: 72
currently lose_sum: 503.2803819179535
time_elpased: 2.656
batch start
#iterations: 73
currently lose_sum: 501.8773030638695
time_elpased: 2.652
batch start
#iterations: 74
currently lose_sum: 501.67976674437523
time_elpased: 2.652
batch start
#iterations: 75
currently lose_sum: 501.7675074338913
time_elpased: 2.652
batch start
#iterations: 76
currently lose_sum: 502.21913635730743
time_elpased: 2.655
batch start
#iterations: 77
currently lose_sum: 501.16870602965355
time_elpased: 2.659
batch start
#iterations: 78
currently lose_sum: 500.80308067798615
time_elpased: 2.648
batch start
#iterations: 79
currently lose_sum: 502.435682028532
time_elpased: 2.648
start validation test
0.593041237113
0.90004199916
0.218673469388
0.351859453247
0
validation finish
batch start
#iterations: 80
currently lose_sum: 500.99009227752686
time_elpased: 2.653
batch start
#iterations: 81
currently lose_sum: 501.1304822564125
time_elpased: 2.653
batch start
#iterations: 82
currently lose_sum: 501.33653074502945
time_elpased: 2.653
batch start
#iterations: 83
currently lose_sum: 501.21071922779083
time_elpased: 2.655
batch start
#iterations: 84
currently lose_sum: 501.30081847310066
time_elpased: 2.656
batch start
#iterations: 85
currently lose_sum: 501.198566198349
time_elpased: 2.649
batch start
#iterations: 86
currently lose_sum: 501.8990205824375
time_elpased: 2.659
batch start
#iterations: 87
currently lose_sum: 498.8907104730606
time_elpased: 2.637
batch start
#iterations: 88
currently lose_sum: 499.35415583848953
time_elpased: 2.651
batch start
#iterations: 89
currently lose_sum: 503.2879753112793
time_elpased: 2.65
batch start
#iterations: 90
currently lose_sum: 500.0647831261158
time_elpased: 2.652
batch start
#iterations: 91
currently lose_sum: 497.52839773893356
time_elpased: 2.64
batch start
#iterations: 92
currently lose_sum: 501.31489208340645
time_elpased: 2.654
batch start
#iterations: 93
currently lose_sum: 500.0825831890106
time_elpased: 2.652
batch start
#iterations: 94
currently lose_sum: 498.45008674263954
time_elpased: 2.638
batch start
#iterations: 95
currently lose_sum: 499.38198202848434
time_elpased: 2.651
batch start
#iterations: 96
currently lose_sum: 500.91083800792694
time_elpased: 2.638
batch start
#iterations: 97
currently lose_sum: 498.9949262738228
time_elpased: 2.654
batch start
#iterations: 98
currently lose_sum: 500.1978635787964
time_elpased: 2.655
batch start
#iterations: 99
currently lose_sum: 499.7863490283489
time_elpased: 2.644
start validation test
0.640103092784
0.906990179087
0.320408163265
0.47353340371
0
validation finish
batch start
#iterations: 100
currently lose_sum: 500.0745123922825
time_elpased: 2.651
batch start
#iterations: 101
currently lose_sum: 500.0367758870125
time_elpased: 2.659
batch start
#iterations: 102
currently lose_sum: 497.7515603005886
time_elpased: 2.677
batch start
#iterations: 103
currently lose_sum: 498.3558874428272
time_elpased: 2.664
batch start
#iterations: 104
currently lose_sum: 500.48097038269043
time_elpased: 2.658
batch start
#iterations: 105
currently lose_sum: 499.159515529871
time_elpased: 2.656
batch start
#iterations: 106
currently lose_sum: 501.6974662542343
time_elpased: 2.671
batch start
#iterations: 107
currently lose_sum: 500.5948793590069
time_elpased: 2.669
batch start
#iterations: 108
currently lose_sum: 498.7979425787926
time_elpased: 2.656
batch start
#iterations: 109
currently lose_sum: 499.10778906941414
time_elpased: 2.654
batch start
#iterations: 110
currently lose_sum: 499.1540524363518
time_elpased: 2.67
batch start
#iterations: 111
currently lose_sum: 502.84212252497673
time_elpased: 2.663
batch start
#iterations: 112
currently lose_sum: 498.5250527560711
time_elpased: 2.666
batch start
#iterations: 113
currently lose_sum: 498.93877932429314
time_elpased: 2.653
batch start
#iterations: 114
currently lose_sum: 498.6005718111992
time_elpased: 2.654
batch start
#iterations: 115
currently lose_sum: 499.91618967056274
time_elpased: 2.649
batch start
#iterations: 116
currently lose_sum: 499.2013076245785
time_elpased: 2.651
batch start
#iterations: 117
currently lose_sum: 497.6727624833584
time_elpased: 2.647
batch start
#iterations: 118
currently lose_sum: 499.51387548446655
time_elpased: 2.664
batch start
#iterations: 119
currently lose_sum: 498.8052172064781
time_elpased: 2.664
start validation test
0.735
0.862568093385
0.565510204082
0.683143297381
0
validation finish
batch start
#iterations: 120
currently lose_sum: 497.9768825173378
time_elpased: 2.66
batch start
#iterations: 121
currently lose_sum: 498.13503354787827
time_elpased: 2.657
batch start
#iterations: 122
currently lose_sum: 498.7433746755123
time_elpased: 2.676
batch start
#iterations: 123
currently lose_sum: 499.21519580483437
time_elpased: 2.658
batch start
#iterations: 124
currently lose_sum: 499.0788415968418
time_elpased: 2.661
batch start
#iterations: 125
currently lose_sum: 500.4754054546356
time_elpased: 2.668
batch start
#iterations: 126
currently lose_sum: 497.43968892097473
time_elpased: 2.666
batch start
#iterations: 127
currently lose_sum: 500.8489980995655
time_elpased: 2.652
batch start
#iterations: 128
currently lose_sum: 499.11734345555305
time_elpased: 2.672
batch start
#iterations: 129
currently lose_sum: 498.67570492625237
time_elpased: 2.666
batch start
#iterations: 130
currently lose_sum: 498.5859919786453
time_elpased: 2.664
batch start
#iterations: 131
currently lose_sum: 498.1305878162384
time_elpased: 2.669
batch start
#iterations: 132
currently lose_sum: 497.8056027293205
time_elpased: 2.663
batch start
#iterations: 133
currently lose_sum: 501.2618787586689
time_elpased: 2.65
batch start
#iterations: 134
currently lose_sum: 498.3981937468052
time_elpased: 2.668
batch start
#iterations: 135
currently lose_sum: 495.57446908950806
time_elpased: 2.652
batch start
#iterations: 136
currently lose_sum: 497.1126131117344
time_elpased: 2.658
batch start
#iterations: 137
currently lose_sum: 496.9401938319206
time_elpased: 2.662
batch start
#iterations: 138
currently lose_sum: 498.9017248749733
time_elpased: 2.67
batch start
#iterations: 139
currently lose_sum: 498.02279755473137
time_elpased: 2.653
start validation test
0.540051546392
0.904893813481
0.1
0.180097399614
0
validation finish
batch start
#iterations: 140
currently lose_sum: 496.59625789523125
time_elpased: 2.66
batch start
#iterations: 141
currently lose_sum: 495.4690777659416
time_elpased: 2.669
batch start
#iterations: 142
currently lose_sum: 497.374086946249
time_elpased: 2.663
batch start
#iterations: 143
currently lose_sum: 497.52942630648613
time_elpased: 2.674
batch start
#iterations: 144
currently lose_sum: 498.9653312563896
time_elpased: 2.674
batch start
#iterations: 145
currently lose_sum: 496.19347086548805
time_elpased: 2.652
batch start
#iterations: 146
currently lose_sum: 499.3130011856556
time_elpased: 2.659
batch start
#iterations: 147
currently lose_sum: 498.3745444715023
time_elpased: 2.654
batch start
#iterations: 148
currently lose_sum: 495.9404440820217
time_elpased: 2.648
batch start
#iterations: 149
currently lose_sum: 499.1375040411949
time_elpased: 2.651
batch start
#iterations: 150
currently lose_sum: 495.51183462142944
time_elpased: 2.654
batch start
#iterations: 151
currently lose_sum: 494.86007422208786
time_elpased: 2.661
batch start
#iterations: 152
currently lose_sum: 498.6303830444813
time_elpased: 2.67
batch start
#iterations: 153
currently lose_sum: 495.9203751385212
time_elpased: 2.666
batch start
#iterations: 154
currently lose_sum: 500.55643916130066
time_elpased: 2.658
batch start
#iterations: 155
currently lose_sum: 496.3723904788494
time_elpased: 2.672
batch start
#iterations: 156
currently lose_sum: 497.7972213923931
time_elpased: 2.673
batch start
#iterations: 157
currently lose_sum: 497.48182222247124
time_elpased: 2.666
batch start
#iterations: 158
currently lose_sum: 495.8434954583645
time_elpased: 2.672
batch start
#iterations: 159
currently lose_sum: 498.1937183737755
time_elpased: 2.675
start validation test
0.55706185567
0.915347556779
0.135714285714
0.236381409402
0
validation finish
batch start
#iterations: 160
currently lose_sum: 498.8068876564503
time_elpased: 2.664
batch start
#iterations: 161
currently lose_sum: 497.64351683855057
time_elpased: 2.658
batch start
#iterations: 162
currently lose_sum: 495.4884609282017
time_elpased: 2.675
batch start
#iterations: 163
currently lose_sum: 495.01497533917427
time_elpased: 2.665
batch start
#iterations: 164
currently lose_sum: 497.26551139354706
time_elpased: 2.661
batch start
#iterations: 165
currently lose_sum: 498.1482221484184
time_elpased: 2.666
batch start
#iterations: 166
currently lose_sum: 497.9944248199463
time_elpased: 2.669
batch start
#iterations: 167
currently lose_sum: 496.7784972190857
time_elpased: 2.655
batch start
#iterations: 168
currently lose_sum: 497.28857323527336
time_elpased: 2.663
batch start
#iterations: 169
currently lose_sum: 496.9424492716789
time_elpased: 2.665
batch start
#iterations: 170
currently lose_sum: 497.5484031736851
time_elpased: 2.669
batch start
#iterations: 171
currently lose_sum: 495.90424713492393
time_elpased: 2.67
batch start
#iterations: 172
currently lose_sum: 500.01632845401764
time_elpased: 2.666
batch start
#iterations: 173
currently lose_sum: 497.9829313158989
time_elpased: 2.644
batch start
#iterations: 174
currently lose_sum: 497.6885467171669
time_elpased: 2.659
batch start
#iterations: 175
currently lose_sum: 495.8286681175232
time_elpased: 2.632
batch start
#iterations: 176
currently lose_sum: 496.7194899022579
time_elpased: 2.656
batch start
#iterations: 177
currently lose_sum: 496.1597144305706
time_elpased: 2.664
batch start
#iterations: 178
currently lose_sum: 496.3823896050453
time_elpased: 2.652
batch start
#iterations: 179
currently lose_sum: 497.3229047656059
time_elpased: 2.664
start validation test
0.564020618557
0.928480204342
0.148367346939
0.255850783037
0
validation finish
batch start
#iterations: 180
currently lose_sum: 496.5727472603321
time_elpased: 2.649
batch start
#iterations: 181
currently lose_sum: 497.5392074882984
time_elpased: 2.632
batch start
#iterations: 182
currently lose_sum: 496.6336604356766
time_elpased: 2.636
batch start
#iterations: 183
currently lose_sum: 496.92254304885864
time_elpased: 2.653
batch start
#iterations: 184
currently lose_sum: 495.63237419724464
time_elpased: 2.622
batch start
#iterations: 185
currently lose_sum: 496.57729160785675
time_elpased: 2.632
batch start
#iterations: 186
currently lose_sum: 497.08274698257446
time_elpased: 2.647
batch start
#iterations: 187
currently lose_sum: 494.63080886006355
time_elpased: 2.639
batch start
#iterations: 188
currently lose_sum: 496.29043793678284
time_elpased: 2.665
batch start
#iterations: 189
currently lose_sum: 497.8285456895828
time_elpased: 2.68
batch start
#iterations: 190
currently lose_sum: 495.94179579615593
time_elpased: 2.665
batch start
#iterations: 191
currently lose_sum: 497.868240326643
time_elpased: 2.664
batch start
#iterations: 192
currently lose_sum: 495.69463992118835
time_elpased: 2.665
batch start
#iterations: 193
currently lose_sum: 496.0478330552578
time_elpased: 2.637
batch start
#iterations: 194
currently lose_sum: 497.9941785633564
time_elpased: 2.66
batch start
#iterations: 195
currently lose_sum: 495.8120633661747
time_elpased: 2.67
batch start
#iterations: 196
currently lose_sum: 494.0686646103859
time_elpased: 2.66
batch start
#iterations: 197
currently lose_sum: 497.01942589879036
time_elpased: 2.666
batch start
#iterations: 198
currently lose_sum: 495.2895849645138
time_elpased: 2.661
batch start
#iterations: 199
currently lose_sum: 494.0647100806236
time_elpased: 2.664
start validation test
0.621443298969
0.903681788297
0.280510204082
0.428126460053
0
validation finish
batch start
#iterations: 200
currently lose_sum: 495.8490270972252
time_elpased: 2.658
batch start
#iterations: 201
currently lose_sum: 495.0660557448864
time_elpased: 2.662
batch start
#iterations: 202
currently lose_sum: 495.15134608745575
time_elpased: 2.656
batch start
#iterations: 203
currently lose_sum: 495.4149376451969
time_elpased: 2.65
batch start
#iterations: 204
currently lose_sum: 494.2355328500271
time_elpased: 2.649
batch start
#iterations: 205
currently lose_sum: 495.7202634513378
time_elpased: 2.645
batch start
#iterations: 206
currently lose_sum: 498.6613139808178
time_elpased: 2.648
batch start
#iterations: 207
currently lose_sum: 496.7936941385269
time_elpased: 2.663
batch start
#iterations: 208
currently lose_sum: 495.19884756207466
time_elpased: 2.664
batch start
#iterations: 209
currently lose_sum: 494.837625592947
time_elpased: 2.666
batch start
#iterations: 210
currently lose_sum: 495.1645997464657
time_elpased: 2.664
batch start
#iterations: 211
currently lose_sum: 496.758910626173
time_elpased: 2.664
batch start
#iterations: 212
currently lose_sum: 495.42500108480453
time_elpased: 2.668
batch start
#iterations: 213
currently lose_sum: 495.57903477549553
time_elpased: 2.672
batch start
#iterations: 214
currently lose_sum: 496.1424388885498
time_elpased: 2.666
batch start
#iterations: 215
currently lose_sum: 495.9858882725239
time_elpased: 2.64
batch start
#iterations: 216
currently lose_sum: 495.9371593296528
time_elpased: 2.647
batch start
#iterations: 217
currently lose_sum: 495.2517537176609
time_elpased: 2.638
batch start
#iterations: 218
currently lose_sum: 496.126650840044
time_elpased: 2.65
batch start
#iterations: 219
currently lose_sum: 496.2819017469883
time_elpased: 2.661
start validation test
0.522216494845
0.892171344165
0.0616326530612
0.11530018135
0
validation finish
batch start
#iterations: 220
currently lose_sum: 494.4864846765995
time_elpased: 2.671
batch start
#iterations: 221
currently lose_sum: 498.0944087803364
time_elpased: 2.655
batch start
#iterations: 222
currently lose_sum: 494.3189252614975
time_elpased: 2.677
batch start
#iterations: 223
currently lose_sum: 494.2522004842758
time_elpased: 2.657
batch start
#iterations: 224
currently lose_sum: 497.9435710608959
time_elpased: 2.66
batch start
#iterations: 225
currently lose_sum: 496.4810036420822
time_elpased: 2.675
batch start
#iterations: 226
currently lose_sum: 496.8104397058487
time_elpased: 2.68
batch start
#iterations: 227
currently lose_sum: 495.90059545636177
time_elpased: 2.659
batch start
#iterations: 228
currently lose_sum: 493.01532235741615
time_elpased: 2.68
batch start
#iterations: 229
currently lose_sum: 495.7094413936138
time_elpased: 2.666
batch start
#iterations: 230
currently lose_sum: 495.80502516031265
time_elpased: 2.659
batch start
#iterations: 231
currently lose_sum: 497.210576236248
time_elpased: 2.673
batch start
#iterations: 232
currently lose_sum: 491.6747579276562
time_elpased: 2.659
batch start
#iterations: 233
currently lose_sum: 494.4156314432621
time_elpased: 2.656
batch start
#iterations: 234
currently lose_sum: 495.48085936903954
time_elpased: 2.68
batch start
#iterations: 235
currently lose_sum: 495.89722061157227
time_elpased: 2.651
batch start
#iterations: 236
currently lose_sum: 494.91820707917213
time_elpased: 2.654
batch start
#iterations: 237
currently lose_sum: 495.9642625451088
time_elpased: 2.668
batch start
#iterations: 238
currently lose_sum: 493.5314477980137
time_elpased: 2.661
batch start
#iterations: 239
currently lose_sum: 493.44967970252037
time_elpased: 2.647
start validation test
0.515515463918
0.913402061856
0.0452040816327
0.0861448711716
0
validation finish
batch start
#iterations: 240
currently lose_sum: 492.5785935819149
time_elpased: 2.658
batch start
#iterations: 241
currently lose_sum: 494.19798135757446
time_elpased: 2.663
batch start
#iterations: 242
currently lose_sum: 492.07676124572754
time_elpased: 2.651
batch start
#iterations: 243
currently lose_sum: 493.61264780163765
time_elpased: 2.652
batch start
#iterations: 244
currently lose_sum: 494.9737031161785
time_elpased: 2.642
batch start
#iterations: 245
currently lose_sum: 495.0746940076351
time_elpased: 2.652
batch start
#iterations: 246
currently lose_sum: 495.4446467459202
time_elpased: 2.663
batch start
#iterations: 247
currently lose_sum: 495.70941376686096
time_elpased: 2.652
batch start
#iterations: 248
currently lose_sum: 494.4416761994362
time_elpased: 2.645
batch start
#iterations: 249
currently lose_sum: 497.26691830158234
time_elpased: 2.64
batch start
#iterations: 250
currently lose_sum: 493.3834041059017
time_elpased: 2.649
batch start
#iterations: 251
currently lose_sum: 494.382244348526
time_elpased: 2.639
batch start
#iterations: 252
currently lose_sum: 494.85741940140724
time_elpased: 2.671
batch start
#iterations: 253
currently lose_sum: 492.7715353369713
time_elpased: 2.665
batch start
#iterations: 254
currently lose_sum: 496.38315948843956
time_elpased: 2.652
batch start
#iterations: 255
currently lose_sum: 494.84848684072495
time_elpased: 2.657
batch start
#iterations: 256
currently lose_sum: 493.89199736714363
time_elpased: 2.67
batch start
#iterations: 257
currently lose_sum: 493.78541177511215
time_elpased: 2.65
batch start
#iterations: 258
currently lose_sum: 494.21074268221855
time_elpased: 2.683
batch start
#iterations: 259
currently lose_sum: 496.8448457121849
time_elpased: 2.664
start validation test
0.603659793814
0.899962106859
0.242346938776
0.38186349385
0
validation finish
batch start
#iterations: 260
currently lose_sum: 495.1969168484211
time_elpased: 2.652
batch start
#iterations: 261
currently lose_sum: 496.0899470448494
time_elpased: 2.642
batch start
#iterations: 262
currently lose_sum: 495.4297527074814
time_elpased: 2.654
batch start
#iterations: 263
currently lose_sum: 494.6486471295357
time_elpased: 2.658
batch start
#iterations: 264
currently lose_sum: 493.2768839895725
time_elpased: 2.669
batch start
#iterations: 265
currently lose_sum: 494.0376156270504
time_elpased: 2.67
batch start
#iterations: 266
currently lose_sum: 495.12171572446823
time_elpased: 2.67
batch start
#iterations: 267
currently lose_sum: 494.43415066599846
time_elpased: 2.664
batch start
#iterations: 268
currently lose_sum: 495.2491713166237
time_elpased: 2.673
batch start
#iterations: 269
currently lose_sum: 495.9543690085411
time_elpased: 2.658
batch start
#iterations: 270
currently lose_sum: 494.56495705246925
time_elpased: 2.66
batch start
#iterations: 271
currently lose_sum: 489.42316594719887
time_elpased: 2.666
batch start
#iterations: 272
currently lose_sum: 495.38506430387497
time_elpased: 2.655
batch start
#iterations: 273
currently lose_sum: 493.2018353044987
time_elpased: 2.666
batch start
#iterations: 274
currently lose_sum: 494.87208127975464
time_elpased: 2.674
batch start
#iterations: 275
currently lose_sum: 495.3420278429985
time_elpased: 2.664
batch start
#iterations: 276
currently lose_sum: 492.92054709792137
time_elpased: 2.674
batch start
#iterations: 277
currently lose_sum: 496.184302508831
time_elpased: 2.662
batch start
#iterations: 278
currently lose_sum: 495.46030071377754
time_elpased: 2.666
batch start
#iterations: 279
currently lose_sum: 494.2983402609825
time_elpased: 2.67
start validation test
0.547164948454
0.890685142417
0.11806122449
0.208487251104
0
validation finish
batch start
#iterations: 280
currently lose_sum: 492.9079114496708
time_elpased: 2.668
batch start
#iterations: 281
currently lose_sum: 494.09733045101166
time_elpased: 2.664
batch start
#iterations: 282
currently lose_sum: 495.0876146554947
time_elpased: 2.656
batch start
#iterations: 283
currently lose_sum: 494.03921300172806
time_elpased: 2.662
batch start
#iterations: 284
currently lose_sum: 495.24852579832077
time_elpased: 2.66
batch start
#iterations: 285
currently lose_sum: 494.0868321657181
time_elpased: 2.648
batch start
#iterations: 286
currently lose_sum: 494.6818647682667
time_elpased: 2.661
batch start
#iterations: 287
currently lose_sum: 493.95120853185654
time_elpased: 2.668
batch start
#iterations: 288
currently lose_sum: 495.25046095252037
time_elpased: 2.663
batch start
#iterations: 289
currently lose_sum: 494.0849044919014
time_elpased: 2.675
batch start
#iterations: 290
currently lose_sum: 492.87375742197037
time_elpased: 2.666
batch start
#iterations: 291
currently lose_sum: 494.4740352332592
time_elpased: 2.669
batch start
#iterations: 292
currently lose_sum: 493.8507151603699
time_elpased: 2.664
batch start
#iterations: 293
currently lose_sum: 493.1331669986248
time_elpased: 2.675
batch start
#iterations: 294
currently lose_sum: 494.50448605418205
time_elpased: 2.658
batch start
#iterations: 295
currently lose_sum: 494.3543618917465
time_elpased: 2.67
batch start
#iterations: 296
currently lose_sum: 491.0884653031826
time_elpased: 2.669
batch start
#iterations: 297
currently lose_sum: 494.4048393368721
time_elpased: 2.677
batch start
#iterations: 298
currently lose_sum: 493.2009573876858
time_elpased: 2.656
batch start
#iterations: 299
currently lose_sum: 492.5458181798458
time_elpased: 2.655
start validation test
0.674226804124
0.881578947368
0.410204081633
0.559888579387
0
validation finish
batch start
#iterations: 300
currently lose_sum: 491.630807697773
time_elpased: 2.669
batch start
#iterations: 301
currently lose_sum: 493.5646924674511
time_elpased: 2.674
batch start
#iterations: 302
currently lose_sum: 494.13329085707664
time_elpased: 2.667
batch start
#iterations: 303
currently lose_sum: 493.5673081576824
time_elpased: 2.655
batch start
#iterations: 304
currently lose_sum: 494.55457854270935
time_elpased: 2.673
batch start
#iterations: 305
currently lose_sum: 495.14706096053123
time_elpased: 2.675
batch start
#iterations: 306
currently lose_sum: 493.3199402689934
time_elpased: 2.662
batch start
#iterations: 307
currently lose_sum: 496.7865594625473
time_elpased: 2.664
batch start
#iterations: 308
currently lose_sum: 493.40864580869675
time_elpased: 2.661
batch start
#iterations: 309
currently lose_sum: 495.4716389477253
time_elpased: 2.663
batch start
#iterations: 310
currently lose_sum: 492.3163192868233
time_elpased: 2.676
batch start
#iterations: 311
currently lose_sum: 492.3592399060726
time_elpased: 2.666
batch start
#iterations: 312
currently lose_sum: 492.90657368302345
time_elpased: 2.666
batch start
#iterations: 313
currently lose_sum: 494.15360048413277
time_elpased: 2.682
batch start
#iterations: 314
currently lose_sum: 492.5471484065056
time_elpased: 2.671
batch start
#iterations: 315
currently lose_sum: 493.7270732522011
time_elpased: 2.669
batch start
#iterations: 316
currently lose_sum: 494.4109995961189
time_elpased: 2.676
batch start
#iterations: 317
currently lose_sum: 492.819589227438
time_elpased: 2.674
batch start
#iterations: 318
currently lose_sum: 496.07598373293877
time_elpased: 2.666
batch start
#iterations: 319
currently lose_sum: 493.98325204849243
time_elpased: 2.683
start validation test
0.515154639175
0.905349794239
0.0448979591837
0.0855531790784
0
validation finish
batch start
#iterations: 320
currently lose_sum: 493.6716487109661
time_elpased: 2.673
batch start
#iterations: 321
currently lose_sum: 492.23001947999
time_elpased: 2.662
batch start
#iterations: 322
currently lose_sum: 491.5192679464817
time_elpased: 2.674
batch start
#iterations: 323
currently lose_sum: 493.2245953679085
time_elpased: 2.667
batch start
#iterations: 324
currently lose_sum: 495.6216844320297
time_elpased: 2.658
batch start
#iterations: 325
currently lose_sum: 494.40851429104805
time_elpased: 2.667
batch start
#iterations: 326
currently lose_sum: 494.9184390902519
time_elpased: 2.68
batch start
#iterations: 327
currently lose_sum: 492.91455909609795
time_elpased: 2.667
batch start
#iterations: 328
currently lose_sum: 492.3405720293522
time_elpased: 2.668
batch start
#iterations: 329
currently lose_sum: 493.6863213479519
time_elpased: 2.665
batch start
#iterations: 330
currently lose_sum: 494.6806944310665
time_elpased: 2.673
batch start
#iterations: 331
currently lose_sum: 495.14979469776154
time_elpased: 2.673
batch start
#iterations: 332
currently lose_sum: 493.0274068713188
time_elpased: 2.674
batch start
#iterations: 333
currently lose_sum: 494.88327702879906
time_elpased: 2.65
batch start
#iterations: 334
currently lose_sum: 493.6649297773838
time_elpased: 2.656
batch start
#iterations: 335
currently lose_sum: 490.90641099214554
time_elpased: 2.662
batch start
#iterations: 336
currently lose_sum: 494.47552770376205
time_elpased: 2.675
batch start
#iterations: 337
currently lose_sum: 493.4670485854149
time_elpased: 2.67
batch start
#iterations: 338
currently lose_sum: 494.8516219854355
time_elpased: 2.661
batch start
#iterations: 339
currently lose_sum: 491.9318582415581
time_elpased: 2.67
start validation test
0.59087628866
0.898247114151
0.214387755102
0.346157014581
0
validation finish
batch start
#iterations: 340
currently lose_sum: 494.05952003598213
time_elpased: 2.664
batch start
#iterations: 341
currently lose_sum: 494.29723384976387
time_elpased: 2.663
batch start
#iterations: 342
currently lose_sum: 492.5207654237747
time_elpased: 2.674
batch start
#iterations: 343
currently lose_sum: 494.33376157283783
time_elpased: 2.659
batch start
#iterations: 344
currently lose_sum: 494.48718962073326
time_elpased: 2.662
batch start
#iterations: 345
currently lose_sum: 494.48208770155907
time_elpased: 2.656
batch start
#iterations: 346
currently lose_sum: 492.65559485554695
time_elpased: 2.669
batch start
#iterations: 347
currently lose_sum: 493.011990070343
time_elpased: 2.679
batch start
#iterations: 348
currently lose_sum: 494.07502833008766
time_elpased: 2.661
batch start
#iterations: 349
currently lose_sum: 494.1470837891102
time_elpased: 2.678
batch start
#iterations: 350
currently lose_sum: 494.01976120471954
time_elpased: 2.676
batch start
#iterations: 351
currently lose_sum: 492.38590583205223
time_elpased: 2.681
batch start
#iterations: 352
currently lose_sum: 492.82316970825195
time_elpased: 2.687
batch start
#iterations: 353
currently lose_sum: 495.3629648387432
time_elpased: 2.688
batch start
#iterations: 354
currently lose_sum: 492.64733213186264
time_elpased: 2.676
batch start
#iterations: 355
currently lose_sum: 494.21885320544243
time_elpased: 2.681
batch start
#iterations: 356
currently lose_sum: 494.52208107709885
time_elpased: 2.678
batch start
#iterations: 357
currently lose_sum: 493.5971937775612
time_elpased: 2.687
batch start
#iterations: 358
currently lose_sum: 493.56555134058
time_elpased: 2.696
batch start
#iterations: 359
currently lose_sum: 493.19858890771866
time_elpased: 2.695
start validation test
0.605309278351
0.89817911557
0.246632653061
0.38699863902
0
validation finish
batch start
#iterations: 360
currently lose_sum: 493.5654830634594
time_elpased: 2.678
batch start
#iterations: 361
currently lose_sum: 492.76845291256905
time_elpased: 2.664
batch start
#iterations: 362
currently lose_sum: 493.6189447045326
time_elpased: 2.683
batch start
#iterations: 363
currently lose_sum: 492.70823657512665
time_elpased: 2.68
batch start
#iterations: 364
currently lose_sum: 492.4618397951126
time_elpased: 2.673
batch start
#iterations: 365
currently lose_sum: 493.71636509895325
time_elpased: 2.684
batch start
#iterations: 366
currently lose_sum: 494.3130633533001
time_elpased: 2.666
batch start
#iterations: 367
currently lose_sum: 494.9697153568268
time_elpased: 2.665
batch start
#iterations: 368
currently lose_sum: 491.3841253221035
time_elpased: 2.672
batch start
#iterations: 369
currently lose_sum: 491.5424646139145
time_elpased: 2.663
batch start
#iterations: 370
currently lose_sum: 491.360698312521
time_elpased: 2.665
batch start
#iterations: 371
currently lose_sum: 494.4057146906853
time_elpased: 2.67
batch start
#iterations: 372
currently lose_sum: 493.1335481405258
time_elpased: 2.683
batch start
#iterations: 373
currently lose_sum: 492.44554406404495
time_elpased: 2.676
batch start
#iterations: 374
currently lose_sum: 494.1257186830044
time_elpased: 2.679
batch start
#iterations: 375
currently lose_sum: 492.48803609609604
time_elpased: 2.68
batch start
#iterations: 376
currently lose_sum: 494.05582270026207
time_elpased: 2.664
batch start
#iterations: 377
currently lose_sum: 494.94193321466446
time_elpased: 2.692
batch start
#iterations: 378
currently lose_sum: 493.9986818432808
time_elpased: 2.691
batch start
#iterations: 379
currently lose_sum: 493.9413451850414
time_elpased: 2.675
start validation test
0.622886597938
0.891551071879
0.288571428571
0.436016034536
0
validation finish
batch start
#iterations: 380
currently lose_sum: 492.464822858572
time_elpased: 2.679
batch start
#iterations: 381
currently lose_sum: 491.56982776522636
time_elpased: 2.683
batch start
#iterations: 382
currently lose_sum: 493.20179760456085
time_elpased: 2.674
batch start
#iterations: 383
currently lose_sum: 491.056057035923
time_elpased: 2.677
batch start
#iterations: 384
currently lose_sum: 492.4050377011299
time_elpased: 2.679
batch start
#iterations: 385
currently lose_sum: 490.23931020498276
time_elpased: 2.68
batch start
#iterations: 386
currently lose_sum: 492.74468713998795
time_elpased: 2.674
batch start
#iterations: 387
currently lose_sum: 494.6187562942505
time_elpased: 2.686
batch start
#iterations: 388
currently lose_sum: 494.37895768880844
time_elpased: 2.699
batch start
#iterations: 389
currently lose_sum: 490.52052316069603
time_elpased: 2.697
batch start
#iterations: 390
currently lose_sum: 491.308938652277
time_elpased: 2.679
batch start
#iterations: 391
currently lose_sum: 492.7222137749195
time_elpased: 2.689
batch start
#iterations: 392
currently lose_sum: 492.6467568576336
time_elpased: 2.676
batch start
#iterations: 393
currently lose_sum: 492.38424640893936
time_elpased: 2.666
batch start
#iterations: 394
currently lose_sum: 492.8058389723301
time_elpased: 2.688
batch start
#iterations: 395
currently lose_sum: 491.7888605296612
time_elpased: 2.666
batch start
#iterations: 396
currently lose_sum: 492.89003598690033
time_elpased: 2.673
batch start
#iterations: 397
currently lose_sum: 493.79905584454536
time_elpased: 2.659
batch start
#iterations: 398
currently lose_sum: 492.82846784591675
time_elpased: 2.694
batch start
#iterations: 399
currently lose_sum: 493.28933092951775
time_elpased: 2.694
start validation test
0.555360824742
0.886693017128
0.137346938776
0.237851210461
0
validation finish
acc: 0.748
pre: 0.870
rec: 0.576
F1: 0.693
auc: 0.000
