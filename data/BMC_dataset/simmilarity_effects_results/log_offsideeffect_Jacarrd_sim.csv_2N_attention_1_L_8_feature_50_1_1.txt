start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 551.5824817419052
time_elpased: 1.622
batch start
#iterations: 1
currently lose_sum: 542.5116587281227
time_elpased: 1.601
batch start
#iterations: 2
currently lose_sum: 538.8535285592079
time_elpased: 1.597
batch start
#iterations: 3
currently lose_sum: 533.4419504404068
time_elpased: 1.589
batch start
#iterations: 4
currently lose_sum: 531.2077331840992
time_elpased: 1.595
batch start
#iterations: 5
currently lose_sum: 528.30901029706
time_elpased: 1.592
batch start
#iterations: 6
currently lose_sum: 524.2689497768879
time_elpased: 1.595
batch start
#iterations: 7
currently lose_sum: 524.0346357524395
time_elpased: 1.595
batch start
#iterations: 8
currently lose_sum: 522.5686963200569
time_elpased: 1.586
batch start
#iterations: 9
currently lose_sum: 521.6972341239452
time_elpased: 1.595
batch start
#iterations: 10
currently lose_sum: 520.1432306468487
time_elpased: 1.588
batch start
#iterations: 11
currently lose_sum: 521.8711428940296
time_elpased: 1.592
batch start
#iterations: 12
currently lose_sum: 517.5374195873737
time_elpased: 1.595
batch start
#iterations: 13
currently lose_sum: 517.3509250283241
time_elpased: 1.593
batch start
#iterations: 14
currently lose_sum: 516.1021393835545
time_elpased: 1.587
batch start
#iterations: 15
currently lose_sum: 514.6645405888557
time_elpased: 1.587
batch start
#iterations: 16
currently lose_sum: 517.2841174900532
time_elpased: 1.596
batch start
#iterations: 17
currently lose_sum: 514.201130926609
time_elpased: 1.598
batch start
#iterations: 18
currently lose_sum: 513.9552536904812
time_elpased: 1.595
batch start
#iterations: 19
currently lose_sum: 511.9464839696884
time_elpased: 1.595
start validation test
0.551237113402
0.924031007752
0.121632653061
0.214968440036
0
validation finish
batch start
#iterations: 20
currently lose_sum: 513.2372899651527
time_elpased: 1.59
batch start
#iterations: 21
currently lose_sum: 514.6267183423042
time_elpased: 1.581
batch start
#iterations: 22
currently lose_sum: 512.7789397835732
time_elpased: 1.589
batch start
#iterations: 23
currently lose_sum: 511.5789532959461
time_elpased: 1.587
batch start
#iterations: 24
currently lose_sum: 510.06043472886086
time_elpased: 1.582
batch start
#iterations: 25
currently lose_sum: 511.76556745171547
time_elpased: 1.587
batch start
#iterations: 26
currently lose_sum: 511.93403708934784
time_elpased: 1.599
batch start
#iterations: 27
currently lose_sum: 511.67587250471115
time_elpased: 1.606
batch start
#iterations: 28
currently lose_sum: 510.61951816082
time_elpased: 1.593
batch start
#iterations: 29
currently lose_sum: 511.07718002796173
time_elpased: 1.599
batch start
#iterations: 30
currently lose_sum: 509.1720805466175
time_elpased: 1.601
batch start
#iterations: 31
currently lose_sum: 510.12637865543365
time_elpased: 1.579
batch start
#iterations: 32
currently lose_sum: 510.511209666729
time_elpased: 1.597
batch start
#iterations: 33
currently lose_sum: 507.7519491314888
time_elpased: 1.584
batch start
#iterations: 34
currently lose_sum: 509.066010504961
time_elpased: 1.599
batch start
#iterations: 35
currently lose_sum: 506.5000963807106
time_elpased: 1.593
batch start
#iterations: 36
currently lose_sum: 507.9494615793228
time_elpased: 1.585
batch start
#iterations: 37
currently lose_sum: 507.2247182428837
time_elpased: 1.597
batch start
#iterations: 38
currently lose_sum: 507.7779538333416
time_elpased: 1.592
batch start
#iterations: 39
currently lose_sum: 508.4768488705158
time_elpased: 1.593
start validation test
0.588556701031
0.907989228007
0.206428571429
0.336381775856
0
validation finish
batch start
#iterations: 40
currently lose_sum: 507.2331457436085
time_elpased: 1.602
batch start
#iterations: 41
currently lose_sum: 506.500290453434
time_elpased: 1.599
batch start
#iterations: 42
currently lose_sum: 505.2681505680084
time_elpased: 1.605
batch start
#iterations: 43
currently lose_sum: 506.47263941168785
time_elpased: 1.579
batch start
#iterations: 44
currently lose_sum: 504.8585036993027
time_elpased: 1.595
batch start
#iterations: 45
currently lose_sum: 505.73215010762215
time_elpased: 1.59
batch start
#iterations: 46
currently lose_sum: 504.4333481788635
time_elpased: 1.594
batch start
#iterations: 47
currently lose_sum: 506.9604191482067
time_elpased: 1.6
batch start
#iterations: 48
currently lose_sum: 503.0833938419819
time_elpased: 1.59
batch start
#iterations: 49
currently lose_sum: 504.04083919525146
time_elpased: 1.598
batch start
#iterations: 50
currently lose_sum: 506.5744158923626
time_elpased: 1.592
batch start
#iterations: 51
currently lose_sum: 504.8772949874401
time_elpased: 1.579
batch start
#iterations: 52
currently lose_sum: 504.4116213321686
time_elpased: 1.598
batch start
#iterations: 53
currently lose_sum: 504.96613135933876
time_elpased: 1.597
batch start
#iterations: 54
currently lose_sum: 502.85563960671425
time_elpased: 1.596
batch start
#iterations: 55
currently lose_sum: 504.6895585656166
time_elpased: 1.587
batch start
#iterations: 56
currently lose_sum: 505.0411954522133
time_elpased: 1.599
batch start
#iterations: 57
currently lose_sum: 503.82870349287987
time_elpased: 1.604
batch start
#iterations: 58
currently lose_sum: 502.9777714908123
time_elpased: 1.592
batch start
#iterations: 59
currently lose_sum: 501.6955750286579
time_elpased: 1.602
start validation test
0.608041237113
0.925251742835
0.243775510204
0.385882733
0
validation finish
batch start
#iterations: 60
currently lose_sum: 504.2994893491268
time_elpased: 1.603
batch start
#iterations: 61
currently lose_sum: 502.75972828269005
time_elpased: 1.591
batch start
#iterations: 62
currently lose_sum: 505.8387984633446
time_elpased: 1.598
batch start
#iterations: 63
currently lose_sum: 501.83257830142975
time_elpased: 1.603
batch start
#iterations: 64
currently lose_sum: 503.28041684627533
time_elpased: 1.604
batch start
#iterations: 65
currently lose_sum: 503.19049084186554
time_elpased: 1.593
batch start
#iterations: 66
currently lose_sum: 501.5886636376381
time_elpased: 1.582
batch start
#iterations: 67
currently lose_sum: 502.0700439810753
time_elpased: 1.613
batch start
#iterations: 68
currently lose_sum: 504.22924104332924
time_elpased: 1.581
batch start
#iterations: 69
currently lose_sum: 502.32325011491776
time_elpased: 1.592
batch start
#iterations: 70
currently lose_sum: 502.44466269016266
time_elpased: 1.604
batch start
#iterations: 71
currently lose_sum: 502.2934276461601
time_elpased: 1.587
batch start
#iterations: 72
currently lose_sum: 503.1878064572811
time_elpased: 1.613
batch start
#iterations: 73
currently lose_sum: 501.7460581958294
time_elpased: 1.594
batch start
#iterations: 74
currently lose_sum: 501.6608962416649
time_elpased: 1.594
batch start
#iterations: 75
currently lose_sum: 501.74552378058434
time_elpased: 1.59
batch start
#iterations: 76
currently lose_sum: 502.3231720328331
time_elpased: 1.588
batch start
#iterations: 77
currently lose_sum: 501.1870132088661
time_elpased: 1.604
batch start
#iterations: 78
currently lose_sum: 500.6114020347595
time_elpased: 1.584
batch start
#iterations: 79
currently lose_sum: 502.4536801278591
time_elpased: 1.617
start validation test
0.591804123711
0.914865460962
0.211632653061
0.343747410293
0
validation finish
batch start
#iterations: 80
currently lose_sum: 500.91042724251747
time_elpased: 1.612
batch start
#iterations: 81
currently lose_sum: 501.1803493499756
time_elpased: 1.59
batch start
#iterations: 82
currently lose_sum: 501.204347461462
time_elpased: 1.605
batch start
#iterations: 83
currently lose_sum: 501.3776734471321
time_elpased: 1.585
batch start
#iterations: 84
currently lose_sum: 501.16911736130714
time_elpased: 1.577
batch start
#iterations: 85
currently lose_sum: 501.24351865053177
time_elpased: 1.602
batch start
#iterations: 86
currently lose_sum: 501.95914670825005
time_elpased: 1.589
batch start
#iterations: 87
currently lose_sum: 498.94513234496117
time_elpased: 1.597
batch start
#iterations: 88
currently lose_sum: 499.17598804831505
time_elpased: 1.59
batch start
#iterations: 89
currently lose_sum: 503.28552252054214
time_elpased: 1.599
batch start
#iterations: 90
currently lose_sum: 499.9711785912514
time_elpased: 1.595
batch start
#iterations: 91
currently lose_sum: 497.4782364368439
time_elpased: 1.596
batch start
#iterations: 92
currently lose_sum: 501.25992196798325
time_elpased: 1.594
batch start
#iterations: 93
currently lose_sum: 500.3970208466053
time_elpased: 1.603
batch start
#iterations: 94
currently lose_sum: 498.3459569811821
time_elpased: 1.581
batch start
#iterations: 95
currently lose_sum: 499.1895583868027
time_elpased: 1.587
batch start
#iterations: 96
currently lose_sum: 500.9924021959305
time_elpased: 1.586
batch start
#iterations: 97
currently lose_sum: 499.2037961781025
time_elpased: 1.59
batch start
#iterations: 98
currently lose_sum: 500.1121964752674
time_elpased: 1.601
batch start
#iterations: 99
currently lose_sum: 499.8494547009468
time_elpased: 1.584
start validation test
0.639690721649
0.913722025913
0.316632653061
0.470294028493
0
validation finish
batch start
#iterations: 100
currently lose_sum: 500.17075249552727
time_elpased: 1.589
batch start
#iterations: 101
currently lose_sum: 500.11556750535965
time_elpased: 1.588
batch start
#iterations: 102
currently lose_sum: 497.59508004784584
time_elpased: 1.597
batch start
#iterations: 103
currently lose_sum: 498.4226252734661
time_elpased: 1.593
batch start
#iterations: 104
currently lose_sum: 500.6649665236473
time_elpased: 1.588
batch start
#iterations: 105
currently lose_sum: 499.09640473127365
time_elpased: 1.597
batch start
#iterations: 106
currently lose_sum: 501.5505627989769
time_elpased: 1.603
batch start
#iterations: 107
currently lose_sum: 500.7132880985737
time_elpased: 1.595
batch start
#iterations: 108
currently lose_sum: 498.773750603199
time_elpased: 1.596
batch start
#iterations: 109
currently lose_sum: 499.1075060069561
time_elpased: 1.595
batch start
#iterations: 110
currently lose_sum: 499.13973304629326
time_elpased: 1.6
batch start
#iterations: 111
currently lose_sum: 502.738521784544
time_elpased: 1.606
batch start
#iterations: 112
currently lose_sum: 498.4723670184612
time_elpased: 1.597
batch start
#iterations: 113
currently lose_sum: 498.87275487184525
time_elpased: 1.604
batch start
#iterations: 114
currently lose_sum: 498.63859862089157
time_elpased: 1.61
batch start
#iterations: 115
currently lose_sum: 500.04081508517265
time_elpased: 1.598
batch start
#iterations: 116
currently lose_sum: 499.2047848701477
time_elpased: 1.592
batch start
#iterations: 117
currently lose_sum: 497.6789480149746
time_elpased: 1.584
batch start
#iterations: 118
currently lose_sum: 499.3727077245712
time_elpased: 1.588
batch start
#iterations: 119
currently lose_sum: 498.84593391418457
time_elpased: 1.598
start validation test
0.735309278351
0.865080607294
0.563979591837
0.682809314967
0
validation finish
batch start
#iterations: 120
currently lose_sum: 497.67239305377007
time_elpased: 1.605
batch start
#iterations: 121
currently lose_sum: 498.07440957427025
time_elpased: 1.596
batch start
#iterations: 122
currently lose_sum: 498.86161598563194
time_elpased: 1.599
batch start
#iterations: 123
currently lose_sum: 499.0103991627693
time_elpased: 1.591
batch start
#iterations: 124
currently lose_sum: 499.0242264866829
time_elpased: 1.606
batch start
#iterations: 125
currently lose_sum: 500.5919536650181
time_elpased: 1.586
batch start
#iterations: 126
currently lose_sum: 497.3688097894192
time_elpased: 1.592
batch start
#iterations: 127
currently lose_sum: 500.83586725592613
time_elpased: 1.585
batch start
#iterations: 128
currently lose_sum: 499.03518438339233
time_elpased: 1.581
batch start
#iterations: 129
currently lose_sum: 498.66039049625397
time_elpased: 1.608
batch start
#iterations: 130
currently lose_sum: 498.5981528162956
time_elpased: 1.586
batch start
#iterations: 131
currently lose_sum: 498.16936123371124
time_elpased: 1.586
batch start
#iterations: 132
currently lose_sum: 497.9074105620384
time_elpased: 1.6
batch start
#iterations: 133
currently lose_sum: 501.20196706056595
time_elpased: 1.598
batch start
#iterations: 134
currently lose_sum: 498.2962012588978
time_elpased: 1.589
batch start
#iterations: 135
currently lose_sum: 495.5175731778145
time_elpased: 1.596
batch start
#iterations: 136
currently lose_sum: 497.08780309557915
time_elpased: 1.589
batch start
#iterations: 137
currently lose_sum: 496.91186368465424
time_elpased: 1.594
batch start
#iterations: 138
currently lose_sum: 498.9151592850685
time_elpased: 1.592
batch start
#iterations: 139
currently lose_sum: 497.9226031601429
time_elpased: 1.594
start validation test
0.542886597938
0.910211267606
0.105510204082
0.189100219459
0
validation finish
batch start
#iterations: 140
currently lose_sum: 496.48890617489815
time_elpased: 1.594
batch start
#iterations: 141
currently lose_sum: 495.60825076699257
time_elpased: 1.607
batch start
#iterations: 142
currently lose_sum: 497.4092693030834
time_elpased: 1.593
batch start
#iterations: 143
currently lose_sum: 497.42304450273514
time_elpased: 1.586
batch start
#iterations: 144
currently lose_sum: 499.0664576292038
time_elpased: 1.602
batch start
#iterations: 145
currently lose_sum: 496.18484434485435
time_elpased: 1.587
batch start
#iterations: 146
currently lose_sum: 499.487021446228
time_elpased: 1.583
batch start
#iterations: 147
currently lose_sum: 498.78541484475136
time_elpased: 1.607
batch start
#iterations: 148
currently lose_sum: 496.1446135044098
time_elpased: 1.588
batch start
#iterations: 149
currently lose_sum: 499.35317808389664
time_elpased: 1.599
batch start
#iterations: 150
currently lose_sum: 495.62122762203217
time_elpased: 1.583
batch start
#iterations: 151
currently lose_sum: 494.6344176530838
time_elpased: 1.614
batch start
#iterations: 152
currently lose_sum: 498.59195667505264
time_elpased: 1.603
batch start
#iterations: 153
currently lose_sum: 496.0234440565109
time_elpased: 1.607
batch start
#iterations: 154
currently lose_sum: 500.36737394332886
time_elpased: 1.597
batch start
#iterations: 155
currently lose_sum: 496.44811630249023
time_elpased: 1.595
batch start
#iterations: 156
currently lose_sum: 497.73117020726204
time_elpased: 1.589
batch start
#iterations: 157
currently lose_sum: 497.56914469599724
time_elpased: 1.594
batch start
#iterations: 158
currently lose_sum: 495.70948830246925
time_elpased: 1.616
batch start
#iterations: 159
currently lose_sum: 498.12075451016426
time_elpased: 1.605
start validation test
0.556340206186
0.925160370634
0.132448979592
0.231723645452
0
validation finish
batch start
#iterations: 160
currently lose_sum: 498.84246215224266
time_elpased: 1.602
batch start
#iterations: 161
currently lose_sum: 497.7812604904175
time_elpased: 1.607
batch start
#iterations: 162
currently lose_sum: 495.51460486650467
time_elpased: 1.591
batch start
#iterations: 163
currently lose_sum: 495.177756100893
time_elpased: 1.602
batch start
#iterations: 164
currently lose_sum: 497.43487733602524
time_elpased: 1.609
batch start
#iterations: 165
currently lose_sum: 498.4341926276684
time_elpased: 1.597
batch start
#iterations: 166
currently lose_sum: 498.0241619348526
time_elpased: 1.609
batch start
#iterations: 167
currently lose_sum: 496.774966776371
time_elpased: 1.598
batch start
#iterations: 168
currently lose_sum: 497.42426910996437
time_elpased: 1.597
batch start
#iterations: 169
currently lose_sum: 496.96235209703445
time_elpased: 1.589
batch start
#iterations: 170
currently lose_sum: 497.72510600090027
time_elpased: 1.591
batch start
#iterations: 171
currently lose_sum: 495.9577848613262
time_elpased: 1.586
batch start
#iterations: 172
currently lose_sum: 500.06652027368546
time_elpased: 1.604
batch start
#iterations: 173
currently lose_sum: 497.93134996294975
time_elpased: 1.587
batch start
#iterations: 174
currently lose_sum: 497.6809586584568
time_elpased: 1.589
batch start
#iterations: 175
currently lose_sum: 495.79555210471153
time_elpased: 1.583
batch start
#iterations: 176
currently lose_sum: 496.7505079805851
time_elpased: 1.593
batch start
#iterations: 177
currently lose_sum: 496.2518781423569
time_elpased: 1.587
batch start
#iterations: 178
currently lose_sum: 496.5654206573963
time_elpased: 1.61
batch start
#iterations: 179
currently lose_sum: 497.37415704131126
time_elpased: 1.588
start validation test
0.573195876289
0.928893905192
0.167959183673
0.284479778776
0
validation finish
batch start
#iterations: 180
currently lose_sum: 496.7411261200905
time_elpased: 1.604
batch start
#iterations: 181
currently lose_sum: 497.7963309288025
time_elpased: 1.603
batch start
#iterations: 182
currently lose_sum: 496.76515024900436
time_elpased: 1.587
batch start
#iterations: 183
currently lose_sum: 496.9665372669697
time_elpased: 1.588
batch start
#iterations: 184
currently lose_sum: 495.91365027427673
time_elpased: 1.602
batch start
#iterations: 185
currently lose_sum: 496.5761124789715
time_elpased: 1.59
batch start
#iterations: 186
currently lose_sum: 497.215340256691
time_elpased: 1.594
batch start
#iterations: 187
currently lose_sum: 494.7089398801327
time_elpased: 1.601
batch start
#iterations: 188
currently lose_sum: 496.4717419743538
time_elpased: 1.581
batch start
#iterations: 189
currently lose_sum: 497.85568353533745
time_elpased: 1.593
batch start
#iterations: 190
currently lose_sum: 495.9107101857662
time_elpased: 1.591
batch start
#iterations: 191
currently lose_sum: 497.71197444200516
time_elpased: 1.599
batch start
#iterations: 192
currently lose_sum: 495.8736002147198
time_elpased: 1.601
batch start
#iterations: 193
currently lose_sum: 496.05979242920876
time_elpased: 1.595
batch start
#iterations: 194
currently lose_sum: 498.21274095773697
time_elpased: 1.596
batch start
#iterations: 195
currently lose_sum: 495.89190298318863
time_elpased: 1.607
batch start
#iterations: 196
currently lose_sum: 494.1111767888069
time_elpased: 1.604
batch start
#iterations: 197
currently lose_sum: 497.4047192335129
time_elpased: 1.602
batch start
#iterations: 198
currently lose_sum: 495.4538015127182
time_elpased: 1.591
batch start
#iterations: 199
currently lose_sum: 494.2125897407532
time_elpased: 1.595
start validation test
0.622164948454
0.923525377229
0.274795918367
0.423560868198
0
validation finish
batch start
#iterations: 200
currently lose_sum: 495.8431651890278
time_elpased: 1.596
batch start
#iterations: 201
currently lose_sum: 495.2661523222923
time_elpased: 1.589
batch start
#iterations: 202
currently lose_sum: 495.293810993433
time_elpased: 1.587
batch start
#iterations: 203
currently lose_sum: 495.47638100385666
time_elpased: 1.601
batch start
#iterations: 204
currently lose_sum: 494.590390175581
time_elpased: 1.611
batch start
#iterations: 205
currently lose_sum: 495.9005293250084
time_elpased: 1.591
batch start
#iterations: 206
currently lose_sum: 498.85427993535995
time_elpased: 1.594
batch start
#iterations: 207
currently lose_sum: 497.0208125412464
time_elpased: 1.606
batch start
#iterations: 208
currently lose_sum: 495.3206638991833
time_elpased: 1.6
batch start
#iterations: 209
currently lose_sum: 495.0886023938656
time_elpased: 1.61
batch start
#iterations: 210
currently lose_sum: 495.087903380394
time_elpased: 1.595
batch start
#iterations: 211
currently lose_sum: 496.82287895679474
time_elpased: 1.599
batch start
#iterations: 212
currently lose_sum: 495.589753061533
time_elpased: 1.604
batch start
#iterations: 213
currently lose_sum: 495.6051257252693
time_elpased: 1.597
batch start
#iterations: 214
currently lose_sum: 496.51745876669884
time_elpased: 1.614
batch start
#iterations: 215
currently lose_sum: 496.09272214770317
time_elpased: 1.601
batch start
#iterations: 216
currently lose_sum: 495.93739756941795
time_elpased: 1.59
batch start
#iterations: 217
currently lose_sum: 495.2809701561928
time_elpased: 1.591
batch start
#iterations: 218
currently lose_sum: 496.05611553788185
time_elpased: 1.598
batch start
#iterations: 219
currently lose_sum: 496.418900847435
time_elpased: 1.594
start validation test
0.526855670103
0.91677852349
0.069693877551
0.129540066382
0
validation finish
batch start
#iterations: 220
currently lose_sum: 494.84452161192894
time_elpased: 1.599
batch start
#iterations: 221
currently lose_sum: 498.3522111773491
time_elpased: 1.606
batch start
#iterations: 222
currently lose_sum: 494.64440327882767
time_elpased: 1.602
batch start
#iterations: 223
currently lose_sum: 494.41757503151894
time_elpased: 1.594
batch start
#iterations: 224
currently lose_sum: 498.05962029099464
time_elpased: 1.593
batch start
#iterations: 225
currently lose_sum: 496.5153104066849
time_elpased: 1.603
batch start
#iterations: 226
currently lose_sum: 496.71879559755325
time_elpased: 1.604
batch start
#iterations: 227
currently lose_sum: 496.02600678801537
time_elpased: 1.599
batch start
#iterations: 228
currently lose_sum: 493.0537306666374
time_elpased: 1.596
batch start
#iterations: 229
currently lose_sum: 495.81517082452774
time_elpased: 1.595
batch start
#iterations: 230
currently lose_sum: 496.08219450712204
time_elpased: 1.603
batch start
#iterations: 231
currently lose_sum: 497.0516611635685
time_elpased: 1.602
batch start
#iterations: 232
currently lose_sum: 491.85722425580025
time_elpased: 1.608
batch start
#iterations: 233
currently lose_sum: 494.54431477189064
time_elpased: 1.589
batch start
#iterations: 234
currently lose_sum: 495.78781896829605
time_elpased: 1.605
batch start
#iterations: 235
currently lose_sum: 496.2021561264992
time_elpased: 1.59
batch start
#iterations: 236
currently lose_sum: 495.265890866518
time_elpased: 1.61
batch start
#iterations: 237
currently lose_sum: 495.97464939951897
time_elpased: 1.591
batch start
#iterations: 238
currently lose_sum: 493.495417535305
time_elpased: 1.592
batch start
#iterations: 239
currently lose_sum: 493.31018087267876
time_elpased: 1.61
start validation test
0.516546391753
0.911937377691
0.0475510204082
0.0903889050529
0
validation finish
batch start
#iterations: 240
currently lose_sum: 492.5308705866337
time_elpased: 1.6
batch start
#iterations: 241
currently lose_sum: 494.18568417429924
time_elpased: 1.605
batch start
#iterations: 242
currently lose_sum: 492.41996052861214
time_elpased: 1.594
batch start
#iterations: 243
currently lose_sum: 493.72359931468964
time_elpased: 1.602
batch start
#iterations: 244
currently lose_sum: 494.97765612602234
time_elpased: 1.593
batch start
#iterations: 245
currently lose_sum: 495.3800508379936
time_elpased: 1.6
batch start
#iterations: 246
currently lose_sum: 495.52471393346786
time_elpased: 1.598
batch start
#iterations: 247
currently lose_sum: 495.84024745225906
time_elpased: 1.597
batch start
#iterations: 248
currently lose_sum: 494.43069353699684
time_elpased: 1.598
batch start
#iterations: 249
currently lose_sum: 497.4386242032051
time_elpased: 1.594
batch start
#iterations: 250
currently lose_sum: 493.67962965369225
time_elpased: 1.581
batch start
#iterations: 251
currently lose_sum: 494.41560912132263
time_elpased: 1.597
batch start
#iterations: 252
currently lose_sum: 495.32456746697426
time_elpased: 1.584
batch start
#iterations: 253
currently lose_sum: 493.077119320631
time_elpased: 1.596
batch start
#iterations: 254
currently lose_sum: 496.5378592014313
time_elpased: 1.597
batch start
#iterations: 255
currently lose_sum: 495.3031394779682
time_elpased: 1.585
batch start
#iterations: 256
currently lose_sum: 493.90776440501213
time_elpased: 1.605
batch start
#iterations: 257
currently lose_sum: 493.62684670090675
time_elpased: 1.595
batch start
#iterations: 258
currently lose_sum: 494.5620379447937
time_elpased: 1.593
batch start
#iterations: 259
currently lose_sum: 496.9097622334957
time_elpased: 1.606
start validation test
0.609226804124
0.916948515596
0.248979591837
0.391621860204
0
validation finish
batch start
#iterations: 260
currently lose_sum: 495.56740295886993
time_elpased: 1.598
batch start
#iterations: 261
currently lose_sum: 496.3151599764824
time_elpased: 1.602
batch start
#iterations: 262
currently lose_sum: 495.61057990789413
time_elpased: 1.613
batch start
#iterations: 263
currently lose_sum: 494.78273525834084
time_elpased: 1.627
batch start
#iterations: 264
currently lose_sum: 493.33329543471336
time_elpased: 1.595
batch start
#iterations: 265
currently lose_sum: 494.2300269007683
time_elpased: 1.602
batch start
#iterations: 266
currently lose_sum: 495.1721111238003
time_elpased: 1.592
batch start
#iterations: 267
currently lose_sum: 494.47408708930016
time_elpased: 1.596
batch start
#iterations: 268
currently lose_sum: 495.15215238928795
time_elpased: 1.607
batch start
#iterations: 269
currently lose_sum: 496.15941563248634
time_elpased: 1.61
batch start
#iterations: 270
currently lose_sum: 494.32833629846573
time_elpased: 1.603
batch start
#iterations: 271
currently lose_sum: 489.5241817831993
time_elpased: 1.593
batch start
#iterations: 272
currently lose_sum: 495.3537766933441
time_elpased: 1.594
batch start
#iterations: 273
currently lose_sum: 493.3305207490921
time_elpased: 1.594
batch start
#iterations: 274
currently lose_sum: 495.0389846563339
time_elpased: 1.591
batch start
#iterations: 275
currently lose_sum: 495.28180438280106
time_elpased: 1.597
batch start
#iterations: 276
currently lose_sum: 492.9938336610794
time_elpased: 1.602
batch start
#iterations: 277
currently lose_sum: 496.55934169888496
time_elpased: 1.596
batch start
#iterations: 278
currently lose_sum: 496.011309415102
time_elpased: 1.614
batch start
#iterations: 279
currently lose_sum: 494.68572041392326
time_elpased: 1.585
start validation test
0.555257731959
0.908647140865
0.132959183673
0.231974363539
0
validation finish
batch start
#iterations: 280
currently lose_sum: 493.26172107458115
time_elpased: 1.586
batch start
#iterations: 281
currently lose_sum: 494.07890394330025
time_elpased: 1.589
batch start
#iterations: 282
currently lose_sum: 495.03068098425865
time_elpased: 1.592
batch start
#iterations: 283
currently lose_sum: 494.226361066103
time_elpased: 1.604
batch start
#iterations: 284
currently lose_sum: 495.32973515987396
time_elpased: 1.605
batch start
#iterations: 285
currently lose_sum: 494.0503046810627
time_elpased: 1.584
batch start
#iterations: 286
currently lose_sum: 494.7776156961918
time_elpased: 1.591
batch start
#iterations: 287
currently lose_sum: 494.5046235024929
time_elpased: 1.599
batch start
#iterations: 288
currently lose_sum: 495.31723406910896
time_elpased: 1.592
batch start
#iterations: 289
currently lose_sum: 494.1404977738857
time_elpased: 1.612
batch start
#iterations: 290
currently lose_sum: 492.9647834300995
time_elpased: 1.595
batch start
#iterations: 291
currently lose_sum: 494.47147437930107
time_elpased: 1.58
batch start
#iterations: 292
currently lose_sum: 494.17772990465164
time_elpased: 1.583
batch start
#iterations: 293
currently lose_sum: 493.3004261851311
time_elpased: 1.598
batch start
#iterations: 294
currently lose_sum: 494.9580939114094
time_elpased: 1.599
batch start
#iterations: 295
currently lose_sum: 494.59027948975563
time_elpased: 1.604
batch start
#iterations: 296
currently lose_sum: 491.29132330417633
time_elpased: 1.59
batch start
#iterations: 297
currently lose_sum: 494.40992480516434
time_elpased: 1.599
batch start
#iterations: 298
currently lose_sum: 493.27459251880646
time_elpased: 1.603
batch start
#iterations: 299
currently lose_sum: 492.52121073007584
time_elpased: 1.592
start validation test
0.677268041237
0.898087739033
0.407346938776
0.560477360477
0
validation finish
batch start
#iterations: 300
currently lose_sum: 491.9105698168278
time_elpased: 1.604
batch start
#iterations: 301
currently lose_sum: 493.82784393429756
time_elpased: 1.601
batch start
#iterations: 302
currently lose_sum: 494.4819663465023
time_elpased: 1.614
batch start
#iterations: 303
currently lose_sum: 493.57243263721466
time_elpased: 1.603
batch start
#iterations: 304
currently lose_sum: 494.93644866347313
time_elpased: 1.595
batch start
#iterations: 305
currently lose_sum: 495.46405574679375
time_elpased: 1.6
batch start
#iterations: 306
currently lose_sum: 493.6667680442333
time_elpased: 1.591
batch start
#iterations: 307
currently lose_sum: 497.0297797322273
time_elpased: 1.591
batch start
#iterations: 308
currently lose_sum: 493.64235839247704
time_elpased: 1.595
batch start
#iterations: 309
currently lose_sum: 495.66668060421944
time_elpased: 1.583
batch start
#iterations: 310
currently lose_sum: 492.7768259346485
time_elpased: 1.591
batch start
#iterations: 311
currently lose_sum: 492.5843058824539
time_elpased: 1.625
batch start
#iterations: 312
currently lose_sum: 493.11056637763977
time_elpased: 1.582
batch start
#iterations: 313
currently lose_sum: 494.0988918840885
time_elpased: 1.604
batch start
#iterations: 314
currently lose_sum: 492.4914782643318
time_elpased: 1.583
batch start
#iterations: 315
currently lose_sum: 494.0165694952011
time_elpased: 1.595
batch start
#iterations: 316
currently lose_sum: 494.51609003543854
time_elpased: 1.59
batch start
#iterations: 317
currently lose_sum: 493.0082250535488
time_elpased: 1.591
batch start
#iterations: 318
currently lose_sum: 496.33832398056984
time_elpased: 1.59
batch start
#iterations: 319
currently lose_sum: 494.31230142712593
time_elpased: 1.595
start validation test
0.512886597938
0.906976744186
0.0397959183673
0.0762463343109
0
validation finish
batch start
#iterations: 320
currently lose_sum: 494.346752256155
time_elpased: 1.605
batch start
#iterations: 321
currently lose_sum: 492.6707588136196
time_elpased: 1.593
batch start
#iterations: 322
currently lose_sum: 491.9630692601204
time_elpased: 1.592
batch start
#iterations: 323
currently lose_sum: 493.3054518997669
time_elpased: 1.604
batch start
#iterations: 324
currently lose_sum: 495.98650017380714
time_elpased: 1.591
batch start
#iterations: 325
currently lose_sum: 494.45389780402184
time_elpased: 1.594
batch start
#iterations: 326
currently lose_sum: 495.001462161541
time_elpased: 1.588
batch start
#iterations: 327
currently lose_sum: 492.9598142206669
time_elpased: 1.585
batch start
#iterations: 328
currently lose_sum: 492.34408724308014
time_elpased: 1.599
batch start
#iterations: 329
currently lose_sum: 493.7072916328907
time_elpased: 1.596
batch start
#iterations: 330
currently lose_sum: 494.9989817738533
time_elpased: 1.605
batch start
#iterations: 331
currently lose_sum: 495.5735793411732
time_elpased: 1.611
batch start
#iterations: 332
currently lose_sum: 493.0271865725517
time_elpased: 1.593
batch start
#iterations: 333
currently lose_sum: 495.10658076405525
time_elpased: 1.588
batch start
#iterations: 334
currently lose_sum: 493.79319554567337
time_elpased: 1.582
batch start
#iterations: 335
currently lose_sum: 491.3660778105259
time_elpased: 1.584
batch start
#iterations: 336
currently lose_sum: 494.6174491047859
time_elpased: 1.586
batch start
#iterations: 337
currently lose_sum: 493.9311079978943
time_elpased: 1.576
batch start
#iterations: 338
currently lose_sum: 495.1743712723255
time_elpased: 1.606
batch start
#iterations: 339
currently lose_sum: 492.43479043245316
time_elpased: 1.601
start validation test
0.591030927835
0.913197519929
0.210408163265
0.342013600929
0
validation finish
batch start
#iterations: 340
currently lose_sum: 493.8165211081505
time_elpased: 1.595
batch start
#iterations: 341
currently lose_sum: 494.34943705797195
time_elpased: 1.593
batch start
#iterations: 342
currently lose_sum: 492.9981513619423
time_elpased: 1.6
batch start
#iterations: 343
currently lose_sum: 494.5989222228527
time_elpased: 1.604
batch start
#iterations: 344
currently lose_sum: 494.74196618795395
time_elpased: 1.599
batch start
#iterations: 345
currently lose_sum: 494.61290523409843
time_elpased: 1.591
batch start
#iterations: 346
currently lose_sum: 492.91700822114944
time_elpased: 1.609
batch start
#iterations: 347
currently lose_sum: 493.3248634636402
time_elpased: 1.607
batch start
#iterations: 348
currently lose_sum: 494.540022701025
time_elpased: 1.585
batch start
#iterations: 349
currently lose_sum: 494.2201968729496
time_elpased: 1.586
batch start
#iterations: 350
currently lose_sum: 494.5727350115776
time_elpased: 1.592
batch start
#iterations: 351
currently lose_sum: 492.9255306124687
time_elpased: 1.596
batch start
#iterations: 352
currently lose_sum: 493.3469033539295
time_elpased: 1.59
batch start
#iterations: 353
currently lose_sum: 495.5955066084862
time_elpased: 1.603
batch start
#iterations: 354
currently lose_sum: 492.64581948518753
time_elpased: 1.595
batch start
#iterations: 355
currently lose_sum: 494.64694595336914
time_elpased: 1.584
batch start
#iterations: 356
currently lose_sum: 494.6033051609993
time_elpased: 1.586
batch start
#iterations: 357
currently lose_sum: 493.6611639857292
time_elpased: 1.592
batch start
#iterations: 358
currently lose_sum: 494.22560331225395
time_elpased: 1.603
batch start
#iterations: 359
currently lose_sum: 493.62658992409706
time_elpased: 1.607
start validation test
0.594742268041
0.914102564103
0.218265306122
0.352388797364
0
validation finish
batch start
#iterations: 360
currently lose_sum: 493.9047392308712
time_elpased: 1.597
batch start
#iterations: 361
currently lose_sum: 493.06548646092415
time_elpased: 1.588
batch start
#iterations: 362
currently lose_sum: 494.0609340965748
time_elpased: 1.594
batch start
#iterations: 363
currently lose_sum: 492.6420195400715
time_elpased: 1.601
batch start
#iterations: 364
currently lose_sum: 493.08447179198265
time_elpased: 1.595
batch start
#iterations: 365
currently lose_sum: 493.95059245824814
time_elpased: 1.593
batch start
#iterations: 366
currently lose_sum: 494.8511323928833
time_elpased: 1.589
batch start
#iterations: 367
currently lose_sum: 495.0525067150593
time_elpased: 1.595
batch start
#iterations: 368
currently lose_sum: 491.7202852368355
time_elpased: 1.607
batch start
#iterations: 369
currently lose_sum: 491.8204778134823
time_elpased: 1.601
batch start
#iterations: 370
currently lose_sum: 491.75379505753517
time_elpased: 1.593
batch start
#iterations: 371
currently lose_sum: 494.49251610040665
time_elpased: 1.596
batch start
#iterations: 372
currently lose_sum: 493.1066093444824
time_elpased: 1.6
batch start
#iterations: 373
currently lose_sum: 493.045640617609
time_elpased: 1.586
batch start
#iterations: 374
currently lose_sum: 494.3656234741211
time_elpased: 1.592
batch start
#iterations: 375
currently lose_sum: 492.64558589458466
time_elpased: 1.588
batch start
#iterations: 376
currently lose_sum: 494.2540543079376
time_elpased: 1.601
batch start
#iterations: 377
currently lose_sum: 495.12005987763405
time_elpased: 1.582
batch start
#iterations: 378
currently lose_sum: 494.29308235645294
time_elpased: 1.594
batch start
#iterations: 379
currently lose_sum: 494.07233226299286
time_elpased: 1.59
start validation test
0.626494845361
0.907726692209
0.290102040816
0.439684503557
0
validation finish
batch start
#iterations: 380
currently lose_sum: 492.84116420149803
time_elpased: 1.601
batch start
#iterations: 381
currently lose_sum: 492.16969004273415
time_elpased: 1.607
batch start
#iterations: 382
currently lose_sum: 493.65467885136604
time_elpased: 1.601
batch start
#iterations: 383
currently lose_sum: 491.81489995121956
time_elpased: 1.591
batch start
#iterations: 384
currently lose_sum: 493.0767832696438
time_elpased: 1.596
batch start
#iterations: 385
currently lose_sum: 490.6259199678898
time_elpased: 1.602
batch start
#iterations: 386
currently lose_sum: 493.30679443478584
time_elpased: 1.594
batch start
#iterations: 387
currently lose_sum: 494.4962832033634
time_elpased: 1.597
batch start
#iterations: 388
currently lose_sum: 494.66954040527344
time_elpased: 1.589
batch start
#iterations: 389
currently lose_sum: 490.8810858428478
time_elpased: 1.592
batch start
#iterations: 390
currently lose_sum: 491.92785316705704
time_elpased: 1.597
batch start
#iterations: 391
currently lose_sum: 493.1566292643547
time_elpased: 1.596
batch start
#iterations: 392
currently lose_sum: 493.2678241431713
time_elpased: 1.59
batch start
#iterations: 393
currently lose_sum: 492.9376949965954
time_elpased: 1.603
batch start
#iterations: 394
currently lose_sum: 492.87769025564194
time_elpased: 1.582
batch start
#iterations: 395
currently lose_sum: 491.94327211380005
time_elpased: 1.61
batch start
#iterations: 396
currently lose_sum: 493.02546164393425
time_elpased: 1.601
batch start
#iterations: 397
currently lose_sum: 493.67641466856
time_elpased: 1.595
batch start
#iterations: 398
currently lose_sum: 493.650836288929
time_elpased: 1.595
batch start
#iterations: 399
currently lose_sum: 493.97599095106125
time_elpased: 1.594
start validation test
0.546082474227
0.902103559871
0.113775510204
0.20206596593
0
validation finish
acc: 0.748
pre: 0.873
rec: 0.574
F1: 0.692
auc: 0.000
