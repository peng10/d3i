start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 550.8806565999985
time_elpased: 1.675
batch start
#iterations: 1
currently lose_sum: 542.4323187470436
time_elpased: 1.612
batch start
#iterations: 2
currently lose_sum: 538.7374621331692
time_elpased: 1.615
batch start
#iterations: 3
currently lose_sum: 533.7810323834419
time_elpased: 1.624
batch start
#iterations: 4
currently lose_sum: 531.4017663300037
time_elpased: 1.593
batch start
#iterations: 5
currently lose_sum: 529.2927289903164
time_elpased: 1.611
batch start
#iterations: 6
currently lose_sum: 524.8752657175064
time_elpased: 1.614
batch start
#iterations: 7
currently lose_sum: 524.9798443615437
time_elpased: 1.619
batch start
#iterations: 8
currently lose_sum: 523.3397760391235
time_elpased: 1.616
batch start
#iterations: 9
currently lose_sum: 521.4174737036228
time_elpased: 1.612
batch start
#iterations: 10
currently lose_sum: 519.7580044865608
time_elpased: 1.608
batch start
#iterations: 11
currently lose_sum: 521.5456351935863
time_elpased: 1.62
batch start
#iterations: 12
currently lose_sum: 518.5860371887684
time_elpased: 1.606
batch start
#iterations: 13
currently lose_sum: 517.3115732073784
time_elpased: 1.623
batch start
#iterations: 14
currently lose_sum: 516.6087390780449
time_elpased: 1.611
batch start
#iterations: 15
currently lose_sum: 516.838479757309
time_elpased: 1.625
batch start
#iterations: 16
currently lose_sum: 517.5017694234848
time_elpased: 1.623
batch start
#iterations: 17
currently lose_sum: 515.922765225172
time_elpased: 1.611
batch start
#iterations: 18
currently lose_sum: 514.4011265039444
time_elpased: 1.618
batch start
#iterations: 19
currently lose_sum: 515.4413233697414
time_elpased: 1.625
start validation test
0.598298969072
0.924852874604
0.211271975181
0.343968347504
0
validation finish
batch start
#iterations: 20
currently lose_sum: 514.9290961921215
time_elpased: 1.601
batch start
#iterations: 21
currently lose_sum: 515.8063846230507
time_elpased: 1.627
batch start
#iterations: 22
currently lose_sum: 512.7937296330929
time_elpased: 1.601
batch start
#iterations: 23
currently lose_sum: 513.8533234000206
time_elpased: 1.622
batch start
#iterations: 24
currently lose_sum: 512.313648879528
time_elpased: 1.602
batch start
#iterations: 25
currently lose_sum: 514.0871568024158
time_elpased: 1.607
batch start
#iterations: 26
currently lose_sum: 513.0509209632874
time_elpased: 1.615
batch start
#iterations: 27
currently lose_sum: 512.839443653822
time_elpased: 1.606
batch start
#iterations: 28
currently lose_sum: 510.93695798516273
time_elpased: 1.616
batch start
#iterations: 29
currently lose_sum: 510.7587912976742
time_elpased: 1.611
batch start
#iterations: 30
currently lose_sum: 511.29443630576134
time_elpased: 1.62
batch start
#iterations: 31
currently lose_sum: 509.51486790180206
time_elpased: 1.616
batch start
#iterations: 32
currently lose_sum: 511.7722753584385
time_elpased: 1.603
batch start
#iterations: 33
currently lose_sum: 509.914528042078
time_elpased: 1.607
batch start
#iterations: 34
currently lose_sum: 512.2460817098618
time_elpased: 1.614
batch start
#iterations: 35
currently lose_sum: 507.3263821899891
time_elpased: 1.635
batch start
#iterations: 36
currently lose_sum: 510.40826138854027
time_elpased: 1.599
batch start
#iterations: 37
currently lose_sum: 508.0831498205662
time_elpased: 1.612
batch start
#iterations: 38
currently lose_sum: 508.89683735370636
time_elpased: 1.595
batch start
#iterations: 39
currently lose_sum: 509.8971861600876
time_elpased: 1.638
start validation test
0.536958762887
0.954966887417
0.0745604963806
0.138321342926
0
validation finish
batch start
#iterations: 40
currently lose_sum: 511.42634573578835
time_elpased: 1.625
batch start
#iterations: 41
currently lose_sum: 509.8606967329979
time_elpased: 1.605
batch start
#iterations: 42
currently lose_sum: 507.7062269449234
time_elpased: 1.602
batch start
#iterations: 43
currently lose_sum: 508.7375658750534
time_elpased: 1.609
batch start
#iterations: 44
currently lose_sum: 507.51007211208344
time_elpased: 1.622
batch start
#iterations: 45
currently lose_sum: 507.43839716911316
time_elpased: 1.615
batch start
#iterations: 46
currently lose_sum: 506.0371205210686
time_elpased: 1.626
batch start
#iterations: 47
currently lose_sum: 507.5116542875767
time_elpased: 1.621
batch start
#iterations: 48
currently lose_sum: 506.86625069379807
time_elpased: 1.627
batch start
#iterations: 49
currently lose_sum: 506.94673162698746
time_elpased: 1.609
batch start
#iterations: 50
currently lose_sum: 507.4959477484226
time_elpased: 1.601
batch start
#iterations: 51
currently lose_sum: 505.94195330142975
time_elpased: 1.63
batch start
#iterations: 52
currently lose_sum: 506.24101012945175
time_elpased: 1.607
batch start
#iterations: 53
currently lose_sum: 508.90452644228935
time_elpased: 1.617
batch start
#iterations: 54
currently lose_sum: 505.52645817399025
time_elpased: 1.612
batch start
#iterations: 55
currently lose_sum: 508.44974517822266
time_elpased: 1.604
batch start
#iterations: 56
currently lose_sum: 506.62807339429855
time_elpased: 1.624
batch start
#iterations: 57
currently lose_sum: 505.9772243797779
time_elpased: 1.625
batch start
#iterations: 58
currently lose_sum: 507.1405951976776
time_elpased: 1.611
batch start
#iterations: 59
currently lose_sum: 504.9836921095848
time_elpased: 1.646
start validation test
0.617628865979
0.921722846442
0.254498448811
0.39886547812
0
validation finish
batch start
#iterations: 60
currently lose_sum: 507.231712192297
time_elpased: 1.615
batch start
#iterations: 61
currently lose_sum: 507.14509642124176
time_elpased: 1.629
batch start
#iterations: 62
currently lose_sum: 508.6630514860153
time_elpased: 1.621
batch start
#iterations: 63
currently lose_sum: 507.2526268362999
time_elpased: 1.602
batch start
#iterations: 64
currently lose_sum: 506.7115141749382
time_elpased: 1.613
batch start
#iterations: 65
currently lose_sum: 505.39587077498436
time_elpased: 1.615
batch start
#iterations: 66
currently lose_sum: 505.6953345835209
time_elpased: 1.615
batch start
#iterations: 67
currently lose_sum: 503.91363856196404
time_elpased: 1.625
batch start
#iterations: 68
currently lose_sum: 506.86516067385674
time_elpased: 1.618
batch start
#iterations: 69
currently lose_sum: 505.5652756392956
time_elpased: 1.623
batch start
#iterations: 70
currently lose_sum: 505.5673174262047
time_elpased: 1.612
batch start
#iterations: 71
currently lose_sum: 505.64473709464073
time_elpased: 1.613
batch start
#iterations: 72
currently lose_sum: 505.70024567842484
time_elpased: 1.599
batch start
#iterations: 73
currently lose_sum: 503.6577487587929
time_elpased: 1.611
batch start
#iterations: 74
currently lose_sum: 505.06588247418404
time_elpased: 1.617
batch start
#iterations: 75
currently lose_sum: 503.63200056552887
time_elpased: 1.624
batch start
#iterations: 76
currently lose_sum: 505.62293285131454
time_elpased: 1.595
batch start
#iterations: 77
currently lose_sum: 504.1063506305218
time_elpased: 1.612
batch start
#iterations: 78
currently lose_sum: 504.4772426187992
time_elpased: 1.602
batch start
#iterations: 79
currently lose_sum: 506.48066809773445
time_elpased: 1.604
start validation test
0.643865979381
0.894992846924
0.323474663909
0.475199392328
0
validation finish
batch start
#iterations: 80
currently lose_sum: 502.63305470347404
time_elpased: 1.621
batch start
#iterations: 81
currently lose_sum: 503.5830591917038
time_elpased: 1.614
batch start
#iterations: 82
currently lose_sum: 504.2266335785389
time_elpased: 1.625
batch start
#iterations: 83
currently lose_sum: 504.4211944639683
time_elpased: 1.62
batch start
#iterations: 84
currently lose_sum: 504.93542528152466
time_elpased: 1.614
batch start
#iterations: 85
currently lose_sum: 502.50584188103676
time_elpased: 1.61
batch start
#iterations: 86
currently lose_sum: 502.6461124718189
time_elpased: 1.613
batch start
#iterations: 87
currently lose_sum: 502.7751282453537
time_elpased: 1.612
batch start
#iterations: 88
currently lose_sum: 503.3843173086643
time_elpased: 1.608
batch start
#iterations: 89
currently lose_sum: 502.74087756872177
time_elpased: 1.615
batch start
#iterations: 90
currently lose_sum: 505.4756669998169
time_elpased: 1.602
batch start
#iterations: 91
currently lose_sum: 501.8713609576225
time_elpased: 1.609
batch start
#iterations: 92
currently lose_sum: 503.83005544543266
time_elpased: 1.611
batch start
#iterations: 93
currently lose_sum: 502.5440478026867
time_elpased: 1.61
batch start
#iterations: 94
currently lose_sum: 502.54521682858467
time_elpased: 1.608
batch start
#iterations: 95
currently lose_sum: 502.90422916412354
time_elpased: 1.617
batch start
#iterations: 96
currently lose_sum: 503.98928239941597
time_elpased: 1.611
batch start
#iterations: 97
currently lose_sum: 502.479471385479
time_elpased: 1.611
batch start
#iterations: 98
currently lose_sum: 502.4667888879776
time_elpased: 1.622
batch start
#iterations: 99
currently lose_sum: 502.25051856040955
time_elpased: 1.616
start validation test
0.655257731959
0.891338582677
0.351189245088
0.503857566766
0
validation finish
batch start
#iterations: 100
currently lose_sum: 502.55793780088425
time_elpased: 1.617
batch start
#iterations: 101
currently lose_sum: 502.2485422492027
time_elpased: 1.606
batch start
#iterations: 102
currently lose_sum: 500.8430334031582
time_elpased: 1.613
batch start
#iterations: 103
currently lose_sum: 501.81739342212677
time_elpased: 1.608
batch start
#iterations: 104
currently lose_sum: 502.79586696624756
time_elpased: 1.609
batch start
#iterations: 105
currently lose_sum: 501.4148724973202
time_elpased: 1.618
batch start
#iterations: 106
currently lose_sum: 502.75933358073235
time_elpased: 1.621
batch start
#iterations: 107
currently lose_sum: 503.0987829566002
time_elpased: 1.614
batch start
#iterations: 108
currently lose_sum: 500.6747914850712
time_elpased: 1.603
batch start
#iterations: 109
currently lose_sum: 502.4114438891411
time_elpased: 1.612
batch start
#iterations: 110
currently lose_sum: 502.1874089539051
time_elpased: 1.632
batch start
#iterations: 111
currently lose_sum: 504.28692841529846
time_elpased: 1.622
batch start
#iterations: 112
currently lose_sum: 501.03857788443565
time_elpased: 1.609
batch start
#iterations: 113
currently lose_sum: 500.8699986934662
time_elpased: 1.624
batch start
#iterations: 114
currently lose_sum: 500.7789916396141
time_elpased: 1.618
batch start
#iterations: 115
currently lose_sum: 500.900806337595
time_elpased: 1.631
batch start
#iterations: 116
currently lose_sum: 500.9795939028263
time_elpased: 1.614
batch start
#iterations: 117
currently lose_sum: 502.3635936975479
time_elpased: 1.62
batch start
#iterations: 118
currently lose_sum: 500.73686105012894
time_elpased: 1.636
batch start
#iterations: 119
currently lose_sum: 500.58704298734665
time_elpased: 1.619
start validation test
0.737577319588
0.885372832856
0.543950361944
0.673883799885
0
validation finish
batch start
#iterations: 120
currently lose_sum: 502.91984662413597
time_elpased: 1.625
batch start
#iterations: 121
currently lose_sum: 499.65195313096046
time_elpased: 1.614
batch start
#iterations: 122
currently lose_sum: 499.6278828382492
time_elpased: 1.608
batch start
#iterations: 123
currently lose_sum: 499.1074612438679
time_elpased: 1.612
batch start
#iterations: 124
currently lose_sum: 498.81596860289574
time_elpased: 1.603
batch start
#iterations: 125
currently lose_sum: 500.1203128695488
time_elpased: 1.625
batch start
#iterations: 126
currently lose_sum: 499.64050927758217
time_elpased: 1.608
batch start
#iterations: 127
currently lose_sum: 502.6570508778095
time_elpased: 1.616
batch start
#iterations: 128
currently lose_sum: 499.4418145418167
time_elpased: 1.61
batch start
#iterations: 129
currently lose_sum: 499.7558368444443
time_elpased: 1.604
batch start
#iterations: 130
currently lose_sum: 499.3896911740303
time_elpased: 1.609
batch start
#iterations: 131
currently lose_sum: 500.0993174314499
time_elpased: 1.613
batch start
#iterations: 132
currently lose_sum: 502.18547853827477
time_elpased: 1.598
batch start
#iterations: 133
currently lose_sum: 500.43823939561844
time_elpased: 1.607
batch start
#iterations: 134
currently lose_sum: 501.2716173827648
time_elpased: 1.609
batch start
#iterations: 135
currently lose_sum: 498.4419412910938
time_elpased: 1.615
batch start
#iterations: 136
currently lose_sum: 499.99906927347183
time_elpased: 1.6
batch start
#iterations: 137
currently lose_sum: 499.23663625121117
time_elpased: 1.622
batch start
#iterations: 138
currently lose_sum: 500.2502363026142
time_elpased: 1.603
batch start
#iterations: 139
currently lose_sum: 500.9301007390022
time_elpased: 1.609
start validation test
0.586649484536
0.905252822779
0.190692864529
0.315025198599
0
validation finish
batch start
#iterations: 140
currently lose_sum: 497.371170014143
time_elpased: 1.621
batch start
#iterations: 141
currently lose_sum: 501.1632766723633
time_elpased: 1.606
batch start
#iterations: 142
currently lose_sum: 499.8387230038643
time_elpased: 1.614
batch start
#iterations: 143
currently lose_sum: 500.94630539417267
time_elpased: 1.634
batch start
#iterations: 144
currently lose_sum: 500.47922828793526
time_elpased: 1.599
batch start
#iterations: 145
currently lose_sum: 501.80874729156494
time_elpased: 1.609
batch start
#iterations: 146
currently lose_sum: 502.2047670185566
time_elpased: 1.6
batch start
#iterations: 147
currently lose_sum: 499.72590559720993
time_elpased: 1.617
batch start
#iterations: 148
currently lose_sum: 498.28435561060905
time_elpased: 1.619
batch start
#iterations: 149
currently lose_sum: 501.823246717453
time_elpased: 1.622
batch start
#iterations: 150
currently lose_sum: 499.3806990683079
time_elpased: 1.619
batch start
#iterations: 151
currently lose_sum: 499.89751449227333
time_elpased: 1.621
batch start
#iterations: 152
currently lose_sum: 499.9489623308182
time_elpased: 1.602
batch start
#iterations: 153
currently lose_sum: 496.8439421951771
time_elpased: 1.614
batch start
#iterations: 154
currently lose_sum: 503.17515420913696
time_elpased: 1.604
batch start
#iterations: 155
currently lose_sum: 499.41587603092194
time_elpased: 1.616
batch start
#iterations: 156
currently lose_sum: 499.7540156841278
time_elpased: 1.6
batch start
#iterations: 157
currently lose_sum: 499.77011120319366
time_elpased: 1.61
batch start
#iterations: 158
currently lose_sum: 499.0518192052841
time_elpased: 1.611
batch start
#iterations: 159
currently lose_sum: 498.902458101511
time_elpased: 1.605
start validation test
0.525515463918
0.92817679558
0.052119958635
0.0986977381768
0
validation finish
batch start
#iterations: 160
currently lose_sum: 500.19399496912956
time_elpased: 1.623
batch start
#iterations: 161
currently lose_sum: 500.033869355917
time_elpased: 1.639
batch start
#iterations: 162
currently lose_sum: 498.5415368080139
time_elpased: 1.616
batch start
#iterations: 163
currently lose_sum: 497.3698613345623
time_elpased: 1.616
batch start
#iterations: 164
currently lose_sum: 500.7972985804081
time_elpased: 1.614
batch start
#iterations: 165
currently lose_sum: 499.14743426442146
time_elpased: 1.604
batch start
#iterations: 166
currently lose_sum: 499.76892042160034
time_elpased: 1.617
batch start
#iterations: 167
currently lose_sum: 500.2902738749981
time_elpased: 1.614
batch start
#iterations: 168
currently lose_sum: 499.43168368935585
time_elpased: 1.622
batch start
#iterations: 169
currently lose_sum: 501.56568267941475
time_elpased: 1.609
batch start
#iterations: 170
currently lose_sum: 499.82599851489067
time_elpased: 1.604
batch start
#iterations: 171
currently lose_sum: 498.29058825969696
time_elpased: 1.623
batch start
#iterations: 172
currently lose_sum: 500.3349621295929
time_elpased: 1.612
batch start
#iterations: 173
currently lose_sum: 499.560469776392
time_elpased: 1.6
batch start
#iterations: 174
currently lose_sum: 500.24611845612526
time_elpased: 1.611
batch start
#iterations: 175
currently lose_sum: 499.8398131430149
time_elpased: 1.613
batch start
#iterations: 176
currently lose_sum: 498.95073613524437
time_elpased: 1.631
batch start
#iterations: 177
currently lose_sum: 500.0472613275051
time_elpased: 1.6
batch start
#iterations: 178
currently lose_sum: 499.21757528185844
time_elpased: 1.617
batch start
#iterations: 179
currently lose_sum: 498.53789708018303
time_elpased: 1.609
start validation test
0.589639175258
0.913801452785
0.195139607032
0.321602045164
0
validation finish
batch start
#iterations: 180
currently lose_sum: 498.70063492655754
time_elpased: 1.623
batch start
#iterations: 181
currently lose_sum: 500.8143335878849
time_elpased: 1.597
batch start
#iterations: 182
currently lose_sum: 497.5390402674675
time_elpased: 1.609
batch start
#iterations: 183
currently lose_sum: 499.4027717411518
time_elpased: 1.605
batch start
#iterations: 184
currently lose_sum: 497.615507632494
time_elpased: 1.625
batch start
#iterations: 185
currently lose_sum: 496.749225795269
time_elpased: 1.608
batch start
#iterations: 186
currently lose_sum: 500.7298984825611
time_elpased: 1.609
batch start
#iterations: 187
currently lose_sum: 498.8098490834236
time_elpased: 1.603
batch start
#iterations: 188
currently lose_sum: 495.8660434484482
time_elpased: 1.601
batch start
#iterations: 189
currently lose_sum: 497.55076518654823
time_elpased: 1.615
batch start
#iterations: 190
currently lose_sum: 497.32066974043846
time_elpased: 1.608
batch start
#iterations: 191
currently lose_sum: 498.96507704257965
time_elpased: 1.597
batch start
#iterations: 192
currently lose_sum: 498.34345123171806
time_elpased: 1.612
batch start
#iterations: 193
currently lose_sum: 498.3996439576149
time_elpased: 1.603
batch start
#iterations: 194
currently lose_sum: 499.04762202501297
time_elpased: 1.606
batch start
#iterations: 195
currently lose_sum: 499.0151708126068
time_elpased: 1.593
batch start
#iterations: 196
currently lose_sum: 497.502036690712
time_elpased: 1.608
batch start
#iterations: 197
currently lose_sum: 500.2047810256481
time_elpased: 1.606
batch start
#iterations: 198
currently lose_sum: 497.8009526729584
time_elpased: 1.612
batch start
#iterations: 199
currently lose_sum: 498.47284638881683
time_elpased: 1.615
start validation test
0.634639175258
0.898702903027
0.300930713547
0.450883173226
0
validation finish
batch start
#iterations: 200
currently lose_sum: 499.8896435201168
time_elpased: 1.622
batch start
#iterations: 201
currently lose_sum: 497.67321398854256
time_elpased: 1.616
batch start
#iterations: 202
currently lose_sum: 497.5818839967251
time_elpased: 1.605
batch start
#iterations: 203
currently lose_sum: 497.14303439855576
time_elpased: 1.604
batch start
#iterations: 204
currently lose_sum: 498.2243430316448
time_elpased: 1.636
batch start
#iterations: 205
currently lose_sum: 498.9357826411724
time_elpased: 1.611
batch start
#iterations: 206
currently lose_sum: 499.96595323085785
time_elpased: 1.624
batch start
#iterations: 207
currently lose_sum: 496.3322240412235
time_elpased: 1.61
batch start
#iterations: 208
currently lose_sum: 497.67783039808273
time_elpased: 1.599
batch start
#iterations: 209
currently lose_sum: 496.55294820666313
time_elpased: 1.603
batch start
#iterations: 210
currently lose_sum: 498.8496384322643
time_elpased: 1.624
batch start
#iterations: 211
currently lose_sum: 498.36907318234444
time_elpased: 1.598
batch start
#iterations: 212
currently lose_sum: 498.03340741991997
time_elpased: 1.615
batch start
#iterations: 213
currently lose_sum: 497.5198746621609
time_elpased: 1.627
batch start
#iterations: 214
currently lose_sum: 498.01355186104774
time_elpased: 1.61
batch start
#iterations: 215
currently lose_sum: 497.7421830892563
time_elpased: 1.608
batch start
#iterations: 216
currently lose_sum: 498.40707117319107
time_elpased: 1.618
batch start
#iterations: 217
currently lose_sum: 496.84158766269684
time_elpased: 1.624
batch start
#iterations: 218
currently lose_sum: 498.48037445545197
time_elpased: 1.615
batch start
#iterations: 219
currently lose_sum: 498.896964520216
time_elpased: 1.612
start validation test
0.526546391753
0.917383820998
0.0551189245088
0.103989854648
0
validation finish
batch start
#iterations: 220
currently lose_sum: 496.0618612766266
time_elpased: 1.62
batch start
#iterations: 221
currently lose_sum: 499.1395117342472
time_elpased: 1.61
batch start
#iterations: 222
currently lose_sum: 497.0702917277813
time_elpased: 1.602
batch start
#iterations: 223
currently lose_sum: 499.10987517237663
time_elpased: 1.61
batch start
#iterations: 224
currently lose_sum: 497.2096639573574
time_elpased: 1.603
batch start
#iterations: 225
currently lose_sum: 501.16961216926575
time_elpased: 1.627
batch start
#iterations: 226
currently lose_sum: 498.2479302585125
time_elpased: 1.605
batch start
#iterations: 227
currently lose_sum: 495.411843419075
time_elpased: 1.611
batch start
#iterations: 228
currently lose_sum: 496.22941625118256
time_elpased: 1.613
batch start
#iterations: 229
currently lose_sum: 495.407406270504
time_elpased: 1.609
batch start
#iterations: 230
currently lose_sum: 499.4717815220356
time_elpased: 1.612
batch start
#iterations: 231
currently lose_sum: 500.4122701585293
time_elpased: 1.617
batch start
#iterations: 232
currently lose_sum: 497.3952570259571
time_elpased: 1.61
batch start
#iterations: 233
currently lose_sum: 496.1047596335411
time_elpased: 1.606
batch start
#iterations: 234
currently lose_sum: 496.9891923069954
time_elpased: 1.626
batch start
#iterations: 235
currently lose_sum: 496.4396276772022
time_elpased: 1.615
batch start
#iterations: 236
currently lose_sum: 496.0530746281147
time_elpased: 1.616
batch start
#iterations: 237
currently lose_sum: 498.17120441794395
time_elpased: 1.619
batch start
#iterations: 238
currently lose_sum: 495.5636567771435
time_elpased: 1.611
batch start
#iterations: 239
currently lose_sum: 497.08347898721695
time_elpased: 1.621
start validation test
0.518608247423
0.941333333333
0.0365046535677
0.0702837232454
0
validation finish
batch start
#iterations: 240
currently lose_sum: 495.22735235095024
time_elpased: 1.615
batch start
#iterations: 241
currently lose_sum: 498.75193950533867
time_elpased: 1.635
batch start
#iterations: 242
currently lose_sum: 496.14807718992233
time_elpased: 1.604
batch start
#iterations: 243
currently lose_sum: 495.33639192581177
time_elpased: 1.619
batch start
#iterations: 244
currently lose_sum: 497.4523289501667
time_elpased: 1.628
batch start
#iterations: 245
currently lose_sum: 496.0359587073326
time_elpased: 1.615
batch start
#iterations: 246
currently lose_sum: 496.0125388801098
time_elpased: 1.605
batch start
#iterations: 247
currently lose_sum: 496.2593340873718
time_elpased: 1.617
batch start
#iterations: 248
currently lose_sum: 495.91052013635635
time_elpased: 1.609
batch start
#iterations: 249
currently lose_sum: 495.42328307032585
time_elpased: 1.638
batch start
#iterations: 250
currently lose_sum: 496.6732724606991
time_elpased: 1.637
batch start
#iterations: 251
currently lose_sum: 498.7139550745487
time_elpased: 1.624
batch start
#iterations: 252
currently lose_sum: 497.1845810711384
time_elpased: 1.604
batch start
#iterations: 253
currently lose_sum: 496.13159903883934
time_elpased: 1.613
batch start
#iterations: 254
currently lose_sum: 498.33688473701477
time_elpased: 1.621
batch start
#iterations: 255
currently lose_sum: 496.5166355073452
time_elpased: 1.613
batch start
#iterations: 256
currently lose_sum: 496.183604568243
time_elpased: 1.639
batch start
#iterations: 257
currently lose_sum: 495.5269373655319
time_elpased: 1.603
batch start
#iterations: 258
currently lose_sum: 498.0314415693283
time_elpased: 1.613
batch start
#iterations: 259
currently lose_sum: 497.8475162386894
time_elpased: 1.593
start validation test
0.59912371134
0.901569792109
0.219751809721
0.35337158061
0
validation finish
batch start
#iterations: 260
currently lose_sum: 498.4845752120018
time_elpased: 1.632
batch start
#iterations: 261
currently lose_sum: 497.6540513038635
time_elpased: 1.609
batch start
#iterations: 262
currently lose_sum: 495.9250255525112
time_elpased: 1.62
batch start
#iterations: 263
currently lose_sum: 496.42497581243515
time_elpased: 1.608
batch start
#iterations: 264
currently lose_sum: 497.00406673550606
time_elpased: 1.611
batch start
#iterations: 265
currently lose_sum: 497.4126950800419
time_elpased: 1.615
batch start
#iterations: 266
currently lose_sum: 496.2884483933449
time_elpased: 1.616
batch start
#iterations: 267
currently lose_sum: 495.03444069623947
time_elpased: 1.614
batch start
#iterations: 268
currently lose_sum: 497.19496819376945
time_elpased: 1.622
batch start
#iterations: 269
currently lose_sum: 497.2320525944233
time_elpased: 1.609
batch start
#iterations: 270
currently lose_sum: 495.65468069911003
time_elpased: 1.615
batch start
#iterations: 271
currently lose_sum: 494.8260000050068
time_elpased: 1.603
batch start
#iterations: 272
currently lose_sum: 495.74489656090736
time_elpased: 1.612
batch start
#iterations: 273
currently lose_sum: 494.0211618244648
time_elpased: 1.618
batch start
#iterations: 274
currently lose_sum: 497.7085816860199
time_elpased: 1.603
batch start
#iterations: 275
currently lose_sum: 495.48898527026176
time_elpased: 1.608
batch start
#iterations: 276
currently lose_sum: 496.3444454371929
time_elpased: 1.617
batch start
#iterations: 277
currently lose_sum: 497.7062945663929
time_elpased: 1.602
batch start
#iterations: 278
currently lose_sum: 497.81638488173485
time_elpased: 1.628
batch start
#iterations: 279
currently lose_sum: 497.5932665467262
time_elpased: 1.611
start validation test
0.549793814433
0.917112299465
0.106411582213
0.190696812454
0
validation finish
batch start
#iterations: 280
currently lose_sum: 494.76078119874
time_elpased: 1.623
batch start
#iterations: 281
currently lose_sum: 494.3345444202423
time_elpased: 1.622
batch start
#iterations: 282
currently lose_sum: 497.9080547988415
time_elpased: 1.6
batch start
#iterations: 283
currently lose_sum: 495.90260657668114
time_elpased: 1.628
batch start
#iterations: 284
currently lose_sum: 497.8435143530369
time_elpased: 1.609
batch start
#iterations: 285
currently lose_sum: 497.8196968436241
time_elpased: 1.604
batch start
#iterations: 286
currently lose_sum: 499.6398228406906
time_elpased: 1.611
batch start
#iterations: 287
currently lose_sum: 497.4080709517002
time_elpased: 1.642
batch start
#iterations: 288
currently lose_sum: 497.99207866191864
time_elpased: 1.616
batch start
#iterations: 289
currently lose_sum: 496.48434069752693
time_elpased: 1.628
batch start
#iterations: 290
currently lose_sum: 495.7042919099331
time_elpased: 1.612
batch start
#iterations: 291
currently lose_sum: 496.8573490381241
time_elpased: 1.61
batch start
#iterations: 292
currently lose_sum: 495.4145784676075
time_elpased: 1.624
batch start
#iterations: 293
currently lose_sum: 497.0147048532963
time_elpased: 1.612
batch start
#iterations: 294
currently lose_sum: 496.69068679213524
time_elpased: 1.622
batch start
#iterations: 295
currently lose_sum: 496.63296723365784
time_elpased: 1.616
batch start
#iterations: 296
currently lose_sum: 495.1078428328037
time_elpased: 1.61
batch start
#iterations: 297
currently lose_sum: 497.32709017395973
time_elpased: 1.613
batch start
#iterations: 298
currently lose_sum: 495.02695444226265
time_elpased: 1.611
batch start
#iterations: 299
currently lose_sum: 497.4804454445839
time_elpased: 1.611
start validation test
0.635051546392
0.891238670695
0.305067218201
0.454545454545
0
validation finish
batch start
#iterations: 300
currently lose_sum: 495.71431800723076
time_elpased: 1.626
batch start
#iterations: 301
currently lose_sum: 496.1005359888077
time_elpased: 1.596
batch start
#iterations: 302
currently lose_sum: 498.0648228228092
time_elpased: 1.623
batch start
#iterations: 303
currently lose_sum: 497.83470982313156
time_elpased: 1.61
batch start
#iterations: 304
currently lose_sum: 493.45783668756485
time_elpased: 1.612
batch start
#iterations: 305
currently lose_sum: 497.82830369472504
time_elpased: 1.621
batch start
#iterations: 306
currently lose_sum: 495.03862619400024
time_elpased: 1.618
batch start
#iterations: 307
currently lose_sum: 496.22179043293
time_elpased: 1.623
batch start
#iterations: 308
currently lose_sum: 494.70490580797195
time_elpased: 1.628
batch start
#iterations: 309
currently lose_sum: 495.09106984734535
time_elpased: 1.613
batch start
#iterations: 310
currently lose_sum: 496.69045585393906
time_elpased: 1.617
batch start
#iterations: 311
currently lose_sum: 494.6104808151722
time_elpased: 1.609
batch start
#iterations: 312
currently lose_sum: 496.38273361325264
time_elpased: 1.611
batch start
#iterations: 313
currently lose_sum: 498.13715732097626
time_elpased: 1.606
batch start
#iterations: 314
currently lose_sum: 494.73631051182747
time_elpased: 1.62
batch start
#iterations: 315
currently lose_sum: 496.1506738960743
time_elpased: 1.616
batch start
#iterations: 316
currently lose_sum: 496.0779954791069
time_elpased: 1.606
batch start
#iterations: 317
currently lose_sum: 496.50299030542374
time_elpased: 1.615
batch start
#iterations: 318
currently lose_sum: 498.4340350627899
time_elpased: 1.62
batch start
#iterations: 319
currently lose_sum: 495.0652012228966
time_elpased: 1.605
start validation test
0.530257731959
0.925190839695
0.0626680455016
0.117384987893
0
validation finish
batch start
#iterations: 320
currently lose_sum: 496.7971313893795
time_elpased: 1.642
batch start
#iterations: 321
currently lose_sum: 498.4186090230942
time_elpased: 1.605
batch start
#iterations: 322
currently lose_sum: 494.6387318074703
time_elpased: 1.627
batch start
#iterations: 323
currently lose_sum: 494.23943200707436
time_elpased: 1.626
batch start
#iterations: 324
currently lose_sum: 497.56979608535767
time_elpased: 1.615
batch start
#iterations: 325
currently lose_sum: 494.04135328531265
time_elpased: 1.626
batch start
#iterations: 326
currently lose_sum: 493.7956491410732
time_elpased: 1.606
batch start
#iterations: 327
currently lose_sum: 496.3144809305668
time_elpased: 1.616
batch start
#iterations: 328
currently lose_sum: 496.8290155827999
time_elpased: 1.612
batch start
#iterations: 329
currently lose_sum: 495.8424727320671
time_elpased: 1.615
batch start
#iterations: 330
currently lose_sum: 498.6569829285145
time_elpased: 1.64
batch start
#iterations: 331
currently lose_sum: 498.0902156531811
time_elpased: 1.602
batch start
#iterations: 332
currently lose_sum: 496.92046108841896
time_elpased: 1.619
batch start
#iterations: 333
currently lose_sum: 494.929726600647
time_elpased: 1.599
batch start
#iterations: 334
currently lose_sum: 493.8025248646736
time_elpased: 1.638
batch start
#iterations: 335
currently lose_sum: 494.67152243852615
time_elpased: 1.606
batch start
#iterations: 336
currently lose_sum: 496.9256013035774
time_elpased: 1.613
batch start
#iterations: 337
currently lose_sum: 495.13023287057877
time_elpased: 1.608
batch start
#iterations: 338
currently lose_sum: 496.18150144815445
time_elpased: 1.606
batch start
#iterations: 339
currently lose_sum: 496.75012677907944
time_elpased: 1.611
start validation test
0.626958762887
0.896383186706
0.284488107549
0.431902033127
0
validation finish
batch start
#iterations: 340
currently lose_sum: 496.97719356417656
time_elpased: 1.603
batch start
#iterations: 341
currently lose_sum: 496.0430699288845
time_elpased: 1.626
batch start
#iterations: 342
currently lose_sum: 497.3025358915329
time_elpased: 1.614
batch start
#iterations: 343
currently lose_sum: 497.55315750837326
time_elpased: 1.611
batch start
#iterations: 344
currently lose_sum: 495.06905722618103
time_elpased: 1.616
batch start
#iterations: 345
currently lose_sum: 495.08944088220596
time_elpased: 1.607
batch start
#iterations: 346
currently lose_sum: 493.3701854646206
time_elpased: 1.65
batch start
#iterations: 347
currently lose_sum: 495.2292520701885
time_elpased: 1.612
batch start
#iterations: 348
currently lose_sum: 496.51309156417847
time_elpased: 1.611
batch start
#iterations: 349
currently lose_sum: 495.91733941435814
time_elpased: 1.601
batch start
#iterations: 350
currently lose_sum: 496.4997661411762
time_elpased: 1.638
batch start
#iterations: 351
currently lose_sum: 495.0521666109562
time_elpased: 1.615
batch start
#iterations: 352
currently lose_sum: 496.1551693081856
time_elpased: 1.611
batch start
#iterations: 353
currently lose_sum: 497.14907881617546
time_elpased: 1.608
batch start
#iterations: 354
currently lose_sum: 494.9348620772362
time_elpased: 1.624
batch start
#iterations: 355
currently lose_sum: 496.5914333462715
time_elpased: 1.618
batch start
#iterations: 356
currently lose_sum: 496.9805193543434
time_elpased: 1.618
batch start
#iterations: 357
currently lose_sum: 495.19243508577347
time_elpased: 1.6
batch start
#iterations: 358
currently lose_sum: 496.5863566994667
time_elpased: 1.624
batch start
#iterations: 359
currently lose_sum: 495.4323867559433
time_elpased: 1.618
start validation test
0.56881443299
0.919075144509
0.147983453981
0.254921172174
0
validation finish
batch start
#iterations: 360
currently lose_sum: 497.3591106235981
time_elpased: 1.622
batch start
#iterations: 361
currently lose_sum: 494.28147956728935
time_elpased: 1.609
batch start
#iterations: 362
currently lose_sum: 494.26759973168373
time_elpased: 1.619
batch start
#iterations: 363
currently lose_sum: 495.6358038187027
time_elpased: 1.625
batch start
#iterations: 364
currently lose_sum: 496.6393895447254
time_elpased: 1.604
batch start
#iterations: 365
currently lose_sum: 495.6139532029629
time_elpased: 1.601
batch start
#iterations: 366
currently lose_sum: 495.8355637192726
time_elpased: 1.616
batch start
#iterations: 367
currently lose_sum: 497.6536636054516
time_elpased: 1.622
batch start
#iterations: 368
currently lose_sum: 492.9907745420933
time_elpased: 1.611
batch start
#iterations: 369
currently lose_sum: 495.6109162569046
time_elpased: 1.628
batch start
#iterations: 370
currently lose_sum: 494.5499286055565
time_elpased: 1.621
batch start
#iterations: 371
currently lose_sum: 496.2081468105316
time_elpased: 1.611
batch start
#iterations: 372
currently lose_sum: 495.7655919790268
time_elpased: 1.62
batch start
#iterations: 373
currently lose_sum: 495.9630564749241
time_elpased: 1.623
batch start
#iterations: 374
currently lose_sum: 497.07862108945847
time_elpased: 1.616
batch start
#iterations: 375
currently lose_sum: 497.1107679307461
time_elpased: 1.611
batch start
#iterations: 376
currently lose_sum: 497.0643538236618
time_elpased: 1.618
batch start
#iterations: 377
currently lose_sum: 494.72587633132935
time_elpased: 1.626
batch start
#iterations: 378
currently lose_sum: 496.1054470539093
time_elpased: 1.618
batch start
#iterations: 379
currently lose_sum: 493.7469090819359
time_elpased: 1.626
start validation test
0.590515463918
0.906691800189
0.198965873837
0.326322930801
0
validation finish
batch start
#iterations: 380
currently lose_sum: 496.5798045694828
time_elpased: 1.622
batch start
#iterations: 381
currently lose_sum: 495.0858878791332
time_elpased: 1.614
batch start
#iterations: 382
currently lose_sum: 496.8577823638916
time_elpased: 1.614
batch start
#iterations: 383
currently lose_sum: 493.6017581522465
time_elpased: 1.616
batch start
#iterations: 384
currently lose_sum: 493.6322101354599
time_elpased: 1.615
batch start
#iterations: 385
currently lose_sum: 494.03458911180496
time_elpased: 1.605
batch start
#iterations: 386
currently lose_sum: 495.0547999739647
time_elpased: 1.628
batch start
#iterations: 387
currently lose_sum: 494.78301975131035
time_elpased: 1.606
batch start
#iterations: 388
currently lose_sum: 495.83656617999077
time_elpased: 1.601
batch start
#iterations: 389
currently lose_sum: 495.8623278141022
time_elpased: 1.618
batch start
#iterations: 390
currently lose_sum: 496.4742591679096
time_elpased: 1.613
batch start
#iterations: 391
currently lose_sum: 495.71612679958344
time_elpased: 1.616
batch start
#iterations: 392
currently lose_sum: 495.206184476614
time_elpased: 1.607
batch start
#iterations: 393
currently lose_sum: 494.2419246137142
time_elpased: 1.625
batch start
#iterations: 394
currently lose_sum: 496.0655282139778
time_elpased: 1.611
batch start
#iterations: 395
currently lose_sum: 494.1613045334816
time_elpased: 1.606
batch start
#iterations: 396
currently lose_sum: 496.5569324195385
time_elpased: 1.613
batch start
#iterations: 397
currently lose_sum: 495.63720297813416
time_elpased: 1.608
batch start
#iterations: 398
currently lose_sum: 495.6311031281948
time_elpased: 1.613
batch start
#iterations: 399
currently lose_sum: 494.4467423260212
time_elpased: 1.617
start validation test
0.584896907216
0.908539666498
0.185935884178
0.30869602541
0
validation finish
acc: 0.734
pre: 0.885
rec: 0.544
F1: 0.674
auc: 0.000
