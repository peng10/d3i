start to construct graph
graph construct over
58300
epochs start
batch start
#iterations: 0
currently lose_sum: 402.25072956085205
time_elpased: 1.419
batch start
#iterations: 1
currently lose_sum: 397.4373290538788
time_elpased: 1.411
batch start
#iterations: 2
currently lose_sum: 394.1228546500206
time_elpased: 1.396
batch start
#iterations: 3
currently lose_sum: 392.3942640423775
time_elpased: 1.408
batch start
#iterations: 4
currently lose_sum: 390.7790457010269
time_elpased: 1.413
batch start
#iterations: 5
currently lose_sum: 390.41234707832336
time_elpased: 1.387
batch start
#iterations: 6
currently lose_sum: 388.1109232902527
time_elpased: 1.407
batch start
#iterations: 7
currently lose_sum: 388.06556099653244
time_elpased: 1.376
batch start
#iterations: 8
currently lose_sum: 388.28802305459976
time_elpased: 1.402
batch start
#iterations: 9
currently lose_sum: 386.82161635160446
time_elpased: 1.425
batch start
#iterations: 10
currently lose_sum: 384.99393540620804
time_elpased: 1.409
batch start
#iterations: 11
currently lose_sum: 385.1336789727211
time_elpased: 1.4
batch start
#iterations: 12
currently lose_sum: 385.04072403907776
time_elpased: 1.433
batch start
#iterations: 13
currently lose_sum: 384.1761100292206
time_elpased: 1.398
batch start
#iterations: 14
currently lose_sum: 383.5236101746559
time_elpased: 1.403
batch start
#iterations: 15
currently lose_sum: 383.7899689078331
time_elpased: 1.415
batch start
#iterations: 16
currently lose_sum: 383.92093098163605
time_elpased: 1.41
batch start
#iterations: 17
currently lose_sum: 384.0976728796959
time_elpased: 1.41
batch start
#iterations: 18
currently lose_sum: 382.14202827215195
time_elpased: 1.407
batch start
#iterations: 19
currently lose_sum: 382.4459066390991
time_elpased: 1.408
start validation test
0.65618556701
0.869038607116
0.358936946326
0.508039533855
0
validation finish
batch start
#iterations: 20
currently lose_sum: 382.2397652864456
time_elpased: 1.415
batch start
#iterations: 21
currently lose_sum: 381.8933306336403
time_elpased: 1.41
batch start
#iterations: 22
currently lose_sum: 382.44351774454117
time_elpased: 1.414
batch start
#iterations: 23
currently lose_sum: 382.7945553660393
time_elpased: 1.415
batch start
#iterations: 24
currently lose_sum: 382.0705955028534
time_elpased: 1.405
batch start
#iterations: 25
currently lose_sum: 382.0361302495003
time_elpased: 1.412
batch start
#iterations: 26
currently lose_sum: 380.5783366560936
time_elpased: 1.417
batch start
#iterations: 27
currently lose_sum: 381.70041459798813
time_elpased: 1.413
batch start
#iterations: 28
currently lose_sum: 381.0804135799408
time_elpased: 1.413
batch start
#iterations: 29
currently lose_sum: 382.0969150066376
time_elpased: 1.408
batch start
#iterations: 30
currently lose_sum: 382.85969138145447
time_elpased: 1.41
batch start
#iterations: 31
currently lose_sum: 381.87117594480515
time_elpased: 1.42
batch start
#iterations: 32
currently lose_sum: 380.00559788942337
time_elpased: 1.405
batch start
#iterations: 33
currently lose_sum: 382.15004378557205
time_elpased: 1.411
batch start
#iterations: 34
currently lose_sum: 380.64697366952896
time_elpased: 1.413
batch start
#iterations: 35
currently lose_sum: 381.1808925271034
time_elpased: 1.41
batch start
#iterations: 36
currently lose_sum: 382.6155763268471
time_elpased: 1.405
batch start
#iterations: 37
currently lose_sum: 381.03913044929504
time_elpased: 1.41
batch start
#iterations: 38
currently lose_sum: 382.15058875083923
time_elpased: 1.402
batch start
#iterations: 39
currently lose_sum: 381.1450729370117
time_elpased: 1.402
start validation test
0.710206185567
0.859157476044
0.495257946847
0.628322094407
0
validation finish
batch start
#iterations: 40
currently lose_sum: 381.5788967013359
time_elpased: 1.416
batch start
#iterations: 41
currently lose_sum: 381.33984619379044
time_elpased: 1.403
batch start
#iterations: 42
currently lose_sum: 380.4122979044914
time_elpased: 1.416
batch start
#iterations: 43
currently lose_sum: 380.29779547452927
time_elpased: 1.402
batch start
#iterations: 44
currently lose_sum: 382.09159302711487
time_elpased: 1.399
batch start
#iterations: 45
currently lose_sum: 381.7399682402611
time_elpased: 1.41
batch start
#iterations: 46
currently lose_sum: 379.6900253891945
time_elpased: 1.414
batch start
#iterations: 47
currently lose_sum: 380.91373574733734
time_elpased: 1.414
batch start
#iterations: 48
currently lose_sum: 381.6073217391968
time_elpased: 1.418
batch start
#iterations: 49
currently lose_sum: 381.8047726750374
time_elpased: 1.423
batch start
#iterations: 50
currently lose_sum: 380.5785705447197
time_elpased: 1.401
batch start
#iterations: 51
currently lose_sum: 379.75253117084503
time_elpased: 1.423
batch start
#iterations: 52
currently lose_sum: 380.1551773548126
time_elpased: 1.417
batch start
#iterations: 53
currently lose_sum: 379.9272864460945
time_elpased: 1.416
batch start
#iterations: 54
currently lose_sum: 380.1002027988434
time_elpased: 1.433
batch start
#iterations: 55
currently lose_sum: 380.82468420267105
time_elpased: 1.411
batch start
#iterations: 56
currently lose_sum: 379.6517441868782
time_elpased: 1.419
batch start
#iterations: 57
currently lose_sum: 381.6462564468384
time_elpased: 1.43
batch start
#iterations: 58
currently lose_sum: 381.3711276650429
time_elpased: 1.419
batch start
#iterations: 59
currently lose_sum: 380.4756671190262
time_elpased: 1.427
start validation test
0.754072164948
0.820574162679
0.643460135487
0.721303814475
0
validation finish
batch start
#iterations: 60
currently lose_sum: 381.42713713645935
time_elpased: 1.416
batch start
#iterations: 61
currently lose_sum: 379.89663821458817
time_elpased: 1.423
batch start
#iterations: 62
currently lose_sum: 380.3361625671387
time_elpased: 1.435
batch start
#iterations: 63
currently lose_sum: 381.3639336824417
time_elpased: 1.424
batch start
#iterations: 64
currently lose_sum: 379.9611114859581
time_elpased: 1.424
batch start
#iterations: 65
currently lose_sum: 379.2524079680443
time_elpased: 1.43
batch start
#iterations: 66
currently lose_sum: 381.75015074014664
time_elpased: 1.412
batch start
#iterations: 67
currently lose_sum: 380.82441318035126
time_elpased: 1.424
batch start
#iterations: 68
currently lose_sum: 380.45474177598953
time_elpased: 1.434
batch start
#iterations: 69
currently lose_sum: 379.5298282504082
time_elpased: 1.42
batch start
#iterations: 70
currently lose_sum: 379.7113837003708
time_elpased: 1.437
batch start
#iterations: 71
currently lose_sum: 378.87700659036636
time_elpased: 1.435
batch start
#iterations: 72
currently lose_sum: 380.5597605705261
time_elpased: 1.432
batch start
#iterations: 73
currently lose_sum: 381.4765250682831
time_elpased: 1.43
batch start
#iterations: 74
currently lose_sum: 380.8317760229111
time_elpased: 1.433
batch start
#iterations: 75
currently lose_sum: 379.80282682180405
time_elpased: 1.431
batch start
#iterations: 76
currently lose_sum: 380.08738750219345
time_elpased: 1.445
batch start
#iterations: 77
currently lose_sum: 379.7686285376549
time_elpased: 1.421
batch start
#iterations: 78
currently lose_sum: 379.1206463575363
time_elpased: 1.435
batch start
#iterations: 79
currently lose_sum: 379.95384126901627
time_elpased: 1.44
start validation test
0.757628865979
0.788060755917
0.697550807712
0.740048651039
0
validation finish
batch start
#iterations: 80
currently lose_sum: 380.5699989795685
time_elpased: 1.416
batch start
#iterations: 81
currently lose_sum: 380.72385746240616
time_elpased: 1.437
batch start
#iterations: 82
currently lose_sum: 379.83642172813416
time_elpased: 1.438
batch start
#iterations: 83
currently lose_sum: 380.116685628891
time_elpased: 1.429
batch start
#iterations: 84
currently lose_sum: 379.37848430871964
time_elpased: 1.436
batch start
#iterations: 85
currently lose_sum: 379.85382229089737
time_elpased: 1.427
batch start
#iterations: 86
currently lose_sum: 379.7898779511452
time_elpased: 1.432
batch start
#iterations: 87
currently lose_sum: 379.92645877599716
time_elpased: 1.447
batch start
#iterations: 88
currently lose_sum: 379.94219183921814
time_elpased: 1.438
batch start
#iterations: 89
currently lose_sum: 379.44555217027664
time_elpased: 1.438
batch start
#iterations: 90
currently lose_sum: 379.06645357608795
time_elpased: 1.436
batch start
#iterations: 91
currently lose_sum: 380.1765366792679
time_elpased: 1.421
batch start
#iterations: 92
currently lose_sum: 380.0222580432892
time_elpased: 1.439
batch start
#iterations: 93
currently lose_sum: 380.77744859457016
time_elpased: 1.427
batch start
#iterations: 94
currently lose_sum: 379.20010352134705
time_elpased: 1.421
batch start
#iterations: 95
currently lose_sum: 380.1006298661232
time_elpased: 1.439
batch start
#iterations: 96
currently lose_sum: 379.69678968191147
time_elpased: 1.435
batch start
#iterations: 97
currently lose_sum: 378.72453796863556
time_elpased: 1.429
batch start
#iterations: 98
currently lose_sum: 379.54821783304214
time_elpased: 1.443
batch start
#iterations: 99
currently lose_sum: 379.70565271377563
time_elpased: 1.441
start validation test
0.759639175258
0.786145277327
0.706096925482
0.743974084445
0
validation finish
batch start
#iterations: 100
currently lose_sum: 379.96593421697617
time_elpased: 1.434
batch start
#iterations: 101
currently lose_sum: 379.9859575033188
time_elpased: 1.428
batch start
#iterations: 102
currently lose_sum: 378.944180727005
time_elpased: 1.411
batch start
#iterations: 103
currently lose_sum: 379.6125268936157
time_elpased: 1.417
batch start
#iterations: 104
currently lose_sum: 379.2755469083786
time_elpased: 1.428
batch start
#iterations: 105
currently lose_sum: 379.6294136047363
time_elpased: 1.423
batch start
#iterations: 106
currently lose_sum: 381.18805396556854
time_elpased: 1.437
batch start
#iterations: 107
currently lose_sum: 379.9418488740921
time_elpased: 1.43
batch start
#iterations: 108
currently lose_sum: 379.5331162214279
time_elpased: 1.438
batch start
#iterations: 109
currently lose_sum: 379.92658960819244
time_elpased: 1.449
batch start
#iterations: 110
currently lose_sum: 379.9987953901291
time_elpased: 1.42
batch start
#iterations: 111
currently lose_sum: 378.55546283721924
time_elpased: 1.42
batch start
#iterations: 112
currently lose_sum: 379.3517814874649
time_elpased: 1.449
batch start
#iterations: 113
currently lose_sum: 381.04821544885635
time_elpased: 1.427
batch start
#iterations: 114
currently lose_sum: 381.86128252744675
time_elpased: 1.438
batch start
#iterations: 115
currently lose_sum: 379.48826014995575
time_elpased: 1.437
batch start
#iterations: 116
currently lose_sum: 379.77056670188904
time_elpased: 1.438
batch start
#iterations: 117
currently lose_sum: 379.1199706196785
time_elpased: 1.433
batch start
#iterations: 118
currently lose_sum: 380.09583806991577
time_elpased: 1.446
batch start
#iterations: 119
currently lose_sum: 380.6456577181816
time_elpased: 1.437
start validation test
0.73706185567
0.857574793125
0.561646690985
0.678758108193
0
validation finish
batch start
#iterations: 120
currently lose_sum: 379.2788465619087
time_elpased: 1.448
batch start
#iterations: 121
currently lose_sum: 381.0881150364876
time_elpased: 1.427
batch start
#iterations: 122
currently lose_sum: 378.96206188201904
time_elpased: 1.421
batch start
#iterations: 123
currently lose_sum: 379.38266307115555
time_elpased: 1.436
batch start
#iterations: 124
currently lose_sum: 379.18752509355545
time_elpased: 1.418
batch start
#iterations: 125
currently lose_sum: 379.62620002031326
time_elpased: 1.422
batch start
#iterations: 126
currently lose_sum: 379.6051350235939
time_elpased: 1.434
batch start
#iterations: 127
currently lose_sum: 379.7132216691971
time_elpased: 1.426
batch start
#iterations: 128
currently lose_sum: 380.01002103090286
time_elpased: 1.432
batch start
#iterations: 129
currently lose_sum: 379.8344741463661
time_elpased: 1.415
batch start
#iterations: 130
currently lose_sum: 379.9212795495987
time_elpased: 1.411
batch start
#iterations: 131
currently lose_sum: 378.7069979906082
time_elpased: 1.425
batch start
#iterations: 132
currently lose_sum: 380.19825369119644
time_elpased: 1.43
batch start
#iterations: 133
currently lose_sum: 378.82463389635086
time_elpased: 1.422
batch start
#iterations: 134
currently lose_sum: 379.20980697870255
time_elpased: 1.429
batch start
#iterations: 135
currently lose_sum: 379.3553196191788
time_elpased: 1.422
batch start
#iterations: 136
currently lose_sum: 379.8884878754616
time_elpased: 1.422
batch start
#iterations: 137
currently lose_sum: 379.27424681186676
time_elpased: 1.426
batch start
#iterations: 138
currently lose_sum: 379.8347016572952
time_elpased: 1.417
batch start
#iterations: 139
currently lose_sum: 378.4828908443451
time_elpased: 1.417
start validation test
0.760206185567
0.820349967596
0.659614382491
0.73125361063
0
validation finish
batch start
#iterations: 140
currently lose_sum: 379.501360476017
time_elpased: 1.436
batch start
#iterations: 141
currently lose_sum: 379.09837448596954
time_elpased: 1.419
batch start
#iterations: 142
currently lose_sum: 379.5097934603691
time_elpased: 1.43
batch start
#iterations: 143
currently lose_sum: 379.9579328894615
time_elpased: 1.426
batch start
#iterations: 144
currently lose_sum: 379.2271566390991
time_elpased: 1.431
batch start
#iterations: 145
currently lose_sum: 379.6802659034729
time_elpased: 1.436
batch start
#iterations: 146
currently lose_sum: 378.10586726665497
time_elpased: 1.416
batch start
#iterations: 147
currently lose_sum: 379.12921667099
time_elpased: 1.417
batch start
#iterations: 148
currently lose_sum: 379.11512064933777
time_elpased: 1.434
batch start
#iterations: 149
currently lose_sum: 379.0033077597618
time_elpased: 1.424
batch start
#iterations: 150
currently lose_sum: 379.65609711408615
time_elpased: 1.431
batch start
#iterations: 151
currently lose_sum: 379.20467150211334
time_elpased: 1.425
batch start
#iterations: 152
currently lose_sum: 380.36850118637085
time_elpased: 1.423
batch start
#iterations: 153
currently lose_sum: 380.034076154232
time_elpased: 1.433
batch start
#iterations: 154
currently lose_sum: 378.76505649089813
time_elpased: 1.428
batch start
#iterations: 155
currently lose_sum: 379.6154775619507
time_elpased: 1.419
batch start
#iterations: 156
currently lose_sum: 377.756349503994
time_elpased: 1.445
batch start
#iterations: 157
currently lose_sum: 379.56655180454254
time_elpased: 1.425
batch start
#iterations: 158
currently lose_sum: 378.3541212081909
time_elpased: 1.427
batch start
#iterations: 159
currently lose_sum: 379.04615956544876
time_elpased: 1.45
start validation test
0.760412371134
0.807076350093
0.677540385618
0.736657223796
0
validation finish
batch start
#iterations: 160
currently lose_sum: 378.92033392190933
time_elpased: 1.438
batch start
#iterations: 161
currently lose_sum: 379.73218834400177
time_elpased: 1.439
batch start
#iterations: 162
currently lose_sum: 378.1902877688408
time_elpased: 1.429
batch start
#iterations: 163
currently lose_sum: 379.5506149530411
time_elpased: 1.406
batch start
#iterations: 164
currently lose_sum: 378.79523527622223
time_elpased: 1.42
batch start
#iterations: 165
currently lose_sum: 379.3597598671913
time_elpased: 1.414
batch start
#iterations: 166
currently lose_sum: 379.7718443274498
time_elpased: 1.398
batch start
#iterations: 167
currently lose_sum: 378.34811639785767
time_elpased: 1.406
batch start
#iterations: 168
currently lose_sum: 378.323807656765
time_elpased: 1.407
batch start
#iterations: 169
currently lose_sum: 379.3261347413063
time_elpased: 1.392
batch start
#iterations: 170
currently lose_sum: 379.6379886865616
time_elpased: 1.416
batch start
#iterations: 171
currently lose_sum: 379.71923100948334
time_elpased: 1.404
batch start
#iterations: 172
currently lose_sum: 379.97370463609695
time_elpased: 1.406
batch start
#iterations: 173
currently lose_sum: 379.07814568281174
time_elpased: 1.426
batch start
#iterations: 174
currently lose_sum: 379.7232024669647
time_elpased: 1.399
batch start
#iterations: 175
currently lose_sum: 378.8554929494858
time_elpased: 1.414
batch start
#iterations: 176
currently lose_sum: 378.89104521274567
time_elpased: 1.416
batch start
#iterations: 177
currently lose_sum: 379.76739805936813
time_elpased: 1.414
batch start
#iterations: 178
currently lose_sum: 380.21356439590454
time_elpased: 1.415
batch start
#iterations: 179
currently lose_sum: 379.50732374191284
time_elpased: 1.414
start validation test
0.750463917526
0.84270472895
0.60917144346
0.70715625189
0
validation finish
batch start
#iterations: 180
currently lose_sum: 379.43137270212173
time_elpased: 1.413
batch start
#iterations: 181
currently lose_sum: 377.7063913345337
time_elpased: 1.432
batch start
#iterations: 182
currently lose_sum: 379.7315174341202
time_elpased: 1.396
batch start
#iterations: 183
currently lose_sum: 379.79830026626587
time_elpased: 1.386
batch start
#iterations: 184
currently lose_sum: 379.64723140001297
time_elpased: 1.43
batch start
#iterations: 185
currently lose_sum: 378.76896494627
time_elpased: 1.378
batch start
#iterations: 186
currently lose_sum: 380.37410467863083
time_elpased: 1.413
batch start
#iterations: 187
currently lose_sum: 379.37763172388077
time_elpased: 1.412
batch start
#iterations: 188
currently lose_sum: 378.92037296295166
time_elpased: 1.416
batch start
#iterations: 189
currently lose_sum: 379.5459517836571
time_elpased: 1.421
batch start
#iterations: 190
currently lose_sum: 379.22713351249695
time_elpased: 1.412
batch start
#iterations: 191
currently lose_sum: 379.1787962317467
time_elpased: 1.393
batch start
#iterations: 192
currently lose_sum: 380.320594727993
time_elpased: 1.418
batch start
#iterations: 193
currently lose_sum: 378.8866760134697
time_elpased: 1.414
batch start
#iterations: 194
currently lose_sum: 379.7517905831337
time_elpased: 1.403
batch start
#iterations: 195
currently lose_sum: 378.1824404001236
time_elpased: 1.417
batch start
#iterations: 196
currently lose_sum: 380.30854880809784
time_elpased: 1.402
batch start
#iterations: 197
currently lose_sum: 379.476855635643
time_elpased: 1.407
batch start
#iterations: 198
currently lose_sum: 379.06514382362366
time_elpased: 1.42
batch start
#iterations: 199
currently lose_sum: 378.34438133239746
time_elpased: 1.391
start validation test
0.740412371134
0.838757616288
0.58822303283
0.691497182063
0
validation finish
batch start
#iterations: 200
currently lose_sum: 379.351135969162
time_elpased: 1.394
batch start
#iterations: 201
currently lose_sum: 378.91676265001297
time_elpased: 1.417
batch start
#iterations: 202
currently lose_sum: 379.51766860485077
time_elpased: 1.396
batch start
#iterations: 203
currently lose_sum: 378.8991092443466
time_elpased: 1.41
batch start
#iterations: 204
currently lose_sum: 379.83657455444336
time_elpased: 1.406
batch start
#iterations: 205
currently lose_sum: 378.49893420934677
time_elpased: 1.391
batch start
#iterations: 206
currently lose_sum: 379.04585683345795
time_elpased: 1.413
batch start
#iterations: 207
currently lose_sum: 378.93622601032257
time_elpased: 1.436
batch start
#iterations: 208
currently lose_sum: 378.93319070339203
time_elpased: 1.443
batch start
#iterations: 209
currently lose_sum: 379.33035534620285
time_elpased: 1.426
batch start
#iterations: 210
currently lose_sum: 378.5472116470337
time_elpased: 1.422
batch start
#iterations: 211
currently lose_sum: 380.1824080944061
time_elpased: 1.414
batch start
#iterations: 212
currently lose_sum: 379.3845345377922
time_elpased: 1.429
batch start
#iterations: 213
currently lose_sum: 378.5877124071121
time_elpased: 1.416
batch start
#iterations: 214
currently lose_sum: 380.1401873230934
time_elpased: 1.413
batch start
#iterations: 215
currently lose_sum: 379.4073631763458
time_elpased: 1.422
batch start
#iterations: 216
currently lose_sum: 379.9975212216377
time_elpased: 1.417
batch start
#iterations: 217
currently lose_sum: 378.4878339767456
time_elpased: 1.425
batch start
#iterations: 218
currently lose_sum: 378.75917887687683
time_elpased: 1.411
batch start
#iterations: 219
currently lose_sum: 380.2976053953171
time_elpased: 1.408
start validation test
0.747680412371
0.837159253945
0.608129233976
0.70449743435
0
validation finish
batch start
#iterations: 220
currently lose_sum: 379.18807232379913
time_elpased: 1.418
batch start
#iterations: 221
currently lose_sum: 379.01908671855927
time_elpased: 1.409
batch start
#iterations: 222
currently lose_sum: 380.09159034490585
time_elpased: 1.407
batch start
#iterations: 223
currently lose_sum: 379.0478528738022
time_elpased: 1.423
batch start
#iterations: 224
currently lose_sum: 379.0555225610733
time_elpased: 1.406
batch start
#iterations: 225
currently lose_sum: 378.34132343530655
time_elpased: 1.403
batch start
#iterations: 226
currently lose_sum: 378.78048408031464
time_elpased: 1.413
batch start
#iterations: 227
currently lose_sum: 379.8708294034004
time_elpased: 1.409
batch start
#iterations: 228
currently lose_sum: 379.2399293780327
time_elpased: 1.4
batch start
#iterations: 229
currently lose_sum: 378.05956614017487
time_elpased: 1.404
batch start
#iterations: 230
currently lose_sum: 379.3262693285942
time_elpased: 1.41
batch start
#iterations: 231
currently lose_sum: 379.4943190217018
time_elpased: 1.415
batch start
#iterations: 232
currently lose_sum: 379.24021315574646
time_elpased: 1.415
batch start
#iterations: 233
currently lose_sum: 379.6423857808113
time_elpased: 1.412
batch start
#iterations: 234
currently lose_sum: 379.83539056777954
time_elpased: 1.41
batch start
#iterations: 235
currently lose_sum: 378.493017911911
time_elpased: 1.407
batch start
#iterations: 236
currently lose_sum: 379.6723977327347
time_elpased: 1.393
batch start
#iterations: 237
currently lose_sum: 379.85461258888245
time_elpased: 1.404
batch start
#iterations: 238
currently lose_sum: 379.49764907360077
time_elpased: 1.409
batch start
#iterations: 239
currently lose_sum: 379.1011675000191
time_elpased: 1.398
start validation test
0.750824742268
0.839925746109
0.613027618551
0.708760091577
0
validation finish
batch start
#iterations: 240
currently lose_sum: 379.4413276910782
time_elpased: 1.409
batch start
#iterations: 241
currently lose_sum: 379.02881211042404
time_elpased: 1.404
batch start
#iterations: 242
currently lose_sum: 380.44808584451675
time_elpased: 1.395
batch start
#iterations: 243
currently lose_sum: 378.46215999126434
time_elpased: 1.411
batch start
#iterations: 244
currently lose_sum: 379.8161955475807
time_elpased: 1.406
batch start
#iterations: 245
currently lose_sum: 379.5046241879463
time_elpased: 1.422
batch start
#iterations: 246
currently lose_sum: 378.98770278692245
time_elpased: 1.409
batch start
#iterations: 247
currently lose_sum: 377.50417506694794
time_elpased: 1.396
batch start
#iterations: 248
currently lose_sum: 378.96302157640457
time_elpased: 1.403
batch start
#iterations: 249
currently lose_sum: 378.38134974241257
time_elpased: 1.396
batch start
#iterations: 250
currently lose_sum: 379.16486942768097
time_elpased: 1.401
batch start
#iterations: 251
currently lose_sum: 379.34875440597534
time_elpased: 1.412
batch start
#iterations: 252
currently lose_sum: 377.91336673498154
time_elpased: 1.412
batch start
#iterations: 253
currently lose_sum: 379.6619083881378
time_elpased: 1.406
batch start
#iterations: 254
currently lose_sum: 378.4831228852272
time_elpased: 1.421
batch start
#iterations: 255
currently lose_sum: 379.25165885686874
time_elpased: 1.415
batch start
#iterations: 256
currently lose_sum: 379.01449567079544
time_elpased: 1.403
batch start
#iterations: 257
currently lose_sum: 379.3114706873894
time_elpased: 1.412
batch start
#iterations: 258
currently lose_sum: 378.4940865635872
time_elpased: 1.392
batch start
#iterations: 259
currently lose_sum: 379.66350543498993
time_elpased: 1.398
start validation test
0.755463917526
0.836547800749
0.628348097968
0.717652660398
0
validation finish
batch start
#iterations: 260
currently lose_sum: 379.48176777362823
time_elpased: 1.407
batch start
#iterations: 261
currently lose_sum: 378.7636476755142
time_elpased: 1.401
batch start
#iterations: 262
currently lose_sum: 377.7139983177185
time_elpased: 1.404
batch start
#iterations: 263
currently lose_sum: 378.8644242286682
time_elpased: 1.403
batch start
#iterations: 264
currently lose_sum: 380.4736722111702
time_elpased: 1.393
batch start
#iterations: 265
currently lose_sum: 379.2609165906906
time_elpased: 1.407
batch start
#iterations: 266
currently lose_sum: 379.31670838594437
time_elpased: 1.413
batch start
#iterations: 267
currently lose_sum: 379.3214411139488
time_elpased: 1.406
batch start
#iterations: 268
currently lose_sum: 380.0891320705414
time_elpased: 1.419
batch start
#iterations: 269
currently lose_sum: 379.6077786684036
time_elpased: 1.408
batch start
#iterations: 270
currently lose_sum: 379.10176396369934
time_elpased: 1.403
batch start
#iterations: 271
currently lose_sum: 378.90927666425705
time_elpased: 1.412
batch start
#iterations: 272
currently lose_sum: 378.5449222922325
time_elpased: 1.405
batch start
#iterations: 273
currently lose_sum: 378.87894409894943
time_elpased: 1.404
batch start
#iterations: 274
currently lose_sum: 379.1400561928749
time_elpased: 1.411
batch start
#iterations: 275
currently lose_sum: 378.672366976738
time_elpased: 1.42
batch start
#iterations: 276
currently lose_sum: 380.147918343544
time_elpased: 1.415
batch start
#iterations: 277
currently lose_sum: 379.26758229732513
time_elpased: 1.411
batch start
#iterations: 278
currently lose_sum: 380.2378326058388
time_elpased: 1.411
batch start
#iterations: 279
currently lose_sum: 378.46953505277634
time_elpased: 1.408
start validation test
0.744587628866
0.848557692308
0.588639916623
0.695095686419
0
validation finish
batch start
#iterations: 280
currently lose_sum: 379.2464966773987
time_elpased: 1.419
batch start
#iterations: 281
currently lose_sum: 377.7825739979744
time_elpased: 1.408
batch start
#iterations: 282
currently lose_sum: 379.4172773361206
time_elpased: 1.415
batch start
#iterations: 283
currently lose_sum: 379.6794892549515
time_elpased: 1.408
batch start
#iterations: 284
currently lose_sum: 379.2949057817459
time_elpased: 1.416
batch start
#iterations: 285
currently lose_sum: 379.3415311574936
time_elpased: 1.428
batch start
#iterations: 286
currently lose_sum: 378.87882775068283
time_elpased: 1.408
batch start
#iterations: 287
currently lose_sum: 379.4742862582207
time_elpased: 1.408
batch start
#iterations: 288
currently lose_sum: 378.44795775413513
time_elpased: 1.413
batch start
#iterations: 289
currently lose_sum: 376.88786989450455
time_elpased: 1.401
batch start
#iterations: 290
currently lose_sum: 378.64400774240494
time_elpased: 1.405
batch start
#iterations: 291
currently lose_sum: 379.3343218564987
time_elpased: 1.412
batch start
#iterations: 292
currently lose_sum: 379.0704085826874
time_elpased: 1.406
batch start
#iterations: 293
currently lose_sum: 379.44608533382416
time_elpased: 1.413
batch start
#iterations: 294
currently lose_sum: 378.91067111492157
time_elpased: 1.409
batch start
#iterations: 295
currently lose_sum: 380.1557264328003
time_elpased: 1.406
batch start
#iterations: 296
currently lose_sum: 378.58559215068817
time_elpased: 1.411
batch start
#iterations: 297
currently lose_sum: 380.16556775569916
time_elpased: 1.411
batch start
#iterations: 298
currently lose_sum: 379.4097141623497
time_elpased: 1.395
batch start
#iterations: 299
currently lose_sum: 378.2318531870842
time_elpased: 1.405
start validation test
0.755515463918
0.831783369803
0.633871808233
0.719465310227
0
validation finish
batch start
#iterations: 300
currently lose_sum: 378.5649348497391
time_elpased: 1.396
batch start
#iterations: 301
currently lose_sum: 378.90505933761597
time_elpased: 1.403
batch start
#iterations: 302
currently lose_sum: 379.00063449144363
time_elpased: 1.41
batch start
#iterations: 303
currently lose_sum: 380.21300625801086
time_elpased: 1.398
batch start
#iterations: 304
currently lose_sum: 379.7635762691498
time_elpased: 1.41
batch start
#iterations: 305
currently lose_sum: 378.2429391145706
time_elpased: 1.416
batch start
#iterations: 306
currently lose_sum: 379.1323491334915
time_elpased: 1.397
batch start
#iterations: 307
currently lose_sum: 378.6248462796211
time_elpased: 1.404
batch start
#iterations: 308
currently lose_sum: 378.4055004119873
time_elpased: 1.4
batch start
#iterations: 309
currently lose_sum: 378.1453779935837
time_elpased: 1.399
batch start
#iterations: 310
currently lose_sum: 379.5691772699356
time_elpased: 1.401
batch start
#iterations: 311
currently lose_sum: 378.8301613330841
time_elpased: 1.397
batch start
#iterations: 312
currently lose_sum: 377.98726111650467
time_elpased: 1.404
batch start
#iterations: 313
currently lose_sum: 379.3004559278488
time_elpased: 1.43
batch start
#iterations: 314
currently lose_sum: 378.2960546016693
time_elpased: 1.421
batch start
#iterations: 315
currently lose_sum: 378.5944713950157
time_elpased: 1.416
batch start
#iterations: 316
currently lose_sum: 378.07154589891434
time_elpased: 1.423
batch start
#iterations: 317
currently lose_sum: 379.52632862329483
time_elpased: 1.42
batch start
#iterations: 318
currently lose_sum: 378.22476983070374
time_elpased: 1.411
batch start
#iterations: 319
currently lose_sum: 379.53491830825806
time_elpased: 1.413
start validation test
0.757835051546
0.833719503884
0.637519541428
0.722537207654
0
validation finish
batch start
#iterations: 320
currently lose_sum: 377.8164907693863
time_elpased: 1.409
batch start
#iterations: 321
currently lose_sum: 379.6397063136101
time_elpased: 1.423
batch start
#iterations: 322
currently lose_sum: 378.0569133758545
time_elpased: 1.409
batch start
#iterations: 323
currently lose_sum: 377.97720116376877
time_elpased: 1.401
batch start
#iterations: 324
currently lose_sum: 377.75507205724716
time_elpased: 1.423
batch start
#iterations: 325
currently lose_sum: 378.8705654144287
time_elpased: 1.43
batch start
#iterations: 326
currently lose_sum: 380.43624609708786
time_elpased: 1.416
batch start
#iterations: 327
currently lose_sum: 378.59936225414276
time_elpased: 1.425
batch start
#iterations: 328
currently lose_sum: 380.26743096113205
time_elpased: 1.417
batch start
#iterations: 329
currently lose_sum: 379.19949519634247
time_elpased: 1.409
batch start
#iterations: 330
currently lose_sum: 378.9734891653061
time_elpased: 1.416
batch start
#iterations: 331
currently lose_sum: 379.70061725378036
time_elpased: 1.401
batch start
#iterations: 332
currently lose_sum: 379.8214445710182
time_elpased: 1.41
batch start
#iterations: 333
currently lose_sum: 378.7116191983223
time_elpased: 1.425
batch start
#iterations: 334
currently lose_sum: 378.8905524611473
time_elpased: 1.413
batch start
#iterations: 335
currently lose_sum: 378.9381223320961
time_elpased: 1.414
batch start
#iterations: 336
currently lose_sum: 377.707938849926
time_elpased: 1.423
batch start
#iterations: 337
currently lose_sum: 379.23670929670334
time_elpased: 1.408
batch start
#iterations: 338
currently lose_sum: 379.9570867419243
time_elpased: 1.416
batch start
#iterations: 339
currently lose_sum: 378.6302142739296
time_elpased: 1.407
start validation test
0.750360824742
0.847673397717
0.603751954143
0.70521638566
0
validation finish
batch start
#iterations: 340
currently lose_sum: 379.48367965221405
time_elpased: 1.41
batch start
#iterations: 341
currently lose_sum: 379.5931230187416
time_elpased: 1.424
batch start
#iterations: 342
currently lose_sum: 379.1759008169174
time_elpased: 1.403
batch start
#iterations: 343
currently lose_sum: 378.1428937315941
time_elpased: 1.42
batch start
#iterations: 344
currently lose_sum: 379.56794983148575
time_elpased: 1.409
batch start
#iterations: 345
currently lose_sum: 379.14207822084427
time_elpased: 1.404
batch start
#iterations: 346
currently lose_sum: 378.61752021312714
time_elpased: 1.41
batch start
#iterations: 347
currently lose_sum: 378.0165708065033
time_elpased: 1.422
batch start
#iterations: 348
currently lose_sum: 377.9561136960983
time_elpased: 1.402
batch start
#iterations: 349
currently lose_sum: 377.8929862976074
time_elpased: 1.405
batch start
#iterations: 350
currently lose_sum: 379.7387709617615
time_elpased: 1.416
batch start
#iterations: 351
currently lose_sum: 377.83547270298004
time_elpased: 1.414
batch start
#iterations: 352
currently lose_sum: 379.85713136196136
time_elpased: 1.414
batch start
#iterations: 353
currently lose_sum: 379.4833577275276
time_elpased: 1.411
batch start
#iterations: 354
currently lose_sum: 379.71752309799194
time_elpased: 1.408
batch start
#iterations: 355
currently lose_sum: 379.9457718729973
time_elpased: 1.42
batch start
#iterations: 356
currently lose_sum: 378.5086477994919
time_elpased: 1.41
batch start
#iterations: 357
currently lose_sum: 378.0961250066757
time_elpased: 1.404
batch start
#iterations: 358
currently lose_sum: 377.87612599134445
time_elpased: 1.41
batch start
#iterations: 359
currently lose_sum: 379.65621650218964
time_elpased: 1.398
start validation test
0.745927835052
0.842483852026
0.598124022929
0.699579447797
0
validation finish
batch start
#iterations: 360
currently lose_sum: 378.8206671476364
time_elpased: 1.403
batch start
#iterations: 361
currently lose_sum: 379.3929508328438
time_elpased: 1.411
batch start
#iterations: 362
currently lose_sum: 379.84491950273514
time_elpased: 1.416
batch start
#iterations: 363
currently lose_sum: 379.4801214337349
time_elpased: 1.421
batch start
#iterations: 364
currently lose_sum: 378.3466281890869
time_elpased: 1.43
batch start
#iterations: 365
currently lose_sum: 378.7752653956413
time_elpased: 1.409
batch start
#iterations: 366
currently lose_sum: 379.59993493556976
time_elpased: 1.423
batch start
#iterations: 367
currently lose_sum: 379.0268362760544
time_elpased: 1.407
batch start
#iterations: 368
currently lose_sum: 379.44589322805405
time_elpased: 1.404
batch start
#iterations: 369
currently lose_sum: 377.5553007721901
time_elpased: 1.41
batch start
#iterations: 370
currently lose_sum: 378.1706152558327
time_elpased: 1.395
batch start
#iterations: 371
currently lose_sum: 378.8711005449295
time_elpased: 1.403
batch start
#iterations: 372
currently lose_sum: 377.9177767634392
time_elpased: 1.419
batch start
#iterations: 373
currently lose_sum: 378.3489448428154
time_elpased: 1.403
batch start
#iterations: 374
currently lose_sum: 379.89725732803345
time_elpased: 1.402
batch start
#iterations: 375
currently lose_sum: 378.3437893986702
time_elpased: 1.405
batch start
#iterations: 376
currently lose_sum: 378.94099855422974
time_elpased: 1.395
batch start
#iterations: 377
currently lose_sum: 378.84062176942825
time_elpased: 1.408
batch start
#iterations: 378
currently lose_sum: 378.09707725048065
time_elpased: 1.418
batch start
#iterations: 379
currently lose_sum: 380.47926419973373
time_elpased: 1.42
start validation test
0.753402061856
0.841738883364
0.617509119333
0.712396296742
0
validation finish
batch start
#iterations: 380
currently lose_sum: 379.22808957099915
time_elpased: 1.435
batch start
#iterations: 381
currently lose_sum: 378.5700760483742
time_elpased: 1.426
batch start
#iterations: 382
currently lose_sum: 379.9881578683853
time_elpased: 1.406
batch start
#iterations: 383
currently lose_sum: 377.35034638643265
time_elpased: 1.422
batch start
#iterations: 384
currently lose_sum: 379.02516919374466
time_elpased: 1.407
batch start
#iterations: 385
currently lose_sum: 379.18324041366577
time_elpased: 1.417
batch start
#iterations: 386
currently lose_sum: 377.7149105668068
time_elpased: 1.421
batch start
#iterations: 387
currently lose_sum: 378.06230050325394
time_elpased: 1.416
batch start
#iterations: 388
currently lose_sum: 378.3787657022476
time_elpased: 1.431
batch start
#iterations: 389
currently lose_sum: 378.9733139872551
time_elpased: 1.42
batch start
#iterations: 390
currently lose_sum: 378.32949739694595
time_elpased: 1.414
batch start
#iterations: 391
currently lose_sum: 379.113868534565
time_elpased: 1.423
batch start
#iterations: 392
currently lose_sum: 379.143946826458
time_elpased: 1.427
batch start
#iterations: 393
currently lose_sum: 378.3953357934952
time_elpased: 1.422
batch start
#iterations: 394
currently lose_sum: 379.0360413789749
time_elpased: 1.441
batch start
#iterations: 395
currently lose_sum: 378.51927971839905
time_elpased: 1.423
batch start
#iterations: 396
currently lose_sum: 377.887059032917
time_elpased: 1.419
batch start
#iterations: 397
currently lose_sum: 379.8198264837265
time_elpased: 1.453
batch start
#iterations: 398
currently lose_sum: 378.74868965148926
time_elpased: 1.421
batch start
#iterations: 399
currently lose_sum: 379.15482610464096
time_elpased: 1.422
start validation test
0.756288659794
0.833859239951
0.63345492444
0.719971570718
0
validation finish
acc: 0.758
pre: 0.788
rec: 0.710
F1: 0.747
auc: 0.000
