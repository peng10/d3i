start to construct graph
graph construct over
58300
epochs start
batch start
#iterations: 0
currently lose_sum: 402.1874353289604
time_elpased: 1.246
batch start
#iterations: 1
currently lose_sum: 397.4976220726967
time_elpased: 1.163
batch start
#iterations: 2
currently lose_sum: 393.69642919301987
time_elpased: 1.22
batch start
#iterations: 3
currently lose_sum: 392.19616025686264
time_elpased: 1.174
batch start
#iterations: 4
currently lose_sum: 390.2625470161438
time_elpased: 1.163
batch start
#iterations: 5
currently lose_sum: 389.45718961954117
time_elpased: 1.184
batch start
#iterations: 6
currently lose_sum: 389.0486274957657
time_elpased: 1.164
batch start
#iterations: 7
currently lose_sum: 387.94826728105545
time_elpased: 1.196
batch start
#iterations: 8
currently lose_sum: 385.7805914878845
time_elpased: 1.187
batch start
#iterations: 9
currently lose_sum: 385.3763487935066
time_elpased: 1.2
batch start
#iterations: 10
currently lose_sum: 384.74247032403946
time_elpased: 1.193
batch start
#iterations: 11
currently lose_sum: 384.87004458904266
time_elpased: 1.184
batch start
#iterations: 12
currently lose_sum: 384.1708492040634
time_elpased: 1.191
batch start
#iterations: 13
currently lose_sum: 384.1312553882599
time_elpased: 1.177
batch start
#iterations: 14
currently lose_sum: 383.0917857885361
time_elpased: 1.17
batch start
#iterations: 15
currently lose_sum: 382.5121660232544
time_elpased: 1.176
batch start
#iterations: 16
currently lose_sum: 381.590889275074
time_elpased: 1.179
batch start
#iterations: 17
currently lose_sum: 383.5857967734337
time_elpased: 1.195
batch start
#iterations: 18
currently lose_sum: 382.3189236521721
time_elpased: 1.242
batch start
#iterations: 19
currently lose_sum: 382.13713026046753
time_elpased: 1.203
start validation test
0.696701030928
0.843267882188
0.490816326531
0.62048503612
0
validation finish
batch start
#iterations: 20
currently lose_sum: 381.73929250240326
time_elpased: 1.24
batch start
#iterations: 21
currently lose_sum: 381.78619503974915
time_elpased: 1.192
batch start
#iterations: 22
currently lose_sum: 381.29373985528946
time_elpased: 1.277
batch start
#iterations: 23
currently lose_sum: 382.17619425058365
time_elpased: 1.203
batch start
#iterations: 24
currently lose_sum: 381.9994129538536
time_elpased: 1.256
batch start
#iterations: 25
currently lose_sum: 381.89441949129105
time_elpased: 1.299
batch start
#iterations: 26
currently lose_sum: 381.9193349480629
time_elpased: 1.213
batch start
#iterations: 27
currently lose_sum: 381.35105389356613
time_elpased: 1.254
batch start
#iterations: 28
currently lose_sum: 381.6020292043686
time_elpased: 1.244
batch start
#iterations: 29
currently lose_sum: 381.00680524110794
time_elpased: 1.188
batch start
#iterations: 30
currently lose_sum: 380.96923810243607
time_elpased: 1.219
batch start
#iterations: 31
currently lose_sum: 381.0840294957161
time_elpased: 1.247
batch start
#iterations: 32
currently lose_sum: 380.3494155406952
time_elpased: 1.242
batch start
#iterations: 33
currently lose_sum: 380.3159828186035
time_elpased: 1.201
batch start
#iterations: 34
currently lose_sum: 380.816505253315
time_elpased: 1.194
batch start
#iterations: 35
currently lose_sum: 379.798013150692
time_elpased: 1.211
batch start
#iterations: 36
currently lose_sum: 380.6982626914978
time_elpased: 1.169
batch start
#iterations: 37
currently lose_sum: 381.064327955246
time_elpased: 1.184
batch start
#iterations: 38
currently lose_sum: 379.7835695743561
time_elpased: 1.15
batch start
#iterations: 39
currently lose_sum: 381.10557490587234
time_elpased: 1.152
start validation test
0.711701030928
0.857798945399
0.514591836735
0.643280821481
0
validation finish
batch start
#iterations: 40
currently lose_sum: 381.57638770341873
time_elpased: 1.179
batch start
#iterations: 41
currently lose_sum: 380.1389529109001
time_elpased: 1.192
batch start
#iterations: 42
currently lose_sum: 380.06060296297073
time_elpased: 1.171
batch start
#iterations: 43
currently lose_sum: 379.58987188339233
time_elpased: 1.175
batch start
#iterations: 44
currently lose_sum: 379.78162091970444
time_elpased: 1.171
batch start
#iterations: 45
currently lose_sum: 380.3499467968941
time_elpased: 1.165
batch start
#iterations: 46
currently lose_sum: 379.6718022823334
time_elpased: 1.186
batch start
#iterations: 47
currently lose_sum: 380.27186012268066
time_elpased: 1.175
batch start
#iterations: 48
currently lose_sum: 380.49621176719666
time_elpased: 1.164
batch start
#iterations: 49
currently lose_sum: 380.3196526169777
time_elpased: 1.199
batch start
#iterations: 50
currently lose_sum: 380.4872004389763
time_elpased: 1.164
batch start
#iterations: 51
currently lose_sum: 379.08553290367126
time_elpased: 1.177
batch start
#iterations: 52
currently lose_sum: 379.80893218517303
time_elpased: 1.155
batch start
#iterations: 53
currently lose_sum: 379.54803520441055
time_elpased: 1.177
batch start
#iterations: 54
currently lose_sum: 379.63435512781143
time_elpased: 1.158
batch start
#iterations: 55
currently lose_sum: 379.849451482296
time_elpased: 1.18
batch start
#iterations: 56
currently lose_sum: 380.04360485076904
time_elpased: 1.16
batch start
#iterations: 57
currently lose_sum: 379.38807171583176
time_elpased: 1.174
batch start
#iterations: 58
currently lose_sum: 379.8285637497902
time_elpased: 1.178
batch start
#iterations: 59
currently lose_sum: 380.12143844366074
time_elpased: 1.172
start validation test
0.748402061856
0.810896220453
0.654591836735
0.724408559652
0
validation finish
batch start
#iterations: 60
currently lose_sum: 380.01360243558884
time_elpased: 1.199
batch start
#iterations: 61
currently lose_sum: 380.43657982349396
time_elpased: 1.163
batch start
#iterations: 62
currently lose_sum: 380.32194125652313
time_elpased: 1.175
batch start
#iterations: 63
currently lose_sum: 379.7281524538994
time_elpased: 1.169
batch start
#iterations: 64
currently lose_sum: 379.53250527381897
time_elpased: 1.15
batch start
#iterations: 65
currently lose_sum: 379.0585622191429
time_elpased: 1.144
batch start
#iterations: 66
currently lose_sum: 379.13980680704117
time_elpased: 1.157
batch start
#iterations: 67
currently lose_sum: 379.75925105810165
time_elpased: 1.152
batch start
#iterations: 68
currently lose_sum: 380.5935848355293
time_elpased: 1.152
batch start
#iterations: 69
currently lose_sum: 380.76983219385147
time_elpased: 1.158
batch start
#iterations: 70
currently lose_sum: 378.74511075019836
time_elpased: 1.171
batch start
#iterations: 71
currently lose_sum: 380.59121745824814
time_elpased: 1.165
batch start
#iterations: 72
currently lose_sum: 379.00041049718857
time_elpased: 1.147
batch start
#iterations: 73
currently lose_sum: 380.53932988643646
time_elpased: 1.14
batch start
#iterations: 74
currently lose_sum: 379.18300580978394
time_elpased: 1.149
batch start
#iterations: 75
currently lose_sum: 379.50677210092545
time_elpased: 1.153
batch start
#iterations: 76
currently lose_sum: 378.9354158639908
time_elpased: 1.152
batch start
#iterations: 77
currently lose_sum: 379.6925554871559
time_elpased: 1.165
batch start
#iterations: 78
currently lose_sum: 378.7771881222725
time_elpased: 1.156
batch start
#iterations: 79
currently lose_sum: 378.84676307439804
time_elpased: 1.16
start validation test
0.75587628866
0.796973961999
0.693367346939
0.741569355015
0
validation finish
batch start
#iterations: 80
currently lose_sum: 379.56874614953995
time_elpased: 1.197
batch start
#iterations: 81
currently lose_sum: 380.161148250103
time_elpased: 1.166
batch start
#iterations: 82
currently lose_sum: 379.4326530098915
time_elpased: 1.156
batch start
#iterations: 83
currently lose_sum: 379.15985906124115
time_elpased: 1.159
batch start
#iterations: 84
currently lose_sum: 379.2878643274307
time_elpased: 1.143
batch start
#iterations: 85
currently lose_sum: 378.4728150367737
time_elpased: 1.145
batch start
#iterations: 86
currently lose_sum: 378.6858586072922
time_elpased: 1.157
batch start
#iterations: 87
currently lose_sum: 379.62655568122864
time_elpased: 1.164
batch start
#iterations: 88
currently lose_sum: 378.37751108407974
time_elpased: 1.143
batch start
#iterations: 89
currently lose_sum: 379.2770597934723
time_elpased: 1.16
batch start
#iterations: 90
currently lose_sum: 379.6281200647354
time_elpased: 1.153
batch start
#iterations: 91
currently lose_sum: 378.79239988327026
time_elpased: 1.158
batch start
#iterations: 92
currently lose_sum: 379.93597173690796
time_elpased: 1.158
batch start
#iterations: 93
currently lose_sum: 380.02538126707077
time_elpased: 1.149
batch start
#iterations: 94
currently lose_sum: 379.87692415714264
time_elpased: 1.155
batch start
#iterations: 95
currently lose_sum: 379.5184329152107
time_elpased: 1.157
batch start
#iterations: 96
currently lose_sum: 378.95406395196915
time_elpased: 1.161
batch start
#iterations: 97
currently lose_sum: 380.41288805007935
time_elpased: 1.164
batch start
#iterations: 98
currently lose_sum: 378.55845218896866
time_elpased: 1.174
batch start
#iterations: 99
currently lose_sum: 379.06549739837646
time_elpased: 1.152
start validation test
0.744278350515
0.839198093369
0.610816326531
0.707021791768
0
validation finish
batch start
#iterations: 100
currently lose_sum: 379.7435966730118
time_elpased: 1.185
batch start
#iterations: 101
currently lose_sum: 378.1078482866287
time_elpased: 1.188
batch start
#iterations: 102
currently lose_sum: 378.6258936524391
time_elpased: 1.193
batch start
#iterations: 103
currently lose_sum: 378.9086025953293
time_elpased: 1.163
batch start
#iterations: 104
currently lose_sum: 378.9231218099594
time_elpased: 1.141
batch start
#iterations: 105
currently lose_sum: 378.74550473690033
time_elpased: 1.153
batch start
#iterations: 106
currently lose_sum: 380.13178819417953
time_elpased: 1.184
batch start
#iterations: 107
currently lose_sum: 379.1964992284775
time_elpased: 1.156
batch start
#iterations: 108
currently lose_sum: 378.5999827384949
time_elpased: 1.18
batch start
#iterations: 109
currently lose_sum: 379.1235282421112
time_elpased: 1.185
batch start
#iterations: 110
currently lose_sum: 379.0430545806885
time_elpased: 1.165
batch start
#iterations: 111
currently lose_sum: 378.2264176607132
time_elpased: 1.158
batch start
#iterations: 112
currently lose_sum: 378.21907007694244
time_elpased: 1.168
batch start
#iterations: 113
currently lose_sum: 378.6930168271065
time_elpased: 1.157
batch start
#iterations: 114
currently lose_sum: 378.9462702870369
time_elpased: 1.214
batch start
#iterations: 115
currently lose_sum: 379.5930914878845
time_elpased: 1.157
batch start
#iterations: 116
currently lose_sum: 378.6911813020706
time_elpased: 1.182
batch start
#iterations: 117
currently lose_sum: 379.37922686338425
time_elpased: 1.174
batch start
#iterations: 118
currently lose_sum: 378.4949389696121
time_elpased: 1.188
batch start
#iterations: 119
currently lose_sum: 378.2030454277992
time_elpased: 1.153
start validation test
0.734484536082
0.850656207573
0.575408163265
0.686469048634
0
validation finish
batch start
#iterations: 120
currently lose_sum: 377.9996470808983
time_elpased: 1.163
batch start
#iterations: 121
currently lose_sum: 379.67681843042374
time_elpased: 1.173
batch start
#iterations: 122
currently lose_sum: 378.4988434910774
time_elpased: 1.151
batch start
#iterations: 123
currently lose_sum: 377.59796237945557
time_elpased: 1.173
batch start
#iterations: 124
currently lose_sum: 379.7877599596977
time_elpased: 1.146
batch start
#iterations: 125
currently lose_sum: 378.1065847873688
time_elpased: 1.16
batch start
#iterations: 126
currently lose_sum: 378.60074973106384
time_elpased: 1.158
batch start
#iterations: 127
currently lose_sum: 377.7595554590225
time_elpased: 1.176
batch start
#iterations: 128
currently lose_sum: 378.82016599178314
time_elpased: 1.172
batch start
#iterations: 129
currently lose_sum: 380.0881996154785
time_elpased: 1.165
batch start
#iterations: 130
currently lose_sum: 377.6032994389534
time_elpased: 1.158
batch start
#iterations: 131
currently lose_sum: 378.5680419206619
time_elpased: 1.154
batch start
#iterations: 132
currently lose_sum: 378.3044179081917
time_elpased: 1.167
batch start
#iterations: 133
currently lose_sum: 378.0282503962517
time_elpased: 1.192
batch start
#iterations: 134
currently lose_sum: 377.7284096479416
time_elpased: 1.166
batch start
#iterations: 135
currently lose_sum: 379.4577245116234
time_elpased: 1.171
batch start
#iterations: 136
currently lose_sum: 378.64041608572006
time_elpased: 1.202
batch start
#iterations: 137
currently lose_sum: 378.34397941827774
time_elpased: 1.238
batch start
#iterations: 138
currently lose_sum: 379.3876141309738
time_elpased: 1.232
batch start
#iterations: 139
currently lose_sum: 377.24344688653946
time_elpased: 1.267
start validation test
0.757319587629
0.81223939171
0.675816326531
0.737774312131
0
validation finish
batch start
#iterations: 140
currently lose_sum: 378.1575677394867
time_elpased: 1.269
batch start
#iterations: 141
currently lose_sum: 378.0255693793297
time_elpased: 1.267
batch start
#iterations: 142
currently lose_sum: 379.74545139074326
time_elpased: 1.254
batch start
#iterations: 143
currently lose_sum: 378.4947386980057
time_elpased: 1.232
batch start
#iterations: 144
currently lose_sum: 378.2735145688057
time_elpased: 1.257
batch start
#iterations: 145
currently lose_sum: 380.1863611936569
time_elpased: 1.22
batch start
#iterations: 146
currently lose_sum: 379.2748634815216
time_elpased: 1.219
batch start
#iterations: 147
currently lose_sum: 379.199629843235
time_elpased: 1.27
batch start
#iterations: 148
currently lose_sum: 379.8700262904167
time_elpased: 1.238
batch start
#iterations: 149
currently lose_sum: 378.2562178373337
time_elpased: 1.249
batch start
#iterations: 150
currently lose_sum: 377.39721500873566
time_elpased: 1.225
batch start
#iterations: 151
currently lose_sum: 378.58010214567184
time_elpased: 1.218
batch start
#iterations: 152
currently lose_sum: 378.9185565710068
time_elpased: 1.218
batch start
#iterations: 153
currently lose_sum: 379.81889510154724
time_elpased: 1.236
batch start
#iterations: 154
currently lose_sum: 378.3243686556816
time_elpased: 1.256
batch start
#iterations: 155
currently lose_sum: 378.5155732035637
time_elpased: 1.285
batch start
#iterations: 156
currently lose_sum: 379.3388531804085
time_elpased: 1.241
batch start
#iterations: 157
currently lose_sum: 378.20932108163834
time_elpased: 1.25
batch start
#iterations: 158
currently lose_sum: 378.17336785793304
time_elpased: 1.228
batch start
#iterations: 159
currently lose_sum: 379.37820667028427
time_elpased: 1.177
start validation test
0.75675257732
0.817522809649
0.667448979592
0.734902533566
0
validation finish
batch start
#iterations: 160
currently lose_sum: 378.98010075092316
time_elpased: 1.268
batch start
#iterations: 161
currently lose_sum: 378.92043513059616
time_elpased: 1.227
batch start
#iterations: 162
currently lose_sum: 379.1467083096504
time_elpased: 1.225
batch start
#iterations: 163
currently lose_sum: 378.164886534214
time_elpased: 1.251
batch start
#iterations: 164
currently lose_sum: 378.07203763723373
time_elpased: 1.201
batch start
#iterations: 165
currently lose_sum: 379.53203362226486
time_elpased: 1.269
batch start
#iterations: 166
currently lose_sum: 379.22217363119125
time_elpased: 1.272
batch start
#iterations: 167
currently lose_sum: 378.13830214738846
time_elpased: 1.216
batch start
#iterations: 168
currently lose_sum: 377.6123346686363
time_elpased: 1.261
batch start
#iterations: 169
currently lose_sum: 378.3212223649025
time_elpased: 1.241
batch start
#iterations: 170
currently lose_sum: 377.0844473838806
time_elpased: 1.264
batch start
#iterations: 171
currently lose_sum: 378.28364676237106
time_elpased: 1.262
batch start
#iterations: 172
currently lose_sum: 379.21564811468124
time_elpased: 1.292
batch start
#iterations: 173
currently lose_sum: 377.6590206027031
time_elpased: 1.274
batch start
#iterations: 174
currently lose_sum: 378.22796750068665
time_elpased: 1.235
batch start
#iterations: 175
currently lose_sum: 378.5403512120247
time_elpased: 1.257
batch start
#iterations: 176
currently lose_sum: 377.7552709579468
time_elpased: 1.237
batch start
#iterations: 177
currently lose_sum: 379.28942716121674
time_elpased: 1.205
batch start
#iterations: 178
currently lose_sum: 378.1877189874649
time_elpased: 1.217
batch start
#iterations: 179
currently lose_sum: 379.7384414076805
time_elpased: 1.233
start validation test
0.744278350515
0.839674294539
0.610306122449
0.706848667494
0
validation finish
batch start
#iterations: 180
currently lose_sum: 379.99781984090805
time_elpased: 1.237
batch start
#iterations: 181
currently lose_sum: 378.4235047698021
time_elpased: 1.223
batch start
#iterations: 182
currently lose_sum: 379.7740250825882
time_elpased: 1.23
batch start
#iterations: 183
currently lose_sum: 376.95226579904556
time_elpased: 1.196
batch start
#iterations: 184
currently lose_sum: 377.87567830085754
time_elpased: 1.192
batch start
#iterations: 185
currently lose_sum: 378.7925413250923
time_elpased: 1.212
batch start
#iterations: 186
currently lose_sum: 378.25875157117844
time_elpased: 1.203
batch start
#iterations: 187
currently lose_sum: 378.6557509303093
time_elpased: 1.211
batch start
#iterations: 188
currently lose_sum: 378.14780789613724
time_elpased: 1.218
batch start
#iterations: 189
currently lose_sum: 378.9189713001251
time_elpased: 1.229
batch start
#iterations: 190
currently lose_sum: 377.7347868680954
time_elpased: 1.208
batch start
#iterations: 191
currently lose_sum: 378.3477861881256
time_elpased: 1.228
batch start
#iterations: 192
currently lose_sum: 379.8018524646759
time_elpased: 1.218
batch start
#iterations: 193
currently lose_sum: 378.0294834971428
time_elpased: 1.201
batch start
#iterations: 194
currently lose_sum: 378.4800257086754
time_elpased: 1.214
batch start
#iterations: 195
currently lose_sum: 377.8999168276787
time_elpased: 1.189
batch start
#iterations: 196
currently lose_sum: 379.3158081769943
time_elpased: 1.223
batch start
#iterations: 197
currently lose_sum: 379.1679755449295
time_elpased: 1.231
batch start
#iterations: 198
currently lose_sum: 379.4179161787033
time_elpased: 1.279
batch start
#iterations: 199
currently lose_sum: 378.0947550535202
time_elpased: 1.267
start validation test
0.742628865979
0.848687073843
0.59693877551
0.700892589708
0
validation finish
batch start
#iterations: 200
currently lose_sum: 377.9432844519615
time_elpased: 1.244
batch start
#iterations: 201
currently lose_sum: 378.0879470705986
time_elpased: 1.211
batch start
#iterations: 202
currently lose_sum: 378.26195365190506
time_elpased: 1.19
batch start
#iterations: 203
currently lose_sum: 378.75252825021744
time_elpased: 1.215
batch start
#iterations: 204
currently lose_sum: 378.5050414800644
time_elpased: 1.241
batch start
#iterations: 205
currently lose_sum: 379.19841903448105
time_elpased: 1.221
batch start
#iterations: 206
currently lose_sum: 378.49277007579803
time_elpased: 1.199
batch start
#iterations: 207
currently lose_sum: 379.2814128398895
time_elpased: 1.194
batch start
#iterations: 208
currently lose_sum: 377.72704350948334
time_elpased: 1.188
batch start
#iterations: 209
currently lose_sum: 378.90949434041977
time_elpased: 1.198
batch start
#iterations: 210
currently lose_sum: 377.2138497233391
time_elpased: 1.202
batch start
#iterations: 211
currently lose_sum: 377.7359589934349
time_elpased: 1.218
batch start
#iterations: 212
currently lose_sum: 378.0650084614754
time_elpased: 1.184
batch start
#iterations: 213
currently lose_sum: 377.1367880702019
time_elpased: 1.213
batch start
#iterations: 214
currently lose_sum: 378.5151416063309
time_elpased: 1.192
batch start
#iterations: 215
currently lose_sum: 379.6055254340172
time_elpased: 1.168
batch start
#iterations: 216
currently lose_sum: 378.1165786385536
time_elpased: 1.172
batch start
#iterations: 217
currently lose_sum: 377.59763354063034
time_elpased: 1.175
batch start
#iterations: 218
currently lose_sum: 378.84892958402634
time_elpased: 1.201
batch start
#iterations: 219
currently lose_sum: 379.146202981472
time_elpased: 1.193
start validation test
0.735515463918
0.849633068743
0.57887755102
0.688596225041
0
validation finish
batch start
#iterations: 220
currently lose_sum: 377.9647227525711
time_elpased: 1.196
batch start
#iterations: 221
currently lose_sum: 377.3727799654007
time_elpased: 1.163
batch start
#iterations: 222
currently lose_sum: 377.1785206794739
time_elpased: 1.188
batch start
#iterations: 223
currently lose_sum: 378.4786006808281
time_elpased: 1.217
batch start
#iterations: 224
currently lose_sum: 378.2752631306648
time_elpased: 1.183
batch start
#iterations: 225
currently lose_sum: 378.1052854657173
time_elpased: 1.169
batch start
#iterations: 226
currently lose_sum: 379.6986166238785
time_elpased: 1.172
batch start
#iterations: 227
currently lose_sum: 379.98358672857285
time_elpased: 1.173
batch start
#iterations: 228
currently lose_sum: 378.5326511859894
time_elpased: 1.182
batch start
#iterations: 229
currently lose_sum: 377.24996292591095
time_elpased: 1.182
batch start
#iterations: 230
currently lose_sum: 378.98883831501007
time_elpased: 1.184
batch start
#iterations: 231
currently lose_sum: 378.16658622026443
time_elpased: 1.166
batch start
#iterations: 232
currently lose_sum: 378.974524974823
time_elpased: 1.216
batch start
#iterations: 233
currently lose_sum: 379.15322226285934
time_elpased: 1.185
batch start
#iterations: 234
currently lose_sum: 377.6417655944824
time_elpased: 1.182
batch start
#iterations: 235
currently lose_sum: 378.4548138976097
time_elpased: 1.179
batch start
#iterations: 236
currently lose_sum: 377.54321414232254
time_elpased: 1.162
batch start
#iterations: 237
currently lose_sum: 379.6564449071884
time_elpased: 1.165
batch start
#iterations: 238
currently lose_sum: 378.2350819706917
time_elpased: 1.227
batch start
#iterations: 239
currently lose_sum: 378.37038308382034
time_elpased: 1.263
start validation test
0.740567010309
0.851912003543
0.588775510204
0.696313280637
0
validation finish
batch start
#iterations: 240
currently lose_sum: 377.4130354523659
time_elpased: 1.218
batch start
#iterations: 241
currently lose_sum: 378.6295906305313
time_elpased: 1.207
batch start
#iterations: 242
currently lose_sum: 378.10578966140747
time_elpased: 1.178
batch start
#iterations: 243
currently lose_sum: 377.80262649059296
time_elpased: 1.181
batch start
#iterations: 244
currently lose_sum: 378.1042369008064
time_elpased: 1.218
batch start
#iterations: 245
currently lose_sum: 377.75520569086075
time_elpased: 1.198
batch start
#iterations: 246
currently lose_sum: 378.1415674686432
time_elpased: 1.195
batch start
#iterations: 247
currently lose_sum: 378.62703961133957
time_elpased: 1.188
batch start
#iterations: 248
currently lose_sum: 379.643812417984
time_elpased: 1.198
batch start
#iterations: 249
currently lose_sum: 378.5754184126854
time_elpased: 1.164
batch start
#iterations: 250
currently lose_sum: 378.7071121931076
time_elpased: 1.178
batch start
#iterations: 251
currently lose_sum: 379.1291571855545
time_elpased: 1.167
batch start
#iterations: 252
currently lose_sum: 377.76132076978683
time_elpased: 1.168
batch start
#iterations: 253
currently lose_sum: 378.7088631987572
time_elpased: 1.193
batch start
#iterations: 254
currently lose_sum: 378.09557723999023
time_elpased: 1.211
batch start
#iterations: 255
currently lose_sum: 378.48585563898087
time_elpased: 1.197
batch start
#iterations: 256
currently lose_sum: 378.29850924015045
time_elpased: 1.174
batch start
#iterations: 257
currently lose_sum: 378.9888640642166
time_elpased: 1.19
batch start
#iterations: 258
currently lose_sum: 377.253757417202
time_elpased: 1.195
batch start
#iterations: 259
currently lose_sum: 378.12510669231415
time_elpased: 1.186
start validation test
0.755567010309
0.83425852498
0.644081632653
0.726937694345
0
validation finish
batch start
#iterations: 260
currently lose_sum: 378.48106974363327
time_elpased: 1.207
batch start
#iterations: 261
currently lose_sum: 378.32885468006134
time_elpased: 1.191
batch start
#iterations: 262
currently lose_sum: 378.07314145565033
time_elpased: 1.17
batch start
#iterations: 263
currently lose_sum: 377.8032165169716
time_elpased: 1.209
batch start
#iterations: 264
currently lose_sum: 378.0723171234131
time_elpased: 1.18
batch start
#iterations: 265
currently lose_sum: 379.0237305164337
time_elpased: 1.213
batch start
#iterations: 266
currently lose_sum: 377.92571127414703
time_elpased: 1.188
batch start
#iterations: 267
currently lose_sum: 377.55216455459595
time_elpased: 1.208
batch start
#iterations: 268
currently lose_sum: 378.24502140283585
time_elpased: 1.196
batch start
#iterations: 269
currently lose_sum: 378.35889649391174
time_elpased: 1.182
batch start
#iterations: 270
currently lose_sum: 378.484198987484
time_elpased: 1.193
batch start
#iterations: 271
currently lose_sum: 378.94646191596985
time_elpased: 1.212
batch start
#iterations: 272
currently lose_sum: 378.3242118358612
time_elpased: 1.18
batch start
#iterations: 273
currently lose_sum: 377.7173970937729
time_elpased: 1.206
batch start
#iterations: 274
currently lose_sum: 379.03980857133865
time_elpased: 1.183
batch start
#iterations: 275
currently lose_sum: 378.65062725543976
time_elpased: 1.201
batch start
#iterations: 276
currently lose_sum: 379.06425911188126
time_elpased: 1.175
batch start
#iterations: 277
currently lose_sum: 379.41005641222
time_elpased: 1.189
batch start
#iterations: 278
currently lose_sum: 377.52687841653824
time_elpased: 1.196
batch start
#iterations: 279
currently lose_sum: 380.37136352062225
time_elpased: 1.229
start validation test
0.742268041237
0.85097981866
0.593775510204
0.699483110951
0
validation finish
batch start
#iterations: 280
currently lose_sum: 379.0995927453041
time_elpased: 1.214
batch start
#iterations: 281
currently lose_sum: 378.56368386745453
time_elpased: 1.206
batch start
#iterations: 282
currently lose_sum: 378.9378150701523
time_elpased: 1.185
batch start
#iterations: 283
currently lose_sum: 379.3128935098648
time_elpased: 1.222
batch start
#iterations: 284
currently lose_sum: 378.2456775903702
time_elpased: 1.195
batch start
#iterations: 285
currently lose_sum: 378.97306925058365
time_elpased: 1.202
batch start
#iterations: 286
currently lose_sum: 378.62880128622055
time_elpased: 1.198
batch start
#iterations: 287
currently lose_sum: 378.385691344738
time_elpased: 1.18
batch start
#iterations: 288
currently lose_sum: 378.46327060461044
time_elpased: 1.189
batch start
#iterations: 289
currently lose_sum: 377.4014105796814
time_elpased: 1.198
batch start
#iterations: 290
currently lose_sum: 377.7440799474716
time_elpased: 1.171
batch start
#iterations: 291
currently lose_sum: 377.5582158565521
time_elpased: 1.187
batch start
#iterations: 292
currently lose_sum: 377.6844298839569
time_elpased: 1.166
batch start
#iterations: 293
currently lose_sum: 378.3522387742996
time_elpased: 1.185
batch start
#iterations: 294
currently lose_sum: 377.3227626681328
time_elpased: 1.199
batch start
#iterations: 295
currently lose_sum: 377.9219495654106
time_elpased: 1.187
batch start
#iterations: 296
currently lose_sum: 377.84034383296967
time_elpased: 1.199
batch start
#iterations: 297
currently lose_sum: 378.7912123799324
time_elpased: 1.194
batch start
#iterations: 298
currently lose_sum: 377.7089219689369
time_elpased: 1.203
batch start
#iterations: 299
currently lose_sum: 378.27955335378647
time_elpased: 1.226
start validation test
0.755
0.838588487857
0.637755102041
0.724511679128
0
validation finish
batch start
#iterations: 300
currently lose_sum: 379.54479801654816
time_elpased: 1.224
batch start
#iterations: 301
currently lose_sum: 377.5799416899681
time_elpased: 1.199
batch start
#iterations: 302
currently lose_sum: 378.5240304470062
time_elpased: 1.177
batch start
#iterations: 303
currently lose_sum: 378.0683826804161
time_elpased: 1.187
batch start
#iterations: 304
currently lose_sum: 378.81148022413254
time_elpased: 1.226
batch start
#iterations: 305
currently lose_sum: 378.2654284834862
time_elpased: 1.238
batch start
#iterations: 306
currently lose_sum: 378.137011051178
time_elpased: 1.213
batch start
#iterations: 307
currently lose_sum: 377.2789507508278
time_elpased: 1.199
batch start
#iterations: 308
currently lose_sum: 377.0718193054199
time_elpased: 1.171
batch start
#iterations: 309
currently lose_sum: 377.3640312552452
time_elpased: 1.2
batch start
#iterations: 310
currently lose_sum: 377.5135165452957
time_elpased: 1.25
batch start
#iterations: 311
currently lose_sum: 378.545478284359
time_elpased: 1.196
batch start
#iterations: 312
currently lose_sum: 377.7028462290764
time_elpased: 1.166
batch start
#iterations: 313
currently lose_sum: 377.5929073691368
time_elpased: 1.183
batch start
#iterations: 314
currently lose_sum: 376.53254121541977
time_elpased: 1.206
batch start
#iterations: 315
currently lose_sum: 377.57822531461716
time_elpased: 1.189
batch start
#iterations: 316
currently lose_sum: 378.2825456261635
time_elpased: 1.213
batch start
#iterations: 317
currently lose_sum: 378.5695983171463
time_elpased: 1.208
batch start
#iterations: 318
currently lose_sum: 378.2418540120125
time_elpased: 1.197
batch start
#iterations: 319
currently lose_sum: 378.72872692346573
time_elpased: 1.196
start validation test
0.755103092784
0.835214446953
0.641836734694
0.725866943627
0
validation finish
batch start
#iterations: 320
currently lose_sum: 379.5163668990135
time_elpased: 1.246
batch start
#iterations: 321
currently lose_sum: 379.8592832684517
time_elpased: 1.186
batch start
#iterations: 322
currently lose_sum: 377.9408836364746
time_elpased: 1.174
batch start
#iterations: 323
currently lose_sum: 377.73562985658646
time_elpased: 1.197
batch start
#iterations: 324
currently lose_sum: 378.370545566082
time_elpased: 1.197
batch start
#iterations: 325
currently lose_sum: 377.51548659801483
time_elpased: 1.203
batch start
#iterations: 326
currently lose_sum: 378.84011220932007
time_elpased: 1.167
batch start
#iterations: 327
currently lose_sum: 379.0407962799072
time_elpased: 1.171
batch start
#iterations: 328
currently lose_sum: 377.9418777823448
time_elpased: 1.165
batch start
#iterations: 329
currently lose_sum: 377.3480980992317
time_elpased: 1.215
batch start
#iterations: 330
currently lose_sum: 379.16558384895325
time_elpased: 1.17
batch start
#iterations: 331
currently lose_sum: 378.39497488737106
time_elpased: 1.179
batch start
#iterations: 332
currently lose_sum: 378.1191294193268
time_elpased: 1.177
batch start
#iterations: 333
currently lose_sum: 378.056786775589
time_elpased: 1.213
batch start
#iterations: 334
currently lose_sum: 378.8535786271095
time_elpased: 1.216
batch start
#iterations: 335
currently lose_sum: 377.211250603199
time_elpased: 1.2
batch start
#iterations: 336
currently lose_sum: 377.77084988355637
time_elpased: 1.198
batch start
#iterations: 337
currently lose_sum: 378.0880457162857
time_elpased: 1.172
batch start
#iterations: 338
currently lose_sum: 378.5902125239372
time_elpased: 1.179
batch start
#iterations: 339
currently lose_sum: 377.29749685525894
time_elpased: 1.197
start validation test
0.744226804124
0.851496657948
0.597959183673
0.702553650641
0
validation finish
batch start
#iterations: 340
currently lose_sum: 377.8116983771324
time_elpased: 1.195
batch start
#iterations: 341
currently lose_sum: 378.44961655139923
time_elpased: 1.158
batch start
#iterations: 342
currently lose_sum: 377.43011409044266
time_elpased: 1.197
batch start
#iterations: 343
currently lose_sum: 378.0184452533722
time_elpased: 1.188
batch start
#iterations: 344
currently lose_sum: 376.814699947834
time_elpased: 1.176
batch start
#iterations: 345
currently lose_sum: 379.0748789906502
time_elpased: 1.187
batch start
#iterations: 346
currently lose_sum: 377.61370557546616
time_elpased: 1.159
batch start
#iterations: 347
currently lose_sum: 376.8041120171547
time_elpased: 1.156
batch start
#iterations: 348
currently lose_sum: 379.3147268295288
time_elpased: 1.183
batch start
#iterations: 349
currently lose_sum: 378.1030721068382
time_elpased: 1.19
batch start
#iterations: 350
currently lose_sum: 378.666531085968
time_elpased: 1.181
batch start
#iterations: 351
currently lose_sum: 377.31464517116547
time_elpased: 1.183
batch start
#iterations: 352
currently lose_sum: 378.6006659269333
time_elpased: 1.153
batch start
#iterations: 353
currently lose_sum: 378.0415410399437
time_elpased: 1.17
batch start
#iterations: 354
currently lose_sum: 377.71116399765015
time_elpased: 1.164
batch start
#iterations: 355
currently lose_sum: 378.2754076719284
time_elpased: 1.195
batch start
#iterations: 356
currently lose_sum: 377.5012077689171
time_elpased: 1.207
batch start
#iterations: 357
currently lose_sum: 379.0084989666939
time_elpased: 1.17
batch start
#iterations: 358
currently lose_sum: 378.89964497089386
time_elpased: 1.159
batch start
#iterations: 359
currently lose_sum: 378.62988674640656
time_elpased: 1.165
start validation test
0.744278350515
0.853572994301
0.596020408163
0.701916721745
0
validation finish
batch start
#iterations: 360
currently lose_sum: 377.28771221637726
time_elpased: 1.184
batch start
#iterations: 361
currently lose_sum: 378.2939838171005
time_elpased: 1.15
batch start
#iterations: 362
currently lose_sum: 378.81892716884613
time_elpased: 1.191
batch start
#iterations: 363
currently lose_sum: 378.1818386912346
time_elpased: 1.164
batch start
#iterations: 364
currently lose_sum: 379.28352373838425
time_elpased: 1.163
batch start
#iterations: 365
currently lose_sum: 379.3895403146744
time_elpased: 1.176
batch start
#iterations: 366
currently lose_sum: 379.2383280992508
time_elpased: 1.163
batch start
#iterations: 367
currently lose_sum: 378.5511816740036
time_elpased: 1.164
batch start
#iterations: 368
currently lose_sum: 377.90403574705124
time_elpased: 1.144
batch start
#iterations: 369
currently lose_sum: 376.83383280038834
time_elpased: 1.164
batch start
#iterations: 370
currently lose_sum: 377.97362619638443
time_elpased: 1.159
batch start
#iterations: 371
currently lose_sum: 378.5349010825157
time_elpased: 1.158
batch start
#iterations: 372
currently lose_sum: 378.0127252936363
time_elpased: 1.148
batch start
#iterations: 373
currently lose_sum: 378.78716772794724
time_elpased: 1.177
batch start
#iterations: 374
currently lose_sum: 377.7430452108383
time_elpased: 1.176
batch start
#iterations: 375
currently lose_sum: 378.07667338848114
time_elpased: 1.187
batch start
#iterations: 376
currently lose_sum: 378.8218364715576
time_elpased: 1.181
batch start
#iterations: 377
currently lose_sum: 378.862952709198
time_elpased: 1.181
batch start
#iterations: 378
currently lose_sum: 377.9582417011261
time_elpased: 1.168
batch start
#iterations: 379
currently lose_sum: 378.46120327711105
time_elpased: 1.172
start validation test
0.750360824742
0.844762832105
0.619693877551
0.714933192065
0
validation finish
batch start
#iterations: 380
currently lose_sum: 377.04055708646774
time_elpased: 1.202
batch start
#iterations: 381
currently lose_sum: 377.2408280968666
time_elpased: 1.185
batch start
#iterations: 382
currently lose_sum: 378.2108007669449
time_elpased: 1.186
batch start
#iterations: 383
currently lose_sum: 377.19877433776855
time_elpased: 1.169
batch start
#iterations: 384
currently lose_sum: 377.3913628458977
time_elpased: 1.165
batch start
#iterations: 385
currently lose_sum: 378.3042703270912
time_elpased: 1.191
batch start
#iterations: 386
currently lose_sum: 377.78313452005386
time_elpased: 1.2
batch start
#iterations: 387
currently lose_sum: 378.4545758366585
time_elpased: 1.197
batch start
#iterations: 388
currently lose_sum: 377.9231602549553
time_elpased: 1.173
batch start
#iterations: 389
currently lose_sum: 378.52184796333313
time_elpased: 1.181
batch start
#iterations: 390
currently lose_sum: 379.6860989332199
time_elpased: 1.153
batch start
#iterations: 391
currently lose_sum: 377.985116481781
time_elpased: 1.172
batch start
#iterations: 392
currently lose_sum: 376.53284734487534
time_elpased: 1.159
batch start
#iterations: 393
currently lose_sum: 377.6149482727051
time_elpased: 1.187
batch start
#iterations: 394
currently lose_sum: 379.2588896751404
time_elpased: 1.193
batch start
#iterations: 395
currently lose_sum: 378.2513852715492
time_elpased: 1.161
batch start
#iterations: 396
currently lose_sum: 377.88264375925064
time_elpased: 1.163
batch start
#iterations: 397
currently lose_sum: 378.5713589787483
time_elpased: 1.159
batch start
#iterations: 398
currently lose_sum: 377.444400370121
time_elpased: 1.168
batch start
#iterations: 399
currently lose_sum: 377.9192777276039
time_elpased: 1.168
start validation test
0.754484536082
0.838099073701
0.637040816327
0.723868050322
0
validation finish
acc: 0.756
pre: 0.788
rec: 0.692
F1: 0.737
auc: 0.000
