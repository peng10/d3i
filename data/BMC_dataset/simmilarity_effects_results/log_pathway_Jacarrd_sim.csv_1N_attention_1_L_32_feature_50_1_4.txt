start to construct graph
graph construct over
58302
epochs start
batch start
#iterations: 0
currently lose_sum: 401.4613023996353
time_elpased: 1.786
batch start
#iterations: 1
currently lose_sum: 395.43528008461
time_elpased: 1.762
batch start
#iterations: 2
currently lose_sum: 390.2661395072937
time_elpased: 1.766
batch start
#iterations: 3
currently lose_sum: 388.43661242723465
time_elpased: 1.752
batch start
#iterations: 4
currently lose_sum: 386.0996473431587
time_elpased: 1.761
batch start
#iterations: 5
currently lose_sum: 384.3160921931267
time_elpased: 1.764
batch start
#iterations: 6
currently lose_sum: 382.07090026140213
time_elpased: 1.767
batch start
#iterations: 7
currently lose_sum: 379.33557575941086
time_elpased: 1.767
batch start
#iterations: 8
currently lose_sum: 380.5860968232155
time_elpased: 1.749
batch start
#iterations: 9
currently lose_sum: 380.28806042671204
time_elpased: 1.773
batch start
#iterations: 10
currently lose_sum: 378.53629302978516
time_elpased: 1.748
batch start
#iterations: 11
currently lose_sum: 378.6606523990631
time_elpased: 1.775
batch start
#iterations: 12
currently lose_sum: 378.6169202923775
time_elpased: 1.767
batch start
#iterations: 13
currently lose_sum: 378.2958901524544
time_elpased: 1.773
batch start
#iterations: 14
currently lose_sum: 377.812725007534
time_elpased: 1.774
batch start
#iterations: 15
currently lose_sum: 376.517995595932
time_elpased: 1.772
batch start
#iterations: 16
currently lose_sum: 376.4416736960411
time_elpased: 1.773
batch start
#iterations: 17
currently lose_sum: 375.613417327404
time_elpased: 1.765
batch start
#iterations: 18
currently lose_sum: 375.25978487730026
time_elpased: 1.776
batch start
#iterations: 19
currently lose_sum: 376.39426028728485
time_elpased: 1.767
start validation test
0.738556701031
0.731644567584
0.758047980316
0.744612286002
0
validation finish
batch start
#iterations: 20
currently lose_sum: 375.3623480796814
time_elpased: 1.778
batch start
#iterations: 21
currently lose_sum: 375.2675236463547
time_elpased: 1.769
batch start
#iterations: 22
currently lose_sum: 373.5392589569092
time_elpased: 1.779
batch start
#iterations: 23
currently lose_sum: 374.7406653165817
time_elpased: 1.787
batch start
#iterations: 24
currently lose_sum: 374.1211823821068
time_elpased: 1.77
batch start
#iterations: 25
currently lose_sum: 373.71690660715103
time_elpased: 1.769
batch start
#iterations: 26
currently lose_sum: 374.0442452430725
time_elpased: 1.765
batch start
#iterations: 27
currently lose_sum: 374.24405002593994
time_elpased: 1.77
batch start
#iterations: 28
currently lose_sum: 372.3324359059334
time_elpased: 1.766
batch start
#iterations: 29
currently lose_sum: 373.3788058757782
time_elpased: 1.779
batch start
#iterations: 30
currently lose_sum: 372.4631981253624
time_elpased: 1.757
batch start
#iterations: 31
currently lose_sum: 374.20550870895386
time_elpased: 1.763
batch start
#iterations: 32
currently lose_sum: 373.4404020309448
time_elpased: 1.759
batch start
#iterations: 33
currently lose_sum: 372.50271117687225
time_elpased: 1.763
batch start
#iterations: 34
currently lose_sum: 371.88730931282043
time_elpased: 1.762
batch start
#iterations: 35
currently lose_sum: 371.29902333021164
time_elpased: 1.763
batch start
#iterations: 36
currently lose_sum: 372.6121575832367
time_elpased: 1.766
batch start
#iterations: 37
currently lose_sum: 372.47343999147415
time_elpased: 1.743
batch start
#iterations: 38
currently lose_sum: 372.674124956131
time_elpased: 1.763
batch start
#iterations: 39
currently lose_sum: 372.20746529102325
time_elpased: 1.759
start validation test
0.748402061856
0.732158170557
0.787779372565
0.758951059312
0
validation finish
batch start
#iterations: 40
currently lose_sum: 371.94560647010803
time_elpased: 1.774
batch start
#iterations: 41
currently lose_sum: 372.5715802907944
time_elpased: 1.759
batch start
#iterations: 42
currently lose_sum: 372.50137293338776
time_elpased: 1.771
batch start
#iterations: 43
currently lose_sum: 371.4717761874199
time_elpased: 1.775
batch start
#iterations: 44
currently lose_sum: 370.5442823767662
time_elpased: 1.763
batch start
#iterations: 45
currently lose_sum: 372.53681242465973
time_elpased: 1.764
batch start
#iterations: 46
currently lose_sum: 372.11400055885315
time_elpased: 1.768
batch start
#iterations: 47
currently lose_sum: 372.5244593024254
time_elpased: 1.78
batch start
#iterations: 48
currently lose_sum: 371.99213391542435
time_elpased: 1.767
batch start
#iterations: 49
currently lose_sum: 372.24130564928055
time_elpased: 1.767
batch start
#iterations: 50
currently lose_sum: 371.45737713575363
time_elpased: 1.765
batch start
#iterations: 51
currently lose_sum: 371.5025894641876
time_elpased: 1.771
batch start
#iterations: 52
currently lose_sum: 371.9191285967827
time_elpased: 1.766
batch start
#iterations: 53
currently lose_sum: 371.2360559105873
time_elpased: 1.763
batch start
#iterations: 54
currently lose_sum: 371.71741873025894
time_elpased: 1.763
batch start
#iterations: 55
currently lose_sum: 371.1337658762932
time_elpased: 1.754
batch start
#iterations: 56
currently lose_sum: 370.5741219520569
time_elpased: 1.77
batch start
#iterations: 57
currently lose_sum: 370.18906474113464
time_elpased: 1.77
batch start
#iterations: 58
currently lose_sum: 371.5486834049225
time_elpased: 1.763
batch start
#iterations: 59
currently lose_sum: 371.68900007009506
time_elpased: 1.763
start validation test
0.748969072165
0.741447498517
0.768812794751
0.75488222267
0
validation finish
batch start
#iterations: 60
currently lose_sum: 371.098980486393
time_elpased: 1.775
batch start
#iterations: 61
currently lose_sum: 371.95185631513596
time_elpased: 1.771
batch start
#iterations: 62
currently lose_sum: 371.0509155392647
time_elpased: 1.773
batch start
#iterations: 63
currently lose_sum: 370.99182319641113
time_elpased: 1.763
batch start
#iterations: 64
currently lose_sum: 370.58405727148056
time_elpased: 1.766
batch start
#iterations: 65
currently lose_sum: 369.64296501874924
time_elpased: 1.778
batch start
#iterations: 66
currently lose_sum: 371.2566277384758
time_elpased: 1.764
batch start
#iterations: 67
currently lose_sum: 370.85875111818314
time_elpased: 1.773
batch start
#iterations: 68
currently lose_sum: 370.20682632923126
time_elpased: 1.769
batch start
#iterations: 69
currently lose_sum: 370.62804543972015
time_elpased: 1.764
batch start
#iterations: 70
currently lose_sum: 371.13655388355255
time_elpased: 1.763
batch start
#iterations: 71
currently lose_sum: 371.36399376392365
time_elpased: 1.771
batch start
#iterations: 72
currently lose_sum: 368.57750618457794
time_elpased: 1.775
batch start
#iterations: 73
currently lose_sum: 370.9448173046112
time_elpased: 1.77
batch start
#iterations: 74
currently lose_sum: 370.0931774377823
time_elpased: 1.778
batch start
#iterations: 75
currently lose_sum: 369.83460879325867
time_elpased: 1.772
batch start
#iterations: 76
currently lose_sum: 371.2220019698143
time_elpased: 1.774
batch start
#iterations: 77
currently lose_sum: 370.35433119535446
time_elpased: 1.768
batch start
#iterations: 78
currently lose_sum: 370.28665030002594
time_elpased: 1.762
batch start
#iterations: 79
currently lose_sum: 369.1611785888672
time_elpased: 1.77
start validation test
0.749175257732
0.743523316062
0.765019479188
0.754118241536
0
validation finish
batch start
#iterations: 80
currently lose_sum: 371.28712183237076
time_elpased: 1.776
batch start
#iterations: 81
currently lose_sum: 371.34878504276276
time_elpased: 1.77
batch start
#iterations: 82
currently lose_sum: 369.8759462237358
time_elpased: 1.769
batch start
#iterations: 83
currently lose_sum: 369.50227534770966
time_elpased: 1.762
batch start
#iterations: 84
currently lose_sum: 370.84233593940735
time_elpased: 1.76
batch start
#iterations: 85
currently lose_sum: 370.6946558356285
time_elpased: 1.775
batch start
#iterations: 86
currently lose_sum: 369.6126801967621
time_elpased: 1.757
batch start
#iterations: 87
currently lose_sum: 370.36098235845566
time_elpased: 1.763
batch start
#iterations: 88
currently lose_sum: 370.26324969530106
time_elpased: 1.759
batch start
#iterations: 89
currently lose_sum: 369.55361169576645
time_elpased: 1.756
batch start
#iterations: 90
currently lose_sum: 370.2653130888939
time_elpased: 1.754
batch start
#iterations: 91
currently lose_sum: 369.65153777599335
time_elpased: 1.756
batch start
#iterations: 92
currently lose_sum: 371.0312843322754
time_elpased: 1.761
batch start
#iterations: 93
currently lose_sum: 369.4000468850136
time_elpased: 1.764
batch start
#iterations: 94
currently lose_sum: 370.4371320605278
time_elpased: 1.778
batch start
#iterations: 95
currently lose_sum: 369.2360372543335
time_elpased: 1.763
batch start
#iterations: 96
currently lose_sum: 371.2103055715561
time_elpased: 1.775
batch start
#iterations: 97
currently lose_sum: 369.5471984744072
time_elpased: 1.766
batch start
#iterations: 98
currently lose_sum: 370.1443382501602
time_elpased: 1.769
batch start
#iterations: 99
currently lose_sum: 369.1998279094696
time_elpased: 1.776
start validation test
0.749278350515
0.750666393275
0.750666393275
0.750666393275
0
validation finish
batch start
#iterations: 100
currently lose_sum: 369.7813346982002
time_elpased: 1.772
batch start
#iterations: 101
currently lose_sum: 371.11678767204285
time_elpased: 1.779
batch start
#iterations: 102
currently lose_sum: 369.7911624908447
time_elpased: 1.769
batch start
#iterations: 103
currently lose_sum: 370.1280951499939
time_elpased: 1.776
batch start
#iterations: 104
currently lose_sum: 368.83321553468704
time_elpased: 1.766
batch start
#iterations: 105
currently lose_sum: 370.4028860926628
time_elpased: 1.782
batch start
#iterations: 106
currently lose_sum: 369.1809111237526
time_elpased: 1.764
batch start
#iterations: 107
currently lose_sum: 370.02979815006256
time_elpased: 1.769
batch start
#iterations: 108
currently lose_sum: 370.20219522714615
time_elpased: 1.768
batch start
#iterations: 109
currently lose_sum: 369.07698154449463
time_elpased: 1.763
batch start
#iterations: 110
currently lose_sum: 370.0812793970108
time_elpased: 1.765
batch start
#iterations: 111
currently lose_sum: 368.2940008044243
time_elpased: 1.769
batch start
#iterations: 112
currently lose_sum: 369.566134929657
time_elpased: 1.771
batch start
#iterations: 113
currently lose_sum: 369.75373780727386
time_elpased: 1.766
batch start
#iterations: 114
currently lose_sum: 369.2944875359535
time_elpased: 1.783
batch start
#iterations: 115
currently lose_sum: 367.49654150009155
time_elpased: 1.771
batch start
#iterations: 116
currently lose_sum: 369.1846169233322
time_elpased: 1.776
batch start
#iterations: 117
currently lose_sum: 369.4903872013092
time_elpased: 1.762
batch start
#iterations: 118
currently lose_sum: 368.9126507639885
time_elpased: 1.777
batch start
#iterations: 119
currently lose_sum: 370.2373379468918
time_elpased: 1.774
start validation test
0.754484536082
0.747103673631
0.773528808694
0.76008663678
0
validation finish
batch start
#iterations: 120
currently lose_sum: 368.90624552965164
time_elpased: 1.756
batch start
#iterations: 121
currently lose_sum: 368.8432384133339
time_elpased: 1.77
batch start
#iterations: 122
currently lose_sum: 369.592247068882
time_elpased: 1.765
batch start
#iterations: 123
currently lose_sum: 368.2296531200409
time_elpased: 1.775
batch start
#iterations: 124
currently lose_sum: 369.27828192710876
time_elpased: 1.77
batch start
#iterations: 125
currently lose_sum: 368.1596157550812
time_elpased: 1.773
batch start
#iterations: 126
currently lose_sum: 368.7818058729172
time_elpased: 1.765
batch start
#iterations: 127
currently lose_sum: 369.2992833852768
time_elpased: 1.775
batch start
#iterations: 128
currently lose_sum: 369.0491224527359
time_elpased: 1.768
batch start
#iterations: 129
currently lose_sum: 369.098837852478
time_elpased: 1.776
batch start
#iterations: 130
currently lose_sum: 367.5778043270111
time_elpased: 1.77
batch start
#iterations: 131
currently lose_sum: 369.0851367712021
time_elpased: 1.763
batch start
#iterations: 132
currently lose_sum: 368.6385074853897
time_elpased: 1.78
batch start
#iterations: 133
currently lose_sum: 368.3037386536598
time_elpased: 1.772
batch start
#iterations: 134
currently lose_sum: 368.51467126607895
time_elpased: 1.773
batch start
#iterations: 135
currently lose_sum: 368.9301354289055
time_elpased: 1.763
batch start
#iterations: 136
currently lose_sum: 367.88815701007843
time_elpased: 1.766
batch start
#iterations: 137
currently lose_sum: 368.5937243103981
time_elpased: 1.766
batch start
#iterations: 138
currently lose_sum: 368.6702112555504
time_elpased: 1.765
batch start
#iterations: 139
currently lose_sum: 369.3429586291313
time_elpased: 1.768
start validation test
0.752886597938
0.744479495268
0.77424646299
0.759071263444
0
validation finish
batch start
#iterations: 140
currently lose_sum: 368.0925714969635
time_elpased: 1.751
batch start
#iterations: 141
currently lose_sum: 368.7732896208763
time_elpased: 1.771
batch start
#iterations: 142
currently lose_sum: 367.8504958152771
time_elpased: 1.755
batch start
#iterations: 143
currently lose_sum: 368.3050067424774
time_elpased: 1.766
batch start
#iterations: 144
currently lose_sum: 368.9754034280777
time_elpased: 1.761
batch start
#iterations: 145
currently lose_sum: 368.76045739650726
time_elpased: 1.775
batch start
#iterations: 146
currently lose_sum: 368.71582984924316
time_elpased: 1.78
batch start
#iterations: 147
currently lose_sum: 368.4123975932598
time_elpased: 1.777
batch start
#iterations: 148
currently lose_sum: 369.55636978149414
time_elpased: 1.771
batch start
#iterations: 149
currently lose_sum: 367.60085994005203
time_elpased: 1.764
batch start
#iterations: 150
currently lose_sum: 369.37237203121185
time_elpased: 1.775
batch start
#iterations: 151
currently lose_sum: 367.899126291275
time_elpased: 1.767
batch start
#iterations: 152
currently lose_sum: 368.0484815835953
time_elpased: 1.775
batch start
#iterations: 153
currently lose_sum: 367.3428565263748
time_elpased: 1.763
batch start
#iterations: 154
currently lose_sum: 366.79615902900696
time_elpased: 1.771
batch start
#iterations: 155
currently lose_sum: 368.2445538043976
time_elpased: 1.778
batch start
#iterations: 156
currently lose_sum: 367.65393656492233
time_elpased: 1.769
batch start
#iterations: 157
currently lose_sum: 367.5451693534851
time_elpased: 1.769
batch start
#iterations: 158
currently lose_sum: 366.0357194542885
time_elpased: 1.775
batch start
#iterations: 159
currently lose_sum: 367.9839777946472
time_elpased: 1.781
start validation test
0.753762886598
0.730951276102
0.807463604675
0.767304788348
0
validation finish
batch start
#iterations: 160
currently lose_sum: 369.0200979113579
time_elpased: 1.765
batch start
#iterations: 161
currently lose_sum: 367.71557676792145
time_elpased: 1.778
batch start
#iterations: 162
currently lose_sum: 367.9195568561554
time_elpased: 1.766
batch start
#iterations: 163
currently lose_sum: 368.71517127752304
time_elpased: 1.779
batch start
#iterations: 164
currently lose_sum: 368.2002657651901
time_elpased: 1.774
batch start
#iterations: 165
currently lose_sum: 368.4132739305496
time_elpased: 1.782
batch start
#iterations: 166
currently lose_sum: 367.85430014133453
time_elpased: 1.78
batch start
#iterations: 167
currently lose_sum: 367.50977724790573
time_elpased: 1.777
batch start
#iterations: 168
currently lose_sum: 367.55831027030945
time_elpased: 1.78
batch start
#iterations: 169
currently lose_sum: 367.29485362768173
time_elpased: 1.769
batch start
#iterations: 170
currently lose_sum: 367.74527806043625
time_elpased: 1.781
batch start
#iterations: 171
currently lose_sum: 368.076496720314
time_elpased: 1.777
batch start
#iterations: 172
currently lose_sum: 366.48196095228195
time_elpased: 1.775
batch start
#iterations: 173
currently lose_sum: 368.4001351594925
time_elpased: 1.771
batch start
#iterations: 174
currently lose_sum: 367.73329919576645
time_elpased: 1.775
batch start
#iterations: 175
currently lose_sum: 368.2508074641228
time_elpased: 1.774
batch start
#iterations: 176
currently lose_sum: 367.64720076322556
time_elpased: 1.775
batch start
#iterations: 177
currently lose_sum: 367.8031847476959
time_elpased: 1.776
batch start
#iterations: 178
currently lose_sum: 368.49262154102325
time_elpased: 1.768
batch start
#iterations: 179
currently lose_sum: 368.6211126446724
time_elpased: 1.782
start validation test
0.750412371134
0.734440626193
0.788804592987
0.760652496293
0
validation finish
batch start
#iterations: 180
currently lose_sum: 366.6495996117592
time_elpased: 1.772
batch start
#iterations: 181
currently lose_sum: 366.64071476459503
time_elpased: 1.783
batch start
#iterations: 182
currently lose_sum: 367.97282803058624
time_elpased: 1.772
batch start
#iterations: 183
currently lose_sum: 367.0975567102432
time_elpased: 1.778
batch start
#iterations: 184
currently lose_sum: 367.6455389261246
time_elpased: 1.773
batch start
#iterations: 185
currently lose_sum: 366.778236746788
time_elpased: 1.78
batch start
#iterations: 186
currently lose_sum: 366.88940662145615
time_elpased: 1.767
batch start
#iterations: 187
currently lose_sum: 367.2970017194748
time_elpased: 1.774
batch start
#iterations: 188
currently lose_sum: 367.13529604673386
time_elpased: 1.777
batch start
#iterations: 189
currently lose_sum: 368.30022019147873
time_elpased: 1.776
batch start
#iterations: 190
currently lose_sum: 367.3026134967804
time_elpased: 1.787
batch start
#iterations: 191
currently lose_sum: 368.019229888916
time_elpased: 1.77
batch start
#iterations: 192
currently lose_sum: 367.39924812316895
time_elpased: 1.775
batch start
#iterations: 193
currently lose_sum: 366.48850947618484
time_elpased: 1.768
batch start
#iterations: 194
currently lose_sum: 366.6478925347328
time_elpased: 1.779
batch start
#iterations: 195
currently lose_sum: 366.47587168216705
time_elpased: 1.77
batch start
#iterations: 196
currently lose_sum: 369.04618459939957
time_elpased: 1.767
batch start
#iterations: 197
currently lose_sum: 368.7313863635063
time_elpased: 1.77
batch start
#iterations: 198
currently lose_sum: 367.1827557682991
time_elpased: 1.769
batch start
#iterations: 199
currently lose_sum: 368.3089192509651
time_elpased: 1.773
start validation test
0.752731958763
0.740094933643
0.783268402707
0.761069880958
0
validation finish
batch start
#iterations: 200
currently lose_sum: 367.9643243551254
time_elpased: 1.761
batch start
#iterations: 201
currently lose_sum: 367.0319384932518
time_elpased: 1.77
batch start
#iterations: 202
currently lose_sum: 366.81195068359375
time_elpased: 1.777
batch start
#iterations: 203
currently lose_sum: 367.2991794347763
time_elpased: 1.775
batch start
#iterations: 204
currently lose_sum: 368.379274725914
time_elpased: 1.777
batch start
#iterations: 205
currently lose_sum: 366.98913848400116
time_elpased: 1.785
batch start
#iterations: 206
currently lose_sum: 367.3765226006508
time_elpased: 1.764
batch start
#iterations: 207
currently lose_sum: 367.1912024617195
time_elpased: 1.778
batch start
#iterations: 208
currently lose_sum: 368.78039425611496
time_elpased: 1.783
batch start
#iterations: 209
currently lose_sum: 366.61799001693726
time_elpased: 1.772
batch start
#iterations: 210
currently lose_sum: 368.0363031029701
time_elpased: 1.792
batch start
#iterations: 211
currently lose_sum: 367.6879332661629
time_elpased: 1.786
batch start
#iterations: 212
currently lose_sum: 366.91329419612885
time_elpased: 1.783
batch start
#iterations: 213
currently lose_sum: 366.6609618663788
time_elpased: 1.782
batch start
#iterations: 214
currently lose_sum: 367.72413659095764
time_elpased: 1.779
batch start
#iterations: 215
currently lose_sum: 367.8049091696739
time_elpased: 1.789
batch start
#iterations: 216
currently lose_sum: 366.6039699912071
time_elpased: 1.768
batch start
#iterations: 217
currently lose_sum: 368.1614851951599
time_elpased: 1.78
batch start
#iterations: 218
currently lose_sum: 366.6356235742569
time_elpased: 1.771
batch start
#iterations: 219
currently lose_sum: 367.43505012989044
time_elpased: 1.787
start validation test
0.750360824742
0.738282387191
0.779987697355
0.758562241388
0
validation finish
batch start
#iterations: 220
currently lose_sum: 368.29567486047745
time_elpased: 1.775
batch start
#iterations: 221
currently lose_sum: 367.265862762928
time_elpased: 1.793
batch start
#iterations: 222
currently lose_sum: 367.07259821891785
time_elpased: 1.763
batch start
#iterations: 223
currently lose_sum: 364.92608338594437
time_elpased: 1.777
batch start
#iterations: 224
currently lose_sum: 366.95686835050583
time_elpased: 1.778
batch start
#iterations: 225
currently lose_sum: 367.8125885128975
time_elpased: 1.784
batch start
#iterations: 226
currently lose_sum: 367.06825268268585
time_elpased: 1.772
batch start
#iterations: 227
currently lose_sum: 366.7534600496292
time_elpased: 1.777
batch start
#iterations: 228
currently lose_sum: 367.69263565540314
time_elpased: 1.785
batch start
#iterations: 229
currently lose_sum: 367.26203310489655
time_elpased: 1.777
batch start
#iterations: 230
currently lose_sum: 368.3179975748062
time_elpased: 1.79
batch start
#iterations: 231
currently lose_sum: 367.71788811683655
time_elpased: 1.77
batch start
#iterations: 232
currently lose_sum: 368.07231771945953
time_elpased: 1.783
batch start
#iterations: 233
currently lose_sum: 367.213758289814
time_elpased: 1.768
batch start
#iterations: 234
currently lose_sum: 366.0833337903023
time_elpased: 1.77
batch start
#iterations: 235
currently lose_sum: 367.53881961107254
time_elpased: 1.768
batch start
#iterations: 236
currently lose_sum: 367.245496571064
time_elpased: 1.765
batch start
#iterations: 237
currently lose_sum: 367.1884306669235
time_elpased: 1.777
batch start
#iterations: 238
currently lose_sum: 367.7604333162308
time_elpased: 1.761
batch start
#iterations: 239
currently lose_sum: 368.6961042881012
time_elpased: 1.779
start validation test
0.753917525773
0.734949990564
0.798544187
0.765428459119
0
validation finish
batch start
#iterations: 240
currently lose_sum: 368.77074444293976
time_elpased: 1.752
batch start
#iterations: 241
currently lose_sum: 366.8756227493286
time_elpased: 1.803
batch start
#iterations: 242
currently lose_sum: 365.8981748223305
time_elpased: 1.772
batch start
#iterations: 243
currently lose_sum: 368.44493198394775
time_elpased: 1.784
batch start
#iterations: 244
currently lose_sum: 367.11502254009247
time_elpased: 1.785
batch start
#iterations: 245
currently lose_sum: 367.3064270019531
time_elpased: 1.785
batch start
#iterations: 246
currently lose_sum: 367.4426318407059
time_elpased: 1.773
batch start
#iterations: 247
currently lose_sum: 367.52323335409164
time_elpased: 1.769
batch start
#iterations: 248
currently lose_sum: 366.8862727880478
time_elpased: 1.784
batch start
#iterations: 249
currently lose_sum: 366.5737175345421
time_elpased: 1.779
batch start
#iterations: 250
currently lose_sum: 366.4674614071846
time_elpased: 1.786
batch start
#iterations: 251
currently lose_sum: 368.37292659282684
time_elpased: 1.771
batch start
#iterations: 252
currently lose_sum: 367.83196049928665
time_elpased: 1.797
batch start
#iterations: 253
currently lose_sum: 366.7235015630722
time_elpased: 1.781
batch start
#iterations: 254
currently lose_sum: 366.89086294174194
time_elpased: 1.787
batch start
#iterations: 255
currently lose_sum: 367.4738512635231
time_elpased: 1.782
batch start
#iterations: 256
currently lose_sum: 368.01362186670303
time_elpased: 1.776
batch start
#iterations: 257
currently lose_sum: 367.811558842659
time_elpased: 1.792
batch start
#iterations: 258
currently lose_sum: 367.10429006814957
time_elpased: 1.787
batch start
#iterations: 259
currently lose_sum: 365.8353878259659
time_elpased: 1.79
start validation test
0.752268041237
0.74169597499
0.778347344679
0.759579789895
0
validation finish
batch start
#iterations: 260
currently lose_sum: 367.15313309431076
time_elpased: 1.78
batch start
#iterations: 261
currently lose_sum: 367.60984551906586
time_elpased: 1.786
batch start
#iterations: 262
currently lose_sum: 367.48913913965225
time_elpased: 1.774
batch start
#iterations: 263
currently lose_sum: 366.56757831573486
time_elpased: 1.786
batch start
#iterations: 264
currently lose_sum: 365.8238280415535
time_elpased: 1.778
batch start
#iterations: 265
currently lose_sum: 367.27850979566574
time_elpased: 1.771
batch start
#iterations: 266
currently lose_sum: 366.68084102869034
time_elpased: 1.779
batch start
#iterations: 267
currently lose_sum: 368.0256628394127
time_elpased: 1.775
batch start
#iterations: 268
currently lose_sum: 365.4658615589142
time_elpased: 1.788
batch start
#iterations: 269
currently lose_sum: 366.4104592204094
time_elpased: 1.781
batch start
#iterations: 270
currently lose_sum: 365.82000970840454
time_elpased: 1.788
batch start
#iterations: 271
currently lose_sum: 366.19121742248535
time_elpased: 1.781
batch start
#iterations: 272
currently lose_sum: 367.4559180736542
time_elpased: 1.795
batch start
#iterations: 273
currently lose_sum: 366.2914497256279
time_elpased: 1.786
batch start
#iterations: 274
currently lose_sum: 367.62923461198807
time_elpased: 1.788
batch start
#iterations: 275
currently lose_sum: 366.79408073425293
time_elpased: 1.784
batch start
#iterations: 276
currently lose_sum: 367.55932182073593
time_elpased: 1.794
batch start
#iterations: 277
currently lose_sum: 366.63851845264435
time_elpased: 1.79
batch start
#iterations: 278
currently lose_sum: 367.69303745031357
time_elpased: 1.786
batch start
#iterations: 279
currently lose_sum: 366.3389879465103
time_elpased: 1.801
start validation test
0.753453608247
0.73548081478
0.795878613902
0.764488650352
0
validation finish
batch start
#iterations: 280
currently lose_sum: 365.68142664432526
time_elpased: 1.773
batch start
#iterations: 281
currently lose_sum: 366.19026494026184
time_elpased: 1.801
batch start
#iterations: 282
currently lose_sum: 367.19534742832184
time_elpased: 1.786
batch start
#iterations: 283
currently lose_sum: 368.1097391843796
time_elpased: 1.789
batch start
#iterations: 284
currently lose_sum: 366.99666517972946
time_elpased: 1.777
batch start
#iterations: 285
currently lose_sum: 367.3417272567749
time_elpased: 1.793
batch start
#iterations: 286
currently lose_sum: 366.20859640836716
time_elpased: 1.783
batch start
#iterations: 287
currently lose_sum: 366.5960639119148
time_elpased: 1.778
batch start
#iterations: 288
currently lose_sum: 368.0227427482605
time_elpased: 1.794
batch start
#iterations: 289
currently lose_sum: 365.3725274205208
time_elpased: 1.775
batch start
#iterations: 290
currently lose_sum: 366.2075183391571
time_elpased: 1.783
batch start
#iterations: 291
currently lose_sum: 365.6821019053459
time_elpased: 1.769
batch start
#iterations: 292
currently lose_sum: 365.59447079896927
time_elpased: 1.794
batch start
#iterations: 293
currently lose_sum: 366.3709092736244
time_elpased: 1.776
batch start
#iterations: 294
currently lose_sum: 365.4126289486885
time_elpased: 1.792
batch start
#iterations: 295
currently lose_sum: 366.12313091754913
time_elpased: 1.789
batch start
#iterations: 296
currently lose_sum: 366.4135525226593
time_elpased: 1.789
batch start
#iterations: 297
currently lose_sum: 366.2349659204483
time_elpased: 1.799
batch start
#iterations: 298
currently lose_sum: 367.3166569471359
time_elpased: 1.791
batch start
#iterations: 299
currently lose_sum: 366.880593419075
time_elpased: 1.8
start validation test
0.754175257732
0.733708391936
0.802234980521
0.766443018757
0
validation finish
batch start
#iterations: 300
currently lose_sum: 367.66199404001236
time_elpased: 1.773
batch start
#iterations: 301
currently lose_sum: 364.71232628822327
time_elpased: 1.801
batch start
#iterations: 302
currently lose_sum: 367.1032202243805
time_elpased: 1.78
batch start
#iterations: 303
currently lose_sum: 366.50846844911575
time_elpased: 1.79
batch start
#iterations: 304
currently lose_sum: 366.19322979450226
time_elpased: 1.779
batch start
#iterations: 305
currently lose_sum: 367.00869488716125
time_elpased: 1.79
batch start
#iterations: 306
currently lose_sum: 365.43373078107834
time_elpased: 1.781
batch start
#iterations: 307
currently lose_sum: 367.7536364197731
time_elpased: 1.769
batch start
#iterations: 308
currently lose_sum: 368.37347412109375
time_elpased: 1.78
batch start
#iterations: 309
currently lose_sum: 366.85384887456894
time_elpased: 1.767
batch start
#iterations: 310
currently lose_sum: 367.3857636451721
time_elpased: 1.788
batch start
#iterations: 311
currently lose_sum: 365.01041358709335
time_elpased: 1.771
batch start
#iterations: 312
currently lose_sum: 367.1924122571945
time_elpased: 1.791
batch start
#iterations: 313
currently lose_sum: 367.7342122197151
time_elpased: 1.782
batch start
#iterations: 314
currently lose_sum: 365.88820511102676
time_elpased: 1.779
batch start
#iterations: 315
currently lose_sum: 365.7051206231117
time_elpased: 1.768
batch start
#iterations: 316
currently lose_sum: 368.1284380555153
time_elpased: 1.777
batch start
#iterations: 317
currently lose_sum: 365.49015814065933
time_elpased: 1.769
batch start
#iterations: 318
currently lose_sum: 364.71121180057526
time_elpased: 1.771
batch start
#iterations: 319
currently lose_sum: 367.40706300735474
time_elpased: 1.786
start validation test
0.753402061856
0.732416760195
0.802850112774
0.766017802993
0
validation finish
batch start
#iterations: 320
currently lose_sum: 365.8119519352913
time_elpased: 1.752
batch start
#iterations: 321
currently lose_sum: 366.6912913322449
time_elpased: 1.774
batch start
#iterations: 322
currently lose_sum: 367.0975350737572
time_elpased: 1.761
batch start
#iterations: 323
currently lose_sum: 365.50381845235825
time_elpased: 1.772
batch start
#iterations: 324
currently lose_sum: 365.4615715742111
time_elpased: 1.761
batch start
#iterations: 325
currently lose_sum: 365.6611981987953
time_elpased: 1.768
batch start
#iterations: 326
currently lose_sum: 366.28674042224884
time_elpased: 1.774
batch start
#iterations: 327
currently lose_sum: 366.03321969509125
time_elpased: 1.77
batch start
#iterations: 328
currently lose_sum: 366.008108317852
time_elpased: 1.78
batch start
#iterations: 329
currently lose_sum: 366.502645611763
time_elpased: 1.766
batch start
#iterations: 330
currently lose_sum: 365.5770916938782
time_elpased: 1.798
batch start
#iterations: 331
currently lose_sum: 366.4454184770584
time_elpased: 1.772
batch start
#iterations: 332
currently lose_sum: 366.35975992679596
time_elpased: 1.779
batch start
#iterations: 333
currently lose_sum: 366.77306216955185
time_elpased: 1.786
batch start
#iterations: 334
currently lose_sum: 365.8727148771286
time_elpased: 1.788
batch start
#iterations: 335
currently lose_sum: 365.1756488084793
time_elpased: 1.795
batch start
#iterations: 336
currently lose_sum: 364.52667796611786
time_elpased: 1.792
batch start
#iterations: 337
currently lose_sum: 366.4198216199875
time_elpased: 1.798
batch start
#iterations: 338
currently lose_sum: 366.52790170907974
time_elpased: 1.775
batch start
#iterations: 339
currently lose_sum: 366.9744658470154
time_elpased: 1.799
start validation test
0.753917525773
0.737052551409
0.793725651015
0.764340013822
0
validation finish
batch start
#iterations: 340
currently lose_sum: 367.30253607034683
time_elpased: 1.769
batch start
#iterations: 341
currently lose_sum: 364.6370489001274
time_elpased: 1.785
batch start
#iterations: 342
currently lose_sum: 365.1354330778122
time_elpased: 1.777
batch start
#iterations: 343
currently lose_sum: 365.57164973020554
time_elpased: 1.778
batch start
#iterations: 344
currently lose_sum: 366.9082419872284
time_elpased: 1.783
batch start
#iterations: 345
currently lose_sum: 365.35119664669037
time_elpased: 1.778
batch start
#iterations: 346
currently lose_sum: 364.8242822289467
time_elpased: 1.781
batch start
#iterations: 347
currently lose_sum: 365.7936421632767
time_elpased: 1.777
batch start
#iterations: 348
currently lose_sum: 367.2322130203247
time_elpased: 1.78
batch start
#iterations: 349
currently lose_sum: 365.947125017643
time_elpased: 1.783
batch start
#iterations: 350
currently lose_sum: 365.9780215024948
time_elpased: 1.794
batch start
#iterations: 351
currently lose_sum: 366.2221205830574
time_elpased: 1.793
batch start
#iterations: 352
currently lose_sum: 366.2739621400833
time_elpased: 1.794
batch start
#iterations: 353
currently lose_sum: 365.2216314673424
time_elpased: 1.775
batch start
#iterations: 354
currently lose_sum: 365.12816137075424
time_elpased: 1.786
batch start
#iterations: 355
currently lose_sum: 366.04520934820175
time_elpased: 1.777
batch start
#iterations: 356
currently lose_sum: 366.875153362751
time_elpased: 1.769
batch start
#iterations: 357
currently lose_sum: 365.76170712709427
time_elpased: 1.781
batch start
#iterations: 358
currently lose_sum: 366.904921233654
time_elpased: 1.772
batch start
#iterations: 359
currently lose_sum: 365.7889481186867
time_elpased: 1.791
start validation test
0.753556701031
0.734641879777
0.798134098831
0.765072969387
0
validation finish
batch start
#iterations: 360
currently lose_sum: 364.84696316719055
time_elpased: 1.785
batch start
#iterations: 361
currently lose_sum: 367.058580160141
time_elpased: 1.793
batch start
#iterations: 362
currently lose_sum: 365.6254534125328
time_elpased: 1.778
batch start
#iterations: 363
currently lose_sum: 365.9899764060974
time_elpased: 1.793
batch start
#iterations: 364
currently lose_sum: 364.8849662542343
time_elpased: 1.779
batch start
#iterations: 365
currently lose_sum: 366.32088428735733
time_elpased: 1.78
batch start
#iterations: 366
currently lose_sum: 366.3579300045967
time_elpased: 1.78
batch start
#iterations: 367
currently lose_sum: 365.45932772755623
time_elpased: 1.777
batch start
#iterations: 368
currently lose_sum: 366.8431067466736
time_elpased: 1.766
batch start
#iterations: 369
currently lose_sum: 367.26300179958344
time_elpased: 1.764
batch start
#iterations: 370
currently lose_sum: 366.52016592025757
time_elpased: 1.785
batch start
#iterations: 371
currently lose_sum: 365.47567719221115
time_elpased: 1.755
batch start
#iterations: 372
currently lose_sum: 365.7346738576889
time_elpased: 1.775
batch start
#iterations: 373
currently lose_sum: 366.5164834856987
time_elpased: 1.77
batch start
#iterations: 374
currently lose_sum: 365.74729269742966
time_elpased: 1.777
batch start
#iterations: 375
currently lose_sum: 365.86773240566254
time_elpased: 1.771
batch start
#iterations: 376
currently lose_sum: 366.6430394053459
time_elpased: 1.768
batch start
#iterations: 377
currently lose_sum: 366.2062880396843
time_elpased: 1.778
batch start
#iterations: 378
currently lose_sum: 365.91731625795364
time_elpased: 1.78
batch start
#iterations: 379
currently lose_sum: 366.33563446998596
time_elpased: 1.804
start validation test
0.752474226804
0.732400976159
0.799979495592
0.764700117601
0
validation finish
batch start
#iterations: 380
currently lose_sum: 365.8438403606415
time_elpased: 1.781
batch start
#iterations: 381
currently lose_sum: 366.5817726254463
time_elpased: 1.809
batch start
#iterations: 382
currently lose_sum: 366.10635083913803
time_elpased: 1.778
batch start
#iterations: 383
currently lose_sum: 366.0788015127182
time_elpased: 1.789
batch start
#iterations: 384
currently lose_sum: 365.92880898714066
time_elpased: 1.787
batch start
#iterations: 385
currently lose_sum: 365.04390436410904
time_elpased: 1.781
batch start
#iterations: 386
currently lose_sum: 365.7923706769943
time_elpased: 1.775
batch start
#iterations: 387
currently lose_sum: 367.26187521219254
time_elpased: 1.776
batch start
#iterations: 388
currently lose_sum: 365.71325808763504
time_elpased: 1.783
batch start
#iterations: 389
currently lose_sum: 366.2081481218338
time_elpased: 1.781
batch start
#iterations: 390
currently lose_sum: 366.80743873119354
time_elpased: 1.784
batch start
#iterations: 391
currently lose_sum: 365.797257065773
time_elpased: 1.795
batch start
#iterations: 392
currently lose_sum: 366.0209300518036
time_elpased: 1.798
batch start
#iterations: 393
currently lose_sum: 365.67142802476883
time_elpased: 1.785
batch start
#iterations: 394
currently lose_sum: 365.64661890268326
time_elpased: 1.797
batch start
#iterations: 395
currently lose_sum: 365.75724881887436
time_elpased: 1.782
batch start
#iterations: 396
currently lose_sum: 365.6511950492859
time_elpased: 1.774
batch start
#iterations: 397
currently lose_sum: 367.02917367219925
time_elpased: 1.791
batch start
#iterations: 398
currently lose_sum: 366.0306380391121
time_elpased: 1.779
batch start
#iterations: 399
currently lose_sum: 366.0637635588646
time_elpased: 1.785
start validation test
0.755206185567
0.73363831575
0.805618207915
0.767945272416
0
validation finish
acc: 0.749
pre: 0.728
rec: 0.798
F1: 0.762
auc: 0.000
