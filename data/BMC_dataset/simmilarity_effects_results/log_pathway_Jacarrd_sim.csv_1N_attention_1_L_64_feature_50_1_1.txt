start to construct graph
graph construct over
58300
epochs start
batch start
#iterations: 0
currently lose_sum: 401.8408525586128
time_elpased: 1.245
batch start
#iterations: 1
currently lose_sum: 395.74332904815674
time_elpased: 1.21
batch start
#iterations: 2
currently lose_sum: 390.8540972471237
time_elpased: 1.193
batch start
#iterations: 3
currently lose_sum: 388.16447281837463
time_elpased: 1.205
batch start
#iterations: 4
currently lose_sum: 385.9122152328491
time_elpased: 1.203
batch start
#iterations: 5
currently lose_sum: 384.656842648983
time_elpased: 1.203
batch start
#iterations: 6
currently lose_sum: 384.0965645313263
time_elpased: 1.193
batch start
#iterations: 7
currently lose_sum: 382.7249981164932
time_elpased: 1.201
batch start
#iterations: 8
currently lose_sum: 380.4893684387207
time_elpased: 1.214
batch start
#iterations: 9
currently lose_sum: 379.4911249279976
time_elpased: 1.195
batch start
#iterations: 10
currently lose_sum: 379.50151324272156
time_elpased: 1.186
batch start
#iterations: 11
currently lose_sum: 379.4587325453758
time_elpased: 1.206
batch start
#iterations: 12
currently lose_sum: 378.9721758365631
time_elpased: 1.182
batch start
#iterations: 13
currently lose_sum: 378.1813441514969
time_elpased: 1.219
batch start
#iterations: 14
currently lose_sum: 377.7931287288666
time_elpased: 1.21
batch start
#iterations: 15
currently lose_sum: 376.3704968094826
time_elpased: 1.199
batch start
#iterations: 16
currently lose_sum: 375.5549902319908
time_elpased: 1.189
batch start
#iterations: 17
currently lose_sum: 377.70834416151047
time_elpased: 1.198
batch start
#iterations: 18
currently lose_sum: 376.3224402666092
time_elpased: 1.208
batch start
#iterations: 19
currently lose_sum: 376.0086330771446
time_elpased: 1.188
start validation test
0.714432989691
0.766917293233
0.624489795918
0.688413948256
0
validation finish
batch start
#iterations: 20
currently lose_sum: 375.9829047322273
time_elpased: 1.188
batch start
#iterations: 21
currently lose_sum: 375.51931458711624
time_elpased: 1.186
batch start
#iterations: 22
currently lose_sum: 374.5980062484741
time_elpased: 1.187
batch start
#iterations: 23
currently lose_sum: 375.3372074365616
time_elpased: 1.196
batch start
#iterations: 24
currently lose_sum: 374.5852420926094
time_elpased: 1.2
batch start
#iterations: 25
currently lose_sum: 375.75485694408417
time_elpased: 1.2
batch start
#iterations: 26
currently lose_sum: 374.95589727163315
time_elpased: 1.185
batch start
#iterations: 27
currently lose_sum: 374.8646495938301
time_elpased: 1.188
batch start
#iterations: 28
currently lose_sum: 374.8038669228554
time_elpased: 1.202
batch start
#iterations: 29
currently lose_sum: 374.31961941719055
time_elpased: 1.187
batch start
#iterations: 30
currently lose_sum: 373.2574805021286
time_elpased: 1.19
batch start
#iterations: 31
currently lose_sum: 374.36155688762665
time_elpased: 1.216
batch start
#iterations: 32
currently lose_sum: 373.59962862730026
time_elpased: 1.196
batch start
#iterations: 33
currently lose_sum: 373.2742649912834
time_elpased: 1.188
batch start
#iterations: 34
currently lose_sum: 373.9700240492821
time_elpased: 1.206
batch start
#iterations: 35
currently lose_sum: 372.41179901361465
time_elpased: 1.215
batch start
#iterations: 36
currently lose_sum: 372.8018371462822
time_elpased: 1.192
batch start
#iterations: 37
currently lose_sum: 372.93523555994034
time_elpased: 1.207
batch start
#iterations: 38
currently lose_sum: 372.029320538044
time_elpased: 1.199
batch start
#iterations: 39
currently lose_sum: 373.16316336393356
time_elpased: 1.204
start validation test
0.735824742268
0.761611639619
0.694387755102
0.726447824927
0
validation finish
batch start
#iterations: 40
currently lose_sum: 373.0227516889572
time_elpased: 1.206
batch start
#iterations: 41
currently lose_sum: 372.1124817728996
time_elpased: 1.195
batch start
#iterations: 42
currently lose_sum: 372.50310146808624
time_elpased: 1.193
batch start
#iterations: 43
currently lose_sum: 371.01308423280716
time_elpased: 1.187
batch start
#iterations: 44
currently lose_sum: 371.92836993932724
time_elpased: 1.199
batch start
#iterations: 45
currently lose_sum: 372.4017911553383
time_elpased: 1.197
batch start
#iterations: 46
currently lose_sum: 371.31075859069824
time_elpased: 1.188
batch start
#iterations: 47
currently lose_sum: 372.845744907856
time_elpased: 1.204
batch start
#iterations: 48
currently lose_sum: 371.76941126585007
time_elpased: 1.19
batch start
#iterations: 49
currently lose_sum: 372.4323976635933
time_elpased: 1.188
batch start
#iterations: 50
currently lose_sum: 372.5550859570503
time_elpased: 1.209
batch start
#iterations: 51
currently lose_sum: 370.4173496365547
time_elpased: 1.196
batch start
#iterations: 52
currently lose_sum: 371.33910244703293
time_elpased: 1.191
batch start
#iterations: 53
currently lose_sum: 371.6164661645889
time_elpased: 1.171
batch start
#iterations: 54
currently lose_sum: 371.1594625711441
time_elpased: 1.188
batch start
#iterations: 55
currently lose_sum: 371.27805852890015
time_elpased: 1.188
batch start
#iterations: 56
currently lose_sum: 371.2132555246353
time_elpased: 1.188
batch start
#iterations: 57
currently lose_sum: 370.26500034332275
time_elpased: 1.196
batch start
#iterations: 58
currently lose_sum: 371.3169343471527
time_elpased: 1.199
batch start
#iterations: 59
currently lose_sum: 371.4532473683357
time_elpased: 1.198
start validation test
0.746855670103
0.743597409068
0.761428571429
0.752407360726
0
validation finish
batch start
#iterations: 60
currently lose_sum: 371.40186566114426
time_elpased: 1.195
batch start
#iterations: 61
currently lose_sum: 371.6484664082527
time_elpased: 1.183
batch start
#iterations: 62
currently lose_sum: 371.1562812924385
time_elpased: 1.195
batch start
#iterations: 63
currently lose_sum: 370.10365706682205
time_elpased: 1.185
batch start
#iterations: 64
currently lose_sum: 370.5689787864685
time_elpased: 1.198
batch start
#iterations: 65
currently lose_sum: 369.97566825151443
time_elpased: 1.206
batch start
#iterations: 66
currently lose_sum: 370.9741125702858
time_elpased: 1.199
batch start
#iterations: 67
currently lose_sum: 371.11604285240173
time_elpased: 1.2
batch start
#iterations: 68
currently lose_sum: 371.98494613170624
time_elpased: 1.185
batch start
#iterations: 69
currently lose_sum: 371.75188106298447
time_elpased: 1.193
batch start
#iterations: 70
currently lose_sum: 370.14569514989853
time_elpased: 1.203
batch start
#iterations: 71
currently lose_sum: 371.44117814302444
time_elpased: 1.184
batch start
#iterations: 72
currently lose_sum: 370.4273031949997
time_elpased: 1.186
batch start
#iterations: 73
currently lose_sum: 371.13213604688644
time_elpased: 1.189
batch start
#iterations: 74
currently lose_sum: 371.4793708920479
time_elpased: 1.192
batch start
#iterations: 75
currently lose_sum: 371.1187082529068
time_elpased: 1.188
batch start
#iterations: 76
currently lose_sum: 369.2041785120964
time_elpased: 1.235
batch start
#iterations: 77
currently lose_sum: 371.24497425556183
time_elpased: 1.194
batch start
#iterations: 78
currently lose_sum: 369.5741989016533
time_elpased: 1.185
batch start
#iterations: 79
currently lose_sum: 369.64501267671585
time_elpased: 1.201
start validation test
0.749793814433
0.73268724125
0.794591836735
0.762384961817
0
validation finish
batch start
#iterations: 80
currently lose_sum: 369.27730602025986
time_elpased: 1.204
batch start
#iterations: 81
currently lose_sum: 370.36455523967743
time_elpased: 1.201
batch start
#iterations: 82
currently lose_sum: 369.86697751283646
time_elpased: 1.185
batch start
#iterations: 83
currently lose_sum: 369.99127835035324
time_elpased: 1.19
batch start
#iterations: 84
currently lose_sum: 369.8150451183319
time_elpased: 1.195
batch start
#iterations: 85
currently lose_sum: 369.3080156445503
time_elpased: 1.196
batch start
#iterations: 86
currently lose_sum: 369.0110186934471
time_elpased: 1.189
batch start
#iterations: 87
currently lose_sum: 369.72123539447784
time_elpased: 1.21
batch start
#iterations: 88
currently lose_sum: 369.4189547896385
time_elpased: 1.203
batch start
#iterations: 89
currently lose_sum: 370.32752072811127
time_elpased: 1.188
batch start
#iterations: 90
currently lose_sum: 370.95668625831604
time_elpased: 1.199
batch start
#iterations: 91
currently lose_sum: 369.61653542518616
time_elpased: 1.194
batch start
#iterations: 92
currently lose_sum: 371.3783711194992
time_elpased: 1.181
batch start
#iterations: 93
currently lose_sum: 370.8006983399391
time_elpased: 1.188
batch start
#iterations: 94
currently lose_sum: 370.23116582632065
time_elpased: 1.205
batch start
#iterations: 95
currently lose_sum: 370.26641553640366
time_elpased: 1.188
batch start
#iterations: 96
currently lose_sum: 369.80969619750977
time_elpased: 1.199
batch start
#iterations: 97
currently lose_sum: 370.5388017296791
time_elpased: 1.193
batch start
#iterations: 98
currently lose_sum: 369.4334324002266
time_elpased: 1.2
batch start
#iterations: 99
currently lose_sum: 369.0565932393074
time_elpased: 1.189
start validation test
0.744896907216
0.73805083914
0.767346938776
0.752413827605
0
validation finish
batch start
#iterations: 100
currently lose_sum: 369.0320991873741
time_elpased: 1.222
batch start
#iterations: 101
currently lose_sum: 368.08481138944626
time_elpased: 1.196
batch start
#iterations: 102
currently lose_sum: 369.3133507966995
time_elpased: 1.185
batch start
#iterations: 103
currently lose_sum: 369.5597802400589
time_elpased: 1.218
batch start
#iterations: 104
currently lose_sum: 369.1547282934189
time_elpased: 1.195
batch start
#iterations: 105
currently lose_sum: 369.59506261348724
time_elpased: 1.193
batch start
#iterations: 106
currently lose_sum: 370.4587705731392
time_elpased: 1.2
batch start
#iterations: 107
currently lose_sum: 369.0236663222313
time_elpased: 1.203
batch start
#iterations: 108
currently lose_sum: 369.4810127019882
time_elpased: 1.221
batch start
#iterations: 109
currently lose_sum: 369.183258831501
time_elpased: 1.189
batch start
#iterations: 110
currently lose_sum: 370.0659150481224
time_elpased: 1.203
batch start
#iterations: 111
currently lose_sum: 368.87928426265717
time_elpased: 1.19
batch start
#iterations: 112
currently lose_sum: 368.8582964539528
time_elpased: 1.194
batch start
#iterations: 113
currently lose_sum: 369.78115171194077
time_elpased: 1.207
batch start
#iterations: 114
currently lose_sum: 369.55130392313004
time_elpased: 1.195
batch start
#iterations: 115
currently lose_sum: 369.23365890979767
time_elpased: 1.196
batch start
#iterations: 116
currently lose_sum: 368.15449464321136
time_elpased: 1.198
batch start
#iterations: 117
currently lose_sum: 370.05869394540787
time_elpased: 1.201
batch start
#iterations: 118
currently lose_sum: 368.6064509153366
time_elpased: 1.196
batch start
#iterations: 119
currently lose_sum: 368.17449885606766
time_elpased: 1.188
start validation test
0.744484536082
0.744029023481
0.753367346939
0.748669066572
0
validation finish
batch start
#iterations: 120
currently lose_sum: 368.255254983902
time_elpased: 1.191
batch start
#iterations: 121
currently lose_sum: 369.7359760403633
time_elpased: 1.196
batch start
#iterations: 122
currently lose_sum: 368.4919016957283
time_elpased: 1.189
batch start
#iterations: 123
currently lose_sum: 367.66509383916855
time_elpased: 1.189
batch start
#iterations: 124
currently lose_sum: 370.63635951280594
time_elpased: 1.195
batch start
#iterations: 125
currently lose_sum: 369.17458897829056
time_elpased: 1.178
batch start
#iterations: 126
currently lose_sum: 368.8155424594879
time_elpased: 1.188
batch start
#iterations: 127
currently lose_sum: 367.33301669359207
time_elpased: 1.192
batch start
#iterations: 128
currently lose_sum: 368.5366233587265
time_elpased: 1.198
batch start
#iterations: 129
currently lose_sum: 369.53288888931274
time_elpased: 1.21
batch start
#iterations: 130
currently lose_sum: 368.15016436576843
time_elpased: 1.199
batch start
#iterations: 131
currently lose_sum: 368.4906140565872
time_elpased: 1.198
batch start
#iterations: 132
currently lose_sum: 367.6652328968048
time_elpased: 1.2
batch start
#iterations: 133
currently lose_sum: 368.322433590889
time_elpased: 1.221
batch start
#iterations: 134
currently lose_sum: 367.6333075761795
time_elpased: 1.206
batch start
#iterations: 135
currently lose_sum: 369.47885316610336
time_elpased: 1.217
batch start
#iterations: 136
currently lose_sum: 368.6447450518608
time_elpased: 1.194
batch start
#iterations: 137
currently lose_sum: 367.4750093817711
time_elpased: 1.191
batch start
#iterations: 138
currently lose_sum: 369.4284409880638
time_elpased: 1.2
batch start
#iterations: 139
currently lose_sum: 366.93969362974167
time_elpased: 1.199
start validation test
0.748453608247
0.728115727003
0.801224489796
0.762922658375
0
validation finish
batch start
#iterations: 140
currently lose_sum: 367.9055737257004
time_elpased: 1.203
batch start
#iterations: 141
currently lose_sum: 368.04866433143616
time_elpased: 1.203
batch start
#iterations: 142
currently lose_sum: 369.6194452047348
time_elpased: 1.186
batch start
#iterations: 143
currently lose_sum: 367.2249617576599
time_elpased: 1.194
batch start
#iterations: 144
currently lose_sum: 368.0843862295151
time_elpased: 1.2
batch start
#iterations: 145
currently lose_sum: 369.9178791642189
time_elpased: 1.189
batch start
#iterations: 146
currently lose_sum: 368.6495067477226
time_elpased: 1.211
batch start
#iterations: 147
currently lose_sum: 368.8602395057678
time_elpased: 1.2
batch start
#iterations: 148
currently lose_sum: 370.04021269083023
time_elpased: 1.197
batch start
#iterations: 149
currently lose_sum: 367.8778881430626
time_elpased: 1.182
batch start
#iterations: 150
currently lose_sum: 366.91222381591797
time_elpased: 1.184
batch start
#iterations: 151
currently lose_sum: 369.6404637694359
time_elpased: 1.202
batch start
#iterations: 152
currently lose_sum: 368.76200300455093
time_elpased: 1.207
batch start
#iterations: 153
currently lose_sum: 369.772163271904
time_elpased: 1.192
batch start
#iterations: 154
currently lose_sum: 368.48040997982025
time_elpased: 1.203
batch start
#iterations: 155
currently lose_sum: 369.0157309770584
time_elpased: 1.204
batch start
#iterations: 156
currently lose_sum: 369.5669502019882
time_elpased: 1.205
batch start
#iterations: 157
currently lose_sum: 367.7833762764931
time_elpased: 1.204
batch start
#iterations: 158
currently lose_sum: 367.61568331718445
time_elpased: 1.199
batch start
#iterations: 159
currently lose_sum: 368.3013827800751
time_elpased: 1.19
start validation test
0.750360824742
0.734063650959
0.793163265306
0.762469959292
0
validation finish
batch start
#iterations: 160
currently lose_sum: 367.6404587030411
time_elpased: 1.212
batch start
#iterations: 161
currently lose_sum: 369.5048315525055
time_elpased: 1.182
batch start
#iterations: 162
currently lose_sum: 369.2944818139076
time_elpased: 1.195
batch start
#iterations: 163
currently lose_sum: 367.69389778375626
time_elpased: 1.192
batch start
#iterations: 164
currently lose_sum: 367.6915558576584
time_elpased: 1.2
batch start
#iterations: 165
currently lose_sum: 369.052137196064
time_elpased: 1.182
batch start
#iterations: 166
currently lose_sum: 369.5324291586876
time_elpased: 1.193
batch start
#iterations: 167
currently lose_sum: 367.41273748874664
time_elpased: 1.207
batch start
#iterations: 168
currently lose_sum: 367.827295422554
time_elpased: 1.187
batch start
#iterations: 169
currently lose_sum: 368.1203480362892
time_elpased: 1.19
batch start
#iterations: 170
currently lose_sum: 366.64759796857834
time_elpased: 1.2
batch start
#iterations: 171
currently lose_sum: 367.9139531850815
time_elpased: 1.2
batch start
#iterations: 172
currently lose_sum: 368.36623138189316
time_elpased: 1.201
batch start
#iterations: 173
currently lose_sum: 367.18956649303436
time_elpased: 1.191
batch start
#iterations: 174
currently lose_sum: 367.45512050390244
time_elpased: 1.192
batch start
#iterations: 175
currently lose_sum: 367.4136984348297
time_elpased: 1.189
batch start
#iterations: 176
currently lose_sum: 367.9481801390648
time_elpased: 1.19
batch start
#iterations: 177
currently lose_sum: 369.2785712480545
time_elpased: 1.194
batch start
#iterations: 178
currently lose_sum: 367.29164189100266
time_elpased: 1.194
batch start
#iterations: 179
currently lose_sum: 369.5124124288559
time_elpased: 1.209
start validation test
0.746649484536
0.734607626549
0.780408163265
0.756815595468
0
validation finish
batch start
#iterations: 180
currently lose_sum: 368.2905663251877
time_elpased: 1.208
batch start
#iterations: 181
currently lose_sum: 367.7597276568413
time_elpased: 1.197
batch start
#iterations: 182
currently lose_sum: 369.22867596149445
time_elpased: 1.188
batch start
#iterations: 183
currently lose_sum: 366.70238268375397
time_elpased: 1.22
batch start
#iterations: 184
currently lose_sum: 369.39240378141403
time_elpased: 1.201
batch start
#iterations: 185
currently lose_sum: 368.2500007748604
time_elpased: 1.174
batch start
#iterations: 186
currently lose_sum: 367.6722412109375
time_elpased: 1.2
batch start
#iterations: 187
currently lose_sum: 368.07824486494064
time_elpased: 1.177
batch start
#iterations: 188
currently lose_sum: 368.31269586086273
time_elpased: 1.185
batch start
#iterations: 189
currently lose_sum: 369.24746841192245
time_elpased: 1.21
batch start
#iterations: 190
currently lose_sum: 367.7250602841377
time_elpased: 1.206
batch start
#iterations: 191
currently lose_sum: 368.05854547023773
time_elpased: 1.2
batch start
#iterations: 192
currently lose_sum: 368.56876343488693
time_elpased: 1.187
batch start
#iterations: 193
currently lose_sum: 366.9012967348099
time_elpased: 1.209
batch start
#iterations: 194
currently lose_sum: 368.0951027274132
time_elpased: 1.199
batch start
#iterations: 195
currently lose_sum: 367.18594551086426
time_elpased: 1.197
batch start
#iterations: 196
currently lose_sum: 368.6617109775543
time_elpased: 1.196
batch start
#iterations: 197
currently lose_sum: 368.0056891441345
time_elpased: 1.21
batch start
#iterations: 198
currently lose_sum: 368.35079342126846
time_elpased: 1.187
batch start
#iterations: 199
currently lose_sum: 367.76510375738144
time_elpased: 1.198
start validation test
0.742525773196
0.73551612587
0.765612244898
0.750262486876
0
validation finish
batch start
#iterations: 200
currently lose_sum: 367.34972620010376
time_elpased: 1.22
batch start
#iterations: 201
currently lose_sum: 368.0223298072815
time_elpased: 1.193
batch start
#iterations: 202
currently lose_sum: 367.1258170604706
time_elpased: 1.194
batch start
#iterations: 203
currently lose_sum: 367.5599710345268
time_elpased: 1.199
batch start
#iterations: 204
currently lose_sum: 368.4402502775192
time_elpased: 1.185
batch start
#iterations: 205
currently lose_sum: 367.8375539779663
time_elpased: 1.197
batch start
#iterations: 206
currently lose_sum: 367.79712748527527
time_elpased: 1.216
batch start
#iterations: 207
currently lose_sum: 369.16372042894363
time_elpased: 1.197
batch start
#iterations: 208
currently lose_sum: 366.59436053037643
time_elpased: 1.195
batch start
#iterations: 209
currently lose_sum: 368.1338531970978
time_elpased: 1.215
batch start
#iterations: 210
currently lose_sum: 367.3873533010483
time_elpased: 1.201
batch start
#iterations: 211
currently lose_sum: 366.9407761693001
time_elpased: 1.207
batch start
#iterations: 212
currently lose_sum: 367.8844864368439
time_elpased: 1.194
batch start
#iterations: 213
currently lose_sum: 366.99909818172455
time_elpased: 1.209
batch start
#iterations: 214
currently lose_sum: 367.26535201072693
time_elpased: 1.216
batch start
#iterations: 215
currently lose_sum: 368.66040843725204
time_elpased: 1.196
batch start
#iterations: 216
currently lose_sum: 367.50699561834335
time_elpased: 1.201
batch start
#iterations: 217
currently lose_sum: 367.05759632587433
time_elpased: 1.195
batch start
#iterations: 218
currently lose_sum: 368.5957335829735
time_elpased: 1.185
batch start
#iterations: 219
currently lose_sum: 367.4836163520813
time_elpased: 1.193
start validation test
0.744690721649
0.738980376689
0.764693877551
0.751617270949
0
validation finish
batch start
#iterations: 220
currently lose_sum: 367.63992047309875
time_elpased: 1.218
batch start
#iterations: 221
currently lose_sum: 365.6484187245369
time_elpased: 1.185
batch start
#iterations: 222
currently lose_sum: 365.9174643754959
time_elpased: 1.195
batch start
#iterations: 223
currently lose_sum: 367.213226377964
time_elpased: 1.192
batch start
#iterations: 224
currently lose_sum: 367.36992049217224
time_elpased: 1.199
batch start
#iterations: 225
currently lose_sum: 367.0896824002266
time_elpased: 1.199
batch start
#iterations: 226
currently lose_sum: 369.36595195531845
time_elpased: 1.217
batch start
#iterations: 227
currently lose_sum: 368.82223558425903
time_elpased: 1.199
batch start
#iterations: 228
currently lose_sum: 367.2308856844902
time_elpased: 1.192
batch start
#iterations: 229
currently lose_sum: 366.8345703482628
time_elpased: 1.189
batch start
#iterations: 230
currently lose_sum: 367.7344580888748
time_elpased: 1.19
batch start
#iterations: 231
currently lose_sum: 366.892791390419
time_elpased: 1.183
batch start
#iterations: 232
currently lose_sum: 367.654080927372
time_elpased: 1.205
batch start
#iterations: 233
currently lose_sum: 367.9208615422249
time_elpased: 1.215
batch start
#iterations: 234
currently lose_sum: 366.9712845683098
time_elpased: 1.199
batch start
#iterations: 235
currently lose_sum: 367.04683339595795
time_elpased: 1.201
batch start
#iterations: 236
currently lose_sum: 366.3532305955887
time_elpased: 1.21
batch start
#iterations: 237
currently lose_sum: 367.65829330682755
time_elpased: 1.211
batch start
#iterations: 238
currently lose_sum: 367.7376184463501
time_elpased: 1.185
batch start
#iterations: 239
currently lose_sum: 368.176081597805
time_elpased: 1.21
start validation test
0.747371134021
0.734873909291
0.782040816327
0.757724059519
0
validation finish
batch start
#iterations: 240
currently lose_sum: 365.81369411945343
time_elpased: 1.206
batch start
#iterations: 241
currently lose_sum: 367.02138978242874
time_elpased: 1.199
batch start
#iterations: 242
currently lose_sum: 367.2653606534004
time_elpased: 1.195
batch start
#iterations: 243
currently lose_sum: 365.66020065546036
time_elpased: 1.202
batch start
#iterations: 244
currently lose_sum: 366.2829418182373
time_elpased: 1.194
batch start
#iterations: 245
currently lose_sum: 367.31578540802
time_elpased: 1.211
batch start
#iterations: 246
currently lose_sum: 366.64454025030136
time_elpased: 1.208
batch start
#iterations: 247
currently lose_sum: 366.9511696100235
time_elpased: 1.202
batch start
#iterations: 248
currently lose_sum: 368.6483963727951
time_elpased: 1.188
batch start
#iterations: 249
currently lose_sum: 368.0858829021454
time_elpased: 1.199
batch start
#iterations: 250
currently lose_sum: 367.536183655262
time_elpased: 1.208
batch start
#iterations: 251
currently lose_sum: 367.8252304792404
time_elpased: 1.181
batch start
#iterations: 252
currently lose_sum: 367.58029103279114
time_elpased: 1.206
batch start
#iterations: 253
currently lose_sum: 366.95416206121445
time_elpased: 1.2
batch start
#iterations: 254
currently lose_sum: 366.9489786028862
time_elpased: 1.196
batch start
#iterations: 255
currently lose_sum: 366.7939822077751
time_elpased: 1.202
batch start
#iterations: 256
currently lose_sum: 367.09168177843094
time_elpased: 1.209
batch start
#iterations: 257
currently lose_sum: 368.0963770747185
time_elpased: 1.198
batch start
#iterations: 258
currently lose_sum: 365.46809619665146
time_elpased: 1.199
batch start
#iterations: 259
currently lose_sum: 366.5174785256386
time_elpased: 1.219
start validation test
0.74706185567
0.730693069307
0.790714285714
0.759519725557
0
validation finish
batch start
#iterations: 260
currently lose_sum: 366.52248179912567
time_elpased: 1.201
batch start
#iterations: 261
currently lose_sum: 367.0133346915245
time_elpased: 1.188
batch start
#iterations: 262
currently lose_sum: 367.42489928007126
time_elpased: 1.221
batch start
#iterations: 263
currently lose_sum: 366.936164021492
time_elpased: 1.202
batch start
#iterations: 264
currently lose_sum: 367.02290558815
time_elpased: 1.188
batch start
#iterations: 265
currently lose_sum: 368.7970585823059
time_elpased: 1.191
batch start
#iterations: 266
currently lose_sum: 367.23871326446533
time_elpased: 1.203
batch start
#iterations: 267
currently lose_sum: 366.9269204735756
time_elpased: 1.196
batch start
#iterations: 268
currently lose_sum: 367.6493501663208
time_elpased: 1.188
batch start
#iterations: 269
currently lose_sum: 366.42580884695053
time_elpased: 1.195
batch start
#iterations: 270
currently lose_sum: 367.40259206295013
time_elpased: 1.219
batch start
#iterations: 271
currently lose_sum: 367.50336706638336
time_elpased: 1.19
batch start
#iterations: 272
currently lose_sum: 366.8824657201767
time_elpased: 1.196
batch start
#iterations: 273
currently lose_sum: 365.9723881483078
time_elpased: 1.188
batch start
#iterations: 274
currently lose_sum: 367.44999372959137
time_elpased: 1.199
batch start
#iterations: 275
currently lose_sum: 367.62448900938034
time_elpased: 1.208
batch start
#iterations: 276
currently lose_sum: 367.48146533966064
time_elpased: 1.197
batch start
#iterations: 277
currently lose_sum: 367.710466504097
time_elpased: 1.204
batch start
#iterations: 278
currently lose_sum: 366.59603571891785
time_elpased: 1.22
batch start
#iterations: 279
currently lose_sum: 368.9545802474022
time_elpased: 1.211
start validation test
0.743762886598
0.738280864502
0.763367346939
0.750614558772
0
validation finish
batch start
#iterations: 280
currently lose_sum: 367.43849009275436
time_elpased: 1.201
batch start
#iterations: 281
currently lose_sum: 368.06731098890305
time_elpased: 1.2
batch start
#iterations: 282
currently lose_sum: 366.93318128585815
time_elpased: 1.207
batch start
#iterations: 283
currently lose_sum: 367.32841885089874
time_elpased: 1.196
batch start
#iterations: 284
currently lose_sum: 367.19578289985657
time_elpased: 1.189
batch start
#iterations: 285
currently lose_sum: 367.3838651776314
time_elpased: 1.196
batch start
#iterations: 286
currently lose_sum: 366.6449646949768
time_elpased: 1.206
batch start
#iterations: 287
currently lose_sum: 366.69230180978775
time_elpased: 1.195
batch start
#iterations: 288
currently lose_sum: 366.9093804359436
time_elpased: 1.198
batch start
#iterations: 289
currently lose_sum: 366.33412581682205
time_elpased: 1.21
batch start
#iterations: 290
currently lose_sum: 366.1791676878929
time_elpased: 1.191
batch start
#iterations: 291
currently lose_sum: 365.9400119781494
time_elpased: 1.194
batch start
#iterations: 292
currently lose_sum: 367.0225547552109
time_elpased: 1.209
batch start
#iterations: 293
currently lose_sum: 366.1133464574814
time_elpased: 1.194
batch start
#iterations: 294
currently lose_sum: 366.29574048519135
time_elpased: 1.192
batch start
#iterations: 295
currently lose_sum: 366.28316074609756
time_elpased: 1.19
batch start
#iterations: 296
currently lose_sum: 366.5688018798828
time_elpased: 1.197
batch start
#iterations: 297
currently lose_sum: 366.5710458755493
time_elpased: 1.182
batch start
#iterations: 298
currently lose_sum: 366.75784558057785
time_elpased: 1.221
batch start
#iterations: 299
currently lose_sum: 366.2526589035988
time_elpased: 1.211
start validation test
0.747371134021
0.73062800113
0.791836734694
0.760001958768
0
validation finish
batch start
#iterations: 300
currently lose_sum: 367.4695026874542
time_elpased: 1.203
batch start
#iterations: 301
currently lose_sum: 366.56749641895294
time_elpased: 1.21
batch start
#iterations: 302
currently lose_sum: 368.3424832224846
time_elpased: 1.201
batch start
#iterations: 303
currently lose_sum: 367.7187528014183
time_elpased: 1.198
batch start
#iterations: 304
currently lose_sum: 367.1646085381508
time_elpased: 1.194
batch start
#iterations: 305
currently lose_sum: 366.8711282610893
time_elpased: 1.204
batch start
#iterations: 306
currently lose_sum: 365.9761584997177
time_elpased: 1.219
batch start
#iterations: 307
currently lose_sum: 365.4654446840286
time_elpased: 1.2
batch start
#iterations: 308
currently lose_sum: 365.8542250394821
time_elpased: 1.196
batch start
#iterations: 309
currently lose_sum: 366.8747173547745
time_elpased: 1.205
batch start
#iterations: 310
currently lose_sum: 366.2762686610222
time_elpased: 1.19
batch start
#iterations: 311
currently lose_sum: 366.78136360645294
time_elpased: 1.206
batch start
#iterations: 312
currently lose_sum: 365.88407665491104
time_elpased: 1.213
batch start
#iterations: 313
currently lose_sum: 366.25921672582626
time_elpased: 1.191
batch start
#iterations: 314
currently lose_sum: 365.884372651577
time_elpased: 1.207
batch start
#iterations: 315
currently lose_sum: 366.10218131542206
time_elpased: 1.207
batch start
#iterations: 316
currently lose_sum: 365.899123609066
time_elpased: 1.223
batch start
#iterations: 317
currently lose_sum: 366.26162242889404
time_elpased: 1.204
batch start
#iterations: 318
currently lose_sum: 366.1926400065422
time_elpased: 1.219
batch start
#iterations: 319
currently lose_sum: 366.81716948747635
time_elpased: 1.202
start validation test
0.749278350515
0.728815130725
0.802142857143
0.763722918488
0
validation finish
batch start
#iterations: 320
currently lose_sum: 368.9191367030144
time_elpased: 1.205
batch start
#iterations: 321
currently lose_sum: 367.9719990491867
time_elpased: 1.205
batch start
#iterations: 322
currently lose_sum: 366.058747112751
time_elpased: 1.193
batch start
#iterations: 323
currently lose_sum: 366.1474766135216
time_elpased: 1.199
batch start
#iterations: 324
currently lose_sum: 367.52088338136673
time_elpased: 1.207
batch start
#iterations: 325
currently lose_sum: 365.4903001189232
time_elpased: 1.196
batch start
#iterations: 326
currently lose_sum: 367.60163283348083
time_elpased: 1.208
batch start
#iterations: 327
currently lose_sum: 367.3808629512787
time_elpased: 1.204
batch start
#iterations: 328
currently lose_sum: 365.953693985939
time_elpased: 1.207
batch start
#iterations: 329
currently lose_sum: 366.13545149564743
time_elpased: 1.196
batch start
#iterations: 330
currently lose_sum: 367.0044865012169
time_elpased: 1.197
batch start
#iterations: 331
currently lose_sum: 366.51608312129974
time_elpased: 1.212
batch start
#iterations: 332
currently lose_sum: 366.1184072494507
time_elpased: 1.201
batch start
#iterations: 333
currently lose_sum: 365.7305359840393
time_elpased: 1.2
batch start
#iterations: 334
currently lose_sum: 367.1361183524132
time_elpased: 1.199
batch start
#iterations: 335
currently lose_sum: 365.59624511003494
time_elpased: 1.194
batch start
#iterations: 336
currently lose_sum: 366.0133228302002
time_elpased: 1.198
batch start
#iterations: 337
currently lose_sum: 366.20876628160477
time_elpased: 1.195
batch start
#iterations: 338
currently lose_sum: 367.11731588840485
time_elpased: 1.205
batch start
#iterations: 339
currently lose_sum: 365.30400305986404
time_elpased: 1.205
start validation test
0.74675257732
0.731545532076
0.787755102041
0.758610524247
0
validation finish
batch start
#iterations: 340
currently lose_sum: 365.14787751436234
time_elpased: 1.185
batch start
#iterations: 341
currently lose_sum: 367.42905336618423
time_elpased: 1.212
batch start
#iterations: 342
currently lose_sum: 365.81983083486557
time_elpased: 1.202
batch start
#iterations: 343
currently lose_sum: 367.10897052288055
time_elpased: 1.209
batch start
#iterations: 344
currently lose_sum: 365.7900968194008
time_elpased: 1.214
batch start
#iterations: 345
currently lose_sum: 367.0021249651909
time_elpased: 1.199
batch start
#iterations: 346
currently lose_sum: 365.74841034412384
time_elpased: 1.207
batch start
#iterations: 347
currently lose_sum: 365.1719949245453
time_elpased: 1.203
batch start
#iterations: 348
currently lose_sum: 366.82456558942795
time_elpased: 1.21
batch start
#iterations: 349
currently lose_sum: 365.99330258369446
time_elpased: 1.194
batch start
#iterations: 350
currently lose_sum: 367.1162264943123
time_elpased: 1.194
batch start
#iterations: 351
currently lose_sum: 366.0696910619736
time_elpased: 1.211
batch start
#iterations: 352
currently lose_sum: 365.9996809363365
time_elpased: 1.199
batch start
#iterations: 353
currently lose_sum: 365.25644862651825
time_elpased: 1.194
batch start
#iterations: 354
currently lose_sum: 365.8732786178589
time_elpased: 1.227
batch start
#iterations: 355
currently lose_sum: 366.0861155986786
time_elpased: 1.207
batch start
#iterations: 356
currently lose_sum: 365.6785207390785
time_elpased: 1.185
batch start
#iterations: 357
currently lose_sum: 367.0274335741997
time_elpased: 1.215
batch start
#iterations: 358
currently lose_sum: 366.9865251779556
time_elpased: 1.193
batch start
#iterations: 359
currently lose_sum: 367.1156791448593
time_elpased: 1.191
start validation test
0.744793814433
0.737207709617
0.76887755102
0.752709654862
0
validation finish
batch start
#iterations: 360
currently lose_sum: 365.8854325413704
time_elpased: 1.216
batch start
#iterations: 361
currently lose_sum: 366.840718626976
time_elpased: 1.241
batch start
#iterations: 362
currently lose_sum: 367.47844129800797
time_elpased: 1.202
batch start
#iterations: 363
currently lose_sum: 366.34426695108414
time_elpased: 1.186
batch start
#iterations: 364
currently lose_sum: 366.32518714666367
time_elpased: 1.21
batch start
#iterations: 365
currently lose_sum: 367.5087652206421
time_elpased: 1.195
batch start
#iterations: 366
currently lose_sum: 367.9253922700882
time_elpased: 1.259
batch start
#iterations: 367
currently lose_sum: 366.98530077934265
time_elpased: 1.224
batch start
#iterations: 368
currently lose_sum: 366.181014418602
time_elpased: 1.184
batch start
#iterations: 369
currently lose_sum: 364.6307553052902
time_elpased: 1.216
batch start
#iterations: 370
currently lose_sum: 367.2126502394676
time_elpased: 1.2
batch start
#iterations: 371
currently lose_sum: 367.3340508341789
time_elpased: 1.195
batch start
#iterations: 372
currently lose_sum: 366.7211863398552
time_elpased: 1.187
batch start
#iterations: 373
currently lose_sum: 367.28281354904175
time_elpased: 1.189
batch start
#iterations: 374
currently lose_sum: 366.39728140830994
time_elpased: 1.211
batch start
#iterations: 375
currently lose_sum: 366.8328374028206
time_elpased: 1.201
batch start
#iterations: 376
currently lose_sum: 367.12975573539734
time_elpased: 1.184
batch start
#iterations: 377
currently lose_sum: 366.7410699725151
time_elpased: 1.213
batch start
#iterations: 378
currently lose_sum: 366.01427364349365
time_elpased: 1.225
batch start
#iterations: 379
currently lose_sum: 366.6965683102608
time_elpased: 1.201
start validation test
0.74881443299
0.729611333768
0.798775510204
0.762628476789
0
validation finish
batch start
#iterations: 380
currently lose_sum: 364.93838208913803
time_elpased: 1.22
batch start
#iterations: 381
currently lose_sum: 365.6448202729225
time_elpased: 1.204
batch start
#iterations: 382
currently lose_sum: 365.8266575932503
time_elpased: 1.194
batch start
#iterations: 383
currently lose_sum: 364.91680884361267
time_elpased: 1.206
batch start
#iterations: 384
currently lose_sum: 365.40249848365784
time_elpased: 1.197
batch start
#iterations: 385
currently lose_sum: 367.07913315296173
time_elpased: 1.194
batch start
#iterations: 386
currently lose_sum: 365.9901457428932
time_elpased: 1.213
batch start
#iterations: 387
currently lose_sum: 366.09281581640244
time_elpased: 1.199
batch start
#iterations: 388
currently lose_sum: 365.4343487024307
time_elpased: 1.209
batch start
#iterations: 389
currently lose_sum: 367.09261935949326
time_elpased: 1.193
batch start
#iterations: 390
currently lose_sum: 367.81938791275024
time_elpased: 1.213
batch start
#iterations: 391
currently lose_sum: 365.9390527009964
time_elpased: 1.19
batch start
#iterations: 392
currently lose_sum: 364.1965531408787
time_elpased: 1.184
batch start
#iterations: 393
currently lose_sum: 366.0553473830223
time_elpased: 1.197
batch start
#iterations: 394
currently lose_sum: 366.8196544647217
time_elpased: 1.198
batch start
#iterations: 395
currently lose_sum: 366.5237327218056
time_elpased: 1.196
batch start
#iterations: 396
currently lose_sum: 365.98407024145126
time_elpased: 1.187
batch start
#iterations: 397
currently lose_sum: 366.97720515727997
time_elpased: 1.206
batch start
#iterations: 398
currently lose_sum: 365.73113787174225
time_elpased: 1.205
batch start
#iterations: 399
currently lose_sum: 366.4409718513489
time_elpased: 1.19
start validation test
0.747319587629
0.729996243426
0.793163265306
0.760269953052
0
validation finish
acc: 0.749
pre: 0.722
rec: 0.802
F1: 0.760
auc: 0.000
