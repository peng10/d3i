start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 554.6340212225914
time_elpased: 2.403
batch start
#iterations: 1
currently lose_sum: 542.6052894890308
time_elpased: 2.272
batch start
#iterations: 2
currently lose_sum: 536.9527262151241
time_elpased: 2.324
batch start
#iterations: 3
currently lose_sum: 531.7355022132397
time_elpased: 2.342
batch start
#iterations: 4
currently lose_sum: 528.0451261103153
time_elpased: 2.288
batch start
#iterations: 5
currently lose_sum: 525.9778365790844
time_elpased: 2.308
batch start
#iterations: 6
currently lose_sum: 524.799036771059
time_elpased: 2.264
batch start
#iterations: 7
currently lose_sum: 522.8636553883553
time_elpased: 2.415
batch start
#iterations: 8
currently lose_sum: 522.6891747713089
time_elpased: 2.429
batch start
#iterations: 9
currently lose_sum: 524.5496378540993
time_elpased: 2.365
batch start
#iterations: 10
currently lose_sum: 522.5162271261215
time_elpased: 2.283
batch start
#iterations: 11
currently lose_sum: 523.659635335207
time_elpased: 2.266
batch start
#iterations: 12
currently lose_sum: 522.9200124144554
time_elpased: 2.335
batch start
#iterations: 13
currently lose_sum: 520.864377707243
time_elpased: 2.365
batch start
#iterations: 14
currently lose_sum: 521.0174829661846
time_elpased: 2.33
batch start
#iterations: 15
currently lose_sum: 518.4176937639713
time_elpased: 2.378
batch start
#iterations: 16
currently lose_sum: 519.620912283659
time_elpased: 2.341
batch start
#iterations: 17
currently lose_sum: 518.474400639534
time_elpased: 2.406
batch start
#iterations: 18
currently lose_sum: 518.0192964971066
time_elpased: 2.262
batch start
#iterations: 19
currently lose_sum: 518.9706243276596
time_elpased: 2.389
start validation test
0.58412371134
0.884634760705
0.183011985409
0.303281519862
0
validation finish
batch start
#iterations: 20
currently lose_sum: 517.6823819279671
time_elpased: 2.363
batch start
#iterations: 21
currently lose_sum: 519.2738008499146
time_elpased: 2.396
batch start
#iterations: 22
currently lose_sum: 517.4946495890617
time_elpased: 2.405
batch start
#iterations: 23
currently lose_sum: 517.8620201647282
time_elpased: 2.423
batch start
#iterations: 24
currently lose_sum: 517.5754742324352
time_elpased: 2.438
batch start
#iterations: 25
currently lose_sum: 517.2080519497395
time_elpased: 2.391
batch start
#iterations: 26
currently lose_sum: 517.3911434710026
time_elpased: 2.381
batch start
#iterations: 27
currently lose_sum: 516.9348088204861
time_elpased: 2.366
batch start
#iterations: 28
currently lose_sum: 515.2096148133278
time_elpased: 2.359
batch start
#iterations: 29
currently lose_sum: 516.7127685844898
time_elpased: 2.44
batch start
#iterations: 30
currently lose_sum: 514.4884311258793
time_elpased: 2.388
batch start
#iterations: 31
currently lose_sum: 516.1967489719391
time_elpased: 2.383
batch start
#iterations: 32
currently lose_sum: 515.4905290901661
time_elpased: 2.446
batch start
#iterations: 33
currently lose_sum: 514.7793234884739
time_elpased: 2.389
batch start
#iterations: 34
currently lose_sum: 514.2348721623421
time_elpased: 2.347
batch start
#iterations: 35
currently lose_sum: 514.2906684875488
time_elpased: 2.366
batch start
#iterations: 36
currently lose_sum: 514.9495515525341
time_elpased: 2.312
batch start
#iterations: 37
currently lose_sum: 513.4478626251221
time_elpased: 2.358
batch start
#iterations: 38
currently lose_sum: 514.749882042408
time_elpased: 2.355
batch start
#iterations: 39
currently lose_sum: 515.3748578429222
time_elpased: 2.388
start validation test
0.546134020619
0.910602910603
0.0912975508077
0.165956237567
0
validation finish
batch start
#iterations: 40
currently lose_sum: 515.5752937197685
time_elpased: 2.387
batch start
#iterations: 41
currently lose_sum: 515.4772688150406
time_elpased: 2.34
batch start
#iterations: 42
currently lose_sum: 514.0355919003487
time_elpased: 2.389
batch start
#iterations: 43
currently lose_sum: 513.0716284513474
time_elpased: 2.332
batch start
#iterations: 44
currently lose_sum: 515.3818157017231
time_elpased: 2.349
batch start
#iterations: 45
currently lose_sum: 515.4586753547192
time_elpased: 2.339
batch start
#iterations: 46
currently lose_sum: 512.4981804788113
time_elpased: 2.307
batch start
#iterations: 47
currently lose_sum: 513.4361107945442
time_elpased: 2.299
batch start
#iterations: 48
currently lose_sum: 514.0363774299622
time_elpased: 2.338
batch start
#iterations: 49
currently lose_sum: 511.9639438390732
time_elpased: 2.35
batch start
#iterations: 50
currently lose_sum: 514.213537812233
time_elpased: 2.217
batch start
#iterations: 51
currently lose_sum: 513.1929752528667
time_elpased: 2.235
batch start
#iterations: 52
currently lose_sum: 514.5667436122894
time_elpased: 2.297
batch start
#iterations: 53
currently lose_sum: 513.6280982196331
time_elpased: 2.39
batch start
#iterations: 54
currently lose_sum: 512.2877759337425
time_elpased: 2.325
batch start
#iterations: 55
currently lose_sum: 513.540919572115
time_elpased: 2.285
batch start
#iterations: 56
currently lose_sum: 513.5820150077343
time_elpased: 2.341
batch start
#iterations: 57
currently lose_sum: 511.96502140164375
time_elpased: 2.288
batch start
#iterations: 58
currently lose_sum: 514.5686568021774
time_elpased: 2.325
batch start
#iterations: 59
currently lose_sum: 512.5602654516697
time_elpased: 2.348
start validation test
0.570670103093
0.902160101652
0.147993746743
0.254275226072
0
validation finish
batch start
#iterations: 60
currently lose_sum: 512.6668392717838
time_elpased: 2.408
batch start
#iterations: 61
currently lose_sum: 511.9939179420471
time_elpased: 2.324
batch start
#iterations: 62
currently lose_sum: 514.4305020272732
time_elpased: 2.405
batch start
#iterations: 63
currently lose_sum: 513.5470952391624
time_elpased: 2.313
batch start
#iterations: 64
currently lose_sum: 513.6725382506847
time_elpased: 2.308
batch start
#iterations: 65
currently lose_sum: 513.5327193439007
time_elpased: 2.275
batch start
#iterations: 66
currently lose_sum: 512.3438962996006
time_elpased: 2.323
batch start
#iterations: 67
currently lose_sum: 510.898955732584
time_elpased: 2.286
batch start
#iterations: 68
currently lose_sum: 512.722592830658
time_elpased: 2.332
batch start
#iterations: 69
currently lose_sum: 514.600969016552
time_elpased: 2.35
batch start
#iterations: 70
currently lose_sum: 511.3855590522289
time_elpased: 2.292
batch start
#iterations: 71
currently lose_sum: 512.644761800766
time_elpased: 2.343
batch start
#iterations: 72
currently lose_sum: 514.4861300289631
time_elpased: 2.354
batch start
#iterations: 73
currently lose_sum: 511.643665343523
time_elpased: 2.363
batch start
#iterations: 74
currently lose_sum: 513.9071290194988
time_elpased: 2.305
batch start
#iterations: 75
currently lose_sum: 511.90652081370354
time_elpased: 2.29
batch start
#iterations: 76
currently lose_sum: 513.2312007546425
time_elpased: 2.334
batch start
#iterations: 77
currently lose_sum: 512.9918912947178
time_elpased: 2.292
batch start
#iterations: 78
currently lose_sum: 513.1463344693184
time_elpased: 2.338
batch start
#iterations: 79
currently lose_sum: 514.1832800507545
time_elpased: 2.361
start validation test
0.610979381443
0.856545961003
0.25638353309
0.394641854496
0
validation finish
batch start
#iterations: 80
currently lose_sum: 510.88790661096573
time_elpased: 2.355
batch start
#iterations: 81
currently lose_sum: 511.19977885484695
time_elpased: 2.344
batch start
#iterations: 82
currently lose_sum: 511.77709183096886
time_elpased: 2.342
batch start
#iterations: 83
currently lose_sum: 513.0147434473038
time_elpased: 2.278
batch start
#iterations: 84
currently lose_sum: 512.1406479179859
time_elpased: 2.239
batch start
#iterations: 85
currently lose_sum: 510.637866795063
time_elpased: 2.34
batch start
#iterations: 86
currently lose_sum: 512.0005572140217
time_elpased: 2.342
batch start
#iterations: 87
currently lose_sum: 510.21565741300583
time_elpased: 2.266
batch start
#iterations: 88
currently lose_sum: 511.5440683364868
time_elpased: 2.332
batch start
#iterations: 89
currently lose_sum: 511.5853739082813
time_elpased: 2.28
batch start
#iterations: 90
currently lose_sum: 511.1295055747032
time_elpased: 2.305
batch start
#iterations: 91
currently lose_sum: 510.0488950908184
time_elpased: 2.339
batch start
#iterations: 92
currently lose_sum: 512.467998445034
time_elpased: 2.375
batch start
#iterations: 93
currently lose_sum: 510.7851517498493
time_elpased: 2.298
batch start
#iterations: 94
currently lose_sum: 510.7815356552601
time_elpased: 2.31
batch start
#iterations: 95
currently lose_sum: 510.3878021836281
time_elpased: 2.303
batch start
#iterations: 96
currently lose_sum: 511.98368495702744
time_elpased: 2.229
batch start
#iterations: 97
currently lose_sum: 508.8281585276127
time_elpased: 2.344
batch start
#iterations: 98
currently lose_sum: 511.94998225569725
time_elpased: 2.328
batch start
#iterations: 99
currently lose_sum: 512.4338234961033
time_elpased: 2.339
start validation test
0.548092783505
0.90350877193
0.0966128191767
0.174559834291
0
validation finish
batch start
#iterations: 100
currently lose_sum: 511.98569136857986
time_elpased: 2.278
batch start
#iterations: 101
currently lose_sum: 511.47489726543427
time_elpased: 2.368
batch start
#iterations: 102
currently lose_sum: 510.01688876748085
time_elpased: 2.323
batch start
#iterations: 103
currently lose_sum: 512.012265920639
time_elpased: 2.32
batch start
#iterations: 104
currently lose_sum: 511.2714632153511
time_elpased: 2.287
batch start
#iterations: 105
currently lose_sum: 510.71017718315125
time_elpased: 2.352
batch start
#iterations: 106
currently lose_sum: 512.9206508696079
time_elpased: 2.349
batch start
#iterations: 107
currently lose_sum: 509.406818985939
time_elpased: 2.3
batch start
#iterations: 108
currently lose_sum: 509.3322288095951
time_elpased: 2.294
batch start
#iterations: 109
currently lose_sum: 511.46397110819817
time_elpased: 2.279
batch start
#iterations: 110
currently lose_sum: 512.0902552902699
time_elpased: 2.34
batch start
#iterations: 111
currently lose_sum: 514.1369691491127
time_elpased: 2.374
batch start
#iterations: 112
currently lose_sum: 509.4881126880646
time_elpased: 2.365
batch start
#iterations: 113
currently lose_sum: 511.0945528447628
time_elpased: 2.31
batch start
#iterations: 114
currently lose_sum: 510.1168420612812
time_elpased: 2.338
batch start
#iterations: 115
currently lose_sum: 510.7597905397415
time_elpased: 2.296
batch start
#iterations: 116
currently lose_sum: 510.236043125391
time_elpased: 2.37
batch start
#iterations: 117
currently lose_sum: 511.5790905356407
time_elpased: 2.309
batch start
#iterations: 118
currently lose_sum: 511.1692450940609
time_elpased: 2.29
batch start
#iterations: 119
currently lose_sum: 510.8527443110943
time_elpased: 2.335
start validation test
0.579329896907
0.897450110865
0.168733715477
0.284060005264
0
validation finish
batch start
#iterations: 120
currently lose_sum: 511.44167667627335
time_elpased: 2.368
batch start
#iterations: 121
currently lose_sum: 510.0152636766434
time_elpased: 2.29
batch start
#iterations: 122
currently lose_sum: 509.2731663286686
time_elpased: 2.324
batch start
#iterations: 123
currently lose_sum: 509.306021720171
time_elpased: 2.36
batch start
#iterations: 124
currently lose_sum: 508.8368030786514
time_elpased: 2.353
batch start
#iterations: 125
currently lose_sum: 511.3114719390869
time_elpased: 2.345
batch start
#iterations: 126
currently lose_sum: 511.71343889832497
time_elpased: 2.357
batch start
#iterations: 127
currently lose_sum: 511.2039307951927
time_elpased: 2.419
batch start
#iterations: 128
currently lose_sum: 510.28941306471825
time_elpased: 2.41
batch start
#iterations: 129
currently lose_sum: 510.2429112493992
time_elpased: 2.405
batch start
#iterations: 130
currently lose_sum: 510.06428149342537
time_elpased: 2.352
batch start
#iterations: 131
currently lose_sum: 510.45085471868515
time_elpased: 2.344
batch start
#iterations: 132
currently lose_sum: 510.37856593728065
time_elpased: 2.329
batch start
#iterations: 133
currently lose_sum: 511.65395817160606
time_elpased: 2.321
batch start
#iterations: 134
currently lose_sum: 509.5016594827175
time_elpased: 2.316
batch start
#iterations: 135
currently lose_sum: 509.61584535241127
time_elpased: 2.406
batch start
#iterations: 136
currently lose_sum: 510.22977194190025
time_elpased: 2.38
batch start
#iterations: 137
currently lose_sum: 508.4375368654728
time_elpased: 2.383
batch start
#iterations: 138
currently lose_sum: 508.09633031487465
time_elpased: 2.387
batch start
#iterations: 139
currently lose_sum: 510.2076228559017
time_elpased: 2.355
start validation test
0.566340206186
0.89295212766
0.139968733715
0.242003784125
0
validation finish
batch start
#iterations: 140
currently lose_sum: 508.37809309363365
time_elpased: 2.404
batch start
#iterations: 141
currently lose_sum: 509.50001361966133
time_elpased: 2.373
batch start
#iterations: 142
currently lose_sum: 508.9501970112324
time_elpased: 2.374
batch start
#iterations: 143
currently lose_sum: 509.84951388835907
time_elpased: 2.375
batch start
#iterations: 144
currently lose_sum: 508.53763431310654
time_elpased: 2.394
batch start
#iterations: 145
currently lose_sum: 510.06593587994576
time_elpased: 2.332
batch start
#iterations: 146
currently lose_sum: 511.63186687231064
time_elpased: 2.363
batch start
#iterations: 147
currently lose_sum: 510.72826260328293
time_elpased: 2.335
batch start
#iterations: 148
currently lose_sum: 508.32723718881607
time_elpased: 2.345
batch start
#iterations: 149
currently lose_sum: 510.5344080030918
time_elpased: 2.379
batch start
#iterations: 150
currently lose_sum: 509.700973123312
time_elpased: 2.328
batch start
#iterations: 151
currently lose_sum: 508.69149401783943
time_elpased: 2.352
batch start
#iterations: 152
currently lose_sum: 509.5243408679962
time_elpased: 2.373
batch start
#iterations: 153
currently lose_sum: 507.26417222619057
time_elpased: 2.315
batch start
#iterations: 154
currently lose_sum: 512.8733835816383
time_elpased: 2.407
batch start
#iterations: 155
currently lose_sum: 507.2837423682213
time_elpased: 2.342
batch start
#iterations: 156
currently lose_sum: 508.3995820581913
time_elpased: 2.367
batch start
#iterations: 157
currently lose_sum: 509.0538060963154
time_elpased: 2.439
batch start
#iterations: 158
currently lose_sum: 509.7510179877281
time_elpased: 2.345
batch start
#iterations: 159
currently lose_sum: 509.9252499639988
time_elpased: 2.332
start validation test
0.566597938144
0.900742741391
0.13903074518
0.240881184543
0
validation finish
batch start
#iterations: 160
currently lose_sum: 510.80199322104454
time_elpased: 2.379
batch start
#iterations: 161
currently lose_sum: 508.5298599898815
time_elpased: 2.405
batch start
#iterations: 162
currently lose_sum: 508.7371811568737
time_elpased: 2.346
batch start
#iterations: 163
currently lose_sum: 508.42399632930756
time_elpased: 2.407
batch start
#iterations: 164
currently lose_sum: 510.2611058652401
time_elpased: 2.395
batch start
#iterations: 165
currently lose_sum: 511.42555275559425
time_elpased: 2.348
batch start
#iterations: 166
currently lose_sum: 509.48275700211525
time_elpased: 2.353
batch start
#iterations: 167
currently lose_sum: 508.63919973373413
time_elpased: 2.39
batch start
#iterations: 168
currently lose_sum: 508.87207248806953
time_elpased: 2.322
batch start
#iterations: 169
currently lose_sum: 509.8754861354828
time_elpased: 2.36
batch start
#iterations: 170
currently lose_sum: 508.49502366781235
time_elpased: 2.381
batch start
#iterations: 171
currently lose_sum: 508.9008158147335
time_elpased: 2.351
batch start
#iterations: 172
currently lose_sum: 510.3161478936672
time_elpased: 2.361
batch start
#iterations: 173
currently lose_sum: 509.72736102342606
time_elpased: 2.371
batch start
#iterations: 174
currently lose_sum: 509.7205959558487
time_elpased: 2.335
batch start
#iterations: 175
currently lose_sum: 510.00014141201973
time_elpased: 2.369
batch start
#iterations: 176
currently lose_sum: 507.27132922410965
time_elpased: 2.36
batch start
#iterations: 177
currently lose_sum: 510.0682876110077
time_elpased: 2.307
batch start
#iterations: 178
currently lose_sum: 509.99898585677147
time_elpased: 2.345
batch start
#iterations: 179
currently lose_sum: 509.9166061580181
time_elpased: 2.365
start validation test
0.576030927835
0.889647326507
0.163001563314
0.275521888488
0
validation finish
batch start
#iterations: 180
currently lose_sum: 508.5553149878979
time_elpased: 2.387
batch start
#iterations: 181
currently lose_sum: 510.37136086821556
time_elpased: 2.384
batch start
#iterations: 182
currently lose_sum: 510.46166372299194
time_elpased: 2.396
batch start
#iterations: 183
currently lose_sum: 510.76310139894485
time_elpased: 2.38
batch start
#iterations: 184
currently lose_sum: 508.5321570932865
time_elpased: 2.4
batch start
#iterations: 185
currently lose_sum: 509.95440313220024
time_elpased: 2.386
batch start
#iterations: 186
currently lose_sum: 510.02424454689026
time_elpased: 2.369
batch start
#iterations: 187
currently lose_sum: 510.8229933977127
time_elpased: 2.406
batch start
#iterations: 188
currently lose_sum: 508.83131140470505
time_elpased: 2.364
batch start
#iterations: 189
currently lose_sum: 510.21368047595024
time_elpased: 2.342
batch start
#iterations: 190
currently lose_sum: 508.6484540104866
time_elpased: 2.328
batch start
#iterations: 191
currently lose_sum: 510.1874013841152
time_elpased: 2.386
batch start
#iterations: 192
currently lose_sum: 507.78690707683563
time_elpased: 2.404
batch start
#iterations: 193
currently lose_sum: 508.6362042725086
time_elpased: 2.375
batch start
#iterations: 194
currently lose_sum: 510.1268549859524
time_elpased: 2.418
batch start
#iterations: 195
currently lose_sum: 511.5945917069912
time_elpased: 2.372
batch start
#iterations: 196
currently lose_sum: 507.1461801826954
time_elpased: 2.398
batch start
#iterations: 197
currently lose_sum: 511.08057433366776
time_elpased: 2.383
batch start
#iterations: 198
currently lose_sum: 508.60130047798157
time_elpased: 2.409
batch start
#iterations: 199
currently lose_sum: 509.02583119273186
time_elpased: 2.434
start validation test
0.569226804124
0.904575163399
0.1442417926
0.248808988764
0
validation finish
batch start
#iterations: 200
currently lose_sum: 509.3756016790867
time_elpased: 2.401
batch start
#iterations: 201
currently lose_sum: 510.69308161735535
time_elpased: 2.395
batch start
#iterations: 202
currently lose_sum: 507.25328397750854
time_elpased: 2.31
batch start
#iterations: 203
currently lose_sum: 507.98714277148247
time_elpased: 2.336
batch start
#iterations: 204
currently lose_sum: 508.3026424944401
time_elpased: 2.345
batch start
#iterations: 205
currently lose_sum: 508.813860476017
time_elpased: 2.392
batch start
#iterations: 206
currently lose_sum: 510.51538756489754
time_elpased: 2.379
batch start
#iterations: 207
currently lose_sum: 508.7161227762699
time_elpased: 2.357
batch start
#iterations: 208
currently lose_sum: 507.4785422384739
time_elpased: 2.359
batch start
#iterations: 209
currently lose_sum: 510.1813055276871
time_elpased: 2.384
batch start
#iterations: 210
currently lose_sum: 507.9890390634537
time_elpased: 2.408
batch start
#iterations: 211
currently lose_sum: 507.7604919075966
time_elpased: 2.406
batch start
#iterations: 212
currently lose_sum: 507.8011413216591
time_elpased: 2.374
batch start
#iterations: 213
currently lose_sum: 509.169282078743
time_elpased: 2.415
batch start
#iterations: 214
currently lose_sum: 507.98246344923973
time_elpased: 2.372
batch start
#iterations: 215
currently lose_sum: 510.20238357782364
time_elpased: 2.417
batch start
#iterations: 216
currently lose_sum: 509.17570623755455
time_elpased: 2.392
batch start
#iterations: 217
currently lose_sum: 508.91950875520706
time_elpased: 2.413
batch start
#iterations: 218
currently lose_sum: 508.78978338837624
time_elpased: 2.412
batch start
#iterations: 219
currently lose_sum: 510.3903653025627
time_elpased: 2.398
start validation test
0.569536082474
0.892676767677
0.147368421053
0.252974326863
0
validation finish
batch start
#iterations: 220
currently lose_sum: 509.2829975783825
time_elpased: 2.395
batch start
#iterations: 221
currently lose_sum: 510.1649613082409
time_elpased: 2.379
batch start
#iterations: 222
currently lose_sum: 507.3883882164955
time_elpased: 2.388
batch start
#iterations: 223
currently lose_sum: 508.55886802077293
time_elpased: 2.393
batch start
#iterations: 224
currently lose_sum: 508.79646322131157
time_elpased: 2.395
batch start
#iterations: 225
currently lose_sum: 510.8437833786011
time_elpased: 2.408
batch start
#iterations: 226
currently lose_sum: 508.72801715135574
time_elpased: 2.389
batch start
#iterations: 227
currently lose_sum: 508.81598633527756
time_elpased: 2.369
batch start
#iterations: 228
currently lose_sum: 507.4324871599674
time_elpased: 2.386
batch start
#iterations: 229
currently lose_sum: 509.5225875079632
time_elpased: 2.378
batch start
#iterations: 230
currently lose_sum: 510.1180655658245
time_elpased: 2.385
batch start
#iterations: 231
currently lose_sum: 510.56050273776054
time_elpased: 2.395
batch start
#iterations: 232
currently lose_sum: 508.4568483233452
time_elpased: 2.364
batch start
#iterations: 233
currently lose_sum: 507.07295617461205
time_elpased: 2.339
batch start
#iterations: 234
currently lose_sum: 507.6769376695156
time_elpased: 2.415
batch start
#iterations: 235
currently lose_sum: 506.536778986454
time_elpased: 2.429
batch start
#iterations: 236
currently lose_sum: 508.04485389590263
time_elpased: 2.427
batch start
#iterations: 237
currently lose_sum: 509.98863610625267
time_elpased: 2.385
batch start
#iterations: 238
currently lose_sum: 507.0710011422634
time_elpased: 2.408
batch start
#iterations: 239
currently lose_sum: 507.05107378959656
time_elpased: 2.359
start validation test
0.555773195876
0.898125509372
0.114851485149
0.203659212715
0
validation finish
batch start
#iterations: 240
currently lose_sum: 507.98058265447617
time_elpased: 2.411
batch start
#iterations: 241
currently lose_sum: 507.31354197859764
time_elpased: 2.406
batch start
#iterations: 242
currently lose_sum: 507.66425439715385
time_elpased: 2.411
batch start
#iterations: 243
currently lose_sum: 507.5954991579056
time_elpased: 2.378
batch start
#iterations: 244
currently lose_sum: 508.3113540112972
time_elpased: 2.394
batch start
#iterations: 245
currently lose_sum: 506.9772875905037
time_elpased: 2.382
batch start
#iterations: 246
currently lose_sum: 507.5160465836525
time_elpased: 2.375
batch start
#iterations: 247
currently lose_sum: 508.6453921496868
time_elpased: 2.361
batch start
#iterations: 248
currently lose_sum: 507.4812043607235
time_elpased: 2.386
batch start
#iterations: 249
currently lose_sum: 509.48899668455124
time_elpased: 2.395
batch start
#iterations: 250
currently lose_sum: 508.15512838959694
time_elpased: 2.326
batch start
#iterations: 251
currently lose_sum: 509.2674376964569
time_elpased: 2.319
batch start
#iterations: 252
currently lose_sum: 510.94517639279366
time_elpased: 2.312
batch start
#iterations: 253
currently lose_sum: 508.10567757487297
time_elpased: 2.408
batch start
#iterations: 254
currently lose_sum: 510.0696622133255
time_elpased: 2.405
batch start
#iterations: 255
currently lose_sum: 506.5687045156956
time_elpased: 2.42
batch start
#iterations: 256
currently lose_sum: 509.68579587340355
time_elpased: 2.373
batch start
#iterations: 257
currently lose_sum: 507.2447889447212
time_elpased: 2.368
batch start
#iterations: 258
currently lose_sum: 507.8760669827461
time_elpased: 2.39
batch start
#iterations: 259
currently lose_sum: 509.4732957780361
time_elpased: 2.351
start validation test
0.565309278351
0.895238095238
0.137154768108
0.237867148667
0
validation finish
batch start
#iterations: 260
currently lose_sum: 509.27934020757675
time_elpased: 2.382
batch start
#iterations: 261
currently lose_sum: 509.3251915574074
time_elpased: 2.383
batch start
#iterations: 262
currently lose_sum: 508.3659505844116
time_elpased: 2.367
batch start
#iterations: 263
currently lose_sum: 508.25877273082733
time_elpased: 2.375
batch start
#iterations: 264
currently lose_sum: 507.3499031364918
time_elpased: 2.413
batch start
#iterations: 265
currently lose_sum: 508.63254725933075
time_elpased: 2.39
batch start
#iterations: 266
currently lose_sum: 507.43462067842484
time_elpased: 2.401
batch start
#iterations: 267
currently lose_sum: 506.9226031899452
time_elpased: 2.368
batch start
#iterations: 268
currently lose_sum: 508.14982852339745
time_elpased: 2.38
batch start
#iterations: 269
currently lose_sum: 505.62090626358986
time_elpased: 2.384
batch start
#iterations: 270
currently lose_sum: 508.7515517771244
time_elpased: 2.402
batch start
#iterations: 271
currently lose_sum: 505.623674929142
time_elpased: 2.381
batch start
#iterations: 272
currently lose_sum: 508.3095137178898
time_elpased: 2.407
batch start
#iterations: 273
currently lose_sum: 507.0569654107094
time_elpased: 2.365
batch start
#iterations: 274
currently lose_sum: 507.8695552945137
time_elpased: 2.411
batch start
#iterations: 275
currently lose_sum: 507.11357420682907
time_elpased: 2.409
batch start
#iterations: 276
currently lose_sum: 507.88149750232697
time_elpased: 2.422
batch start
#iterations: 277
currently lose_sum: 509.26996329426765
time_elpased: 2.42
batch start
#iterations: 278
currently lose_sum: 507.54114720225334
time_elpased: 2.401
batch start
#iterations: 279
currently lose_sum: 509.23033356666565
time_elpased: 2.391
start validation test
0.560309278351
0.903714935557
0.124231370505
0.218435037566
0
validation finish
batch start
#iterations: 280
currently lose_sum: 507.48563772439957
time_elpased: 2.399
batch start
#iterations: 281
currently lose_sum: 505.9278325140476
time_elpased: 2.402
batch start
#iterations: 282
currently lose_sum: 508.17205768823624
time_elpased: 2.383
batch start
#iterations: 283
currently lose_sum: 508.34670731425285
time_elpased: 2.402
batch start
#iterations: 284
currently lose_sum: 508.1350729763508
time_elpased: 2.407
batch start
#iterations: 285
currently lose_sum: 508.54115572571754
time_elpased: 2.384
batch start
#iterations: 286
currently lose_sum: 509.3455291390419
time_elpased: 2.416
batch start
#iterations: 287
currently lose_sum: 509.9081342816353
time_elpased: 2.408
batch start
#iterations: 288
currently lose_sum: 507.3598973751068
time_elpased: 2.403
batch start
#iterations: 289
currently lose_sum: 507.6558104455471
time_elpased: 2.391
batch start
#iterations: 290
currently lose_sum: 506.889340788126
time_elpased: 2.376
batch start
#iterations: 291
currently lose_sum: 508.8972321152687
time_elpased: 2.41
batch start
#iterations: 292
currently lose_sum: 507.55515843629837
time_elpased: 2.393
batch start
#iterations: 293
currently lose_sum: 508.8374188244343
time_elpased: 2.408
batch start
#iterations: 294
currently lose_sum: 509.2689018249512
time_elpased: 2.396
batch start
#iterations: 295
currently lose_sum: 507.7635644376278
time_elpased: 2.398
batch start
#iterations: 296
currently lose_sum: 507.5873457789421
time_elpased: 2.444
batch start
#iterations: 297
currently lose_sum: 508.1226017177105
time_elpased: 2.402
batch start
#iterations: 298
currently lose_sum: 509.4485053420067
time_elpased: 2.383
batch start
#iterations: 299
currently lose_sum: 509.61017844080925
time_elpased: 2.435
start validation test
0.570618556701
0.884031572556
0.151745700886
0.25902864259
0
validation finish
batch start
#iterations: 300
currently lose_sum: 508.67586773633957
time_elpased: 2.386
batch start
#iterations: 301
currently lose_sum: 507.1939131617546
time_elpased: 2.384
batch start
#iterations: 302
currently lose_sum: 510.2951214313507
time_elpased: 2.385
batch start
#iterations: 303
currently lose_sum: 508.9107179045677
time_elpased: 2.379
batch start
#iterations: 304
currently lose_sum: 506.84954166412354
time_elpased: 2.403
batch start
#iterations: 305
currently lose_sum: 510.6971683204174
time_elpased: 2.414
batch start
#iterations: 306
currently lose_sum: 508.68217861652374
time_elpased: 2.409
batch start
#iterations: 307
currently lose_sum: 510.68069764971733
time_elpased: 2.395
batch start
#iterations: 308
currently lose_sum: 507.70342084765434
time_elpased: 2.288
batch start
#iterations: 309
currently lose_sum: 509.21328896284103
time_elpased: 2.351
batch start
#iterations: 310
currently lose_sum: 506.1108336150646
time_elpased: 2.346
batch start
#iterations: 311
currently lose_sum: 506.90285620093346
time_elpased: 2.391
batch start
#iterations: 312
currently lose_sum: 509.38618528842926
time_elpased: 2.349
batch start
#iterations: 313
currently lose_sum: 509.20990148186684
time_elpased: 2.344
batch start
#iterations: 314
currently lose_sum: 507.12711265683174
time_elpased: 2.374
batch start
#iterations: 315
currently lose_sum: 508.9916470050812
time_elpased: 2.376
batch start
#iterations: 316
currently lose_sum: 508.2658405303955
time_elpased: 2.437
batch start
#iterations: 317
currently lose_sum: 507.44767478108406
time_elpased: 2.394
batch start
#iterations: 318
currently lose_sum: 509.4636075794697
time_elpased: 2.355
batch start
#iterations: 319
currently lose_sum: 507.71087074279785
time_elpased: 2.396
start validation test
0.564329896907
0.896599583622
0.134653465347
0.234142805364
0
validation finish
batch start
#iterations: 320
currently lose_sum: 508.72926408052444
time_elpased: 2.39
batch start
#iterations: 321
currently lose_sum: 509.356330960989
time_elpased: 2.415
batch start
#iterations: 322
currently lose_sum: 505.8580858409405
time_elpased: 2.404
batch start
#iterations: 323
currently lose_sum: 507.9380609691143
time_elpased: 2.424
batch start
#iterations: 324
currently lose_sum: 509.8706028163433
time_elpased: 2.404
batch start
#iterations: 325
currently lose_sum: 508.35931956768036
time_elpased: 2.401
batch start
#iterations: 326
currently lose_sum: 508.35816034674644
time_elpased: 2.393
batch start
#iterations: 327
currently lose_sum: 508.74500158429146
time_elpased: 2.412
batch start
#iterations: 328
currently lose_sum: 508.1733774840832
time_elpased: 2.401
batch start
#iterations: 329
currently lose_sum: 507.01787745952606
time_elpased: 2.398
batch start
#iterations: 330
currently lose_sum: 509.8383930325508
time_elpased: 2.375
batch start
#iterations: 331
currently lose_sum: 509.6794196367264
time_elpased: 2.38
batch start
#iterations: 332
currently lose_sum: 510.42945897579193
time_elpased: 2.376
batch start
#iterations: 333
currently lose_sum: 508.20276057720184
time_elpased: 2.367
batch start
#iterations: 334
currently lose_sum: 508.5398492515087
time_elpased: 2.377
batch start
#iterations: 335
currently lose_sum: 508.609012812376
time_elpased: 2.383
batch start
#iterations: 336
currently lose_sum: 509.4705802202225
time_elpased: 2.422
batch start
#iterations: 337
currently lose_sum: 508.32575383782387
time_elpased: 2.417
batch start
#iterations: 338
currently lose_sum: 506.74532583355904
time_elpased: 2.386
batch start
#iterations: 339
currently lose_sum: 508.61447912454605
time_elpased: 2.359
start validation test
0.585670103093
0.8933804952
0.18426263679
0.305512355279
0
validation finish
batch start
#iterations: 340
currently lose_sum: 506.7315456569195
time_elpased: 2.386
batch start
#iterations: 341
currently lose_sum: 508.8312392234802
time_elpased: 2.389
batch start
#iterations: 342
currently lose_sum: 510.17158618569374
time_elpased: 2.358
batch start
#iterations: 343
currently lose_sum: 509.24937829375267
time_elpased: 2.338
batch start
#iterations: 344
currently lose_sum: 509.5854442715645
time_elpased: 2.353
batch start
#iterations: 345
currently lose_sum: 508.97432258725166
time_elpased: 2.348
batch start
#iterations: 346
currently lose_sum: 507.6529752910137
time_elpased: 2.33
batch start
#iterations: 347
currently lose_sum: 506.56064891815186
time_elpased: 2.395
batch start
#iterations: 348
currently lose_sum: 508.97484347224236
time_elpased: 2.394
batch start
#iterations: 349
currently lose_sum: 509.3471767306328
time_elpased: 2.389
batch start
#iterations: 350
currently lose_sum: 508.64416536688805
time_elpased: 2.388
batch start
#iterations: 351
currently lose_sum: 507.8546657860279
time_elpased: 2.416
batch start
#iterations: 352
currently lose_sum: 507.23932841420174
time_elpased: 2.397
batch start
#iterations: 353
currently lose_sum: 509.3797837495804
time_elpased: 2.399
batch start
#iterations: 354
currently lose_sum: 506.9926206767559
time_elpased: 2.409
batch start
#iterations: 355
currently lose_sum: 507.6726266145706
time_elpased: 2.341
batch start
#iterations: 356
currently lose_sum: 506.5927774608135
time_elpased: 2.4
batch start
#iterations: 357
currently lose_sum: 508.40730717778206
time_elpased: 2.309
batch start
#iterations: 358
currently lose_sum: 509.1798152923584
time_elpased: 2.379
batch start
#iterations: 359
currently lose_sum: 507.3502966463566
time_elpased: 2.397
start validation test
0.573762886598
0.89605734767
0.156331422616
0.266217055639
0
validation finish
batch start
#iterations: 360
currently lose_sum: 508.03413796424866
time_elpased: 2.426
batch start
#iterations: 361
currently lose_sum: 507.122099339962
time_elpased: 2.401
batch start
#iterations: 362
currently lose_sum: 507.1512562036514
time_elpased: 2.38
batch start
#iterations: 363
currently lose_sum: 508.2109846174717
time_elpased: 2.421
batch start
#iterations: 364
currently lose_sum: 509.0967909395695
time_elpased: 2.408
batch start
#iterations: 365
currently lose_sum: 507.19129115343094
time_elpased: 2.437
batch start
#iterations: 366
currently lose_sum: 507.5867296755314
time_elpased: 2.421
batch start
#iterations: 367
currently lose_sum: 508.40431612730026
time_elpased: 2.422
batch start
#iterations: 368
currently lose_sum: 506.6458425819874
time_elpased: 2.402
batch start
#iterations: 369
currently lose_sum: 506.9734607934952
time_elpased: 2.422
batch start
#iterations: 370
currently lose_sum: 506.00563022494316
time_elpased: 2.381
batch start
#iterations: 371
currently lose_sum: 507.19590550661087
time_elpased: 2.402
batch start
#iterations: 372
currently lose_sum: 507.2860387265682
time_elpased: 2.411
batch start
#iterations: 373
currently lose_sum: 507.16517174243927
time_elpased: 2.426
batch start
#iterations: 374
currently lose_sum: 508.4913449883461
time_elpased: 2.429
batch start
#iterations: 375
currently lose_sum: 508.78746715188026
time_elpased: 2.43
batch start
#iterations: 376
currently lose_sum: 508.15976682305336
time_elpased: 2.338
batch start
#iterations: 377
currently lose_sum: 506.97959676384926
time_elpased: 2.395
batch start
#iterations: 378
currently lose_sum: 508.4031102657318
time_elpased: 2.364
batch start
#iterations: 379
currently lose_sum: 509.1684410870075
time_elpased: 2.44
start validation test
0.580154639175
0.891468682505
0.172068785826
0.288459858478
0
validation finish
batch start
#iterations: 380
currently lose_sum: 508.0569334626198
time_elpased: 2.444
batch start
#iterations: 381
currently lose_sum: 507.40254443883896
time_elpased: 2.431
batch start
#iterations: 382
currently lose_sum: 508.37424981594086
time_elpased: 2.366
batch start
#iterations: 383
currently lose_sum: 505.60839280486107
time_elpased: 2.421
batch start
#iterations: 384
currently lose_sum: 507.0009776651859
time_elpased: 2.401
batch start
#iterations: 385
currently lose_sum: 506.06528800725937
time_elpased: 2.367
batch start
#iterations: 386
currently lose_sum: 508.0799609720707
time_elpased: 2.307
batch start
#iterations: 387
currently lose_sum: 507.3049882054329
time_elpased: 2.383
batch start
#iterations: 388
currently lose_sum: 506.9467341005802
time_elpased: 2.36
batch start
#iterations: 389
currently lose_sum: 507.29139545559883
time_elpased: 2.419
batch start
#iterations: 390
currently lose_sum: 506.9512048959732
time_elpased: 2.384
batch start
#iterations: 391
currently lose_sum: 508.2336068749428
time_elpased: 2.447
batch start
#iterations: 392
currently lose_sum: 506.85631734132767
time_elpased: 2.389
batch start
#iterations: 393
currently lose_sum: 506.9562195241451
time_elpased: 2.39
batch start
#iterations: 394
currently lose_sum: 506.280467659235
time_elpased: 2.411
batch start
#iterations: 395
currently lose_sum: 507.3394849598408
time_elpased: 2.395
batch start
#iterations: 396
currently lose_sum: 508.1449700295925
time_elpased: 2.404
batch start
#iterations: 397
currently lose_sum: 509.16343903541565
time_elpased: 2.412
batch start
#iterations: 398
currently lose_sum: 506.30930694937706
time_elpased: 2.38
batch start
#iterations: 399
currently lose_sum: 506.69710820913315
time_elpased: 2.401
start validation test
0.563453608247
0.897038081805
0.132569046378
0.230999727595
0
validation finish
acc: 0.609
pre: 0.862
rec: 0.266
F1: 0.406
auc: 0.000
