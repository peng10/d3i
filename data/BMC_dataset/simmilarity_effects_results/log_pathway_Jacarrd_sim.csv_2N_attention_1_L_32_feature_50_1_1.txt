start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 555.1637851595879
time_elpased: 2.703
batch start
#iterations: 1
currently lose_sum: 543.5087872743607
time_elpased: 2.693
batch start
#iterations: 2
currently lose_sum: 538.358302116394
time_elpased: 2.705
batch start
#iterations: 3
currently lose_sum: 532.5185711979866
time_elpased: 2.7
batch start
#iterations: 4
currently lose_sum: 529.4418013393879
time_elpased: 2.718
batch start
#iterations: 5
currently lose_sum: 529.358190625906
time_elpased: 2.703
batch start
#iterations: 6
currently lose_sum: 523.6435146331787
time_elpased: 2.699
batch start
#iterations: 7
currently lose_sum: 525.2639682590961
time_elpased: 2.71
batch start
#iterations: 8
currently lose_sum: 522.7422118782997
time_elpased: 2.712
batch start
#iterations: 9
currently lose_sum: 523.2903080880642
time_elpased: 2.705
batch start
#iterations: 10
currently lose_sum: 522.2222202420235
time_elpased: 2.688
batch start
#iterations: 11
currently lose_sum: 525.0586407482624
time_elpased: 2.695
batch start
#iterations: 12
currently lose_sum: 520.5797129869461
time_elpased: 2.697
batch start
#iterations: 13
currently lose_sum: 521.0107060968876
time_elpased: 2.704
batch start
#iterations: 14
currently lose_sum: 518.8508596420288
time_elpased: 2.705
batch start
#iterations: 15
currently lose_sum: 519.4045183956623
time_elpased: 2.695
batch start
#iterations: 16
currently lose_sum: 520.17588570714
time_elpased: 2.705
batch start
#iterations: 17
currently lose_sum: 517.5161572396755
time_elpased: 2.707
batch start
#iterations: 18
currently lose_sum: 518.3761238157749
time_elpased: 2.707
batch start
#iterations: 19
currently lose_sum: 515.6680037677288
time_elpased: 2.703
start validation test
0.576804123711
0.861035422343
0.193469387755
0.31594734211
0
validation finish
batch start
#iterations: 20
currently lose_sum: 517.6368616223335
time_elpased: 2.705
batch start
#iterations: 21
currently lose_sum: 519.1575937271118
time_elpased: 2.712
batch start
#iterations: 22
currently lose_sum: 517.8804287314415
time_elpased: 2.683
batch start
#iterations: 23
currently lose_sum: 515.8010400533676
time_elpased: 2.702
batch start
#iterations: 24
currently lose_sum: 516.6369498074055
time_elpased: 2.713
batch start
#iterations: 25
currently lose_sum: 516.1800868213177
time_elpased: 2.73
batch start
#iterations: 26
currently lose_sum: 515.2119608223438
time_elpased: 2.738
batch start
#iterations: 27
currently lose_sum: 516.4070695340633
time_elpased: 2.737
batch start
#iterations: 28
currently lose_sum: 515.7538216412067
time_elpased: 2.729
batch start
#iterations: 29
currently lose_sum: 516.0002883970737
time_elpased: 2.733
batch start
#iterations: 30
currently lose_sum: 513.833080470562
time_elpased: 2.749
batch start
#iterations: 31
currently lose_sum: 514.8496437966824
time_elpased: 2.732
batch start
#iterations: 32
currently lose_sum: 516.0400631427765
time_elpased: 2.731
batch start
#iterations: 33
currently lose_sum: 514.5749774873257
time_elpased: 2.724
batch start
#iterations: 34
currently lose_sum: 514.7094134688377
time_elpased: 2.718
batch start
#iterations: 35
currently lose_sum: 513.2022870182991
time_elpased: 2.719
batch start
#iterations: 36
currently lose_sum: 514.5342545211315
time_elpased: 2.732
batch start
#iterations: 37
currently lose_sum: 514.0558315217495
time_elpased: 2.725
batch start
#iterations: 38
currently lose_sum: 515.4023723900318
time_elpased: 2.722
batch start
#iterations: 39
currently lose_sum: 514.8691969513893
time_elpased: 2.724
start validation test
0.555206185567
0.860307692308
0.142653061224
0.244726477024
0
validation finish
batch start
#iterations: 40
currently lose_sum: 513.788219422102
time_elpased: 2.733
batch start
#iterations: 41
currently lose_sum: 514.241820961237
time_elpased: 2.723
batch start
#iterations: 42
currently lose_sum: 513.2177217900753
time_elpased: 2.713
batch start
#iterations: 43
currently lose_sum: 513.8339217305183
time_elpased: 2.725
batch start
#iterations: 44
currently lose_sum: 511.4562922716141
time_elpased: 2.719
batch start
#iterations: 45
currently lose_sum: 513.2067119777203
time_elpased: 2.719
batch start
#iterations: 46
currently lose_sum: 513.2616608142853
time_elpased: 2.733
batch start
#iterations: 47
currently lose_sum: 513.5827853679657
time_elpased: 2.713
batch start
#iterations: 48
currently lose_sum: 510.82185384631157
time_elpased: 2.717
batch start
#iterations: 49
currently lose_sum: 512.3325609266758
time_elpased: 2.732
batch start
#iterations: 50
currently lose_sum: 515.3246464729309
time_elpased: 2.719
batch start
#iterations: 51
currently lose_sum: 513.6763406991959
time_elpased: 2.717
batch start
#iterations: 52
currently lose_sum: 513.3862238824368
time_elpased: 2.736
batch start
#iterations: 53
currently lose_sum: 513.8299077749252
time_elpased: 2.726
batch start
#iterations: 54
currently lose_sum: 512.9964165985584
time_elpased: 2.723
batch start
#iterations: 55
currently lose_sum: 512.4535712301731
time_elpased: 2.738
batch start
#iterations: 56
currently lose_sum: 513.6161009371281
time_elpased: 2.72
batch start
#iterations: 57
currently lose_sum: 511.2669647336006
time_elpased: 2.718
batch start
#iterations: 58
currently lose_sum: 512.432845890522
time_elpased: 2.709
batch start
#iterations: 59
currently lose_sum: 510.4927417039871
time_elpased: 2.738
start validation test
0.551958762887
0.884722222222
0.13
0.226690391459
0
validation finish
batch start
#iterations: 60
currently lose_sum: 511.8528665006161
time_elpased: 2.739
batch start
#iterations: 61
currently lose_sum: 513.0575485825539
time_elpased: 2.741
batch start
#iterations: 62
currently lose_sum: 514.1611476838589
time_elpased: 2.727
batch start
#iterations: 63
currently lose_sum: 512.2161967158318
time_elpased: 2.743
batch start
#iterations: 64
currently lose_sum: 510.8646784424782
time_elpased: 2.747
batch start
#iterations: 65
currently lose_sum: 513.1683198213577
time_elpased: 2.747
batch start
#iterations: 66
currently lose_sum: 512.008325368166
time_elpased: 2.748
batch start
#iterations: 67
currently lose_sum: 511.3867356479168
time_elpased: 2.727
batch start
#iterations: 68
currently lose_sum: 514.5086661875248
time_elpased: 2.745
batch start
#iterations: 69
currently lose_sum: 511.85050132870674
time_elpased: 2.753
batch start
#iterations: 70
currently lose_sum: 511.26906913518906
time_elpased: 2.732
batch start
#iterations: 71
currently lose_sum: 510.99683144688606
time_elpased: 2.744
batch start
#iterations: 72
currently lose_sum: 512.9587686955929
time_elpased: 2.747
batch start
#iterations: 73
currently lose_sum: 511.90851560235023
time_elpased: 2.728
batch start
#iterations: 74
currently lose_sum: 511.3064983189106
time_elpased: 2.736
batch start
#iterations: 75
currently lose_sum: 512.2587457299232
time_elpased: 2.742
batch start
#iterations: 76
currently lose_sum: 513.4062110185623
time_elpased: 2.736
batch start
#iterations: 77
currently lose_sum: 511.920418292284
time_elpased: 2.726
batch start
#iterations: 78
currently lose_sum: 510.10610941052437
time_elpased: 2.738
batch start
#iterations: 79
currently lose_sum: 511.18106958270073
time_elpased: 2.751
start validation test
0.610515463918
0.858925143954
0.273979591837
0.41544174532
0
validation finish
batch start
#iterations: 80
currently lose_sum: 510.3575809895992
time_elpased: 2.724
batch start
#iterations: 81
currently lose_sum: 511.2572281360626
time_elpased: 2.755
batch start
#iterations: 82
currently lose_sum: 511.5509640574455
time_elpased: 2.734
batch start
#iterations: 83
currently lose_sum: 511.14660170674324
time_elpased: 2.735
batch start
#iterations: 84
currently lose_sum: 510.70916163921356
time_elpased: 2.745
batch start
#iterations: 85
currently lose_sum: 511.39283376932144
time_elpased: 2.741
batch start
#iterations: 86
currently lose_sum: 511.8328268826008
time_elpased: 2.725
batch start
#iterations: 87
currently lose_sum: 510.83219996094704
time_elpased: 2.716
batch start
#iterations: 88
currently lose_sum: 509.6521931886673
time_elpased: 2.724
batch start
#iterations: 89
currently lose_sum: 511.6489197611809
time_elpased: 2.713
batch start
#iterations: 90
currently lose_sum: 510.27012330293655
time_elpased: 2.726
batch start
#iterations: 91
currently lose_sum: 508.57944229245186
time_elpased: 2.736
batch start
#iterations: 92
currently lose_sum: 511.4916324019432
time_elpased: 2.733
batch start
#iterations: 93
currently lose_sum: 510.75072172284126
time_elpased: 2.737
batch start
#iterations: 94
currently lose_sum: 510.0895209312439
time_elpased: 2.75
batch start
#iterations: 95
currently lose_sum: 509.74910044670105
time_elpased: 2.736
batch start
#iterations: 96
currently lose_sum: 512.6660646796227
time_elpased: 2.722
batch start
#iterations: 97
currently lose_sum: 509.7009920179844
time_elpased: 2.74
batch start
#iterations: 98
currently lose_sum: 510.89137628674507
time_elpased: 2.733
batch start
#iterations: 99
currently lose_sum: 510.29391571879387
time_elpased: 2.723
start validation test
0.536237113402
0.870729455217
0.0962244897959
0.173297803914
0
validation finish
batch start
#iterations: 100
currently lose_sum: 511.94604843854904
time_elpased: 2.727
batch start
#iterations: 101
currently lose_sum: 511.16262462735176
time_elpased: 2.742
batch start
#iterations: 102
currently lose_sum: 509.71603813767433
time_elpased: 2.735
batch start
#iterations: 103
currently lose_sum: 509.469253718853
time_elpased: 2.729
batch start
#iterations: 104
currently lose_sum: 511.7922253906727
time_elpased: 2.728
batch start
#iterations: 105
currently lose_sum: 510.9682874381542
time_elpased: 2.731
batch start
#iterations: 106
currently lose_sum: 511.5828710794449
time_elpased: 2.721
batch start
#iterations: 107
currently lose_sum: 509.8048244714737
time_elpased: 2.745
batch start
#iterations: 108
currently lose_sum: 509.97734370827675
time_elpased: 2.731
batch start
#iterations: 109
currently lose_sum: 511.0768772363663
time_elpased: 2.743
batch start
#iterations: 110
currently lose_sum: 510.3452197313309
time_elpased: 2.734
batch start
#iterations: 111
currently lose_sum: 513.5194892883301
time_elpased: 2.725
batch start
#iterations: 112
currently lose_sum: 509.4008867442608
time_elpased: 2.71
batch start
#iterations: 113
currently lose_sum: 509.5790778696537
time_elpased: 2.721
batch start
#iterations: 114
currently lose_sum: 511.8325633108616
time_elpased: 2.72
batch start
#iterations: 115
currently lose_sum: 510.1611310839653
time_elpased: 2.708
batch start
#iterations: 116
currently lose_sum: 509.0342611372471
time_elpased: 2.739
batch start
#iterations: 117
currently lose_sum: 509.6694341003895
time_elpased: 2.738
batch start
#iterations: 118
currently lose_sum: 511.2034018933773
time_elpased: 2.741
batch start
#iterations: 119
currently lose_sum: 509.48605075478554
time_elpased: 2.729
start validation test
0.566597938144
0.875809935205
0.165510204082
0.278407140405
0
validation finish
batch start
#iterations: 120
currently lose_sum: 509.7170202732086
time_elpased: 2.734
batch start
#iterations: 121
currently lose_sum: 507.9941458404064
time_elpased: 2.723
batch start
#iterations: 122
currently lose_sum: 509.25800505280495
time_elpased: 2.707
batch start
#iterations: 123
currently lose_sum: 511.2901918888092
time_elpased: 2.719
batch start
#iterations: 124
currently lose_sum: 510.1043699681759
time_elpased: 2.726
batch start
#iterations: 125
currently lose_sum: 510.9503819644451
time_elpased: 2.73
batch start
#iterations: 126
currently lose_sum: 508.53608882427216
time_elpased: 2.751
batch start
#iterations: 127
currently lose_sum: 512.0243061184883
time_elpased: 2.74
batch start
#iterations: 128
currently lose_sum: 510.4050586819649
time_elpased: 2.726
batch start
#iterations: 129
currently lose_sum: 509.95698842406273
time_elpased: 2.746
batch start
#iterations: 130
currently lose_sum: 508.76486948132515
time_elpased: 2.734
batch start
#iterations: 131
currently lose_sum: 509.9537748992443
time_elpased: 2.741
batch start
#iterations: 132
currently lose_sum: 509.60018837451935
time_elpased: 2.752
batch start
#iterations: 133
currently lose_sum: 512.1147728860378
time_elpased: 2.753
batch start
#iterations: 134
currently lose_sum: 509.88077557086945
time_elpased: 2.736
batch start
#iterations: 135
currently lose_sum: 507.2923275232315
time_elpased: 2.746
batch start
#iterations: 136
currently lose_sum: 508.73577934503555
time_elpased: 2.747
batch start
#iterations: 137
currently lose_sum: 508.512989372015
time_elpased: 2.744
batch start
#iterations: 138
currently lose_sum: 509.6461961865425
time_elpased: 2.73
batch start
#iterations: 139
currently lose_sum: 509.4816921055317
time_elpased: 2.726
start validation test
0.551855670103
0.865740740741
0.133571428571
0.231435643564
0
validation finish
batch start
#iterations: 140
currently lose_sum: 506.5854393541813
time_elpased: 2.719
batch start
#iterations: 141
currently lose_sum: 509.23700124025345
time_elpased: 2.723
batch start
#iterations: 142
currently lose_sum: 509.770335406065
time_elpased: 2.754
batch start
#iterations: 143
currently lose_sum: 510.13701862096786
time_elpased: 2.736
batch start
#iterations: 144
currently lose_sum: 509.9199021756649
time_elpased: 2.71
batch start
#iterations: 145
currently lose_sum: 508.6423531770706
time_elpased: 2.736
batch start
#iterations: 146
currently lose_sum: 511.0977544784546
time_elpased: 2.758
batch start
#iterations: 147
currently lose_sum: 510.4095222055912
time_elpased: 2.74
batch start
#iterations: 148
currently lose_sum: 508.0974651873112
time_elpased: 2.76
batch start
#iterations: 149
currently lose_sum: 510.7817767560482
time_elpased: 2.76
batch start
#iterations: 150
currently lose_sum: 508.12574991583824
time_elpased: 2.746
batch start
#iterations: 151
currently lose_sum: 507.7350634634495
time_elpased: 2.764
batch start
#iterations: 152
currently lose_sum: 509.94555339217186
time_elpased: 2.755
batch start
#iterations: 153
currently lose_sum: 507.0398791730404
time_elpased: 2.763
batch start
#iterations: 154
currently lose_sum: 512.3426144719124
time_elpased: 2.754
batch start
#iterations: 155
currently lose_sum: 508.46535018086433
time_elpased: 2.731
batch start
#iterations: 156
currently lose_sum: 510.11266952753067
time_elpased: 2.75
batch start
#iterations: 157
currently lose_sum: 508.18045821785927
time_elpased: 2.755
batch start
#iterations: 158
currently lose_sum: 509.0637611448765
time_elpased: 2.726
batch start
#iterations: 159
currently lose_sum: 510.55685499310493
time_elpased: 2.714
start validation test
0.558762886598
0.882244143033
0.146020408163
0.250569077219
0
validation finish
batch start
#iterations: 160
currently lose_sum: 510.2722949683666
time_elpased: 2.71
batch start
#iterations: 161
currently lose_sum: 509.5236836373806
time_elpased: 2.698
batch start
#iterations: 162
currently lose_sum: 506.94101670384407
time_elpased: 2.714
batch start
#iterations: 163
currently lose_sum: 506.20308941602707
time_elpased: 2.714
batch start
#iterations: 164
currently lose_sum: 510.72787603735924
time_elpased: 2.7
batch start
#iterations: 165
currently lose_sum: 509.62646839022636
time_elpased: 2.708
batch start
#iterations: 166
currently lose_sum: 509.8845613002777
time_elpased: 2.694
batch start
#iterations: 167
currently lose_sum: 509.4100728929043
time_elpased: 2.704
batch start
#iterations: 168
currently lose_sum: 510.0175359547138
time_elpased: 2.707
batch start
#iterations: 169
currently lose_sum: 510.14540469646454
time_elpased: 2.706
batch start
#iterations: 170
currently lose_sum: 508.77941033244133
time_elpased: 2.705
batch start
#iterations: 171
currently lose_sum: 506.6082429289818
time_elpased: 2.72
batch start
#iterations: 172
currently lose_sum: 512.2423478960991
time_elpased: 2.7
batch start
#iterations: 173
currently lose_sum: 510.03490659594536
time_elpased: 2.705
batch start
#iterations: 174
currently lose_sum: 510.202535122633
time_elpased: 2.718
batch start
#iterations: 175
currently lose_sum: 507.92392748594284
time_elpased: 2.691
batch start
#iterations: 176
currently lose_sum: 508.59174460172653
time_elpased: 2.691
batch start
#iterations: 177
currently lose_sum: 507.7293843328953
time_elpased: 2.699
batch start
#iterations: 178
currently lose_sum: 509.9053600728512
time_elpased: 2.708
batch start
#iterations: 179
currently lose_sum: 510.3938393294811
time_elpased: 2.704
start validation test
0.563865979381
0.889018012783
0.15612244898
0.265601944276
0
validation finish
batch start
#iterations: 180
currently lose_sum: 507.762101739645
time_elpased: 2.703
batch start
#iterations: 181
currently lose_sum: 510.78429424762726
time_elpased: 2.689
batch start
#iterations: 182
currently lose_sum: 509.068703353405
time_elpased: 2.702
batch start
#iterations: 183
currently lose_sum: 509.504602342844
time_elpased: 2.709
batch start
#iterations: 184
currently lose_sum: 508.69096341729164
time_elpased: 2.698
batch start
#iterations: 185
currently lose_sum: 509.2415374815464
time_elpased: 2.709
batch start
#iterations: 186
currently lose_sum: 509.27596041560173
time_elpased: 2.712
batch start
#iterations: 187
currently lose_sum: 506.56379690766335
time_elpased: 2.707
batch start
#iterations: 188
currently lose_sum: 510.41902098059654
time_elpased: 2.704
batch start
#iterations: 189
currently lose_sum: 510.0364460349083
time_elpased: 2.702
batch start
#iterations: 190
currently lose_sum: 506.8142413198948
time_elpased: 2.71
batch start
#iterations: 191
currently lose_sum: 510.1204150021076
time_elpased: 2.712
batch start
#iterations: 192
currently lose_sum: 506.3250530064106
time_elpased: 2.7
batch start
#iterations: 193
currently lose_sum: 508.95146068930626
time_elpased: 2.722
batch start
#iterations: 194
currently lose_sum: 510.62884226441383
time_elpased: 2.702
batch start
#iterations: 195
currently lose_sum: 508.5405964255333
time_elpased: 2.709
batch start
#iterations: 196
currently lose_sum: 507.36555913090706
time_elpased: 2.711
batch start
#iterations: 197
currently lose_sum: 510.2011336386204
time_elpased: 2.719
batch start
#iterations: 198
currently lose_sum: 508.6310987174511
time_elpased: 2.706
batch start
#iterations: 199
currently lose_sum: 507.61257112026215
time_elpased: 2.72
start validation test
0.542731958763
0.871302957634
0.111224489796
0.197267215637
0
validation finish
batch start
#iterations: 200
currently lose_sum: 508.8620465397835
time_elpased: 2.701
batch start
#iterations: 201
currently lose_sum: 508.8366691470146
time_elpased: 2.71
batch start
#iterations: 202
currently lose_sum: 509.2172788977623
time_elpased: 2.707
batch start
#iterations: 203
currently lose_sum: 507.6879623532295
time_elpased: 2.705
batch start
#iterations: 204
currently lose_sum: 507.30937948822975
time_elpased: 2.702
batch start
#iterations: 205
currently lose_sum: 509.07885643839836
time_elpased: 2.714
batch start
#iterations: 206
currently lose_sum: 510.75126364827156
time_elpased: 2.709
batch start
#iterations: 207
currently lose_sum: 509.46796268224716
time_elpased: 2.704
batch start
#iterations: 208
currently lose_sum: 507.40076380968094
time_elpased: 2.709
batch start
#iterations: 209
currently lose_sum: 507.78197172284126
time_elpased: 2.704
batch start
#iterations: 210
currently lose_sum: 508.321534961462
time_elpased: 2.695
batch start
#iterations: 211
currently lose_sum: 509.4609724879265
time_elpased: 2.699
batch start
#iterations: 212
currently lose_sum: 508.61700144410133
time_elpased: 2.716
batch start
#iterations: 213
currently lose_sum: 507.96187844872475
time_elpased: 2.714
batch start
#iterations: 214
currently lose_sum: 508.2142238020897
time_elpased: 2.714
batch start
#iterations: 215
currently lose_sum: 510.220004349947
time_elpased: 2.719
batch start
#iterations: 216
currently lose_sum: 508.23092979192734
time_elpased: 2.713
batch start
#iterations: 217
currently lose_sum: 508.5768606364727
time_elpased: 2.717
batch start
#iterations: 218
currently lose_sum: 508.32944762706757
time_elpased: 2.718
batch start
#iterations: 219
currently lose_sum: 509.9923594892025
time_elpased: 2.725
start validation test
0.561649484536
0.873271889401
0.154693877551
0.262829403606
0
validation finish
batch start
#iterations: 220
currently lose_sum: 507.75334772467613
time_elpased: 2.712
batch start
#iterations: 221
currently lose_sum: 509.46440067887306
time_elpased: 2.708
batch start
#iterations: 222
currently lose_sum: 507.0215903222561
time_elpased: 2.718
batch start
#iterations: 223
currently lose_sum: 506.62876757979393
time_elpased: 2.693
batch start
#iterations: 224
currently lose_sum: 509.2284308373928
time_elpased: 2.713
batch start
#iterations: 225
currently lose_sum: 509.47343268990517
time_elpased: 2.719
batch start
#iterations: 226
currently lose_sum: 508.8276871442795
time_elpased: 2.69
batch start
#iterations: 227
currently lose_sum: 508.5905671417713
time_elpased: 2.703
batch start
#iterations: 228
currently lose_sum: 505.53250256180763
time_elpased: 2.7
batch start
#iterations: 229
currently lose_sum: 507.78946432471275
time_elpased: 2.704
batch start
#iterations: 230
currently lose_sum: 509.4521255195141
time_elpased: 2.71
batch start
#iterations: 231
currently lose_sum: 510.7292490005493
time_elpased: 2.71
batch start
#iterations: 232
currently lose_sum: 505.8954412639141
time_elpased: 2.694
batch start
#iterations: 233
currently lose_sum: 506.876053661108
time_elpased: 2.701
batch start
#iterations: 234
currently lose_sum: 508.40098455548286
time_elpased: 2.715
batch start
#iterations: 235
currently lose_sum: 508.2320648729801
time_elpased: 2.713
batch start
#iterations: 236
currently lose_sum: 507.89322832226753
time_elpased: 2.704
batch start
#iterations: 237
currently lose_sum: 508.6180864274502
time_elpased: 2.719
batch start
#iterations: 238
currently lose_sum: 507.1464839577675
time_elpased: 2.715
batch start
#iterations: 239
currently lose_sum: 505.7463793158531
time_elpased: 2.71
start validation test
0.535360824742
0.86797752809
0.0945918367347
0.170592565329
0
validation finish
batch start
#iterations: 240
currently lose_sum: 506.54416248202324
time_elpased: 2.716
batch start
#iterations: 241
currently lose_sum: 506.3590385019779
time_elpased: 2.72
batch start
#iterations: 242
currently lose_sum: 505.5877727866173
time_elpased: 2.696
batch start
#iterations: 243
currently lose_sum: 508.35766020417213
time_elpased: 2.714
batch start
#iterations: 244
currently lose_sum: 507.95980018377304
time_elpased: 2.714
batch start
#iterations: 245
currently lose_sum: 507.70023503899574
time_elpased: 2.712
batch start
#iterations: 246
currently lose_sum: 508.4047983288765
time_elpased: 2.727
batch start
#iterations: 247
currently lose_sum: 507.0543111562729
time_elpased: 2.697
batch start
#iterations: 248
currently lose_sum: 507.3448306322098
time_elpased: 2.694
batch start
#iterations: 249
currently lose_sum: 508.7246414721012
time_elpased: 2.702
batch start
#iterations: 250
currently lose_sum: 507.38496363162994
time_elpased: 2.703
batch start
#iterations: 251
currently lose_sum: 507.1044926941395
time_elpased: 2.699
batch start
#iterations: 252
currently lose_sum: 509.1010355055332
time_elpased: 2.703
batch start
#iterations: 253
currently lose_sum: 505.848287075758
time_elpased: 2.713
batch start
#iterations: 254
currently lose_sum: 509.2989307940006
time_elpased: 2.706
batch start
#iterations: 255
currently lose_sum: 507.68442127108574
time_elpased: 2.72
batch start
#iterations: 256
currently lose_sum: 507.7560119330883
time_elpased: 2.705
batch start
#iterations: 257
currently lose_sum: 506.4404499232769
time_elpased: 2.709
batch start
#iterations: 258
currently lose_sum: 508.48608097434044
time_elpased: 2.716
batch start
#iterations: 259
currently lose_sum: 508.7986606657505
time_elpased: 2.708
start validation test
0.554175257732
0.871050934881
0.137857142857
0.23804070126
0
validation finish
batch start
#iterations: 260
currently lose_sum: 508.9111339747906
time_elpased: 2.711
batch start
#iterations: 261
currently lose_sum: 510.6450925171375
time_elpased: 2.708
batch start
#iterations: 262
currently lose_sum: 508.570697247982
time_elpased: 2.728
batch start
#iterations: 263
currently lose_sum: 507.5553748011589
time_elpased: 2.697
batch start
#iterations: 264
currently lose_sum: 507.6218626201153
time_elpased: 2.714
batch start
#iterations: 265
currently lose_sum: 507.5133884847164
time_elpased: 2.715
batch start
#iterations: 266
currently lose_sum: 508.48438027501106
time_elpased: 2.71
batch start
#iterations: 267
currently lose_sum: 506.86371728777885
time_elpased: 2.707
batch start
#iterations: 268
currently lose_sum: 509.48060724139214
time_elpased: 2.706
batch start
#iterations: 269
currently lose_sum: 509.57059609889984
time_elpased: 2.711
batch start
#iterations: 270
currently lose_sum: 508.1557856500149
time_elpased: 2.697
batch start
#iterations: 271
currently lose_sum: 504.81117126345634
time_elpased: 2.707
batch start
#iterations: 272
currently lose_sum: 508.62146320939064
time_elpased: 2.713
batch start
#iterations: 273
currently lose_sum: 507.5274434387684
time_elpased: 2.703
batch start
#iterations: 274
currently lose_sum: 508.5369027554989
time_elpased: 2.707
batch start
#iterations: 275
currently lose_sum: 507.843961507082
time_elpased: 2.72
batch start
#iterations: 276
currently lose_sum: 506.0736320912838
time_elpased: 2.696
batch start
#iterations: 277
currently lose_sum: 509.7325277030468
time_elpased: 2.696
batch start
#iterations: 278
currently lose_sum: 508.41526356339455
time_elpased: 2.715
batch start
#iterations: 279
currently lose_sum: 508.2666436433792
time_elpased: 2.711
start validation test
0.52618556701
0.866265060241
0.0733673469388
0.135277516463
0
validation finish
batch start
#iterations: 280
currently lose_sum: 506.9465087354183
time_elpased: 2.708
batch start
#iterations: 281
currently lose_sum: 507.91674679517746
time_elpased: 2.7
batch start
#iterations: 282
currently lose_sum: 508.17252817749977
time_elpased: 2.707
batch start
#iterations: 283
currently lose_sum: 508.3307522237301
time_elpased: 2.707
batch start
#iterations: 284
currently lose_sum: 508.094165712595
time_elpased: 2.723
batch start
#iterations: 285
currently lose_sum: 507.5049301683903
time_elpased: 2.719
batch start
#iterations: 286
currently lose_sum: 508.0992592871189
time_elpased: 2.703
batch start
#iterations: 287
currently lose_sum: 507.92718747258186
time_elpased: 2.711
batch start
#iterations: 288
currently lose_sum: 508.42651906609535
time_elpased: 2.725
batch start
#iterations: 289
currently lose_sum: 507.14255529642105
time_elpased: 2.706
batch start
#iterations: 290
currently lose_sum: 506.88291627168655
time_elpased: 2.725
batch start
#iterations: 291
currently lose_sum: 508.73839408159256
time_elpased: 2.723
batch start
#iterations: 292
currently lose_sum: 506.8152722120285
time_elpased: 2.707
batch start
#iterations: 293
currently lose_sum: 508.02781841158867
time_elpased: 2.69
batch start
#iterations: 294
currently lose_sum: 506.9268970489502
time_elpased: 2.72
batch start
#iterations: 295
currently lose_sum: 507.30241560935974
time_elpased: 2.716
batch start
#iterations: 296
currently lose_sum: 505.2152927517891
time_elpased: 2.725
batch start
#iterations: 297
currently lose_sum: 508.7796706557274
time_elpased: 2.72
batch start
#iterations: 298
currently lose_sum: 507.83737602829933
time_elpased: 2.724
batch start
#iterations: 299
currently lose_sum: 507.3629014790058
time_elpased: 2.721
start validation test
0.581855670103
0.870826010545
0.202244897959
0.32825438887
0
validation finish
batch start
#iterations: 300
currently lose_sum: 505.2549577951431
time_elpased: 2.714
batch start
#iterations: 301
currently lose_sum: 508.18700763583183
time_elpased: 2.713
batch start
#iterations: 302
currently lose_sum: 509.45133033394814
time_elpased: 2.705
batch start
#iterations: 303
currently lose_sum: 507.74174827337265
time_elpased: 2.695
batch start
#iterations: 304
currently lose_sum: 506.84347799420357
time_elpased: 2.701
batch start
#iterations: 305
currently lose_sum: 508.9743648171425
time_elpased: 2.686
batch start
#iterations: 306
currently lose_sum: 507.23668846488
time_elpased: 2.696
batch start
#iterations: 307
currently lose_sum: 509.67480078339577
time_elpased: 2.699
batch start
#iterations: 308
currently lose_sum: 506.5159826874733
time_elpased: 2.725
batch start
#iterations: 309
currently lose_sum: 509.7269466817379
time_elpased: 2.703
batch start
#iterations: 310
currently lose_sum: 506.27931612730026
time_elpased: 2.704
batch start
#iterations: 311
currently lose_sum: 506.2710990905762
time_elpased: 2.713
batch start
#iterations: 312
currently lose_sum: 508.2405845820904
time_elpased: 2.708
batch start
#iterations: 313
currently lose_sum: 508.43658858537674
time_elpased: 2.724
batch start
#iterations: 314
currently lose_sum: 506.5268405377865
time_elpased: 2.699
batch start
#iterations: 315
currently lose_sum: 506.2083815038204
time_elpased: 2.724
batch start
#iterations: 316
currently lose_sum: 507.20474314689636
time_elpased: 2.73
batch start
#iterations: 317
currently lose_sum: 506.78840881586075
time_elpased: 2.749
batch start
#iterations: 318
currently lose_sum: 509.41796043515205
time_elpased: 2.728
batch start
#iterations: 319
currently lose_sum: 507.4248394668102
time_elpased: 2.753
start validation test
0.545824742268
0.873771730915
0.117959183673
0.207857592376
0
validation finish
batch start
#iterations: 320
currently lose_sum: 507.91079941391945
time_elpased: 2.73
batch start
#iterations: 321
currently lose_sum: 506.5949877798557
time_elpased: 2.737
batch start
#iterations: 322
currently lose_sum: 506.64135295152664
time_elpased: 2.735
batch start
#iterations: 323
currently lose_sum: 508.5453035533428
time_elpased: 2.744
batch start
#iterations: 324
currently lose_sum: 508.90733778476715
time_elpased: 2.719
batch start
#iterations: 325
currently lose_sum: 508.97508332133293
time_elpased: 2.739
batch start
#iterations: 326
currently lose_sum: 508.80521312355995
time_elpased: 2.702
batch start
#iterations: 327
currently lose_sum: 508.29446947574615
time_elpased: 2.723
batch start
#iterations: 328
currently lose_sum: 506.96984604001045
time_elpased: 2.729
batch start
#iterations: 329
currently lose_sum: 507.55916130542755
time_elpased: 2.74
batch start
#iterations: 330
currently lose_sum: 507.5945837199688
time_elpased: 2.714
batch start
#iterations: 331
currently lose_sum: 510.2677155137062
time_elpased: 2.715
batch start
#iterations: 332
currently lose_sum: 508.27657756209373
time_elpased: 2.729
batch start
#iterations: 333
currently lose_sum: 507.856430798769
time_elpased: 2.704
batch start
#iterations: 334
currently lose_sum: 508.99226638674736
time_elpased: 2.727
batch start
#iterations: 335
currently lose_sum: 505.4508322775364
time_elpased: 2.726
batch start
#iterations: 336
currently lose_sum: 507.45510306954384
time_elpased: 2.73
batch start
#iterations: 337
currently lose_sum: 506.9441266655922
time_elpased: 2.708
batch start
#iterations: 338
currently lose_sum: 508.0927663445473
time_elpased: 2.715
batch start
#iterations: 339
currently lose_sum: 506.94783022999763
time_elpased: 2.712
start validation test
0.572525773196
0.869543894066
0.180918367347
0.299518540417
0
validation finish
batch start
#iterations: 340
currently lose_sum: 507.00787031650543
time_elpased: 2.699
batch start
#iterations: 341
currently lose_sum: 507.8510403037071
time_elpased: 2.719
batch start
#iterations: 342
currently lose_sum: 508.1150404214859
time_elpased: 2.72
batch start
#iterations: 343
currently lose_sum: 507.17920672893524
time_elpased: 2.693
batch start
#iterations: 344
currently lose_sum: 508.8767231106758
time_elpased: 2.724
batch start
#iterations: 345
currently lose_sum: 509.4323082268238
time_elpased: 2.726
batch start
#iterations: 346
currently lose_sum: 506.0739316344261
time_elpased: 2.711
batch start
#iterations: 347
currently lose_sum: 508.578447163105
time_elpased: 2.714
batch start
#iterations: 348
currently lose_sum: 508.28281873464584
time_elpased: 2.714
batch start
#iterations: 349
currently lose_sum: 508.54481264948845
time_elpased: 2.697
batch start
#iterations: 350
currently lose_sum: 507.9274706840515
time_elpased: 2.718
batch start
#iterations: 351
currently lose_sum: 506.24035769701004
time_elpased: 2.712
batch start
#iterations: 352
currently lose_sum: 506.78588688373566
time_elpased: 2.702
batch start
#iterations: 353
currently lose_sum: 508.7801739871502
time_elpased: 2.693
batch start
#iterations: 354
currently lose_sum: 506.70190355181694
time_elpased: 2.697
batch start
#iterations: 355
currently lose_sum: 508.09627491235733
time_elpased: 2.671
batch start
#iterations: 356
currently lose_sum: 508.0832234919071
time_elpased: 2.685
batch start
#iterations: 357
currently lose_sum: 508.07539692521095
time_elpased: 2.699
batch start
#iterations: 358
currently lose_sum: 508.25301909446716
time_elpased: 2.703
batch start
#iterations: 359
currently lose_sum: 506.8437269926071
time_elpased: 2.694
start validation test
0.544278350515
0.885140562249
0.112448979592
0.199547306474
0
validation finish
batch start
#iterations: 360
currently lose_sum: 509.1108764708042
time_elpased: 2.69
batch start
#iterations: 361
currently lose_sum: 505.87730848789215
time_elpased: 2.707
batch start
#iterations: 362
currently lose_sum: 508.4848112165928
time_elpased: 2.696
batch start
#iterations: 363
currently lose_sum: 506.62427428364754
time_elpased: 2.726
batch start
#iterations: 364
currently lose_sum: 507.2673426568508
time_elpased: 2.705
batch start
#iterations: 365
currently lose_sum: 507.67281970381737
time_elpased: 2.707
batch start
#iterations: 366
currently lose_sum: 508.9546990990639
time_elpased: 2.732
batch start
#iterations: 367
currently lose_sum: 507.93398383259773
time_elpased: 2.7
batch start
#iterations: 368
currently lose_sum: 506.86214169859886
time_elpased: 2.692
batch start
#iterations: 369
currently lose_sum: 506.92252418398857
time_elpased: 2.705
batch start
#iterations: 370
currently lose_sum: 506.7655164897442
time_elpased: 2.695
batch start
#iterations: 371
currently lose_sum: 507.8587783277035
time_elpased: 2.715
batch start
#iterations: 372
currently lose_sum: 506.7796881198883
time_elpased: 2.722
batch start
#iterations: 373
currently lose_sum: 507.08784621953964
time_elpased: 2.72
batch start
#iterations: 374
currently lose_sum: 507.8492539823055
time_elpased: 2.693
batch start
#iterations: 375
currently lose_sum: 508.0199544727802
time_elpased: 2.755
batch start
#iterations: 376
currently lose_sum: 507.43127477169037
time_elpased: 2.748
batch start
#iterations: 377
currently lose_sum: 508.0874503850937
time_elpased: 2.745
batch start
#iterations: 378
currently lose_sum: 507.5960163772106
time_elpased: 2.716
batch start
#iterations: 379
currently lose_sum: 506.81858694553375
time_elpased: 2.733
start validation test
0.540618556701
0.86815920398
0.106836734694
0.190259858259
0
validation finish
batch start
#iterations: 380
currently lose_sum: 507.2416362762451
time_elpased: 2.731
batch start
#iterations: 381
currently lose_sum: 506.96903771162033
time_elpased: 2.716
batch start
#iterations: 382
currently lose_sum: 508.69947451353073
time_elpased: 2.744
batch start
#iterations: 383
currently lose_sum: 506.0278120338917
time_elpased: 2.737
batch start
#iterations: 384
currently lose_sum: 505.57702270150185
time_elpased: 2.736
batch start
#iterations: 385
currently lose_sum: 505.81997361779213
time_elpased: 2.749
batch start
#iterations: 386
currently lose_sum: 507.3299460411072
time_elpased: 2.758
batch start
#iterations: 387
currently lose_sum: 507.24594643712044
time_elpased: 2.736
batch start
#iterations: 388
currently lose_sum: 507.76404133439064
time_elpased: 2.756
batch start
#iterations: 389
currently lose_sum: 506.0915986299515
time_elpased: 2.739
batch start
#iterations: 390
currently lose_sum: 504.80116990208626
time_elpased: 2.759
batch start
#iterations: 391
currently lose_sum: 507.0980738401413
time_elpased: 2.723
batch start
#iterations: 392
currently lose_sum: 507.30750355124474
time_elpased: 2.746
batch start
#iterations: 393
currently lose_sum: 507.6246908903122
time_elpased: 2.742
batch start
#iterations: 394
currently lose_sum: 507.4362880587578
time_elpased: 2.73
batch start
#iterations: 395
currently lose_sum: 506.7687348127365
time_elpased: 2.741
batch start
#iterations: 396
currently lose_sum: 507.4867517054081
time_elpased: 2.742
batch start
#iterations: 397
currently lose_sum: 507.26924961805344
time_elpased: 2.727
batch start
#iterations: 398
currently lose_sum: 507.1561416387558
time_elpased: 2.72
batch start
#iterations: 399
currently lose_sum: 507.17887273430824
time_elpased: 2.73
start validation test
0.535979381443
0.881453154876
0.0940816326531
0.17001659598
0
validation finish
acc: 0.620
pre: 0.864
rec: 0.276
F1: 0.418
auc: 0.000
