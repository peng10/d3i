start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 554.7745745778084
time_elpased: 2.508
batch start
#iterations: 1
currently lose_sum: 543.0608476400375
time_elpased: 2.494
batch start
#iterations: 2
currently lose_sum: 537.0618552267551
time_elpased: 2.518
batch start
#iterations: 3
currently lose_sum: 531.5477703809738
time_elpased: 2.51
batch start
#iterations: 4
currently lose_sum: 527.7882596850395
time_elpased: 2.513
batch start
#iterations: 5
currently lose_sum: 525.7510012388229
time_elpased: 2.526
batch start
#iterations: 6
currently lose_sum: 524.5276141166687
time_elpased: 2.507
batch start
#iterations: 7
currently lose_sum: 522.4178946316242
time_elpased: 2.5
batch start
#iterations: 8
currently lose_sum: 522.4829961359501
time_elpased: 2.524
batch start
#iterations: 9
currently lose_sum: 524.1812447011471
time_elpased: 2.502
batch start
#iterations: 10
currently lose_sum: 522.3751250207424
time_elpased: 2.528
batch start
#iterations: 11
currently lose_sum: 523.2834427058697
time_elpased: 2.523
batch start
#iterations: 12
currently lose_sum: 522.6630277335644
time_elpased: 2.517
batch start
#iterations: 13
currently lose_sum: 520.6183462142944
time_elpased: 2.541
batch start
#iterations: 14
currently lose_sum: 520.8233714699745
time_elpased: 2.501
batch start
#iterations: 15
currently lose_sum: 518.1504913568497
time_elpased: 2.522
batch start
#iterations: 16
currently lose_sum: 519.4946493506432
time_elpased: 2.526
batch start
#iterations: 17
currently lose_sum: 518.2083363831043
time_elpased: 2.501
batch start
#iterations: 18
currently lose_sum: 517.9863226413727
time_elpased: 2.527
batch start
#iterations: 19
currently lose_sum: 519.2355385124683
time_elpased: 2.539
start validation test
0.575154639175
0.882419446015
0.162688900469
0.274727208729
0
validation finish
batch start
#iterations: 20
currently lose_sum: 517.6950154900551
time_elpased: 2.506
batch start
#iterations: 21
currently lose_sum: 519.5453211963177
time_elpased: 2.538
batch start
#iterations: 22
currently lose_sum: 517.3082833588123
time_elpased: 2.533
batch start
#iterations: 23
currently lose_sum: 517.9737103283405
time_elpased: 2.525
batch start
#iterations: 24
currently lose_sum: 517.5915894508362
time_elpased: 2.518
batch start
#iterations: 25
currently lose_sum: 517.4371775388718
time_elpased: 2.512
batch start
#iterations: 26
currently lose_sum: 517.484979569912
time_elpased: 2.518
batch start
#iterations: 27
currently lose_sum: 516.9116132855415
time_elpased: 2.534
batch start
#iterations: 28
currently lose_sum: 515.7173245847225
time_elpased: 2.498
batch start
#iterations: 29
currently lose_sum: 516.6563993990421
time_elpased: 2.529
batch start
#iterations: 30
currently lose_sum: 514.3171715736389
time_elpased: 2.524
batch start
#iterations: 31
currently lose_sum: 516.1957124769688
time_elpased: 2.515
batch start
#iterations: 32
currently lose_sum: 515.42022767663
time_elpased: 2.518
batch start
#iterations: 33
currently lose_sum: 515.3388889729977
time_elpased: 2.51
batch start
#iterations: 34
currently lose_sum: 514.6358068585396
time_elpased: 2.514
batch start
#iterations: 35
currently lose_sum: 514.5133584737778
time_elpased: 2.528
batch start
#iterations: 36
currently lose_sum: 515.2160138785839
time_elpased: 2.5
batch start
#iterations: 37
currently lose_sum: 513.6011079847813
time_elpased: 2.517
batch start
#iterations: 38
currently lose_sum: 514.6010931432247
time_elpased: 2.519
batch start
#iterations: 39
currently lose_sum: 515.6591803133488
time_elpased: 2.5
start validation test
0.549896907216
0.912129894938
0.0995310057322
0.179477541815
0
validation finish
batch start
#iterations: 40
currently lose_sum: 515.7398827970028
time_elpased: 2.506
batch start
#iterations: 41
currently lose_sum: 515.2162018716335
time_elpased: 2.506
batch start
#iterations: 42
currently lose_sum: 513.7769917845726
time_elpased: 2.49
batch start
#iterations: 43
currently lose_sum: 513.4043930470943
time_elpased: 2.523
batch start
#iterations: 44
currently lose_sum: 515.4036353230476
time_elpased: 2.512
batch start
#iterations: 45
currently lose_sum: 515.6825407743454
time_elpased: 2.49
batch start
#iterations: 46
currently lose_sum: 512.6706204116344
time_elpased: 2.507
batch start
#iterations: 47
currently lose_sum: 513.2536593973637
time_elpased: 2.51
batch start
#iterations: 48
currently lose_sum: 514.3906994760036
time_elpased: 2.522
batch start
#iterations: 49
currently lose_sum: 512.3788844943047
time_elpased: 2.529
batch start
#iterations: 50
currently lose_sum: 514.5018872916698
time_elpased: 2.504
batch start
#iterations: 51
currently lose_sum: 513.1116229891777
time_elpased: 2.533
batch start
#iterations: 52
currently lose_sum: 514.3990226089954
time_elpased: 2.512
batch start
#iterations: 53
currently lose_sum: 513.9131429195404
time_elpased: 2.521
batch start
#iterations: 54
currently lose_sum: 512.4378387033939
time_elpased: 2.519
batch start
#iterations: 55
currently lose_sum: 513.7641170918941
time_elpased: 2.504
batch start
#iterations: 56
currently lose_sum: 513.7887128293514
time_elpased: 2.519
batch start
#iterations: 57
currently lose_sum: 512.1111496984959
time_elpased: 2.515
batch start
#iterations: 58
currently lose_sum: 514.7757239639759
time_elpased: 2.506
batch start
#iterations: 59
currently lose_sum: 512.6469416618347
time_elpased: 2.533
start validation test
0.571237113402
0.898315658141
0.150078165711
0.257188783711
0
validation finish
batch start
#iterations: 60
currently lose_sum: 512.8012849390507
time_elpased: 2.52
batch start
#iterations: 61
currently lose_sum: 512.1692211925983
time_elpased: 2.508
batch start
#iterations: 62
currently lose_sum: 514.7528405785561
time_elpased: 2.528
batch start
#iterations: 63
currently lose_sum: 513.7024320662022
time_elpased: 2.513
batch start
#iterations: 64
currently lose_sum: 513.9562189280987
time_elpased: 2.519
batch start
#iterations: 65
currently lose_sum: 513.5734859406948
time_elpased: 2.527
batch start
#iterations: 66
currently lose_sum: 512.3975552022457
time_elpased: 2.515
batch start
#iterations: 67
currently lose_sum: 511.10702046751976
time_elpased: 2.532
batch start
#iterations: 68
currently lose_sum: 512.8172580301762
time_elpased: 2.517
batch start
#iterations: 69
currently lose_sum: 514.8493667244911
time_elpased: 2.509
batch start
#iterations: 70
currently lose_sum: 511.21140709519386
time_elpased: 2.523
batch start
#iterations: 71
currently lose_sum: 512.6942261457443
time_elpased: 2.523
batch start
#iterations: 72
currently lose_sum: 514.6530974805355
time_elpased: 2.518
batch start
#iterations: 73
currently lose_sum: 512.0855812728405
time_elpased: 2.533
batch start
#iterations: 74
currently lose_sum: 513.725286334753
time_elpased: 2.508
batch start
#iterations: 75
currently lose_sum: 511.8787719607353
time_elpased: 2.511
batch start
#iterations: 76
currently lose_sum: 513.4520651400089
time_elpased: 2.519
batch start
#iterations: 77
currently lose_sum: 513.2245998978615
time_elpased: 2.502
batch start
#iterations: 78
currently lose_sum: 512.9847443401814
time_elpased: 2.519
batch start
#iterations: 79
currently lose_sum: 514.2624379694462
time_elpased: 2.51
start validation test
0.606855670103
0.864444444444
0.24325169359
0.37966653111
0
validation finish
batch start
#iterations: 80
currently lose_sum: 511.4322178661823
time_elpased: 2.497
batch start
#iterations: 81
currently lose_sum: 511.1987429559231
time_elpased: 2.515
batch start
#iterations: 82
currently lose_sum: 511.47168001532555
time_elpased: 2.519
batch start
#iterations: 83
currently lose_sum: 513.27692720294
time_elpased: 2.5
batch start
#iterations: 84
currently lose_sum: 511.9161398112774
time_elpased: 2.512
batch start
#iterations: 85
currently lose_sum: 510.9490582048893
time_elpased: 2.512
batch start
#iterations: 86
currently lose_sum: 511.9110127687454
time_elpased: 2.504
batch start
#iterations: 87
currently lose_sum: 510.74202513694763
time_elpased: 2.518
batch start
#iterations: 88
currently lose_sum: 511.53535321354866
time_elpased: 2.509
batch start
#iterations: 89
currently lose_sum: 511.9038178920746
time_elpased: 2.522
batch start
#iterations: 90
currently lose_sum: 511.61060777306557
time_elpased: 2.53
batch start
#iterations: 91
currently lose_sum: 510.21046939492226
time_elpased: 2.522
batch start
#iterations: 92
currently lose_sum: 512.3072374761105
time_elpased: 2.527
batch start
#iterations: 93
currently lose_sum: 511.3548259139061
time_elpased: 2.517
batch start
#iterations: 94
currently lose_sum: 510.86118748784065
time_elpased: 2.514
batch start
#iterations: 95
currently lose_sum: 510.6084953248501
time_elpased: 2.514
batch start
#iterations: 96
currently lose_sum: 512.6492786705494
time_elpased: 2.51
batch start
#iterations: 97
currently lose_sum: 509.00617933273315
time_elpased: 2.514
batch start
#iterations: 98
currently lose_sum: 511.82976138591766
time_elpased: 2.526
batch start
#iterations: 99
currently lose_sum: 512.5584574043751
time_elpased: 2.518
start validation test
0.550979381443
0.894642857143
0.104429390307
0.187027531498
0
validation finish
batch start
#iterations: 100
currently lose_sum: 512.2095801532269
time_elpased: 2.523
batch start
#iterations: 101
currently lose_sum: 511.93383181095123
time_elpased: 2.526
batch start
#iterations: 102
currently lose_sum: 510.14303663372993
time_elpased: 2.516
batch start
#iterations: 103
currently lose_sum: 512.1471004188061
time_elpased: 2.529
batch start
#iterations: 104
currently lose_sum: 511.5052482485771
time_elpased: 2.521
batch start
#iterations: 105
currently lose_sum: 510.50089770555496
time_elpased: 2.499
batch start
#iterations: 106
currently lose_sum: 512.6944817602634
time_elpased: 2.51
batch start
#iterations: 107
currently lose_sum: 509.6499350667
time_elpased: 2.499
batch start
#iterations: 108
currently lose_sum: 509.2750128209591
time_elpased: 2.513
batch start
#iterations: 109
currently lose_sum: 511.77378436923027
time_elpased: 2.525
batch start
#iterations: 110
currently lose_sum: 512.5174674987793
time_elpased: 2.515
batch start
#iterations: 111
currently lose_sum: 514.3106365501881
time_elpased: 2.543
batch start
#iterations: 112
currently lose_sum: 509.2691399753094
time_elpased: 2.507
batch start
#iterations: 113
currently lose_sum: 511.28879648447037
time_elpased: 2.522
batch start
#iterations: 114
currently lose_sum: 510.8184127509594
time_elpased: 2.518
batch start
#iterations: 115
currently lose_sum: 510.6113679111004
time_elpased: 2.506
batch start
#iterations: 116
currently lose_sum: 510.1703459918499
time_elpased: 2.518
batch start
#iterations: 117
currently lose_sum: 511.7035071849823
time_elpased: 2.525
batch start
#iterations: 118
currently lose_sum: 511.12511372566223
time_elpased: 2.502
batch start
#iterations: 119
currently lose_sum: 511.28462395071983
time_elpased: 2.524
start validation test
0.593298969072
0.898178421298
0.200416883794
0.327709611452
0
validation finish
batch start
#iterations: 120
currently lose_sum: 511.650286257267
time_elpased: 2.507
batch start
#iterations: 121
currently lose_sum: 510.2192896902561
time_elpased: 2.514
batch start
#iterations: 122
currently lose_sum: 509.5013333559036
time_elpased: 2.515
batch start
#iterations: 123
currently lose_sum: 509.1384160518646
time_elpased: 2.501
batch start
#iterations: 124
currently lose_sum: 508.9213137924671
time_elpased: 2.498
batch start
#iterations: 125
currently lose_sum: 511.59743735194206
time_elpased: 2.513
batch start
#iterations: 126
currently lose_sum: 512.0651948451996
time_elpased: 2.502
batch start
#iterations: 127
currently lose_sum: 511.4587362110615
time_elpased: 2.516
batch start
#iterations: 128
currently lose_sum: 510.54067316651344
time_elpased: 2.55
batch start
#iterations: 129
currently lose_sum: 510.76386719942093
time_elpased: 2.521
batch start
#iterations: 130
currently lose_sum: 510.298182874918
time_elpased: 2.526
batch start
#iterations: 131
currently lose_sum: 511.03941798210144
time_elpased: 2.527
batch start
#iterations: 132
currently lose_sum: 510.55401879549026
time_elpased: 2.516
batch start
#iterations: 133
currently lose_sum: 511.6357578933239
time_elpased: 2.512
batch start
#iterations: 134
currently lose_sum: 509.6476123034954
time_elpased: 2.501
batch start
#iterations: 135
currently lose_sum: 509.88165333867073
time_elpased: 2.52
batch start
#iterations: 136
currently lose_sum: 510.3469551205635
time_elpased: 2.522
batch start
#iterations: 137
currently lose_sum: 508.6324541568756
time_elpased: 2.522
batch start
#iterations: 138
currently lose_sum: 508.40745237469673
time_elpased: 2.527
batch start
#iterations: 139
currently lose_sum: 510.1898551285267
time_elpased: 2.529
start validation test
0.56412371134
0.896311760612
0.134236581553
0.233502538071
0
validation finish
batch start
#iterations: 140
currently lose_sum: 508.3126598596573
time_elpased: 2.497
batch start
#iterations: 141
currently lose_sum: 509.82997727394104
time_elpased: 2.539
batch start
#iterations: 142
currently lose_sum: 508.8652442097664
time_elpased: 2.519
batch start
#iterations: 143
currently lose_sum: 510.07109159231186
time_elpased: 2.531
batch start
#iterations: 144
currently lose_sum: 508.5028846859932
time_elpased: 2.523
batch start
#iterations: 145
currently lose_sum: 510.46282544732094
time_elpased: 2.5
batch start
#iterations: 146
currently lose_sum: 511.220682322979
time_elpased: 2.493
batch start
#iterations: 147
currently lose_sum: 511.09379282593727
time_elpased: 2.523
batch start
#iterations: 148
currently lose_sum: 508.7794747054577
time_elpased: 2.489
batch start
#iterations: 149
currently lose_sum: 510.60438203811646
time_elpased: 2.5
batch start
#iterations: 150
currently lose_sum: 510.04804903268814
time_elpased: 2.502
batch start
#iterations: 151
currently lose_sum: 508.7134608030319
time_elpased: 2.484
batch start
#iterations: 152
currently lose_sum: 509.68811669945717
time_elpased: 2.507
batch start
#iterations: 153
currently lose_sum: 507.9008938074112
time_elpased: 2.488
batch start
#iterations: 154
currently lose_sum: 512.913894534111
time_elpased: 2.5
batch start
#iterations: 155
currently lose_sum: 507.7685526609421
time_elpased: 2.518
batch start
#iterations: 156
currently lose_sum: 508.83557108044624
time_elpased: 2.504
batch start
#iterations: 157
currently lose_sum: 508.89775463938713
time_elpased: 2.523
batch start
#iterations: 158
currently lose_sum: 509.70188269019127
time_elpased: 2.516
batch start
#iterations: 159
currently lose_sum: 509.54041615128517
time_elpased: 2.512
start validation test
0.564278350515
0.903818953324
0.133194372069
0.232173676083
0
validation finish
batch start
#iterations: 160
currently lose_sum: 510.7749177515507
time_elpased: 2.523
batch start
#iterations: 161
currently lose_sum: 508.78991320729256
time_elpased: 2.521
batch start
#iterations: 162
currently lose_sum: 509.00304767489433
time_elpased: 2.51
batch start
#iterations: 163
currently lose_sum: 508.82379245758057
time_elpased: 2.516
batch start
#iterations: 164
currently lose_sum: 510.70105293393135
time_elpased: 2.5
batch start
#iterations: 165
currently lose_sum: 511.81730514764786
time_elpased: 2.529
batch start
#iterations: 166
currently lose_sum: 509.91567611694336
time_elpased: 2.524
batch start
#iterations: 167
currently lose_sum: 508.85454857349396
time_elpased: 2.51
batch start
#iterations: 168
currently lose_sum: 508.7726792693138
time_elpased: 2.506
batch start
#iterations: 169
currently lose_sum: 509.99951231479645
time_elpased: 2.525
batch start
#iterations: 170
currently lose_sum: 508.7509610950947
time_elpased: 2.482
batch start
#iterations: 171
currently lose_sum: 508.7493772506714
time_elpased: 2.517
batch start
#iterations: 172
currently lose_sum: 510.8835587799549
time_elpased: 2.496
batch start
#iterations: 173
currently lose_sum: 510.0939356982708
time_elpased: 2.497
batch start
#iterations: 174
currently lose_sum: 509.9886148571968
time_elpased: 2.489
batch start
#iterations: 175
currently lose_sum: 510.7021635174751
time_elpased: 2.498
batch start
#iterations: 176
currently lose_sum: 507.34544718265533
time_elpased: 2.507
batch start
#iterations: 177
currently lose_sum: 509.8697837591171
time_elpased: 2.521
batch start
#iterations: 178
currently lose_sum: 510.13258531689644
time_elpased: 2.511
batch start
#iterations: 179
currently lose_sum: 510.0861787199974
time_elpased: 2.545
start validation test
0.568505154639
0.902102496715
0.143095362168
0.247009085185
0
validation finish
batch start
#iterations: 180
currently lose_sum: 509.0008875131607
time_elpased: 2.528
batch start
#iterations: 181
currently lose_sum: 510.6668673455715
time_elpased: 2.507
batch start
#iterations: 182
currently lose_sum: 510.3238224387169
time_elpased: 2.534
batch start
#iterations: 183
currently lose_sum: 511.43669605255127
time_elpased: 2.53
batch start
#iterations: 184
currently lose_sum: 508.701034784317
time_elpased: 2.518
batch start
#iterations: 185
currently lose_sum: 510.301226913929
time_elpased: 2.539
batch start
#iterations: 186
currently lose_sum: 510.02075457572937
time_elpased: 2.514
batch start
#iterations: 187
currently lose_sum: 510.782517939806
time_elpased: 2.528
batch start
#iterations: 188
currently lose_sum: 509.4278094470501
time_elpased: 2.522
batch start
#iterations: 189
currently lose_sum: 510.44477090239525
time_elpased: 2.501
batch start
#iterations: 190
currently lose_sum: 509.1648009419441
time_elpased: 2.544
batch start
#iterations: 191
currently lose_sum: 510.44686698913574
time_elpased: 2.539
batch start
#iterations: 192
currently lose_sum: 507.74684849381447
time_elpased: 2.521
batch start
#iterations: 193
currently lose_sum: 509.16550439596176
time_elpased: 2.528
batch start
#iterations: 194
currently lose_sum: 510.5045132637024
time_elpased: 2.519
batch start
#iterations: 195
currently lose_sum: 511.73770639300346
time_elpased: 2.531
batch start
#iterations: 196
currently lose_sum: 507.63677403330803
time_elpased: 2.533
batch start
#iterations: 197
currently lose_sum: 511.4397976398468
time_elpased: 2.505
batch start
#iterations: 198
currently lose_sum: 508.9599287509918
time_elpased: 2.528
batch start
#iterations: 199
currently lose_sum: 509.5168626010418
time_elpased: 2.504
start validation test
0.558865979381
0.907305577376
0.120375195414
0.212550607287
0
validation finish
batch start
#iterations: 200
currently lose_sum: 509.60236552357674
time_elpased: 2.501
batch start
#iterations: 201
currently lose_sum: 511.29499822854996
time_elpased: 2.5
batch start
#iterations: 202
currently lose_sum: 507.5772530734539
time_elpased: 2.5
batch start
#iterations: 203
currently lose_sum: 508.4756856262684
time_elpased: 2.498
batch start
#iterations: 204
currently lose_sum: 508.4717339873314
time_elpased: 2.528
batch start
#iterations: 205
currently lose_sum: 509.24367529153824
time_elpased: 2.515
batch start
#iterations: 206
currently lose_sum: 510.86136743426323
time_elpased: 2.501
batch start
#iterations: 207
currently lose_sum: 508.83847147226334
time_elpased: 2.514
batch start
#iterations: 208
currently lose_sum: 507.30376863479614
time_elpased: 2.513
batch start
#iterations: 209
currently lose_sum: 510.4925588965416
time_elpased: 2.507
batch start
#iterations: 210
currently lose_sum: 508.2579116821289
time_elpased: 2.52
batch start
#iterations: 211
currently lose_sum: 508.1243335008621
time_elpased: 2.503
batch start
#iterations: 212
currently lose_sum: 508.30137249827385
time_elpased: 2.522
batch start
#iterations: 213
currently lose_sum: 508.4068876206875
time_elpased: 2.523
batch start
#iterations: 214
currently lose_sum: 508.3090795278549
time_elpased: 2.524
batch start
#iterations: 215
currently lose_sum: 509.81914868950844
time_elpased: 2.526
batch start
#iterations: 216
currently lose_sum: 509.68980008363724
time_elpased: 2.523
batch start
#iterations: 217
currently lose_sum: 509.5141609609127
time_elpased: 2.521
batch start
#iterations: 218
currently lose_sum: 509.1727510392666
time_elpased: 2.524
batch start
#iterations: 219
currently lose_sum: 511.07795214653015
time_elpased: 2.499
start validation test
0.56175257732
0.899780541331
0.128191766545
0.224411603722
0
validation finish
batch start
#iterations: 220
currently lose_sum: 509.5531717836857
time_elpased: 2.509
batch start
#iterations: 221
currently lose_sum: 510.68106520175934
time_elpased: 2.509
batch start
#iterations: 222
currently lose_sum: 507.81088691949844
time_elpased: 2.484
batch start
#iterations: 223
currently lose_sum: 508.7904699742794
time_elpased: 2.511
batch start
#iterations: 224
currently lose_sum: 509.3576517403126
time_elpased: 2.497
batch start
#iterations: 225
currently lose_sum: 510.6299509704113
time_elpased: 2.521
batch start
#iterations: 226
currently lose_sum: 508.7290813624859
time_elpased: 2.526
batch start
#iterations: 227
currently lose_sum: 508.4172615110874
time_elpased: 2.518
batch start
#iterations: 228
currently lose_sum: 507.59723445773125
time_elpased: 2.523
batch start
#iterations: 229
currently lose_sum: 509.86536848545074
time_elpased: 2.513
batch start
#iterations: 230
currently lose_sum: 510.4550114572048
time_elpased: 2.506
batch start
#iterations: 231
currently lose_sum: 511.0811056792736
time_elpased: 2.519
batch start
#iterations: 232
currently lose_sum: 508.6956712901592
time_elpased: 2.509
batch start
#iterations: 233
currently lose_sum: 507.6611662209034
time_elpased: 2.498
batch start
#iterations: 234
currently lose_sum: 507.2118865251541
time_elpased: 2.507
batch start
#iterations: 235
currently lose_sum: 506.7013308405876
time_elpased: 2.492
batch start
#iterations: 236
currently lose_sum: 508.0985261797905
time_elpased: 2.526
batch start
#iterations: 237
currently lose_sum: 510.2333587706089
time_elpased: 2.516
batch start
#iterations: 238
currently lose_sum: 507.8489445745945
time_elpased: 2.497
batch start
#iterations: 239
currently lose_sum: 507.6650316119194
time_elpased: 2.524
start validation test
0.552371134021
0.911472448058
0.105158936946
0.188562885442
0
validation finish
batch start
#iterations: 240
currently lose_sum: 508.25449615716934
time_elpased: 2.52
batch start
#iterations: 241
currently lose_sum: 508.048867225647
time_elpased: 2.517
batch start
#iterations: 242
currently lose_sum: 508.09471395611763
time_elpased: 2.53
batch start
#iterations: 243
currently lose_sum: 507.91098713874817
time_elpased: 2.529
batch start
#iterations: 244
currently lose_sum: 508.5118362903595
time_elpased: 2.523
batch start
#iterations: 245
currently lose_sum: 506.6859417259693
time_elpased: 2.526
batch start
#iterations: 246
currently lose_sum: 508.13333037495613
time_elpased: 2.517
batch start
#iterations: 247
currently lose_sum: 508.68343353271484
time_elpased: 2.532
batch start
#iterations: 248
currently lose_sum: 507.43499422073364
time_elpased: 2.523
batch start
#iterations: 249
currently lose_sum: 510.11587929725647
time_elpased: 2.519
batch start
#iterations: 250
currently lose_sum: 508.2204999923706
time_elpased: 2.521
batch start
#iterations: 251
currently lose_sum: 509.89638978242874
time_elpased: 2.528
batch start
#iterations: 252
currently lose_sum: 511.36047357320786
time_elpased: 2.512
batch start
#iterations: 253
currently lose_sum: 508.7689230442047
time_elpased: 2.519
batch start
#iterations: 254
currently lose_sum: 510.4017913043499
time_elpased: 2.512
batch start
#iterations: 255
currently lose_sum: 506.9078048467636
time_elpased: 2.509
batch start
#iterations: 256
currently lose_sum: 510.30486422777176
time_elpased: 2.52
batch start
#iterations: 257
currently lose_sum: 507.60774651169777
time_elpased: 2.508
batch start
#iterations: 258
currently lose_sum: 507.73038998246193
time_elpased: 2.519
batch start
#iterations: 259
currently lose_sum: 509.7633254826069
time_elpased: 2.513
start validation test
0.56706185567
0.908469945355
0.138613861386
0.24052807668
0
validation finish
batch start
#iterations: 260
currently lose_sum: 509.5073285996914
time_elpased: 2.508
batch start
#iterations: 261
currently lose_sum: 509.54146108031273
time_elpased: 2.523
batch start
#iterations: 262
currently lose_sum: 508.59073382616043
time_elpased: 2.511
batch start
#iterations: 263
currently lose_sum: 508.1717964708805
time_elpased: 2.512
batch start
#iterations: 264
currently lose_sum: 507.3598272204399
time_elpased: 2.536
batch start
#iterations: 265
currently lose_sum: 509.16987016797066
time_elpased: 2.525
batch start
#iterations: 266
currently lose_sum: 507.5557106435299
time_elpased: 2.507
batch start
#iterations: 267
currently lose_sum: 507.31588393449783
time_elpased: 2.528
batch start
#iterations: 268
currently lose_sum: 508.4664571881294
time_elpased: 2.516
batch start
#iterations: 269
currently lose_sum: 506.50406634807587
time_elpased: 2.531
batch start
#iterations: 270
currently lose_sum: 509.36294040083885
time_elpased: 2.531
batch start
#iterations: 271
currently lose_sum: 505.71622785925865
time_elpased: 2.522
batch start
#iterations: 272
currently lose_sum: 508.6305424273014
time_elpased: 2.541
batch start
#iterations: 273
currently lose_sum: 507.83635407686234
time_elpased: 2.529
batch start
#iterations: 274
currently lose_sum: 507.97481593489647
time_elpased: 2.518
batch start
#iterations: 275
currently lose_sum: 507.00230464339256
time_elpased: 2.527
batch start
#iterations: 276
currently lose_sum: 508.11607199907303
time_elpased: 2.516
batch start
#iterations: 277
currently lose_sum: 509.60566863417625
time_elpased: 2.528
batch start
#iterations: 278
currently lose_sum: 508.2959063947201
time_elpased: 2.539
batch start
#iterations: 279
currently lose_sum: 509.83034241199493
time_elpased: 2.529
start validation test
0.57587628866
0.900410076157
0.160187597707
0.271987258892
0
validation finish
batch start
#iterations: 280
currently lose_sum: 507.7679913341999
time_elpased: 2.523
batch start
#iterations: 281
currently lose_sum: 506.2454195022583
time_elpased: 2.538
batch start
#iterations: 282
currently lose_sum: 507.89362758398056
time_elpased: 2.508
batch start
#iterations: 283
currently lose_sum: 509.03563445806503
time_elpased: 2.529
batch start
#iterations: 284
currently lose_sum: 508.5462332367897
time_elpased: 2.519
batch start
#iterations: 285
currently lose_sum: 509.11426812410355
time_elpased: 2.541
batch start
#iterations: 286
currently lose_sum: 509.3834182918072
time_elpased: 2.542
batch start
#iterations: 287
currently lose_sum: 509.9141888320446
time_elpased: 2.533
batch start
#iterations: 288
currently lose_sum: 507.50885713100433
time_elpased: 2.534
batch start
#iterations: 289
currently lose_sum: 507.8129181563854
time_elpased: 2.546
batch start
#iterations: 290
currently lose_sum: 507.3069765865803
time_elpased: 2.518
batch start
#iterations: 291
currently lose_sum: 509.1513643562794
time_elpased: 2.536
batch start
#iterations: 292
currently lose_sum: 508.4704199433327
time_elpased: 2.537
batch start
#iterations: 293
currently lose_sum: 509.5949105322361
time_elpased: 2.539
batch start
#iterations: 294
currently lose_sum: 509.63468196988106
time_elpased: 2.535
batch start
#iterations: 295
currently lose_sum: 508.25688099861145
time_elpased: 2.541
batch start
#iterations: 296
currently lose_sum: 507.600982606411
time_elpased: 2.542
batch start
#iterations: 297
currently lose_sum: 508.3890888094902
time_elpased: 2.527
batch start
#iterations: 298
currently lose_sum: 509.38638985157013
time_elpased: 2.513
batch start
#iterations: 299
currently lose_sum: 509.7943361401558
time_elpased: 2.502
start validation test
0.582164948454
0.893707033316
0.176133402814
0.294271286784
0
validation finish
batch start
#iterations: 300
currently lose_sum: 508.93687868118286
time_elpased: 2.515
batch start
#iterations: 301
currently lose_sum: 507.37290501594543
time_elpased: 2.51
batch start
#iterations: 302
currently lose_sum: 510.38627085089684
time_elpased: 2.526
batch start
#iterations: 303
currently lose_sum: 509.48061296343803
time_elpased: 2.515
batch start
#iterations: 304
currently lose_sum: 506.8648912012577
time_elpased: 2.494
batch start
#iterations: 305
currently lose_sum: 510.94934728741646
time_elpased: 2.527
batch start
#iterations: 306
currently lose_sum: 508.96820989251137
time_elpased: 2.513
batch start
#iterations: 307
currently lose_sum: 511.5333462655544
time_elpased: 2.517
batch start
#iterations: 308
currently lose_sum: 508.42932337522507
time_elpased: 2.537
batch start
#iterations: 309
currently lose_sum: 509.02058374881744
time_elpased: 2.55
batch start
#iterations: 310
currently lose_sum: 506.45597168803215
time_elpased: 2.53
batch start
#iterations: 311
currently lose_sum: 507.55978494882584
time_elpased: 2.535
batch start
#iterations: 312
currently lose_sum: 509.87144857645035
time_elpased: 2.505
batch start
#iterations: 313
currently lose_sum: 509.3844738602638
time_elpased: 2.516
batch start
#iterations: 314
currently lose_sum: 507.28521859645844
time_elpased: 2.52
batch start
#iterations: 315
currently lose_sum: 509.6715258657932
time_elpased: 2.497
batch start
#iterations: 316
currently lose_sum: 508.8369058072567
time_elpased: 2.54
batch start
#iterations: 317
currently lose_sum: 507.5415121614933
time_elpased: 2.539
batch start
#iterations: 318
currently lose_sum: 509.99176079034805
time_elpased: 2.528
batch start
#iterations: 319
currently lose_sum: 507.88728708028793
time_elpased: 2.542
start validation test
0.571958762887
0.895769466585
0.152266805628
0.260288615714
0
validation finish
batch start
#iterations: 320
currently lose_sum: 509.0355363190174
time_elpased: 2.521
batch start
#iterations: 321
currently lose_sum: 509.3803516328335
time_elpased: 2.505
batch start
#iterations: 322
currently lose_sum: 506.2803004682064
time_elpased: 2.534
batch start
#iterations: 323
currently lose_sum: 508.41932982206345
time_elpased: 2.51
batch start
#iterations: 324
currently lose_sum: 509.95088091492653
time_elpased: 2.507
batch start
#iterations: 325
currently lose_sum: 508.4485352933407
time_elpased: 2.513
batch start
#iterations: 326
currently lose_sum: 508.8816584944725
time_elpased: 2.505
batch start
#iterations: 327
currently lose_sum: 509.66279819607735
time_elpased: 2.531
batch start
#iterations: 328
currently lose_sum: 508.966854006052
time_elpased: 2.54
batch start
#iterations: 329
currently lose_sum: 507.6814604997635
time_elpased: 2.534
batch start
#iterations: 330
currently lose_sum: 510.12240079045296
time_elpased: 2.563
batch start
#iterations: 331
currently lose_sum: 510.10482931137085
time_elpased: 2.525
batch start
#iterations: 332
currently lose_sum: 510.3011100888252
time_elpased: 2.524
batch start
#iterations: 333
currently lose_sum: 508.83163970708847
time_elpased: 2.535
batch start
#iterations: 334
currently lose_sum: 508.73350471258163
time_elpased: 2.52
batch start
#iterations: 335
currently lose_sum: 509.16824874281883
time_elpased: 2.535
batch start
#iterations: 336
currently lose_sum: 509.5996249318123
time_elpased: 2.541
batch start
#iterations: 337
currently lose_sum: 508.687166929245
time_elpased: 2.511
batch start
#iterations: 338
currently lose_sum: 507.0543698966503
time_elpased: 2.557
batch start
#iterations: 339
currently lose_sum: 509.1062479317188
time_elpased: 2.532
start validation test
0.58412371134
0.901208617972
0.178738926524
0.298312750043
0
validation finish
batch start
#iterations: 340
currently lose_sum: 506.63641679286957
time_elpased: 2.509
batch start
#iterations: 341
currently lose_sum: 508.7936402261257
time_elpased: 2.551
batch start
#iterations: 342
currently lose_sum: 510.46053552627563
time_elpased: 2.532
batch start
#iterations: 343
currently lose_sum: 508.6501554250717
time_elpased: 2.52
batch start
#iterations: 344
currently lose_sum: 509.9619638323784
time_elpased: 2.536
batch start
#iterations: 345
currently lose_sum: 509.3435792028904
time_elpased: 2.525
batch start
#iterations: 346
currently lose_sum: 508.17184403538704
time_elpased: 2.538
batch start
#iterations: 347
currently lose_sum: 507.48095390200615
time_elpased: 2.524
batch start
#iterations: 348
currently lose_sum: 508.85235372185707
time_elpased: 2.512
batch start
#iterations: 349
currently lose_sum: 509.718067497015
time_elpased: 2.551
batch start
#iterations: 350
currently lose_sum: 508.35980558395386
time_elpased: 2.555
batch start
#iterations: 351
currently lose_sum: 508.15800073742867
time_elpased: 2.533
batch start
#iterations: 352
currently lose_sum: 507.92379465699196
time_elpased: 2.556
batch start
#iterations: 353
currently lose_sum: 509.62242433428764
time_elpased: 2.558
batch start
#iterations: 354
currently lose_sum: 507.61923441290855
time_elpased: 2.539
batch start
#iterations: 355
currently lose_sum: 508.1526216864586
time_elpased: 2.56
batch start
#iterations: 356
currently lose_sum: 506.84120988845825
time_elpased: 2.564
batch start
#iterations: 357
currently lose_sum: 508.9711582660675
time_elpased: 2.545
batch start
#iterations: 358
currently lose_sum: 509.3061848580837
time_elpased: 2.553
batch start
#iterations: 359
currently lose_sum: 507.56536173820496
time_elpased: 2.541
start validation test
0.567319587629
0.912148249828
0.138509640438
0.240499457112
0
validation finish
batch start
#iterations: 360
currently lose_sum: 508.07199773192406
time_elpased: 2.542
batch start
#iterations: 361
currently lose_sum: 507.28910529613495
time_elpased: 2.553
batch start
#iterations: 362
currently lose_sum: 507.4870281815529
time_elpased: 2.532
batch start
#iterations: 363
currently lose_sum: 508.3265027105808
time_elpased: 2.566
batch start
#iterations: 364
currently lose_sum: 509.3233117759228
time_elpased: 2.541
batch start
#iterations: 365
currently lose_sum: 507.83025202155113
time_elpased: 2.523
batch start
#iterations: 366
currently lose_sum: 507.9229298532009
time_elpased: 2.539
batch start
#iterations: 367
currently lose_sum: 508.49872052669525
time_elpased: 2.53
batch start
#iterations: 368
currently lose_sum: 507.20797660946846
time_elpased: 2.53
batch start
#iterations: 369
currently lose_sum: 507.41634249687195
time_elpased: 2.552
batch start
#iterations: 370
currently lose_sum: 506.67900025844574
time_elpased: 2.547
batch start
#iterations: 371
currently lose_sum: 507.3646481335163
time_elpased: 2.546
batch start
#iterations: 372
currently lose_sum: 507.8000871837139
time_elpased: 2.548
batch start
#iterations: 373
currently lose_sum: 507.71513041853905
time_elpased: 2.518
batch start
#iterations: 374
currently lose_sum: 509.2155316770077
time_elpased: 2.526
batch start
#iterations: 375
currently lose_sum: 508.97886458039284
time_elpased: 2.549
batch start
#iterations: 376
currently lose_sum: 508.5463617146015
time_elpased: 2.502
batch start
#iterations: 377
currently lose_sum: 507.43156737089157
time_elpased: 2.533
batch start
#iterations: 378
currently lose_sum: 509.0599316060543
time_elpased: 2.528
batch start
#iterations: 379
currently lose_sum: 509.61028200387955
time_elpased: 2.536
start validation test
0.575309278351
0.903571428571
0.158207399687
0.269268292683
0
validation finish
batch start
#iterations: 380
currently lose_sum: 508.47853621840477
time_elpased: 2.537
batch start
#iterations: 381
currently lose_sum: 507.9060677587986
time_elpased: 2.552
batch start
#iterations: 382
currently lose_sum: 508.49217507243156
time_elpased: 2.534
batch start
#iterations: 383
currently lose_sum: 506.1058270931244
time_elpased: 2.554
batch start
#iterations: 384
currently lose_sum: 507.4066162109375
time_elpased: 2.529
batch start
#iterations: 385
currently lose_sum: 506.85811230540276
time_elpased: 2.55
batch start
#iterations: 386
currently lose_sum: 508.47134217619896
time_elpased: 2.559
batch start
#iterations: 387
currently lose_sum: 507.3440451323986
time_elpased: 2.539
batch start
#iterations: 388
currently lose_sum: 507.54832607507706
time_elpased: 2.557
batch start
#iterations: 389
currently lose_sum: 507.607813000679
time_elpased: 2.543
batch start
#iterations: 390
currently lose_sum: 507.4847076535225
time_elpased: 2.528
batch start
#iterations: 391
currently lose_sum: 508.59138545393944
time_elpased: 2.566
batch start
#iterations: 392
currently lose_sum: 507.42922246456146
time_elpased: 2.539
batch start
#iterations: 393
currently lose_sum: 507.3530813753605
time_elpased: 2.534
batch start
#iterations: 394
currently lose_sum: 506.4183258116245
time_elpased: 2.574
batch start
#iterations: 395
currently lose_sum: 507.77776259183884
time_elpased: 2.541
batch start
#iterations: 396
currently lose_sum: 508.19608703255653
time_elpased: 2.554
batch start
#iterations: 397
currently lose_sum: 508.96812921762466
time_elpased: 2.564
batch start
#iterations: 398
currently lose_sum: 506.5524578988552
time_elpased: 2.554
batch start
#iterations: 399
currently lose_sum: 507.04583048820496
time_elpased: 2.568
start validation test
0.567835051546
0.915010281014
0.139134966128
0.24154152343
0
validation finish
acc: 0.604
pre: 0.865
rec: 0.253
F1: 0.391
auc: 0.000
