start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 556.0065421462059
time_elpased: 1.654
batch start
#iterations: 1
currently lose_sum: 544.6240966916084
time_elpased: 1.607
batch start
#iterations: 2
currently lose_sum: 540.0521076619625
time_elpased: 1.6
batch start
#iterations: 3
currently lose_sum: 536.229967623949
time_elpased: 1.599
batch start
#iterations: 4
currently lose_sum: 531.9720177054405
time_elpased: 1.619
batch start
#iterations: 5
currently lose_sum: 530.7298184633255
time_elpased: 1.604
batch start
#iterations: 6
currently lose_sum: 526.3007474541664
time_elpased: 1.595
batch start
#iterations: 7
currently lose_sum: 527.106208384037
time_elpased: 1.593
batch start
#iterations: 8
currently lose_sum: 524.944976836443
time_elpased: 1.593
batch start
#iterations: 9
currently lose_sum: 524.6766228973866
time_elpased: 1.596
batch start
#iterations: 10
currently lose_sum: 524.1690121889114
time_elpased: 1.601
batch start
#iterations: 11
currently lose_sum: 523.7126425802708
time_elpased: 1.598
batch start
#iterations: 12
currently lose_sum: 522.4309604167938
time_elpased: 1.603
batch start
#iterations: 13
currently lose_sum: 521.7923094332218
time_elpased: 1.598
batch start
#iterations: 14
currently lose_sum: 521.5827999711037
time_elpased: 1.601
batch start
#iterations: 15
currently lose_sum: 520.7713766694069
time_elpased: 1.613
batch start
#iterations: 16
currently lose_sum: 521.234386652708
time_elpased: 1.602
batch start
#iterations: 17
currently lose_sum: 520.962164580822
time_elpased: 1.606
batch start
#iterations: 18
currently lose_sum: 520.3843414485455
time_elpased: 1.616
batch start
#iterations: 19
currently lose_sum: 520.1063004136086
time_elpased: 1.604
start validation test
0.583298969072
0.874763705104
0.191416752844
0.314101476328
0
validation finish
batch start
#iterations: 20
currently lose_sum: 520.4457115530968
time_elpased: 1.61
batch start
#iterations: 21
currently lose_sum: 521.5286489725113
time_elpased: 1.601
batch start
#iterations: 22
currently lose_sum: 519.8894280791283
time_elpased: 1.625
batch start
#iterations: 23
currently lose_sum: 518.1966136395931
time_elpased: 1.617
batch start
#iterations: 24
currently lose_sum: 517.5896751880646
time_elpased: 1.599
batch start
#iterations: 25
currently lose_sum: 520.0737344026566
time_elpased: 1.603
batch start
#iterations: 26
currently lose_sum: 518.2364978790283
time_elpased: 1.61
batch start
#iterations: 27
currently lose_sum: 517.9059432148933
time_elpased: 1.6
batch start
#iterations: 28
currently lose_sum: 516.9468228518963
time_elpased: 1.591
batch start
#iterations: 29
currently lose_sum: 517.3652093410492
time_elpased: 1.619
batch start
#iterations: 30
currently lose_sum: 516.2691379189491
time_elpased: 1.607
batch start
#iterations: 31
currently lose_sum: 516.1191602349281
time_elpased: 1.597
batch start
#iterations: 32
currently lose_sum: 518.1259860396385
time_elpased: 1.604
batch start
#iterations: 33
currently lose_sum: 516.2821828722954
time_elpased: 1.602
batch start
#iterations: 34
currently lose_sum: 518.0061224400997
time_elpased: 1.618
batch start
#iterations: 35
currently lose_sum: 515.3014349639416
time_elpased: 1.62
batch start
#iterations: 36
currently lose_sum: 517.0044693350792
time_elpased: 1.601
batch start
#iterations: 37
currently lose_sum: 515.2128749191761
time_elpased: 1.612
batch start
#iterations: 38
currently lose_sum: 516.6101113259792
time_elpased: 1.588
batch start
#iterations: 39
currently lose_sum: 516.851936519146
time_elpased: 1.6
start validation test
0.565670103093
0.857471264368
0.154291623578
0.261524978089
0
validation finish
batch start
#iterations: 40
currently lose_sum: 517.3747330605984
time_elpased: 1.608
batch start
#iterations: 41
currently lose_sum: 517.68538159132
time_elpased: 1.606
batch start
#iterations: 42
currently lose_sum: 516.0298800766468
time_elpased: 1.593
batch start
#iterations: 43
currently lose_sum: 515.9507490992546
time_elpased: 1.606
batch start
#iterations: 44
currently lose_sum: 515.3494492769241
time_elpased: 1.598
batch start
#iterations: 45
currently lose_sum: 514.8124320209026
time_elpased: 1.602
batch start
#iterations: 46
currently lose_sum: 515.1219965815544
time_elpased: 1.599
batch start
#iterations: 47
currently lose_sum: 515.0266344547272
time_elpased: 1.605
batch start
#iterations: 48
currently lose_sum: 515.1721843779087
time_elpased: 1.585
batch start
#iterations: 49
currently lose_sum: 513.7501786649227
time_elpased: 1.609
batch start
#iterations: 50
currently lose_sum: 516.6104937195778
time_elpased: 1.605
batch start
#iterations: 51
currently lose_sum: 515.7005814611912
time_elpased: 1.61
batch start
#iterations: 52
currently lose_sum: 514.2338226139545
time_elpased: 1.606
batch start
#iterations: 53
currently lose_sum: 516.8258427679539
time_elpased: 1.6
batch start
#iterations: 54
currently lose_sum: 514.1235810816288
time_elpased: 1.593
batch start
#iterations: 55
currently lose_sum: 515.8305993676186
time_elpased: 1.61
batch start
#iterations: 56
currently lose_sum: 515.6692621707916
time_elpased: 1.597
batch start
#iterations: 57
currently lose_sum: 512.2213244736195
time_elpased: 1.606
batch start
#iterations: 58
currently lose_sum: 516.2130396664143
time_elpased: 1.586
batch start
#iterations: 59
currently lose_sum: 513.277334779501
time_elpased: 1.607
start validation test
0.579020618557
0.882442748092
0.179317476732
0.29806617963
0
validation finish
batch start
#iterations: 60
currently lose_sum: 514.9825897812843
time_elpased: 1.633
batch start
#iterations: 61
currently lose_sum: 515.1859089136124
time_elpased: 1.603
batch start
#iterations: 62
currently lose_sum: 517.0240076780319
time_elpased: 1.602
batch start
#iterations: 63
currently lose_sum: 516.0859933793545
time_elpased: 1.618
batch start
#iterations: 64
currently lose_sum: 516.1069132983685
time_elpased: 1.605
batch start
#iterations: 65
currently lose_sum: 515.3467524051666
time_elpased: 1.614
batch start
#iterations: 66
currently lose_sum: 514.7329839169979
time_elpased: 1.596
batch start
#iterations: 67
currently lose_sum: 512.6498465240002
time_elpased: 1.604
batch start
#iterations: 68
currently lose_sum: 516.257273286581
time_elpased: 1.605
batch start
#iterations: 69
currently lose_sum: 514.1873851120472
time_elpased: 1.603
batch start
#iterations: 70
currently lose_sum: 515.0371492505074
time_elpased: 1.597
batch start
#iterations: 71
currently lose_sum: 515.3652501702309
time_elpased: 1.596
batch start
#iterations: 72
currently lose_sum: 514.580081909895
time_elpased: 1.601
batch start
#iterations: 73
currently lose_sum: 513.8134013712406
time_elpased: 1.599
batch start
#iterations: 74
currently lose_sum: 513.78599396348
time_elpased: 1.595
batch start
#iterations: 75
currently lose_sum: 513.1420630216599
time_elpased: 1.604
batch start
#iterations: 76
currently lose_sum: 514.479546636343
time_elpased: 1.608
batch start
#iterations: 77
currently lose_sum: 514.1018861532211
time_elpased: 1.604
batch start
#iterations: 78
currently lose_sum: 514.2784640192986
time_elpased: 1.597
batch start
#iterations: 79
currently lose_sum: 516.2158914208412
time_elpased: 1.597
start validation test
0.596958762887
0.873637464675
0.223784901758
0.356301967564
0
validation finish
batch start
#iterations: 80
currently lose_sum: 512.0032476186752
time_elpased: 1.617
batch start
#iterations: 81
currently lose_sum: 514.2229062914848
time_elpased: 1.598
batch start
#iterations: 82
currently lose_sum: 514.1607755124569
time_elpased: 1.603
batch start
#iterations: 83
currently lose_sum: 515.1658626794815
time_elpased: 1.605
batch start
#iterations: 84
currently lose_sum: 514.4050333201885
time_elpased: 1.61
batch start
#iterations: 85
currently lose_sum: 511.88079380989075
time_elpased: 1.602
batch start
#iterations: 86
currently lose_sum: 514.0677503347397
time_elpased: 1.606
batch start
#iterations: 87
currently lose_sum: 511.4920258820057
time_elpased: 1.596
batch start
#iterations: 88
currently lose_sum: 513.5536185503006
time_elpased: 1.619
batch start
#iterations: 89
currently lose_sum: 512.9482788145542
time_elpased: 1.594
batch start
#iterations: 90
currently lose_sum: 514.2799192368984
time_elpased: 1.603
batch start
#iterations: 91
currently lose_sum: 512.8511072993279
time_elpased: 1.599
batch start
#iterations: 92
currently lose_sum: 513.9893718063831
time_elpased: 1.61
batch start
#iterations: 93
currently lose_sum: 513.882846057415
time_elpased: 1.611
batch start
#iterations: 94
currently lose_sum: 511.85959899425507
time_elpased: 1.608
batch start
#iterations: 95
currently lose_sum: 512.0240331292152
time_elpased: 1.597
batch start
#iterations: 96
currently lose_sum: 513.6153038740158
time_elpased: 1.616
batch start
#iterations: 97
currently lose_sum: 512.2522366344929
time_elpased: 1.601
batch start
#iterations: 98
currently lose_sum: 513.5410458743572
time_elpased: 1.609
batch start
#iterations: 99
currently lose_sum: 513.6389588415623
time_elpased: 1.608
start validation test
0.534948453608
0.909090909091
0.0744570837642
0.137640986427
0
validation finish
batch start
#iterations: 100
currently lose_sum: 513.7203499376774
time_elpased: 1.604
batch start
#iterations: 101
currently lose_sum: 511.5484598875046
time_elpased: 1.602
batch start
#iterations: 102
currently lose_sum: 510.9676502943039
time_elpased: 1.595
batch start
#iterations: 103
currently lose_sum: 512.2043330967426
time_elpased: 1.6
batch start
#iterations: 104
currently lose_sum: 513.502018481493
time_elpased: 1.601
batch start
#iterations: 105
currently lose_sum: 511.8218349814415
time_elpased: 1.597
batch start
#iterations: 106
currently lose_sum: 513.944516569376
time_elpased: 1.615
batch start
#iterations: 107
currently lose_sum: 512.548038393259
time_elpased: 1.6
batch start
#iterations: 108
currently lose_sum: 511.2162995040417
time_elpased: 1.614
batch start
#iterations: 109
currently lose_sum: 513.5285138487816
time_elpased: 1.605
batch start
#iterations: 110
currently lose_sum: 513.107833057642
time_elpased: 1.613
batch start
#iterations: 111
currently lose_sum: 515.3360983431339
time_elpased: 1.617
batch start
#iterations: 112
currently lose_sum: 510.4758434891701
time_elpased: 1.602
batch start
#iterations: 113
currently lose_sum: 512.0956107079983
time_elpased: 1.61
batch start
#iterations: 114
currently lose_sum: 512.5999859571457
time_elpased: 1.599
batch start
#iterations: 115
currently lose_sum: 511.740073800087
time_elpased: 1.603
batch start
#iterations: 116
currently lose_sum: 509.98625671863556
time_elpased: 1.613
batch start
#iterations: 117
currently lose_sum: 512.2361043989658
time_elpased: 1.602
batch start
#iterations: 118
currently lose_sum: 511.73514583706856
time_elpased: 1.614
batch start
#iterations: 119
currently lose_sum: 511.6174971461296
time_elpased: 1.616
start validation test
0.582268041237
0.88685770751
0.185625646329
0.306995040192
0
validation finish
batch start
#iterations: 120
currently lose_sum: 513.060504257679
time_elpased: 1.634
batch start
#iterations: 121
currently lose_sum: 510.2061016857624
time_elpased: 1.595
batch start
#iterations: 122
currently lose_sum: 511.47247090935707
time_elpased: 1.598
batch start
#iterations: 123
currently lose_sum: 510.81614658236504
time_elpased: 1.615
batch start
#iterations: 124
currently lose_sum: 512.1385162174702
time_elpased: 1.605
batch start
#iterations: 125
currently lose_sum: 512.364726215601
time_elpased: 1.605
batch start
#iterations: 126
currently lose_sum: 511.20733919739723
time_elpased: 1.618
batch start
#iterations: 127
currently lose_sum: 512.9675564169884
time_elpased: 1.608
batch start
#iterations: 128
currently lose_sum: 512.0580649077892
time_elpased: 1.6
batch start
#iterations: 129
currently lose_sum: 511.166628241539
time_elpased: 1.613
batch start
#iterations: 130
currently lose_sum: 511.8417270183563
time_elpased: 1.624
batch start
#iterations: 131
currently lose_sum: 511.6679218709469
time_elpased: 1.619
batch start
#iterations: 132
currently lose_sum: 512.8716343939304
time_elpased: 1.597
batch start
#iterations: 133
currently lose_sum: 512.8692332208157
time_elpased: 1.61
batch start
#iterations: 134
currently lose_sum: 512.5465009510517
time_elpased: 1.612
batch start
#iterations: 135
currently lose_sum: 510.0127376616001
time_elpased: 1.627
batch start
#iterations: 136
currently lose_sum: 511.1491212248802
time_elpased: 1.597
batch start
#iterations: 137
currently lose_sum: 510.8851960301399
time_elpased: 1.605
batch start
#iterations: 138
currently lose_sum: 512.38419470191
time_elpased: 1.611
batch start
#iterations: 139
currently lose_sum: 511.8238300383091
time_elpased: 1.599
start validation test
0.556494845361
0.885115606936
0.126680455016
0.22163922562
0
validation finish
batch start
#iterations: 140
currently lose_sum: 508.38300612568855
time_elpased: 1.63
batch start
#iterations: 141
currently lose_sum: 513.3399814367294
time_elpased: 1.609
batch start
#iterations: 142
currently lose_sum: 512.0218949019909
time_elpased: 1.641
batch start
#iterations: 143
currently lose_sum: 513.4974434673786
time_elpased: 1.61
batch start
#iterations: 144
currently lose_sum: 512.5775816142559
time_elpased: 1.593
batch start
#iterations: 145
currently lose_sum: 512.2506828606129
time_elpased: 1.612
batch start
#iterations: 146
currently lose_sum: 514.6079822480679
time_elpased: 1.605
batch start
#iterations: 147
currently lose_sum: 511.2621451020241
time_elpased: 1.597
batch start
#iterations: 148
currently lose_sum: 509.2135507762432
time_elpased: 1.607
batch start
#iterations: 149
currently lose_sum: 514.4620893895626
time_elpased: 1.607
batch start
#iterations: 150
currently lose_sum: 512.399565756321
time_elpased: 1.611
batch start
#iterations: 151
currently lose_sum: 511.44065049290657
time_elpased: 1.617
batch start
#iterations: 152
currently lose_sum: 512.5908115506172
time_elpased: 1.613
batch start
#iterations: 153
currently lose_sum: 508.2236468195915
time_elpased: 1.61
batch start
#iterations: 154
currently lose_sum: 514.5805602371693
time_elpased: 1.615
batch start
#iterations: 155
currently lose_sum: 510.04372626543045
time_elpased: 1.605
batch start
#iterations: 156
currently lose_sum: 511.45868304371834
time_elpased: 1.624
batch start
#iterations: 157
currently lose_sum: 512.1855220198631
time_elpased: 1.596
batch start
#iterations: 158
currently lose_sum: 510.47762551903725
time_elpased: 1.617
batch start
#iterations: 159
currently lose_sum: 512.6832138299942
time_elpased: 1.619
start validation test
0.546082474227
0.907547169811
0.0994829369183
0.179310344828
0
validation finish
batch start
#iterations: 160
currently lose_sum: 511.89370879530907
time_elpased: 1.611
batch start
#iterations: 161
currently lose_sum: 512.0694613158703
time_elpased: 1.599
batch start
#iterations: 162
currently lose_sum: 511.1041089296341
time_elpased: 1.61
batch start
#iterations: 163
currently lose_sum: 509.3586294949055
time_elpased: 1.599
batch start
#iterations: 164
currently lose_sum: 513.1252260506153
time_elpased: 1.602
batch start
#iterations: 165
currently lose_sum: 511.4117514193058
time_elpased: 1.601
batch start
#iterations: 166
currently lose_sum: 510.549656689167
time_elpased: 1.591
batch start
#iterations: 167
currently lose_sum: 512.9977025091648
time_elpased: 1.605
batch start
#iterations: 168
currently lose_sum: 511.78571259975433
time_elpased: 1.61
batch start
#iterations: 169
currently lose_sum: 514.4707254171371
time_elpased: 1.606
batch start
#iterations: 170
currently lose_sum: 511.17357674241066
time_elpased: 1.601
batch start
#iterations: 171
currently lose_sum: 510.43564131855965
time_elpased: 1.601
batch start
#iterations: 172
currently lose_sum: 513.4152733981609
time_elpased: 1.611
batch start
#iterations: 173
currently lose_sum: 511.6907514035702
time_elpased: 1.601
batch start
#iterations: 174
currently lose_sum: 513.2140626013279
time_elpased: 1.624
batch start
#iterations: 175
currently lose_sum: 510.9038381278515
time_elpased: 1.604
batch start
#iterations: 176
currently lose_sum: 511.56038546562195
time_elpased: 1.619
batch start
#iterations: 177
currently lose_sum: 511.51041570305824
time_elpased: 1.618
batch start
#iterations: 178
currently lose_sum: 511.423100233078
time_elpased: 1.611
batch start
#iterations: 179
currently lose_sum: 511.632021009922
time_elpased: 1.607
start validation test
0.554690721649
0.89623366641
0.120579110651
0.212560386473
0
validation finish
batch start
#iterations: 180
currently lose_sum: 512.3385587632656
time_elpased: 1.607
batch start
#iterations: 181
currently lose_sum: 514.1573485434055
time_elpased: 1.609
batch start
#iterations: 182
currently lose_sum: 509.8243253827095
time_elpased: 1.605
batch start
#iterations: 183
currently lose_sum: 512.2035121023655
time_elpased: 1.618
batch start
#iterations: 184
currently lose_sum: 511.11879247426987
time_elpased: 1.602
batch start
#iterations: 185
currently lose_sum: 510.16021245718
time_elpased: 1.599
batch start
#iterations: 186
currently lose_sum: 513.0706585347652
time_elpased: 1.602
batch start
#iterations: 187
currently lose_sum: 510.4658711850643
time_elpased: 1.611
batch start
#iterations: 188
currently lose_sum: 509.42165756225586
time_elpased: 1.62
batch start
#iterations: 189
currently lose_sum: 509.2362511456013
time_elpased: 1.606
batch start
#iterations: 190
currently lose_sum: 509.2074241042137
time_elpased: 1.603
batch start
#iterations: 191
currently lose_sum: 513.0070929527283
time_elpased: 1.609
batch start
#iterations: 192
currently lose_sum: 510.48465645313263
time_elpased: 1.611
batch start
#iterations: 193
currently lose_sum: 510.517312169075
time_elpased: 1.61
batch start
#iterations: 194
currently lose_sum: 511.7833804190159
time_elpased: 1.638
batch start
#iterations: 195
currently lose_sum: 511.6887676715851
time_elpased: 1.601
batch start
#iterations: 196
currently lose_sum: 509.87317752838135
time_elpased: 1.605
batch start
#iterations: 197
currently lose_sum: 512.1299081742764
time_elpased: 1.62
batch start
#iterations: 198
currently lose_sum: 512.7449062168598
time_elpased: 1.62
batch start
#iterations: 199
currently lose_sum: 510.98010927438736
time_elpased: 1.604
start validation test
0.534432989691
0.902777777778
0.0739400206825
0.136685146244
0
validation finish
batch start
#iterations: 200
currently lose_sum: 513.1742176413536
time_elpased: 1.627
batch start
#iterations: 201
currently lose_sum: 511.5334560573101
time_elpased: 1.608
batch start
#iterations: 202
currently lose_sum: 511.5382970869541
time_elpased: 1.612
batch start
#iterations: 203
currently lose_sum: 510.0884085595608
time_elpased: 1.607
batch start
#iterations: 204
currently lose_sum: 511.96609801054
time_elpased: 1.603
batch start
#iterations: 205
currently lose_sum: 511.4607543349266
time_elpased: 1.604
batch start
#iterations: 206
currently lose_sum: 511.93796545267105
time_elpased: 1.603
batch start
#iterations: 207
currently lose_sum: 510.67988270521164
time_elpased: 1.607
batch start
#iterations: 208
currently lose_sum: 510.2611086666584
time_elpased: 1.609
batch start
#iterations: 209
currently lose_sum: 510.71258983016014
time_elpased: 1.611
batch start
#iterations: 210
currently lose_sum: 512.6458474695683
time_elpased: 1.604
batch start
#iterations: 211
currently lose_sum: 511.137843310833
time_elpased: 1.608
batch start
#iterations: 212
currently lose_sum: 511.2484191954136
time_elpased: 1.601
batch start
#iterations: 213
currently lose_sum: 509.7147659063339
time_elpased: 1.607
batch start
#iterations: 214
currently lose_sum: 511.28327268362045
time_elpased: 1.611
batch start
#iterations: 215
currently lose_sum: 511.4476051926613
time_elpased: 1.613
batch start
#iterations: 216
currently lose_sum: 511.3809284567833
time_elpased: 1.61
batch start
#iterations: 217
currently lose_sum: 510.42513459920883
time_elpased: 1.604
batch start
#iterations: 218
currently lose_sum: 512.2145997285843
time_elpased: 1.601
batch start
#iterations: 219
currently lose_sum: 511.9034725725651
time_elpased: 1.606
start validation test
0.562474226804
0.889841688654
0.139503619442
0.24119435008
0
validation finish
batch start
#iterations: 220
currently lose_sum: 510.37499314546585
time_elpased: 1.612
batch start
#iterations: 221
currently lose_sum: 511.217785179615
time_elpased: 1.61
batch start
#iterations: 222
currently lose_sum: 510.02423533797264
time_elpased: 1.617
batch start
#iterations: 223
currently lose_sum: 512.0610955357552
time_elpased: 1.606
batch start
#iterations: 224
currently lose_sum: 510.56809121370316
time_elpased: 1.622
batch start
#iterations: 225
currently lose_sum: 512.464575022459
time_elpased: 1.619
batch start
#iterations: 226
currently lose_sum: 511.8928379714489
time_elpased: 1.604
batch start
#iterations: 227
currently lose_sum: 509.20881363749504
time_elpased: 1.611
batch start
#iterations: 228
currently lose_sum: 509.42504692077637
time_elpased: 1.608
batch start
#iterations: 229
currently lose_sum: 509.78393337130547
time_elpased: 1.615
batch start
#iterations: 230
currently lose_sum: 512.5059833824635
time_elpased: 1.619
batch start
#iterations: 231
currently lose_sum: 514.9939702451229
time_elpased: 1.611
batch start
#iterations: 232
currently lose_sum: 510.1942881643772
time_elpased: 1.614
batch start
#iterations: 233
currently lose_sum: 509.31659173965454
time_elpased: 1.611
batch start
#iterations: 234
currently lose_sum: 510.09335348010063
time_elpased: 1.606
batch start
#iterations: 235
currently lose_sum: 510.7352582216263
time_elpased: 1.634
batch start
#iterations: 236
currently lose_sum: 508.7801864743233
time_elpased: 1.621
batch start
#iterations: 237
currently lose_sum: 511.76836186647415
time_elpased: 1.609
batch start
#iterations: 238
currently lose_sum: 509.74739959836006
time_elpased: 1.608
batch start
#iterations: 239
currently lose_sum: 510.74850434064865
time_elpased: 1.611
start validation test
0.524587628866
0.901256732496
0.0519131334023
0.0981715067957
0
validation finish
batch start
#iterations: 240
currently lose_sum: 509.49328911304474
time_elpased: 1.615
batch start
#iterations: 241
currently lose_sum: 510.97683477401733
time_elpased: 1.608
batch start
#iterations: 242
currently lose_sum: 509.4894898235798
time_elpased: 1.611
batch start
#iterations: 243
currently lose_sum: 508.8049885034561
time_elpased: 1.615
batch start
#iterations: 244
currently lose_sum: 510.13683182001114
time_elpased: 1.603
batch start
#iterations: 245
currently lose_sum: 509.4996339082718
time_elpased: 1.61
batch start
#iterations: 246
currently lose_sum: 510.33400893211365
time_elpased: 1.608
batch start
#iterations: 247
currently lose_sum: 508.3306634128094
time_elpased: 1.632
batch start
#iterations: 248
currently lose_sum: 509.7577142715454
time_elpased: 1.595
batch start
#iterations: 249
currently lose_sum: 511.21076276898384
time_elpased: 1.612
batch start
#iterations: 250
currently lose_sum: 509.7449421286583
time_elpased: 1.61
batch start
#iterations: 251
currently lose_sum: 512.5872182548046
time_elpased: 1.628
batch start
#iterations: 252
currently lose_sum: 510.9953165948391
time_elpased: 1.605
batch start
#iterations: 253
currently lose_sum: 509.5922337472439
time_elpased: 1.609
batch start
#iterations: 254
currently lose_sum: 510.09175711870193
time_elpased: 1.617
batch start
#iterations: 255
currently lose_sum: 508.99960601329803
time_elpased: 1.612
batch start
#iterations: 256
currently lose_sum: 509.9104324877262
time_elpased: 1.62
batch start
#iterations: 257
currently lose_sum: 508.82350796461105
time_elpased: 1.598
batch start
#iterations: 258
currently lose_sum: 510.8435002863407
time_elpased: 1.598
batch start
#iterations: 259
currently lose_sum: 512.1619284152985
time_elpased: 1.612
start validation test
0.553762886598
0.880540946657
0.12119958635
0.213071538951
0
validation finish
batch start
#iterations: 260
currently lose_sum: 512.5757995247841
time_elpased: 1.608
batch start
#iterations: 261
currently lose_sum: 511.40059411525726
time_elpased: 1.621
batch start
#iterations: 262
currently lose_sum: 509.27934616804123
time_elpased: 1.628
batch start
#iterations: 263
currently lose_sum: 510.56275314092636
time_elpased: 1.595
batch start
#iterations: 264
currently lose_sum: 510.97299748659134
time_elpased: 1.612
batch start
#iterations: 265
currently lose_sum: 511.01839074492455
time_elpased: 1.613
batch start
#iterations: 266
currently lose_sum: 510.1304160952568
time_elpased: 1.634
batch start
#iterations: 267
currently lose_sum: 510.95808240771294
time_elpased: 1.608
batch start
#iterations: 268
currently lose_sum: 509.87836876511574
time_elpased: 1.602
batch start
#iterations: 269
currently lose_sum: 509.93949419260025
time_elpased: 1.606
batch start
#iterations: 270
currently lose_sum: 509.0115274786949
time_elpased: 1.613
batch start
#iterations: 271
currently lose_sum: 508.0879308283329
time_elpased: 1.648
batch start
#iterations: 272
currently lose_sum: 510.52652594447136
time_elpased: 1.605
batch start
#iterations: 273
currently lose_sum: 508.8077303469181
time_elpased: 1.629
batch start
#iterations: 274
currently lose_sum: 509.04416078329086
time_elpased: 1.621
batch start
#iterations: 275
currently lose_sum: 510.79769867658615
time_elpased: 1.602
batch start
#iterations: 276
currently lose_sum: 509.6431967318058
time_elpased: 1.596
batch start
#iterations: 277
currently lose_sum: 511.95286682248116
time_elpased: 1.601
batch start
#iterations: 278
currently lose_sum: 509.75179478526115
time_elpased: 1.599
batch start
#iterations: 279
currently lose_sum: 511.8177123963833
time_elpased: 1.618
start validation test
0.533556701031
0.908015768725
0.0714581178904
0.132489694181
0
validation finish
batch start
#iterations: 280
currently lose_sum: 508.17606833577156
time_elpased: 1.624
batch start
#iterations: 281
currently lose_sum: 509.98113733530045
time_elpased: 1.608
batch start
#iterations: 282
currently lose_sum: 510.209725856781
time_elpased: 1.596
batch start
#iterations: 283
currently lose_sum: 510.2193342447281
time_elpased: 1.608
batch start
#iterations: 284
currently lose_sum: 511.5695414841175
time_elpased: 1.618
batch start
#iterations: 285
currently lose_sum: 509.90973871946335
time_elpased: 1.604
batch start
#iterations: 286
currently lose_sum: 512.9981159567833
time_elpased: 1.61
batch start
#iterations: 287
currently lose_sum: 511.54099717736244
time_elpased: 1.594
batch start
#iterations: 288
currently lose_sum: 511.980608522892
time_elpased: 1.612
batch start
#iterations: 289
currently lose_sum: 510.02882969379425
time_elpased: 1.627
batch start
#iterations: 290
currently lose_sum: 509.448010802269
time_elpased: 1.603
batch start
#iterations: 291
currently lose_sum: 510.7807445824146
time_elpased: 1.623
batch start
#iterations: 292
currently lose_sum: 510.33777180314064
time_elpased: 1.617
batch start
#iterations: 293
currently lose_sum: 510.07356294989586
time_elpased: 1.61
batch start
#iterations: 294
currently lose_sum: 510.9717301428318
time_elpased: 1.611
batch start
#iterations: 295
currently lose_sum: 510.26374289393425
time_elpased: 1.602
batch start
#iterations: 296
currently lose_sum: 508.97671657800674
time_elpased: 1.615
batch start
#iterations: 297
currently lose_sum: 512.0984541475773
time_elpased: 1.634
batch start
#iterations: 298
currently lose_sum: 509.4832915663719
time_elpased: 1.621
batch start
#iterations: 299
currently lose_sum: 511.22224190831184
time_elpased: 1.607
start validation test
0.567113402062
0.884522370012
0.151292657704
0.258389261745
0
validation finish
batch start
#iterations: 300
currently lose_sum: 510.5362273156643
time_elpased: 1.623
batch start
#iterations: 301
currently lose_sum: 510.4175797402859
time_elpased: 1.608
batch start
#iterations: 302
currently lose_sum: 512.1433965265751
time_elpased: 1.595
batch start
#iterations: 303
currently lose_sum: 511.5301025211811
time_elpased: 1.607
batch start
#iterations: 304
currently lose_sum: 507.0291926562786
time_elpased: 1.62
batch start
#iterations: 305
currently lose_sum: 511.2682172358036
time_elpased: 1.607
batch start
#iterations: 306
currently lose_sum: 510.16476610302925
time_elpased: 1.615
batch start
#iterations: 307
currently lose_sum: 510.7820979952812
time_elpased: 1.596
batch start
#iterations: 308
currently lose_sum: 509.0794279575348
time_elpased: 1.609
batch start
#iterations: 309
currently lose_sum: 510.6331013441086
time_elpased: 1.605
batch start
#iterations: 310
currently lose_sum: 509.4188511669636
time_elpased: 1.616
batch start
#iterations: 311
currently lose_sum: 508.38873744010925
time_elpased: 1.603
batch start
#iterations: 312
currently lose_sum: 509.87855300307274
time_elpased: 1.601
batch start
#iterations: 313
currently lose_sum: 512.0461390018463
time_elpased: 1.604
batch start
#iterations: 314
currently lose_sum: 509.4717125892639
time_elpased: 1.607
batch start
#iterations: 315
currently lose_sum: 509.9802937209606
time_elpased: 1.619
batch start
#iterations: 316
currently lose_sum: 509.9643497467041
time_elpased: 1.608
batch start
#iterations: 317
currently lose_sum: 509.4956107735634
time_elpased: 1.607
batch start
#iterations: 318
currently lose_sum: 513.2137927114964
time_elpased: 1.604
batch start
#iterations: 319
currently lose_sum: 510.36389604210854
time_elpased: 1.597
start validation test
0.538762886598
0.892391304348
0.0849017580145
0.155051935788
0
validation finish
batch start
#iterations: 320
currently lose_sum: 509.9960342645645
time_elpased: 1.611
batch start
#iterations: 321
currently lose_sum: 511.3432345688343
time_elpased: 1.606
batch start
#iterations: 322
currently lose_sum: 508.4156173169613
time_elpased: 1.6
batch start
#iterations: 323
currently lose_sum: 507.79099693894386
time_elpased: 1.612
batch start
#iterations: 324
currently lose_sum: 511.9645761847496
time_elpased: 1.595
batch start
#iterations: 325
currently lose_sum: 509.05523949861526
time_elpased: 1.604
batch start
#iterations: 326
currently lose_sum: 509.76718041300774
time_elpased: 1.619
batch start
#iterations: 327
currently lose_sum: 511.2368732392788
time_elpased: 1.604
batch start
#iterations: 328
currently lose_sum: 511.5164598226547
time_elpased: 1.607
batch start
#iterations: 329
currently lose_sum: 509.34572032094
time_elpased: 1.611
batch start
#iterations: 330
currently lose_sum: 512.3140476942062
time_elpased: 1.61
batch start
#iterations: 331
currently lose_sum: 512.154982149601
time_elpased: 1.604
batch start
#iterations: 332
currently lose_sum: 511.24828627705574
time_elpased: 1.601
batch start
#iterations: 333
currently lose_sum: 509.789452701807
time_elpased: 1.594
batch start
#iterations: 334
currently lose_sum: 509.6869983077049
time_elpased: 1.606
batch start
#iterations: 335
currently lose_sum: 509.1738823354244
time_elpased: 1.605
batch start
#iterations: 336
currently lose_sum: 510.2013669013977
time_elpased: 1.607
batch start
#iterations: 337
currently lose_sum: 510.5362560451031
time_elpased: 1.6
batch start
#iterations: 338
currently lose_sum: 511.01533222198486
time_elpased: 1.603
batch start
#iterations: 339
currently lose_sum: 509.60786589980125
time_elpased: 1.617
start validation test
0.55
0.892320534224
0.110548086867
0.196724328303
0
validation finish
batch start
#iterations: 340
currently lose_sum: 510.84239661693573
time_elpased: 1.625
batch start
#iterations: 341
currently lose_sum: 509.59473541378975
time_elpased: 1.645
batch start
#iterations: 342
currently lose_sum: 510.84904393553734
time_elpased: 1.61
batch start
#iterations: 343
currently lose_sum: 510.75758120417595
time_elpased: 1.615
batch start
#iterations: 344
currently lose_sum: 510.9099393785
time_elpased: 1.598
batch start
#iterations: 345
currently lose_sum: 509.7720249891281
time_elpased: 1.607
batch start
#iterations: 346
currently lose_sum: 508.8604620695114
time_elpased: 1.605
batch start
#iterations: 347
currently lose_sum: 510.27343958616257
time_elpased: 1.606
batch start
#iterations: 348
currently lose_sum: 509.38680750131607
time_elpased: 1.617
batch start
#iterations: 349
currently lose_sum: 510.75932335853577
time_elpased: 1.6
batch start
#iterations: 350
currently lose_sum: 510.28708866238594
time_elpased: 1.613
batch start
#iterations: 351
currently lose_sum: 509.8385641872883
time_elpased: 1.611
batch start
#iterations: 352
currently lose_sum: 510.0472710132599
time_elpased: 1.617
batch start
#iterations: 353
currently lose_sum: 512.2980481684208
time_elpased: 1.617
batch start
#iterations: 354
currently lose_sum: 509.43258446455
time_elpased: 1.593
batch start
#iterations: 355
currently lose_sum: 510.16417744755745
time_elpased: 1.615
batch start
#iterations: 356
currently lose_sum: 509.52357906103134
time_elpased: 1.61
batch start
#iterations: 357
currently lose_sum: 510.1572808623314
time_elpased: 1.606
batch start
#iterations: 358
currently lose_sum: 510.5774784088135
time_elpased: 1.621
batch start
#iterations: 359
currently lose_sum: 509.4639181792736
time_elpased: 1.603
start validation test
0.542216494845
0.912225705329
0.0902792140641
0.164298484991
0
validation finish
batch start
#iterations: 360
currently lose_sum: 511.66910821199417
time_elpased: 1.606
batch start
#iterations: 361
currently lose_sum: 508.3223770558834
time_elpased: 1.609
batch start
#iterations: 362
currently lose_sum: 508.88838467001915
time_elpased: 1.602
batch start
#iterations: 363
currently lose_sum: 510.62018197774887
time_elpased: 1.623
batch start
#iterations: 364
currently lose_sum: 510.10191851854324
time_elpased: 1.604
batch start
#iterations: 365
currently lose_sum: 511.2136218249798
time_elpased: 1.619
batch start
#iterations: 366
currently lose_sum: 510.29268208146095
time_elpased: 1.615
batch start
#iterations: 367
currently lose_sum: 511.2954008281231
time_elpased: 1.608
batch start
#iterations: 368
currently lose_sum: 508.0781115889549
time_elpased: 1.605
batch start
#iterations: 369
currently lose_sum: 509.05333918333054
time_elpased: 1.626
batch start
#iterations: 370
currently lose_sum: 509.75598523020744
time_elpased: 1.611
batch start
#iterations: 371
currently lose_sum: 509.9003902077675
time_elpased: 1.611
batch start
#iterations: 372
currently lose_sum: 510.35550874471664
time_elpased: 1.609
batch start
#iterations: 373
currently lose_sum: 510.6359736919403
time_elpased: 1.601
batch start
#iterations: 374
currently lose_sum: 510.1544950604439
time_elpased: 1.602
batch start
#iterations: 375
currently lose_sum: 512.0011649727821
time_elpased: 1.622
batch start
#iterations: 376
currently lose_sum: 509.256984770298
time_elpased: 1.6
batch start
#iterations: 377
currently lose_sum: 508.71624049544334
time_elpased: 1.631
batch start
#iterations: 378
currently lose_sum: 509.48402884602547
time_elpased: 1.625
batch start
#iterations: 379
currently lose_sum: 508.37422066926956
time_elpased: 1.601
start validation test
0.546649484536
0.898814949863
0.10196483971
0.183152224389
0
validation finish
batch start
#iterations: 380
currently lose_sum: 510.9368829727173
time_elpased: 1.62
batch start
#iterations: 381
currently lose_sum: 509.88029766082764
time_elpased: 1.62
batch start
#iterations: 382
currently lose_sum: 511.16231298446655
time_elpased: 1.627
batch start
#iterations: 383
currently lose_sum: 509.6800611317158
time_elpased: 1.603
batch start
#iterations: 384
currently lose_sum: 507.96884378790855
time_elpased: 1.61
batch start
#iterations: 385
currently lose_sum: 508.0525755286217
time_elpased: 1.64
batch start
#iterations: 386
currently lose_sum: 510.10815238952637
time_elpased: 1.612
batch start
#iterations: 387
currently lose_sum: 509.87327921390533
time_elpased: 1.601
batch start
#iterations: 388
currently lose_sum: 509.64727845788
time_elpased: 1.604
batch start
#iterations: 389
currently lose_sum: 509.1146378815174
time_elpased: 1.62
batch start
#iterations: 390
currently lose_sum: 509.7135027348995
time_elpased: 1.62
batch start
#iterations: 391
currently lose_sum: 510.34550097584724
time_elpased: 1.606
batch start
#iterations: 392
currently lose_sum: 509.62980502843857
time_elpased: 1.599
batch start
#iterations: 393
currently lose_sum: 508.27019438147545
time_elpased: 1.607
batch start
#iterations: 394
currently lose_sum: 510.34478893876076
time_elpased: 1.609
batch start
#iterations: 395
currently lose_sum: 508.68892484903336
time_elpased: 1.609
batch start
#iterations: 396
currently lose_sum: 510.4925025999546
time_elpased: 1.617
batch start
#iterations: 397
currently lose_sum: 510.5424827337265
time_elpased: 1.608
batch start
#iterations: 398
currently lose_sum: 510.0602003335953
time_elpased: 1.608
batch start
#iterations: 399
currently lose_sum: 509.0951300263405
time_elpased: 1.604
start validation test
0.534536082474
0.912371134021
0.0732161323681
0.13555427915
0
validation finish
acc: 0.594
pre: 0.876
rec: 0.229
F1: 0.364
auc: 0.000
