start to construct graph
graph construct over
58302
epochs start
batch start
#iterations: 0
currently lose_sum: 398.88399988412857
time_elpased: 1.235
batch start
#iterations: 1
currently lose_sum: 392.24069517850876
time_elpased: 1.131
batch start
#iterations: 2
currently lose_sum: 387.89699470996857
time_elpased: 1.131
batch start
#iterations: 3
currently lose_sum: 385.8444474339485
time_elpased: 1.142
batch start
#iterations: 4
currently lose_sum: 384.50058829784393
time_elpased: 1.139
batch start
#iterations: 5
currently lose_sum: 382.77601343393326
time_elpased: 1.157
batch start
#iterations: 6
currently lose_sum: 382.6286585330963
time_elpased: 1.151
batch start
#iterations: 7
currently lose_sum: 381.318778693676
time_elpased: 1.134
batch start
#iterations: 8
currently lose_sum: 381.27140760421753
time_elpased: 1.16
batch start
#iterations: 9
currently lose_sum: 382.34927302598953
time_elpased: 1.138
batch start
#iterations: 10
currently lose_sum: 380.0016739368439
time_elpased: 1.167
batch start
#iterations: 11
currently lose_sum: 380.4627547264099
time_elpased: 1.132
batch start
#iterations: 12
currently lose_sum: 380.07981330156326
time_elpased: 1.132
batch start
#iterations: 13
currently lose_sum: 380.94926047325134
time_elpased: 1.188
batch start
#iterations: 14
currently lose_sum: 381.5744552016258
time_elpased: 1.136
batch start
#iterations: 15
currently lose_sum: 380.1660273075104
time_elpased: 1.171
batch start
#iterations: 16
currently lose_sum: 379.7153933644295
time_elpased: 1.142
batch start
#iterations: 17
currently lose_sum: 379.05763334035873
time_elpased: 1.158
batch start
#iterations: 18
currently lose_sum: 379.4004637002945
time_elpased: 1.123
batch start
#iterations: 19
currently lose_sum: 380.07681089639664
time_elpased: 1.131
start validation test
0.75587628866
0.83170280275
0.644966167726
0.726527312623
0
validation finish
batch start
#iterations: 20
currently lose_sum: 378.9646341204643
time_elpased: 1.217
batch start
#iterations: 21
currently lose_sum: 380.3680939078331
time_elpased: 1.163
batch start
#iterations: 22
currently lose_sum: 378.41931688785553
time_elpased: 1.173
batch start
#iterations: 23
currently lose_sum: 379.49027252197266
time_elpased: 1.161
batch start
#iterations: 24
currently lose_sum: 378.30389630794525
time_elpased: 1.157
batch start
#iterations: 25
currently lose_sum: 378.88353085517883
time_elpased: 1.129
batch start
#iterations: 26
currently lose_sum: 378.1815157532692
time_elpased: 1.171
batch start
#iterations: 27
currently lose_sum: 378.5723600387573
time_elpased: 1.183
batch start
#iterations: 28
currently lose_sum: 378.487029671669
time_elpased: 1.153
batch start
#iterations: 29
currently lose_sum: 378.73345613479614
time_elpased: 1.184
batch start
#iterations: 30
currently lose_sum: 377.89749425649643
time_elpased: 1.128
batch start
#iterations: 31
currently lose_sum: 379.02317386865616
time_elpased: 1.174
batch start
#iterations: 32
currently lose_sum: 377.90547347068787
time_elpased: 1.153
batch start
#iterations: 33
currently lose_sum: 377.6489008665085
time_elpased: 1.152
batch start
#iterations: 34
currently lose_sum: 376.31781154870987
time_elpased: 1.161
batch start
#iterations: 35
currently lose_sum: 375.79710680246353
time_elpased: 1.145
batch start
#iterations: 36
currently lose_sum: 378.24054604768753
time_elpased: 1.165
batch start
#iterations: 37
currently lose_sum: 377.1838280558586
time_elpased: 1.225
batch start
#iterations: 38
currently lose_sum: 378.1664329767227
time_elpased: 1.155
batch start
#iterations: 39
currently lose_sum: 378.33339434862137
time_elpased: 1.194
start validation test
0.756804123711
0.847982310669
0.629075251179
0.722307239553
0
validation finish
batch start
#iterations: 40
currently lose_sum: 377.2341272830963
time_elpased: 1.218
batch start
#iterations: 41
currently lose_sum: 378.71819645166397
time_elpased: 1.141
batch start
#iterations: 42
currently lose_sum: 377.6510402560234
time_elpased: 1.216
batch start
#iterations: 43
currently lose_sum: 377.5097861289978
time_elpased: 1.127
batch start
#iterations: 44
currently lose_sum: 376.79395347833633
time_elpased: 1.127
batch start
#iterations: 45
currently lose_sum: 378.2776113152504
time_elpased: 1.179
batch start
#iterations: 46
currently lose_sum: 377.5861533880234
time_elpased: 1.191
batch start
#iterations: 47
currently lose_sum: 376.88959807157516
time_elpased: 1.213
batch start
#iterations: 48
currently lose_sum: 377.9537345170975
time_elpased: 1.182
batch start
#iterations: 49
currently lose_sum: 377.70528995990753
time_elpased: 1.219
batch start
#iterations: 50
currently lose_sum: 377.5574087500572
time_elpased: 1.19
batch start
#iterations: 51
currently lose_sum: 377.2540509700775
time_elpased: 1.159
batch start
#iterations: 52
currently lose_sum: 376.8636015057564
time_elpased: 1.221
batch start
#iterations: 53
currently lose_sum: 377.0449483990669
time_elpased: 1.196
batch start
#iterations: 54
currently lose_sum: 378.1487190127373
time_elpased: 1.15
batch start
#iterations: 55
currently lose_sum: 376.60908365249634
time_elpased: 1.182
batch start
#iterations: 56
currently lose_sum: 376.2668290734291
time_elpased: 1.229
batch start
#iterations: 57
currently lose_sum: 376.4250303506851
time_elpased: 1.216
batch start
#iterations: 58
currently lose_sum: 376.81703466176987
time_elpased: 1.203
batch start
#iterations: 59
currently lose_sum: 377.62762093544006
time_elpased: 1.239
start validation test
0.758092783505
0.874057649667
0.60621283576
0.715902899691
0
validation finish
batch start
#iterations: 60
currently lose_sum: 376.7347614169121
time_elpased: 1.244
batch start
#iterations: 61
currently lose_sum: 377.23570811748505
time_elpased: 1.194
batch start
#iterations: 62
currently lose_sum: 377.49964731931686
time_elpased: 1.191
batch start
#iterations: 63
currently lose_sum: 377.88605082035065
time_elpased: 1.204
batch start
#iterations: 64
currently lose_sum: 377.8088920712471
time_elpased: 1.21
batch start
#iterations: 65
currently lose_sum: 376.52418607473373
time_elpased: 1.143
batch start
#iterations: 66
currently lose_sum: 377.2966032028198
time_elpased: 1.145
batch start
#iterations: 67
currently lose_sum: 376.30283296108246
time_elpased: 1.139
batch start
#iterations: 68
currently lose_sum: 375.7895871400833
time_elpased: 1.238
batch start
#iterations: 69
currently lose_sum: 376.63156056404114
time_elpased: 1.235
batch start
#iterations: 70
currently lose_sum: 377.3544389605522
time_elpased: 1.224
batch start
#iterations: 71
currently lose_sum: 377.51928770542145
time_elpased: 1.203
batch start
#iterations: 72
currently lose_sum: 375.5638633966446
time_elpased: 1.226
batch start
#iterations: 73
currently lose_sum: 377.26063323020935
time_elpased: 1.2
batch start
#iterations: 74
currently lose_sum: 376.88353019952774
time_elpased: 1.218
batch start
#iterations: 75
currently lose_sum: 377.03726875782013
time_elpased: 1.216
batch start
#iterations: 76
currently lose_sum: 377.28637593984604
time_elpased: 1.194
batch start
#iterations: 77
currently lose_sum: 376.2693054676056
time_elpased: 1.155
batch start
#iterations: 78
currently lose_sum: 377.23210656642914
time_elpased: 1.187
batch start
#iterations: 79
currently lose_sum: 375.6551507115364
time_elpased: 1.156
start validation test
0.770773195876
0.858532630726
0.651425056387
0.740775284174
0
validation finish
batch start
#iterations: 80
currently lose_sum: 377.2355909347534
time_elpased: 1.23
batch start
#iterations: 81
currently lose_sum: 377.7875779271126
time_elpased: 1.169
batch start
#iterations: 82
currently lose_sum: 375.7604437470436
time_elpased: 1.203
batch start
#iterations: 83
currently lose_sum: 375.3766576051712
time_elpased: 1.152
batch start
#iterations: 84
currently lose_sum: 377.50164037942886
time_elpased: 1.135
batch start
#iterations: 85
currently lose_sum: 376.97901982069016
time_elpased: 1.158
batch start
#iterations: 86
currently lose_sum: 376.18614584207535
time_elpased: 1.147
batch start
#iterations: 87
currently lose_sum: 376.92119961977005
time_elpased: 1.232
batch start
#iterations: 88
currently lose_sum: 376.77395862340927
time_elpased: 1.154
batch start
#iterations: 89
currently lose_sum: 377.1276286840439
time_elpased: 1.188
batch start
#iterations: 90
currently lose_sum: 377.0012152791023
time_elpased: 1.148
batch start
#iterations: 91
currently lose_sum: 374.92846834659576
time_elpased: 1.163
batch start
#iterations: 92
currently lose_sum: 376.9743450284004
time_elpased: 1.167
batch start
#iterations: 93
currently lose_sum: 376.0653856396675
time_elpased: 1.17
batch start
#iterations: 94
currently lose_sum: 377.6225358247757
time_elpased: 1.148
batch start
#iterations: 95
currently lose_sum: 375.715944647789
time_elpased: 1.188
batch start
#iterations: 96
currently lose_sum: 377.95876121520996
time_elpased: 1.196
batch start
#iterations: 97
currently lose_sum: 376.58942395448685
time_elpased: 1.147
batch start
#iterations: 98
currently lose_sum: 376.1394048333168
time_elpased: 1.143
batch start
#iterations: 99
currently lose_sum: 376.56046533584595
time_elpased: 1.195
start validation test
0.77324742268
0.86763696279
0.647836784909
0.741797264777
0
validation finish
batch start
#iterations: 100
currently lose_sum: 376.8128830194473
time_elpased: 1.198
batch start
#iterations: 101
currently lose_sum: 378.1097229719162
time_elpased: 1.127
batch start
#iterations: 102
currently lose_sum: 376.30816727876663
time_elpased: 1.127
batch start
#iterations: 103
currently lose_sum: 376.2293706536293
time_elpased: 1.155
batch start
#iterations: 104
currently lose_sum: 376.20518308877945
time_elpased: 1.161
batch start
#iterations: 105
currently lose_sum: 377.398379445076
time_elpased: 1.164
batch start
#iterations: 106
currently lose_sum: 376.13363498449326
time_elpased: 1.152
batch start
#iterations: 107
currently lose_sum: 376.6922136545181
time_elpased: 1.164
batch start
#iterations: 108
currently lose_sum: 377.4208455681801
time_elpased: 1.181
batch start
#iterations: 109
currently lose_sum: 375.2488111257553
time_elpased: 1.188
batch start
#iterations: 110
currently lose_sum: 376.3072207570076
time_elpased: 1.164
batch start
#iterations: 111
currently lose_sum: 375.89886778593063
time_elpased: 1.184
batch start
#iterations: 112
currently lose_sum: 376.15092158317566
time_elpased: 1.169
batch start
#iterations: 113
currently lose_sum: 376.8863991498947
time_elpased: 1.184
batch start
#iterations: 114
currently lose_sum: 375.55323845148087
time_elpased: 1.224
batch start
#iterations: 115
currently lose_sum: 375.1644341945648
time_elpased: 1.157
batch start
#iterations: 116
currently lose_sum: 376.62546890974045
time_elpased: 1.22
batch start
#iterations: 117
currently lose_sum: 377.2138843536377
time_elpased: 1.151
batch start
#iterations: 118
currently lose_sum: 375.2988664507866
time_elpased: 1.143
batch start
#iterations: 119
currently lose_sum: 377.53922379016876
time_elpased: 1.14
start validation test
0.768969072165
0.87635636779
0.629280295263
0.732545649839
0
validation finish
batch start
#iterations: 120
currently lose_sum: 376.02990794181824
time_elpased: 1.176
batch start
#iterations: 121
currently lose_sum: 375.7141660451889
time_elpased: 1.16
batch start
#iterations: 122
currently lose_sum: 376.0387607216835
time_elpased: 1.157
batch start
#iterations: 123
currently lose_sum: 375.1312880516052
time_elpased: 1.141
batch start
#iterations: 124
currently lose_sum: 375.71392846107483
time_elpased: 1.145
batch start
#iterations: 125
currently lose_sum: 375.86588752269745
time_elpased: 1.134
batch start
#iterations: 126
currently lose_sum: 375.193596303463
time_elpased: 1.166
batch start
#iterations: 127
currently lose_sum: 375.08898746967316
time_elpased: 1.134
batch start
#iterations: 128
currently lose_sum: 376.20258462429047
time_elpased: 1.123
batch start
#iterations: 129
currently lose_sum: 376.05576127767563
time_elpased: 1.138
batch start
#iterations: 130
currently lose_sum: 374.62438237667084
time_elpased: 1.149
batch start
#iterations: 131
currently lose_sum: 376.78446048498154
time_elpased: 1.18
batch start
#iterations: 132
currently lose_sum: 375.71973890066147
time_elpased: 1.173
batch start
#iterations: 133
currently lose_sum: 375.15231800079346
time_elpased: 1.227
batch start
#iterations: 134
currently lose_sum: 376.1188594698906
time_elpased: 1.185
batch start
#iterations: 135
currently lose_sum: 376.0072386264801
time_elpased: 1.134
batch start
#iterations: 136
currently lose_sum: 375.8604636788368
time_elpased: 1.124
batch start
#iterations: 137
currently lose_sum: 377.6116542816162
time_elpased: 1.146
batch start
#iterations: 138
currently lose_sum: 376.65747171640396
time_elpased: 1.144
batch start
#iterations: 139
currently lose_sum: 376.7382324934006
time_elpased: 1.131
start validation test
0.773453608247
0.870557322639
0.645376255895
0.741242272593
0
validation finish
batch start
#iterations: 140
currently lose_sum: 376.24091041088104
time_elpased: 1.143
batch start
#iterations: 141
currently lose_sum: 376.66309559345245
time_elpased: 1.15
batch start
#iterations: 142
currently lose_sum: 375.94456058740616
time_elpased: 1.133
batch start
#iterations: 143
currently lose_sum: 375.6802222132683
time_elpased: 1.145
batch start
#iterations: 144
currently lose_sum: 376.84023731946945
time_elpased: 1.137
batch start
#iterations: 145
currently lose_sum: 375.7617821097374
time_elpased: 1.145
batch start
#iterations: 146
currently lose_sum: 376.07949459552765
time_elpased: 1.131
batch start
#iterations: 147
currently lose_sum: 376.04623287916183
time_elpased: 1.142
batch start
#iterations: 148
currently lose_sum: 377.2623742222786
time_elpased: 1.143
batch start
#iterations: 149
currently lose_sum: 375.4615333676338
time_elpased: 1.108
batch start
#iterations: 150
currently lose_sum: 376.21925419569016
time_elpased: 1.152
batch start
#iterations: 151
currently lose_sum: 375.72048902511597
time_elpased: 1.151
batch start
#iterations: 152
currently lose_sum: 376.10209971666336
time_elpased: 1.12
batch start
#iterations: 153
currently lose_sum: 376.15741926431656
time_elpased: 1.133
batch start
#iterations: 154
currently lose_sum: 375.0178831219673
time_elpased: 1.123
batch start
#iterations: 155
currently lose_sum: 376.41575759649277
time_elpased: 1.135
batch start
#iterations: 156
currently lose_sum: 376.4184541106224
time_elpased: 1.106
batch start
#iterations: 157
currently lose_sum: 375.51171922683716
time_elpased: 1.146
batch start
#iterations: 158
currently lose_sum: 374.4947738647461
time_elpased: 1.126
batch start
#iterations: 159
currently lose_sum: 375.36523455381393
time_elpased: 1.138
start validation test
0.785154639175
0.85363383135
0.691203608776
0.763879447088
0
validation finish
batch start
#iterations: 160
currently lose_sum: 376.926735162735
time_elpased: 1.191
batch start
#iterations: 161
currently lose_sum: 376.61413049697876
time_elpased: 1.144
batch start
#iterations: 162
currently lose_sum: 375.98629969358444
time_elpased: 1.127
batch start
#iterations: 163
currently lose_sum: 377.7461527585983
time_elpased: 1.128
batch start
#iterations: 164
currently lose_sum: 375.9121124744415
time_elpased: 1.144
batch start
#iterations: 165
currently lose_sum: 376.4304491877556
time_elpased: 1.137
batch start
#iterations: 166
currently lose_sum: 375.49864798784256
time_elpased: 1.117
batch start
#iterations: 167
currently lose_sum: 375.51355385780334
time_elpased: 1.167
batch start
#iterations: 168
currently lose_sum: 375.48953318595886
time_elpased: 1.129
batch start
#iterations: 169
currently lose_sum: 375.66815346479416
time_elpased: 1.118
batch start
#iterations: 170
currently lose_sum: 375.45345175266266
time_elpased: 1.118
batch start
#iterations: 171
currently lose_sum: 376.4204869866371
time_elpased: 1.129
batch start
#iterations: 172
currently lose_sum: 375.6413514018059
time_elpased: 1.133
batch start
#iterations: 173
currently lose_sum: 375.9128005504608
time_elpased: 1.134
batch start
#iterations: 174
currently lose_sum: 376.1351675391197
time_elpased: 1.152
batch start
#iterations: 175
currently lose_sum: 375.41995602846146
time_elpased: 1.116
batch start
#iterations: 176
currently lose_sum: 376.1472492814064
time_elpased: 1.111
batch start
#iterations: 177
currently lose_sum: 376.2879807949066
time_elpased: 1.116
batch start
#iterations: 178
currently lose_sum: 376.43025344610214
time_elpased: 1.139
batch start
#iterations: 179
currently lose_sum: 375.97255504131317
time_elpased: 1.131
start validation test
0.775
0.878919983125
0.640762763994
0.741179958494
0
validation finish
batch start
#iterations: 180
currently lose_sum: 374.91968804597855
time_elpased: 1.154
batch start
#iterations: 181
currently lose_sum: 374.9603459239006
time_elpased: 1.115
batch start
#iterations: 182
currently lose_sum: 375.3354508280754
time_elpased: 1.142
batch start
#iterations: 183
currently lose_sum: 376.13383769989014
time_elpased: 1.146
batch start
#iterations: 184
currently lose_sum: 375.1389594078064
time_elpased: 1.139
batch start
#iterations: 185
currently lose_sum: 374.9447825551033
time_elpased: 1.133
batch start
#iterations: 186
currently lose_sum: 375.58164048194885
time_elpased: 1.118
batch start
#iterations: 187
currently lose_sum: 375.9900349378586
time_elpased: 1.14
batch start
#iterations: 188
currently lose_sum: 374.1636914014816
time_elpased: 1.125
batch start
#iterations: 189
currently lose_sum: 374.97794234752655
time_elpased: 1.123
batch start
#iterations: 190
currently lose_sum: 374.9554545879364
time_elpased: 1.136
batch start
#iterations: 191
currently lose_sum: 374.8951028585434
time_elpased: 1.121
batch start
#iterations: 192
currently lose_sum: 376.2340191602707
time_elpased: 1.121
batch start
#iterations: 193
currently lose_sum: 374.77655148506165
time_elpased: 1.114
batch start
#iterations: 194
currently lose_sum: 375.19902473688126
time_elpased: 1.107
batch start
#iterations: 195
currently lose_sum: 374.8414772748947
time_elpased: 1.124
batch start
#iterations: 196
currently lose_sum: 376.6056841015816
time_elpased: 1.118
batch start
#iterations: 197
currently lose_sum: 375.6879692077637
time_elpased: 1.122
batch start
#iterations: 198
currently lose_sum: 375.7823169827461
time_elpased: 1.128
batch start
#iterations: 199
currently lose_sum: 376.2710429430008
time_elpased: 1.133
start validation test
0.780670103093
0.864413518887
0.668648759483
0.754032024973
0
validation finish
batch start
#iterations: 200
currently lose_sum: 376.58552849292755
time_elpased: 1.13
batch start
#iterations: 201
currently lose_sum: 374.5836026072502
time_elpased: 1.126
batch start
#iterations: 202
currently lose_sum: 375.8041918873787
time_elpased: 1.109
batch start
#iterations: 203
currently lose_sum: 375.5996741056442
time_elpased: 1.134
batch start
#iterations: 204
currently lose_sum: 376.0304507613182
time_elpased: 1.117
batch start
#iterations: 205
currently lose_sum: 375.11876076459885
time_elpased: 1.13
batch start
#iterations: 206
currently lose_sum: 375.68386059999466
time_elpased: 1.121
batch start
#iterations: 207
currently lose_sum: 374.8723803162575
time_elpased: 1.134
batch start
#iterations: 208
currently lose_sum: 376.7701994776726
time_elpased: 1.136
batch start
#iterations: 209
currently lose_sum: 374.28518372774124
time_elpased: 1.153
batch start
#iterations: 210
currently lose_sum: 375.6366184949875
time_elpased: 1.139
batch start
#iterations: 211
currently lose_sum: 375.1852272748947
time_elpased: 1.151
batch start
#iterations: 212
currently lose_sum: 374.61524522304535
time_elpased: 1.128
batch start
#iterations: 213
currently lose_sum: 374.9713250398636
time_elpased: 1.135
batch start
#iterations: 214
currently lose_sum: 375.0289491415024
time_elpased: 1.143
batch start
#iterations: 215
currently lose_sum: 375.6867100596428
time_elpased: 1.12
batch start
#iterations: 216
currently lose_sum: 375.5173289179802
time_elpased: 1.152
batch start
#iterations: 217
currently lose_sum: 376.78117376565933
time_elpased: 1.109
batch start
#iterations: 218
currently lose_sum: 375.38699036836624
time_elpased: 1.121
batch start
#iterations: 219
currently lose_sum: 375.28085392713547
time_elpased: 1.106
start validation test
0.775463917526
0.873822714681
0.646811564486
0.743372216331
0
validation finish
batch start
#iterations: 220
currently lose_sum: 375.8143998980522
time_elpased: 1.165
batch start
#iterations: 221
currently lose_sum: 376.0946455001831
time_elpased: 1.119
batch start
#iterations: 222
currently lose_sum: 375.1399028301239
time_elpased: 1.111
batch start
#iterations: 223
currently lose_sum: 374.0992572903633
time_elpased: 1.166
batch start
#iterations: 224
currently lose_sum: 375.9829806089401
time_elpased: 1.122
batch start
#iterations: 225
currently lose_sum: 377.00288516283035
time_elpased: 1.111
batch start
#iterations: 226
currently lose_sum: 375.09024465084076
time_elpased: 1.129
batch start
#iterations: 227
currently lose_sum: 375.4737812280655
time_elpased: 1.132
batch start
#iterations: 228
currently lose_sum: 376.22321605682373
time_elpased: 1.136
batch start
#iterations: 229
currently lose_sum: 375.8074219226837
time_elpased: 1.114
batch start
#iterations: 230
currently lose_sum: 375.8292672634125
time_elpased: 1.146
batch start
#iterations: 231
currently lose_sum: 375.02341091632843
time_elpased: 1.132
batch start
#iterations: 232
currently lose_sum: 376.1018108725548
time_elpased: 1.132
batch start
#iterations: 233
currently lose_sum: 375.5306732058525
time_elpased: 1.138
batch start
#iterations: 234
currently lose_sum: 375.3986648321152
time_elpased: 1.136
batch start
#iterations: 235
currently lose_sum: 375.3091769814491
time_elpased: 1.131
batch start
#iterations: 236
currently lose_sum: 375.558306992054
time_elpased: 1.128
batch start
#iterations: 237
currently lose_sum: 375.4213880300522
time_elpased: 1.137
batch start
#iterations: 238
currently lose_sum: 376.30995297431946
time_elpased: 1.121
batch start
#iterations: 239
currently lose_sum: 377.82544553279877
time_elpased: 1.122
start validation test
0.773762886598
0.884698121325
0.632458478573
0.737609852335
0
validation finish
batch start
#iterations: 240
currently lose_sum: 375.9231218099594
time_elpased: 1.166
batch start
#iterations: 241
currently lose_sum: 374.88832652568817
time_elpased: 1.175
batch start
#iterations: 242
currently lose_sum: 374.54067981243134
time_elpased: 1.123
batch start
#iterations: 243
currently lose_sum: 376.3839069008827
time_elpased: 1.159
batch start
#iterations: 244
currently lose_sum: 375.30823028087616
time_elpased: 1.161
batch start
#iterations: 245
currently lose_sum: 375.2630668282509
time_elpased: 1.122
batch start
#iterations: 246
currently lose_sum: 375.73844915628433
time_elpased: 1.14
batch start
#iterations: 247
currently lose_sum: 376.10838508605957
time_elpased: 1.144
batch start
#iterations: 248
currently lose_sum: 375.59369987249374
time_elpased: 1.126
batch start
#iterations: 249
currently lose_sum: 374.889368057251
time_elpased: 1.139
batch start
#iterations: 250
currently lose_sum: 375.12889844179153
time_elpased: 1.122
batch start
#iterations: 251
currently lose_sum: 375.9294500350952
time_elpased: 1.128
batch start
#iterations: 252
currently lose_sum: 377.69556057453156
time_elpased: 1.127
batch start
#iterations: 253
currently lose_sum: 375.3134686946869
time_elpased: 1.131
batch start
#iterations: 254
currently lose_sum: 375.3573012948036
time_elpased: 1.131
batch start
#iterations: 255
currently lose_sum: 375.51200848817825
time_elpased: 1.135
batch start
#iterations: 256
currently lose_sum: 376.7512060403824
time_elpased: 1.116
batch start
#iterations: 257
currently lose_sum: 376.49980199337006
time_elpased: 1.144
batch start
#iterations: 258
currently lose_sum: 375.3260950446129
time_elpased: 1.127
batch start
#iterations: 259
currently lose_sum: 374.874109685421
time_elpased: 1.105
start validation test
0.775927835052
0.886048836213
0.636149272094
0.740586023751
0
validation finish
batch start
#iterations: 260
currently lose_sum: 375.49032694101334
time_elpased: 1.16
batch start
#iterations: 261
currently lose_sum: 375.6248535513878
time_elpased: 1.133
batch start
#iterations: 262
currently lose_sum: 375.6076481938362
time_elpased: 1.14
batch start
#iterations: 263
currently lose_sum: 376.195739030838
time_elpased: 1.152
batch start
#iterations: 264
currently lose_sum: 375.01449686288834
time_elpased: 1.132
batch start
#iterations: 265
currently lose_sum: 376.0214996933937
time_elpased: 1.133
batch start
#iterations: 266
currently lose_sum: 374.9900337457657
time_elpased: 1.129
batch start
#iterations: 267
currently lose_sum: 376.22173887491226
time_elpased: 1.171
batch start
#iterations: 268
currently lose_sum: 375.05653208494186
time_elpased: 1.133
batch start
#iterations: 269
currently lose_sum: 375.3246909379959
time_elpased: 1.131
batch start
#iterations: 270
currently lose_sum: 375.02777844667435
time_elpased: 1.126
batch start
#iterations: 271
currently lose_sum: 374.22513568401337
time_elpased: 1.125
batch start
#iterations: 272
currently lose_sum: 375.2757447361946
time_elpased: 1.132
batch start
#iterations: 273
currently lose_sum: 375.55373960733414
time_elpased: 1.132
batch start
#iterations: 274
currently lose_sum: 375.78864043951035
time_elpased: 1.123
batch start
#iterations: 275
currently lose_sum: 375.34371238946915
time_elpased: 1.128
batch start
#iterations: 276
currently lose_sum: 377.1128029227257
time_elpased: 1.125
batch start
#iterations: 277
currently lose_sum: 375.3724883198738
time_elpased: 1.128
batch start
#iterations: 278
currently lose_sum: 376.6861496567726
time_elpased: 1.13
batch start
#iterations: 279
currently lose_sum: 375.6604570746422
time_elpased: 1.146
start validation test
0.779329896907
0.874196636127
0.655423416034
0.74916505537
0
validation finish
batch start
#iterations: 280
currently lose_sum: 374.73790657520294
time_elpased: 1.167
batch start
#iterations: 281
currently lose_sum: 375.22593462467194
time_elpased: 1.117
batch start
#iterations: 282
currently lose_sum: 376.22095000743866
time_elpased: 1.144
batch start
#iterations: 283
currently lose_sum: 377.0428764820099
time_elpased: 1.13
batch start
#iterations: 284
currently lose_sum: 375.5991275906563
time_elpased: 1.123
batch start
#iterations: 285
currently lose_sum: 374.78033286333084
time_elpased: 1.12
batch start
#iterations: 286
currently lose_sum: 374.9839619398117
time_elpased: 1.147
batch start
#iterations: 287
currently lose_sum: 374.7954743504524
time_elpased: 1.133
batch start
#iterations: 288
currently lose_sum: 376.7799665927887
time_elpased: 1.126
batch start
#iterations: 289
currently lose_sum: 374.0602557659149
time_elpased: 1.114
batch start
#iterations: 290
currently lose_sum: 375.7293645143509
time_elpased: 1.14
batch start
#iterations: 291
currently lose_sum: 374.4835160970688
time_elpased: 1.136
batch start
#iterations: 292
currently lose_sum: 375.29313760995865
time_elpased: 1.16
batch start
#iterations: 293
currently lose_sum: 375.42793560028076
time_elpased: 1.157
batch start
#iterations: 294
currently lose_sum: 375.4417508840561
time_elpased: 1.138
batch start
#iterations: 295
currently lose_sum: 375.4173414707184
time_elpased: 1.141
batch start
#iterations: 296
currently lose_sum: 375.4343039393425
time_elpased: 1.119
batch start
#iterations: 297
currently lose_sum: 375.310961663723
time_elpased: 1.126
batch start
#iterations: 298
currently lose_sum: 376.0457490682602
time_elpased: 1.133
batch start
#iterations: 299
currently lose_sum: 375.9822607636452
time_elpased: 1.119
start validation test
0.779948453608
0.875325030792
0.655730982161
0.749780200457
0
validation finish
batch start
#iterations: 300
currently lose_sum: 375.9322490096092
time_elpased: 1.153
batch start
#iterations: 301
currently lose_sum: 374.6850928068161
time_elpased: 1.145
batch start
#iterations: 302
currently lose_sum: 374.6783972978592
time_elpased: 1.126
batch start
#iterations: 303
currently lose_sum: 374.27173268795013
time_elpased: 1.165
batch start
#iterations: 304
currently lose_sum: 375.1931164264679
time_elpased: 1.179
batch start
#iterations: 305
currently lose_sum: 375.3564952611923
time_elpased: 1.116
batch start
#iterations: 306
currently lose_sum: 374.92035031318665
time_elpased: 1.162
batch start
#iterations: 307
currently lose_sum: 376.27056580781937
time_elpased: 1.139
batch start
#iterations: 308
currently lose_sum: 376.9455670118332
time_elpased: 1.171
batch start
#iterations: 309
currently lose_sum: 375.2225811481476
time_elpased: 1.138
batch start
#iterations: 310
currently lose_sum: 376.0598322749138
time_elpased: 1.158
batch start
#iterations: 311
currently lose_sum: 374.82456666231155
time_elpased: 1.147
batch start
#iterations: 312
currently lose_sum: 375.53048342466354
time_elpased: 1.162
batch start
#iterations: 313
currently lose_sum: 376.1239929795265
time_elpased: 1.17
batch start
#iterations: 314
currently lose_sum: 374.4151844382286
time_elpased: 1.153
batch start
#iterations: 315
currently lose_sum: 374.56296598911285
time_elpased: 1.146
batch start
#iterations: 316
currently lose_sum: 376.7497983574867
time_elpased: 1.142
batch start
#iterations: 317
currently lose_sum: 374.56883734464645
time_elpased: 1.153
batch start
#iterations: 318
currently lose_sum: 373.3984828591347
time_elpased: 1.138
batch start
#iterations: 319
currently lose_sum: 375.4838134050369
time_elpased: 1.251
start validation test
0.776391752577
0.879059350504
0.643838425261
0.743283228784
0
validation finish
batch start
#iterations: 320
currently lose_sum: 374.79666262865067
time_elpased: 1.267
batch start
#iterations: 321
currently lose_sum: 375.9769875407219
time_elpased: 1.231
batch start
#iterations: 322
currently lose_sum: 375.83710592985153
time_elpased: 1.147
batch start
#iterations: 323
currently lose_sum: 373.73516458272934
time_elpased: 1.199
batch start
#iterations: 324
currently lose_sum: 374.68437272310257
time_elpased: 1.235
batch start
#iterations: 325
currently lose_sum: 375.1087472438812
time_elpased: 1.227
batch start
#iterations: 326
currently lose_sum: 376.0895835161209
time_elpased: 1.201
batch start
#iterations: 327
currently lose_sum: 374.92320346832275
time_elpased: 1.228
batch start
#iterations: 328
currently lose_sum: 375.57043182849884
time_elpased: 1.227
batch start
#iterations: 329
currently lose_sum: 376.39063638448715
time_elpased: 1.212
batch start
#iterations: 330
currently lose_sum: 374.77076983451843
time_elpased: 1.26
batch start
#iterations: 331
currently lose_sum: 375.1863643527031
time_elpased: 1.211
batch start
#iterations: 332
currently lose_sum: 375.4699857831001
time_elpased: 1.232
batch start
#iterations: 333
currently lose_sum: 375.1408476829529
time_elpased: 1.259
batch start
#iterations: 334
currently lose_sum: 374.574272274971
time_elpased: 1.215
batch start
#iterations: 335
currently lose_sum: 374.4882931113243
time_elpased: 1.236
batch start
#iterations: 336
currently lose_sum: 374.55500346422195
time_elpased: 1.224
batch start
#iterations: 337
currently lose_sum: 375.4065355658531
time_elpased: 1.198
batch start
#iterations: 338
currently lose_sum: 376.01853972673416
time_elpased: 1.239
batch start
#iterations: 339
currently lose_sum: 376.69131499528885
time_elpased: 1.233
start validation test
0.781546391753
0.874932028276
0.659831863851
0.752308591467
0
validation finish
batch start
#iterations: 340
currently lose_sum: 375.81071150302887
time_elpased: 1.224
batch start
#iterations: 341
currently lose_sum: 374.03572177886963
time_elpased: 1.237
batch start
#iterations: 342
currently lose_sum: 373.97253954410553
time_elpased: 1.214
batch start
#iterations: 343
currently lose_sum: 375.74803441762924
time_elpased: 1.254
batch start
#iterations: 344
currently lose_sum: 376.3783736228943
time_elpased: 1.249
batch start
#iterations: 345
currently lose_sum: 374.48899298906326
time_elpased: 1.189
batch start
#iterations: 346
currently lose_sum: 374.6320922970772
time_elpased: 1.233
batch start
#iterations: 347
currently lose_sum: 375.1720862388611
time_elpased: 1.232
batch start
#iterations: 348
currently lose_sum: 375.5748396515846
time_elpased: 1.166
batch start
#iterations: 349
currently lose_sum: 375.7773082256317
time_elpased: 1.24
batch start
#iterations: 350
currently lose_sum: 374.80970615148544
time_elpased: 1.218
batch start
#iterations: 351
currently lose_sum: 375.2805881500244
time_elpased: 1.247
batch start
#iterations: 352
currently lose_sum: 375.14352411031723
time_elpased: 1.189
batch start
#iterations: 353
currently lose_sum: 374.67886263132095
time_elpased: 1.172
batch start
#iterations: 354
currently lose_sum: 373.559334397316
time_elpased: 1.242
batch start
#iterations: 355
currently lose_sum: 375.2275108098984
time_elpased: 1.177
batch start
#iterations: 356
currently lose_sum: 376.4471273422241
time_elpased: 1.186
batch start
#iterations: 357
currently lose_sum: 375.1430718898773
time_elpased: 1.203
batch start
#iterations: 358
currently lose_sum: 375.33688974380493
time_elpased: 1.156
batch start
#iterations: 359
currently lose_sum: 375.25283670425415
time_elpased: 1.163
start validation test
0.777422680412
0.883465011287
0.641993028501
0.743617147607
0
validation finish
batch start
#iterations: 360
currently lose_sum: 373.90221107006073
time_elpased: 1.181
batch start
#iterations: 361
currently lose_sum: 376.07357800006866
time_elpased: 1.16
batch start
#iterations: 362
currently lose_sum: 375.8204591870308
time_elpased: 1.147
batch start
#iterations: 363
currently lose_sum: 375.0756685137749
time_elpased: 1.133
batch start
#iterations: 364
currently lose_sum: 374.79900974035263
time_elpased: 1.152
batch start
#iterations: 365
currently lose_sum: 374.4266604781151
time_elpased: 1.142
batch start
#iterations: 366
currently lose_sum: 374.9138916730881
time_elpased: 1.141
batch start
#iterations: 367
currently lose_sum: 375.4018156528473
time_elpased: 1.158
batch start
#iterations: 368
currently lose_sum: 376.0001211166382
time_elpased: 1.155
batch start
#iterations: 369
currently lose_sum: 375.5702471137047
time_elpased: 1.156
batch start
#iterations: 370
currently lose_sum: 376.4290552139282
time_elpased: 1.132
batch start
#iterations: 371
currently lose_sum: 375.7811669111252
time_elpased: 1.15
batch start
#iterations: 372
currently lose_sum: 374.85646736621857
time_elpased: 1.128
batch start
#iterations: 373
currently lose_sum: 376.32044488191605
time_elpased: 1.159
batch start
#iterations: 374
currently lose_sum: 375.692100584507
time_elpased: 1.163
batch start
#iterations: 375
currently lose_sum: 375.0151491165161
time_elpased: 1.132
batch start
#iterations: 376
currently lose_sum: 375.46344870328903
time_elpased: 1.155
batch start
#iterations: 377
currently lose_sum: 375.8990398645401
time_elpased: 1.13
batch start
#iterations: 378
currently lose_sum: 374.9464752674103
time_elpased: 1.132
batch start
#iterations: 379
currently lose_sum: 375.6440951228142
time_elpased: 1.126
start validation test
0.776907216495
0.884931895573
0.639429977445
0.742411617665
0
validation finish
batch start
#iterations: 380
currently lose_sum: 375.47130435705185
time_elpased: 1.15
batch start
#iterations: 381
currently lose_sum: 374.9067814350128
time_elpased: 1.155
batch start
#iterations: 382
currently lose_sum: 375.6053249835968
time_elpased: 1.117
batch start
#iterations: 383
currently lose_sum: 374.93588811159134
time_elpased: 1.143
batch start
#iterations: 384
currently lose_sum: 375.26192462444305
time_elpased: 1.151
batch start
#iterations: 385
currently lose_sum: 373.97908532619476
time_elpased: 1.176
batch start
#iterations: 386
currently lose_sum: 375.3086531162262
time_elpased: 1.147
batch start
#iterations: 387
currently lose_sum: 376.44623947143555
time_elpased: 1.155
batch start
#iterations: 388
currently lose_sum: 375.3035830259323
time_elpased: 1.138
batch start
#iterations: 389
currently lose_sum: 375.6127546429634
time_elpased: 1.128
batch start
#iterations: 390
currently lose_sum: 376.47706162929535
time_elpased: 1.167
batch start
#iterations: 391
currently lose_sum: 375.5422952771187
time_elpased: 1.168
batch start
#iterations: 392
currently lose_sum: 375.175444483757
time_elpased: 1.146
batch start
#iterations: 393
currently lose_sum: 373.8966675400734
time_elpased: 1.166
batch start
#iterations: 394
currently lose_sum: 374.40721666812897
time_elpased: 1.13
batch start
#iterations: 395
currently lose_sum: 375.24767607450485
time_elpased: 1.145
batch start
#iterations: 396
currently lose_sum: 374.64167135953903
time_elpased: 1.135
batch start
#iterations: 397
currently lose_sum: 375.5169994235039
time_elpased: 1.133
batch start
#iterations: 398
currently lose_sum: 375.59082466363907
time_elpased: 1.134
batch start
#iterations: 399
currently lose_sum: 375.3873533010483
time_elpased: 1.15
start validation test
0.782783505155
0.876665760131
0.660959606315
0.753682487725
0
validation finish
acc: 0.777
pre: 0.851
rec: 0.676
F1: 0.753
auc: 0.000
