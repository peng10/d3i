start to construct graph
graph construct over
58302
epochs start
batch start
#iterations: 0
currently lose_sum: 398.4930575489998
time_elpased: 1.138
batch start
#iterations: 1
currently lose_sum: 390.14232325553894
time_elpased: 1.09
batch start
#iterations: 2
currently lose_sum: 383.89277970790863
time_elpased: 1.095
batch start
#iterations: 3
currently lose_sum: 381.06165742874146
time_elpased: 1.091
batch start
#iterations: 4
currently lose_sum: 378.3698396086693
time_elpased: 1.099
batch start
#iterations: 5
currently lose_sum: 375.6354985833168
time_elpased: 1.106
batch start
#iterations: 6
currently lose_sum: 374.86208122968674
time_elpased: 1.095
batch start
#iterations: 7
currently lose_sum: 373.1602865457535
time_elpased: 1.086
batch start
#iterations: 8
currently lose_sum: 373.1431366801262
time_elpased: 1.097
batch start
#iterations: 9
currently lose_sum: 373.2982249855995
time_elpased: 1.126
batch start
#iterations: 10
currently lose_sum: 371.6555944085121
time_elpased: 1.077
batch start
#iterations: 11
currently lose_sum: 371.56568068265915
time_elpased: 1.098
batch start
#iterations: 12
currently lose_sum: 370.61991852521896
time_elpased: 1.084
batch start
#iterations: 13
currently lose_sum: 371.1708735227585
time_elpased: 1.098
batch start
#iterations: 14
currently lose_sum: 371.13690960407257
time_elpased: 1.096
batch start
#iterations: 15
currently lose_sum: 369.288212120533
time_elpased: 1.089
batch start
#iterations: 16
currently lose_sum: 369.0586650967598
time_elpased: 1.095
batch start
#iterations: 17
currently lose_sum: 368.18657809495926
time_elpased: 1.088
batch start
#iterations: 18
currently lose_sum: 368.0138586759567
time_elpased: 1.094
batch start
#iterations: 19
currently lose_sum: 369.5820224881172
time_elpased: 1.084
start validation test
0.772628865979
0.820900900901
0.700635636662
0.756015266331
0
validation finish
batch start
#iterations: 20
currently lose_sum: 367.9001812338829
time_elpased: 1.127
batch start
#iterations: 21
currently lose_sum: 369.6138727068901
time_elpased: 1.101
batch start
#iterations: 22
currently lose_sum: 367.17490714788437
time_elpased: 1.094
batch start
#iterations: 23
currently lose_sum: 368.16507655382156
time_elpased: 1.097
batch start
#iterations: 24
currently lose_sum: 367.0185790657997
time_elpased: 1.088
batch start
#iterations: 25
currently lose_sum: 367.44392216205597
time_elpased: 1.102
batch start
#iterations: 26
currently lose_sum: 366.99395048618317
time_elpased: 1.085
batch start
#iterations: 27
currently lose_sum: 366.6308119893074
time_elpased: 1.077
batch start
#iterations: 28
currently lose_sum: 367.01113045215607
time_elpased: 1.082
batch start
#iterations: 29
currently lose_sum: 367.0669854879379
time_elpased: 1.085
batch start
#iterations: 30
currently lose_sum: 366.5098360776901
time_elpased: 1.106
batch start
#iterations: 31
currently lose_sum: 366.70819199085236
time_elpased: 1.098
batch start
#iterations: 32
currently lose_sum: 366.81479847431183
time_elpased: 1.079
batch start
#iterations: 33
currently lose_sum: 365.2011786699295
time_elpased: 1.098
batch start
#iterations: 34
currently lose_sum: 365.06996619701385
time_elpased: 1.095
batch start
#iterations: 35
currently lose_sum: 363.52354687452316
time_elpased: 1.091
batch start
#iterations: 36
currently lose_sum: 365.26272374391556
time_elpased: 1.084
batch start
#iterations: 37
currently lose_sum: 365.27572667598724
time_elpased: 1.089
batch start
#iterations: 38
currently lose_sum: 366.47201853990555
time_elpased: 1.087
batch start
#iterations: 39
currently lose_sum: 365.96947181224823
time_elpased: 1.111
start validation test
0.780257731959
0.810050818746
0.735390608981
0.770917298082
0
validation finish
batch start
#iterations: 40
currently lose_sum: 365.12401008605957
time_elpased: 1.1
batch start
#iterations: 41
currently lose_sum: 365.5015820860863
time_elpased: 1.085
batch start
#iterations: 42
currently lose_sum: 365.8788713812828
time_elpased: 1.099
batch start
#iterations: 43
currently lose_sum: 364.96141934394836
time_elpased: 1.087
batch start
#iterations: 44
currently lose_sum: 364.3478428721428
time_elpased: 1.091
batch start
#iterations: 45
currently lose_sum: 365.1641040444374
time_elpased: 1.09
batch start
#iterations: 46
currently lose_sum: 365.2520304322243
time_elpased: 1.095
batch start
#iterations: 47
currently lose_sum: 365.4520727992058
time_elpased: 1.09
batch start
#iterations: 48
currently lose_sum: 365.95396929979324
time_elpased: 1.087
batch start
#iterations: 49
currently lose_sum: 365.7861803174019
time_elpased: 1.1
batch start
#iterations: 50
currently lose_sum: 365.02226638793945
time_elpased: 1.104
batch start
#iterations: 51
currently lose_sum: 364.52223855257034
time_elpased: 1.1
batch start
#iterations: 52
currently lose_sum: 364.9640504717827
time_elpased: 1.092
batch start
#iterations: 53
currently lose_sum: 364.9693401455879
time_elpased: 1.099
batch start
#iterations: 54
currently lose_sum: 366.22893583774567
time_elpased: 1.079
batch start
#iterations: 55
currently lose_sum: 364.3845458626747
time_elpased: 1.086
batch start
#iterations: 56
currently lose_sum: 363.7865388393402
time_elpased: 1.092
batch start
#iterations: 57
currently lose_sum: 363.0352831482887
time_elpased: 1.095
batch start
#iterations: 58
currently lose_sum: 364.27182853221893
time_elpased: 1.085
batch start
#iterations: 59
currently lose_sum: 363.9700424671173
time_elpased: 1.088
start validation test
0.785257731959
0.82048635008
0.733340168136
0.774469467302
0
validation finish
batch start
#iterations: 60
currently lose_sum: 364.4292359948158
time_elpased: 1.098
batch start
#iterations: 61
currently lose_sum: 365.27276372909546
time_elpased: 1.084
batch start
#iterations: 62
currently lose_sum: 365.2124391198158
time_elpased: 1.085
batch start
#iterations: 63
currently lose_sum: 364.1958972811699
time_elpased: 1.086
batch start
#iterations: 64
currently lose_sum: 365.10041043162346
time_elpased: 1.085
batch start
#iterations: 65
currently lose_sum: 364.0917823314667
time_elpased: 1.091
batch start
#iterations: 66
currently lose_sum: 364.2645218372345
time_elpased: 1.094
batch start
#iterations: 67
currently lose_sum: 362.9020901322365
time_elpased: 1.105
batch start
#iterations: 68
currently lose_sum: 363.24115121364594
time_elpased: 1.09
batch start
#iterations: 69
currently lose_sum: 363.5686181783676
time_elpased: 1.1
batch start
#iterations: 70
currently lose_sum: 364.60447055101395
time_elpased: 1.09
batch start
#iterations: 71
currently lose_sum: 364.5039516687393
time_elpased: 1.091
batch start
#iterations: 72
currently lose_sum: 362.3708873093128
time_elpased: 1.086
batch start
#iterations: 73
currently lose_sum: 364.26408064365387
time_elpased: 1.095
batch start
#iterations: 74
currently lose_sum: 364.59827268123627
time_elpased: 1.092
batch start
#iterations: 75
currently lose_sum: 363.20467096567154
time_elpased: 1.091
batch start
#iterations: 76
currently lose_sum: 364.5526017546654
time_elpased: 1.086
batch start
#iterations: 77
currently lose_sum: 362.81030946969986
time_elpased: 1.1
batch start
#iterations: 78
currently lose_sum: 363.8376051187515
time_elpased: 1.091
batch start
#iterations: 79
currently lose_sum: 362.5355495214462
time_elpased: 1.085
start validation test
0.789432989691
0.816456402813
0.749743694894
0.781679226124
0
validation finish
batch start
#iterations: 80
currently lose_sum: 363.72523432970047
time_elpased: 1.103
batch start
#iterations: 81
currently lose_sum: 364.33178746700287
time_elpased: 1.088
batch start
#iterations: 82
currently lose_sum: 362.7247961759567
time_elpased: 1.088
batch start
#iterations: 83
currently lose_sum: 362.0918928384781
time_elpased: 1.091
batch start
#iterations: 84
currently lose_sum: 363.79844534397125
time_elpased: 1.092
batch start
#iterations: 85
currently lose_sum: 363.1527879834175
time_elpased: 1.085
batch start
#iterations: 86
currently lose_sum: 362.7408815026283
time_elpased: 1.096
batch start
#iterations: 87
currently lose_sum: 363.64143216609955
time_elpased: 1.094
batch start
#iterations: 88
currently lose_sum: 362.5049866437912
time_elpased: 1.089
batch start
#iterations: 89
currently lose_sum: 362.64911514520645
time_elpased: 1.096
batch start
#iterations: 90
currently lose_sum: 364.34639662504196
time_elpased: 1.087
batch start
#iterations: 91
currently lose_sum: 361.8536291718483
time_elpased: 1.09
batch start
#iterations: 92
currently lose_sum: 363.3024987578392
time_elpased: 1.094
batch start
#iterations: 93
currently lose_sum: 361.93826526403427
time_elpased: 1.092
batch start
#iterations: 94
currently lose_sum: 364.0645800232887
time_elpased: 1.088
batch start
#iterations: 95
currently lose_sum: 361.8777837753296
time_elpased: 1.091
batch start
#iterations: 96
currently lose_sum: 364.1206061244011
time_elpased: 1.104
batch start
#iterations: 97
currently lose_sum: 364.1935518980026
time_elpased: 1.094
batch start
#iterations: 98
currently lose_sum: 363.2021613717079
time_elpased: 1.082
batch start
#iterations: 99
currently lose_sum: 362.7812556028366
time_elpased: 1.101
start validation test
0.787422680412
0.811049723757
0.752511790035
0.780684960647
0
validation finish
batch start
#iterations: 100
currently lose_sum: 363.6773900985718
time_elpased: 1.094
batch start
#iterations: 101
currently lose_sum: 364.5845550298691
time_elpased: 1.082
batch start
#iterations: 102
currently lose_sum: 363.4003599882126
time_elpased: 1.085
batch start
#iterations: 103
currently lose_sum: 362.7144972085953
time_elpased: 1.093
batch start
#iterations: 104
currently lose_sum: 362.674427986145
time_elpased: 1.09
batch start
#iterations: 105
currently lose_sum: 363.14447379112244
time_elpased: 1.086
batch start
#iterations: 106
currently lose_sum: 362.2850799560547
time_elpased: 1.091
batch start
#iterations: 107
currently lose_sum: 363.8444053530693
time_elpased: 1.09
batch start
#iterations: 108
currently lose_sum: 362.7487470507622
time_elpased: 1.087
batch start
#iterations: 109
currently lose_sum: 362.08186984062195
time_elpased: 1.101
batch start
#iterations: 110
currently lose_sum: 363.5356779694557
time_elpased: 1.096
batch start
#iterations: 111
currently lose_sum: 361.4182080626488
time_elpased: 1.088
batch start
#iterations: 112
currently lose_sum: 362.448831140995
time_elpased: 1.114
batch start
#iterations: 113
currently lose_sum: 363.7202011346817
time_elpased: 1.089
batch start
#iterations: 114
currently lose_sum: 362.75973504781723
time_elpased: 1.102
batch start
#iterations: 115
currently lose_sum: 361.54039800167084
time_elpased: 1.097
batch start
#iterations: 116
currently lose_sum: 362.7612480521202
time_elpased: 1.099
batch start
#iterations: 117
currently lose_sum: 362.83861243724823
time_elpased: 1.106
batch start
#iterations: 118
currently lose_sum: 361.40979063510895
time_elpased: 1.087
batch start
#iterations: 119
currently lose_sum: 363.54609811306
time_elpased: 1.085
start validation test
0.788865979381
0.808304272014
0.760405987287
0.783623877443
0
validation finish
batch start
#iterations: 120
currently lose_sum: 362.27108800411224
time_elpased: 1.09
batch start
#iterations: 121
currently lose_sum: 361.9055584669113
time_elpased: 1.091
batch start
#iterations: 122
currently lose_sum: 362.78044414520264
time_elpased: 1.092
batch start
#iterations: 123
currently lose_sum: 361.82042664289474
time_elpased: 1.092
batch start
#iterations: 124
currently lose_sum: 361.9591337442398
time_elpased: 1.102
batch start
#iterations: 125
currently lose_sum: 361.0835260748863
time_elpased: 1.085
batch start
#iterations: 126
currently lose_sum: 361.3704397678375
time_elpased: 1.096
batch start
#iterations: 127
currently lose_sum: 361.65921956300735
time_elpased: 1.103
batch start
#iterations: 128
currently lose_sum: 362.1429557800293
time_elpased: 1.097
batch start
#iterations: 129
currently lose_sum: 361.9478822350502
time_elpased: 1.087
batch start
#iterations: 130
currently lose_sum: 360.5863939523697
time_elpased: 1.092
batch start
#iterations: 131
currently lose_sum: 362.56081742048264
time_elpased: 1.1
batch start
#iterations: 132
currently lose_sum: 361.70554399490356
time_elpased: 1.103
batch start
#iterations: 133
currently lose_sum: 362.1946752667427
time_elpased: 1.1
batch start
#iterations: 134
currently lose_sum: 360.7744792699814
time_elpased: 1.113
batch start
#iterations: 135
currently lose_sum: 361.2488462328911
time_elpased: 1.092
batch start
#iterations: 136
currently lose_sum: 361.5003515481949
time_elpased: 1.101
batch start
#iterations: 137
currently lose_sum: 363.28178256750107
time_elpased: 1.093
batch start
#iterations: 138
currently lose_sum: 362.60780692100525
time_elpased: 1.094
batch start
#iterations: 139
currently lose_sum: 362.37986397743225
time_elpased: 1.095
start validation test
0.788762886598
0.808733624454
0.759483288907
0.783335095696
0
validation finish
batch start
#iterations: 140
currently lose_sum: 362.80869138240814
time_elpased: 1.117
batch start
#iterations: 141
currently lose_sum: 361.9812196493149
time_elpased: 1.095
batch start
#iterations: 142
currently lose_sum: 361.5580067038536
time_elpased: 1.107
batch start
#iterations: 143
currently lose_sum: 361.4731178879738
time_elpased: 1.106
batch start
#iterations: 144
currently lose_sum: 362.0288959443569
time_elpased: 1.086
batch start
#iterations: 145
currently lose_sum: 361.2455050945282
time_elpased: 1.088
batch start
#iterations: 146
currently lose_sum: 362.98823976516724
time_elpased: 1.104
batch start
#iterations: 147
currently lose_sum: 361.59111246466637
time_elpased: 1.093
batch start
#iterations: 148
currently lose_sum: 363.04041504859924
time_elpased: 1.093
batch start
#iterations: 149
currently lose_sum: 362.42431902885437
time_elpased: 1.089
batch start
#iterations: 150
currently lose_sum: 362.7824593782425
time_elpased: 1.102
batch start
#iterations: 151
currently lose_sum: 361.86992490291595
time_elpased: 1.085
batch start
#iterations: 152
currently lose_sum: 361.16254460811615
time_elpased: 1.081
batch start
#iterations: 153
currently lose_sum: 361.2132721543312
time_elpased: 1.093
batch start
#iterations: 154
currently lose_sum: 359.90159153938293
time_elpased: 1.094
batch start
#iterations: 155
currently lose_sum: 362.1879959702492
time_elpased: 1.096
batch start
#iterations: 156
currently lose_sum: 361.33179527521133
time_elpased: 1.088
batch start
#iterations: 157
currently lose_sum: 361.165468275547
time_elpased: 1.106
batch start
#iterations: 158
currently lose_sum: 360.3328831791878
time_elpased: 1.087
batch start
#iterations: 159
currently lose_sum: 361.0360267162323
time_elpased: 1.098
start validation test
0.78881443299
0.801964342906
0.7701455813
0.785732963757
0
validation finish
batch start
#iterations: 160
currently lose_sum: 362.82778787612915
time_elpased: 1.11
batch start
#iterations: 161
currently lose_sum: 361.5272899866104
time_elpased: 1.084
batch start
#iterations: 162
currently lose_sum: 362.0772615671158
time_elpased: 1.089
batch start
#iterations: 163
currently lose_sum: 362.8384345769882
time_elpased: 1.086
batch start
#iterations: 164
currently lose_sum: 362.0644800066948
time_elpased: 1.098
batch start
#iterations: 165
currently lose_sum: 362.17300391197205
time_elpased: 1.1
batch start
#iterations: 166
currently lose_sum: 361.4936555624008
time_elpased: 1.09
batch start
#iterations: 167
currently lose_sum: 361.4350251555443
time_elpased: 1.092
batch start
#iterations: 168
currently lose_sum: 361.46832966804504
time_elpased: 1.1
batch start
#iterations: 169
currently lose_sum: 361.4512062072754
time_elpased: 1.113
batch start
#iterations: 170
currently lose_sum: 360.05965250730515
time_elpased: 1.093
batch start
#iterations: 171
currently lose_sum: 361.9282341003418
time_elpased: 1.087
batch start
#iterations: 172
currently lose_sum: 362.0436370372772
time_elpased: 1.101
batch start
#iterations: 173
currently lose_sum: 361.6388657093048
time_elpased: 1.089
batch start
#iterations: 174
currently lose_sum: 361.5965438783169
time_elpased: 1.106
batch start
#iterations: 175
currently lose_sum: 361.5614221096039
time_elpased: 1.094
batch start
#iterations: 176
currently lose_sum: 362.14918166399
time_elpased: 1.093
batch start
#iterations: 177
currently lose_sum: 361.9561839103699
time_elpased: 1.09
batch start
#iterations: 178
currently lose_sum: 362.21922785043716
time_elpased: 1.098
batch start
#iterations: 179
currently lose_sum: 361.7166327238083
time_elpased: 1.094
start validation test
0.789329896907
0.807621322332
0.762661472217
0.78449775903
0
validation finish
batch start
#iterations: 180
currently lose_sum: 360.2913147807121
time_elpased: 1.091
batch start
#iterations: 181
currently lose_sum: 360.72221314907074
time_elpased: 1.095
batch start
#iterations: 182
currently lose_sum: 360.6741203069687
time_elpased: 1.108
batch start
#iterations: 183
currently lose_sum: 361.21169060468674
time_elpased: 1.086
batch start
#iterations: 184
currently lose_sum: 361.1480482816696
time_elpased: 1.09
batch start
#iterations: 185
currently lose_sum: 360.7878465652466
time_elpased: 1.109
batch start
#iterations: 186
currently lose_sum: 360.90429228544235
time_elpased: 1.096
batch start
#iterations: 187
currently lose_sum: 361.07291638851166
time_elpased: 1.108
batch start
#iterations: 188
currently lose_sum: 359.3338232636452
time_elpased: 1.09
batch start
#iterations: 189
currently lose_sum: 361.50961261987686
time_elpased: 1.093
batch start
#iterations: 190
currently lose_sum: 359.76953053474426
time_elpased: 1.09
batch start
#iterations: 191
currently lose_sum: 360.54532343149185
time_elpased: 1.093
batch start
#iterations: 192
currently lose_sum: 361.7810383439064
time_elpased: 1.09
batch start
#iterations: 193
currently lose_sum: 360.4801419377327
time_elpased: 1.119
batch start
#iterations: 194
currently lose_sum: 360.16562962532043
time_elpased: 1.092
batch start
#iterations: 195
currently lose_sum: 359.98550260066986
time_elpased: 1.1
batch start
#iterations: 196
currently lose_sum: 361.38666743040085
time_elpased: 1.112
batch start
#iterations: 197
currently lose_sum: 361.05363804101944
time_elpased: 1.095
batch start
#iterations: 198
currently lose_sum: 361.45797204971313
time_elpased: 1.095
batch start
#iterations: 199
currently lose_sum: 362.16992151737213
time_elpased: 1.098
start validation test
0.792216494845
0.80004194191
0.782243182284
0.791042455031
0
validation finish
batch start
#iterations: 200
currently lose_sum: 361.54307985305786
time_elpased: 1.103
batch start
#iterations: 201
currently lose_sum: 360.93431729078293
time_elpased: 1.082
batch start
#iterations: 202
currently lose_sum: 361.0832055211067
time_elpased: 1.095
batch start
#iterations: 203
currently lose_sum: 361.66534304618835
time_elpased: 1.099
batch start
#iterations: 204
currently lose_sum: 361.47567158937454
time_elpased: 1.095
batch start
#iterations: 205
currently lose_sum: 360.2182704806328
time_elpased: 1.093
batch start
#iterations: 206
currently lose_sum: 360.51452761888504
time_elpased: 1.094
batch start
#iterations: 207
currently lose_sum: 360.6414524316788
time_elpased: 1.111
batch start
#iterations: 208
currently lose_sum: 361.4060956835747
time_elpased: 1.097
batch start
#iterations: 209
currently lose_sum: 360.83792597055435
time_elpased: 1.086
batch start
#iterations: 210
currently lose_sum: 361.06246519088745
time_elpased: 1.092
batch start
#iterations: 211
currently lose_sum: 360.8330634236336
time_elpased: 1.095
batch start
#iterations: 212
currently lose_sum: 360.2433978319168
time_elpased: 1.091
batch start
#iterations: 213
currently lose_sum: 360.50804901123047
time_elpased: 1.096
batch start
#iterations: 214
currently lose_sum: 361.080903172493
time_elpased: 1.097
batch start
#iterations: 215
currently lose_sum: 359.61395704746246
time_elpased: 1.102
batch start
#iterations: 216
currently lose_sum: 359.6539377570152
time_elpased: 1.09
batch start
#iterations: 217
currently lose_sum: 362.1821438074112
time_elpased: 1.102
batch start
#iterations: 218
currently lose_sum: 360.9977477192879
time_elpased: 1.088
batch start
#iterations: 219
currently lose_sum: 360.44795686006546
time_elpased: 1.092
start validation test
0.789020618557
0.806430659305
0.763686692639
0.784476857459
0
validation finish
batch start
#iterations: 220
currently lose_sum: 362.04977667331696
time_elpased: 1.099
batch start
#iterations: 221
currently lose_sum: 362.00948536396027
time_elpased: 1.114
batch start
#iterations: 222
currently lose_sum: 360.07423639297485
time_elpased: 1.095
batch start
#iterations: 223
currently lose_sum: 359.6069384813309
time_elpased: 1.101
batch start
#iterations: 224
currently lose_sum: 361.90926617383957
time_elpased: 1.098
batch start
#iterations: 225
currently lose_sum: 362.5897558927536
time_elpased: 1.116
batch start
#iterations: 226
currently lose_sum: 359.96296268701553
time_elpased: 1.093
batch start
#iterations: 227
currently lose_sum: 360.7177622318268
time_elpased: 1.097
batch start
#iterations: 228
currently lose_sum: 361.1178152561188
time_elpased: 1.088
batch start
#iterations: 229
currently lose_sum: 360.994408428669
time_elpased: 1.09
batch start
#iterations: 230
currently lose_sum: 361.47838163375854
time_elpased: 1.095
batch start
#iterations: 231
currently lose_sum: 360.2784984111786
time_elpased: 1.098
batch start
#iterations: 232
currently lose_sum: 361.3612711429596
time_elpased: 1.098
batch start
#iterations: 233
currently lose_sum: 361.1432455778122
time_elpased: 1.104
batch start
#iterations: 234
currently lose_sum: 360.56273368000984
time_elpased: 1.083
batch start
#iterations: 235
currently lose_sum: 359.7210772037506
time_elpased: 1.092
batch start
#iterations: 236
currently lose_sum: 360.6366713643074
time_elpased: 1.084
batch start
#iterations: 237
currently lose_sum: 360.99069505929947
time_elpased: 1.097
batch start
#iterations: 238
currently lose_sum: 360.6074583530426
time_elpased: 1.09
batch start
#iterations: 239
currently lose_sum: 362.9873218536377
time_elpased: 1.092
start validation test
0.788917525773
0.803236523417
0.768402706582
0.785433586586
0
validation finish
batch start
#iterations: 240
currently lose_sum: 361.6516046524048
time_elpased: 1.111
batch start
#iterations: 241
currently lose_sum: 360.0482317209244
time_elpased: 1.099
batch start
#iterations: 242
currently lose_sum: 359.5784741640091
time_elpased: 1.095
batch start
#iterations: 243
currently lose_sum: 361.7992419600487
time_elpased: 1.093
batch start
#iterations: 244
currently lose_sum: 360.1693828701973
time_elpased: 1.105
batch start
#iterations: 245
currently lose_sum: 360.90589970350266
time_elpased: 1.095
batch start
#iterations: 246
currently lose_sum: 361.5622735619545
time_elpased: 1.091
batch start
#iterations: 247
currently lose_sum: 360.71720761060715
time_elpased: 1.097
batch start
#iterations: 248
currently lose_sum: 360.73473089933395
time_elpased: 1.099
batch start
#iterations: 249
currently lose_sum: 360.0671725869179
time_elpased: 1.103
batch start
#iterations: 250
currently lose_sum: 359.554011285305
time_elpased: 1.086
batch start
#iterations: 251
currently lose_sum: 360.8662471175194
time_elpased: 1.097
batch start
#iterations: 252
currently lose_sum: 361.8532630801201
time_elpased: 1.091
batch start
#iterations: 253
currently lose_sum: 360.6511330604553
time_elpased: 1.096
batch start
#iterations: 254
currently lose_sum: 360.46448826789856
time_elpased: 1.114
batch start
#iterations: 255
currently lose_sum: 360.35805732011795
time_elpased: 1.104
batch start
#iterations: 256
currently lose_sum: 360.9150921702385
time_elpased: 1.098
batch start
#iterations: 257
currently lose_sum: 362.2865775823593
time_elpased: 1.092
batch start
#iterations: 258
currently lose_sum: 360.5340917110443
time_elpased: 1.104
batch start
#iterations: 259
currently lose_sum: 359.99976348876953
time_elpased: 1.103
start validation test
0.788917525773
0.804542029921
0.766352265737
0.784982935154
0
validation finish
batch start
#iterations: 260
currently lose_sum: 360.8238961696625
time_elpased: 1.095
batch start
#iterations: 261
currently lose_sum: 360.60563641786575
time_elpased: 1.092
batch start
#iterations: 262
currently lose_sum: 359.8618117570877
time_elpased: 1.102
batch start
#iterations: 263
currently lose_sum: 361.29871439933777
time_elpased: 1.088
batch start
#iterations: 264
currently lose_sum: 360.3869687318802
time_elpased: 1.101
batch start
#iterations: 265
currently lose_sum: 361.4342303276062
time_elpased: 1.097
batch start
#iterations: 266
currently lose_sum: 359.84714835882187
time_elpased: 1.099
batch start
#iterations: 267
currently lose_sum: 360.65597528219223
time_elpased: 1.101
batch start
#iterations: 268
currently lose_sum: 359.7363324165344
time_elpased: 1.108
batch start
#iterations: 269
currently lose_sum: 359.8422257900238
time_elpased: 1.094
batch start
#iterations: 270
currently lose_sum: 359.8602412343025
time_elpased: 1.101
batch start
#iterations: 271
currently lose_sum: 359.5705537199974
time_elpased: 1.096
batch start
#iterations: 272
currently lose_sum: 361.0416752099991
time_elpased: 1.094
batch start
#iterations: 273
currently lose_sum: 359.92389500141144
time_elpased: 1.09
batch start
#iterations: 274
currently lose_sum: 361.4318954348564
time_elpased: 1.085
batch start
#iterations: 275
currently lose_sum: 360.9222663640976
time_elpased: 1.092
batch start
#iterations: 276
currently lose_sum: 360.73871302604675
time_elpased: 1.103
batch start
#iterations: 277
currently lose_sum: 360.2226051092148
time_elpased: 1.108
batch start
#iterations: 278
currently lose_sum: 360.9688574075699
time_elpased: 1.087
batch start
#iterations: 279
currently lose_sum: 361.0739846229553
time_elpased: 1.089
start validation test
0.790721649485
0.79886626076
0.780192741439
0.789419087137
0
validation finish
batch start
#iterations: 280
currently lose_sum: 359.56595343351364
time_elpased: 1.103
batch start
#iterations: 281
currently lose_sum: 359.34789633750916
time_elpased: 1.099
batch start
#iterations: 282
currently lose_sum: 361.60239112377167
time_elpased: 1.103
batch start
#iterations: 283
currently lose_sum: 362.2521022558212
time_elpased: 1.091
batch start
#iterations: 284
currently lose_sum: 361.57140678167343
time_elpased: 1.101
batch start
#iterations: 285
currently lose_sum: 359.5482340455055
time_elpased: 1.105
batch start
#iterations: 286
currently lose_sum: 359.9347929954529
time_elpased: 1.102
batch start
#iterations: 287
currently lose_sum: 359.56594091653824
time_elpased: 1.095
batch start
#iterations: 288
currently lose_sum: 361.26768016815186
time_elpased: 1.084
batch start
#iterations: 289
currently lose_sum: 358.7549455165863
time_elpased: 1.106
batch start
#iterations: 290
currently lose_sum: 360.51117420196533
time_elpased: 1.089
batch start
#iterations: 291
currently lose_sum: 359.4849666953087
time_elpased: 1.092
batch start
#iterations: 292
currently lose_sum: 360.10013312101364
time_elpased: 1.097
batch start
#iterations: 293
currently lose_sum: 360.11567586660385
time_elpased: 1.107
batch start
#iterations: 294
currently lose_sum: 359.73787796497345
time_elpased: 1.098
batch start
#iterations: 295
currently lose_sum: 359.91419607400894
time_elpased: 1.094
batch start
#iterations: 296
currently lose_sum: 359.4898074865341
time_elpased: 1.103
batch start
#iterations: 297
currently lose_sum: 360.38598692417145
time_elpased: 1.088
batch start
#iterations: 298
currently lose_sum: 361.34739875793457
time_elpased: 1.104
batch start
#iterations: 299
currently lose_sum: 360.3368415236473
time_elpased: 1.098
start validation test
0.789639175258
0.801274561869
0.773426286652
0.787104178622
0
validation finish
batch start
#iterations: 300
currently lose_sum: 361.02419954538345
time_elpased: 1.11
batch start
#iterations: 301
currently lose_sum: 359.8901677727699
time_elpased: 1.098
batch start
#iterations: 302
currently lose_sum: 360.0650622844696
time_elpased: 1.091
batch start
#iterations: 303
currently lose_sum: 360.0618265271187
time_elpased: 1.092
batch start
#iterations: 304
currently lose_sum: 359.30493235588074
time_elpased: 1.097
batch start
#iterations: 305
currently lose_sum: 360.51470851898193
time_elpased: 1.098
batch start
#iterations: 306
currently lose_sum: 358.8379727602005
time_elpased: 1.09
batch start
#iterations: 307
currently lose_sum: 360.8809703588486
time_elpased: 1.096
batch start
#iterations: 308
currently lose_sum: 362.24617326259613
time_elpased: 1.093
batch start
#iterations: 309
currently lose_sum: 360.5624647140503
time_elpased: 1.095
batch start
#iterations: 310
currently lose_sum: 360.8117492198944
time_elpased: 1.102
batch start
#iterations: 311
currently lose_sum: 358.67501693964005
time_elpased: 1.098
batch start
#iterations: 312
currently lose_sum: 360.98024505376816
time_elpased: 1.089
batch start
#iterations: 313
currently lose_sum: 359.8812748193741
time_elpased: 1.087
batch start
#iterations: 314
currently lose_sum: 360.0627163052559
time_elpased: 1.101
batch start
#iterations: 315
currently lose_sum: 358.4456831216812
time_elpased: 1.107
batch start
#iterations: 316
currently lose_sum: 361.24227398633957
time_elpased: 1.089
batch start
#iterations: 317
currently lose_sum: 358.9094128012657
time_elpased: 1.091
batch start
#iterations: 318
currently lose_sum: 358.1991733908653
time_elpased: 1.084
batch start
#iterations: 319
currently lose_sum: 360.23620492219925
time_elpased: 1.097
start validation test
0.79175257732
0.804389516301
0.774041418905
0.788923719958
0
validation finish
batch start
#iterations: 320
currently lose_sum: 359.9034024477005
time_elpased: 1.1
batch start
#iterations: 321
currently lose_sum: 359.9033333957195
time_elpased: 1.091
batch start
#iterations: 322
currently lose_sum: 361.1374025940895
time_elpased: 1.109
batch start
#iterations: 323
currently lose_sum: 358.0167330503464
time_elpased: 1.096
batch start
#iterations: 324
currently lose_sum: 359.09149450063705
time_elpased: 1.088
batch start
#iterations: 325
currently lose_sum: 360.256265103817
time_elpased: 1.095
batch start
#iterations: 326
currently lose_sum: 360.88700622320175
time_elpased: 1.091
batch start
#iterations: 327
currently lose_sum: 360.1634722352028
time_elpased: 1.099
batch start
#iterations: 328
currently lose_sum: 361.17543053627014
time_elpased: 1.094
batch start
#iterations: 329
currently lose_sum: 360.2607591152191
time_elpased: 1.106
batch start
#iterations: 330
currently lose_sum: 359.9079882502556
time_elpased: 1.102
batch start
#iterations: 331
currently lose_sum: 359.0879782438278
time_elpased: 1.088
batch start
#iterations: 332
currently lose_sum: 359.68207663297653
time_elpased: 1.098
batch start
#iterations: 333
currently lose_sum: 360.0856736898422
time_elpased: 1.094
batch start
#iterations: 334
currently lose_sum: 358.445269882679
time_elpased: 1.093
batch start
#iterations: 335
currently lose_sum: 359.347743332386
time_elpased: 1.107
batch start
#iterations: 336
currently lose_sum: 359.4228432774544
time_elpased: 1.1
batch start
#iterations: 337
currently lose_sum: 360.147169649601
time_elpased: 1.089
batch start
#iterations: 338
currently lose_sum: 360.0245521068573
time_elpased: 1.108
batch start
#iterations: 339
currently lose_sum: 359.9432170987129
time_elpased: 1.108
start validation test
0.790721649485
0.798240100566
0.781217961862
0.789637305699
0
validation finish
batch start
#iterations: 340
currently lose_sum: 360.46556067466736
time_elpased: 1.093
batch start
#iterations: 341
currently lose_sum: 357.5115457177162
time_elpased: 1.116
batch start
#iterations: 342
currently lose_sum: 357.76321226358414
time_elpased: 1.088
batch start
#iterations: 343
currently lose_sum: 360.5306451320648
time_elpased: 1.091
batch start
#iterations: 344
currently lose_sum: 360.39719611406326
time_elpased: 1.091
batch start
#iterations: 345
currently lose_sum: 359.32530546188354
time_elpased: 1.111
batch start
#iterations: 346
currently lose_sum: 358.92253589630127
time_elpased: 1.084
batch start
#iterations: 347
currently lose_sum: 359.24845361709595
time_elpased: 1.106
batch start
#iterations: 348
currently lose_sum: 360.54352074861526
time_elpased: 1.1
batch start
#iterations: 349
currently lose_sum: 360.0296652317047
time_elpased: 1.096
batch start
#iterations: 350
currently lose_sum: 358.72361755371094
time_elpased: 1.098
batch start
#iterations: 351
currently lose_sum: 360.53377801179886
time_elpased: 1.086
batch start
#iterations: 352
currently lose_sum: 360.3220825791359
time_elpased: 1.093
batch start
#iterations: 353
currently lose_sum: 359.7763549685478
time_elpased: 1.09
batch start
#iterations: 354
currently lose_sum: 358.17836940288544
time_elpased: 1.1
batch start
#iterations: 355
currently lose_sum: 358.80320286750793
time_elpased: 1.089
batch start
#iterations: 356
currently lose_sum: 361.0829043984413
time_elpased: 1.091
batch start
#iterations: 357
currently lose_sum: 359.0360125005245
time_elpased: 1.088
batch start
#iterations: 358
currently lose_sum: 359.724980533123
time_elpased: 1.1
batch start
#iterations: 359
currently lose_sum: 360.0508009195328
time_elpased: 1.098
start validation test
0.78912371134
0.803255863768
0.768915316793
0.785710544236
0
validation finish
batch start
#iterations: 360
currently lose_sum: 357.7273512482643
time_elpased: 1.096
batch start
#iterations: 361
currently lose_sum: 359.50953060388565
time_elpased: 1.112
batch start
#iterations: 362
currently lose_sum: 360.1215991973877
time_elpased: 1.1
batch start
#iterations: 363
currently lose_sum: 358.76675271987915
time_elpased: 1.097
batch start
#iterations: 364
currently lose_sum: 358.9919557571411
time_elpased: 1.1
batch start
#iterations: 365
currently lose_sum: 359.7204455733299
time_elpased: 1.092
batch start
#iterations: 366
currently lose_sum: 359.5745034813881
time_elpased: 1.107
batch start
#iterations: 367
currently lose_sum: 359.11995923519135
time_elpased: 1.095
batch start
#iterations: 368
currently lose_sum: 361.05494433641434
time_elpased: 1.087
batch start
#iterations: 369
currently lose_sum: 360.48769956827164
time_elpased: 1.097
batch start
#iterations: 370
currently lose_sum: 359.88261491060257
time_elpased: 1.092
batch start
#iterations: 371
currently lose_sum: 359.1744812428951
time_elpased: 1.089
batch start
#iterations: 372
currently lose_sum: 358.71743255853653
time_elpased: 1.102
batch start
#iterations: 373
currently lose_sum: 360.54702973365784
time_elpased: 1.099
batch start
#iterations: 374
currently lose_sum: 360.38237750530243
time_elpased: 1.089
batch start
#iterations: 375
currently lose_sum: 359.068659722805
time_elpased: 1.105
batch start
#iterations: 376
currently lose_sum: 360.5964262485504
time_elpased: 1.106
batch start
#iterations: 377
currently lose_sum: 361.17476600408554
time_elpased: 1.109
batch start
#iterations: 378
currently lose_sum: 358.62834602594376
time_elpased: 1.093
batch start
#iterations: 379
currently lose_sum: 359.8340754508972
time_elpased: 1.097
start validation test
0.791597938144
0.800547310809
0.77978265327
0.790028564009
0
validation finish
batch start
#iterations: 380
currently lose_sum: 360.532574236393
time_elpased: 1.103
batch start
#iterations: 381
currently lose_sum: 359.64440220594406
time_elpased: 1.104
batch start
#iterations: 382
currently lose_sum: 359.7423307299614
time_elpased: 1.103
batch start
#iterations: 383
currently lose_sum: 358.6667922437191
time_elpased: 1.093
batch start
#iterations: 384
currently lose_sum: 359.24413162469864
time_elpased: 1.093
batch start
#iterations: 385
currently lose_sum: 359.0313324332237
time_elpased: 1.122
batch start
#iterations: 386
currently lose_sum: 358.96007281541824
time_elpased: 1.106
batch start
#iterations: 387
currently lose_sum: 360.64855068922043
time_elpased: 1.089
batch start
#iterations: 388
currently lose_sum: 359.58287101984024
time_elpased: 1.093
batch start
#iterations: 389
currently lose_sum: 359.5478250384331
time_elpased: 1.091
batch start
#iterations: 390
currently lose_sum: 360.322558760643
time_elpased: 1.1
batch start
#iterations: 391
currently lose_sum: 359.69732624292374
time_elpased: 1.085
batch start
#iterations: 392
currently lose_sum: 359.2188612818718
time_elpased: 1.094
batch start
#iterations: 393
currently lose_sum: 358.32081830501556
time_elpased: 1.086
batch start
#iterations: 394
currently lose_sum: 358.5905804038048
time_elpased: 1.105
batch start
#iterations: 395
currently lose_sum: 359.86538314819336
time_elpased: 1.093
batch start
#iterations: 396
currently lose_sum: 359.6461819410324
time_elpased: 1.095
batch start
#iterations: 397
currently lose_sum: 360.77308440208435
time_elpased: 1.095
batch start
#iterations: 398
currently lose_sum: 359.8486480116844
time_elpased: 1.09
batch start
#iterations: 399
currently lose_sum: 358.7445603609085
time_elpased: 1.101
start validation test
0.791443298969
0.798660527417
0.782448226369
0.790471258415
0
validation finish
acc: 0.788
pre: 0.801
rec: 0.769
F1: 0.785
auc: 0.000
