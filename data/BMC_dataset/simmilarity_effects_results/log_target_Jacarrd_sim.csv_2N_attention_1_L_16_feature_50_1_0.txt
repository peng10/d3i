start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 555.494811296463
time_elpased: 2.555
batch start
#iterations: 1
currently lose_sum: 537.1375576853752
time_elpased: 2.559
batch start
#iterations: 2
currently lose_sum: 529.8404478430748
time_elpased: 2.577
batch start
#iterations: 3
currently lose_sum: 525.4237607717514
time_elpased: 2.566
batch start
#iterations: 4
currently lose_sum: 521.4488397240639
time_elpased: 2.571
batch start
#iterations: 5
currently lose_sum: 520.0612050890923
time_elpased: 2.567
batch start
#iterations: 6
currently lose_sum: 518.5872668325901
time_elpased: 2.548
batch start
#iterations: 7
currently lose_sum: 516.6809458434582
time_elpased: 2.567
batch start
#iterations: 8
currently lose_sum: 516.5340175032616
time_elpased: 2.566
batch start
#iterations: 9
currently lose_sum: 518.0018716156483
time_elpased: 2.562
batch start
#iterations: 10
currently lose_sum: 516.9104228913784
time_elpased: 2.58
batch start
#iterations: 11
currently lose_sum: 518.309449493885
time_elpased: 2.564
batch start
#iterations: 12
currently lose_sum: 516.8710866868496
time_elpased: 2.569
batch start
#iterations: 13
currently lose_sum: 514.4209802150726
time_elpased: 2.572
batch start
#iterations: 14
currently lose_sum: 515.2555711269379
time_elpased: 2.566
batch start
#iterations: 15
currently lose_sum: 512.8027234375477
time_elpased: 2.581
batch start
#iterations: 16
currently lose_sum: 513.3514954149723
time_elpased: 2.571
batch start
#iterations: 17
currently lose_sum: 513.8511054515839
time_elpased: 2.573
batch start
#iterations: 18
currently lose_sum: 512.4607020914555
time_elpased: 2.577
batch start
#iterations: 19
currently lose_sum: 512.4553197622299
time_elpased: 2.588
start validation test
0.593608247423
0.88939462904
0.203647733194
0.331411126187
0
validation finish
batch start
#iterations: 20
currently lose_sum: 511.2206401526928
time_elpased: 2.565
batch start
#iterations: 21
currently lose_sum: 513.1529847979546
time_elpased: 2.585
batch start
#iterations: 22
currently lose_sum: 511.6883423924446
time_elpased: 2.578
batch start
#iterations: 23
currently lose_sum: 512.0846779346466
time_elpased: 2.586
batch start
#iterations: 24
currently lose_sum: 510.1005211174488
time_elpased: 2.585
batch start
#iterations: 25
currently lose_sum: 510.9353615939617
time_elpased: 2.586
batch start
#iterations: 26
currently lose_sum: 510.2773635685444
time_elpased: 2.56
batch start
#iterations: 27
currently lose_sum: 509.93198350071907
time_elpased: 2.571
batch start
#iterations: 28
currently lose_sum: 508.6938344538212
time_elpased: 2.583
batch start
#iterations: 29
currently lose_sum: 509.61495420336723
time_elpased: 2.579
batch start
#iterations: 30
currently lose_sum: 506.87409326434135
time_elpased: 2.599
batch start
#iterations: 31
currently lose_sum: 509.0131246447563
time_elpased: 2.577
batch start
#iterations: 32
currently lose_sum: 509.2018648982048
time_elpased: 2.587
batch start
#iterations: 33
currently lose_sum: 508.41937789320946
time_elpased: 2.576
batch start
#iterations: 34
currently lose_sum: 508.93620947003365
time_elpased: 2.588
batch start
#iterations: 35
currently lose_sum: 507.3866271674633
time_elpased: 2.592
batch start
#iterations: 36
currently lose_sum: 507.07984778285027
time_elpased: 2.599
batch start
#iterations: 37
currently lose_sum: 506.35621294379234
time_elpased: 2.576
batch start
#iterations: 38
currently lose_sum: 507.41514056921005
time_elpased: 2.579
batch start
#iterations: 39
currently lose_sum: 509.4025074541569
time_elpased: 2.573
start validation test
0.587371134021
0.908110882957
0.184366857738
0.306506107598
0
validation finish
batch start
#iterations: 40
currently lose_sum: 507.8303717672825
time_elpased: 2.553
batch start
#iterations: 41
currently lose_sum: 509.2838649749756
time_elpased: 2.571
batch start
#iterations: 42
currently lose_sum: 505.9891310632229
time_elpased: 2.578
batch start
#iterations: 43
currently lose_sum: 506.7873739898205
time_elpased: 2.556
batch start
#iterations: 44
currently lose_sum: 508.88820001482964
time_elpased: 2.593
batch start
#iterations: 45
currently lose_sum: 509.33876621723175
time_elpased: 2.582
batch start
#iterations: 46
currently lose_sum: 504.9073491692543
time_elpased: 2.574
batch start
#iterations: 47
currently lose_sum: 507.06006157398224
time_elpased: 2.583
batch start
#iterations: 48
currently lose_sum: 507.37440559268
time_elpased: 2.572
batch start
#iterations: 49
currently lose_sum: 504.596050709486
time_elpased: 2.578
batch start
#iterations: 50
currently lose_sum: 507.7430981993675
time_elpased: 2.607
batch start
#iterations: 51
currently lose_sum: 506.3583692908287
time_elpased: 2.583
batch start
#iterations: 52
currently lose_sum: 505.92022609710693
time_elpased: 2.574
batch start
#iterations: 53
currently lose_sum: 506.6267279088497
time_elpased: 2.6
batch start
#iterations: 54
currently lose_sum: 505.8199620246887
time_elpased: 2.597
batch start
#iterations: 55
currently lose_sum: 506.09531447291374
time_elpased: 2.595
batch start
#iterations: 56
currently lose_sum: 506.61288100481033
time_elpased: 2.584
batch start
#iterations: 57
currently lose_sum: 505.71946716308594
time_elpased: 2.559
batch start
#iterations: 58
currently lose_sum: 506.98168274760246
time_elpased: 2.585
batch start
#iterations: 59
currently lose_sum: 504.7848945260048
time_elpased: 2.594
start validation test
0.599639175258
0.914700544465
0.210109431996
0.341723874905
0
validation finish
batch start
#iterations: 60
currently lose_sum: 504.4552465379238
time_elpased: 2.572
batch start
#iterations: 61
currently lose_sum: 505.7457562685013
time_elpased: 2.574
batch start
#iterations: 62
currently lose_sum: 507.814018458128
time_elpased: 2.575
batch start
#iterations: 63
currently lose_sum: 507.34801346063614
time_elpased: 2.57
batch start
#iterations: 64
currently lose_sum: 505.69256660342216
time_elpased: 2.592
batch start
#iterations: 65
currently lose_sum: 506.7299785912037
time_elpased: 2.588
batch start
#iterations: 66
currently lose_sum: 505.0698726475239
time_elpased: 2.575
batch start
#iterations: 67
currently lose_sum: 504.08784902095795
time_elpased: 2.604
batch start
#iterations: 68
currently lose_sum: 505.90775522589684
time_elpased: 2.582
batch start
#iterations: 69
currently lose_sum: 507.38166585564613
time_elpased: 2.581
batch start
#iterations: 70
currently lose_sum: 502.81427133083344
time_elpased: 2.584
batch start
#iterations: 71
currently lose_sum: 505.32659938931465
time_elpased: 2.583
batch start
#iterations: 72
currently lose_sum: 507.4856578409672
time_elpased: 2.579
batch start
#iterations: 73
currently lose_sum: 503.65159180760384
time_elpased: 2.6
batch start
#iterations: 74
currently lose_sum: 506.4970416724682
time_elpased: 2.587
batch start
#iterations: 75
currently lose_sum: 505.2750655114651
time_elpased: 2.593
batch start
#iterations: 76
currently lose_sum: 504.6395328640938
time_elpased: 2.595
batch start
#iterations: 77
currently lose_sum: 505.5805991292
time_elpased: 2.572
batch start
#iterations: 78
currently lose_sum: 506.1258062720299
time_elpased: 2.583
batch start
#iterations: 79
currently lose_sum: 507.3057328760624
time_elpased: 2.593
start validation test
0.618865979381
0.896290961469
0.259405940594
0.402360168122
0
validation finish
batch start
#iterations: 80
currently lose_sum: 505.0221719145775
time_elpased: 2.57
batch start
#iterations: 81
currently lose_sum: 503.4430057108402
time_elpased: 2.589
batch start
#iterations: 82
currently lose_sum: 504.43110504746437
time_elpased: 2.588
batch start
#iterations: 83
currently lose_sum: 504.1178089380264
time_elpased: 2.573
batch start
#iterations: 84
currently lose_sum: 504.3704418540001
time_elpased: 2.598
batch start
#iterations: 85
currently lose_sum: 503.39259162545204
time_elpased: 2.598
batch start
#iterations: 86
currently lose_sum: 504.03013721108437
time_elpased: 2.571
batch start
#iterations: 87
currently lose_sum: 502.96292334795
time_elpased: 2.597
batch start
#iterations: 88
currently lose_sum: 505.12857669591904
time_elpased: 2.579
batch start
#iterations: 89
currently lose_sum: 505.32651391625404
time_elpased: 2.551
batch start
#iterations: 90
currently lose_sum: 503.25364312529564
time_elpased: 2.59
batch start
#iterations: 91
currently lose_sum: 503.4597169160843
time_elpased: 2.577
batch start
#iterations: 92
currently lose_sum: 504.23016038537025
time_elpased: 2.585
batch start
#iterations: 93
currently lose_sum: 503.9202697277069
time_elpased: 2.581
batch start
#iterations: 94
currently lose_sum: 503.9791032075882
time_elpased: 2.575
batch start
#iterations: 95
currently lose_sum: 502.7699039578438
time_elpased: 2.576
batch start
#iterations: 96
currently lose_sum: 505.70529955625534
time_elpased: 2.598
batch start
#iterations: 97
currently lose_sum: 501.94011667370796
time_elpased: 2.571
batch start
#iterations: 98
currently lose_sum: 504.50644022226334
time_elpased: 2.579
batch start
#iterations: 99
currently lose_sum: 505.0557926893234
time_elpased: 2.585
start validation test
0.606597938144
0.908788004998
0.227410109432
0.36378792931
0
validation finish
batch start
#iterations: 100
currently lose_sum: 504.34523966908455
time_elpased: 2.578
batch start
#iterations: 101
currently lose_sum: 504.900748193264
time_elpased: 2.59
batch start
#iterations: 102
currently lose_sum: 502.11889734864235
time_elpased: 2.588
batch start
#iterations: 103
currently lose_sum: 505.05855363607407
time_elpased: 2.561
batch start
#iterations: 104
currently lose_sum: 503.47961750626564
time_elpased: 2.572
batch start
#iterations: 105
currently lose_sum: 502.9873035252094
time_elpased: 2.571
batch start
#iterations: 106
currently lose_sum: 505.6245899796486
time_elpased: 2.544
batch start
#iterations: 107
currently lose_sum: 502.6037239432335
time_elpased: 2.573
batch start
#iterations: 108
currently lose_sum: 501.86042085289955
time_elpased: 2.557
batch start
#iterations: 109
currently lose_sum: 503.6795963346958
time_elpased: 2.563
batch start
#iterations: 110
currently lose_sum: 505.09383139014244
time_elpased: 2.6
batch start
#iterations: 111
currently lose_sum: 505.886881172657
time_elpased: 2.588
batch start
#iterations: 112
currently lose_sum: 502.0372782945633
time_elpased: 2.583
batch start
#iterations: 113
currently lose_sum: 504.1680172085762
time_elpased: 2.6
batch start
#iterations: 114
currently lose_sum: 504.37569561600685
time_elpased: 2.584
batch start
#iterations: 115
currently lose_sum: 504.2314129769802
time_elpased: 2.593
batch start
#iterations: 116
currently lose_sum: 501.9981159865856
time_elpased: 2.583
batch start
#iterations: 117
currently lose_sum: 504.7113619148731
time_elpased: 2.559
batch start
#iterations: 118
currently lose_sum: 503.503790050745
time_elpased: 2.57
batch start
#iterations: 119
currently lose_sum: 503.7601636350155
time_elpased: 2.581
start validation test
0.623762886598
0.912060301508
0.264825429911
0.410467652047
0
validation finish
batch start
#iterations: 120
currently lose_sum: 503.52434226870537
time_elpased: 2.562
batch start
#iterations: 121
currently lose_sum: 501.88686206936836
time_elpased: 2.58
batch start
#iterations: 122
currently lose_sum: 502.6072687804699
time_elpased: 2.579
batch start
#iterations: 123
currently lose_sum: 502.19136103987694
time_elpased: 2.569
batch start
#iterations: 124
currently lose_sum: 502.68104270100594
time_elpased: 2.585
batch start
#iterations: 125
currently lose_sum: 503.8008756041527
time_elpased: 2.576
batch start
#iterations: 126
currently lose_sum: 504.04260098934174
time_elpased: 2.569
batch start
#iterations: 127
currently lose_sum: 503.9239513576031
time_elpased: 2.6
batch start
#iterations: 128
currently lose_sum: 503.2646478712559
time_elpased: 2.581
batch start
#iterations: 129
currently lose_sum: 503.0721469819546
time_elpased: 2.569
batch start
#iterations: 130
currently lose_sum: 503.2580165863037
time_elpased: 2.583
batch start
#iterations: 131
currently lose_sum: 503.7569742500782
time_elpased: 2.574
batch start
#iterations: 132
currently lose_sum: 503.09788700938225
time_elpased: 2.568
batch start
#iterations: 133
currently lose_sum: 504.5823408961296
time_elpased: 2.585
batch start
#iterations: 134
currently lose_sum: 502.4079869389534
time_elpased: 2.584
batch start
#iterations: 135
currently lose_sum: 502.0031501352787
time_elpased: 2.583
batch start
#iterations: 136
currently lose_sum: 502.6189489066601
time_elpased: 2.602
batch start
#iterations: 137
currently lose_sum: 501.8517825305462
time_elpased: 2.576
batch start
#iterations: 138
currently lose_sum: 499.81514513492584
time_elpased: 2.584
batch start
#iterations: 139
currently lose_sum: 502.34390392899513
time_elpased: 2.576
start validation test
0.59824742268
0.918642491864
0.205940594059
0.336454963392
0
validation finish
batch start
#iterations: 140
currently lose_sum: 501.384828209877
time_elpased: 2.572
batch start
#iterations: 141
currently lose_sum: 502.5709829032421
time_elpased: 2.585
batch start
#iterations: 142
currently lose_sum: 499.6829937696457
time_elpased: 2.59
batch start
#iterations: 143
currently lose_sum: 502.4117090702057
time_elpased: 2.558
batch start
#iterations: 144
currently lose_sum: 501.40929663181305
time_elpased: 2.568
batch start
#iterations: 145
currently lose_sum: 503.93679374456406
time_elpased: 2.572
batch start
#iterations: 146
currently lose_sum: 504.0269809663296
time_elpased: 2.568
batch start
#iterations: 147
currently lose_sum: 502.9017714858055
time_elpased: 2.57
batch start
#iterations: 148
currently lose_sum: 500.89222240448
time_elpased: 2.579
batch start
#iterations: 149
currently lose_sum: 503.64432460069656
time_elpased: 2.574
batch start
#iterations: 150
currently lose_sum: 501.7626551389694
time_elpased: 2.577
batch start
#iterations: 151
currently lose_sum: 500.44472709298134
time_elpased: 2.564
batch start
#iterations: 152
currently lose_sum: 501.4191098809242
time_elpased: 2.572
batch start
#iterations: 153
currently lose_sum: 499.84604927897453
time_elpased: 2.586
batch start
#iterations: 154
currently lose_sum: 505.5791840851307
time_elpased: 2.578
batch start
#iterations: 155
currently lose_sum: 499.90296149253845
time_elpased: 2.574
batch start
#iterations: 156
currently lose_sum: 501.8435029089451
time_elpased: 2.589
batch start
#iterations: 157
currently lose_sum: 501.33560940623283
time_elpased: 2.568
batch start
#iterations: 158
currently lose_sum: 501.066348195076
time_elpased: 2.576
batch start
#iterations: 159
currently lose_sum: 501.4268773794174
time_elpased: 2.575
start validation test
0.622474226804
0.906552094522
0.263887441376
0.408782692929
0
validation finish
batch start
#iterations: 160
currently lose_sum: 503.54774287343025
time_elpased: 2.554
batch start
#iterations: 161
currently lose_sum: 500.7151423096657
time_elpased: 2.555
batch start
#iterations: 162
currently lose_sum: 502.0308692753315
time_elpased: 2.563
batch start
#iterations: 163
currently lose_sum: 501.6138482391834
time_elpased: 2.544
batch start
#iterations: 164
currently lose_sum: 502.49324002861977
time_elpased: 2.567
batch start
#iterations: 165
currently lose_sum: 503.64103975892067
time_elpased: 2.561
batch start
#iterations: 166
currently lose_sum: 503.2397793531418
time_elpased: 2.553
batch start
#iterations: 167
currently lose_sum: 502.06075260043144
time_elpased: 2.574
batch start
#iterations: 168
currently lose_sum: 501.6556208729744
time_elpased: 2.573
batch start
#iterations: 169
currently lose_sum: 502.70939829945564
time_elpased: 2.562
batch start
#iterations: 170
currently lose_sum: 500.7789000570774
time_elpased: 2.598
batch start
#iterations: 171
currently lose_sum: 501.13672429323196
time_elpased: 2.576
batch start
#iterations: 172
currently lose_sum: 504.0371288359165
time_elpased: 2.582
batch start
#iterations: 173
currently lose_sum: 503.13723504543304
time_elpased: 2.59
batch start
#iterations: 174
currently lose_sum: 502.2373442351818
time_elpased: 2.58
batch start
#iterations: 175
currently lose_sum: 503.1344273984432
time_elpased: 2.595
batch start
#iterations: 176
currently lose_sum: 499.9962109327316
time_elpased: 2.593
batch start
#iterations: 177
currently lose_sum: 502.5829258263111
time_elpased: 2.591
batch start
#iterations: 178
currently lose_sum: 502.77335461974144
time_elpased: 2.594
batch start
#iterations: 179
currently lose_sum: 504.0897645652294
time_elpased: 2.594
start validation test
0.605927835052
0.907949790795
0.226159458051
0.362119315811
0
validation finish
batch start
#iterations: 180
currently lose_sum: 503.0905720591545
time_elpased: 2.583
batch start
#iterations: 181
currently lose_sum: 505.1899471282959
time_elpased: 2.592
batch start
#iterations: 182
currently lose_sum: 502.5554577410221
time_elpased: 2.596
batch start
#iterations: 183
currently lose_sum: 502.9257487654686
time_elpased: 2.567
batch start
#iterations: 184
currently lose_sum: 501.31797206401825
time_elpased: 2.596
batch start
#iterations: 185
currently lose_sum: 502.2974382340908
time_elpased: 2.576
batch start
#iterations: 186
currently lose_sum: 503.3140565752983
time_elpased: 2.571
batch start
#iterations: 187
currently lose_sum: 502.3071620762348
time_elpased: 2.593
batch start
#iterations: 188
currently lose_sum: 501.58525437116623
time_elpased: 2.582
batch start
#iterations: 189
currently lose_sum: 501.3356986641884
time_elpased: 2.58
batch start
#iterations: 190
currently lose_sum: 501.81795370578766
time_elpased: 2.583
batch start
#iterations: 191
currently lose_sum: 502.04995197057724
time_elpased: 2.579
batch start
#iterations: 192
currently lose_sum: 500.3888753950596
time_elpased: 2.571
batch start
#iterations: 193
currently lose_sum: 501.6286843419075
time_elpased: 2.573
batch start
#iterations: 194
currently lose_sum: 502.5025152564049
time_elpased: 2.561
batch start
#iterations: 195
currently lose_sum: 503.53989684581757
time_elpased: 2.577
batch start
#iterations: 196
currently lose_sum: 499.8445717096329
time_elpased: 2.575
batch start
#iterations: 197
currently lose_sum: 503.94613113999367
time_elpased: 2.557
batch start
#iterations: 198
currently lose_sum: 501.5128521025181
time_elpased: 2.587
batch start
#iterations: 199
currently lose_sum: 502.85760366916656
time_elpased: 2.577
start validation test
0.614896907216
0.920760697306
0.242209484106
0.383529994224
0
validation finish
batch start
#iterations: 200
currently lose_sum: 502.00809702277184
time_elpased: 2.572
batch start
#iterations: 201
currently lose_sum: 503.54354509711266
time_elpased: 2.573
batch start
#iterations: 202
currently lose_sum: 500.2477533519268
time_elpased: 2.576
batch start
#iterations: 203
currently lose_sum: 500.9995520412922
time_elpased: 2.581
batch start
#iterations: 204
currently lose_sum: 501.16771706938744
time_elpased: 2.577
batch start
#iterations: 205
currently lose_sum: 502.1516489684582
time_elpased: 2.565
batch start
#iterations: 206
currently lose_sum: 504.59315222501755
time_elpased: 2.571
batch start
#iterations: 207
currently lose_sum: 500.56868028640747
time_elpased: 2.593
batch start
#iterations: 208
currently lose_sum: 501.0296753644943
time_elpased: 2.575
batch start
#iterations: 209
currently lose_sum: 501.6092574298382
time_elpased: 2.582
batch start
#iterations: 210
currently lose_sum: 500.9295756518841
time_elpased: 2.584
batch start
#iterations: 211
currently lose_sum: 501.95103576779366
time_elpased: 2.58
batch start
#iterations: 212
currently lose_sum: 501.7418192625046
time_elpased: 2.584
batch start
#iterations: 213
currently lose_sum: 501.12447199225426
time_elpased: 2.574
batch start
#iterations: 214
currently lose_sum: 501.7109619677067
time_elpased: 2.58
batch start
#iterations: 215
currently lose_sum: 502.1473218202591
time_elpased: 2.584
batch start
#iterations: 216
currently lose_sum: 501.9892414212227
time_elpased: 2.565
batch start
#iterations: 217
currently lose_sum: 501.22461798787117
time_elpased: 2.56
batch start
#iterations: 218
currently lose_sum: 501.93638747930527
time_elpased: 2.581
batch start
#iterations: 219
currently lose_sum: 504.054875344038
time_elpased: 2.566
start validation test
0.602577319588
0.911392405063
0.217613340281
0.351337708228
0
validation finish
batch start
#iterations: 220
currently lose_sum: 502.4521452188492
time_elpased: 2.548
batch start
#iterations: 221
currently lose_sum: 503.2034608721733
time_elpased: 2.583
batch start
#iterations: 222
currently lose_sum: 499.237536162138
time_elpased: 2.585
batch start
#iterations: 223
currently lose_sum: 502.6195484995842
time_elpased: 2.57
batch start
#iterations: 224
currently lose_sum: 502.06690403819084
time_elpased: 2.582
batch start
#iterations: 225
currently lose_sum: 503.8720706105232
time_elpased: 2.572
batch start
#iterations: 226
currently lose_sum: 501.61375641822815
time_elpased: 2.559
batch start
#iterations: 227
currently lose_sum: 502.3700276315212
time_elpased: 2.573
batch start
#iterations: 228
currently lose_sum: 499.99511593580246
time_elpased: 2.559
batch start
#iterations: 229
currently lose_sum: 502.41905334591866
time_elpased: 2.562
batch start
#iterations: 230
currently lose_sum: 502.68749141693115
time_elpased: 2.574
batch start
#iterations: 231
currently lose_sum: 503.9529137611389
time_elpased: 2.586
batch start
#iterations: 232
currently lose_sum: 501.2343020141125
time_elpased: 2.578
batch start
#iterations: 233
currently lose_sum: 500.25479450821877
time_elpased: 2.577
batch start
#iterations: 234
currently lose_sum: 500.27849817276
time_elpased: 2.581
batch start
#iterations: 235
currently lose_sum: 500.760560631752
time_elpased: 2.579
batch start
#iterations: 236
currently lose_sum: 500.7158695459366
time_elpased: 2.571
batch start
#iterations: 237
currently lose_sum: 502.4643707573414
time_elpased: 2.521
batch start
#iterations: 238
currently lose_sum: 500.5276825428009
time_elpased: 2.567
batch start
#iterations: 239
currently lose_sum: 500.83258256316185
time_elpased: 2.517
start validation test
0.589845360825
0.91700610998
0.187701928088
0.311618652133
0
validation finish
batch start
#iterations: 240
currently lose_sum: 501.2114104628563
time_elpased: 2.526
batch start
#iterations: 241
currently lose_sum: 501.26339161396027
time_elpased: 2.56
batch start
#iterations: 242
currently lose_sum: 500.3744330108166
time_elpased: 2.591
batch start
#iterations: 243
currently lose_sum: 500.31141489744186
time_elpased: 2.603
batch start
#iterations: 244
currently lose_sum: 501.3441864848137
time_elpased: 2.587
batch start
#iterations: 245
currently lose_sum: 499.1152669787407
time_elpased: 2.571
batch start
#iterations: 246
currently lose_sum: 500.0006649196148
time_elpased: 2.557
batch start
#iterations: 247
currently lose_sum: 501.38191121816635
time_elpased: 2.562
batch start
#iterations: 248
currently lose_sum: 500.76685631275177
time_elpased: 2.566
batch start
#iterations: 249
currently lose_sum: 501.1774336695671
time_elpased: 2.585
batch start
#iterations: 250
currently lose_sum: 501.1818102300167
time_elpased: 2.588
batch start
#iterations: 251
currently lose_sum: 502.9190628528595
time_elpased: 2.558
batch start
#iterations: 252
currently lose_sum: 503.1158289015293
time_elpased: 2.58
batch start
#iterations: 253
currently lose_sum: 501.1904922425747
time_elpased: 2.579
batch start
#iterations: 254
currently lose_sum: 504.22433438897133
time_elpased: 2.563
batch start
#iterations: 255
currently lose_sum: 500.9599004983902
time_elpased: 2.587
batch start
#iterations: 256
currently lose_sum: 503.3416343033314
time_elpased: 2.58
batch start
#iterations: 257
currently lose_sum: 500.18597117066383
time_elpased: 2.573
batch start
#iterations: 258
currently lose_sum: 501.41450464725494
time_elpased: 2.581
batch start
#iterations: 259
currently lose_sum: 502.76398953795433
time_elpased: 2.569
start validation test
0.605206185567
0.917962003454
0.221573736321
0.356980941986
0
validation finish
batch start
#iterations: 260
currently lose_sum: 502.9335730075836
time_elpased: 2.566
batch start
#iterations: 261
currently lose_sum: 502.641167730093
time_elpased: 2.581
batch start
#iterations: 262
currently lose_sum: 500.64476332068443
time_elpased: 2.577
batch start
#iterations: 263
currently lose_sum: 501.0932507812977
time_elpased: 2.565
batch start
#iterations: 264
currently lose_sum: 500.98524820804596
time_elpased: 2.574
batch start
#iterations: 265
currently lose_sum: 502.7351672053337
time_elpased: 2.544
batch start
#iterations: 266
currently lose_sum: 501.3324784338474
time_elpased: 2.563
batch start
#iterations: 267
currently lose_sum: 500.2269018292427
time_elpased: 2.564
batch start
#iterations: 268
currently lose_sum: 501.1196737587452
time_elpased: 2.55
batch start
#iterations: 269
currently lose_sum: 498.7168999016285
time_elpased: 2.578
batch start
#iterations: 270
currently lose_sum: 502.0854232907295
time_elpased: 2.581
batch start
#iterations: 271
currently lose_sum: 498.75339901447296
time_elpased: 2.569
batch start
#iterations: 272
currently lose_sum: 500.5912883579731
time_elpased: 2.59
batch start
#iterations: 273
currently lose_sum: 501.56918182969093
time_elpased: 2.578
batch start
#iterations: 274
currently lose_sum: 501.9023383259773
time_elpased: 2.573
batch start
#iterations: 275
currently lose_sum: 500.45235243439674
time_elpased: 2.583
batch start
#iterations: 276
currently lose_sum: 501.27456879615784
time_elpased: 2.558
batch start
#iterations: 277
currently lose_sum: 502.13504552841187
time_elpased: 2.566
batch start
#iterations: 278
currently lose_sum: 500.66736111044884
time_elpased: 2.556
batch start
#iterations: 279
currently lose_sum: 503.0550839304924
time_elpased: 2.539
start validation test
0.615670103093
0.909299655568
0.247628973424
0.389252948886
0
validation finish
batch start
#iterations: 280
currently lose_sum: 501.17304441332817
time_elpased: 2.553
batch start
#iterations: 281
currently lose_sum: 498.4488176703453
time_elpased: 2.591
batch start
#iterations: 282
currently lose_sum: 501.52548015117645
time_elpased: 2.574
batch start
#iterations: 283
currently lose_sum: 501.2520537376404
time_elpased: 2.563
batch start
#iterations: 284
currently lose_sum: 501.08947998285294
time_elpased: 2.576
batch start
#iterations: 285
currently lose_sum: 502.9915034174919
time_elpased: 2.575
batch start
#iterations: 286
currently lose_sum: 501.4452926516533
time_elpased: 2.575
batch start
#iterations: 287
currently lose_sum: 503.4689972996712
time_elpased: 2.582
batch start
#iterations: 288
currently lose_sum: 499.2716085910797
time_elpased: 2.562
batch start
#iterations: 289
currently lose_sum: 500.05128970742226
time_elpased: 2.608
batch start
#iterations: 290
currently lose_sum: 499.3112922310829
time_elpased: 2.587
batch start
#iterations: 291
currently lose_sum: 501.43407490849495
time_elpased: 2.593
batch start
#iterations: 292
currently lose_sum: 501.42587569355965
time_elpased: 2.608
batch start
#iterations: 293
currently lose_sum: 502.0469111800194
time_elpased: 2.594
batch start
#iterations: 294
currently lose_sum: 503.83143642544746
time_elpased: 2.604
batch start
#iterations: 295
currently lose_sum: 502.15458247065544
time_elpased: 2.606
batch start
#iterations: 296
currently lose_sum: 500.65626949071884
time_elpased: 2.595
batch start
#iterations: 297
currently lose_sum: 500.9097834825516
time_elpased: 2.59
batch start
#iterations: 298
currently lose_sum: 501.64814403653145
time_elpased: 2.603
batch start
#iterations: 299
currently lose_sum: 502.21888464689255
time_elpased: 2.59
start validation test
0.605154639175
0.913284920974
0.222824387702
0.358243967828
0
validation finish
batch start
#iterations: 300
currently lose_sum: 502.28474059700966
time_elpased: 2.579
batch start
#iterations: 301
currently lose_sum: 499.69436725974083
time_elpased: 2.586
batch start
#iterations: 302
currently lose_sum: 502.36038264632225
time_elpased: 2.58
batch start
#iterations: 303
currently lose_sum: 502.3098524212837
time_elpased: 2.564
batch start
#iterations: 304
currently lose_sum: 500.01182413101196
time_elpased: 2.585
batch start
#iterations: 305
currently lose_sum: 502.53779089450836
time_elpased: 2.571
batch start
#iterations: 306
currently lose_sum: 502.6091167628765
time_elpased: 2.582
batch start
#iterations: 307
currently lose_sum: 503.10202330350876
time_elpased: 2.59
batch start
#iterations: 308
currently lose_sum: 502.2534362077713
time_elpased: 2.61
batch start
#iterations: 309
currently lose_sum: 501.7803093492985
time_elpased: 2.612
batch start
#iterations: 310
currently lose_sum: 499.75134778022766
time_elpased: 2.605
batch start
#iterations: 311
currently lose_sum: 500.05820068717003
time_elpased: 2.595
batch start
#iterations: 312
currently lose_sum: 502.83097168803215
time_elpased: 2.611
batch start
#iterations: 313
currently lose_sum: 502.92157220840454
time_elpased: 2.596
batch start
#iterations: 314
currently lose_sum: 501.07331198453903
time_elpased: 2.575
batch start
#iterations: 315
currently lose_sum: 501.4396673440933
time_elpased: 2.595
batch start
#iterations: 316
currently lose_sum: 502.40805304050446
time_elpased: 2.615
batch start
#iterations: 317
currently lose_sum: 499.7805885672569
time_elpased: 2.606
batch start
#iterations: 318
currently lose_sum: 503.16603565216064
time_elpased: 2.611
batch start
#iterations: 319
currently lose_sum: 500.65191009640694
time_elpased: 2.599
start validation test
0.615824742268
0.910659509202
0.247524752475
0.38924854544
0
validation finish
batch start
#iterations: 320
currently lose_sum: 502.1176227033138
time_elpased: 2.583
batch start
#iterations: 321
currently lose_sum: 501.10218727588654
time_elpased: 2.583
batch start
#iterations: 322
currently lose_sum: 499.8051435649395
time_elpased: 2.581
batch start
#iterations: 323
currently lose_sum: 500.93243634700775
time_elpased: 2.591
batch start
#iterations: 324
currently lose_sum: 503.21232014894485
time_elpased: 2.594
batch start
#iterations: 325
currently lose_sum: 499.7407996356487
time_elpased: 2.592
batch start
#iterations: 326
currently lose_sum: 502.37694308161736
time_elpased: 2.594
batch start
#iterations: 327
currently lose_sum: 501.60294714570045
time_elpased: 2.593
batch start
#iterations: 328
currently lose_sum: 501.391961812973
time_elpased: 2.567
batch start
#iterations: 329
currently lose_sum: 500.7207677960396
time_elpased: 2.567
batch start
#iterations: 330
currently lose_sum: 502.2039830982685
time_elpased: 2.578
batch start
#iterations: 331
currently lose_sum: 502.7756954431534
time_elpased: 2.56
batch start
#iterations: 332
currently lose_sum: 502.3599964380264
time_elpased: 2.587
batch start
#iterations: 333
currently lose_sum: 501.3875415921211
time_elpased: 2.606
batch start
#iterations: 334
currently lose_sum: 500.4492356479168
time_elpased: 2.588
batch start
#iterations: 335
currently lose_sum: 500.95873218774796
time_elpased: 2.616
batch start
#iterations: 336
currently lose_sum: 501.36641070246696
time_elpased: 2.598
batch start
#iterations: 337
currently lose_sum: 500.82724156975746
time_elpased: 2.592
batch start
#iterations: 338
currently lose_sum: 499.9596982598305
time_elpased: 2.586
batch start
#iterations: 339
currently lose_sum: 501.9615498483181
time_elpased: 2.577
start validation test
0.60912371134
0.912972085386
0.231787389265
0.369711578422
0
validation finish
batch start
#iterations: 340
currently lose_sum: 500.1321293413639
time_elpased: 2.569
batch start
#iterations: 341
currently lose_sum: 501.9458729624748
time_elpased: 2.594
batch start
#iterations: 342
currently lose_sum: 503.11206102371216
time_elpased: 2.577
batch start
#iterations: 343
currently lose_sum: 501.8884310722351
time_elpased: 2.58
batch start
#iterations: 344
currently lose_sum: 503.3247034549713
time_elpased: 2.588
batch start
#iterations: 345
currently lose_sum: 503.015000551939
time_elpased: 2.584
batch start
#iterations: 346
currently lose_sum: 501.3274896144867
time_elpased: 2.596
batch start
#iterations: 347
currently lose_sum: 501.11118826270103
time_elpased: 2.588
batch start
#iterations: 348
currently lose_sum: 503.3492269217968
time_elpased: 2.587
batch start
#iterations: 349
currently lose_sum: 501.9333906471729
time_elpased: 2.568
batch start
#iterations: 350
currently lose_sum: 501.27862653136253
time_elpased: 2.592
batch start
#iterations: 351
currently lose_sum: 501.9969443976879
time_elpased: 2.564
batch start
#iterations: 352
currently lose_sum: 500.6466534733772
time_elpased: 2.577
batch start
#iterations: 353
currently lose_sum: 502.7802204489708
time_elpased: 2.568
batch start
#iterations: 354
currently lose_sum: 501.2750453352928
time_elpased: 2.586
batch start
#iterations: 355
currently lose_sum: 501.87987250089645
time_elpased: 2.6
batch start
#iterations: 356
currently lose_sum: 499.82400816679
time_elpased: 2.576
batch start
#iterations: 357
currently lose_sum: 499.2772383391857
time_elpased: 2.562
batch start
#iterations: 358
currently lose_sum: 502.8420642912388
time_elpased: 2.58
batch start
#iterations: 359
currently lose_sum: 499.42327123880386
time_elpased: 2.588
start validation test
0.620463917526
0.908193123628
0.258780614904
0.402790169519
0
validation finish
batch start
#iterations: 360
currently lose_sum: 501.2976177930832
time_elpased: 2.577
batch start
#iterations: 361
currently lose_sum: 500.31924346089363
time_elpased: 2.594
batch start
#iterations: 362
currently lose_sum: 500.5189411342144
time_elpased: 2.576
batch start
#iterations: 363
currently lose_sum: 501.14812111854553
time_elpased: 2.577
batch start
#iterations: 364
currently lose_sum: 502.175732254982
time_elpased: 2.591
batch start
#iterations: 365
currently lose_sum: 501.7037670314312
time_elpased: 2.575
batch start
#iterations: 366
currently lose_sum: 502.31284898519516
time_elpased: 2.596
batch start
#iterations: 367
currently lose_sum: 501.95979845523834
time_elpased: 2.587
batch start
#iterations: 368
currently lose_sum: 500.9378128349781
time_elpased: 2.605
batch start
#iterations: 369
currently lose_sum: 500.1174504458904
time_elpased: 2.593
batch start
#iterations: 370
currently lose_sum: 498.9043574631214
time_elpased: 2.607
batch start
#iterations: 371
currently lose_sum: 501.3674463927746
time_elpased: 2.596
batch start
#iterations: 372
currently lose_sum: 500.0178941190243
time_elpased: 2.579
batch start
#iterations: 373
currently lose_sum: 501.1172794997692
time_elpased: 2.598
batch start
#iterations: 374
currently lose_sum: 502.8490760922432
time_elpased: 2.571
batch start
#iterations: 375
currently lose_sum: 502.5362601876259
time_elpased: 2.57
batch start
#iterations: 376
currently lose_sum: 501.922957777977
time_elpased: 2.565
batch start
#iterations: 377
currently lose_sum: 500.28686198592186
time_elpased: 2.575
batch start
#iterations: 378
currently lose_sum: 502.87025862932205
time_elpased: 2.592
batch start
#iterations: 379
currently lose_sum: 501.6301708817482
time_elpased: 2.602
start validation test
0.609432989691
0.907512116317
0.234184471079
0.372297241322
0
validation finish
batch start
#iterations: 380
currently lose_sum: 499.77167814970016
time_elpased: 2.591
batch start
#iterations: 381
currently lose_sum: 499.74513083696365
time_elpased: 2.608
batch start
#iterations: 382
currently lose_sum: 500.58276957273483
time_elpased: 2.613
batch start
#iterations: 383
currently lose_sum: 498.8620283007622
time_elpased: 2.573
batch start
#iterations: 384
currently lose_sum: 499.1990784406662
time_elpased: 2.607
batch start
#iterations: 385
currently lose_sum: 500.48195457458496
time_elpased: 2.596
batch start
#iterations: 386
currently lose_sum: 499.6981655061245
time_elpased: 2.59
batch start
#iterations: 387
currently lose_sum: 500.0921294093132
time_elpased: 2.606
batch start
#iterations: 388
currently lose_sum: 500.60734951496124
time_elpased: 2.597
batch start
#iterations: 389
currently lose_sum: 501.82725352048874
time_elpased: 2.593
batch start
#iterations: 390
currently lose_sum: 500.57275328040123
time_elpased: 2.615
batch start
#iterations: 391
currently lose_sum: 502.2623145878315
time_elpased: 2.583
batch start
#iterations: 392
currently lose_sum: 499.794371843338
time_elpased: 2.592
batch start
#iterations: 393
currently lose_sum: 499.34977000951767
time_elpased: 2.614
batch start
#iterations: 394
currently lose_sum: 499.26037606596947
time_elpased: 2.593
batch start
#iterations: 395
currently lose_sum: 500.02634850144386
time_elpased: 2.606
batch start
#iterations: 396
currently lose_sum: 501.567414611578
time_elpased: 2.608
batch start
#iterations: 397
currently lose_sum: 501.7318826317787
time_elpased: 2.582
batch start
#iterations: 398
currently lose_sum: 500.3707996606827
time_elpased: 2.599
batch start
#iterations: 399
currently lose_sum: 499.88311272859573
time_elpased: 2.584
start validation test
0.599845360825
0.919413919414
0.209275664409
0.340945750913
0
validation finish
acc: 0.618
pre: 0.917
rec: 0.265
F1: 0.411
auc: 0.000
