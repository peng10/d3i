start to construct graph
graph construct over
58300
epochs start
batch start
#iterations: 0
currently lose_sum: 404.0694497823715
time_elpased: 1.104
batch start
#iterations: 1
currently lose_sum: 399.57183796167374
time_elpased: 1.071
batch start
#iterations: 2
currently lose_sum: 397.33858120441437
time_elpased: 1.06
batch start
#iterations: 3
currently lose_sum: 396.3864061832428
time_elpased: 1.074
batch start
#iterations: 4
currently lose_sum: 395.0886592268944
time_elpased: 1.074
batch start
#iterations: 5
currently lose_sum: 394.8834518790245
time_elpased: 1.069
batch start
#iterations: 6
currently lose_sum: 394.0198165178299
time_elpased: 1.07
batch start
#iterations: 7
currently lose_sum: 393.3631081581116
time_elpased: 1.06
batch start
#iterations: 8
currently lose_sum: 392.8355153799057
time_elpased: 1.07
batch start
#iterations: 9
currently lose_sum: 391.22881841659546
time_elpased: 1.061
batch start
#iterations: 10
currently lose_sum: 392.04361867904663
time_elpased: 1.065
batch start
#iterations: 11
currently lose_sum: 391.92480105161667
time_elpased: 1.07
batch start
#iterations: 12
currently lose_sum: 390.98327082395554
time_elpased: 1.075
batch start
#iterations: 13
currently lose_sum: 390.9017571210861
time_elpased: 1.068
batch start
#iterations: 14
currently lose_sum: 391.0565250515938
time_elpased: 1.059
batch start
#iterations: 15
currently lose_sum: 390.6038074493408
time_elpased: 1.071
batch start
#iterations: 16
currently lose_sum: 389.91286128759384
time_elpased: 1.059
batch start
#iterations: 17
currently lose_sum: 390.48523765802383
time_elpased: 1.061
batch start
#iterations: 18
currently lose_sum: 389.8353399038315
time_elpased: 1.062
batch start
#iterations: 19
currently lose_sum: 389.21990352869034
time_elpased: 1.072
start validation test
0.668402061856
0.782229673093
0.47612244898
0.591944180146
0
validation finish
batch start
#iterations: 20
currently lose_sum: 389.2093078494072
time_elpased: 1.079
batch start
#iterations: 21
currently lose_sum: 388.7011309862137
time_elpased: 1.062
batch start
#iterations: 22
currently lose_sum: 389.3369582891464
time_elpased: 1.065
batch start
#iterations: 23
currently lose_sum: 389.05212837457657
time_elpased: 1.072
batch start
#iterations: 24
currently lose_sum: 389.2299542427063
time_elpased: 1.083
batch start
#iterations: 25
currently lose_sum: 388.9896962046623
time_elpased: 1.055
batch start
#iterations: 26
currently lose_sum: 389.0496438741684
time_elpased: 1.087
batch start
#iterations: 27
currently lose_sum: 388.643451154232
time_elpased: 1.072
batch start
#iterations: 28
currently lose_sum: 387.82532262802124
time_elpased: 1.069
batch start
#iterations: 29
currently lose_sum: 388.9919200539589
time_elpased: 1.072
batch start
#iterations: 30
currently lose_sum: 388.7330656647682
time_elpased: 1.075
batch start
#iterations: 31
currently lose_sum: 388.7755069732666
time_elpased: 1.066
batch start
#iterations: 32
currently lose_sum: 388.02254045009613
time_elpased: 1.064
batch start
#iterations: 33
currently lose_sum: 387.94376343488693
time_elpased: 1.062
batch start
#iterations: 34
currently lose_sum: 387.68411016464233
time_elpased: 1.077
batch start
#iterations: 35
currently lose_sum: 387.6679176688194
time_elpased: 1.075
batch start
#iterations: 36
currently lose_sum: 387.96175998449326
time_elpased: 1.073
batch start
#iterations: 37
currently lose_sum: 388.0223183631897
time_elpased: 1.104
batch start
#iterations: 38
currently lose_sum: 387.60488015413284
time_elpased: 1.071
batch start
#iterations: 39
currently lose_sum: 389.0737248659134
time_elpased: 1.07
start validation test
0.675721649485
0.780495603517
0.498163265306
0.608159451884
0
validation finish
batch start
#iterations: 40
currently lose_sum: 388.4502503871918
time_elpased: 1.06
batch start
#iterations: 41
currently lose_sum: 387.68291848897934
time_elpased: 1.066
batch start
#iterations: 42
currently lose_sum: 387.5540680885315
time_elpased: 1.074
batch start
#iterations: 43
currently lose_sum: 387.0785484313965
time_elpased: 1.075
batch start
#iterations: 44
currently lose_sum: 387.4521486759186
time_elpased: 1.073
batch start
#iterations: 45
currently lose_sum: 388.01535522937775
time_elpased: 1.068
batch start
#iterations: 46
currently lose_sum: 388.29193037748337
time_elpased: 1.08
batch start
#iterations: 47
currently lose_sum: 388.2430509328842
time_elpased: 1.072
batch start
#iterations: 48
currently lose_sum: 388.0259280204773
time_elpased: 1.069
batch start
#iterations: 49
currently lose_sum: 388.0629182457924
time_elpased: 1.078
batch start
#iterations: 50
currently lose_sum: 387.9002920985222
time_elpased: 1.081
batch start
#iterations: 51
currently lose_sum: 387.4224650859833
time_elpased: 1.061
batch start
#iterations: 52
currently lose_sum: 387.00700038671494
time_elpased: 1.063
batch start
#iterations: 53
currently lose_sum: 385.8163629770279
time_elpased: 1.079
batch start
#iterations: 54
currently lose_sum: 387.72796404361725
time_elpased: 1.07
batch start
#iterations: 55
currently lose_sum: 387.7122411131859
time_elpased: 1.079
batch start
#iterations: 56
currently lose_sum: 388.2644723057747
time_elpased: 1.069
batch start
#iterations: 57
currently lose_sum: 387.7320388555527
time_elpased: 1.066
batch start
#iterations: 58
currently lose_sum: 386.97180074453354
time_elpased: 1.059
batch start
#iterations: 59
currently lose_sum: 387.36484932899475
time_elpased: 1.058
start validation test
0.677474226804
0.767476974181
0.518673469388
0.619009925105
0
validation finish
batch start
#iterations: 60
currently lose_sum: 387.5152388215065
time_elpased: 1.07
batch start
#iterations: 61
currently lose_sum: 387.52262622117996
time_elpased: 1.068
batch start
#iterations: 62
currently lose_sum: 387.8718734383583
time_elpased: 1.073
batch start
#iterations: 63
currently lose_sum: 386.9808580875397
time_elpased: 1.075
batch start
#iterations: 64
currently lose_sum: 388.1217357516289
time_elpased: 1.076
batch start
#iterations: 65
currently lose_sum: 386.03668534755707
time_elpased: 1.075
batch start
#iterations: 66
currently lose_sum: 385.9833884835243
time_elpased: 1.059
batch start
#iterations: 67
currently lose_sum: 387.2621531486511
time_elpased: 1.067
batch start
#iterations: 68
currently lose_sum: 387.6874852180481
time_elpased: 1.061
batch start
#iterations: 69
currently lose_sum: 387.68183624744415
time_elpased: 1.071
batch start
#iterations: 70
currently lose_sum: 386.7429811358452
time_elpased: 1.062
batch start
#iterations: 71
currently lose_sum: 388.15338665246964
time_elpased: 1.082
batch start
#iterations: 72
currently lose_sum: 386.4363502860069
time_elpased: 1.074
batch start
#iterations: 73
currently lose_sum: 387.78171014785767
time_elpased: 1.071
batch start
#iterations: 74
currently lose_sum: 386.67917877435684
time_elpased: 1.061
batch start
#iterations: 75
currently lose_sum: 385.8144234418869
time_elpased: 1.078
batch start
#iterations: 76
currently lose_sum: 386.8126195073128
time_elpased: 1.072
batch start
#iterations: 77
currently lose_sum: 386.60543781518936
time_elpased: 1.063
batch start
#iterations: 78
currently lose_sum: 386.7838086485863
time_elpased: 1.082
batch start
#iterations: 79
currently lose_sum: 387.04326915740967
time_elpased: 1.071
start validation test
0.684896907216
0.759904130833
0.55
0.638134138401
0
validation finish
batch start
#iterations: 80
currently lose_sum: 386.8292282819748
time_elpased: 1.092
batch start
#iterations: 81
currently lose_sum: 386.4691373705864
time_elpased: 1.071
batch start
#iterations: 82
currently lose_sum: 386.6347177028656
time_elpased: 1.088
batch start
#iterations: 83
currently lose_sum: 386.75568199157715
time_elpased: 1.08
batch start
#iterations: 84
currently lose_sum: 386.6045828461647
time_elpased: 1.07
batch start
#iterations: 85
currently lose_sum: 386.74481934309006
time_elpased: 1.075
batch start
#iterations: 86
currently lose_sum: 386.6893364787102
time_elpased: 1.066
batch start
#iterations: 87
currently lose_sum: 386.71802693605423
time_elpased: 1.073
batch start
#iterations: 88
currently lose_sum: 385.86219680309296
time_elpased: 1.064
batch start
#iterations: 89
currently lose_sum: 386.2278613448143
time_elpased: 1.069
batch start
#iterations: 90
currently lose_sum: 385.66067254543304
time_elpased: 1.061
batch start
#iterations: 91
currently lose_sum: 385.9031889438629
time_elpased: 1.063
batch start
#iterations: 92
currently lose_sum: 387.07611471414566
time_elpased: 1.078
batch start
#iterations: 93
currently lose_sum: 386.3815687298775
time_elpased: 1.073
batch start
#iterations: 94
currently lose_sum: 385.9196975827217
time_elpased: 1.079
batch start
#iterations: 95
currently lose_sum: 386.3067966103554
time_elpased: 1.076
batch start
#iterations: 96
currently lose_sum: 386.0719428062439
time_elpased: 1.081
batch start
#iterations: 97
currently lose_sum: 386.6008247733116
time_elpased: 1.082
batch start
#iterations: 98
currently lose_sum: 386.69454622268677
time_elpased: 1.064
batch start
#iterations: 99
currently lose_sum: 386.58253812789917
time_elpased: 1.065
start validation test
0.680670103093
0.784800126402
0.506836734694
0.615909231818
0
validation finish
batch start
#iterations: 100
currently lose_sum: 387.1644737124443
time_elpased: 1.069
batch start
#iterations: 101
currently lose_sum: 386.47244560718536
time_elpased: 1.065
batch start
#iterations: 102
currently lose_sum: 386.7866415977478
time_elpased: 1.065
batch start
#iterations: 103
currently lose_sum: 386.6175408959389
time_elpased: 1.07
batch start
#iterations: 104
currently lose_sum: 386.3124706149101
time_elpased: 1.065
batch start
#iterations: 105
currently lose_sum: 386.6996386051178
time_elpased: 1.067
batch start
#iterations: 106
currently lose_sum: 388.2559106349945
time_elpased: 1.068
batch start
#iterations: 107
currently lose_sum: 387.26126658916473
time_elpased: 1.066
batch start
#iterations: 108
currently lose_sum: 385.99160093069077
time_elpased: 1.065
batch start
#iterations: 109
currently lose_sum: 385.8142955303192
time_elpased: 1.065
batch start
#iterations: 110
currently lose_sum: 386.8814055919647
time_elpased: 1.067
batch start
#iterations: 111
currently lose_sum: 385.2392555475235
time_elpased: 1.067
batch start
#iterations: 112
currently lose_sum: 385.1416898369789
time_elpased: 1.062
batch start
#iterations: 113
currently lose_sum: 386.1098464727402
time_elpased: 1.068
batch start
#iterations: 114
currently lose_sum: 386.25308126211166
time_elpased: 1.069
batch start
#iterations: 115
currently lose_sum: 386.82531303167343
time_elpased: 1.074
batch start
#iterations: 116
currently lose_sum: 386.7250508069992
time_elpased: 1.074
batch start
#iterations: 117
currently lose_sum: 386.9942305088043
time_elpased: 1.064
batch start
#iterations: 118
currently lose_sum: 386.20683562755585
time_elpased: 1.066
batch start
#iterations: 119
currently lose_sum: 385.64133936166763
time_elpased: 1.068
start validation test
0.680412371134
0.782840980515
0.508367346939
0.616431576342
0
validation finish
batch start
#iterations: 120
currently lose_sum: 385.5615267753601
time_elpased: 1.071
batch start
#iterations: 121
currently lose_sum: 385.5295960903168
time_elpased: 1.065
batch start
#iterations: 122
currently lose_sum: 385.45378267765045
time_elpased: 1.071
batch start
#iterations: 123
currently lose_sum: 385.36307066679
time_elpased: 1.067
batch start
#iterations: 124
currently lose_sum: 386.6422879099846
time_elpased: 1.063
batch start
#iterations: 125
currently lose_sum: 385.2400096654892
time_elpased: 1.069
batch start
#iterations: 126
currently lose_sum: 385.60610979795456
time_elpased: 1.069
batch start
#iterations: 127
currently lose_sum: 384.97910463809967
time_elpased: 1.067
batch start
#iterations: 128
currently lose_sum: 386.22181445360184
time_elpased: 1.088
batch start
#iterations: 129
currently lose_sum: 385.87716341018677
time_elpased: 1.07
batch start
#iterations: 130
currently lose_sum: 385.4955238699913
time_elpased: 1.067
batch start
#iterations: 131
currently lose_sum: 386.91078370809555
time_elpased: 1.077
batch start
#iterations: 132
currently lose_sum: 385.53341138362885
time_elpased: 1.083
batch start
#iterations: 133
currently lose_sum: 385.8209697008133
time_elpased: 1.064
batch start
#iterations: 134
currently lose_sum: 385.55070143938065
time_elpased: 1.066
batch start
#iterations: 135
currently lose_sum: 386.3642696738243
time_elpased: 1.072
batch start
#iterations: 136
currently lose_sum: 385.95271241664886
time_elpased: 1.062
batch start
#iterations: 137
currently lose_sum: 385.22769367694855
time_elpased: 1.072
batch start
#iterations: 138
currently lose_sum: 386.6950865983963
time_elpased: 1.067
batch start
#iterations: 139
currently lose_sum: 386.1420726776123
time_elpased: 1.067
start validation test
0.682010309278
0.772474861174
0.525204081633
0.625280933001
0
validation finish
batch start
#iterations: 140
currently lose_sum: 385.3718937635422
time_elpased: 1.07
batch start
#iterations: 141
currently lose_sum: 386.3255413174629
time_elpased: 1.068
batch start
#iterations: 142
currently lose_sum: 385.85741525888443
time_elpased: 1.064
batch start
#iterations: 143
currently lose_sum: 386.4485709667206
time_elpased: 1.065
batch start
#iterations: 144
currently lose_sum: 385.8562926054001
time_elpased: 1.071
batch start
#iterations: 145
currently lose_sum: 386.4746326804161
time_elpased: 1.07
batch start
#iterations: 146
currently lose_sum: 385.58169108629227
time_elpased: 1.081
batch start
#iterations: 147
currently lose_sum: 385.7721827030182
time_elpased: 1.074
batch start
#iterations: 148
currently lose_sum: 386.09385842084885
time_elpased: 1.069
batch start
#iterations: 149
currently lose_sum: 385.78445559740067
time_elpased: 1.066
batch start
#iterations: 150
currently lose_sum: 385.32954865694046
time_elpased: 1.071
batch start
#iterations: 151
currently lose_sum: 385.61448603868484
time_elpased: 1.07
batch start
#iterations: 152
currently lose_sum: 385.6519521474838
time_elpased: 1.067
batch start
#iterations: 153
currently lose_sum: 386.1146704554558
time_elpased: 1.076
batch start
#iterations: 154
currently lose_sum: 385.3900642991066
time_elpased: 1.069
batch start
#iterations: 155
currently lose_sum: 386.3108769059181
time_elpased: 1.075
batch start
#iterations: 156
currently lose_sum: 386.6482225060463
time_elpased: 1.063
batch start
#iterations: 157
currently lose_sum: 385.7059965133667
time_elpased: 1.079
batch start
#iterations: 158
currently lose_sum: 385.3369872570038
time_elpased: 1.069
batch start
#iterations: 159
currently lose_sum: 386.3519295454025
time_elpased: 1.077
start validation test
0.684896907216
0.766209386282
0.541428571429
0.634499252616
0
validation finish
batch start
#iterations: 160
currently lose_sum: 386.1783127784729
time_elpased: 1.078
batch start
#iterations: 161
currently lose_sum: 386.44547814130783
time_elpased: 1.073
batch start
#iterations: 162
currently lose_sum: 385.75452399253845
time_elpased: 1.062
batch start
#iterations: 163
currently lose_sum: 385.3585293292999
time_elpased: 1.068
batch start
#iterations: 164
currently lose_sum: 386.3959296941757
time_elpased: 1.062
batch start
#iterations: 165
currently lose_sum: 386.0465966463089
time_elpased: 1.067
batch start
#iterations: 166
currently lose_sum: 386.1796033978462
time_elpased: 1.064
batch start
#iterations: 167
currently lose_sum: 385.09498792886734
time_elpased: 1.065
batch start
#iterations: 168
currently lose_sum: 386.0118818283081
time_elpased: 1.068
batch start
#iterations: 169
currently lose_sum: 385.7422788143158
time_elpased: 1.063
batch start
#iterations: 170
currently lose_sum: 385.0855800509453
time_elpased: 1.064
batch start
#iterations: 171
currently lose_sum: 385.52871966362
time_elpased: 1.068
batch start
#iterations: 172
currently lose_sum: 385.84605491161346
time_elpased: 1.071
batch start
#iterations: 173
currently lose_sum: 384.5148064494133
time_elpased: 1.067
batch start
#iterations: 174
currently lose_sum: 385.9742978811264
time_elpased: 1.071
batch start
#iterations: 175
currently lose_sum: 385.18248319625854
time_elpased: 1.069
batch start
#iterations: 176
currently lose_sum: 385.6237143278122
time_elpased: 1.058
batch start
#iterations: 177
currently lose_sum: 385.9122081398964
time_elpased: 1.07
batch start
#iterations: 178
currently lose_sum: 385.1554154753685
time_elpased: 1.069
batch start
#iterations: 179
currently lose_sum: 386.2855796813965
time_elpased: 1.064
start validation test
0.683453608247
0.7815048469
0.518265306122
0.623228418921
0
validation finish
batch start
#iterations: 180
currently lose_sum: 386.46753990650177
time_elpased: 1.069
batch start
#iterations: 181
currently lose_sum: 386.4933434724808
time_elpased: 1.076
batch start
#iterations: 182
currently lose_sum: 385.27872866392136
time_elpased: 1.066
batch start
#iterations: 183
currently lose_sum: 384.80323618650436
time_elpased: 1.081
batch start
#iterations: 184
currently lose_sum: 384.9572312235832
time_elpased: 1.066
batch start
#iterations: 185
currently lose_sum: 386.19490456581116
time_elpased: 1.073
batch start
#iterations: 186
currently lose_sum: 385.17119735479355
time_elpased: 1.067
batch start
#iterations: 187
currently lose_sum: 385.481077671051
time_elpased: 1.072
batch start
#iterations: 188
currently lose_sum: 386.1060318350792
time_elpased: 1.058
batch start
#iterations: 189
currently lose_sum: 385.6741734147072
time_elpased: 1.067
batch start
#iterations: 190
currently lose_sum: 384.1599724292755
time_elpased: 1.063
batch start
#iterations: 191
currently lose_sum: 386.0249554514885
time_elpased: 1.073
batch start
#iterations: 192
currently lose_sum: 386.61887019872665
time_elpased: 1.066
batch start
#iterations: 193
currently lose_sum: 385.1756646037102
time_elpased: 1.074
batch start
#iterations: 194
currently lose_sum: 385.4147106409073
time_elpased: 1.075
batch start
#iterations: 195
currently lose_sum: 385.8086991906166
time_elpased: 1.068
batch start
#iterations: 196
currently lose_sum: 387.0324539542198
time_elpased: 1.07
batch start
#iterations: 197
currently lose_sum: 386.2479084134102
time_elpased: 1.071
batch start
#iterations: 198
currently lose_sum: 385.99148029088974
time_elpased: 1.072
batch start
#iterations: 199
currently lose_sum: 385.5949627161026
time_elpased: 1.067
start validation test
0.679639175258
0.786845895343
0.501734693878
0.612748457848
0
validation finish
batch start
#iterations: 200
currently lose_sum: 384.89403891563416
time_elpased: 1.073
batch start
#iterations: 201
currently lose_sum: 384.34760278463364
time_elpased: 1.067
batch start
#iterations: 202
currently lose_sum: 385.1110814213753
time_elpased: 1.064
batch start
#iterations: 203
currently lose_sum: 385.9417029619217
time_elpased: 1.071
batch start
#iterations: 204
currently lose_sum: 385.38092601299286
time_elpased: 1.069
batch start
#iterations: 205
currently lose_sum: 385.60198855400085
time_elpased: 1.068
batch start
#iterations: 206
currently lose_sum: 385.5396377444267
time_elpased: 1.068
batch start
#iterations: 207
currently lose_sum: 385.58357524871826
time_elpased: 1.072
batch start
#iterations: 208
currently lose_sum: 385.1709359884262
time_elpased: 1.064
batch start
#iterations: 209
currently lose_sum: 385.1284590959549
time_elpased: 1.07
batch start
#iterations: 210
currently lose_sum: 384.8405438065529
time_elpased: 1.073
batch start
#iterations: 211
currently lose_sum: 384.86769610643387
time_elpased: 1.056
batch start
#iterations: 212
currently lose_sum: 385.64544528722763
time_elpased: 1.081
batch start
#iterations: 213
currently lose_sum: 384.67418360710144
time_elpased: 1.069
batch start
#iterations: 214
currently lose_sum: 385.7327479124069
time_elpased: 1.075
batch start
#iterations: 215
currently lose_sum: 386.1395993232727
time_elpased: 1.07
batch start
#iterations: 216
currently lose_sum: 385.4390016198158
time_elpased: 1.057
batch start
#iterations: 217
currently lose_sum: 385.23819130659103
time_elpased: 1.064
batch start
#iterations: 218
currently lose_sum: 384.852297604084
time_elpased: 1.064
batch start
#iterations: 219
currently lose_sum: 386.68556386232376
time_elpased: 1.067
start validation test
0.682835051546
0.778014941302
0.520714285714
0.623876765083
0
validation finish
batch start
#iterations: 220
currently lose_sum: 384.0594853758812
time_elpased: 1.079
batch start
#iterations: 221
currently lose_sum: 385.16915994882584
time_elpased: 1.087
batch start
#iterations: 222
currently lose_sum: 384.6730380654335
time_elpased: 1.067
batch start
#iterations: 223
currently lose_sum: 385.3438946008682
time_elpased: 1.069
batch start
#iterations: 224
currently lose_sum: 385.21423268318176
time_elpased: 1.068
batch start
#iterations: 225
currently lose_sum: 385.37917417287827
time_elpased: 1.064
batch start
#iterations: 226
currently lose_sum: 385.89683723449707
time_elpased: 1.069
batch start
#iterations: 227
currently lose_sum: 385.0150762796402
time_elpased: 1.074
batch start
#iterations: 228
currently lose_sum: 385.42856162786484
time_elpased: 1.066
batch start
#iterations: 229
currently lose_sum: 384.97330844402313
time_elpased: 1.068
batch start
#iterations: 230
currently lose_sum: 385.55251109600067
time_elpased: 1.065
batch start
#iterations: 231
currently lose_sum: 384.9655295610428
time_elpased: 1.07
batch start
#iterations: 232
currently lose_sum: 385.6967376470566
time_elpased: 1.065
batch start
#iterations: 233
currently lose_sum: 386.2197700738907
time_elpased: 1.079
batch start
#iterations: 234
currently lose_sum: 385.17549455165863
time_elpased: 1.073
batch start
#iterations: 235
currently lose_sum: 385.41382598876953
time_elpased: 1.067
batch start
#iterations: 236
currently lose_sum: 385.2503761649132
time_elpased: 1.073
batch start
#iterations: 237
currently lose_sum: 385.4998833537102
time_elpased: 1.068
batch start
#iterations: 238
currently lose_sum: 384.2288876771927
time_elpased: 1.061
batch start
#iterations: 239
currently lose_sum: 384.9080368876457
time_elpased: 1.068
start validation test
0.682783505155
0.78020288964
0.517959183673
0.622592910585
0
validation finish
batch start
#iterations: 240
currently lose_sum: 383.8747740983963
time_elpased: 1.066
batch start
#iterations: 241
currently lose_sum: 385.3245906829834
time_elpased: 1.066
batch start
#iterations: 242
currently lose_sum: 386.00795847177505
time_elpased: 1.081
batch start
#iterations: 243
currently lose_sum: 385.5782537460327
time_elpased: 1.063
batch start
#iterations: 244
currently lose_sum: 385.3571507334709
time_elpased: 1.071
batch start
#iterations: 245
currently lose_sum: 385.50796073675156
time_elpased: 1.067
batch start
#iterations: 246
currently lose_sum: 385.0600306391716
time_elpased: 1.069
batch start
#iterations: 247
currently lose_sum: 385.2374978065491
time_elpased: 1.073
batch start
#iterations: 248
currently lose_sum: 386.07091414928436
time_elpased: 1.072
batch start
#iterations: 249
currently lose_sum: 385.6575480103493
time_elpased: 1.07
batch start
#iterations: 250
currently lose_sum: 386.15255457162857
time_elpased: 1.076
batch start
#iterations: 251
currently lose_sum: 385.46896547079086
time_elpased: 1.07
batch start
#iterations: 252
currently lose_sum: 385.0852361321449
time_elpased: 1.075
batch start
#iterations: 253
currently lose_sum: 385.1447997689247
time_elpased: 1.068
batch start
#iterations: 254
currently lose_sum: 385.6784831881523
time_elpased: 1.06
batch start
#iterations: 255
currently lose_sum: 385.4113048315048
time_elpased: 1.08
batch start
#iterations: 256
currently lose_sum: 385.4451473355293
time_elpased: 1.077
batch start
#iterations: 257
currently lose_sum: 386.30934512615204
time_elpased: 1.071
batch start
#iterations: 258
currently lose_sum: 384.7835472226143
time_elpased: 1.07
batch start
#iterations: 259
currently lose_sum: 385.48690539598465
time_elpased: 1.097
start validation test
0.682783505155
0.776379624015
0.522551020408
0.62466455233
0
validation finish
batch start
#iterations: 260
currently lose_sum: 385.0944907069206
time_elpased: 1.076
batch start
#iterations: 261
currently lose_sum: 385.67148727178574
time_elpased: 1.067
batch start
#iterations: 262
currently lose_sum: 385.7805238366127
time_elpased: 1.063
batch start
#iterations: 263
currently lose_sum: 385.1053962111473
time_elpased: 1.072
batch start
#iterations: 264
currently lose_sum: 385.6130433678627
time_elpased: 1.073
batch start
#iterations: 265
currently lose_sum: 385.83128786087036
time_elpased: 1.069
batch start
#iterations: 266
currently lose_sum: 385.195852458477
time_elpased: 1.07
batch start
#iterations: 267
currently lose_sum: 385.5787256360054
time_elpased: 1.066
batch start
#iterations: 268
currently lose_sum: 384.9856855273247
time_elpased: 1.077
batch start
#iterations: 269
currently lose_sum: 385.3106692433357
time_elpased: 1.075
batch start
#iterations: 270
currently lose_sum: 385.6108115911484
time_elpased: 1.069
batch start
#iterations: 271
currently lose_sum: 385.39457845687866
time_elpased: 1.068
batch start
#iterations: 272
currently lose_sum: 385.68691939115524
time_elpased: 1.064
batch start
#iterations: 273
currently lose_sum: 386.0136797428131
time_elpased: 1.068
batch start
#iterations: 274
currently lose_sum: 385.2678160071373
time_elpased: 1.07
batch start
#iterations: 275
currently lose_sum: 385.7456553578377
time_elpased: 1.07
batch start
#iterations: 276
currently lose_sum: 385.2018508911133
time_elpased: 1.066
batch start
#iterations: 277
currently lose_sum: 385.73424780368805
time_elpased: 1.081
batch start
#iterations: 278
currently lose_sum: 383.9155613183975
time_elpased: 1.082
batch start
#iterations: 279
currently lose_sum: 385.6493517756462
time_elpased: 1.059
start validation test
0.681134020619
0.778600061671
0.515306122449
0.620164558517
0
validation finish
batch start
#iterations: 280
currently lose_sum: 385.292702794075
time_elpased: 1.08
batch start
#iterations: 281
currently lose_sum: 385.6239753961563
time_elpased: 1.078
batch start
#iterations: 282
currently lose_sum: 384.853471159935
time_elpased: 1.08
batch start
#iterations: 283
currently lose_sum: 386.1004111766815
time_elpased: 1.067
batch start
#iterations: 284
currently lose_sum: 385.6134594678879
time_elpased: 1.079
batch start
#iterations: 285
currently lose_sum: 385.22571551799774
time_elpased: 1.087
batch start
#iterations: 286
currently lose_sum: 384.7739714384079
time_elpased: 1.069
batch start
#iterations: 287
currently lose_sum: 384.8529513478279
time_elpased: 1.071
batch start
#iterations: 288
currently lose_sum: 385.08108299970627
time_elpased: 1.063
batch start
#iterations: 289
currently lose_sum: 385.57889729738235
time_elpased: 1.074
batch start
#iterations: 290
currently lose_sum: 386.14031594991684
time_elpased: 1.068
batch start
#iterations: 291
currently lose_sum: 385.0659999847412
time_elpased: 1.064
batch start
#iterations: 292
currently lose_sum: 385.5306153893471
time_elpased: 1.074
batch start
#iterations: 293
currently lose_sum: 386.01999628543854
time_elpased: 1.071
batch start
#iterations: 294
currently lose_sum: 385.5006645321846
time_elpased: 1.072
batch start
#iterations: 295
currently lose_sum: 383.91035813093185
time_elpased: 1.07
batch start
#iterations: 296
currently lose_sum: 384.68515932559967
time_elpased: 1.091
batch start
#iterations: 297
currently lose_sum: 385.8231827020645
time_elpased: 1.089
batch start
#iterations: 298
currently lose_sum: 385.9965441226959
time_elpased: 1.074
batch start
#iterations: 299
currently lose_sum: 385.37577414512634
time_elpased: 1.068
start validation test
0.683865979381
0.777508702891
0.524183673469
0.626196135796
0
validation finish
batch start
#iterations: 300
currently lose_sum: 385.6192402243614
time_elpased: 1.085
batch start
#iterations: 301
currently lose_sum: 384.5592781305313
time_elpased: 1.073
batch start
#iterations: 302
currently lose_sum: 385.3818671107292
time_elpased: 1.079
batch start
#iterations: 303
currently lose_sum: 384.66428792476654
time_elpased: 1.073
batch start
#iterations: 304
currently lose_sum: 385.8371140360832
time_elpased: 1.076
batch start
#iterations: 305
currently lose_sum: 384.74335092306137
time_elpased: 1.089
batch start
#iterations: 306
currently lose_sum: 384.8131665587425
time_elpased: 1.062
batch start
#iterations: 307
currently lose_sum: 385.05539816617966
time_elpased: 1.073
batch start
#iterations: 308
currently lose_sum: 385.3367475271225
time_elpased: 1.072
batch start
#iterations: 309
currently lose_sum: 385.06519055366516
time_elpased: 1.076
batch start
#iterations: 310
currently lose_sum: 386.20636785030365
time_elpased: 1.068
batch start
#iterations: 311
currently lose_sum: 384.4977452158928
time_elpased: 1.071
batch start
#iterations: 312
currently lose_sum: 384.2811693549156
time_elpased: 1.089
batch start
#iterations: 313
currently lose_sum: 385.5584453344345
time_elpased: 1.068
batch start
#iterations: 314
currently lose_sum: 384.120287835598
time_elpased: 1.079
batch start
#iterations: 315
currently lose_sum: 384.68505132198334
time_elpased: 1.071
batch start
#iterations: 316
currently lose_sum: 385.1826751232147
time_elpased: 1.071
batch start
#iterations: 317
currently lose_sum: 384.94947642087936
time_elpased: 1.071
batch start
#iterations: 318
currently lose_sum: 386.1941941976547
time_elpased: 1.074
batch start
#iterations: 319
currently lose_sum: 385.6647033691406
time_elpased: 1.067
start validation test
0.684072164948
0.765821868211
0.539591836735
0.633103861119
0
validation finish
batch start
#iterations: 320
currently lose_sum: 385.1234355568886
time_elpased: 1.066
batch start
#iterations: 321
currently lose_sum: 386.05934351682663
time_elpased: 1.069
batch start
#iterations: 322
currently lose_sum: 385.4062615633011
time_elpased: 1.065
batch start
#iterations: 323
currently lose_sum: 384.72850972414017
time_elpased: 1.068
batch start
#iterations: 324
currently lose_sum: 384.78339886665344
time_elpased: 1.064
batch start
#iterations: 325
currently lose_sum: 384.9985933303833
time_elpased: 1.072
batch start
#iterations: 326
currently lose_sum: 385.74438321590424
time_elpased: 1.08
batch start
#iterations: 327
currently lose_sum: 386.1096891760826
time_elpased: 1.068
batch start
#iterations: 328
currently lose_sum: 385.03427267074585
time_elpased: 1.08
batch start
#iterations: 329
currently lose_sum: 385.2675107717514
time_elpased: 1.073
batch start
#iterations: 330
currently lose_sum: 385.5473412871361
time_elpased: 1.077
batch start
#iterations: 331
currently lose_sum: 385.6353688836098
time_elpased: 1.066
batch start
#iterations: 332
currently lose_sum: 385.5761952996254
time_elpased: 1.072
batch start
#iterations: 333
currently lose_sum: 385.2507554888725
time_elpased: 1.079
batch start
#iterations: 334
currently lose_sum: 385.90407359600067
time_elpased: 1.072
batch start
#iterations: 335
currently lose_sum: 384.50251442193985
time_elpased: 1.077
batch start
#iterations: 336
currently lose_sum: 383.7710956931114
time_elpased: 1.087
batch start
#iterations: 337
currently lose_sum: 384.56506752967834
time_elpased: 1.066
batch start
#iterations: 338
currently lose_sum: 385.4202806353569
time_elpased: 1.064
batch start
#iterations: 339
currently lose_sum: 384.83307230472565
time_elpased: 1.067
start validation test
0.683762886598
0.773385051469
0.528979591837
0.628249409198
0
validation finish
batch start
#iterations: 340
currently lose_sum: 385.12520176172256
time_elpased: 1.084
batch start
#iterations: 341
currently lose_sum: 384.9102872610092
time_elpased: 1.073
batch start
#iterations: 342
currently lose_sum: 385.40355998277664
time_elpased: 1.083
batch start
#iterations: 343
currently lose_sum: 385.9608985185623
time_elpased: 1.065
batch start
#iterations: 344
currently lose_sum: 384.50811779499054
time_elpased: 1.068
batch start
#iterations: 345
currently lose_sum: 385.6333988904953
time_elpased: 1.066
batch start
#iterations: 346
currently lose_sum: 384.54074388742447
time_elpased: 1.075
batch start
#iterations: 347
currently lose_sum: 383.4059892296791
time_elpased: 1.085
batch start
#iterations: 348
currently lose_sum: 385.91645896434784
time_elpased: 1.07
batch start
#iterations: 349
currently lose_sum: 385.06147080659866
time_elpased: 1.068
batch start
#iterations: 350
currently lose_sum: 385.2761725783348
time_elpased: 1.066
batch start
#iterations: 351
currently lose_sum: 384.16033631563187
time_elpased: 1.075
batch start
#iterations: 352
currently lose_sum: 383.8844293355942
time_elpased: 1.069
batch start
#iterations: 353
currently lose_sum: 384.6124464869499
time_elpased: 1.079
batch start
#iterations: 354
currently lose_sum: 383.7327373623848
time_elpased: 1.06
batch start
#iterations: 355
currently lose_sum: 384.7068645954132
time_elpased: 1.076
batch start
#iterations: 356
currently lose_sum: 384.590946495533
time_elpased: 1.089
batch start
#iterations: 357
currently lose_sum: 385.82082241773605
time_elpased: 1.068
batch start
#iterations: 358
currently lose_sum: 385.9681199193001
time_elpased: 1.073
batch start
#iterations: 359
currently lose_sum: 386.20500642061234
time_elpased: 1.071
start validation test
0.683402061856
0.780607548328
0.519183673469
0.623605834048
0
validation finish
batch start
#iterations: 360
currently lose_sum: 384.94619077444077
time_elpased: 1.085
batch start
#iterations: 361
currently lose_sum: 384.41283625364304
time_elpased: 1.066
batch start
#iterations: 362
currently lose_sum: 386.01536124944687
time_elpased: 1.069
batch start
#iterations: 363
currently lose_sum: 384.8772340416908
time_elpased: 1.087
batch start
#iterations: 364
currently lose_sum: 384.813580930233
time_elpased: 1.071
batch start
#iterations: 365
currently lose_sum: 385.2477224469185
time_elpased: 1.067
batch start
#iterations: 366
currently lose_sum: 385.02442467212677
time_elpased: 1.08
batch start
#iterations: 367
currently lose_sum: 384.6161233186722
time_elpased: 1.072
batch start
#iterations: 368
currently lose_sum: 384.77508050203323
time_elpased: 1.076
batch start
#iterations: 369
currently lose_sum: 385.06109219789505
time_elpased: 1.082
batch start
#iterations: 370
currently lose_sum: 384.8856218457222
time_elpased: 1.084
batch start
#iterations: 371
currently lose_sum: 384.904444873333
time_elpased: 1.088
batch start
#iterations: 372
currently lose_sum: 384.2798154950142
time_elpased: 1.073
batch start
#iterations: 373
currently lose_sum: 385.66785460710526
time_elpased: 1.086
batch start
#iterations: 374
currently lose_sum: 384.9479569196701
time_elpased: 1.067
batch start
#iterations: 375
currently lose_sum: 384.4617620706558
time_elpased: 1.075
batch start
#iterations: 376
currently lose_sum: 384.39753663539886
time_elpased: 1.069
batch start
#iterations: 377
currently lose_sum: 386.38824105262756
time_elpased: 1.085
batch start
#iterations: 378
currently lose_sum: 384.76385974884033
time_elpased: 1.083
batch start
#iterations: 379
currently lose_sum: 385.27596002817154
time_elpased: 1.072
start validation test
0.683453608247
0.774000299536
0.527346938776
0.627298658736
0
validation finish
batch start
#iterations: 380
currently lose_sum: 384.8686491250992
time_elpased: 1.062
batch start
#iterations: 381
currently lose_sum: 384.14953446388245
time_elpased: 1.08
batch start
#iterations: 382
currently lose_sum: 384.1299403309822
time_elpased: 1.071
batch start
#iterations: 383
currently lose_sum: 384.4532638192177
time_elpased: 1.052
batch start
#iterations: 384
currently lose_sum: 385.4333545565605
time_elpased: 1.071
batch start
#iterations: 385
currently lose_sum: 385.126405775547
time_elpased: 1.061
batch start
#iterations: 386
currently lose_sum: 385.06618535518646
time_elpased: 1.058
batch start
#iterations: 387
currently lose_sum: 385.35779941082
time_elpased: 1.068
batch start
#iterations: 388
currently lose_sum: 384.32398253679276
time_elpased: 1.071
batch start
#iterations: 389
currently lose_sum: 385.54501938819885
time_elpased: 1.067
batch start
#iterations: 390
currently lose_sum: 386.4870859384537
time_elpased: 1.082
batch start
#iterations: 391
currently lose_sum: 385.3406311869621
time_elpased: 1.078
batch start
#iterations: 392
currently lose_sum: 383.7862318754196
time_elpased: 1.073
batch start
#iterations: 393
currently lose_sum: 384.4413868188858
time_elpased: 1.078
batch start
#iterations: 394
currently lose_sum: 385.5630776286125
time_elpased: 1.109
batch start
#iterations: 395
currently lose_sum: 384.9649756550789
time_elpased: 1.083
batch start
#iterations: 396
currently lose_sum: 383.9555433988571
time_elpased: 1.077
batch start
#iterations: 397
currently lose_sum: 385.5674589276314
time_elpased: 1.072
batch start
#iterations: 398
currently lose_sum: 384.0404667854309
time_elpased: 1.063
batch start
#iterations: 399
currently lose_sum: 384.4198752641678
time_elpased: 1.088
start validation test
0.684175257732
0.779230652273
0.522959183673
0.625877755389
0
validation finish
acc: 0.688
pre: 0.758
rec: 0.542
F1: 0.632
auc: 0.000
