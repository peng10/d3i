start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 572.9115016460419
time_elpased: 1.664
batch start
#iterations: 1
currently lose_sum: 551.8454815149307
time_elpased: 1.619
batch start
#iterations: 2
currently lose_sum: 546.9820500016212
time_elpased: 1.638
batch start
#iterations: 3
currently lose_sum: 544.4775069355965
time_elpased: 1.621
batch start
#iterations: 4
currently lose_sum: 542.5701757073402
time_elpased: 1.624
batch start
#iterations: 5
currently lose_sum: 543.6178483366966
time_elpased: 1.616
batch start
#iterations: 6
currently lose_sum: 540.5863400101662
time_elpased: 1.654
batch start
#iterations: 7
currently lose_sum: 541.7893988490105
time_elpased: 1.626
batch start
#iterations: 8
currently lose_sum: 539.0641811490059
time_elpased: 1.611
batch start
#iterations: 9
currently lose_sum: 540.4744396209717
time_elpased: 1.621
batch start
#iterations: 10
currently lose_sum: 539.9445666670799
time_elpased: 1.617
batch start
#iterations: 11
currently lose_sum: 541.2173156738281
time_elpased: 1.627
batch start
#iterations: 12
currently lose_sum: 539.7068530917168
time_elpased: 1.627
batch start
#iterations: 13
currently lose_sum: 537.4442417323589
time_elpased: 1.626
batch start
#iterations: 14
currently lose_sum: 537.9964804053307
time_elpased: 1.637
batch start
#iterations: 15
currently lose_sum: 538.5326367020607
time_elpased: 1.609
batch start
#iterations: 16
currently lose_sum: 538.5731638073921
time_elpased: 1.626
batch start
#iterations: 17
currently lose_sum: 537.3338729143143
time_elpased: 1.645
batch start
#iterations: 18
currently lose_sum: 538.0181976854801
time_elpased: 1.621
batch start
#iterations: 19
currently lose_sum: 538.099967956543
time_elpased: 1.617
start validation test
0.529896907216
0.875683060109
0.0662874870734
0.123245529706
0
validation finish
batch start
#iterations: 20
currently lose_sum: 537.8714990913868
time_elpased: 1.636
batch start
#iterations: 21
currently lose_sum: 539.630871295929
time_elpased: 1.622
batch start
#iterations: 22
currently lose_sum: 536.7922849059105
time_elpased: 1.624
batch start
#iterations: 23
currently lose_sum: 537.6872026622295
time_elpased: 1.623
batch start
#iterations: 24
currently lose_sum: 535.2197642922401
time_elpased: 1.619
batch start
#iterations: 25
currently lose_sum: 537.3252995610237
time_elpased: 1.626
batch start
#iterations: 26
currently lose_sum: 536.8275290727615
time_elpased: 1.619
batch start
#iterations: 27
currently lose_sum: 536.2486877441406
time_elpased: 1.624
batch start
#iterations: 28
currently lose_sum: 536.3233040869236
time_elpased: 1.629
batch start
#iterations: 29
currently lose_sum: 536.483833193779
time_elpased: 1.628
batch start
#iterations: 30
currently lose_sum: 535.3244470953941
time_elpased: 1.628
batch start
#iterations: 31
currently lose_sum: 535.4481714963913
time_elpased: 1.628
batch start
#iterations: 32
currently lose_sum: 537.4868002533913
time_elpased: 1.627
batch start
#iterations: 33
currently lose_sum: 535.9725994467735
time_elpased: 1.629
batch start
#iterations: 34
currently lose_sum: 535.8208890557289
time_elpased: 1.636
batch start
#iterations: 35
currently lose_sum: 533.6264732182026
time_elpased: 1.617
batch start
#iterations: 36
currently lose_sum: 535.6703602075577
time_elpased: 1.63
batch start
#iterations: 37
currently lose_sum: 533.8193433880806
time_elpased: 1.624
batch start
#iterations: 38
currently lose_sum: 536.1119339168072
time_elpased: 1.614
batch start
#iterations: 39
currently lose_sum: 536.6559371352196
time_elpased: 1.63
start validation test
0.545
0.910418695229
0.0966907962771
0.174815368795
0
validation finish
batch start
#iterations: 40
currently lose_sum: 536.2594860196114
time_elpased: 1.641
batch start
#iterations: 41
currently lose_sum: 536.0629183650017
time_elpased: 1.628
batch start
#iterations: 42
currently lose_sum: 534.2307194173336
time_elpased: 1.629
batch start
#iterations: 43
currently lose_sum: 534.6228745281696
time_elpased: 1.63
batch start
#iterations: 44
currently lose_sum: 534.7286595702171
time_elpased: 1.626
batch start
#iterations: 45
currently lose_sum: 535.0615853965282
time_elpased: 1.616
batch start
#iterations: 46
currently lose_sum: 534.3589061796665
time_elpased: 1.623
batch start
#iterations: 47
currently lose_sum: 534.6746047735214
time_elpased: 1.619
batch start
#iterations: 48
currently lose_sum: 533.6717604994774
time_elpased: 1.631
batch start
#iterations: 49
currently lose_sum: 534.0194491147995
time_elpased: 1.639
batch start
#iterations: 50
currently lose_sum: 536.0173556804657
time_elpased: 1.627
batch start
#iterations: 51
currently lose_sum: 534.671137034893
time_elpased: 1.639
batch start
#iterations: 52
currently lose_sum: 534.8315850198269
time_elpased: 1.619
batch start
#iterations: 53
currently lose_sum: 535.8411511778831
time_elpased: 1.632
batch start
#iterations: 54
currently lose_sum: 533.1021963357925
time_elpased: 1.638
batch start
#iterations: 55
currently lose_sum: 535.9400251507759
time_elpased: 1.624
batch start
#iterations: 56
currently lose_sum: 534.0242457389832
time_elpased: 1.642
batch start
#iterations: 57
currently lose_sum: 533.3655231595039
time_elpased: 1.627
batch start
#iterations: 58
currently lose_sum: 534.4310388565063
time_elpased: 1.631
batch start
#iterations: 59
currently lose_sum: 533.4784315228462
time_elpased: 1.646
start validation test
0.559793814433
0.901849217639
0.131127197518
0.228963524738
0
validation finish
batch start
#iterations: 60
currently lose_sum: 535.3355216383934
time_elpased: 1.639
batch start
#iterations: 61
currently lose_sum: 535.0701537430286
time_elpased: 1.624
batch start
#iterations: 62
currently lose_sum: 536.4389654994011
time_elpased: 1.627
batch start
#iterations: 63
currently lose_sum: 536.0344646573067
time_elpased: 1.636
batch start
#iterations: 64
currently lose_sum: 535.0844710171223
time_elpased: 1.629
batch start
#iterations: 65
currently lose_sum: 536.7218955159187
time_elpased: 1.631
batch start
#iterations: 66
currently lose_sum: 534.3889130949974
time_elpased: 1.632
batch start
#iterations: 67
currently lose_sum: 532.4042306542397
time_elpased: 1.622
batch start
#iterations: 68
currently lose_sum: 536.1683871150017
time_elpased: 1.629
batch start
#iterations: 69
currently lose_sum: 536.3513987660408
time_elpased: 1.629
batch start
#iterations: 70
currently lose_sum: 535.0890960991383
time_elpased: 1.623
batch start
#iterations: 71
currently lose_sum: 534.3636251688004
time_elpased: 1.635
batch start
#iterations: 72
currently lose_sum: 535.210986495018
time_elpased: 1.618
batch start
#iterations: 73
currently lose_sum: 533.573778450489
time_elpased: 1.628
batch start
#iterations: 74
currently lose_sum: 534.8511080741882
time_elpased: 1.628
batch start
#iterations: 75
currently lose_sum: 534.0875950753689
time_elpased: 1.624
batch start
#iterations: 76
currently lose_sum: 535.8173975348473
time_elpased: 1.626
batch start
#iterations: 77
currently lose_sum: 533.7952933013439
time_elpased: 1.622
batch start
#iterations: 78
currently lose_sum: 534.9544163346291
time_elpased: 1.648
batch start
#iterations: 79
currently lose_sum: 535.6982925534248
time_elpased: 1.609
start validation test
0.571030927835
0.890498261877
0.158945191313
0.269743769744
0
validation finish
batch start
#iterations: 80
currently lose_sum: 533.595852971077
time_elpased: 1.654
batch start
#iterations: 81
currently lose_sum: 534.3964591026306
time_elpased: 1.626
batch start
#iterations: 82
currently lose_sum: 534.6027713418007
time_elpased: 1.615
batch start
#iterations: 83
currently lose_sum: 535.5646976232529
time_elpased: 1.634
batch start
#iterations: 84
currently lose_sum: 534.8326046764851
time_elpased: 1.62
batch start
#iterations: 85
currently lose_sum: 533.2314968705177
time_elpased: 1.619
batch start
#iterations: 86
currently lose_sum: 533.4665747284889
time_elpased: 1.62
batch start
#iterations: 87
currently lose_sum: 533.0570695996284
time_elpased: 1.632
batch start
#iterations: 88
currently lose_sum: 534.1898728609085
time_elpased: 1.628
batch start
#iterations: 89
currently lose_sum: 533.9318240880966
time_elpased: 1.629
batch start
#iterations: 90
currently lose_sum: 534.9139841794968
time_elpased: 1.626
batch start
#iterations: 91
currently lose_sum: 533.322224020958
time_elpased: 1.626
batch start
#iterations: 92
currently lose_sum: 535.1167703866959
time_elpased: 1.617
batch start
#iterations: 93
currently lose_sum: 533.5388256311417
time_elpased: 1.631
batch start
#iterations: 94
currently lose_sum: 532.5251946747303
time_elpased: 1.635
batch start
#iterations: 95
currently lose_sum: 534.1797840595245
time_elpased: 1.635
batch start
#iterations: 96
currently lose_sum: 535.8635184764862
time_elpased: 1.619
batch start
#iterations: 97
currently lose_sum: 533.0581216216087
time_elpased: 1.617
batch start
#iterations: 98
currently lose_sum: 535.0017866790295
time_elpased: 1.631
batch start
#iterations: 99
currently lose_sum: 534.8262522816658
time_elpased: 1.615
start validation test
0.532164948454
0.914804469274
0.0677352637022
0.126131330637
0
validation finish
batch start
#iterations: 100
currently lose_sum: 533.9742167592049
time_elpased: 1.639
batch start
#iterations: 101
currently lose_sum: 534.5204802751541
time_elpased: 1.643
batch start
#iterations: 102
currently lose_sum: 532.8487610220909
time_elpased: 1.629
batch start
#iterations: 103
currently lose_sum: 534.1595480144024
time_elpased: 1.635
batch start
#iterations: 104
currently lose_sum: 533.8920247852802
time_elpased: 1.615
batch start
#iterations: 105
currently lose_sum: 533.9884490072727
time_elpased: 1.624
batch start
#iterations: 106
currently lose_sum: 535.2560113966465
time_elpased: 1.623
batch start
#iterations: 107
currently lose_sum: 534.0200856029987
time_elpased: 1.624
batch start
#iterations: 108
currently lose_sum: 532.7452033162117
time_elpased: 1.618
batch start
#iterations: 109
currently lose_sum: 534.2538960874081
time_elpased: 1.641
batch start
#iterations: 110
currently lose_sum: 534.5711387991905
time_elpased: 1.624
batch start
#iterations: 111
currently lose_sum: 534.9205983877182
time_elpased: 1.62
batch start
#iterations: 112
currently lose_sum: 533.5982508957386
time_elpased: 1.635
batch start
#iterations: 113
currently lose_sum: 533.0725891292095
time_elpased: 1.617
batch start
#iterations: 114
currently lose_sum: 533.7688051462173
time_elpased: 1.636
batch start
#iterations: 115
currently lose_sum: 533.8186564147472
time_elpased: 1.626
batch start
#iterations: 116
currently lose_sum: 532.5991955399513
time_elpased: 1.62
batch start
#iterations: 117
currently lose_sum: 533.6038073003292
time_elpased: 1.626
batch start
#iterations: 118
currently lose_sum: 533.5396364331245
time_elpased: 1.621
batch start
#iterations: 119
currently lose_sum: 533.8038909435272
time_elpased: 1.631
start validation test
0.575051546392
0.904653802497
0.164839710445
0.278866340098
0
validation finish
batch start
#iterations: 120
currently lose_sum: 534.1100967228413
time_elpased: 1.638
batch start
#iterations: 121
currently lose_sum: 532.8831934034824
time_elpased: 1.642
batch start
#iterations: 122
currently lose_sum: 531.2915130257607
time_elpased: 1.623
batch start
#iterations: 123
currently lose_sum: 532.3536089658737
time_elpased: 1.613
batch start
#iterations: 124
currently lose_sum: 533.2187201976776
time_elpased: 1.637
batch start
#iterations: 125
currently lose_sum: 533.1767961382866
time_elpased: 1.631
batch start
#iterations: 126
currently lose_sum: 533.1814758181572
time_elpased: 1.636
batch start
#iterations: 127
currently lose_sum: 534.263887822628
time_elpased: 1.632
batch start
#iterations: 128
currently lose_sum: 532.1332452893257
time_elpased: 1.632
batch start
#iterations: 129
currently lose_sum: 533.5348662734032
time_elpased: 1.629
batch start
#iterations: 130
currently lose_sum: 532.149753510952
time_elpased: 1.635
batch start
#iterations: 131
currently lose_sum: 532.7016604244709
time_elpased: 1.627
batch start
#iterations: 132
currently lose_sum: 534.0849021077156
time_elpased: 1.63
batch start
#iterations: 133
currently lose_sum: 534.4831618070602
time_elpased: 1.62
batch start
#iterations: 134
currently lose_sum: 534.113651394844
time_elpased: 1.619
batch start
#iterations: 135
currently lose_sum: 532.539337426424
time_elpased: 1.629
batch start
#iterations: 136
currently lose_sum: 533.9299245476723
time_elpased: 1.625
batch start
#iterations: 137
currently lose_sum: 532.2539894580841
time_elpased: 1.632
batch start
#iterations: 138
currently lose_sum: 533.1759303510189
time_elpased: 1.636
batch start
#iterations: 139
currently lose_sum: 533.535906970501
time_elpased: 1.636
start validation test
0.535773195876
0.911910669975
0.0760082730093
0.140320733104
0
validation finish
batch start
#iterations: 140
currently lose_sum: 530.3590994179249
time_elpased: 1.636
batch start
#iterations: 141
currently lose_sum: 533.5973230600357
time_elpased: 1.619
batch start
#iterations: 142
currently lose_sum: 532.2893253862858
time_elpased: 1.627
batch start
#iterations: 143
currently lose_sum: 533.8819938004017
time_elpased: 1.642
batch start
#iterations: 144
currently lose_sum: 533.0281037092209
time_elpased: 1.639
batch start
#iterations: 145
currently lose_sum: 534.6013020575047
time_elpased: 1.618
batch start
#iterations: 146
currently lose_sum: 535.1325907707214
time_elpased: 1.633
batch start
#iterations: 147
currently lose_sum: 533.9494654238224
time_elpased: 1.625
batch start
#iterations: 148
currently lose_sum: 532.0300741791725
time_elpased: 1.624
batch start
#iterations: 149
currently lose_sum: 534.350797444582
time_elpased: 1.646
batch start
#iterations: 150
currently lose_sum: 532.949845135212
time_elpased: 1.62
batch start
#iterations: 151
currently lose_sum: 532.1113099455833
time_elpased: 1.635
batch start
#iterations: 152
currently lose_sum: 533.2858029007912
time_elpased: 1.612
batch start
#iterations: 153
currently lose_sum: 531.4772620797157
time_elpased: 1.622
batch start
#iterations: 154
currently lose_sum: 536.0525062084198
time_elpased: 1.629
batch start
#iterations: 155
currently lose_sum: 531.9778996109962
time_elpased: 1.611
batch start
#iterations: 156
currently lose_sum: 533.4871021211147
time_elpased: 1.622
batch start
#iterations: 157
currently lose_sum: 533.2212691307068
time_elpased: 1.629
batch start
#iterations: 158
currently lose_sum: 531.8277963101864
time_elpased: 1.631
batch start
#iterations: 159
currently lose_sum: 533.3344320952892
time_elpased: 1.642
start validation test
0.550206185567
0.899323181049
0.109927611169
0.195908588279
0
validation finish
batch start
#iterations: 160
currently lose_sum: 532.639170050621
time_elpased: 1.641
batch start
#iterations: 161
currently lose_sum: 534.0784324407578
time_elpased: 1.614
batch start
#iterations: 162
currently lose_sum: 532.4520495533943
time_elpased: 1.641
batch start
#iterations: 163
currently lose_sum: 530.968221783638
time_elpased: 1.624
batch start
#iterations: 164
currently lose_sum: 534.6106337308884
time_elpased: 1.636
batch start
#iterations: 165
currently lose_sum: 532.6334530413151
time_elpased: 1.624
batch start
#iterations: 166
currently lose_sum: 533.506522834301
time_elpased: 1.637
batch start
#iterations: 167
currently lose_sum: 533.6692626178265
time_elpased: 1.628
batch start
#iterations: 168
currently lose_sum: 532.5197075605392
time_elpased: 1.651
batch start
#iterations: 169
currently lose_sum: 534.1135007739067
time_elpased: 1.629
batch start
#iterations: 170
currently lose_sum: 531.9747381806374
time_elpased: 1.636
batch start
#iterations: 171
currently lose_sum: 532.3330377340317
time_elpased: 1.625
batch start
#iterations: 172
currently lose_sum: 535.3144550919533
time_elpased: 1.623
batch start
#iterations: 173
currently lose_sum: 534.1415005624294
time_elpased: 1.636
batch start
#iterations: 174
currently lose_sum: 533.9378024339676
time_elpased: 1.625
batch start
#iterations: 175
currently lose_sum: 533.0808497667313
time_elpased: 1.635
batch start
#iterations: 176
currently lose_sum: 533.2567229866982
time_elpased: 1.62
batch start
#iterations: 177
currently lose_sum: 534.0841475129128
time_elpased: 1.631
batch start
#iterations: 178
currently lose_sum: 532.7225829958916
time_elpased: 1.619
batch start
#iterations: 179
currently lose_sum: 534.2405383884907
time_elpased: 1.628
start validation test
0.550515463918
0.912326388889
0.108686659772
0.194233967843
0
validation finish
batch start
#iterations: 180
currently lose_sum: 533.3479514718056
time_elpased: 1.653
batch start
#iterations: 181
currently lose_sum: 534.5752115547657
time_elpased: 1.643
batch start
#iterations: 182
currently lose_sum: 532.9999305605888
time_elpased: 1.628
batch start
#iterations: 183
currently lose_sum: 533.8245729506016
time_elpased: 1.641
batch start
#iterations: 184
currently lose_sum: 532.6083146631718
time_elpased: 1.63
batch start
#iterations: 185
currently lose_sum: 532.2344109117985
time_elpased: 1.635
batch start
#iterations: 186
currently lose_sum: 534.4547884464264
time_elpased: 1.617
batch start
#iterations: 187
currently lose_sum: 532.8048278689384
time_elpased: 1.647
batch start
#iterations: 188
currently lose_sum: 531.1045278906822
time_elpased: 1.633
batch start
#iterations: 189
currently lose_sum: 533.5586573779583
time_elpased: 1.614
batch start
#iterations: 190
currently lose_sum: 532.2366673350334
time_elpased: 1.634
batch start
#iterations: 191
currently lose_sum: 533.9736289381981
time_elpased: 1.633
batch start
#iterations: 192
currently lose_sum: 532.5069409012794
time_elpased: 1.628
batch start
#iterations: 193
currently lose_sum: 532.7317749857903
time_elpased: 1.639
batch start
#iterations: 194
currently lose_sum: 532.7674112319946
time_elpased: 1.623
batch start
#iterations: 195
currently lose_sum: 533.7354643940926
time_elpased: 1.629
batch start
#iterations: 196
currently lose_sum: 530.8284530639648
time_elpased: 1.639
batch start
#iterations: 197
currently lose_sum: 533.8305304944515
time_elpased: 1.635
batch start
#iterations: 198
currently lose_sum: 532.7521828711033
time_elpased: 1.634
batch start
#iterations: 199
currently lose_sum: 532.9155451059341
time_elpased: 1.624
start validation test
0.561391752577
0.911994322214
0.132885211996
0.23197039444
0
validation finish
batch start
#iterations: 200
currently lose_sum: 533.7242408394814
time_elpased: 1.658
batch start
#iterations: 201
currently lose_sum: 533.2855728268623
time_elpased: 1.634
batch start
#iterations: 202
currently lose_sum: 533.1037073135376
time_elpased: 1.626
batch start
#iterations: 203
currently lose_sum: 532.8916687071323
time_elpased: 1.614
batch start
#iterations: 204
currently lose_sum: 532.620698928833
time_elpased: 1.638
batch start
#iterations: 205
currently lose_sum: 533.6269755363464
time_elpased: 1.617
batch start
#iterations: 206
currently lose_sum: 533.8385839760303
time_elpased: 1.639
batch start
#iterations: 207
currently lose_sum: 532.0347261428833
time_elpased: 1.628
batch start
#iterations: 208
currently lose_sum: 531.4471166729927
time_elpased: 1.646
batch start
#iterations: 209
currently lose_sum: 531.9966642856598
time_elpased: 1.642
batch start
#iterations: 210
currently lose_sum: 533.6709881424904
time_elpased: 1.632
batch start
#iterations: 211
currently lose_sum: 534.200520336628
time_elpased: 1.625
batch start
#iterations: 212
currently lose_sum: 533.1383273601532
time_elpased: 1.646
batch start
#iterations: 213
currently lose_sum: 532.4174724817276
time_elpased: 1.632
batch start
#iterations: 214
currently lose_sum: 533.8333241343498
time_elpased: 1.646
batch start
#iterations: 215
currently lose_sum: 531.873076826334
time_elpased: 1.625
batch start
#iterations: 216
currently lose_sum: 534.4513450562954
time_elpased: 1.645
batch start
#iterations: 217
currently lose_sum: 533.2368980050087
time_elpased: 1.623
batch start
#iterations: 218
currently lose_sum: 533.720283806324
time_elpased: 1.628
batch start
#iterations: 219
currently lose_sum: 533.75984364748
time_elpased: 1.623
start validation test
0.551855670103
0.898042414356
0.113857290589
0.202092511013
0
validation finish
batch start
#iterations: 220
currently lose_sum: 531.0503421723843
time_elpased: 1.664
batch start
#iterations: 221
currently lose_sum: 534.1136715710163
time_elpased: 1.624
batch start
#iterations: 222
currently lose_sum: 533.1531936824322
time_elpased: 1.633
batch start
#iterations: 223
currently lose_sum: 532.7764009237289
time_elpased: 1.627
batch start
#iterations: 224
currently lose_sum: 533.1701422929764
time_elpased: 1.624
batch start
#iterations: 225
currently lose_sum: 534.6270709633827
time_elpased: 1.628
batch start
#iterations: 226
currently lose_sum: 533.3959046900272
time_elpased: 1.634
batch start
#iterations: 227
currently lose_sum: 532.7161597907543
time_elpased: 1.625
batch start
#iterations: 228
currently lose_sum: 531.4355589747429
time_elpased: 1.625
batch start
#iterations: 229
currently lose_sum: 532.3999798297882
time_elpased: 1.637
batch start
#iterations: 230
currently lose_sum: 533.8066809773445
time_elpased: 1.63
batch start
#iterations: 231
currently lose_sum: 534.8173308372498
time_elpased: 1.619
batch start
#iterations: 232
currently lose_sum: 531.8215058147907
time_elpased: 1.628
batch start
#iterations: 233
currently lose_sum: 530.823155105114
time_elpased: 1.627
batch start
#iterations: 234
currently lose_sum: 531.4119390845299
time_elpased: 1.632
batch start
#iterations: 235
currently lose_sum: 532.0508759915829
time_elpased: 1.624
batch start
#iterations: 236
currently lose_sum: 531.9688106775284
time_elpased: 1.623
batch start
#iterations: 237
currently lose_sum: 532.5320019125938
time_elpased: 1.638
batch start
#iterations: 238
currently lose_sum: 532.3929128050804
time_elpased: 1.62
batch start
#iterations: 239
currently lose_sum: 531.2266145050526
time_elpased: 1.621
start validation test
0.530463917526
0.945945945946
0.0615305067218
0.115545198563
0
validation finish
batch start
#iterations: 240
currently lose_sum: 531.1482617259026
time_elpased: 1.632
batch start
#iterations: 241
currently lose_sum: 533.0389537513256
time_elpased: 1.626
batch start
#iterations: 242
currently lose_sum: 530.9268455505371
time_elpased: 1.628
batch start
#iterations: 243
currently lose_sum: 531.1383779346943
time_elpased: 1.625
batch start
#iterations: 244
currently lose_sum: 533.2707729041576
time_elpased: 1.637
batch start
#iterations: 245
currently lose_sum: 532.6978347599506
time_elpased: 1.631
batch start
#iterations: 246
currently lose_sum: 531.3616052567959
time_elpased: 1.642
batch start
#iterations: 247
currently lose_sum: 532.1282288432121
time_elpased: 1.628
batch start
#iterations: 248
currently lose_sum: 532.0330911576748
time_elpased: 1.629
batch start
#iterations: 249
currently lose_sum: 531.9869983792305
time_elpased: 1.644
batch start
#iterations: 250
currently lose_sum: 531.3578836023808
time_elpased: 1.628
batch start
#iterations: 251
currently lose_sum: 534.1253080368042
time_elpased: 1.63
batch start
#iterations: 252
currently lose_sum: 533.6546476483345
time_elpased: 1.633
batch start
#iterations: 253
currently lose_sum: 531.5043878555298
time_elpased: 1.638
batch start
#iterations: 254
currently lose_sum: 534.4533051848412
time_elpased: 1.633
batch start
#iterations: 255
currently lose_sum: 531.1717360317707
time_elpased: 1.632
batch start
#iterations: 256
currently lose_sum: 532.1957556605339
time_elpased: 1.635
batch start
#iterations: 257
currently lose_sum: 531.2066186964512
time_elpased: 1.617
batch start
#iterations: 258
currently lose_sum: 533.7973365783691
time_elpased: 1.641
batch start
#iterations: 259
currently lose_sum: 532.7717424631119
time_elpased: 1.628
start validation test
0.573041237113
0.909144542773
0.159358841779
0.271183457985
0
validation finish
batch start
#iterations: 260
currently lose_sum: 534.6589369177818
time_elpased: 1.641
batch start
#iterations: 261
currently lose_sum: 534.4824903607368
time_elpased: 1.636
batch start
#iterations: 262
currently lose_sum: 532.2144525647163
time_elpased: 1.628
batch start
#iterations: 263
currently lose_sum: 532.7154424190521
time_elpased: 1.613
batch start
#iterations: 264
currently lose_sum: 532.3170672059059
time_elpased: 1.642
batch start
#iterations: 265
currently lose_sum: 533.2377871274948
time_elpased: 1.633
batch start
#iterations: 266
currently lose_sum: 531.9227424561977
time_elpased: 1.636
batch start
#iterations: 267
currently lose_sum: 530.994537293911
time_elpased: 1.62
batch start
#iterations: 268
currently lose_sum: 531.4257430136204
time_elpased: 1.643
batch start
#iterations: 269
currently lose_sum: 532.4899525642395
time_elpased: 1.625
batch start
#iterations: 270
currently lose_sum: 530.9749923050404
time_elpased: 1.631
batch start
#iterations: 271
currently lose_sum: 530.2108813524246
time_elpased: 1.621
batch start
#iterations: 272
currently lose_sum: 532.562937527895
time_elpased: 1.631
batch start
#iterations: 273
currently lose_sum: 529.9119775891304
time_elpased: 1.62
batch start
#iterations: 274
currently lose_sum: 532.2372162342072
time_elpased: 1.638
batch start
#iterations: 275
currently lose_sum: 531.8582140803337
time_elpased: 1.638
batch start
#iterations: 276
currently lose_sum: 532.0389422774315
time_elpased: 1.638
batch start
#iterations: 277
currently lose_sum: 534.1232687830925
time_elpased: 1.651
batch start
#iterations: 278
currently lose_sum: 532.9712105095387
time_elpased: 1.635
batch start
#iterations: 279
currently lose_sum: 534.5587248206139
time_elpased: 1.631
start validation test
0.547835051546
0.91042047532
0.102998965874
0.185061315496
0
validation finish
batch start
#iterations: 280
currently lose_sum: 530.1718622148037
time_elpased: 1.664
batch start
#iterations: 281
currently lose_sum: 531.6237729787827
time_elpased: 1.625
batch start
#iterations: 282
currently lose_sum: 533.5876013934612
time_elpased: 1.632
batch start
#iterations: 283
currently lose_sum: 531.7904024124146
time_elpased: 1.622
batch start
#iterations: 284
currently lose_sum: 534.1617543101311
time_elpased: 1.636
batch start
#iterations: 285
currently lose_sum: 532.924881696701
time_elpased: 1.636
batch start
#iterations: 286
currently lose_sum: 534.1585747599602
time_elpased: 1.637
batch start
#iterations: 287
currently lose_sum: 533.0935225486755
time_elpased: 1.639
batch start
#iterations: 288
currently lose_sum: 532.7773133516312
time_elpased: 1.636
batch start
#iterations: 289
currently lose_sum: 531.910901516676
time_elpased: 1.638
batch start
#iterations: 290
currently lose_sum: 531.3044133484364
time_elpased: 1.624
batch start
#iterations: 291
currently lose_sum: 532.8866161108017
time_elpased: 1.629
batch start
#iterations: 292
currently lose_sum: 532.8288307189941
time_elpased: 1.641
batch start
#iterations: 293
currently lose_sum: 533.9616245031357
time_elpased: 1.64
batch start
#iterations: 294
currently lose_sum: 532.5643292963505
time_elpased: 1.636
batch start
#iterations: 295
currently lose_sum: 532.3825425505638
time_elpased: 1.626
batch start
#iterations: 296
currently lose_sum: 530.6480450928211
time_elpased: 1.617
batch start
#iterations: 297
currently lose_sum: 531.8434422016144
time_elpased: 1.643
batch start
#iterations: 298
currently lose_sum: 532.3390677571297
time_elpased: 1.622
batch start
#iterations: 299
currently lose_sum: 532.4165318310261
time_elpased: 1.618
start validation test
0.558144329897
0.902492668622
0.127300930714
0.223128511872
0
validation finish
batch start
#iterations: 300
currently lose_sum: 531.3886665999889
time_elpased: 1.653
batch start
#iterations: 301
currently lose_sum: 532.1422491669655
time_elpased: 1.633
batch start
#iterations: 302
currently lose_sum: 534.0967248976231
time_elpased: 1.64
batch start
#iterations: 303
currently lose_sum: 535.9041986465454
time_elpased: 1.636
batch start
#iterations: 304
currently lose_sum: 529.8707168102264
time_elpased: 1.635
batch start
#iterations: 305
currently lose_sum: 533.5486791729927
time_elpased: 1.627
batch start
#iterations: 306
currently lose_sum: 533.1183172464371
time_elpased: 1.639
batch start
#iterations: 307
currently lose_sum: 534.0303063094616
time_elpased: 1.629
batch start
#iterations: 308
currently lose_sum: 532.0126794278622
time_elpased: 1.627
batch start
#iterations: 309
currently lose_sum: 533.4577246308327
time_elpased: 1.642
batch start
#iterations: 310
currently lose_sum: 531.6216340661049
time_elpased: 1.63
batch start
#iterations: 311
currently lose_sum: 532.0196059346199
time_elpased: 1.645
batch start
#iterations: 312
currently lose_sum: 532.2047054767609
time_elpased: 1.628
batch start
#iterations: 313
currently lose_sum: 534.0029063522816
time_elpased: 1.621
batch start
#iterations: 314
currently lose_sum: 531.2490813732147
time_elpased: 1.632
batch start
#iterations: 315
currently lose_sum: 532.0042859911919
time_elpased: 1.62
batch start
#iterations: 316
currently lose_sum: 532.426679700613
time_elpased: 1.642
batch start
#iterations: 317
currently lose_sum: 532.4080924093723
time_elpased: 1.63
batch start
#iterations: 318
currently lose_sum: 533.8578293919563
time_elpased: 1.63
batch start
#iterations: 319
currently lose_sum: 532.3536206781864
time_elpased: 1.629
start validation test
0.560154639175
0.930355791067
0.127094105481
0.223637521609
0
validation finish
batch start
#iterations: 320
currently lose_sum: 532.8994612693787
time_elpased: 1.626
batch start
#iterations: 321
currently lose_sum: 533.4373130202293
time_elpased: 1.63
batch start
#iterations: 322
currently lose_sum: 532.2962477207184
time_elpased: 1.629
batch start
#iterations: 323
currently lose_sum: 530.3960421681404
time_elpased: 1.635
batch start
#iterations: 324
currently lose_sum: 534.6472065150738
time_elpased: 1.637
batch start
#iterations: 325
currently lose_sum: 530.2437895536423
time_elpased: 1.641
batch start
#iterations: 326
currently lose_sum: 532.1937126517296
time_elpased: 1.641
batch start
#iterations: 327
currently lose_sum: 533.4067288637161
time_elpased: 1.626
batch start
#iterations: 328
currently lose_sum: 533.3386731743813
time_elpased: 1.638
batch start
#iterations: 329
currently lose_sum: 532.1715785264969
time_elpased: 1.638
batch start
#iterations: 330
currently lose_sum: 534.5283959507942
time_elpased: 1.638
batch start
#iterations: 331
currently lose_sum: 534.7141416370869
time_elpased: 1.644
batch start
#iterations: 332
currently lose_sum: 533.2926885783672
time_elpased: 1.62
batch start
#iterations: 333
currently lose_sum: 530.8657772541046
time_elpased: 1.629
batch start
#iterations: 334
currently lose_sum: 531.8681168556213
time_elpased: 1.644
batch start
#iterations: 335
currently lose_sum: 530.8955833911896
time_elpased: 1.626
batch start
#iterations: 336
currently lose_sum: 534.070976883173
time_elpased: 1.644
batch start
#iterations: 337
currently lose_sum: 532.8185767829418
time_elpased: 1.633
batch start
#iterations: 338
currently lose_sum: 531.6018747091293
time_elpased: 1.63
batch start
#iterations: 339
currently lose_sum: 532.852535367012
time_elpased: 1.629
start validation test
0.565979381443
0.922868741543
0.141054808687
0.244707570865
0
validation finish
batch start
#iterations: 340
currently lose_sum: 532.4203709065914
time_elpased: 1.64
batch start
#iterations: 341
currently lose_sum: 531.9816130399704
time_elpased: 1.639
batch start
#iterations: 342
currently lose_sum: 534.97108528018
time_elpased: 1.633
batch start
#iterations: 343
currently lose_sum: 533.1041881144047
time_elpased: 1.652
batch start
#iterations: 344
currently lose_sum: 532.4429655075073
time_elpased: 1.616
batch start
#iterations: 345
currently lose_sum: 532.2242786884308
time_elpased: 1.636
batch start
#iterations: 346
currently lose_sum: 530.2393662333488
time_elpased: 1.641
batch start
#iterations: 347
currently lose_sum: 532.2696450650692
time_elpased: 1.634
batch start
#iterations: 348
currently lose_sum: 532.194158166647
time_elpased: 1.632
batch start
#iterations: 349
currently lose_sum: 532.2523196935654
time_elpased: 1.631
batch start
#iterations: 350
currently lose_sum: 532.7651394605637
time_elpased: 1.639
batch start
#iterations: 351
currently lose_sum: 531.6867175400257
time_elpased: 1.63
batch start
#iterations: 352
currently lose_sum: 531.6833114922047
time_elpased: 1.636
batch start
#iterations: 353
currently lose_sum: 532.7718230485916
time_elpased: 1.633
batch start
#iterations: 354
currently lose_sum: 532.074228644371
time_elpased: 1.631
batch start
#iterations: 355
currently lose_sum: 531.9941721856594
time_elpased: 1.641
batch start
#iterations: 356
currently lose_sum: 533.0069079995155
time_elpased: 1.635
batch start
#iterations: 357
currently lose_sum: 532.3937982022762
time_elpased: 1.636
batch start
#iterations: 358
currently lose_sum: 532.801386654377
time_elpased: 1.624
batch start
#iterations: 359
currently lose_sum: 531.8189933896065
time_elpased: 1.625
start validation test
0.55087628866
0.928379588183
0.107238883144
0.192268471308
0
validation finish
batch start
#iterations: 360
currently lose_sum: 532.8663472533226
time_elpased: 1.655
batch start
#iterations: 361
currently lose_sum: 530.4558246433735
time_elpased: 1.638
batch start
#iterations: 362
currently lose_sum: 532.1332374215126
time_elpased: 1.641
batch start
#iterations: 363
currently lose_sum: 532.1558880805969
time_elpased: 1.637
batch start
#iterations: 364
currently lose_sum: 533.2239594459534
time_elpased: 1.645
batch start
#iterations: 365
currently lose_sum: 531.5402584671974
time_elpased: 1.64
batch start
#iterations: 366
currently lose_sum: 533.3304523229599
time_elpased: 1.632
batch start
#iterations: 367
currently lose_sum: 533.6119340956211
time_elpased: 1.64
batch start
#iterations: 368
currently lose_sum: 530.361570596695
time_elpased: 1.633
batch start
#iterations: 369
currently lose_sum: 531.8424386978149
time_elpased: 1.625
batch start
#iterations: 370
currently lose_sum: 531.5101138055325
time_elpased: 1.633
batch start
#iterations: 371
currently lose_sum: 532.4530214071274
time_elpased: 1.633
batch start
#iterations: 372
currently lose_sum: 531.7131712436676
time_elpased: 1.645
batch start
#iterations: 373
currently lose_sum: 532.6757007837296
time_elpased: 1.626
batch start
#iterations: 374
currently lose_sum: 534.0926386117935
time_elpased: 1.642
batch start
#iterations: 375
currently lose_sum: 534.0626952648163
time_elpased: 1.646
batch start
#iterations: 376
currently lose_sum: 532.8065501153469
time_elpased: 1.635
batch start
#iterations: 377
currently lose_sum: 532.6386366486549
time_elpased: 1.632
batch start
#iterations: 378
currently lose_sum: 533.283359348774
time_elpased: 1.626
batch start
#iterations: 379
currently lose_sum: 530.674727499485
time_elpased: 1.647
start validation test
0.549226804124
0.925482980681
0.104033092037
0.18704099656
0
validation finish
batch start
#iterations: 380
currently lose_sum: 533.0133594870567
time_elpased: 1.647
batch start
#iterations: 381
currently lose_sum: 531.1658556461334
time_elpased: 1.643
batch start
#iterations: 382
currently lose_sum: 533.7633323073387
time_elpased: 1.645
batch start
#iterations: 383
currently lose_sum: 530.5409579277039
time_elpased: 1.636
batch start
#iterations: 384
currently lose_sum: 530.5608757436275
time_elpased: 1.638
batch start
#iterations: 385
currently lose_sum: 531.3553875088692
time_elpased: 1.645
batch start
#iterations: 386
currently lose_sum: 532.4113955795765
time_elpased: 1.634
batch start
#iterations: 387
currently lose_sum: 532.2889938652515
time_elpased: 1.633
batch start
#iterations: 388
currently lose_sum: 532.602759718895
time_elpased: 1.629
batch start
#iterations: 389
currently lose_sum: 531.8078912198544
time_elpased: 1.633
batch start
#iterations: 390
currently lose_sum: 531.6899133622646
time_elpased: 1.636
batch start
#iterations: 391
currently lose_sum: 532.9836944937706
time_elpased: 1.637
batch start
#iterations: 392
currently lose_sum: 531.2445271909237
time_elpased: 1.634
batch start
#iterations: 393
currently lose_sum: 532.221475481987
time_elpased: 1.65
batch start
#iterations: 394
currently lose_sum: 532.3996038138866
time_elpased: 1.652
batch start
#iterations: 395
currently lose_sum: 531.0021796226501
time_elpased: 1.629
batch start
#iterations: 396
currently lose_sum: 531.5130738019943
time_elpased: 1.644
batch start
#iterations: 397
currently lose_sum: 532.6249193549156
time_elpased: 1.638
batch start
#iterations: 398
currently lose_sum: 532.2404648959637
time_elpased: 1.633
batch start
#iterations: 399
currently lose_sum: 531.221512645483
time_elpased: 1.628
start validation test
0.553711340206
0.916803953871
0.115098241986
0.204520396913
0
validation finish
acc: 0.564
pre: 0.896
rec: 0.155
F1: 0.264
auc: 0.000
