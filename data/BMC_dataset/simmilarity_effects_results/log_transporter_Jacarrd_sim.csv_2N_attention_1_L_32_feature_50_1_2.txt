start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 572.8915465474129
time_elpased: 2.655
batch start
#iterations: 1
currently lose_sum: 551.8289086818695
time_elpased: 2.635
batch start
#iterations: 2
currently lose_sum: 546.9641416072845
time_elpased: 2.639
batch start
#iterations: 3
currently lose_sum: 544.4371448755264
time_elpased: 2.65
batch start
#iterations: 4
currently lose_sum: 542.5875778794289
time_elpased: 2.641
batch start
#iterations: 5
currently lose_sum: 543.6532873809338
time_elpased: 2.654
batch start
#iterations: 6
currently lose_sum: 540.5508763194084
time_elpased: 2.651
batch start
#iterations: 7
currently lose_sum: 541.809595823288
time_elpased: 2.646
batch start
#iterations: 8
currently lose_sum: 539.0953914523125
time_elpased: 2.64
batch start
#iterations: 9
currently lose_sum: 540.5304635167122
time_elpased: 2.649
batch start
#iterations: 10
currently lose_sum: 539.9168200790882
time_elpased: 2.651
batch start
#iterations: 11
currently lose_sum: 541.2119989395142
time_elpased: 2.647
batch start
#iterations: 12
currently lose_sum: 539.6929669976234
time_elpased: 2.65
batch start
#iterations: 13
currently lose_sum: 537.5400464832783
time_elpased: 2.649
batch start
#iterations: 14
currently lose_sum: 538.0890097022057
time_elpased: 2.673
batch start
#iterations: 15
currently lose_sum: 538.6829433441162
time_elpased: 2.648
batch start
#iterations: 16
currently lose_sum: 538.6788603067398
time_elpased: 2.642
batch start
#iterations: 17
currently lose_sum: 537.7255167961121
time_elpased: 2.663
batch start
#iterations: 18
currently lose_sum: 538.4633786976337
time_elpased: 2.646
batch start
#iterations: 19
currently lose_sum: 538.3611525297165
time_elpased: 2.649
start validation test
0.530206185567
0.875675675676
0.0670113753878
0.124495677233
0
validation finish
batch start
#iterations: 20
currently lose_sum: 538.3762821555138
time_elpased: 2.653
batch start
#iterations: 21
currently lose_sum: 539.9875481128693
time_elpased: 2.664
batch start
#iterations: 22
currently lose_sum: 536.9793629646301
time_elpased: 2.647
batch start
#iterations: 23
currently lose_sum: 537.9269751310349
time_elpased: 2.661
batch start
#iterations: 24
currently lose_sum: 535.4071645438671
time_elpased: 2.654
batch start
#iterations: 25
currently lose_sum: 537.4162412881851
time_elpased: 2.644
batch start
#iterations: 26
currently lose_sum: 536.9682930707932
time_elpased: 2.652
batch start
#iterations: 27
currently lose_sum: 536.3272589445114
time_elpased: 2.66
batch start
#iterations: 28
currently lose_sum: 536.3321837186813
time_elpased: 2.647
batch start
#iterations: 29
currently lose_sum: 536.6422833204269
time_elpased: 2.649
batch start
#iterations: 30
currently lose_sum: 535.5044960975647
time_elpased: 2.653
batch start
#iterations: 31
currently lose_sum: 535.5497542619705
time_elpased: 2.634
batch start
#iterations: 32
currently lose_sum: 537.535447537899
time_elpased: 2.65
batch start
#iterations: 33
currently lose_sum: 535.9598942995071
time_elpased: 2.646
batch start
#iterations: 34
currently lose_sum: 535.8586885333061
time_elpased: 2.638
batch start
#iterations: 35
currently lose_sum: 533.6689007878304
time_elpased: 2.65
batch start
#iterations: 36
currently lose_sum: 535.713695526123
time_elpased: 2.625
batch start
#iterations: 37
currently lose_sum: 533.8336574435234
time_elpased: 2.637
batch start
#iterations: 38
currently lose_sum: 536.3002241551876
time_elpased: 2.661
batch start
#iterations: 39
currently lose_sum: 536.6976870894432
time_elpased: 2.641
start validation test
0.537010309278
0.892694063927
0.0808686659772
0.148302674
0
validation finish
batch start
#iterations: 40
currently lose_sum: 536.3603051900864
time_elpased: 2.639
batch start
#iterations: 41
currently lose_sum: 536.075303465128
time_elpased: 2.665
batch start
#iterations: 42
currently lose_sum: 534.2281312644482
time_elpased: 2.656
batch start
#iterations: 43
currently lose_sum: 534.6660001277924
time_elpased: 2.654
batch start
#iterations: 44
currently lose_sum: 534.7664669156075
time_elpased: 2.649
batch start
#iterations: 45
currently lose_sum: 535.1405319273472
time_elpased: 2.654
batch start
#iterations: 46
currently lose_sum: 534.5632590651512
time_elpased: 2.65
batch start
#iterations: 47
currently lose_sum: 534.7181780040264
time_elpased: 2.647
batch start
#iterations: 48
currently lose_sum: 533.7569816708565
time_elpased: 2.654
batch start
#iterations: 49
currently lose_sum: 534.1435988843441
time_elpased: 2.636
batch start
#iterations: 50
currently lose_sum: 536.1171599030495
time_elpased: 2.656
batch start
#iterations: 51
currently lose_sum: 534.7330787181854
time_elpased: 2.651
batch start
#iterations: 52
currently lose_sum: 534.9195842444897
time_elpased: 2.636
batch start
#iterations: 53
currently lose_sum: 535.8245356976986
time_elpased: 2.633
batch start
#iterations: 54
currently lose_sum: 533.2294933199883
time_elpased: 2.641
batch start
#iterations: 55
currently lose_sum: 535.8720144927502
time_elpased: 2.647
batch start
#iterations: 56
currently lose_sum: 534.0352482497692
time_elpased: 2.648
batch start
#iterations: 57
currently lose_sum: 533.5074111223221
time_elpased: 2.647
batch start
#iterations: 58
currently lose_sum: 534.4557363986969
time_elpased: 2.643
batch start
#iterations: 59
currently lose_sum: 533.594111084938
time_elpased: 2.651
start validation test
0.564226804124
0.883354350567
0.144881075491
0.248933901919
0
validation finish
batch start
#iterations: 60
currently lose_sum: 535.6037554740906
time_elpased: 2.655
batch start
#iterations: 61
currently lose_sum: 535.1382471323013
time_elpased: 2.645
batch start
#iterations: 62
currently lose_sum: 536.5721052885056
time_elpased: 2.656
batch start
#iterations: 63
currently lose_sum: 536.1762679219246
time_elpased: 2.66
batch start
#iterations: 64
currently lose_sum: 535.0923194885254
time_elpased: 2.641
batch start
#iterations: 65
currently lose_sum: 536.7896556258202
time_elpased: 2.661
batch start
#iterations: 66
currently lose_sum: 534.428826212883
time_elpased: 2.661
batch start
#iterations: 67
currently lose_sum: 532.5649113953114
time_elpased: 2.649
batch start
#iterations: 68
currently lose_sum: 536.1244341731071
time_elpased: 2.661
batch start
#iterations: 69
currently lose_sum: 536.3635913729668
time_elpased: 2.665
batch start
#iterations: 70
currently lose_sum: 535.2131964564323
time_elpased: 2.657
batch start
#iterations: 71
currently lose_sum: 534.4548051059246
time_elpased: 2.66
batch start
#iterations: 72
currently lose_sum: 535.2342590391636
time_elpased: 2.657
batch start
#iterations: 73
currently lose_sum: 533.7249594926834
time_elpased: 2.656
batch start
#iterations: 74
currently lose_sum: 534.8419718742371
time_elpased: 2.669
batch start
#iterations: 75
currently lose_sum: 534.2128350138664
time_elpased: 2.649
batch start
#iterations: 76
currently lose_sum: 536.0576173663139
time_elpased: 2.651
batch start
#iterations: 77
currently lose_sum: 533.9906142950058
time_elpased: 2.644
batch start
#iterations: 78
currently lose_sum: 534.956198066473
time_elpased: 2.651
batch start
#iterations: 79
currently lose_sum: 535.8960736095905
time_elpased: 2.652
start validation test
0.573402061856
0.877164502165
0.167631851086
0.281472477861
0
validation finish
batch start
#iterations: 80
currently lose_sum: 533.7898065447807
time_elpased: 2.658
batch start
#iterations: 81
currently lose_sum: 534.4918401241302
time_elpased: 2.672
batch start
#iterations: 82
currently lose_sum: 534.6078639626503
time_elpased: 2.656
batch start
#iterations: 83
currently lose_sum: 535.6900127530098
time_elpased: 2.661
batch start
#iterations: 84
currently lose_sum: 535.0217334330082
time_elpased: 2.659
batch start
#iterations: 85
currently lose_sum: 533.2206206023693
time_elpased: 2.655
batch start
#iterations: 86
currently lose_sum: 533.6410235762596
time_elpased: 2.66
batch start
#iterations: 87
currently lose_sum: 533.132469534874
time_elpased: 2.652
batch start
#iterations: 88
currently lose_sum: 534.3112084269524
time_elpased: 2.65
batch start
#iterations: 89
currently lose_sum: 533.9292689561844
time_elpased: 2.666
batch start
#iterations: 90
currently lose_sum: 535.1188462078571
time_elpased: 2.656
batch start
#iterations: 91
currently lose_sum: 533.4062086939812
time_elpased: 2.639
batch start
#iterations: 92
currently lose_sum: 535.1852185130119
time_elpased: 2.642
batch start
#iterations: 93
currently lose_sum: 533.5510731339455
time_elpased: 2.643
batch start
#iterations: 94
currently lose_sum: 532.6535417437553
time_elpased: 2.61
batch start
#iterations: 95
currently lose_sum: 534.3027852773666
time_elpased: 2.66
batch start
#iterations: 96
currently lose_sum: 535.8454875946045
time_elpased: 2.636
batch start
#iterations: 97
currently lose_sum: 533.0885702371597
time_elpased: 2.639
batch start
#iterations: 98
currently lose_sum: 534.9106037914753
time_elpased: 2.648
batch start
#iterations: 99
currently lose_sum: 534.9062744379044
time_elpased: 2.64
start validation test
0.526546391753
0.893030794165
0.0569803516029
0.107125498202
0
validation finish
batch start
#iterations: 100
currently lose_sum: 534.1049921512604
time_elpased: 2.616
batch start
#iterations: 101
currently lose_sum: 534.5515540838242
time_elpased: 2.658
batch start
#iterations: 102
currently lose_sum: 532.8566635847092
time_elpased: 2.666
batch start
#iterations: 103
currently lose_sum: 534.3429492115974
time_elpased: 2.638
batch start
#iterations: 104
currently lose_sum: 533.8733062744141
time_elpased: 2.663
batch start
#iterations: 105
currently lose_sum: 534.1020542085171
time_elpased: 2.665
batch start
#iterations: 106
currently lose_sum: 535.4666098356247
time_elpased: 2.663
batch start
#iterations: 107
currently lose_sum: 534.1136455833912
time_elpased: 2.689
batch start
#iterations: 108
currently lose_sum: 532.8571588993073
time_elpased: 2.673
batch start
#iterations: 109
currently lose_sum: 534.2580409348011
time_elpased: 2.672
batch start
#iterations: 110
currently lose_sum: 534.539645254612
time_elpased: 2.67
batch start
#iterations: 111
currently lose_sum: 535.1227269768715
time_elpased: 2.653
batch start
#iterations: 112
currently lose_sum: 533.6457758843899
time_elpased: 2.64
batch start
#iterations: 113
currently lose_sum: 533.1522899568081
time_elpased: 2.646
batch start
#iterations: 114
currently lose_sum: 533.8587268292904
time_elpased: 2.642
batch start
#iterations: 115
currently lose_sum: 533.9276993274689
time_elpased: 2.637
batch start
#iterations: 116
currently lose_sum: 532.6226836442947
time_elpased: 2.682
batch start
#iterations: 117
currently lose_sum: 533.5672038495541
time_elpased: 2.691
batch start
#iterations: 118
currently lose_sum: 533.4860816895962
time_elpased: 2.669
batch start
#iterations: 119
currently lose_sum: 533.6523186564445
time_elpased: 2.654
start validation test
0.569484536082
0.901829268293
0.152947259566
0.261538461538
0
validation finish
batch start
#iterations: 120
currently lose_sum: 534.284185141325
time_elpased: 2.653
batch start
#iterations: 121
currently lose_sum: 532.9838745892048
time_elpased: 2.65
batch start
#iterations: 122
currently lose_sum: 531.3653992414474
time_elpased: 2.687
batch start
#iterations: 123
currently lose_sum: 532.2847472131252
time_elpased: 2.673
batch start
#iterations: 124
currently lose_sum: 533.0518654882908
time_elpased: 2.673
batch start
#iterations: 125
currently lose_sum: 533.3136806488037
time_elpased: 2.664
batch start
#iterations: 126
currently lose_sum: 533.4321887493134
time_elpased: 2.673
batch start
#iterations: 127
currently lose_sum: 534.388205230236
time_elpased: 2.67
batch start
#iterations: 128
currently lose_sum: 532.208559602499
time_elpased: 2.684
batch start
#iterations: 129
currently lose_sum: 533.421061873436
time_elpased: 2.672
batch start
#iterations: 130
currently lose_sum: 532.2140049338341
time_elpased: 2.646
batch start
#iterations: 131
currently lose_sum: 532.6981590390205
time_elpased: 2.678
batch start
#iterations: 132
currently lose_sum: 534.0124224424362
time_elpased: 2.706
batch start
#iterations: 133
currently lose_sum: 534.3858689665794
time_elpased: 2.695
batch start
#iterations: 134
currently lose_sum: 533.990669965744
time_elpased: 2.658
batch start
#iterations: 135
currently lose_sum: 532.4625255465508
time_elpased: 2.654
batch start
#iterations: 136
currently lose_sum: 533.9833547770977
time_elpased: 2.654
batch start
#iterations: 137
currently lose_sum: 532.4046220183372
time_elpased: 2.66
batch start
#iterations: 138
currently lose_sum: 532.9698208868504
time_elpased: 2.671
batch start
#iterations: 139
currently lose_sum: 533.3911156654358
time_elpased: 2.636
start validation test
0.528711340206
0.904761904762
0.0609100310238
0.11413622711
0
validation finish
batch start
#iterations: 140
currently lose_sum: 530.4965580999851
time_elpased: 2.641
batch start
#iterations: 141
currently lose_sum: 533.5731750130653
time_elpased: 2.666
batch start
#iterations: 142
currently lose_sum: 532.3648377358913
time_elpased: 2.656
batch start
#iterations: 143
currently lose_sum: 533.7805160880089
time_elpased: 2.648
batch start
#iterations: 144
currently lose_sum: 533.0614832043648
time_elpased: 2.662
batch start
#iterations: 145
currently lose_sum: 534.5051649212837
time_elpased: 2.657
batch start
#iterations: 146
currently lose_sum: 534.9012912511826
time_elpased: 2.644
batch start
#iterations: 147
currently lose_sum: 534.0402022898197
time_elpased: 2.652
batch start
#iterations: 148
currently lose_sum: 531.8750478625298
time_elpased: 2.632
batch start
#iterations: 149
currently lose_sum: 534.3511972427368
time_elpased: 2.647
batch start
#iterations: 150
currently lose_sum: 533.0386217236519
time_elpased: 2.641
batch start
#iterations: 151
currently lose_sum: 532.1946223974228
time_elpased: 2.639
batch start
#iterations: 152
currently lose_sum: 533.2782616615295
time_elpased: 2.653
batch start
#iterations: 153
currently lose_sum: 531.4549787640572
time_elpased: 2.653
batch start
#iterations: 154
currently lose_sum: 536.181254863739
time_elpased: 2.638
batch start
#iterations: 155
currently lose_sum: 531.955181568861
time_elpased: 2.65
batch start
#iterations: 156
currently lose_sum: 533.3966396450996
time_elpased: 2.652
batch start
#iterations: 157
currently lose_sum: 533.2017161250114
time_elpased: 2.653
batch start
#iterations: 158
currently lose_sum: 532.0677401721478
time_elpased: 2.644
batch start
#iterations: 159
currently lose_sum: 533.336012750864
time_elpased: 2.646
start validation test
0.546597938144
0.898722627737
0.101861427094
0.182983466469
0
validation finish
batch start
#iterations: 160
currently lose_sum: 532.7781412601471
time_elpased: 2.617
batch start
#iterations: 161
currently lose_sum: 533.9765311479568
time_elpased: 2.628
batch start
#iterations: 162
currently lose_sum: 532.5128158330917
time_elpased: 2.647
batch start
#iterations: 163
currently lose_sum: 530.8961170017719
time_elpased: 2.644
batch start
#iterations: 164
currently lose_sum: 534.55558142066
time_elpased: 2.662
batch start
#iterations: 165
currently lose_sum: 532.6533416807652
time_elpased: 2.651
batch start
#iterations: 166
currently lose_sum: 533.5139052569866
time_elpased: 2.643
batch start
#iterations: 167
currently lose_sum: 533.4619425833225
time_elpased: 2.647
batch start
#iterations: 168
currently lose_sum: 532.5273358821869
time_elpased: 2.662
batch start
#iterations: 169
currently lose_sum: 534.3104367256165
time_elpased: 2.652
batch start
#iterations: 170
currently lose_sum: 531.9727884531021
time_elpased: 2.643
batch start
#iterations: 171
currently lose_sum: 532.2430309057236
time_elpased: 2.642
batch start
#iterations: 172
currently lose_sum: 535.2037941217422
time_elpased: 2.643
batch start
#iterations: 173
currently lose_sum: 534.0403753817081
time_elpased: 2.643
batch start
#iterations: 174
currently lose_sum: 533.5957288146019
time_elpased: 2.653
batch start
#iterations: 175
currently lose_sum: 532.9213305711746
time_elpased: 2.639
batch start
#iterations: 176
currently lose_sum: 533.1117930412292
time_elpased: 2.64
batch start
#iterations: 177
currently lose_sum: 534.0051512122154
time_elpased: 2.661
batch start
#iterations: 178
currently lose_sum: 532.6801826357841
time_elpased: 2.661
batch start
#iterations: 179
currently lose_sum: 534.2678355872631
time_elpased: 2.667
start validation test
0.550309278351
0.895484949833
0.110754912099
0.1971286582
0
validation finish
batch start
#iterations: 180
currently lose_sum: 533.3337203264236
time_elpased: 2.65
batch start
#iterations: 181
currently lose_sum: 534.5968337655067
time_elpased: 2.654
batch start
#iterations: 182
currently lose_sum: 533.0134765505791
time_elpased: 2.644
batch start
#iterations: 183
currently lose_sum: 533.7786349058151
time_elpased: 2.66
batch start
#iterations: 184
currently lose_sum: 532.3840682208538
time_elpased: 2.647
batch start
#iterations: 185
currently lose_sum: 531.9757562279701
time_elpased: 2.645
batch start
#iterations: 186
currently lose_sum: 534.4991450309753
time_elpased: 2.661
batch start
#iterations: 187
currently lose_sum: 532.83681589365
time_elpased: 2.663
batch start
#iterations: 188
currently lose_sum: 531.0937443971634
time_elpased: 2.666
batch start
#iterations: 189
currently lose_sum: 533.5189462602139
time_elpased: 2.669
batch start
#iterations: 190
currently lose_sum: 532.1611104011536
time_elpased: 2.653
batch start
#iterations: 191
currently lose_sum: 533.9354702234268
time_elpased: 2.664
batch start
#iterations: 192
currently lose_sum: 532.2008412480354
time_elpased: 2.682
batch start
#iterations: 193
currently lose_sum: 532.5352168083191
time_elpased: 2.669
batch start
#iterations: 194
currently lose_sum: 532.3288894891739
time_elpased: 2.664
batch start
#iterations: 195
currently lose_sum: 533.6317541003227
time_elpased: 2.674
batch start
#iterations: 196
currently lose_sum: 530.791581183672
time_elpased: 2.686
batch start
#iterations: 197
currently lose_sum: 533.7645656466484
time_elpased: 2.655
batch start
#iterations: 198
currently lose_sum: 532.4780080616474
time_elpased: 2.654
batch start
#iterations: 199
currently lose_sum: 532.7114576995373
time_elpased: 2.656
start validation test
0.564690721649
0.908605737158
0.140847983454
0.243889336557
0
validation finish
batch start
#iterations: 200
currently lose_sum: 533.7530167996883
time_elpased: 2.644
batch start
#iterations: 201
currently lose_sum: 533.089088499546
time_elpased: 2.657
batch start
#iterations: 202
currently lose_sum: 533.1942081153393
time_elpased: 2.657
batch start
#iterations: 203
currently lose_sum: 532.9745419621468
time_elpased: 2.652
batch start
#iterations: 204
currently lose_sum: 532.6289339661598
time_elpased: 2.66
batch start
#iterations: 205
currently lose_sum: 533.7451076805592
time_elpased: 2.665
batch start
#iterations: 206
currently lose_sum: 533.874931871891
time_elpased: 2.647
batch start
#iterations: 207
currently lose_sum: 531.8843187093735
time_elpased: 2.648
batch start
#iterations: 208
currently lose_sum: 531.6542222797871
time_elpased: 2.642
batch start
#iterations: 209
currently lose_sum: 531.9102258682251
time_elpased: 2.65
batch start
#iterations: 210
currently lose_sum: 533.4596484303474
time_elpased: 2.665
batch start
#iterations: 211
currently lose_sum: 534.38997605443
time_elpased: 2.644
batch start
#iterations: 212
currently lose_sum: 533.0869133472443
time_elpased: 2.646
batch start
#iterations: 213
currently lose_sum: 532.4067513048649
time_elpased: 2.658
batch start
#iterations: 214
currently lose_sum: 533.6600671708584
time_elpased: 2.655
batch start
#iterations: 215
currently lose_sum: 532.0749426484108
time_elpased: 2.658
batch start
#iterations: 216
currently lose_sum: 534.2230677604675
time_elpased: 2.654
batch start
#iterations: 217
currently lose_sum: 533.1429558396339
time_elpased: 2.662
batch start
#iterations: 218
currently lose_sum: 533.659466445446
time_elpased: 2.654
batch start
#iterations: 219
currently lose_sum: 533.6577522456646
time_elpased: 2.651
start validation test
0.54587628866
0.898148148148
0.100310237849
0.180465116279
0
validation finish
batch start
#iterations: 220
currently lose_sum: 530.7865473926067
time_elpased: 2.658
batch start
#iterations: 221
currently lose_sum: 533.9630940258503
time_elpased: 2.638
batch start
#iterations: 222
currently lose_sum: 533.089413523674
time_elpased: 2.649
batch start
#iterations: 223
currently lose_sum: 532.683315217495
time_elpased: 2.646
batch start
#iterations: 224
currently lose_sum: 533.0368947982788
time_elpased: 2.631
batch start
#iterations: 225
currently lose_sum: 534.2936396598816
time_elpased: 2.656
batch start
#iterations: 226
currently lose_sum: 533.4861920177937
time_elpased: 2.647
batch start
#iterations: 227
currently lose_sum: 532.5569229125977
time_elpased: 2.649
batch start
#iterations: 228
currently lose_sum: 531.3424991965294
time_elpased: 2.657
batch start
#iterations: 229
currently lose_sum: 532.2846347093582
time_elpased: 2.671
batch start
#iterations: 230
currently lose_sum: 533.7429463267326
time_elpased: 2.663
batch start
#iterations: 231
currently lose_sum: 534.8882479071617
time_elpased: 2.677
batch start
#iterations: 232
currently lose_sum: 531.4011441171169
time_elpased: 2.676
batch start
#iterations: 233
currently lose_sum: 530.9262243807316
time_elpased: 2.666
batch start
#iterations: 234
currently lose_sum: 531.1684709191322
time_elpased: 2.678
batch start
#iterations: 235
currently lose_sum: 532.1296476721764
time_elpased: 2.68
batch start
#iterations: 236
currently lose_sum: 531.6459218859673
time_elpased: 2.661
batch start
#iterations: 237
currently lose_sum: 532.745354115963
time_elpased: 2.676
batch start
#iterations: 238
currently lose_sum: 532.128103107214
time_elpased: 2.67
batch start
#iterations: 239
currently lose_sum: 531.4156605601311
time_elpased: 2.643
start validation test
0.525154639175
0.919413919414
0.0519131334023
0.0982772122161
0
validation finish
batch start
#iterations: 240
currently lose_sum: 531.04490467906
time_elpased: 2.657
batch start
#iterations: 241
currently lose_sum: 532.9245499670506
time_elpased: 2.659
batch start
#iterations: 242
currently lose_sum: 530.6013326346874
time_elpased: 2.645
batch start
#iterations: 243
currently lose_sum: 531.1307788491249
time_elpased: 2.663
batch start
#iterations: 244
currently lose_sum: 533.127619177103
time_elpased: 2.641
batch start
#iterations: 245
currently lose_sum: 532.3637549877167
time_elpased: 2.653
batch start
#iterations: 246
currently lose_sum: 531.3742845058441
time_elpased: 2.663
batch start
#iterations: 247
currently lose_sum: 532.1978680491447
time_elpased: 2.655
batch start
#iterations: 248
currently lose_sum: 531.7702451050282
time_elpased: 2.656
batch start
#iterations: 249
currently lose_sum: 531.8819843232632
time_elpased: 2.667
batch start
#iterations: 250
currently lose_sum: 531.2194970846176
time_elpased: 2.671
batch start
#iterations: 251
currently lose_sum: 534.1023240089417
time_elpased: 2.667
batch start
#iterations: 252
currently lose_sum: 533.2688106894493
time_elpased: 2.673
batch start
#iterations: 253
currently lose_sum: 531.2763156294823
time_elpased: 2.688
batch start
#iterations: 254
currently lose_sum: 534.1808055639267
time_elpased: 2.665
batch start
#iterations: 255
currently lose_sum: 530.9162391126156
time_elpased: 2.681
batch start
#iterations: 256
currently lose_sum: 532.0313764810562
time_elpased: 2.688
batch start
#iterations: 257
currently lose_sum: 531.1697444915771
time_elpased: 2.66
batch start
#iterations: 258
currently lose_sum: 533.4821394085884
time_elpased: 2.671
batch start
#iterations: 259
currently lose_sum: 532.4762885868549
time_elpased: 2.673
start validation test
0.575824742268
0.901840490798
0.16721820062
0.282125098142
0
validation finish
batch start
#iterations: 260
currently lose_sum: 534.4880527555943
time_elpased: 2.653
batch start
#iterations: 261
currently lose_sum: 534.2920382618904
time_elpased: 2.663
batch start
#iterations: 262
currently lose_sum: 532.0985456109047
time_elpased: 2.675
batch start
#iterations: 263
currently lose_sum: 532.62422773242
time_elpased: 2.653
batch start
#iterations: 264
currently lose_sum: 532.3137721419334
time_elpased: 2.665
batch start
#iterations: 265
currently lose_sum: 533.3343310952187
time_elpased: 2.659
batch start
#iterations: 266
currently lose_sum: 532.0224381685257
time_elpased: 2.653
batch start
#iterations: 267
currently lose_sum: 530.7616233229637
time_elpased: 2.67
batch start
#iterations: 268
currently lose_sum: 531.3229990899563
time_elpased: 2.661
batch start
#iterations: 269
currently lose_sum: 532.1731831729412
time_elpased: 2.655
batch start
#iterations: 270
currently lose_sum: 530.9930965304375
time_elpased: 2.663
batch start
#iterations: 271
currently lose_sum: 529.9527769088745
time_elpased: 2.662
batch start
#iterations: 272
currently lose_sum: 532.3104253411293
time_elpased: 2.675
batch start
#iterations: 273
currently lose_sum: 529.975369155407
time_elpased: 2.662
batch start
#iterations: 274
currently lose_sum: 532.320940464735
time_elpased: 2.684
batch start
#iterations: 275
currently lose_sum: 531.7718070149422
time_elpased: 2.68
batch start
#iterations: 276
currently lose_sum: 531.8897669911385
time_elpased: 2.675
batch start
#iterations: 277
currently lose_sum: 534.0254186689854
time_elpased: 2.688
batch start
#iterations: 278
currently lose_sum: 532.6321773529053
time_elpased: 2.681
batch start
#iterations: 279
currently lose_sum: 534.5695467591286
time_elpased: 2.658
start validation test
0.54175257732
0.896341463415
0.0912099276112
0.165571616294
0
validation finish
batch start
#iterations: 280
currently lose_sum: 529.9274500012398
time_elpased: 2.658
batch start
#iterations: 281
currently lose_sum: 531.6228840947151
time_elpased: 2.646
batch start
#iterations: 282
currently lose_sum: 533.6778528094292
time_elpased: 2.654
batch start
#iterations: 283
currently lose_sum: 531.6408741772175
time_elpased: 2.667
batch start
#iterations: 284
currently lose_sum: 533.7815496921539
time_elpased: 2.662
batch start
#iterations: 285
currently lose_sum: 532.9008754491806
time_elpased: 2.664
batch start
#iterations: 286
currently lose_sum: 534.0285363793373
time_elpased: 2.653
batch start
#iterations: 287
currently lose_sum: 532.9289438724518
time_elpased: 2.655
batch start
#iterations: 288
currently lose_sum: 532.671952009201
time_elpased: 2.652
batch start
#iterations: 289
currently lose_sum: 532.0534269809723
time_elpased: 2.67
batch start
#iterations: 290
currently lose_sum: 530.9693641662598
time_elpased: 2.65
batch start
#iterations: 291
currently lose_sum: 532.7724077403545
time_elpased: 2.662
batch start
#iterations: 292
currently lose_sum: 532.6504893898964
time_elpased: 2.672
batch start
#iterations: 293
currently lose_sum: 534.0135577917099
time_elpased: 2.651
batch start
#iterations: 294
currently lose_sum: 532.5198546946049
time_elpased: 2.666
batch start
#iterations: 295
currently lose_sum: 532.0937715172768
time_elpased: 2.679
batch start
#iterations: 296
currently lose_sum: 530.4698216319084
time_elpased: 2.669
batch start
#iterations: 297
currently lose_sum: 531.5639138817787
time_elpased: 2.671
batch start
#iterations: 298
currently lose_sum: 531.9940102100372
time_elpased: 2.681
batch start
#iterations: 299
currently lose_sum: 532.2898513674736
time_elpased: 2.67
start validation test
0.563092783505
0.893280632411
0.140227507756
0.242402574187
0
validation finish
batch start
#iterations: 300
currently lose_sum: 531.2593100070953
time_elpased: 2.659
batch start
#iterations: 301
currently lose_sum: 532.0342720150948
time_elpased: 2.662
batch start
#iterations: 302
currently lose_sum: 533.9878147244453
time_elpased: 2.673
batch start
#iterations: 303
currently lose_sum: 535.6322264075279
time_elpased: 2.669
batch start
#iterations: 304
currently lose_sum: 529.8593782186508
time_elpased: 2.682
batch start
#iterations: 305
currently lose_sum: 533.3910254836082
time_elpased: 2.674
batch start
#iterations: 306
currently lose_sum: 532.7805863618851
time_elpased: 2.647
batch start
#iterations: 307
currently lose_sum: 533.8308680653572
time_elpased: 2.676
batch start
#iterations: 308
currently lose_sum: 531.9885196685791
time_elpased: 2.669
batch start
#iterations: 309
currently lose_sum: 533.2762098312378
time_elpased: 2.655
batch start
#iterations: 310
currently lose_sum: 531.3329162299633
time_elpased: 2.678
batch start
#iterations: 311
currently lose_sum: 531.7481260001659
time_elpased: 2.668
batch start
#iterations: 312
currently lose_sum: 531.9879394769669
time_elpased: 2.647
batch start
#iterations: 313
currently lose_sum: 534.0179252028465
time_elpased: 2.673
batch start
#iterations: 314
currently lose_sum: 531.2018327713013
time_elpased: 2.7
batch start
#iterations: 315
currently lose_sum: 531.8653083145618
time_elpased: 2.658
batch start
#iterations: 316
currently lose_sum: 532.2041588127613
time_elpased: 2.669
batch start
#iterations: 317
currently lose_sum: 532.4083334803581
time_elpased: 2.663
batch start
#iterations: 318
currently lose_sum: 533.5593726634979
time_elpased: 2.665
batch start
#iterations: 319
currently lose_sum: 532.3911405801773
time_elpased: 2.659
start validation test
0.558917525773
0.916230366492
0.126680455016
0.222585627328
0
validation finish
batch start
#iterations: 320
currently lose_sum: 532.7024685144424
time_elpased: 2.655
batch start
#iterations: 321
currently lose_sum: 533.3063873052597
time_elpased: 2.669
batch start
#iterations: 322
currently lose_sum: 532.1018716096878
time_elpased: 2.65
batch start
#iterations: 323
currently lose_sum: 530.1903448998928
time_elpased: 2.672
batch start
#iterations: 324
currently lose_sum: 534.7165985703468
time_elpased: 2.66
batch start
#iterations: 325
currently lose_sum: 530.171558201313
time_elpased: 2.685
batch start
#iterations: 326
currently lose_sum: 531.560959070921
time_elpased: 2.695
batch start
#iterations: 327
currently lose_sum: 533.2509416937828
time_elpased: 2.652
batch start
#iterations: 328
currently lose_sum: 533.2451345026493
time_elpased: 2.659
batch start
#iterations: 329
currently lose_sum: 532.1235357820988
time_elpased: 2.656
batch start
#iterations: 330
currently lose_sum: 534.3547887504101
time_elpased: 2.641
batch start
#iterations: 331
currently lose_sum: 534.4944394230843
time_elpased: 2.652
batch start
#iterations: 332
currently lose_sum: 533.0178400576115
time_elpased: 2.67
batch start
#iterations: 333
currently lose_sum: 530.7405133247375
time_elpased: 2.667
batch start
#iterations: 334
currently lose_sum: 531.77608025074
time_elpased: 2.668
batch start
#iterations: 335
currently lose_sum: 530.6994277834892
time_elpased: 2.677
batch start
#iterations: 336
currently lose_sum: 533.8403534293175
time_elpased: 2.665
batch start
#iterations: 337
currently lose_sum: 532.5573033094406
time_elpased: 2.661
batch start
#iterations: 338
currently lose_sum: 531.3316494822502
time_elpased: 2.672
batch start
#iterations: 339
currently lose_sum: 532.8192611634731
time_elpased: 2.67
start validation test
0.556907216495
0.908054711246
0.123578076525
0.217549608593
0
validation finish
batch start
#iterations: 340
currently lose_sum: 532.4906024038792
time_elpased: 2.658
batch start
#iterations: 341
currently lose_sum: 531.9398573637009
time_elpased: 2.673
batch start
#iterations: 342
currently lose_sum: 534.7246935069561
time_elpased: 2.663
batch start
#iterations: 343
currently lose_sum: 532.8578423857689
time_elpased: 2.652
batch start
#iterations: 344
currently lose_sum: 532.3671147227287
time_elpased: 2.668
batch start
#iterations: 345
currently lose_sum: 532.0208252668381
time_elpased: 2.647
batch start
#iterations: 346
currently lose_sum: 530.079759657383
time_elpased: 2.658
batch start
#iterations: 347
currently lose_sum: 532.0590616762638
time_elpased: 2.658
batch start
#iterations: 348
currently lose_sum: 531.9660739004612
time_elpased: 2.646
batch start
#iterations: 349
currently lose_sum: 532.1051835715771
time_elpased: 2.658
batch start
#iterations: 350
currently lose_sum: 532.408523440361
time_elpased: 2.669
batch start
#iterations: 351
currently lose_sum: 531.5029714405537
time_elpased: 2.655
batch start
#iterations: 352
currently lose_sum: 531.4900327324867
time_elpased: 2.666
batch start
#iterations: 353
currently lose_sum: 532.3819395899773
time_elpased: 2.668
batch start
#iterations: 354
currently lose_sum: 531.7196925878525
time_elpased: 2.666
batch start
#iterations: 355
currently lose_sum: 531.8190721571445
time_elpased: 2.667
batch start
#iterations: 356
currently lose_sum: 532.8746578097343
time_elpased: 2.691
batch start
#iterations: 357
currently lose_sum: 531.973856985569
time_elpased: 2.641
batch start
#iterations: 358
currently lose_sum: 532.5278605818748
time_elpased: 2.657
batch start
#iterations: 359
currently lose_sum: 531.7220175266266
time_elpased: 2.649
start validation test
0.560773195876
0.909479686386
0.131954498449
0.230470513863
0
validation finish
batch start
#iterations: 360
currently lose_sum: 532.6829535067081
time_elpased: 2.659
batch start
#iterations: 361
currently lose_sum: 530.4856361150742
time_elpased: 2.659
batch start
#iterations: 362
currently lose_sum: 532.1468391418457
time_elpased: 2.652
batch start
#iterations: 363
currently lose_sum: 532.0171012282372
time_elpased: 2.653
batch start
#iterations: 364
currently lose_sum: 532.9601091146469
time_elpased: 2.663
batch start
#iterations: 365
currently lose_sum: 531.6343860626221
time_elpased: 2.664
batch start
#iterations: 366
currently lose_sum: 533.1767348051071
time_elpased: 2.664
batch start
#iterations: 367
currently lose_sum: 533.6093935370445
time_elpased: 2.65
batch start
#iterations: 368
currently lose_sum: 530.3179016411304
time_elpased: 2.66
batch start
#iterations: 369
currently lose_sum: 531.520176410675
time_elpased: 2.665
batch start
#iterations: 370
currently lose_sum: 531.3960684239864
time_elpased: 2.661
batch start
#iterations: 371
currently lose_sum: 532.2648794949055
time_elpased: 2.674
batch start
#iterations: 372
currently lose_sum: 531.7024993300438
time_elpased: 2.668
batch start
#iterations: 373
currently lose_sum: 532.5943053662777
time_elpased: 2.667
batch start
#iterations: 374
currently lose_sum: 534.009414255619
time_elpased: 2.673
batch start
#iterations: 375
currently lose_sum: 533.7791209220886
time_elpased: 2.672
batch start
#iterations: 376
currently lose_sum: 532.6919303834438
time_elpased: 2.657
batch start
#iterations: 377
currently lose_sum: 532.4953970611095
time_elpased: 2.661
batch start
#iterations: 378
currently lose_sum: 533.1408104896545
time_elpased: 2.667
batch start
#iterations: 379
currently lose_sum: 530.5599048137665
time_elpased: 2.667
start validation test
0.55824742268
0.906804733728
0.126783867632
0.222464162584
0
validation finish
batch start
#iterations: 380
currently lose_sum: 532.8618072271347
time_elpased: 2.66
batch start
#iterations: 381
currently lose_sum: 531.1836202442646
time_elpased: 2.655
batch start
#iterations: 382
currently lose_sum: 533.2012563347816
time_elpased: 2.646
batch start
#iterations: 383
currently lose_sum: 530.3614536225796
time_elpased: 2.66
batch start
#iterations: 384
currently lose_sum: 530.36048874259
time_elpased: 2.664
batch start
#iterations: 385
currently lose_sum: 531.0525172352791
time_elpased: 2.648
batch start
#iterations: 386
currently lose_sum: 532.0813208520412
time_elpased: 2.666
batch start
#iterations: 387
currently lose_sum: 531.9558447897434
time_elpased: 2.662
batch start
#iterations: 388
currently lose_sum: 532.3787591457367
time_elpased: 2.631
batch start
#iterations: 389
currently lose_sum: 531.7977559864521
time_elpased: 2.662
batch start
#iterations: 390
currently lose_sum: 531.7196800708771
time_elpased: 2.653
batch start
#iterations: 391
currently lose_sum: 532.934771001339
time_elpased: 2.648
batch start
#iterations: 392
currently lose_sum: 531.2619751095772
time_elpased: 2.687
batch start
#iterations: 393
currently lose_sum: 531.9456521868706
time_elpased: 2.68
batch start
#iterations: 394
currently lose_sum: 532.3925350308418
time_elpased: 2.664
batch start
#iterations: 395
currently lose_sum: 531.1525767445564
time_elpased: 2.671
batch start
#iterations: 396
currently lose_sum: 531.3639176487923
time_elpased: 2.667
batch start
#iterations: 397
currently lose_sum: 532.5050475895405
time_elpased: 2.682
batch start
#iterations: 398
currently lose_sum: 532.1261369884014
time_elpased: 2.68
batch start
#iterations: 399
currently lose_sum: 531.0732482969761
time_elpased: 2.652
start validation test
0.550618556701
0.894039735099
0.111685625646
0.198565912852
0
validation finish
acc: 0.567
pre: 0.892
rec: 0.162
F1: 0.275
auc: 0.000
