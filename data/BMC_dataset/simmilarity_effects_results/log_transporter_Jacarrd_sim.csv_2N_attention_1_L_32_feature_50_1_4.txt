start to construct graph
graph construct over
87453
epochs start
batch start
#iterations: 0
currently lose_sum: 571.2782992720604
time_elpased: 2.626
batch start
#iterations: 1
currently lose_sum: 552.5511211752892
time_elpased: 2.644
batch start
#iterations: 2
currently lose_sum: 546.5834769010544
time_elpased: 2.642
batch start
#iterations: 3
currently lose_sum: 545.1186879873276
time_elpased: 2.619
batch start
#iterations: 4
currently lose_sum: 540.8767166137695
time_elpased: 2.634
batch start
#iterations: 5
currently lose_sum: 542.0689585208893
time_elpased: 2.642
batch start
#iterations: 6
currently lose_sum: 540.3389264345169
time_elpased: 2.639
batch start
#iterations: 7
currently lose_sum: 539.7207859754562
time_elpased: 2.66
batch start
#iterations: 8
currently lose_sum: 540.2856795191765
time_elpased: 2.653
batch start
#iterations: 9
currently lose_sum: 538.7490358352661
time_elpased: 2.626
batch start
#iterations: 10
currently lose_sum: 538.2515487074852
time_elpased: 2.659
batch start
#iterations: 11
currently lose_sum: 539.4329532980919
time_elpased: 2.651
batch start
#iterations: 12
currently lose_sum: 539.9850487411022
time_elpased: 2.624
batch start
#iterations: 13
currently lose_sum: 536.019288957119
time_elpased: 2.646
batch start
#iterations: 14
currently lose_sum: 538.2616288363934
time_elpased: 2.631
batch start
#iterations: 15
currently lose_sum: 536.4506088495255
time_elpased: 2.624
batch start
#iterations: 16
currently lose_sum: 538.4588030576706
time_elpased: 2.644
batch start
#iterations: 17
currently lose_sum: 537.7462078034878
time_elpased: 2.638
batch start
#iterations: 18
currently lose_sum: 536.5208976864815
time_elpased: 2.633
batch start
#iterations: 19
currently lose_sum: 537.0612843334675
time_elpased: 2.657
start validation test
0.554175257732
0.890459363958
0.129177773221
0.225624496374
0
validation finish
batch start
#iterations: 20
currently lose_sum: 537.5185426473618
time_elpased: 2.636
batch start
#iterations: 21
currently lose_sum: 538.0994408726692
time_elpased: 2.615
batch start
#iterations: 22
currently lose_sum: 537.851062297821
time_elpased: 2.645
batch start
#iterations: 23
currently lose_sum: 535.9959732294083
time_elpased: 2.636
batch start
#iterations: 24
currently lose_sum: 536.8514430522919
time_elpased: 2.638
batch start
#iterations: 25
currently lose_sum: 536.411779999733
time_elpased: 2.664
batch start
#iterations: 26
currently lose_sum: 534.0674630999565
time_elpased: 2.64
batch start
#iterations: 27
currently lose_sum: 537.1045060753822
time_elpased: 2.642
batch start
#iterations: 28
currently lose_sum: 536.199792265892
time_elpased: 2.649
batch start
#iterations: 29
currently lose_sum: 534.3198388814926
time_elpased: 2.644
batch start
#iterations: 30
currently lose_sum: 534.6869197487831
time_elpased: 2.639
batch start
#iterations: 31
currently lose_sum: 536.3970544338226
time_elpased: 2.651
batch start
#iterations: 32
currently lose_sum: 536.1543132066727
time_elpased: 2.647
batch start
#iterations: 33
currently lose_sum: 536.3007818460464
time_elpased: 2.644
batch start
#iterations: 34
currently lose_sum: 535.4325413405895
time_elpased: 2.664
batch start
#iterations: 35
currently lose_sum: 536.083492487669
time_elpased: 2.664
batch start
#iterations: 36
currently lose_sum: 535.25923833251
time_elpased: 2.665
batch start
#iterations: 37
currently lose_sum: 534.3801121115685
time_elpased: 2.669
batch start
#iterations: 38
currently lose_sum: 536.2335029542446
time_elpased: 2.663
batch start
#iterations: 39
currently lose_sum: 536.9797708392143
time_elpased: 2.654
start validation test
0.544020618557
0.869706840391
0.109493541111
0.194500091058
0
validation finish
batch start
#iterations: 40
currently lose_sum: 535.9250196814537
time_elpased: 2.661
batch start
#iterations: 41
currently lose_sum: 535.222917765379
time_elpased: 2.645
batch start
#iterations: 42
currently lose_sum: 535.8766526579857
time_elpased: 2.651
batch start
#iterations: 43
currently lose_sum: 535.8860836625099
time_elpased: 2.643
batch start
#iterations: 44
currently lose_sum: 533.7178137600422
time_elpased: 2.651
batch start
#iterations: 45
currently lose_sum: 533.8594003915787
time_elpased: 2.629
batch start
#iterations: 46
currently lose_sum: 538.4163101315498
time_elpased: 2.643
batch start
#iterations: 47
currently lose_sum: 535.7238817214966
time_elpased: 2.632
batch start
#iterations: 48
currently lose_sum: 532.5596713423729
time_elpased: 2.632
batch start
#iterations: 49
currently lose_sum: 534.475359082222
time_elpased: 2.659
batch start
#iterations: 50
currently lose_sum: 532.4939569532871
time_elpased: 2.642
batch start
#iterations: 51
currently lose_sum: 534.1580476164818
time_elpased: 2.625
batch start
#iterations: 52
currently lose_sum: 535.9630489051342
time_elpased: 2.651
batch start
#iterations: 53
currently lose_sum: 535.4568434357643
time_elpased: 2.635
batch start
#iterations: 54
currently lose_sum: 535.5044758319855
time_elpased: 2.622
batch start
#iterations: 55
currently lose_sum: 533.4167116880417
time_elpased: 2.639
batch start
#iterations: 56
currently lose_sum: 534.1768970787525
time_elpased: 2.633
batch start
#iterations: 57
currently lose_sum: 534.3330959379673
time_elpased: 2.626
batch start
#iterations: 58
currently lose_sum: 534.6641946434975
time_elpased: 2.632
batch start
#iterations: 59
currently lose_sum: 533.369164288044
time_elpased: 2.63
start validation test
0.51793814433
0.925847457627
0.0448021324585
0.0854684138471
0
validation finish
batch start
#iterations: 60
currently lose_sum: 535.0102234482765
time_elpased: 2.619
batch start
#iterations: 61
currently lose_sum: 533.3287215828896
time_elpased: 2.649
batch start
#iterations: 62
currently lose_sum: 534.2702985405922
time_elpased: 2.645
batch start
#iterations: 63
currently lose_sum: 535.7225645184517
time_elpased: 2.625
batch start
#iterations: 64
currently lose_sum: 533.57400816679
time_elpased: 2.648
batch start
#iterations: 65
currently lose_sum: 534.4317441284657
time_elpased: 2.631
batch start
#iterations: 66
currently lose_sum: 535.7095133662224
time_elpased: 2.638
batch start
#iterations: 67
currently lose_sum: 532.6390993893147
time_elpased: 2.629
batch start
#iterations: 68
currently lose_sum: 532.9143681824207
time_elpased: 2.629
batch start
#iterations: 69
currently lose_sum: 533.7619731426239
time_elpased: 2.618
batch start
#iterations: 70
currently lose_sum: 534.7855734825134
time_elpased: 2.639
batch start
#iterations: 71
currently lose_sum: 534.1872974336147
time_elpased: 2.639
batch start
#iterations: 72
currently lose_sum: 533.3032450675964
time_elpased: 2.63
batch start
#iterations: 73
currently lose_sum: 533.9480363726616
time_elpased: 2.644
batch start
#iterations: 74
currently lose_sum: 533.2975219786167
time_elpased: 2.64
batch start
#iterations: 75
currently lose_sum: 533.3399401307106
time_elpased: 2.645
batch start
#iterations: 76
currently lose_sum: 534.7072446942329
time_elpased: 2.649
batch start
#iterations: 77
currently lose_sum: 533.2447067499161
time_elpased: 2.642
batch start
#iterations: 78
currently lose_sum: 534.2291861772537
time_elpased: 2.647
batch start
#iterations: 79
currently lose_sum: 534.7947724461555
time_elpased: 2.658
start validation test
0.547783505155
0.909090909091
0.111749026041
0.199032228613
0
validation finish
batch start
#iterations: 80
currently lose_sum: 534.1276509165764
time_elpased: 2.641
batch start
#iterations: 81
currently lose_sum: 534.4586053192616
time_elpased: 2.643
batch start
#iterations: 82
currently lose_sum: 533.2358321547508
time_elpased: 2.652
batch start
#iterations: 83
currently lose_sum: 532.0000213980675
time_elpased: 2.646
batch start
#iterations: 84
currently lose_sum: 532.7041248679161
time_elpased: 2.631
batch start
#iterations: 85
currently lose_sum: 533.6280499994755
time_elpased: 2.653
batch start
#iterations: 86
currently lose_sum: 532.5936840474606
time_elpased: 2.643
batch start
#iterations: 87
currently lose_sum: 533.5639033019543
time_elpased: 2.637
batch start
#iterations: 88
currently lose_sum: 534.4572994112968
time_elpased: 2.643
batch start
#iterations: 89
currently lose_sum: 532.2585055828094
time_elpased: 2.629
batch start
#iterations: 90
currently lose_sum: 532.5897605121136
time_elpased: 2.653
batch start
#iterations: 91
currently lose_sum: 531.7130744457245
time_elpased: 2.645
batch start
#iterations: 92
currently lose_sum: 531.299289137125
time_elpased: 2.641
batch start
#iterations: 93
currently lose_sum: 532.9348905682564
time_elpased: 2.647
batch start
#iterations: 94
currently lose_sum: 533.4405003786087
time_elpased: 2.648
batch start
#iterations: 95
currently lose_sum: 533.867908269167
time_elpased: 2.64
batch start
#iterations: 96
currently lose_sum: 532.7963532805443
time_elpased: 2.652
batch start
#iterations: 97
currently lose_sum: 532.9284105598927
time_elpased: 2.644
batch start
#iterations: 98
currently lose_sum: 530.134098649025
time_elpased: 2.645
batch start
#iterations: 99
currently lose_sum: 533.66620439291
time_elpased: 2.644
start validation test
0.528659793814
0.913279132791
0.0690998564691
0.128478841022
0
validation finish
batch start
#iterations: 100
currently lose_sum: 533.8412269055843
time_elpased: 2.653
batch start
#iterations: 101
currently lose_sum: 532.3274266421795
time_elpased: 2.656
batch start
#iterations: 102
currently lose_sum: 535.2117756605148
time_elpased: 2.65
batch start
#iterations: 103
currently lose_sum: 531.8116152882576
time_elpased: 2.645
batch start
#iterations: 104
currently lose_sum: 530.7041129171848
time_elpased: 2.657
batch start
#iterations: 105
currently lose_sum: 531.0757223367691
time_elpased: 2.652
batch start
#iterations: 106
currently lose_sum: 532.1026809215546
time_elpased: 2.64
batch start
#iterations: 107
currently lose_sum: 533.1507825255394
time_elpased: 2.651
batch start
#iterations: 108
currently lose_sum: 531.3349303901196
time_elpased: 2.64
batch start
#iterations: 109
currently lose_sum: 535.5065757334232
time_elpased: 2.645
batch start
#iterations: 110
currently lose_sum: 533.2025700211525
time_elpased: 2.65
batch start
#iterations: 111
currently lose_sum: 534.2624990046024
time_elpased: 2.646
batch start
#iterations: 112
currently lose_sum: 533.4322062432766
time_elpased: 2.658
batch start
#iterations: 113
currently lose_sum: 533.6448495388031
time_elpased: 2.627
batch start
#iterations: 114
currently lose_sum: 532.8308016657829
time_elpased: 2.633
batch start
#iterations: 115
currently lose_sum: 532.6092235445976
time_elpased: 2.66
batch start
#iterations: 116
currently lose_sum: 534.193301141262
time_elpased: 2.655
batch start
#iterations: 117
currently lose_sum: 531.3929226994514
time_elpased: 2.644
batch start
#iterations: 118
currently lose_sum: 532.788112193346
time_elpased: 2.662
batch start
#iterations: 119
currently lose_sum: 531.6889470517635
time_elpased: 2.644
start validation test
0.551443298969
0.926256077796
0.117182694279
0.208045140153
0
validation finish
batch start
#iterations: 120
currently lose_sum: 531.9720799922943
time_elpased: 2.645
batch start
#iterations: 121
currently lose_sum: 533.5528832674026
time_elpased: 2.699
batch start
#iterations: 122
currently lose_sum: 534.6393849849701
time_elpased: 2.684
batch start
#iterations: 123
currently lose_sum: 533.8739220499992
time_elpased: 2.634
batch start
#iterations: 124
currently lose_sum: 531.6694016456604
time_elpased: 2.665
batch start
#iterations: 125
currently lose_sum: 531.9961994588375
time_elpased: 2.643
batch start
#iterations: 126
currently lose_sum: 530.9043911993504
time_elpased: 2.636
batch start
#iterations: 127
currently lose_sum: 531.9777929782867
time_elpased: 2.659
batch start
#iterations: 128
currently lose_sum: 534.8084397315979
time_elpased: 2.651
batch start
#iterations: 129
currently lose_sum: 531.4608036279678
time_elpased: 2.625
batch start
#iterations: 130
currently lose_sum: 532.1015437841415
time_elpased: 2.65
batch start
#iterations: 131
currently lose_sum: 532.4377088844776
time_elpased: 2.632
batch start
#iterations: 132
currently lose_sum: 532.0933858752251
time_elpased: 2.628
batch start
#iterations: 133
currently lose_sum: 532.4166693091393
time_elpased: 2.645
batch start
#iterations: 134
currently lose_sum: 533.1357773840427
time_elpased: 2.647
batch start
#iterations: 135
currently lose_sum: 531.6472102403641
time_elpased: 2.648
batch start
#iterations: 136
currently lose_sum: 532.6063986122608
time_elpased: 2.66
batch start
#iterations: 137
currently lose_sum: 531.5877288579941
time_elpased: 2.655
batch start
#iterations: 138
currently lose_sum: 533.0701344311237
time_elpased: 2.634
batch start
#iterations: 139
currently lose_sum: 533.1830332875252
time_elpased: 2.654
start validation test
0.558917525773
0.897146648971
0.138609801107
0.240120770802
0
validation finish
batch start
#iterations: 140
currently lose_sum: 532.0382583737373
time_elpased: 2.643
batch start
#iterations: 141
currently lose_sum: 532.7000694274902
time_elpased: 2.62
batch start
#iterations: 142
currently lose_sum: 532.3890351355076
time_elpased: 2.668
batch start
#iterations: 143
currently lose_sum: 532.7576514482498
time_elpased: 2.645
batch start
#iterations: 144
currently lose_sum: 532.9852787256241
time_elpased: 2.641
batch start
#iterations: 145
currently lose_sum: 532.7441304624081
time_elpased: 2.658
batch start
#iterations: 146
currently lose_sum: 532.301639020443
time_elpased: 2.648
batch start
#iterations: 147
currently lose_sum: 530.1163068711758
time_elpased: 2.654
batch start
#iterations: 148
currently lose_sum: 532.8770980536938
time_elpased: 2.655
batch start
#iterations: 149
currently lose_sum: 533.2459355294704
time_elpased: 2.659
batch start
#iterations: 150
currently lose_sum: 532.2388674914837
time_elpased: 2.639
batch start
#iterations: 151
currently lose_sum: 530.0916385352612
time_elpased: 2.664
batch start
#iterations: 152
currently lose_sum: 531.490061044693
time_elpased: 2.67
batch start
#iterations: 153
currently lose_sum: 529.612082183361
time_elpased: 2.666
batch start
#iterations: 154
currently lose_sum: 533.2482233345509
time_elpased: 2.666
batch start
#iterations: 155
currently lose_sum: 533.0539437532425
time_elpased: 2.661
batch start
#iterations: 156
currently lose_sum: 531.8192440867424
time_elpased: 2.669
batch start
#iterations: 157
currently lose_sum: 530.3436317741871
time_elpased: 2.66
batch start
#iterations: 158
currently lose_sum: 532.7729533016682
time_elpased: 2.678
batch start
#iterations: 159
currently lose_sum: 531.9287276864052
time_elpased: 2.664
start validation test
0.539381443299
0.924273858921
0.091347139635
0.166262362381
0
validation finish
batch start
#iterations: 160
currently lose_sum: 533.049120515585
time_elpased: 2.664
batch start
#iterations: 161
currently lose_sum: 532.4681057035923
time_elpased: 2.649
batch start
#iterations: 162
currently lose_sum: 531.5290004014969
time_elpased: 2.655
batch start
#iterations: 163
currently lose_sum: 531.7834393680096
time_elpased: 2.658
batch start
#iterations: 164
currently lose_sum: 531.919519841671
time_elpased: 2.667
batch start
#iterations: 165
currently lose_sum: 534.2147851586342
time_elpased: 2.638
batch start
#iterations: 166
currently lose_sum: 533.1345180869102
time_elpased: 2.655
batch start
#iterations: 167
currently lose_sum: 533.3816487789154
time_elpased: 2.659
batch start
#iterations: 168
currently lose_sum: 531.2309684753418
time_elpased: 2.652
batch start
#iterations: 169
currently lose_sum: 531.0484417080879
time_elpased: 2.656
batch start
#iterations: 170
currently lose_sum: 533.077303647995
time_elpased: 2.642
batch start
#iterations: 171
currently lose_sum: 533.5767315924168
time_elpased: 2.627
batch start
#iterations: 172
currently lose_sum: 532.0606101751328
time_elpased: 2.657
batch start
#iterations: 173
currently lose_sum: 531.3289981782436
time_elpased: 2.642
batch start
#iterations: 174
currently lose_sum: 533.6110723614693
time_elpased: 2.636
batch start
#iterations: 175
currently lose_sum: 533.1443684697151
time_elpased: 2.655
batch start
#iterations: 176
currently lose_sum: 533.4180153608322
time_elpased: 2.654
batch start
#iterations: 177
currently lose_sum: 532.2804005146027
time_elpased: 2.654
batch start
#iterations: 178
currently lose_sum: 531.962463259697
time_elpased: 2.674
batch start
#iterations: 179
currently lose_sum: 533.5706753134727
time_elpased: 2.656
start validation test
0.543195876289
0.94246031746
0.0973959401271
0.176547110203
0
validation finish
batch start
#iterations: 180
currently lose_sum: 532.9959886670113
time_elpased: 2.644
batch start
#iterations: 181
currently lose_sum: 533.6908674240112
time_elpased: 2.65
batch start
#iterations: 182
currently lose_sum: 531.8059223294258
time_elpased: 2.649
batch start
#iterations: 183
currently lose_sum: 531.063231408596
time_elpased: 2.636
batch start
#iterations: 184
currently lose_sum: 532.3063979744911
time_elpased: 2.661
batch start
#iterations: 185
currently lose_sum: 531.2109382152557
time_elpased: 2.643
batch start
#iterations: 186
currently lose_sum: 530.8261942267418
time_elpased: 2.642
batch start
#iterations: 187
currently lose_sum: 534.0950568318367
time_elpased: 2.668
batch start
#iterations: 188
currently lose_sum: 531.8722487092018
time_elpased: 2.669
batch start
#iterations: 189
currently lose_sum: 531.194160848856
time_elpased: 2.645
batch start
#iterations: 190
currently lose_sum: 532.0869753658772
time_elpased: 2.676
batch start
#iterations: 191
currently lose_sum: 531.5178752243519
time_elpased: 2.648
batch start
#iterations: 192
currently lose_sum: 531.0876717567444
time_elpased: 2.655
batch start
#iterations: 193
currently lose_sum: 531.7705257833004
time_elpased: 2.661
batch start
#iterations: 194
currently lose_sum: 532.5486212968826
time_elpased: 2.654
batch start
#iterations: 195
currently lose_sum: 531.0753547251225
time_elpased: 2.636
batch start
#iterations: 196
currently lose_sum: 532.6544705331326
time_elpased: 2.654
batch start
#iterations: 197
currently lose_sum: 532.2985381484032
time_elpased: 2.649
batch start
#iterations: 198
currently lose_sum: 531.5306938290596
time_elpased: 2.645
batch start
#iterations: 199
currently lose_sum: 534.4990314245224
time_elpased: 2.659
start validation test
0.56206185567
0.926729986431
0.140045109699
0.243320270752
0
validation finish
batch start
#iterations: 200
currently lose_sum: 527.5911182165146
time_elpased: 2.668
batch start
#iterations: 201
currently lose_sum: 532.4333550333977
time_elpased: 2.637
batch start
#iterations: 202
currently lose_sum: 531.3680366873741
time_elpased: 2.652
batch start
#iterations: 203
currently lose_sum: 533.1777884960175
time_elpased: 2.66
batch start
#iterations: 204
currently lose_sum: 531.5586729347706
time_elpased: 2.641
batch start
#iterations: 205
currently lose_sum: 531.5295590162277
time_elpased: 2.655
batch start
#iterations: 206
currently lose_sum: 530.089702963829
time_elpased: 2.654
batch start
#iterations: 207
currently lose_sum: 530.9535894095898
time_elpased: 2.637
batch start
#iterations: 208
currently lose_sum: 531.9682897925377
time_elpased: 2.652
batch start
#iterations: 209
currently lose_sum: 531.330982208252
time_elpased: 2.674
batch start
#iterations: 210
currently lose_sum: 530.7452840805054
time_elpased: 2.662
batch start
#iterations: 211
currently lose_sum: 531.6946806013584
time_elpased: 2.674
batch start
#iterations: 212
currently lose_sum: 530.9758612513542
time_elpased: 2.669
batch start
#iterations: 213
currently lose_sum: 532.8020715415478
time_elpased: 2.65
batch start
#iterations: 214
currently lose_sum: 531.5038754343987
time_elpased: 2.661
batch start
#iterations: 215
currently lose_sum: 532.901073127985
time_elpased: 2.643
batch start
#iterations: 216
currently lose_sum: 532.5318951308727
time_elpased: 2.641
batch start
#iterations: 217
currently lose_sum: 533.0676310062408
time_elpased: 2.685
batch start
#iterations: 218
currently lose_sum: 532.2562871277332
time_elpased: 2.66
batch start
#iterations: 219
currently lose_sum: 532.31343922019
time_elpased: 2.665
start validation test
0.540463917526
0.917412935323
0.0945253229444
0.171391393252
0
validation finish
batch start
#iterations: 220
currently lose_sum: 531.622061252594
time_elpased: 2.665
batch start
#iterations: 221
currently lose_sum: 534.2465631663799
time_elpased: 2.665
batch start
#iterations: 222
currently lose_sum: 533.1241157650948
time_elpased: 2.643
batch start
#iterations: 223
currently lose_sum: 531.6461151838303
time_elpased: 2.651
batch start
#iterations: 224
currently lose_sum: 532.965050548315
time_elpased: 2.662
batch start
#iterations: 225
currently lose_sum: 531.884107530117
time_elpased: 2.646
batch start
#iterations: 226
currently lose_sum: 531.5326351225376
time_elpased: 2.65
batch start
#iterations: 227
currently lose_sum: 530.6479955911636
time_elpased: 2.66
batch start
#iterations: 228
currently lose_sum: 529.9522819519043
time_elpased: 2.66
batch start
#iterations: 229
currently lose_sum: 530.0600159168243
time_elpased: 2.651
batch start
#iterations: 230
currently lose_sum: 531.1231651902199
time_elpased: 2.664
batch start
#iterations: 231
currently lose_sum: 532.8403281867504
time_elpased: 2.64
batch start
#iterations: 232
currently lose_sum: 532.1339247226715
time_elpased: 2.653
batch start
#iterations: 233
currently lose_sum: 533.5014398694038
time_elpased: 2.658
batch start
#iterations: 234
currently lose_sum: 533.80819272995
time_elpased: 2.643
batch start
#iterations: 235
currently lose_sum: 530.7783880233765
time_elpased: 2.651
batch start
#iterations: 236
currently lose_sum: 529.3582145869732
time_elpased: 2.668
batch start
#iterations: 237
currently lose_sum: 531.5499866604805
time_elpased: 2.657
batch start
#iterations: 238
currently lose_sum: 530.4208714365959
time_elpased: 2.649
batch start
#iterations: 239
currently lose_sum: 532.3093183636665
time_elpased: 2.654
start validation test
0.559329896907
0.911262798635
0.136866926389
0.237989125591
0
validation finish
batch start
#iterations: 240
currently lose_sum: 530.706299751997
time_elpased: 2.652
batch start
#iterations: 241
currently lose_sum: 531.8351738750935
time_elpased: 2.653
batch start
#iterations: 242
currently lose_sum: 532.5657217502594
time_elpased: 2.67
batch start
#iterations: 243
currently lose_sum: 530.4075638055801
time_elpased: 2.652
batch start
#iterations: 244
currently lose_sum: 533.9117721915245
time_elpased: 2.66
batch start
#iterations: 245
currently lose_sum: 532.0213156938553
time_elpased: 2.667
batch start
#iterations: 246
currently lose_sum: 532.5236900150776
time_elpased: 2.651
batch start
#iterations: 247
currently lose_sum: 532.8841380774975
time_elpased: 2.651
batch start
#iterations: 248
currently lose_sum: 530.4300947785378
time_elpased: 2.668
batch start
#iterations: 249
currently lose_sum: 533.2253348827362
time_elpased: 2.655
batch start
#iterations: 250
currently lose_sum: 532.3196904063225
time_elpased: 2.66
batch start
#iterations: 251
currently lose_sum: 531.555048584938
time_elpased: 2.669
batch start
#iterations: 252
currently lose_sum: 531.4234523773193
time_elpased: 2.654
batch start
#iterations: 253
currently lose_sum: 530.2479889690876
time_elpased: 2.656
batch start
#iterations: 254
currently lose_sum: 530.6643334627151
time_elpased: 2.647
batch start
#iterations: 255
currently lose_sum: 530.8619122505188
time_elpased: 2.642
batch start
#iterations: 256
currently lose_sum: 530.4473984539509
time_elpased: 2.664
batch start
#iterations: 257
currently lose_sum: 529.9035910964012
time_elpased: 2.665
batch start
#iterations: 258
currently lose_sum: 529.4544740915298
time_elpased: 2.65
batch start
#iterations: 259
currently lose_sum: 533.7557192146778
time_elpased: 2.666
start validation test
0.560309278351
0.922068965517
0.137071970474
0.238664762585
0
validation finish
batch start
#iterations: 260
currently lose_sum: 529.9804722070694
time_elpased: 2.653
batch start
#iterations: 261
currently lose_sum: 531.475110322237
time_elpased: 2.652
batch start
#iterations: 262
currently lose_sum: 532.8828258514404
time_elpased: 2.636
batch start
#iterations: 263
currently lose_sum: 531.6821228563786
time_elpased: 2.655
batch start
#iterations: 264
currently lose_sum: 533.8442515134811
time_elpased: 2.669
batch start
#iterations: 265
currently lose_sum: 531.9946105480194
time_elpased: 2.64
batch start
#iterations: 266
currently lose_sum: 531.3962124288082
time_elpased: 2.664
batch start
#iterations: 267
currently lose_sum: 532.8215077519417
time_elpased: 2.647
batch start
#iterations: 268
currently lose_sum: 529.604077398777
time_elpased: 2.646
batch start
#iterations: 269
currently lose_sum: 530.3069912493229
time_elpased: 2.652
batch start
#iterations: 270
currently lose_sum: 531.9138391017914
time_elpased: 2.648
batch start
#iterations: 271
currently lose_sum: 532.9025921523571
time_elpased: 2.642
batch start
#iterations: 272
currently lose_sum: 531.8053779304028
time_elpased: 2.656
batch start
#iterations: 273
currently lose_sum: 531.1749614477158
time_elpased: 2.63
batch start
#iterations: 274
currently lose_sum: 533.2142837643623
time_elpased: 2.643
batch start
#iterations: 275
currently lose_sum: 530.6791521012783
time_elpased: 2.662
batch start
#iterations: 276
currently lose_sum: 532.643258959055
time_elpased: 2.655
batch start
#iterations: 277
currently lose_sum: 532.8346033990383
time_elpased: 2.651
batch start
#iterations: 278
currently lose_sum: 532.0691196322441
time_elpased: 2.658
batch start
#iterations: 279
currently lose_sum: 530.7723211050034
time_elpased: 2.662
start validation test
0.580618556701
0.906532663317
0.184949764199
0.307220708447
0
validation finish
batch start
#iterations: 280
currently lose_sum: 532.5979121923447
time_elpased: 2.634
batch start
#iterations: 281
currently lose_sum: 530.8798196017742
time_elpased: 2.646
batch start
#iterations: 282
currently lose_sum: 531.7009217143059
time_elpased: 2.646
batch start
#iterations: 283
currently lose_sum: 533.009835511446
time_elpased: 2.642
batch start
#iterations: 284
currently lose_sum: 530.7525484263897
time_elpased: 2.664
batch start
#iterations: 285
currently lose_sum: 529.856338351965
time_elpased: 2.661
batch start
#iterations: 286
currently lose_sum: 529.8428254723549
time_elpased: 2.653
batch start
#iterations: 287
currently lose_sum: 532.0269183218479
time_elpased: 2.654
batch start
#iterations: 288
currently lose_sum: 533.8001970946789
time_elpased: 2.64
batch start
#iterations: 289
currently lose_sum: 533.6209800541401
time_elpased: 2.629
batch start
#iterations: 290
currently lose_sum: 531.8965608179569
time_elpased: 2.664
batch start
#iterations: 291
currently lose_sum: 531.3349956274033
time_elpased: 2.647
batch start
#iterations: 292
currently lose_sum: 531.078815817833
time_elpased: 2.651
batch start
#iterations: 293
currently lose_sum: 533.418197453022
time_elpased: 2.656
batch start
#iterations: 294
currently lose_sum: 529.7754083275795
time_elpased: 2.652
batch start
#iterations: 295
currently lose_sum: 530.5976866185665
time_elpased: 2.641
batch start
#iterations: 296
currently lose_sum: 531.1608621478081
time_elpased: 2.661
batch start
#iterations: 297
currently lose_sum: 530.8856662809849
time_elpased: 2.651
batch start
#iterations: 298
currently lose_sum: 531.3579457998276
time_elpased: 2.658
batch start
#iterations: 299
currently lose_sum: 532.6037340164185
time_elpased: 2.67
start validation test
0.541701030928
0.93807106599
0.0947303670289
0.172083061738
0
validation finish
batch start
#iterations: 300
currently lose_sum: 531.6619527339935
time_elpased: 2.667
batch start
#iterations: 301
currently lose_sum: 531.5042361319065
time_elpased: 2.661
batch start
#iterations: 302
currently lose_sum: 531.6758118867874
time_elpased: 2.669
batch start
#iterations: 303
currently lose_sum: 533.8637942671776
time_elpased: 2.662
batch start
#iterations: 304
currently lose_sum: 531.0033143758774
time_elpased: 2.657
batch start
#iterations: 305
currently lose_sum: 532.2497407793999
time_elpased: 2.675
batch start
#iterations: 306
currently lose_sum: 530.4878196120262
time_elpased: 2.652
batch start
#iterations: 307
currently lose_sum: 531.169519841671
time_elpased: 2.664
batch start
#iterations: 308
currently lose_sum: 531.5338830649853
time_elpased: 2.642
batch start
#iterations: 309
currently lose_sum: 531.0561434924603
time_elpased: 2.643
batch start
#iterations: 310
currently lose_sum: 532.2023653686047
time_elpased: 2.65
batch start
#iterations: 311
currently lose_sum: 531.8066091537476
time_elpased: 2.66
batch start
#iterations: 312
currently lose_sum: 529.8372041583061
time_elpased: 2.65
batch start
#iterations: 313
currently lose_sum: 533.1520120203495
time_elpased: 2.643
batch start
#iterations: 314
currently lose_sum: 533.0935506820679
time_elpased: 2.643
batch start
#iterations: 315
currently lose_sum: 532.0291619002819
time_elpased: 2.635
batch start
#iterations: 316
currently lose_sum: 531.6306140422821
time_elpased: 2.635
batch start
#iterations: 317
currently lose_sum: 532.0720216333866
time_elpased: 2.635
batch start
#iterations: 318
currently lose_sum: 530.2904559671879
time_elpased: 2.637
batch start
#iterations: 319
currently lose_sum: 529.7299154698849
time_elpased: 2.654
start validation test
0.55675257732
0.923076923077
0.129177773221
0.226639086249
0
validation finish
batch start
#iterations: 320
currently lose_sum: 532.8986524343491
time_elpased: 2.669
batch start
#iterations: 321
currently lose_sum: 529.8187588453293
time_elpased: 2.663
batch start
#iterations: 322
currently lose_sum: 530.4244712591171
time_elpased: 2.655
batch start
#iterations: 323
currently lose_sum: 531.7414663136005
time_elpased: 2.669
batch start
#iterations: 324
currently lose_sum: 528.9295729696751
time_elpased: 2.673
batch start
#iterations: 325
currently lose_sum: 533.5481603145599
time_elpased: 2.659
batch start
#iterations: 326
currently lose_sum: 531.784471154213
time_elpased: 2.672
batch start
#iterations: 327
currently lose_sum: 532.694378554821
time_elpased: 2.672
batch start
#iterations: 328
currently lose_sum: 531.9377318322659
time_elpased: 2.667
batch start
#iterations: 329
currently lose_sum: 529.4775525331497
time_elpased: 2.679
batch start
#iterations: 330
currently lose_sum: 532.2620754539967
time_elpased: 2.67
batch start
#iterations: 331
currently lose_sum: 531.019249945879
time_elpased: 2.665
batch start
#iterations: 332
currently lose_sum: 531.5519297420979
time_elpased: 2.668
batch start
#iterations: 333
currently lose_sum: 531.7381727695465
time_elpased: 2.668
batch start
#iterations: 334
currently lose_sum: 530.2410616874695
time_elpased: 2.654
batch start
#iterations: 335
currently lose_sum: 530.2268495857716
time_elpased: 2.671
batch start
#iterations: 336
currently lose_sum: 530.8481630384922
time_elpased: 2.662
batch start
#iterations: 337
currently lose_sum: 531.6422815918922
time_elpased: 2.654
batch start
#iterations: 338
currently lose_sum: 533.4540948569775
time_elpased: 2.665
batch start
#iterations: 339
currently lose_sum: 530.9269105195999
time_elpased: 2.656
start validation test
0.550618556701
0.926688632619
0.115337297519
0.205142231947
0
validation finish
batch start
#iterations: 340
currently lose_sum: 532.0044485330582
time_elpased: 2.651
batch start
#iterations: 341
currently lose_sum: 533.4117447435856
time_elpased: 2.667
batch start
#iterations: 342
currently lose_sum: 531.3463313281536
time_elpased: 2.66
batch start
#iterations: 343
currently lose_sum: 532.957556605339
time_elpased: 2.656
batch start
#iterations: 344
currently lose_sum: 531.940587759018
time_elpased: 2.664
batch start
#iterations: 345
currently lose_sum: 532.2231997549534
time_elpased: 2.654
batch start
#iterations: 346
currently lose_sum: 529.7197016477585
time_elpased: 2.633
batch start
#iterations: 347
currently lose_sum: 532.1209493875504
time_elpased: 2.645
batch start
#iterations: 348
currently lose_sum: 532.5042293071747
time_elpased: 2.65
batch start
#iterations: 349
currently lose_sum: 529.4221478402615
time_elpased: 2.646
batch start
#iterations: 350
currently lose_sum: 530.3525728881359
time_elpased: 2.669
batch start
#iterations: 351
currently lose_sum: 533.2205486893654
time_elpased: 2.673
batch start
#iterations: 352
currently lose_sum: 533.3038652837276
time_elpased: 2.65
batch start
#iterations: 353
currently lose_sum: 530.4348333775997
time_elpased: 2.657
batch start
#iterations: 354
currently lose_sum: 531.6732599437237
time_elpased: 2.665
batch start
#iterations: 355
currently lose_sum: 532.3534446954727
time_elpased: 2.657
batch start
#iterations: 356
currently lose_sum: 531.4205080270767
time_elpased: 2.674
batch start
#iterations: 357
currently lose_sum: 531.243381023407
time_elpased: 2.671
batch start
#iterations: 358
currently lose_sum: 532.1565038561821
time_elpased: 2.662
batch start
#iterations: 359
currently lose_sum: 529.9878415763378
time_elpased: 2.678
start validation test
0.567164948454
0.917025199754
0.152962887021
0.262191371584
0
validation finish
batch start
#iterations: 360
currently lose_sum: 531.8692845702171
time_elpased: 2.661
batch start
#iterations: 361
currently lose_sum: 529.9036726355553
time_elpased: 2.658
batch start
#iterations: 362
currently lose_sum: 530.8673492670059
time_elpased: 2.658
batch start
#iterations: 363
currently lose_sum: 530.403050661087
time_elpased: 2.657
batch start
#iterations: 364
currently lose_sum: 531.6100412011147
time_elpased: 2.652
batch start
#iterations: 365
currently lose_sum: 530.4962908625603
time_elpased: 2.665
batch start
#iterations: 366
currently lose_sum: 531.2785545289516
time_elpased: 2.662
batch start
#iterations: 367
currently lose_sum: 533.1051747202873
time_elpased: 2.651
batch start
#iterations: 368
currently lose_sum: 532.3860648274422
time_elpased: 2.644
batch start
#iterations: 369
currently lose_sum: 530.3224107027054
time_elpased: 2.659
batch start
#iterations: 370
currently lose_sum: 532.0520747303963
time_elpased: 2.663
batch start
#iterations: 371
currently lose_sum: 530.7605985403061
time_elpased: 2.675
batch start
#iterations: 372
currently lose_sum: 533.6217023134232
time_elpased: 2.676
batch start
#iterations: 373
currently lose_sum: 534.9157972931862
time_elpased: 2.662
batch start
#iterations: 374
currently lose_sum: 530.0909167826176
time_elpased: 2.671
batch start
#iterations: 375
currently lose_sum: 532.7019783556461
time_elpased: 2.675
batch start
#iterations: 376
currently lose_sum: 530.6635318994522
time_elpased: 2.644
batch start
#iterations: 377
currently lose_sum: 532.0346689224243
time_elpased: 2.654
batch start
#iterations: 378
currently lose_sum: 532.8272729218006
time_elpased: 2.663
batch start
#iterations: 379
currently lose_sum: 529.4070031642914
time_elpased: 2.646
start validation test
0.55912371134
0.929235167977
0.133278654911
0.233121133327
0
validation finish
batch start
#iterations: 380
currently lose_sum: 531.0071085393429
time_elpased: 2.653
batch start
#iterations: 381
currently lose_sum: 530.7492410242558
time_elpased: 2.648
batch start
#iterations: 382
currently lose_sum: 530.5976048111916
time_elpased: 2.644
batch start
#iterations: 383
currently lose_sum: 532.8350175023079
time_elpased: 2.66
batch start
#iterations: 384
currently lose_sum: 531.9235079884529
time_elpased: 2.659
batch start
#iterations: 385
currently lose_sum: 530.4686161577702
time_elpased: 2.656
batch start
#iterations: 386
currently lose_sum: 533.6119480133057
time_elpased: 2.666
batch start
#iterations: 387
currently lose_sum: 530.3647036254406
time_elpased: 2.656
batch start
#iterations: 388
currently lose_sum: 531.0162787437439
time_elpased: 2.665
batch start
#iterations: 389
currently lose_sum: 530.8580763041973
time_elpased: 2.668
batch start
#iterations: 390
currently lose_sum: 530.1900569796562
time_elpased: 2.667
batch start
#iterations: 391
currently lose_sum: 529.5086002349854
time_elpased: 2.669
batch start
#iterations: 392
currently lose_sum: 531.4549405574799
time_elpased: 2.659
batch start
#iterations: 393
currently lose_sum: 531.53670540452
time_elpased: 2.688
batch start
#iterations: 394
currently lose_sum: 530.2857884168625
time_elpased: 2.647
batch start
#iterations: 395
currently lose_sum: 532.7254943847656
time_elpased: 2.658
batch start
#iterations: 396
currently lose_sum: 531.5095592737198
time_elpased: 2.653
batch start
#iterations: 397
currently lose_sum: 532.9388475418091
time_elpased: 2.67
batch start
#iterations: 398
currently lose_sum: 530.2395023107529
time_elpased: 2.659
batch start
#iterations: 399
currently lose_sum: 531.2980982661247
time_elpased: 2.681
start validation test
0.563144329897
0.923790589795
0.142915726881
0.247536180414
0
validation finish
acc: 0.578
pre: 0.911
rec: 0.179
F1: 0.300
auc: 0.000
