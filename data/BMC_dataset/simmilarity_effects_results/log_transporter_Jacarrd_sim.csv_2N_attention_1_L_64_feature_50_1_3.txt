start to construct graph
graph construct over
87453
epochs start
batch start
#iterations: 0
currently lose_sum: 571.0775609016418
time_elpased: 1.826
batch start
#iterations: 1
currently lose_sum: 552.094698548317
time_elpased: 1.841
batch start
#iterations: 2
currently lose_sum: 546.5085458159447
time_elpased: 1.78
batch start
#iterations: 3
currently lose_sum: 544.9108572900295
time_elpased: 1.791
batch start
#iterations: 4
currently lose_sum: 541.1511773467064
time_elpased: 1.795
batch start
#iterations: 5
currently lose_sum: 541.0663875937462
time_elpased: 1.789
batch start
#iterations: 6
currently lose_sum: 540.2947454452515
time_elpased: 1.79
batch start
#iterations: 7
currently lose_sum: 539.0296036601067
time_elpased: 1.802
batch start
#iterations: 8
currently lose_sum: 541.0198320150375
time_elpased: 1.799
batch start
#iterations: 9
currently lose_sum: 538.0790983438492
time_elpased: 1.799
batch start
#iterations: 10
currently lose_sum: 538.5392860770226
time_elpased: 1.796
batch start
#iterations: 11
currently lose_sum: 539.2897565364838
time_elpased: 1.8
batch start
#iterations: 12
currently lose_sum: 541.2284290492535
time_elpased: 1.807
batch start
#iterations: 13
currently lose_sum: 537.8927407562733
time_elpased: 1.781
batch start
#iterations: 14
currently lose_sum: 539.2877929210663
time_elpased: 1.813
batch start
#iterations: 15
currently lose_sum: 537.5406652987003
time_elpased: 1.787
batch start
#iterations: 16
currently lose_sum: 538.1924332976341
time_elpased: 1.79
batch start
#iterations: 17
currently lose_sum: 537.9035318493843
time_elpased: 1.794
batch start
#iterations: 18
currently lose_sum: 538.2129919826984
time_elpased: 1.813
batch start
#iterations: 19
currently lose_sum: 536.5728043019772
time_elpased: 1.808
start validation test
0.528762886598
0.886956521739
0.0731182795699
0.135099337748
0
validation finish
batch start
#iterations: 20
currently lose_sum: 538.1001890897751
time_elpased: 1.818
batch start
#iterations: 21
currently lose_sum: 537.2939089238644
time_elpased: 1.789
batch start
#iterations: 22
currently lose_sum: 537.4705294668674
time_elpased: 1.81
batch start
#iterations: 23
currently lose_sum: 536.794765740633
time_elpased: 1.867
batch start
#iterations: 24
currently lose_sum: 536.1098797917366
time_elpased: 1.806
batch start
#iterations: 25
currently lose_sum: 536.34921002388
time_elpased: 1.79
batch start
#iterations: 26
currently lose_sum: 534.9212036728859
time_elpased: 1.794
batch start
#iterations: 27
currently lose_sum: 537.1679421961308
time_elpased: 1.804
batch start
#iterations: 28
currently lose_sum: 535.9244369864464
time_elpased: 1.791
batch start
#iterations: 29
currently lose_sum: 534.5717018842697
time_elpased: 1.805
batch start
#iterations: 30
currently lose_sum: 535.2079395055771
time_elpased: 1.791
batch start
#iterations: 31
currently lose_sum: 535.7178664505482
time_elpased: 1.784
batch start
#iterations: 32
currently lose_sum: 536.2876220345497
time_elpased: 1.809
batch start
#iterations: 33
currently lose_sum: 535.35730651021
time_elpased: 1.787
batch start
#iterations: 34
currently lose_sum: 534.9331721961498
time_elpased: 1.806
batch start
#iterations: 35
currently lose_sum: 535.8798564076424
time_elpased: 1.804
batch start
#iterations: 36
currently lose_sum: 535.6208549439907
time_elpased: 1.813
batch start
#iterations: 37
currently lose_sum: 534.5979984998703
time_elpased: 1.795
batch start
#iterations: 38
currently lose_sum: 536.1642808914185
time_elpased: 1.798
batch start
#iterations: 39
currently lose_sum: 536.7515049576759
time_elpased: 1.81
start validation test
0.569896907216
0.925194494315
0.158320532514
0.270374256733
0
validation finish
batch start
#iterations: 40
currently lose_sum: 536.326343536377
time_elpased: 1.807
batch start
#iterations: 41
currently lose_sum: 536.2135148048401
time_elpased: 1.798
batch start
#iterations: 42
currently lose_sum: 535.3695712089539
time_elpased: 1.779
batch start
#iterations: 43
currently lose_sum: 536.1645613312721
time_elpased: 1.803
batch start
#iterations: 44
currently lose_sum: 533.8556706905365
time_elpased: 1.786
batch start
#iterations: 45
currently lose_sum: 533.591164290905
time_elpased: 1.784
batch start
#iterations: 46
currently lose_sum: 537.7522552609444
time_elpased: 1.783
batch start
#iterations: 47
currently lose_sum: 536.1514276266098
time_elpased: 1.823
batch start
#iterations: 48
currently lose_sum: 532.9960683584213
time_elpased: 1.809
batch start
#iterations: 49
currently lose_sum: 534.3144125342369
time_elpased: 1.784
batch start
#iterations: 50
currently lose_sum: 533.754201233387
time_elpased: 1.797
batch start
#iterations: 51
currently lose_sum: 536.4461228847504
time_elpased: 1.788
batch start
#iterations: 52
currently lose_sum: 537.1221261620522
time_elpased: 1.791
batch start
#iterations: 53
currently lose_sum: 535.2343873381615
time_elpased: 1.787
batch start
#iterations: 54
currently lose_sum: 534.2713822722435
time_elpased: 1.805
batch start
#iterations: 55
currently lose_sum: 533.0030606985092
time_elpased: 1.785
batch start
#iterations: 56
currently lose_sum: 533.8695531487465
time_elpased: 1.794
batch start
#iterations: 57
currently lose_sum: 535.1427747607231
time_elpased: 1.793
batch start
#iterations: 58
currently lose_sum: 534.5555383563042
time_elpased: 1.813
batch start
#iterations: 59
currently lose_sum: 533.8894485831261
time_elpased: 1.794
start validation test
0.52618556701
0.941448382126
0.0625704045059
0.117342039562
0
validation finish
batch start
#iterations: 60
currently lose_sum: 535.0455090105534
time_elpased: 1.802
batch start
#iterations: 61
currently lose_sum: 533.8900404572487
time_elpased: 1.854
batch start
#iterations: 62
currently lose_sum: 534.3781073689461
time_elpased: 1.811
batch start
#iterations: 63
currently lose_sum: 535.8084950149059
time_elpased: 1.806
batch start
#iterations: 64
currently lose_sum: 534.9828606247902
time_elpased: 1.799
batch start
#iterations: 65
currently lose_sum: 535.5969098806381
time_elpased: 1.818
batch start
#iterations: 66
currently lose_sum: 534.2545129060745
time_elpased: 1.844
batch start
#iterations: 67
currently lose_sum: 532.3306159377098
time_elpased: 1.787
batch start
#iterations: 68
currently lose_sum: 531.7063130736351
time_elpased: 1.786
batch start
#iterations: 69
currently lose_sum: 532.9703355431557
time_elpased: 1.798
batch start
#iterations: 70
currently lose_sum: 534.0088051557541
time_elpased: 1.799
batch start
#iterations: 71
currently lose_sum: 534.7695531845093
time_elpased: 1.792
batch start
#iterations: 72
currently lose_sum: 532.9202642738819
time_elpased: 1.796
batch start
#iterations: 73
currently lose_sum: 534.1566909849644
time_elpased: 1.797
batch start
#iterations: 74
currently lose_sum: 533.928554713726
time_elpased: 1.812
batch start
#iterations: 75
currently lose_sum: 533.6636695861816
time_elpased: 1.775
batch start
#iterations: 76
currently lose_sum: 535.7998763024807
time_elpased: 1.811
batch start
#iterations: 77
currently lose_sum: 534.6488370299339
time_elpased: 1.799
batch start
#iterations: 78
currently lose_sum: 533.8112164139748
time_elpased: 1.787
batch start
#iterations: 79
currently lose_sum: 533.9856339097023
time_elpased: 1.795
start validation test
0.547783505155
0.907224958949
0.113159242192
0.201220067377
0
validation finish
batch start
#iterations: 80
currently lose_sum: 533.778818666935
time_elpased: 1.805
batch start
#iterations: 81
currently lose_sum: 533.9742943942547
time_elpased: 1.796
batch start
#iterations: 82
currently lose_sum: 533.7850350737572
time_elpased: 1.81
batch start
#iterations: 83
currently lose_sum: 531.9173685312271
time_elpased: 1.789
batch start
#iterations: 84
currently lose_sum: 533.8308377563953
time_elpased: 1.791
batch start
#iterations: 85
currently lose_sum: 535.2567474544048
time_elpased: 1.816
batch start
#iterations: 86
currently lose_sum: 534.227452814579
time_elpased: 1.793
batch start
#iterations: 87
currently lose_sum: 534.2211950421333
time_elpased: 1.787
batch start
#iterations: 88
currently lose_sum: 533.681278526783
time_elpased: 1.791
batch start
#iterations: 89
currently lose_sum: 531.6539295017719
time_elpased: 1.785
batch start
#iterations: 90
currently lose_sum: 533.0257108211517
time_elpased: 1.797
batch start
#iterations: 91
currently lose_sum: 531.2447670698166
time_elpased: 1.804
batch start
#iterations: 92
currently lose_sum: 531.9110151529312
time_elpased: 1.807
batch start
#iterations: 93
currently lose_sum: 531.6056310534477
time_elpased: 1.8
batch start
#iterations: 94
currently lose_sum: 532.1746359169483
time_elpased: 1.8
batch start
#iterations: 95
currently lose_sum: 532.4612640738487
time_elpased: 1.792
batch start
#iterations: 96
currently lose_sum: 533.3114258348942
time_elpased: 1.803
batch start
#iterations: 97
currently lose_sum: 535.6499376296997
time_elpased: 1.785
batch start
#iterations: 98
currently lose_sum: 531.2304872274399
time_elpased: 1.79
batch start
#iterations: 99
currently lose_sum: 532.9118179678917
time_elpased: 1.797
start validation test
0.566804123711
0.933715742511
0.150025601639
0.258514205047
0
validation finish
batch start
#iterations: 100
currently lose_sum: 532.7730757892132
time_elpased: 1.805
batch start
#iterations: 101
currently lose_sum: 531.1777978539467
time_elpased: 1.795
batch start
#iterations: 102
currently lose_sum: 533.9947328567505
time_elpased: 1.787
batch start
#iterations: 103
currently lose_sum: 531.6410283446312
time_elpased: 1.793
batch start
#iterations: 104
currently lose_sum: 531.5752487778664
time_elpased: 1.816
batch start
#iterations: 105
currently lose_sum: 530.9789592325687
time_elpased: 1.793
batch start
#iterations: 106
currently lose_sum: 534.0191097855568
time_elpased: 1.797
batch start
#iterations: 107
currently lose_sum: 533.1698747575283
time_elpased: 1.8
batch start
#iterations: 108
currently lose_sum: 531.9175798296928
time_elpased: 1.786
batch start
#iterations: 109
currently lose_sum: 535.211943924427
time_elpased: 1.792
batch start
#iterations: 110
currently lose_sum: 532.7042736113071
time_elpased: 1.797
batch start
#iterations: 111
currently lose_sum: 532.8326637148857
time_elpased: 1.795
batch start
#iterations: 112
currently lose_sum: 532.4372442364693
time_elpased: 1.791
batch start
#iterations: 113
currently lose_sum: 533.4482418894768
time_elpased: 1.797
batch start
#iterations: 114
currently lose_sum: 533.7131385803223
time_elpased: 1.816
batch start
#iterations: 115
currently lose_sum: 534.0146366655827
time_elpased: 1.793
batch start
#iterations: 116
currently lose_sum: 532.8806850612164
time_elpased: 1.798
batch start
#iterations: 117
currently lose_sum: 533.5620476305485
time_elpased: 1.787
batch start
#iterations: 118
currently lose_sum: 532.7985146045685
time_elpased: 1.802
batch start
#iterations: 119
currently lose_sum: 531.2179417610168
time_elpased: 1.797
start validation test
0.535927835052
0.937931034483
0.0835637480799
0.153455571227
0
validation finish
batch start
#iterations: 120
currently lose_sum: 533.4674100875854
time_elpased: 1.788
batch start
#iterations: 121
currently lose_sum: 532.69556042552
time_elpased: 1.786
batch start
#iterations: 122
currently lose_sum: 535.6104744076729
time_elpased: 1.788
batch start
#iterations: 123
currently lose_sum: 534.0782588124275
time_elpased: 1.805
batch start
#iterations: 124
currently lose_sum: 532.3126279115677
time_elpased: 1.798
batch start
#iterations: 125
currently lose_sum: 531.2127572894096
time_elpased: 1.79
batch start
#iterations: 126
currently lose_sum: 530.4545597434044
time_elpased: 1.784
batch start
#iterations: 127
currently lose_sum: 531.5705481469631
time_elpased: 1.798
batch start
#iterations: 128
currently lose_sum: 533.0722703635693
time_elpased: 1.791
batch start
#iterations: 129
currently lose_sum: 531.5198586285114
time_elpased: 1.803
batch start
#iterations: 130
currently lose_sum: 531.2111649215221
time_elpased: 1.792
batch start
#iterations: 131
currently lose_sum: 532.0429403185844
time_elpased: 1.786
batch start
#iterations: 132
currently lose_sum: 531.9740529060364
time_elpased: 1.792
batch start
#iterations: 133
currently lose_sum: 531.1986424922943
time_elpased: 1.795
batch start
#iterations: 134
currently lose_sum: 531.8931476175785
time_elpased: 1.801
batch start
#iterations: 135
currently lose_sum: 530.9034560620785
time_elpased: 1.786
batch start
#iterations: 136
currently lose_sum: 532.8211069107056
time_elpased: 1.794
batch start
#iterations: 137
currently lose_sum: 531.2110288739204
time_elpased: 1.786
batch start
#iterations: 138
currently lose_sum: 532.3123253583908
time_elpased: 1.819
batch start
#iterations: 139
currently lose_sum: 533.0620929896832
time_elpased: 1.796
start validation test
0.567371134021
0.924504950495
0.152995391705
0.262542834549
0
validation finish
batch start
#iterations: 140
currently lose_sum: 530.4090574383736
time_elpased: 1.789
batch start
#iterations: 141
currently lose_sum: 531.8698734939098
time_elpased: 1.791
batch start
#iterations: 142
currently lose_sum: 532.7225779891014
time_elpased: 1.793
batch start
#iterations: 143
currently lose_sum: 531.7910629808903
time_elpased: 1.798
batch start
#iterations: 144
currently lose_sum: 531.7361896932125
time_elpased: 1.811
batch start
#iterations: 145
currently lose_sum: 533.1737466454506
time_elpased: 1.783
batch start
#iterations: 146
currently lose_sum: 532.8554155230522
time_elpased: 1.795
batch start
#iterations: 147
currently lose_sum: 531.432903200388
time_elpased: 1.798
batch start
#iterations: 148
currently lose_sum: 531.7675657868385
time_elpased: 1.798
batch start
#iterations: 149
currently lose_sum: 533.141703248024
time_elpased: 1.822
batch start
#iterations: 150
currently lose_sum: 532.4154654443264
time_elpased: 1.798
batch start
#iterations: 151
currently lose_sum: 530.8468888700008
time_elpased: 1.799
batch start
#iterations: 152
currently lose_sum: 529.96947067976
time_elpased: 1.783
batch start
#iterations: 153
currently lose_sum: 528.8954266309738
time_elpased: 1.8
batch start
#iterations: 154
currently lose_sum: 533.281869918108
time_elpased: 1.797
batch start
#iterations: 155
currently lose_sum: 532.8870164453983
time_elpased: 1.806
batch start
#iterations: 156
currently lose_sum: 532.116974234581
time_elpased: 1.816
batch start
#iterations: 157
currently lose_sum: 531.2003775835037
time_elpased: 1.795
batch start
#iterations: 158
currently lose_sum: 531.2161042094231
time_elpased: 1.799
batch start
#iterations: 159
currently lose_sum: 532.062660574913
time_elpased: 1.809
start validation test
0.555051546392
0.925619834711
0.126164874552
0.222062004326
0
validation finish
batch start
#iterations: 160
currently lose_sum: 532.321038544178
time_elpased: 1.816
batch start
#iterations: 161
currently lose_sum: 531.9207998514175
time_elpased: 1.809
batch start
#iterations: 162
currently lose_sum: 530.6998481154442
time_elpased: 1.796
batch start
#iterations: 163
currently lose_sum: 532.3593743741512
time_elpased: 1.784
batch start
#iterations: 164
currently lose_sum: 531.9519273042679
time_elpased: 1.817
batch start
#iterations: 165
currently lose_sum: 532.4319753050804
time_elpased: 1.796
batch start
#iterations: 166
currently lose_sum: 531.6929936408997
time_elpased: 1.811
batch start
#iterations: 167
currently lose_sum: 532.1864249110222
time_elpased: 1.807
batch start
#iterations: 168
currently lose_sum: 532.2088279128075
time_elpased: 1.822
batch start
#iterations: 169
currently lose_sum: 529.7412316203117
time_elpased: 1.791
batch start
#iterations: 170
currently lose_sum: 530.9960220456123
time_elpased: 1.785
batch start
#iterations: 171
currently lose_sum: 533.1144720315933
time_elpased: 1.826
batch start
#iterations: 172
currently lose_sum: 531.953466117382
time_elpased: 1.779
batch start
#iterations: 173
currently lose_sum: 530.4853655695915
time_elpased: 1.807
batch start
#iterations: 174
currently lose_sum: 532.8756085336208
time_elpased: 1.803
batch start
#iterations: 175
currently lose_sum: 531.3192980885506
time_elpased: 1.801
batch start
#iterations: 176
currently lose_sum: 531.8132777810097
time_elpased: 1.806
batch start
#iterations: 177
currently lose_sum: 532.7700555920601
time_elpased: 1.8
batch start
#iterations: 178
currently lose_sum: 530.8374657034874
time_elpased: 1.807
batch start
#iterations: 179
currently lose_sum: 534.3124585449696
time_elpased: 1.787
start validation test
0.550670103093
0.925324675325
0.116743471582
0.20732927162
0
validation finish
batch start
#iterations: 180
currently lose_sum: 532.5055462121964
time_elpased: 1.798
batch start
#iterations: 181
currently lose_sum: 533.815711915493
time_elpased: 1.79
batch start
#iterations: 182
currently lose_sum: 531.745166182518
time_elpased: 1.796
batch start
#iterations: 183
currently lose_sum: 530.6235485970974
time_elpased: 1.778
batch start
#iterations: 184
currently lose_sum: 531.2261505424976
time_elpased: 1.788
batch start
#iterations: 185
currently lose_sum: 531.6235023140907
time_elpased: 1.792
batch start
#iterations: 186
currently lose_sum: 530.1884343624115
time_elpased: 1.81
batch start
#iterations: 187
currently lose_sum: 533.3190119564533
time_elpased: 1.802
batch start
#iterations: 188
currently lose_sum: 531.0892296731472
time_elpased: 1.789
batch start
#iterations: 189
currently lose_sum: 531.7791932225227
time_elpased: 1.806
batch start
#iterations: 190
currently lose_sum: 532.0175269842148
time_elpased: 1.79
batch start
#iterations: 191
currently lose_sum: 531.9829223453999
time_elpased: 1.789
batch start
#iterations: 192
currently lose_sum: 531.1468116939068
time_elpased: 1.818
batch start
#iterations: 193
currently lose_sum: 531.5749512910843
time_elpased: 1.806
batch start
#iterations: 194
currently lose_sum: 532.4772007763386
time_elpased: 1.789
batch start
#iterations: 195
currently lose_sum: 530.739097237587
time_elpased: 1.812
batch start
#iterations: 196
currently lose_sum: 532.3591559529305
time_elpased: 1.79
batch start
#iterations: 197
currently lose_sum: 531.7393308877945
time_elpased: 1.786
batch start
#iterations: 198
currently lose_sum: 531.1651630103588
time_elpased: 1.806
batch start
#iterations: 199
currently lose_sum: 533.8889473676682
time_elpased: 1.792
start validation test
0.569329896907
0.926239419589
0.156886840758
0.268324721955
0
validation finish
batch start
#iterations: 200
currently lose_sum: 529.6507379412651
time_elpased: 1.801
batch start
#iterations: 201
currently lose_sum: 530.9820872545242
time_elpased: 1.794
batch start
#iterations: 202
currently lose_sum: 529.3480782210827
time_elpased: 1.794
batch start
#iterations: 203
currently lose_sum: 532.4027686715126
time_elpased: 1.793
batch start
#iterations: 204
currently lose_sum: 532.2507899999619
time_elpased: 1.796
batch start
#iterations: 205
currently lose_sum: 531.1181736886501
time_elpased: 1.786
batch start
#iterations: 206
currently lose_sum: 530.3549358248711
time_elpased: 1.784
batch start
#iterations: 207
currently lose_sum: 530.1049306988716
time_elpased: 1.807
batch start
#iterations: 208
currently lose_sum: 531.8602696657181
time_elpased: 1.806
batch start
#iterations: 209
currently lose_sum: 533.3939571380615
time_elpased: 1.795
batch start
#iterations: 210
currently lose_sum: 530.2305634021759
time_elpased: 1.807
batch start
#iterations: 211
currently lose_sum: 530.6988128423691
time_elpased: 1.789
batch start
#iterations: 212
currently lose_sum: 531.1149682402611
time_elpased: 1.798
batch start
#iterations: 213
currently lose_sum: 533.2472995817661
time_elpased: 1.796
batch start
#iterations: 214
currently lose_sum: 533.7668226957321
time_elpased: 1.793
batch start
#iterations: 215
currently lose_sum: 532.339744836092
time_elpased: 1.777
batch start
#iterations: 216
currently lose_sum: 530.689112007618
time_elpased: 1.791
batch start
#iterations: 217
currently lose_sum: 531.0992748737335
time_elpased: 1.797
batch start
#iterations: 218
currently lose_sum: 532.8259818553925
time_elpased: 1.804
batch start
#iterations: 219
currently lose_sum: 532.2379364967346
time_elpased: 1.786
start validation test
0.543144329897
0.934489402697
0.0993343573989
0.179579746367
0
validation finish
batch start
#iterations: 220
currently lose_sum: 531.5500707030296
time_elpased: 1.828
batch start
#iterations: 221
currently lose_sum: 533.4198948144913
time_elpased: 1.794
batch start
#iterations: 222
currently lose_sum: 531.1139742732048
time_elpased: 1.787
batch start
#iterations: 223
currently lose_sum: 529.2674041688442
time_elpased: 1.79
batch start
#iterations: 224
currently lose_sum: 532.5894529223442
time_elpased: 1.798
batch start
#iterations: 225
currently lose_sum: 530.4968151152134
time_elpased: 1.795
batch start
#iterations: 226
currently lose_sum: 531.8909112215042
time_elpased: 1.806
batch start
#iterations: 227
currently lose_sum: 530.400081127882
time_elpased: 1.797
batch start
#iterations: 228
currently lose_sum: 530.601772993803
time_elpased: 1.811
batch start
#iterations: 229
currently lose_sum: 530.6407814025879
time_elpased: 1.805
batch start
#iterations: 230
currently lose_sum: 531.2926954030991
time_elpased: 1.783
batch start
#iterations: 231
currently lose_sum: 534.0550032556057
time_elpased: 1.797
batch start
#iterations: 232
currently lose_sum: 532.3857978582382
time_elpased: 1.805
batch start
#iterations: 233
currently lose_sum: 531.6419639289379
time_elpased: 1.8
batch start
#iterations: 234
currently lose_sum: 531.3769327998161
time_elpased: 1.783
batch start
#iterations: 235
currently lose_sum: 532.3918332159519
time_elpased: 1.797
batch start
#iterations: 236
currently lose_sum: 529.6612715125084
time_elpased: 1.79
batch start
#iterations: 237
currently lose_sum: 531.4833320379257
time_elpased: 1.806
batch start
#iterations: 238
currently lose_sum: 529.3607895970345
time_elpased: 1.805
batch start
#iterations: 239
currently lose_sum: 533.4881956577301
time_elpased: 1.805
start validation test
0.56175257732
0.925252525253
0.140706605223
0.244266666667
0
validation finish
batch start
#iterations: 240
currently lose_sum: 531.7486295700073
time_elpased: 1.811
batch start
#iterations: 241
currently lose_sum: 531.6716564297676
time_elpased: 1.806
batch start
#iterations: 242
currently lose_sum: 531.8281854391098
time_elpased: 1.792
batch start
#iterations: 243
currently lose_sum: 531.4015641212463
time_elpased: 1.796
batch start
#iterations: 244
currently lose_sum: 532.9954897761345
time_elpased: 1.801
batch start
#iterations: 245
currently lose_sum: 530.6940013766289
time_elpased: 1.784
batch start
#iterations: 246
currently lose_sum: 532.6056468486786
time_elpased: 1.804
batch start
#iterations: 247
currently lose_sum: 532.5434068143368
time_elpased: 1.783
batch start
#iterations: 248
currently lose_sum: 530.7997261285782
time_elpased: 1.805
batch start
#iterations: 249
currently lose_sum: 531.9822868704796
time_elpased: 1.787
batch start
#iterations: 250
currently lose_sum: 531.8626965582371
time_elpased: 1.799
batch start
#iterations: 251
currently lose_sum: 530.5915566682816
time_elpased: 1.79
batch start
#iterations: 252
currently lose_sum: 531.5478804409504
time_elpased: 1.802
batch start
#iterations: 253
currently lose_sum: 530.6569849848747
time_elpased: 1.787
batch start
#iterations: 254
currently lose_sum: 531.4392721056938
time_elpased: 1.785
batch start
#iterations: 255
currently lose_sum: 530.586074411869
time_elpased: 1.835
batch start
#iterations: 256
currently lose_sum: 531.272724121809
time_elpased: 1.784
batch start
#iterations: 257
currently lose_sum: 529.8787131309509
time_elpased: 1.782
batch start
#iterations: 258
currently lose_sum: 528.3511770367622
time_elpased: 1.801
batch start
#iterations: 259
currently lose_sum: 532.6458942294121
time_elpased: 1.805
start validation test
0.570618556701
0.930930930931
0.15873015873
0.271216097988
0
validation finish
batch start
#iterations: 260
currently lose_sum: 530.2690853178501
time_elpased: 1.803
batch start
#iterations: 261
currently lose_sum: 531.3392855226994
time_elpased: 1.804
batch start
#iterations: 262
currently lose_sum: 531.6011255383492
time_elpased: 1.8
batch start
#iterations: 263
currently lose_sum: 530.9380844533443
time_elpased: 1.783
batch start
#iterations: 264
currently lose_sum: 532.7734073996544
time_elpased: 1.805
batch start
#iterations: 265
currently lose_sum: 532.0906405448914
time_elpased: 1.797
batch start
#iterations: 266
currently lose_sum: 531.0740338563919
time_elpased: 1.803
batch start
#iterations: 267
currently lose_sum: 531.245912373066
time_elpased: 1.785
batch start
#iterations: 268
currently lose_sum: 529.649502068758
time_elpased: 1.807
batch start
#iterations: 269
currently lose_sum: 529.6453775167465
time_elpased: 1.797
batch start
#iterations: 270
currently lose_sum: 531.4565327763557
time_elpased: 1.817
batch start
#iterations: 271
currently lose_sum: 533.1527668535709
time_elpased: 1.808
batch start
#iterations: 272
currently lose_sum: 531.1974197924137
time_elpased: 1.798
batch start
#iterations: 273
currently lose_sum: 531.711046487093
time_elpased: 1.804
batch start
#iterations: 274
currently lose_sum: 531.1893500685692
time_elpased: 1.795
batch start
#iterations: 275
currently lose_sum: 531.4555702805519
time_elpased: 1.794
batch start
#iterations: 276
currently lose_sum: 532.4476656019688
time_elpased: 1.805
batch start
#iterations: 277
currently lose_sum: 532.3631942868233
time_elpased: 1.806
batch start
#iterations: 278
currently lose_sum: 530.1248765587807
time_elpased: 1.807
batch start
#iterations: 279
currently lose_sum: 529.1102364957333
time_elpased: 1.804
start validation test
0.564536082474
0.925662572721
0.146646185356
0.253182461103
0
validation finish
batch start
#iterations: 280
currently lose_sum: 532.9868258237839
time_elpased: 1.789
batch start
#iterations: 281
currently lose_sum: 531.701317936182
time_elpased: 1.795
batch start
#iterations: 282
currently lose_sum: 532.6198447048664
time_elpased: 1.793
batch start
#iterations: 283
currently lose_sum: 531.5527854561806
time_elpased: 1.785
batch start
#iterations: 284
currently lose_sum: 529.7017250061035
time_elpased: 1.799
batch start
#iterations: 285
currently lose_sum: 529.4118137955666
time_elpased: 1.788
batch start
#iterations: 286
currently lose_sum: 531.4866579771042
time_elpased: 1.797
batch start
#iterations: 287
currently lose_sum: 531.4294745624065
time_elpased: 1.809
batch start
#iterations: 288
currently lose_sum: 532.6939206123352
time_elpased: 1.823
batch start
#iterations: 289
currently lose_sum: 534.9061360359192
time_elpased: 1.8
batch start
#iterations: 290
currently lose_sum: 530.6349486410618
time_elpased: 1.802
batch start
#iterations: 291
currently lose_sum: 530.0967171192169
time_elpased: 1.787
batch start
#iterations: 292
currently lose_sum: 530.731288164854
time_elpased: 1.798
batch start
#iterations: 293
currently lose_sum: 531.8111775517464
time_elpased: 1.81
batch start
#iterations: 294
currently lose_sum: 530.8190594911575
time_elpased: 1.813
batch start
#iterations: 295
currently lose_sum: 531.3290024399757
time_elpased: 1.805
batch start
#iterations: 296
currently lose_sum: 530.5539929866791
time_elpased: 1.787
batch start
#iterations: 297
currently lose_sum: 530.4378787875175
time_elpased: 1.789
batch start
#iterations: 298
currently lose_sum: 531.8091567456722
time_elpased: 1.787
batch start
#iterations: 299
currently lose_sum: 531.9208586513996
time_elpased: 1.813
start validation test
0.560721649485
0.928916494134
0.13783922171
0.240057071518
0
validation finish
batch start
#iterations: 300
currently lose_sum: 531.7570172548294
time_elpased: 1.793
batch start
#iterations: 301
currently lose_sum: 530.2922325134277
time_elpased: 1.797
batch start
#iterations: 302
currently lose_sum: 531.9867103397846
time_elpased: 1.79
batch start
#iterations: 303
currently lose_sum: 532.1883613467216
time_elpased: 1.795
batch start
#iterations: 304
currently lose_sum: 529.5538247823715
time_elpased: 1.798
batch start
#iterations: 305
currently lose_sum: 531.4780017137527
time_elpased: 1.804
batch start
#iterations: 306
currently lose_sum: 530.652753919363
time_elpased: 1.815
batch start
#iterations: 307
currently lose_sum: 530.820169866085
time_elpased: 1.824
batch start
#iterations: 308
currently lose_sum: 531.5894073843956
time_elpased: 1.81
batch start
#iterations: 309
currently lose_sum: 531.8849216401577
time_elpased: 1.795
batch start
#iterations: 310
currently lose_sum: 532.2269806563854
time_elpased: 1.81
batch start
#iterations: 311
currently lose_sum: 531.5498636960983
time_elpased: 1.787
batch start
#iterations: 312
currently lose_sum: 529.7257902622223
time_elpased: 1.79
batch start
#iterations: 313
currently lose_sum: 530.7615379691124
time_elpased: 1.816
batch start
#iterations: 314
currently lose_sum: 533.2419775724411
time_elpased: 1.812
batch start
#iterations: 315
currently lose_sum: 530.7671597599983
time_elpased: 1.798
batch start
#iterations: 316
currently lose_sum: 531.4136337041855
time_elpased: 1.802
batch start
#iterations: 317
currently lose_sum: 530.7018782198429
time_elpased: 1.796
batch start
#iterations: 318
currently lose_sum: 528.4818462729454
time_elpased: 1.784
batch start
#iterations: 319
currently lose_sum: 529.5433894991875
time_elpased: 1.822
start validation test
0.561855670103
0.920825016633
0.141730670763
0.245651402201
0
validation finish
batch start
#iterations: 320
currently lose_sum: 533.449113458395
time_elpased: 1.819
batch start
#iterations: 321
currently lose_sum: 530.3745042979717
time_elpased: 1.803
batch start
#iterations: 322
currently lose_sum: 528.4758965969086
time_elpased: 1.795
batch start
#iterations: 323
currently lose_sum: 532.219828337431
time_elpased: 1.806
batch start
#iterations: 324
currently lose_sum: 529.4208151996136
time_elpased: 1.807
batch start
#iterations: 325
currently lose_sum: 530.7356614172459
time_elpased: 1.805
batch start
#iterations: 326
currently lose_sum: 530.3727033734322
time_elpased: 1.803
batch start
#iterations: 327
currently lose_sum: 531.053892493248
time_elpased: 1.783
batch start
#iterations: 328
currently lose_sum: 532.5292285084724
time_elpased: 1.804
batch start
#iterations: 329
currently lose_sum: 530.8587033450603
time_elpased: 1.817
batch start
#iterations: 330
currently lose_sum: 532.4122737646103
time_elpased: 1.809
batch start
#iterations: 331
currently lose_sum: 531.7424978613853
time_elpased: 1.795
batch start
#iterations: 332
currently lose_sum: 531.288757622242
time_elpased: 1.805
batch start
#iterations: 333
currently lose_sum: 529.1385740339756
time_elpased: 1.796
batch start
#iterations: 334
currently lose_sum: 529.4459691047668
time_elpased: 1.815
batch start
#iterations: 335
currently lose_sum: 531.0965990722179
time_elpased: 1.793
batch start
#iterations: 336
currently lose_sum: 531.4149158895016
time_elpased: 1.801
batch start
#iterations: 337
currently lose_sum: 530.4801208972931
time_elpased: 1.799
batch start
#iterations: 338
currently lose_sum: 532.0979854464531
time_elpased: 1.796
batch start
#iterations: 339
currently lose_sum: 531.944979518652
time_elpased: 1.811
start validation test
0.550051546392
0.926688632619
0.115207373272
0.204936697331
0
validation finish
batch start
#iterations: 340
currently lose_sum: 531.8928880989552
time_elpased: 1.794
batch start
#iterations: 341
currently lose_sum: 530.603450357914
time_elpased: 1.793
batch start
#iterations: 342
currently lose_sum: 530.0557347238064
time_elpased: 1.807
batch start
#iterations: 343
currently lose_sum: 532.0444953143597
time_elpased: 1.813
batch start
#iterations: 344
currently lose_sum: 533.978935778141
time_elpased: 1.807
batch start
#iterations: 345
currently lose_sum: 532.4460279345512
time_elpased: 1.817
batch start
#iterations: 346
currently lose_sum: 528.2835803031921
time_elpased: 1.809
batch start
#iterations: 347
currently lose_sum: 532.0951118469238
time_elpased: 1.8
batch start
#iterations: 348
currently lose_sum: 530.9100461602211
time_elpased: 1.805
batch start
#iterations: 349
currently lose_sum: 530.6108569204807
time_elpased: 1.786
batch start
#iterations: 350
currently lose_sum: 530.7147890925407
time_elpased: 1.832
batch start
#iterations: 351
currently lose_sum: 532.4245252609253
time_elpased: 1.788
batch start
#iterations: 352
currently lose_sum: 531.5717872083187
time_elpased: 1.799
batch start
#iterations: 353
currently lose_sum: 530.9157316088676
time_elpased: 1.81
batch start
#iterations: 354
currently lose_sum: 533.3084253668785
time_elpased: 1.796
batch start
#iterations: 355
currently lose_sum: 531.6684436202049
time_elpased: 1.787
batch start
#iterations: 356
currently lose_sum: 530.7440212666988
time_elpased: 1.79
batch start
#iterations: 357
currently lose_sum: 530.3065766692162
time_elpased: 1.812
batch start
#iterations: 358
currently lose_sum: 530.2181760072708
time_elpased: 1.794
batch start
#iterations: 359
currently lose_sum: 530.467391461134
time_elpased: 1.799
start validation test
0.563711340206
0.921035598706
0.14572452637
0.251635720601
0
validation finish
batch start
#iterations: 360
currently lose_sum: 530.2280794382095
time_elpased: 1.812
batch start
#iterations: 361
currently lose_sum: 531.3465858101845
time_elpased: 1.809
batch start
#iterations: 362
currently lose_sum: 531.9319003820419
time_elpased: 1.791
batch start
#iterations: 363
currently lose_sum: 530.2547126710415
time_elpased: 1.804
batch start
#iterations: 364
currently lose_sum: 532.1480261087418
time_elpased: 1.79
batch start
#iterations: 365
currently lose_sum: 530.1059275567532
time_elpased: 1.814
batch start
#iterations: 366
currently lose_sum: 531.2759620547295
time_elpased: 1.797
batch start
#iterations: 367
currently lose_sum: 533.7406794726849
time_elpased: 1.798
batch start
#iterations: 368
currently lose_sum: 530.4382232427597
time_elpased: 1.809
batch start
#iterations: 369
currently lose_sum: 530.6648938655853
time_elpased: 1.801
batch start
#iterations: 370
currently lose_sum: 532.6084358990192
time_elpased: 1.818
batch start
#iterations: 371
currently lose_sum: 529.8283813595772
time_elpased: 1.793
batch start
#iterations: 372
currently lose_sum: 532.0917773246765
time_elpased: 1.802
batch start
#iterations: 373
currently lose_sum: 533.8380682468414
time_elpased: 1.798
batch start
#iterations: 374
currently lose_sum: 530.7188310623169
time_elpased: 1.817
batch start
#iterations: 375
currently lose_sum: 532.7026388049126
time_elpased: 1.797
batch start
#iterations: 376
currently lose_sum: 529.9083151519299
time_elpased: 1.81
batch start
#iterations: 377
currently lose_sum: 530.5675156712532
time_elpased: 1.806
batch start
#iterations: 378
currently lose_sum: 532.0700733661652
time_elpased: 1.807
batch start
#iterations: 379
currently lose_sum: 529.460044503212
time_elpased: 1.809
start validation test
0.568195876289
0.920606060606
0.155555555556
0.266141042488
0
validation finish
batch start
#iterations: 380
currently lose_sum: 530.5722950696945
time_elpased: 1.801
batch start
#iterations: 381
currently lose_sum: 529.5174317955971
time_elpased: 1.815
batch start
#iterations: 382
currently lose_sum: 529.3812640011311
time_elpased: 1.813
batch start
#iterations: 383
currently lose_sum: 531.6288167238235
time_elpased: 1.794
batch start
#iterations: 384
currently lose_sum: 531.0842163860798
time_elpased: 1.801
batch start
#iterations: 385
currently lose_sum: 531.5413637161255
time_elpased: 1.788
batch start
#iterations: 386
currently lose_sum: 533.713711231947
time_elpased: 1.793
batch start
#iterations: 387
currently lose_sum: 529.9940220415592
time_elpased: 1.781
batch start
#iterations: 388
currently lose_sum: 531.4963418841362
time_elpased: 1.793
batch start
#iterations: 389
currently lose_sum: 531.1795657277107
time_elpased: 1.789
batch start
#iterations: 390
currently lose_sum: 531.2310881912708
time_elpased: 1.807
batch start
#iterations: 391
currently lose_sum: 530.8606653809547
time_elpased: 1.793
batch start
#iterations: 392
currently lose_sum: 530.9013763964176
time_elpased: 1.809
batch start
#iterations: 393
currently lose_sum: 532.1385940909386
time_elpased: 1.796
batch start
#iterations: 394
currently lose_sum: 530.1633421778679
time_elpased: 1.824
batch start
#iterations: 395
currently lose_sum: 532.1657537817955
time_elpased: 1.81
batch start
#iterations: 396
currently lose_sum: 531.0292983353138
time_elpased: 1.821
batch start
#iterations: 397
currently lose_sum: 532.0595283806324
time_elpased: 1.799
batch start
#iterations: 398
currently lose_sum: 530.1936407387257
time_elpased: 1.809
batch start
#iterations: 399
currently lose_sum: 531.1681040227413
time_elpased: 1.803
start validation test
0.570206185567
0.922439313203
0.159549411162
0.272044700541
0
validation finish
acc: 0.573
pre: 0.909
rec: 0.159
F1: 0.271
auc: 0.000
