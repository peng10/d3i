start to construct graph
graph construct over
87453
epochs start
batch start
#iterations: 0
currently lose_sum: 571.3464311361313
time_elpased: 1.807
batch start
#iterations: 1
currently lose_sum: 552.5241195559502
time_elpased: 1.786
batch start
#iterations: 2
currently lose_sum: 546.5415390133858
time_elpased: 1.776
batch start
#iterations: 3
currently lose_sum: 544.6926229000092
time_elpased: 1.79
batch start
#iterations: 4
currently lose_sum: 540.2508296966553
time_elpased: 1.773
batch start
#iterations: 5
currently lose_sum: 541.552602827549
time_elpased: 1.788
batch start
#iterations: 6
currently lose_sum: 539.6353524923325
time_elpased: 1.773
batch start
#iterations: 7
currently lose_sum: 539.4455083310604
time_elpased: 1.786
batch start
#iterations: 8
currently lose_sum: 540.331952393055
time_elpased: 1.803
batch start
#iterations: 9
currently lose_sum: 538.8852962255478
time_elpased: 1.788
batch start
#iterations: 10
currently lose_sum: 538.358858525753
time_elpased: 1.791
batch start
#iterations: 11
currently lose_sum: 539.4446302056313
time_elpased: 1.804
batch start
#iterations: 12
currently lose_sum: 540.1312274932861
time_elpased: 1.78
batch start
#iterations: 13
currently lose_sum: 536.1788041591644
time_elpased: 1.801
batch start
#iterations: 14
currently lose_sum: 538.2297052443027
time_elpased: 1.78
batch start
#iterations: 15
currently lose_sum: 536.5557776689529
time_elpased: 1.804
batch start
#iterations: 16
currently lose_sum: 538.5841818451881
time_elpased: 1.796
batch start
#iterations: 17
currently lose_sum: 537.740211546421
time_elpased: 1.789
batch start
#iterations: 18
currently lose_sum: 536.615879714489
time_elpased: 1.81
batch start
#iterations: 19
currently lose_sum: 537.1265076994896
time_elpased: 1.796
start validation test
0.540567010309
0.881233000907
0.0996514250564
0.179054987566
0
validation finish
batch start
#iterations: 20
currently lose_sum: 537.5146702528
time_elpased: 1.802
batch start
#iterations: 21
currently lose_sum: 538.1733020544052
time_elpased: 1.792
batch start
#iterations: 22
currently lose_sum: 537.949432849884
time_elpased: 1.781
batch start
#iterations: 23
currently lose_sum: 536.1478744745255
time_elpased: 1.787
batch start
#iterations: 24
currently lose_sum: 536.8426040112972
time_elpased: 1.799
batch start
#iterations: 25
currently lose_sum: 536.4925506711006
time_elpased: 1.823
batch start
#iterations: 26
currently lose_sum: 534.2005305886269
time_elpased: 1.794
batch start
#iterations: 27
currently lose_sum: 537.044641315937
time_elpased: 1.77
batch start
#iterations: 28
currently lose_sum: 536.1725298762321
time_elpased: 1.789
batch start
#iterations: 29
currently lose_sum: 534.273800432682
time_elpased: 1.794
batch start
#iterations: 30
currently lose_sum: 534.6820413172245
time_elpased: 1.81
batch start
#iterations: 31
currently lose_sum: 536.4419088363647
time_elpased: 1.794
batch start
#iterations: 32
currently lose_sum: 536.032673895359
time_elpased: 1.798
batch start
#iterations: 33
currently lose_sum: 536.0966135263443
time_elpased: 1.801
batch start
#iterations: 34
currently lose_sum: 535.4397339224815
time_elpased: 1.796
batch start
#iterations: 35
currently lose_sum: 536.2026972472668
time_elpased: 1.796
batch start
#iterations: 36
currently lose_sum: 535.27360445261
time_elpased: 1.774
batch start
#iterations: 37
currently lose_sum: 534.3863839805126
time_elpased: 1.778
batch start
#iterations: 38
currently lose_sum: 536.301033437252
time_elpased: 1.784
batch start
#iterations: 39
currently lose_sum: 536.9189482331276
time_elpased: 1.803
start validation test
0.541494845361
0.866780529462
0.104059872873
0.185812356979
0
validation finish
batch start
#iterations: 40
currently lose_sum: 535.8667142987251
time_elpased: 1.805
batch start
#iterations: 41
currently lose_sum: 535.2015582025051
time_elpased: 1.821
batch start
#iterations: 42
currently lose_sum: 535.9042870998383
time_elpased: 1.802
batch start
#iterations: 43
currently lose_sum: 535.9088251888752
time_elpased: 1.775
batch start
#iterations: 44
currently lose_sum: 533.7841237783432
time_elpased: 1.785
batch start
#iterations: 45
currently lose_sum: 534.1308709681034
time_elpased: 1.786
batch start
#iterations: 46
currently lose_sum: 538.5281509757042
time_elpased: 1.791
batch start
#iterations: 47
currently lose_sum: 535.9385645389557
time_elpased: 1.792
batch start
#iterations: 48
currently lose_sum: 532.7169854640961
time_elpased: 1.79
batch start
#iterations: 49
currently lose_sum: 534.6593819260597
time_elpased: 1.788
batch start
#iterations: 50
currently lose_sum: 532.6408622264862
time_elpased: 1.79
batch start
#iterations: 51
currently lose_sum: 534.2742583751678
time_elpased: 1.804
batch start
#iterations: 52
currently lose_sum: 536.0664804577827
time_elpased: 1.801
batch start
#iterations: 53
currently lose_sum: 535.5721413493156
time_elpased: 1.8
batch start
#iterations: 54
currently lose_sum: 535.694945037365
time_elpased: 1.788
batch start
#iterations: 55
currently lose_sum: 533.5769746899605
time_elpased: 1.812
batch start
#iterations: 56
currently lose_sum: 534.2803333997726
time_elpased: 1.791
batch start
#iterations: 57
currently lose_sum: 534.4430085718632
time_elpased: 1.805
batch start
#iterations: 58
currently lose_sum: 534.7408233880997
time_elpased: 1.796
batch start
#iterations: 59
currently lose_sum: 533.6224440932274
time_elpased: 1.787
start validation test
0.517319587629
0.92576419214
0.0434693459094
0.0830395613004
0
validation finish
batch start
#iterations: 60
currently lose_sum: 535.2182456254959
time_elpased: 1.794
batch start
#iterations: 61
currently lose_sum: 533.2952208220959
time_elpased: 1.783
batch start
#iterations: 62
currently lose_sum: 534.5290036201477
time_elpased: 1.786
batch start
#iterations: 63
currently lose_sum: 535.6319389343262
time_elpased: 1.786
batch start
#iterations: 64
currently lose_sum: 533.6931818127632
time_elpased: 1.788
batch start
#iterations: 65
currently lose_sum: 534.5918256342411
time_elpased: 1.784
batch start
#iterations: 66
currently lose_sum: 535.8805277943611
time_elpased: 1.796
batch start
#iterations: 67
currently lose_sum: 532.8302257061005
time_elpased: 1.805
batch start
#iterations: 68
currently lose_sum: 532.9334896802902
time_elpased: 1.782
batch start
#iterations: 69
currently lose_sum: 533.937498152256
time_elpased: 1.787
batch start
#iterations: 70
currently lose_sum: 534.8354561924934
time_elpased: 1.79
batch start
#iterations: 71
currently lose_sum: 534.2701303958893
time_elpased: 1.794
batch start
#iterations: 72
currently lose_sum: 533.2974224090576
time_elpased: 1.806
batch start
#iterations: 73
currently lose_sum: 534.0209162831306
time_elpased: 1.81
batch start
#iterations: 74
currently lose_sum: 533.3145445287228
time_elpased: 1.801
batch start
#iterations: 75
currently lose_sum: 533.3789153695107
time_elpased: 1.789
batch start
#iterations: 76
currently lose_sum: 534.6687571704388
time_elpased: 1.798
batch start
#iterations: 77
currently lose_sum: 533.1748891472816
time_elpased: 1.783
batch start
#iterations: 78
currently lose_sum: 534.3333556950092
time_elpased: 1.788
batch start
#iterations: 79
currently lose_sum: 534.971274137497
time_elpased: 1.79
start validation test
0.535412371134
0.906695938529
0.0846832068895
0.154899203
0
validation finish
batch start
#iterations: 80
currently lose_sum: 534.1113592386246
time_elpased: 1.794
batch start
#iterations: 81
currently lose_sum: 534.5701912939548
time_elpased: 1.79
batch start
#iterations: 82
currently lose_sum: 533.1916121840477
time_elpased: 1.798
batch start
#iterations: 83
currently lose_sum: 532.1189189553261
time_elpased: 1.793
batch start
#iterations: 84
currently lose_sum: 532.6831971406937
time_elpased: 1.79
batch start
#iterations: 85
currently lose_sum: 533.4725230336189
time_elpased: 1.8
batch start
#iterations: 86
currently lose_sum: 532.6407039165497
time_elpased: 1.793
batch start
#iterations: 87
currently lose_sum: 533.7151013314724
time_elpased: 1.782
batch start
#iterations: 88
currently lose_sum: 534.3396707773209
time_elpased: 1.787
batch start
#iterations: 89
currently lose_sum: 532.3864395618439
time_elpased: 1.792
batch start
#iterations: 90
currently lose_sum: 532.5739748477936
time_elpased: 1.8
batch start
#iterations: 91
currently lose_sum: 531.7389848530293
time_elpased: 1.785
batch start
#iterations: 92
currently lose_sum: 531.360681772232
time_elpased: 1.806
batch start
#iterations: 93
currently lose_sum: 532.8630501627922
time_elpased: 1.787
batch start
#iterations: 94
currently lose_sum: 533.3832155764103
time_elpased: 1.785
batch start
#iterations: 95
currently lose_sum: 533.7887961268425
time_elpased: 1.785
batch start
#iterations: 96
currently lose_sum: 532.7840896546841
time_elpased: 1.785
batch start
#iterations: 97
currently lose_sum: 532.8467574715614
time_elpased: 1.794
batch start
#iterations: 98
currently lose_sum: 530.1271860301495
time_elpased: 1.789
batch start
#iterations: 99
currently lose_sum: 533.7115490436554
time_elpased: 1.8
start validation test
0.534381443299
0.912943871707
0.0817100676645
0.149995295003
0
validation finish
batch start
#iterations: 100
currently lose_sum: 533.7694383263588
time_elpased: 1.793
batch start
#iterations: 101
currently lose_sum: 532.3111658096313
time_elpased: 1.786
batch start
#iterations: 102
currently lose_sum: 535.1899862289429
time_elpased: 1.784
batch start
#iterations: 103
currently lose_sum: 531.7846207618713
time_elpased: 1.791
batch start
#iterations: 104
currently lose_sum: 530.6407423913479
time_elpased: 1.795
batch start
#iterations: 105
currently lose_sum: 531.059422492981
time_elpased: 1.813
batch start
#iterations: 106
currently lose_sum: 532.0792406201363
time_elpased: 1.79
batch start
#iterations: 107
currently lose_sum: 532.9093278646469
time_elpased: 1.79
batch start
#iterations: 108
currently lose_sum: 531.3067446649075
time_elpased: 1.804
batch start
#iterations: 109
currently lose_sum: 535.485564827919
time_elpased: 1.791
batch start
#iterations: 110
currently lose_sum: 533.2117008566856
time_elpased: 1.808
batch start
#iterations: 111
currently lose_sum: 534.3230244219303
time_elpased: 1.772
batch start
#iterations: 112
currently lose_sum: 533.3309161961079
time_elpased: 1.803
batch start
#iterations: 113
currently lose_sum: 533.5888963341713
time_elpased: 1.785
batch start
#iterations: 114
currently lose_sum: 532.9308991134167
time_elpased: 1.778
batch start
#iterations: 115
currently lose_sum: 532.5122877061367
time_elpased: 1.789
batch start
#iterations: 116
currently lose_sum: 534.09479367733
time_elpased: 1.792
batch start
#iterations: 117
currently lose_sum: 531.390513241291
time_elpased: 1.781
batch start
#iterations: 118
currently lose_sum: 532.7600239515305
time_elpased: 1.794
batch start
#iterations: 119
currently lose_sum: 531.6820216178894
time_elpased: 1.804
start validation test
0.549639175258
0.923397169026
0.113696944843
0.202464628024
0
validation finish
batch start
#iterations: 120
currently lose_sum: 532.0137466192245
time_elpased: 1.795
batch start
#iterations: 121
currently lose_sum: 533.5080798864365
time_elpased: 1.784
batch start
#iterations: 122
currently lose_sum: 534.5952352881432
time_elpased: 1.794
batch start
#iterations: 123
currently lose_sum: 533.9006858766079
time_elpased: 1.792
batch start
#iterations: 124
currently lose_sum: 531.6581082940102
time_elpased: 1.775
batch start
#iterations: 125
currently lose_sum: 531.9155593514442
time_elpased: 1.797
batch start
#iterations: 126
currently lose_sum: 530.7843854129314
time_elpased: 1.788
batch start
#iterations: 127
currently lose_sum: 531.9912560582161
time_elpased: 1.789
batch start
#iterations: 128
currently lose_sum: 534.7193483114243
time_elpased: 1.801
batch start
#iterations: 129
currently lose_sum: 531.3360592722893
time_elpased: 1.786
batch start
#iterations: 130
currently lose_sum: 532.1892983019352
time_elpased: 1.786
batch start
#iterations: 131
currently lose_sum: 532.4753887057304
time_elpased: 1.794
batch start
#iterations: 132
currently lose_sum: 532.0099865794182
time_elpased: 1.801
batch start
#iterations: 133
currently lose_sum: 532.4712156057358
time_elpased: 1.791
batch start
#iterations: 134
currently lose_sum: 533.0482573509216
time_elpased: 1.782
batch start
#iterations: 135
currently lose_sum: 531.5308153629303
time_elpased: 1.77
batch start
#iterations: 136
currently lose_sum: 532.3860223293304
time_elpased: 1.792
batch start
#iterations: 137
currently lose_sum: 531.5648050010204
time_elpased: 1.783
batch start
#iterations: 138
currently lose_sum: 532.8797290921211
time_elpased: 1.795
batch start
#iterations: 139
currently lose_sum: 533.0604942440987
time_elpased: 1.784
start validation test
0.550257731959
0.885971492873
0.121078531884
0.213042301795
0
validation finish
batch start
#iterations: 140
currently lose_sum: 531.9002217650414
time_elpased: 1.797
batch start
#iterations: 141
currently lose_sum: 532.6622376143932
time_elpased: 1.809
batch start
#iterations: 142
currently lose_sum: 532.212190747261
time_elpased: 1.788
batch start
#iterations: 143
currently lose_sum: 532.4462082386017
time_elpased: 1.797
batch start
#iterations: 144
currently lose_sum: 532.8685782849789
time_elpased: 1.793
batch start
#iterations: 145
currently lose_sum: 532.6308322250843
time_elpased: 1.789
batch start
#iterations: 146
currently lose_sum: 532.3145697712898
time_elpased: 1.797
batch start
#iterations: 147
currently lose_sum: 530.115432202816
time_elpased: 1.783
batch start
#iterations: 148
currently lose_sum: 532.7414899170399
time_elpased: 1.796
batch start
#iterations: 149
currently lose_sum: 533.2902522683144
time_elpased: 1.783
batch start
#iterations: 150
currently lose_sum: 532.1282555162907
time_elpased: 1.805
batch start
#iterations: 151
currently lose_sum: 529.9801783561707
time_elpased: 1.783
batch start
#iterations: 152
currently lose_sum: 531.4135165214539
time_elpased: 1.815
batch start
#iterations: 153
currently lose_sum: 529.6853581666946
time_elpased: 1.801
batch start
#iterations: 154
currently lose_sum: 533.024426817894
time_elpased: 1.786
batch start
#iterations: 155
currently lose_sum: 532.8129061460495
time_elpased: 1.778
batch start
#iterations: 156
currently lose_sum: 531.7714906036854
time_elpased: 1.807
batch start
#iterations: 157
currently lose_sum: 530.1268180310726
time_elpased: 1.824
batch start
#iterations: 158
currently lose_sum: 532.5193636119366
time_elpased: 1.778
batch start
#iterations: 159
currently lose_sum: 531.7641343176365
time_elpased: 1.796
start validation test
0.538453608247
0.920168067227
0.0898093090014
0.163646553335
0
validation finish
batch start
#iterations: 160
currently lose_sum: 532.870241612196
time_elpased: 1.8
batch start
#iterations: 161
currently lose_sum: 532.338084936142
time_elpased: 1.796
batch start
#iterations: 162
currently lose_sum: 531.459370970726
time_elpased: 1.798
batch start
#iterations: 163
currently lose_sum: 531.5865948796272
time_elpased: 1.792
batch start
#iterations: 164
currently lose_sum: 531.7016663253307
time_elpased: 1.792
batch start
#iterations: 165
currently lose_sum: 534.1054002642632
time_elpased: 1.795
batch start
#iterations: 166
currently lose_sum: 532.9657700061798
time_elpased: 1.778
batch start
#iterations: 167
currently lose_sum: 533.3725197315216
time_elpased: 1.793
batch start
#iterations: 168
currently lose_sum: 530.937717974186
time_elpased: 1.775
batch start
#iterations: 169
currently lose_sum: 530.9062040448189
time_elpased: 1.819
batch start
#iterations: 170
currently lose_sum: 532.8573717176914
time_elpased: 1.797
batch start
#iterations: 171
currently lose_sum: 533.3230157196522
time_elpased: 1.788
batch start
#iterations: 172
currently lose_sum: 531.9459126889706
time_elpased: 1.822
batch start
#iterations: 173
currently lose_sum: 530.9887262880802
time_elpased: 1.786
batch start
#iterations: 174
currently lose_sum: 533.3817487955093
time_elpased: 1.793
batch start
#iterations: 175
currently lose_sum: 532.9405781626701
time_elpased: 1.773
batch start
#iterations: 176
currently lose_sum: 533.2816165685654
time_elpased: 1.798
batch start
#iterations: 177
currently lose_sum: 532.1125694215298
time_elpased: 1.792
batch start
#iterations: 178
currently lose_sum: 531.8158012628555
time_elpased: 1.79
batch start
#iterations: 179
currently lose_sum: 533.4731385409832
time_elpased: 1.795
start validation test
0.541546391753
0.939672801636
0.0942177568177
0.171263510995
0
validation finish
batch start
#iterations: 180
currently lose_sum: 532.8192789852619
time_elpased: 1.813
batch start
#iterations: 181
currently lose_sum: 533.7296015620232
time_elpased: 1.783
batch start
#iterations: 182
currently lose_sum: 531.6490507125854
time_elpased: 1.789
batch start
#iterations: 183
currently lose_sum: 530.7976298034191
time_elpased: 1.795
batch start
#iterations: 184
currently lose_sum: 532.0878071188927
time_elpased: 1.796
batch start
#iterations: 185
currently lose_sum: 531.2633425593376
time_elpased: 1.791
batch start
#iterations: 186
currently lose_sum: 530.5218901336193
time_elpased: 1.795
batch start
#iterations: 187
currently lose_sum: 533.7357627153397
time_elpased: 1.789
batch start
#iterations: 188
currently lose_sum: 531.5978601574898
time_elpased: 1.796
batch start
#iterations: 189
currently lose_sum: 530.9313865602016
time_elpased: 1.787
batch start
#iterations: 190
currently lose_sum: 531.8641468286514
time_elpased: 1.796
batch start
#iterations: 191
currently lose_sum: 531.3115603923798
time_elpased: 1.802
batch start
#iterations: 192
currently lose_sum: 530.7936391234398
time_elpased: 1.808
batch start
#iterations: 193
currently lose_sum: 531.485403239727
time_elpased: 1.798
batch start
#iterations: 194
currently lose_sum: 532.420537352562
time_elpased: 1.802
batch start
#iterations: 195
currently lose_sum: 530.8044526875019
time_elpased: 1.894
batch start
#iterations: 196
currently lose_sum: 532.5461257100105
time_elpased: 1.793
batch start
#iterations: 197
currently lose_sum: 531.7514950037003
time_elpased: 1.787
batch start
#iterations: 198
currently lose_sum: 531.3018142282963
time_elpased: 1.791
batch start
#iterations: 199
currently lose_sum: 534.1940587759018
time_elpased: 1.795
start validation test
0.557113402062
0.921625544267
0.130202993644
0.228171038448
0
validation finish
batch start
#iterations: 200
currently lose_sum: 527.2072876691818
time_elpased: 1.801
batch start
#iterations: 201
currently lose_sum: 532.0603155493736
time_elpased: 1.801
batch start
#iterations: 202
currently lose_sum: 531.0812251269817
time_elpased: 1.795
batch start
#iterations: 203
currently lose_sum: 532.9921238124371
time_elpased: 1.849
batch start
#iterations: 204
currently lose_sum: 531.0254997313023
time_elpased: 1.798
batch start
#iterations: 205
currently lose_sum: 531.1648050248623
time_elpased: 1.799
batch start
#iterations: 206
currently lose_sum: 529.9393029510975
time_elpased: 1.875
batch start
#iterations: 207
currently lose_sum: 530.6848172843456
time_elpased: 1.796
batch start
#iterations: 208
currently lose_sum: 531.646455436945
time_elpased: 1.811
batch start
#iterations: 209
currently lose_sum: 531.2054359018803
time_elpased: 1.79
batch start
#iterations: 210
currently lose_sum: 530.4097891449928
time_elpased: 1.811
batch start
#iterations: 211
currently lose_sum: 531.3386242687702
time_elpased: 1.787
batch start
#iterations: 212
currently lose_sum: 530.674825578928
time_elpased: 1.789
batch start
#iterations: 213
currently lose_sum: 532.7647354006767
time_elpased: 1.781
batch start
#iterations: 214
currently lose_sum: 531.1238133311272
time_elpased: 1.791
batch start
#iterations: 215
currently lose_sum: 532.8941346108913
time_elpased: 1.784
batch start
#iterations: 216
currently lose_sum: 532.1871480047703
time_elpased: 1.799
batch start
#iterations: 217
currently lose_sum: 532.7889904379845
time_elpased: 1.778
batch start
#iterations: 218
currently lose_sum: 532.1018404662609
time_elpased: 1.789
batch start
#iterations: 219
currently lose_sum: 531.9998267591
time_elpased: 1.788
start validation test
0.543659793814
0.902591599643
0.103547262661
0.185781293111
0
validation finish
batch start
#iterations: 220
currently lose_sum: 531.5561218261719
time_elpased: 1.81
batch start
#iterations: 221
currently lose_sum: 533.8981423974037
time_elpased: 1.799
batch start
#iterations: 222
currently lose_sum: 532.5741764307022
time_elpased: 1.797
batch start
#iterations: 223
currently lose_sum: 531.4907293319702
time_elpased: 1.799
batch start
#iterations: 224
currently lose_sum: 532.6380065083504
time_elpased: 1.782
batch start
#iterations: 225
currently lose_sum: 531.5956865549088
time_elpased: 1.788
batch start
#iterations: 226
currently lose_sum: 531.456622749567
time_elpased: 1.795
batch start
#iterations: 227
currently lose_sum: 530.3422459363937
time_elpased: 1.798
batch start
#iterations: 228
currently lose_sum: 529.7259324789047
time_elpased: 1.801
batch start
#iterations: 229
currently lose_sum: 529.6212817430496
time_elpased: 1.809
batch start
#iterations: 230
currently lose_sum: 530.9823341965675
time_elpased: 1.806
batch start
#iterations: 231
currently lose_sum: 532.6238943636417
time_elpased: 1.804
batch start
#iterations: 232
currently lose_sum: 531.8135619163513
time_elpased: 1.793
batch start
#iterations: 233
currently lose_sum: 533.1102781891823
time_elpased: 1.787
batch start
#iterations: 234
currently lose_sum: 533.5154467821121
time_elpased: 1.797
batch start
#iterations: 235
currently lose_sum: 530.4406058490276
time_elpased: 1.775
batch start
#iterations: 236
currently lose_sum: 529.2445449531078
time_elpased: 1.795
batch start
#iterations: 237
currently lose_sum: 531.3126009702682
time_elpased: 1.789
batch start
#iterations: 238
currently lose_sum: 530.2413073778152
time_elpased: 1.795
batch start
#iterations: 239
currently lose_sum: 532.1788752675056
time_elpased: 1.785
start validation test
0.556237113402
0.91515591008
0.129382817306
0.226713374652
0
validation finish
batch start
#iterations: 240
currently lose_sum: 530.446447879076
time_elpased: 1.804
batch start
#iterations: 241
currently lose_sum: 531.5297853052616
time_elpased: 1.788
batch start
#iterations: 242
currently lose_sum: 532.3872821927071
time_elpased: 1.813
batch start
#iterations: 243
currently lose_sum: 530.0169970393181
time_elpased: 1.78
batch start
#iterations: 244
currently lose_sum: 533.5387109518051
time_elpased: 1.786
batch start
#iterations: 245
currently lose_sum: 531.6908169984818
time_elpased: 1.797
batch start
#iterations: 246
currently lose_sum: 532.1169738173485
time_elpased: 1.798
batch start
#iterations: 247
currently lose_sum: 532.4539080560207
time_elpased: 1.808
batch start
#iterations: 248
currently lose_sum: 530.0806806981564
time_elpased: 1.801
batch start
#iterations: 249
currently lose_sum: 532.8502568006516
time_elpased: 1.796
batch start
#iterations: 250
currently lose_sum: 531.9847289323807
time_elpased: 1.785
batch start
#iterations: 251
currently lose_sum: 531.2644431591034
time_elpased: 1.78
batch start
#iterations: 252
currently lose_sum: 531.0561846494675
time_elpased: 1.79
batch start
#iterations: 253
currently lose_sum: 530.0870523750782
time_elpased: 1.828
batch start
#iterations: 254
currently lose_sum: 530.4222944974899
time_elpased: 1.785
batch start
#iterations: 255
currently lose_sum: 530.6033076047897
time_elpased: 1.792
batch start
#iterations: 256
currently lose_sum: 530.2033833563328
time_elpased: 1.791
batch start
#iterations: 257
currently lose_sum: 529.632018327713
time_elpased: 1.792
batch start
#iterations: 258
currently lose_sum: 529.0360952019691
time_elpased: 1.795
batch start
#iterations: 259
currently lose_sum: 533.4968401193619
time_elpased: 1.786
start validation test
0.558350515464
0.917018284107
0.13368874308
0.233357193987
0
validation finish
batch start
#iterations: 260
currently lose_sum: 529.6811031699181
time_elpased: 1.795
batch start
#iterations: 261
currently lose_sum: 531.1512232720852
time_elpased: 1.782
batch start
#iterations: 262
currently lose_sum: 532.6338056325912
time_elpased: 1.79
batch start
#iterations: 263
currently lose_sum: 531.4849480688572
time_elpased: 1.802
batch start
#iterations: 264
currently lose_sum: 533.5415461957455
time_elpased: 1.795
batch start
#iterations: 265
currently lose_sum: 531.5168155133724
time_elpased: 1.795
batch start
#iterations: 266
currently lose_sum: 531.1896139681339
time_elpased: 1.79
batch start
#iterations: 267
currently lose_sum: 532.5499041676521
time_elpased: 1.799
batch start
#iterations: 268
currently lose_sum: 529.3734200000763
time_elpased: 1.796
batch start
#iterations: 269
currently lose_sum: 529.9999315738678
time_elpased: 1.798
batch start
#iterations: 270
currently lose_sum: 531.6262377500534
time_elpased: 1.787
batch start
#iterations: 271
currently lose_sum: 532.6978751122952
time_elpased: 1.792
batch start
#iterations: 272
currently lose_sum: 531.575789809227
time_elpased: 1.796
batch start
#iterations: 273
currently lose_sum: 530.7360655069351
time_elpased: 1.802
batch start
#iterations: 274
currently lose_sum: 532.9707751572132
time_elpased: 1.815
batch start
#iterations: 275
currently lose_sum: 530.3834351599216
time_elpased: 1.779
batch start
#iterations: 276
currently lose_sum: 532.2844527959824
time_elpased: 1.788
batch start
#iterations: 277
currently lose_sum: 532.4400273263454
time_elpased: 1.786
batch start
#iterations: 278
currently lose_sum: 531.7689388096333
time_elpased: 1.791
batch start
#iterations: 279
currently lose_sum: 530.5744201242924
time_elpased: 1.791
start validation test
0.572216494845
0.916905444126
0.164035267583
0.278285068267
0
validation finish
batch start
#iterations: 280
currently lose_sum: 532.090926349163
time_elpased: 1.812
batch start
#iterations: 281
currently lose_sum: 530.6875632107258
time_elpased: 1.797
batch start
#iterations: 282
currently lose_sum: 531.4725514054298
time_elpased: 1.799
batch start
#iterations: 283
currently lose_sum: 532.898100912571
time_elpased: 1.798
batch start
#iterations: 284
currently lose_sum: 530.5314587950706
time_elpased: 1.798
batch start
#iterations: 285
currently lose_sum: 529.5042109489441
time_elpased: 1.794
batch start
#iterations: 286
currently lose_sum: 529.6018058657646
time_elpased: 1.794
batch start
#iterations: 287
currently lose_sum: 531.8608454167843
time_elpased: 1.807
batch start
#iterations: 288
currently lose_sum: 533.5596604347229
time_elpased: 1.784
batch start
#iterations: 289
currently lose_sum: 533.2767086625099
time_elpased: 1.811
batch start
#iterations: 290
currently lose_sum: 531.5925675034523
time_elpased: 1.786
batch start
#iterations: 291
currently lose_sum: 530.9411855340004
time_elpased: 1.802
batch start
#iterations: 292
currently lose_sum: 530.570149064064
time_elpased: 1.813
batch start
#iterations: 293
currently lose_sum: 532.9766327738762
time_elpased: 1.801
batch start
#iterations: 294
currently lose_sum: 529.536672025919
time_elpased: 1.792
batch start
#iterations: 295
currently lose_sum: 530.5368993282318
time_elpased: 1.807
batch start
#iterations: 296
currently lose_sum: 530.8382658958435
time_elpased: 1.804
batch start
#iterations: 297
currently lose_sum: 530.6262060403824
time_elpased: 1.776
batch start
#iterations: 298
currently lose_sum: 531.0569462776184
time_elpased: 1.795
batch start
#iterations: 299
currently lose_sum: 532.1061261892319
time_elpased: 1.791
start validation test
0.550927835052
0.933444259567
0.115029731392
0.204819277108
0
validation finish
batch start
#iterations: 300
currently lose_sum: 531.2566174268723
time_elpased: 1.813
batch start
#iterations: 301
currently lose_sum: 531.1474798619747
time_elpased: 1.804
batch start
#iterations: 302
currently lose_sum: 531.2201708853245
time_elpased: 1.802
batch start
#iterations: 303
currently lose_sum: 533.5979317426682
time_elpased: 1.817
batch start
#iterations: 304
currently lose_sum: 530.4780077338219
time_elpased: 1.794
batch start
#iterations: 305
currently lose_sum: 532.0407712459564
time_elpased: 1.796
batch start
#iterations: 306
currently lose_sum: 530.0108450055122
time_elpased: 1.806
batch start
#iterations: 307
currently lose_sum: 530.9497899115086
time_elpased: 1.804
batch start
#iterations: 308
currently lose_sum: 531.3236084282398
time_elpased: 1.8
batch start
#iterations: 309
currently lose_sum: 530.6837396621704
time_elpased: 1.779
batch start
#iterations: 310
currently lose_sum: 531.9314005970955
time_elpased: 1.794
batch start
#iterations: 311
currently lose_sum: 531.3350474238396
time_elpased: 1.787
batch start
#iterations: 312
currently lose_sum: 529.4895017147064
time_elpased: 1.806
batch start
#iterations: 313
currently lose_sum: 532.7972384095192
time_elpased: 1.79
batch start
#iterations: 314
currently lose_sum: 532.8524513840675
time_elpased: 1.799
batch start
#iterations: 315
currently lose_sum: 531.7988526821136
time_elpased: 1.792
batch start
#iterations: 316
currently lose_sum: 531.3266223371029
time_elpased: 1.806
batch start
#iterations: 317
currently lose_sum: 531.6327685415745
time_elpased: 1.777
batch start
#iterations: 318
currently lose_sum: 529.9281203448772
time_elpased: 1.808
batch start
#iterations: 319
currently lose_sum: 529.2220248281956
time_elpased: 1.791
start validation test
0.557164948454
0.919855595668
0.130613081813
0.228745847922
0
validation finish
batch start
#iterations: 320
currently lose_sum: 532.7125347852707
time_elpased: 1.804
batch start
#iterations: 321
currently lose_sum: 529.5250385403633
time_elpased: 1.779
batch start
#iterations: 322
currently lose_sum: 530.0972971320152
time_elpased: 1.806
batch start
#iterations: 323
currently lose_sum: 531.2697402238846
time_elpased: 1.791
batch start
#iterations: 324
currently lose_sum: 528.7107241153717
time_elpased: 1.794
batch start
#iterations: 325
currently lose_sum: 532.9958326816559
time_elpased: 1.79
batch start
#iterations: 326
currently lose_sum: 531.4544405937195
time_elpased: 1.787
batch start
#iterations: 327
currently lose_sum: 532.6655673384666
time_elpased: 1.809
batch start
#iterations: 328
currently lose_sum: 531.4442613422871
time_elpased: 1.786
batch start
#iterations: 329
currently lose_sum: 529.2363786101341
time_elpased: 1.782
batch start
#iterations: 330
currently lose_sum: 531.9594388306141
time_elpased: 1.789
batch start
#iterations: 331
currently lose_sum: 530.6706200540066
time_elpased: 1.793
batch start
#iterations: 332
currently lose_sum: 531.2188990116119
time_elpased: 1.791
batch start
#iterations: 333
currently lose_sum: 531.316200375557
time_elpased: 1.796
batch start
#iterations: 334
currently lose_sum: 529.8416447043419
time_elpased: 1.803
batch start
#iterations: 335
currently lose_sum: 529.9877754151821
time_elpased: 1.806
batch start
#iterations: 336
currently lose_sum: 530.5280828177929
time_elpased: 1.797
batch start
#iterations: 337
currently lose_sum: 531.3099328875542
time_elpased: 1.781
batch start
#iterations: 338
currently lose_sum: 533.4799904823303
time_elpased: 1.793
batch start
#iterations: 339
currently lose_sum: 530.6367538273335
time_elpased: 1.79
start validation test
0.547525773196
0.937275985663
0.107238056182
0.192456301748
0
validation finish
batch start
#iterations: 340
currently lose_sum: 531.5151726901531
time_elpased: 1.811
batch start
#iterations: 341
currently lose_sum: 532.9741006493568
time_elpased: 1.787
batch start
#iterations: 342
currently lose_sum: 531.0991706848145
time_elpased: 1.838
batch start
#iterations: 343
currently lose_sum: 532.7927317917347
time_elpased: 1.791
batch start
#iterations: 344
currently lose_sum: 531.83423396945
time_elpased: 1.826
batch start
#iterations: 345
currently lose_sum: 531.7577178180218
time_elpased: 1.775
batch start
#iterations: 346
currently lose_sum: 529.2302956581116
time_elpased: 1.798
batch start
#iterations: 347
currently lose_sum: 531.6657436788082
time_elpased: 1.806
batch start
#iterations: 348
currently lose_sum: 532.253747433424
time_elpased: 1.773
batch start
#iterations: 349
currently lose_sum: 529.0163520872593
time_elpased: 1.789
batch start
#iterations: 350
currently lose_sum: 529.9167739152908
time_elpased: 1.791
batch start
#iterations: 351
currently lose_sum: 532.5905547142029
time_elpased: 1.805
batch start
#iterations: 352
currently lose_sum: 532.930407255888
time_elpased: 1.782
batch start
#iterations: 353
currently lose_sum: 530.1820515394211
time_elpased: 1.79
batch start
#iterations: 354
currently lose_sum: 531.3352922201157
time_elpased: 1.782
batch start
#iterations: 355
currently lose_sum: 532.0950212478638
time_elpased: 1.791
batch start
#iterations: 356
currently lose_sum: 530.9829601943493
time_elpased: 1.793
batch start
#iterations: 357
currently lose_sum: 530.881517380476
time_elpased: 1.787
batch start
#iterations: 358
currently lose_sum: 531.7224861383438
time_elpased: 1.807
batch start
#iterations: 359
currently lose_sum: 529.5010124742985
time_elpased: 1.777
start validation test
0.566597938144
0.91801242236
0.151527578429
0.26011967617
0
validation finish
batch start
#iterations: 360
currently lose_sum: 531.3947238326073
time_elpased: 1.796
batch start
#iterations: 361
currently lose_sum: 529.656388759613
time_elpased: 1.779
batch start
#iterations: 362
currently lose_sum: 530.6100587248802
time_elpased: 1.797
batch start
#iterations: 363
currently lose_sum: 529.9198949337006
time_elpased: 1.793
batch start
#iterations: 364
currently lose_sum: 531.3134088218212
time_elpased: 1.81
batch start
#iterations: 365
currently lose_sum: 530.2758246660233
time_elpased: 1.811
batch start
#iterations: 366
currently lose_sum: 530.9513455331326
time_elpased: 1.793
batch start
#iterations: 367
currently lose_sum: 532.6449864506721
time_elpased: 1.784
batch start
#iterations: 368
currently lose_sum: 532.0454418063164
time_elpased: 1.788
batch start
#iterations: 369
currently lose_sum: 529.7988570928574
time_elpased: 1.775
batch start
#iterations: 370
currently lose_sum: 531.8175115585327
time_elpased: 1.779
batch start
#iterations: 371
currently lose_sum: 530.1717012524605
time_elpased: 1.796
batch start
#iterations: 372
currently lose_sum: 533.1265124678612
time_elpased: 1.798
batch start
#iterations: 373
currently lose_sum: 534.4736044406891
time_elpased: 1.796
batch start
#iterations: 374
currently lose_sum: 529.8180133104324
time_elpased: 1.796
batch start
#iterations: 375
currently lose_sum: 532.3786174952984
time_elpased: 1.797
batch start
#iterations: 376
currently lose_sum: 530.2642498016357
time_elpased: 1.78
batch start
#iterations: 377
currently lose_sum: 531.7275587916374
time_elpased: 1.794
batch start
#iterations: 378
currently lose_sum: 532.4249812662601
time_elpased: 1.788
batch start
#iterations: 379
currently lose_sum: 529.043525993824
time_elpased: 1.802
start validation test
0.555154639175
0.925113464448
0.125384457658
0.220837847598
0
validation finish
batch start
#iterations: 380
currently lose_sum: 530.5255838930607
time_elpased: 1.807
batch start
#iterations: 381
currently lose_sum: 530.3303038775921
time_elpased: 1.797
batch start
#iterations: 382
currently lose_sum: 530.4861113131046
time_elpased: 1.792
batch start
#iterations: 383
currently lose_sum: 532.5371457338333
time_elpased: 1.779
batch start
#iterations: 384
currently lose_sum: 531.5096197128296
time_elpased: 1.814
batch start
#iterations: 385
currently lose_sum: 530.1304853260517
time_elpased: 1.797
batch start
#iterations: 386
currently lose_sum: 533.0888011455536
time_elpased: 1.822
batch start
#iterations: 387
currently lose_sum: 530.1383703351021
time_elpased: 1.779
batch start
#iterations: 388
currently lose_sum: 530.5543490946293
time_elpased: 1.792
batch start
#iterations: 389
currently lose_sum: 530.5761623084545
time_elpased: 1.787
batch start
#iterations: 390
currently lose_sum: 529.7853047847748
time_elpased: 1.801
batch start
#iterations: 391
currently lose_sum: 529.4204396605492
time_elpased: 1.79
batch start
#iterations: 392
currently lose_sum: 531.1367661058903
time_elpased: 1.779
batch start
#iterations: 393
currently lose_sum: 531.1800344586372
time_elpased: 1.817
batch start
#iterations: 394
currently lose_sum: 529.8470378518105
time_elpased: 1.781
batch start
#iterations: 395
currently lose_sum: 532.2640810310841
time_elpased: 1.811
batch start
#iterations: 396
currently lose_sum: 531.0761867165565
time_elpased: 1.781
batch start
#iterations: 397
currently lose_sum: 532.5508895516396
time_elpased: 1.804
batch start
#iterations: 398
currently lose_sum: 529.9678255021572
time_elpased: 1.795
batch start
#iterations: 399
currently lose_sum: 530.8658714294434
time_elpased: 1.791
start validation test
0.567680412371
0.913991520291
0.154705761739
0.264620780359
0
validation finish
acc: 0.570
pre: 0.925
rec: 0.160
F1: 0.272
auc: 0.000
