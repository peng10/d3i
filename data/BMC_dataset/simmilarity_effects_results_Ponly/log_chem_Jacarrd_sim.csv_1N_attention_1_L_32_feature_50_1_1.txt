start to construct graph
graph construct over
58300
epochs start
batch start
#iterations: 0
currently lose_sum: 405.25011175870895
time_elpased: 1.133
batch start
#iterations: 1
currently lose_sum: 403.4305573105812
time_elpased: 1.112
batch start
#iterations: 2
currently lose_sum: 402.3014270067215
time_elpased: 1.112
batch start
#iterations: 3
currently lose_sum: 402.115767121315
time_elpased: 1.128
batch start
#iterations: 4
currently lose_sum: 401.2524185180664
time_elpased: 1.123
batch start
#iterations: 5
currently lose_sum: 401.0749029517174
time_elpased: 1.117
batch start
#iterations: 6
currently lose_sum: 400.2061648964882
time_elpased: 1.127
batch start
#iterations: 7
currently lose_sum: 400.4315205812454
time_elpased: 1.126
batch start
#iterations: 8
currently lose_sum: 398.9935676455498
time_elpased: 1.116
batch start
#iterations: 9
currently lose_sum: 397.67336469888687
time_elpased: 1.103
batch start
#iterations: 10
currently lose_sum: 396.1876272559166
time_elpased: 1.128
batch start
#iterations: 11
currently lose_sum: 395.3428715467453
time_elpased: 1.115
batch start
#iterations: 12
currently lose_sum: 394.66523027420044
time_elpased: 1.104
batch start
#iterations: 13
currently lose_sum: 393.8278836607933
time_elpased: 1.102
batch start
#iterations: 14
currently lose_sum: 392.3214538693428
time_elpased: 1.109
batch start
#iterations: 15
currently lose_sum: 392.30884206295013
time_elpased: 1.11
batch start
#iterations: 16
currently lose_sum: 390.6630721092224
time_elpased: 1.107
batch start
#iterations: 17
currently lose_sum: 391.2300651073456
time_elpased: 1.116
batch start
#iterations: 18
currently lose_sum: 390.7000267505646
time_elpased: 1.114
batch start
#iterations: 19
currently lose_sum: 389.3689980506897
time_elpased: 1.114
start validation test
0.352164948454
1.0
0.352164948454
0.520890515401
0
validation finish
batch start
#iterations: 20
currently lose_sum: 388.92799150943756
time_elpased: 1.127
batch start
#iterations: 21
currently lose_sum: 387.5335778594017
time_elpased: 1.116
batch start
#iterations: 22
currently lose_sum: 387.3501769900322
time_elpased: 1.112
batch start
#iterations: 23
currently lose_sum: 387.03570008277893
time_elpased: 1.105
batch start
#iterations: 24
currently lose_sum: 386.5332251191139
time_elpased: 1.118
batch start
#iterations: 25
currently lose_sum: 386.35136622190475
time_elpased: 1.12
batch start
#iterations: 26
currently lose_sum: 385.63968366384506
time_elpased: 1.102
batch start
#iterations: 27
currently lose_sum: 385.7222679257393
time_elpased: 1.115
batch start
#iterations: 28
currently lose_sum: 385.12823659181595
time_elpased: 1.11
batch start
#iterations: 29
currently lose_sum: 385.6715723872185
time_elpased: 1.125
batch start
#iterations: 30
currently lose_sum: 383.5785415172577
time_elpased: 1.112
batch start
#iterations: 31
currently lose_sum: 383.98522198200226
time_elpased: 1.103
batch start
#iterations: 32
currently lose_sum: 382.98738437891006
time_elpased: 1.117
batch start
#iterations: 33
currently lose_sum: 382.6378197669983
time_elpased: 1.111
batch start
#iterations: 34
currently lose_sum: 382.0679428577423
time_elpased: 1.104
batch start
#iterations: 35
currently lose_sum: 382.29320442676544
time_elpased: 1.122
batch start
#iterations: 36
currently lose_sum: 381.88354671001434
time_elpased: 1.118
batch start
#iterations: 37
currently lose_sum: 380.02616810798645
time_elpased: 1.107
batch start
#iterations: 38
currently lose_sum: 380.70222306251526
time_elpased: 1.116
batch start
#iterations: 39
currently lose_sum: 381.16830492019653
time_elpased: 1.107
start validation test
0.569072164948
1.0
0.569072164948
0.725361366623
0
validation finish
batch start
#iterations: 40
currently lose_sum: 381.62892013788223
time_elpased: 1.104
batch start
#iterations: 41
currently lose_sum: 379.4477182626724
time_elpased: 1.113
batch start
#iterations: 42
currently lose_sum: 381.5640324354172
time_elpased: 1.123
batch start
#iterations: 43
currently lose_sum: 378.9502462744713
time_elpased: 1.116
batch start
#iterations: 44
currently lose_sum: 379.2639867067337
time_elpased: 1.115
batch start
#iterations: 45
currently lose_sum: 379.95464408397675
time_elpased: 1.111
batch start
#iterations: 46
currently lose_sum: 379.9251943230629
time_elpased: 1.114
batch start
#iterations: 47
currently lose_sum: 379.94549602270126
time_elpased: 1.123
batch start
#iterations: 48
currently lose_sum: 379.993461728096
time_elpased: 1.108
batch start
#iterations: 49
currently lose_sum: 379.4124044775963
time_elpased: 1.138
batch start
#iterations: 50
currently lose_sum: 379.2957261800766
time_elpased: 1.12
batch start
#iterations: 51
currently lose_sum: 377.8772194981575
time_elpased: 1.125
batch start
#iterations: 52
currently lose_sum: 377.89369636774063
time_elpased: 1.113
batch start
#iterations: 53
currently lose_sum: 377.40713942050934
time_elpased: 1.112
batch start
#iterations: 54
currently lose_sum: 378.3632338643074
time_elpased: 1.122
batch start
#iterations: 55
currently lose_sum: 377.9503003358841
time_elpased: 1.135
batch start
#iterations: 56
currently lose_sum: 378.808018386364
time_elpased: 1.119
batch start
#iterations: 57
currently lose_sum: 376.08895045518875
time_elpased: 1.113
batch start
#iterations: 58
currently lose_sum: 376.45988816022873
time_elpased: 1.114
batch start
#iterations: 59
currently lose_sum: 376.17149180173874
time_elpased: 1.103
start validation test
0.702371134021
1.0
0.702371134021
0.825168049416
0
validation finish
batch start
#iterations: 60
currently lose_sum: 376.08195328712463
time_elpased: 1.119
batch start
#iterations: 61
currently lose_sum: 377.190777361393
time_elpased: 1.121
batch start
#iterations: 62
currently lose_sum: 376.59759563207626
time_elpased: 1.108
batch start
#iterations: 63
currently lose_sum: 374.2139009833336
time_elpased: 1.11
batch start
#iterations: 64
currently lose_sum: 375.39906603097916
time_elpased: 1.119
batch start
#iterations: 65
currently lose_sum: 375.0244410634041
time_elpased: 1.107
batch start
#iterations: 66
currently lose_sum: 374.57970386743546
time_elpased: 1.115
batch start
#iterations: 67
currently lose_sum: 374.183081805706
time_elpased: 1.114
batch start
#iterations: 68
currently lose_sum: 376.0939230322838
time_elpased: 1.107
batch start
#iterations: 69
currently lose_sum: 375.4918313026428
time_elpased: 1.112
batch start
#iterations: 70
currently lose_sum: 374.27798306941986
time_elpased: 1.108
batch start
#iterations: 71
currently lose_sum: 374.79707193374634
time_elpased: 1.13
batch start
#iterations: 72
currently lose_sum: 373.6864620447159
time_elpased: 1.116
batch start
#iterations: 73
currently lose_sum: 374.63689947128296
time_elpased: 1.115
batch start
#iterations: 74
currently lose_sum: 374.36705589294434
time_elpased: 1.123
batch start
#iterations: 75
currently lose_sum: 373.9355477690697
time_elpased: 1.117
batch start
#iterations: 76
currently lose_sum: 373.080904006958
time_elpased: 1.118
batch start
#iterations: 77
currently lose_sum: 373.98746395111084
time_elpased: 1.105
batch start
#iterations: 78
currently lose_sum: 373.68714410066605
time_elpased: 1.139
batch start
#iterations: 79
currently lose_sum: 374.0413767695427
time_elpased: 1.127
start validation test
0.792474226804
1.0
0.792474226804
0.884223845402
0
validation finish
batch start
#iterations: 80
currently lose_sum: 372.4652621746063
time_elpased: 1.116
batch start
#iterations: 81
currently lose_sum: 373.1352862715721
time_elpased: 1.122
batch start
#iterations: 82
currently lose_sum: 373.52204716205597
time_elpased: 1.147
batch start
#iterations: 83
currently lose_sum: 372.8399928212166
time_elpased: 1.133
batch start
#iterations: 84
currently lose_sum: 373.3361619710922
time_elpased: 1.108
batch start
#iterations: 85
currently lose_sum: 371.9622296690941
time_elpased: 1.122
batch start
#iterations: 86
currently lose_sum: 372.13414281606674
time_elpased: 1.109
batch start
#iterations: 87
currently lose_sum: 373.37841796875
time_elpased: 1.111
batch start
#iterations: 88
currently lose_sum: 372.16204822063446
time_elpased: 1.143
batch start
#iterations: 89
currently lose_sum: 372.0223628282547
time_elpased: 1.11
batch start
#iterations: 90
currently lose_sum: 371.82954704761505
time_elpased: 1.103
batch start
#iterations: 91
currently lose_sum: 372.42335844039917
time_elpased: 1.107
batch start
#iterations: 92
currently lose_sum: 372.5073371529579
time_elpased: 1.115
batch start
#iterations: 93
currently lose_sum: 372.40861493349075
time_elpased: 1.108
batch start
#iterations: 94
currently lose_sum: 372.4915339946747
time_elpased: 1.123
batch start
#iterations: 95
currently lose_sum: 371.6675937175751
time_elpased: 1.174
batch start
#iterations: 96
currently lose_sum: 371.73901295661926
time_elpased: 1.107
batch start
#iterations: 97
currently lose_sum: 372.19795340299606
time_elpased: 1.167
batch start
#iterations: 98
currently lose_sum: 371.72623431682587
time_elpased: 1.106
batch start
#iterations: 99
currently lose_sum: 372.9211777448654
time_elpased: 1.102
start validation test
0.723505154639
1.0
0.723505154639
0.839574111736
0
validation finish
batch start
#iterations: 100
currently lose_sum: 370.91741865873337
time_elpased: 1.103
batch start
#iterations: 101
currently lose_sum: 371.5177181959152
time_elpased: 1.129
batch start
#iterations: 102
currently lose_sum: 371.6106765270233
time_elpased: 1.116
batch start
#iterations: 103
currently lose_sum: 370.64352399110794
time_elpased: 1.171
batch start
#iterations: 104
currently lose_sum: 371.4926692247391
time_elpased: 1.111
batch start
#iterations: 105
currently lose_sum: 371.76730865240097
time_elpased: 1.124
batch start
#iterations: 106
currently lose_sum: 372.51630610227585
time_elpased: 1.116
batch start
#iterations: 107
currently lose_sum: 371.55093175172806
time_elpased: 1.157
batch start
#iterations: 108
currently lose_sum: 370.81775879859924
time_elpased: 1.105
batch start
#iterations: 109
currently lose_sum: 370.08857583999634
time_elpased: 1.11
batch start
#iterations: 110
currently lose_sum: 371.763729095459
time_elpased: 1.116
batch start
#iterations: 111
currently lose_sum: 370.9828100204468
time_elpased: 1.173
batch start
#iterations: 112
currently lose_sum: 369.9116117954254
time_elpased: 1.123
batch start
#iterations: 113
currently lose_sum: 371.25324296951294
time_elpased: 1.118
batch start
#iterations: 114
currently lose_sum: 371.2926360964775
time_elpased: 1.106
batch start
#iterations: 115
currently lose_sum: 371.5932050347328
time_elpased: 1.133
batch start
#iterations: 116
currently lose_sum: 369.195650100708
time_elpased: 1.114
batch start
#iterations: 117
currently lose_sum: 371.63531905412674
time_elpased: 1.113
batch start
#iterations: 118
currently lose_sum: 369.6009024977684
time_elpased: 1.154
batch start
#iterations: 119
currently lose_sum: 369.32334738969803
time_elpased: 1.139
start validation test
0.765979381443
1.0
0.765979381443
0.867483946293
0
validation finish
batch start
#iterations: 120
currently lose_sum: 369.8869735598564
time_elpased: 1.112
batch start
#iterations: 121
currently lose_sum: 370.8527777791023
time_elpased: 1.148
batch start
#iterations: 122
currently lose_sum: 370.0101224184036
time_elpased: 1.159
batch start
#iterations: 123
currently lose_sum: 367.72516292333603
time_elpased: 1.112
batch start
#iterations: 124
currently lose_sum: 371.2843749523163
time_elpased: 1.118
batch start
#iterations: 125
currently lose_sum: 369.5607987642288
time_elpased: 1.11
batch start
#iterations: 126
currently lose_sum: 369.4342178106308
time_elpased: 1.127
batch start
#iterations: 127
currently lose_sum: 369.49158948659897
time_elpased: 1.108
batch start
#iterations: 128
currently lose_sum: 370.23450523614883
time_elpased: 1.143
batch start
#iterations: 129
currently lose_sum: 371.59672498703003
time_elpased: 1.126
batch start
#iterations: 130
currently lose_sum: 368.9405327439308
time_elpased: 1.131
batch start
#iterations: 131
currently lose_sum: 370.001980304718
time_elpased: 1.12
batch start
#iterations: 132
currently lose_sum: 368.6008664369583
time_elpased: 1.113
batch start
#iterations: 133
currently lose_sum: 369.66463297605515
time_elpased: 1.159
batch start
#iterations: 134
currently lose_sum: 369.5124494433403
time_elpased: 1.127
batch start
#iterations: 135
currently lose_sum: 370.6231395602226
time_elpased: 1.121
batch start
#iterations: 136
currently lose_sum: 369.67030388116837
time_elpased: 1.153
batch start
#iterations: 137
currently lose_sum: 368.09693044424057
time_elpased: 1.109
batch start
#iterations: 138
currently lose_sum: 370.6368844509125
time_elpased: 1.158
batch start
#iterations: 139
currently lose_sum: 369.61215472221375
time_elpased: 1.128
start validation test
0.770927835052
1.0
0.770927835052
0.8706485039
0
validation finish
batch start
#iterations: 140
currently lose_sum: 367.5103496313095
time_elpased: 1.117
batch start
#iterations: 141
currently lose_sum: 369.2611346244812
time_elpased: 1.11
batch start
#iterations: 142
currently lose_sum: 369.7673892378807
time_elpased: 1.11
batch start
#iterations: 143
currently lose_sum: 369.2271765470505
time_elpased: 1.107
batch start
#iterations: 144
currently lose_sum: 368.4102655649185
time_elpased: 1.105
batch start
#iterations: 145
currently lose_sum: 371.20013254880905
time_elpased: 1.145
batch start
#iterations: 146
currently lose_sum: 369.56113636493683
time_elpased: 1.112
batch start
#iterations: 147
currently lose_sum: 369.94425016641617
time_elpased: 1.13
batch start
#iterations: 148
currently lose_sum: 369.8972697854042
time_elpased: 1.11
batch start
#iterations: 149
currently lose_sum: 368.84629786014557
time_elpased: 1.118
batch start
#iterations: 150
currently lose_sum: 367.2987265586853
time_elpased: 1.148
batch start
#iterations: 151
currently lose_sum: 368.48188585042953
time_elpased: 1.135
batch start
#iterations: 152
currently lose_sum: 368.4242449402809
time_elpased: 1.117
batch start
#iterations: 153
currently lose_sum: 369.692275762558
time_elpased: 1.142
batch start
#iterations: 154
currently lose_sum: 368.95532888174057
time_elpased: 1.116
batch start
#iterations: 155
currently lose_sum: 368.6175107359886
time_elpased: 1.114
batch start
#iterations: 156
currently lose_sum: 369.69731986522675
time_elpased: 1.139
batch start
#iterations: 157
currently lose_sum: 368.46141999959946
time_elpased: 1.109
batch start
#iterations: 158
currently lose_sum: 367.3291187286377
time_elpased: 1.113
batch start
#iterations: 159
currently lose_sum: 369.2722700238228
time_elpased: 1.115
start validation test
0.745463917526
1.0
0.745463917526
0.854172819089
0
validation finish
batch start
#iterations: 160
currently lose_sum: 369.2606775164604
time_elpased: 1.131
batch start
#iterations: 161
currently lose_sum: 368.5800889134407
time_elpased: 1.105
batch start
#iterations: 162
currently lose_sum: 368.77381271123886
time_elpased: 1.13
batch start
#iterations: 163
currently lose_sum: 368.60108083486557
time_elpased: 1.128
batch start
#iterations: 164
currently lose_sum: 368.867958009243
time_elpased: 1.109
batch start
#iterations: 165
currently lose_sum: 369.33712738752365
time_elpased: 1.139
batch start
#iterations: 166
currently lose_sum: 368.111025929451
time_elpased: 1.122
batch start
#iterations: 167
currently lose_sum: 366.8428013920784
time_elpased: 1.129
batch start
#iterations: 168
currently lose_sum: 367.47217959165573
time_elpased: 1.108
batch start
#iterations: 169
currently lose_sum: 369.70828622579575
time_elpased: 1.101
batch start
#iterations: 170
currently lose_sum: 366.8157287836075
time_elpased: 1.114
batch start
#iterations: 171
currently lose_sum: 365.9909538626671
time_elpased: 1.16
batch start
#iterations: 172
currently lose_sum: 368.61958879232407
time_elpased: 1.106
batch start
#iterations: 173
currently lose_sum: 367.87432676553726
time_elpased: 1.114
batch start
#iterations: 174
currently lose_sum: 367.95317631959915
time_elpased: 1.118
batch start
#iterations: 175
currently lose_sum: 368.16085290908813
time_elpased: 1.174
batch start
#iterations: 176
currently lose_sum: 367.7009871006012
time_elpased: 1.153
batch start
#iterations: 177
currently lose_sum: 367.92416989803314
time_elpased: 1.121
batch start
#iterations: 178
currently lose_sum: 367.7854658961296
time_elpased: 1.121
batch start
#iterations: 179
currently lose_sum: 368.38631957769394
time_elpased: 1.112
start validation test
0.747010309278
1.0
0.747010309278
0.855187064794
0
validation finish
batch start
#iterations: 180
currently lose_sum: 369.50725895166397
time_elpased: 1.119
batch start
#iterations: 181
currently lose_sum: 367.6755851507187
time_elpased: 1.118
batch start
#iterations: 182
currently lose_sum: 368.6974174976349
time_elpased: 1.153
batch start
#iterations: 183
currently lose_sum: 366.6493464112282
time_elpased: 1.1
batch start
#iterations: 184
currently lose_sum: 366.8339612483978
time_elpased: 1.112
batch start
#iterations: 185
currently lose_sum: 368.3721920847893
time_elpased: 1.109
batch start
#iterations: 186
currently lose_sum: 365.64996016025543
time_elpased: 1.126
batch start
#iterations: 187
currently lose_sum: 367.34496837854385
time_elpased: 1.109
batch start
#iterations: 188
currently lose_sum: 367.0762193799019
time_elpased: 1.173
batch start
#iterations: 189
currently lose_sum: 367.48032742738724
time_elpased: 1.112
batch start
#iterations: 190
currently lose_sum: 367.0052876472473
time_elpased: 1.117
batch start
#iterations: 191
currently lose_sum: 367.4672616124153
time_elpased: 1.13
batch start
#iterations: 192
currently lose_sum: 368.3992797732353
time_elpased: 1.123
batch start
#iterations: 193
currently lose_sum: 367.00112611055374
time_elpased: 1.097
batch start
#iterations: 194
currently lose_sum: 366.51562464237213
time_elpased: 1.108
batch start
#iterations: 195
currently lose_sum: 366.35677164793015
time_elpased: 1.168
batch start
#iterations: 196
currently lose_sum: 367.1796729564667
time_elpased: 1.115
batch start
#iterations: 197
currently lose_sum: 367.31708723306656
time_elpased: 1.13
batch start
#iterations: 198
currently lose_sum: 367.3596270084381
time_elpased: 1.132
batch start
#iterations: 199
currently lose_sum: 367.18067240715027
time_elpased: 1.11
start validation test
0.739690721649
1.0
0.739690721649
0.85037037037
0
validation finish
batch start
#iterations: 200
currently lose_sum: 367.2652226090431
time_elpased: 1.122
batch start
#iterations: 201
currently lose_sum: 366.76514506340027
time_elpased: 1.111
batch start
#iterations: 202
currently lose_sum: 366.56670463085175
time_elpased: 1.123
batch start
#iterations: 203
currently lose_sum: 367.7644748687744
time_elpased: 1.159
batch start
#iterations: 204
currently lose_sum: 365.9808104336262
time_elpased: 1.15
batch start
#iterations: 205
currently lose_sum: 367.2107881307602
time_elpased: 1.124
batch start
#iterations: 206
currently lose_sum: 367.1023246049881
time_elpased: 1.123
batch start
#iterations: 207
currently lose_sum: 367.23414248228073
time_elpased: 1.182
batch start
#iterations: 208
currently lose_sum: 366.8024955391884
time_elpased: 1.105
batch start
#iterations: 209
currently lose_sum: 367.3779396414757
time_elpased: 1.121
batch start
#iterations: 210
currently lose_sum: 366.77516853809357
time_elpased: 1.123
batch start
#iterations: 211
currently lose_sum: 365.60651910305023
time_elpased: 1.146
batch start
#iterations: 212
currently lose_sum: 366.45358419418335
time_elpased: 1.121
batch start
#iterations: 213
currently lose_sum: 365.46760737895966
time_elpased: 1.111
batch start
#iterations: 214
currently lose_sum: 366.4825991988182
time_elpased: 1.109
batch start
#iterations: 215
currently lose_sum: 366.7141823172569
time_elpased: 1.12
batch start
#iterations: 216
currently lose_sum: 366.33337450027466
time_elpased: 1.114
batch start
#iterations: 217
currently lose_sum: 366.1749009490013
time_elpased: 1.117
batch start
#iterations: 218
currently lose_sum: 366.6316664814949
time_elpased: 1.119
batch start
#iterations: 219
currently lose_sum: 367.00757497549057
time_elpased: 1.111
start validation test
0.769690721649
1.0
0.769690721649
0.869859023651
0
validation finish
batch start
#iterations: 220
currently lose_sum: 366.58124619722366
time_elpased: 1.119
batch start
#iterations: 221
currently lose_sum: 365.0411289334297
time_elpased: 1.114
batch start
#iterations: 222
currently lose_sum: 365.1276727318764
time_elpased: 1.133
batch start
#iterations: 223
currently lose_sum: 367.03507947921753
time_elpased: 1.116
batch start
#iterations: 224
currently lose_sum: 367.42536532878876
time_elpased: 1.149
batch start
#iterations: 225
currently lose_sum: 366.04721093177795
time_elpased: 1.115
batch start
#iterations: 226
currently lose_sum: 368.20617508888245
time_elpased: 1.106
batch start
#iterations: 227
currently lose_sum: 367.0480669736862
time_elpased: 1.119
batch start
#iterations: 228
currently lose_sum: 366.1019455194473
time_elpased: 1.116
batch start
#iterations: 229
currently lose_sum: 366.04368829727173
time_elpased: 1.151
batch start
#iterations: 230
currently lose_sum: 365.2576177120209
time_elpased: 1.11
batch start
#iterations: 231
currently lose_sum: 366.29260551929474
time_elpased: 1.175
batch start
#iterations: 232
currently lose_sum: 365.8822206258774
time_elpased: 1.114
batch start
#iterations: 233
currently lose_sum: 366.98034250736237
time_elpased: 1.145
batch start
#iterations: 234
currently lose_sum: 366.3804677724838
time_elpased: 1.133
batch start
#iterations: 235
currently lose_sum: 367.26864182949066
time_elpased: 1.114
batch start
#iterations: 236
currently lose_sum: 365.54090720415115
time_elpased: 1.168
batch start
#iterations: 237
currently lose_sum: 366.8356403708458
time_elpased: 1.154
batch start
#iterations: 238
currently lose_sum: 365.98780876398087
time_elpased: 1.122
batch start
#iterations: 239
currently lose_sum: 366.541723549366
time_elpased: 1.108
start validation test
0.729896907216
1.0
0.729896907216
0.843861740167
0
validation finish
batch start
#iterations: 240
currently lose_sum: 365.18602418899536
time_elpased: 1.16
batch start
#iterations: 241
currently lose_sum: 365.922382414341
time_elpased: 1.112
batch start
#iterations: 242
currently lose_sum: 366.25679111480713
time_elpased: 1.121
batch start
#iterations: 243
currently lose_sum: 365.9915906190872
time_elpased: 1.102
batch start
#iterations: 244
currently lose_sum: 365.79592645168304
time_elpased: 1.111
batch start
#iterations: 245
currently lose_sum: 365.7075071334839
time_elpased: 1.136
batch start
#iterations: 246
currently lose_sum: 364.89647084474564
time_elpased: 1.112
batch start
#iterations: 247
currently lose_sum: 365.9432485103607
time_elpased: 1.134
batch start
#iterations: 248
currently lose_sum: 367.00863713026047
time_elpased: 1.152
batch start
#iterations: 249
currently lose_sum: 365.9048937559128
time_elpased: 1.119
batch start
#iterations: 250
currently lose_sum: 365.74365413188934
time_elpased: 1.117
batch start
#iterations: 251
currently lose_sum: 366.0002529025078
time_elpased: 1.123
batch start
#iterations: 252
currently lose_sum: 365.45273238420486
time_elpased: 1.112
batch start
#iterations: 253
currently lose_sum: 365.32413053512573
time_elpased: 1.171
batch start
#iterations: 254
currently lose_sum: 365.4299423098564
time_elpased: 1.134
batch start
#iterations: 255
currently lose_sum: 364.55813014507294
time_elpased: 1.178
batch start
#iterations: 256
currently lose_sum: 365.7258965373039
time_elpased: 1.181
batch start
#iterations: 257
currently lose_sum: 364.9868798851967
time_elpased: 1.129
batch start
#iterations: 258
currently lose_sum: 364.3192221522331
time_elpased: 1.136
batch start
#iterations: 259
currently lose_sum: 364.3182784318924
time_elpased: 1.118
start validation test
0.763608247423
1.0
0.763608247423
0.865961302391
0
validation finish
batch start
#iterations: 260
currently lose_sum: 364.37296402454376
time_elpased: 1.112
batch start
#iterations: 261
currently lose_sum: 365.27152675390244
time_elpased: 1.115
batch start
#iterations: 262
currently lose_sum: 366.1003427505493
time_elpased: 1.128
batch start
#iterations: 263
currently lose_sum: 365.05697560310364
time_elpased: 1.117
batch start
#iterations: 264
currently lose_sum: 364.8190405666828
time_elpased: 1.15
batch start
#iterations: 265
currently lose_sum: 365.9366772174835
time_elpased: 1.128
batch start
#iterations: 266
currently lose_sum: 365.3381807208061
time_elpased: 1.109
batch start
#iterations: 267
currently lose_sum: 363.9563183784485
time_elpased: 1.109
batch start
#iterations: 268
currently lose_sum: 364.7591987848282
time_elpased: 1.126
batch start
#iterations: 269
currently lose_sum: 365.1195673942566
time_elpased: 1.174
batch start
#iterations: 270
currently lose_sum: 365.1817510128021
time_elpased: 1.119
batch start
#iterations: 271
currently lose_sum: 364.90144860744476
time_elpased: 1.109
batch start
#iterations: 272
currently lose_sum: 364.6224977374077
time_elpased: 1.11
batch start
#iterations: 273
currently lose_sum: 364.4125044941902
time_elpased: 1.121
batch start
#iterations: 274
currently lose_sum: 364.0182313323021
time_elpased: 1.117
batch start
#iterations: 275
currently lose_sum: 364.2034539580345
time_elpased: 1.115
batch start
#iterations: 276
currently lose_sum: 364.3989513516426
time_elpased: 1.147
batch start
#iterations: 277
currently lose_sum: 364.8291948437691
time_elpased: 1.141
batch start
#iterations: 278
currently lose_sum: 362.23679208755493
time_elpased: 1.107
batch start
#iterations: 279
currently lose_sum: 364.5258192420006
time_elpased: 1.132
start validation test
0.766804123711
1.0
0.766804123711
0.868012603571
0
validation finish
batch start
#iterations: 280
currently lose_sum: 364.4312572479248
time_elpased: 1.11
batch start
#iterations: 281
currently lose_sum: 364.65356624126434
time_elpased: 1.104
batch start
#iterations: 282
currently lose_sum: 364.42858988046646
time_elpased: 1.11
batch start
#iterations: 283
currently lose_sum: 366.0685310959816
time_elpased: 1.161
batch start
#iterations: 284
currently lose_sum: 363.7534617781639
time_elpased: 1.113
batch start
#iterations: 285
currently lose_sum: 364.0900844037533
time_elpased: 1.109
batch start
#iterations: 286
currently lose_sum: 363.16930490732193
time_elpased: 1.122
batch start
#iterations: 287
currently lose_sum: 365.2075268626213
time_elpased: 1.117
batch start
#iterations: 288
currently lose_sum: 363.2994503378868
time_elpased: 1.133
batch start
#iterations: 289
currently lose_sum: 363.3630626797676
time_elpased: 1.121
batch start
#iterations: 290
currently lose_sum: 363.3158429861069
time_elpased: 1.174
batch start
#iterations: 291
currently lose_sum: 363.3292655944824
time_elpased: 1.111
batch start
#iterations: 292
currently lose_sum: 363.3921500444412
time_elpased: 1.129
batch start
#iterations: 293
currently lose_sum: 364.8312913775444
time_elpased: 1.168
batch start
#iterations: 294
currently lose_sum: 363.1400520205498
time_elpased: 1.115
batch start
#iterations: 295
currently lose_sum: 362.23516058921814
time_elpased: 1.164
batch start
#iterations: 296
currently lose_sum: 363.4919512271881
time_elpased: 1.117
batch start
#iterations: 297
currently lose_sum: 364.44675332307816
time_elpased: 1.173
batch start
#iterations: 298
currently lose_sum: 362.957852602005
time_elpased: 1.156
batch start
#iterations: 299
currently lose_sum: 364.5909871459007
time_elpased: 1.116
start validation test
0.755257731959
1.0
0.755257731959
0.860566192881
0
validation finish
batch start
#iterations: 300
currently lose_sum: 364.0392292737961
time_elpased: 1.128
batch start
#iterations: 301
currently lose_sum: 361.75582563877106
time_elpased: 1.129
batch start
#iterations: 302
currently lose_sum: 365.6479802131653
time_elpased: 1.119
batch start
#iterations: 303
currently lose_sum: 363.2268307209015
time_elpased: 1.104
batch start
#iterations: 304
currently lose_sum: 364.9285588860512
time_elpased: 1.116
batch start
#iterations: 305
currently lose_sum: 363.9102421402931
time_elpased: 1.113
batch start
#iterations: 306
currently lose_sum: 362.7603991627693
time_elpased: 1.214
batch start
#iterations: 307
currently lose_sum: 362.81539130210876
time_elpased: 1.119
batch start
#iterations: 308
currently lose_sum: 362.52095901966095
time_elpased: 1.118
batch start
#iterations: 309
currently lose_sum: 363.38365387916565
time_elpased: 1.117
batch start
#iterations: 310
currently lose_sum: 363.90326952934265
time_elpased: 1.109
batch start
#iterations: 311
currently lose_sum: 364.1803247332573
time_elpased: 1.11
batch start
#iterations: 312
currently lose_sum: 362.94221663475037
time_elpased: 1.112
batch start
#iterations: 313
currently lose_sum: 363.85811883211136
time_elpased: 1.118
batch start
#iterations: 314
currently lose_sum: 361.68362122774124
time_elpased: 1.114
batch start
#iterations: 315
currently lose_sum: 363.18034797906876
time_elpased: 1.151
batch start
#iterations: 316
currently lose_sum: 363.61678344011307
time_elpased: 1.114
batch start
#iterations: 317
currently lose_sum: 364.3250696659088
time_elpased: 1.106
batch start
#iterations: 318
currently lose_sum: 363.76869440078735
time_elpased: 1.122
batch start
#iterations: 319
currently lose_sum: 363.4040824174881
time_elpased: 1.109
start validation test
0.764639175258
1.0
0.764639175258
0.866623824268
0
validation finish
batch start
#iterations: 320
currently lose_sum: 364.8130010962486
time_elpased: 1.166
batch start
#iterations: 321
currently lose_sum: 366.3315158486366
time_elpased: 1.16
batch start
#iterations: 322
currently lose_sum: 362.52131259441376
time_elpased: 1.171
batch start
#iterations: 323
currently lose_sum: 362.50624853372574
time_elpased: 1.104
batch start
#iterations: 324
currently lose_sum: 362.9512996673584
time_elpased: 1.115
batch start
#iterations: 325
currently lose_sum: 360.89692318439484
time_elpased: 1.108
batch start
#iterations: 326
currently lose_sum: 363.52347707748413
time_elpased: 1.129
batch start
#iterations: 327
currently lose_sum: 363.5683333873749
time_elpased: 1.125
batch start
#iterations: 328
currently lose_sum: 362.6557694673538
time_elpased: 1.179
batch start
#iterations: 329
currently lose_sum: 362.29862397909164
time_elpased: 1.168
batch start
#iterations: 330
currently lose_sum: 363.9933639764786
time_elpased: 1.149
batch start
#iterations: 331
currently lose_sum: 363.74966192245483
time_elpased: 1.118
batch start
#iterations: 332
currently lose_sum: 364.1196228861809
time_elpased: 1.17
batch start
#iterations: 333
currently lose_sum: 361.1767795085907
time_elpased: 1.117
batch start
#iterations: 334
currently lose_sum: 363.31374901533127
time_elpased: 1.122
batch start
#iterations: 335
currently lose_sum: 362.82089191675186
time_elpased: 1.109
batch start
#iterations: 336
currently lose_sum: 361.38040375709534
time_elpased: 1.13
batch start
#iterations: 337
currently lose_sum: 361.90263575315475
time_elpased: 1.143
batch start
#iterations: 338
currently lose_sum: 361.886427462101
time_elpased: 1.127
batch start
#iterations: 339
currently lose_sum: 361.152508020401
time_elpased: 1.128
start validation test
0.771237113402
1.0
0.771237113402
0.870845701647
0
validation finish
batch start
#iterations: 340
currently lose_sum: 362.7704283595085
time_elpased: 1.162
batch start
#iterations: 341
currently lose_sum: 362.77478289604187
time_elpased: 1.115
batch start
#iterations: 342
currently lose_sum: 362.32712161540985
time_elpased: 1.166
batch start
#iterations: 343
currently lose_sum: 362.67478173971176
time_elpased: 1.127
batch start
#iterations: 344
currently lose_sum: 361.73069071769714
time_elpased: 1.117
batch start
#iterations: 345
currently lose_sum: 363.5348467230797
time_elpased: 1.144
batch start
#iterations: 346
currently lose_sum: 362.68679881095886
time_elpased: 1.126
batch start
#iterations: 347
currently lose_sum: 360.2810223698616
time_elpased: 1.111
batch start
#iterations: 348
currently lose_sum: 362.9712503552437
time_elpased: 1.117
batch start
#iterations: 349
currently lose_sum: 364.05050617456436
time_elpased: 1.114
batch start
#iterations: 350
currently lose_sum: 363.2153816819191
time_elpased: 1.125
batch start
#iterations: 351
currently lose_sum: 362.1359604001045
time_elpased: 1.145
batch start
#iterations: 352
currently lose_sum: 361.6543372273445
time_elpased: 1.123
batch start
#iterations: 353
currently lose_sum: 363.12920129299164
time_elpased: 1.144
batch start
#iterations: 354
currently lose_sum: 361.6231790781021
time_elpased: 1.119
batch start
#iterations: 355
currently lose_sum: 361.750196993351
time_elpased: 1.106
batch start
#iterations: 356
currently lose_sum: 362.7364161014557
time_elpased: 1.11
batch start
#iterations: 357
currently lose_sum: 363.4425641298294
time_elpased: 1.118
batch start
#iterations: 358
currently lose_sum: 363.6727309823036
time_elpased: 1.117
batch start
#iterations: 359
currently lose_sum: 362.8714492917061
time_elpased: 1.161
start validation test
0.763402061856
1.0
0.763402061856
0.865828705057
0
validation finish
batch start
#iterations: 360
currently lose_sum: 362.376059114933
time_elpased: 1.115
batch start
#iterations: 361
currently lose_sum: 363.3933602273464
time_elpased: 1.114
batch start
#iterations: 362
currently lose_sum: 364.0801029205322
time_elpased: 1.167
batch start
#iterations: 363
currently lose_sum: 362.4283979535103
time_elpased: 1.122
batch start
#iterations: 364
currently lose_sum: 363.4698566496372
time_elpased: 1.109
batch start
#iterations: 365
currently lose_sum: 364.3873586654663
time_elpased: 1.124
batch start
#iterations: 366
currently lose_sum: 363.32257866859436
time_elpased: 1.114
batch start
#iterations: 367
currently lose_sum: 363.58712565898895
time_elpased: 1.111
batch start
#iterations: 368
currently lose_sum: 362.6062297821045
time_elpased: 1.14
batch start
#iterations: 369
currently lose_sum: 361.70916801691055
time_elpased: 1.113
batch start
#iterations: 370
currently lose_sum: 362.2314819097519
time_elpased: 1.113
batch start
#iterations: 371
currently lose_sum: 361.387261480093
time_elpased: 1.123
batch start
#iterations: 372
currently lose_sum: 361.3539081811905
time_elpased: 1.122
batch start
#iterations: 373
currently lose_sum: 362.8810909986496
time_elpased: 1.107
batch start
#iterations: 374
currently lose_sum: 361.82441595196724
time_elpased: 1.126
batch start
#iterations: 375
currently lose_sum: 363.0209477543831
time_elpased: 1.115
batch start
#iterations: 376
currently lose_sum: 362.81196880340576
time_elpased: 1.114
batch start
#iterations: 377
currently lose_sum: 363.52841252088547
time_elpased: 1.139
batch start
#iterations: 378
currently lose_sum: 362.11921739578247
time_elpased: 1.133
batch start
#iterations: 379
currently lose_sum: 361.96578109264374
time_elpased: 1.166
start validation test
0.780927835052
1.0
0.780927835052
0.876989869754
0
validation finish
batch start
#iterations: 380
currently lose_sum: 361.33138823509216
time_elpased: 1.117
batch start
#iterations: 381
currently lose_sum: 361.39792144298553
time_elpased: 1.109
batch start
#iterations: 382
currently lose_sum: 362.059642970562
time_elpased: 1.112
batch start
#iterations: 383
currently lose_sum: 360.5704303383827
time_elpased: 1.13
batch start
#iterations: 384
currently lose_sum: 361.6811668872833
time_elpased: 1.114
batch start
#iterations: 385
currently lose_sum: 362.80079609155655
time_elpased: 1.161
batch start
#iterations: 386
currently lose_sum: 362.3137310743332
time_elpased: 1.109
batch start
#iterations: 387
currently lose_sum: 363.05628818273544
time_elpased: 1.11
batch start
#iterations: 388
currently lose_sum: 361.14014729857445
time_elpased: 1.141
batch start
#iterations: 389
currently lose_sum: 361.8527120947838
time_elpased: 1.117
batch start
#iterations: 390
currently lose_sum: 363.9388724565506
time_elpased: 1.125
batch start
#iterations: 391
currently lose_sum: 361.84095019102097
time_elpased: 1.125
batch start
#iterations: 392
currently lose_sum: 361.8844646513462
time_elpased: 1.173
batch start
#iterations: 393
currently lose_sum: 362.11012107133865
time_elpased: 1.134
batch start
#iterations: 394
currently lose_sum: 362.0460327267647
time_elpased: 1.122
batch start
#iterations: 395
currently lose_sum: 361.2112039923668
time_elpased: 1.107
batch start
#iterations: 396
currently lose_sum: 361.39406275749207
time_elpased: 1.117
batch start
#iterations: 397
currently lose_sum: 362.72754925489426
time_elpased: 1.135
batch start
#iterations: 398
currently lose_sum: 361.7967793941498
time_elpased: 1.139
batch start
#iterations: 399
currently lose_sum: 361.7075244784355
time_elpased: 1.166
start validation test
0.764432989691
1.0
0.764432989691
0.866491381829
0
validation finish
acc: 0.795
pre: 1.000
rec: 0.795
F1: 0.886
auc: 0.000
