start to construct graph
graph construct over
58302
epochs start
batch start
#iterations: 0
currently lose_sum: 405.5967361330986
time_elpased: 1.139
batch start
#iterations: 1
currently lose_sum: 403.0598632097244
time_elpased: 1.122
batch start
#iterations: 2
currently lose_sum: 402.630102455616
time_elpased: 1.103
batch start
#iterations: 3
currently lose_sum: 402.06303280591965
time_elpased: 1.112
batch start
#iterations: 4
currently lose_sum: 401.7630831003189
time_elpased: 1.101
batch start
#iterations: 5
currently lose_sum: 401.4924963712692
time_elpased: 1.116
batch start
#iterations: 6
currently lose_sum: 400.94966316223145
time_elpased: 1.11
batch start
#iterations: 7
currently lose_sum: 399.8659560084343
time_elpased: 1.118
batch start
#iterations: 8
currently lose_sum: 399.65432596206665
time_elpased: 1.111
batch start
#iterations: 9
currently lose_sum: 398.5710534453392
time_elpased: 1.115
batch start
#iterations: 10
currently lose_sum: 396.38647705316544
time_elpased: 1.112
batch start
#iterations: 11
currently lose_sum: 395.84597969055176
time_elpased: 1.108
batch start
#iterations: 12
currently lose_sum: 394.25290882587433
time_elpased: 1.107
batch start
#iterations: 13
currently lose_sum: 393.4939397573471
time_elpased: 1.106
batch start
#iterations: 14
currently lose_sum: 392.76850908994675
time_elpased: 1.103
batch start
#iterations: 15
currently lose_sum: 391.5880675315857
time_elpased: 1.108
batch start
#iterations: 16
currently lose_sum: 391.33245503902435
time_elpased: 1.107
batch start
#iterations: 17
currently lose_sum: 391.9007830619812
time_elpased: 1.092
batch start
#iterations: 18
currently lose_sum: 389.8243211507797
time_elpased: 1.109
batch start
#iterations: 19
currently lose_sum: 388.6463294029236
time_elpased: 1.104
start validation test
0.719587628866
1.0
0.719587628866
0.836930455635
0
validation finish
batch start
#iterations: 20
currently lose_sum: 387.4512757062912
time_elpased: 1.114
batch start
#iterations: 21
currently lose_sum: 387.6025792360306
time_elpased: 1.115
batch start
#iterations: 22
currently lose_sum: 386.70055997371674
time_elpased: 1.118
batch start
#iterations: 23
currently lose_sum: 385.67009568214417
time_elpased: 1.108
batch start
#iterations: 24
currently lose_sum: 386.0832334756851
time_elpased: 1.109
batch start
#iterations: 25
currently lose_sum: 386.83395355939865
time_elpased: 1.099
batch start
#iterations: 26
currently lose_sum: 384.28146636486053
time_elpased: 1.105
batch start
#iterations: 27
currently lose_sum: 385.3976371884346
time_elpased: 1.109
batch start
#iterations: 28
currently lose_sum: 384.69408321380615
time_elpased: 1.121
batch start
#iterations: 29
currently lose_sum: 385.25180846452713
time_elpased: 1.169
batch start
#iterations: 30
currently lose_sum: 383.7365794181824
time_elpased: 1.109
batch start
#iterations: 31
currently lose_sum: 383.93397879600525
time_elpased: 1.126
batch start
#iterations: 32
currently lose_sum: 384.10405147075653
time_elpased: 1.121
batch start
#iterations: 33
currently lose_sum: 384.0344249010086
time_elpased: 1.102
batch start
#iterations: 34
currently lose_sum: 382.81180042028427
time_elpased: 1.097
batch start
#iterations: 35
currently lose_sum: 383.77129703760147
time_elpased: 1.103
batch start
#iterations: 36
currently lose_sum: 382.3092083930969
time_elpased: 1.146
batch start
#iterations: 37
currently lose_sum: 381.58967262506485
time_elpased: 1.106
batch start
#iterations: 38
currently lose_sum: 381.90290600061417
time_elpased: 1.111
batch start
#iterations: 39
currently lose_sum: 381.81477469205856
time_elpased: 1.14
start validation test
0.717628865979
1.0
0.717628865979
0.835604105396
0
validation finish
batch start
#iterations: 40
currently lose_sum: 380.84594064950943
time_elpased: 1.12
batch start
#iterations: 41
currently lose_sum: 380.2174394726753
time_elpased: 1.129
batch start
#iterations: 42
currently lose_sum: 380.5152221918106
time_elpased: 1.106
batch start
#iterations: 43
currently lose_sum: 379.29342806339264
time_elpased: 1.122
batch start
#iterations: 44
currently lose_sum: 379.9519157409668
time_elpased: 1.112
batch start
#iterations: 45
currently lose_sum: 378.7388000488281
time_elpased: 1.12
batch start
#iterations: 46
currently lose_sum: 380.8326793909073
time_elpased: 1.117
batch start
#iterations: 47
currently lose_sum: 378.92251229286194
time_elpased: 1.101
batch start
#iterations: 48
currently lose_sum: 379.16634368896484
time_elpased: 1.118
batch start
#iterations: 49
currently lose_sum: 378.45270383358
time_elpased: 1.109
batch start
#iterations: 50
currently lose_sum: 380.7949165701866
time_elpased: 1.108
batch start
#iterations: 51
currently lose_sum: 379.8846898674965
time_elpased: 1.105
batch start
#iterations: 52
currently lose_sum: 378.9797191619873
time_elpased: 1.124
batch start
#iterations: 53
currently lose_sum: 378.69390708208084
time_elpased: 1.105
batch start
#iterations: 54
currently lose_sum: 378.5110499858856
time_elpased: 1.154
batch start
#iterations: 55
currently lose_sum: 377.67819732427597
time_elpased: 1.107
batch start
#iterations: 56
currently lose_sum: 377.79388546943665
time_elpased: 1.111
batch start
#iterations: 57
currently lose_sum: 378.09622567892075
time_elpased: 1.116
batch start
#iterations: 58
currently lose_sum: 377.05907076597214
time_elpased: 1.106
batch start
#iterations: 59
currently lose_sum: 378.63796389102936
time_elpased: 1.142
start validation test
0.726597938144
1.0
0.726597938144
0.841652734655
0
validation finish
batch start
#iterations: 60
currently lose_sum: 376.9964327812195
time_elpased: 1.107
batch start
#iterations: 61
currently lose_sum: 377.0904550552368
time_elpased: 1.107
batch start
#iterations: 62
currently lose_sum: 377.03049778938293
time_elpased: 1.12
batch start
#iterations: 63
currently lose_sum: 377.0329440832138
time_elpased: 1.119
batch start
#iterations: 64
currently lose_sum: 377.0080971121788
time_elpased: 1.102
batch start
#iterations: 65
currently lose_sum: 376.5149302482605
time_elpased: 1.112
batch start
#iterations: 66
currently lose_sum: 377.5013128519058
time_elpased: 1.167
batch start
#iterations: 67
currently lose_sum: 376.63479566574097
time_elpased: 1.108
batch start
#iterations: 68
currently lose_sum: 375.8356977701187
time_elpased: 1.125
batch start
#iterations: 69
currently lose_sum: 375.57056188583374
time_elpased: 1.114
batch start
#iterations: 70
currently lose_sum: 375.7794165611267
time_elpased: 1.156
batch start
#iterations: 71
currently lose_sum: 375.6893302798271
time_elpased: 1.135
batch start
#iterations: 72
currently lose_sum: 373.91028249263763
time_elpased: 1.108
batch start
#iterations: 73
currently lose_sum: 375.9855716228485
time_elpased: 1.115
batch start
#iterations: 74
currently lose_sum: 376.03547590970993
time_elpased: 1.116
batch start
#iterations: 75
currently lose_sum: 375.1679736971855
time_elpased: 1.159
batch start
#iterations: 76
currently lose_sum: 375.915231525898
time_elpased: 1.138
batch start
#iterations: 77
currently lose_sum: 374.5268650650978
time_elpased: 1.117
batch start
#iterations: 78
currently lose_sum: 373.65624701976776
time_elpased: 1.112
batch start
#iterations: 79
currently lose_sum: 373.2516257762909
time_elpased: 1.113
start validation test
0.756597938144
1.0
0.756597938144
0.861435530254
0
validation finish
batch start
#iterations: 80
currently lose_sum: 374.67531341314316
time_elpased: 1.114
batch start
#iterations: 81
currently lose_sum: 375.7593507170677
time_elpased: 1.112
batch start
#iterations: 82
currently lose_sum: 375.9656096100807
time_elpased: 1.122
batch start
#iterations: 83
currently lose_sum: 374.44142985343933
time_elpased: 1.108
batch start
#iterations: 84
currently lose_sum: 375.0477139353752
time_elpased: 1.164
batch start
#iterations: 85
currently lose_sum: 374.56742918491364
time_elpased: 1.118
batch start
#iterations: 86
currently lose_sum: 375.00522112846375
time_elpased: 1.134
batch start
#iterations: 87
currently lose_sum: 375.33512330055237
time_elpased: 1.12
batch start
#iterations: 88
currently lose_sum: 372.6412115097046
time_elpased: 1.162
batch start
#iterations: 89
currently lose_sum: 374.42630076408386
time_elpased: 1.122
batch start
#iterations: 90
currently lose_sum: 373.90666592121124
time_elpased: 1.159
batch start
#iterations: 91
currently lose_sum: 374.08282059431076
time_elpased: 1.144
batch start
#iterations: 92
currently lose_sum: 373.8977441191673
time_elpased: 1.173
batch start
#iterations: 93
currently lose_sum: 375.7476413846016
time_elpased: 1.12
batch start
#iterations: 94
currently lose_sum: 374.263238966465
time_elpased: 1.105
batch start
#iterations: 95
currently lose_sum: 371.726598739624
time_elpased: 1.121
batch start
#iterations: 96
currently lose_sum: 373.56332463026047
time_elpased: 1.117
batch start
#iterations: 97
currently lose_sum: 373.29935401678085
time_elpased: 1.137
batch start
#iterations: 98
currently lose_sum: 372.48929888010025
time_elpased: 1.13
batch start
#iterations: 99
currently lose_sum: 373.76708751916885
time_elpased: 1.122
start validation test
0.76793814433
1.0
0.76793814433
0.868738701965
0
validation finish
batch start
#iterations: 100
currently lose_sum: 373.9740888476372
time_elpased: 1.141
batch start
#iterations: 101
currently lose_sum: 375.2014072537422
time_elpased: 1.16
batch start
#iterations: 102
currently lose_sum: 371.3797960281372
time_elpased: 1.143
batch start
#iterations: 103
currently lose_sum: 372.48799031972885
time_elpased: 1.15
batch start
#iterations: 104
currently lose_sum: 374.0461935997009
time_elpased: 1.114
batch start
#iterations: 105
currently lose_sum: 371.3355835676193
time_elpased: 1.153
batch start
#iterations: 106
currently lose_sum: 373.54035156965256
time_elpased: 1.147
batch start
#iterations: 107
currently lose_sum: 372.2245218157768
time_elpased: 1.112
batch start
#iterations: 108
currently lose_sum: 372.22266215085983
time_elpased: 1.113
batch start
#iterations: 109
currently lose_sum: 373.3545590043068
time_elpased: 1.106
batch start
#iterations: 110
currently lose_sum: 373.07310765981674
time_elpased: 1.157
batch start
#iterations: 111
currently lose_sum: 372.30053436756134
time_elpased: 1.109
batch start
#iterations: 112
currently lose_sum: 371.42128241062164
time_elpased: 1.11
batch start
#iterations: 113
currently lose_sum: 371.9970861673355
time_elpased: 1.171
batch start
#iterations: 114
currently lose_sum: 373.85844641923904
time_elpased: 1.148
batch start
#iterations: 115
currently lose_sum: 371.3811274766922
time_elpased: 1.118
batch start
#iterations: 116
currently lose_sum: 371.96667915582657
time_elpased: 1.152
batch start
#iterations: 117
currently lose_sum: 371.76965552568436
time_elpased: 1.185
batch start
#iterations: 118
currently lose_sum: 371.27516371011734
time_elpased: 1.112
batch start
#iterations: 119
currently lose_sum: 371.8249832391739
time_elpased: 1.122
start validation test
0.696907216495
1.0
0.696907216495
0.821385176185
0
validation finish
batch start
#iterations: 120
currently lose_sum: 371.9451770186424
time_elpased: 1.125
batch start
#iterations: 121
currently lose_sum: 372.12484526634216
time_elpased: 1.174
batch start
#iterations: 122
currently lose_sum: 372.1120976805687
time_elpased: 1.128
batch start
#iterations: 123
currently lose_sum: 370.21816116571426
time_elpased: 1.123
batch start
#iterations: 124
currently lose_sum: 370.30937045812607
time_elpased: 1.119
batch start
#iterations: 125
currently lose_sum: 371.49414640665054
time_elpased: 1.116
batch start
#iterations: 126
currently lose_sum: 371.57867258787155
time_elpased: 1.13
batch start
#iterations: 127
currently lose_sum: 372.5668630003929
time_elpased: 1.172
batch start
#iterations: 128
currently lose_sum: 372.70402032136917
time_elpased: 1.118
batch start
#iterations: 129
currently lose_sum: 370.3280431032181
time_elpased: 1.119
batch start
#iterations: 130
currently lose_sum: 369.8431401848793
time_elpased: 1.149
batch start
#iterations: 131
currently lose_sum: 372.2631859779358
time_elpased: 1.108
batch start
#iterations: 132
currently lose_sum: 372.0121334195137
time_elpased: 1.147
batch start
#iterations: 133
currently lose_sum: 372.23173117637634
time_elpased: 1.117
batch start
#iterations: 134
currently lose_sum: 370.08074605464935
time_elpased: 1.13
batch start
#iterations: 135
currently lose_sum: 370.82679957151413
time_elpased: 1.115
batch start
#iterations: 136
currently lose_sum: 369.37177473306656
time_elpased: 1.172
batch start
#iterations: 137
currently lose_sum: 370.16643887758255
time_elpased: 1.118
batch start
#iterations: 138
currently lose_sum: 371.1102574467659
time_elpased: 1.111
batch start
#iterations: 139
currently lose_sum: 369.7936782836914
time_elpased: 1.114
start validation test
0.690206185567
1.0
0.690206185567
0.816712412321
0
validation finish
batch start
#iterations: 140
currently lose_sum: 369.454790353775
time_elpased: 1.142
batch start
#iterations: 141
currently lose_sum: 369.6894219517708
time_elpased: 1.155
batch start
#iterations: 142
currently lose_sum: 368.2657116651535
time_elpased: 1.162
batch start
#iterations: 143
currently lose_sum: 368.3504465818405
time_elpased: 1.186
batch start
#iterations: 144
currently lose_sum: 368.9878041148186
time_elpased: 1.124
batch start
#iterations: 145
currently lose_sum: 370.49506640434265
time_elpased: 1.119
batch start
#iterations: 146
currently lose_sum: 367.07772493362427
time_elpased: 1.177
batch start
#iterations: 147
currently lose_sum: 371.568024456501
time_elpased: 1.155
batch start
#iterations: 148
currently lose_sum: 369.52323693037033
time_elpased: 1.154
batch start
#iterations: 149
currently lose_sum: 369.07520294189453
time_elpased: 1.123
batch start
#iterations: 150
currently lose_sum: 369.40555077791214
time_elpased: 1.179
batch start
#iterations: 151
currently lose_sum: 369.341053545475
time_elpased: 1.135
batch start
#iterations: 152
currently lose_sum: 368.0808072090149
time_elpased: 1.167
batch start
#iterations: 153
currently lose_sum: 369.6251433491707
time_elpased: 1.118
batch start
#iterations: 154
currently lose_sum: 370.3445534110069
time_elpased: 1.134
batch start
#iterations: 155
currently lose_sum: 369.5853588581085
time_elpased: 1.133
batch start
#iterations: 156
currently lose_sum: 370.70528215169907
time_elpased: 1.123
batch start
#iterations: 157
currently lose_sum: 370.2392448782921
time_elpased: 1.126
batch start
#iterations: 158
currently lose_sum: 368.76516073942184
time_elpased: 1.179
batch start
#iterations: 159
currently lose_sum: 368.6398676633835
time_elpased: 1.133
start validation test
0.757216494845
1.0
0.757216494845
0.861836315635
0
validation finish
batch start
#iterations: 160
currently lose_sum: 369.48569560050964
time_elpased: 1.118
batch start
#iterations: 161
currently lose_sum: 369.94358146190643
time_elpased: 1.108
batch start
#iterations: 162
currently lose_sum: 368.8843709230423
time_elpased: 1.13
batch start
#iterations: 163
currently lose_sum: 369.47545951604843
time_elpased: 1.143
batch start
#iterations: 164
currently lose_sum: 368.94872093200684
time_elpased: 1.124
batch start
#iterations: 165
currently lose_sum: 368.4167197942734
time_elpased: 1.14
batch start
#iterations: 166
currently lose_sum: 369.36618369817734
time_elpased: 1.162
batch start
#iterations: 167
currently lose_sum: 369.6739557981491
time_elpased: 1.153
batch start
#iterations: 168
currently lose_sum: 369.60419726371765
time_elpased: 1.117
batch start
#iterations: 169
currently lose_sum: 368.8607768416405
time_elpased: 1.113
batch start
#iterations: 170
currently lose_sum: 368.29638689756393
time_elpased: 1.176
batch start
#iterations: 171
currently lose_sum: 367.55705136060715
time_elpased: 1.144
batch start
#iterations: 172
currently lose_sum: 368.84684920310974
time_elpased: 1.161
batch start
#iterations: 173
currently lose_sum: 369.1334481239319
time_elpased: 1.115
batch start
#iterations: 174
currently lose_sum: 368.4726308584213
time_elpased: 1.115
batch start
#iterations: 175
currently lose_sum: 368.2426755428314
time_elpased: 1.112
batch start
#iterations: 176
currently lose_sum: 368.6080045104027
time_elpased: 1.122
batch start
#iterations: 177
currently lose_sum: 368.4670721292496
time_elpased: 1.116
batch start
#iterations: 178
currently lose_sum: 368.4003264307976
time_elpased: 1.12
batch start
#iterations: 179
currently lose_sum: 367.1198207139969
time_elpased: 1.118
start validation test
0.758144329897
1.0
0.758144329897
0.862436964935
0
validation finish
batch start
#iterations: 180
currently lose_sum: 367.74683755636215
time_elpased: 1.17
batch start
#iterations: 181
currently lose_sum: 368.58569318056107
time_elpased: 1.112
batch start
#iterations: 182
currently lose_sum: 370.1080757379532
time_elpased: 1.173
batch start
#iterations: 183
currently lose_sum: 368.44498336315155
time_elpased: 1.117
batch start
#iterations: 184
currently lose_sum: 368.250581741333
time_elpased: 1.12
batch start
#iterations: 185
currently lose_sum: 367.90913635492325
time_elpased: 1.12
batch start
#iterations: 186
currently lose_sum: 369.2336654663086
time_elpased: 1.112
batch start
#iterations: 187
currently lose_sum: 368.50092685222626
time_elpased: 1.124
batch start
#iterations: 188
currently lose_sum: 367.4451901912689
time_elpased: 1.116
batch start
#iterations: 189
currently lose_sum: 368.7244388461113
time_elpased: 1.16
batch start
#iterations: 190
currently lose_sum: 367.9664594531059
time_elpased: 1.159
batch start
#iterations: 191
currently lose_sum: 369.3295624256134
time_elpased: 1.116
batch start
#iterations: 192
currently lose_sum: 367.6009488105774
time_elpased: 1.129
batch start
#iterations: 193
currently lose_sum: 367.62377309799194
time_elpased: 1.121
batch start
#iterations: 194
currently lose_sum: 367.68492889404297
time_elpased: 1.164
batch start
#iterations: 195
currently lose_sum: 367.10769909620285
time_elpased: 1.123
batch start
#iterations: 196
currently lose_sum: 368.470676779747
time_elpased: 1.173
batch start
#iterations: 197
currently lose_sum: 369.28295969963074
time_elpased: 1.19
batch start
#iterations: 198
currently lose_sum: 368.3116520047188
time_elpased: 1.134
batch start
#iterations: 199
currently lose_sum: 368.20141011476517
time_elpased: 1.125
start validation test
0.753195876289
1.0
0.753195876289
0.859226155475
0
validation finish
batch start
#iterations: 200
currently lose_sum: 365.83418357372284
time_elpased: 1.123
batch start
#iterations: 201
currently lose_sum: 368.5047961473465
time_elpased: 1.146
batch start
#iterations: 202
currently lose_sum: 367.93963265419006
time_elpased: 1.136
batch start
#iterations: 203
currently lose_sum: 367.0551643371582
time_elpased: 1.148
batch start
#iterations: 204
currently lose_sum: 366.92317366600037
time_elpased: 1.121
batch start
#iterations: 205
currently lose_sum: 367.0514813065529
time_elpased: 1.125
batch start
#iterations: 206
currently lose_sum: 367.26133531332016
time_elpased: 1.112
batch start
#iterations: 207
currently lose_sum: 365.99904680252075
time_elpased: 1.18
batch start
#iterations: 208
currently lose_sum: 367.9641913175583
time_elpased: 1.124
batch start
#iterations: 209
currently lose_sum: 364.5874517261982
time_elpased: 1.114
batch start
#iterations: 210
currently lose_sum: 367.2564272284508
time_elpased: 1.123
batch start
#iterations: 211
currently lose_sum: 365.40766084194183
time_elpased: 1.114
batch start
#iterations: 212
currently lose_sum: 366.2438198328018
time_elpased: 1.138
batch start
#iterations: 213
currently lose_sum: 367.3682312965393
time_elpased: 1.172
batch start
#iterations: 214
currently lose_sum: 367.274227976799
time_elpased: 1.169
batch start
#iterations: 215
currently lose_sum: 365.5238299369812
time_elpased: 1.124
batch start
#iterations: 216
currently lose_sum: 366.8451820909977
time_elpased: 1.119
batch start
#iterations: 217
currently lose_sum: 367.82599353790283
time_elpased: 1.133
batch start
#iterations: 218
currently lose_sum: 365.8641858100891
time_elpased: 1.126
batch start
#iterations: 219
currently lose_sum: 367.8081371188164
time_elpased: 1.162
start validation test
0.741546391753
1.0
0.741546391753
0.851595335346
0
validation finish
batch start
#iterations: 220
currently lose_sum: 366.1013944745064
time_elpased: 1.12
batch start
#iterations: 221
currently lose_sum: 366.60580414533615
time_elpased: 1.138
batch start
#iterations: 222
currently lose_sum: 366.5328520536423
time_elpased: 1.11
batch start
#iterations: 223
currently lose_sum: 366.4900596141815
time_elpased: 1.118
batch start
#iterations: 224
currently lose_sum: 365.12842017412186
time_elpased: 1.165
batch start
#iterations: 225
currently lose_sum: 366.1809830069542
time_elpased: 1.118
batch start
#iterations: 226
currently lose_sum: 365.9916875362396
time_elpased: 1.125
batch start
#iterations: 227
currently lose_sum: 366.62146854400635
time_elpased: 1.122
batch start
#iterations: 228
currently lose_sum: 365.2429573535919
time_elpased: 1.116
batch start
#iterations: 229
currently lose_sum: 365.15214616060257
time_elpased: 1.138
batch start
#iterations: 230
currently lose_sum: 366.3579238653183
time_elpased: 1.146
batch start
#iterations: 231
currently lose_sum: 367.15087151527405
time_elpased: 1.116
batch start
#iterations: 232
currently lose_sum: 366.4724872112274
time_elpased: 1.138
batch start
#iterations: 233
currently lose_sum: 364.390616774559
time_elpased: 1.159
batch start
#iterations: 234
currently lose_sum: 364.2608714103699
time_elpased: 1.113
batch start
#iterations: 235
currently lose_sum: 365.87364798784256
time_elpased: 1.145
batch start
#iterations: 236
currently lose_sum: 366.26799243688583
time_elpased: 1.123
batch start
#iterations: 237
currently lose_sum: 367.4899967312813
time_elpased: 1.118
batch start
#iterations: 238
currently lose_sum: 367.00465965270996
time_elpased: 1.117
batch start
#iterations: 239
currently lose_sum: 366.3652659058571
time_elpased: 1.121
start validation test
0.718144329897
1.0
0.718144329897
0.835953438138
0
validation finish
batch start
#iterations: 240
currently lose_sum: 365.75722002983093
time_elpased: 1.109
batch start
#iterations: 241
currently lose_sum: 365.46773594617844
time_elpased: 1.13
batch start
#iterations: 242
currently lose_sum: 364.18119472265244
time_elpased: 1.161
batch start
#iterations: 243
currently lose_sum: 368.44349682331085
time_elpased: 1.141
batch start
#iterations: 244
currently lose_sum: 366.8168770670891
time_elpased: 1.136
batch start
#iterations: 245
currently lose_sum: 366.43206840753555
time_elpased: 1.135
batch start
#iterations: 246
currently lose_sum: 365.3209794163704
time_elpased: 1.118
batch start
#iterations: 247
currently lose_sum: 366.1165705919266
time_elpased: 1.161
batch start
#iterations: 248
currently lose_sum: 366.41337925195694
time_elpased: 1.136
batch start
#iterations: 249
currently lose_sum: 366.6239421367645
time_elpased: 1.115
batch start
#iterations: 250
currently lose_sum: 365.4631715416908
time_elpased: 1.119
batch start
#iterations: 251
currently lose_sum: 366.15917348861694
time_elpased: 1.122
batch start
#iterations: 252
currently lose_sum: 364.8947196006775
time_elpased: 1.166
batch start
#iterations: 253
currently lose_sum: 365.12046974897385
time_elpased: 1.124
batch start
#iterations: 254
currently lose_sum: 365.08839923143387
time_elpased: 1.147
batch start
#iterations: 255
currently lose_sum: 366.06851518154144
time_elpased: 1.127
batch start
#iterations: 256
currently lose_sum: 366.0964586138725
time_elpased: 1.117
batch start
#iterations: 257
currently lose_sum: 365.0621117949486
time_elpased: 1.109
batch start
#iterations: 258
currently lose_sum: 366.06418001651764
time_elpased: 1.156
batch start
#iterations: 259
currently lose_sum: 367.14441281557083
time_elpased: 1.114
start validation test
0.735051546392
1.0
0.735051546392
0.847296494355
0
validation finish
batch start
#iterations: 260
currently lose_sum: 365.24788188934326
time_elpased: 1.144
batch start
#iterations: 261
currently lose_sum: 366.1786647439003
time_elpased: 1.116
batch start
#iterations: 262
currently lose_sum: 365.85621374845505
time_elpased: 1.117
batch start
#iterations: 263
currently lose_sum: 365.1379192173481
time_elpased: 1.186
batch start
#iterations: 264
currently lose_sum: 364.37453919649124
time_elpased: 1.15
batch start
#iterations: 265
currently lose_sum: 366.1508713364601
time_elpased: 1.11
batch start
#iterations: 266
currently lose_sum: 365.21259450912476
time_elpased: 1.114
batch start
#iterations: 267
currently lose_sum: 364.0684821009636
time_elpased: 1.127
batch start
#iterations: 268
currently lose_sum: 364.2952170968056
time_elpased: 1.117
batch start
#iterations: 269
currently lose_sum: 365.75078374147415
time_elpased: 1.115
batch start
#iterations: 270
currently lose_sum: 363.70139026641846
time_elpased: 1.17
batch start
#iterations: 271
currently lose_sum: 366.05535167455673
time_elpased: 1.135
batch start
#iterations: 272
currently lose_sum: 364.3356286883354
time_elpased: 1.132
batch start
#iterations: 273
currently lose_sum: 364.32799285650253
time_elpased: 1.141
batch start
#iterations: 274
currently lose_sum: 365.0192356109619
time_elpased: 1.117
batch start
#iterations: 275
currently lose_sum: 365.9754646420479
time_elpased: 1.124
batch start
#iterations: 276
currently lose_sum: 364.1743765473366
time_elpased: 1.122
batch start
#iterations: 277
currently lose_sum: 365.6107197999954
time_elpased: 1.113
batch start
#iterations: 278
currently lose_sum: 365.2353567481041
time_elpased: 1.113
batch start
#iterations: 279
currently lose_sum: 364.75339794158936
time_elpased: 1.114
start validation test
0.737113402062
1.0
0.737113402062
0.848664688427
0
validation finish
batch start
#iterations: 280
currently lose_sum: 363.95185363292694
time_elpased: 1.168
batch start
#iterations: 281
currently lose_sum: 364.3699215054512
time_elpased: 1.169
batch start
#iterations: 282
currently lose_sum: 365.02660197019577
time_elpased: 1.117
batch start
#iterations: 283
currently lose_sum: 363.1846959590912
time_elpased: 1.121
batch start
#iterations: 284
currently lose_sum: 365.4410457611084
time_elpased: 1.123
batch start
#iterations: 285
currently lose_sum: 365.06106770038605
time_elpased: 1.175
batch start
#iterations: 286
currently lose_sum: 365.20731085538864
time_elpased: 1.125
batch start
#iterations: 287
currently lose_sum: 366.85427874326706
time_elpased: 1.112
batch start
#iterations: 288
currently lose_sum: 365.2841283082962
time_elpased: 1.111
batch start
#iterations: 289
currently lose_sum: 363.67106181383133
time_elpased: 1.159
batch start
#iterations: 290
currently lose_sum: 363.16538721323013
time_elpased: 1.129
batch start
#iterations: 291
currently lose_sum: 364.1273342370987
time_elpased: 1.121
batch start
#iterations: 292
currently lose_sum: 365.4172718524933
time_elpased: 1.178
batch start
#iterations: 293
currently lose_sum: 364.60080176591873
time_elpased: 1.132
batch start
#iterations: 294
currently lose_sum: 364.3800472021103
time_elpased: 1.128
batch start
#iterations: 295
currently lose_sum: 364.70373821258545
time_elpased: 1.13
batch start
#iterations: 296
currently lose_sum: 364.20703768730164
time_elpased: 1.141
batch start
#iterations: 297
currently lose_sum: 364.75472861528397
time_elpased: 1.129
batch start
#iterations: 298
currently lose_sum: 366.14732217788696
time_elpased: 1.16
batch start
#iterations: 299
currently lose_sum: 364.15327006578445
time_elpased: 1.128
start validation test
0.76
1.0
0.76
0.863636363636
0
validation finish
batch start
#iterations: 300
currently lose_sum: 364.73537665605545
time_elpased: 1.126
batch start
#iterations: 301
currently lose_sum: 364.55401569604874
time_elpased: 1.119
batch start
#iterations: 302
currently lose_sum: 363.31954431533813
time_elpased: 1.143
batch start
#iterations: 303
currently lose_sum: 363.55252438783646
time_elpased: 1.113
batch start
#iterations: 304
currently lose_sum: 363.80869153141975
time_elpased: 1.121
batch start
#iterations: 305
currently lose_sum: 364.0986068248749
time_elpased: 1.127
batch start
#iterations: 306
currently lose_sum: 364.69044548273087
time_elpased: 1.138
batch start
#iterations: 307
currently lose_sum: 365.10341477394104
time_elpased: 1.152
batch start
#iterations: 308
currently lose_sum: 363.97840332984924
time_elpased: 1.141
batch start
#iterations: 309
currently lose_sum: 363.7522490620613
time_elpased: 1.168
batch start
#iterations: 310
currently lose_sum: 364.769210934639
time_elpased: 1.115
batch start
#iterations: 311
currently lose_sum: 365.1895833015442
time_elpased: 1.119
batch start
#iterations: 312
currently lose_sum: 363.5914803147316
time_elpased: 1.127
batch start
#iterations: 313
currently lose_sum: 364.72579288482666
time_elpased: 1.129
batch start
#iterations: 314
currently lose_sum: 362.9479438662529
time_elpased: 1.145
batch start
#iterations: 315
currently lose_sum: 364.4932246208191
time_elpased: 1.179
batch start
#iterations: 316
currently lose_sum: 364.3254828453064
time_elpased: 1.131
batch start
#iterations: 317
currently lose_sum: 364.36482352018356
time_elpased: 1.121
batch start
#iterations: 318
currently lose_sum: 364.6974981725216
time_elpased: 1.113
batch start
#iterations: 319
currently lose_sum: 363.8407781124115
time_elpased: 1.159
start validation test
0.749278350515
1.0
0.749278350515
0.856671381424
0
validation finish
batch start
#iterations: 320
currently lose_sum: 363.3897588253021
time_elpased: 1.116
batch start
#iterations: 321
currently lose_sum: 363.70631259679794
time_elpased: 1.128
batch start
#iterations: 322
currently lose_sum: 363.8377694487572
time_elpased: 1.119
batch start
#iterations: 323
currently lose_sum: 364.7035024166107
time_elpased: 1.13
batch start
#iterations: 324
currently lose_sum: 361.8636125922203
time_elpased: 1.204
batch start
#iterations: 325
currently lose_sum: 362.87733459472656
time_elpased: 1.126
batch start
#iterations: 326
currently lose_sum: 363.8582298755646
time_elpased: 1.143
batch start
#iterations: 327
currently lose_sum: 364.05484318733215
time_elpased: 1.125
batch start
#iterations: 328
currently lose_sum: 364.8397718667984
time_elpased: 1.13
batch start
#iterations: 329
currently lose_sum: 365.8870474100113
time_elpased: 1.181
batch start
#iterations: 330
currently lose_sum: 363.94465267658234
time_elpased: 1.169
batch start
#iterations: 331
currently lose_sum: 366.42283791303635
time_elpased: 1.122
batch start
#iterations: 332
currently lose_sum: 364.1018078327179
time_elpased: 1.176
batch start
#iterations: 333
currently lose_sum: 365.0516807436943
time_elpased: 1.119
batch start
#iterations: 334
currently lose_sum: 362.94957131147385
time_elpased: 1.134
batch start
#iterations: 335
currently lose_sum: 364.73364597558975
time_elpased: 1.12
batch start
#iterations: 336
currently lose_sum: 363.9743810892105
time_elpased: 1.126
batch start
#iterations: 337
currently lose_sum: 365.47557908296585
time_elpased: 1.143
batch start
#iterations: 338
currently lose_sum: 363.4855965971947
time_elpased: 1.16
batch start
#iterations: 339
currently lose_sum: 363.5544903278351
time_elpased: 1.179
start validation test
0.758453608247
1.0
0.758453608247
0.862637040511
0
validation finish
batch start
#iterations: 340
currently lose_sum: 364.0591668486595
time_elpased: 1.128
batch start
#iterations: 341
currently lose_sum: 362.99263620376587
time_elpased: 1.134
batch start
#iterations: 342
currently lose_sum: 364.88811671733856
time_elpased: 1.115
batch start
#iterations: 343
currently lose_sum: 362.89716815948486
time_elpased: 1.158
batch start
#iterations: 344
currently lose_sum: 364.45248144865036
time_elpased: 1.117
batch start
#iterations: 345
currently lose_sum: 363.9295400381088
time_elpased: 1.126
batch start
#iterations: 346
currently lose_sum: 363.93828201293945
time_elpased: 1.117
batch start
#iterations: 347
currently lose_sum: 364.0457664132118
time_elpased: 1.19
batch start
#iterations: 348
currently lose_sum: 363.71891820430756
time_elpased: 1.132
batch start
#iterations: 349
currently lose_sum: 364.2184306383133
time_elpased: 1.122
batch start
#iterations: 350
currently lose_sum: 364.0059536099434
time_elpased: 1.149
batch start
#iterations: 351
currently lose_sum: 364.0480293035507
time_elpased: 1.128
batch start
#iterations: 352
currently lose_sum: 363.5125367641449
time_elpased: 1.125
batch start
#iterations: 353
currently lose_sum: 363.83775371313095
time_elpased: 1.138
batch start
#iterations: 354
currently lose_sum: 363.6612820029259
time_elpased: 1.135
batch start
#iterations: 355
currently lose_sum: 361.7590161561966
time_elpased: 1.116
batch start
#iterations: 356
currently lose_sum: 364.2452265024185
time_elpased: 1.183
batch start
#iterations: 357
currently lose_sum: 362.24048322439194
time_elpased: 1.121
batch start
#iterations: 358
currently lose_sum: 363.14831352233887
time_elpased: 1.119
batch start
#iterations: 359
currently lose_sum: 363.1159074306488
time_elpased: 1.123
start validation test
0.766907216495
1.0
0.766907216495
0.86807865103
0
validation finish
batch start
#iterations: 360
currently lose_sum: 365.4203647375107
time_elpased: 1.131
batch start
#iterations: 361
currently lose_sum: 362.86334639787674
time_elpased: 1.126
batch start
#iterations: 362
currently lose_sum: 362.404427587986
time_elpased: 1.162
batch start
#iterations: 363
currently lose_sum: 364.015909075737
time_elpased: 1.113
batch start
#iterations: 364
currently lose_sum: 364.83833771944046
time_elpased: 1.12
batch start
#iterations: 365
currently lose_sum: 364.33023899793625
time_elpased: 1.114
batch start
#iterations: 366
currently lose_sum: 363.5785480737686
time_elpased: 1.119
batch start
#iterations: 367
currently lose_sum: 364.1450819373131
time_elpased: 1.118
batch start
#iterations: 368
currently lose_sum: 363.32636496424675
time_elpased: 1.136
batch start
#iterations: 369
currently lose_sum: 364.5706810951233
time_elpased: 1.146
batch start
#iterations: 370
currently lose_sum: 363.6301165819168
time_elpased: 1.12
batch start
#iterations: 371
currently lose_sum: 364.330615401268
time_elpased: 1.116
batch start
#iterations: 372
currently lose_sum: 363.1318330168724
time_elpased: 1.115
batch start
#iterations: 373
currently lose_sum: 363.5038388967514
time_elpased: 1.122
batch start
#iterations: 374
currently lose_sum: 364.35930037498474
time_elpased: 1.167
batch start
#iterations: 375
currently lose_sum: 364.06765443086624
time_elpased: 1.129
batch start
#iterations: 376
currently lose_sum: 363.36019748449326
time_elpased: 1.149
batch start
#iterations: 377
currently lose_sum: 362.84820169210434
time_elpased: 1.131
batch start
#iterations: 378
currently lose_sum: 364.1419170498848
time_elpased: 1.13
batch start
#iterations: 379
currently lose_sum: 363.2391562461853
time_elpased: 1.123
start validation test
0.753402061856
1.0
0.753402061856
0.859360301035
0
validation finish
batch start
#iterations: 380
currently lose_sum: 363.26609897613525
time_elpased: 1.18
batch start
#iterations: 381
currently lose_sum: 363.791140794754
time_elpased: 1.117
batch start
#iterations: 382
currently lose_sum: 362.19521790742874
time_elpased: 1.12
batch start
#iterations: 383
currently lose_sum: 361.32642006874084
time_elpased: 1.124
batch start
#iterations: 384
currently lose_sum: 362.3682409822941
time_elpased: 1.122
batch start
#iterations: 385
currently lose_sum: 362.03808468580246
time_elpased: 1.131
batch start
#iterations: 386
currently lose_sum: 363.59658259153366
time_elpased: 1.145
batch start
#iterations: 387
currently lose_sum: 361.831038415432
time_elpased: 1.178
batch start
#iterations: 388
currently lose_sum: 364.0540589094162
time_elpased: 1.135
batch start
#iterations: 389
currently lose_sum: 362.7648614048958
time_elpased: 1.126
batch start
#iterations: 390
currently lose_sum: 362.3401647210121
time_elpased: 1.122
batch start
#iterations: 391
currently lose_sum: 362.61070841550827
time_elpased: 1.181
batch start
#iterations: 392
currently lose_sum: 362.7191952466965
time_elpased: 1.119
batch start
#iterations: 393
currently lose_sum: 364.5169072151184
time_elpased: 1.123
batch start
#iterations: 394
currently lose_sum: 362.2618409395218
time_elpased: 1.141
batch start
#iterations: 395
currently lose_sum: 362.5032088756561
time_elpased: 1.169
batch start
#iterations: 396
currently lose_sum: 363.8109807372093
time_elpased: 1.128
batch start
#iterations: 397
currently lose_sum: 363.56185191869736
time_elpased: 1.114
batch start
#iterations: 398
currently lose_sum: 363.6009200811386
time_elpased: 1.105
batch start
#iterations: 399
currently lose_sum: 363.67447340488434
time_elpased: 1.147
start validation test
0.757835051546
1.0
0.757835051546
0.862236818955
0
validation finish
acc: 0.775
pre: 1.000
rec: 0.775
F1: 0.873
auc: 0.000
