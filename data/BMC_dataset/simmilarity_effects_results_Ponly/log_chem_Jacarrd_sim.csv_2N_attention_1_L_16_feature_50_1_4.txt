start to construct graph
graph construct over
87453
epochs start
batch start
#iterations: 0
currently lose_sum: 556.0731926560402
time_elpased: 1.65
batch start
#iterations: 1
currently lose_sum: 555.4654085636139
time_elpased: 1.698
batch start
#iterations: 2
currently lose_sum: 554.3290003538132
time_elpased: 1.614
batch start
#iterations: 3
currently lose_sum: 553.7890568375587
time_elpased: 1.6
batch start
#iterations: 4
currently lose_sum: 549.3038247823715
time_elpased: 1.626
batch start
#iterations: 5
currently lose_sum: 549.0477333664894
time_elpased: 1.603
batch start
#iterations: 6
currently lose_sum: 547.30554074049
time_elpased: 1.622
batch start
#iterations: 7
currently lose_sum: 544.9220728874207
time_elpased: 1.692
batch start
#iterations: 8
currently lose_sum: 545.6441427767277
time_elpased: 1.618
batch start
#iterations: 9
currently lose_sum: 542.0555897951126
time_elpased: 1.605
batch start
#iterations: 10
currently lose_sum: 541.0661946833134
time_elpased: 1.623
batch start
#iterations: 11
currently lose_sum: 541.6263071298599
time_elpased: 1.607
batch start
#iterations: 12
currently lose_sum: 541.2324852049351
time_elpased: 1.654
batch start
#iterations: 13
currently lose_sum: 536.8532305955887
time_elpased: 1.622
batch start
#iterations: 14
currently lose_sum: 537.671472132206
time_elpased: 1.611
batch start
#iterations: 15
currently lose_sum: 535.3425598740578
time_elpased: 1.6
batch start
#iterations: 16
currently lose_sum: 536.5159419178963
time_elpased: 1.695
batch start
#iterations: 17
currently lose_sum: 535.2485969662666
time_elpased: 1.672
batch start
#iterations: 18
currently lose_sum: 534.3222619891167
time_elpased: 1.61
batch start
#iterations: 19
currently lose_sum: 533.7647753059864
time_elpased: 1.664
start validation test
0.155567010309
1.0
0.155567010309
0.269247925774
0
validation finish
batch start
#iterations: 20
currently lose_sum: 535.4510567188263
time_elpased: 1.609
batch start
#iterations: 21
currently lose_sum: 532.560958892107
time_elpased: 1.599
batch start
#iterations: 22
currently lose_sum: 532.9665388464928
time_elpased: 1.681
batch start
#iterations: 23
currently lose_sum: 530.690099388361
time_elpased: 1.615
batch start
#iterations: 24
currently lose_sum: 532.0766244232655
time_elpased: 1.616
batch start
#iterations: 25
currently lose_sum: 530.8439335823059
time_elpased: 1.672
batch start
#iterations: 26
currently lose_sum: 528.1467473208904
time_elpased: 1.608
batch start
#iterations: 27
currently lose_sum: 530.3232067525387
time_elpased: 1.613
batch start
#iterations: 28
currently lose_sum: 528.2689751088619
time_elpased: 1.641
batch start
#iterations: 29
currently lose_sum: 525.8358943164349
time_elpased: 1.656
batch start
#iterations: 30
currently lose_sum: 526.2376071214676
time_elpased: 1.612
batch start
#iterations: 31
currently lose_sum: 527.8340626358986
time_elpased: 1.691
batch start
#iterations: 32
currently lose_sum: 526.9312347769737
time_elpased: 1.621
batch start
#iterations: 33
currently lose_sum: 527.9808182418346
time_elpased: 1.639
batch start
#iterations: 34
currently lose_sum: 525.8619831204414
time_elpased: 1.601
batch start
#iterations: 35
currently lose_sum: 526.7250545620918
time_elpased: 1.692
batch start
#iterations: 36
currently lose_sum: 526.7825535237789
time_elpased: 1.703
batch start
#iterations: 37
currently lose_sum: 524.3147271573544
time_elpased: 1.689
batch start
#iterations: 38
currently lose_sum: 526.5382260680199
time_elpased: 1.61
batch start
#iterations: 39
currently lose_sum: 526.013432264328
time_elpased: 1.616
start validation test
0.191340206186
1.0
0.191340206186
0.321218414676
0
validation finish
batch start
#iterations: 40
currently lose_sum: 525.1476126611233
time_elpased: 1.682
batch start
#iterations: 41
currently lose_sum: 525.0362983047962
time_elpased: 1.625
batch start
#iterations: 42
currently lose_sum: 525.9608908295631
time_elpased: 1.609
batch start
#iterations: 43
currently lose_sum: 525.1001326441765
time_elpased: 1.639
batch start
#iterations: 44
currently lose_sum: 523.7195715606213
time_elpased: 1.66
batch start
#iterations: 45
currently lose_sum: 524.0254194438457
time_elpased: 1.636
batch start
#iterations: 46
currently lose_sum: 525.2681868672371
time_elpased: 1.648
batch start
#iterations: 47
currently lose_sum: 523.5555890500546
time_elpased: 1.612
batch start
#iterations: 48
currently lose_sum: 521.2479661107063
time_elpased: 1.628
batch start
#iterations: 49
currently lose_sum: 522.1351118087769
time_elpased: 1.674
batch start
#iterations: 50
currently lose_sum: 520.4516595602036
time_elpased: 1.62
batch start
#iterations: 51
currently lose_sum: 520.2551740407944
time_elpased: 1.633
batch start
#iterations: 52
currently lose_sum: 524.0022656321526
time_elpased: 1.714
batch start
#iterations: 53
currently lose_sum: 523.6189951300621
time_elpased: 1.637
batch start
#iterations: 54
currently lose_sum: 521.9343966841698
time_elpased: 1.61
batch start
#iterations: 55
currently lose_sum: 521.2589514553547
time_elpased: 1.608
batch start
#iterations: 56
currently lose_sum: 520.2272915542126
time_elpased: 1.613
batch start
#iterations: 57
currently lose_sum: 519.4097743630409
time_elpased: 1.616
batch start
#iterations: 58
currently lose_sum: 518.9630082845688
time_elpased: 1.696
batch start
#iterations: 59
currently lose_sum: 518.5311197936535
time_elpased: 1.643
start validation test
0.0368041237113
1.0
0.0368041237113
0.0709953266382
0
validation finish
batch start
#iterations: 60
currently lose_sum: 518.8995723426342
time_elpased: 1.617
batch start
#iterations: 61
currently lose_sum: 517.18474650383
time_elpased: 1.639
batch start
#iterations: 62
currently lose_sum: 517.9943350553513
time_elpased: 1.662
batch start
#iterations: 63
currently lose_sum: 519.1239551603794
time_elpased: 1.697
batch start
#iterations: 64
currently lose_sum: 516.2254311442375
time_elpased: 1.668
batch start
#iterations: 65
currently lose_sum: 518.938999414444
time_elpased: 1.611
batch start
#iterations: 66
currently lose_sum: 518.084262907505
time_elpased: 1.714
batch start
#iterations: 67
currently lose_sum: 516.3854959309101
time_elpased: 1.679
batch start
#iterations: 68
currently lose_sum: 516.8424887061119
time_elpased: 1.633
batch start
#iterations: 69
currently lose_sum: 516.9278463721275
time_elpased: 1.686
batch start
#iterations: 70
currently lose_sum: 518.7450696527958
time_elpased: 1.661
batch start
#iterations: 71
currently lose_sum: 518.198866635561
time_elpased: 1.674
batch start
#iterations: 72
currently lose_sum: 515.8906015455723
time_elpased: 1.682
batch start
#iterations: 73
currently lose_sum: 516.2540454566479
time_elpased: 1.677
batch start
#iterations: 74
currently lose_sum: 514.3968277573586
time_elpased: 1.684
batch start
#iterations: 75
currently lose_sum: 514.0455467700958
time_elpased: 1.677
batch start
#iterations: 76
currently lose_sum: 516.9438346624374
time_elpased: 1.624
batch start
#iterations: 77
currently lose_sum: 515.0801812708378
time_elpased: 1.661
batch start
#iterations: 78
currently lose_sum: 517.2118898928165
time_elpased: 1.657
batch start
#iterations: 79
currently lose_sum: 515.845704138279
time_elpased: 1.627
start validation test
0.30206185567
1.0
0.30206185567
0.4639746635
0
validation finish
batch start
#iterations: 80
currently lose_sum: 516.5171230137348
time_elpased: 1.653
batch start
#iterations: 81
currently lose_sum: 515.0241388082504
time_elpased: 1.607
batch start
#iterations: 82
currently lose_sum: 513.1441822350025
time_elpased: 1.614
batch start
#iterations: 83
currently lose_sum: 513.0446095764637
time_elpased: 1.626
batch start
#iterations: 84
currently lose_sum: 513.9341269433498
time_elpased: 1.611
batch start
#iterations: 85
currently lose_sum: 513.8739650547504
time_elpased: 1.682
batch start
#iterations: 86
currently lose_sum: 515.1394442915916
time_elpased: 1.621
batch start
#iterations: 87
currently lose_sum: 514.8823490142822
time_elpased: 1.638
batch start
#iterations: 88
currently lose_sum: 515.7428629994392
time_elpased: 1.685
batch start
#iterations: 89
currently lose_sum: 513.2361242175102
time_elpased: 1.635
batch start
#iterations: 90
currently lose_sum: 513.8467566072941
time_elpased: 1.647
batch start
#iterations: 91
currently lose_sum: 513.108607172966
time_elpased: 1.629
batch start
#iterations: 92
currently lose_sum: 512.0902768373489
time_elpased: 1.685
batch start
#iterations: 93
currently lose_sum: 515.2478832900524
time_elpased: 1.746
batch start
#iterations: 94
currently lose_sum: 512.9465526342392
time_elpased: 1.678
batch start
#iterations: 95
currently lose_sum: 513.9224853217602
time_elpased: 1.612
batch start
#iterations: 96
currently lose_sum: 513.0546298623085
time_elpased: 1.688
batch start
#iterations: 97
currently lose_sum: 513.1327458620071
time_elpased: 1.609
batch start
#iterations: 98
currently lose_sum: 509.4805028140545
time_elpased: 1.604
batch start
#iterations: 99
currently lose_sum: 514.2015784978867
time_elpased: 1.618
start validation test
0.183298969072
1.0
0.183298969072
0.309810071441
0
validation finish
batch start
#iterations: 100
currently lose_sum: 513.5557607114315
time_elpased: 1.619
batch start
#iterations: 101
currently lose_sum: 511.25116392970085
time_elpased: 1.633
batch start
#iterations: 102
currently lose_sum: 514.0638597309589
time_elpased: 1.669
batch start
#iterations: 103
currently lose_sum: 511.59249141812325
time_elpased: 1.678
batch start
#iterations: 104
currently lose_sum: 510.55473986268044
time_elpased: 1.663
batch start
#iterations: 105
currently lose_sum: 510.74083238840103
time_elpased: 1.667
batch start
#iterations: 106
currently lose_sum: 512.9381801784039
time_elpased: 1.621
batch start
#iterations: 107
currently lose_sum: 511.74051201343536
time_elpased: 1.648
batch start
#iterations: 108
currently lose_sum: 511.0940101146698
time_elpased: 1.702
batch start
#iterations: 109
currently lose_sum: 514.1263419985771
time_elpased: 1.669
batch start
#iterations: 110
currently lose_sum: 512.811421841383
time_elpased: 1.619
batch start
#iterations: 111
currently lose_sum: 513.2920225560665
time_elpased: 1.638
batch start
#iterations: 112
currently lose_sum: 511.80022779107094
time_elpased: 1.626
batch start
#iterations: 113
currently lose_sum: 512.3300758004189
time_elpased: 1.646
batch start
#iterations: 114
currently lose_sum: 511.74307894706726
time_elpased: 1.614
batch start
#iterations: 115
currently lose_sum: 509.55583876371384
time_elpased: 1.627
batch start
#iterations: 116
currently lose_sum: 513.2388994991779
time_elpased: 1.63
batch start
#iterations: 117
currently lose_sum: 510.8445014357567
time_elpased: 1.678
batch start
#iterations: 118
currently lose_sum: 511.54659366607666
time_elpased: 1.639
batch start
#iterations: 119
currently lose_sum: 510.08203861117363
time_elpased: 1.609
start validation test
0.324742268041
1.0
0.324742268041
0.490272373541
0
validation finish
batch start
#iterations: 120
currently lose_sum: 513.3371637165546
time_elpased: 1.615
batch start
#iterations: 121
currently lose_sum: 511.7548390030861
time_elpased: 1.669
batch start
#iterations: 122
currently lose_sum: 512.9902858436108
time_elpased: 1.66
batch start
#iterations: 123
currently lose_sum: 512.9316248595715
time_elpased: 1.674
batch start
#iterations: 124
currently lose_sum: 510.72573068737984
time_elpased: 1.609
batch start
#iterations: 125
currently lose_sum: 508.93331879377365
time_elpased: 1.687
batch start
#iterations: 126
currently lose_sum: 509.1796361207962
time_elpased: 1.683
batch start
#iterations: 127
currently lose_sum: 511.0079321861267
time_elpased: 1.684
batch start
#iterations: 128
currently lose_sum: 512.0420942306519
time_elpased: 1.676
batch start
#iterations: 129
currently lose_sum: 509.9735854566097
time_elpased: 1.617
batch start
#iterations: 130
currently lose_sum: 510.306218534708
time_elpased: 1.613
batch start
#iterations: 131
currently lose_sum: 512.1346598267555
time_elpased: 1.625
batch start
#iterations: 132
currently lose_sum: 510.57355159521103
time_elpased: 1.637
batch start
#iterations: 133
currently lose_sum: 510.745851367712
time_elpased: 1.685
batch start
#iterations: 134
currently lose_sum: 510.0280182659626
time_elpased: 1.62
batch start
#iterations: 135
currently lose_sum: 510.69784393906593
time_elpased: 1.679
batch start
#iterations: 136
currently lose_sum: 511.8928168416023
time_elpased: 1.624
batch start
#iterations: 137
currently lose_sum: 507.80765599012375
time_elpased: 1.611
batch start
#iterations: 138
currently lose_sum: 512.0404069721699
time_elpased: 1.665
batch start
#iterations: 139
currently lose_sum: 511.5906657576561
time_elpased: 1.698
start validation test
0.0848453608247
1.0
0.0848453608247
0.156419272071
0
validation finish
batch start
#iterations: 140
currently lose_sum: 509.12078762054443
time_elpased: 1.685
batch start
#iterations: 141
currently lose_sum: 509.206187158823
time_elpased: 1.622
batch start
#iterations: 142
currently lose_sum: 510.4266095459461
time_elpased: 1.627
batch start
#iterations: 143
currently lose_sum: 509.99257457256317
time_elpased: 1.69
batch start
#iterations: 144
currently lose_sum: 510.45784336328506
time_elpased: 1.624
batch start
#iterations: 145
currently lose_sum: 510.75404411554337
time_elpased: 1.62
batch start
#iterations: 146
currently lose_sum: 511.02688306570053
time_elpased: 1.62
batch start
#iterations: 147
currently lose_sum: 507.27291679382324
time_elpased: 1.634
batch start
#iterations: 148
currently lose_sum: 509.0435189604759
time_elpased: 1.609
batch start
#iterations: 149
currently lose_sum: 510.0016368627548
time_elpased: 1.673
batch start
#iterations: 150
currently lose_sum: 508.97613194584846
time_elpased: 1.688
batch start
#iterations: 151
currently lose_sum: 509.82018196582794
time_elpased: 1.684
batch start
#iterations: 152
currently lose_sum: 508.2215005457401
time_elpased: 1.661
batch start
#iterations: 153
currently lose_sum: 507.62161087989807
time_elpased: 1.636
batch start
#iterations: 154
currently lose_sum: 510.0668323338032
time_elpased: 1.627
batch start
#iterations: 155
currently lose_sum: 509.8964616060257
time_elpased: 1.675
batch start
#iterations: 156
currently lose_sum: 509.90902319550514
time_elpased: 1.621
batch start
#iterations: 157
currently lose_sum: 508.13436910510063
time_elpased: 1.659
batch start
#iterations: 158
currently lose_sum: 509.56560641527176
time_elpased: 1.716
batch start
#iterations: 159
currently lose_sum: 508.19890785217285
time_elpased: 1.625
start validation test
0.059175257732
1.0
0.059175257732
0.111738368698
0
validation finish
batch start
#iterations: 160
currently lose_sum: 509.9784155189991
time_elpased: 1.63
batch start
#iterations: 161
currently lose_sum: 509.3170107603073
time_elpased: 1.696
batch start
#iterations: 162
currently lose_sum: 507.09235978126526
time_elpased: 1.621
batch start
#iterations: 163
currently lose_sum: 509.36166420578957
time_elpased: 1.633
batch start
#iterations: 164
currently lose_sum: 509.252794533968
time_elpased: 1.633
batch start
#iterations: 165
currently lose_sum: 511.3751029074192
time_elpased: 1.649
batch start
#iterations: 166
currently lose_sum: 509.97721230983734
time_elpased: 1.642
batch start
#iterations: 167
currently lose_sum: 509.95973122119904
time_elpased: 1.683
batch start
#iterations: 168
currently lose_sum: 508.8684287369251
time_elpased: 1.636
batch start
#iterations: 169
currently lose_sum: 507.89838165044785
time_elpased: 1.62
batch start
#iterations: 170
currently lose_sum: 509.16628643870354
time_elpased: 1.668
batch start
#iterations: 171
currently lose_sum: 508.5882000923157
time_elpased: 1.657
batch start
#iterations: 172
currently lose_sum: 508.15662947297096
time_elpased: 1.639
batch start
#iterations: 173
currently lose_sum: 507.60663333535194
time_elpased: 1.614
batch start
#iterations: 174
currently lose_sum: 510.1956394612789
time_elpased: 1.618
batch start
#iterations: 175
currently lose_sum: 510.12330970168114
time_elpased: 1.615
batch start
#iterations: 176
currently lose_sum: 510.7349262237549
time_elpased: 1.696
batch start
#iterations: 177
currently lose_sum: 508.290197879076
time_elpased: 1.628
batch start
#iterations: 178
currently lose_sum: 507.32572200894356
time_elpased: 1.622
batch start
#iterations: 179
currently lose_sum: 510.50307723879814
time_elpased: 1.657
start validation test
0.102164948454
1.0
0.102164948454
0.185389580021
0
validation finish
batch start
#iterations: 180
currently lose_sum: 507.97649213671684
time_elpased: 1.613
batch start
#iterations: 181
currently lose_sum: 509.61048635840416
time_elpased: 1.614
batch start
#iterations: 182
currently lose_sum: 508.33446472883224
time_elpased: 1.7
batch start
#iterations: 183
currently lose_sum: 508.45637533068657
time_elpased: 1.634
batch start
#iterations: 184
currently lose_sum: 509.15877944231033
time_elpased: 1.622
batch start
#iterations: 185
currently lose_sum: 507.6560146212578
time_elpased: 1.633
batch start
#iterations: 186
currently lose_sum: 508.6864994466305
time_elpased: 1.656
batch start
#iterations: 187
currently lose_sum: 509.7824548780918
time_elpased: 1.635
batch start
#iterations: 188
currently lose_sum: 508.4724435210228
time_elpased: 1.614
batch start
#iterations: 189
currently lose_sum: 508.66961669921875
time_elpased: 1.713
batch start
#iterations: 190
currently lose_sum: 508.9461106657982
time_elpased: 1.623
batch start
#iterations: 191
currently lose_sum: 507.25218683481216
time_elpased: 1.616
batch start
#iterations: 192
currently lose_sum: 507.2820026278496
time_elpased: 1.616
batch start
#iterations: 193
currently lose_sum: 508.57661256194115
time_elpased: 1.664
batch start
#iterations: 194
currently lose_sum: 509.470057785511
time_elpased: 1.62
batch start
#iterations: 195
currently lose_sum: 506.6839402616024
time_elpased: 1.612
batch start
#iterations: 196
currently lose_sum: 507.8164905309677
time_elpased: 1.634
batch start
#iterations: 197
currently lose_sum: 508.07630747556686
time_elpased: 1.671
batch start
#iterations: 198
currently lose_sum: 508.2076653242111
time_elpased: 1.655
batch start
#iterations: 199
currently lose_sum: 510.07407224178314
time_elpased: 1.675
start validation test
0.145773195876
1.0
0.145773195876
0.254453842001
0
validation finish
batch start
#iterations: 200
currently lose_sum: 503.3641817867756
time_elpased: 1.655
batch start
#iterations: 201
currently lose_sum: 508.6044310927391
time_elpased: 1.661
batch start
#iterations: 202
currently lose_sum: 507.41886788606644
time_elpased: 1.715
batch start
#iterations: 203
currently lose_sum: 508.5658380687237
time_elpased: 1.645
batch start
#iterations: 204
currently lose_sum: 508.52374854683876
time_elpased: 1.707
batch start
#iterations: 205
currently lose_sum: 506.19469279050827
time_elpased: 1.632
batch start
#iterations: 206
currently lose_sum: 505.2528463304043
time_elpased: 1.627
batch start
#iterations: 207
currently lose_sum: 505.34930938482285
time_elpased: 1.718
batch start
#iterations: 208
currently lose_sum: 507.2220169007778
time_elpased: 1.695
batch start
#iterations: 209
currently lose_sum: 507.5721900165081
time_elpased: 1.674
batch start
#iterations: 210
currently lose_sum: 508.8052213191986
time_elpased: 1.662
batch start
#iterations: 211
currently lose_sum: 507.6548960506916
time_elpased: 1.694
batch start
#iterations: 212
currently lose_sum: 506.7370550632477
time_elpased: 1.646
batch start
#iterations: 213
currently lose_sum: 507.6158923506737
time_elpased: 1.689
batch start
#iterations: 214
currently lose_sum: 508.0406193435192
time_elpased: 1.616
batch start
#iterations: 215
currently lose_sum: 507.161851644516
time_elpased: 1.614
batch start
#iterations: 216
currently lose_sum: 509.14300072193146
time_elpased: 1.694
batch start
#iterations: 217
currently lose_sum: 507.39944899082184
time_elpased: 1.652
batch start
#iterations: 218
currently lose_sum: 507.55985325574875
time_elpased: 1.65
batch start
#iterations: 219
currently lose_sum: 509.4897249341011
time_elpased: 1.612
start validation test
0.0343298969072
1.0
0.0343298969072
0.0663809428885
0
validation finish
batch start
#iterations: 220
currently lose_sum: 507.2040367424488
time_elpased: 1.639
batch start
#iterations: 221
currently lose_sum: 508.8405864536762
time_elpased: 1.713
batch start
#iterations: 222
currently lose_sum: 505.94057646393776
time_elpased: 1.633
batch start
#iterations: 223
currently lose_sum: 506.97457176446915
time_elpased: 1.624
batch start
#iterations: 224
currently lose_sum: 508.2904832661152
time_elpased: 1.638
batch start
#iterations: 225
currently lose_sum: 507.4946664273739
time_elpased: 1.665
batch start
#iterations: 226
currently lose_sum: 505.91570967435837
time_elpased: 1.625
batch start
#iterations: 227
currently lose_sum: 507.1329679489136
time_elpased: 1.62
batch start
#iterations: 228
currently lose_sum: 504.00549536943436
time_elpased: 1.616
batch start
#iterations: 229
currently lose_sum: 505.4538798928261
time_elpased: 1.627
batch start
#iterations: 230
currently lose_sum: 506.3812825381756
time_elpased: 1.631
batch start
#iterations: 231
currently lose_sum: 507.4435870349407
time_elpased: 1.677
batch start
#iterations: 232
currently lose_sum: 508.3442075252533
time_elpased: 1.631
batch start
#iterations: 233
currently lose_sum: 506.31316938996315
time_elpased: 1.635
batch start
#iterations: 234
currently lose_sum: 508.85748785734177
time_elpased: 1.616
batch start
#iterations: 235
currently lose_sum: 504.6847821176052
time_elpased: 1.634
batch start
#iterations: 236
currently lose_sum: 502.91080519557
time_elpased: 1.623
batch start
#iterations: 237
currently lose_sum: 504.7627340257168
time_elpased: 1.628
batch start
#iterations: 238
currently lose_sum: 503.4297041594982
time_elpased: 1.683
batch start
#iterations: 239
currently lose_sum: 507.48865431547165
time_elpased: 1.64
start validation test
0.242886597938
1.0
0.242886597938
0.390842733908
0
validation finish
batch start
#iterations: 240
currently lose_sum: 505.1928753554821
time_elpased: 1.63
batch start
#iterations: 241
currently lose_sum: 506.9631122946739
time_elpased: 1.626
batch start
#iterations: 242
currently lose_sum: 507.95141384005547
time_elpased: 1.669
batch start
#iterations: 243
currently lose_sum: 506.22729593515396
time_elpased: 1.694
batch start
#iterations: 244
currently lose_sum: 508.02051135897636
time_elpased: 1.645
batch start
#iterations: 245
currently lose_sum: 506.2721416056156
time_elpased: 1.611
batch start
#iterations: 246
currently lose_sum: 507.72735741734505
time_elpased: 1.628
batch start
#iterations: 247
currently lose_sum: 509.3259904682636
time_elpased: 1.697
batch start
#iterations: 248
currently lose_sum: 506.3446786403656
time_elpased: 1.687
batch start
#iterations: 249
currently lose_sum: 506.40651631355286
time_elpased: 1.688
batch start
#iterations: 250
currently lose_sum: 508.5631167292595
time_elpased: 1.689
batch start
#iterations: 251
currently lose_sum: 505.63462698459625
time_elpased: 1.622
batch start
#iterations: 252
currently lose_sum: 505.0327365398407
time_elpased: 1.687
batch start
#iterations: 253
currently lose_sum: 505.026765614748
time_elpased: 1.675
batch start
#iterations: 254
currently lose_sum: 505.13687747716904
time_elpased: 1.619
batch start
#iterations: 255
currently lose_sum: 505.17585268616676
time_elpased: 1.689
batch start
#iterations: 256
currently lose_sum: 506.66549068689346
time_elpased: 1.628
batch start
#iterations: 257
currently lose_sum: 504.3246619999409
time_elpased: 1.663
batch start
#iterations: 258
currently lose_sum: 504.3178379535675
time_elpased: 1.638
batch start
#iterations: 259
currently lose_sum: 508.30097272992134
time_elpased: 1.63
start validation test
0.151443298969
1.0
0.151443298969
0.263049512042
0
validation finish
batch start
#iterations: 260
currently lose_sum: 503.87951758503914
time_elpased: 1.608
batch start
#iterations: 261
currently lose_sum: 506.0026484429836
time_elpased: 1.612
batch start
#iterations: 262
currently lose_sum: 507.4504199028015
time_elpased: 1.628
batch start
#iterations: 263
currently lose_sum: 506.63899949193
time_elpased: 1.666
batch start
#iterations: 264
currently lose_sum: 508.81750509142876
time_elpased: 1.616
batch start
#iterations: 265
currently lose_sum: 506.05542328953743
time_elpased: 1.707
batch start
#iterations: 266
currently lose_sum: 505.73977053165436
time_elpased: 1.695
batch start
#iterations: 267
currently lose_sum: 507.35363391041756
time_elpased: 1.68
batch start
#iterations: 268
currently lose_sum: 505.1399032175541
time_elpased: 1.603
batch start
#iterations: 269
currently lose_sum: 506.0559325814247
time_elpased: 1.671
batch start
#iterations: 270
currently lose_sum: 506.8605042695999
time_elpased: 1.645
batch start
#iterations: 271
currently lose_sum: 507.3621886074543
time_elpased: 1.674
batch start
#iterations: 272
currently lose_sum: 506.0617254078388
time_elpased: 1.689
batch start
#iterations: 273
currently lose_sum: 506.32207652926445
time_elpased: 1.614
batch start
#iterations: 274
currently lose_sum: 507.41587349772453
time_elpased: 1.638
batch start
#iterations: 275
currently lose_sum: 504.4561786055565
time_elpased: 1.647
batch start
#iterations: 276
currently lose_sum: 507.46689558029175
time_elpased: 1.672
batch start
#iterations: 277
currently lose_sum: 507.04131469130516
time_elpased: 1.614
batch start
#iterations: 278
currently lose_sum: 504.89461237192154
time_elpased: 1.694
batch start
#iterations: 279
currently lose_sum: 504.6789271235466
time_elpased: 1.672
start validation test
0.215773195876
1.0
0.215773195876
0.354956330026
0
validation finish
batch start
#iterations: 280
currently lose_sum: 505.49865648150444
time_elpased: 1.628
batch start
#iterations: 281
currently lose_sum: 503.92982068657875
time_elpased: 1.67
batch start
#iterations: 282
currently lose_sum: 505.62810984253883
time_elpased: 1.629
batch start
#iterations: 283
currently lose_sum: 505.44817703962326
time_elpased: 1.704
batch start
#iterations: 284
currently lose_sum: 504.25428384542465
time_elpased: 1.625
batch start
#iterations: 285
currently lose_sum: 505.03968116641045
time_elpased: 1.631
batch start
#iterations: 286
currently lose_sum: 503.7233990430832
time_elpased: 1.61
batch start
#iterations: 287
currently lose_sum: 506.2402626276016
time_elpased: 1.696
batch start
#iterations: 288
currently lose_sum: 506.63389497995377
time_elpased: 1.662
batch start
#iterations: 289
currently lose_sum: 507.2908733189106
time_elpased: 1.634
batch start
#iterations: 290
currently lose_sum: 503.9769322872162
time_elpased: 1.684
batch start
#iterations: 291
currently lose_sum: 504.1603801548481
time_elpased: 1.69
batch start
#iterations: 292
currently lose_sum: 504.96204367280006
time_elpased: 1.619
batch start
#iterations: 293
currently lose_sum: 507.27031058073044
time_elpased: 1.694
batch start
#iterations: 294
currently lose_sum: 503.86353063583374
time_elpased: 1.619
batch start
#iterations: 295
currently lose_sum: 506.03210148215294
time_elpased: 1.702
batch start
#iterations: 296
currently lose_sum: 503.6901749074459
time_elpased: 1.634
batch start
#iterations: 297
currently lose_sum: 504.3585989177227
time_elpased: 1.624
batch start
#iterations: 298
currently lose_sum: 504.55055728554726
time_elpased: 1.711
batch start
#iterations: 299
currently lose_sum: 505.48626869916916
time_elpased: 1.698
start validation test
0.14824742268
1.0
0.14824742268
0.258215119411
0
validation finish
batch start
#iterations: 300
currently lose_sum: 505.17779248952866
time_elpased: 1.655
batch start
#iterations: 301
currently lose_sum: 505.79078486561775
time_elpased: 1.665
batch start
#iterations: 302
currently lose_sum: 505.1317506432533
time_elpased: 1.622
batch start
#iterations: 303
currently lose_sum: 507.25083419680595
time_elpased: 1.703
batch start
#iterations: 304
currently lose_sum: 504.4004862308502
time_elpased: 1.621
batch start
#iterations: 305
currently lose_sum: 506.80697298049927
time_elpased: 1.618
batch start
#iterations: 306
currently lose_sum: 505.3603494465351
time_elpased: 1.668
batch start
#iterations: 307
currently lose_sum: 503.58606082201004
time_elpased: 1.669
batch start
#iterations: 308
currently lose_sum: 506.05448365211487
time_elpased: 1.702
batch start
#iterations: 309
currently lose_sum: 504.8577267229557
time_elpased: 1.687
batch start
#iterations: 310
currently lose_sum: 505.4389861524105
time_elpased: 1.609
batch start
#iterations: 311
currently lose_sum: 504.6415397822857
time_elpased: 1.713
batch start
#iterations: 312
currently lose_sum: 503.90146270394325
time_elpased: 1.631
batch start
#iterations: 313
currently lose_sum: 506.12641629576683
time_elpased: 1.678
batch start
#iterations: 314
currently lose_sum: 505.9621567726135
time_elpased: 1.658
batch start
#iterations: 315
currently lose_sum: 506.47868052124977
time_elpased: 1.625
batch start
#iterations: 316
currently lose_sum: 505.3798856437206
time_elpased: 1.62
batch start
#iterations: 317
currently lose_sum: 504.58170983195305
time_elpased: 1.632
batch start
#iterations: 318
currently lose_sum: 502.6484720110893
time_elpased: 1.646
batch start
#iterations: 319
currently lose_sum: 503.32485288381577
time_elpased: 1.682
start validation test
0.123711340206
1.0
0.123711340206
0.220183486239
0
validation finish
batch start
#iterations: 320
currently lose_sum: 505.7679803073406
time_elpased: 1.677
batch start
#iterations: 321
currently lose_sum: 503.33582550287247
time_elpased: 1.719
batch start
#iterations: 322
currently lose_sum: 504.47159281373024
time_elpased: 1.619
batch start
#iterations: 323
currently lose_sum: 504.5906741321087
time_elpased: 1.688
batch start
#iterations: 324
currently lose_sum: 501.6456099450588
time_elpased: 1.667
batch start
#iterations: 325
currently lose_sum: 506.82745960354805
time_elpased: 1.71
batch start
#iterations: 326
currently lose_sum: 504.7935243844986
time_elpased: 1.617
batch start
#iterations: 327
currently lose_sum: 505.5335097312927
time_elpased: 1.682
batch start
#iterations: 328
currently lose_sum: 504.1057244539261
time_elpased: 1.644
batch start
#iterations: 329
currently lose_sum: 502.8846109509468
time_elpased: 1.638
batch start
#iterations: 330
currently lose_sum: 506.427427649498
time_elpased: 1.636
batch start
#iterations: 331
currently lose_sum: 503.8044304549694
time_elpased: 1.681
batch start
#iterations: 332
currently lose_sum: 503.49866926670074
time_elpased: 1.681
batch start
#iterations: 333
currently lose_sum: 504.0420964062214
time_elpased: 1.633
batch start
#iterations: 334
currently lose_sum: 503.6595281660557
time_elpased: 1.683
batch start
#iterations: 335
currently lose_sum: 503.03944903612137
time_elpased: 1.651
batch start
#iterations: 336
currently lose_sum: 503.55024591088295
time_elpased: 1.681
batch start
#iterations: 337
currently lose_sum: 504.64335083961487
time_elpased: 1.701
batch start
#iterations: 338
currently lose_sum: 507.3133909404278
time_elpased: 1.719
batch start
#iterations: 339
currently lose_sum: 503.51486971974373
time_elpased: 1.626
start validation test
0.285257731959
1.0
0.285257731959
0.443891874549
0
validation finish
batch start
#iterations: 340
currently lose_sum: 504.42086121439934
time_elpased: 1.656
batch start
#iterations: 341
currently lose_sum: 505.7593540549278
time_elpased: 1.613
batch start
#iterations: 342
currently lose_sum: 504.74803775548935
time_elpased: 1.7
batch start
#iterations: 343
currently lose_sum: 505.5140958428383
time_elpased: 1.671
batch start
#iterations: 344
currently lose_sum: 506.3258957564831
time_elpased: 1.685
batch start
#iterations: 345
currently lose_sum: 506.335734218359
time_elpased: 1.627
batch start
#iterations: 346
currently lose_sum: 503.67863950133324
time_elpased: 1.673
batch start
#iterations: 347
currently lose_sum: 504.78252947330475
time_elpased: 1.692
batch start
#iterations: 348
currently lose_sum: 505.72375839948654
time_elpased: 1.619
batch start
#iterations: 349
currently lose_sum: 504.45990177989006
time_elpased: 1.689
batch start
#iterations: 350
currently lose_sum: 503.05031275749207
time_elpased: 1.617
batch start
#iterations: 351
currently lose_sum: 505.85189005732536
time_elpased: 1.623
batch start
#iterations: 352
currently lose_sum: 506.00076511502266
time_elpased: 1.637
batch start
#iterations: 353
currently lose_sum: 503.50998067855835
time_elpased: 1.634
batch start
#iterations: 354
currently lose_sum: 506.14215674996376
time_elpased: 1.655
batch start
#iterations: 355
currently lose_sum: 506.3366282880306
time_elpased: 1.627
batch start
#iterations: 356
currently lose_sum: 505.5403755605221
time_elpased: 1.621
batch start
#iterations: 357
currently lose_sum: 503.5834535062313
time_elpased: 1.619
batch start
#iterations: 358
currently lose_sum: 504.50494560599327
time_elpased: 1.661
batch start
#iterations: 359
currently lose_sum: 502.118381023407
time_elpased: 1.675
start validation test
0.246701030928
1.0
0.246701030928
0.395766145704
0
validation finish
batch start
#iterations: 360
currently lose_sum: 502.56830018758774
time_elpased: 1.618
batch start
#iterations: 361
currently lose_sum: 503.5849621295929
time_elpased: 1.627
batch start
#iterations: 362
currently lose_sum: 503.7548348605633
time_elpased: 1.627
batch start
#iterations: 363
currently lose_sum: 502.42657232284546
time_elpased: 1.622
batch start
#iterations: 364
currently lose_sum: 503.9915151298046
time_elpased: 1.616
batch start
#iterations: 365
currently lose_sum: 503.64210134744644
time_elpased: 1.602
batch start
#iterations: 366
currently lose_sum: 503.9243951141834
time_elpased: 1.634
batch start
#iterations: 367
currently lose_sum: 506.7424539923668
time_elpased: 1.7
batch start
#iterations: 368
currently lose_sum: 504.66332149505615
time_elpased: 1.674
batch start
#iterations: 369
currently lose_sum: 502.43919438123703
time_elpased: 1.641
batch start
#iterations: 370
currently lose_sum: 503.5377741456032
time_elpased: 1.636
batch start
#iterations: 371
currently lose_sum: 502.27005365490913
time_elpased: 1.666
batch start
#iterations: 372
currently lose_sum: 504.9666829407215
time_elpased: 1.68
batch start
#iterations: 373
currently lose_sum: 506.75983169674873
time_elpased: 1.66
batch start
#iterations: 374
currently lose_sum: 503.04629212617874
time_elpased: 1.639
batch start
#iterations: 375
currently lose_sum: 508.1859987080097
time_elpased: 1.616
batch start
#iterations: 376
currently lose_sum: 502.70113429427147
time_elpased: 1.626
batch start
#iterations: 377
currently lose_sum: 503.57293751835823
time_elpased: 1.636
batch start
#iterations: 378
currently lose_sum: 506.03470635414124
time_elpased: 1.612
batch start
#iterations: 379
currently lose_sum: 501.472009986639
time_elpased: 1.709
start validation test
0.183608247423
1.0
0.183608247423
0.310251720233
0
validation finish
batch start
#iterations: 380
currently lose_sum: 503.6412055194378
time_elpased: 1.638
batch start
#iterations: 381
currently lose_sum: 503.44061160087585
time_elpased: 1.621
batch start
#iterations: 382
currently lose_sum: 503.3143326342106
time_elpased: 1.624
batch start
#iterations: 383
currently lose_sum: 506.2585057914257
time_elpased: 1.642
batch start
#iterations: 384
currently lose_sum: 504.3354671895504
time_elpased: 1.685
batch start
#iterations: 385
currently lose_sum: 503.2673839330673
time_elpased: 1.629
batch start
#iterations: 386
currently lose_sum: 505.74388748407364
time_elpased: 1.638
batch start
#iterations: 387
currently lose_sum: 503.8185067474842
time_elpased: 1.62
batch start
#iterations: 388
currently lose_sum: 504.2305500507355
time_elpased: 1.718
batch start
#iterations: 389
currently lose_sum: 503.62407970428467
time_elpased: 1.625
batch start
#iterations: 390
currently lose_sum: 504.4018384516239
time_elpased: 1.665
batch start
#iterations: 391
currently lose_sum: 504.17112082242966
time_elpased: 1.674
batch start
#iterations: 392
currently lose_sum: 502.87225008010864
time_elpased: 1.69
batch start
#iterations: 393
currently lose_sum: 505.0442477762699
time_elpased: 1.676
batch start
#iterations: 394
currently lose_sum: 503.4998052418232
time_elpased: 1.668
batch start
#iterations: 395
currently lose_sum: 505.13993725180626
time_elpased: 1.631
batch start
#iterations: 396
currently lose_sum: 503.3904309272766
time_elpased: 1.625
batch start
#iterations: 397
currently lose_sum: 503.3752530813217
time_elpased: 1.678
batch start
#iterations: 398
currently lose_sum: 503.5887862145901
time_elpased: 1.621
batch start
#iterations: 399
currently lose_sum: 502.95449525117874
time_elpased: 1.612
start validation test
0.107010309278
1.0
0.107010309278
0.193332091637
0
validation finish
acc: 0.326
pre: 1.000
rec: 0.326
F1: 0.492
auc: 0.000
