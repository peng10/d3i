start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 557.5096853375435
time_elpased: 1.845
batch start
#iterations: 1
currently lose_sum: 554.8702344298363
time_elpased: 1.771
batch start
#iterations: 2
currently lose_sum: 555.4015174508095
time_elpased: 1.782
batch start
#iterations: 3
currently lose_sum: 553.9261810183525
time_elpased: 1.786
batch start
#iterations: 4
currently lose_sum: 552.8715199828148
time_elpased: 1.773
batch start
#iterations: 5
currently lose_sum: 553.0056029558182
time_elpased: 1.77
batch start
#iterations: 6
currently lose_sum: 549.5515986680984
time_elpased: 1.76
batch start
#iterations: 7
currently lose_sum: 550.0198750495911
time_elpased: 1.786
batch start
#iterations: 8
currently lose_sum: 548.2092203497887
time_elpased: 1.789
batch start
#iterations: 9
currently lose_sum: 548.1842342615128
time_elpased: 1.774
batch start
#iterations: 10
currently lose_sum: 545.5767440795898
time_elpased: 1.762
batch start
#iterations: 11
currently lose_sum: 545.7575818300247
time_elpased: 1.783
batch start
#iterations: 12
currently lose_sum: 543.1631137728691
time_elpased: 1.773
batch start
#iterations: 13
currently lose_sum: 540.0545613765717
time_elpased: 1.768
batch start
#iterations: 14
currently lose_sum: 538.3375380635262
time_elpased: 1.775
batch start
#iterations: 15
currently lose_sum: 536.8135816454887
time_elpased: 1.77
batch start
#iterations: 16
currently lose_sum: 536.1049621105194
time_elpased: 1.77
batch start
#iterations: 17
currently lose_sum: 534.9732190668583
time_elpased: 1.772
batch start
#iterations: 18
currently lose_sum: 533.3061520159245
time_elpased: 1.781
batch start
#iterations: 19
currently lose_sum: 532.1838886737823
time_elpased: 1.791
start validation test
0.16587628866
1.0
0.16587628866
0.284552126625
0
validation finish
batch start
#iterations: 20
currently lose_sum: 531.9093390703201
time_elpased: 1.76
batch start
#iterations: 21
currently lose_sum: 532.9351171851158
time_elpased: 1.767
batch start
#iterations: 22
currently lose_sum: 531.5457446575165
time_elpased: 1.794
batch start
#iterations: 23
currently lose_sum: 529.9778678715229
time_elpased: 1.779
batch start
#iterations: 24
currently lose_sum: 528.6811512112617
time_elpased: 1.775
batch start
#iterations: 25
currently lose_sum: 528.2261129915714
time_elpased: 1.748
batch start
#iterations: 26
currently lose_sum: 529.225916236639
time_elpased: 1.785
batch start
#iterations: 27
currently lose_sum: 528.3095266819
time_elpased: 1.772
batch start
#iterations: 28
currently lose_sum: 525.7809994518757
time_elpased: 1.778
batch start
#iterations: 29
currently lose_sum: 527.1500201821327
time_elpased: 1.78
batch start
#iterations: 30
currently lose_sum: 525.7372932732105
time_elpased: 1.781
batch start
#iterations: 31
currently lose_sum: 526.2015191614628
time_elpased: 1.785
batch start
#iterations: 32
currently lose_sum: 526.8614310622215
time_elpased: 1.782
batch start
#iterations: 33
currently lose_sum: 525.5999142229557
time_elpased: 1.774
batch start
#iterations: 34
currently lose_sum: 524.8541778624058
time_elpased: 1.774
batch start
#iterations: 35
currently lose_sum: 522.5803262889385
time_elpased: 1.785
batch start
#iterations: 36
currently lose_sum: 524.0301982462406
time_elpased: 1.781
batch start
#iterations: 37
currently lose_sum: 521.6712927222252
time_elpased: 1.759
batch start
#iterations: 38
currently lose_sum: 522.4289399087429
time_elpased: 1.778
batch start
#iterations: 39
currently lose_sum: 523.1221684515476
time_elpased: 1.77
start validation test
0.0971134020619
1.0
0.0971134020619
0.177034392032
0
validation finish
batch start
#iterations: 40
currently lose_sum: 523.0846978724003
time_elpased: 1.771
batch start
#iterations: 41
currently lose_sum: 523.4720437526703
time_elpased: 1.78
batch start
#iterations: 42
currently lose_sum: 519.7558819949627
time_elpased: 1.789
batch start
#iterations: 43
currently lose_sum: 520.1935102045536
time_elpased: 1.755
batch start
#iterations: 44
currently lose_sum: 521.3822835385799
time_elpased: 1.794
batch start
#iterations: 45
currently lose_sum: 521.8136207163334
time_elpased: 1.785
batch start
#iterations: 46
currently lose_sum: 518.2580752670765
time_elpased: 1.786
batch start
#iterations: 47
currently lose_sum: 519.9909521639347
time_elpased: 1.773
batch start
#iterations: 48
currently lose_sum: 519.6358483433723
time_elpased: 1.761
batch start
#iterations: 49
currently lose_sum: 516.49742436409
time_elpased: 1.784
batch start
#iterations: 50
currently lose_sum: 518.6089638471603
time_elpased: 1.799
batch start
#iterations: 51
currently lose_sum: 518.6816317141056
time_elpased: 1.785
batch start
#iterations: 52
currently lose_sum: 518.9543917775154
time_elpased: 1.778
batch start
#iterations: 53
currently lose_sum: 519.0513601303101
time_elpased: 1.77
batch start
#iterations: 54
currently lose_sum: 517.4356049597263
time_elpased: 1.772
batch start
#iterations: 55
currently lose_sum: 518.4970631301403
time_elpased: 1.8
batch start
#iterations: 56
currently lose_sum: 519.0875465273857
time_elpased: 1.77
batch start
#iterations: 57
currently lose_sum: 518.3046597540379
time_elpased: 1.783
batch start
#iterations: 58
currently lose_sum: 517.9585792422295
time_elpased: 1.78
batch start
#iterations: 59
currently lose_sum: 518.1509563624859
time_elpased: 1.77
start validation test
0.188453608247
1.0
0.188453608247
0.317140874393
0
validation finish
batch start
#iterations: 60
currently lose_sum: 517.4121223688126
time_elpased: 1.775
batch start
#iterations: 61
currently lose_sum: 516.1994623243809
time_elpased: 1.78
batch start
#iterations: 62
currently lose_sum: 520.5870446264744
time_elpased: 1.76
batch start
#iterations: 63
currently lose_sum: 517.6782779991627
time_elpased: 1.768
batch start
#iterations: 64
currently lose_sum: 516.8381094634533
time_elpased: 1.757
batch start
#iterations: 65
currently lose_sum: 516.974397867918
time_elpased: 1.775
batch start
#iterations: 66
currently lose_sum: 516.518946915865
time_elpased: 1.766
batch start
#iterations: 67
currently lose_sum: 514.015720307827
time_elpased: 1.763
batch start
#iterations: 68
currently lose_sum: 516.6638671159744
time_elpased: 1.761
batch start
#iterations: 69
currently lose_sum: 518.6371469199657
time_elpased: 1.78
batch start
#iterations: 70
currently lose_sum: 513.9188852906227
time_elpased: 1.774
batch start
#iterations: 71
currently lose_sum: 516.1365282833576
time_elpased: 1.784
batch start
#iterations: 72
currently lose_sum: 517.2422495782375
time_elpased: 1.772
batch start
#iterations: 73
currently lose_sum: 515.63646748662
time_elpased: 1.773
batch start
#iterations: 74
currently lose_sum: 515.6900263726711
time_elpased: 1.779
batch start
#iterations: 75
currently lose_sum: 512.5425232052803
time_elpased: 1.777
batch start
#iterations: 76
currently lose_sum: 514.1064088046551
time_elpased: 1.776
batch start
#iterations: 77
currently lose_sum: 514.706640869379
time_elpased: 1.772
batch start
#iterations: 78
currently lose_sum: 514.6023253202438
time_elpased: 1.763
batch start
#iterations: 79
currently lose_sum: 516.5648932158947
time_elpased: 1.759
start validation test
0.148865979381
1.0
0.148865979381
0.259152907394
0
validation finish
batch start
#iterations: 80
currently lose_sum: 514.743664175272
time_elpased: 1.783
batch start
#iterations: 81
currently lose_sum: 512.4605461061001
time_elpased: 1.767
batch start
#iterations: 82
currently lose_sum: 515.457437902689
time_elpased: 1.782
batch start
#iterations: 83
currently lose_sum: 513.6754225790501
time_elpased: 1.779
batch start
#iterations: 84
currently lose_sum: 513.4215239286423
time_elpased: 1.782
batch start
#iterations: 85
currently lose_sum: 513.2354633510113
time_elpased: 1.795
batch start
#iterations: 86
currently lose_sum: 512.7344811558723
time_elpased: 1.774
batch start
#iterations: 87
currently lose_sum: 512.5293068885803
time_elpased: 1.785
batch start
#iterations: 88
currently lose_sum: 512.9710829854012
time_elpased: 1.769
batch start
#iterations: 89
currently lose_sum: 514.7722888588905
time_elpased: 1.776
batch start
#iterations: 90
currently lose_sum: 513.38257843256
time_elpased: 1.78
batch start
#iterations: 91
currently lose_sum: 512.5955730676651
time_elpased: 1.779
batch start
#iterations: 92
currently lose_sum: 513.4004184305668
time_elpased: 1.792
batch start
#iterations: 93
currently lose_sum: 512.9034028947353
time_elpased: 1.785
batch start
#iterations: 94
currently lose_sum: 512.0779934823513
time_elpased: 1.771
batch start
#iterations: 95
currently lose_sum: 512.989402115345
time_elpased: 1.779
batch start
#iterations: 96
currently lose_sum: 514.1009603142738
time_elpased: 1.773
batch start
#iterations: 97
currently lose_sum: 511.86658295989037
time_elpased: 1.792
batch start
#iterations: 98
currently lose_sum: 513.3907406926155
time_elpased: 1.768
batch start
#iterations: 99
currently lose_sum: 512.6168791353703
time_elpased: 1.789
start validation test
0.346701030928
1.0
0.346701030928
0.514889382225
0
validation finish
batch start
#iterations: 100
currently lose_sum: 512.686032563448
time_elpased: 1.777
batch start
#iterations: 101
currently lose_sum: 512.8281016945839
time_elpased: 1.79
batch start
#iterations: 102
currently lose_sum: 511.1395745277405
time_elpased: 1.779
batch start
#iterations: 103
currently lose_sum: 512.827275454998
time_elpased: 1.789
batch start
#iterations: 104
currently lose_sum: 512.085150629282
time_elpased: 1.772
batch start
#iterations: 105
currently lose_sum: 512.4664230644703
time_elpased: 1.783
batch start
#iterations: 106
currently lose_sum: 513.4124942421913
time_elpased: 1.787
batch start
#iterations: 107
currently lose_sum: 511.77909660339355
time_elpased: 1.771
batch start
#iterations: 108
currently lose_sum: 509.73974564671516
time_elpased: 1.769
batch start
#iterations: 109
currently lose_sum: 511.8490151464939
time_elpased: 1.769
batch start
#iterations: 110
currently lose_sum: 513.9175577759743
time_elpased: 1.778
batch start
#iterations: 111
currently lose_sum: 514.7642853856087
time_elpased: 1.766
batch start
#iterations: 112
currently lose_sum: 509.47442626953125
time_elpased: 1.774
batch start
#iterations: 113
currently lose_sum: 511.4187482893467
time_elpased: 1.785
batch start
#iterations: 114
currently lose_sum: 513.0683400034904
time_elpased: 1.77
batch start
#iterations: 115
currently lose_sum: 512.7405534088612
time_elpased: 1.777
batch start
#iterations: 116
currently lose_sum: 509.0063888132572
time_elpased: 1.775
batch start
#iterations: 117
currently lose_sum: 512.148059040308
time_elpased: 1.766
batch start
#iterations: 118
currently lose_sum: 510.62110552191734
time_elpased: 1.783
batch start
#iterations: 119
currently lose_sum: 510.6667982041836
time_elpased: 1.856
start validation test
0.429484536082
1.0
0.429484536082
0.600894273763
0
validation finish
batch start
#iterations: 120
currently lose_sum: 511.66892755031586
time_elpased: 1.79
batch start
#iterations: 121
currently lose_sum: 509.59745213389397
time_elpased: 1.77
batch start
#iterations: 122
currently lose_sum: 509.88845190405846
time_elpased: 1.787
batch start
#iterations: 123
currently lose_sum: 509.90443500876427
time_elpased: 1.789
batch start
#iterations: 124
currently lose_sum: 509.28709200024605
time_elpased: 1.767
batch start
#iterations: 125
currently lose_sum: 511.69045263528824
time_elpased: 1.811
batch start
#iterations: 126
currently lose_sum: 511.4159043431282
time_elpased: 1.784
batch start
#iterations: 127
currently lose_sum: 510.19533973932266
time_elpased: 1.791
batch start
#iterations: 128
currently lose_sum: 509.59568548202515
time_elpased: 1.782
batch start
#iterations: 129
currently lose_sum: 510.2402618229389
time_elpased: 1.773
batch start
#iterations: 130
currently lose_sum: 509.992271065712
time_elpased: 1.781
batch start
#iterations: 131
currently lose_sum: 511.7397438287735
time_elpased: 1.765
batch start
#iterations: 132
currently lose_sum: 508.77669179439545
time_elpased: 1.783
batch start
#iterations: 133
currently lose_sum: 510.4038819372654
time_elpased: 1.8
batch start
#iterations: 134
currently lose_sum: 509.97546193003654
time_elpased: 1.772
batch start
#iterations: 135
currently lose_sum: 508.58404049277306
time_elpased: 1.781
batch start
#iterations: 136
currently lose_sum: 509.15729358792305
time_elpased: 1.772
batch start
#iterations: 137
currently lose_sum: 507.87216225266457
time_elpased: 1.774
batch start
#iterations: 138
currently lose_sum: 509.19037249684334
time_elpased: 1.779
batch start
#iterations: 139
currently lose_sum: 508.9730179607868
time_elpased: 1.775
start validation test
0.155154639175
1.0
0.155154639175
0.268630075859
0
validation finish
batch start
#iterations: 140
currently lose_sum: 507.54440262913704
time_elpased: 1.768
batch start
#iterations: 141
currently lose_sum: 509.5135319828987
time_elpased: 1.776
batch start
#iterations: 142
currently lose_sum: 507.41510513424873
time_elpased: 1.788
batch start
#iterations: 143
currently lose_sum: 509.11990678310394
time_elpased: 1.786
batch start
#iterations: 144
currently lose_sum: 509.6635630428791
time_elpased: 1.776
batch start
#iterations: 145
currently lose_sum: 510.65173879265785
time_elpased: 1.788
batch start
#iterations: 146
currently lose_sum: 511.66440680623055
time_elpased: 1.778
batch start
#iterations: 147
currently lose_sum: 510.02716356515884
time_elpased: 1.779
batch start
#iterations: 148
currently lose_sum: 506.81806683540344
time_elpased: 1.782
batch start
#iterations: 149
currently lose_sum: 509.4104515016079
time_elpased: 1.784
batch start
#iterations: 150
currently lose_sum: 508.08766654133797
time_elpased: 1.76
batch start
#iterations: 151
currently lose_sum: 507.5138120353222
time_elpased: 1.779
batch start
#iterations: 152
currently lose_sum: 508.14825731515884
time_elpased: 1.779
batch start
#iterations: 153
currently lose_sum: 506.66023594141006
time_elpased: 1.771
batch start
#iterations: 154
currently lose_sum: 511.9270742237568
time_elpased: 1.794
batch start
#iterations: 155
currently lose_sum: 506.3742260336876
time_elpased: 1.781
batch start
#iterations: 156
currently lose_sum: 509.06300526857376
time_elpased: 1.785
batch start
#iterations: 157
currently lose_sum: 507.63747984170914
time_elpased: 1.787
batch start
#iterations: 158
currently lose_sum: 507.9575116932392
time_elpased: 1.776
batch start
#iterations: 159
currently lose_sum: 508.65886229276657
time_elpased: 1.766
start validation test
0.122371134021
1.0
0.122371134021
0.218058234592
0
validation finish
batch start
#iterations: 160
currently lose_sum: 510.8777394890785
time_elpased: 1.771
batch start
#iterations: 161
currently lose_sum: 506.55366122722626
time_elpased: 1.773
batch start
#iterations: 162
currently lose_sum: 507.47921457886696
time_elpased: 1.775
batch start
#iterations: 163
currently lose_sum: 507.30360329151154
time_elpased: 1.775
batch start
#iterations: 164
currently lose_sum: 508.60331705212593
time_elpased: 1.762
batch start
#iterations: 165
currently lose_sum: 509.85066971182823
time_elpased: 1.782
batch start
#iterations: 166
currently lose_sum: 509.739718824625
time_elpased: 1.769
batch start
#iterations: 167
currently lose_sum: 507.1172348856926
time_elpased: 1.794
batch start
#iterations: 168
currently lose_sum: 508.3868895173073
time_elpased: 1.778
batch start
#iterations: 169
currently lose_sum: 507.7252077162266
time_elpased: 1.782
batch start
#iterations: 170
currently lose_sum: 507.6054503917694
time_elpased: 1.785
batch start
#iterations: 171
currently lose_sum: 507.8415707349777
time_elpased: 1.782
batch start
#iterations: 172
currently lose_sum: 509.4682774543762
time_elpased: 1.768
batch start
#iterations: 173
currently lose_sum: 508.1722414493561
time_elpased: 1.755
batch start
#iterations: 174
currently lose_sum: 508.2969623208046
time_elpased: 1.795
batch start
#iterations: 175
currently lose_sum: 508.5056538283825
time_elpased: 1.779
batch start
#iterations: 176
currently lose_sum: 506.0523162484169
time_elpased: 1.778
batch start
#iterations: 177
currently lose_sum: 508.23355972766876
time_elpased: 1.776
batch start
#iterations: 178
currently lose_sum: 508.2842607796192
time_elpased: 1.779
batch start
#iterations: 179
currently lose_sum: 509.9769424498081
time_elpased: 1.781
start validation test
0.125257731959
1.0
0.125257731959
0.22262940907
0
validation finish
batch start
#iterations: 180
currently lose_sum: 507.1851087510586
time_elpased: 1.771
batch start
#iterations: 181
currently lose_sum: 508.8134474158287
time_elpased: 1.779
batch start
#iterations: 182
currently lose_sum: 507.6681431531906
time_elpased: 1.777
batch start
#iterations: 183
currently lose_sum: 508.3960815668106
time_elpased: 1.783
batch start
#iterations: 184
currently lose_sum: 507.1106970310211
time_elpased: 1.77
batch start
#iterations: 185
currently lose_sum: 507.5728237628937
time_elpased: 1.781
batch start
#iterations: 186
currently lose_sum: 507.8808528184891
time_elpased: 1.784
batch start
#iterations: 187
currently lose_sum: 509.03788861632347
time_elpased: 1.783
batch start
#iterations: 188
currently lose_sum: 507.3981292247772
time_elpased: 1.816
batch start
#iterations: 189
currently lose_sum: 508.3597711622715
time_elpased: 1.769
batch start
#iterations: 190
currently lose_sum: 506.89382991194725
time_elpased: 1.778
batch start
#iterations: 191
currently lose_sum: 506.133930593729
time_elpased: 1.778
batch start
#iterations: 192
currently lose_sum: 506.68863502144814
time_elpased: 1.782
batch start
#iterations: 193
currently lose_sum: 506.72258174419403
time_elpased: 1.776
batch start
#iterations: 194
currently lose_sum: 507.98303204774857
time_elpased: 1.777
batch start
#iterations: 195
currently lose_sum: 510.42427402734756
time_elpased: 1.796
batch start
#iterations: 196
currently lose_sum: 505.4662917852402
time_elpased: 1.768
batch start
#iterations: 197
currently lose_sum: 509.59625816345215
time_elpased: 1.773
batch start
#iterations: 198
currently lose_sum: 506.63369584083557
time_elpased: 1.776
batch start
#iterations: 199
currently lose_sum: 507.2805079817772
time_elpased: 1.771
start validation test
0.0942268041237
1.0
0.0942268041237
0.172225362728
0
validation finish
batch start
#iterations: 200
currently lose_sum: 505.9917941689491
time_elpased: 1.772
batch start
#iterations: 201
currently lose_sum: 509.4799992442131
time_elpased: 1.779
batch start
#iterations: 202
currently lose_sum: 505.70173835754395
time_elpased: 1.77
batch start
#iterations: 203
currently lose_sum: 506.03154188394547
time_elpased: 1.772
batch start
#iterations: 204
currently lose_sum: 505.9360547363758
time_elpased: 1.795
batch start
#iterations: 205
currently lose_sum: 507.2189056277275
time_elpased: 1.802
batch start
#iterations: 206
currently lose_sum: 510.19953522086143
time_elpased: 1.776
batch start
#iterations: 207
currently lose_sum: 506.1066636145115
time_elpased: 1.77
batch start
#iterations: 208
currently lose_sum: 505.5035136640072
time_elpased: 1.778
batch start
#iterations: 209
currently lose_sum: 506.8724711239338
time_elpased: 1.776
batch start
#iterations: 210
currently lose_sum: 506.500323086977
time_elpased: 1.78
batch start
#iterations: 211
currently lose_sum: 506.90816336870193
time_elpased: 1.786
batch start
#iterations: 212
currently lose_sum: 506.94429844617844
time_elpased: 1.767
batch start
#iterations: 213
currently lose_sum: 507.1523230075836
time_elpased: 1.779
batch start
#iterations: 214
currently lose_sum: 506.3499301970005
time_elpased: 1.769
batch start
#iterations: 215
currently lose_sum: 506.66999050974846
time_elpased: 1.774
batch start
#iterations: 216
currently lose_sum: 507.4100236296654
time_elpased: 1.799
batch start
#iterations: 217
currently lose_sum: 506.8084164559841
time_elpased: 1.773
batch start
#iterations: 218
currently lose_sum: 505.3342658877373
time_elpased: 1.782
batch start
#iterations: 219
currently lose_sum: 507.8400946557522
time_elpased: 1.765
start validation test
0.050824742268
1.0
0.050824742268
0.0967330520946
0
validation finish
batch start
#iterations: 220
currently lose_sum: 506.08407098054886
time_elpased: 1.773
batch start
#iterations: 221
currently lose_sum: 507.347231566906
time_elpased: 1.783
batch start
#iterations: 222
currently lose_sum: 504.96093395352364
time_elpased: 1.772
batch start
#iterations: 223
currently lose_sum: 506.3527161478996
time_elpased: 1.777
batch start
#iterations: 224
currently lose_sum: 506.6745416224003
time_elpased: 1.794
batch start
#iterations: 225
currently lose_sum: 508.0754859149456
time_elpased: 1.768
batch start
#iterations: 226
currently lose_sum: 506.674292832613
time_elpased: 1.792
batch start
#iterations: 227
currently lose_sum: 506.47944059967995
time_elpased: 1.771
batch start
#iterations: 228
currently lose_sum: 504.55214300751686
time_elpased: 1.776
batch start
#iterations: 229
currently lose_sum: 507.09113669395447
time_elpased: 1.773
batch start
#iterations: 230
currently lose_sum: 506.7075670659542
time_elpased: 1.779
batch start
#iterations: 231
currently lose_sum: 507.72705686092377
time_elpased: 1.778
batch start
#iterations: 232
currently lose_sum: 506.1211094260216
time_elpased: 1.764
batch start
#iterations: 233
currently lose_sum: 504.2492065727711
time_elpased: 1.783
batch start
#iterations: 234
currently lose_sum: 505.2863082885742
time_elpased: 1.793
batch start
#iterations: 235
currently lose_sum: 504.69043189287186
time_elpased: 1.774
batch start
#iterations: 236
currently lose_sum: 503.8842226564884
time_elpased: 1.765
batch start
#iterations: 237
currently lose_sum: 507.1205476820469
time_elpased: 1.782
batch start
#iterations: 238
currently lose_sum: 504.6865331232548
time_elpased: 1.764
batch start
#iterations: 239
currently lose_sum: 504.873166680336
time_elpased: 1.779
start validation test
0.0142268041237
1.0
0.0142268041237
0.0280544826184
0
validation finish
batch start
#iterations: 240
currently lose_sum: 505.7593923509121
time_elpased: 1.782
batch start
#iterations: 241
currently lose_sum: 504.7296761572361
time_elpased: 1.771
batch start
#iterations: 242
currently lose_sum: 504.83717000484467
time_elpased: 1.811
batch start
#iterations: 243
currently lose_sum: 504.77407747507095
time_elpased: 1.782
batch start
#iterations: 244
currently lose_sum: 506.1487975716591
time_elpased: 1.798
batch start
#iterations: 245
currently lose_sum: 505.37174940109253
time_elpased: 1.792
batch start
#iterations: 246
currently lose_sum: 504.44067361950874
time_elpased: 1.781
batch start
#iterations: 247
currently lose_sum: 504.79388588666916
time_elpased: 1.791
batch start
#iterations: 248
currently lose_sum: 504.916935890913
time_elpased: 1.784
batch start
#iterations: 249
currently lose_sum: 506.29116800427437
time_elpased: 1.768
batch start
#iterations: 250
currently lose_sum: 505.32534506917
time_elpased: 1.776
batch start
#iterations: 251
currently lose_sum: 506.72937858104706
time_elpased: 1.779
batch start
#iterations: 252
currently lose_sum: 507.44986429810524
time_elpased: 1.775
batch start
#iterations: 253
currently lose_sum: 505.1348404288292
time_elpased: 1.791
batch start
#iterations: 254
currently lose_sum: 508.04063096642494
time_elpased: 1.772
batch start
#iterations: 255
currently lose_sum: 503.70624819397926
time_elpased: 1.8
batch start
#iterations: 256
currently lose_sum: 507.6892455816269
time_elpased: 1.777
batch start
#iterations: 257
currently lose_sum: 504.4597018659115
time_elpased: 1.797
batch start
#iterations: 258
currently lose_sum: 504.4631150662899
time_elpased: 1.776
batch start
#iterations: 259
currently lose_sum: 505.7984613776207
time_elpased: 1.773
start validation test
0.219587628866
1.0
0.219587628866
0.360101437025
0
validation finish
batch start
#iterations: 260
currently lose_sum: 506.22930109500885
time_elpased: 1.795
batch start
#iterations: 261
currently lose_sum: 505.8065077364445
time_elpased: 1.796
batch start
#iterations: 262
currently lose_sum: 504.61055797338486
time_elpased: 1.773
batch start
#iterations: 263
currently lose_sum: 504.8978912830353
time_elpased: 1.781
batch start
#iterations: 264
currently lose_sum: 503.8063968718052
time_elpased: 1.796
batch start
#iterations: 265
currently lose_sum: 506.3990463614464
time_elpased: 1.778
batch start
#iterations: 266
currently lose_sum: 504.3693617284298
time_elpased: 1.782
batch start
#iterations: 267
currently lose_sum: 505.4535712003708
time_elpased: 1.773
batch start
#iterations: 268
currently lose_sum: 504.4031785428524
time_elpased: 1.79
batch start
#iterations: 269
currently lose_sum: 503.2006014883518
time_elpased: 1.849
batch start
#iterations: 270
currently lose_sum: 506.348986774683
time_elpased: 1.783
batch start
#iterations: 271
currently lose_sum: 502.6191358566284
time_elpased: 1.778
batch start
#iterations: 272
currently lose_sum: 504.4088585674763
time_elpased: 1.766
batch start
#iterations: 273
currently lose_sum: 504.95749548077583
time_elpased: 1.795
batch start
#iterations: 274
currently lose_sum: 506.54607850313187
time_elpased: 1.779
batch start
#iterations: 275
currently lose_sum: 503.13206991553307
time_elpased: 1.777
batch start
#iterations: 276
currently lose_sum: 503.89023596048355
time_elpased: 1.78
batch start
#iterations: 277
currently lose_sum: 505.2798635661602
time_elpased: 1.786
batch start
#iterations: 278
currently lose_sum: 503.7170269191265
time_elpased: 1.767
batch start
#iterations: 279
currently lose_sum: 506.7272718846798
time_elpased: 1.768
start validation test
0.184226804124
1.0
0.184226804124
0.31113432576
0
validation finish
batch start
#iterations: 280
currently lose_sum: 503.25964120030403
time_elpased: 1.781
batch start
#iterations: 281
currently lose_sum: 502.7932747602463
time_elpased: 1.79
batch start
#iterations: 282
currently lose_sum: 504.7239394187927
time_elpased: 1.788
batch start
#iterations: 283
currently lose_sum: 505.8168717622757
time_elpased: 1.772
batch start
#iterations: 284
currently lose_sum: 505.26536694169044
time_elpased: 1.787
batch start
#iterations: 285
currently lose_sum: 504.3916429877281
time_elpased: 1.779
batch start
#iterations: 286
currently lose_sum: 505.0894200205803
time_elpased: 1.787
batch start
#iterations: 287
currently lose_sum: 506.6353071331978
time_elpased: 1.785
batch start
#iterations: 288
currently lose_sum: 502.48947075009346
time_elpased: 1.777
batch start
#iterations: 289
currently lose_sum: 503.14435854554176
time_elpased: 1.788
batch start
#iterations: 290
currently lose_sum: 502.48312148451805
time_elpased: 1.777
batch start
#iterations: 291
currently lose_sum: 504.5396311879158
time_elpased: 1.775
batch start
#iterations: 292
currently lose_sum: 506.39560574293137
time_elpased: 1.8
batch start
#iterations: 293
currently lose_sum: 504.9477629363537
time_elpased: 1.779
batch start
#iterations: 294
currently lose_sum: 504.8056362271309
time_elpased: 1.771
batch start
#iterations: 295
currently lose_sum: 504.7090370953083
time_elpased: 1.778
batch start
#iterations: 296
currently lose_sum: 504.04773169755936
time_elpased: 1.772
batch start
#iterations: 297
currently lose_sum: 505.65186098217964
time_elpased: 1.779
batch start
#iterations: 298
currently lose_sum: 504.59552639722824
time_elpased: 1.773
batch start
#iterations: 299
currently lose_sum: 505.13254603743553
time_elpased: 1.776
start validation test
0.245979381443
1.0
0.245979381443
0.394837001489
0
validation finish
batch start
#iterations: 300
currently lose_sum: 503.8952053487301
time_elpased: 1.788
batch start
#iterations: 301
currently lose_sum: 503.540853112936
time_elpased: 1.777
batch start
#iterations: 302
currently lose_sum: 505.82034665346146
time_elpased: 1.772
batch start
#iterations: 303
currently lose_sum: 505.38805440068245
time_elpased: 1.774
batch start
#iterations: 304
currently lose_sum: 503.4103616774082
time_elpased: 1.782
batch start
#iterations: 305
currently lose_sum: 506.3562924563885
time_elpased: 1.785
batch start
#iterations: 306
currently lose_sum: 505.0298598110676
time_elpased: 1.801
batch start
#iterations: 307
currently lose_sum: 506.9950522482395
time_elpased: 1.782
batch start
#iterations: 308
currently lose_sum: 505.8257125914097
time_elpased: 1.77
batch start
#iterations: 309
currently lose_sum: 504.50682535767555
time_elpased: 1.791
batch start
#iterations: 310
currently lose_sum: 502.8181305527687
time_elpased: 1.777
batch start
#iterations: 311
currently lose_sum: 504.45187509059906
time_elpased: 1.784
batch start
#iterations: 312
currently lose_sum: 505.045010894537
time_elpased: 1.784
batch start
#iterations: 313
currently lose_sum: 504.6434375047684
time_elpased: 1.784
batch start
#iterations: 314
currently lose_sum: 502.63884723186493
time_elpased: 1.787
batch start
#iterations: 315
currently lose_sum: 504.31446465849876
time_elpased: 1.778
batch start
#iterations: 316
currently lose_sum: 505.0775756239891
time_elpased: 1.794
batch start
#iterations: 317
currently lose_sum: 502.67081385850906
time_elpased: 1.783
batch start
#iterations: 318
currently lose_sum: 506.0164914429188
time_elpased: 1.78
batch start
#iterations: 319
currently lose_sum: 502.7878819704056
time_elpased: 1.773
start validation test
0.0570103092784
1.0
0.0570103092784
0.107870867063
0
validation finish
batch start
#iterations: 320
currently lose_sum: 505.1732539534569
time_elpased: 1.785
batch start
#iterations: 321
currently lose_sum: 504.661568492651
time_elpased: 1.783
batch start
#iterations: 322
currently lose_sum: 503.21458142995834
time_elpased: 1.794
batch start
#iterations: 323
currently lose_sum: 503.507635474205
time_elpased: 1.788
batch start
#iterations: 324
currently lose_sum: 505.952289044857
time_elpased: 1.769
batch start
#iterations: 325
currently lose_sum: 502.56652572751045
time_elpased: 1.774
batch start
#iterations: 326
currently lose_sum: 504.78669252991676
time_elpased: 1.775
batch start
#iterations: 327
currently lose_sum: 503.57001584768295
time_elpased: 1.792
batch start
#iterations: 328
currently lose_sum: 503.5012395977974
time_elpased: 1.775
batch start
#iterations: 329
currently lose_sum: 504.42757070064545
time_elpased: 1.793
batch start
#iterations: 330
currently lose_sum: 504.8525787293911
time_elpased: 1.781
batch start
#iterations: 331
currently lose_sum: 505.4571088254452
time_elpased: 1.786
batch start
#iterations: 332
currently lose_sum: 504.17276868224144
time_elpased: 1.781
batch start
#iterations: 333
currently lose_sum: 505.0502879321575
time_elpased: 1.784
batch start
#iterations: 334
currently lose_sum: 502.6785975396633
time_elpased: 1.795
batch start
#iterations: 335
currently lose_sum: 503.171989351511
time_elpased: 1.776
batch start
#iterations: 336
currently lose_sum: 504.2216513156891
time_elpased: 1.772
batch start
#iterations: 337
currently lose_sum: 502.7784687578678
time_elpased: 1.777
batch start
#iterations: 338
currently lose_sum: 501.7267690896988
time_elpased: 1.779
batch start
#iterations: 339
currently lose_sum: 504.4490848183632
time_elpased: 1.784
start validation test
0.147628865979
1.0
0.147628865979
0.257276320517
0
validation finish
batch start
#iterations: 340
currently lose_sum: 502.17355409264565
time_elpased: 1.778
batch start
#iterations: 341
currently lose_sum: 504.10506677627563
time_elpased: 1.817
batch start
#iterations: 342
currently lose_sum: 504.62333104014397
time_elpased: 1.783
batch start
#iterations: 343
currently lose_sum: 505.4965631365776
time_elpased: 1.766
batch start
#iterations: 344
currently lose_sum: 505.7656754553318
time_elpased: 1.787
batch start
#iterations: 345
currently lose_sum: 504.36770236492157
time_elpased: 1.789
batch start
#iterations: 346
currently lose_sum: 503.8940035700798
time_elpased: 1.776
batch start
#iterations: 347
currently lose_sum: 504.22860848903656
time_elpased: 1.769
batch start
#iterations: 348
currently lose_sum: 505.41342291235924
time_elpased: 1.788
batch start
#iterations: 349
currently lose_sum: 503.71836519241333
time_elpased: 1.787
batch start
#iterations: 350
currently lose_sum: 504.22449630498886
time_elpased: 1.791
batch start
#iterations: 351
currently lose_sum: 503.51705437898636
time_elpased: 1.803
batch start
#iterations: 352
currently lose_sum: 503.88965675234795
time_elpased: 1.772
batch start
#iterations: 353
currently lose_sum: 505.66560915112495
time_elpased: 1.806
batch start
#iterations: 354
currently lose_sum: 504.6696177124977
time_elpased: 1.776
batch start
#iterations: 355
currently lose_sum: 504.17419305443764
time_elpased: 1.782
batch start
#iterations: 356
currently lose_sum: 501.5748356580734
time_elpased: 1.775
batch start
#iterations: 357
currently lose_sum: 504.13488763570786
time_elpased: 1.782
batch start
#iterations: 358
currently lose_sum: 504.4906478226185
time_elpased: 1.776
batch start
#iterations: 359
currently lose_sum: 501.63834288716316
time_elpased: 1.798
start validation test
0.137628865979
1.0
0.137628865979
0.241957408246
0
validation finish
batch start
#iterations: 360
currently lose_sum: 501.99847054481506
time_elpased: 1.778
batch start
#iterations: 361
currently lose_sum: 502.28018683195114
time_elpased: 1.768
batch start
#iterations: 362
currently lose_sum: 502.6445481479168
time_elpased: 1.788
batch start
#iterations: 363
currently lose_sum: 504.09692829847336
time_elpased: 1.79
batch start
#iterations: 364
currently lose_sum: 504.2118348777294
time_elpased: 1.777
batch start
#iterations: 365
currently lose_sum: 503.28516551852226
time_elpased: 1.803
batch start
#iterations: 366
currently lose_sum: 504.14135283231735
time_elpased: 1.786
batch start
#iterations: 367
currently lose_sum: 504.24376767873764
time_elpased: 1.774
batch start
#iterations: 368
currently lose_sum: 502.7858757376671
time_elpased: 1.792
batch start
#iterations: 369
currently lose_sum: 501.99874410033226
time_elpased: 1.819
batch start
#iterations: 370
currently lose_sum: 501.4959682226181
time_elpased: 1.775
batch start
#iterations: 371
currently lose_sum: 503.05098193883896
time_elpased: 1.783
batch start
#iterations: 372
currently lose_sum: 502.9111445248127
time_elpased: 1.785
batch start
#iterations: 373
currently lose_sum: 501.6460180878639
time_elpased: 1.777
batch start
#iterations: 374
currently lose_sum: 504.29598808288574
time_elpased: 1.796
batch start
#iterations: 375
currently lose_sum: 503.38274362683296
time_elpased: 1.781
batch start
#iterations: 376
currently lose_sum: 503.33803963661194
time_elpased: 1.788
batch start
#iterations: 377
currently lose_sum: 502.0351083278656
time_elpased: 1.791
batch start
#iterations: 378
currently lose_sum: 505.8948038518429
time_elpased: 1.791
batch start
#iterations: 379
currently lose_sum: 503.19524428248405
time_elpased: 1.797
start validation test
0.194329896907
1.0
0.194329896907
0.325420802762
0
validation finish
batch start
#iterations: 380
currently lose_sum: 501.8855856359005
time_elpased: 1.781
batch start
#iterations: 381
currently lose_sum: 501.90566650032997
time_elpased: 1.775
batch start
#iterations: 382
currently lose_sum: 501.69374337792397
time_elpased: 1.784
batch start
#iterations: 383
currently lose_sum: 501.68470177054405
time_elpased: 1.778
batch start
#iterations: 384
currently lose_sum: 500.95345428586006
time_elpased: 1.792
batch start
#iterations: 385
currently lose_sum: 501.26118582487106
time_elpased: 1.792
batch start
#iterations: 386
currently lose_sum: 502.4451978504658
time_elpased: 1.773
batch start
#iterations: 387
currently lose_sum: 501.6164586544037
time_elpased: 1.787
batch start
#iterations: 388
currently lose_sum: 501.45759961009026
time_elpased: 1.791
batch start
#iterations: 389
currently lose_sum: 504.09025490283966
time_elpased: 1.784
batch start
#iterations: 390
currently lose_sum: 502.25458058714867
time_elpased: 1.78
batch start
#iterations: 391
currently lose_sum: 503.0548101067543
time_elpased: 1.792
batch start
#iterations: 392
currently lose_sum: 502.66669288277626
time_elpased: 1.777
batch start
#iterations: 393
currently lose_sum: 502.3812603056431
time_elpased: 1.783
batch start
#iterations: 394
currently lose_sum: 502.1677529513836
time_elpased: 1.795
batch start
#iterations: 395
currently lose_sum: 502.13867515325546
time_elpased: 1.779
batch start
#iterations: 396
currently lose_sum: 503.00882962346077
time_elpased: 1.767
batch start
#iterations: 397
currently lose_sum: 503.86194732785225
time_elpased: 1.776
batch start
#iterations: 398
currently lose_sum: 502.1796301603317
time_elpased: 1.787
batch start
#iterations: 399
currently lose_sum: 500.93055391311646
time_elpased: 1.777
start validation test
0.12587628866
1.0
0.12587628866
0.223605896896
0
validation finish
acc: 0.424
pre: 1.000
rec: 0.424
F1: 0.595
auc: 0.000
