start to construct graph
graph construct over
58300
epochs start
batch start
#iterations: 0
currently lose_sum: 402.15614753961563
time_elpased: 0.942
batch start
#iterations: 1
currently lose_sum: 396.8482554554939
time_elpased: 0.915
batch start
#iterations: 2
currently lose_sum: 393.86679857969284
time_elpased: 0.902
batch start
#iterations: 3
currently lose_sum: 391.7893970608711
time_elpased: 0.903
batch start
#iterations: 4
currently lose_sum: 390.9456986784935
time_elpased: 0.913
batch start
#iterations: 5
currently lose_sum: 390.0787683725357
time_elpased: 0.904
batch start
#iterations: 6
currently lose_sum: 389.1780168414116
time_elpased: 0.91
batch start
#iterations: 7
currently lose_sum: 388.49260437488556
time_elpased: 0.91
batch start
#iterations: 8
currently lose_sum: 387.9066950082779
time_elpased: 0.922
batch start
#iterations: 9
currently lose_sum: 388.26821625232697
time_elpased: 0.914
batch start
#iterations: 10
currently lose_sum: 387.5634871125221
time_elpased: 0.923
batch start
#iterations: 11
currently lose_sum: 386.90348559617996
time_elpased: 0.911
batch start
#iterations: 12
currently lose_sum: 386.7411840558052
time_elpased: 0.924
batch start
#iterations: 13
currently lose_sum: 387.5666180253029
time_elpased: 0.912
batch start
#iterations: 14
currently lose_sum: 384.69418638944626
time_elpased: 0.922
batch start
#iterations: 15
currently lose_sum: 386.14899158477783
time_elpased: 0.918
batch start
#iterations: 16
currently lose_sum: 385.870566368103
time_elpased: 0.915
batch start
#iterations: 17
currently lose_sum: 386.7511458992958
time_elpased: 0.914
batch start
#iterations: 18
currently lose_sum: 384.8644223809242
time_elpased: 0.918
batch start
#iterations: 19
currently lose_sum: 386.0863146185875
time_elpased: 0.905
start validation test
0.41381443299
1.0
0.41381443299
0.585387195567
0
validation finish
batch start
#iterations: 20
currently lose_sum: 385.9018686413765
time_elpased: 0.919
batch start
#iterations: 21
currently lose_sum: 385.7470615506172
time_elpased: 0.909
batch start
#iterations: 22
currently lose_sum: 385.4537133574486
time_elpased: 0.913
batch start
#iterations: 23
currently lose_sum: 386.78940510749817
time_elpased: 0.934
batch start
#iterations: 24
currently lose_sum: 385.8095620274544
time_elpased: 0.911
batch start
#iterations: 25
currently lose_sum: 385.6839689016342
time_elpased: 0.916
batch start
#iterations: 26
currently lose_sum: 384.47182351350784
time_elpased: 0.916
batch start
#iterations: 27
currently lose_sum: 385.2010719180107
time_elpased: 0.908
batch start
#iterations: 28
currently lose_sum: 384.5709801912308
time_elpased: 0.924
batch start
#iterations: 29
currently lose_sum: 385.1787973642349
time_elpased: 0.924
batch start
#iterations: 30
currently lose_sum: 385.06348127126694
time_elpased: 0.911
batch start
#iterations: 31
currently lose_sum: 384.5232165455818
time_elpased: 0.923
batch start
#iterations: 32
currently lose_sum: 386.0500885248184
time_elpased: 0.911
batch start
#iterations: 33
currently lose_sum: 385.44826847314835
time_elpased: 0.915
batch start
#iterations: 34
currently lose_sum: 385.10759568214417
time_elpased: 0.913
batch start
#iterations: 35
currently lose_sum: 386.79872500896454
time_elpased: 0.933
batch start
#iterations: 36
currently lose_sum: 384.9638931155205
time_elpased: 0.913
batch start
#iterations: 37
currently lose_sum: 384.1043373942375
time_elpased: 0.917
batch start
#iterations: 38
currently lose_sum: 385.3146920800209
time_elpased: 0.911
batch start
#iterations: 39
currently lose_sum: 385.418882727623
time_elpased: 0.923
start validation test
0.426701030928
1.0
0.426701030928
0.598164607269
0
validation finish
batch start
#iterations: 40
currently lose_sum: 384.60055005550385
time_elpased: 0.91
batch start
#iterations: 41
currently lose_sum: 384.70291888713837
time_elpased: 0.919
batch start
#iterations: 42
currently lose_sum: 384.18483048677444
time_elpased: 0.917
batch start
#iterations: 43
currently lose_sum: 384.70826625823975
time_elpased: 0.916
batch start
#iterations: 44
currently lose_sum: 385.1257806420326
time_elpased: 0.915
batch start
#iterations: 45
currently lose_sum: 384.2686987519264
time_elpased: 0.917
batch start
#iterations: 46
currently lose_sum: 384.1087071299553
time_elpased: 0.928
batch start
#iterations: 47
currently lose_sum: 385.42777132987976
time_elpased: 0.912
batch start
#iterations: 48
currently lose_sum: 384.4032641649246
time_elpased: 0.929
batch start
#iterations: 49
currently lose_sum: 384.95768171548843
time_elpased: 0.919
batch start
#iterations: 50
currently lose_sum: 383.11805111169815
time_elpased: 0.913
batch start
#iterations: 51
currently lose_sum: 384.12521517276764
time_elpased: 0.913
batch start
#iterations: 52
currently lose_sum: 385.3989362716675
time_elpased: 0.92
batch start
#iterations: 53
currently lose_sum: 385.38374722003937
time_elpased: 0.917
batch start
#iterations: 54
currently lose_sum: 385.2805404663086
time_elpased: 0.915
batch start
#iterations: 55
currently lose_sum: 383.6776288151741
time_elpased: 0.914
batch start
#iterations: 56
currently lose_sum: 384.540222465992
time_elpased: 0.922
batch start
#iterations: 57
currently lose_sum: 384.00240528583527
time_elpased: 0.926
batch start
#iterations: 58
currently lose_sum: 383.7461197376251
time_elpased: 0.926
batch start
#iterations: 59
currently lose_sum: 384.42331498861313
time_elpased: 0.913
start validation test
0.588041237113
1.0
0.588041237113
0.740586860556
0
validation finish
batch start
#iterations: 60
currently lose_sum: 384.06173527240753
time_elpased: 0.914
batch start
#iterations: 61
currently lose_sum: 384.8141144514084
time_elpased: 0.919
batch start
#iterations: 62
currently lose_sum: 384.64900439977646
time_elpased: 0.918
batch start
#iterations: 63
currently lose_sum: 383.8116771578789
time_elpased: 0.914
batch start
#iterations: 64
currently lose_sum: 384.2113219499588
time_elpased: 0.912
batch start
#iterations: 65
currently lose_sum: 384.04380971193314
time_elpased: 0.922
batch start
#iterations: 66
currently lose_sum: 383.6916108727455
time_elpased: 0.92
batch start
#iterations: 67
currently lose_sum: 383.74172872304916
time_elpased: 0.915
batch start
#iterations: 68
currently lose_sum: 384.6609612107277
time_elpased: 0.916
batch start
#iterations: 69
currently lose_sum: 384.09125447273254
time_elpased: 0.91
batch start
#iterations: 70
currently lose_sum: 383.9829617738724
time_elpased: 0.92
batch start
#iterations: 71
currently lose_sum: 384.3683387041092
time_elpased: 0.913
batch start
#iterations: 72
currently lose_sum: 384.1896448135376
time_elpased: 0.907
batch start
#iterations: 73
currently lose_sum: 384.3032949566841
time_elpased: 0.92
batch start
#iterations: 74
currently lose_sum: 384.20309084653854
time_elpased: 0.917
batch start
#iterations: 75
currently lose_sum: 383.3870042562485
time_elpased: 0.913
batch start
#iterations: 76
currently lose_sum: 383.9911367893219
time_elpased: 0.913
batch start
#iterations: 77
currently lose_sum: 383.85939675569534
time_elpased: 0.922
batch start
#iterations: 78
currently lose_sum: 384.23588424921036
time_elpased: 0.913
batch start
#iterations: 79
currently lose_sum: 383.935791015625
time_elpased: 0.917
start validation test
0.602164948454
1.0
0.602164948454
0.751689080497
0
validation finish
batch start
#iterations: 80
currently lose_sum: 383.2020317912102
time_elpased: 0.916
batch start
#iterations: 81
currently lose_sum: 384.96668553352356
time_elpased: 0.931
batch start
#iterations: 82
currently lose_sum: 384.1124772429466
time_elpased: 0.918
batch start
#iterations: 83
currently lose_sum: 383.64420634508133
time_elpased: 0.92
batch start
#iterations: 84
currently lose_sum: 384.47670781612396
time_elpased: 0.931
batch start
#iterations: 85
currently lose_sum: 384.20441603660583
time_elpased: 0.921
batch start
#iterations: 86
currently lose_sum: 384.5212446451187
time_elpased: 0.914
batch start
#iterations: 87
currently lose_sum: 383.73250502347946
time_elpased: 0.916
batch start
#iterations: 88
currently lose_sum: 383.1965349316597
time_elpased: 0.914
batch start
#iterations: 89
currently lose_sum: 384.58978593349457
time_elpased: 0.913
batch start
#iterations: 90
currently lose_sum: 384.5781201720238
time_elpased: 0.909
batch start
#iterations: 91
currently lose_sum: 384.10180151462555
time_elpased: 0.908
batch start
#iterations: 92
currently lose_sum: 383.8971027135849
time_elpased: 0.916
batch start
#iterations: 93
currently lose_sum: 384.336610019207
time_elpased: 0.92
batch start
#iterations: 94
currently lose_sum: 383.2426958680153
time_elpased: 0.922
batch start
#iterations: 95
currently lose_sum: 383.02423936128616
time_elpased: 0.921
batch start
#iterations: 96
currently lose_sum: 383.8289814591408
time_elpased: 0.914
batch start
#iterations: 97
currently lose_sum: 384.0828487277031
time_elpased: 0.924
batch start
#iterations: 98
currently lose_sum: 383.2099639773369
time_elpased: 0.917
batch start
#iterations: 99
currently lose_sum: 384.18634831905365
time_elpased: 0.94
start validation test
0.57
1.0
0.57
0.726114649682
0
validation finish
batch start
#iterations: 100
currently lose_sum: 384.24857276678085
time_elpased: 0.916
batch start
#iterations: 101
currently lose_sum: 383.3124819993973
time_elpased: 0.932
batch start
#iterations: 102
currently lose_sum: 383.2658957839012
time_elpased: 0.931
batch start
#iterations: 103
currently lose_sum: 383.9166573882103
time_elpased: 0.931
batch start
#iterations: 104
currently lose_sum: 384.3740166425705
time_elpased: 0.909
batch start
#iterations: 105
currently lose_sum: 383.2400036454201
time_elpased: 0.916
batch start
#iterations: 106
currently lose_sum: 384.3915230035782
time_elpased: 0.921
batch start
#iterations: 107
currently lose_sum: 383.67067927122116
time_elpased: 0.922
batch start
#iterations: 108
currently lose_sum: 383.8845756649971
time_elpased: 0.904
batch start
#iterations: 109
currently lose_sum: 382.8434108495712
time_elpased: 0.917
batch start
#iterations: 110
currently lose_sum: 383.743749499321
time_elpased: 0.947
batch start
#iterations: 111
currently lose_sum: 382.81828755140305
time_elpased: 0.916
batch start
#iterations: 112
currently lose_sum: 383.28083050251007
time_elpased: 0.928
batch start
#iterations: 113
currently lose_sum: 384.122695684433
time_elpased: 0.908
batch start
#iterations: 114
currently lose_sum: 383.69854575395584
time_elpased: 0.916
batch start
#iterations: 115
currently lose_sum: 383.3193658590317
time_elpased: 0.931
batch start
#iterations: 116
currently lose_sum: 384.5983048081398
time_elpased: 0.914
batch start
#iterations: 117
currently lose_sum: 382.89409548044205
time_elpased: 0.912
batch start
#iterations: 118
currently lose_sum: 382.4085686802864
time_elpased: 0.918
batch start
#iterations: 119
currently lose_sum: 384.1716189980507
time_elpased: 0.917
start validation test
0.562783505155
1.0
0.562783505155
0.720232205291
0
validation finish
batch start
#iterations: 120
currently lose_sum: 384.31226229667664
time_elpased: 0.915
batch start
#iterations: 121
currently lose_sum: 383.01309180259705
time_elpased: 0.914
batch start
#iterations: 122
currently lose_sum: 383.0181453824043
time_elpased: 0.931
batch start
#iterations: 123
currently lose_sum: 382.99849677085876
time_elpased: 0.915
batch start
#iterations: 124
currently lose_sum: 382.6264744400978
time_elpased: 0.918
batch start
#iterations: 125
currently lose_sum: 383.45225644111633
time_elpased: 0.914
batch start
#iterations: 126
currently lose_sum: 383.94427847862244
time_elpased: 0.912
batch start
#iterations: 127
currently lose_sum: 384.22969049215317
time_elpased: 0.927
batch start
#iterations: 128
currently lose_sum: 383.16142559051514
time_elpased: 0.914
batch start
#iterations: 129
currently lose_sum: 383.75637251138687
time_elpased: 0.913
batch start
#iterations: 130
currently lose_sum: 383.15137296915054
time_elpased: 0.914
batch start
#iterations: 131
currently lose_sum: 383.5660485625267
time_elpased: 0.925
batch start
#iterations: 132
currently lose_sum: 383.0968677997589
time_elpased: 0.93
batch start
#iterations: 133
currently lose_sum: 383.72591882944107
time_elpased: 0.912
batch start
#iterations: 134
currently lose_sum: 383.9855777621269
time_elpased: 0.927
batch start
#iterations: 135
currently lose_sum: 383.5279392004013
time_elpased: 0.922
batch start
#iterations: 136
currently lose_sum: 383.6997619867325
time_elpased: 0.915
batch start
#iterations: 137
currently lose_sum: 382.8296622633934
time_elpased: 0.92
batch start
#iterations: 138
currently lose_sum: 384.58035027980804
time_elpased: 0.92
batch start
#iterations: 139
currently lose_sum: 383.27312445640564
time_elpased: 0.923
start validation test
0.61412371134
1.0
0.61412371134
0.760937599796
0
validation finish
batch start
#iterations: 140
currently lose_sum: 383.280690073967
time_elpased: 0.928
batch start
#iterations: 141
currently lose_sum: 383.8949872851372
time_elpased: 0.925
batch start
#iterations: 142
currently lose_sum: 383.325778901577
time_elpased: 0.919
batch start
#iterations: 143
currently lose_sum: 383.85519230365753
time_elpased: 0.919
batch start
#iterations: 144
currently lose_sum: 383.6704141497612
time_elpased: 0.918
batch start
#iterations: 145
currently lose_sum: 382.68582409620285
time_elpased: 0.923
batch start
#iterations: 146
currently lose_sum: 382.98780500888824
time_elpased: 0.922
batch start
#iterations: 147
currently lose_sum: 383.8261417746544
time_elpased: 0.932
batch start
#iterations: 148
currently lose_sum: 384.0909289121628
time_elpased: 0.916
batch start
#iterations: 149
currently lose_sum: 383.40806460380554
time_elpased: 0.927
batch start
#iterations: 150
currently lose_sum: 383.0417715907097
time_elpased: 0.919
batch start
#iterations: 151
currently lose_sum: 382.8461045026779
time_elpased: 0.918
batch start
#iterations: 152
currently lose_sum: 382.9901247024536
time_elpased: 0.924
batch start
#iterations: 153
currently lose_sum: 384.37852972745895
time_elpased: 0.927
batch start
#iterations: 154
currently lose_sum: 383.6293942928314
time_elpased: 0.921
batch start
#iterations: 155
currently lose_sum: 383.68944478034973
time_elpased: 0.915
batch start
#iterations: 156
currently lose_sum: 384.26705008745193
time_elpased: 0.913
batch start
#iterations: 157
currently lose_sum: 382.8930172920227
time_elpased: 0.935
batch start
#iterations: 158
currently lose_sum: 384.02970576286316
time_elpased: 0.926
batch start
#iterations: 159
currently lose_sum: 384.8967542052269
time_elpased: 0.922
start validation test
0.612783505155
1.0
0.612783505155
0.75990795193
0
validation finish
batch start
#iterations: 160
currently lose_sum: 383.80022245645523
time_elpased: 0.936
batch start
#iterations: 161
currently lose_sum: 383.75836312770844
time_elpased: 0.923
batch start
#iterations: 162
currently lose_sum: 383.7291437983513
time_elpased: 0.917
batch start
#iterations: 163
currently lose_sum: 383.0878960490227
time_elpased: 0.915
batch start
#iterations: 164
currently lose_sum: 382.9625269174576
time_elpased: 0.916
batch start
#iterations: 165
currently lose_sum: 382.80088263750076
time_elpased: 0.929
batch start
#iterations: 166
currently lose_sum: 384.0128606557846
time_elpased: 0.922
batch start
#iterations: 167
currently lose_sum: 383.36976146698
time_elpased: 0.918
batch start
#iterations: 168
currently lose_sum: 383.6250337958336
time_elpased: 0.915
batch start
#iterations: 169
currently lose_sum: 382.8496999144554
time_elpased: 0.935
batch start
#iterations: 170
currently lose_sum: 383.122038602829
time_elpased: 0.92
batch start
#iterations: 171
currently lose_sum: 383.21715092658997
time_elpased: 0.934
batch start
#iterations: 172
currently lose_sum: 383.94989013671875
time_elpased: 0.926
batch start
#iterations: 173
currently lose_sum: 383.48437172174454
time_elpased: 0.914
batch start
#iterations: 174
currently lose_sum: 382.72032529115677
time_elpased: 0.91
batch start
#iterations: 175
currently lose_sum: 384.1016091108322
time_elpased: 0.924
batch start
#iterations: 176
currently lose_sum: 383.21377873420715
time_elpased: 0.919
batch start
#iterations: 177
currently lose_sum: 382.5889813899994
time_elpased: 0.914
batch start
#iterations: 178
currently lose_sum: 384.255657017231
time_elpased: 0.92
batch start
#iterations: 179
currently lose_sum: 383.7045620083809
time_elpased: 0.931
start validation test
0.588762886598
1.0
0.588762886598
0.741158912465
0
validation finish
batch start
#iterations: 180
currently lose_sum: 382.90810328722
time_elpased: 0.914
batch start
#iterations: 181
currently lose_sum: 382.936784863472
time_elpased: 0.918
batch start
#iterations: 182
currently lose_sum: 383.1202753186226
time_elpased: 0.936
batch start
#iterations: 183
currently lose_sum: 383.15046685934067
time_elpased: 0.908
batch start
#iterations: 184
currently lose_sum: 383.8912967443466
time_elpased: 0.92
batch start
#iterations: 185
currently lose_sum: 382.7961195707321
time_elpased: 0.916
batch start
#iterations: 186
currently lose_sum: 383.258488714695
time_elpased: 0.902
batch start
#iterations: 187
currently lose_sum: 382.2162951827049
time_elpased: 0.919
batch start
#iterations: 188
currently lose_sum: 384.2753406763077
time_elpased: 0.928
batch start
#iterations: 189
currently lose_sum: 383.6547822356224
time_elpased: 0.932
batch start
#iterations: 190
currently lose_sum: 382.1586861014366
time_elpased: 0.919
batch start
#iterations: 191
currently lose_sum: 383.15939873456955
time_elpased: 0.918
batch start
#iterations: 192
currently lose_sum: 383.45328122377396
time_elpased: 0.92
batch start
#iterations: 193
currently lose_sum: 384.55256539583206
time_elpased: 0.944
batch start
#iterations: 194
currently lose_sum: 383.0177036523819
time_elpased: 0.921
batch start
#iterations: 195
currently lose_sum: 382.7901887893677
time_elpased: 0.912
batch start
#iterations: 196
currently lose_sum: 384.6317020058632
time_elpased: 0.929
batch start
#iterations: 197
currently lose_sum: 383.17284458875656
time_elpased: 0.935
batch start
#iterations: 198
currently lose_sum: 383.9982663989067
time_elpased: 0.908
batch start
#iterations: 199
currently lose_sum: 382.9672280550003
time_elpased: 0.92
start validation test
0.547731958763
1.0
0.547731958763
0.70778658496
0
validation finish
batch start
#iterations: 200
currently lose_sum: 384.1099624633789
time_elpased: 0.923
batch start
#iterations: 201
currently lose_sum: 382.39868944883347
time_elpased: 0.921
batch start
#iterations: 202
currently lose_sum: 382.7184293270111
time_elpased: 0.92
batch start
#iterations: 203
currently lose_sum: 382.0882656574249
time_elpased: 0.913
batch start
#iterations: 204
currently lose_sum: 384.07902443408966
time_elpased: 0.926
batch start
#iterations: 205
currently lose_sum: 382.4626789689064
time_elpased: 0.916
batch start
#iterations: 206
currently lose_sum: 382.57947063446045
time_elpased: 0.923
batch start
#iterations: 207
currently lose_sum: 384.05031180381775
time_elpased: 0.917
batch start
#iterations: 208
currently lose_sum: 383.19792008399963
time_elpased: 0.912
batch start
#iterations: 209
currently lose_sum: 383.3828201889992
time_elpased: 0.919
batch start
#iterations: 210
currently lose_sum: 382.5824521780014
time_elpased: 0.913
batch start
#iterations: 211
currently lose_sum: 383.3839369416237
time_elpased: 0.924
batch start
#iterations: 212
currently lose_sum: 383.93582022190094
time_elpased: 0.919
batch start
#iterations: 213
currently lose_sum: 383.86275416612625
time_elpased: 0.92
batch start
#iterations: 214
currently lose_sum: 383.8605763912201
time_elpased: 0.917
batch start
#iterations: 215
currently lose_sum: 383.960974752903
time_elpased: 0.93
batch start
#iterations: 216
currently lose_sum: 383.5013293623924
time_elpased: 0.918
batch start
#iterations: 217
currently lose_sum: 383.0133391022682
time_elpased: 0.918
batch start
#iterations: 218
currently lose_sum: 382.6596192717552
time_elpased: 0.925
batch start
#iterations: 219
currently lose_sum: 384.04847383499146
time_elpased: 0.915
start validation test
0.564020618557
1.0
0.564020618557
0.721244479599
0
validation finish
batch start
#iterations: 220
currently lose_sum: 383.4166491627693
time_elpased: 0.922
batch start
#iterations: 221
currently lose_sum: 383.8692542910576
time_elpased: 0.923
batch start
#iterations: 222
currently lose_sum: 383.3044753074646
time_elpased: 0.913
batch start
#iterations: 223
currently lose_sum: 383.07511419057846
time_elpased: 0.929
batch start
#iterations: 224
currently lose_sum: 383.6580403447151
time_elpased: 0.919
batch start
#iterations: 225
currently lose_sum: 382.7555938363075
time_elpased: 0.909
batch start
#iterations: 226
currently lose_sum: 383.78989696502686
time_elpased: 0.92
batch start
#iterations: 227
currently lose_sum: 382.98371881246567
time_elpased: 0.933
batch start
#iterations: 228
currently lose_sum: 382.9940502643585
time_elpased: 0.916
batch start
#iterations: 229
currently lose_sum: 382.0342320203781
time_elpased: 0.92
batch start
#iterations: 230
currently lose_sum: 383.8025243282318
time_elpased: 0.917
batch start
#iterations: 231
currently lose_sum: 383.4433256983757
time_elpased: 0.924
batch start
#iterations: 232
currently lose_sum: 383.26870626211166
time_elpased: 0.909
batch start
#iterations: 233
currently lose_sum: 383.892658829689
time_elpased: 0.922
batch start
#iterations: 234
currently lose_sum: 383.277881026268
time_elpased: 0.929
batch start
#iterations: 235
currently lose_sum: 382.6719731092453
time_elpased: 0.917
batch start
#iterations: 236
currently lose_sum: 383.2989789843559
time_elpased: 0.922
batch start
#iterations: 237
currently lose_sum: 382.7906962633133
time_elpased: 0.923
batch start
#iterations: 238
currently lose_sum: 383.45239812135696
time_elpased: 0.924
batch start
#iterations: 239
currently lose_sum: 382.5492868423462
time_elpased: 0.919
start validation test
0.566701030928
1.0
0.566701030928
0.723432256366
0
validation finish
batch start
#iterations: 240
currently lose_sum: 382.3188797235489
time_elpased: 0.92
batch start
#iterations: 241
currently lose_sum: 382.28575283288956
time_elpased: 0.923
batch start
#iterations: 242
currently lose_sum: 381.6787717938423
time_elpased: 0.922
batch start
#iterations: 243
currently lose_sum: 383.46336603164673
time_elpased: 0.931
batch start
#iterations: 244
currently lose_sum: 384.22486889362335
time_elpased: 0.935
batch start
#iterations: 245
currently lose_sum: 382.84802401065826
time_elpased: 0.923
batch start
#iterations: 246
currently lose_sum: 382.8338894844055
time_elpased: 0.916
batch start
#iterations: 247
currently lose_sum: 383.80386078357697
time_elpased: 0.915
batch start
#iterations: 248
currently lose_sum: 384.0801993012428
time_elpased: 0.921
batch start
#iterations: 249
currently lose_sum: 383.67134445905685
time_elpased: 0.916
batch start
#iterations: 250
currently lose_sum: 383.3227695822716
time_elpased: 0.935
batch start
#iterations: 251
currently lose_sum: 384.3588271141052
time_elpased: 0.926
batch start
#iterations: 252
currently lose_sum: 384.06045109033585
time_elpased: 0.908
batch start
#iterations: 253
currently lose_sum: 383.1742975115776
time_elpased: 0.918
batch start
#iterations: 254
currently lose_sum: 384.1448163986206
time_elpased: 0.919
batch start
#iterations: 255
currently lose_sum: 384.5493102669716
time_elpased: 0.919
batch start
#iterations: 256
currently lose_sum: 382.9682434797287
time_elpased: 0.922
batch start
#iterations: 257
currently lose_sum: 383.4902206659317
time_elpased: 0.919
batch start
#iterations: 258
currently lose_sum: 384.077489733696
time_elpased: 0.936
batch start
#iterations: 259
currently lose_sum: 383.0238990187645
time_elpased: 0.916
start validation test
0.59793814433
1.0
0.59793814433
0.748387096774
0
validation finish
batch start
#iterations: 260
currently lose_sum: 383.4272018671036
time_elpased: 0.925
batch start
#iterations: 261
currently lose_sum: 382.2827466726303
time_elpased: 0.926
batch start
#iterations: 262
currently lose_sum: 383.8247756958008
time_elpased: 0.926
batch start
#iterations: 263
currently lose_sum: 384.983118057251
time_elpased: 0.921
batch start
#iterations: 264
currently lose_sum: 383.1850701570511
time_elpased: 0.923
batch start
#iterations: 265
currently lose_sum: 383.0320131778717
time_elpased: 0.914
batch start
#iterations: 266
currently lose_sum: 383.7952048778534
time_elpased: 0.921
batch start
#iterations: 267
currently lose_sum: 383.1468383073807
time_elpased: 0.921
batch start
#iterations: 268
currently lose_sum: 382.648784160614
time_elpased: 0.916
batch start
#iterations: 269
currently lose_sum: 383.5129935145378
time_elpased: 0.929
batch start
#iterations: 270
currently lose_sum: 382.5599141716957
time_elpased: 0.916
batch start
#iterations: 271
currently lose_sum: 383.35559779405594
time_elpased: 0.928
batch start
#iterations: 272
currently lose_sum: 382.9346170425415
time_elpased: 0.927
batch start
#iterations: 273
currently lose_sum: 383.711155295372
time_elpased: 0.916
batch start
#iterations: 274
currently lose_sum: 383.6200524568558
time_elpased: 0.917
batch start
#iterations: 275
currently lose_sum: 384.8725538253784
time_elpased: 0.931
batch start
#iterations: 276
currently lose_sum: 382.42210149765015
time_elpased: 0.923
batch start
#iterations: 277
currently lose_sum: 383.8240889310837
time_elpased: 0.931
batch start
#iterations: 278
currently lose_sum: 382.8890089392662
time_elpased: 0.927
batch start
#iterations: 279
currently lose_sum: 383.20067822933197
time_elpased: 0.919
start validation test
0.544536082474
1.0
0.544536082474
0.705112802029
0
validation finish
batch start
#iterations: 280
currently lose_sum: 382.7029241323471
time_elpased: 0.926
batch start
#iterations: 281
currently lose_sum: 383.8959017395973
time_elpased: 0.909
batch start
#iterations: 282
currently lose_sum: 383.99313175678253
time_elpased: 0.932
batch start
#iterations: 283
currently lose_sum: 383.1853374838829
time_elpased: 0.913
batch start
#iterations: 284
currently lose_sum: 383.34602558612823
time_elpased: 0.916
batch start
#iterations: 285
currently lose_sum: 382.16781693696976
time_elpased: 0.927
batch start
#iterations: 286
currently lose_sum: 383.01642352342606
time_elpased: 0.922
batch start
#iterations: 287
currently lose_sum: 382.5695892572403
time_elpased: 0.918
batch start
#iterations: 288
currently lose_sum: 383.36852610111237
time_elpased: 0.928
batch start
#iterations: 289
currently lose_sum: 383.0726692080498
time_elpased: 0.922
batch start
#iterations: 290
currently lose_sum: 382.5601291656494
time_elpased: 0.923
batch start
#iterations: 291
currently lose_sum: 382.8670986890793
time_elpased: 0.915
batch start
#iterations: 292
currently lose_sum: 382.91551572084427
time_elpased: 0.926
batch start
#iterations: 293
currently lose_sum: 384.38681292533875
time_elpased: 0.926
batch start
#iterations: 294
currently lose_sum: 382.8624809384346
time_elpased: 0.919
batch start
#iterations: 295
currently lose_sum: 382.6261405944824
time_elpased: 0.912
batch start
#iterations: 296
currently lose_sum: 382.6044678092003
time_elpased: 0.919
batch start
#iterations: 297
currently lose_sum: 383.2736585736275
time_elpased: 0.923
batch start
#iterations: 298
currently lose_sum: 382.85123151540756
time_elpased: 0.915
batch start
#iterations: 299
currently lose_sum: 381.85251969099045
time_elpased: 0.919
start validation test
0.590103092784
1.0
0.590103092784
0.742219917012
0
validation finish
batch start
#iterations: 300
currently lose_sum: 382.1328318119049
time_elpased: 0.925
batch start
#iterations: 301
currently lose_sum: 383.1574614048004
time_elpased: 0.918
batch start
#iterations: 302
currently lose_sum: 384.2938379049301
time_elpased: 0.927
batch start
#iterations: 303
currently lose_sum: 385.0647547841072
time_elpased: 0.918
batch start
#iterations: 304
currently lose_sum: 383.3007797598839
time_elpased: 0.922
batch start
#iterations: 305
currently lose_sum: 384.13085275888443
time_elpased: 0.915
batch start
#iterations: 306
currently lose_sum: 382.4935467839241
time_elpased: 0.93
batch start
#iterations: 307
currently lose_sum: 382.7750426530838
time_elpased: 0.914
batch start
#iterations: 308
currently lose_sum: 382.8173957467079
time_elpased: 0.92
batch start
#iterations: 309
currently lose_sum: 383.39517372846603
time_elpased: 0.925
batch start
#iterations: 310
currently lose_sum: 381.87342780828476
time_elpased: 0.926
batch start
#iterations: 311
currently lose_sum: 384.0680181980133
time_elpased: 0.92
batch start
#iterations: 312
currently lose_sum: 382.99153566360474
time_elpased: 0.917
batch start
#iterations: 313
currently lose_sum: 382.3658254146576
time_elpased: 0.925
batch start
#iterations: 314
currently lose_sum: 383.2530464529991
time_elpased: 0.924
batch start
#iterations: 315
currently lose_sum: 383.32637333869934
time_elpased: 0.945
batch start
#iterations: 316
currently lose_sum: 383.2222479581833
time_elpased: 0.917
batch start
#iterations: 317
currently lose_sum: 382.7882724404335
time_elpased: 0.92
batch start
#iterations: 318
currently lose_sum: 383.80606013536453
time_elpased: 0.924
batch start
#iterations: 319
currently lose_sum: 382.6821403503418
time_elpased: 0.923
start validation test
0.599278350515
1.0
0.599278350515
0.749435956939
0
validation finish
batch start
#iterations: 320
currently lose_sum: 382.97441470623016
time_elpased: 0.915
batch start
#iterations: 321
currently lose_sum: 383.25283336639404
time_elpased: 0.927
batch start
#iterations: 322
currently lose_sum: 383.785853266716
time_elpased: 0.915
batch start
#iterations: 323
currently lose_sum: 383.3400997519493
time_elpased: 0.933
batch start
#iterations: 324
currently lose_sum: 383.1298898458481
time_elpased: 0.938
batch start
#iterations: 325
currently lose_sum: 383.7197430729866
time_elpased: 0.91
batch start
#iterations: 326
currently lose_sum: 383.3030142188072
time_elpased: 0.914
batch start
#iterations: 327
currently lose_sum: 382.69156897068024
time_elpased: 0.925
batch start
#iterations: 328
currently lose_sum: 382.72896188497543
time_elpased: 0.928
batch start
#iterations: 329
currently lose_sum: 382.39712142944336
time_elpased: 0.934
batch start
#iterations: 330
currently lose_sum: 382.68448692560196
time_elpased: 0.919
batch start
#iterations: 331
currently lose_sum: 383.017527282238
time_elpased: 0.921
batch start
#iterations: 332
currently lose_sum: 382.86912417411804
time_elpased: 0.913
batch start
#iterations: 333
currently lose_sum: 383.6590017080307
time_elpased: 0.921
batch start
#iterations: 334
currently lose_sum: 383.69236809015274
time_elpased: 0.92
batch start
#iterations: 335
currently lose_sum: 382.5851032137871
time_elpased: 0.938
batch start
#iterations: 336
currently lose_sum: 383.47175616025925
time_elpased: 0.928
batch start
#iterations: 337
currently lose_sum: 382.4784332513809
time_elpased: 0.907
batch start
#iterations: 338
currently lose_sum: 384.0368838906288
time_elpased: 0.933
batch start
#iterations: 339
currently lose_sum: 383.8401029706001
time_elpased: 0.914
start validation test
0.574639175258
1.0
0.574639175258
0.729867749116
0
validation finish
batch start
#iterations: 340
currently lose_sum: 383.68799036741257
time_elpased: 0.921
batch start
#iterations: 341
currently lose_sum: 383.5398018360138
time_elpased: 0.921
batch start
#iterations: 342
currently lose_sum: 383.1662366390228
time_elpased: 0.918
batch start
#iterations: 343
currently lose_sum: 382.92556768655777
time_elpased: 0.919
batch start
#iterations: 344
currently lose_sum: 383.6786739230156
time_elpased: 0.936
batch start
#iterations: 345
currently lose_sum: 383.268495619297
time_elpased: 0.921
batch start
#iterations: 346
currently lose_sum: 383.3183544278145
time_elpased: 0.916
batch start
#iterations: 347
currently lose_sum: 383.1233617067337
time_elpased: 0.938
batch start
#iterations: 348
currently lose_sum: 381.4145430326462
time_elpased: 0.915
batch start
#iterations: 349
currently lose_sum: 382.785096347332
time_elpased: 0.92
batch start
#iterations: 350
currently lose_sum: 383.10195714235306
time_elpased: 0.921
batch start
#iterations: 351
currently lose_sum: 383.6155099272728
time_elpased: 0.928
batch start
#iterations: 352
currently lose_sum: 383.92475789785385
time_elpased: 0.915
batch start
#iterations: 353
currently lose_sum: 382.7778582572937
time_elpased: 0.915
batch start
#iterations: 354
currently lose_sum: 382.55053013563156
time_elpased: 0.918
batch start
#iterations: 355
currently lose_sum: 382.6472749710083
time_elpased: 0.921
batch start
#iterations: 356
currently lose_sum: 382.6999742984772
time_elpased: 0.928
batch start
#iterations: 357
currently lose_sum: 383.4657281637192
time_elpased: 0.916
batch start
#iterations: 358
currently lose_sum: 383.2466216683388
time_elpased: 0.908
batch start
#iterations: 359
currently lose_sum: 381.9256963133812
time_elpased: 0.928
start validation test
0.555051546392
1.0
0.555051546392
0.713869000265
0
validation finish
batch start
#iterations: 360
currently lose_sum: 383.0063323378563
time_elpased: 0.925
batch start
#iterations: 361
currently lose_sum: 382.85254311561584
time_elpased: 0.906
batch start
#iterations: 362
currently lose_sum: 382.5777022242546
time_elpased: 0.912
batch start
#iterations: 363
currently lose_sum: 384.21889930963516
time_elpased: 0.93
batch start
#iterations: 364
currently lose_sum: 382.6802269220352
time_elpased: 0.924
batch start
#iterations: 365
currently lose_sum: 382.7784418463707
time_elpased: 0.915
batch start
#iterations: 366
currently lose_sum: 384.0006155371666
time_elpased: 0.924
batch start
#iterations: 367
currently lose_sum: 383.9845551252365
time_elpased: 0.915
batch start
#iterations: 368
currently lose_sum: 383.2104569673538
time_elpased: 0.929
batch start
#iterations: 369
currently lose_sum: 382.20111978054047
time_elpased: 0.916
batch start
#iterations: 370
currently lose_sum: 381.614011824131
time_elpased: 0.919
batch start
#iterations: 371
currently lose_sum: 383.55996787548065
time_elpased: 0.923
batch start
#iterations: 372
currently lose_sum: 382.6905850172043
time_elpased: 0.915
batch start
#iterations: 373
currently lose_sum: 383.9306262731552
time_elpased: 0.909
batch start
#iterations: 374
currently lose_sum: 383.2304844260216
time_elpased: 0.922
batch start
#iterations: 375
currently lose_sum: 382.0586448311806
time_elpased: 0.921
batch start
#iterations: 376
currently lose_sum: 382.3393123149872
time_elpased: 0.933
batch start
#iterations: 377
currently lose_sum: 383.26504892110825
time_elpased: 0.922
batch start
#iterations: 378
currently lose_sum: 382.7960957288742
time_elpased: 0.915
batch start
#iterations: 379
currently lose_sum: 383.66749185323715
time_elpased: 0.932
start validation test
0.572680412371
1.0
0.572680412371
0.728285807932
0
validation finish
batch start
#iterations: 380
currently lose_sum: 383.32373958826065
time_elpased: 0.913
batch start
#iterations: 381
currently lose_sum: 383.82367837429047
time_elpased: 0.921
batch start
#iterations: 382
currently lose_sum: 382.9056353569031
time_elpased: 0.944
batch start
#iterations: 383
currently lose_sum: 382.7745966911316
time_elpased: 0.914
batch start
#iterations: 384
currently lose_sum: 382.10914373397827
time_elpased: 0.919
batch start
#iterations: 385
currently lose_sum: 382.74116736650467
time_elpased: 0.923
batch start
#iterations: 386
currently lose_sum: 384.25701224803925
time_elpased: 0.924
batch start
#iterations: 387
currently lose_sum: 382.72242772579193
time_elpased: 0.916
batch start
#iterations: 388
currently lose_sum: 382.7599550485611
time_elpased: 0.915
batch start
#iterations: 389
currently lose_sum: 382.8500226140022
time_elpased: 0.914
batch start
#iterations: 390
currently lose_sum: 383.0032178759575
time_elpased: 0.924
batch start
#iterations: 391
currently lose_sum: 382.3262468576431
time_elpased: 0.911
batch start
#iterations: 392
currently lose_sum: 381.96357107162476
time_elpased: 0.914
batch start
#iterations: 393
currently lose_sum: 383.38650488853455
time_elpased: 0.927
batch start
#iterations: 394
currently lose_sum: 382.3005518913269
time_elpased: 0.931
batch start
#iterations: 395
currently lose_sum: 383.5069484114647
time_elpased: 0.918
batch start
#iterations: 396
currently lose_sum: 383.1468920111656
time_elpased: 0.924
batch start
#iterations: 397
currently lose_sum: 382.7326674461365
time_elpased: 0.93
batch start
#iterations: 398
currently lose_sum: 383.3851932287216
time_elpased: 0.917
batch start
#iterations: 399
currently lose_sum: 382.50352174043655
time_elpased: 0.92
start validation test
0.584845360825
1.0
0.584845360825
0.738047225655
0
validation finish
acc: 0.604
pre: 1.000
rec: 0.604
F1: 0.753
auc: 0.000
