start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 564.242909014225
time_elpased: 1.647
batch start
#iterations: 1
currently lose_sum: 547.4133141040802
time_elpased: 1.641
batch start
#iterations: 2
currently lose_sum: 541.093454003334
time_elpased: 1.62
batch start
#iterations: 3
currently lose_sum: 536.422901570797
time_elpased: 1.609
batch start
#iterations: 4
currently lose_sum: 533.9972521662712
time_elpased: 1.632
batch start
#iterations: 5
currently lose_sum: 534.0939021706581
time_elpased: 1.634
batch start
#iterations: 6
currently lose_sum: 529.8104218542576
time_elpased: 1.605
batch start
#iterations: 7
currently lose_sum: 530.2546119689941
time_elpased: 1.629
batch start
#iterations: 8
currently lose_sum: 528.9643417298794
time_elpased: 1.665
batch start
#iterations: 9
currently lose_sum: 528.4112750589848
time_elpased: 1.622
batch start
#iterations: 10
currently lose_sum: 529.3164884150028
time_elpased: 1.637
batch start
#iterations: 11
currently lose_sum: 530.8465805053711
time_elpased: 1.614
batch start
#iterations: 12
currently lose_sum: 528.0853707790375
time_elpased: 1.614
batch start
#iterations: 13
currently lose_sum: 528.491787135601
time_elpased: 1.698
batch start
#iterations: 14
currently lose_sum: 525.0550145804882
time_elpased: 1.655
batch start
#iterations: 15
currently lose_sum: 525.6659110784531
time_elpased: 1.62
batch start
#iterations: 16
currently lose_sum: 527.1410291194916
time_elpased: 1.612
batch start
#iterations: 17
currently lose_sum: 525.5784803330898
time_elpased: 1.655
batch start
#iterations: 18
currently lose_sum: 525.7383309006691
time_elpased: 1.676
batch start
#iterations: 19
currently lose_sum: 524.5374976098537
time_elpased: 1.698
start validation test
0.150309278351
1.0
0.150309278351
0.261337157197
0
validation finish
batch start
#iterations: 20
currently lose_sum: 526.3686090707779
time_elpased: 1.622
batch start
#iterations: 21
currently lose_sum: 527.4265216588974
time_elpased: 1.655
batch start
#iterations: 22
currently lose_sum: 526.5738006234169
time_elpased: 1.693
batch start
#iterations: 23
currently lose_sum: 524.7666147351265
time_elpased: 1.692
batch start
#iterations: 24
currently lose_sum: 522.7235188782215
time_elpased: 1.709
batch start
#iterations: 25
currently lose_sum: 524.6970515847206
time_elpased: 1.635
batch start
#iterations: 26
currently lose_sum: 524.2382131814957
time_elpased: 1.652
batch start
#iterations: 27
currently lose_sum: 523.8153642416
time_elpased: 1.694
batch start
#iterations: 28
currently lose_sum: 523.2469734251499
time_elpased: 1.621
batch start
#iterations: 29
currently lose_sum: 523.800643414259
time_elpased: 1.681
batch start
#iterations: 30
currently lose_sum: 522.5614302754402
time_elpased: 1.631
batch start
#iterations: 31
currently lose_sum: 522.5700016915798
time_elpased: 1.612
batch start
#iterations: 32
currently lose_sum: 525.0640084147453
time_elpased: 1.638
batch start
#iterations: 33
currently lose_sum: 521.7600858211517
time_elpased: 1.625
batch start
#iterations: 34
currently lose_sum: 523.4044180810452
time_elpased: 1.692
batch start
#iterations: 35
currently lose_sum: 521.8575049042702
time_elpased: 1.7
batch start
#iterations: 36
currently lose_sum: 523.038957118988
time_elpased: 1.672
batch start
#iterations: 37
currently lose_sum: 521.5767679214478
time_elpased: 1.624
batch start
#iterations: 38
currently lose_sum: 522.6296043694019
time_elpased: 1.622
batch start
#iterations: 39
currently lose_sum: 523.3120246231556
time_elpased: 1.652
start validation test
0.175979381443
1.0
0.175979381443
0.299289909705
0
validation finish
batch start
#iterations: 40
currently lose_sum: 521.9168340265751
time_elpased: 1.639
batch start
#iterations: 41
currently lose_sum: 523.1361410617828
time_elpased: 1.707
batch start
#iterations: 42
currently lose_sum: 521.0327905416489
time_elpased: 1.65
batch start
#iterations: 43
currently lose_sum: 520.5797164142132
time_elpased: 1.605
batch start
#iterations: 44
currently lose_sum: 521.1658994555473
time_elpased: 1.645
batch start
#iterations: 45
currently lose_sum: 522.2404761612415
time_elpased: 1.645
batch start
#iterations: 46
currently lose_sum: 520.7370870411396
time_elpased: 1.652
batch start
#iterations: 47
currently lose_sum: 522.1248790025711
time_elpased: 1.701
batch start
#iterations: 48
currently lose_sum: 519.9799308180809
time_elpased: 1.632
batch start
#iterations: 49
currently lose_sum: 520.8292674124241
time_elpased: 1.673
batch start
#iterations: 50
currently lose_sum: 522.9362378120422
time_elpased: 1.664
batch start
#iterations: 51
currently lose_sum: 521.517338514328
time_elpased: 1.699
batch start
#iterations: 52
currently lose_sum: 522.2057124078274
time_elpased: 1.714
batch start
#iterations: 53
currently lose_sum: 521.0520147681236
time_elpased: 1.661
batch start
#iterations: 54
currently lose_sum: 519.6320540606976
time_elpased: 1.628
batch start
#iterations: 55
currently lose_sum: 521.046368420124
time_elpased: 1.648
batch start
#iterations: 56
currently lose_sum: 521.5879243910313
time_elpased: 1.617
batch start
#iterations: 57
currently lose_sum: 519.6951387226582
time_elpased: 1.607
batch start
#iterations: 58
currently lose_sum: 519.3202651441097
time_elpased: 1.702
batch start
#iterations: 59
currently lose_sum: 518.7484233379364
time_elpased: 1.664
start validation test
0.197216494845
1.0
0.197216494845
0.329458365625
0
validation finish
batch start
#iterations: 60
currently lose_sum: 519.7711740732193
time_elpased: 1.688
batch start
#iterations: 61
currently lose_sum: 521.0326347053051
time_elpased: 1.628
batch start
#iterations: 62
currently lose_sum: 521.0431740581989
time_elpased: 1.619
batch start
#iterations: 63
currently lose_sum: 521.0298941731453
time_elpased: 1.689
batch start
#iterations: 64
currently lose_sum: 519.9326894879341
time_elpased: 1.688
batch start
#iterations: 65
currently lose_sum: 520.7667669653893
time_elpased: 1.699
batch start
#iterations: 66
currently lose_sum: 519.8796266019344
time_elpased: 1.66
batch start
#iterations: 67
currently lose_sum: 519.8064042627811
time_elpased: 1.631
batch start
#iterations: 68
currently lose_sum: 522.7162297964096
time_elpased: 1.632
batch start
#iterations: 69
currently lose_sum: 521.3701367378235
time_elpased: 1.638
batch start
#iterations: 70
currently lose_sum: 520.0450091660023
time_elpased: 1.676
batch start
#iterations: 71
currently lose_sum: 519.7340502738953
time_elpased: 1.617
batch start
#iterations: 72
currently lose_sum: 520.4452493488789
time_elpased: 1.615
batch start
#iterations: 73
currently lose_sum: 520.2047141492367
time_elpased: 1.616
batch start
#iterations: 74
currently lose_sum: 520.3457900881767
time_elpased: 1.648
batch start
#iterations: 75
currently lose_sum: 519.7506282329559
time_elpased: 1.695
batch start
#iterations: 76
currently lose_sum: 521.0733450651169
time_elpased: 1.631
batch start
#iterations: 77
currently lose_sum: 520.0937106311321
time_elpased: 1.704
batch start
#iterations: 78
currently lose_sum: 519.1290016770363
time_elpased: 1.687
batch start
#iterations: 79
currently lose_sum: 520.7131258547306
time_elpased: 1.638
start validation test
0.207216494845
1.0
0.207216494845
0.343296327925
0
validation finish
batch start
#iterations: 80
currently lose_sum: 519.7655591666698
time_elpased: 1.625
batch start
#iterations: 81
currently lose_sum: 519.6749926805496
time_elpased: 1.619
batch start
#iterations: 82
currently lose_sum: 520.1592121720314
time_elpased: 1.693
batch start
#iterations: 83
currently lose_sum: 519.8284256160259
time_elpased: 1.72
batch start
#iterations: 84
currently lose_sum: 519.7726432085037
time_elpased: 1.669
batch start
#iterations: 85
currently lose_sum: 520.0496972799301
time_elpased: 1.691
batch start
#iterations: 86
currently lose_sum: 519.5128493607044
time_elpased: 1.629
batch start
#iterations: 87
currently lose_sum: 517.7060370147228
time_elpased: 1.65
batch start
#iterations: 88
currently lose_sum: 518.972781509161
time_elpased: 1.674
batch start
#iterations: 89
currently lose_sum: 520.302997469902
time_elpased: 1.702
batch start
#iterations: 90
currently lose_sum: 518.2428250014782
time_elpased: 1.635
batch start
#iterations: 91
currently lose_sum: 517.2293140888214
time_elpased: 1.696
batch start
#iterations: 92
currently lose_sum: 519.7341751456261
time_elpased: 1.633
batch start
#iterations: 93
currently lose_sum: 519.8546417057514
time_elpased: 1.621
batch start
#iterations: 94
currently lose_sum: 517.606987118721
time_elpased: 1.641
batch start
#iterations: 95
currently lose_sum: 519.0320308506489
time_elpased: 1.689
batch start
#iterations: 96
currently lose_sum: 521.0488618016243
time_elpased: 1.685
batch start
#iterations: 97
currently lose_sum: 518.9413806200027
time_elpased: 1.628
batch start
#iterations: 98
currently lose_sum: 519.3267714977264
time_elpased: 1.611
batch start
#iterations: 99
currently lose_sum: 519.8085117042065
time_elpased: 1.631
start validation test
0.0605154639175
1.0
0.0605154639175
0.114124623311
0
validation finish
batch start
#iterations: 100
currently lose_sum: 519.8678349554539
time_elpased: 1.615
batch start
#iterations: 101
currently lose_sum: 517.5530633628368
time_elpased: 1.624
batch start
#iterations: 102
currently lose_sum: 516.3681537508965
time_elpased: 1.699
batch start
#iterations: 103
currently lose_sum: 518.7292068898678
time_elpased: 1.671
batch start
#iterations: 104
currently lose_sum: 519.7747380435467
time_elpased: 1.633
batch start
#iterations: 105
currently lose_sum: 519.1141530573368
time_elpased: 1.684
batch start
#iterations: 106
currently lose_sum: 519.6402055025101
time_elpased: 1.666
batch start
#iterations: 107
currently lose_sum: 520.1174768507481
time_elpased: 1.671
batch start
#iterations: 108
currently lose_sum: 518.5933116972446
time_elpased: 1.721
batch start
#iterations: 109
currently lose_sum: 519.5537316203117
time_elpased: 1.651
batch start
#iterations: 110
currently lose_sum: 517.5054186880589
time_elpased: 1.712
batch start
#iterations: 111
currently lose_sum: 521.3984033465385
time_elpased: 1.624
batch start
#iterations: 112
currently lose_sum: 517.8288120925426
time_elpased: 1.648
batch start
#iterations: 113
currently lose_sum: 518.331719815731
time_elpased: 1.722
batch start
#iterations: 114
currently lose_sum: 519.6052476167679
time_elpased: 1.639
batch start
#iterations: 115
currently lose_sum: 518.4278850257397
time_elpased: 1.693
batch start
#iterations: 116
currently lose_sum: 518.3344965875149
time_elpased: 1.688
batch start
#iterations: 117
currently lose_sum: 517.6695959568024
time_elpased: 1.687
batch start
#iterations: 118
currently lose_sum: 519.980439811945
time_elpased: 1.634
batch start
#iterations: 119
currently lose_sum: 518.8232027888298
time_elpased: 1.635
start validation test
0.132680412371
1.0
0.132680412371
0.234276872668
0
validation finish
batch start
#iterations: 120
currently lose_sum: 518.8749421238899
time_elpased: 1.699
batch start
#iterations: 121
currently lose_sum: 518.157678514719
time_elpased: 1.681
batch start
#iterations: 122
currently lose_sum: 518.2577577829361
time_elpased: 1.621
batch start
#iterations: 123
currently lose_sum: 519.2394749522209
time_elpased: 1.688
batch start
#iterations: 124
currently lose_sum: 518.958222836256
time_elpased: 1.685
batch start
#iterations: 125
currently lose_sum: 520.2280001938343
time_elpased: 1.707
batch start
#iterations: 126
currently lose_sum: 518.4319569170475
time_elpased: 1.617
batch start
#iterations: 127
currently lose_sum: 520.3708281815052
time_elpased: 1.658
batch start
#iterations: 128
currently lose_sum: 519.4807659685612
time_elpased: 1.707
batch start
#iterations: 129
currently lose_sum: 519.8507589399815
time_elpased: 1.624
batch start
#iterations: 130
currently lose_sum: 518.435405164957
time_elpased: 1.64
batch start
#iterations: 131
currently lose_sum: 518.8991990685463
time_elpased: 1.681
batch start
#iterations: 132
currently lose_sum: 518.157269179821
time_elpased: 1.623
batch start
#iterations: 133
currently lose_sum: 520.9174025952816
time_elpased: 1.697
batch start
#iterations: 134
currently lose_sum: 519.7652216851711
time_elpased: 1.669
batch start
#iterations: 135
currently lose_sum: 516.006421148777
time_elpased: 1.636
batch start
#iterations: 136
currently lose_sum: 517.3282173871994
time_elpased: 1.691
batch start
#iterations: 137
currently lose_sum: 516.5414324998856
time_elpased: 1.723
batch start
#iterations: 138
currently lose_sum: 518.4024130105972
time_elpased: 1.63
batch start
#iterations: 139
currently lose_sum: 518.7108298242092
time_elpased: 1.639
start validation test
0.147010309278
1.0
0.147010309278
0.256336509078
0
validation finish
batch start
#iterations: 140
currently lose_sum: 515.7074118554592
time_elpased: 1.67
batch start
#iterations: 141
currently lose_sum: 516.7618255019188
time_elpased: 1.656
batch start
#iterations: 142
currently lose_sum: 518.9624657928944
time_elpased: 1.641
batch start
#iterations: 143
currently lose_sum: 518.4922533929348
time_elpased: 1.626
batch start
#iterations: 144
currently lose_sum: 519.7561786472797
time_elpased: 1.665
batch start
#iterations: 145
currently lose_sum: 518.0030519068241
time_elpased: 1.629
batch start
#iterations: 146
currently lose_sum: 520.0398392081261
time_elpased: 1.656
batch start
#iterations: 147
currently lose_sum: 519.375061839819
time_elpased: 1.704
batch start
#iterations: 148
currently lose_sum: 518.0073773860931
time_elpased: 1.642
batch start
#iterations: 149
currently lose_sum: 519.9582378864288
time_elpased: 1.633
batch start
#iterations: 150
currently lose_sum: 516.6085568666458
time_elpased: 1.629
batch start
#iterations: 151
currently lose_sum: 516.47425827384
time_elpased: 1.627
batch start
#iterations: 152
currently lose_sum: 518.1424831748009
time_elpased: 1.641
batch start
#iterations: 153
currently lose_sum: 515.7396142184734
time_elpased: 1.698
batch start
#iterations: 154
currently lose_sum: 522.130607187748
time_elpased: 1.658
batch start
#iterations: 155
currently lose_sum: 516.2622169554234
time_elpased: 1.633
batch start
#iterations: 156
currently lose_sum: 518.5266286730766
time_elpased: 1.664
batch start
#iterations: 157
currently lose_sum: 516.7539855539799
time_elpased: 1.697
batch start
#iterations: 158
currently lose_sum: 516.8887172043324
time_elpased: 1.697
batch start
#iterations: 159
currently lose_sum: 519.7908455729485
time_elpased: 1.689
start validation test
0.121443298969
1.0
0.121443298969
0.21658393087
0
validation finish
batch start
#iterations: 160
currently lose_sum: 519.6598409116268
time_elpased: 1.664
batch start
#iterations: 161
currently lose_sum: 518.9160820543766
time_elpased: 1.648
batch start
#iterations: 162
currently lose_sum: 515.7776088416576
time_elpased: 1.669
batch start
#iterations: 163
currently lose_sum: 515.9578439593315
time_elpased: 1.624
batch start
#iterations: 164
currently lose_sum: 519.4489832520485
time_elpased: 1.704
batch start
#iterations: 165
currently lose_sum: 520.3383312225342
time_elpased: 1.679
batch start
#iterations: 166
currently lose_sum: 519.7500358521938
time_elpased: 1.616
batch start
#iterations: 167
currently lose_sum: 519.3043129146099
time_elpased: 1.664
batch start
#iterations: 168
currently lose_sum: 518.502996712923
time_elpased: 1.628
batch start
#iterations: 169
currently lose_sum: 519.1693069934845
time_elpased: 1.62
batch start
#iterations: 170
currently lose_sum: 519.3083589673042
time_elpased: 1.658
batch start
#iterations: 171
currently lose_sum: 516.2717019021511
time_elpased: 1.688
batch start
#iterations: 172
currently lose_sum: 522.2120378911495
time_elpased: 1.667
batch start
#iterations: 173
currently lose_sum: 518.0377613902092
time_elpased: 1.628
batch start
#iterations: 174
currently lose_sum: 519.1542118787766
time_elpased: 1.696
batch start
#iterations: 175
currently lose_sum: 518.1776742041111
time_elpased: 1.614
batch start
#iterations: 176
currently lose_sum: 518.0873141288757
time_elpased: 1.651
batch start
#iterations: 177
currently lose_sum: 517.7796284556389
time_elpased: 1.629
batch start
#iterations: 178
currently lose_sum: 517.9909161627293
time_elpased: 1.669
batch start
#iterations: 179
currently lose_sum: 518.5573213100433
time_elpased: 1.641
start validation test
0.170515463918
1.0
0.170515463918
0.291351065704
0
validation finish
batch start
#iterations: 180
currently lose_sum: 519.1805229783058
time_elpased: 1.703
batch start
#iterations: 181
currently lose_sum: 518.6442478895187
time_elpased: 1.701
batch start
#iterations: 182
currently lose_sum: 517.7078206837177
time_elpased: 1.724
batch start
#iterations: 183
currently lose_sum: 520.026143014431
time_elpased: 1.634
batch start
#iterations: 184
currently lose_sum: 516.85816603899
time_elpased: 1.701
batch start
#iterations: 185
currently lose_sum: 518.3034398853779
time_elpased: 1.679
batch start
#iterations: 186
currently lose_sum: 519.4508580267429
time_elpased: 1.709
batch start
#iterations: 187
currently lose_sum: 516.8306507766247
time_elpased: 1.631
batch start
#iterations: 188
currently lose_sum: 518.7605100870132
time_elpased: 1.628
batch start
#iterations: 189
currently lose_sum: 517.7763751745224
time_elpased: 1.657
batch start
#iterations: 190
currently lose_sum: 517.2760678231716
time_elpased: 1.626
batch start
#iterations: 191
currently lose_sum: 519.4359616041183
time_elpased: 1.645
batch start
#iterations: 192
currently lose_sum: 517.0610204041004
time_elpased: 1.724
batch start
#iterations: 193
currently lose_sum: 516.82127353549
time_elpased: 1.635
batch start
#iterations: 194
currently lose_sum: 520.0837292671204
time_elpased: 1.683
batch start
#iterations: 195
currently lose_sum: 517.9860465228558
time_elpased: 1.703
batch start
#iterations: 196
currently lose_sum: 515.448664098978
time_elpased: 1.628
batch start
#iterations: 197
currently lose_sum: 519.2574653029442
time_elpased: 1.68
batch start
#iterations: 198
currently lose_sum: 517.7213915884495
time_elpased: 1.669
batch start
#iterations: 199
currently lose_sum: 517.411062926054
time_elpased: 1.635
start validation test
0.128453608247
1.0
0.128453608247
0.227663073269
0
validation finish
batch start
#iterations: 200
currently lose_sum: 518.7655068337917
time_elpased: 1.679
batch start
#iterations: 201
currently lose_sum: 517.3971266448498
time_elpased: 1.713
batch start
#iterations: 202
currently lose_sum: 518.9669685065746
time_elpased: 1.723
batch start
#iterations: 203
currently lose_sum: 517.0765760838985
time_elpased: 1.637
batch start
#iterations: 204
currently lose_sum: 518.496807217598
time_elpased: 1.619
batch start
#iterations: 205
currently lose_sum: 518.8029461801052
time_elpased: 1.711
batch start
#iterations: 206
currently lose_sum: 520.3737896382809
time_elpased: 1.633
batch start
#iterations: 207
currently lose_sum: 519.202116549015
time_elpased: 1.624
batch start
#iterations: 208
currently lose_sum: 517.1101361513138
time_elpased: 1.621
batch start
#iterations: 209
currently lose_sum: 517.8807429075241
time_elpased: 1.651
batch start
#iterations: 210
currently lose_sum: 517.0920165777206
time_elpased: 1.709
batch start
#iterations: 211
currently lose_sum: 518.254360049963
time_elpased: 1.62
batch start
#iterations: 212
currently lose_sum: 518.2786991000175
time_elpased: 1.675
batch start
#iterations: 213
currently lose_sum: 518.0430378913879
time_elpased: 1.663
batch start
#iterations: 214
currently lose_sum: 517.4155928790569
time_elpased: 1.678
batch start
#iterations: 215
currently lose_sum: 518.7162460386753
time_elpased: 1.65
batch start
#iterations: 216
currently lose_sum: 516.6532202959061
time_elpased: 1.686
batch start
#iterations: 217
currently lose_sum: 518.6046026647091
time_elpased: 1.684
batch start
#iterations: 218
currently lose_sum: 518.6647382378578
time_elpased: 1.675
batch start
#iterations: 219
currently lose_sum: 519.0480915307999
time_elpased: 1.624
start validation test
0.183505154639
1.0
0.183505154639
0.310104529617
0
validation finish
batch start
#iterations: 220
currently lose_sum: 517.9534377455711
time_elpased: 1.641
batch start
#iterations: 221
currently lose_sum: 518.874475389719
time_elpased: 1.631
batch start
#iterations: 222
currently lose_sum: 516.9128688573837
time_elpased: 1.727
batch start
#iterations: 223
currently lose_sum: 516.5423184037209
time_elpased: 1.676
batch start
#iterations: 224
currently lose_sum: 518.7032962143421
time_elpased: 1.697
batch start
#iterations: 225
currently lose_sum: 519.322207480669
time_elpased: 1.683
batch start
#iterations: 226
currently lose_sum: 518.9131932258606
time_elpased: 1.688
batch start
#iterations: 227
currently lose_sum: 518.0853860080242
time_elpased: 1.617
batch start
#iterations: 228
currently lose_sum: 516.2901210188866
time_elpased: 1.712
batch start
#iterations: 229
currently lose_sum: 518.7617048621178
time_elpased: 1.72
batch start
#iterations: 230
currently lose_sum: 519.2117083668709
time_elpased: 1.612
batch start
#iterations: 231
currently lose_sum: 520.2041026353836
time_elpased: 1.631
batch start
#iterations: 232
currently lose_sum: 515.4349431693554
time_elpased: 1.65
batch start
#iterations: 233
currently lose_sum: 515.6462717950344
time_elpased: 1.647
batch start
#iterations: 234
currently lose_sum: 517.5595658123493
time_elpased: 1.707
batch start
#iterations: 235
currently lose_sum: 518.2423341870308
time_elpased: 1.616
batch start
#iterations: 236
currently lose_sum: 517.7312539219856
time_elpased: 1.671
batch start
#iterations: 237
currently lose_sum: 518.4874374866486
time_elpased: 1.645
batch start
#iterations: 238
currently lose_sum: 516.3996933996677
time_elpased: 1.665
batch start
#iterations: 239
currently lose_sum: 515.8450796604156
time_elpased: 1.702
start validation test
0.116082474227
1.0
0.116082474227
0.208017735082
0
validation finish
batch start
#iterations: 240
currently lose_sum: 515.7051067352295
time_elpased: 1.644
batch start
#iterations: 241
currently lose_sum: 516.5234377384186
time_elpased: 1.688
batch start
#iterations: 242
currently lose_sum: 515.4335277080536
time_elpased: 1.641
batch start
#iterations: 243
currently lose_sum: 517.8506732583046
time_elpased: 1.644
batch start
#iterations: 244
currently lose_sum: 517.0114597976208
time_elpased: 1.622
batch start
#iterations: 245
currently lose_sum: 518.5482976734638
time_elpased: 1.687
batch start
#iterations: 246
currently lose_sum: 517.9099017381668
time_elpased: 1.666
batch start
#iterations: 247
currently lose_sum: 518.5131500661373
time_elpased: 1.657
batch start
#iterations: 248
currently lose_sum: 517.7439152896404
time_elpased: 1.707
batch start
#iterations: 249
currently lose_sum: 519.460160702467
time_elpased: 1.676
batch start
#iterations: 250
currently lose_sum: 517.4230892658234
time_elpased: 1.668
batch start
#iterations: 251
currently lose_sum: 517.5133597552776
time_elpased: 1.622
batch start
#iterations: 252
currently lose_sum: 519.0065492391586
time_elpased: 1.631
batch start
#iterations: 253
currently lose_sum: 515.034720480442
time_elpased: 1.652
batch start
#iterations: 254
currently lose_sum: 519.4971305429935
time_elpased: 1.628
batch start
#iterations: 255
currently lose_sum: 517.7518877089024
time_elpased: 1.667
batch start
#iterations: 256
currently lose_sum: 517.0690957605839
time_elpased: 1.623
batch start
#iterations: 257
currently lose_sum: 517.3818931281567
time_elpased: 1.633
batch start
#iterations: 258
currently lose_sum: 517.7990498840809
time_elpased: 1.65
batch start
#iterations: 259
currently lose_sum: 518.9002347588539
time_elpased: 1.665
start validation test
0.174948453608
1.0
0.174948453608
0.297797666052
0
validation finish
batch start
#iterations: 260
currently lose_sum: 519.4000744223595
time_elpased: 1.626
batch start
#iterations: 261
currently lose_sum: 520.7852953076363
time_elpased: 1.631
batch start
#iterations: 262
currently lose_sum: 518.0972476899624
time_elpased: 1.659
batch start
#iterations: 263
currently lose_sum: 517.056947439909
time_elpased: 1.647
batch start
#iterations: 264
currently lose_sum: 517.751062721014
time_elpased: 1.704
batch start
#iterations: 265
currently lose_sum: 517.3143038451672
time_elpased: 1.696
batch start
#iterations: 266
currently lose_sum: 517.922529220581
time_elpased: 1.634
batch start
#iterations: 267
currently lose_sum: 517.1563788354397
time_elpased: 1.686
batch start
#iterations: 268
currently lose_sum: 518.186937391758
time_elpased: 1.619
batch start
#iterations: 269
currently lose_sum: 518.4278955459595
time_elpased: 1.626
batch start
#iterations: 270
currently lose_sum: 517.2923993170261
time_elpased: 1.72
batch start
#iterations: 271
currently lose_sum: 514.8335039913654
time_elpased: 1.703
batch start
#iterations: 272
currently lose_sum: 518.6697084605694
time_elpased: 1.633
batch start
#iterations: 273
currently lose_sum: 515.7275772988796
time_elpased: 1.7
batch start
#iterations: 274
currently lose_sum: 519.3219174444675
time_elpased: 1.63
batch start
#iterations: 275
currently lose_sum: 518.5674738287926
time_elpased: 1.692
batch start
#iterations: 276
currently lose_sum: 515.3991996645927
time_elpased: 1.657
batch start
#iterations: 277
currently lose_sum: 520.1483646333218
time_elpased: 1.618
batch start
#iterations: 278
currently lose_sum: 517.9611185491085
time_elpased: 1.677
batch start
#iterations: 279
currently lose_sum: 518.0470453202724
time_elpased: 1.677
start validation test
0.12412371134
1.0
0.12412371134
0.220836390315
0
validation finish
batch start
#iterations: 280
currently lose_sum: 517.0544070005417
time_elpased: 1.62
batch start
#iterations: 281
currently lose_sum: 518.0112855732441
time_elpased: 1.697
batch start
#iterations: 282
currently lose_sum: 517.3304051160812
time_elpased: 1.629
batch start
#iterations: 283
currently lose_sum: 517.2092937231064
time_elpased: 1.632
batch start
#iterations: 284
currently lose_sum: 518.7246051430702
time_elpased: 1.702
batch start
#iterations: 285
currently lose_sum: 517.3690212965012
time_elpased: 1.679
batch start
#iterations: 286
currently lose_sum: 518.8686116635799
time_elpased: 1.677
batch start
#iterations: 287
currently lose_sum: 517.9824986457825
time_elpased: 1.623
batch start
#iterations: 288
currently lose_sum: 518.7424247860909
time_elpased: 1.623
batch start
#iterations: 289
currently lose_sum: 517.2484086155891
time_elpased: 1.62
batch start
#iterations: 290
currently lose_sum: 515.8014400601387
time_elpased: 1.647
batch start
#iterations: 291
currently lose_sum: 518.1174595952034
time_elpased: 1.621
batch start
#iterations: 292
currently lose_sum: 518.1287105083466
time_elpased: 1.64
batch start
#iterations: 293
currently lose_sum: 518.2443226575851
time_elpased: 1.695
batch start
#iterations: 294
currently lose_sum: 518.3470363616943
time_elpased: 1.635
batch start
#iterations: 295
currently lose_sum: 518.2166979312897
time_elpased: 1.63
batch start
#iterations: 296
currently lose_sum: 515.6264126002789
time_elpased: 1.629
batch start
#iterations: 297
currently lose_sum: 517.3659744560719
time_elpased: 1.636
batch start
#iterations: 298
currently lose_sum: 517.077005982399
time_elpased: 1.69
batch start
#iterations: 299
currently lose_sum: 517.6273622214794
time_elpased: 1.633
start validation test
0.153092783505
1.0
0.153092783505
0.265534197586
0
validation finish
batch start
#iterations: 300
currently lose_sum: 514.5462717413902
time_elpased: 1.659
batch start
#iterations: 301
currently lose_sum: 517.4813666343689
time_elpased: 1.629
batch start
#iterations: 302
currently lose_sum: 518.4591152966022
time_elpased: 1.634
batch start
#iterations: 303
currently lose_sum: 518.7759610414505
time_elpased: 1.625
batch start
#iterations: 304
currently lose_sum: 518.6984920203686
time_elpased: 1.675
batch start
#iterations: 305
currently lose_sum: 518.4546257555485
time_elpased: 1.644
batch start
#iterations: 306
currently lose_sum: 516.8935844600201
time_elpased: 1.633
batch start
#iterations: 307
currently lose_sum: 520.2227767109871
time_elpased: 1.628
batch start
#iterations: 308
currently lose_sum: 516.2312948107719
time_elpased: 1.647
batch start
#iterations: 309
currently lose_sum: 518.8629554212093
time_elpased: 1.622
batch start
#iterations: 310
currently lose_sum: 515.8372872173786
time_elpased: 1.666
batch start
#iterations: 311
currently lose_sum: 516.3502260744572
time_elpased: 1.663
batch start
#iterations: 312
currently lose_sum: 517.4399181902409
time_elpased: 1.714
batch start
#iterations: 313
currently lose_sum: 517.7618507742882
time_elpased: 1.636
batch start
#iterations: 314
currently lose_sum: 515.873104006052
time_elpased: 1.722
batch start
#iterations: 315
currently lose_sum: 517.9965934455395
time_elpased: 1.627
batch start
#iterations: 316
currently lose_sum: 517.066913664341
time_elpased: 1.631
batch start
#iterations: 317
currently lose_sum: 516.6795105338097
time_elpased: 1.667
batch start
#iterations: 318
currently lose_sum: 519.1976592540741
time_elpased: 1.718
batch start
#iterations: 319
currently lose_sum: 518.089124172926
time_elpased: 1.632
start validation test
0.14412371134
1.0
0.14412371134
0.251937285997
0
validation finish
batch start
#iterations: 320
currently lose_sum: 517.1303468644619
time_elpased: 1.644
batch start
#iterations: 321
currently lose_sum: 516.7287614941597
time_elpased: 1.626
batch start
#iterations: 322
currently lose_sum: 517.3850101530552
time_elpased: 1.712
batch start
#iterations: 323
currently lose_sum: 516.7147520184517
time_elpased: 1.64
batch start
#iterations: 324
currently lose_sum: 518.9117916822433
time_elpased: 1.626
batch start
#iterations: 325
currently lose_sum: 518.435056746006
time_elpased: 1.636
batch start
#iterations: 326
currently lose_sum: 518.3228577673435
time_elpased: 1.67
batch start
#iterations: 327
currently lose_sum: 517.9235247373581
time_elpased: 1.695
batch start
#iterations: 328
currently lose_sum: 515.4434877932072
time_elpased: 1.635
batch start
#iterations: 329
currently lose_sum: 518.7251212596893
time_elpased: 1.624
batch start
#iterations: 330
currently lose_sum: 518.0543451309204
time_elpased: 1.615
batch start
#iterations: 331
currently lose_sum: 519.6383277177811
time_elpased: 1.646
batch start
#iterations: 332
currently lose_sum: 518.7939967215061
time_elpased: 1.619
batch start
#iterations: 333
currently lose_sum: 518.4764481186867
time_elpased: 1.688
batch start
#iterations: 334
currently lose_sum: 517.2873438298702
time_elpased: 1.636
batch start
#iterations: 335
currently lose_sum: 516.0765587389469
time_elpased: 1.639
batch start
#iterations: 336
currently lose_sum: 518.101290255785
time_elpased: 1.707
batch start
#iterations: 337
currently lose_sum: 517.8406038880348
time_elpased: 1.687
batch start
#iterations: 338
currently lose_sum: 517.7573447525501
time_elpased: 1.662
batch start
#iterations: 339
currently lose_sum: 516.5104673504829
time_elpased: 1.687
start validation test
0.165360824742
1.0
0.165360824742
0.283793347488
0
validation finish
batch start
#iterations: 340
currently lose_sum: 518.1469650864601
time_elpased: 1.69
batch start
#iterations: 341
currently lose_sum: 518.7627809047699
time_elpased: 1.69
batch start
#iterations: 342
currently lose_sum: 517.4967026114464
time_elpased: 1.694
batch start
#iterations: 343
currently lose_sum: 518.3917624652386
time_elpased: 1.622
batch start
#iterations: 344
currently lose_sum: 517.7864312231541
time_elpased: 1.627
batch start
#iterations: 345
currently lose_sum: 517.4486637413502
time_elpased: 1.711
batch start
#iterations: 346
currently lose_sum: 516.6721048951149
time_elpased: 1.668
batch start
#iterations: 347
currently lose_sum: 516.5725751817226
time_elpased: 1.618
batch start
#iterations: 348
currently lose_sum: 518.2989000082016
time_elpased: 1.659
batch start
#iterations: 349
currently lose_sum: 517.829911261797
time_elpased: 1.643
batch start
#iterations: 350
currently lose_sum: 518.2667411565781
time_elpased: 1.625
batch start
#iterations: 351
currently lose_sum: 517.066917270422
time_elpased: 1.687
batch start
#iterations: 352
currently lose_sum: 517.8301546573639
time_elpased: 1.673
batch start
#iterations: 353
currently lose_sum: 519.6209772825241
time_elpased: 1.643
batch start
#iterations: 354
currently lose_sum: 516.2741902172565
time_elpased: 1.682
batch start
#iterations: 355
currently lose_sum: 517.9945132732391
time_elpased: 1.661
batch start
#iterations: 356
currently lose_sum: 518.4659418463707
time_elpased: 1.688
batch start
#iterations: 357
currently lose_sum: 517.2902679443359
time_elpased: 1.626
batch start
#iterations: 358
currently lose_sum: 517.8513653874397
time_elpased: 1.724
batch start
#iterations: 359
currently lose_sum: 518.0785048305988
time_elpased: 1.638
start validation test
0.148350515464
1.0
0.148350515464
0.258371487566
0
validation finish
batch start
#iterations: 360
currently lose_sum: 517.83937895298
time_elpased: 1.638
batch start
#iterations: 361
currently lose_sum: 515.4551099538803
time_elpased: 1.628
batch start
#iterations: 362
currently lose_sum: 518.1882193982601
time_elpased: 1.64
batch start
#iterations: 363
currently lose_sum: 517.4947012960911
time_elpased: 1.651
batch start
#iterations: 364
currently lose_sum: 516.8559513390064
time_elpased: 1.694
batch start
#iterations: 365
currently lose_sum: 516.9021422863007
time_elpased: 1.647
batch start
#iterations: 366
currently lose_sum: 518.4902057945728
time_elpased: 1.725
batch start
#iterations: 367
currently lose_sum: 518.7869077324867
time_elpased: 1.683
batch start
#iterations: 368
currently lose_sum: 516.1903129220009
time_elpased: 1.684
batch start
#iterations: 369
currently lose_sum: 515.6030479371548
time_elpased: 1.617
batch start
#iterations: 370
currently lose_sum: 515.3215438723564
time_elpased: 1.725
batch start
#iterations: 371
currently lose_sum: 519.3906862437725
time_elpased: 1.657
batch start
#iterations: 372
currently lose_sum: 517.7381776571274
time_elpased: 1.688
batch start
#iterations: 373
currently lose_sum: 517.1962647140026
time_elpased: 1.709
batch start
#iterations: 374
currently lose_sum: 518.4945240914822
time_elpased: 1.626
batch start
#iterations: 375
currently lose_sum: 517.6328607797623
time_elpased: 1.631
batch start
#iterations: 376
currently lose_sum: 516.7288290858269
time_elpased: 1.668
batch start
#iterations: 377
currently lose_sum: 517.4985239207745
time_elpased: 1.626
batch start
#iterations: 378
currently lose_sum: 517.3525075912476
time_elpased: 1.674
batch start
#iterations: 379
currently lose_sum: 516.104789942503
time_elpased: 1.687
start validation test
0.146804123711
1.0
0.146804123711
0.256023013305
0
validation finish
batch start
#iterations: 380
currently lose_sum: 517.0838054418564
time_elpased: 1.634
batch start
#iterations: 381
currently lose_sum: 517.3147923946381
time_elpased: 1.711
batch start
#iterations: 382
currently lose_sum: 517.918745547533
time_elpased: 1.63
batch start
#iterations: 383
currently lose_sum: 514.7630313634872
time_elpased: 1.724
batch start
#iterations: 384
currently lose_sum: 516.4405920803547
time_elpased: 1.68
batch start
#iterations: 385
currently lose_sum: 515.6399415433407
time_elpased: 1.704
batch start
#iterations: 386
currently lose_sum: 517.7828651070595
time_elpased: 1.639
batch start
#iterations: 387
currently lose_sum: 518.0551337003708
time_elpased: 1.685
batch start
#iterations: 388
currently lose_sum: 518.2491733729839
time_elpased: 1.626
batch start
#iterations: 389
currently lose_sum: 516.6211766600609
time_elpased: 1.639
batch start
#iterations: 390
currently lose_sum: 515.3516619205475
time_elpased: 1.648
batch start
#iterations: 391
currently lose_sum: 516.9689008891582
time_elpased: 1.653
batch start
#iterations: 392
currently lose_sum: 516.9652760326862
time_elpased: 1.718
batch start
#iterations: 393
currently lose_sum: 516.4330733120441
time_elpased: 1.636
batch start
#iterations: 394
currently lose_sum: 517.586542069912
time_elpased: 1.725
batch start
#iterations: 395
currently lose_sum: 517.3952392339706
time_elpased: 1.646
batch start
#iterations: 396
currently lose_sum: 516.6536359488964
time_elpased: 1.736
batch start
#iterations: 397
currently lose_sum: 518.2166483402252
time_elpased: 1.65
batch start
#iterations: 398
currently lose_sum: 517.1845752298832
time_elpased: 1.689
batch start
#iterations: 399
currently lose_sum: 517.0043399035931
time_elpased: 1.692
start validation test
0.140927835052
1.0
0.140927835052
0.247040751785
0
validation finish
acc: 0.208
pre: 1.000
rec: 0.208
F1: 0.344
auc: 0.000
