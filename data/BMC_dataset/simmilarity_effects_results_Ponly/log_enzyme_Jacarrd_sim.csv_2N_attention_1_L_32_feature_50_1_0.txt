start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 564.4187316894531
time_elpased: 1.749
batch start
#iterations: 1
currently lose_sum: 545.9842151403427
time_elpased: 1.668
batch start
#iterations: 2
currently lose_sum: 539.873797595501
time_elpased: 1.652
batch start
#iterations: 3
currently lose_sum: 536.1682622432709
time_elpased: 1.67
batch start
#iterations: 4
currently lose_sum: 533.2088124752045
time_elpased: 1.677
batch start
#iterations: 5
currently lose_sum: 532.5948144197464
time_elpased: 1.671
batch start
#iterations: 6
currently lose_sum: 529.7326609492302
time_elpased: 1.68
batch start
#iterations: 7
currently lose_sum: 528.8387540578842
time_elpased: 1.698
batch start
#iterations: 8
currently lose_sum: 528.817269474268
time_elpased: 1.717
batch start
#iterations: 9
currently lose_sum: 531.1360312700272
time_elpased: 1.662
batch start
#iterations: 10
currently lose_sum: 529.8295916318893
time_elpased: 1.671
batch start
#iterations: 11
currently lose_sum: 530.798543959856
time_elpased: 1.742
batch start
#iterations: 12
currently lose_sum: 529.6813545525074
time_elpased: 1.665
batch start
#iterations: 13
currently lose_sum: 527.3692831993103
time_elpased: 1.712
batch start
#iterations: 14
currently lose_sum: 527.8094756007195
time_elpased: 1.666
batch start
#iterations: 15
currently lose_sum: 525.7207435369492
time_elpased: 1.663
batch start
#iterations: 16
currently lose_sum: 526.1883542835712
time_elpased: 1.664
batch start
#iterations: 17
currently lose_sum: 526.8602229654789
time_elpased: 1.661
batch start
#iterations: 18
currently lose_sum: 526.1170374155045
time_elpased: 1.662
batch start
#iterations: 19
currently lose_sum: 525.5977185964584
time_elpased: 1.665
start validation test
0.122680412371
1.0
0.122680412371
0.21854912764
0
validation finish
batch start
#iterations: 20
currently lose_sum: 526.0543320477009
time_elpased: 1.67
batch start
#iterations: 21
currently lose_sum: 526.1828067004681
time_elpased: 1.677
batch start
#iterations: 22
currently lose_sum: 525.095071464777
time_elpased: 1.714
batch start
#iterations: 23
currently lose_sum: 525.178846269846
time_elpased: 1.714
batch start
#iterations: 24
currently lose_sum: 523.4422268867493
time_elpased: 1.703
batch start
#iterations: 25
currently lose_sum: 525.2851571440697
time_elpased: 1.691
batch start
#iterations: 26
currently lose_sum: 524.5266041457653
time_elpased: 1.705
batch start
#iterations: 27
currently lose_sum: 525.1593990325928
time_elpased: 1.739
batch start
#iterations: 28
currently lose_sum: 523.6480422019958
time_elpased: 1.701
batch start
#iterations: 29
currently lose_sum: 523.1323320567608
time_elpased: 1.681
batch start
#iterations: 30
currently lose_sum: 522.2880612611771
time_elpased: 1.763
batch start
#iterations: 31
currently lose_sum: 523.1144838631153
time_elpased: 1.719
batch start
#iterations: 32
currently lose_sum: 524.8278552889824
time_elpased: 1.69
batch start
#iterations: 33
currently lose_sum: 523.5931991040707
time_elpased: 1.738
batch start
#iterations: 34
currently lose_sum: 522.993456274271
time_elpased: 1.673
batch start
#iterations: 35
currently lose_sum: 522.3638184070587
time_elpased: 1.664
batch start
#iterations: 36
currently lose_sum: 523.016405493021
time_elpased: 1.683
batch start
#iterations: 37
currently lose_sum: 520.9632749855518
time_elpased: 1.724
batch start
#iterations: 38
currently lose_sum: 522.4336658418179
time_elpased: 1.767
batch start
#iterations: 39
currently lose_sum: 523.1278019845486
time_elpased: 1.731
start validation test
0.101030927835
1.0
0.101030927835
0.183520599251
0
validation finish
batch start
#iterations: 40
currently lose_sum: 523.0634105205536
time_elpased: 1.659
batch start
#iterations: 41
currently lose_sum: 524.5268169641495
time_elpased: 1.66
batch start
#iterations: 42
currently lose_sum: 521.617268383503
time_elpased: 1.73
batch start
#iterations: 43
currently lose_sum: 521.9252970516682
time_elpased: 1.668
batch start
#iterations: 44
currently lose_sum: 523.1439527571201
time_elpased: 1.676
batch start
#iterations: 45
currently lose_sum: 524.3276819288731
time_elpased: 1.713
batch start
#iterations: 46
currently lose_sum: 521.9147690236568
time_elpased: 1.688
batch start
#iterations: 47
currently lose_sum: 521.2451147735119
time_elpased: 1.717
batch start
#iterations: 48
currently lose_sum: 522.572318315506
time_elpased: 1.68
batch start
#iterations: 49
currently lose_sum: 520.4603956341743
time_elpased: 1.675
batch start
#iterations: 50
currently lose_sum: 522.677869707346
time_elpased: 1.732
batch start
#iterations: 51
currently lose_sum: 522.4038364291191
time_elpased: 1.69
batch start
#iterations: 52
currently lose_sum: 522.423848092556
time_elpased: 1.669
batch start
#iterations: 53
currently lose_sum: 521.4043339192867
time_elpased: 1.678
batch start
#iterations: 54
currently lose_sum: 521.452088445425
time_elpased: 1.691
batch start
#iterations: 55
currently lose_sum: 522.4228767156601
time_elpased: 1.684
batch start
#iterations: 56
currently lose_sum: 521.9403027296066
time_elpased: 1.665
batch start
#iterations: 57
currently lose_sum: 520.6810994148254
time_elpased: 1.678
batch start
#iterations: 58
currently lose_sum: 523.4012592732906
time_elpased: 1.753
batch start
#iterations: 59
currently lose_sum: 520.6088332533836
time_elpased: 1.68
start validation test
0.177422680412
1.0
0.177422680412
0.301374660713
0
validation finish
batch start
#iterations: 60
currently lose_sum: 520.8833301961422
time_elpased: 1.676
batch start
#iterations: 61
currently lose_sum: 521.5301414132118
time_elpased: 1.718
batch start
#iterations: 62
currently lose_sum: 523.5264848172665
time_elpased: 1.67
batch start
#iterations: 63
currently lose_sum: 522.0783522129059
time_elpased: 1.746
batch start
#iterations: 64
currently lose_sum: 521.4017699062824
time_elpased: 1.755
batch start
#iterations: 65
currently lose_sum: 522.4842600822449
time_elpased: 1.675
batch start
#iterations: 66
currently lose_sum: 521.9224924743176
time_elpased: 1.696
batch start
#iterations: 67
currently lose_sum: 519.1227436959743
time_elpased: 1.711
batch start
#iterations: 68
currently lose_sum: 520.6361591517925
time_elpased: 1.67
batch start
#iterations: 69
currently lose_sum: 523.5012245774269
time_elpased: 1.682
batch start
#iterations: 70
currently lose_sum: 520.4568775892258
time_elpased: 1.681
batch start
#iterations: 71
currently lose_sum: 521.9210402071476
time_elpased: 1.742
batch start
#iterations: 72
currently lose_sum: 524.0817443430424
time_elpased: 1.697
batch start
#iterations: 73
currently lose_sum: 520.2614133059978
time_elpased: 1.711
batch start
#iterations: 74
currently lose_sum: 521.4226196408272
time_elpased: 1.684
batch start
#iterations: 75
currently lose_sum: 519.6720469892025
time_elpased: 1.685
batch start
#iterations: 76
currently lose_sum: 521.3031425476074
time_elpased: 1.727
batch start
#iterations: 77
currently lose_sum: 520.7857780754566
time_elpased: 1.724
batch start
#iterations: 78
currently lose_sum: 521.1027972698212
time_elpased: 1.694
batch start
#iterations: 79
currently lose_sum: 522.372477799654
time_elpased: 1.734
start validation test
0.245979381443
1.0
0.245979381443
0.394837001489
0
validation finish
batch start
#iterations: 80
currently lose_sum: 520.3232972323895
time_elpased: 1.713
batch start
#iterations: 81
currently lose_sum: 520.8035461306572
time_elpased: 1.74
batch start
#iterations: 82
currently lose_sum: 521.7327526807785
time_elpased: 1.723
batch start
#iterations: 83
currently lose_sum: 520.6167404651642
time_elpased: 1.71
batch start
#iterations: 84
currently lose_sum: 520.1140696704388
time_elpased: 1.768
batch start
#iterations: 85
currently lose_sum: 519.8361400961876
time_elpased: 1.701
batch start
#iterations: 86
currently lose_sum: 521.1446036398411
time_elpased: 1.751
batch start
#iterations: 87
currently lose_sum: 518.6485107541084
time_elpased: 1.73
batch start
#iterations: 88
currently lose_sum: 519.899288058281
time_elpased: 1.682
batch start
#iterations: 89
currently lose_sum: 521.6336407959461
time_elpased: 1.735
batch start
#iterations: 90
currently lose_sum: 520.549503415823
time_elpased: 1.718
batch start
#iterations: 91
currently lose_sum: 519.7322433888912
time_elpased: 1.753
batch start
#iterations: 92
currently lose_sum: 520.4477972984314
time_elpased: 1.739
batch start
#iterations: 93
currently lose_sum: 520.6625832617283
time_elpased: 1.745
batch start
#iterations: 94
currently lose_sum: 519.5494956076145
time_elpased: 1.684
batch start
#iterations: 95
currently lose_sum: 520.0779707431793
time_elpased: 1.746
batch start
#iterations: 96
currently lose_sum: 522.1170846223831
time_elpased: 1.672
batch start
#iterations: 97
currently lose_sum: 517.929133027792
time_elpased: 1.672
batch start
#iterations: 98
currently lose_sum: 521.1541196405888
time_elpased: 1.704
batch start
#iterations: 99
currently lose_sum: 521.7136581540108
time_elpased: 1.671
start validation test
0.166907216495
1.0
0.166907216495
0.286067673823
0
validation finish
batch start
#iterations: 100
currently lose_sum: 519.5445085167885
time_elpased: 1.676
batch start
#iterations: 101
currently lose_sum: 521.0078922212124
time_elpased: 1.737
batch start
#iterations: 102
currently lose_sum: 518.0911555290222
time_elpased: 1.767
batch start
#iterations: 103
currently lose_sum: 521.6541897654533
time_elpased: 1.735
batch start
#iterations: 104
currently lose_sum: 521.5022078454494
time_elpased: 1.669
batch start
#iterations: 105
currently lose_sum: 519.9248854517937
time_elpased: 1.724
batch start
#iterations: 106
currently lose_sum: 521.2113440930843
time_elpased: 1.682
batch start
#iterations: 107
currently lose_sum: 519.2660343050957
time_elpased: 1.723
batch start
#iterations: 108
currently lose_sum: 518.7234695255756
time_elpased: 1.69
batch start
#iterations: 109
currently lose_sum: 520.3076092898846
time_elpased: 1.714
batch start
#iterations: 110
currently lose_sum: 521.4289518892765
time_elpased: 1.683
batch start
#iterations: 111
currently lose_sum: 522.9124589264393
time_elpased: 1.695
batch start
#iterations: 112
currently lose_sum: 518.6945395171642
time_elpased: 1.753
batch start
#iterations: 113
currently lose_sum: 519.8543410897255
time_elpased: 1.68
batch start
#iterations: 114
currently lose_sum: 520.5021198391914
time_elpased: 1.756
batch start
#iterations: 115
currently lose_sum: 519.913498044014
time_elpased: 1.7
batch start
#iterations: 116
currently lose_sum: 518.7752215564251
time_elpased: 1.76
batch start
#iterations: 117
currently lose_sum: 520.9324067533016
time_elpased: 1.711
batch start
#iterations: 118
currently lose_sum: 520.5978418588638
time_elpased: 1.686
batch start
#iterations: 119
currently lose_sum: 520.0383301377296
time_elpased: 1.661
start validation test
0.132680412371
1.0
0.132680412371
0.234276872668
0
validation finish
batch start
#iterations: 120
currently lose_sum: 521.1453584134579
time_elpased: 1.714
batch start
#iterations: 121
currently lose_sum: 520.6780413985252
time_elpased: 1.692
batch start
#iterations: 122
currently lose_sum: 519.0743865072727
time_elpased: 1.695
batch start
#iterations: 123
currently lose_sum: 519.1098197996616
time_elpased: 1.744
batch start
#iterations: 124
currently lose_sum: 519.5058885514736
time_elpased: 1.676
batch start
#iterations: 125
currently lose_sum: 520.5384798049927
time_elpased: 1.761
batch start
#iterations: 126
currently lose_sum: 520.9248541593552
time_elpased: 1.716
batch start
#iterations: 127
currently lose_sum: 519.4695698618889
time_elpased: 1.682
batch start
#iterations: 128
currently lose_sum: 519.8552069962025
time_elpased: 1.681
batch start
#iterations: 129
currently lose_sum: 520.6511189043522
time_elpased: 1.685
batch start
#iterations: 130
currently lose_sum: 519.0317439138889
time_elpased: 1.721
batch start
#iterations: 131
currently lose_sum: 520.2867463827133
time_elpased: 1.781
batch start
#iterations: 132
currently lose_sum: 520.5570012927055
time_elpased: 1.748
batch start
#iterations: 133
currently lose_sum: 521.1250286102295
time_elpased: 1.735
batch start
#iterations: 134
currently lose_sum: 520.4507343173027
time_elpased: 1.698
batch start
#iterations: 135
currently lose_sum: 517.8726412653923
time_elpased: 1.696
batch start
#iterations: 136
currently lose_sum: 520.1262905597687
time_elpased: 1.757
batch start
#iterations: 137
currently lose_sum: 516.9705128669739
time_elpased: 1.729
batch start
#iterations: 138
currently lose_sum: 517.8299677371979
time_elpased: 1.681
batch start
#iterations: 139
currently lose_sum: 519.1926436722279
time_elpased: 1.686
start validation test
0.157010309278
1.0
0.157010309278
0.271406932193
0
validation finish
batch start
#iterations: 140
currently lose_sum: 518.001668959856
time_elpased: 1.672
batch start
#iterations: 141
currently lose_sum: 519.7471761405468
time_elpased: 1.715
batch start
#iterations: 142
currently lose_sum: 517.706512004137
time_elpased: 1.788
batch start
#iterations: 143
currently lose_sum: 520.1185753047466
time_elpased: 1.683
batch start
#iterations: 144
currently lose_sum: 518.5574035644531
time_elpased: 1.754
batch start
#iterations: 145
currently lose_sum: 520.6227540969849
time_elpased: 1.693
batch start
#iterations: 146
currently lose_sum: 522.1197589933872
time_elpased: 1.71
batch start
#iterations: 147
currently lose_sum: 520.8462808430195
time_elpased: 1.697
batch start
#iterations: 148
currently lose_sum: 518.5857203304768
time_elpased: 1.69
batch start
#iterations: 149
currently lose_sum: 520.4065987467766
time_elpased: 1.731
batch start
#iterations: 150
currently lose_sum: 519.3120522201061
time_elpased: 1.683
batch start
#iterations: 151
currently lose_sum: 518.8060060441494
time_elpased: 1.778
batch start
#iterations: 152
currently lose_sum: 519.4911131262779
time_elpased: 1.709
batch start
#iterations: 153
currently lose_sum: 517.1314102113247
time_elpased: 1.694
batch start
#iterations: 154
currently lose_sum: 523.0165046155453
time_elpased: 1.72
batch start
#iterations: 155
currently lose_sum: 517.0454859733582
time_elpased: 1.689
batch start
#iterations: 156
currently lose_sum: 519.8859150707722
time_elpased: 1.685
batch start
#iterations: 157
currently lose_sum: 517.427950501442
time_elpased: 1.686
batch start
#iterations: 158
currently lose_sum: 518.5721440017223
time_elpased: 1.767
batch start
#iterations: 159
currently lose_sum: 519.7977069616318
time_elpased: 1.754
start validation test
0.124742268041
1.0
0.124742268041
0.221814848763
0
validation finish
batch start
#iterations: 160
currently lose_sum: 521.2439194619656
time_elpased: 1.695
batch start
#iterations: 161
currently lose_sum: 517.874918371439
time_elpased: 1.712
batch start
#iterations: 162
currently lose_sum: 518.9134186804295
time_elpased: 1.684
batch start
#iterations: 163
currently lose_sum: 517.7610782384872
time_elpased: 1.768
batch start
#iterations: 164
currently lose_sum: 519.8214882612228
time_elpased: 1.686
batch start
#iterations: 165
currently lose_sum: 520.9150172173977
time_elpased: 1.756
batch start
#iterations: 166
currently lose_sum: 519.8621238470078
time_elpased: 1.744
batch start
#iterations: 167
currently lose_sum: 519.5149404406548
time_elpased: 1.771
batch start
#iterations: 168
currently lose_sum: 519.4833101332188
time_elpased: 1.691
batch start
#iterations: 169
currently lose_sum: 520.0057311654091
time_elpased: 1.695
batch start
#iterations: 170
currently lose_sum: 517.7428545951843
time_elpased: 1.718
batch start
#iterations: 171
currently lose_sum: 517.7356865108013
time_elpased: 1.747
batch start
#iterations: 172
currently lose_sum: 521.0638968348503
time_elpased: 1.717
batch start
#iterations: 173
currently lose_sum: 519.8357078135014
time_elpased: 1.755
batch start
#iterations: 174
currently lose_sum: 519.5383343398571
time_elpased: 1.73
batch start
#iterations: 175
currently lose_sum: 519.845154106617
time_elpased: 1.691
batch start
#iterations: 176
currently lose_sum: 519.2569807171822
time_elpased: 1.714
batch start
#iterations: 177
currently lose_sum: 520.7472788393497
time_elpased: 1.737
batch start
#iterations: 178
currently lose_sum: 519.5296227633953
time_elpased: 1.72
batch start
#iterations: 179
currently lose_sum: 521.4497663080692
time_elpased: 1.752
start validation test
0.182680412371
1.0
0.182680412371
0.308926080893
0
validation finish
batch start
#iterations: 180
currently lose_sum: 520.8022388517857
time_elpased: 1.698
batch start
#iterations: 181
currently lose_sum: 520.9292631447315
time_elpased: 1.69
batch start
#iterations: 182
currently lose_sum: 520.2151711285114
time_elpased: 1.718
batch start
#iterations: 183
currently lose_sum: 520.0131085813046
time_elpased: 1.68
batch start
#iterations: 184
currently lose_sum: 518.4718621075153
time_elpased: 1.692
batch start
#iterations: 185
currently lose_sum: 520.1906026899815
time_elpased: 1.678
batch start
#iterations: 186
currently lose_sum: 519.1252649724483
time_elpased: 1.672
batch start
#iterations: 187
currently lose_sum: 519.8333201110363
time_elpased: 1.714
batch start
#iterations: 188
currently lose_sum: 519.07318136096
time_elpased: 1.693
batch start
#iterations: 189
currently lose_sum: 519.2442257404327
time_elpased: 1.753
batch start
#iterations: 190
currently lose_sum: 518.9245573282242
time_elpased: 1.679
batch start
#iterations: 191
currently lose_sum: 520.034239500761
time_elpased: 1.677
batch start
#iterations: 192
currently lose_sum: 517.2460064888
time_elpased: 1.664
batch start
#iterations: 193
currently lose_sum: 519.2058186531067
time_elpased: 1.69
batch start
#iterations: 194
currently lose_sum: 520.4297560453415
time_elpased: 1.684
batch start
#iterations: 195
currently lose_sum: 521.5272939503193
time_elpased: 1.686
batch start
#iterations: 196
currently lose_sum: 517.3347077071667
time_elpased: 1.725
batch start
#iterations: 197
currently lose_sum: 520.6035408675671
time_elpased: 1.688
batch start
#iterations: 198
currently lose_sum: 519.2537172734737
time_elpased: 1.669
batch start
#iterations: 199
currently lose_sum: 518.9076416790485
time_elpased: 1.689
start validation test
0.137319587629
1.0
0.137319587629
0.24147933285
0
validation finish
batch start
#iterations: 200
currently lose_sum: 520.1998093724251
time_elpased: 1.772
batch start
#iterations: 201
currently lose_sum: 521.3641450703144
time_elpased: 1.676
batch start
#iterations: 202
currently lose_sum: 518.1579041182995
time_elpased: 1.72
batch start
#iterations: 203
currently lose_sum: 517.1461733877659
time_elpased: 1.688
batch start
#iterations: 204
currently lose_sum: 518.0712850093842
time_elpased: 1.724
batch start
#iterations: 205
currently lose_sum: 520.2853883802891
time_elpased: 1.661
batch start
#iterations: 206
currently lose_sum: 521.8712099790573
time_elpased: 1.724
batch start
#iterations: 207
currently lose_sum: 518.5734086334705
time_elpased: 1.731
batch start
#iterations: 208
currently lose_sum: 517.4551793336868
time_elpased: 1.68
batch start
#iterations: 209
currently lose_sum: 519.0745539665222
time_elpased: 1.681
batch start
#iterations: 210
currently lose_sum: 519.1450253427029
time_elpased: 1.741
batch start
#iterations: 211
currently lose_sum: 519.9273089170456
time_elpased: 1.724
batch start
#iterations: 212
currently lose_sum: 519.061849206686
time_elpased: 1.732
batch start
#iterations: 213
currently lose_sum: 519.5895430147648
time_elpased: 1.68
batch start
#iterations: 214
currently lose_sum: 519.4376918077469
time_elpased: 1.719
batch start
#iterations: 215
currently lose_sum: 518.6982952952385
time_elpased: 1.669
batch start
#iterations: 216
currently lose_sum: 519.238679766655
time_elpased: 1.73
batch start
#iterations: 217
currently lose_sum: 520.581512928009
time_elpased: 1.735
batch start
#iterations: 218
currently lose_sum: 518.8372275233269
time_elpased: 1.712
batch start
#iterations: 219
currently lose_sum: 521.9956457614899
time_elpased: 1.669
start validation test
0.110309278351
1.0
0.110309278351
0.198700092851
0
validation finish
batch start
#iterations: 220
currently lose_sum: 519.5859848558903
time_elpased: 1.732
batch start
#iterations: 221
currently lose_sum: 519.3992002308369
time_elpased: 1.671
batch start
#iterations: 222
currently lose_sum: 517.2858673930168
time_elpased: 1.673
batch start
#iterations: 223
currently lose_sum: 519.7519444823265
time_elpased: 1.7
batch start
#iterations: 224
currently lose_sum: 519.6699623465538
time_elpased: 1.689
batch start
#iterations: 225
currently lose_sum: 521.7609402239323
time_elpased: 1.68
batch start
#iterations: 226
currently lose_sum: 519.5337311625481
time_elpased: 1.678
batch start
#iterations: 227
currently lose_sum: 519.100814640522
time_elpased: 1.675
batch start
#iterations: 228
currently lose_sum: 516.6469088792801
time_elpased: 1.75
batch start
#iterations: 229
currently lose_sum: 519.6365536153316
time_elpased: 1.68
batch start
#iterations: 230
currently lose_sum: 520.6055082678795
time_elpased: 1.779
batch start
#iterations: 231
currently lose_sum: 521.3832678496838
time_elpased: 1.673
batch start
#iterations: 232
currently lose_sum: 519.318400233984
time_elpased: 1.728
batch start
#iterations: 233
currently lose_sum: 517.5260868668556
time_elpased: 1.745
batch start
#iterations: 234
currently lose_sum: 517.1318254470825
time_elpased: 1.692
batch start
#iterations: 235
currently lose_sum: 517.7260945141315
time_elpased: 1.733
batch start
#iterations: 236
currently lose_sum: 518.3756628632545
time_elpased: 1.686
batch start
#iterations: 237
currently lose_sum: 519.1708462536335
time_elpased: 1.708
batch start
#iterations: 238
currently lose_sum: 518.7677753269672
time_elpased: 1.671
batch start
#iterations: 239
currently lose_sum: 517.7159367501736
time_elpased: 1.771
start validation test
0.0939175257732
1.0
0.0939175257732
0.171708604279
0
validation finish
batch start
#iterations: 240
currently lose_sum: 519.776625007391
time_elpased: 1.711
batch start
#iterations: 241
currently lose_sum: 517.1076128482819
time_elpased: 1.715
batch start
#iterations: 242
currently lose_sum: 517.6772600412369
time_elpased: 1.683
batch start
#iterations: 243
currently lose_sum: 517.5457963049412
time_elpased: 1.75
batch start
#iterations: 244
currently lose_sum: 519.1025189757347
time_elpased: 1.704
batch start
#iterations: 245
currently lose_sum: 517.3608214259148
time_elpased: 1.728
batch start
#iterations: 246
currently lose_sum: 517.7698105871677
time_elpased: 1.664
batch start
#iterations: 247
currently lose_sum: 518.6414502859116
time_elpased: 1.74
batch start
#iterations: 248
currently lose_sum: 517.7548225522041
time_elpased: 1.677
batch start
#iterations: 249
currently lose_sum: 518.8859397172928
time_elpased: 1.669
batch start
#iterations: 250
currently lose_sum: 518.4700357317924
time_elpased: 1.674
batch start
#iterations: 251
currently lose_sum: 519.2670761942863
time_elpased: 1.757
batch start
#iterations: 252
currently lose_sum: 520.2641465067863
time_elpased: 1.683
batch start
#iterations: 253
currently lose_sum: 518.8296988606453
time_elpased: 1.762
batch start
#iterations: 254
currently lose_sum: 520.9812641143799
time_elpased: 1.722
batch start
#iterations: 255
currently lose_sum: 517.6869957745075
time_elpased: 1.728
batch start
#iterations: 256
currently lose_sum: 520.7112082242966
time_elpased: 1.699
batch start
#iterations: 257
currently lose_sum: 517.5806681513786
time_elpased: 1.676
batch start
#iterations: 258
currently lose_sum: 519.4241928160191
time_elpased: 1.736
batch start
#iterations: 259
currently lose_sum: 519.4972058832645
time_elpased: 1.669
start validation test
0.151134020619
1.0
0.151134020619
0.262582840767
0
validation finish
batch start
#iterations: 260
currently lose_sum: 520.8244543671608
time_elpased: 1.677
batch start
#iterations: 261
currently lose_sum: 520.059196472168
time_elpased: 1.686
batch start
#iterations: 262
currently lose_sum: 518.6696767807007
time_elpased: 1.68
batch start
#iterations: 263
currently lose_sum: 518.3779438138008
time_elpased: 1.689
batch start
#iterations: 264
currently lose_sum: 518.5437259078026
time_elpased: 1.734
batch start
#iterations: 265
currently lose_sum: 518.4947883486748
time_elpased: 1.715
batch start
#iterations: 266
currently lose_sum: 518.398807913065
time_elpased: 1.724
batch start
#iterations: 267
currently lose_sum: 518.5350810885429
time_elpased: 1.674
batch start
#iterations: 268
currently lose_sum: 518.8341621160507
time_elpased: 1.675
batch start
#iterations: 269
currently lose_sum: 516.7845191657543
time_elpased: 1.714
batch start
#iterations: 270
currently lose_sum: 518.618901014328
time_elpased: 1.68
batch start
#iterations: 271
currently lose_sum: 517.0420331954956
time_elpased: 1.75
batch start
#iterations: 272
currently lose_sum: 518.8580293059349
time_elpased: 1.734
batch start
#iterations: 273
currently lose_sum: 519.0395301282406
time_elpased: 1.67
batch start
#iterations: 274
currently lose_sum: 520.2755473554134
time_elpased: 1.725
batch start
#iterations: 275
currently lose_sum: 518.6196230053902
time_elpased: 1.685
batch start
#iterations: 276
currently lose_sum: 517.6769063770771
time_elpased: 1.748
batch start
#iterations: 277
currently lose_sum: 519.7884805500507
time_elpased: 1.672
batch start
#iterations: 278
currently lose_sum: 519.1839069128036
time_elpased: 1.719
batch start
#iterations: 279
currently lose_sum: 520.1652238965034
time_elpased: 1.688
start validation test
0.11824742268
1.0
0.11824742268
0.21148704711
0
validation finish
batch start
#iterations: 280
currently lose_sum: 517.5728287696838
time_elpased: 1.691
batch start
#iterations: 281
currently lose_sum: 517.0326706767082
time_elpased: 1.678
batch start
#iterations: 282
currently lose_sum: 516.8878457248211
time_elpased: 1.748
batch start
#iterations: 283
currently lose_sum: 519.3006534874439
time_elpased: 1.752
batch start
#iterations: 284
currently lose_sum: 518.6081039309502
time_elpased: 1.687
batch start
#iterations: 285
currently lose_sum: 519.5457447469234
time_elpased: 1.765
batch start
#iterations: 286
currently lose_sum: 519.6946122646332
time_elpased: 1.738
batch start
#iterations: 287
currently lose_sum: 521.4825155735016
time_elpased: 1.745
batch start
#iterations: 288
currently lose_sum: 518.8552194237709
time_elpased: 1.672
batch start
#iterations: 289
currently lose_sum: 518.1131118237972
time_elpased: 1.668
batch start
#iterations: 290
currently lose_sum: 517.0240377485752
time_elpased: 1.739
batch start
#iterations: 291
currently lose_sum: 518.4289298653603
time_elpased: 1.67
batch start
#iterations: 292
currently lose_sum: 519.4244861602783
time_elpased: 1.709
batch start
#iterations: 293
currently lose_sum: 518.7834208905697
time_elpased: 1.746
batch start
#iterations: 294
currently lose_sum: 520.1591700315475
time_elpased: 1.685
batch start
#iterations: 295
currently lose_sum: 518.5211949050426
time_elpased: 1.705
batch start
#iterations: 296
currently lose_sum: 517.7233963310719
time_elpased: 1.698
batch start
#iterations: 297
currently lose_sum: 519.1650507450104
time_elpased: 1.738
batch start
#iterations: 298
currently lose_sum: 519.1980730295181
time_elpased: 1.714
batch start
#iterations: 299
currently lose_sum: 520.0731956958771
time_elpased: 1.753
start validation test
0.16412371134
1.0
0.16412371134
0.281969535955
0
validation finish
batch start
#iterations: 300
currently lose_sum: 518.728956669569
time_elpased: 1.678
batch start
#iterations: 301
currently lose_sum: 518.1038089990616
time_elpased: 1.669
batch start
#iterations: 302
currently lose_sum: 519.5612766444683
time_elpased: 1.773
batch start
#iterations: 303
currently lose_sum: 519.8837590813637
time_elpased: 1.703
batch start
#iterations: 304
currently lose_sum: 517.9762690067291
time_elpased: 1.746
batch start
#iterations: 305
currently lose_sum: 519.965925693512
time_elpased: 1.703
batch start
#iterations: 306
currently lose_sum: 520.7711448967457
time_elpased: 1.743
batch start
#iterations: 307
currently lose_sum: 520.7132034599781
time_elpased: 1.757
batch start
#iterations: 308
currently lose_sum: 518.9196542203426
time_elpased: 1.676
batch start
#iterations: 309
currently lose_sum: 519.1396227478981
time_elpased: 1.677
batch start
#iterations: 310
currently lose_sum: 516.4034367501736
time_elpased: 1.71
batch start
#iterations: 311
currently lose_sum: 517.1176640987396
time_elpased: 1.701
batch start
#iterations: 312
currently lose_sum: 519.4646001458168
time_elpased: 1.693
batch start
#iterations: 313
currently lose_sum: 519.1777897775173
time_elpased: 1.671
batch start
#iterations: 314
currently lose_sum: 517.4301412701607
time_elpased: 1.725
batch start
#iterations: 315
currently lose_sum: 519.354404181242
time_elpased: 1.691
batch start
#iterations: 316
currently lose_sum: 518.9709165096283
time_elpased: 1.666
batch start
#iterations: 317
currently lose_sum: 517.4784306287766
time_elpased: 1.756
batch start
#iterations: 318
currently lose_sum: 520.6414784491062
time_elpased: 1.738
batch start
#iterations: 319
currently lose_sum: 517.9436608850956
time_elpased: 1.688
start validation test
0.183092783505
1.0
0.183092783505
0.309515510631
0
validation finish
batch start
#iterations: 320
currently lose_sum: 518.6332018375397
time_elpased: 1.733
batch start
#iterations: 321
currently lose_sum: 519.24994546175
time_elpased: 1.716
batch start
#iterations: 322
currently lose_sum: 518.7834765613079
time_elpased: 1.666
batch start
#iterations: 323
currently lose_sum: 518.4544527828693
time_elpased: 1.684
batch start
#iterations: 324
currently lose_sum: 520.3065244555473
time_elpased: 1.751
batch start
#iterations: 325
currently lose_sum: 517.4450123012066
time_elpased: 1.675
batch start
#iterations: 326
currently lose_sum: 518.8951459228992
time_elpased: 1.684
batch start
#iterations: 327
currently lose_sum: 520.0352151989937
time_elpased: 1.73
batch start
#iterations: 328
currently lose_sum: 519.350013166666
time_elpased: 1.692
batch start
#iterations: 329
currently lose_sum: 518.3562063276768
time_elpased: 1.676
batch start
#iterations: 330
currently lose_sum: 519.8689634799957
time_elpased: 1.715
batch start
#iterations: 331
currently lose_sum: 520.8943132162094
time_elpased: 1.704
batch start
#iterations: 332
currently lose_sum: 520.4046074151993
time_elpased: 1.667
batch start
#iterations: 333
currently lose_sum: 519.0744387209415
time_elpased: 1.72
batch start
#iterations: 334
currently lose_sum: 518.8938590586185
time_elpased: 1.678
batch start
#iterations: 335
currently lose_sum: 517.9951501488686
time_elpased: 1.727
batch start
#iterations: 336
currently lose_sum: 518.8887827396393
time_elpased: 1.745
batch start
#iterations: 337
currently lose_sum: 518.4103211760521
time_elpased: 1.677
batch start
#iterations: 338
currently lose_sum: 518.4318284094334
time_elpased: 1.762
batch start
#iterations: 339
currently lose_sum: 519.6844829320908
time_elpased: 1.724
start validation test
0.159690721649
1.0
0.159690721649
0.275402257978
0
validation finish
batch start
#iterations: 340
currently lose_sum: 516.5383417606354
time_elpased: 1.709
batch start
#iterations: 341
currently lose_sum: 518.9012567102909
time_elpased: 1.742
batch start
#iterations: 342
currently lose_sum: 521.74464443326
time_elpased: 1.732
batch start
#iterations: 343
currently lose_sum: 519.4252198338509
time_elpased: 1.684
batch start
#iterations: 344
currently lose_sum: 520.8517439961433
time_elpased: 1.715
batch start
#iterations: 345
currently lose_sum: 520.707551330328
time_elpased: 1.672
batch start
#iterations: 346
currently lose_sum: 518.9251741468906
time_elpased: 1.721
batch start
#iterations: 347
currently lose_sum: 519.216369509697
time_elpased: 1.691
batch start
#iterations: 348
currently lose_sum: 519.8972370922565
time_elpased: 1.676
batch start
#iterations: 349
currently lose_sum: 519.6081950962543
time_elpased: 1.681
batch start
#iterations: 350
currently lose_sum: 518.7183019816875
time_elpased: 1.746
batch start
#iterations: 351
currently lose_sum: 519.8190978765488
time_elpased: 1.72
batch start
#iterations: 352
currently lose_sum: 518.9842248558998
time_elpased: 1.765
batch start
#iterations: 353
currently lose_sum: 521.5780440866947
time_elpased: 1.68
batch start
#iterations: 354
currently lose_sum: 519.6857581138611
time_elpased: 1.725
batch start
#iterations: 355
currently lose_sum: 519.3415460884571
time_elpased: 1.772
batch start
#iterations: 356
currently lose_sum: 518.3200975358486
time_elpased: 1.698
batch start
#iterations: 357
currently lose_sum: 518.9326260983944
time_elpased: 1.684
batch start
#iterations: 358
currently lose_sum: 519.5104221999645
time_elpased: 1.705
batch start
#iterations: 359
currently lose_sum: 518.9506478309631
time_elpased: 1.759
start validation test
0.0979381443299
1.0
0.0979381443299
0.178403755869
0
validation finish
batch start
#iterations: 360
currently lose_sum: 519.4270331561565
time_elpased: 1.689
batch start
#iterations: 361
currently lose_sum: 517.2153277397156
time_elpased: 1.733
batch start
#iterations: 362
currently lose_sum: 519.2360074222088
time_elpased: 1.742
batch start
#iterations: 363
currently lose_sum: 518.7755759060383
time_elpased: 1.745
batch start
#iterations: 364
currently lose_sum: 520.0917011201382
time_elpased: 1.678
batch start
#iterations: 365
currently lose_sum: 518.2417446374893
time_elpased: 1.719
batch start
#iterations: 366
currently lose_sum: 519.5196577608585
time_elpased: 1.677
batch start
#iterations: 367
currently lose_sum: 519.1929901540279
time_elpased: 1.676
batch start
#iterations: 368
currently lose_sum: 517.7033737301826
time_elpased: 1.769
batch start
#iterations: 369
currently lose_sum: 516.9746450483799
time_elpased: 1.688
batch start
#iterations: 370
currently lose_sum: 517.5593943297863
time_elpased: 1.674
batch start
#iterations: 371
currently lose_sum: 519.2537494599819
time_elpased: 1.674
batch start
#iterations: 372
currently lose_sum: 518.501875847578
time_elpased: 1.696
batch start
#iterations: 373
currently lose_sum: 519.4001975953579
time_elpased: 1.735
batch start
#iterations: 374
currently lose_sum: 520.3503318727016
time_elpased: 1.681
batch start
#iterations: 375
currently lose_sum: 520.1949411928654
time_elpased: 1.717
batch start
#iterations: 376
currently lose_sum: 519.6987951695919
time_elpased: 1.762
batch start
#iterations: 377
currently lose_sum: 518.2601636946201
time_elpased: 1.691
batch start
#iterations: 378
currently lose_sum: 519.8525013327599
time_elpased: 1.692
batch start
#iterations: 379
currently lose_sum: 519.042380720377
time_elpased: 1.672
start validation test
0.152268041237
1.0
0.152268041237
0.264292744028
0
validation finish
batch start
#iterations: 380
currently lose_sum: 518.6030936539173
time_elpased: 1.683
batch start
#iterations: 381
currently lose_sum: 517.5447968840599
time_elpased: 1.673
batch start
#iterations: 382
currently lose_sum: 518.237935602665
time_elpased: 1.75
batch start
#iterations: 383
currently lose_sum: 515.9919949173927
time_elpased: 1.672
batch start
#iterations: 384
currently lose_sum: 515.9553351402283
time_elpased: 1.723
batch start
#iterations: 385
currently lose_sum: 517.5193053483963
time_elpased: 1.689
batch start
#iterations: 386
currently lose_sum: 517.8027419447899
time_elpased: 1.747
batch start
#iterations: 387
currently lose_sum: 517.302302211523
time_elpased: 1.684
batch start
#iterations: 388
currently lose_sum: 518.3032289743423
time_elpased: 1.71
batch start
#iterations: 389
currently lose_sum: 518.4414673447609
time_elpased: 1.742
batch start
#iterations: 390
currently lose_sum: 517.7305036187172
time_elpased: 1.767
batch start
#iterations: 391
currently lose_sum: 519.8087671101093
time_elpased: 1.688
batch start
#iterations: 392
currently lose_sum: 518.2860245108604
time_elpased: 1.68
batch start
#iterations: 393
currently lose_sum: 517.3979644775391
time_elpased: 1.764
batch start
#iterations: 394
currently lose_sum: 517.9934082329273
time_elpased: 1.726
batch start
#iterations: 395
currently lose_sum: 518.1955799162388
time_elpased: 1.688
batch start
#iterations: 396
currently lose_sum: 518.3337306678295
time_elpased: 1.685
batch start
#iterations: 397
currently lose_sum: 519.2881584763527
time_elpased: 1.698
batch start
#iterations: 398
currently lose_sum: 517.8002628087997
time_elpased: 1.714
batch start
#iterations: 399
currently lose_sum: 518.0132446587086
time_elpased: 1.675
start validation test
0.123505154639
1.0
0.123505154639
0.219856854469
0
validation finish
acc: 0.241
pre: 1.000
rec: 0.241
F1: 0.388
auc: 0.000
