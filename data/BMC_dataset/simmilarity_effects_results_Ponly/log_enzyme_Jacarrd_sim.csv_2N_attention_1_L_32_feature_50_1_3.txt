start to construct graph
graph construct over
87453
epochs start
batch start
#iterations: 0
currently lose_sum: 562.5776796936989
time_elpased: 1.705
batch start
#iterations: 1
currently lose_sum: 548.7110866308212
time_elpased: 1.675
batch start
#iterations: 2
currently lose_sum: 541.2723897695541
time_elpased: 1.659
batch start
#iterations: 3
currently lose_sum: 537.4554083049297
time_elpased: 1.664
batch start
#iterations: 4
currently lose_sum: 533.5653490424156
time_elpased: 1.675
batch start
#iterations: 5
currently lose_sum: 533.3006638884544
time_elpased: 1.65
batch start
#iterations: 6
currently lose_sum: 532.461873292923
time_elpased: 1.687
batch start
#iterations: 7
currently lose_sum: 530.6674668192863
time_elpased: 1.67
batch start
#iterations: 8
currently lose_sum: 531.1130341887474
time_elpased: 1.66
batch start
#iterations: 9
currently lose_sum: 528.6054463982582
time_elpased: 1.724
batch start
#iterations: 10
currently lose_sum: 528.4583231508732
time_elpased: 1.671
batch start
#iterations: 11
currently lose_sum: 526.9196101725101
time_elpased: 1.648
batch start
#iterations: 12
currently lose_sum: 530.1264284849167
time_elpased: 1.664
batch start
#iterations: 13
currently lose_sum: 526.872497022152
time_elpased: 1.752
batch start
#iterations: 14
currently lose_sum: 528.1865742504597
time_elpased: 1.675
batch start
#iterations: 15
currently lose_sum: 526.9292640089989
time_elpased: 1.684
batch start
#iterations: 16
currently lose_sum: 526.6848303079605
time_elpased: 1.672
batch start
#iterations: 17
currently lose_sum: 526.5956454277039
time_elpased: 1.674
batch start
#iterations: 18
currently lose_sum: 527.1585027575493
time_elpased: 1.712
batch start
#iterations: 19
currently lose_sum: 525.7302533388138
time_elpased: 1.663
start validation test
0.0855670103093
1.0
0.0855670103093
0.157644824311
0
validation finish
batch start
#iterations: 20
currently lose_sum: 528.3998119831085
time_elpased: 1.676
batch start
#iterations: 21
currently lose_sum: 525.6942420899868
time_elpased: 1.727
batch start
#iterations: 22
currently lose_sum: 527.2210408747196
time_elpased: 1.667
batch start
#iterations: 23
currently lose_sum: 526.355155736208
time_elpased: 1.651
batch start
#iterations: 24
currently lose_sum: 525.9650188684464
time_elpased: 1.66
batch start
#iterations: 25
currently lose_sum: 524.4962069392204
time_elpased: 1.668
batch start
#iterations: 26
currently lose_sum: 524.4302106797695
time_elpased: 1.657
batch start
#iterations: 27
currently lose_sum: 525.4245384335518
time_elpased: 1.762
batch start
#iterations: 28
currently lose_sum: 524.079710483551
time_elpased: 1.665
batch start
#iterations: 29
currently lose_sum: 523.0755213499069
time_elpased: 1.68
batch start
#iterations: 30
currently lose_sum: 523.2793805301189
time_elpased: 1.672
batch start
#iterations: 31
currently lose_sum: 524.9707870185375
time_elpased: 1.658
batch start
#iterations: 32
currently lose_sum: 525.6331326067448
time_elpased: 1.662
batch start
#iterations: 33
currently lose_sum: 523.8549491763115
time_elpased: 1.667
batch start
#iterations: 34
currently lose_sum: 523.2897116839886
time_elpased: 1.651
batch start
#iterations: 35
currently lose_sum: 524.6140438616276
time_elpased: 1.722
batch start
#iterations: 36
currently lose_sum: 525.17333522439
time_elpased: 1.661
batch start
#iterations: 37
currently lose_sum: 523.385744959116
time_elpased: 1.711
batch start
#iterations: 38
currently lose_sum: 524.6971507966518
time_elpased: 1.718
batch start
#iterations: 39
currently lose_sum: 524.2679868936539
time_elpased: 1.654
start validation test
0.160927835052
1.0
0.160927835052
0.277240031969
0
validation finish
batch start
#iterations: 40
currently lose_sum: 523.7357506453991
time_elpased: 1.647
batch start
#iterations: 41
currently lose_sum: 524.7103987038136
time_elpased: 1.714
batch start
#iterations: 42
currently lose_sum: 524.3152795135975
time_elpased: 1.706
batch start
#iterations: 43
currently lose_sum: 526.1259779930115
time_elpased: 1.68
batch start
#iterations: 44
currently lose_sum: 522.5624670088291
time_elpased: 1.666
batch start
#iterations: 45
currently lose_sum: 521.9784573614597
time_elpased: 1.715
batch start
#iterations: 46
currently lose_sum: 526.0685212314129
time_elpased: 1.66
batch start
#iterations: 47
currently lose_sum: 524.6614801883698
time_elpased: 1.648
batch start
#iterations: 48
currently lose_sum: 520.7077576518059
time_elpased: 1.662
batch start
#iterations: 49
currently lose_sum: 523.5972945690155
time_elpased: 1.671
batch start
#iterations: 50
currently lose_sum: 521.2702268064022
time_elpased: 1.664
batch start
#iterations: 51
currently lose_sum: 524.6248588562012
time_elpased: 1.674
batch start
#iterations: 52
currently lose_sum: 525.257729768753
time_elpased: 1.658
batch start
#iterations: 53
currently lose_sum: 523.9689227938652
time_elpased: 1.678
batch start
#iterations: 54
currently lose_sum: 522.9028065502644
time_elpased: 1.726
batch start
#iterations: 55
currently lose_sum: 521.7786503434181
time_elpased: 1.665
batch start
#iterations: 56
currently lose_sum: 522.6079076230526
time_elpased: 1.655
batch start
#iterations: 57
currently lose_sum: 521.0228769481182
time_elpased: 1.68
batch start
#iterations: 58
currently lose_sum: 522.7018840909004
time_elpased: 1.715
batch start
#iterations: 59
currently lose_sum: 523.120318710804
time_elpased: 1.72
start validation test
0.111443298969
1.0
0.111443298969
0.200537983489
0
validation finish
batch start
#iterations: 60
currently lose_sum: 522.2391713559628
time_elpased: 1.66
batch start
#iterations: 61
currently lose_sum: 522.4449408650398
time_elpased: 1.659
batch start
#iterations: 62
currently lose_sum: 522.5536647737026
time_elpased: 1.67
batch start
#iterations: 63
currently lose_sum: 525.0809313356876
time_elpased: 1.652
batch start
#iterations: 64
currently lose_sum: 522.1206959187984
time_elpased: 1.657
batch start
#iterations: 65
currently lose_sum: 523.9296422600746
time_elpased: 1.675
batch start
#iterations: 66
currently lose_sum: 521.4260207116604
time_elpased: 1.727
batch start
#iterations: 67
currently lose_sum: 520.4227786362171
time_elpased: 1.717
batch start
#iterations: 68
currently lose_sum: 520.3810945749283
time_elpased: 1.676
batch start
#iterations: 69
currently lose_sum: 521.9982197582722
time_elpased: 1.655
batch start
#iterations: 70
currently lose_sum: 522.3232346773148
time_elpased: 1.669
batch start
#iterations: 71
currently lose_sum: 522.5556906461716
time_elpased: 1.663
batch start
#iterations: 72
currently lose_sum: 521.8227471411228
time_elpased: 1.747
batch start
#iterations: 73
currently lose_sum: 522.3068050742149
time_elpased: 1.703
batch start
#iterations: 74
currently lose_sum: 521.6872925758362
time_elpased: 1.645
batch start
#iterations: 75
currently lose_sum: 522.5124355256557
time_elpased: 1.681
batch start
#iterations: 76
currently lose_sum: 522.6849842071533
time_elpased: 1.657
batch start
#iterations: 77
currently lose_sum: 521.4223498702049
time_elpased: 1.671
batch start
#iterations: 78
currently lose_sum: 521.7598030567169
time_elpased: 1.758
batch start
#iterations: 79
currently lose_sum: 521.6859146952629
time_elpased: 1.744
start validation test
0.186288659794
1.0
0.186288659794
0.314069696706
0
validation finish
batch start
#iterations: 80
currently lose_sum: 521.06096175313
time_elpased: 1.667
batch start
#iterations: 81
currently lose_sum: 523.3388357460499
time_elpased: 1.678
batch start
#iterations: 82
currently lose_sum: 522.4002012908459
time_elpased: 1.731
batch start
#iterations: 83
currently lose_sum: 519.5325938165188
time_elpased: 1.657
batch start
#iterations: 84
currently lose_sum: 521.2355851531029
time_elpased: 1.724
batch start
#iterations: 85
currently lose_sum: 523.0417664349079
time_elpased: 1.672
batch start
#iterations: 86
currently lose_sum: 522.1418361663818
time_elpased: 1.675
batch start
#iterations: 87
currently lose_sum: 521.8994355499744
time_elpased: 1.678
batch start
#iterations: 88
currently lose_sum: 522.5440487861633
time_elpased: 1.653
batch start
#iterations: 89
currently lose_sum: 520.1066790223122
time_elpased: 1.718
batch start
#iterations: 90
currently lose_sum: 521.7883667349815
time_elpased: 1.673
batch start
#iterations: 91
currently lose_sum: 519.7366407215595
time_elpased: 1.696
batch start
#iterations: 92
currently lose_sum: 520.1082584857941
time_elpased: 1.659
batch start
#iterations: 93
currently lose_sum: 519.9741201102734
time_elpased: 1.669
batch start
#iterations: 94
currently lose_sum: 521.3999896347523
time_elpased: 1.669
batch start
#iterations: 95
currently lose_sum: 520.4831110239029
time_elpased: 1.659
batch start
#iterations: 96
currently lose_sum: 520.6285555660725
time_elpased: 1.701
batch start
#iterations: 97
currently lose_sum: 523.777655094862
time_elpased: 1.665
batch start
#iterations: 98
currently lose_sum: 519.5244970023632
time_elpased: 1.708
batch start
#iterations: 99
currently lose_sum: 522.0036563277245
time_elpased: 1.659
start validation test
0.129587628866
1.0
0.129587628866
0.229442365611
0
validation finish
batch start
#iterations: 100
currently lose_sum: 521.5850737690926
time_elpased: 1.731
batch start
#iterations: 101
currently lose_sum: 521.0925471782684
time_elpased: 1.659
batch start
#iterations: 102
currently lose_sum: 522.7468971610069
time_elpased: 1.72
batch start
#iterations: 103
currently lose_sum: 520.7063175737858
time_elpased: 1.676
batch start
#iterations: 104
currently lose_sum: 519.784240424633
time_elpased: 1.663
batch start
#iterations: 105
currently lose_sum: 520.0415103435516
time_elpased: 1.735
batch start
#iterations: 106
currently lose_sum: 521.9106896817684
time_elpased: 1.751
batch start
#iterations: 107
currently lose_sum: 521.0328473448753
time_elpased: 1.672
batch start
#iterations: 108
currently lose_sum: 520.6057268679142
time_elpased: 1.741
batch start
#iterations: 109
currently lose_sum: 523.6792892217636
time_elpased: 1.657
batch start
#iterations: 110
currently lose_sum: 522.0017201900482
time_elpased: 1.659
batch start
#iterations: 111
currently lose_sum: 522.0322661697865
time_elpased: 1.695
batch start
#iterations: 112
currently lose_sum: 520.4560104310513
time_elpased: 1.728
batch start
#iterations: 113
currently lose_sum: 522.124747812748
time_elpased: 1.67
batch start
#iterations: 114
currently lose_sum: 521.8390702307224
time_elpased: 1.679
batch start
#iterations: 115
currently lose_sum: 522.8275451362133
time_elpased: 1.727
batch start
#iterations: 116
currently lose_sum: 521.0002927780151
time_elpased: 1.716
batch start
#iterations: 117
currently lose_sum: 519.9836021363735
time_elpased: 1.664
batch start
#iterations: 118
currently lose_sum: 522.1962565481663
time_elpased: 1.657
batch start
#iterations: 119
currently lose_sum: 520.1556199789047
time_elpased: 1.675
start validation test
0.126391752577
1.0
0.126391752577
0.2244188175
0
validation finish
batch start
#iterations: 120
currently lose_sum: 520.9143893420696
time_elpased: 1.69
batch start
#iterations: 121
currently lose_sum: 521.3677499592304
time_elpased: 1.665
batch start
#iterations: 122
currently lose_sum: 523.3432866632938
time_elpased: 1.68
batch start
#iterations: 123
currently lose_sum: 523.2307645976543
time_elpased: 1.689
batch start
#iterations: 124
currently lose_sum: 520.915048032999
time_elpased: 1.675
batch start
#iterations: 125
currently lose_sum: 519.0151075720787
time_elpased: 1.665
batch start
#iterations: 126
currently lose_sum: 519.2205118238926
time_elpased: 1.706
batch start
#iterations: 127
currently lose_sum: 520.7614796161652
time_elpased: 1.672
batch start
#iterations: 128
currently lose_sum: 521.2497355639935
time_elpased: 1.721
batch start
#iterations: 129
currently lose_sum: 520.0171061754227
time_elpased: 1.676
batch start
#iterations: 130
currently lose_sum: 519.325635612011
time_elpased: 1.665
batch start
#iterations: 131
currently lose_sum: 520.1831561625004
time_elpased: 1.654
batch start
#iterations: 132
currently lose_sum: 520.2203109264374
time_elpased: 1.702
batch start
#iterations: 133
currently lose_sum: 519.9415440261364
time_elpased: 1.763
batch start
#iterations: 134
currently lose_sum: 519.4429869055748
time_elpased: 1.662
batch start
#iterations: 135
currently lose_sum: 520.4830600321293
time_elpased: 1.657
batch start
#iterations: 136
currently lose_sum: 522.4334615767002
time_elpased: 1.672
batch start
#iterations: 137
currently lose_sum: 519.9714656770229
time_elpased: 1.663
batch start
#iterations: 138
currently lose_sum: 522.1802390813828
time_elpased: 1.695
batch start
#iterations: 139
currently lose_sum: 521.2322188615799
time_elpased: 1.676
start validation test
0.176082474227
1.0
0.176082474227
0.299438990182
0
validation finish
batch start
#iterations: 140
currently lose_sum: 519.8544451892376
time_elpased: 1.695
batch start
#iterations: 141
currently lose_sum: 519.4168545901775
time_elpased: 1.726
batch start
#iterations: 142
currently lose_sum: 519.3011013567448
time_elpased: 1.67
batch start
#iterations: 143
currently lose_sum: 519.5121346414089
time_elpased: 1.748
batch start
#iterations: 144
currently lose_sum: 520.616642177105
time_elpased: 1.729
batch start
#iterations: 145
currently lose_sum: 521.7384866178036
time_elpased: 1.664
batch start
#iterations: 146
currently lose_sum: 521.08742877841
time_elpased: 1.68
batch start
#iterations: 147
currently lose_sum: 519.8629127144814
time_elpased: 1.747
batch start
#iterations: 148
currently lose_sum: 519.2085308730602
time_elpased: 1.677
batch start
#iterations: 149
currently lose_sum: 522.001537591219
time_elpased: 1.744
batch start
#iterations: 150
currently lose_sum: 521.2483492791653
time_elpased: 1.692
batch start
#iterations: 151
currently lose_sum: 519.6903927922249
time_elpased: 1.668
batch start
#iterations: 152
currently lose_sum: 519.8518823385239
time_elpased: 1.672
batch start
#iterations: 153
currently lose_sum: 518.5274927318096
time_elpased: 1.655
batch start
#iterations: 154
currently lose_sum: 521.7643591463566
time_elpased: 1.667
batch start
#iterations: 155
currently lose_sum: 522.3302716016769
time_elpased: 1.719
batch start
#iterations: 156
currently lose_sum: 520.8434811532497
time_elpased: 1.758
batch start
#iterations: 157
currently lose_sum: 519.8862549960613
time_elpased: 1.711
batch start
#iterations: 158
currently lose_sum: 521.1022841334343
time_elpased: 1.681
batch start
#iterations: 159
currently lose_sum: 519.78436216712
time_elpased: 1.732
start validation test
0.173608247423
1.0
0.173608247423
0.295853829937
0
validation finish
batch start
#iterations: 160
currently lose_sum: 520.818422973156
time_elpased: 1.701
batch start
#iterations: 161
currently lose_sum: 520.188503921032
time_elpased: 1.704
batch start
#iterations: 162
currently lose_sum: 519.0206786692142
time_elpased: 1.762
batch start
#iterations: 163
currently lose_sum: 519.6909990310669
time_elpased: 1.704
batch start
#iterations: 164
currently lose_sum: 519.6486035883427
time_elpased: 1.751
batch start
#iterations: 165
currently lose_sum: 519.4254522323608
time_elpased: 1.666
batch start
#iterations: 166
currently lose_sum: 519.0938929915428
time_elpased: 1.684
batch start
#iterations: 167
currently lose_sum: 519.2876736223698
time_elpased: 1.67
batch start
#iterations: 168
currently lose_sum: 519.962443292141
time_elpased: 1.668
batch start
#iterations: 169
currently lose_sum: 519.2633891701698
time_elpased: 1.659
batch start
#iterations: 170
currently lose_sum: 521.3880761265755
time_elpased: 1.685
batch start
#iterations: 171
currently lose_sum: 521.27861982584
time_elpased: 1.747
batch start
#iterations: 172
currently lose_sum: 519.7725785970688
time_elpased: 1.741
batch start
#iterations: 173
currently lose_sum: 518.9369811117649
time_elpased: 1.683
batch start
#iterations: 174
currently lose_sum: 519.8021258115768
time_elpased: 1.657
batch start
#iterations: 175
currently lose_sum: 518.4154337048531
time_elpased: 1.654
batch start
#iterations: 176
currently lose_sum: 521.2020945847034
time_elpased: 1.744
batch start
#iterations: 177
currently lose_sum: 520.2218216061592
time_elpased: 1.67
batch start
#iterations: 178
currently lose_sum: 519.4943471252918
time_elpased: 1.685
batch start
#iterations: 179
currently lose_sum: 521.9571706950665
time_elpased: 1.743
start validation test
0.196907216495
1.0
0.196907216495
0.32902670112
0
validation finish
batch start
#iterations: 180
currently lose_sum: 520.5193607211113
time_elpased: 1.673
batch start
#iterations: 181
currently lose_sum: 522.1964364945889
time_elpased: 1.716
batch start
#iterations: 182
currently lose_sum: 519.2159655690193
time_elpased: 1.655
batch start
#iterations: 183
currently lose_sum: 519.6528733372688
time_elpased: 1.664
batch start
#iterations: 184
currently lose_sum: 520.1488648056984
time_elpased: 1.656
batch start
#iterations: 185
currently lose_sum: 520.1957306265831
time_elpased: 1.663
batch start
#iterations: 186
currently lose_sum: 519.5482064187527
time_elpased: 1.674
batch start
#iterations: 187
currently lose_sum: 522.5587542653084
time_elpased: 1.726
batch start
#iterations: 188
currently lose_sum: 521.3354669809341
time_elpased: 1.662
batch start
#iterations: 189
currently lose_sum: 520.8613256514072
time_elpased: 1.724
batch start
#iterations: 190
currently lose_sum: 521.0997496247292
time_elpased: 1.654
batch start
#iterations: 191
currently lose_sum: 519.7277974188328
time_elpased: 1.718
batch start
#iterations: 192
currently lose_sum: 518.86972412467
time_elpased: 1.669
batch start
#iterations: 193
currently lose_sum: 519.7226172089577
time_elpased: 1.66
batch start
#iterations: 194
currently lose_sum: 521.5569495558739
time_elpased: 1.653
batch start
#iterations: 195
currently lose_sum: 519.0232937037945
time_elpased: 1.741
batch start
#iterations: 196
currently lose_sum: 521.841093480587
time_elpased: 1.668
batch start
#iterations: 197
currently lose_sum: 520.5726725459099
time_elpased: 1.672
batch start
#iterations: 198
currently lose_sum: 521.2596178352833
time_elpased: 1.668
batch start
#iterations: 199
currently lose_sum: 521.5213187634945
time_elpased: 1.679
start validation test
0.106494845361
1.0
0.106494845361
0.192490450014
0
validation finish
batch start
#iterations: 200
currently lose_sum: 517.0574138760567
time_elpased: 1.677
batch start
#iterations: 201
currently lose_sum: 520.5129972994328
time_elpased: 1.659
batch start
#iterations: 202
currently lose_sum: 519.3778749406338
time_elpased: 1.675
batch start
#iterations: 203
currently lose_sum: 520.108739554882
time_elpased: 1.714
batch start
#iterations: 204
currently lose_sum: 519.791999399662
time_elpased: 1.723
batch start
#iterations: 205
currently lose_sum: 519.4219085276127
time_elpased: 1.694
batch start
#iterations: 206
currently lose_sum: 518.9729229807854
time_elpased: 1.781
batch start
#iterations: 207
currently lose_sum: 518.3957304358482
time_elpased: 1.705
batch start
#iterations: 208
currently lose_sum: 520.4906958937645
time_elpased: 1.708
batch start
#iterations: 209
currently lose_sum: 519.9242864251137
time_elpased: 1.666
batch start
#iterations: 210
currently lose_sum: 520.2652787268162
time_elpased: 1.663
batch start
#iterations: 211
currently lose_sum: 519.783755928278
time_elpased: 1.667
batch start
#iterations: 212
currently lose_sum: 519.7913987934589
time_elpased: 1.66
batch start
#iterations: 213
currently lose_sum: 522.0366690456867
time_elpased: 1.731
batch start
#iterations: 214
currently lose_sum: 522.3069643676281
time_elpased: 1.662
batch start
#iterations: 215
currently lose_sum: 521.4642989337444
time_elpased: 1.748
batch start
#iterations: 216
currently lose_sum: 520.8435132801533
time_elpased: 1.658
batch start
#iterations: 217
currently lose_sum: 519.2714334130287
time_elpased: 1.733
batch start
#iterations: 218
currently lose_sum: 520.2988680005074
time_elpased: 1.726
batch start
#iterations: 219
currently lose_sum: 520.4700618684292
time_elpased: 1.68
start validation test
0.0926804123711
1.0
0.0926804123711
0.169638645155
0
validation finish
batch start
#iterations: 220
currently lose_sum: 519.8942850232124
time_elpased: 1.672
batch start
#iterations: 221
currently lose_sum: 522.5364385843277
time_elpased: 1.689
batch start
#iterations: 222
currently lose_sum: 519.9624928236008
time_elpased: 1.66
batch start
#iterations: 223
currently lose_sum: 518.8021161854267
time_elpased: 1.665
batch start
#iterations: 224
currently lose_sum: 521.7996707856655
time_elpased: 1.714
batch start
#iterations: 225
currently lose_sum: 519.5427465438843
time_elpased: 1.691
batch start
#iterations: 226
currently lose_sum: 521.0274646878242
time_elpased: 1.676
batch start
#iterations: 227
currently lose_sum: 517.9228437840939
time_elpased: 1.668
batch start
#iterations: 228
currently lose_sum: 519.2149820923805
time_elpased: 1.671
batch start
#iterations: 229
currently lose_sum: 519.8395994901657
time_elpased: 1.702
batch start
#iterations: 230
currently lose_sum: 519.6504620015621
time_elpased: 1.673
batch start
#iterations: 231
currently lose_sum: 523.4556710422039
time_elpased: 1.666
batch start
#iterations: 232
currently lose_sum: 520.7416771352291
time_elpased: 1.679
batch start
#iterations: 233
currently lose_sum: 520.205762565136
time_elpased: 1.756
batch start
#iterations: 234
currently lose_sum: 519.6827040910721
time_elpased: 1.74
batch start
#iterations: 235
currently lose_sum: 519.358697116375
time_elpased: 1.726
batch start
#iterations: 236
currently lose_sum: 518.2899201512337
time_elpased: 1.68
batch start
#iterations: 237
currently lose_sum: 519.7126867771149
time_elpased: 1.668
batch start
#iterations: 238
currently lose_sum: 517.3885160684586
time_elpased: 1.683
batch start
#iterations: 239
currently lose_sum: 522.6566357910633
time_elpased: 1.694
start validation test
0.166288659794
1.0
0.166288659794
0.28515866702
0
validation finish
batch start
#iterations: 240
currently lose_sum: 520.2085950374603
time_elpased: 1.661
batch start
#iterations: 241
currently lose_sum: 520.6131020486355
time_elpased: 1.665
batch start
#iterations: 242
currently lose_sum: 520.5776353478432
time_elpased: 1.677
batch start
#iterations: 243
currently lose_sum: 518.6527975797653
time_elpased: 1.65
batch start
#iterations: 244
currently lose_sum: 521.9173620939255
time_elpased: 1.701
batch start
#iterations: 245
currently lose_sum: 520.1700847148895
time_elpased: 1.669
batch start
#iterations: 246
currently lose_sum: 520.3082746267319
time_elpased: 1.655
batch start
#iterations: 247
currently lose_sum: 520.8569615185261
time_elpased: 1.678
batch start
#iterations: 248
currently lose_sum: 517.4099786877632
time_elpased: 1.698
batch start
#iterations: 249
currently lose_sum: 521.1537380218506
time_elpased: 1.719
batch start
#iterations: 250
currently lose_sum: 519.7061001062393
time_elpased: 1.671
batch start
#iterations: 251
currently lose_sum: 518.8082470595837
time_elpased: 1.743
batch start
#iterations: 252
currently lose_sum: 521.4530831873417
time_elpased: 1.67
batch start
#iterations: 253
currently lose_sum: 519.1422609090805
time_elpased: 1.662
batch start
#iterations: 254
currently lose_sum: 521.4149878025055
time_elpased: 1.662
batch start
#iterations: 255
currently lose_sum: 519.4400120675564
time_elpased: 1.695
batch start
#iterations: 256
currently lose_sum: 519.9960149824619
time_elpased: 1.655
batch start
#iterations: 257
currently lose_sum: 517.8602889478207
time_elpased: 1.704
batch start
#iterations: 258
currently lose_sum: 517.6957805752754
time_elpased: 1.675
batch start
#iterations: 259
currently lose_sum: 520.4855427742004
time_elpased: 1.661
start validation test
0.181340206186
1.0
0.181340206186
0.307007592286
0
validation finish
batch start
#iterations: 260
currently lose_sum: 519.9303234517574
time_elpased: 1.663
batch start
#iterations: 261
currently lose_sum: 519.759231954813
time_elpased: 1.709
batch start
#iterations: 262
currently lose_sum: 520.1755631864071
time_elpased: 1.672
batch start
#iterations: 263
currently lose_sum: 519.4512398540974
time_elpased: 1.665
batch start
#iterations: 264
currently lose_sum: 520.1331032514572
time_elpased: 1.71
batch start
#iterations: 265
currently lose_sum: 521.0836182534695
time_elpased: 1.747
batch start
#iterations: 266
currently lose_sum: 521.4007451832294
time_elpased: 1.686
batch start
#iterations: 267
currently lose_sum: 521.1572875976562
time_elpased: 1.741
batch start
#iterations: 268
currently lose_sum: 517.7575453519821
time_elpased: 1.72
batch start
#iterations: 269
currently lose_sum: 517.9925253689289
time_elpased: 1.656
batch start
#iterations: 270
currently lose_sum: 521.3005108833313
time_elpased: 1.732
batch start
#iterations: 271
currently lose_sum: 521.0401445627213
time_elpased: 1.665
batch start
#iterations: 272
currently lose_sum: 520.0871996581554
time_elpased: 1.752
batch start
#iterations: 273
currently lose_sum: 519.2448390126228
time_elpased: 1.759
batch start
#iterations: 274
currently lose_sum: 519.9290888309479
time_elpased: 1.734
batch start
#iterations: 275
currently lose_sum: 517.815779030323
time_elpased: 1.675
batch start
#iterations: 276
currently lose_sum: 520.0314772129059
time_elpased: 1.663
batch start
#iterations: 277
currently lose_sum: 519.5108258128166
time_elpased: 1.693
batch start
#iterations: 278
currently lose_sum: 518.2493514716625
time_elpased: 1.667
batch start
#iterations: 279
currently lose_sum: 518.1798936724663
time_elpased: 1.66
start validation test
0.161546391753
1.0
0.161546391753
0.278157450963
0
validation finish
batch start
#iterations: 280
currently lose_sum: 522.8299695551395
time_elpased: 1.678
batch start
#iterations: 281
currently lose_sum: 520.0354353785515
time_elpased: 1.713
batch start
#iterations: 282
currently lose_sum: 520.392860263586
time_elpased: 1.686
batch start
#iterations: 283
currently lose_sum: 519.762205094099
time_elpased: 1.658
batch start
#iterations: 284
currently lose_sum: 519.2180652916431
time_elpased: 1.655
batch start
#iterations: 285
currently lose_sum: 517.701489508152
time_elpased: 1.659
batch start
#iterations: 286
currently lose_sum: 520.0195415616035
time_elpased: 1.714
batch start
#iterations: 287
currently lose_sum: 520.3900309801102
time_elpased: 1.654
batch start
#iterations: 288
currently lose_sum: 521.9681260883808
time_elpased: 1.7
batch start
#iterations: 289
currently lose_sum: 521.3095342516899
time_elpased: 1.693
batch start
#iterations: 290
currently lose_sum: 518.9881442189217
time_elpased: 1.678
batch start
#iterations: 291
currently lose_sum: 518.0789287984371
time_elpased: 1.69
batch start
#iterations: 292
currently lose_sum: 520.1447495520115
time_elpased: 1.686
batch start
#iterations: 293
currently lose_sum: 520.1922554671764
time_elpased: 1.667
batch start
#iterations: 294
currently lose_sum: 519.7667464613914
time_elpased: 1.673
batch start
#iterations: 295
currently lose_sum: 520.1369424164295
time_elpased: 1.651
batch start
#iterations: 296
currently lose_sum: 519.6340872943401
time_elpased: 1.667
batch start
#iterations: 297
currently lose_sum: 517.9964967370033
time_elpased: 1.677
batch start
#iterations: 298
currently lose_sum: 519.8489102125168
time_elpased: 1.711
batch start
#iterations: 299
currently lose_sum: 521.3140224814415
time_elpased: 1.68
start validation test
0.156804123711
1.0
0.156804123711
0.271098832546
0
validation finish
batch start
#iterations: 300
currently lose_sum: 520.2390183508396
time_elpased: 1.668
batch start
#iterations: 301
currently lose_sum: 519.5681235790253
time_elpased: 1.669
batch start
#iterations: 302
currently lose_sum: 520.5403650701046
time_elpased: 1.662
batch start
#iterations: 303
currently lose_sum: 520.2169753909111
time_elpased: 1.669
batch start
#iterations: 304
currently lose_sum: 518.3206018805504
time_elpased: 1.767
batch start
#iterations: 305
currently lose_sum: 519.6255335509777
time_elpased: 1.666
batch start
#iterations: 306
currently lose_sum: 519.1279716789722
time_elpased: 1.669
batch start
#iterations: 307
currently lose_sum: 518.7226337790489
time_elpased: 1.677
batch start
#iterations: 308
currently lose_sum: 521.3970395326614
time_elpased: 1.667
batch start
#iterations: 309
currently lose_sum: 519.137190580368
time_elpased: 1.746
batch start
#iterations: 310
currently lose_sum: 521.4219408035278
time_elpased: 1.663
batch start
#iterations: 311
currently lose_sum: 519.3131865262985
time_elpased: 1.722
batch start
#iterations: 312
currently lose_sum: 518.0750826001167
time_elpased: 1.653
batch start
#iterations: 313
currently lose_sum: 519.0694086253643
time_elpased: 1.676
batch start
#iterations: 314
currently lose_sum: 520.8753479421139
time_elpased: 1.667
batch start
#iterations: 315
currently lose_sum: 519.2731837034225
time_elpased: 1.674
batch start
#iterations: 316
currently lose_sum: 520.0545304715633
time_elpased: 1.723
batch start
#iterations: 317
currently lose_sum: 518.9072025418282
time_elpased: 1.68
batch start
#iterations: 318
currently lose_sum: 517.9463297724724
time_elpased: 1.729
batch start
#iterations: 319
currently lose_sum: 519.1385805904865
time_elpased: 1.656
start validation test
0.160515463918
1.0
0.160515463918
0.276627875988
0
validation finish
batch start
#iterations: 320
currently lose_sum: 522.0198740661144
time_elpased: 1.676
batch start
#iterations: 321
currently lose_sum: 519.1110257804394
time_elpased: 1.665
batch start
#iterations: 322
currently lose_sum: 516.7494477927685
time_elpased: 1.711
batch start
#iterations: 323
currently lose_sum: 521.730881690979
time_elpased: 1.673
batch start
#iterations: 324
currently lose_sum: 518.5006383359432
time_elpased: 1.694
batch start
#iterations: 325
currently lose_sum: 520.8852237164974
time_elpased: 1.719
batch start
#iterations: 326
currently lose_sum: 519.8222393095493
time_elpased: 1.685
batch start
#iterations: 327
currently lose_sum: 519.3383775055408
time_elpased: 1.688
batch start
#iterations: 328
currently lose_sum: 520.8078768253326
time_elpased: 1.689
batch start
#iterations: 329
currently lose_sum: 518.4923459291458
time_elpased: 1.673
batch start
#iterations: 330
currently lose_sum: 520.3501703739166
time_elpased: 1.731
batch start
#iterations: 331
currently lose_sum: 519.5846986472607
time_elpased: 1.714
batch start
#iterations: 332
currently lose_sum: 520.2539448142052
time_elpased: 1.667
batch start
#iterations: 333
currently lose_sum: 518.7420461773872
time_elpased: 1.678
batch start
#iterations: 334
currently lose_sum: 518.2308195531368
time_elpased: 1.744
batch start
#iterations: 335
currently lose_sum: 519.2837760746479
time_elpased: 1.751
batch start
#iterations: 336
currently lose_sum: 519.2470046579838
time_elpased: 1.675
batch start
#iterations: 337
currently lose_sum: 520.2367876172066
time_elpased: 1.74
batch start
#iterations: 338
currently lose_sum: 519.7115609645844
time_elpased: 1.664
batch start
#iterations: 339
currently lose_sum: 520.4010449051857
time_elpased: 1.679
start validation test
0.180412371134
1.0
0.180412371134
0.305676855895
0
validation finish
batch start
#iterations: 340
currently lose_sum: 520.8803408443928
time_elpased: 1.687
batch start
#iterations: 341
currently lose_sum: 519.6599299013615
time_elpased: 1.669
batch start
#iterations: 342
currently lose_sum: 518.7252581417561
time_elpased: 1.676
batch start
#iterations: 343
currently lose_sum: 520.6325603723526
time_elpased: 1.742
batch start
#iterations: 344
currently lose_sum: 521.3884750902653
time_elpased: 1.679
batch start
#iterations: 345
currently lose_sum: 521.7895056605339
time_elpased: 1.695
batch start
#iterations: 346
currently lose_sum: 517.213787317276
time_elpased: 1.751
batch start
#iterations: 347
currently lose_sum: 519.4467036724091
time_elpased: 1.667
batch start
#iterations: 348
currently lose_sum: 520.2477802038193
time_elpased: 1.744
batch start
#iterations: 349
currently lose_sum: 519.8327377736568
time_elpased: 1.671
batch start
#iterations: 350
currently lose_sum: 519.5042724013329
time_elpased: 1.763
batch start
#iterations: 351
currently lose_sum: 520.8153041899204
time_elpased: 1.716
batch start
#iterations: 352
currently lose_sum: 520.1007786095142
time_elpased: 1.663
batch start
#iterations: 353
currently lose_sum: 519.6610841751099
time_elpased: 1.669
batch start
#iterations: 354
currently lose_sum: 521.3619258105755
time_elpased: 1.75
batch start
#iterations: 355
currently lose_sum: 521.4373658895493
time_elpased: 1.676
batch start
#iterations: 356
currently lose_sum: 518.5903752446175
time_elpased: 1.729
batch start
#iterations: 357
currently lose_sum: 519.4670888483524
time_elpased: 1.731
batch start
#iterations: 358
currently lose_sum: 518.9489102065563
time_elpased: 1.675
batch start
#iterations: 359
currently lose_sum: 518.9392922520638
time_elpased: 1.685
start validation test
0.170515463918
1.0
0.170515463918
0.291351065704
0
validation finish
batch start
#iterations: 360
currently lose_sum: 519.2836152911186
time_elpased: 1.67
batch start
#iterations: 361
currently lose_sum: 519.7197363674641
time_elpased: 1.73
batch start
#iterations: 362
currently lose_sum: 520.847990244627
time_elpased: 1.727
batch start
#iterations: 363
currently lose_sum: 518.4227155148983
time_elpased: 1.766
batch start
#iterations: 364
currently lose_sum: 519.1430999338627
time_elpased: 1.741
batch start
#iterations: 365
currently lose_sum: 517.4309809207916
time_elpased: 1.659
batch start
#iterations: 366
currently lose_sum: 519.9851530194283
time_elpased: 1.763
batch start
#iterations: 367
currently lose_sum: 522.4392160773277
time_elpased: 1.661
batch start
#iterations: 368
currently lose_sum: 518.2898785173893
time_elpased: 1.719
batch start
#iterations: 369
currently lose_sum: 519.507004827261
time_elpased: 1.672
batch start
#iterations: 370
currently lose_sum: 521.1425296664238
time_elpased: 1.682
batch start
#iterations: 371
currently lose_sum: 517.9057020545006
time_elpased: 1.681
batch start
#iterations: 372
currently lose_sum: 521.2198044657707
time_elpased: 1.738
batch start
#iterations: 373
currently lose_sum: 522.2452407777309
time_elpased: 1.662
batch start
#iterations: 374
currently lose_sum: 520.3010492622852
time_elpased: 1.752
batch start
#iterations: 375
currently lose_sum: 519.7813940048218
time_elpased: 1.67
batch start
#iterations: 376
currently lose_sum: 518.1953861713409
time_elpased: 1.688
batch start
#iterations: 377
currently lose_sum: 518.935709476471
time_elpased: 1.674
batch start
#iterations: 378
currently lose_sum: 519.7109823524952
time_elpased: 1.748
batch start
#iterations: 379
currently lose_sum: 517.2437309026718
time_elpased: 1.675
start validation test
0.179793814433
1.0
0.179793814433
0.304788535477
0
validation finish
batch start
#iterations: 380
currently lose_sum: 519.0939974784851
time_elpased: 1.76
batch start
#iterations: 381
currently lose_sum: 518.1167781352997
time_elpased: 1.676
batch start
#iterations: 382
currently lose_sum: 517.5516988635063
time_elpased: 1.668
batch start
#iterations: 383
currently lose_sum: 518.3747390508652
time_elpased: 1.755
batch start
#iterations: 384
currently lose_sum: 518.9620113968849
time_elpased: 1.663
batch start
#iterations: 385
currently lose_sum: 520.1277593672276
time_elpased: 1.667
batch start
#iterations: 386
currently lose_sum: 521.6969363093376
time_elpased: 1.696
batch start
#iterations: 387
currently lose_sum: 518.0492227971554
time_elpased: 1.748
batch start
#iterations: 388
currently lose_sum: 518.8828325271606
time_elpased: 1.672
batch start
#iterations: 389
currently lose_sum: 520.2249887287617
time_elpased: 1.653
batch start
#iterations: 390
currently lose_sum: 519.2588529884815
time_elpased: 1.672
batch start
#iterations: 391
currently lose_sum: 518.2280592620373
time_elpased: 1.706
batch start
#iterations: 392
currently lose_sum: 519.9557859301567
time_elpased: 1.711
batch start
#iterations: 393
currently lose_sum: 521.0002451241016
time_elpased: 1.7
batch start
#iterations: 394
currently lose_sum: 519.1607542932034
time_elpased: 1.679
batch start
#iterations: 395
currently lose_sum: 520.6591206490993
time_elpased: 1.66
batch start
#iterations: 396
currently lose_sum: 518.2581666707993
time_elpased: 1.758
batch start
#iterations: 397
currently lose_sum: 520.1668134331703
time_elpased: 1.688
batch start
#iterations: 398
currently lose_sum: 518.8665245771408
time_elpased: 1.743
batch start
#iterations: 399
currently lose_sum: 520.0004354417324
time_elpased: 1.689
start validation test
0.157319587629
1.0
0.157319587629
0.271868875824
0
validation finish
acc: 0.196
pre: 1.000
rec: 0.196
F1: 0.328
auc: 0.000
