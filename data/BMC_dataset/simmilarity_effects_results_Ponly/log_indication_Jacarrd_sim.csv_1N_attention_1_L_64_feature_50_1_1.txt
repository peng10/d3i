start to construct graph
graph construct over
58300
epochs start
batch start
#iterations: 0
currently lose_sum: 393.44577622413635
time_elpased: 1.215
batch start
#iterations: 1
currently lose_sum: 385.5439240336418
time_elpased: 1.18
batch start
#iterations: 2
currently lose_sum: 380.6687778234482
time_elpased: 1.188
batch start
#iterations: 3
currently lose_sum: 375.79747700691223
time_elpased: 1.188
batch start
#iterations: 4
currently lose_sum: 374.64709609746933
time_elpased: 1.197
batch start
#iterations: 5
currently lose_sum: 372.7459236383438
time_elpased: 1.18
batch start
#iterations: 6
currently lose_sum: 372.6765133738518
time_elpased: 1.175
batch start
#iterations: 7
currently lose_sum: 371.93845534324646
time_elpased: 1.186
batch start
#iterations: 8
currently lose_sum: 368.99655973911285
time_elpased: 1.186
batch start
#iterations: 9
currently lose_sum: 368.4642371535301
time_elpased: 1.192
batch start
#iterations: 10
currently lose_sum: 368.7846137881279
time_elpased: 1.189
batch start
#iterations: 11
currently lose_sum: 367.9033876657486
time_elpased: 1.183
batch start
#iterations: 12
currently lose_sum: 366.91844499111176
time_elpased: 1.211
batch start
#iterations: 13
currently lose_sum: 366.46848571300507
time_elpased: 1.181
batch start
#iterations: 14
currently lose_sum: 366.3920610547066
time_elpased: 1.187
batch start
#iterations: 15
currently lose_sum: 365.75300484895706
time_elpased: 1.184
batch start
#iterations: 16
currently lose_sum: 365.01639980077744
time_elpased: 1.192
batch start
#iterations: 17
currently lose_sum: 364.21339029073715
time_elpased: 1.201
batch start
#iterations: 18
currently lose_sum: 365.1841242313385
time_elpased: 1.178
batch start
#iterations: 19
currently lose_sum: 364.25534623861313
time_elpased: 1.223
start validation test
0.710618556701
1.0
0.710618556701
0.830832278672
0
validation finish
batch start
#iterations: 20
currently lose_sum: 364.04740911722183
time_elpased: 1.18
batch start
#iterations: 21
currently lose_sum: 363.02412807941437
time_elpased: 1.185
batch start
#iterations: 22
currently lose_sum: 362.7865301370621
time_elpased: 1.197
batch start
#iterations: 23
currently lose_sum: 362.956523001194
time_elpased: 1.228
batch start
#iterations: 24
currently lose_sum: 363.2356444001198
time_elpased: 1.182
batch start
#iterations: 25
currently lose_sum: 362.0828899741173
time_elpased: 1.187
batch start
#iterations: 26
currently lose_sum: 361.96887534856796
time_elpased: 1.174
batch start
#iterations: 27
currently lose_sum: 361.8652811050415
time_elpased: 1.183
batch start
#iterations: 28
currently lose_sum: 361.6046008467674
time_elpased: 1.186
batch start
#iterations: 29
currently lose_sum: 362.5167169570923
time_elpased: 1.225
batch start
#iterations: 30
currently lose_sum: 361.1915475130081
time_elpased: 1.2
batch start
#iterations: 31
currently lose_sum: 362.72148644924164
time_elpased: 1.205
batch start
#iterations: 32
currently lose_sum: 360.4831473827362
time_elpased: 1.186
batch start
#iterations: 33
currently lose_sum: 360.39210879802704
time_elpased: 1.197
batch start
#iterations: 34
currently lose_sum: 359.6536138653755
time_elpased: 1.207
batch start
#iterations: 35
currently lose_sum: 360.1370053887367
time_elpased: 1.179
batch start
#iterations: 36
currently lose_sum: 359.5834332406521
time_elpased: 1.194
batch start
#iterations: 37
currently lose_sum: 360.266881108284
time_elpased: 1.192
batch start
#iterations: 38
currently lose_sum: 359.71545428037643
time_elpased: 1.18
batch start
#iterations: 39
currently lose_sum: 360.141945540905
time_elpased: 1.204
start validation test
0.705773195876
1.0
0.705773195876
0.82751118095
0
validation finish
batch start
#iterations: 40
currently lose_sum: 360.9981642961502
time_elpased: 1.183
batch start
#iterations: 41
currently lose_sum: 359.9857739210129
time_elpased: 1.181
batch start
#iterations: 42
currently lose_sum: 359.4011163711548
time_elpased: 1.206
batch start
#iterations: 43
currently lose_sum: 357.895431637764
time_elpased: 1.208
batch start
#iterations: 44
currently lose_sum: 358.58698266744614
time_elpased: 1.185
batch start
#iterations: 45
currently lose_sum: 359.86787581443787
time_elpased: 1.191
batch start
#iterations: 46
currently lose_sum: 359.3884745836258
time_elpased: 1.187
batch start
#iterations: 47
currently lose_sum: 360.0588393807411
time_elpased: 1.174
batch start
#iterations: 48
currently lose_sum: 359.7239179611206
time_elpased: 1.189
batch start
#iterations: 49
currently lose_sum: 359.531307220459
time_elpased: 1.171
batch start
#iterations: 50
currently lose_sum: 360.26347106695175
time_elpased: 1.232
batch start
#iterations: 51
currently lose_sum: 357.7825011610985
time_elpased: 1.22
batch start
#iterations: 52
currently lose_sum: 357.9576759338379
time_elpased: 1.186
batch start
#iterations: 53
currently lose_sum: 357.86096000671387
time_elpased: 1.223
batch start
#iterations: 54
currently lose_sum: 358.31494292616844
time_elpased: 1.224
batch start
#iterations: 55
currently lose_sum: 358.9086151123047
time_elpased: 1.185
batch start
#iterations: 56
currently lose_sum: 359.71323388814926
time_elpased: 1.203
batch start
#iterations: 57
currently lose_sum: 357.1664677262306
time_elpased: 1.181
batch start
#iterations: 58
currently lose_sum: 358.1665651500225
time_elpased: 1.191
batch start
#iterations: 59
currently lose_sum: 357.5905762910843
time_elpased: 1.175
start validation test
0.780618556701
1.0
0.780618556701
0.876794812413
0
validation finish
batch start
#iterations: 60
currently lose_sum: 359.0750489830971
time_elpased: 1.191
batch start
#iterations: 61
currently lose_sum: 358.53696528077126
time_elpased: 1.183
batch start
#iterations: 62
currently lose_sum: 358.7285346984863
time_elpased: 1.209
batch start
#iterations: 63
currently lose_sum: 356.6438311636448
time_elpased: 1.194
batch start
#iterations: 64
currently lose_sum: 358.17272609472275
time_elpased: 1.184
batch start
#iterations: 65
currently lose_sum: 357.60096979141235
time_elpased: 1.251
batch start
#iterations: 66
currently lose_sum: 356.37474596500397
time_elpased: 1.181
batch start
#iterations: 67
currently lose_sum: 358.29999619722366
time_elpased: 1.186
batch start
#iterations: 68
currently lose_sum: 358.9255316257477
time_elpased: 1.175
batch start
#iterations: 69
currently lose_sum: 359.3805425167084
time_elpased: 1.17
batch start
#iterations: 70
currently lose_sum: 357.32424795627594
time_elpased: 1.193
batch start
#iterations: 71
currently lose_sum: 358.2272115945816
time_elpased: 1.196
batch start
#iterations: 72
currently lose_sum: 357.1097324490547
time_elpased: 1.18
batch start
#iterations: 73
currently lose_sum: 358.401608645916
time_elpased: 1.196
batch start
#iterations: 74
currently lose_sum: 357.5034649372101
time_elpased: 1.18
batch start
#iterations: 75
currently lose_sum: 356.7570927143097
time_elpased: 1.181
batch start
#iterations: 76
currently lose_sum: 355.23902493715286
time_elpased: 1.184
batch start
#iterations: 77
currently lose_sum: 357.81070697307587
time_elpased: 1.185
batch start
#iterations: 78
currently lose_sum: 356.43311908841133
time_elpased: 1.242
batch start
#iterations: 79
currently lose_sum: 357.4826993942261
time_elpased: 1.188
start validation test
0.800515463918
1.0
0.800515463918
0.889206985399
0
validation finish
batch start
#iterations: 80
currently lose_sum: 355.7797971367836
time_elpased: 1.237
batch start
#iterations: 81
currently lose_sum: 356.4413990974426
time_elpased: 1.174
batch start
#iterations: 82
currently lose_sum: 357.5273983180523
time_elpased: 1.19
batch start
#iterations: 83
currently lose_sum: 355.8522604703903
time_elpased: 1.176
batch start
#iterations: 84
currently lose_sum: 357.367900788784
time_elpased: 1.246
batch start
#iterations: 85
currently lose_sum: 355.77168399095535
time_elpased: 1.197
batch start
#iterations: 86
currently lose_sum: 356.00244522094727
time_elpased: 1.195
batch start
#iterations: 87
currently lose_sum: 357.3327199816704
time_elpased: 1.176
batch start
#iterations: 88
currently lose_sum: 355.6026834845543
time_elpased: 1.175
batch start
#iterations: 89
currently lose_sum: 356.58946827054024
time_elpased: 1.195
batch start
#iterations: 90
currently lose_sum: 355.94235372543335
time_elpased: 1.207
batch start
#iterations: 91
currently lose_sum: 356.3873866200447
time_elpased: 1.194
batch start
#iterations: 92
currently lose_sum: 356.92748659849167
time_elpased: 1.183
batch start
#iterations: 93
currently lose_sum: 356.2585638165474
time_elpased: 1.191
batch start
#iterations: 94
currently lose_sum: 356.5978660583496
time_elpased: 1.176
batch start
#iterations: 95
currently lose_sum: 355.9524989724159
time_elpased: 1.193
batch start
#iterations: 96
currently lose_sum: 356.56683826446533
time_elpased: 1.183
batch start
#iterations: 97
currently lose_sum: 356.057665348053
time_elpased: 1.185
batch start
#iterations: 98
currently lose_sum: 355.4751554131508
time_elpased: 1.176
batch start
#iterations: 99
currently lose_sum: 357.05957198143005
time_elpased: 1.18
start validation test
0.785979381443
1.0
0.785979381443
0.880166243362
0
validation finish
batch start
#iterations: 100
currently lose_sum: 355.5337986946106
time_elpased: 1.211
batch start
#iterations: 101
currently lose_sum: 355.14615190029144
time_elpased: 1.219
batch start
#iterations: 102
currently lose_sum: 356.37024277448654
time_elpased: 1.187
batch start
#iterations: 103
currently lose_sum: 356.22240272164345
time_elpased: 1.221
batch start
#iterations: 104
currently lose_sum: 355.99835032224655
time_elpased: 1.198
batch start
#iterations: 105
currently lose_sum: 355.1997665166855
time_elpased: 1.186
batch start
#iterations: 106
currently lose_sum: 357.8537197113037
time_elpased: 1.21
batch start
#iterations: 107
currently lose_sum: 356.55437391996384
time_elpased: 1.185
batch start
#iterations: 108
currently lose_sum: 355.9442068338394
time_elpased: 1.238
batch start
#iterations: 109
currently lose_sum: 355.91939210891724
time_elpased: 1.18
batch start
#iterations: 110
currently lose_sum: 356.7591480612755
time_elpased: 1.19
batch start
#iterations: 111
currently lose_sum: 355.8493549823761
time_elpased: 1.23
batch start
#iterations: 112
currently lose_sum: 354.25113916397095
time_elpased: 1.252
batch start
#iterations: 113
currently lose_sum: 356.112227499485
time_elpased: 1.185
batch start
#iterations: 114
currently lose_sum: 355.94097125530243
time_elpased: 1.203
batch start
#iterations: 115
currently lose_sum: 356.6071527004242
time_elpased: 1.199
batch start
#iterations: 116
currently lose_sum: 355.1187784075737
time_elpased: 1.186
batch start
#iterations: 117
currently lose_sum: 356.5606032907963
time_elpased: 1.24
batch start
#iterations: 118
currently lose_sum: 354.8088044822216
time_elpased: 1.18
batch start
#iterations: 119
currently lose_sum: 355.2513396143913
time_elpased: 1.178
start validation test
0.774742268041
1.0
0.774742268041
0.873075805983
0
validation finish
batch start
#iterations: 120
currently lose_sum: 355.3816123306751
time_elpased: 1.195
batch start
#iterations: 121
currently lose_sum: 355.9972433447838
time_elpased: 1.185
batch start
#iterations: 122
currently lose_sum: 355.6413027048111
time_elpased: 1.177
batch start
#iterations: 123
currently lose_sum: 354.3780908882618
time_elpased: 1.17
batch start
#iterations: 124
currently lose_sum: 356.1425918340683
time_elpased: 1.197
batch start
#iterations: 125
currently lose_sum: 354.9643853902817
time_elpased: 1.181
batch start
#iterations: 126
currently lose_sum: 356.06036335229874
time_elpased: 1.211
batch start
#iterations: 127
currently lose_sum: 354.2075880765915
time_elpased: 1.19
batch start
#iterations: 128
currently lose_sum: 354.13325119018555
time_elpased: 1.179
batch start
#iterations: 129
currently lose_sum: 357.22579461336136
time_elpased: 1.18
batch start
#iterations: 130
currently lose_sum: 354.5126565992832
time_elpased: 1.184
batch start
#iterations: 131
currently lose_sum: 355.5409219264984
time_elpased: 1.228
batch start
#iterations: 132
currently lose_sum: 353.99209958314896
time_elpased: 1.194
batch start
#iterations: 133
currently lose_sum: 354.4882924258709
time_elpased: 1.239
batch start
#iterations: 134
currently lose_sum: 355.0556076169014
time_elpased: 1.197
batch start
#iterations: 135
currently lose_sum: 356.5057924389839
time_elpased: 1.182
batch start
#iterations: 136
currently lose_sum: 356.09844475984573
time_elpased: 1.191
batch start
#iterations: 137
currently lose_sum: 354.6294645369053
time_elpased: 1.215
batch start
#iterations: 138
currently lose_sum: 356.6727512180805
time_elpased: 1.185
batch start
#iterations: 139
currently lose_sum: 355.08944088220596
time_elpased: 1.172
start validation test
0.772474226804
1.0
0.772474226804
0.871633804455
0
validation finish
batch start
#iterations: 140
currently lose_sum: 354.1965889632702
time_elpased: 1.196
batch start
#iterations: 141
currently lose_sum: 355.0542713999748
time_elpased: 1.251
batch start
#iterations: 142
currently lose_sum: 355.7544194161892
time_elpased: 1.257
batch start
#iterations: 143
currently lose_sum: 354.6074231863022
time_elpased: 1.183
batch start
#iterations: 144
currently lose_sum: 355.06012749671936
time_elpased: 1.182
batch start
#iterations: 145
currently lose_sum: 357.0693544745445
time_elpased: 1.242
batch start
#iterations: 146
currently lose_sum: 355.9112829566002
time_elpased: 1.236
batch start
#iterations: 147
currently lose_sum: 355.0040722191334
time_elpased: 1.185
batch start
#iterations: 148
currently lose_sum: 356.7548278570175
time_elpased: 1.187
batch start
#iterations: 149
currently lose_sum: 354.9824922680855
time_elpased: 1.239
batch start
#iterations: 150
currently lose_sum: 353.2897450327873
time_elpased: 1.243
batch start
#iterations: 151
currently lose_sum: 356.52750569581985
time_elpased: 1.229
batch start
#iterations: 152
currently lose_sum: 355.4562072455883
time_elpased: 1.205
batch start
#iterations: 153
currently lose_sum: 355.74241334199905
time_elpased: 1.195
batch start
#iterations: 154
currently lose_sum: 354.96261566877365
time_elpased: 1.197
batch start
#iterations: 155
currently lose_sum: 355.9498100876808
time_elpased: 1.175
batch start
#iterations: 156
currently lose_sum: 356.3478665947914
time_elpased: 1.186
batch start
#iterations: 157
currently lose_sum: 355.4587318301201
time_elpased: 1.184
batch start
#iterations: 158
currently lose_sum: 353.921196192503
time_elpased: 1.248
batch start
#iterations: 159
currently lose_sum: 356.2310524880886
time_elpased: 1.181
start validation test
0.796804123711
1.0
0.796804123711
0.886912616903
0
validation finish
batch start
#iterations: 160
currently lose_sum: 355.5663435161114
time_elpased: 1.175
batch start
#iterations: 161
currently lose_sum: 355.6999282836914
time_elpased: 1.22
batch start
#iterations: 162
currently lose_sum: 356.47362327575684
time_elpased: 1.21
batch start
#iterations: 163
currently lose_sum: 354.77376157045364
time_elpased: 1.188
batch start
#iterations: 164
currently lose_sum: 355.23207622766495
time_elpased: 1.182
batch start
#iterations: 165
currently lose_sum: 355.69925087690353
time_elpased: 1.177
batch start
#iterations: 166
currently lose_sum: 355.61737835407257
time_elpased: 1.182
batch start
#iterations: 167
currently lose_sum: 353.97448086738586
time_elpased: 1.187
batch start
#iterations: 168
currently lose_sum: 354.3641066849232
time_elpased: 1.18
batch start
#iterations: 169
currently lose_sum: 356.30367958545685
time_elpased: 1.245
batch start
#iterations: 170
currently lose_sum: 353.2915990948677
time_elpased: 1.184
batch start
#iterations: 171
currently lose_sum: 354.0878393948078
time_elpased: 1.241
batch start
#iterations: 172
currently lose_sum: 355.4014803469181
time_elpased: 1.224
batch start
#iterations: 173
currently lose_sum: 354.57363069057465
time_elpased: 1.213
batch start
#iterations: 174
currently lose_sum: 355.41861283779144
time_elpased: 1.207
batch start
#iterations: 175
currently lose_sum: 354.6837969124317
time_elpased: 1.186
batch start
#iterations: 176
currently lose_sum: 354.4806524515152
time_elpased: 1.174
batch start
#iterations: 177
currently lose_sum: 356.0779653787613
time_elpased: 1.207
batch start
#iterations: 178
currently lose_sum: 354.35296043753624
time_elpased: 1.243
batch start
#iterations: 179
currently lose_sum: 356.0207217633724
time_elpased: 1.186
start validation test
0.789690721649
1.0
0.789690721649
0.882488479263
0
validation finish
batch start
#iterations: 180
currently lose_sum: 356.4786157608032
time_elpased: 1.185
batch start
#iterations: 181
currently lose_sum: 354.5314072072506
time_elpased: 1.181
batch start
#iterations: 182
currently lose_sum: 356.69909155368805
time_elpased: 1.226
batch start
#iterations: 183
currently lose_sum: 353.3577287495136
time_elpased: 1.191
batch start
#iterations: 184
currently lose_sum: 354.0510230064392
time_elpased: 1.189
batch start
#iterations: 185
currently lose_sum: 355.39132982492447
time_elpased: 1.243
batch start
#iterations: 186
currently lose_sum: 352.469478726387
time_elpased: 1.206
batch start
#iterations: 187
currently lose_sum: 355.3451590538025
time_elpased: 1.184
batch start
#iterations: 188
currently lose_sum: 354.1283030509949
time_elpased: 1.192
batch start
#iterations: 189
currently lose_sum: 354.9427360892296
time_elpased: 1.173
batch start
#iterations: 190
currently lose_sum: 354.0061649084091
time_elpased: 1.187
batch start
#iterations: 191
currently lose_sum: 354.99125868082047
time_elpased: 1.183
batch start
#iterations: 192
currently lose_sum: 355.76687532663345
time_elpased: 1.18
batch start
#iterations: 193
currently lose_sum: 354.9596906900406
time_elpased: 1.237
batch start
#iterations: 194
currently lose_sum: 353.06906071305275
time_elpased: 1.187
batch start
#iterations: 195
currently lose_sum: 354.3428715765476
time_elpased: 1.235
batch start
#iterations: 196
currently lose_sum: 354.8001662492752
time_elpased: 1.178
batch start
#iterations: 197
currently lose_sum: 354.7634541988373
time_elpased: 1.23
batch start
#iterations: 198
currently lose_sum: 355.7506425976753
time_elpased: 1.187
batch start
#iterations: 199
currently lose_sum: 355.20893305540085
time_elpased: 1.174
start validation test
0.770412371134
1.0
0.770412371134
0.870319687882
0
validation finish
batch start
#iterations: 200
currently lose_sum: 354.1866220831871
time_elpased: 1.182
batch start
#iterations: 201
currently lose_sum: 355.0089984536171
time_elpased: 1.183
batch start
#iterations: 202
currently lose_sum: 354.5287050008774
time_elpased: 1.2
batch start
#iterations: 203
currently lose_sum: 355.79466623067856
time_elpased: 1.186
batch start
#iterations: 204
currently lose_sum: 353.691592246294
time_elpased: 1.176
batch start
#iterations: 205
currently lose_sum: 354.2464693784714
time_elpased: 1.186
batch start
#iterations: 206
currently lose_sum: 355.0385386645794
time_elpased: 1.179
batch start
#iterations: 207
currently lose_sum: 354.97615945339203
time_elpased: 1.194
batch start
#iterations: 208
currently lose_sum: 354.21035861968994
time_elpased: 1.209
batch start
#iterations: 209
currently lose_sum: 356.1426677405834
time_elpased: 1.185
batch start
#iterations: 210
currently lose_sum: 353.9664976596832
time_elpased: 1.22
batch start
#iterations: 211
currently lose_sum: 353.6719939112663
time_elpased: 1.205
batch start
#iterations: 212
currently lose_sum: 354.0165920853615
time_elpased: 1.168
batch start
#iterations: 213
currently lose_sum: 352.44529390335083
time_elpased: 1.215
batch start
#iterations: 214
currently lose_sum: 354.36852192878723
time_elpased: 1.182
batch start
#iterations: 215
currently lose_sum: 355.10455337166786
time_elpased: 1.17
batch start
#iterations: 216
currently lose_sum: 354.3088203072548
time_elpased: 1.191
batch start
#iterations: 217
currently lose_sum: 354.5202772319317
time_elpased: 1.191
batch start
#iterations: 218
currently lose_sum: 354.56155717372894
time_elpased: 1.192
batch start
#iterations: 219
currently lose_sum: 355.1083193421364
time_elpased: 1.202
start validation test
0.774536082474
1.0
0.774536082474
0.872944867251
0
validation finish
batch start
#iterations: 220
currently lose_sum: 355.5610462129116
time_elpased: 1.2
batch start
#iterations: 221
currently lose_sum: 353.1192578077316
time_elpased: 1.208
batch start
#iterations: 222
currently lose_sum: 352.6173985302448
time_elpased: 1.174
batch start
#iterations: 223
currently lose_sum: 355.74820879101753
time_elpased: 1.176
batch start
#iterations: 224
currently lose_sum: 354.9774892926216
time_elpased: 1.174
batch start
#iterations: 225
currently lose_sum: 354.1357828080654
time_elpased: 1.198
batch start
#iterations: 226
currently lose_sum: 355.72332614660263
time_elpased: 1.178
batch start
#iterations: 227
currently lose_sum: 354.0322268605232
time_elpased: 1.19
batch start
#iterations: 228
currently lose_sum: 354.84043323993683
time_elpased: 1.177
batch start
#iterations: 229
currently lose_sum: 354.05568689107895
time_elpased: 1.219
batch start
#iterations: 230
currently lose_sum: 354.10732412338257
time_elpased: 1.187
batch start
#iterations: 231
currently lose_sum: 354.0045902132988
time_elpased: 1.22
batch start
#iterations: 232
currently lose_sum: 354.966155141592
time_elpased: 1.176
batch start
#iterations: 233
currently lose_sum: 354.5509580373764
time_elpased: 1.191
batch start
#iterations: 234
currently lose_sum: 354.26093554496765
time_elpased: 1.179
batch start
#iterations: 235
currently lose_sum: 355.0594510138035
time_elpased: 1.238
batch start
#iterations: 236
currently lose_sum: 353.6158414185047
time_elpased: 1.183
batch start
#iterations: 237
currently lose_sum: 354.86975288391113
time_elpased: 1.216
batch start
#iterations: 238
currently lose_sum: 353.850110411644
time_elpased: 1.213
batch start
#iterations: 239
currently lose_sum: 354.89592212438583
time_elpased: 1.176
start validation test
0.778556701031
1.0
0.778556701031
0.875492696499
0
validation finish
batch start
#iterations: 240
currently lose_sum: 354.34252563118935
time_elpased: 1.19
batch start
#iterations: 241
currently lose_sum: 354.96364361047745
time_elpased: 1.248
batch start
#iterations: 242
currently lose_sum: 355.132112801075
time_elpased: 1.18
batch start
#iterations: 243
currently lose_sum: 353.4086181819439
time_elpased: 1.206
batch start
#iterations: 244
currently lose_sum: 353.6810781955719
time_elpased: 1.182
batch start
#iterations: 245
currently lose_sum: 354.54913008213043
time_elpased: 1.184
batch start
#iterations: 246
currently lose_sum: 354.6381061375141
time_elpased: 1.207
batch start
#iterations: 247
currently lose_sum: 354.7180878520012
time_elpased: 1.19
batch start
#iterations: 248
currently lose_sum: 355.17149516940117
time_elpased: 1.241
batch start
#iterations: 249
currently lose_sum: 354.61311823129654
time_elpased: 1.187
batch start
#iterations: 250
currently lose_sum: 354.2833296954632
time_elpased: 1.2
batch start
#iterations: 251
currently lose_sum: 355.0655364692211
time_elpased: 1.191
batch start
#iterations: 252
currently lose_sum: 354.68386325240135
time_elpased: 1.204
batch start
#iterations: 253
currently lose_sum: 355.54795825481415
time_elpased: 1.191
batch start
#iterations: 254
currently lose_sum: 354.41385793685913
time_elpased: 1.205
batch start
#iterations: 255
currently lose_sum: 353.64858573675156
time_elpased: 1.175
batch start
#iterations: 256
currently lose_sum: 354.35592463612556
time_elpased: 1.199
batch start
#iterations: 257
currently lose_sum: 354.5172911286354
time_elpased: 1.193
batch start
#iterations: 258
currently lose_sum: 353.24011489748955
time_elpased: 1.19
batch start
#iterations: 259
currently lose_sum: 352.88398814201355
time_elpased: 1.174
start validation test
0.791134020619
1.0
0.791134020619
0.883388972027
0
validation finish
batch start
#iterations: 260
currently lose_sum: 353.37511444091797
time_elpased: 1.226
batch start
#iterations: 261
currently lose_sum: 354.0672600567341
time_elpased: 1.207
batch start
#iterations: 262
currently lose_sum: 354.99996519088745
time_elpased: 1.229
batch start
#iterations: 263
currently lose_sum: 353.6345826983452
time_elpased: 1.189
batch start
#iterations: 264
currently lose_sum: 354.1220940053463
time_elpased: 1.177
batch start
#iterations: 265
currently lose_sum: 354.47645795345306
time_elpased: 1.187
batch start
#iterations: 266
currently lose_sum: 355.2042909860611
time_elpased: 1.189
batch start
#iterations: 267
currently lose_sum: 353.9018038511276
time_elpased: 1.192
batch start
#iterations: 268
currently lose_sum: 354.8449253439903
time_elpased: 1.187
batch start
#iterations: 269
currently lose_sum: 353.9732166528702
time_elpased: 1.177
batch start
#iterations: 270
currently lose_sum: 354.71165704727173
time_elpased: 1.186
batch start
#iterations: 271
currently lose_sum: 354.6935814321041
time_elpased: 1.188
batch start
#iterations: 272
currently lose_sum: 353.9707411825657
time_elpased: 1.191
batch start
#iterations: 273
currently lose_sum: 353.6899106502533
time_elpased: 1.194
batch start
#iterations: 274
currently lose_sum: 353.9436298906803
time_elpased: 1.18
batch start
#iterations: 275
currently lose_sum: 354.56741803884506
time_elpased: 1.241
batch start
#iterations: 276
currently lose_sum: 354.7407110631466
time_elpased: 1.266
batch start
#iterations: 277
currently lose_sum: 355.02847123146057
time_elpased: 1.181
batch start
#iterations: 278
currently lose_sum: 352.5611700415611
time_elpased: 1.208
batch start
#iterations: 279
currently lose_sum: 354.4032661318779
time_elpased: 1.174
start validation test
0.777010309278
1.0
0.777010309278
0.874514126588
0
validation finish
batch start
#iterations: 280
currently lose_sum: 353.75734239816666
time_elpased: 1.176
batch start
#iterations: 281
currently lose_sum: 354.069618165493
time_elpased: 1.199
batch start
#iterations: 282
currently lose_sum: 354.05793234705925
time_elpased: 1.208
batch start
#iterations: 283
currently lose_sum: 355.31927222013474
time_elpased: 1.203
batch start
#iterations: 284
currently lose_sum: 354.4350001811981
time_elpased: 1.182
batch start
#iterations: 285
currently lose_sum: 353.5893192887306
time_elpased: 1.203
batch start
#iterations: 286
currently lose_sum: 353.5802562236786
time_elpased: 1.193
batch start
#iterations: 287
currently lose_sum: 355.26566246151924
time_elpased: 1.196
batch start
#iterations: 288
currently lose_sum: 353.32028421759605
time_elpased: 1.172
batch start
#iterations: 289
currently lose_sum: 353.6160964667797
time_elpased: 1.176
batch start
#iterations: 290
currently lose_sum: 354.0997113585472
time_elpased: 1.2
batch start
#iterations: 291
currently lose_sum: 352.9925295114517
time_elpased: 1.218
batch start
#iterations: 292
currently lose_sum: 354.18310832977295
time_elpased: 1.191
batch start
#iterations: 293
currently lose_sum: 354.7889738678932
time_elpased: 1.213
batch start
#iterations: 294
currently lose_sum: 354.06691694259644
time_elpased: 1.178
batch start
#iterations: 295
currently lose_sum: 353.20551240444183
time_elpased: 1.262
batch start
#iterations: 296
currently lose_sum: 353.7080063223839
time_elpased: 1.193
batch start
#iterations: 297
currently lose_sum: 353.9173249602318
time_elpased: 1.217
batch start
#iterations: 298
currently lose_sum: 354.16148895025253
time_elpased: 1.191
batch start
#iterations: 299
currently lose_sum: 354.13443607091904
time_elpased: 1.182
start validation test
0.794639175258
1.0
0.794639175258
0.885569852941
0
validation finish
batch start
#iterations: 300
currently lose_sum: 355.00816982984543
time_elpased: 1.196
batch start
#iterations: 301
currently lose_sum: 353.0719874203205
time_elpased: 1.188
batch start
#iterations: 302
currently lose_sum: 354.8581550717354
time_elpased: 1.207
batch start
#iterations: 303
currently lose_sum: 353.77741000056267
time_elpased: 1.241
batch start
#iterations: 304
currently lose_sum: 355.0368202328682
time_elpased: 1.19
batch start
#iterations: 305
currently lose_sum: 353.28781723976135
time_elpased: 1.192
batch start
#iterations: 306
currently lose_sum: 353.01355481147766
time_elpased: 1.195
batch start
#iterations: 307
currently lose_sum: 353.3354846537113
time_elpased: 1.184
batch start
#iterations: 308
currently lose_sum: 353.12608474493027
time_elpased: 1.213
batch start
#iterations: 309
currently lose_sum: 353.6053156256676
time_elpased: 1.203
batch start
#iterations: 310
currently lose_sum: 354.0266394019127
time_elpased: 1.181
batch start
#iterations: 311
currently lose_sum: 354.9985050559044
time_elpased: 1.202
batch start
#iterations: 312
currently lose_sum: 352.951675593853
time_elpased: 1.195
batch start
#iterations: 313
currently lose_sum: 354.1108114719391
time_elpased: 1.198
batch start
#iterations: 314
currently lose_sum: 352.3445524573326
time_elpased: 1.228
batch start
#iterations: 315
currently lose_sum: 353.9011169075966
time_elpased: 1.175
batch start
#iterations: 316
currently lose_sum: 353.37858587503433
time_elpased: 1.189
batch start
#iterations: 317
currently lose_sum: 354.1490896344185
time_elpased: 1.19
batch start
#iterations: 318
currently lose_sum: 353.8629152774811
time_elpased: 1.175
batch start
#iterations: 319
currently lose_sum: 354.8056955933571
time_elpased: 1.187
start validation test
0.804742268041
1.0
0.804742268041
0.891808522792
0
validation finish
batch start
#iterations: 320
currently lose_sum: 355.2129476368427
time_elpased: 1.183
batch start
#iterations: 321
currently lose_sum: 356.5345698595047
time_elpased: 1.177
batch start
#iterations: 322
currently lose_sum: 353.3144479691982
time_elpased: 1.177
batch start
#iterations: 323
currently lose_sum: 352.65995103120804
time_elpased: 1.184
batch start
#iterations: 324
currently lose_sum: 353.90214824676514
time_elpased: 1.189
batch start
#iterations: 325
currently lose_sum: 352.2400915324688
time_elpased: 1.173
batch start
#iterations: 326
currently lose_sum: 355.01988327503204
time_elpased: 1.188
batch start
#iterations: 327
currently lose_sum: 355.225684016943
time_elpased: 1.184
batch start
#iterations: 328
currently lose_sum: 353.4467505514622
time_elpased: 1.245
batch start
#iterations: 329
currently lose_sum: 352.7126241326332
time_elpased: 1.183
batch start
#iterations: 330
currently lose_sum: 355.123289167881
time_elpased: 1.22
batch start
#iterations: 331
currently lose_sum: 354.4805181026459
time_elpased: 1.179
batch start
#iterations: 332
currently lose_sum: 354.28074756264687
time_elpased: 1.232
batch start
#iterations: 333
currently lose_sum: 351.9762752056122
time_elpased: 1.179
batch start
#iterations: 334
currently lose_sum: 353.97976118326187
time_elpased: 1.176
batch start
#iterations: 335
currently lose_sum: 353.4237906932831
time_elpased: 1.217
batch start
#iterations: 336
currently lose_sum: 351.94853603839874
time_elpased: 1.191
batch start
#iterations: 337
currently lose_sum: 353.8626011610031
time_elpased: 1.181
batch start
#iterations: 338
currently lose_sum: 352.6351290345192
time_elpased: 1.202
batch start
#iterations: 339
currently lose_sum: 352.9213798940182
time_elpased: 1.182
start validation test
0.782474226804
1.0
0.782474226804
0.877964141122
0
validation finish
batch start
#iterations: 340
currently lose_sum: 354.0399740636349
time_elpased: 1.176
batch start
#iterations: 341
currently lose_sum: 353.466769695282
time_elpased: 1.225
batch start
#iterations: 342
currently lose_sum: 353.03955486416817
time_elpased: 1.166
batch start
#iterations: 343
currently lose_sum: 353.92647448182106
time_elpased: 1.187
batch start
#iterations: 344
currently lose_sum: 352.7253685295582
time_elpased: 1.202
batch start
#iterations: 345
currently lose_sum: 354.93741273880005
time_elpased: 1.245
batch start
#iterations: 346
currently lose_sum: 353.15145963430405
time_elpased: 1.191
batch start
#iterations: 347
currently lose_sum: 351.14887523651123
time_elpased: 1.184
batch start
#iterations: 348
currently lose_sum: 354.5067575275898
time_elpased: 1.189
batch start
#iterations: 349
currently lose_sum: 354.3574612736702
time_elpased: 1.188
batch start
#iterations: 350
currently lose_sum: 354.7917328476906
time_elpased: 1.219
batch start
#iterations: 351
currently lose_sum: 352.8663215637207
time_elpased: 1.221
batch start
#iterations: 352
currently lose_sum: 353.17959517240524
time_elpased: 1.218
batch start
#iterations: 353
currently lose_sum: 354.38354244828224
time_elpased: 1.235
batch start
#iterations: 354
currently lose_sum: 352.2003164291382
time_elpased: 1.21
batch start
#iterations: 355
currently lose_sum: 352.85173016786575
time_elpased: 1.18
batch start
#iterations: 356
currently lose_sum: 353.76463159918785
time_elpased: 1.201
batch start
#iterations: 357
currently lose_sum: 354.90194404125214
time_elpased: 1.182
batch start
#iterations: 358
currently lose_sum: 354.66506627202034
time_elpased: 1.177
batch start
#iterations: 359
currently lose_sum: 354.2794488668442
time_elpased: 1.233
start validation test
0.791958762887
1.0
0.791958762887
0.883902888045
0
validation finish
batch start
#iterations: 360
currently lose_sum: 353.51528710126877
time_elpased: 1.18
batch start
#iterations: 361
currently lose_sum: 353.6466583907604
time_elpased: 1.185
batch start
#iterations: 362
currently lose_sum: 354.73864102363586
time_elpased: 1.21
batch start
#iterations: 363
currently lose_sum: 353.51359009742737
time_elpased: 1.209
batch start
#iterations: 364
currently lose_sum: 354.202599555254
time_elpased: 1.237
batch start
#iterations: 365
currently lose_sum: 355.62748670578003
time_elpased: 1.2
batch start
#iterations: 366
currently lose_sum: 354.6551629304886
time_elpased: 1.189
batch start
#iterations: 367
currently lose_sum: 354.5344022512436
time_elpased: 1.204
batch start
#iterations: 368
currently lose_sum: 353.91990572214127
time_elpased: 1.191
batch start
#iterations: 369
currently lose_sum: 352.2345260977745
time_elpased: 1.184
batch start
#iterations: 370
currently lose_sum: 353.39111161231995
time_elpased: 1.209
batch start
#iterations: 371
currently lose_sum: 353.9045737981796
time_elpased: 1.198
batch start
#iterations: 372
currently lose_sum: 351.0317833125591
time_elpased: 1.226
batch start
#iterations: 373
currently lose_sum: 354.7770059108734
time_elpased: 1.183
batch start
#iterations: 374
currently lose_sum: 353.0250912606716
time_elpased: 1.192
batch start
#iterations: 375
currently lose_sum: 354.0923249721527
time_elpased: 1.196
batch start
#iterations: 376
currently lose_sum: 354.0282520055771
time_elpased: 1.201
batch start
#iterations: 377
currently lose_sum: 354.4068679213524
time_elpased: 1.187
batch start
#iterations: 378
currently lose_sum: 353.2859500348568
time_elpased: 1.213
batch start
#iterations: 379
currently lose_sum: 353.68759274482727
time_elpased: 1.193
start validation test
0.796288659794
1.0
0.796288659794
0.886593204775
0
validation finish
batch start
#iterations: 380
currently lose_sum: 353.28487557172775
time_elpased: 1.19
batch start
#iterations: 381
currently lose_sum: 351.7367531955242
time_elpased: 1.179
batch start
#iterations: 382
currently lose_sum: 352.7276611626148
time_elpased: 1.177
batch start
#iterations: 383
currently lose_sum: 352.00252255797386
time_elpased: 1.177
batch start
#iterations: 384
currently lose_sum: 353.5630733668804
time_elpased: 1.195
batch start
#iterations: 385
currently lose_sum: 353.94043216109276
time_elpased: 1.245
batch start
#iterations: 386
currently lose_sum: 353.8075016438961
time_elpased: 1.229
batch start
#iterations: 387
currently lose_sum: 354.4326325058937
time_elpased: 1.19
batch start
#iterations: 388
currently lose_sum: 352.0951998233795
time_elpased: 1.189
batch start
#iterations: 389
currently lose_sum: 352.8009952008724
time_elpased: 1.191
batch start
#iterations: 390
currently lose_sum: 355.6583587527275
time_elpased: 1.225
batch start
#iterations: 391
currently lose_sum: 353.65615797042847
time_elpased: 1.206
batch start
#iterations: 392
currently lose_sum: 352.77887257933617
time_elpased: 1.184
batch start
#iterations: 393
currently lose_sum: 353.72056090831757
time_elpased: 1.245
batch start
#iterations: 394
currently lose_sum: 353.7649537920952
time_elpased: 1.183
batch start
#iterations: 395
currently lose_sum: 354.1391230225563
time_elpased: 1.187
batch start
#iterations: 396
currently lose_sum: 352.8225411772728
time_elpased: 1.185
batch start
#iterations: 397
currently lose_sum: 354.37273210287094
time_elpased: 1.192
batch start
#iterations: 398
currently lose_sum: 353.5071904361248
time_elpased: 1.219
batch start
#iterations: 399
currently lose_sum: 353.12396401166916
time_elpased: 1.236
start validation test
0.79206185567
1.0
0.79206185567
0.883967094288
0
validation finish
acc: 0.804
pre: 1.000
rec: 0.804
F1: 0.891
auc: 0.000
