start to construct graph
graph construct over
58302
epochs start
batch start
#iterations: 0
currently lose_sum: 394.11479300260544
time_elpased: 1.222
batch start
#iterations: 1
currently lose_sum: 384.30947864055634
time_elpased: 1.181
batch start
#iterations: 2
currently lose_sum: 380.92431640625
time_elpased: 1.194
batch start
#iterations: 3
currently lose_sum: 375.61242628097534
time_elpased: 1.202
batch start
#iterations: 4
currently lose_sum: 374.95726972818375
time_elpased: 1.189
batch start
#iterations: 5
currently lose_sum: 374.06759411096573
time_elpased: 1.181
batch start
#iterations: 6
currently lose_sum: 373.3056643605232
time_elpased: 1.203
batch start
#iterations: 7
currently lose_sum: 371.1687363386154
time_elpased: 1.187
batch start
#iterations: 8
currently lose_sum: 369.642499089241
time_elpased: 1.179
batch start
#iterations: 9
currently lose_sum: 369.70860999822617
time_elpased: 1.204
batch start
#iterations: 10
currently lose_sum: 368.57264071702957
time_elpased: 1.187
batch start
#iterations: 11
currently lose_sum: 366.6115028858185
time_elpased: 1.18
batch start
#iterations: 12
currently lose_sum: 366.9372103214264
time_elpased: 1.193
batch start
#iterations: 13
currently lose_sum: 366.22062063217163
time_elpased: 1.195
batch start
#iterations: 14
currently lose_sum: 365.86860913038254
time_elpased: 1.18
batch start
#iterations: 15
currently lose_sum: 364.71634632349014
time_elpased: 1.188
batch start
#iterations: 16
currently lose_sum: 366.81028312444687
time_elpased: 1.186
batch start
#iterations: 17
currently lose_sum: 365.0471822023392
time_elpased: 1.199
batch start
#iterations: 18
currently lose_sum: 364.82902389764786
time_elpased: 1.193
batch start
#iterations: 19
currently lose_sum: 363.41716480255127
time_elpased: 1.175
start validation test
0.78618556701
1.0
0.78618556701
0.880295509639
0
validation finish
batch start
#iterations: 20
currently lose_sum: 363.0132520198822
time_elpased: 1.185
batch start
#iterations: 21
currently lose_sum: 363.619313120842
time_elpased: 1.191
batch start
#iterations: 22
currently lose_sum: 364.1420175433159
time_elpased: 1.235
batch start
#iterations: 23
currently lose_sum: 361.63414549827576
time_elpased: 1.226
batch start
#iterations: 24
currently lose_sum: 363.3461063504219
time_elpased: 1.203
batch start
#iterations: 25
currently lose_sum: 363.4266639947891
time_elpased: 1.187
batch start
#iterations: 26
currently lose_sum: 362.1996951699257
time_elpased: 1.194
batch start
#iterations: 27
currently lose_sum: 364.1020720601082
time_elpased: 1.194
batch start
#iterations: 28
currently lose_sum: 362.33680418133736
time_elpased: 1.201
batch start
#iterations: 29
currently lose_sum: 363.1493903398514
time_elpased: 1.258
batch start
#iterations: 30
currently lose_sum: 362.22499603033066
time_elpased: 1.183
batch start
#iterations: 31
currently lose_sum: 361.90693444013596
time_elpased: 1.211
batch start
#iterations: 32
currently lose_sum: 361.07279473543167
time_elpased: 1.248
batch start
#iterations: 33
currently lose_sum: 361.4328607916832
time_elpased: 1.196
batch start
#iterations: 34
currently lose_sum: 361.3635657429695
time_elpased: 1.215
batch start
#iterations: 35
currently lose_sum: 361.4562029838562
time_elpased: 1.232
batch start
#iterations: 36
currently lose_sum: 361.62533819675446
time_elpased: 1.193
batch start
#iterations: 37
currently lose_sum: 359.4721508026123
time_elpased: 1.185
batch start
#iterations: 38
currently lose_sum: 359.55766010284424
time_elpased: 1.196
batch start
#iterations: 39
currently lose_sum: 360.5332551598549
time_elpased: 1.199
start validation test
0.767525773196
1.0
0.767525773196
0.868474773987
0
validation finish
batch start
#iterations: 40
currently lose_sum: 359.8110902309418
time_elpased: 1.187
batch start
#iterations: 41
currently lose_sum: 359.21420454978943
time_elpased: 1.184
batch start
#iterations: 42
currently lose_sum: 360.2883551120758
time_elpased: 1.255
batch start
#iterations: 43
currently lose_sum: 358.97205808758736
time_elpased: 1.189
batch start
#iterations: 44
currently lose_sum: 358.9393527507782
time_elpased: 1.185
batch start
#iterations: 45
currently lose_sum: 358.767188847065
time_elpased: 1.186
batch start
#iterations: 46
currently lose_sum: 361.4521389603615
time_elpased: 1.211
batch start
#iterations: 47
currently lose_sum: 359.8454530239105
time_elpased: 1.2
batch start
#iterations: 48
currently lose_sum: 359.16422057151794
time_elpased: 1.19
batch start
#iterations: 49
currently lose_sum: 359.0081461071968
time_elpased: 1.258
batch start
#iterations: 50
currently lose_sum: 362.1494985818863
time_elpased: 1.194
batch start
#iterations: 51
currently lose_sum: 359.92214983701706
time_elpased: 1.204
batch start
#iterations: 52
currently lose_sum: 359.53938323259354
time_elpased: 1.195
batch start
#iterations: 53
currently lose_sum: 360.2805606126785
time_elpased: 1.192
batch start
#iterations: 54
currently lose_sum: 358.475954413414
time_elpased: 1.223
batch start
#iterations: 55
currently lose_sum: 358.30224484205246
time_elpased: 1.18
batch start
#iterations: 56
currently lose_sum: 359.74321430921555
time_elpased: 1.177
batch start
#iterations: 57
currently lose_sum: 358.8104847073555
time_elpased: 1.201
batch start
#iterations: 58
currently lose_sum: 357.8208167850971
time_elpased: 1.189
batch start
#iterations: 59
currently lose_sum: 360.1335783004761
time_elpased: 1.201
start validation test
0.746804123711
1.0
0.746804123711
0.855051935788
0
validation finish
batch start
#iterations: 60
currently lose_sum: 358.60975486040115
time_elpased: 1.177
batch start
#iterations: 61
currently lose_sum: 358.92172598838806
time_elpased: 1.203
batch start
#iterations: 62
currently lose_sum: 358.5197242796421
time_elpased: 1.191
batch start
#iterations: 63
currently lose_sum: 359.85978451371193
time_elpased: 1.254
batch start
#iterations: 64
currently lose_sum: 359.08760201931
time_elpased: 1.21
batch start
#iterations: 65
currently lose_sum: 358.25398379564285
time_elpased: 1.184
batch start
#iterations: 66
currently lose_sum: 359.8442927598953
time_elpased: 1.19
batch start
#iterations: 67
currently lose_sum: 359.8967354297638
time_elpased: 1.188
batch start
#iterations: 68
currently lose_sum: 358.05047327280045
time_elpased: 1.245
batch start
#iterations: 69
currently lose_sum: 358.7113022208214
time_elpased: 1.2
batch start
#iterations: 70
currently lose_sum: 358.2392798066139
time_elpased: 1.22
batch start
#iterations: 71
currently lose_sum: 358.4107757508755
time_elpased: 1.245
batch start
#iterations: 72
currently lose_sum: 357.64619928598404
time_elpased: 1.246
batch start
#iterations: 73
currently lose_sum: 358.8470987677574
time_elpased: 1.182
batch start
#iterations: 74
currently lose_sum: 359.2293308377266
time_elpased: 1.206
batch start
#iterations: 75
currently lose_sum: 357.47305285930634
time_elpased: 1.196
batch start
#iterations: 76
currently lose_sum: 358.9752008318901
time_elpased: 1.197
batch start
#iterations: 77
currently lose_sum: 357.87820786237717
time_elpased: 1.199
batch start
#iterations: 78
currently lose_sum: 357.3674433231354
time_elpased: 1.211
batch start
#iterations: 79
currently lose_sum: 356.60563570261
time_elpased: 1.256
start validation test
0.776494845361
1.0
0.776494845361
0.874187558032
0
validation finish
batch start
#iterations: 80
currently lose_sum: 356.7000350356102
time_elpased: 1.235
batch start
#iterations: 81
currently lose_sum: 359.0242891907692
time_elpased: 1.19
batch start
#iterations: 82
currently lose_sum: 358.55196422338486
time_elpased: 1.258
batch start
#iterations: 83
currently lose_sum: 357.7768550515175
time_elpased: 1.192
batch start
#iterations: 84
currently lose_sum: 358.43620240688324
time_elpased: 1.194
batch start
#iterations: 85
currently lose_sum: 358.3831697702408
time_elpased: 1.251
batch start
#iterations: 86
currently lose_sum: 358.6038827896118
time_elpased: 1.198
batch start
#iterations: 87
currently lose_sum: 358.6899142861366
time_elpased: 1.228
batch start
#iterations: 88
currently lose_sum: 356.19167602062225
time_elpased: 1.288
batch start
#iterations: 89
currently lose_sum: 357.2049210071564
time_elpased: 1.232
batch start
#iterations: 90
currently lose_sum: 357.0841269493103
time_elpased: 1.217
batch start
#iterations: 91
currently lose_sum: 356.9879203438759
time_elpased: 1.283
batch start
#iterations: 92
currently lose_sum: 357.35935693979263
time_elpased: 1.225
batch start
#iterations: 93
currently lose_sum: 359.6052859425545
time_elpased: 1.2
batch start
#iterations: 94
currently lose_sum: 358.6177234053612
time_elpased: 1.196
batch start
#iterations: 95
currently lose_sum: 355.38633212447166
time_elpased: 1.201
batch start
#iterations: 96
currently lose_sum: 356.47644543647766
time_elpased: 1.261
batch start
#iterations: 97
currently lose_sum: 357.5198296904564
time_elpased: 1.188
batch start
#iterations: 98
currently lose_sum: 355.78554195165634
time_elpased: 1.241
batch start
#iterations: 99
currently lose_sum: 358.71430963277817
time_elpased: 1.197
start validation test
0.748556701031
1.0
0.748556701031
0.856199516538
0
validation finish
batch start
#iterations: 100
currently lose_sum: 358.0058180093765
time_elpased: 1.203
batch start
#iterations: 101
currently lose_sum: 360.0606015920639
time_elpased: 1.211
batch start
#iterations: 102
currently lose_sum: 355.85698091983795
time_elpased: 1.205
batch start
#iterations: 103
currently lose_sum: 357.9862090945244
time_elpased: 1.241
batch start
#iterations: 104
currently lose_sum: 358.1648465394974
time_elpased: 1.195
batch start
#iterations: 105
currently lose_sum: 357.10093462467194
time_elpased: 1.2
batch start
#iterations: 106
currently lose_sum: 357.62550526857376
time_elpased: 1.234
batch start
#iterations: 107
currently lose_sum: 355.38280948996544
time_elpased: 1.257
batch start
#iterations: 108
currently lose_sum: 357.1082393825054
time_elpased: 1.221
batch start
#iterations: 109
currently lose_sum: 357.3851860165596
time_elpased: 1.202
batch start
#iterations: 110
currently lose_sum: 356.8829283118248
time_elpased: 1.203
batch start
#iterations: 111
currently lose_sum: 356.620192527771
time_elpased: 1.229
batch start
#iterations: 112
currently lose_sum: 356.11598443984985
time_elpased: 1.235
batch start
#iterations: 113
currently lose_sum: 357.7061003446579
time_elpased: 1.19
batch start
#iterations: 114
currently lose_sum: 358.36474722623825
time_elpased: 1.255
batch start
#iterations: 115
currently lose_sum: 356.26895916461945
time_elpased: 1.228
batch start
#iterations: 116
currently lose_sum: 356.5834991335869
time_elpased: 1.223
batch start
#iterations: 117
currently lose_sum: 356.7633807063103
time_elpased: 1.209
batch start
#iterations: 118
currently lose_sum: 355.9234386086464
time_elpased: 1.201
batch start
#iterations: 119
currently lose_sum: 356.50607043504715
time_elpased: 1.219
start validation test
0.751134020619
1.0
0.751134020619
0.85788296244
0
validation finish
batch start
#iterations: 120
currently lose_sum: 356.56250047683716
time_elpased: 1.2
batch start
#iterations: 121
currently lose_sum: 356.6100359559059
time_elpased: 1.245
batch start
#iterations: 122
currently lose_sum: 357.0527039170265
time_elpased: 1.198
batch start
#iterations: 123
currently lose_sum: 354.54985374212265
time_elpased: 1.22
batch start
#iterations: 124
currently lose_sum: 355.4337295293808
time_elpased: 1.208
batch start
#iterations: 125
currently lose_sum: 356.55546391010284
time_elpased: 1.232
batch start
#iterations: 126
currently lose_sum: 357.010930120945
time_elpased: 1.203
batch start
#iterations: 127
currently lose_sum: 357.1884072422981
time_elpased: 1.207
batch start
#iterations: 128
currently lose_sum: 358.64996522665024
time_elpased: 1.203
batch start
#iterations: 129
currently lose_sum: 355.2804237008095
time_elpased: 1.191
batch start
#iterations: 130
currently lose_sum: 356.1526588201523
time_elpased: 1.209
batch start
#iterations: 131
currently lose_sum: 357.23786836862564
time_elpased: 1.194
batch start
#iterations: 132
currently lose_sum: 356.79573994874954
time_elpased: 1.221
batch start
#iterations: 133
currently lose_sum: 358.3823299407959
time_elpased: 1.198
batch start
#iterations: 134
currently lose_sum: 356.14695075154305
time_elpased: 1.205
batch start
#iterations: 135
currently lose_sum: 357.5938866138458
time_elpased: 1.189
batch start
#iterations: 136
currently lose_sum: 356.74819046258926
time_elpased: 1.194
batch start
#iterations: 137
currently lose_sum: 356.33979165554047
time_elpased: 1.211
batch start
#iterations: 138
currently lose_sum: 358.0071640610695
time_elpased: 1.273
batch start
#iterations: 139
currently lose_sum: 355.06918746232986
time_elpased: 1.217
start validation test
0.763195876289
1.0
0.763195876289
0.865696076712
0
validation finish
batch start
#iterations: 140
currently lose_sum: 355.46342062950134
time_elpased: 1.195
batch start
#iterations: 141
currently lose_sum: 356.44878202676773
time_elpased: 1.226
batch start
#iterations: 142
currently lose_sum: 354.1964277625084
time_elpased: 1.22
batch start
#iterations: 143
currently lose_sum: 354.7314555346966
time_elpased: 1.205
batch start
#iterations: 144
currently lose_sum: 355.6692467033863
time_elpased: 1.202
batch start
#iterations: 145
currently lose_sum: 356.83295929431915
time_elpased: 1.204
batch start
#iterations: 146
currently lose_sum: 353.6103350222111
time_elpased: 1.2
batch start
#iterations: 147
currently lose_sum: 357.2601455450058
time_elpased: 1.191
batch start
#iterations: 148
currently lose_sum: 356.51508313417435
time_elpased: 1.19
batch start
#iterations: 149
currently lose_sum: 355.71043890714645
time_elpased: 1.229
batch start
#iterations: 150
currently lose_sum: 356.7680107951164
time_elpased: 1.225
batch start
#iterations: 151
currently lose_sum: 356.52572429180145
time_elpased: 1.209
batch start
#iterations: 152
currently lose_sum: 355.0297654569149
time_elpased: 1.23
batch start
#iterations: 153
currently lose_sum: 356.55090963840485
time_elpased: 1.226
batch start
#iterations: 154
currently lose_sum: 355.6261577010155
time_elpased: 1.191
batch start
#iterations: 155
currently lose_sum: 356.1132542490959
time_elpased: 1.184
batch start
#iterations: 156
currently lose_sum: 357.52917033433914
time_elpased: 1.187
batch start
#iterations: 157
currently lose_sum: 357.36408692598343
time_elpased: 1.232
batch start
#iterations: 158
currently lose_sum: 356.2711818218231
time_elpased: 1.217
batch start
#iterations: 159
currently lose_sum: 355.3980406522751
time_elpased: 1.187
start validation test
0.785051546392
1.0
0.785051546392
0.87958417557
0
validation finish
batch start
#iterations: 160
currently lose_sum: 356.75492864847183
time_elpased: 1.196
batch start
#iterations: 161
currently lose_sum: 356.98933857679367
time_elpased: 1.244
batch start
#iterations: 162
currently lose_sum: 356.5920617878437
time_elpased: 1.222
batch start
#iterations: 163
currently lose_sum: 355.6806326508522
time_elpased: 1.198
batch start
#iterations: 164
currently lose_sum: 356.92263001203537
time_elpased: 1.19
batch start
#iterations: 165
currently lose_sum: 355.14378011226654
time_elpased: 1.252
batch start
#iterations: 166
currently lose_sum: 355.62441498041153
time_elpased: 1.237
batch start
#iterations: 167
currently lose_sum: 356.77704256772995
time_elpased: 1.197
batch start
#iterations: 168
currently lose_sum: 356.2716681063175
time_elpased: 1.205
batch start
#iterations: 169
currently lose_sum: 355.20401898026466
time_elpased: 1.235
batch start
#iterations: 170
currently lose_sum: 355.8434548974037
time_elpased: 1.196
batch start
#iterations: 171
currently lose_sum: 354.67545610666275
time_elpased: 1.199
batch start
#iterations: 172
currently lose_sum: 356.11822488904
time_elpased: 1.193
batch start
#iterations: 173
currently lose_sum: 356.47760450839996
time_elpased: 1.239
batch start
#iterations: 174
currently lose_sum: 356.04969894886017
time_elpased: 1.222
batch start
#iterations: 175
currently lose_sum: 355.72679990530014
time_elpased: 1.206
batch start
#iterations: 176
currently lose_sum: 355.49565452337265
time_elpased: 1.191
batch start
#iterations: 177
currently lose_sum: 356.09559988975525
time_elpased: 1.19
batch start
#iterations: 178
currently lose_sum: 355.2283818125725
time_elpased: 1.218
batch start
#iterations: 179
currently lose_sum: 355.0262451171875
time_elpased: 1.196
start validation test
0.748350515464
1.0
0.748350515464
0.856064626452
0
validation finish
batch start
#iterations: 180
currently lose_sum: 355.95007050037384
time_elpased: 1.21
batch start
#iterations: 181
currently lose_sum: 355.53165274858475
time_elpased: 1.189
batch start
#iterations: 182
currently lose_sum: 357.8075475692749
time_elpased: 1.194
batch start
#iterations: 183
currently lose_sum: 355.79318058490753
time_elpased: 1.2
batch start
#iterations: 184
currently lose_sum: 355.89490616321564
time_elpased: 1.221
batch start
#iterations: 185
currently lose_sum: 354.1931744813919
time_elpased: 1.188
batch start
#iterations: 186
currently lose_sum: 356.64284110069275
time_elpased: 1.217
batch start
#iterations: 187
currently lose_sum: 355.30950278043747
time_elpased: 1.227
batch start
#iterations: 188
currently lose_sum: 355.30654579401016
time_elpased: 1.197
batch start
#iterations: 189
currently lose_sum: 356.23064613342285
time_elpased: 1.237
batch start
#iterations: 190
currently lose_sum: 356.43192875385284
time_elpased: 1.188
batch start
#iterations: 191
currently lose_sum: 357.1967228651047
time_elpased: 1.256
batch start
#iterations: 192
currently lose_sum: 356.51309579610825
time_elpased: 1.195
batch start
#iterations: 193
currently lose_sum: 355.51693844795227
time_elpased: 1.232
batch start
#iterations: 194
currently lose_sum: 354.2688000500202
time_elpased: 1.197
batch start
#iterations: 195
currently lose_sum: 354.2495142221451
time_elpased: 1.19
batch start
#iterations: 196
currently lose_sum: 356.2966305613518
time_elpased: 1.181
batch start
#iterations: 197
currently lose_sum: 357.81204187870026
time_elpased: 1.199
batch start
#iterations: 198
currently lose_sum: 356.0778412222862
time_elpased: 1.19
batch start
#iterations: 199
currently lose_sum: 356.36867237091064
time_elpased: 1.2
start validation test
0.782886597938
1.0
0.782886597938
0.878223661385
0
validation finish
batch start
#iterations: 200
currently lose_sum: 354.79222017526627
time_elpased: 1.19
batch start
#iterations: 201
currently lose_sum: 356.32423931360245
time_elpased: 1.195
batch start
#iterations: 202
currently lose_sum: 356.71031180024147
time_elpased: 1.25
batch start
#iterations: 203
currently lose_sum: 354.15607842803
time_elpased: 1.234
batch start
#iterations: 204
currently lose_sum: 355.4068176150322
time_elpased: 1.203
batch start
#iterations: 205
currently lose_sum: 355.66324147582054
time_elpased: 1.197
batch start
#iterations: 206
currently lose_sum: 356.82176929712296
time_elpased: 1.216
batch start
#iterations: 207
currently lose_sum: 354.29692870378494
time_elpased: 1.266
batch start
#iterations: 208
currently lose_sum: 356.6765275001526
time_elpased: 1.2
batch start
#iterations: 209
currently lose_sum: 352.76221254467964
time_elpased: 1.262
batch start
#iterations: 210
currently lose_sum: 355.29760694503784
time_elpased: 1.209
batch start
#iterations: 211
currently lose_sum: 353.97200083732605
time_elpased: 1.188
batch start
#iterations: 212
currently lose_sum: 354.30119544267654
time_elpased: 1.202
batch start
#iterations: 213
currently lose_sum: 354.8057862520218
time_elpased: 1.194
batch start
#iterations: 214
currently lose_sum: 355.7587879896164
time_elpased: 1.182
batch start
#iterations: 215
currently lose_sum: 353.78621008992195
time_elpased: 1.181
batch start
#iterations: 216
currently lose_sum: 355.07281333208084
time_elpased: 1.21
batch start
#iterations: 217
currently lose_sum: 355.7696423828602
time_elpased: 1.258
batch start
#iterations: 218
currently lose_sum: 354.6224105358124
time_elpased: 1.192
batch start
#iterations: 219
currently lose_sum: 356.9661604166031
time_elpased: 1.192
start validation test
0.779793814433
1.0
0.779793814433
0.876274328082
0
validation finish
batch start
#iterations: 220
currently lose_sum: 355.50852060317993
time_elpased: 1.205
batch start
#iterations: 221
currently lose_sum: 355.0182843208313
time_elpased: 1.189
batch start
#iterations: 222
currently lose_sum: 354.14844581484795
time_elpased: 1.207
batch start
#iterations: 223
currently lose_sum: 355.24549347162247
time_elpased: 1.189
batch start
#iterations: 224
currently lose_sum: 352.9370201230049
time_elpased: 1.206
batch start
#iterations: 225
currently lose_sum: 355.201296210289
time_elpased: 1.244
batch start
#iterations: 226
currently lose_sum: 355.9134348630905
time_elpased: 1.204
batch start
#iterations: 227
currently lose_sum: 354.67790043354034
time_elpased: 1.202
batch start
#iterations: 228
currently lose_sum: 354.2725901007652
time_elpased: 1.182
batch start
#iterations: 229
currently lose_sum: 354.07055750489235
time_elpased: 1.198
batch start
#iterations: 230
currently lose_sum: 355.30232018232346
time_elpased: 1.194
batch start
#iterations: 231
currently lose_sum: 355.5931657254696
time_elpased: 1.202
batch start
#iterations: 232
currently lose_sum: 356.3810416460037
time_elpased: 1.202
batch start
#iterations: 233
currently lose_sum: 354.3109567761421
time_elpased: 1.198
batch start
#iterations: 234
currently lose_sum: 352.9817501604557
time_elpased: 1.19
batch start
#iterations: 235
currently lose_sum: 355.2778539657593
time_elpased: 1.198
batch start
#iterations: 236
currently lose_sum: 354.55849117040634
time_elpased: 1.235
batch start
#iterations: 237
currently lose_sum: 355.1601207256317
time_elpased: 1.236
batch start
#iterations: 238
currently lose_sum: 354.7780651450157
time_elpased: 1.19
batch start
#iterations: 239
currently lose_sum: 355.1285373866558
time_elpased: 1.2
start validation test
0.769381443299
1.0
0.769381443299
0.869661481093
0
validation finish
batch start
#iterations: 240
currently lose_sum: 355.96403539180756
time_elpased: 1.229
batch start
#iterations: 241
currently lose_sum: 353.75191456079483
time_elpased: 1.189
batch start
#iterations: 242
currently lose_sum: 352.69011321663857
time_elpased: 1.204
batch start
#iterations: 243
currently lose_sum: 357.03459656238556
time_elpased: 1.191
batch start
#iterations: 244
currently lose_sum: 356.2462413907051
time_elpased: 1.191
batch start
#iterations: 245
currently lose_sum: 355.88966542482376
time_elpased: 1.25
batch start
#iterations: 246
currently lose_sum: 354.7894633114338
time_elpased: 1.273
batch start
#iterations: 247
currently lose_sum: 354.90777105093
time_elpased: 1.203
batch start
#iterations: 248
currently lose_sum: 355.378221988678
time_elpased: 1.214
batch start
#iterations: 249
currently lose_sum: 356.7196278870106
time_elpased: 1.198
batch start
#iterations: 250
currently lose_sum: 354.94869589805603
time_elpased: 1.273
batch start
#iterations: 251
currently lose_sum: 355.4374322295189
time_elpased: 1.199
batch start
#iterations: 252
currently lose_sum: 354.12191092967987
time_elpased: 1.219
batch start
#iterations: 253
currently lose_sum: 354.96576085686684
time_elpased: 1.212
batch start
#iterations: 254
currently lose_sum: 354.396835654974
time_elpased: 1.193
batch start
#iterations: 255
currently lose_sum: 357.0256508886814
time_elpased: 1.201
batch start
#iterations: 256
currently lose_sum: 355.54172533750534
time_elpased: 1.254
batch start
#iterations: 257
currently lose_sum: 354.82014805078506
time_elpased: 1.217
batch start
#iterations: 258
currently lose_sum: 355.78712436556816
time_elpased: 1.226
batch start
#iterations: 259
currently lose_sum: 356.45173275470734
time_elpased: 1.2
start validation test
0.759175257732
1.0
0.759175257732
0.863103609939
0
validation finish
batch start
#iterations: 260
currently lose_sum: 354.2438310980797
time_elpased: 1.201
batch start
#iterations: 261
currently lose_sum: 356.30568239092827
time_elpased: 1.209
batch start
#iterations: 262
currently lose_sum: 354.3493466973305
time_elpased: 1.218
batch start
#iterations: 263
currently lose_sum: 354.54362750053406
time_elpased: 1.249
batch start
#iterations: 264
currently lose_sum: 354.5647890865803
time_elpased: 1.21
batch start
#iterations: 265
currently lose_sum: 355.2799383997917
time_elpased: 1.201
batch start
#iterations: 266
currently lose_sum: 353.5338887870312
time_elpased: 1.19
batch start
#iterations: 267
currently lose_sum: 354.949695289135
time_elpased: 1.182
batch start
#iterations: 268
currently lose_sum: 353.38450676202774
time_elpased: 1.19
batch start
#iterations: 269
currently lose_sum: 354.27294477820396
time_elpased: 1.194
batch start
#iterations: 270
currently lose_sum: 352.9852827191353
time_elpased: 1.203
batch start
#iterations: 271
currently lose_sum: 355.72820660471916
time_elpased: 1.219
batch start
#iterations: 272
currently lose_sum: 354.11878177523613
time_elpased: 1.218
batch start
#iterations: 273
currently lose_sum: 353.89764761924744
time_elpased: 1.208
batch start
#iterations: 274
currently lose_sum: 354.4359499812126
time_elpased: 1.182
batch start
#iterations: 275
currently lose_sum: 354.8569776415825
time_elpased: 1.207
batch start
#iterations: 276
currently lose_sum: 353.1153674721718
time_elpased: 1.184
batch start
#iterations: 277
currently lose_sum: 355.5446434020996
time_elpased: 1.186
batch start
#iterations: 278
currently lose_sum: 354.38206404447556
time_elpased: 1.195
batch start
#iterations: 279
currently lose_sum: 354.49130472540855
time_elpased: 1.218
start validation test
0.781958762887
1.0
0.781958762887
0.877639571883
0
validation finish
batch start
#iterations: 280
currently lose_sum: 353.43464344739914
time_elpased: 1.204
batch start
#iterations: 281
currently lose_sum: 353.8134534955025
time_elpased: 1.199
batch start
#iterations: 282
currently lose_sum: 354.2096738219261
time_elpased: 1.203
batch start
#iterations: 283
currently lose_sum: 352.66707372665405
time_elpased: 1.212
batch start
#iterations: 284
currently lose_sum: 355.3146846294403
time_elpased: 1.19
batch start
#iterations: 285
currently lose_sum: 354.1975352168083
time_elpased: 1.253
batch start
#iterations: 286
currently lose_sum: 354.79669907689095
time_elpased: 1.206
batch start
#iterations: 287
currently lose_sum: 355.8894909322262
time_elpased: 1.256
batch start
#iterations: 288
currently lose_sum: 354.74257802963257
time_elpased: 1.192
batch start
#iterations: 289
currently lose_sum: 352.4260750114918
time_elpased: 1.21
batch start
#iterations: 290
currently lose_sum: 353.2615062892437
time_elpased: 1.189
batch start
#iterations: 291
currently lose_sum: 354.19459518790245
time_elpased: 1.241
batch start
#iterations: 292
currently lose_sum: 355.0427785515785
time_elpased: 1.236
batch start
#iterations: 293
currently lose_sum: 354.1775622367859
time_elpased: 1.197
batch start
#iterations: 294
currently lose_sum: 353.7786079645157
time_elpased: 1.187
batch start
#iterations: 295
currently lose_sum: 353.8722003996372
time_elpased: 1.212
batch start
#iterations: 296
currently lose_sum: 352.8943080306053
time_elpased: 1.207
batch start
#iterations: 297
currently lose_sum: 355.26024931669235
time_elpased: 1.21
batch start
#iterations: 298
currently lose_sum: 355.6146342754364
time_elpased: 1.195
batch start
#iterations: 299
currently lose_sum: 353.62705409526825
time_elpased: 1.2
start validation test
0.774226804124
1.0
0.774226804124
0.872748402092
0
validation finish
batch start
#iterations: 300
currently lose_sum: 353.46924591064453
time_elpased: 1.2
batch start
#iterations: 301
currently lose_sum: 354.9255394935608
time_elpased: 1.237
batch start
#iterations: 302
currently lose_sum: 353.97745299339294
time_elpased: 1.19
batch start
#iterations: 303
currently lose_sum: 353.4471606016159
time_elpased: 1.247
batch start
#iterations: 304
currently lose_sum: 354.22540882229805
time_elpased: 1.201
batch start
#iterations: 305
currently lose_sum: 354.8668295741081
time_elpased: 1.197
batch start
#iterations: 306
currently lose_sum: 355.22469824552536
time_elpased: 1.215
batch start
#iterations: 307
currently lose_sum: 354.75464379787445
time_elpased: 1.19
batch start
#iterations: 308
currently lose_sum: 353.8610999584198
time_elpased: 1.252
batch start
#iterations: 309
currently lose_sum: 353.6128032207489
time_elpased: 1.194
batch start
#iterations: 310
currently lose_sum: 354.5595415234566
time_elpased: 1.209
batch start
#iterations: 311
currently lose_sum: 356.6738948225975
time_elpased: 1.202
batch start
#iterations: 312
currently lose_sum: 353.65919998288155
time_elpased: 1.252
batch start
#iterations: 313
currently lose_sum: 353.97324603796005
time_elpased: 1.192
batch start
#iterations: 314
currently lose_sum: 352.8126342892647
time_elpased: 1.251
batch start
#iterations: 315
currently lose_sum: 354.16969007253647
time_elpased: 1.223
batch start
#iterations: 316
currently lose_sum: 353.7998893857002
time_elpased: 1.255
batch start
#iterations: 317
currently lose_sum: 353.63186544179916
time_elpased: 1.199
batch start
#iterations: 318
currently lose_sum: 355.2984048128128
time_elpased: 1.227
batch start
#iterations: 319
currently lose_sum: 353.55140537023544
time_elpased: 1.208
start validation test
0.77824742268
1.0
0.77824742268
0.875297118674
0
validation finish
batch start
#iterations: 320
currently lose_sum: 354.00343707203865
time_elpased: 1.197
batch start
#iterations: 321
currently lose_sum: 354.18222016096115
time_elpased: 1.211
batch start
#iterations: 322
currently lose_sum: 354.3917142152786
time_elpased: 1.2
batch start
#iterations: 323
currently lose_sum: 354.61342507600784
time_elpased: 1.206
batch start
#iterations: 324
currently lose_sum: 352.0340863466263
time_elpased: 1.206
batch start
#iterations: 325
currently lose_sum: 352.91593796014786
time_elpased: 1.208
batch start
#iterations: 326
currently lose_sum: 353.3410014510155
time_elpased: 1.268
batch start
#iterations: 327
currently lose_sum: 354.3502421081066
time_elpased: 1.2
batch start
#iterations: 328
currently lose_sum: 355.83682999014854
time_elpased: 1.218
batch start
#iterations: 329
currently lose_sum: 355.73449844121933
time_elpased: 1.202
batch start
#iterations: 330
currently lose_sum: 353.1056900024414
time_elpased: 1.273
batch start
#iterations: 331
currently lose_sum: 355.7718527317047
time_elpased: 1.2
batch start
#iterations: 332
currently lose_sum: 354.1299504041672
time_elpased: 1.203
batch start
#iterations: 333
currently lose_sum: 355.270510494709
time_elpased: 1.201
batch start
#iterations: 334
currently lose_sum: 353.19940263032913
time_elpased: 1.231
batch start
#iterations: 335
currently lose_sum: 353.18075439333916
time_elpased: 1.207
batch start
#iterations: 336
currently lose_sum: 353.02834689617157
time_elpased: 1.217
batch start
#iterations: 337
currently lose_sum: 354.14326363801956
time_elpased: 1.251
batch start
#iterations: 338
currently lose_sum: 352.9331570863724
time_elpased: 1.204
batch start
#iterations: 339
currently lose_sum: 353.5292815864086
time_elpased: 1.211
start validation test
0.780309278351
1.0
0.780309278351
0.876599687301
0
validation finish
batch start
#iterations: 340
currently lose_sum: 353.2067950963974
time_elpased: 1.184
batch start
#iterations: 341
currently lose_sum: 352.7651316523552
time_elpased: 1.226
batch start
#iterations: 342
currently lose_sum: 354.7131218612194
time_elpased: 1.256
batch start
#iterations: 343
currently lose_sum: 353.45759177207947
time_elpased: 1.19
batch start
#iterations: 344
currently lose_sum: 353.71627032756805
time_elpased: 1.202
batch start
#iterations: 345
currently lose_sum: 354.2712700366974
time_elpased: 1.205
batch start
#iterations: 346
currently lose_sum: 354.3557641506195
time_elpased: 1.257
batch start
#iterations: 347
currently lose_sum: 353.91959005594254
time_elpased: 1.205
batch start
#iterations: 348
currently lose_sum: 353.9907065629959
time_elpased: 1.216
batch start
#iterations: 349
currently lose_sum: 354.84563183784485
time_elpased: 1.209
batch start
#iterations: 350
currently lose_sum: 354.4376944899559
time_elpased: 1.201
batch start
#iterations: 351
currently lose_sum: 353.79509967565536
time_elpased: 1.206
batch start
#iterations: 352
currently lose_sum: 352.566059589386
time_elpased: 1.205
batch start
#iterations: 353
currently lose_sum: 353.9045503437519
time_elpased: 1.197
batch start
#iterations: 354
currently lose_sum: 352.9781524538994
time_elpased: 1.193
batch start
#iterations: 355
currently lose_sum: 352.3818156719208
time_elpased: 1.239
batch start
#iterations: 356
currently lose_sum: 354.28292125463486
time_elpased: 1.206
batch start
#iterations: 357
currently lose_sum: 352.84481942653656
time_elpased: 1.224
batch start
#iterations: 358
currently lose_sum: 353.6156939268112
time_elpased: 1.204
batch start
#iterations: 359
currently lose_sum: 353.6362566947937
time_elpased: 1.204
start validation test
0.777216494845
1.0
0.777216494845
0.874644700969
0
validation finish
batch start
#iterations: 360
currently lose_sum: 355.4496593475342
time_elpased: 1.243
batch start
#iterations: 361
currently lose_sum: 353.3659679889679
time_elpased: 1.232
batch start
#iterations: 362
currently lose_sum: 353.2836321592331
time_elpased: 1.198
batch start
#iterations: 363
currently lose_sum: 353.5897409617901
time_elpased: 1.243
batch start
#iterations: 364
currently lose_sum: 355.03236043453217
time_elpased: 1.219
batch start
#iterations: 365
currently lose_sum: 353.7043513059616
time_elpased: 1.19
batch start
#iterations: 366
currently lose_sum: 353.2167401313782
time_elpased: 1.198
batch start
#iterations: 367
currently lose_sum: 354.34569799900055
time_elpased: 1.236
batch start
#iterations: 368
currently lose_sum: 353.37667939066887
time_elpased: 1.216
batch start
#iterations: 369
currently lose_sum: 354.86589673161507
time_elpased: 1.201
batch start
#iterations: 370
currently lose_sum: 354.37583860754967
time_elpased: 1.194
batch start
#iterations: 371
currently lose_sum: 354.0489110350609
time_elpased: 1.196
batch start
#iterations: 372
currently lose_sum: 353.6322519183159
time_elpased: 1.2
batch start
#iterations: 373
currently lose_sum: 352.67047718167305
time_elpased: 1.261
batch start
#iterations: 374
currently lose_sum: 354.6388393342495
time_elpased: 1.209
batch start
#iterations: 375
currently lose_sum: 355.1890940964222
time_elpased: 1.192
batch start
#iterations: 376
currently lose_sum: 354.0535175204277
time_elpased: 1.201
batch start
#iterations: 377
currently lose_sum: 352.4388653039932
time_elpased: 1.225
batch start
#iterations: 378
currently lose_sum: 354.9817708134651
time_elpased: 1.193
batch start
#iterations: 379
currently lose_sum: 353.10823503136635
time_elpased: 1.199
start validation test
0.775670103093
1.0
0.775670103093
0.873664653971
0
validation finish
batch start
#iterations: 380
currently lose_sum: 354.51911836862564
time_elpased: 1.2
batch start
#iterations: 381
currently lose_sum: 353.88526543974876
time_elpased: 1.191
batch start
#iterations: 382
currently lose_sum: 353.3749077320099
time_elpased: 1.194
batch start
#iterations: 383
currently lose_sum: 352.17111602425575
time_elpased: 1.197
batch start
#iterations: 384
currently lose_sum: 353.6286307275295
time_elpased: 1.2
batch start
#iterations: 385
currently lose_sum: 351.759889125824
time_elpased: 1.205
batch start
#iterations: 386
currently lose_sum: 354.1112864613533
time_elpased: 1.189
batch start
#iterations: 387
currently lose_sum: 353.1036465167999
time_elpased: 1.207
batch start
#iterations: 388
currently lose_sum: 353.1813404560089
time_elpased: 1.188
batch start
#iterations: 389
currently lose_sum: 352.63518196344376
time_elpased: 1.209
batch start
#iterations: 390
currently lose_sum: 352.31080332398415
time_elpased: 1.207
batch start
#iterations: 391
currently lose_sum: 351.43882325291634
time_elpased: 1.205
batch start
#iterations: 392
currently lose_sum: 354.03741016983986
time_elpased: 1.205
batch start
#iterations: 393
currently lose_sum: 354.59557300806046
time_elpased: 1.197
batch start
#iterations: 394
currently lose_sum: 352.6651991903782
time_elpased: 1.254
batch start
#iterations: 395
currently lose_sum: 353.35491997003555
time_elpased: 1.202
batch start
#iterations: 396
currently lose_sum: 353.21707487106323
time_elpased: 1.208
batch start
#iterations: 397
currently lose_sum: 353.6206550002098
time_elpased: 1.248
batch start
#iterations: 398
currently lose_sum: 353.7515178620815
time_elpased: 1.194
batch start
#iterations: 399
currently lose_sum: 354.6880446076393
time_elpased: 1.203
start validation test
0.782989690722
1.0
0.782989690722
0.878288522694
0
validation finish
acc: 0.800
pre: 1.000
rec: 0.800
F1: 0.889
auc: 0.000
