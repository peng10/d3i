start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 549.09141010046
time_elpased: 1.416
batch start
#iterations: 1
currently lose_sum: 536.1507662534714
time_elpased: 1.375
batch start
#iterations: 2
currently lose_sum: 533.4010139107704
time_elpased: 1.367
batch start
#iterations: 3
currently lose_sum: 530.7951875030994
time_elpased: 1.377
batch start
#iterations: 4
currently lose_sum: 528.1066987216473
time_elpased: 1.376
batch start
#iterations: 5
currently lose_sum: 526.2929027080536
time_elpased: 1.382
batch start
#iterations: 6
currently lose_sum: 524.8811025321484
time_elpased: 1.378
batch start
#iterations: 7
currently lose_sum: 524.2277924418449
time_elpased: 1.377
batch start
#iterations: 8
currently lose_sum: 523.7484664022923
time_elpased: 1.375
batch start
#iterations: 9
currently lose_sum: 524.958590567112
time_elpased: 1.38
batch start
#iterations: 10
currently lose_sum: 523.8638483285904
time_elpased: 1.386
batch start
#iterations: 11
currently lose_sum: 525.3680164217949
time_elpased: 1.38
batch start
#iterations: 12
currently lose_sum: 523.2385065853596
time_elpased: 1.384
batch start
#iterations: 13
currently lose_sum: 521.3710777461529
time_elpased: 1.38
batch start
#iterations: 14
currently lose_sum: 521.8387566804886
time_elpased: 1.37
batch start
#iterations: 15
currently lose_sum: 520.4443975687027
time_elpased: 1.381
batch start
#iterations: 16
currently lose_sum: 520.7991791367531
time_elpased: 1.377
batch start
#iterations: 17
currently lose_sum: 521.9296015501022
time_elpased: 1.373
batch start
#iterations: 18
currently lose_sum: 521.739240705967
time_elpased: 1.376
batch start
#iterations: 19
currently lose_sum: 521.5633768439293
time_elpased: 1.378
start validation test
0.170515463918
1.0
0.170515463918
0.291351065704
0
validation finish
batch start
#iterations: 20
currently lose_sum: 519.64573392272
time_elpased: 1.384
batch start
#iterations: 21
currently lose_sum: 520.6887530088425
time_elpased: 1.393
batch start
#iterations: 22
currently lose_sum: 520.8421488106251
time_elpased: 1.383
batch start
#iterations: 23
currently lose_sum: 521.1323928236961
time_elpased: 1.375
batch start
#iterations: 24
currently lose_sum: 519.2901011407375
time_elpased: 1.369
batch start
#iterations: 25
currently lose_sum: 519.1103873550892
time_elpased: 1.393
batch start
#iterations: 26
currently lose_sum: 520.0223618745804
time_elpased: 1.384
batch start
#iterations: 27
currently lose_sum: 519.5779550969601
time_elpased: 1.389
batch start
#iterations: 28
currently lose_sum: 517.8820890188217
time_elpased: 1.371
batch start
#iterations: 29
currently lose_sum: 517.345725953579
time_elpased: 1.388
batch start
#iterations: 30
currently lose_sum: 517.574267834425
time_elpased: 1.388
batch start
#iterations: 31
currently lose_sum: 518.9858643710613
time_elpased: 1.383
batch start
#iterations: 32
currently lose_sum: 519.6368025839329
time_elpased: 1.385
batch start
#iterations: 33
currently lose_sum: 518.6997571289539
time_elpased: 1.405
batch start
#iterations: 34
currently lose_sum: 518.36915153265
time_elpased: 1.378
batch start
#iterations: 35
currently lose_sum: 518.2457747757435
time_elpased: 1.367
batch start
#iterations: 36
currently lose_sum: 519.1126970350742
time_elpased: 1.384
batch start
#iterations: 37
currently lose_sum: 516.70470225811
time_elpased: 1.385
batch start
#iterations: 38
currently lose_sum: 517.7233752012253
time_elpased: 1.382
batch start
#iterations: 39
currently lose_sum: 518.5668237507343
time_elpased: 1.376
start validation test
0.163917525773
1.0
0.163917525773
0.281665190434
0
validation finish
batch start
#iterations: 40
currently lose_sum: 518.3636996746063
time_elpased: 1.365
batch start
#iterations: 41
currently lose_sum: 518.6686544716358
time_elpased: 1.381
batch start
#iterations: 42
currently lose_sum: 517.478033810854
time_elpased: 1.382
batch start
#iterations: 43
currently lose_sum: 516.5375137329102
time_elpased: 1.381
batch start
#iterations: 44
currently lose_sum: 519.3738072812557
time_elpased: 1.365
batch start
#iterations: 45
currently lose_sum: 519.6202271282673
time_elpased: 1.371
batch start
#iterations: 46
currently lose_sum: 517.0354129076004
time_elpased: 1.395
batch start
#iterations: 47
currently lose_sum: 518.2608244717121
time_elpased: 1.375
batch start
#iterations: 48
currently lose_sum: 518.5739105641842
time_elpased: 1.381
batch start
#iterations: 49
currently lose_sum: 514.7542735636234
time_elpased: 1.396
batch start
#iterations: 50
currently lose_sum: 518.7877428233624
time_elpased: 1.38
batch start
#iterations: 51
currently lose_sum: 517.7689545154572
time_elpased: 1.381
batch start
#iterations: 52
currently lose_sum: 517.1681243777275
time_elpased: 1.372
batch start
#iterations: 53
currently lose_sum: 518.354872405529
time_elpased: 1.38
batch start
#iterations: 54
currently lose_sum: 516.8257522583008
time_elpased: 1.382
batch start
#iterations: 55
currently lose_sum: 518.8415661156178
time_elpased: 1.372
batch start
#iterations: 56
currently lose_sum: 518.3343036174774
time_elpased: 1.39
batch start
#iterations: 57
currently lose_sum: 516.7358932197094
time_elpased: 1.381
batch start
#iterations: 58
currently lose_sum: 518.129193931818
time_elpased: 1.385
batch start
#iterations: 59
currently lose_sum: 517.0808820128441
time_elpased: 1.373
start validation test
0.176701030928
1.0
0.176701030928
0.300332924479
0
validation finish
batch start
#iterations: 60
currently lose_sum: 516.5539397895336
time_elpased: 1.372
batch start
#iterations: 61
currently lose_sum: 516.4354946315289
time_elpased: 1.379
batch start
#iterations: 62
currently lose_sum: 519.4596013426781
time_elpased: 1.387
batch start
#iterations: 63
currently lose_sum: 519.0643516778946
time_elpased: 1.383
batch start
#iterations: 64
currently lose_sum: 517.1672225892544
time_elpased: 1.375
batch start
#iterations: 65
currently lose_sum: 517.7230362296104
time_elpased: 1.387
batch start
#iterations: 66
currently lose_sum: 517.6399188041687
time_elpased: 1.375
batch start
#iterations: 67
currently lose_sum: 515.6847237348557
time_elpased: 1.384
batch start
#iterations: 68
currently lose_sum: 517.2826746702194
time_elpased: 1.377
batch start
#iterations: 69
currently lose_sum: 519.8816727399826
time_elpased: 1.403
batch start
#iterations: 70
currently lose_sum: 515.0667158961296
time_elpased: 1.372
batch start
#iterations: 71
currently lose_sum: 518.3797011375427
time_elpased: 1.376
batch start
#iterations: 72
currently lose_sum: 518.7595398128033
time_elpased: 1.4
batch start
#iterations: 73
currently lose_sum: 516.257786244154
time_elpased: 1.382
batch start
#iterations: 74
currently lose_sum: 518.3022809624672
time_elpased: 1.382
batch start
#iterations: 75
currently lose_sum: 516.7003411054611
time_elpased: 1.382
batch start
#iterations: 76
currently lose_sum: 516.3590546548367
time_elpased: 1.38
batch start
#iterations: 77
currently lose_sum: 517.4779543876648
time_elpased: 1.374
batch start
#iterations: 78
currently lose_sum: 517.7893905341625
time_elpased: 1.397
batch start
#iterations: 79
currently lose_sum: 519.4917470216751
time_elpased: 1.382
start validation test
0.209896907216
1.0
0.209896907216
0.3469665985
0
validation finish
batch start
#iterations: 80
currently lose_sum: 516.9501980245113
time_elpased: 1.392
batch start
#iterations: 81
currently lose_sum: 516.5882987380028
time_elpased: 1.387
batch start
#iterations: 82
currently lose_sum: 517.6775329113007
time_elpased: 1.383
batch start
#iterations: 83
currently lose_sum: 516.3511351943016
time_elpased: 1.379
batch start
#iterations: 84
currently lose_sum: 517.1373761296272
time_elpased: 1.374
batch start
#iterations: 85
currently lose_sum: 515.7722638249397
time_elpased: 1.405
batch start
#iterations: 86
currently lose_sum: 517.2973502278328
time_elpased: 1.368
batch start
#iterations: 87
currently lose_sum: 516.1800635457039
time_elpased: 1.384
batch start
#iterations: 88
currently lose_sum: 517.5990342795849
time_elpased: 1.384
batch start
#iterations: 89
currently lose_sum: 518.0700308382511
time_elpased: 1.378
batch start
#iterations: 90
currently lose_sum: 517.0989556908607
time_elpased: 1.383
batch start
#iterations: 91
currently lose_sum: 515.7224097549915
time_elpased: 1.396
batch start
#iterations: 92
currently lose_sum: 516.5177702903748
time_elpased: 1.405
batch start
#iterations: 93
currently lose_sum: 517.4062508642673
time_elpased: 1.388
batch start
#iterations: 94
currently lose_sum: 516.4034000039101
time_elpased: 1.381
batch start
#iterations: 95
currently lose_sum: 516.4234399199486
time_elpased: 1.391
batch start
#iterations: 96
currently lose_sum: 518.2117767333984
time_elpased: 1.393
batch start
#iterations: 97
currently lose_sum: 514.3880103826523
time_elpased: 1.375
batch start
#iterations: 98
currently lose_sum: 517.5231798291206
time_elpased: 1.377
batch start
#iterations: 99
currently lose_sum: 518.1710106432438
time_elpased: 1.381
start validation test
0.15793814433
1.0
0.15793814433
0.272792022792
0
validation finish
batch start
#iterations: 100
currently lose_sum: 516.6245872080326
time_elpased: 1.383
batch start
#iterations: 101
currently lose_sum: 517.5269622206688
time_elpased: 1.391
batch start
#iterations: 102
currently lose_sum: 515.11174467206
time_elpased: 1.373
batch start
#iterations: 103
currently lose_sum: 517.7747307121754
time_elpased: 1.385
batch start
#iterations: 104
currently lose_sum: 516.8574068248272
time_elpased: 1.381
batch start
#iterations: 105
currently lose_sum: 516.7896842360497
time_elpased: 1.387
batch start
#iterations: 106
currently lose_sum: 518.5155112147331
time_elpased: 1.373
batch start
#iterations: 107
currently lose_sum: 516.4447407722473
time_elpased: 1.382
batch start
#iterations: 108
currently lose_sum: 515.088924318552
time_elpased: 1.41
batch start
#iterations: 109
currently lose_sum: 517.3732632696629
time_elpased: 1.386
batch start
#iterations: 110
currently lose_sum: 519.0271657705307
time_elpased: 1.385
batch start
#iterations: 111
currently lose_sum: 519.7198287546635
time_elpased: 1.386
batch start
#iterations: 112
currently lose_sum: 515.6180870831013
time_elpased: 1.386
batch start
#iterations: 113
currently lose_sum: 517.4149849414825
time_elpased: 1.386
batch start
#iterations: 114
currently lose_sum: 517.1263340711594
time_elpased: 1.398
batch start
#iterations: 115
currently lose_sum: 516.6312319934368
time_elpased: 1.375
batch start
#iterations: 116
currently lose_sum: 515.5152770876884
time_elpased: 1.389
batch start
#iterations: 117
currently lose_sum: 518.0575993955135
time_elpased: 1.394
batch start
#iterations: 118
currently lose_sum: 516.2421767413616
time_elpased: 1.378
batch start
#iterations: 119
currently lose_sum: 516.5443849861622
time_elpased: 1.383
start validation test
0.190103092784
1.0
0.190103092784
0.319473319473
0
validation finish
batch start
#iterations: 120
currently lose_sum: 517.2606415450573
time_elpased: 1.385
batch start
#iterations: 121
currently lose_sum: 516.0371864140034
time_elpased: 1.385
batch start
#iterations: 122
currently lose_sum: 515.856652200222
time_elpased: 1.398
batch start
#iterations: 123
currently lose_sum: 516.472632586956
time_elpased: 1.388
batch start
#iterations: 124
currently lose_sum: 516.8039658367634
time_elpased: 1.377
batch start
#iterations: 125
currently lose_sum: 517.4563817977905
time_elpased: 1.368
batch start
#iterations: 126
currently lose_sum: 517.6003394126892
time_elpased: 1.398
batch start
#iterations: 127
currently lose_sum: 516.5265931487083
time_elpased: 1.373
batch start
#iterations: 128
currently lose_sum: 516.6234799623489
time_elpased: 1.388
batch start
#iterations: 129
currently lose_sum: 516.9583157598972
time_elpased: 1.388
batch start
#iterations: 130
currently lose_sum: 515.8378212153912
time_elpased: 1.384
batch start
#iterations: 131
currently lose_sum: 517.3288435935974
time_elpased: 1.392
batch start
#iterations: 132
currently lose_sum: 517.1834133565426
time_elpased: 1.393
batch start
#iterations: 133
currently lose_sum: 518.3172847926617
time_elpased: 1.385
batch start
#iterations: 134
currently lose_sum: 516.0581193566322
time_elpased: 1.38
batch start
#iterations: 135
currently lose_sum: 515.353834092617
time_elpased: 1.39
batch start
#iterations: 136
currently lose_sum: 516.5320310294628
time_elpased: 1.372
batch start
#iterations: 137
currently lose_sum: 515.9311563372612
time_elpased: 1.374
batch start
#iterations: 138
currently lose_sum: 514.8354706466198
time_elpased: 1.377
batch start
#iterations: 139
currently lose_sum: 516.9286606609821
time_elpased: 1.386
start validation test
0.182577319588
1.0
0.182577319588
0.308778659228
0
validation finish
batch start
#iterations: 140
currently lose_sum: 514.0431055724621
time_elpased: 1.397
batch start
#iterations: 141
currently lose_sum: 516.17929276824
time_elpased: 1.389
batch start
#iterations: 142
currently lose_sum: 514.7537777125835
time_elpased: 1.393
batch start
#iterations: 143
currently lose_sum: 517.2412960231304
time_elpased: 1.382
batch start
#iterations: 144
currently lose_sum: 516.0516805946827
time_elpased: 1.374
batch start
#iterations: 145
currently lose_sum: 518.2837677896023
time_elpased: 1.374
batch start
#iterations: 146
currently lose_sum: 518.6697049736977
time_elpased: 1.388
batch start
#iterations: 147
currently lose_sum: 517.5641236305237
time_elpased: 1.378
batch start
#iterations: 148
currently lose_sum: 515.1239955723286
time_elpased: 1.4
batch start
#iterations: 149
currently lose_sum: 518.4217930734158
time_elpased: 1.374
batch start
#iterations: 150
currently lose_sum: 515.1346018612385
time_elpased: 1.377
batch start
#iterations: 151
currently lose_sum: 515.0678580701351
time_elpased: 1.395
batch start
#iterations: 152
currently lose_sum: 516.0457788109779
time_elpased: 1.39
batch start
#iterations: 153
currently lose_sum: 514.110656619072
time_elpased: 1.371
batch start
#iterations: 154
currently lose_sum: 518.6146044135094
time_elpased: 1.388
batch start
#iterations: 155
currently lose_sum: 513.834478199482
time_elpased: 1.393
batch start
#iterations: 156
currently lose_sum: 516.3062154352665
time_elpased: 1.383
batch start
#iterations: 157
currently lose_sum: 514.647359251976
time_elpased: 1.391
batch start
#iterations: 158
currently lose_sum: 515.4130213260651
time_elpased: 1.385
batch start
#iterations: 159
currently lose_sum: 516.315001398325
time_elpased: 1.376
start validation test
0.182474226804
1.0
0.182474226804
0.308631211857
0
validation finish
batch start
#iterations: 160
currently lose_sum: 516.9079411029816
time_elpased: 1.394
batch start
#iterations: 161
currently lose_sum: 515.0057995915413
time_elpased: 1.386
batch start
#iterations: 162
currently lose_sum: 515.7472193539143
time_elpased: 1.395
batch start
#iterations: 163
currently lose_sum: 516.1475096344948
time_elpased: 1.403
batch start
#iterations: 164
currently lose_sum: 516.6414947509766
time_elpased: 1.369
batch start
#iterations: 165
currently lose_sum: 518.2921870052814
time_elpased: 1.388
batch start
#iterations: 166
currently lose_sum: 516.8810428380966
time_elpased: 1.369
batch start
#iterations: 167
currently lose_sum: 516.3042465746403
time_elpased: 1.388
batch start
#iterations: 168
currently lose_sum: 515.9213347434998
time_elpased: 1.379
batch start
#iterations: 169
currently lose_sum: 516.2178682684898
time_elpased: 1.385
batch start
#iterations: 170
currently lose_sum: 515.8763891756535
time_elpased: 1.372
batch start
#iterations: 171
currently lose_sum: 514.6581544876099
time_elpased: 1.391
batch start
#iterations: 172
currently lose_sum: 518.3772045373917
time_elpased: 1.38
batch start
#iterations: 173
currently lose_sum: 516.7944376468658
time_elpased: 1.384
batch start
#iterations: 174
currently lose_sum: 517.3238048553467
time_elpased: 1.38
batch start
#iterations: 175
currently lose_sum: 515.7813990712166
time_elpased: 1.384
batch start
#iterations: 176
currently lose_sum: 515.8875242173672
time_elpased: 1.39
batch start
#iterations: 177
currently lose_sum: 517.230605006218
time_elpased: 1.396
batch start
#iterations: 178
currently lose_sum: 517.7698155939579
time_elpased: 1.377
batch start
#iterations: 179
currently lose_sum: 518.3085358738899
time_elpased: 1.394
start validation test
0.18618556701
1.0
0.18618556701
0.31392317052
0
validation finish
batch start
#iterations: 180
currently lose_sum: 517.5188764035702
time_elpased: 1.384
batch start
#iterations: 181
currently lose_sum: 518.346481025219
time_elpased: 1.393
batch start
#iterations: 182
currently lose_sum: 516.1780843734741
time_elpased: 1.377
batch start
#iterations: 183
currently lose_sum: 516.8582541942596
time_elpased: 1.384
batch start
#iterations: 184
currently lose_sum: 515.7752895653248
time_elpased: 1.378
batch start
#iterations: 185
currently lose_sum: 517.608852148056
time_elpased: 1.386
batch start
#iterations: 186
currently lose_sum: 517.1429302990437
time_elpased: 1.395
batch start
#iterations: 187
currently lose_sum: 517.0885471701622
time_elpased: 1.394
batch start
#iterations: 188
currently lose_sum: 515.7505277693272
time_elpased: 1.384
batch start
#iterations: 189
currently lose_sum: 516.4132942855358
time_elpased: 1.39
batch start
#iterations: 190
currently lose_sum: 515.9014582335949
time_elpased: 1.39
batch start
#iterations: 191
currently lose_sum: 517.4777057766914
time_elpased: 1.39
batch start
#iterations: 192
currently lose_sum: 514.2972283363342
time_elpased: 1.398
batch start
#iterations: 193
currently lose_sum: 516.8599678277969
time_elpased: 1.398
batch start
#iterations: 194
currently lose_sum: 517.6794331967831
time_elpased: 1.39
batch start
#iterations: 195
currently lose_sum: 519.3679646253586
time_elpased: 1.37
batch start
#iterations: 196
currently lose_sum: 514.2388303875923
time_elpased: 1.377
batch start
#iterations: 197
currently lose_sum: 517.4066816568375
time_elpased: 1.38
batch start
#iterations: 198
currently lose_sum: 515.4430567026138
time_elpased: 1.388
batch start
#iterations: 199
currently lose_sum: 516.1330394744873
time_elpased: 1.378
start validation test
0.176494845361
1.0
0.176494845361
0.300035050824
0
validation finish
batch start
#iterations: 200
currently lose_sum: 516.4767669439316
time_elpased: 1.408
batch start
#iterations: 201
currently lose_sum: 518.2692985832691
time_elpased: 1.384
batch start
#iterations: 202
currently lose_sum: 515.4663136601448
time_elpased: 1.397
batch start
#iterations: 203
currently lose_sum: 515.1986098587513
time_elpased: 1.379
batch start
#iterations: 204
currently lose_sum: 515.477425724268
time_elpased: 1.374
batch start
#iterations: 205
currently lose_sum: 516.4791594445705
time_elpased: 1.387
batch start
#iterations: 206
currently lose_sum: 518.8269374370575
time_elpased: 1.388
batch start
#iterations: 207
currently lose_sum: 515.743314653635
time_elpased: 1.398
batch start
#iterations: 208
currently lose_sum: 514.7532496452332
time_elpased: 1.396
batch start
#iterations: 209
currently lose_sum: 516.7014198601246
time_elpased: 1.392
batch start
#iterations: 210
currently lose_sum: 515.5434738695621
time_elpased: 1.383
batch start
#iterations: 211
currently lose_sum: 516.5578551888466
time_elpased: 1.383
batch start
#iterations: 212
currently lose_sum: 516.2594682276249
time_elpased: 1.388
batch start
#iterations: 213
currently lose_sum: 516.23397397995
time_elpased: 1.38
batch start
#iterations: 214
currently lose_sum: 517.4334944784641
time_elpased: 1.377
batch start
#iterations: 215
currently lose_sum: 516.52944201231
time_elpased: 1.408
batch start
#iterations: 216
currently lose_sum: 517.0819123089314
time_elpased: 1.371
batch start
#iterations: 217
currently lose_sum: 515.8610883951187
time_elpased: 1.388
batch start
#iterations: 218
currently lose_sum: 515.5042045116425
time_elpased: 1.395
batch start
#iterations: 219
currently lose_sum: 517.8492112159729
time_elpased: 1.391
start validation test
0.190309278351
1.0
0.190309278351
0.319764420579
0
validation finish
batch start
#iterations: 220
currently lose_sum: 516.4645088613033
time_elpased: 1.375
batch start
#iterations: 221
currently lose_sum: 516.8248948454857
time_elpased: 1.376
batch start
#iterations: 222
currently lose_sum: 515.0545642077923
time_elpased: 1.383
batch start
#iterations: 223
currently lose_sum: 516.870786190033
time_elpased: 1.388
batch start
#iterations: 224
currently lose_sum: 516.7264127135277
time_elpased: 1.385
batch start
#iterations: 225
currently lose_sum: 519.1452179253101
time_elpased: 1.379
batch start
#iterations: 226
currently lose_sum: 516.3506274521351
time_elpased: 1.388
batch start
#iterations: 227
currently lose_sum: 515.8323917686939
time_elpased: 1.389
batch start
#iterations: 228
currently lose_sum: 514.5650906860828
time_elpased: 1.372
batch start
#iterations: 229
currently lose_sum: 516.8472589254379
time_elpased: 1.386
batch start
#iterations: 230
currently lose_sum: 517.3355111777782
time_elpased: 1.386
batch start
#iterations: 231
currently lose_sum: 518.7844518125057
time_elpased: 1.377
batch start
#iterations: 232
currently lose_sum: 515.9786148965359
time_elpased: 1.394
batch start
#iterations: 233
currently lose_sum: 514.0384243428707
time_elpased: 1.373
batch start
#iterations: 234
currently lose_sum: 514.9044036865234
time_elpased: 1.388
batch start
#iterations: 235
currently lose_sum: 515.5513406097889
time_elpased: 1.377
batch start
#iterations: 236
currently lose_sum: 515.4347245395184
time_elpased: 1.392
batch start
#iterations: 237
currently lose_sum: 516.7492307722569
time_elpased: 1.376
batch start
#iterations: 238
currently lose_sum: 514.9017759561539
time_elpased: 1.378
batch start
#iterations: 239
currently lose_sum: 515.3181186318398
time_elpased: 1.371
start validation test
0.15412371134
1.0
0.15412371134
0.267083519428
0
validation finish
batch start
#iterations: 240
currently lose_sum: 515.9459857642651
time_elpased: 1.378
batch start
#iterations: 241
currently lose_sum: 514.8042069375515
time_elpased: 1.376
batch start
#iterations: 242
currently lose_sum: 516.501694470644
time_elpased: 1.388
batch start
#iterations: 243
currently lose_sum: 514.9231266975403
time_elpased: 1.404
batch start
#iterations: 244
currently lose_sum: 516.2726603746414
time_elpased: 1.387
batch start
#iterations: 245
currently lose_sum: 515.0900958776474
time_elpased: 1.398
batch start
#iterations: 246
currently lose_sum: 515.3199428915977
time_elpased: 1.378
batch start
#iterations: 247
currently lose_sum: 515.8589718341827
time_elpased: 1.387
batch start
#iterations: 248
currently lose_sum: 515.2657311856747
time_elpased: 1.399
batch start
#iterations: 249
currently lose_sum: 516.0741044282913
time_elpased: 1.389
batch start
#iterations: 250
currently lose_sum: 515.5861499607563
time_elpased: 1.392
batch start
#iterations: 251
currently lose_sum: 517.567911118269
time_elpased: 1.387
batch start
#iterations: 252
currently lose_sum: 518.1636411249638
time_elpased: 1.383
batch start
#iterations: 253
currently lose_sum: 515.1781239509583
time_elpased: 1.374
batch start
#iterations: 254
currently lose_sum: 518.027261018753
time_elpased: 1.388
batch start
#iterations: 255
currently lose_sum: 515.3078100383282
time_elpased: 1.372
batch start
#iterations: 256
currently lose_sum: 517.6659703552723
time_elpased: 1.397
batch start
#iterations: 257
currently lose_sum: 514.3758499920368
time_elpased: 1.375
batch start
#iterations: 258
currently lose_sum: 514.8652104139328
time_elpased: 1.396
batch start
#iterations: 259
currently lose_sum: 517.1172427535057
time_elpased: 1.383
start validation test
0.194536082474
1.0
0.194536082474
0.325709847243
0
validation finish
batch start
#iterations: 260
currently lose_sum: 517.9687718153
time_elpased: 1.398
batch start
#iterations: 261
currently lose_sum: 516.8116304278374
time_elpased: 1.39
batch start
#iterations: 262
currently lose_sum: 515.8262307345867
time_elpased: 1.378
batch start
#iterations: 263
currently lose_sum: 515.6598489582539
time_elpased: 1.383
batch start
#iterations: 264
currently lose_sum: 515.4615768790245
time_elpased: 1.385
batch start
#iterations: 265
currently lose_sum: 516.9843866229057
time_elpased: 1.389
batch start
#iterations: 266
currently lose_sum: 516.3394966721535
time_elpased: 1.379
batch start
#iterations: 267
currently lose_sum: 514.7329829335213
time_elpased: 1.392
batch start
#iterations: 268
currently lose_sum: 515.7034501731396
time_elpased: 1.394
batch start
#iterations: 269
currently lose_sum: 514.4214422702789
time_elpased: 1.388
batch start
#iterations: 270
currently lose_sum: 517.3853530585766
time_elpased: 1.393
batch start
#iterations: 271
currently lose_sum: 514.1486113965511
time_elpased: 1.387
batch start
#iterations: 272
currently lose_sum: 515.8577878177166
time_elpased: 1.391
batch start
#iterations: 273
currently lose_sum: 516.2809784710407
time_elpased: 1.39
batch start
#iterations: 274
currently lose_sum: 516.3618948459625
time_elpased: 1.392
batch start
#iterations: 275
currently lose_sum: 515.6136903762817
time_elpased: 1.386
batch start
#iterations: 276
currently lose_sum: 515.1826319396496
time_elpased: 1.376
batch start
#iterations: 277
currently lose_sum: 516.5720340311527
time_elpased: 1.387
batch start
#iterations: 278
currently lose_sum: 514.9984222948551
time_elpased: 1.375
batch start
#iterations: 279
currently lose_sum: 516.7279823124409
time_elpased: 1.378
start validation test
0.179896907216
1.0
0.179896907216
0.304936653561
0
validation finish
batch start
#iterations: 280
currently lose_sum: 515.5449840128422
time_elpased: 1.391
batch start
#iterations: 281
currently lose_sum: 515.3738203048706
time_elpased: 1.382
batch start
#iterations: 282
currently lose_sum: 515.6521951258183
time_elpased: 1.394
batch start
#iterations: 283
currently lose_sum: 516.4137642085552
time_elpased: 1.387
batch start
#iterations: 284
currently lose_sum: 515.5692442953587
time_elpased: 1.373
batch start
#iterations: 285
currently lose_sum: 517.1214979290962
time_elpased: 1.371
batch start
#iterations: 286
currently lose_sum: 516.20314052701
time_elpased: 1.385
batch start
#iterations: 287
currently lose_sum: 517.9122464060783
time_elpased: 1.389
batch start
#iterations: 288
currently lose_sum: 515.3919113576412
time_elpased: 1.377
batch start
#iterations: 289
currently lose_sum: 515.4684060513973
time_elpased: 1.41
batch start
#iterations: 290
currently lose_sum: 514.0265695154667
time_elpased: 1.383
batch start
#iterations: 291
currently lose_sum: 516.8419628739357
time_elpased: 1.377
batch start
#iterations: 292
currently lose_sum: 515.8358356654644
time_elpased: 1.393
batch start
#iterations: 293
currently lose_sum: 516.6312898099422
time_elpased: 1.379
batch start
#iterations: 294
currently lose_sum: 517.504204839468
time_elpased: 1.388
batch start
#iterations: 295
currently lose_sum: 516.4339308440685
time_elpased: 1.375
batch start
#iterations: 296
currently lose_sum: 515.5401468276978
time_elpased: 1.398
batch start
#iterations: 297
currently lose_sum: 515.8480127155781
time_elpased: 1.396
batch start
#iterations: 298
currently lose_sum: 516.7980140149593
time_elpased: 1.383
batch start
#iterations: 299
currently lose_sum: 517.4228401184082
time_elpased: 1.387
start validation test
0.200515463918
1.0
0.200515463918
0.334048948046
0
validation finish
batch start
#iterations: 300
currently lose_sum: 516.5077053308487
time_elpased: 1.402
batch start
#iterations: 301
currently lose_sum: 514.7550656199455
time_elpased: 1.393
batch start
#iterations: 302
currently lose_sum: 516.8911962211132
time_elpased: 1.383
batch start
#iterations: 303
currently lose_sum: 517.1139416992664
time_elpased: 1.383
batch start
#iterations: 304
currently lose_sum: 514.7490058839321
time_elpased: 1.399
batch start
#iterations: 305
currently lose_sum: 517.4745716154575
time_elpased: 1.385
batch start
#iterations: 306
currently lose_sum: 516.5995174944401
time_elpased: 1.391
batch start
#iterations: 307
currently lose_sum: 517.573469042778
time_elpased: 1.404
batch start
#iterations: 308
currently lose_sum: 516.4813669025898
time_elpased: 1.372
batch start
#iterations: 309
currently lose_sum: 515.9638580679893
time_elpased: 1.388
batch start
#iterations: 310
currently lose_sum: 514.4877628684044
time_elpased: 1.387
batch start
#iterations: 311
currently lose_sum: 515.6132355630398
time_elpased: 1.41
batch start
#iterations: 312
currently lose_sum: 517.3624439537525
time_elpased: 1.379
batch start
#iterations: 313
currently lose_sum: 516.9156392514706
time_elpased: 1.413
batch start
#iterations: 314
currently lose_sum: 515.0961422920227
time_elpased: 1.377
batch start
#iterations: 315
currently lose_sum: 517.2545761466026
time_elpased: 1.385
batch start
#iterations: 316
currently lose_sum: 516.8311964571476
time_elpased: 1.392
batch start
#iterations: 317
currently lose_sum: 515.2032639980316
time_elpased: 1.372
batch start
#iterations: 318
currently lose_sum: 517.8070734739304
time_elpased: 1.406
batch start
#iterations: 319
currently lose_sum: 515.4152818322182
time_elpased: 1.387
start validation test
0.184226804124
1.0
0.184226804124
0.31113432576
0
validation finish
batch start
#iterations: 320
currently lose_sum: 516.8900297880173
time_elpased: 1.388
batch start
#iterations: 321
currently lose_sum: 517.22652810812
time_elpased: 1.375
batch start
#iterations: 322
currently lose_sum: 514.9464750289917
time_elpased: 1.39
batch start
#iterations: 323
currently lose_sum: 515.2958550155163
time_elpased: 1.378
batch start
#iterations: 324
currently lose_sum: 517.9368771016598
time_elpased: 1.368
batch start
#iterations: 325
currently lose_sum: 516.1578584313393
time_elpased: 1.38
batch start
#iterations: 326
currently lose_sum: 516.8817727565765
time_elpased: 1.389
batch start
#iterations: 327
currently lose_sum: 516.7019430696964
time_elpased: 1.393
batch start
#iterations: 328
currently lose_sum: 516.9467387795448
time_elpased: 1.376
batch start
#iterations: 329
currently lose_sum: 515.2494056224823
time_elpased: 1.389
batch start
#iterations: 330
currently lose_sum: 517.8088431954384
time_elpased: 1.404
batch start
#iterations: 331
currently lose_sum: 517.9610112607479
time_elpased: 1.372
batch start
#iterations: 332
currently lose_sum: 517.4350025951862
time_elpased: 1.384
batch start
#iterations: 333
currently lose_sum: 516.5261004567146
time_elpased: 1.377
batch start
#iterations: 334
currently lose_sum: 515.276961773634
time_elpased: 1.392
batch start
#iterations: 335
currently lose_sum: 515.7874193787575
time_elpased: 1.385
batch start
#iterations: 336
currently lose_sum: 517.1780857145786
time_elpased: 1.387
batch start
#iterations: 337
currently lose_sum: 516.2430309653282
time_elpased: 1.392
batch start
#iterations: 338
currently lose_sum: 514.2824719548225
time_elpased: 1.388
batch start
#iterations: 339
currently lose_sum: 517.5385587513447
time_elpased: 1.382
start validation test
0.191134020619
1.0
0.191134020619
0.320927817206
0
validation finish
batch start
#iterations: 340
currently lose_sum: 514.7265344560146
time_elpased: 1.386
batch start
#iterations: 341
currently lose_sum: 516.2569388747215
time_elpased: 1.4
batch start
#iterations: 342
currently lose_sum: 518.023411989212
time_elpased: 1.375
batch start
#iterations: 343
currently lose_sum: 517.5167291164398
time_elpased: 1.388
batch start
#iterations: 344
currently lose_sum: 518.278030693531
time_elpased: 1.387
batch start
#iterations: 345
currently lose_sum: 517.6095775961876
time_elpased: 1.393
batch start
#iterations: 346
currently lose_sum: 516.4630148410797
time_elpased: 1.383
batch start
#iterations: 347
currently lose_sum: 516.7156375348568
time_elpased: 1.386
batch start
#iterations: 348
currently lose_sum: 517.1750083565712
time_elpased: 1.385
batch start
#iterations: 349
currently lose_sum: 518.2189431488514
time_elpased: 1.389
batch start
#iterations: 350
currently lose_sum: 517.2533008158207
time_elpased: 1.399
batch start
#iterations: 351
currently lose_sum: 516.8205731809139
time_elpased: 1.387
batch start
#iterations: 352
currently lose_sum: 516.3376224637032
time_elpased: 1.382
batch start
#iterations: 353
currently lose_sum: 517.7153218388557
time_elpased: 1.393
batch start
#iterations: 354
currently lose_sum: 516.6079315841198
time_elpased: 1.386
batch start
#iterations: 355
currently lose_sum: 516.4792377948761
time_elpased: 1.388
batch start
#iterations: 356
currently lose_sum: 515.1744322180748
time_elpased: 1.376
batch start
#iterations: 357
currently lose_sum: 515.1031523644924
time_elpased: 1.393
batch start
#iterations: 358
currently lose_sum: 517.4260252416134
time_elpased: 1.389
batch start
#iterations: 359
currently lose_sum: 515.3492306470871
time_elpased: 1.399
start validation test
0.178969072165
1.0
0.178969072165
0.303602658272
0
validation finish
batch start
#iterations: 360
currently lose_sum: 516.2266917228699
time_elpased: 1.384
batch start
#iterations: 361
currently lose_sum: 514.99896723032
time_elpased: 1.399
batch start
#iterations: 362
currently lose_sum: 516.2692009210587
time_elpased: 1.38
batch start
#iterations: 363
currently lose_sum: 516.4464862942696
time_elpased: 1.395
batch start
#iterations: 364
currently lose_sum: 516.6720935702324
time_elpased: 1.391
batch start
#iterations: 365
currently lose_sum: 516.3377459049225
time_elpased: 1.413
batch start
#iterations: 366
currently lose_sum: 516.7469109892845
time_elpased: 1.395
batch start
#iterations: 367
currently lose_sum: 516.518426656723
time_elpased: 1.398
batch start
#iterations: 368
currently lose_sum: 515.1212478280067
time_elpased: 1.405
batch start
#iterations: 369
currently lose_sum: 514.7438373565674
time_elpased: 1.403
batch start
#iterations: 370
currently lose_sum: 514.7390170693398
time_elpased: 1.4
batch start
#iterations: 371
currently lose_sum: 516.522328287363
time_elpased: 1.391
batch start
#iterations: 372
currently lose_sum: 515.139770835638
time_elpased: 1.396
batch start
#iterations: 373
currently lose_sum: 516.1148963570595
time_elpased: 1.4
batch start
#iterations: 374
currently lose_sum: 517.2669820189476
time_elpased: 1.373
batch start
#iterations: 375
currently lose_sum: 517.5198368132114
time_elpased: 1.383
batch start
#iterations: 376
currently lose_sum: 516.3177789747715
time_elpased: 1.4
batch start
#iterations: 377
currently lose_sum: 515.0817513167858
time_elpased: 1.387
batch start
#iterations: 378
currently lose_sum: 517.8660852611065
time_elpased: 1.397
batch start
#iterations: 379
currently lose_sum: 516.3795764148235
time_elpased: 1.395
start validation test
0.181958762887
1.0
0.181958762887
0.307893589184
0
validation finish
batch start
#iterations: 380
currently lose_sum: 516.3516635894775
time_elpased: 1.397
batch start
#iterations: 381
currently lose_sum: 515.1506671905518
time_elpased: 1.4
batch start
#iterations: 382
currently lose_sum: 516.2633217871189
time_elpased: 1.374
batch start
#iterations: 383
currently lose_sum: 514.0191312730312
time_elpased: 1.381
batch start
#iterations: 384
currently lose_sum: 514.2423569262028
time_elpased: 1.386
batch start
#iterations: 385
currently lose_sum: 515.3708571195602
time_elpased: 1.393
batch start
#iterations: 386
currently lose_sum: 516.3866322040558
time_elpased: 1.398
batch start
#iterations: 387
currently lose_sum: 515.2813309729099
time_elpased: 1.388
batch start
#iterations: 388
currently lose_sum: 515.747663885355
time_elpased: 1.386
batch start
#iterations: 389
currently lose_sum: 517.4954843819141
time_elpased: 1.416
batch start
#iterations: 390
currently lose_sum: 515.7748965322971
time_elpased: 1.391
batch start
#iterations: 391
currently lose_sum: 516.9256068766117
time_elpased: 1.388
batch start
#iterations: 392
currently lose_sum: 515.8437457978725
time_elpased: 1.385
batch start
#iterations: 393
currently lose_sum: 515.1353961527348
time_elpased: 1.393
batch start
#iterations: 394
currently lose_sum: 515.0856615304947
time_elpased: 1.385
batch start
#iterations: 395
currently lose_sum: 515.7043477594852
time_elpased: 1.389
batch start
#iterations: 396
currently lose_sum: 515.6996820271015
time_elpased: 1.402
batch start
#iterations: 397
currently lose_sum: 517.4452155530453
time_elpased: 1.399
batch start
#iterations: 398
currently lose_sum: 515.3988728821278
time_elpased: 1.402
batch start
#iterations: 399
currently lose_sum: 515.1877425312996
time_elpased: 1.404
start validation test
0.17618556701
1.0
0.17618556701
0.299588044526
0
validation finish
acc: 0.218
pre: 1.000
rec: 0.218
F1: 0.357
auc: 0.000
