start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 548.2459935545921
time_elpased: 1.408
batch start
#iterations: 1
currently lose_sum: 536.835627257824
time_elpased: 1.383
batch start
#iterations: 2
currently lose_sum: 533.1156340241432
time_elpased: 1.366
batch start
#iterations: 3
currently lose_sum: 530.069776147604
time_elpased: 1.38
batch start
#iterations: 4
currently lose_sum: 528.6573236882687
time_elpased: 1.379
batch start
#iterations: 5
currently lose_sum: 527.91341817379
time_elpased: 1.362
batch start
#iterations: 6
currently lose_sum: 524.3693622946739
time_elpased: 1.357
batch start
#iterations: 7
currently lose_sum: 525.45509532094
time_elpased: 1.357
batch start
#iterations: 8
currently lose_sum: 523.4469316005707
time_elpased: 1.377
batch start
#iterations: 9
currently lose_sum: 523.4834048748016
time_elpased: 1.36
batch start
#iterations: 10
currently lose_sum: 522.4025265276432
time_elpased: 1.367
batch start
#iterations: 11
currently lose_sum: 523.3405646085739
time_elpased: 1.362
batch start
#iterations: 12
currently lose_sum: 520.8184904456139
time_elpased: 1.364
batch start
#iterations: 13
currently lose_sum: 519.9026204943657
time_elpased: 1.372
batch start
#iterations: 14
currently lose_sum: 520.7448422908783
time_elpased: 1.37
batch start
#iterations: 15
currently lose_sum: 519.9761784076691
time_elpased: 1.379
batch start
#iterations: 16
currently lose_sum: 520.7471065223217
time_elpased: 1.36
batch start
#iterations: 17
currently lose_sum: 519.9252089262009
time_elpased: 1.368
batch start
#iterations: 18
currently lose_sum: 519.8383019268513
time_elpased: 1.36
batch start
#iterations: 19
currently lose_sum: 521.0202179849148
time_elpased: 1.358
start validation test
0.158350515464
1.0
0.158350515464
0.273406906372
0
validation finish
batch start
#iterations: 20
currently lose_sum: 519.4554923772812
time_elpased: 1.371
batch start
#iterations: 21
currently lose_sum: 521.6508201658726
time_elpased: 1.393
batch start
#iterations: 22
currently lose_sum: 519.1483907699585
time_elpased: 1.361
batch start
#iterations: 23
currently lose_sum: 519.2809314727783
time_elpased: 1.36
batch start
#iterations: 24
currently lose_sum: 517.3398548364639
time_elpased: 1.377
batch start
#iterations: 25
currently lose_sum: 519.8975913822651
time_elpased: 1.37
batch start
#iterations: 26
currently lose_sum: 519.2609937489033
time_elpased: 1.358
batch start
#iterations: 27
currently lose_sum: 518.5546589791775
time_elpased: 1.369
batch start
#iterations: 28
currently lose_sum: 517.4978242218494
time_elpased: 1.361
batch start
#iterations: 29
currently lose_sum: 517.5500313639641
time_elpased: 1.363
batch start
#iterations: 30
currently lose_sum: 517.3574657440186
time_elpased: 1.354
batch start
#iterations: 31
currently lose_sum: 516.0588067173958
time_elpased: 1.363
batch start
#iterations: 32
currently lose_sum: 518.9612451195717
time_elpased: 1.36
batch start
#iterations: 33
currently lose_sum: 516.6803876757622
time_elpased: 1.367
batch start
#iterations: 34
currently lose_sum: 518.6377013325691
time_elpased: 1.364
batch start
#iterations: 35
currently lose_sum: 515.368793785572
time_elpased: 1.368
batch start
#iterations: 36
currently lose_sum: 518.306464523077
time_elpased: 1.365
batch start
#iterations: 37
currently lose_sum: 517.0610825121403
time_elpased: 1.37
batch start
#iterations: 38
currently lose_sum: 517.9001861512661
time_elpased: 1.373
batch start
#iterations: 39
currently lose_sum: 519.1697225570679
time_elpased: 1.348
start validation test
0.151649484536
1.0
0.151649484536
0.263360486975
0
validation finish
batch start
#iterations: 40
currently lose_sum: 519.2148160636425
time_elpased: 1.367
batch start
#iterations: 41
currently lose_sum: 519.5014545619488
time_elpased: 1.362
batch start
#iterations: 42
currently lose_sum: 517.726174980402
time_elpased: 1.373
batch start
#iterations: 43
currently lose_sum: 517.3873706758022
time_elpased: 1.369
batch start
#iterations: 44
currently lose_sum: 517.0131764411926
time_elpased: 1.369
batch start
#iterations: 45
currently lose_sum: 516.9766059219837
time_elpased: 1.364
batch start
#iterations: 46
currently lose_sum: 516.4585120379925
time_elpased: 1.371
batch start
#iterations: 47
currently lose_sum: 517.3963260948658
time_elpased: 1.379
batch start
#iterations: 48
currently lose_sum: 516.1557220816612
time_elpased: 1.372
batch start
#iterations: 49
currently lose_sum: 516.5074778497219
time_elpased: 1.359
batch start
#iterations: 50
currently lose_sum: 518.4384328722954
time_elpased: 1.38
batch start
#iterations: 51
currently lose_sum: 516.946108520031
time_elpased: 1.371
batch start
#iterations: 52
currently lose_sum: 516.3521107435226
time_elpased: 1.368
batch start
#iterations: 53
currently lose_sum: 518.4821572303772
time_elpased: 1.36
batch start
#iterations: 54
currently lose_sum: 516.2834866344929
time_elpased: 1.378
batch start
#iterations: 55
currently lose_sum: 517.8555578589439
time_elpased: 1.358
batch start
#iterations: 56
currently lose_sum: 517.2814616262913
time_elpased: 1.369
batch start
#iterations: 57
currently lose_sum: 516.0495566725731
time_elpased: 1.368
batch start
#iterations: 58
currently lose_sum: 518.1465282440186
time_elpased: 1.371
batch start
#iterations: 59
currently lose_sum: 515.6679474711418
time_elpased: 1.38
start validation test
0.184020618557
1.0
0.184020618557
0.310840226382
0
validation finish
batch start
#iterations: 60
currently lose_sum: 517.7753553986549
time_elpased: 1.373
batch start
#iterations: 61
currently lose_sum: 517.9101753234863
time_elpased: 1.37
batch start
#iterations: 62
currently lose_sum: 519.344647616148
time_elpased: 1.362
batch start
#iterations: 63
currently lose_sum: 518.1691387891769
time_elpased: 1.369
batch start
#iterations: 64
currently lose_sum: 518.3417797088623
time_elpased: 1.383
batch start
#iterations: 65
currently lose_sum: 518.470088660717
time_elpased: 1.372
batch start
#iterations: 66
currently lose_sum: 516.9811822772026
time_elpased: 1.368
batch start
#iterations: 67
currently lose_sum: 514.9013616144657
time_elpased: 1.375
batch start
#iterations: 68
currently lose_sum: 519.2936964929104
time_elpased: 1.378
batch start
#iterations: 69
currently lose_sum: 517.5305260121822
time_elpased: 1.362
batch start
#iterations: 70
currently lose_sum: 517.6827548742294
time_elpased: 1.364
batch start
#iterations: 71
currently lose_sum: 517.1426223218441
time_elpased: 1.372
batch start
#iterations: 72
currently lose_sum: 517.0090876221657
time_elpased: 1.373
batch start
#iterations: 73
currently lose_sum: 515.4206757843494
time_elpased: 1.358
batch start
#iterations: 74
currently lose_sum: 517.1350072920322
time_elpased: 1.368
batch start
#iterations: 75
currently lose_sum: 516.9018410146236
time_elpased: 1.356
batch start
#iterations: 76
currently lose_sum: 518.0874001085758
time_elpased: 1.374
batch start
#iterations: 77
currently lose_sum: 517.1023170053959
time_elpased: 1.38
batch start
#iterations: 78
currently lose_sum: 517.6373095214367
time_elpased: 1.368
batch start
#iterations: 79
currently lose_sum: 518.1774754822254
time_elpased: 1.364
start validation test
0.232577319588
1.0
0.232577319588
0.377383740381
0
validation finish
batch start
#iterations: 80
currently lose_sum: 515.9845632016659
time_elpased: 1.372
batch start
#iterations: 81
currently lose_sum: 518.0691083967686
time_elpased: 1.37
batch start
#iterations: 82
currently lose_sum: 516.6582392156124
time_elpased: 1.365
batch start
#iterations: 83
currently lose_sum: 517.4540036320686
time_elpased: 1.372
batch start
#iterations: 84
currently lose_sum: 518.3797113895416
time_elpased: 1.375
batch start
#iterations: 85
currently lose_sum: 515.4448934197426
time_elpased: 1.37
batch start
#iterations: 86
currently lose_sum: 516.8004117012024
time_elpased: 1.378
batch start
#iterations: 87
currently lose_sum: 515.9344008266926
time_elpased: 1.372
batch start
#iterations: 88
currently lose_sum: 517.5560011863708
time_elpased: 1.37
batch start
#iterations: 89
currently lose_sum: 516.0288680791855
time_elpased: 1.381
batch start
#iterations: 90
currently lose_sum: 517.9986478686333
time_elpased: 1.362
batch start
#iterations: 91
currently lose_sum: 515.664142370224
time_elpased: 1.373
batch start
#iterations: 92
currently lose_sum: 518.0063606798649
time_elpased: 1.359
batch start
#iterations: 93
currently lose_sum: 517.2548636198044
time_elpased: 1.364
batch start
#iterations: 94
currently lose_sum: 516.6710430085659
time_elpased: 1.363
batch start
#iterations: 95
currently lose_sum: 515.4646622538567
time_elpased: 1.362
batch start
#iterations: 96
currently lose_sum: 518.3826901614666
time_elpased: 1.373
batch start
#iterations: 97
currently lose_sum: 515.7382197976112
time_elpased: 1.382
batch start
#iterations: 98
currently lose_sum: 517.4906197488308
time_elpased: 1.371
batch start
#iterations: 99
currently lose_sum: 517.097657084465
time_elpased: 1.365
start validation test
0.172371134021
1.0
0.172371134021
0.294055575097
0
validation finish
batch start
#iterations: 100
currently lose_sum: 517.5347139239311
time_elpased: 1.359
batch start
#iterations: 101
currently lose_sum: 516.3124783933163
time_elpased: 1.358
batch start
#iterations: 102
currently lose_sum: 515.2864230275154
time_elpased: 1.358
batch start
#iterations: 103
currently lose_sum: 517.0035914182663
time_elpased: 1.366
batch start
#iterations: 104
currently lose_sum: 516.495499432087
time_elpased: 1.373
batch start
#iterations: 105
currently lose_sum: 516.598504960537
time_elpased: 1.373
batch start
#iterations: 106
currently lose_sum: 517.676528275013
time_elpased: 1.366
batch start
#iterations: 107
currently lose_sum: 516.8762914538383
time_elpased: 1.372
batch start
#iterations: 108
currently lose_sum: 514.9094998836517
time_elpased: 1.364
batch start
#iterations: 109
currently lose_sum: 516.9720778465271
time_elpased: 1.379
batch start
#iterations: 110
currently lose_sum: 516.745439261198
time_elpased: 1.37
batch start
#iterations: 111
currently lose_sum: 519.2244043052197
time_elpased: 1.374
batch start
#iterations: 112
currently lose_sum: 515.0737536847591
time_elpased: 1.372
batch start
#iterations: 113
currently lose_sum: 517.1421068012714
time_elpased: 1.361
batch start
#iterations: 114
currently lose_sum: 515.7900066971779
time_elpased: 1.376
batch start
#iterations: 115
currently lose_sum: 516.9468196630478
time_elpased: 1.367
batch start
#iterations: 116
currently lose_sum: 514.926597058773
time_elpased: 1.363
batch start
#iterations: 117
currently lose_sum: 517.1878591775894
time_elpased: 1.375
batch start
#iterations: 118
currently lose_sum: 516.17858299613
time_elpased: 1.374
batch start
#iterations: 119
currently lose_sum: 516.0229913890362
time_elpased: 1.358
start validation test
0.195773195876
1.0
0.195773195876
0.327442020864
0
validation finish
batch start
#iterations: 120
currently lose_sum: 516.7995006740093
time_elpased: 1.363
batch start
#iterations: 121
currently lose_sum: 515.552188128233
time_elpased: 1.373
batch start
#iterations: 122
currently lose_sum: 514.6122291684151
time_elpased: 1.365
batch start
#iterations: 123
currently lose_sum: 515.1928651928902
time_elpased: 1.379
batch start
#iterations: 124
currently lose_sum: 515.6250884532928
time_elpased: 1.376
batch start
#iterations: 125
currently lose_sum: 516.3870361447334
time_elpased: 1.362
batch start
#iterations: 126
currently lose_sum: 515.7177037596703
time_elpased: 1.373
batch start
#iterations: 127
currently lose_sum: 517.333987891674
time_elpased: 1.363
batch start
#iterations: 128
currently lose_sum: 515.0158347785473
time_elpased: 1.369
batch start
#iterations: 129
currently lose_sum: 516.3180465102196
time_elpased: 1.366
batch start
#iterations: 130
currently lose_sum: 516.5274146199226
time_elpased: 1.368
batch start
#iterations: 131
currently lose_sum: 516.4022448956966
time_elpased: 1.365
batch start
#iterations: 132
currently lose_sum: 518.1035465598106
time_elpased: 1.357
batch start
#iterations: 133
currently lose_sum: 516.696209192276
time_elpased: 1.36
batch start
#iterations: 134
currently lose_sum: 516.6779490411282
time_elpased: 1.368
batch start
#iterations: 135
currently lose_sum: 514.1270073652267
time_elpased: 1.368
batch start
#iterations: 136
currently lose_sum: 515.7786032259464
time_elpased: 1.367
batch start
#iterations: 137
currently lose_sum: 515.1619160473347
time_elpased: 1.37
batch start
#iterations: 138
currently lose_sum: 516.6852825880051
time_elpased: 1.367
batch start
#iterations: 139
currently lose_sum: 517.3534315228462
time_elpased: 1.365
start validation test
0.163608247423
1.0
0.163608247423
0.281208469921
0
validation finish
batch start
#iterations: 140
currently lose_sum: 513.3686548173428
time_elpased: 1.376
batch start
#iterations: 141
currently lose_sum: 517.7427517771721
time_elpased: 1.372
batch start
#iterations: 142
currently lose_sum: 516.0107502937317
time_elpased: 1.358
batch start
#iterations: 143
currently lose_sum: 516.6898329257965
time_elpased: 1.373
batch start
#iterations: 144
currently lose_sum: 517.4774451255798
time_elpased: 1.367
batch start
#iterations: 145
currently lose_sum: 517.4325325191021
time_elpased: 1.367
batch start
#iterations: 146
currently lose_sum: 518.6618928313255
time_elpased: 1.38
batch start
#iterations: 147
currently lose_sum: 516.2663716971874
time_elpased: 1.368
batch start
#iterations: 148
currently lose_sum: 514.7247115969658
time_elpased: 1.367
batch start
#iterations: 149
currently lose_sum: 517.7089640498161
time_elpased: 1.385
batch start
#iterations: 150
currently lose_sum: 515.5474209785461
time_elpased: 1.37
batch start
#iterations: 151
currently lose_sum: 515.9021889567375
time_elpased: 1.385
batch start
#iterations: 152
currently lose_sum: 516.5569975078106
time_elpased: 1.375
batch start
#iterations: 153
currently lose_sum: 513.4704985022545
time_elpased: 1.371
batch start
#iterations: 154
currently lose_sum: 518.8974314332008
time_elpased: 1.369
batch start
#iterations: 155
currently lose_sum: 515.0850818455219
time_elpased: 1.391
batch start
#iterations: 156
currently lose_sum: 516.9342007637024
time_elpased: 1.368
batch start
#iterations: 157
currently lose_sum: 515.895802885294
time_elpased: 1.366
batch start
#iterations: 158
currently lose_sum: 514.5976214706898
time_elpased: 1.377
batch start
#iterations: 159
currently lose_sum: 516.5184111595154
time_elpased: 1.375
start validation test
0.169278350515
1.0
0.169278350515
0.289543290425
0
validation finish
batch start
#iterations: 160
currently lose_sum: 516.7283562421799
time_elpased: 1.368
batch start
#iterations: 161
currently lose_sum: 516.712852627039
time_elpased: 1.363
batch start
#iterations: 162
currently lose_sum: 515.6439523994923
time_elpased: 1.375
batch start
#iterations: 163
currently lose_sum: 513.5904531180859
time_elpased: 1.364
batch start
#iterations: 164
currently lose_sum: 518.3662557601929
time_elpased: 1.372
batch start
#iterations: 165
currently lose_sum: 515.3078562915325
time_elpased: 1.365
batch start
#iterations: 166
currently lose_sum: 515.5480802357197
time_elpased: 1.367
batch start
#iterations: 167
currently lose_sum: 517.2155603766441
time_elpased: 1.397
batch start
#iterations: 168
currently lose_sum: 516.4465925097466
time_elpased: 1.371
batch start
#iterations: 169
currently lose_sum: 518.1963573098183
time_elpased: 1.376
batch start
#iterations: 170
currently lose_sum: 515.5820693969727
time_elpased: 1.374
batch start
#iterations: 171
currently lose_sum: 515.3389804661274
time_elpased: 1.386
batch start
#iterations: 172
currently lose_sum: 518.3617843687534
time_elpased: 1.375
batch start
#iterations: 173
currently lose_sum: 516.3835360705853
time_elpased: 1.371
batch start
#iterations: 174
currently lose_sum: 517.2182257175446
time_elpased: 1.379
batch start
#iterations: 175
currently lose_sum: 516.8747887313366
time_elpased: 1.366
batch start
#iterations: 176
currently lose_sum: 516.0897776186466
time_elpased: 1.36
batch start
#iterations: 177
currently lose_sum: 516.6861264109612
time_elpased: 1.363
batch start
#iterations: 178
currently lose_sum: 515.1532733738422
time_elpased: 1.366
batch start
#iterations: 179
currently lose_sum: 516.3519226908684
time_elpased: 1.382
start validation test
0.200103092784
1.0
0.200103092784
0.333476505455
0
validation finish
batch start
#iterations: 180
currently lose_sum: 516.1397173404694
time_elpased: 1.361
batch start
#iterations: 181
currently lose_sum: 517.836214184761
time_elpased: 1.37
batch start
#iterations: 182
currently lose_sum: 514.7156090140343
time_elpased: 1.379
batch start
#iterations: 183
currently lose_sum: 516.890439838171
time_elpased: 1.372
batch start
#iterations: 184
currently lose_sum: 516.1527642607689
time_elpased: 1.378
batch start
#iterations: 185
currently lose_sum: 514.5166065394878
time_elpased: 1.385
batch start
#iterations: 186
currently lose_sum: 517.9396135210991
time_elpased: 1.389
batch start
#iterations: 187
currently lose_sum: 515.2326581776142
time_elpased: 1.366
batch start
#iterations: 188
currently lose_sum: 514.9607737660408
time_elpased: 1.366
batch start
#iterations: 189
currently lose_sum: 515.4949400424957
time_elpased: 1.379
batch start
#iterations: 190
currently lose_sum: 513.9309022724628
time_elpased: 1.377
batch start
#iterations: 191
currently lose_sum: 517.3533456027508
time_elpased: 1.372
batch start
#iterations: 192
currently lose_sum: 516.3268091082573
time_elpased: 1.362
batch start
#iterations: 193
currently lose_sum: 516.2820638120174
time_elpased: 1.366
batch start
#iterations: 194
currently lose_sum: 516.1242218613625
time_elpased: 1.371
batch start
#iterations: 195
currently lose_sum: 516.6438641250134
time_elpased: 1.372
batch start
#iterations: 196
currently lose_sum: 514.9431545436382
time_elpased: 1.382
batch start
#iterations: 197
currently lose_sum: 517.359432965517
time_elpased: 1.375
batch start
#iterations: 198
currently lose_sum: 516.374983638525
time_elpased: 1.365
batch start
#iterations: 199
currently lose_sum: 515.7837397754192
time_elpased: 1.366
start validation test
0.190515463918
1.0
0.190515463918
0.320055420852
0
validation finish
batch start
#iterations: 200
currently lose_sum: 518.2384003996849
time_elpased: 1.375
batch start
#iterations: 201
currently lose_sum: 515.9191017448902
time_elpased: 1.386
batch start
#iterations: 202
currently lose_sum: 516.3900558054447
time_elpased: 1.365
batch start
#iterations: 203
currently lose_sum: 515.0978419184685
time_elpased: 1.383
batch start
#iterations: 204
currently lose_sum: 516.4766133129597
time_elpased: 1.388
batch start
#iterations: 205
currently lose_sum: 517.523535490036
time_elpased: 1.381
batch start
#iterations: 206
currently lose_sum: 516.7414594292641
time_elpased: 1.37
batch start
#iterations: 207
currently lose_sum: 514.8358288407326
time_elpased: 1.385
batch start
#iterations: 208
currently lose_sum: 515.8805657625198
time_elpased: 1.379
batch start
#iterations: 209
currently lose_sum: 515.4208659827709
time_elpased: 1.366
batch start
#iterations: 210
currently lose_sum: 517.439673691988
time_elpased: 1.374
batch start
#iterations: 211
currently lose_sum: 517.0815529227257
time_elpased: 1.368
batch start
#iterations: 212
currently lose_sum: 516.1944230794907
time_elpased: 1.379
batch start
#iterations: 213
currently lose_sum: 515.0288471281528
time_elpased: 1.367
batch start
#iterations: 214
currently lose_sum: 517.3315550386906
time_elpased: 1.378
batch start
#iterations: 215
currently lose_sum: 516.694115281105
time_elpased: 1.369
batch start
#iterations: 216
currently lose_sum: 516.3484416007996
time_elpased: 1.374
batch start
#iterations: 217
currently lose_sum: 515.4121632874012
time_elpased: 1.371
batch start
#iterations: 218
currently lose_sum: 517.6541225612164
time_elpased: 1.379
batch start
#iterations: 219
currently lose_sum: 516.9276179075241
time_elpased: 1.386
start validation test
0.187319587629
1.0
0.187319587629
0.315533559087
0
validation finish
batch start
#iterations: 220
currently lose_sum: 514.717238932848
time_elpased: 1.375
batch start
#iterations: 221
currently lose_sum: 517.3579170405865
time_elpased: 1.376
batch start
#iterations: 222
currently lose_sum: 515.330249518156
time_elpased: 1.376
batch start
#iterations: 223
currently lose_sum: 516.9639581143856
time_elpased: 1.37
batch start
#iterations: 224
currently lose_sum: 515.2168967425823
time_elpased: 1.357
batch start
#iterations: 225
currently lose_sum: 518.1600942313671
time_elpased: 1.394
batch start
#iterations: 226
currently lose_sum: 517.0881624221802
time_elpased: 1.387
batch start
#iterations: 227
currently lose_sum: 514.3735401928425
time_elpased: 1.374
batch start
#iterations: 228
currently lose_sum: 514.0828110277653
time_elpased: 1.374
batch start
#iterations: 229
currently lose_sum: 515.7508605718613
time_elpased: 1.371
batch start
#iterations: 230
currently lose_sum: 517.7042119204998
time_elpased: 1.372
batch start
#iterations: 231
currently lose_sum: 518.6113999783993
time_elpased: 1.365
batch start
#iterations: 232
currently lose_sum: 515.4065300524235
time_elpased: 1.39
batch start
#iterations: 233
currently lose_sum: 514.2583871781826
time_elpased: 1.368
batch start
#iterations: 234
currently lose_sum: 514.5501836240292
time_elpased: 1.37
batch start
#iterations: 235
currently lose_sum: 515.6300989687443
time_elpased: 1.372
batch start
#iterations: 236
currently lose_sum: 514.7215493619442
time_elpased: 1.376
batch start
#iterations: 237
currently lose_sum: 515.9225931763649
time_elpased: 1.37
batch start
#iterations: 238
currently lose_sum: 514.4237815737724
time_elpased: 1.368
batch start
#iterations: 239
currently lose_sum: 514.8957509696484
time_elpased: 1.366
start validation test
0.161340206186
1.0
0.161340206186
0.277851753218
0
validation finish
batch start
#iterations: 240
currently lose_sum: 514.4367114305496
time_elpased: 1.373
batch start
#iterations: 241
currently lose_sum: 517.4520482420921
time_elpased: 1.387
batch start
#iterations: 242
currently lose_sum: 514.6578770279884
time_elpased: 1.38
batch start
#iterations: 243
currently lose_sum: 514.2040098011494
time_elpased: 1.37
batch start
#iterations: 244
currently lose_sum: 515.5148446559906
time_elpased: 1.373
batch start
#iterations: 245
currently lose_sum: 516.1369306147099
time_elpased: 1.376
batch start
#iterations: 246
currently lose_sum: 515.5385874211788
time_elpased: 1.393
batch start
#iterations: 247
currently lose_sum: 516.2818799316883
time_elpased: 1.379
batch start
#iterations: 248
currently lose_sum: 515.3849977254868
time_elpased: 1.377
batch start
#iterations: 249
currently lose_sum: 516.1711631417274
time_elpased: 1.373
batch start
#iterations: 250
currently lose_sum: 515.5028127729893
time_elpased: 1.368
batch start
#iterations: 251
currently lose_sum: 517.2013894617558
time_elpased: 1.382
batch start
#iterations: 252
currently lose_sum: 516.3122950196266
time_elpased: 1.38
batch start
#iterations: 253
currently lose_sum: 515.3455549180508
time_elpased: 1.363
batch start
#iterations: 254
currently lose_sum: 516.8971698284149
time_elpased: 1.364
batch start
#iterations: 255
currently lose_sum: 514.837164491415
time_elpased: 1.377
batch start
#iterations: 256
currently lose_sum: 515.002634614706
time_elpased: 1.367
batch start
#iterations: 257
currently lose_sum: 513.8526612818241
time_elpased: 1.381
batch start
#iterations: 258
currently lose_sum: 516.852795958519
time_elpased: 1.357
batch start
#iterations: 259
currently lose_sum: 516.7624817490578
time_elpased: 1.378
start validation test
0.197525773196
1.0
0.197525773196
0.329889807163
0
validation finish
batch start
#iterations: 260
currently lose_sum: 518.513790756464
time_elpased: 1.365
batch start
#iterations: 261
currently lose_sum: 517.4546199440956
time_elpased: 1.364
batch start
#iterations: 262
currently lose_sum: 515.8119204342365
time_elpased: 1.375
batch start
#iterations: 263
currently lose_sum: 515.7230632007122
time_elpased: 1.39
batch start
#iterations: 264
currently lose_sum: 516.3609182834625
time_elpased: 1.38
batch start
#iterations: 265
currently lose_sum: 516.6995382905006
time_elpased: 1.389
batch start
#iterations: 266
currently lose_sum: 514.9053225517273
time_elpased: 1.38
batch start
#iterations: 267
currently lose_sum: 514.1861666440964
time_elpased: 1.381
batch start
#iterations: 268
currently lose_sum: 515.5385993123055
time_elpased: 1.372
batch start
#iterations: 269
currently lose_sum: 516.0251331329346
time_elpased: 1.376
batch start
#iterations: 270
currently lose_sum: 514.7438857257366
time_elpased: 1.383
batch start
#iterations: 271
currently lose_sum: 513.9985627830029
time_elpased: 1.392
batch start
#iterations: 272
currently lose_sum: 516.3677158951759
time_elpased: 1.381
batch start
#iterations: 273
currently lose_sum: 513.9160858094692
time_elpased: 1.377
batch start
#iterations: 274
currently lose_sum: 515.4597772061825
time_elpased: 1.373
batch start
#iterations: 275
currently lose_sum: 515.1036375463009
time_elpased: 1.371
batch start
#iterations: 276
currently lose_sum: 515.8009156286716
time_elpased: 1.362
batch start
#iterations: 277
currently lose_sum: 515.8023330271244
time_elpased: 1.379
batch start
#iterations: 278
currently lose_sum: 515.4748638868332
time_elpased: 1.38
batch start
#iterations: 279
currently lose_sum: 517.7664182186127
time_elpased: 1.395
start validation test
0.178144329897
1.0
0.178144329897
0.302415120756
0
validation finish
batch start
#iterations: 280
currently lose_sum: 513.9597688317299
time_elpased: 1.377
batch start
#iterations: 281
currently lose_sum: 514.219884634018
time_elpased: 1.377
batch start
#iterations: 282
currently lose_sum: 516.8071049153805
time_elpased: 1.364
batch start
#iterations: 283
currently lose_sum: 515.338770031929
time_elpased: 1.378
batch start
#iterations: 284
currently lose_sum: 516.6868839561939
time_elpased: 1.375
batch start
#iterations: 285
currently lose_sum: 515.6255347132683
time_elpased: 1.391
batch start
#iterations: 286
currently lose_sum: 517.3790696561337
time_elpased: 1.372
batch start
#iterations: 287
currently lose_sum: 516.2911253273487
time_elpased: 1.381
batch start
#iterations: 288
currently lose_sum: 516.3804901242256
time_elpased: 1.382
batch start
#iterations: 289
currently lose_sum: 516.3005338013172
time_elpased: 1.373
batch start
#iterations: 290
currently lose_sum: 514.7141598463058
time_elpased: 1.376
batch start
#iterations: 291
currently lose_sum: 516.1106334328651
time_elpased: 1.382
batch start
#iterations: 292
currently lose_sum: 516.4281999468803
time_elpased: 1.383
batch start
#iterations: 293
currently lose_sum: 516.3475315868855
time_elpased: 1.367
batch start
#iterations: 294
currently lose_sum: 517.4250558912754
time_elpased: 1.386
batch start
#iterations: 295
currently lose_sum: 515.7189252078533
time_elpased: 1.37
batch start
#iterations: 296
currently lose_sum: 514.1508320569992
time_elpased: 1.365
batch start
#iterations: 297
currently lose_sum: 516.8797321617603
time_elpased: 1.364
batch start
#iterations: 298
currently lose_sum: 515.2509523034096
time_elpased: 1.37
batch start
#iterations: 299
currently lose_sum: 516.1583644449711
time_elpased: 1.392
start validation test
0.203195876289
1.0
0.203195876289
0.337760260475
0
validation finish
batch start
#iterations: 300
currently lose_sum: 515.4002938866615
time_elpased: 1.362
batch start
#iterations: 301
currently lose_sum: 515.4418912231922
time_elpased: 1.374
batch start
#iterations: 302
currently lose_sum: 518.442071467638
time_elpased: 1.37
batch start
#iterations: 303
currently lose_sum: 518.9720632135868
time_elpased: 1.371
batch start
#iterations: 304
currently lose_sum: 512.014163017273
time_elpased: 1.384
batch start
#iterations: 305
currently lose_sum: 516.8967444598675
time_elpased: 1.378
batch start
#iterations: 306
currently lose_sum: 515.3868751525879
time_elpased: 1.378
batch start
#iterations: 307
currently lose_sum: 515.3734384775162
time_elpased: 1.379
batch start
#iterations: 308
currently lose_sum: 515.8290064632893
time_elpased: 1.378
batch start
#iterations: 309
currently lose_sum: 516.5366232693195
time_elpased: 1.378
batch start
#iterations: 310
currently lose_sum: 515.5714418292046
time_elpased: 1.373
batch start
#iterations: 311
currently lose_sum: 513.9135195314884
time_elpased: 1.369
batch start
#iterations: 312
currently lose_sum: 516.0099842846394
time_elpased: 1.392
batch start
#iterations: 313
currently lose_sum: 517.5093615651131
time_elpased: 1.372
batch start
#iterations: 314
currently lose_sum: 514.9051019251347
time_elpased: 1.373
batch start
#iterations: 315
currently lose_sum: 515.2801396548748
time_elpased: 1.377
batch start
#iterations: 316
currently lose_sum: 515.5577774047852
time_elpased: 1.373
batch start
#iterations: 317
currently lose_sum: 515.9043174982071
time_elpased: 1.37
batch start
#iterations: 318
currently lose_sum: 518.1167890429497
time_elpased: 1.372
batch start
#iterations: 319
currently lose_sum: 515.127958804369
time_elpased: 1.379
start validation test
0.190412371134
1.0
0.190412371134
0.319909933316
0
validation finish
batch start
#iterations: 320
currently lose_sum: 516.3095817565918
time_elpased: 1.387
batch start
#iterations: 321
currently lose_sum: 517.9264525473118
time_elpased: 1.377
batch start
#iterations: 322
currently lose_sum: 514.9362306296825
time_elpased: 1.371
batch start
#iterations: 323
currently lose_sum: 514.1584615409374
time_elpased: 1.368
batch start
#iterations: 324
currently lose_sum: 518.7090235948563
time_elpased: 1.408
batch start
#iterations: 325
currently lose_sum: 514.6063759028912
time_elpased: 1.364
batch start
#iterations: 326
currently lose_sum: 514.9652197659016
time_elpased: 1.36
batch start
#iterations: 327
currently lose_sum: 516.0937883853912
time_elpased: 1.372
batch start
#iterations: 328
currently lose_sum: 515.8523934781551
time_elpased: 1.378
batch start
#iterations: 329
currently lose_sum: 515.461095392704
time_elpased: 1.38
batch start
#iterations: 330
currently lose_sum: 518.2431727945805
time_elpased: 1.379
batch start
#iterations: 331
currently lose_sum: 518.3578341305256
time_elpased: 1.374
batch start
#iterations: 332
currently lose_sum: 517.6464452445507
time_elpased: 1.381
batch start
#iterations: 333
currently lose_sum: 515.4705285429955
time_elpased: 1.373
batch start
#iterations: 334
currently lose_sum: 515.3862127065659
time_elpased: 1.38
batch start
#iterations: 335
currently lose_sum: 515.491756349802
time_elpased: 1.376
batch start
#iterations: 336
currently lose_sum: 517.561967253685
time_elpased: 1.37
batch start
#iterations: 337
currently lose_sum: 516.3844077587128
time_elpased: 1.363
batch start
#iterations: 338
currently lose_sum: 515.7407838702202
time_elpased: 1.362
batch start
#iterations: 339
currently lose_sum: 516.0911124050617
time_elpased: 1.382
start validation test
0.197422680412
1.0
0.197422680412
0.32974601808
0
validation finish
batch start
#iterations: 340
currently lose_sum: 516.3266655802727
time_elpased: 1.378
batch start
#iterations: 341
currently lose_sum: 516.8960848450661
time_elpased: 1.377
batch start
#iterations: 342
currently lose_sum: 517.9744898974895
time_elpased: 1.379
batch start
#iterations: 343
currently lose_sum: 517.0692669451237
time_elpased: 1.373
batch start
#iterations: 344
currently lose_sum: 517.1447834670544
time_elpased: 1.376
batch start
#iterations: 345
currently lose_sum: 516.9762602448463
time_elpased: 1.376
batch start
#iterations: 346
currently lose_sum: 513.6694770753384
time_elpased: 1.377
batch start
#iterations: 347
currently lose_sum: 515.799725651741
time_elpased: 1.382
batch start
#iterations: 348
currently lose_sum: 515.0153583884239
time_elpased: 1.387
batch start
#iterations: 349
currently lose_sum: 516.1343861222267
time_elpased: 1.385
batch start
#iterations: 350
currently lose_sum: 516.4904989302158
time_elpased: 1.385
batch start
#iterations: 351
currently lose_sum: 515.0956044793129
time_elpased: 1.37
batch start
#iterations: 352
currently lose_sum: 515.5985670387745
time_elpased: 1.386
batch start
#iterations: 353
currently lose_sum: 516.9006258547306
time_elpased: 1.384
batch start
#iterations: 354
currently lose_sum: 514.8152921795845
time_elpased: 1.366
batch start
#iterations: 355
currently lose_sum: 515.8053601682186
time_elpased: 1.369
batch start
#iterations: 356
currently lose_sum: 516.4928472936153
time_elpased: 1.382
batch start
#iterations: 357
currently lose_sum: 515.7067324519157
time_elpased: 1.378
batch start
#iterations: 358
currently lose_sum: 515.5347546935081
time_elpased: 1.386
batch start
#iterations: 359
currently lose_sum: 515.3097965121269
time_elpased: 1.37
start validation test
0.170927835052
1.0
0.170927835052
0.291952808593
0
validation finish
batch start
#iterations: 360
currently lose_sum: 516.8606807291508
time_elpased: 1.391
batch start
#iterations: 361
currently lose_sum: 514.2145322859287
time_elpased: 1.379
batch start
#iterations: 362
currently lose_sum: 513.7565928697586
time_elpased: 1.367
batch start
#iterations: 363
currently lose_sum: 516.2217092216015
time_elpased: 1.372
batch start
#iterations: 364
currently lose_sum: 516.650504052639
time_elpased: 1.377
batch start
#iterations: 365
currently lose_sum: 515.1318300962448
time_elpased: 1.378
batch start
#iterations: 366
currently lose_sum: 515.0659810304642
time_elpased: 1.396
batch start
#iterations: 367
currently lose_sum: 517.1632903814316
time_elpased: 1.373
batch start
#iterations: 368
currently lose_sum: 513.3821220099926
time_elpased: 1.385
batch start
#iterations: 369
currently lose_sum: 515.2740483283997
time_elpased: 1.374
batch start
#iterations: 370
currently lose_sum: 514.2904288172722
time_elpased: 1.367
batch start
#iterations: 371
currently lose_sum: 517.1527950465679
time_elpased: 1.386
batch start
#iterations: 372
currently lose_sum: 515.7628057599068
time_elpased: 1.383
batch start
#iterations: 373
currently lose_sum: 516.344257324934
time_elpased: 1.391
batch start
#iterations: 374
currently lose_sum: 517.6258414387703
time_elpased: 1.376
batch start
#iterations: 375
currently lose_sum: 517.6872946023941
time_elpased: 1.383
batch start
#iterations: 376
currently lose_sum: 515.9328474402428
time_elpased: 1.387
batch start
#iterations: 377
currently lose_sum: 514.8836536407471
time_elpased: 1.378
batch start
#iterations: 378
currently lose_sum: 515.4249731600285
time_elpased: 1.38
batch start
#iterations: 379
currently lose_sum: 513.6239257156849
time_elpased: 1.378
start validation test
0.185670103093
1.0
0.185670103093
0.313190157378
0
validation finish
batch start
#iterations: 380
currently lose_sum: 516.966755002737
time_elpased: 1.382
batch start
#iterations: 381
currently lose_sum: 515.5760022103786
time_elpased: 1.385
batch start
#iterations: 382
currently lose_sum: 516.7114630639553
time_elpased: 1.374
batch start
#iterations: 383
currently lose_sum: 514.2892811894417
time_elpased: 1.377
batch start
#iterations: 384
currently lose_sum: 514.2196252644062
time_elpased: 1.386
batch start
#iterations: 385
currently lose_sum: 514.4543622732162
time_elpased: 1.38
batch start
#iterations: 386
currently lose_sum: 515.278782248497
time_elpased: 1.368
batch start
#iterations: 387
currently lose_sum: 515.8186466097832
time_elpased: 1.38
batch start
#iterations: 388
currently lose_sum: 515.807484716177
time_elpased: 1.37
batch start
#iterations: 389
currently lose_sum: 515.89996945858
time_elpased: 1.39
batch start
#iterations: 390
currently lose_sum: 516.1432611644268
time_elpased: 1.381
batch start
#iterations: 391
currently lose_sum: 516.1581002771854
time_elpased: 1.381
batch start
#iterations: 392
currently lose_sum: 515.3974330127239
time_elpased: 1.363
batch start
#iterations: 393
currently lose_sum: 515.0846149921417
time_elpased: 1.37
batch start
#iterations: 394
currently lose_sum: 515.9820225834846
time_elpased: 1.37
batch start
#iterations: 395
currently lose_sum: 514.7277511656284
time_elpased: 1.366
batch start
#iterations: 396
currently lose_sum: 516.4421278834343
time_elpased: 1.381
batch start
#iterations: 397
currently lose_sum: 517.130342900753
time_elpased: 1.379
batch start
#iterations: 398
currently lose_sum: 515.5145085453987
time_elpased: 1.362
batch start
#iterations: 399
currently lose_sum: 514.8250794112682
time_elpased: 1.389
start validation test
0.177319587629
1.0
0.177319587629
0.30122591944
0
validation finish
acc: 0.238
pre: 1.000
rec: 0.238
F1: 0.384
auc: 0.000
