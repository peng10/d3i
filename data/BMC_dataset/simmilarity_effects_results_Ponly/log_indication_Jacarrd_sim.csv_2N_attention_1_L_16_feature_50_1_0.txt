start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 546.286967754364
time_elpased: 1.667
batch start
#iterations: 1
currently lose_sum: 530.1544637084007
time_elpased: 1.617
batch start
#iterations: 2
currently lose_sum: 525.1065147221088
time_elpased: 1.626
batch start
#iterations: 3
currently lose_sum: 520.46517598629
time_elpased: 1.615
batch start
#iterations: 4
currently lose_sum: 516.6904302835464
time_elpased: 1.613
batch start
#iterations: 5
currently lose_sum: 514.4660111665726
time_elpased: 1.619
batch start
#iterations: 6
currently lose_sum: 512.148447573185
time_elpased: 1.62
batch start
#iterations: 7
currently lose_sum: 511.0565930902958
time_elpased: 1.619
batch start
#iterations: 8
currently lose_sum: 510.4852378964424
time_elpased: 1.62
batch start
#iterations: 9
currently lose_sum: 511.7055027484894
time_elpased: 1.619
batch start
#iterations: 10
currently lose_sum: 510.4370181262493
time_elpased: 1.627
batch start
#iterations: 11
currently lose_sum: 510.86087453365326
time_elpased: 1.626
batch start
#iterations: 12
currently lose_sum: 508.622443318367
time_elpased: 1.627
batch start
#iterations: 13
currently lose_sum: 506.94901916384697
time_elpased: 1.615
batch start
#iterations: 14
currently lose_sum: 506.9443937242031
time_elpased: 1.618
batch start
#iterations: 15
currently lose_sum: 504.9932490289211
time_elpased: 1.628
batch start
#iterations: 16
currently lose_sum: 505.4945949912071
time_elpased: 1.626
batch start
#iterations: 17
currently lose_sum: 506.51524022221565
time_elpased: 1.629
batch start
#iterations: 18
currently lose_sum: 505.53975027799606
time_elpased: 1.641
batch start
#iterations: 19
currently lose_sum: 505.50302651524544
time_elpased: 1.619
start validation test
0.228659793814
1.0
0.228659793814
0.372210102366
0
validation finish
batch start
#iterations: 20
currently lose_sum: 504.5882598757744
time_elpased: 1.611
batch start
#iterations: 21
currently lose_sum: 505.9220822751522
time_elpased: 1.632
batch start
#iterations: 22
currently lose_sum: 503.61761751770973
time_elpased: 1.616
batch start
#iterations: 23
currently lose_sum: 505.0876909196377
time_elpased: 1.62
batch start
#iterations: 24
currently lose_sum: 502.61925598978996
time_elpased: 1.621
batch start
#iterations: 25
currently lose_sum: 502.8539369404316
time_elpased: 1.634
batch start
#iterations: 26
currently lose_sum: 503.3767999410629
time_elpased: 1.634
batch start
#iterations: 27
currently lose_sum: 501.94885724782944
time_elpased: 1.625
batch start
#iterations: 28
currently lose_sum: 500.88327583670616
time_elpased: 1.614
batch start
#iterations: 29
currently lose_sum: 502.2466470301151
time_elpased: 1.611
batch start
#iterations: 30
currently lose_sum: 501.3223911523819
time_elpased: 1.632
batch start
#iterations: 31
currently lose_sum: 501.8631810247898
time_elpased: 1.612
batch start
#iterations: 32
currently lose_sum: 502.50356036424637
time_elpased: 1.635
batch start
#iterations: 33
currently lose_sum: 502.37831780314445
time_elpased: 1.618
batch start
#iterations: 34
currently lose_sum: 502.33539167046547
time_elpased: 1.631
batch start
#iterations: 35
currently lose_sum: 500.51868894696236
time_elpased: 1.619
batch start
#iterations: 36
currently lose_sum: 502.18596428632736
time_elpased: 1.62
batch start
#iterations: 37
currently lose_sum: 500.05285757780075
time_elpased: 1.62
batch start
#iterations: 38
currently lose_sum: 499.4366956949234
time_elpased: 1.632
batch start
#iterations: 39
currently lose_sum: 501.32468327879906
time_elpased: 1.616
start validation test
0.180824742268
1.0
0.180824742268
0.306268552471
0
validation finish
batch start
#iterations: 40
currently lose_sum: 500.6069905459881
time_elpased: 1.632
batch start
#iterations: 41
currently lose_sum: 501.28822898864746
time_elpased: 1.634
batch start
#iterations: 42
currently lose_sum: 498.87382596731186
time_elpased: 1.625
batch start
#iterations: 43
currently lose_sum: 498.9880477488041
time_elpased: 1.632
batch start
#iterations: 44
currently lose_sum: 501.84112483263016
time_elpased: 1.642
batch start
#iterations: 45
currently lose_sum: 502.95839485526085
time_elpased: 1.614
batch start
#iterations: 46
currently lose_sum: 498.65348920226097
time_elpased: 1.638
batch start
#iterations: 47
currently lose_sum: 499.95284843444824
time_elpased: 1.621
batch start
#iterations: 48
currently lose_sum: 500.86885446310043
time_elpased: 1.633
batch start
#iterations: 49
currently lose_sum: 496.2926923632622
time_elpased: 1.63
batch start
#iterations: 50
currently lose_sum: 501.10988199710846
time_elpased: 1.624
batch start
#iterations: 51
currently lose_sum: 499.6869629621506
time_elpased: 1.631
batch start
#iterations: 52
currently lose_sum: 499.2556906938553
time_elpased: 1.636
batch start
#iterations: 53
currently lose_sum: 499.6086735725403
time_elpased: 1.618
batch start
#iterations: 54
currently lose_sum: 499.097122579813
time_elpased: 1.633
batch start
#iterations: 55
currently lose_sum: 498.8350299298763
time_elpased: 1.62
batch start
#iterations: 56
currently lose_sum: 499.6945088505745
time_elpased: 1.619
batch start
#iterations: 57
currently lose_sum: 498.8681025803089
time_elpased: 1.62
batch start
#iterations: 58
currently lose_sum: 499.41286823153496
time_elpased: 1.622
batch start
#iterations: 59
currently lose_sum: 499.41169542074203
time_elpased: 1.635
start validation test
0.20793814433
1.0
0.20793814433
0.344286080055
0
validation finish
batch start
#iterations: 60
currently lose_sum: 499.00661611557007
time_elpased: 1.633
batch start
#iterations: 61
currently lose_sum: 497.94716170430183
time_elpased: 1.619
batch start
#iterations: 62
currently lose_sum: 502.00912740826607
time_elpased: 1.635
batch start
#iterations: 63
currently lose_sum: 500.48478093743324
time_elpased: 1.627
batch start
#iterations: 64
currently lose_sum: 499.1566190421581
time_elpased: 1.627
batch start
#iterations: 65
currently lose_sum: 498.8197617530823
time_elpased: 1.633
batch start
#iterations: 66
currently lose_sum: 499.0407065451145
time_elpased: 1.645
batch start
#iterations: 67
currently lose_sum: 496.76571878790855
time_elpased: 1.634
batch start
#iterations: 68
currently lose_sum: 498.8115865588188
time_elpased: 1.627
batch start
#iterations: 69
currently lose_sum: 500.5079493224621
time_elpased: 1.63
batch start
#iterations: 70
currently lose_sum: 497.3277586698532
time_elpased: 1.622
batch start
#iterations: 71
currently lose_sum: 499.32504841685295
time_elpased: 1.628
batch start
#iterations: 72
currently lose_sum: 500.85640677809715
time_elpased: 1.632
batch start
#iterations: 73
currently lose_sum: 498.1291574537754
time_elpased: 1.649
batch start
#iterations: 74
currently lose_sum: 498.7937725484371
time_elpased: 1.614
batch start
#iterations: 75
currently lose_sum: 497.22302582859993
time_elpased: 1.625
batch start
#iterations: 76
currently lose_sum: 497.8675148487091
time_elpased: 1.652
batch start
#iterations: 77
currently lose_sum: 498.69264447689056
time_elpased: 1.619
batch start
#iterations: 78
currently lose_sum: 498.25891694426537
time_elpased: 1.632
batch start
#iterations: 79
currently lose_sum: 499.70772910118103
time_elpased: 1.627
start validation test
0.263505154639
1.0
0.263505154639
0.417101827676
0
validation finish
batch start
#iterations: 80
currently lose_sum: 498.3684746623039
time_elpased: 1.627
batch start
#iterations: 81
currently lose_sum: 497.1311976611614
time_elpased: 1.629
batch start
#iterations: 82
currently lose_sum: 497.83728635311127
time_elpased: 1.629
batch start
#iterations: 83
currently lose_sum: 497.4649194777012
time_elpased: 1.621
batch start
#iterations: 84
currently lose_sum: 497.97255876660347
time_elpased: 1.633
batch start
#iterations: 85
currently lose_sum: 496.68016079068184
time_elpased: 1.644
batch start
#iterations: 86
currently lose_sum: 497.5912644267082
time_elpased: 1.629
batch start
#iterations: 87
currently lose_sum: 496.9693605005741
time_elpased: 1.632
batch start
#iterations: 88
currently lose_sum: 497.231210231781
time_elpased: 1.626
batch start
#iterations: 89
currently lose_sum: 498.6020361483097
time_elpased: 1.625
batch start
#iterations: 90
currently lose_sum: 498.41095009446144
time_elpased: 1.642
batch start
#iterations: 91
currently lose_sum: 496.5841911435127
time_elpased: 1.635
batch start
#iterations: 92
currently lose_sum: 497.59861397743225
time_elpased: 1.64
batch start
#iterations: 93
currently lose_sum: 497.36787435412407
time_elpased: 1.624
batch start
#iterations: 94
currently lose_sum: 496.4632513523102
time_elpased: 1.637
batch start
#iterations: 95
currently lose_sum: 496.9370626807213
time_elpased: 1.625
batch start
#iterations: 96
currently lose_sum: 498.7650865316391
time_elpased: 1.653
batch start
#iterations: 97
currently lose_sum: 496.24619722366333
time_elpased: 1.62
batch start
#iterations: 98
currently lose_sum: 497.3550086915493
time_elpased: 1.634
batch start
#iterations: 99
currently lose_sum: 497.61830204725266
time_elpased: 1.632
start validation test
0.149793814433
1.0
0.149793814433
0.26055769748
0
validation finish
batch start
#iterations: 100
currently lose_sum: 497.09495320916176
time_elpased: 1.641
batch start
#iterations: 101
currently lose_sum: 497.5887740254402
time_elpased: 1.631
batch start
#iterations: 102
currently lose_sum: 496.13923367857933
time_elpased: 1.623
batch start
#iterations: 103
currently lose_sum: 498.665795981884
time_elpased: 1.634
batch start
#iterations: 104
currently lose_sum: 496.86090153455734
time_elpased: 1.635
batch start
#iterations: 105
currently lose_sum: 497.1529636979103
time_elpased: 1.622
batch start
#iterations: 106
currently lose_sum: 499.7172137796879
time_elpased: 1.64
batch start
#iterations: 107
currently lose_sum: 496.9848647117615
time_elpased: 1.638
batch start
#iterations: 108
currently lose_sum: 495.8564802110195
time_elpased: 1.631
batch start
#iterations: 109
currently lose_sum: 497.228570073843
time_elpased: 1.629
batch start
#iterations: 110
currently lose_sum: 498.84321969747543
time_elpased: 1.634
batch start
#iterations: 111
currently lose_sum: 500.1042309105396
time_elpased: 1.62
batch start
#iterations: 112
currently lose_sum: 495.9853413105011
time_elpased: 1.637
batch start
#iterations: 113
currently lose_sum: 497.7739776968956
time_elpased: 1.627
batch start
#iterations: 114
currently lose_sum: 498.19213232398033
time_elpased: 1.631
batch start
#iterations: 115
currently lose_sum: 497.6513059437275
time_elpased: 1.635
batch start
#iterations: 116
currently lose_sum: 496.2453501820564
time_elpased: 1.627
batch start
#iterations: 117
currently lose_sum: 498.0178593695164
time_elpased: 1.659
batch start
#iterations: 118
currently lose_sum: 496.8724462687969
time_elpased: 1.631
batch start
#iterations: 119
currently lose_sum: 497.4015700519085
time_elpased: 1.632
start validation test
0.240721649485
1.0
0.240721649485
0.388034898214
0
validation finish
batch start
#iterations: 120
currently lose_sum: 497.6840652823448
time_elpased: 1.629
batch start
#iterations: 121
currently lose_sum: 495.462405025959
time_elpased: 1.622
batch start
#iterations: 122
currently lose_sum: 495.7312167584896
time_elpased: 1.646
batch start
#iterations: 123
currently lose_sum: 496.7236817777157
time_elpased: 1.62
batch start
#iterations: 124
currently lose_sum: 497.09842905402184
time_elpased: 1.618
batch start
#iterations: 125
currently lose_sum: 497.6423528790474
time_elpased: 1.624
batch start
#iterations: 126
currently lose_sum: 496.82663306593895
time_elpased: 1.628
batch start
#iterations: 127
currently lose_sum: 496.6963506639004
time_elpased: 1.622
batch start
#iterations: 128
currently lose_sum: 496.5706793963909
time_elpased: 1.629
batch start
#iterations: 129
currently lose_sum: 497.8756918013096
time_elpased: 1.629
batch start
#iterations: 130
currently lose_sum: 496.6996196806431
time_elpased: 1.625
batch start
#iterations: 131
currently lose_sum: 496.74124321341515
time_elpased: 1.611
batch start
#iterations: 132
currently lose_sum: 495.6547456383705
time_elpased: 1.641
batch start
#iterations: 133
currently lose_sum: 496.95808443427086
time_elpased: 1.619
batch start
#iterations: 134
currently lose_sum: 495.6502665579319
time_elpased: 1.651
batch start
#iterations: 135
currently lose_sum: 495.4490215778351
time_elpased: 1.631
batch start
#iterations: 136
currently lose_sum: 495.8966127336025
time_elpased: 1.626
batch start
#iterations: 137
currently lose_sum: 495.18606474995613
time_elpased: 1.618
batch start
#iterations: 138
currently lose_sum: 494.55922520160675
time_elpased: 1.635
batch start
#iterations: 139
currently lose_sum: 495.21497824788094
time_elpased: 1.63
start validation test
0.201340206186
1.0
0.201340206186
0.335192654252
0
validation finish
batch start
#iterations: 140
currently lose_sum: 495.1192924082279
time_elpased: 1.631
batch start
#iterations: 141
currently lose_sum: 495.64592692255974
time_elpased: 1.635
batch start
#iterations: 142
currently lose_sum: 493.21741685271263
time_elpased: 1.644
batch start
#iterations: 143
currently lose_sum: 495.6420145332813
time_elpased: 1.628
batch start
#iterations: 144
currently lose_sum: 495.3519116342068
time_elpased: 1.638
batch start
#iterations: 145
currently lose_sum: 496.82358542084694
time_elpased: 1.614
batch start
#iterations: 146
currently lose_sum: 497.1616221666336
time_elpased: 1.633
batch start
#iterations: 147
currently lose_sum: 496.85973116755486
time_elpased: 1.624
batch start
#iterations: 148
currently lose_sum: 493.84243589639664
time_elpased: 1.608
batch start
#iterations: 149
currently lose_sum: 496.6527872979641
time_elpased: 1.631
batch start
#iterations: 150
currently lose_sum: 494.9573044478893
time_elpased: 1.629
batch start
#iterations: 151
currently lose_sum: 494.5553653240204
time_elpased: 1.634
batch start
#iterations: 152
currently lose_sum: 495.59457501769066
time_elpased: 1.643
batch start
#iterations: 153
currently lose_sum: 493.32631880044937
time_elpased: 1.627
batch start
#iterations: 154
currently lose_sum: 498.2589618861675
time_elpased: 1.626
batch start
#iterations: 155
currently lose_sum: 492.783010751009
time_elpased: 1.648
batch start
#iterations: 156
currently lose_sum: 495.583880841732
time_elpased: 1.634
batch start
#iterations: 157
currently lose_sum: 494.375767827034
time_elpased: 1.647
batch start
#iterations: 158
currently lose_sum: 495.2349479496479
time_elpased: 1.644
batch start
#iterations: 159
currently lose_sum: 495.54056069254875
time_elpased: 1.633
start validation test
0.169793814433
1.0
0.169793814433
0.2902969948
0
validation finish
batch start
#iterations: 160
currently lose_sum: 496.63251304626465
time_elpased: 1.634
batch start
#iterations: 161
currently lose_sum: 494.0431041419506
time_elpased: 1.635
batch start
#iterations: 162
currently lose_sum: 495.0062752366066
time_elpased: 1.625
batch start
#iterations: 163
currently lose_sum: 494.41775608062744
time_elpased: 1.634
batch start
#iterations: 164
currently lose_sum: 495.32724845409393
time_elpased: 1.634
batch start
#iterations: 165
currently lose_sum: 497.01968532800674
time_elpased: 1.637
batch start
#iterations: 166
currently lose_sum: 496.10428670048714
time_elpased: 1.63
batch start
#iterations: 167
currently lose_sum: 494.7567566037178
time_elpased: 1.636
batch start
#iterations: 168
currently lose_sum: 495.3560069799423
time_elpased: 1.635
batch start
#iterations: 169
currently lose_sum: 495.4570699930191
time_elpased: 1.636
batch start
#iterations: 170
currently lose_sum: 494.17867359519005
time_elpased: 1.623
batch start
#iterations: 171
currently lose_sum: 495.59477373957634
time_elpased: 1.629
batch start
#iterations: 172
currently lose_sum: 496.7268370091915
time_elpased: 1.624
batch start
#iterations: 173
currently lose_sum: 496.5698044896126
time_elpased: 1.62
batch start
#iterations: 174
currently lose_sum: 495.54634857177734
time_elpased: 1.631
batch start
#iterations: 175
currently lose_sum: 494.93468648195267
time_elpased: 1.619
batch start
#iterations: 176
currently lose_sum: 493.50424763560295
time_elpased: 1.631
batch start
#iterations: 177
currently lose_sum: 496.2602069079876
time_elpased: 1.619
batch start
#iterations: 178
currently lose_sum: 495.6499770283699
time_elpased: 1.625
batch start
#iterations: 179
currently lose_sum: 497.20617604255676
time_elpased: 1.623
start validation test
0.21793814433
1.0
0.21793814433
0.357880480786
0
validation finish
batch start
#iterations: 180
currently lose_sum: 497.76124918460846
time_elpased: 1.635
batch start
#iterations: 181
currently lose_sum: 497.1620647609234
time_elpased: 1.628
batch start
#iterations: 182
currently lose_sum: 494.9086312055588
time_elpased: 1.628
batch start
#iterations: 183
currently lose_sum: 496.7026010751724
time_elpased: 1.642
batch start
#iterations: 184
currently lose_sum: 494.4806086421013
time_elpased: 1.627
batch start
#iterations: 185
currently lose_sum: 495.65327313542366
time_elpased: 1.631
batch start
#iterations: 186
currently lose_sum: 495.12100061774254
time_elpased: 1.62
batch start
#iterations: 187
currently lose_sum: 495.4673877954483
time_elpased: 1.629
batch start
#iterations: 188
currently lose_sum: 495.7594746351242
time_elpased: 1.624
batch start
#iterations: 189
currently lose_sum: 495.84421065449715
time_elpased: 1.629
batch start
#iterations: 190
currently lose_sum: 494.73589727282524
time_elpased: 1.634
batch start
#iterations: 191
currently lose_sum: 495.93667128682137
time_elpased: 1.621
batch start
#iterations: 192
currently lose_sum: 493.39968207478523
time_elpased: 1.61
batch start
#iterations: 193
currently lose_sum: 495.3595585525036
time_elpased: 1.621
batch start
#iterations: 194
currently lose_sum: 496.5156444311142
time_elpased: 1.63
batch start
#iterations: 195
currently lose_sum: 497.3500799834728
time_elpased: 1.623
batch start
#iterations: 196
currently lose_sum: 494.08317989110947
time_elpased: 1.629
batch start
#iterations: 197
currently lose_sum: 498.1649513542652
time_elpased: 1.624
batch start
#iterations: 198
currently lose_sum: 493.99600726366043
time_elpased: 1.643
batch start
#iterations: 199
currently lose_sum: 495.3756212592125
time_elpased: 1.621
start validation test
0.190103092784
1.0
0.190103092784
0.319473319473
0
validation finish
batch start
#iterations: 200
currently lose_sum: 495.4352023303509
time_elpased: 1.629
batch start
#iterations: 201
currently lose_sum: 496.96105164289474
time_elpased: 1.622
batch start
#iterations: 202
currently lose_sum: 493.56901210546494
time_elpased: 1.635
batch start
#iterations: 203
currently lose_sum: 494.07373610138893
time_elpased: 1.634
batch start
#iterations: 204
currently lose_sum: 495.6678209602833
time_elpased: 1.628
batch start
#iterations: 205
currently lose_sum: 495.5768399834633
time_elpased: 1.632
batch start
#iterations: 206
currently lose_sum: 498.2423702478409
time_elpased: 1.633
batch start
#iterations: 207
currently lose_sum: 494.35130152106285
time_elpased: 1.642
batch start
#iterations: 208
currently lose_sum: 494.08719727396965
time_elpased: 1.625
batch start
#iterations: 209
currently lose_sum: 495.27266269922256
time_elpased: 1.625
batch start
#iterations: 210
currently lose_sum: 495.39259883761406
time_elpased: 1.642
batch start
#iterations: 211
currently lose_sum: 494.8424091935158
time_elpased: 1.622
batch start
#iterations: 212
currently lose_sum: 494.9439961910248
time_elpased: 1.639
batch start
#iterations: 213
currently lose_sum: 494.39221236109734
time_elpased: 1.638
batch start
#iterations: 214
currently lose_sum: 494.92960008978844
time_elpased: 1.625
batch start
#iterations: 215
currently lose_sum: 495.24442517757416
time_elpased: 1.62
batch start
#iterations: 216
currently lose_sum: 494.91261902451515
time_elpased: 1.62
batch start
#iterations: 217
currently lose_sum: 494.3900365829468
time_elpased: 1.634
batch start
#iterations: 218
currently lose_sum: 494.185528665781
time_elpased: 1.622
batch start
#iterations: 219
currently lose_sum: 497.2694031894207
time_elpased: 1.631
start validation test
0.235979381443
1.0
0.235979381443
0.381850029193
0
validation finish
batch start
#iterations: 220
currently lose_sum: 494.18038883805275
time_elpased: 1.628
batch start
#iterations: 221
currently lose_sum: 496.4991058409214
time_elpased: 1.641
batch start
#iterations: 222
currently lose_sum: 493.1935230791569
time_elpased: 1.625
batch start
#iterations: 223
currently lose_sum: 495.64477583765984
time_elpased: 1.621
batch start
#iterations: 224
currently lose_sum: 495.47281658649445
time_elpased: 1.627
batch start
#iterations: 225
currently lose_sum: 498.62308210134506
time_elpased: 1.62
batch start
#iterations: 226
currently lose_sum: 494.6870469748974
time_elpased: 1.631
batch start
#iterations: 227
currently lose_sum: 495.6761485040188
time_elpased: 1.63
batch start
#iterations: 228
currently lose_sum: 493.1066543459892
time_elpased: 1.617
batch start
#iterations: 229
currently lose_sum: 496.14423048496246
time_elpased: 1.636
batch start
#iterations: 230
currently lose_sum: 495.9774703979492
time_elpased: 1.622
batch start
#iterations: 231
currently lose_sum: 497.2079143822193
time_elpased: 1.628
batch start
#iterations: 232
currently lose_sum: 494.86978778243065
time_elpased: 1.641
batch start
#iterations: 233
currently lose_sum: 493.5661871433258
time_elpased: 1.622
batch start
#iterations: 234
currently lose_sum: 493.4525597691536
time_elpased: 1.625
batch start
#iterations: 235
currently lose_sum: 493.5090290904045
time_elpased: 1.626
batch start
#iterations: 236
currently lose_sum: 493.62808763980865
time_elpased: 1.608
batch start
#iterations: 237
currently lose_sum: 496.35173812508583
time_elpased: 1.629
batch start
#iterations: 238
currently lose_sum: 493.4962751567364
time_elpased: 1.624
batch start
#iterations: 239
currently lose_sum: 494.2448233962059
time_elpased: 1.631
start validation test
0.115567010309
1.0
0.115567010309
0.207189723685
0
validation finish
batch start
#iterations: 240
currently lose_sum: 493.05654034018517
time_elpased: 1.624
batch start
#iterations: 241
currently lose_sum: 493.7398155927658
time_elpased: 1.631
batch start
#iterations: 242
currently lose_sum: 493.9616508483887
time_elpased: 1.641
batch start
#iterations: 243
currently lose_sum: 493.30779752135277
time_elpased: 1.622
batch start
#iterations: 244
currently lose_sum: 494.89803701639175
time_elpased: 1.628
batch start
#iterations: 245
currently lose_sum: 494.31716656684875
time_elpased: 1.631
batch start
#iterations: 246
currently lose_sum: 493.1708758175373
time_elpased: 1.626
batch start
#iterations: 247
currently lose_sum: 494.6616471707821
time_elpased: 1.648
batch start
#iterations: 248
currently lose_sum: 493.79757621884346
time_elpased: 1.644
batch start
#iterations: 249
currently lose_sum: 494.87368965148926
time_elpased: 1.653
batch start
#iterations: 250
currently lose_sum: 494.87624460458755
time_elpased: 1.625
batch start
#iterations: 251
currently lose_sum: 496.15272322297096
time_elpased: 1.629
batch start
#iterations: 252
currently lose_sum: 496.23360440135
time_elpased: 1.625
batch start
#iterations: 253
currently lose_sum: 493.9142950475216
time_elpased: 1.619
batch start
#iterations: 254
currently lose_sum: 496.81447276473045
time_elpased: 1.63
batch start
#iterations: 255
currently lose_sum: 494.05292570590973
time_elpased: 1.632
batch start
#iterations: 256
currently lose_sum: 495.7633579671383
time_elpased: 1.625
batch start
#iterations: 257
currently lose_sum: 493.7159790098667
time_elpased: 1.636
batch start
#iterations: 258
currently lose_sum: 493.80119678378105
time_elpased: 1.636
batch start
#iterations: 259
currently lose_sum: 495.0023550391197
time_elpased: 1.627
start validation test
0.256701030928
1.0
0.256701030928
0.408531583265
0
validation finish
batch start
#iterations: 260
currently lose_sum: 495.22176840901375
time_elpased: 1.629
batch start
#iterations: 261
currently lose_sum: 496.0111565887928
time_elpased: 1.618
batch start
#iterations: 262
currently lose_sum: 494.22900754213333
time_elpased: 1.637
batch start
#iterations: 263
currently lose_sum: 494.1323266327381
time_elpased: 1.623
batch start
#iterations: 264
currently lose_sum: 494.01354172825813
time_elpased: 1.626
batch start
#iterations: 265
currently lose_sum: 494.5013263821602
time_elpased: 1.621
batch start
#iterations: 266
currently lose_sum: 493.96049454808235
time_elpased: 1.63
batch start
#iterations: 267
currently lose_sum: 493.88485875725746
time_elpased: 1.623
batch start
#iterations: 268
currently lose_sum: 493.3337214589119
time_elpased: 1.633
batch start
#iterations: 269
currently lose_sum: 492.35947573184967
time_elpased: 1.631
batch start
#iterations: 270
currently lose_sum: 494.973701775074
time_elpased: 1.63
batch start
#iterations: 271
currently lose_sum: 492.81163942813873
time_elpased: 1.654
batch start
#iterations: 272
currently lose_sum: 493.8065468966961
time_elpased: 1.63
batch start
#iterations: 273
currently lose_sum: 494.69485878944397
time_elpased: 1.638
batch start
#iterations: 274
currently lose_sum: 496.46406212449074
time_elpased: 1.643
batch start
#iterations: 275
currently lose_sum: 493.85678911209106
time_elpased: 1.628
batch start
#iterations: 276
currently lose_sum: 493.8288313150406
time_elpased: 1.632
batch start
#iterations: 277
currently lose_sum: 495.3420722782612
time_elpased: 1.62
batch start
#iterations: 278
currently lose_sum: 492.9546371996403
time_elpased: 1.632
batch start
#iterations: 279
currently lose_sum: 496.7640554010868
time_elpased: 1.64
start validation test
0.187010309278
1.0
0.187010309278
0.315094667361
0
validation finish
batch start
#iterations: 280
currently lose_sum: 492.86759427189827
time_elpased: 1.62
batch start
#iterations: 281
currently lose_sum: 492.3717305660248
time_elpased: 1.626
batch start
#iterations: 282
currently lose_sum: 493.58905842900276
time_elpased: 1.618
batch start
#iterations: 283
currently lose_sum: 494.80405125021935
time_elpased: 1.618
batch start
#iterations: 284
currently lose_sum: 494.12090715765953
time_elpased: 1.636
batch start
#iterations: 285
currently lose_sum: 494.8032572865486
time_elpased: 1.621
batch start
#iterations: 286
currently lose_sum: 494.27111569046974
time_elpased: 1.618
batch start
#iterations: 287
currently lose_sum: 496.6314859986305
time_elpased: 1.618
batch start
#iterations: 288
currently lose_sum: 493.3192389309406
time_elpased: 1.63
batch start
#iterations: 289
currently lose_sum: 493.1454553306103
time_elpased: 1.626
batch start
#iterations: 290
currently lose_sum: 492.2587366104126
time_elpased: 1.636
batch start
#iterations: 291
currently lose_sum: 495.27417424321175
time_elpased: 1.633
batch start
#iterations: 292
currently lose_sum: 495.7067257463932
time_elpased: 1.628
batch start
#iterations: 293
currently lose_sum: 495.39252027869225
time_elpased: 1.638
batch start
#iterations: 294
currently lose_sum: 495.470301091671
time_elpased: 1.626
batch start
#iterations: 295
currently lose_sum: 495.5284730195999
time_elpased: 1.62
batch start
#iterations: 296
currently lose_sum: 492.8121694922447
time_elpased: 1.638
batch start
#iterations: 297
currently lose_sum: 494.902455419302
time_elpased: 1.637
batch start
#iterations: 298
currently lose_sum: 494.2807260751724
time_elpased: 1.661
batch start
#iterations: 299
currently lose_sum: 495.8259212076664
time_elpased: 1.626
start validation test
0.249072164948
1.0
0.249072164948
0.39881148894
0
validation finish
batch start
#iterations: 300
currently lose_sum: 493.7758477628231
time_elpased: 1.628
batch start
#iterations: 301
currently lose_sum: 493.4540371596813
time_elpased: 1.629
batch start
#iterations: 302
currently lose_sum: 495.41587394475937
time_elpased: 1.637
batch start
#iterations: 303
currently lose_sum: 495.80297034978867
time_elpased: 1.612
batch start
#iterations: 304
currently lose_sum: 493.98180878162384
time_elpased: 1.627
batch start
#iterations: 305
currently lose_sum: 495.00947818160057
time_elpased: 1.632
batch start
#iterations: 306
currently lose_sum: 495.576592117548
time_elpased: 1.625
batch start
#iterations: 307
currently lose_sum: 496.2892527282238
time_elpased: 1.63
batch start
#iterations: 308
currently lose_sum: 495.45984572172165
time_elpased: 1.651
batch start
#iterations: 309
currently lose_sum: 494.50853911042213
time_elpased: 1.616
batch start
#iterations: 310
currently lose_sum: 492.118166744709
time_elpased: 1.626
batch start
#iterations: 311
currently lose_sum: 493.8208090364933
time_elpased: 1.632
batch start
#iterations: 312
currently lose_sum: 495.3951717913151
time_elpased: 1.628
batch start
#iterations: 313
currently lose_sum: 494.9966942667961
time_elpased: 1.619
batch start
#iterations: 314
currently lose_sum: 493.63867405056953
time_elpased: 1.635
batch start
#iterations: 315
currently lose_sum: 494.6248574256897
time_elpased: 1.634
batch start
#iterations: 316
currently lose_sum: 495.802718937397
time_elpased: 1.637
batch start
#iterations: 317
currently lose_sum: 492.69039312005043
time_elpased: 1.632
batch start
#iterations: 318
currently lose_sum: 497.4030703306198
time_elpased: 1.645
batch start
#iterations: 319
currently lose_sum: 492.92691427469254
time_elpased: 1.648
start validation test
0.219587628866
1.0
0.219587628866
0.360101437025
0
validation finish
batch start
#iterations: 320
currently lose_sum: 494.8231721520424
time_elpased: 1.633
batch start
#iterations: 321
currently lose_sum: 495.10346457362175
time_elpased: 1.628
batch start
#iterations: 322
currently lose_sum: 493.22454649209976
time_elpased: 1.662
batch start
#iterations: 323
currently lose_sum: 493.3536012172699
time_elpased: 1.634
batch start
#iterations: 324
currently lose_sum: 496.00396180152893
time_elpased: 1.634
batch start
#iterations: 325
currently lose_sum: 492.98634445667267
time_elpased: 1.631
batch start
#iterations: 326
currently lose_sum: 495.25219812989235
time_elpased: 1.617
batch start
#iterations: 327
currently lose_sum: 495.16643396019936
time_elpased: 1.629
batch start
#iterations: 328
currently lose_sum: 494.1945646703243
time_elpased: 1.622
batch start
#iterations: 329
currently lose_sum: 494.52683490514755
time_elpased: 1.622
batch start
#iterations: 330
currently lose_sum: 496.125031799078
time_elpased: 1.641
batch start
#iterations: 331
currently lose_sum: 494.9734172821045
time_elpased: 1.629
batch start
#iterations: 332
currently lose_sum: 495.50704368948936
time_elpased: 1.634
batch start
#iterations: 333
currently lose_sum: 494.80102548003197
time_elpased: 1.627
batch start
#iterations: 334
currently lose_sum: 493.30215322971344
time_elpased: 1.635
batch start
#iterations: 335
currently lose_sum: 494.31528159976006
time_elpased: 1.618
batch start
#iterations: 336
currently lose_sum: 495.53195199370384
time_elpased: 1.628
batch start
#iterations: 337
currently lose_sum: 493.72217482328415
time_elpased: 1.631
batch start
#iterations: 338
currently lose_sum: 492.2944380939007
time_elpased: 1.626
batch start
#iterations: 339
currently lose_sum: 495.3486124575138
time_elpased: 1.634
start validation test
0.249587628866
1.0
0.249587628866
0.39947199076
0
validation finish
batch start
#iterations: 340
currently lose_sum: 493.01901069283485
time_elpased: 1.632
batch start
#iterations: 341
currently lose_sum: 494.14348325133324
time_elpased: 1.622
batch start
#iterations: 342
currently lose_sum: 495.6114683151245
time_elpased: 1.629
batch start
#iterations: 343
currently lose_sum: 496.0127786099911
time_elpased: 1.629
batch start
#iterations: 344
currently lose_sum: 497.03011986613274
time_elpased: 1.621
batch start
#iterations: 345
currently lose_sum: 496.5457661151886
time_elpased: 1.634
batch start
#iterations: 346
currently lose_sum: 494.7854476571083
time_elpased: 1.622
batch start
#iterations: 347
currently lose_sum: 494.3423283100128
time_elpased: 1.638
batch start
#iterations: 348
currently lose_sum: 496.64207139611244
time_elpased: 1.627
batch start
#iterations: 349
currently lose_sum: 494.7144733965397
time_elpased: 1.639
batch start
#iterations: 350
currently lose_sum: 494.9416073858738
time_elpased: 1.626
batch start
#iterations: 351
currently lose_sum: 494.6689011156559
time_elpased: 1.622
batch start
#iterations: 352
currently lose_sum: 493.71291786432266
time_elpased: 1.617
batch start
#iterations: 353
currently lose_sum: 495.39905577898026
time_elpased: 1.629
batch start
#iterations: 354
currently lose_sum: 494.647050768137
time_elpased: 1.633
batch start
#iterations: 355
currently lose_sum: 494.44589388370514
time_elpased: 1.626
batch start
#iterations: 356
currently lose_sum: 492.3896107375622
time_elpased: 1.635
batch start
#iterations: 357
currently lose_sum: 493.1689705848694
time_elpased: 1.629
batch start
#iterations: 358
currently lose_sum: 495.86281499266624
time_elpased: 1.627
batch start
#iterations: 359
currently lose_sum: 493.34397557377815
time_elpased: 1.63
start validation test
0.201134020619
1.0
0.201134020619
0.334906874946
0
validation finish
batch start
#iterations: 360
currently lose_sum: 493.477331161499
time_elpased: 1.623
batch start
#iterations: 361
currently lose_sum: 493.65229257941246
time_elpased: 1.64
batch start
#iterations: 362
currently lose_sum: 493.53756472468376
time_elpased: 1.626
batch start
#iterations: 363
currently lose_sum: 493.80048260092735
time_elpased: 1.629
batch start
#iterations: 364
currently lose_sum: 494.56018099188805
time_elpased: 1.648
batch start
#iterations: 365
currently lose_sum: 494.00105810165405
time_elpased: 1.631
batch start
#iterations: 366
currently lose_sum: 494.6211639046669
time_elpased: 1.63
batch start
#iterations: 367
currently lose_sum: 495.5800065398216
time_elpased: 1.625
batch start
#iterations: 368
currently lose_sum: 493.4811041355133
time_elpased: 1.629
batch start
#iterations: 369
currently lose_sum: 492.97345301508904
time_elpased: 1.642
batch start
#iterations: 370
currently lose_sum: 493.51182410120964
time_elpased: 1.625
batch start
#iterations: 371
currently lose_sum: 494.35798639059067
time_elpased: 1.63
batch start
#iterations: 372
currently lose_sum: 493.48687103390694
time_elpased: 1.643
batch start
#iterations: 373
currently lose_sum: 493.75283002853394
time_elpased: 1.621
batch start
#iterations: 374
currently lose_sum: 495.89584997296333
time_elpased: 1.622
batch start
#iterations: 375
currently lose_sum: 494.7905173301697
time_elpased: 1.627
batch start
#iterations: 376
currently lose_sum: 494.9871956706047
time_elpased: 1.633
batch start
#iterations: 377
currently lose_sum: 492.7734235525131
time_elpased: 1.631
batch start
#iterations: 378
currently lose_sum: 496.42485949397087
time_elpased: 1.631
batch start
#iterations: 379
currently lose_sum: 494.7740470468998
time_elpased: 1.636
start validation test
0.197835051546
1.0
0.197835051546
0.330321025906
0
validation finish
batch start
#iterations: 380
currently lose_sum: 492.61453551054
time_elpased: 1.624
batch start
#iterations: 381
currently lose_sum: 492.8885985314846
time_elpased: 1.647
batch start
#iterations: 382
currently lose_sum: 492.83440005779266
time_elpased: 1.621
batch start
#iterations: 383
currently lose_sum: 492.4089631140232
time_elpased: 1.633
batch start
#iterations: 384
currently lose_sum: 492.200183570385
time_elpased: 1.63
batch start
#iterations: 385
currently lose_sum: 492.0592839717865
time_elpased: 1.625
batch start
#iterations: 386
currently lose_sum: 493.35579711198807
time_elpased: 1.631
batch start
#iterations: 387
currently lose_sum: 492.97659292817116
time_elpased: 1.621
batch start
#iterations: 388
currently lose_sum: 493.024470359087
time_elpased: 1.637
batch start
#iterations: 389
currently lose_sum: 495.54222121834755
time_elpased: 1.624
batch start
#iterations: 390
currently lose_sum: 493.3908360898495
time_elpased: 1.638
batch start
#iterations: 391
currently lose_sum: 495.8912102282047
time_elpased: 1.64
batch start
#iterations: 392
currently lose_sum: 493.21105229854584
time_elpased: 1.634
batch start
#iterations: 393
currently lose_sum: 493.3559466600418
time_elpased: 1.629
batch start
#iterations: 394
currently lose_sum: 492.15710347890854
time_elpased: 1.633
batch start
#iterations: 395
currently lose_sum: 492.43037262558937
time_elpased: 1.633
batch start
#iterations: 396
currently lose_sum: 494.73069739341736
time_elpased: 1.632
batch start
#iterations: 397
currently lose_sum: 495.2506317794323
time_elpased: 1.622
batch start
#iterations: 398
currently lose_sum: 493.1858422458172
time_elpased: 1.629
batch start
#iterations: 399
currently lose_sum: 493.6341584324837
time_elpased: 1.628
start validation test
0.207319587629
1.0
0.207319587629
0.343437793527
0
validation finish
acc: 0.254
pre: 1.000
rec: 0.254
F1: 0.405
auc: 0.000
