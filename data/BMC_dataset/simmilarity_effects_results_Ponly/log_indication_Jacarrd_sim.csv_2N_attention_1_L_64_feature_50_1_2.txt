start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 546.0105013251305
time_elpased: 1.813
batch start
#iterations: 1
currently lose_sum: 530.9388239383698
time_elpased: 1.88
batch start
#iterations: 2
currently lose_sum: 525.9031466841698
time_elpased: 1.775
batch start
#iterations: 3
currently lose_sum: 520.8914854824543
time_elpased: 1.787
batch start
#iterations: 4
currently lose_sum: 519.3610424101353
time_elpased: 1.808
batch start
#iterations: 5
currently lose_sum: 518.391245752573
time_elpased: 1.796
batch start
#iterations: 6
currently lose_sum: 514.4216228723526
time_elpased: 1.771
batch start
#iterations: 7
currently lose_sum: 513.8898465633392
time_elpased: 1.807
batch start
#iterations: 8
currently lose_sum: 511.32311353087425
time_elpased: 1.787
batch start
#iterations: 9
currently lose_sum: 511.2284820973873
time_elpased: 1.783
batch start
#iterations: 10
currently lose_sum: 510.1790309250355
time_elpased: 1.862
batch start
#iterations: 11
currently lose_sum: 510.6757451593876
time_elpased: 1.773
batch start
#iterations: 12
currently lose_sum: 507.67669746279716
time_elpased: 1.78
batch start
#iterations: 13
currently lose_sum: 506.66287606954575
time_elpased: 1.817
batch start
#iterations: 14
currently lose_sum: 506.88860630989075
time_elpased: 1.802
batch start
#iterations: 15
currently lose_sum: 508.0967434644699
time_elpased: 1.805
batch start
#iterations: 16
currently lose_sum: 506.52645042538643
time_elpased: 1.776
batch start
#iterations: 17
currently lose_sum: 505.628634929657
time_elpased: 1.794
batch start
#iterations: 18
currently lose_sum: 505.92867520451546
time_elpased: 1.78
batch start
#iterations: 19
currently lose_sum: 505.7826734483242
time_elpased: 1.778
start validation test
0.166597938144
1.0
0.166597938144
0.285613290916
0
validation finish
batch start
#iterations: 20
currently lose_sum: 505.4850806593895
time_elpased: 1.851
batch start
#iterations: 21
currently lose_sum: 506.0277569293976
time_elpased: 1.824
batch start
#iterations: 22
currently lose_sum: 504.7566905915737
time_elpased: 1.835
batch start
#iterations: 23
currently lose_sum: 505.0150779783726
time_elpased: 1.8
batch start
#iterations: 24
currently lose_sum: 502.9096817076206
time_elpased: 1.776
batch start
#iterations: 25
currently lose_sum: 505.4054407775402
time_elpased: 1.779
batch start
#iterations: 26
currently lose_sum: 503.9772703945637
time_elpased: 1.783
batch start
#iterations: 27
currently lose_sum: 503.3244899213314
time_elpased: 1.788
batch start
#iterations: 28
currently lose_sum: 501.4938532412052
time_elpased: 1.769
batch start
#iterations: 29
currently lose_sum: 502.0005279779434
time_elpased: 1.857
batch start
#iterations: 30
currently lose_sum: 502.0936521291733
time_elpased: 1.785
batch start
#iterations: 31
currently lose_sum: 501.7432834804058
time_elpased: 1.782
batch start
#iterations: 32
currently lose_sum: 503.22915944457054
time_elpased: 1.804
batch start
#iterations: 33
currently lose_sum: 500.03939071297646
time_elpased: 1.798
batch start
#iterations: 34
currently lose_sum: 502.790823251009
time_elpased: 1.812
batch start
#iterations: 35
currently lose_sum: 500.62944915890694
time_elpased: 1.874
batch start
#iterations: 36
currently lose_sum: 501.1270328462124
time_elpased: 1.798
batch start
#iterations: 37
currently lose_sum: 500.4654422700405
time_elpased: 1.79
batch start
#iterations: 38
currently lose_sum: 501.6833757162094
time_elpased: 1.781
batch start
#iterations: 39
currently lose_sum: 502.89045640826225
time_elpased: 1.847
start validation test
0.138865979381
1.0
0.138865979381
0.243867113243
0
validation finish
batch start
#iterations: 40
currently lose_sum: 502.6847635805607
time_elpased: 1.784
batch start
#iterations: 41
currently lose_sum: 502.1687805056572
time_elpased: 1.883
batch start
#iterations: 42
currently lose_sum: 500.296173363924
time_elpased: 1.831
batch start
#iterations: 43
currently lose_sum: 500.4993459582329
time_elpased: 1.865
batch start
#iterations: 44
currently lose_sum: 499.9591621160507
time_elpased: 1.863
batch start
#iterations: 45
currently lose_sum: 498.9207031726837
time_elpased: 1.779
batch start
#iterations: 46
currently lose_sum: 499.46118715405464
time_elpased: 1.784
batch start
#iterations: 47
currently lose_sum: 498.86753925681114
time_elpased: 1.885
batch start
#iterations: 48
currently lose_sum: 499.3758306503296
time_elpased: 1.817
batch start
#iterations: 49
currently lose_sum: 499.35023006796837
time_elpased: 1.885
batch start
#iterations: 50
currently lose_sum: 500.09824684262276
time_elpased: 1.87
batch start
#iterations: 51
currently lose_sum: 498.7336626946926
time_elpased: 1.789
batch start
#iterations: 52
currently lose_sum: 498.97693035006523
time_elpased: 1.808
batch start
#iterations: 53
currently lose_sum: 501.1081926226616
time_elpased: 1.879
batch start
#iterations: 54
currently lose_sum: 498.7151142656803
time_elpased: 1.866
batch start
#iterations: 55
currently lose_sum: 501.8487882614136
time_elpased: 1.8
batch start
#iterations: 56
currently lose_sum: 499.2290277481079
time_elpased: 1.881
batch start
#iterations: 57
currently lose_sum: 499.48541340231895
time_elpased: 1.78
batch start
#iterations: 58
currently lose_sum: 500.4652988612652
time_elpased: 1.799
batch start
#iterations: 59
currently lose_sum: 498.3870552480221
time_elpased: 1.779
start validation test
0.23824742268
1.0
0.23824742268
0.384813920573
0
validation finish
batch start
#iterations: 60
currently lose_sum: 500.6320542693138
time_elpased: 1.82
batch start
#iterations: 61
currently lose_sum: 500.53752848505974
time_elpased: 1.79
batch start
#iterations: 62
currently lose_sum: 502.27003386616707
time_elpased: 1.789
batch start
#iterations: 63
currently lose_sum: 501.6464098393917
time_elpased: 1.831
batch start
#iterations: 64
currently lose_sum: 500.382888764143
time_elpased: 1.87
batch start
#iterations: 65
currently lose_sum: 499.77368089556694
time_elpased: 1.862
batch start
#iterations: 66
currently lose_sum: 498.99745520949364
time_elpased: 1.795
batch start
#iterations: 67
currently lose_sum: 497.519067466259
time_elpased: 1.797
batch start
#iterations: 68
currently lose_sum: 501.0805003643036
time_elpased: 1.921
batch start
#iterations: 69
currently lose_sum: 499.58242005109787
time_elpased: 1.801
batch start
#iterations: 70
currently lose_sum: 499.0660920739174
time_elpased: 1.846
batch start
#iterations: 71
currently lose_sum: 499.7466387450695
time_elpased: 1.8
batch start
#iterations: 72
currently lose_sum: 499.17683547735214
time_elpased: 1.813
batch start
#iterations: 73
currently lose_sum: 498.260170429945
time_elpased: 1.875
batch start
#iterations: 74
currently lose_sum: 499.9812421500683
time_elpased: 1.784
batch start
#iterations: 75
currently lose_sum: 497.99558728933334
time_elpased: 1.795
batch start
#iterations: 76
currently lose_sum: 500.14364689588547
time_elpased: 1.794
batch start
#iterations: 77
currently lose_sum: 498.46291929483414
time_elpased: 1.85
batch start
#iterations: 78
currently lose_sum: 499.2447170317173
time_elpased: 1.799
batch start
#iterations: 79
currently lose_sum: 500.44212675094604
time_elpased: 1.843
start validation test
0.282371134021
1.0
0.282371134021
0.440389098802
0
validation finish
batch start
#iterations: 80
currently lose_sum: 496.64089918136597
time_elpased: 1.792
batch start
#iterations: 81
currently lose_sum: 497.69968551397324
time_elpased: 1.801
batch start
#iterations: 82
currently lose_sum: 498.0816024541855
time_elpased: 1.792
batch start
#iterations: 83
currently lose_sum: 499.4154480099678
time_elpased: 1.781
batch start
#iterations: 84
currently lose_sum: 499.0062273442745
time_elpased: 1.785
batch start
#iterations: 85
currently lose_sum: 496.394490391016
time_elpased: 1.875
batch start
#iterations: 86
currently lose_sum: 497.19106844067574
time_elpased: 1.806
batch start
#iterations: 87
currently lose_sum: 497.2545692920685
time_elpased: 1.867
batch start
#iterations: 88
currently lose_sum: 497.1879326403141
time_elpased: 1.812
batch start
#iterations: 89
currently lose_sum: 497.5741179585457
time_elpased: 1.776
batch start
#iterations: 90
currently lose_sum: 499.040465593338
time_elpased: 1.792
batch start
#iterations: 91
currently lose_sum: 496.28032964468
time_elpased: 1.777
batch start
#iterations: 92
currently lose_sum: 498.3946999013424
time_elpased: 1.792
batch start
#iterations: 93
currently lose_sum: 497.07084572315216
time_elpased: 1.794
batch start
#iterations: 94
currently lose_sum: 496.7476235628128
time_elpased: 1.781
batch start
#iterations: 95
currently lose_sum: 496.9259058833122
time_elpased: 1.791
batch start
#iterations: 96
currently lose_sum: 497.75107622146606
time_elpased: 1.771
batch start
#iterations: 97
currently lose_sum: 496.8415394127369
time_elpased: 1.793
batch start
#iterations: 98
currently lose_sum: 497.5137528479099
time_elpased: 1.87
batch start
#iterations: 99
currently lose_sum: 497.08208081126213
time_elpased: 1.8
start validation test
0.209587628866
1.0
0.209587628866
0.346543935907
0
validation finish
batch start
#iterations: 100
currently lose_sum: 497.50390630960464
time_elpased: 1.863
batch start
#iterations: 101
currently lose_sum: 497.1120005249977
time_elpased: 1.808
batch start
#iterations: 102
currently lose_sum: 495.9754658639431
time_elpased: 1.843
batch start
#iterations: 103
currently lose_sum: 496.26343366503716
time_elpased: 1.79
batch start
#iterations: 104
currently lose_sum: 497.6112828552723
time_elpased: 1.803
batch start
#iterations: 105
currently lose_sum: 496.0365762412548
time_elpased: 1.781
batch start
#iterations: 106
currently lose_sum: 497.8829412162304
time_elpased: 1.858
batch start
#iterations: 107
currently lose_sum: 497.3687990307808
time_elpased: 1.795
batch start
#iterations: 108
currently lose_sum: 495.7855064570904
time_elpased: 1.812
batch start
#iterations: 109
currently lose_sum: 496.9226171374321
time_elpased: 1.885
batch start
#iterations: 110
currently lose_sum: 497.26797953248024
time_elpased: 1.783
batch start
#iterations: 111
currently lose_sum: 498.60701447725296
time_elpased: 1.792
batch start
#iterations: 112
currently lose_sum: 495.28863418102264
time_elpased: 1.844
batch start
#iterations: 113
currently lose_sum: 497.2380890250206
time_elpased: 1.811
batch start
#iterations: 114
currently lose_sum: 495.846138715744
time_elpased: 1.879
batch start
#iterations: 115
currently lose_sum: 496.66319036483765
time_elpased: 1.808
batch start
#iterations: 116
currently lose_sum: 495.44986844062805
time_elpased: 1.871
batch start
#iterations: 117
currently lose_sum: 497.091080725193
time_elpased: 1.791
batch start
#iterations: 118
currently lose_sum: 495.89896127581596
time_elpased: 1.802
batch start
#iterations: 119
currently lose_sum: 495.12874963879585
time_elpased: 1.797
start validation test
0.269896907216
1.0
0.269896907216
0.425069004709
0
validation finish
batch start
#iterations: 120
currently lose_sum: 497.61524668335915
time_elpased: 1.805
batch start
#iterations: 121
currently lose_sum: 493.8817718923092
time_elpased: 1.801
batch start
#iterations: 122
currently lose_sum: 494.5013555586338
time_elpased: 1.766
batch start
#iterations: 123
currently lose_sum: 494.265826523304
time_elpased: 1.792
batch start
#iterations: 124
currently lose_sum: 494.508592158556
time_elpased: 1.782
batch start
#iterations: 125
currently lose_sum: 494.9088155031204
time_elpased: 1.807
batch start
#iterations: 126
currently lose_sum: 494.7002274096012
time_elpased: 1.841
batch start
#iterations: 127
currently lose_sum: 498.1467395722866
time_elpased: 1.841
batch start
#iterations: 128
currently lose_sum: 494.0776571929455
time_elpased: 1.781
batch start
#iterations: 129
currently lose_sum: 494.4895989000797
time_elpased: 1.87
batch start
#iterations: 130
currently lose_sum: 494.5750675499439
time_elpased: 1.789
batch start
#iterations: 131
currently lose_sum: 495.0504343211651
time_elpased: 1.842
batch start
#iterations: 132
currently lose_sum: 497.3961350619793
time_elpased: 1.791
batch start
#iterations: 133
currently lose_sum: 494.97591149806976
time_elpased: 1.789
batch start
#iterations: 134
currently lose_sum: 495.27292716503143
time_elpased: 1.794
batch start
#iterations: 135
currently lose_sum: 493.67601189017296
time_elpased: 1.816
batch start
#iterations: 136
currently lose_sum: 493.5678029358387
time_elpased: 1.881
batch start
#iterations: 137
currently lose_sum: 493.5732255280018
time_elpased: 1.814
batch start
#iterations: 138
currently lose_sum: 495.18957620859146
time_elpased: 1.865
batch start
#iterations: 139
currently lose_sum: 495.50345209240913
time_elpased: 1.783
start validation test
0.171340206186
1.0
0.171340206186
0.292554127794
0
validation finish
batch start
#iterations: 140
currently lose_sum: 493.5219343304634
time_elpased: 1.795
batch start
#iterations: 141
currently lose_sum: 496.89058089256287
time_elpased: 1.79
batch start
#iterations: 142
currently lose_sum: 494.7732068300247
time_elpased: 1.858
batch start
#iterations: 143
currently lose_sum: 495.7432703971863
time_elpased: 1.855
batch start
#iterations: 144
currently lose_sum: 496.10988265275955
time_elpased: 1.79
batch start
#iterations: 145
currently lose_sum: 496.35873624682426
time_elpased: 1.804
batch start
#iterations: 146
currently lose_sum: 496.38121488690376
time_elpased: 1.772
batch start
#iterations: 147
currently lose_sum: 495.0202828645706
time_elpased: 1.848
batch start
#iterations: 148
currently lose_sum: 493.06117007136345
time_elpased: 1.793
batch start
#iterations: 149
currently lose_sum: 497.37868773937225
time_elpased: 1.783
batch start
#iterations: 150
currently lose_sum: 494.4355307519436
time_elpased: 1.799
batch start
#iterations: 151
currently lose_sum: 495.7675890624523
time_elpased: 1.776
batch start
#iterations: 152
currently lose_sum: 494.4536710381508
time_elpased: 1.856
batch start
#iterations: 153
currently lose_sum: 491.10436603426933
time_elpased: 1.79
batch start
#iterations: 154
currently lose_sum: 496.47323366999626
time_elpased: 1.811
batch start
#iterations: 155
currently lose_sum: 493.5344229340553
time_elpased: 1.779
batch start
#iterations: 156
currently lose_sum: 494.60692101716995
time_elpased: 1.802
batch start
#iterations: 157
currently lose_sum: 495.4647616446018
time_elpased: 1.868
batch start
#iterations: 158
currently lose_sum: 492.91755053400993
time_elpased: 1.78
batch start
#iterations: 159
currently lose_sum: 495.0324182510376
time_elpased: 1.835
start validation test
0.236804123711
1.0
0.236804123711
0.3829290656
0
validation finish
batch start
#iterations: 160
currently lose_sum: 494.93392300605774
time_elpased: 1.77
batch start
#iterations: 161
currently lose_sum: 495.0914471745491
time_elpased: 1.786
batch start
#iterations: 162
currently lose_sum: 493.5146870613098
time_elpased: 1.797
batch start
#iterations: 163
currently lose_sum: 493.18353632092476
time_elpased: 1.805
batch start
#iterations: 164
currently lose_sum: 496.75262343883514
time_elpased: 1.778
batch start
#iterations: 165
currently lose_sum: 493.98359686136246
time_elpased: 1.873
batch start
#iterations: 166
currently lose_sum: 494.87616541981697
time_elpased: 1.783
batch start
#iterations: 167
currently lose_sum: 495.58272114396095
time_elpased: 1.857
batch start
#iterations: 168
currently lose_sum: 495.2129086256027
time_elpased: 1.792
batch start
#iterations: 169
currently lose_sum: 497.1987775862217
time_elpased: 1.881
batch start
#iterations: 170
currently lose_sum: 495.10302317142487
time_elpased: 1.801
batch start
#iterations: 171
currently lose_sum: 493.85756453871727
time_elpased: 1.795
batch start
#iterations: 172
currently lose_sum: 496.0886122882366
time_elpased: 1.877
batch start
#iterations: 173
currently lose_sum: 495.3890768587589
time_elpased: 1.786
batch start
#iterations: 174
currently lose_sum: 496.26964792609215
time_elpased: 1.797
batch start
#iterations: 175
currently lose_sum: 495.78879275918007
time_elpased: 1.799
batch start
#iterations: 176
currently lose_sum: 494.4986525774002
time_elpased: 1.777
batch start
#iterations: 177
currently lose_sum: 495.65833273530006
time_elpased: 1.866
batch start
#iterations: 178
currently lose_sum: 494.694219827652
time_elpased: 1.837
batch start
#iterations: 179
currently lose_sum: 493.36817169189453
time_elpased: 1.804
start validation test
0.29381443299
1.0
0.29381443299
0.454183266932
0
validation finish
batch start
#iterations: 180
currently lose_sum: 494.3078147470951
time_elpased: 1.884
batch start
#iterations: 181
currently lose_sum: 495.7022370696068
time_elpased: 1.801
batch start
#iterations: 182
currently lose_sum: 492.40794625878334
time_elpased: 1.802
batch start
#iterations: 183
currently lose_sum: 495.5861289203167
time_elpased: 1.798
batch start
#iterations: 184
currently lose_sum: 493.66062384843826
time_elpased: 1.84
batch start
#iterations: 185
currently lose_sum: 491.69165870547295
time_elpased: 1.793
batch start
#iterations: 186
currently lose_sum: 495.9428562819958
time_elpased: 1.801
batch start
#iterations: 187
currently lose_sum: 494.29051411151886
time_elpased: 1.791
batch start
#iterations: 188
currently lose_sum: 491.92773884534836
time_elpased: 1.831
batch start
#iterations: 189
currently lose_sum: 492.5746117532253
time_elpased: 1.792
batch start
#iterations: 190
currently lose_sum: 491.84533700346947
time_elpased: 1.919
batch start
#iterations: 191
currently lose_sum: 493.84612092375755
time_elpased: 1.868
batch start
#iterations: 192
currently lose_sum: 493.54165199398994
time_elpased: 1.784
batch start
#iterations: 193
currently lose_sum: 493.7749851644039
time_elpased: 1.798
batch start
#iterations: 194
currently lose_sum: 494.3773145377636
time_elpased: 1.774
batch start
#iterations: 195
currently lose_sum: 494.345242023468
time_elpased: 1.783
batch start
#iterations: 196
currently lose_sum: 493.04424515366554
time_elpased: 1.831
batch start
#iterations: 197
currently lose_sum: 495.5739118754864
time_elpased: 1.819
batch start
#iterations: 198
currently lose_sum: 493.58631655573845
time_elpased: 1.808
batch start
#iterations: 199
currently lose_sum: 494.16588819026947
time_elpased: 1.862
start validation test
0.257422680412
1.0
0.257422680412
0.409444945478
0
validation finish
batch start
#iterations: 200
currently lose_sum: 494.98720958828926
time_elpased: 1.856
batch start
#iterations: 201
currently lose_sum: 493.4735241532326
time_elpased: 1.85
batch start
#iterations: 202
currently lose_sum: 493.74657756090164
time_elpased: 1.805
batch start
#iterations: 203
currently lose_sum: 492.39519065618515
time_elpased: 1.792
batch start
#iterations: 204
currently lose_sum: 494.4567380845547
time_elpased: 1.78
batch start
#iterations: 205
currently lose_sum: 493.8028296530247
time_elpased: 1.861
batch start
#iterations: 206
currently lose_sum: 495.00931280851364
time_elpased: 1.857
batch start
#iterations: 207
currently lose_sum: 491.4729770421982
time_elpased: 1.799
batch start
#iterations: 208
currently lose_sum: 493.20451134443283
time_elpased: 1.869
batch start
#iterations: 209
currently lose_sum: 492.6216236948967
time_elpased: 1.782
batch start
#iterations: 210
currently lose_sum: 493.65220490098
time_elpased: 1.883
batch start
#iterations: 211
currently lose_sum: 494.82934537529945
time_elpased: 1.774
batch start
#iterations: 212
currently lose_sum: 493.73417633771896
time_elpased: 1.788
batch start
#iterations: 213
currently lose_sum: 493.05173376202583
time_elpased: 1.795
batch start
#iterations: 214
currently lose_sum: 494.2904383838177
time_elpased: 1.795
batch start
#iterations: 215
currently lose_sum: 493.4637418985367
time_elpased: 1.789
batch start
#iterations: 216
currently lose_sum: 493.965319365263
time_elpased: 1.778
batch start
#iterations: 217
currently lose_sum: 492.8277678787708
time_elpased: 1.786
batch start
#iterations: 218
currently lose_sum: 493.30764627456665
time_elpased: 1.843
batch start
#iterations: 219
currently lose_sum: 493.92004320025444
time_elpased: 1.793
start validation test
0.277216494845
1.0
0.277216494845
0.434094761482
0
validation finish
batch start
#iterations: 220
currently lose_sum: 491.75548246502876
time_elpased: 1.8
batch start
#iterations: 221
currently lose_sum: 493.9844104349613
time_elpased: 1.878
batch start
#iterations: 222
currently lose_sum: 491.8281928896904
time_elpased: 1.803
batch start
#iterations: 223
currently lose_sum: 494.17679592967033
time_elpased: 1.854
batch start
#iterations: 224
currently lose_sum: 493.31734147667885
time_elpased: 1.805
batch start
#iterations: 225
currently lose_sum: 496.9306147992611
time_elpased: 1.798
batch start
#iterations: 226
currently lose_sum: 493.4458526670933
time_elpased: 1.871
batch start
#iterations: 227
currently lose_sum: 491.3723230957985
time_elpased: 1.791
batch start
#iterations: 228
currently lose_sum: 491.78485825657845
time_elpased: 1.787
batch start
#iterations: 229
currently lose_sum: 491.3744532763958
time_elpased: 1.784
batch start
#iterations: 230
currently lose_sum: 494.82168954610825
time_elpased: 1.813
batch start
#iterations: 231
currently lose_sum: 496.56512984633446
time_elpased: 1.79
batch start
#iterations: 232
currently lose_sum: 492.14160826802254
time_elpased: 1.813
batch start
#iterations: 233
currently lose_sum: 491.6112397015095
time_elpased: 1.789
batch start
#iterations: 234
currently lose_sum: 492.1560754477978
time_elpased: 1.801
batch start
#iterations: 235
currently lose_sum: 491.856258302927
time_elpased: 1.772
batch start
#iterations: 236
currently lose_sum: 491.22521939873695
time_elpased: 1.801
batch start
#iterations: 237
currently lose_sum: 492.93182173371315
time_elpased: 1.787
batch start
#iterations: 238
currently lose_sum: 491.3507709503174
time_elpased: 1.788
batch start
#iterations: 239
currently lose_sum: 492.82223430275917
time_elpased: 1.794
start validation test
0.140103092784
1.0
0.140103092784
0.24577267384
0
validation finish
batch start
#iterations: 240
currently lose_sum: 490.9493806064129
time_elpased: 1.8
batch start
#iterations: 241
currently lose_sum: 493.8153189122677
time_elpased: 1.794
batch start
#iterations: 242
currently lose_sum: 491.46599957346916
time_elpased: 1.783
batch start
#iterations: 243
currently lose_sum: 490.51241785287857
time_elpased: 1.799
batch start
#iterations: 244
currently lose_sum: 492.8178913593292
time_elpased: 1.812
batch start
#iterations: 245
currently lose_sum: 491.44606801867485
time_elpased: 1.786
batch start
#iterations: 246
currently lose_sum: 491.67065915465355
time_elpased: 1.805
batch start
#iterations: 247
currently lose_sum: 492.4985189437866
time_elpased: 1.859
batch start
#iterations: 248
currently lose_sum: 491.2399782836437
time_elpased: 1.778
batch start
#iterations: 249
currently lose_sum: 490.9654668569565
time_elpased: 1.784
batch start
#iterations: 250
currently lose_sum: 492.08475628495216
time_elpased: 1.783
batch start
#iterations: 251
currently lose_sum: 494.0170465707779
time_elpased: 1.78
batch start
#iterations: 252
currently lose_sum: 493.12992736697197
time_elpased: 1.812
batch start
#iterations: 253
currently lose_sum: 491.90467381477356
time_elpased: 1.812
batch start
#iterations: 254
currently lose_sum: 493.7672184407711
time_elpased: 1.848
batch start
#iterations: 255
currently lose_sum: 492.43742087483406
time_elpased: 1.852
batch start
#iterations: 256
currently lose_sum: 490.4291957616806
time_elpased: 1.825
batch start
#iterations: 257
currently lose_sum: 491.3025949001312
time_elpased: 1.874
batch start
#iterations: 258
currently lose_sum: 493.3449576199055
time_elpased: 1.859
batch start
#iterations: 259
currently lose_sum: 493.0826303064823
time_elpased: 1.785
start validation test
0.291649484536
1.0
0.291649484536
0.45159230585
0
validation finish
batch start
#iterations: 260
currently lose_sum: 494.27732583880424
time_elpased: 1.783
batch start
#iterations: 261
currently lose_sum: 493.47971281409264
time_elpased: 1.771
batch start
#iterations: 262
currently lose_sum: 490.6690140366554
time_elpased: 1.802
batch start
#iterations: 263
currently lose_sum: 491.35647216439247
time_elpased: 1.782
batch start
#iterations: 264
currently lose_sum: 491.71095460653305
time_elpased: 1.845
batch start
#iterations: 265
currently lose_sum: 492.7977710366249
time_elpased: 1.814
batch start
#iterations: 266
currently lose_sum: 492.02806851267815
time_elpased: 1.789
batch start
#iterations: 267
currently lose_sum: 491.010190308094
time_elpased: 1.845
batch start
#iterations: 268
currently lose_sum: 492.77315840125084
time_elpased: 1.793
batch start
#iterations: 269
currently lose_sum: 493.05564844608307
time_elpased: 1.788
batch start
#iterations: 270
currently lose_sum: 491.66249868273735
time_elpased: 1.771
batch start
#iterations: 271
currently lose_sum: 490.5654549896717
time_elpased: 1.858
batch start
#iterations: 272
currently lose_sum: 491.9196266531944
time_elpased: 1.853
batch start
#iterations: 273
currently lose_sum: 490.2712952196598
time_elpased: 1.873
batch start
#iterations: 274
currently lose_sum: 492.6976565718651
time_elpased: 1.781
batch start
#iterations: 275
currently lose_sum: 490.87763115763664
time_elpased: 1.812
batch start
#iterations: 276
currently lose_sum: 492.0309910774231
time_elpased: 1.786
batch start
#iterations: 277
currently lose_sum: 492.7573453783989
time_elpased: 1.817
batch start
#iterations: 278
currently lose_sum: 492.70978304743767
time_elpased: 1.795
batch start
#iterations: 279
currently lose_sum: 493.45905590057373
time_elpased: 1.794
start validation test
0.188865979381
1.0
0.188865979381
0.317724592438
0
validation finish
batch start
#iterations: 280
currently lose_sum: 490.00771790742874
time_elpased: 1.842
batch start
#iterations: 281
currently lose_sum: 489.921289563179
time_elpased: 1.825
batch start
#iterations: 282
currently lose_sum: 493.15283876657486
time_elpased: 1.887
batch start
#iterations: 283
currently lose_sum: 491.9806918799877
time_elpased: 1.789
batch start
#iterations: 284
currently lose_sum: 493.68708223104477
time_elpased: 1.786
batch start
#iterations: 285
currently lose_sum: 492.49530404806137
time_elpased: 1.813
batch start
#iterations: 286
currently lose_sum: 494.8868511915207
time_elpased: 1.791
batch start
#iterations: 287
currently lose_sum: 492.893412232399
time_elpased: 1.788
batch start
#iterations: 288
currently lose_sum: 493.61690351366997
time_elpased: 1.786
batch start
#iterations: 289
currently lose_sum: 492.28484788537025
time_elpased: 1.853
batch start
#iterations: 290
currently lose_sum: 491.2692092061043
time_elpased: 1.886
batch start
#iterations: 291
currently lose_sum: 493.1336759328842
time_elpased: 1.838
batch start
#iterations: 292
currently lose_sum: 491.3957860171795
time_elpased: 1.784
batch start
#iterations: 293
currently lose_sum: 491.7382400929928
time_elpased: 1.789
batch start
#iterations: 294
currently lose_sum: 492.34823885560036
time_elpased: 1.859
batch start
#iterations: 295
currently lose_sum: 492.5096243619919
time_elpased: 1.79
batch start
#iterations: 296
currently lose_sum: 490.62002363801
time_elpased: 1.777
batch start
#iterations: 297
currently lose_sum: 492.2684465646744
time_elpased: 1.798
batch start
#iterations: 298
currently lose_sum: 490.4786189198494
time_elpased: 1.79
batch start
#iterations: 299
currently lose_sum: 491.7554953992367
time_elpased: 1.842
start validation test
0.308453608247
1.0
0.308453608247
0.471478096439
0
validation finish
batch start
#iterations: 300
currently lose_sum: 491.3035834431648
time_elpased: 1.829
batch start
#iterations: 301
currently lose_sum: 491.05147445201874
time_elpased: 1.871
batch start
#iterations: 302
currently lose_sum: 494.865795314312
time_elpased: 1.804
batch start
#iterations: 303
currently lose_sum: 494.08015418052673
time_elpased: 1.784
batch start
#iterations: 304
currently lose_sum: 488.5557363629341
time_elpased: 1.78
batch start
#iterations: 305
currently lose_sum: 493.0082277059555
time_elpased: 1.782
batch start
#iterations: 306
currently lose_sum: 490.01455822587013
time_elpased: 1.791
batch start
#iterations: 307
currently lose_sum: 491.2749534547329
time_elpased: 1.781
batch start
#iterations: 308
currently lose_sum: 490.5309243798256
time_elpased: 1.809
batch start
#iterations: 309
currently lose_sum: 490.4534237086773
time_elpased: 1.797
batch start
#iterations: 310
currently lose_sum: 491.67641431093216
time_elpased: 1.776
batch start
#iterations: 311
currently lose_sum: 489.5817033648491
time_elpased: 1.792
batch start
#iterations: 312
currently lose_sum: 491.78867211937904
time_elpased: 1.85
batch start
#iterations: 313
currently lose_sum: 493.48541405797005
time_elpased: 1.873
batch start
#iterations: 314
currently lose_sum: 490.7116704881191
time_elpased: 1.794
batch start
#iterations: 315
currently lose_sum: 490.9166249334812
time_elpased: 1.782
batch start
#iterations: 316
currently lose_sum: 492.2142367362976
time_elpased: 1.837
batch start
#iterations: 317
currently lose_sum: 491.7275919318199
time_elpased: 1.786
batch start
#iterations: 318
currently lose_sum: 493.8369168937206
time_elpased: 1.792
batch start
#iterations: 319
currently lose_sum: 492.01712185144424
time_elpased: 1.783
start validation test
0.242886597938
1.0
0.242886597938
0.390842733908
0
validation finish
batch start
#iterations: 320
currently lose_sum: 492.89250868558884
time_elpased: 1.82
batch start
#iterations: 321
currently lose_sum: 493.4922999739647
time_elpased: 1.865
batch start
#iterations: 322
currently lose_sum: 491.08619317412376
time_elpased: 1.795
batch start
#iterations: 323
currently lose_sum: 489.78171825408936
time_elpased: 1.791
batch start
#iterations: 324
currently lose_sum: 492.52813202142715
time_elpased: 1.878
batch start
#iterations: 325
currently lose_sum: 490.2561079263687
time_elpased: 1.789
batch start
#iterations: 326
currently lose_sum: 489.1880765259266
time_elpased: 1.791
batch start
#iterations: 327
currently lose_sum: 491.9362013041973
time_elpased: 1.861
batch start
#iterations: 328
currently lose_sum: 492.0625894665718
time_elpased: 1.834
batch start
#iterations: 329
currently lose_sum: 491.6630610227585
time_elpased: 1.896
batch start
#iterations: 330
currently lose_sum: 494.1255579292774
time_elpased: 1.791
batch start
#iterations: 331
currently lose_sum: 493.3979000747204
time_elpased: 1.785
batch start
#iterations: 332
currently lose_sum: 491.75645542144775
time_elpased: 1.788
batch start
#iterations: 333
currently lose_sum: 490.79012659192085
time_elpased: 1.855
batch start
#iterations: 334
currently lose_sum: 489.5968716740608
time_elpased: 1.821
batch start
#iterations: 335
currently lose_sum: 489.9986583292484
time_elpased: 1.843
batch start
#iterations: 336
currently lose_sum: 493.20600640773773
time_elpased: 1.871
batch start
#iterations: 337
currently lose_sum: 490.88154143095016
time_elpased: 1.872
batch start
#iterations: 338
currently lose_sum: 491.70779901742935
time_elpased: 1.796
batch start
#iterations: 339
currently lose_sum: 491.2818808555603
time_elpased: 1.797
start validation test
0.279793814433
1.0
0.279793814433
0.437248268084
0
validation finish
batch start
#iterations: 340
currently lose_sum: 493.6736043393612
time_elpased: 1.796
batch start
#iterations: 341
currently lose_sum: 491.60234665870667
time_elpased: 1.877
batch start
#iterations: 342
currently lose_sum: 494.0834181904793
time_elpased: 1.801
batch start
#iterations: 343
currently lose_sum: 493.11009269952774
time_elpased: 1.897
batch start
#iterations: 344
currently lose_sum: 490.51478123664856
time_elpased: 1.819
batch start
#iterations: 345
currently lose_sum: 490.4703647494316
time_elpased: 1.801
batch start
#iterations: 346
currently lose_sum: 489.91452836990356
time_elpased: 1.855
batch start
#iterations: 347
currently lose_sum: 489.9626520872116
time_elpased: 1.86
batch start
#iterations: 348
currently lose_sum: 491.94083923101425
time_elpased: 1.801
batch start
#iterations: 349
currently lose_sum: 492.0342279970646
time_elpased: 1.824
batch start
#iterations: 350
currently lose_sum: 492.6126337945461
time_elpased: 1.832
batch start
#iterations: 351
currently lose_sum: 489.99604296684265
time_elpased: 1.872
batch start
#iterations: 352
currently lose_sum: 491.69585382938385
time_elpased: 1.806
batch start
#iterations: 353
currently lose_sum: 491.50738102197647
time_elpased: 1.802
batch start
#iterations: 354
currently lose_sum: 491.41397300362587
time_elpased: 1.799
batch start
#iterations: 355
currently lose_sum: 492.5020199418068
time_elpased: 1.774
batch start
#iterations: 356
currently lose_sum: 492.3773983120918
time_elpased: 1.867
batch start
#iterations: 357
currently lose_sum: 492.01194778084755
time_elpased: 1.784
batch start
#iterations: 358
currently lose_sum: 492.19378370046616
time_elpased: 1.813
batch start
#iterations: 359
currently lose_sum: 491.4778565466404
time_elpased: 1.876
start validation test
0.216082474227
1.0
0.216082474227
0.355374703289
0
validation finish
batch start
#iterations: 360
currently lose_sum: 493.00124630331993
time_elpased: 1.806
batch start
#iterations: 361
currently lose_sum: 489.00039449334145
time_elpased: 1.818
batch start
#iterations: 362
currently lose_sum: 489.76619213819504
time_elpased: 1.798
batch start
#iterations: 363
currently lose_sum: 490.87878715991974
time_elpased: 1.866
batch start
#iterations: 364
currently lose_sum: 492.95062163472176
time_elpased: 1.799
batch start
#iterations: 365
currently lose_sum: 491.5950922369957
time_elpased: 1.804
batch start
#iterations: 366
currently lose_sum: 492.14925172924995
time_elpased: 1.878
batch start
#iterations: 367
currently lose_sum: 493.13075518608093
time_elpased: 1.795
batch start
#iterations: 368
currently lose_sum: 488.4257610142231
time_elpased: 1.882
batch start
#iterations: 369
currently lose_sum: 491.29818296432495
time_elpased: 1.862
batch start
#iterations: 370
currently lose_sum: 490.30157139897346
time_elpased: 1.794
batch start
#iterations: 371
currently lose_sum: 491.39172008633614
time_elpased: 1.854
batch start
#iterations: 372
currently lose_sum: 492.0360201597214
time_elpased: 1.816
batch start
#iterations: 373
currently lose_sum: 491.56283301115036
time_elpased: 1.829
batch start
#iterations: 374
currently lose_sum: 493.0079616308212
time_elpased: 1.819
batch start
#iterations: 375
currently lose_sum: 493.81005001068115
time_elpased: 1.811
batch start
#iterations: 376
currently lose_sum: 491.80448028445244
time_elpased: 1.814
batch start
#iterations: 377
currently lose_sum: 489.73384431004524
time_elpased: 1.801
batch start
#iterations: 378
currently lose_sum: 490.53734546899796
time_elpased: 1.795
batch start
#iterations: 379
currently lose_sum: 489.24325954914093
time_elpased: 1.805
start validation test
0.247319587629
1.0
0.247319587629
0.396561699314
0
validation finish
batch start
#iterations: 380
currently lose_sum: 492.01961252093315
time_elpased: 1.813
batch start
#iterations: 381
currently lose_sum: 489.93736150860786
time_elpased: 1.788
batch start
#iterations: 382
currently lose_sum: 492.6457564532757
time_elpased: 1.865
batch start
#iterations: 383
currently lose_sum: 488.9556922316551
time_elpased: 1.798
batch start
#iterations: 384
currently lose_sum: 489.4258654117584
time_elpased: 1.852
batch start
#iterations: 385
currently lose_sum: 490.6158677339554
time_elpased: 1.791
batch start
#iterations: 386
currently lose_sum: 490.6922411620617
time_elpased: 1.864
batch start
#iterations: 387
currently lose_sum: 490.3649107515812
time_elpased: 1.807
batch start
#iterations: 388
currently lose_sum: 491.8247571885586
time_elpased: 1.83
batch start
#iterations: 389
currently lose_sum: 491.37887647747993
time_elpased: 1.796
batch start
#iterations: 390
currently lose_sum: 492.05861496925354
time_elpased: 1.879
batch start
#iterations: 391
currently lose_sum: 492.0744703710079
time_elpased: 1.871
batch start
#iterations: 392
currently lose_sum: 491.02540799975395
time_elpased: 1.875
batch start
#iterations: 393
currently lose_sum: 490.40545296669006
time_elpased: 1.79
batch start
#iterations: 394
currently lose_sum: 492.1411304473877
time_elpased: 1.786
batch start
#iterations: 395
currently lose_sum: 490.1605159640312
time_elpased: 1.915
batch start
#iterations: 396
currently lose_sum: 492.4081139266491
time_elpased: 1.794
batch start
#iterations: 397
currently lose_sum: 491.74277153611183
time_elpased: 1.79
batch start
#iterations: 398
currently lose_sum: 490.66639387607574
time_elpased: 1.876
batch start
#iterations: 399
currently lose_sum: 491.2965984940529
time_elpased: 1.823
start validation test
0.200412371134
1.0
0.200412371134
0.33390587427
0
validation finish
acc: 0.302
pre: 1.000
rec: 0.302
F1: 0.464
auc: 0.000
