start to construct graph
graph construct over
58300
epochs start
batch start
#iterations: 0
currently lose_sum: 401.06119179725647
time_elpased: 1.115
batch start
#iterations: 1
currently lose_sum: 396.40874856710434
time_elpased: 1.083
batch start
#iterations: 2
currently lose_sum: 391.74162089824677
time_elpased: 1.069
batch start
#iterations: 3
currently lose_sum: 389.66321992874146
time_elpased: 1.079
batch start
#iterations: 4
currently lose_sum: 386.9554916024208
time_elpased: 1.103
batch start
#iterations: 5
currently lose_sum: 385.2885062098503
time_elpased: 1.097
batch start
#iterations: 6
currently lose_sum: 383.724278151989
time_elpased: 1.088
batch start
#iterations: 7
currently lose_sum: 382.47671443223953
time_elpased: 1.088
batch start
#iterations: 8
currently lose_sum: 381.58079290390015
time_elpased: 1.081
batch start
#iterations: 9
currently lose_sum: 382.1435850262642
time_elpased: 1.095
batch start
#iterations: 10
currently lose_sum: 377.781677544117
time_elpased: 1.082
batch start
#iterations: 11
currently lose_sum: 378.24593687057495
time_elpased: 1.085
batch start
#iterations: 12
currently lose_sum: 378.05245065689087
time_elpased: 1.096
batch start
#iterations: 13
currently lose_sum: 377.95261961221695
time_elpased: 1.078
batch start
#iterations: 14
currently lose_sum: 373.7933576107025
time_elpased: 1.077
batch start
#iterations: 15
currently lose_sum: 376.3176871538162
time_elpased: 1.097
batch start
#iterations: 16
currently lose_sum: 374.6796222925186
time_elpased: 1.077
batch start
#iterations: 17
currently lose_sum: 375.7455313205719
time_elpased: 1.069
batch start
#iterations: 18
currently lose_sum: 372.83793675899506
time_elpased: 1.085
batch start
#iterations: 19
currently lose_sum: 374.5089725255966
time_elpased: 1.112
start validation test
0.452886597938
1.0
0.452886597938
0.623430071667
0
validation finish
batch start
#iterations: 20
currently lose_sum: 373.1899027824402
time_elpased: 1.081
batch start
#iterations: 21
currently lose_sum: 372.27215898036957
time_elpased: 1.074
batch start
#iterations: 22
currently lose_sum: 373.77176344394684
time_elpased: 1.088
batch start
#iterations: 23
currently lose_sum: 373.99103701114655
time_elpased: 1.101
batch start
#iterations: 24
currently lose_sum: 372.5936138033867
time_elpased: 1.083
batch start
#iterations: 25
currently lose_sum: 371.66039299964905
time_elpased: 1.091
batch start
#iterations: 26
currently lose_sum: 372.5230446457863
time_elpased: 1.103
batch start
#iterations: 27
currently lose_sum: 371.71045833826065
time_elpased: 1.082
batch start
#iterations: 28
currently lose_sum: 370.18144476413727
time_elpased: 1.086
batch start
#iterations: 29
currently lose_sum: 371.3249671459198
time_elpased: 1.083
batch start
#iterations: 30
currently lose_sum: 371.6485410928726
time_elpased: 1.092
batch start
#iterations: 31
currently lose_sum: 371.586740732193
time_elpased: 1.088
batch start
#iterations: 32
currently lose_sum: 370.58478540182114
time_elpased: 1.122
batch start
#iterations: 33
currently lose_sum: 371.4384117126465
time_elpased: 1.145
batch start
#iterations: 34
currently lose_sum: 370.9185380935669
time_elpased: 1.123
batch start
#iterations: 35
currently lose_sum: 371.26517140865326
time_elpased: 1.112
batch start
#iterations: 36
currently lose_sum: 370.1335750222206
time_elpased: 1.108
batch start
#iterations: 37
currently lose_sum: 368.1768247485161
time_elpased: 1.115
batch start
#iterations: 38
currently lose_sum: 370.8147572875023
time_elpased: 1.084
batch start
#iterations: 39
currently lose_sum: 369.76207399368286
time_elpased: 1.072
start validation test
0.757731958763
1.0
0.757731958763
0.862170087977
0
validation finish
batch start
#iterations: 40
currently lose_sum: 369.9825346469879
time_elpased: 1.085
batch start
#iterations: 41
currently lose_sum: 368.6889400482178
time_elpased: 1.087
batch start
#iterations: 42
currently lose_sum: 368.15070700645447
time_elpased: 1.074
batch start
#iterations: 43
currently lose_sum: 369.244747877121
time_elpased: 1.076
batch start
#iterations: 44
currently lose_sum: 369.01151102781296
time_elpased: 1.132
batch start
#iterations: 45
currently lose_sum: 367.8893983364105
time_elpased: 1.087
batch start
#iterations: 46
currently lose_sum: 368.2358825802803
time_elpased: 1.082
batch start
#iterations: 47
currently lose_sum: 368.1377314925194
time_elpased: 1.124
batch start
#iterations: 48
currently lose_sum: 367.0342642068863
time_elpased: 1.144
batch start
#iterations: 49
currently lose_sum: 367.42317736148834
time_elpased: 1.094
batch start
#iterations: 50
currently lose_sum: 367.3487745523453
time_elpased: 1.083
batch start
#iterations: 51
currently lose_sum: 367.5555072426796
time_elpased: 1.086
batch start
#iterations: 52
currently lose_sum: 367.7636277079582
time_elpased: 1.093
batch start
#iterations: 53
currently lose_sum: 367.7267548441887
time_elpased: 1.136
batch start
#iterations: 54
currently lose_sum: 367.9286224246025
time_elpased: 1.086
batch start
#iterations: 55
currently lose_sum: 366.9926801919937
time_elpased: 1.11
batch start
#iterations: 56
currently lose_sum: 367.5724484324455
time_elpased: 1.087
batch start
#iterations: 57
currently lose_sum: 367.5671133995056
time_elpased: 1.121
batch start
#iterations: 58
currently lose_sum: 367.00464248657227
time_elpased: 1.09
batch start
#iterations: 59
currently lose_sum: 368.22007632255554
time_elpased: 1.081
start validation test
0.725360824742
1.0
0.725360824742
0.840822179732
0
validation finish
batch start
#iterations: 60
currently lose_sum: 367.01891499757767
time_elpased: 1.079
batch start
#iterations: 61
currently lose_sum: 366.54809510707855
time_elpased: 1.08
batch start
#iterations: 62
currently lose_sum: 366.1672746539116
time_elpased: 1.087
batch start
#iterations: 63
currently lose_sum: 365.61948919296265
time_elpased: 1.094
batch start
#iterations: 64
currently lose_sum: 367.28161484003067
time_elpased: 1.11
batch start
#iterations: 65
currently lose_sum: 366.0805946588516
time_elpased: 1.088
batch start
#iterations: 66
currently lose_sum: 366.3704659342766
time_elpased: 1.131
batch start
#iterations: 67
currently lose_sum: 366.02668273448944
time_elpased: 1.081
batch start
#iterations: 68
currently lose_sum: 366.46061593294144
time_elpased: 1.071
batch start
#iterations: 69
currently lose_sum: 364.81837767362595
time_elpased: 1.09
batch start
#iterations: 70
currently lose_sum: 365.71228206157684
time_elpased: 1.091
batch start
#iterations: 71
currently lose_sum: 366.07244580984116
time_elpased: 1.13
batch start
#iterations: 72
currently lose_sum: 365.76993495225906
time_elpased: 1.082
batch start
#iterations: 73
currently lose_sum: 364.9568741321564
time_elpased: 1.09
batch start
#iterations: 74
currently lose_sum: 364.17738860845566
time_elpased: 1.129
batch start
#iterations: 75
currently lose_sum: 364.07969349622726
time_elpased: 1.089
batch start
#iterations: 76
currently lose_sum: 364.8961161971092
time_elpased: 1.083
batch start
#iterations: 77
currently lose_sum: 365.7624535560608
time_elpased: 1.152
batch start
#iterations: 78
currently lose_sum: 365.8390410542488
time_elpased: 1.139
batch start
#iterations: 79
currently lose_sum: 364.6955095529556
time_elpased: 1.088
start validation test
0.834742268041
1.0
0.834742268041
0.909928639658
0
validation finish
batch start
#iterations: 80
currently lose_sum: 364.23570489883423
time_elpased: 1.087
batch start
#iterations: 81
currently lose_sum: 366.2384009361267
time_elpased: 1.113
batch start
#iterations: 82
currently lose_sum: 364.9482942223549
time_elpased: 1.087
batch start
#iterations: 83
currently lose_sum: 365.18154072761536
time_elpased: 1.081
batch start
#iterations: 84
currently lose_sum: 366.3100599050522
time_elpased: 1.093
batch start
#iterations: 85
currently lose_sum: 365.50495517253876
time_elpased: 1.099
batch start
#iterations: 86
currently lose_sum: 365.2928215265274
time_elpased: 1.091
batch start
#iterations: 87
currently lose_sum: 364.5174312591553
time_elpased: 1.09
batch start
#iterations: 88
currently lose_sum: 364.13870763778687
time_elpased: 1.082
batch start
#iterations: 89
currently lose_sum: 363.84037059545517
time_elpased: 1.094
batch start
#iterations: 90
currently lose_sum: 365.75702357292175
time_elpased: 1.086
batch start
#iterations: 91
currently lose_sum: 364.1954313516617
time_elpased: 1.105
batch start
#iterations: 92
currently lose_sum: 362.91577672958374
time_elpased: 1.091
batch start
#iterations: 93
currently lose_sum: 365.15328419208527
time_elpased: 1.123
batch start
#iterations: 94
currently lose_sum: 364.7709951996803
time_elpased: 1.094
batch start
#iterations: 95
currently lose_sum: 364.034142434597
time_elpased: 1.122
batch start
#iterations: 96
currently lose_sum: 364.1895515322685
time_elpased: 1.139
batch start
#iterations: 97
currently lose_sum: 364.03494864702225
time_elpased: 1.125
batch start
#iterations: 98
currently lose_sum: 364.4778484106064
time_elpased: 1.092
batch start
#iterations: 99
currently lose_sum: 363.16369515657425
time_elpased: 1.079
start validation test
0.738350515464
1.0
0.738350515464
0.84948404697
0
validation finish
batch start
#iterations: 100
currently lose_sum: 363.7033358812332
time_elpased: 1.116
batch start
#iterations: 101
currently lose_sum: 362.80418622493744
time_elpased: 1.113
batch start
#iterations: 102
currently lose_sum: 363.4638051688671
time_elpased: 1.105
batch start
#iterations: 103
currently lose_sum: 363.0994456410408
time_elpased: 1.107
batch start
#iterations: 104
currently lose_sum: 363.4664595723152
time_elpased: 1.134
batch start
#iterations: 105
currently lose_sum: 363.6942442059517
time_elpased: 1.096
batch start
#iterations: 106
currently lose_sum: 363.9734501838684
time_elpased: 1.088
batch start
#iterations: 107
currently lose_sum: 363.2356885075569
time_elpased: 1.123
batch start
#iterations: 108
currently lose_sum: 363.7203686237335
time_elpased: 1.087
batch start
#iterations: 109
currently lose_sum: 362.64135494828224
time_elpased: 1.083
batch start
#iterations: 110
currently lose_sum: 364.1839950084686
time_elpased: 1.131
batch start
#iterations: 111
currently lose_sum: 362.91900259256363
time_elpased: 1.087
batch start
#iterations: 112
currently lose_sum: 362.99283027648926
time_elpased: 1.105
batch start
#iterations: 113
currently lose_sum: 363.211155295372
time_elpased: 1.116
batch start
#iterations: 114
currently lose_sum: 362.6520958542824
time_elpased: 1.101
batch start
#iterations: 115
currently lose_sum: 362.39055919647217
time_elpased: 1.08
batch start
#iterations: 116
currently lose_sum: 363.0078352689743
time_elpased: 1.108
batch start
#iterations: 117
currently lose_sum: 362.24007922410965
time_elpased: 1.123
batch start
#iterations: 118
currently lose_sum: 361.96425515413284
time_elpased: 1.103
batch start
#iterations: 119
currently lose_sum: 362.23700773715973
time_elpased: 1.085
start validation test
0.814639175258
1.0
0.814639175258
0.897852516759
0
validation finish
batch start
#iterations: 120
currently lose_sum: 362.9849986433983
time_elpased: 1.089
batch start
#iterations: 121
currently lose_sum: 363.0000396370888
time_elpased: 1.096
batch start
#iterations: 122
currently lose_sum: 359.79171735048294
time_elpased: 1.086
batch start
#iterations: 123
currently lose_sum: 362.29847037792206
time_elpased: 1.136
batch start
#iterations: 124
currently lose_sum: 361.354271709919
time_elpased: 1.119
batch start
#iterations: 125
currently lose_sum: 362.79400140047073
time_elpased: 1.149
batch start
#iterations: 126
currently lose_sum: 363.5524435043335
time_elpased: 1.118
batch start
#iterations: 127
currently lose_sum: 362.47398138046265
time_elpased: 1.09
batch start
#iterations: 128
currently lose_sum: 361.9154728651047
time_elpased: 1.124
batch start
#iterations: 129
currently lose_sum: 363.1805222630501
time_elpased: 1.076
batch start
#iterations: 130
currently lose_sum: 362.17429798841476
time_elpased: 1.096
batch start
#iterations: 131
currently lose_sum: 361.4519325494766
time_elpased: 1.104
batch start
#iterations: 132
currently lose_sum: 362.3087106347084
time_elpased: 1.134
batch start
#iterations: 133
currently lose_sum: 362.0607903599739
time_elpased: 1.081
batch start
#iterations: 134
currently lose_sum: 362.06812942028046
time_elpased: 1.144
batch start
#iterations: 135
currently lose_sum: 362.2028524875641
time_elpased: 1.087
batch start
#iterations: 136
currently lose_sum: 363.7192741036415
time_elpased: 1.119
batch start
#iterations: 137
currently lose_sum: 361.57765144109726
time_elpased: 1.081
batch start
#iterations: 138
currently lose_sum: 363.23534417152405
time_elpased: 1.088
batch start
#iterations: 139
currently lose_sum: 361.45594877004623
time_elpased: 1.114
start validation test
0.807835051546
1.0
0.807835051546
0.893704379562
0
validation finish
batch start
#iterations: 140
currently lose_sum: 362.49191802740097
time_elpased: 1.083
batch start
#iterations: 141
currently lose_sum: 363.2763392329216
time_elpased: 1.117
batch start
#iterations: 142
currently lose_sum: 362.8045675754547
time_elpased: 1.093
batch start
#iterations: 143
currently lose_sum: 362.2853717803955
time_elpased: 1.1
batch start
#iterations: 144
currently lose_sum: 362.2121676802635
time_elpased: 1.076
batch start
#iterations: 145
currently lose_sum: 361.9920241832733
time_elpased: 1.095
batch start
#iterations: 146
currently lose_sum: 360.869218736887
time_elpased: 1.094
batch start
#iterations: 147
currently lose_sum: 361.6377871632576
time_elpased: 1.099
batch start
#iterations: 148
currently lose_sum: 361.5509629845619
time_elpased: 1.092
batch start
#iterations: 149
currently lose_sum: 362.2332750558853
time_elpased: 1.095
batch start
#iterations: 150
currently lose_sum: 361.64427947998047
time_elpased: 1.088
batch start
#iterations: 151
currently lose_sum: 361.2626923918724
time_elpased: 1.105
batch start
#iterations: 152
currently lose_sum: 360.48588448762894
time_elpased: 1.094
batch start
#iterations: 153
currently lose_sum: 363.4402083158493
time_elpased: 1.084
batch start
#iterations: 154
currently lose_sum: 361.38510715961456
time_elpased: 1.083
batch start
#iterations: 155
currently lose_sum: 362.01618015766144
time_elpased: 1.081
batch start
#iterations: 156
currently lose_sum: 361.2891919016838
time_elpased: 1.13
batch start
#iterations: 157
currently lose_sum: 360.9206072688103
time_elpased: 1.094
batch start
#iterations: 158
currently lose_sum: 361.4086195230484
time_elpased: 1.089
batch start
#iterations: 159
currently lose_sum: 363.17444932460785
time_elpased: 1.096
start validation test
0.779381443299
1.0
0.779381443299
0.876013904983
0
validation finish
batch start
#iterations: 160
currently lose_sum: 361.52523308992386
time_elpased: 1.094
batch start
#iterations: 161
currently lose_sum: 360.87406265735626
time_elpased: 1.105
batch start
#iterations: 162
currently lose_sum: 361.434109210968
time_elpased: 1.093
batch start
#iterations: 163
currently lose_sum: 361.74085772037506
time_elpased: 1.158
batch start
#iterations: 164
currently lose_sum: 360.710800409317
time_elpased: 1.094
batch start
#iterations: 165
currently lose_sum: 359.2862076163292
time_elpased: 1.095
batch start
#iterations: 166
currently lose_sum: 360.7073096036911
time_elpased: 1.086
batch start
#iterations: 167
currently lose_sum: 361.6304569244385
time_elpased: 1.098
batch start
#iterations: 168
currently lose_sum: 360.72025805711746
time_elpased: 1.099
batch start
#iterations: 169
currently lose_sum: 360.2178587317467
time_elpased: 1.083
batch start
#iterations: 170
currently lose_sum: 362.0998021364212
time_elpased: 1.145
batch start
#iterations: 171
currently lose_sum: 360.40086936950684
time_elpased: 1.14
batch start
#iterations: 172
currently lose_sum: 360.1966535449028
time_elpased: 1.093
batch start
#iterations: 173
currently lose_sum: 361.6768134832382
time_elpased: 1.117
batch start
#iterations: 174
currently lose_sum: 359.72126269340515
time_elpased: 1.091
batch start
#iterations: 175
currently lose_sum: 361.8952949643135
time_elpased: 1.11
batch start
#iterations: 176
currently lose_sum: 361.2244890332222
time_elpased: 1.086
batch start
#iterations: 177
currently lose_sum: 360.0789084434509
time_elpased: 1.096
batch start
#iterations: 178
currently lose_sum: 361.50566840171814
time_elpased: 1.1
batch start
#iterations: 179
currently lose_sum: 361.48458057641983
time_elpased: 1.089
start validation test
0.776804123711
1.0
0.776804123711
0.874383521903
0
validation finish
batch start
#iterations: 180
currently lose_sum: 361.3981699049473
time_elpased: 1.087
batch start
#iterations: 181
currently lose_sum: 359.2026135921478
time_elpased: 1.101
batch start
#iterations: 182
currently lose_sum: 360.3802042603493
time_elpased: 1.091
batch start
#iterations: 183
currently lose_sum: 361.2254242300987
time_elpased: 1.093
batch start
#iterations: 184
currently lose_sum: 360.2488394975662
time_elpased: 1.087
batch start
#iterations: 185
currently lose_sum: 360.4079557657242
time_elpased: 1.075
batch start
#iterations: 186
currently lose_sum: 360.3985534310341
time_elpased: 1.083
batch start
#iterations: 187
currently lose_sum: 359.76250845193863
time_elpased: 1.092
batch start
#iterations: 188
currently lose_sum: 361.09641522169113
time_elpased: 1.12
batch start
#iterations: 189
currently lose_sum: 360.9405082464218
time_elpased: 1.105
batch start
#iterations: 190
currently lose_sum: 359.35326927900314
time_elpased: 1.085
batch start
#iterations: 191
currently lose_sum: 360.0722820162773
time_elpased: 1.085
batch start
#iterations: 192
currently lose_sum: 361.4414584636688
time_elpased: 1.117
batch start
#iterations: 193
currently lose_sum: 361.0146438777447
time_elpased: 1.09
batch start
#iterations: 194
currently lose_sum: 359.9983539581299
time_elpased: 1.087
batch start
#iterations: 195
currently lose_sum: 360.4520651102066
time_elpased: 1.147
batch start
#iterations: 196
currently lose_sum: 362.08662766218185
time_elpased: 1.13
batch start
#iterations: 197
currently lose_sum: 360.62048506736755
time_elpased: 1.112
batch start
#iterations: 198
currently lose_sum: 360.75342363119125
time_elpased: 1.081
batch start
#iterations: 199
currently lose_sum: 360.3856585621834
time_elpased: 1.124
start validation test
0.788969072165
1.0
0.788969072165
0.882037688008
0
validation finish
batch start
#iterations: 200
currently lose_sum: 360.8867258429527
time_elpased: 1.082
batch start
#iterations: 201
currently lose_sum: 358.98308062553406
time_elpased: 1.115
batch start
#iterations: 202
currently lose_sum: 359.0869147181511
time_elpased: 1.112
batch start
#iterations: 203
currently lose_sum: 358.09338265657425
time_elpased: 1.112
batch start
#iterations: 204
currently lose_sum: 361.0961063504219
time_elpased: 1.081
batch start
#iterations: 205
currently lose_sum: 358.98443508148193
time_elpased: 1.082
batch start
#iterations: 206
currently lose_sum: 361.13431161642075
time_elpased: 1.098
batch start
#iterations: 207
currently lose_sum: 362.01146906614304
time_elpased: 1.1
batch start
#iterations: 208
currently lose_sum: 360.19996160268784
time_elpased: 1.088
batch start
#iterations: 209
currently lose_sum: 360.00045335292816
time_elpased: 1.083
batch start
#iterations: 210
currently lose_sum: 359.32720971107483
time_elpased: 1.085
batch start
#iterations: 211
currently lose_sum: 359.9131393432617
time_elpased: 1.121
batch start
#iterations: 212
currently lose_sum: 360.1407464146614
time_elpased: 1.142
batch start
#iterations: 213
currently lose_sum: 360.4071405529976
time_elpased: 1.107
batch start
#iterations: 214
currently lose_sum: 360.471356511116
time_elpased: 1.088
batch start
#iterations: 215
currently lose_sum: 359.35012090206146
time_elpased: 1.085
batch start
#iterations: 216
currently lose_sum: 360.61914122104645
time_elpased: 1.129
batch start
#iterations: 217
currently lose_sum: 360.1032045483589
time_elpased: 1.095
batch start
#iterations: 218
currently lose_sum: 360.176804959774
time_elpased: 1.08
batch start
#iterations: 219
currently lose_sum: 360.60356467962265
time_elpased: 1.097
start validation test
0.788350515464
1.0
0.788350515464
0.881651005938
0
validation finish
batch start
#iterations: 220
currently lose_sum: 359.0169422030449
time_elpased: 1.089
batch start
#iterations: 221
currently lose_sum: 359.6837502717972
time_elpased: 1.14
batch start
#iterations: 222
currently lose_sum: 360.04973143339157
time_elpased: 1.095
batch start
#iterations: 223
currently lose_sum: 361.1619384288788
time_elpased: 1.087
batch start
#iterations: 224
currently lose_sum: 360.69532376527786
time_elpased: 1.085
batch start
#iterations: 225
currently lose_sum: 359.46166402101517
time_elpased: 1.109
batch start
#iterations: 226
currently lose_sum: 359.2788522839546
time_elpased: 1.083
batch start
#iterations: 227
currently lose_sum: 360.3313997387886
time_elpased: 1.13
batch start
#iterations: 228
currently lose_sum: 359.92137908935547
time_elpased: 1.123
batch start
#iterations: 229
currently lose_sum: 359.45655661821365
time_elpased: 1.091
batch start
#iterations: 230
currently lose_sum: 359.70026671886444
time_elpased: 1.079
batch start
#iterations: 231
currently lose_sum: 360.11278998851776
time_elpased: 1.129
batch start
#iterations: 232
currently lose_sum: 360.358078122139
time_elpased: 1.095
batch start
#iterations: 233
currently lose_sum: 359.6770969033241
time_elpased: 1.143
batch start
#iterations: 234
currently lose_sum: 360.3032275438309
time_elpased: 1.096
batch start
#iterations: 235
currently lose_sum: 358.42847460508347
time_elpased: 1.104
batch start
#iterations: 236
currently lose_sum: 359.5018702149391
time_elpased: 1.094
batch start
#iterations: 237
currently lose_sum: 359.686377286911
time_elpased: 1.131
batch start
#iterations: 238
currently lose_sum: 359.99921131134033
time_elpased: 1.098
batch start
#iterations: 239
currently lose_sum: 357.96644753217697
time_elpased: 1.095
start validation test
0.751443298969
1.0
0.751443298969
0.858084643004
0
validation finish
batch start
#iterations: 240
currently lose_sum: 358.29394978284836
time_elpased: 1.114
batch start
#iterations: 241
currently lose_sum: 358.77077329158783
time_elpased: 1.083
batch start
#iterations: 242
currently lose_sum: 357.76527988910675
time_elpased: 1.128
batch start
#iterations: 243
currently lose_sum: 359.42045772075653
time_elpased: 1.135
batch start
#iterations: 244
currently lose_sum: 360.0164845585823
time_elpased: 1.14
batch start
#iterations: 245
currently lose_sum: 358.8251240849495
time_elpased: 1.145
batch start
#iterations: 246
currently lose_sum: 359.2881555557251
time_elpased: 1.137
batch start
#iterations: 247
currently lose_sum: 360.05466413497925
time_elpased: 1.127
batch start
#iterations: 248
currently lose_sum: 360.9786745905876
time_elpased: 1.082
batch start
#iterations: 249
currently lose_sum: 358.0417000055313
time_elpased: 1.087
batch start
#iterations: 250
currently lose_sum: 360.2642796635628
time_elpased: 1.124
batch start
#iterations: 251
currently lose_sum: 360.58937007188797
time_elpased: 1.123
batch start
#iterations: 252
currently lose_sum: 358.92215996980667
time_elpased: 1.089
batch start
#iterations: 253
currently lose_sum: 357.8164775967598
time_elpased: 1.107
batch start
#iterations: 254
currently lose_sum: 359.36215621232986
time_elpased: 1.085
batch start
#iterations: 255
currently lose_sum: 359.5132225751877
time_elpased: 1.15
batch start
#iterations: 256
currently lose_sum: 359.45012521743774
time_elpased: 1.14
batch start
#iterations: 257
currently lose_sum: 360.2184081673622
time_elpased: 1.086
batch start
#iterations: 258
currently lose_sum: 360.6889334321022
time_elpased: 1.105
batch start
#iterations: 259
currently lose_sum: 358.5070629119873
time_elpased: 1.113
start validation test
0.774020618557
1.0
0.774020618557
0.872617387262
0
validation finish
batch start
#iterations: 260
currently lose_sum: 358.79957914352417
time_elpased: 1.086
batch start
#iterations: 261
currently lose_sum: 358.2243010997772
time_elpased: 1.09
batch start
#iterations: 262
currently lose_sum: 359.70756244659424
time_elpased: 1.094
batch start
#iterations: 263
currently lose_sum: 360.6884008049965
time_elpased: 1.093
batch start
#iterations: 264
currently lose_sum: 359.26343709230423
time_elpased: 1.088
batch start
#iterations: 265
currently lose_sum: 358.7342289686203
time_elpased: 1.087
batch start
#iterations: 266
currently lose_sum: 359.5567710995674
time_elpased: 1.096
batch start
#iterations: 267
currently lose_sum: 359.04239958524704
time_elpased: 1.088
batch start
#iterations: 268
currently lose_sum: 359.3306814432144
time_elpased: 1.142
batch start
#iterations: 269
currently lose_sum: 360.3867991566658
time_elpased: 1.08
batch start
#iterations: 270
currently lose_sum: 358.0280339717865
time_elpased: 1.102
batch start
#iterations: 271
currently lose_sum: 358.59124529361725
time_elpased: 1.129
batch start
#iterations: 272
currently lose_sum: 358.70024037361145
time_elpased: 1.108
batch start
#iterations: 273
currently lose_sum: 359.0231523513794
time_elpased: 1.124
batch start
#iterations: 274
currently lose_sum: 359.88557386398315
time_elpased: 1.122
batch start
#iterations: 275
currently lose_sum: 359.28248903155327
time_elpased: 1.091
batch start
#iterations: 276
currently lose_sum: 357.76403391361237
time_elpased: 1.135
batch start
#iterations: 277
currently lose_sum: 359.29502856731415
time_elpased: 1.101
batch start
#iterations: 278
currently lose_sum: 358.7772690951824
time_elpased: 1.123
batch start
#iterations: 279
currently lose_sum: 358.2269821166992
time_elpased: 1.089
start validation test
0.792268041237
1.0
0.792268041237
0.884095484613
0
validation finish
batch start
#iterations: 280
currently lose_sum: 358.56792372465134
time_elpased: 1.115
batch start
#iterations: 281
currently lose_sum: 360.558079123497
time_elpased: 1.092
batch start
#iterations: 282
currently lose_sum: 358.8816679120064
time_elpased: 1.104
batch start
#iterations: 283
currently lose_sum: 358.05555361509323
time_elpased: 1.145
batch start
#iterations: 284
currently lose_sum: 359.4122608304024
time_elpased: 1.078
batch start
#iterations: 285
currently lose_sum: 356.7617746889591
time_elpased: 1.095
batch start
#iterations: 286
currently lose_sum: 358.3726846575737
time_elpased: 1.092
batch start
#iterations: 287
currently lose_sum: 357.7458328604698
time_elpased: 1.109
batch start
#iterations: 288
currently lose_sum: 359.0350976586342
time_elpased: 1.093
batch start
#iterations: 289
currently lose_sum: 358.303477704525
time_elpased: 1.111
batch start
#iterations: 290
currently lose_sum: 358.0541949272156
time_elpased: 1.095
batch start
#iterations: 291
currently lose_sum: 358.5760161280632
time_elpased: 1.124
batch start
#iterations: 292
currently lose_sum: 359.15663677453995
time_elpased: 1.116
batch start
#iterations: 293
currently lose_sum: 359.0654929280281
time_elpased: 1.084
batch start
#iterations: 294
currently lose_sum: 358.09787303209305
time_elpased: 1.085
batch start
#iterations: 295
currently lose_sum: 358.48742043972015
time_elpased: 1.079
batch start
#iterations: 296
currently lose_sum: 357.4592300057411
time_elpased: 1.096
batch start
#iterations: 297
currently lose_sum: 358.03492641448975
time_elpased: 1.118
batch start
#iterations: 298
currently lose_sum: 357.0543628334999
time_elpased: 1.09
batch start
#iterations: 299
currently lose_sum: 357.39300352334976
time_elpased: 1.142
start validation test
0.771546391753
1.0
0.771546391753
0.87104283054
0
validation finish
batch start
#iterations: 300
currently lose_sum: 358.2237892150879
time_elpased: 1.139
batch start
#iterations: 301
currently lose_sum: 358.9754201769829
time_elpased: 1.097
batch start
#iterations: 302
currently lose_sum: 360.1094604730606
time_elpased: 1.126
batch start
#iterations: 303
currently lose_sum: 359.7784522175789
time_elpased: 1.094
batch start
#iterations: 304
currently lose_sum: 359.4629687666893
time_elpased: 1.11
batch start
#iterations: 305
currently lose_sum: 358.42485654354095
time_elpased: 1.116
batch start
#iterations: 306
currently lose_sum: 357.0936357975006
time_elpased: 1.098
batch start
#iterations: 307
currently lose_sum: 357.8511465191841
time_elpased: 1.146
batch start
#iterations: 308
currently lose_sum: 357.3246411085129
time_elpased: 1.09
batch start
#iterations: 309
currently lose_sum: 358.42593240737915
time_elpased: 1.162
batch start
#iterations: 310
currently lose_sum: 357.100641310215
time_elpased: 1.143
batch start
#iterations: 311
currently lose_sum: 359.3057821393013
time_elpased: 1.113
batch start
#iterations: 312
currently lose_sum: 355.6807106733322
time_elpased: 1.103
batch start
#iterations: 313
currently lose_sum: 357.643689930439
time_elpased: 1.085
batch start
#iterations: 314
currently lose_sum: 359.4547933936119
time_elpased: 1.094
batch start
#iterations: 315
currently lose_sum: 359.0341343283653
time_elpased: 1.142
batch start
#iterations: 316
currently lose_sum: 357.1476329565048
time_elpased: 1.106
batch start
#iterations: 317
currently lose_sum: 359.9777751863003
time_elpased: 1.107
batch start
#iterations: 318
currently lose_sum: 359.17142555117607
time_elpased: 1.092
batch start
#iterations: 319
currently lose_sum: 358.1284437775612
time_elpased: 1.09
start validation test
0.784020618557
1.0
0.784020618557
0.87893672349
0
validation finish
batch start
#iterations: 320
currently lose_sum: 357.42321848869324
time_elpased: 1.129
batch start
#iterations: 321
currently lose_sum: 358.99828189611435
time_elpased: 1.124
batch start
#iterations: 322
currently lose_sum: 358.451904296875
time_elpased: 1.091
batch start
#iterations: 323
currently lose_sum: 359.7621066570282
time_elpased: 1.111
batch start
#iterations: 324
currently lose_sum: 359.0212312936783
time_elpased: 1.093
batch start
#iterations: 325
currently lose_sum: 358.5807073712349
time_elpased: 1.143
batch start
#iterations: 326
currently lose_sum: 358.47812482714653
time_elpased: 1.136
batch start
#iterations: 327
currently lose_sum: 358.0461420416832
time_elpased: 1.137
batch start
#iterations: 328
currently lose_sum: 357.2673154771328
time_elpased: 1.098
batch start
#iterations: 329
currently lose_sum: 357.4657408595085
time_elpased: 1.131
batch start
#iterations: 330
currently lose_sum: 358.948968231678
time_elpased: 1.09
batch start
#iterations: 331
currently lose_sum: 357.3542200922966
time_elpased: 1.141
batch start
#iterations: 332
currently lose_sum: 358.4080283641815
time_elpased: 1.091
batch start
#iterations: 333
currently lose_sum: 358.1399747133255
time_elpased: 1.143
batch start
#iterations: 334
currently lose_sum: 357.95912981033325
time_elpased: 1.099
batch start
#iterations: 335
currently lose_sum: 357.7338285446167
time_elpased: 1.083
batch start
#iterations: 336
currently lose_sum: 359.0616402029991
time_elpased: 1.149
batch start
#iterations: 337
currently lose_sum: 356.50684344768524
time_elpased: 1.086
batch start
#iterations: 338
currently lose_sum: 358.78405326604843
time_elpased: 1.086
batch start
#iterations: 339
currently lose_sum: 358.0644541978836
time_elpased: 1.11
start validation test
0.771237113402
1.0
0.771237113402
0.870845701647
0
validation finish
batch start
#iterations: 340
currently lose_sum: 357.779269695282
time_elpased: 1.091
batch start
#iterations: 341
currently lose_sum: 359.41890889406204
time_elpased: 1.092
batch start
#iterations: 342
currently lose_sum: 357.3241165280342
time_elpased: 1.095
batch start
#iterations: 343
currently lose_sum: 357.2742291688919
time_elpased: 1.151
batch start
#iterations: 344
currently lose_sum: 356.7068940401077
time_elpased: 1.094
batch start
#iterations: 345
currently lose_sum: 357.80911749601364
time_elpased: 1.13
batch start
#iterations: 346
currently lose_sum: 357.9743056297302
time_elpased: 1.093
batch start
#iterations: 347
currently lose_sum: 358.4072037935257
time_elpased: 1.145
batch start
#iterations: 348
currently lose_sum: 356.2021887898445
time_elpased: 1.116
batch start
#iterations: 349
currently lose_sum: 357.47526663541794
time_elpased: 1.091
batch start
#iterations: 350
currently lose_sum: 358.6755311489105
time_elpased: 1.085
batch start
#iterations: 351
currently lose_sum: 358.41632717847824
time_elpased: 1.106
batch start
#iterations: 352
currently lose_sum: 359.3620050549507
time_elpased: 1.086
batch start
#iterations: 353
currently lose_sum: 357.75650346279144
time_elpased: 1.09
batch start
#iterations: 354
currently lose_sum: 356.8017485141754
time_elpased: 1.091
batch start
#iterations: 355
currently lose_sum: 356.96798872947693
time_elpased: 1.119
batch start
#iterations: 356
currently lose_sum: 358.39090198278427
time_elpased: 1.096
batch start
#iterations: 357
currently lose_sum: 357.69111585617065
time_elpased: 1.102
batch start
#iterations: 358
currently lose_sum: 356.9973375797272
time_elpased: 1.095
batch start
#iterations: 359
currently lose_sum: 356.3560227751732
time_elpased: 1.119
start validation test
0.794742268041
1.0
0.794742268041
0.88563386754
0
validation finish
batch start
#iterations: 360
currently lose_sum: 358.70694518089294
time_elpased: 1.106
batch start
#iterations: 361
currently lose_sum: 357.68747597932816
time_elpased: 1.124
batch start
#iterations: 362
currently lose_sum: 356.5127051770687
time_elpased: 1.133
batch start
#iterations: 363
currently lose_sum: 359.1560785770416
time_elpased: 1.087
batch start
#iterations: 364
currently lose_sum: 356.4635469317436
time_elpased: 1.094
batch start
#iterations: 365
currently lose_sum: 357.49772572517395
time_elpased: 1.095
batch start
#iterations: 366
currently lose_sum: 358.20799773931503
time_elpased: 1.131
batch start
#iterations: 367
currently lose_sum: 358.98776096105576
time_elpased: 1.102
batch start
#iterations: 368
currently lose_sum: 357.19212156534195
time_elpased: 1.096
batch start
#iterations: 369
currently lose_sum: 355.82705095410347
time_elpased: 1.13
batch start
#iterations: 370
currently lose_sum: 357.29945451021194
time_elpased: 1.09
batch start
#iterations: 371
currently lose_sum: 358.0872932970524
time_elpased: 1.098
batch start
#iterations: 372
currently lose_sum: 358.3800683617592
time_elpased: 1.103
batch start
#iterations: 373
currently lose_sum: 357.9692044854164
time_elpased: 1.138
batch start
#iterations: 374
currently lose_sum: 357.8244530558586
time_elpased: 1.136
batch start
#iterations: 375
currently lose_sum: 355.8899466395378
time_elpased: 1.102
batch start
#iterations: 376
currently lose_sum: 357.56311506032944
time_elpased: 1.098
batch start
#iterations: 377
currently lose_sum: 358.15701484680176
time_elpased: 1.141
batch start
#iterations: 378
currently lose_sum: 357.5914378166199
time_elpased: 1.105
batch start
#iterations: 379
currently lose_sum: 357.7494105696678
time_elpased: 1.11
start validation test
0.791134020619
1.0
0.791134020619
0.883388972027
0
validation finish
batch start
#iterations: 380
currently lose_sum: 358.30954799056053
time_elpased: 1.095
batch start
#iterations: 381
currently lose_sum: 358.01121616363525
time_elpased: 1.084
batch start
#iterations: 382
currently lose_sum: 357.5001227557659
time_elpased: 1.137
batch start
#iterations: 383
currently lose_sum: 357.7343667149544
time_elpased: 1.09
batch start
#iterations: 384
currently lose_sum: 357.5567260980606
time_elpased: 1.091
batch start
#iterations: 385
currently lose_sum: 357.3907250761986
time_elpased: 1.086
batch start
#iterations: 386
currently lose_sum: 357.7962018251419
time_elpased: 1.095
batch start
#iterations: 387
currently lose_sum: 356.8450090289116
time_elpased: 1.093
batch start
#iterations: 388
currently lose_sum: 357.55217784643173
time_elpased: 1.086
batch start
#iterations: 389
currently lose_sum: 357.0572549700737
time_elpased: 1.089
batch start
#iterations: 390
currently lose_sum: 356.59214466810226
time_elpased: 1.132
batch start
#iterations: 391
currently lose_sum: 356.9066991209984
time_elpased: 1.105
batch start
#iterations: 392
currently lose_sum: 357.4868931174278
time_elpased: 1.09
batch start
#iterations: 393
currently lose_sum: 358.00894114375114
time_elpased: 1.094
batch start
#iterations: 394
currently lose_sum: 358.31070429086685
time_elpased: 1.162
batch start
#iterations: 395
currently lose_sum: 358.3649079799652
time_elpased: 1.09
batch start
#iterations: 396
currently lose_sum: 358.19881331920624
time_elpased: 1.091
batch start
#iterations: 397
currently lose_sum: 356.04888451099396
time_elpased: 1.097
batch start
#iterations: 398
currently lose_sum: 358.5135663151741
time_elpased: 1.092
batch start
#iterations: 399
currently lose_sum: 356.18649300932884
time_elpased: 1.096
start validation test
0.770721649485
1.0
0.770721649485
0.870517000466
0
validation finish
acc: 0.825
pre: 1.000
rec: 0.825
F1: 0.904
auc: 0.000
