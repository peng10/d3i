start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 551.4376641511917
time_elpased: 1.668
batch start
#iterations: 1
currently lose_sum: 542.011013686657
time_elpased: 1.62
batch start
#iterations: 2
currently lose_sum: 538.3784792423248
time_elpased: 1.622
batch start
#iterations: 3
currently lose_sum: 533.2126767039299
time_elpased: 1.617
batch start
#iterations: 4
currently lose_sum: 530.8657973408699
time_elpased: 1.612
batch start
#iterations: 5
currently lose_sum: 528.1095689237118
time_elpased: 1.616
batch start
#iterations: 6
currently lose_sum: 524.1355111896992
time_elpased: 1.64
batch start
#iterations: 7
currently lose_sum: 523.7258943021297
time_elpased: 1.685
batch start
#iterations: 8
currently lose_sum: 522.4162195324898
time_elpased: 1.63
batch start
#iterations: 9
currently lose_sum: 521.4507615864277
time_elpased: 1.622
batch start
#iterations: 10
currently lose_sum: 520.1398333013058
time_elpased: 1.622
batch start
#iterations: 11
currently lose_sum: 521.8558654785156
time_elpased: 1.622
batch start
#iterations: 12
currently lose_sum: 517.4890461564064
time_elpased: 1.685
batch start
#iterations: 13
currently lose_sum: 517.2855212986469
time_elpased: 1.709
batch start
#iterations: 14
currently lose_sum: 516.2118092477322
time_elpased: 1.644
batch start
#iterations: 15
currently lose_sum: 514.8816869854927
time_elpased: 1.61
batch start
#iterations: 16
currently lose_sum: 517.314769089222
time_elpased: 1.621
batch start
#iterations: 17
currently lose_sum: 514.2700306475163
time_elpased: 1.719
batch start
#iterations: 18
currently lose_sum: 513.9044507443905
time_elpased: 1.628
batch start
#iterations: 19
currently lose_sum: 512.0532809197903
time_elpased: 1.652
start validation test
0.126391752577
1.0
0.126391752577
0.2244188175
0
validation finish
batch start
#iterations: 20
currently lose_sum: 513.2509133517742
time_elpased: 1.63
batch start
#iterations: 21
currently lose_sum: 514.7652788460255
time_elpased: 1.704
batch start
#iterations: 22
currently lose_sum: 513.0426290035248
time_elpased: 1.626
batch start
#iterations: 23
currently lose_sum: 511.736741065979
time_elpased: 1.632
batch start
#iterations: 24
currently lose_sum: 510.1850389242172
time_elpased: 1.673
batch start
#iterations: 25
currently lose_sum: 511.78944504261017
time_elpased: 1.628
batch start
#iterations: 26
currently lose_sum: 512.1272819638252
time_elpased: 1.671
batch start
#iterations: 27
currently lose_sum: 511.8296158015728
time_elpased: 1.636
batch start
#iterations: 28
currently lose_sum: 510.54671344161034
time_elpased: 1.641
batch start
#iterations: 29
currently lose_sum: 511.23442524671555
time_elpased: 1.666
batch start
#iterations: 30
currently lose_sum: 509.1693945527077
time_elpased: 1.709
batch start
#iterations: 31
currently lose_sum: 510.368885666132
time_elpased: 1.707
batch start
#iterations: 32
currently lose_sum: 510.81204584240913
time_elpased: 1.642
batch start
#iterations: 33
currently lose_sum: 507.58503752946854
time_elpased: 1.657
batch start
#iterations: 34
currently lose_sum: 509.2165046930313
time_elpased: 1.636
batch start
#iterations: 35
currently lose_sum: 506.45616015791893
time_elpased: 1.638
batch start
#iterations: 36
currently lose_sum: 508.11266243457794
time_elpased: 1.722
batch start
#iterations: 37
currently lose_sum: 507.2919343113899
time_elpased: 1.648
batch start
#iterations: 38
currently lose_sum: 507.9322088062763
time_elpased: 1.649
batch start
#iterations: 39
currently lose_sum: 508.6190031170845
time_elpased: 1.702
start validation test
0.216082474227
1.0
0.216082474227
0.355374703289
0
validation finish
batch start
#iterations: 40
currently lose_sum: 507.2507746517658
time_elpased: 1.667
batch start
#iterations: 41
currently lose_sum: 506.29305869340897
time_elpased: 1.65
batch start
#iterations: 42
currently lose_sum: 505.3817564845085
time_elpased: 1.629
batch start
#iterations: 43
currently lose_sum: 506.402547031641
time_elpased: 1.63
batch start
#iterations: 44
currently lose_sum: 504.82019808888435
time_elpased: 1.695
batch start
#iterations: 45
currently lose_sum: 505.8801316022873
time_elpased: 1.68
batch start
#iterations: 46
currently lose_sum: 504.41315773129463
time_elpased: 1.686
batch start
#iterations: 47
currently lose_sum: 506.9662083387375
time_elpased: 1.644
batch start
#iterations: 48
currently lose_sum: 503.14882734417915
time_elpased: 1.625
batch start
#iterations: 49
currently lose_sum: 504.10507559776306
time_elpased: 1.622
batch start
#iterations: 50
currently lose_sum: 506.453566044569
time_elpased: 1.643
batch start
#iterations: 51
currently lose_sum: 504.87672567367554
time_elpased: 1.636
batch start
#iterations: 52
currently lose_sum: 504.74587923288345
time_elpased: 1.649
batch start
#iterations: 53
currently lose_sum: 505.06162863969803
time_elpased: 1.63
batch start
#iterations: 54
currently lose_sum: 502.9673027396202
time_elpased: 1.635
batch start
#iterations: 55
currently lose_sum: 504.8325951695442
time_elpased: 1.65
batch start
#iterations: 56
currently lose_sum: 504.9958768785
time_elpased: 1.63
batch start
#iterations: 57
currently lose_sum: 503.8603317439556
time_elpased: 1.709
batch start
#iterations: 58
currently lose_sum: 503.08849933743477
time_elpased: 1.702
batch start
#iterations: 59
currently lose_sum: 501.77149042487144
time_elpased: 1.643
start validation test
0.223298969072
1.0
0.223298969072
0.365076689702
0
validation finish
batch start
#iterations: 60
currently lose_sum: 504.2776392400265
time_elpased: 1.684
batch start
#iterations: 61
currently lose_sum: 502.8257375061512
time_elpased: 1.652
batch start
#iterations: 62
currently lose_sum: 505.5743143558502
time_elpased: 1.712
batch start
#iterations: 63
currently lose_sum: 501.9323455989361
time_elpased: 1.642
batch start
#iterations: 64
currently lose_sum: 503.1094100475311
time_elpased: 1.694
batch start
#iterations: 65
currently lose_sum: 503.3486630022526
time_elpased: 1.635
batch start
#iterations: 66
currently lose_sum: 501.4168743789196
time_elpased: 1.625
batch start
#iterations: 67
currently lose_sum: 502.0363089442253
time_elpased: 1.634
batch start
#iterations: 68
currently lose_sum: 504.3365606069565
time_elpased: 1.667
batch start
#iterations: 69
currently lose_sum: 502.60756266117096
time_elpased: 1.638
batch start
#iterations: 70
currently lose_sum: 502.2970440983772
time_elpased: 1.626
batch start
#iterations: 71
currently lose_sum: 502.488140642643
time_elpased: 1.637
batch start
#iterations: 72
currently lose_sum: 503.27005010843277
time_elpased: 1.7
batch start
#iterations: 73
currently lose_sum: 502.05820220708847
time_elpased: 1.632
batch start
#iterations: 74
currently lose_sum: 501.61852395534515
time_elpased: 1.661
batch start
#iterations: 75
currently lose_sum: 501.7491488158703
time_elpased: 1.66
batch start
#iterations: 76
currently lose_sum: 502.41203412413597
time_elpased: 1.651
batch start
#iterations: 77
currently lose_sum: 501.21254765987396
time_elpased: 1.629
batch start
#iterations: 78
currently lose_sum: 500.76231905817986
time_elpased: 1.628
batch start
#iterations: 79
currently lose_sum: 502.54059502482414
time_elpased: 1.699
start validation test
0.252680412371
1.0
0.252680412371
0.403423586536
0
validation finish
batch start
#iterations: 80
currently lose_sum: 500.94335025548935
time_elpased: 1.633
batch start
#iterations: 81
currently lose_sum: 501.4332685768604
time_elpased: 1.631
batch start
#iterations: 82
currently lose_sum: 501.4423907697201
time_elpased: 1.723
batch start
#iterations: 83
currently lose_sum: 501.44474628567696
time_elpased: 1.64
batch start
#iterations: 84
currently lose_sum: 501.39923536777496
time_elpased: 1.633
batch start
#iterations: 85
currently lose_sum: 501.2658045887947
time_elpased: 1.666
batch start
#iterations: 86
currently lose_sum: 501.9091835618019
time_elpased: 1.688
batch start
#iterations: 87
currently lose_sum: 498.911439627409
time_elpased: 1.71
batch start
#iterations: 88
currently lose_sum: 499.5862118303776
time_elpased: 1.625
batch start
#iterations: 89
currently lose_sum: 503.4213578402996
time_elpased: 1.653
batch start
#iterations: 90
currently lose_sum: 500.1016843020916
time_elpased: 1.636
batch start
#iterations: 91
currently lose_sum: 497.6833847761154
time_elpased: 1.642
batch start
#iterations: 92
currently lose_sum: 501.3144327402115
time_elpased: 1.674
batch start
#iterations: 93
currently lose_sum: 500.23115533590317
time_elpased: 1.633
batch start
#iterations: 94
currently lose_sum: 498.38353034853935
time_elpased: 1.708
batch start
#iterations: 95
currently lose_sum: 499.54239723086357
time_elpased: 1.629
batch start
#iterations: 96
currently lose_sum: 501.05496177077293
time_elpased: 1.715
batch start
#iterations: 97
currently lose_sum: 499.09204092621803
time_elpased: 1.672
batch start
#iterations: 98
currently lose_sum: 500.18200954794884
time_elpased: 1.638
batch start
#iterations: 99
currently lose_sum: 499.8444314002991
time_elpased: 1.609
start validation test
0.306288659794
1.0
0.306288659794
0.468944834662
0
validation finish
batch start
#iterations: 100
currently lose_sum: 500.1432811021805
time_elpased: 1.654
batch start
#iterations: 101
currently lose_sum: 500.12449645996094
time_elpased: 1.683
batch start
#iterations: 102
currently lose_sum: 497.9195166528225
time_elpased: 1.675
batch start
#iterations: 103
currently lose_sum: 498.42743533849716
time_elpased: 1.657
batch start
#iterations: 104
currently lose_sum: 500.59434485435486
time_elpased: 1.642
batch start
#iterations: 105
currently lose_sum: 499.2455417215824
time_elpased: 1.625
batch start
#iterations: 106
currently lose_sum: 501.732280343771
time_elpased: 1.622
batch start
#iterations: 107
currently lose_sum: 500.6642954349518
time_elpased: 1.624
batch start
#iterations: 108
currently lose_sum: 498.7995465695858
time_elpased: 1.705
batch start
#iterations: 109
currently lose_sum: 499.20994171500206
time_elpased: 1.636
batch start
#iterations: 110
currently lose_sum: 499.14024421572685
time_elpased: 1.652
batch start
#iterations: 111
currently lose_sum: 502.9485849440098
time_elpased: 1.628
batch start
#iterations: 112
currently lose_sum: 498.5803261399269
time_elpased: 1.632
batch start
#iterations: 113
currently lose_sum: 498.98695889115334
time_elpased: 1.61
batch start
#iterations: 114
currently lose_sum: 498.55956295132637
time_elpased: 1.642
batch start
#iterations: 115
currently lose_sum: 499.9904175698757
time_elpased: 1.645
batch start
#iterations: 116
currently lose_sum: 499.40307772159576
time_elpased: 1.696
batch start
#iterations: 117
currently lose_sum: 497.69024181365967
time_elpased: 1.616
batch start
#iterations: 118
currently lose_sum: 499.62647461891174
time_elpased: 1.629
batch start
#iterations: 119
currently lose_sum: 498.93428882956505
time_elpased: 1.645
start validation test
0.557422680412
1.0
0.557422680412
0.71582710002
0
validation finish
batch start
#iterations: 120
currently lose_sum: 498.1106979548931
time_elpased: 1.707
batch start
#iterations: 121
currently lose_sum: 498.23373141884804
time_elpased: 1.709
batch start
#iterations: 122
currently lose_sum: 498.74000626802444
time_elpased: 1.62
batch start
#iterations: 123
currently lose_sum: 499.2274529337883
time_elpased: 1.625
batch start
#iterations: 124
currently lose_sum: 499.1172916293144
time_elpased: 1.711
batch start
#iterations: 125
currently lose_sum: 500.5016424357891
time_elpased: 1.668
batch start
#iterations: 126
currently lose_sum: 497.41323867440224
time_elpased: 1.67
batch start
#iterations: 127
currently lose_sum: 500.7945311963558
time_elpased: 1.637
batch start
#iterations: 128
currently lose_sum: 499.08173620700836
time_elpased: 1.724
batch start
#iterations: 129
currently lose_sum: 498.58538496494293
time_elpased: 1.613
batch start
#iterations: 130
currently lose_sum: 498.59380891919136
time_elpased: 1.67
batch start
#iterations: 131
currently lose_sum: 498.2085682451725
time_elpased: 1.703
batch start
#iterations: 132
currently lose_sum: 497.86885756254196
time_elpased: 1.705
batch start
#iterations: 133
currently lose_sum: 501.2816609740257
time_elpased: 1.621
batch start
#iterations: 134
currently lose_sum: 498.2643373608589
time_elpased: 1.647
batch start
#iterations: 135
currently lose_sum: 495.49751764535904
time_elpased: 1.624
batch start
#iterations: 136
currently lose_sum: 497.10397842526436
time_elpased: 1.71
batch start
#iterations: 137
currently lose_sum: 497.04701286554337
time_elpased: 1.704
batch start
#iterations: 138
currently lose_sum: 499.0818369090557
time_elpased: 1.694
batch start
#iterations: 139
currently lose_sum: 498.1346504688263
time_elpased: 1.646
start validation test
0.10412371134
1.0
0.10412371134
0.188608776844
0
validation finish
batch start
#iterations: 140
currently lose_sum: 496.56646478176117
time_elpased: 1.686
batch start
#iterations: 141
currently lose_sum: 495.60843259096146
time_elpased: 1.651
batch start
#iterations: 142
currently lose_sum: 497.29568311572075
time_elpased: 1.635
batch start
#iterations: 143
currently lose_sum: 497.6075751185417
time_elpased: 1.624
batch start
#iterations: 144
currently lose_sum: 499.07903042435646
time_elpased: 1.65
batch start
#iterations: 145
currently lose_sum: 496.16229870915413
time_elpased: 1.643
batch start
#iterations: 146
currently lose_sum: 499.5131842792034
time_elpased: 1.726
batch start
#iterations: 147
currently lose_sum: 498.5225549042225
time_elpased: 1.635
batch start
#iterations: 148
currently lose_sum: 496.1518616974354
time_elpased: 1.675
batch start
#iterations: 149
currently lose_sum: 499.15172481536865
time_elpased: 1.696
batch start
#iterations: 150
currently lose_sum: 495.5760880112648
time_elpased: 1.699
batch start
#iterations: 151
currently lose_sum: 494.9602337181568
time_elpased: 1.634
batch start
#iterations: 152
currently lose_sum: 498.6609928011894
time_elpased: 1.666
batch start
#iterations: 153
currently lose_sum: 495.8716259896755
time_elpased: 1.643
batch start
#iterations: 154
currently lose_sum: 500.4243986904621
time_elpased: 1.651
batch start
#iterations: 155
currently lose_sum: 496.4696332812309
time_elpased: 1.622
batch start
#iterations: 156
currently lose_sum: 497.77248626947403
time_elpased: 1.664
batch start
#iterations: 157
currently lose_sum: 497.4559001326561
time_elpased: 1.693
batch start
#iterations: 158
currently lose_sum: 495.8259814083576
time_elpased: 1.656
batch start
#iterations: 159
currently lose_sum: 498.26096814870834
time_elpased: 1.687
start validation test
0.127216494845
1.0
0.127216494845
0.225717944028
0
validation finish
batch start
#iterations: 160
currently lose_sum: 498.9245105087757
time_elpased: 1.639
batch start
#iterations: 161
currently lose_sum: 497.6690359711647
time_elpased: 1.721
batch start
#iterations: 162
currently lose_sum: 495.3966584801674
time_elpased: 1.687
batch start
#iterations: 163
currently lose_sum: 495.05498257279396
time_elpased: 1.642
batch start
#iterations: 164
currently lose_sum: 497.41981956362724
time_elpased: 1.634
batch start
#iterations: 165
currently lose_sum: 498.27563455700874
time_elpased: 1.635
batch start
#iterations: 166
currently lose_sum: 498.2077096402645
time_elpased: 1.689
batch start
#iterations: 167
currently lose_sum: 496.86837607622147
time_elpased: 1.727
batch start
#iterations: 168
currently lose_sum: 497.30925527215004
time_elpased: 1.711
batch start
#iterations: 169
currently lose_sum: 497.0541389286518
time_elpased: 1.633
batch start
#iterations: 170
currently lose_sum: 497.70021492242813
time_elpased: 1.634
batch start
#iterations: 171
currently lose_sum: 495.99922582507133
time_elpased: 1.655
batch start
#iterations: 172
currently lose_sum: 500.011085242033
time_elpased: 1.643
batch start
#iterations: 173
currently lose_sum: 498.1361907720566
time_elpased: 1.7
batch start
#iterations: 174
currently lose_sum: 497.7463011145592
time_elpased: 1.668
batch start
#iterations: 175
currently lose_sum: 495.7784224152565
time_elpased: 1.622
batch start
#iterations: 176
currently lose_sum: 496.8509749174118
time_elpased: 1.632
batch start
#iterations: 177
currently lose_sum: 496.21917659044266
time_elpased: 1.625
batch start
#iterations: 178
currently lose_sum: 496.3987936079502
time_elpased: 1.652
batch start
#iterations: 179
currently lose_sum: 497.4630040228367
time_elpased: 1.617
start validation test
0.143711340206
1.0
0.143711340206
0.2513070128
0
validation finish
batch start
#iterations: 180
currently lose_sum: 496.5499857366085
time_elpased: 1.689
batch start
#iterations: 181
currently lose_sum: 497.5769386589527
time_elpased: 1.649
batch start
#iterations: 182
currently lose_sum: 496.6791521310806
time_elpased: 1.704
batch start
#iterations: 183
currently lose_sum: 497.1432848870754
time_elpased: 1.734
batch start
#iterations: 184
currently lose_sum: 495.5786890387535
time_elpased: 1.627
batch start
#iterations: 185
currently lose_sum: 496.6847802102566
time_elpased: 1.676
batch start
#iterations: 186
currently lose_sum: 497.1807716488838
time_elpased: 1.681
batch start
#iterations: 187
currently lose_sum: 494.65935856103897
time_elpased: 1.644
batch start
#iterations: 188
currently lose_sum: 496.3912507891655
time_elpased: 1.622
batch start
#iterations: 189
currently lose_sum: 497.772523522377
time_elpased: 1.689
batch start
#iterations: 190
currently lose_sum: 496.160419523716
time_elpased: 1.633
batch start
#iterations: 191
currently lose_sum: 497.8497422635555
time_elpased: 1.661
batch start
#iterations: 192
currently lose_sum: 495.81042486429214
time_elpased: 1.677
batch start
#iterations: 193
currently lose_sum: 496.0258284807205
time_elpased: 1.631
batch start
#iterations: 194
currently lose_sum: 498.12195032835007
time_elpased: 1.627
batch start
#iterations: 195
currently lose_sum: 496.0329927802086
time_elpased: 1.689
batch start
#iterations: 196
currently lose_sum: 494.1741988956928
time_elpased: 1.618
batch start
#iterations: 197
currently lose_sum: 497.1875740289688
time_elpased: 1.712
batch start
#iterations: 198
currently lose_sum: 495.3063269853592
time_elpased: 1.727
batch start
#iterations: 199
currently lose_sum: 494.08207392692566
time_elpased: 1.69
start validation test
0.285463917526
1.0
0.285463917526
0.444141470848
0
validation finish
batch start
#iterations: 200
currently lose_sum: 496.0303858220577
time_elpased: 1.641
batch start
#iterations: 201
currently lose_sum: 495.23221999406815
time_elpased: 1.702
batch start
#iterations: 202
currently lose_sum: 495.1870307922363
time_elpased: 1.644
batch start
#iterations: 203
currently lose_sum: 495.5468936264515
time_elpased: 1.703
batch start
#iterations: 204
currently lose_sum: 494.38827207684517
time_elpased: 1.643
batch start
#iterations: 205
currently lose_sum: 495.9388692677021
time_elpased: 1.639
batch start
#iterations: 206
currently lose_sum: 498.7263640463352
time_elpased: 1.637
batch start
#iterations: 207
currently lose_sum: 496.8906296789646
time_elpased: 1.625
batch start
#iterations: 208
currently lose_sum: 495.40954998135567
time_elpased: 1.646
batch start
#iterations: 209
currently lose_sum: 495.01418805122375
time_elpased: 1.718
batch start
#iterations: 210
currently lose_sum: 495.16247856616974
time_elpased: 1.674
batch start
#iterations: 211
currently lose_sum: 496.8536258339882
time_elpased: 1.654
batch start
#iterations: 212
currently lose_sum: 495.56226721405983
time_elpased: 1.625
batch start
#iterations: 213
currently lose_sum: 495.7034600973129
time_elpased: 1.628
batch start
#iterations: 214
currently lose_sum: 496.1735759675503
time_elpased: 1.635
batch start
#iterations: 215
currently lose_sum: 496.09473061561584
time_elpased: 1.639
batch start
#iterations: 216
currently lose_sum: 496.1258969604969
time_elpased: 1.682
batch start
#iterations: 217
currently lose_sum: 495.4975054562092
time_elpased: 1.65
batch start
#iterations: 218
currently lose_sum: 496.25737565755844
time_elpased: 1.627
batch start
#iterations: 219
currently lose_sum: 496.47426784038544
time_elpased: 1.643
start validation test
0.0687628865979
1.0
0.0687628865979
0.128677534484
0
validation finish
batch start
#iterations: 220
currently lose_sum: 494.57874631881714
time_elpased: 1.689
batch start
#iterations: 221
currently lose_sum: 498.2733347117901
time_elpased: 1.633
batch start
#iterations: 222
currently lose_sum: 494.38033443689346
time_elpased: 1.681
batch start
#iterations: 223
currently lose_sum: 494.36866679787636
time_elpased: 1.631
batch start
#iterations: 224
currently lose_sum: 497.93413493037224
time_elpased: 1.627
batch start
#iterations: 225
currently lose_sum: 496.5080208182335
time_elpased: 1.697
batch start
#iterations: 226
currently lose_sum: 496.769619256258
time_elpased: 1.641
batch start
#iterations: 227
currently lose_sum: 496.01354056596756
time_elpased: 1.707
batch start
#iterations: 228
currently lose_sum: 493.2008717060089
time_elpased: 1.631
batch start
#iterations: 229
currently lose_sum: 495.6929114460945
time_elpased: 1.705
batch start
#iterations: 230
currently lose_sum: 495.91347101330757
time_elpased: 1.617
batch start
#iterations: 231
currently lose_sum: 497.26084849238396
time_elpased: 1.737
batch start
#iterations: 232
currently lose_sum: 491.85867577791214
time_elpased: 1.726
batch start
#iterations: 233
currently lose_sum: 494.39207565784454
time_elpased: 1.639
batch start
#iterations: 234
currently lose_sum: 495.5710833966732
time_elpased: 1.679
batch start
#iterations: 235
currently lose_sum: 496.16095116734505
time_elpased: 1.69
batch start
#iterations: 236
currently lose_sum: 494.9706534743309
time_elpased: 1.704
batch start
#iterations: 237
currently lose_sum: 496.2502087056637
time_elpased: 1.63
batch start
#iterations: 238
currently lose_sum: 493.48111313581467
time_elpased: 1.639
batch start
#iterations: 239
currently lose_sum: 493.43293684720993
time_elpased: 1.715
start validation test
0.0451546391753
1.0
0.0451546391753
0.0864075754587
0
validation finish
batch start
#iterations: 240
currently lose_sum: 492.84611397981644
time_elpased: 1.632
batch start
#iterations: 241
currently lose_sum: 494.29817190766335
time_elpased: 1.689
batch start
#iterations: 242
currently lose_sum: 492.23125034570694
time_elpased: 1.643
batch start
#iterations: 243
currently lose_sum: 493.74312353134155
time_elpased: 1.684
batch start
#iterations: 244
currently lose_sum: 495.1740335524082
time_elpased: 1.631
batch start
#iterations: 245
currently lose_sum: 495.08304128050804
time_elpased: 1.68
batch start
#iterations: 246
currently lose_sum: 495.5893987417221
time_elpased: 1.622
batch start
#iterations: 247
currently lose_sum: 495.80509996414185
time_elpased: 1.685
batch start
#iterations: 248
currently lose_sum: 494.49575623869896
time_elpased: 1.68
batch start
#iterations: 249
currently lose_sum: 497.3123798072338
time_elpased: 1.702
batch start
#iterations: 250
currently lose_sum: 493.49522331357
time_elpased: 1.632
batch start
#iterations: 251
currently lose_sum: 494.4829885661602
time_elpased: 1.668
batch start
#iterations: 252
currently lose_sum: 494.96965822577477
time_elpased: 1.642
batch start
#iterations: 253
currently lose_sum: 492.98239701986313
time_elpased: 1.711
batch start
#iterations: 254
currently lose_sum: 496.44049021601677
time_elpased: 1.634
batch start
#iterations: 255
currently lose_sum: 495.02012026309967
time_elpased: 1.646
batch start
#iterations: 256
currently lose_sum: 494.1053161919117
time_elpased: 1.621
batch start
#iterations: 257
currently lose_sum: 493.9629714488983
time_elpased: 1.715
batch start
#iterations: 258
currently lose_sum: 494.37827253341675
time_elpased: 1.637
batch start
#iterations: 259
currently lose_sum: 497.04478773474693
time_elpased: 1.709
start validation test
0.184639175258
1.0
0.184639175258
0.311722217388
0
validation finish
batch start
#iterations: 260
currently lose_sum: 495.4350355267525
time_elpased: 1.631
batch start
#iterations: 261
currently lose_sum: 496.27531373500824
time_elpased: 1.74
batch start
#iterations: 262
currently lose_sum: 495.3488047719002
time_elpased: 1.716
batch start
#iterations: 263
currently lose_sum: 494.8548037111759
time_elpased: 1.629
batch start
#iterations: 264
currently lose_sum: 493.56577917933464
time_elpased: 1.683
batch start
#iterations: 265
currently lose_sum: 494.1574801802635
time_elpased: 1.642
batch start
#iterations: 266
currently lose_sum: 495.3363407552242
time_elpased: 1.671
batch start
#iterations: 267
currently lose_sum: 494.38063448667526
time_elpased: 1.687
batch start
#iterations: 268
currently lose_sum: 495.4351990520954
time_elpased: 1.644
batch start
#iterations: 269
currently lose_sum: 496.0826758444309
time_elpased: 1.67
batch start
#iterations: 270
currently lose_sum: 494.71350663900375
time_elpased: 1.671
batch start
#iterations: 271
currently lose_sum: 489.61576920747757
time_elpased: 1.692
batch start
#iterations: 272
currently lose_sum: 495.4091128706932
time_elpased: 1.634
batch start
#iterations: 273
currently lose_sum: 493.1288553774357
time_elpased: 1.725
batch start
#iterations: 274
currently lose_sum: 494.88782584667206
time_elpased: 1.661
batch start
#iterations: 275
currently lose_sum: 495.59400483965874
time_elpased: 1.645
batch start
#iterations: 276
currently lose_sum: 493.03996655344963
time_elpased: 1.72
batch start
#iterations: 277
currently lose_sum: 496.37316647171974
time_elpased: 1.639
batch start
#iterations: 278
currently lose_sum: 495.7143603861332
time_elpased: 1.633
batch start
#iterations: 279
currently lose_sum: 494.50939843058586
time_elpased: 1.665
start validation test
0.108762886598
1.0
0.108762886598
0.196187819619
0
validation finish
batch start
#iterations: 280
currently lose_sum: 493.11848971247673
time_elpased: 1.643
batch start
#iterations: 281
currently lose_sum: 494.14677464962006
time_elpased: 1.72
batch start
#iterations: 282
currently lose_sum: 495.0988946259022
time_elpased: 1.67
batch start
#iterations: 283
currently lose_sum: 494.2075851857662
time_elpased: 1.697
batch start
#iterations: 284
currently lose_sum: 495.4447838962078
time_elpased: 1.7
batch start
#iterations: 285
currently lose_sum: 494.20431485772133
time_elpased: 1.642
batch start
#iterations: 286
currently lose_sum: 494.69498923420906
time_elpased: 1.638
batch start
#iterations: 287
currently lose_sum: 494.084849357605
time_elpased: 1.632
batch start
#iterations: 288
currently lose_sum: 495.41594687104225
time_elpased: 1.724
batch start
#iterations: 289
currently lose_sum: 494.0991457104683
time_elpased: 1.684
batch start
#iterations: 290
currently lose_sum: 492.8433669805527
time_elpased: 1.708
batch start
#iterations: 291
currently lose_sum: 494.5691234469414
time_elpased: 1.654
batch start
#iterations: 292
currently lose_sum: 494.0052077770233
time_elpased: 1.711
batch start
#iterations: 293
currently lose_sum: 493.0818766951561
time_elpased: 1.627
batch start
#iterations: 294
currently lose_sum: 494.7324957847595
time_elpased: 1.644
batch start
#iterations: 295
currently lose_sum: 494.4941338300705
time_elpased: 1.646
batch start
#iterations: 296
currently lose_sum: 491.1989924609661
time_elpased: 1.674
batch start
#iterations: 297
currently lose_sum: 494.54100826382637
time_elpased: 1.679
batch start
#iterations: 298
currently lose_sum: 493.4059006869793
time_elpased: 1.635
batch start
#iterations: 299
currently lose_sum: 492.6717823147774
time_elpased: 1.646
start validation test
0.392371134021
1.0
0.392371134021
0.563601362357
0
validation finish
batch start
#iterations: 300
currently lose_sum: 491.7872604727745
time_elpased: 1.715
batch start
#iterations: 301
currently lose_sum: 493.76346150040627
time_elpased: 1.708
batch start
#iterations: 302
currently lose_sum: 494.3903609216213
time_elpased: 1.624
batch start
#iterations: 303
currently lose_sum: 493.80294770002365
time_elpased: 1.628
batch start
#iterations: 304
currently lose_sum: 494.61035844683647
time_elpased: 1.668
batch start
#iterations: 305
currently lose_sum: 495.36522349715233
time_elpased: 1.671
batch start
#iterations: 306
currently lose_sum: 493.7071684598923
time_elpased: 1.696
batch start
#iterations: 307
currently lose_sum: 497.0949620306492
time_elpased: 1.702
batch start
#iterations: 308
currently lose_sum: 493.629786670208
time_elpased: 1.697
batch start
#iterations: 309
currently lose_sum: 495.68405029177666
time_elpased: 1.641
batch start
#iterations: 310
currently lose_sum: 492.31485813856125
time_elpased: 1.725
batch start
#iterations: 311
currently lose_sum: 492.6031183600426
time_elpased: 1.692
batch start
#iterations: 312
currently lose_sum: 493.26114296913147
time_elpased: 1.646
batch start
#iterations: 313
currently lose_sum: 494.2092424631119
time_elpased: 1.722
batch start
#iterations: 314
currently lose_sum: 492.5795895755291
time_elpased: 1.623
batch start
#iterations: 315
currently lose_sum: 494.07924684882164
time_elpased: 1.712
batch start
#iterations: 316
currently lose_sum: 494.4208386540413
time_elpased: 1.707
batch start
#iterations: 317
currently lose_sum: 493.0787664949894
time_elpased: 1.649
batch start
#iterations: 318
currently lose_sum: 496.13566237688065
time_elpased: 1.709
batch start
#iterations: 319
currently lose_sum: 494.04085686802864
time_elpased: 1.716
start validation test
0.0383505154639
1.0
0.0383505154639
0.0738681493249
0
validation finish
batch start
#iterations: 320
currently lose_sum: 493.87975710630417
time_elpased: 1.705
batch start
#iterations: 321
currently lose_sum: 492.4681980609894
time_elpased: 1.68
batch start
#iterations: 322
currently lose_sum: 491.89371263980865
time_elpased: 1.648
batch start
#iterations: 323
currently lose_sum: 493.38095435500145
time_elpased: 1.64
batch start
#iterations: 324
currently lose_sum: 495.7968056499958
time_elpased: 1.694
batch start
#iterations: 325
currently lose_sum: 494.37303736805916
time_elpased: 1.634
batch start
#iterations: 326
currently lose_sum: 495.0855474770069
time_elpased: 1.712
batch start
#iterations: 327
currently lose_sum: 493.18845412135124
time_elpased: 1.634
batch start
#iterations: 328
currently lose_sum: 492.5734711587429
time_elpased: 1.713
batch start
#iterations: 329
currently lose_sum: 493.7828798890114
time_elpased: 1.675
batch start
#iterations: 330
currently lose_sum: 494.7943579554558
time_elpased: 1.629
batch start
#iterations: 331
currently lose_sum: 495.33588805794716
time_elpased: 1.663
batch start
#iterations: 332
currently lose_sum: 493.2789172530174
time_elpased: 1.691
batch start
#iterations: 333
currently lose_sum: 495.24849316477776
time_elpased: 1.706
batch start
#iterations: 334
currently lose_sum: 493.94092920422554
time_elpased: 1.635
batch start
#iterations: 335
currently lose_sum: 490.9362268149853
time_elpased: 1.628
batch start
#iterations: 336
currently lose_sum: 494.68997579813004
time_elpased: 1.664
batch start
#iterations: 337
currently lose_sum: 493.6743334829807
time_elpased: 1.726
batch start
#iterations: 338
currently lose_sum: 494.96775659918785
time_elpased: 1.673
batch start
#iterations: 339
currently lose_sum: 492.05036386847496
time_elpased: 1.642
start validation test
0.215979381443
1.0
0.215979381443
0.355235269182
0
validation finish
batch start
#iterations: 340
currently lose_sum: 494.36639472842216
time_elpased: 1.637
batch start
#iterations: 341
currently lose_sum: 494.5727201998234
time_elpased: 1.642
batch start
#iterations: 342
currently lose_sum: 492.7338996231556
time_elpased: 1.7
batch start
#iterations: 343
currently lose_sum: 494.5106570124626
time_elpased: 1.667
batch start
#iterations: 344
currently lose_sum: 494.53978008031845
time_elpased: 1.658
batch start
#iterations: 345
currently lose_sum: 494.67322877049446
time_elpased: 1.68
batch start
#iterations: 346
currently lose_sum: 492.8925901055336
time_elpased: 1.649
batch start
#iterations: 347
currently lose_sum: 493.1847474575043
time_elpased: 1.636
batch start
#iterations: 348
currently lose_sum: 494.28358736634254
time_elpased: 1.632
batch start
#iterations: 349
currently lose_sum: 494.28442031145096
time_elpased: 1.677
batch start
#iterations: 350
currently lose_sum: 494.27449238300323
time_elpased: 1.647
batch start
#iterations: 351
currently lose_sum: 492.73438519239426
time_elpased: 1.617
batch start
#iterations: 352
currently lose_sum: 492.7755135893822
time_elpased: 1.688
batch start
#iterations: 353
currently lose_sum: 495.7611215412617
time_elpased: 1.693
batch start
#iterations: 354
currently lose_sum: 492.73941162228584
time_elpased: 1.629
batch start
#iterations: 355
currently lose_sum: 494.41153344511986
time_elpased: 1.718
batch start
#iterations: 356
currently lose_sum: 494.8675724864006
time_elpased: 1.637
batch start
#iterations: 357
currently lose_sum: 493.6133772134781
time_elpased: 1.637
batch start
#iterations: 358
currently lose_sum: 493.73066541552544
time_elpased: 1.685
batch start
#iterations: 359
currently lose_sum: 493.18174412846565
time_elpased: 1.709
start validation test
0.218453608247
1.0
0.218453608247
0.358575175565
0
validation finish
batch start
#iterations: 360
currently lose_sum: 493.64285627007484
time_elpased: 1.712
batch start
#iterations: 361
currently lose_sum: 492.9453051686287
time_elpased: 1.645
batch start
#iterations: 362
currently lose_sum: 493.89397966861725
time_elpased: 1.739
batch start
#iterations: 363
currently lose_sum: 492.75393944978714
time_elpased: 1.652
batch start
#iterations: 364
currently lose_sum: 492.76518031954765
time_elpased: 1.677
batch start
#iterations: 365
currently lose_sum: 493.99531772732735
time_elpased: 1.64
batch start
#iterations: 366
currently lose_sum: 494.50097462534904
time_elpased: 1.632
batch start
#iterations: 367
currently lose_sum: 495.1742132604122
time_elpased: 1.626
batch start
#iterations: 368
currently lose_sum: 491.26101422309875
time_elpased: 1.723
batch start
#iterations: 369
currently lose_sum: 491.836402118206
time_elpased: 1.66
batch start
#iterations: 370
currently lose_sum: 491.5648901462555
time_elpased: 1.71
batch start
#iterations: 371
currently lose_sum: 494.5247338414192
time_elpased: 1.631
batch start
#iterations: 372
currently lose_sum: 493.4847487807274
time_elpased: 1.681
batch start
#iterations: 373
currently lose_sum: 492.690427839756
time_elpased: 1.736
batch start
#iterations: 374
currently lose_sum: 494.1440290212631
time_elpased: 1.687
batch start
#iterations: 375
currently lose_sum: 492.62867933511734
time_elpased: 1.691
batch start
#iterations: 376
currently lose_sum: 494.06717601418495
time_elpased: 1.636
batch start
#iterations: 377
currently lose_sum: 495.28115567564964
time_elpased: 1.704
batch start
#iterations: 378
currently lose_sum: 494.1104580760002
time_elpased: 1.708
batch start
#iterations: 379
currently lose_sum: 493.7968084514141
time_elpased: 1.715
start validation test
0.284020618557
1.0
0.284020618557
0.442392613408
0
validation finish
batch start
#iterations: 380
currently lose_sum: 492.76768082380295
time_elpased: 1.748
batch start
#iterations: 381
currently lose_sum: 491.7979215681553
time_elpased: 1.699
batch start
#iterations: 382
currently lose_sum: 493.53039130568504
time_elpased: 1.71
batch start
#iterations: 383
currently lose_sum: 491.25608590245247
time_elpased: 1.632
batch start
#iterations: 384
currently lose_sum: 492.6795116662979
time_elpased: 1.641
batch start
#iterations: 385
currently lose_sum: 490.4585391879082
time_elpased: 1.661
batch start
#iterations: 386
currently lose_sum: 493.0966067612171
time_elpased: 1.628
batch start
#iterations: 387
currently lose_sum: 494.64460280537605
time_elpased: 1.684
batch start
#iterations: 388
currently lose_sum: 494.4812078177929
time_elpased: 1.714
batch start
#iterations: 389
currently lose_sum: 490.7432456314564
time_elpased: 1.664
batch start
#iterations: 390
currently lose_sum: 491.52205607295036
time_elpased: 1.679
batch start
#iterations: 391
currently lose_sum: 493.07779145240784
time_elpased: 1.666
batch start
#iterations: 392
currently lose_sum: 492.9377236068249
time_elpased: 1.681
batch start
#iterations: 393
currently lose_sum: 492.8128927052021
time_elpased: 1.645
batch start
#iterations: 394
currently lose_sum: 493.06335550546646
time_elpased: 1.626
batch start
#iterations: 395
currently lose_sum: 491.9648510813713
time_elpased: 1.628
batch start
#iterations: 396
currently lose_sum: 493.10351797938347
time_elpased: 1.639
batch start
#iterations: 397
currently lose_sum: 493.90681996941566
time_elpased: 1.634
batch start
#iterations: 398
currently lose_sum: 493.07559809088707
time_elpased: 1.684
batch start
#iterations: 399
currently lose_sum: 493.6300762295723
time_elpased: 1.642
start validation test
0.136288659794
1.0
0.136288659794
0.239883868626
0
validation finish
acc: 0.547
pre: 1.000
rec: 0.547
F1: 0.707
auc: 0.000
