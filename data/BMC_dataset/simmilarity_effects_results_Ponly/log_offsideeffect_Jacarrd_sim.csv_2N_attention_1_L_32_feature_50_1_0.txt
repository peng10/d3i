start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 551.1902267336845
time_elpased: 1.694
batch start
#iterations: 1
currently lose_sum: 542.1382043361664
time_elpased: 1.668
batch start
#iterations: 2
currently lose_sum: 538.0666552186012
time_elpased: 1.725
batch start
#iterations: 3
currently lose_sum: 533.9308968782425
time_elpased: 1.681
batch start
#iterations: 4
currently lose_sum: 530.99238884449
time_elpased: 1.669
batch start
#iterations: 5
currently lose_sum: 527.2719729840755
time_elpased: 1.664
batch start
#iterations: 6
currently lose_sum: 524.8324114084244
time_elpased: 1.703
batch start
#iterations: 7
currently lose_sum: 525.0601232349873
time_elpased: 1.727
batch start
#iterations: 8
currently lose_sum: 523.3864595293999
time_elpased: 1.665
batch start
#iterations: 9
currently lose_sum: 523.6345728039742
time_elpased: 1.66
batch start
#iterations: 10
currently lose_sum: 523.1802800893784
time_elpased: 1.739
batch start
#iterations: 11
currently lose_sum: 522.9892864525318
time_elpased: 1.739
batch start
#iterations: 12
currently lose_sum: 520.8875613212585
time_elpased: 1.742
batch start
#iterations: 13
currently lose_sum: 518.5619367063046
time_elpased: 1.693
batch start
#iterations: 14
currently lose_sum: 518.6634564995766
time_elpased: 1.69
batch start
#iterations: 15
currently lose_sum: 515.6118602156639
time_elpased: 1.667
batch start
#iterations: 16
currently lose_sum: 517.6789053976536
time_elpased: 1.665
batch start
#iterations: 17
currently lose_sum: 517.0788748562336
time_elpased: 1.667
batch start
#iterations: 18
currently lose_sum: 514.4434456825256
time_elpased: 1.765
batch start
#iterations: 19
currently lose_sum: 514.5876778364182
time_elpased: 1.684
start validation test
0.293711340206
1.0
0.293711340206
0.454060084469
0
validation finish
batch start
#iterations: 20
currently lose_sum: 514.8836551010609
time_elpased: 1.691
batch start
#iterations: 21
currently lose_sum: 515.9099727272987
time_elpased: 1.689
batch start
#iterations: 22
currently lose_sum: 512.8367359042168
time_elpased: 1.661
batch start
#iterations: 23
currently lose_sum: 513.4453300833702
time_elpased: 1.673
batch start
#iterations: 24
currently lose_sum: 511.34052035212517
time_elpased: 1.685
batch start
#iterations: 25
currently lose_sum: 511.57018634676933
time_elpased: 1.705
batch start
#iterations: 26
currently lose_sum: 512.3862693309784
time_elpased: 1.736
batch start
#iterations: 27
currently lose_sum: 513.0156672000885
time_elpased: 1.669
batch start
#iterations: 28
currently lose_sum: 510.0907216370106
time_elpased: 1.678
batch start
#iterations: 29
currently lose_sum: 510.6066827774048
time_elpased: 1.746
batch start
#iterations: 30
currently lose_sum: 510.11252999305725
time_elpased: 1.739
batch start
#iterations: 31
currently lose_sum: 509.4353549480438
time_elpased: 1.684
batch start
#iterations: 32
currently lose_sum: 511.3738896548748
time_elpased: 1.74
batch start
#iterations: 33
currently lose_sum: 509.25844299793243
time_elpased: 1.7
batch start
#iterations: 34
currently lose_sum: 510.14360427856445
time_elpased: 1.678
batch start
#iterations: 35
currently lose_sum: 507.69239842891693
time_elpased: 1.714
batch start
#iterations: 36
currently lose_sum: 508.04095378518105
time_elpased: 1.683
batch start
#iterations: 37
currently lose_sum: 506.8857955634594
time_elpased: 1.742
batch start
#iterations: 38
currently lose_sum: 507.2241562604904
time_elpased: 1.676
batch start
#iterations: 39
currently lose_sum: 507.1839408278465
time_elpased: 1.712
start validation test
0.288659793814
1.0
0.288659793814
0.448
0
validation finish
batch start
#iterations: 40
currently lose_sum: 508.2846723496914
time_elpased: 1.713
batch start
#iterations: 41
currently lose_sum: 508.3206277489662
time_elpased: 1.709
batch start
#iterations: 42
currently lose_sum: 505.46943429112434
time_elpased: 1.687
batch start
#iterations: 43
currently lose_sum: 506.2467669248581
time_elpased: 1.684
batch start
#iterations: 44
currently lose_sum: 508.4355700314045
time_elpased: 1.745
batch start
#iterations: 45
currently lose_sum: 509.0185494720936
time_elpased: 1.67
batch start
#iterations: 46
currently lose_sum: 504.36187478899956
time_elpased: 1.67
batch start
#iterations: 47
currently lose_sum: 505.42944782972336
time_elpased: 1.671
batch start
#iterations: 48
currently lose_sum: 507.00325125455856
time_elpased: 1.666
batch start
#iterations: 49
currently lose_sum: 502.23211374878883
time_elpased: 1.729
batch start
#iterations: 50
currently lose_sum: 506.47900027036667
time_elpased: 1.663
batch start
#iterations: 51
currently lose_sum: 505.67291790246964
time_elpased: 1.733
batch start
#iterations: 52
currently lose_sum: 505.07990795373917
time_elpased: 1.673
batch start
#iterations: 53
currently lose_sum: 506.0767332613468
time_elpased: 1.667
batch start
#iterations: 54
currently lose_sum: 503.748463511467
time_elpased: 1.666
batch start
#iterations: 55
currently lose_sum: 503.8907852470875
time_elpased: 1.744
batch start
#iterations: 56
currently lose_sum: 505.22414270043373
time_elpased: 1.676
batch start
#iterations: 57
currently lose_sum: 504.6527124643326
time_elpased: 1.678
batch start
#iterations: 58
currently lose_sum: 505.2507050335407
time_elpased: 1.703
batch start
#iterations: 59
currently lose_sum: 503.77265653014183
time_elpased: 1.756
start validation test
0.235670103093
1.0
0.235670103093
0.381445019189
0
validation finish
batch start
#iterations: 60
currently lose_sum: 504.15195509791374
time_elpased: 1.683
batch start
#iterations: 61
currently lose_sum: 502.0636409819126
time_elpased: 1.668
batch start
#iterations: 62
currently lose_sum: 506.94453647732735
time_elpased: 1.687
batch start
#iterations: 63
currently lose_sum: 505.2483674287796
time_elpased: 1.7
batch start
#iterations: 64
currently lose_sum: 502.37369030714035
time_elpased: 1.741
batch start
#iterations: 65
currently lose_sum: 503.19494462013245
time_elpased: 1.677
batch start
#iterations: 66
currently lose_sum: 504.0283422470093
time_elpased: 1.692
batch start
#iterations: 67
currently lose_sum: 502.0046654045582
time_elpased: 1.767
batch start
#iterations: 68
currently lose_sum: 503.8168915808201
time_elpased: 1.696
batch start
#iterations: 69
currently lose_sum: 505.5962140262127
time_elpased: 1.738
batch start
#iterations: 70
currently lose_sum: 501.38034480810165
time_elpased: 1.683
batch start
#iterations: 71
currently lose_sum: 503.3523801267147
time_elpased: 1.694
batch start
#iterations: 72
currently lose_sum: 505.7306805551052
time_elpased: 1.755
batch start
#iterations: 73
currently lose_sum: 502.16045239567757
time_elpased: 1.736
batch start
#iterations: 74
currently lose_sum: 502.5090687572956
time_elpased: 1.765
batch start
#iterations: 75
currently lose_sum: 501.91006630659103
time_elpased: 1.727
batch start
#iterations: 76
currently lose_sum: 502.02626863121986
time_elpased: 1.675
batch start
#iterations: 77
currently lose_sum: 502.1205858886242
time_elpased: 1.717
batch start
#iterations: 78
currently lose_sum: 501.9304763972759
time_elpased: 1.719
batch start
#iterations: 79
currently lose_sum: 503.8838962316513
time_elpased: 1.676
start validation test
0.192783505155
1.0
0.192783505155
0.323249783924
0
validation finish
batch start
#iterations: 80
currently lose_sum: 502.8012108206749
time_elpased: 1.693
batch start
#iterations: 81
currently lose_sum: 500.49512377381325
time_elpased: 1.731
batch start
#iterations: 82
currently lose_sum: 502.6782287955284
time_elpased: 1.67
batch start
#iterations: 83
currently lose_sum: 501.0334641635418
time_elpased: 1.669
batch start
#iterations: 84
currently lose_sum: 502.45445290207863
time_elpased: 1.687
batch start
#iterations: 85
currently lose_sum: 500.31609132885933
time_elpased: 1.756
batch start
#iterations: 86
currently lose_sum: 501.2123095393181
time_elpased: 1.751
batch start
#iterations: 87
currently lose_sum: 500.7239718437195
time_elpased: 1.749
batch start
#iterations: 88
currently lose_sum: 501.39444211125374
time_elpased: 1.731
batch start
#iterations: 89
currently lose_sum: 502.88592144846916
time_elpased: 1.671
batch start
#iterations: 90
currently lose_sum: 501.60760685801506
time_elpased: 1.725
batch start
#iterations: 91
currently lose_sum: 500.3347679078579
time_elpased: 1.768
batch start
#iterations: 92
currently lose_sum: 500.88946011662483
time_elpased: 1.67
batch start
#iterations: 93
currently lose_sum: 500.77126294374466
time_elpased: 1.698
batch start
#iterations: 94
currently lose_sum: 499.87948551774025
time_elpased: 1.685
batch start
#iterations: 95
currently lose_sum: 501.0728778243065
time_elpased: 1.752
batch start
#iterations: 96
currently lose_sum: 502.8445653915405
time_elpased: 1.717
batch start
#iterations: 97
currently lose_sum: 499.25151339173317
time_elpased: 1.679
batch start
#iterations: 98
currently lose_sum: 501.52465814352036
time_elpased: 1.661
batch start
#iterations: 99
currently lose_sum: 500.26362305879593
time_elpased: 1.676
start validation test
0.371030927835
1.0
0.371030927835
0.541243702534
0
validation finish
batch start
#iterations: 100
currently lose_sum: 501.027899235487
time_elpased: 1.697
batch start
#iterations: 101
currently lose_sum: 501.97117578983307
time_elpased: 1.672
batch start
#iterations: 102
currently lose_sum: 498.99734884500504
time_elpased: 1.666
batch start
#iterations: 103
currently lose_sum: 501.2808890044689
time_elpased: 1.666
batch start
#iterations: 104
currently lose_sum: 499.8361141383648
time_elpased: 1.693
batch start
#iterations: 105
currently lose_sum: 500.32995253801346
time_elpased: 1.722
batch start
#iterations: 106
currently lose_sum: 502.36345529556274
time_elpased: 1.668
batch start
#iterations: 107
currently lose_sum: 500.3824186325073
time_elpased: 1.692
batch start
#iterations: 108
currently lose_sum: 498.7479039132595
time_elpased: 1.705
batch start
#iterations: 109
currently lose_sum: 499.820688277483
time_elpased: 1.741
batch start
#iterations: 110
currently lose_sum: 501.42936486005783
time_elpased: 1.71
batch start
#iterations: 111
currently lose_sum: 502.59816920757294
time_elpased: 1.686
batch start
#iterations: 112
currently lose_sum: 498.5236624777317
time_elpased: 1.689
batch start
#iterations: 113
currently lose_sum: 500.49695456027985
time_elpased: 1.745
batch start
#iterations: 114
currently lose_sum: 501.36713752150536
time_elpased: 1.688
batch start
#iterations: 115
currently lose_sum: 499.9183903634548
time_elpased: 1.738
batch start
#iterations: 116
currently lose_sum: 498.5405620932579
time_elpased: 1.689
batch start
#iterations: 117
currently lose_sum: 500.62045672535896
time_elpased: 1.705
batch start
#iterations: 118
currently lose_sum: 499.1322832405567
time_elpased: 1.7
batch start
#iterations: 119
currently lose_sum: 499.9044066667557
time_elpased: 1.797
start validation test
0.386391752577
1.0
0.386391752577
0.55740630577
0
validation finish
batch start
#iterations: 120
currently lose_sum: 500.17448315024376
time_elpased: 1.733
batch start
#iterations: 121
currently lose_sum: 498.21724170446396
time_elpased: 1.728
batch start
#iterations: 122
currently lose_sum: 498.17155945301056
time_elpased: 1.751
batch start
#iterations: 123
currently lose_sum: 499.4754729270935
time_elpased: 1.673
batch start
#iterations: 124
currently lose_sum: 499.06252351403236
time_elpased: 1.739
batch start
#iterations: 125
currently lose_sum: 499.9071924984455
time_elpased: 1.761
batch start
#iterations: 126
currently lose_sum: 499.3379309773445
time_elpased: 1.663
batch start
#iterations: 127
currently lose_sum: 500.18238657712936
time_elpased: 1.725
batch start
#iterations: 128
currently lose_sum: 499.78043335676193
time_elpased: 1.737
batch start
#iterations: 129
currently lose_sum: 500.2911166548729
time_elpased: 1.737
batch start
#iterations: 130
currently lose_sum: 499.1194867193699
time_elpased: 1.737
batch start
#iterations: 131
currently lose_sum: 499.81316685676575
time_elpased: 1.675
batch start
#iterations: 132
currently lose_sum: 498.58619248867035
time_elpased: 1.685
batch start
#iterations: 133
currently lose_sum: 500.3154856264591
time_elpased: 1.712
batch start
#iterations: 134
currently lose_sum: 498.35238379240036
time_elpased: 1.682
batch start
#iterations: 135
currently lose_sum: 496.8468394577503
time_elpased: 1.7
batch start
#iterations: 136
currently lose_sum: 497.8446661233902
time_elpased: 1.69
batch start
#iterations: 137
currently lose_sum: 496.9973055422306
time_elpased: 1.692
batch start
#iterations: 138
currently lose_sum: 497.2684490978718
time_elpased: 1.733
batch start
#iterations: 139
currently lose_sum: 497.9616940319538
time_elpased: 1.741
start validation test
0.158144329897
1.0
0.158144329897
0.273099519316
0
validation finish
batch start
#iterations: 140
currently lose_sum: 497.7618011832237
time_elpased: 1.743
batch start
#iterations: 141
currently lose_sum: 498.9836749136448
time_elpased: 1.741
batch start
#iterations: 142
currently lose_sum: 495.71935161948204
time_elpased: 1.684
batch start
#iterations: 143
currently lose_sum: 497.46080031991005
time_elpased: 1.691
batch start
#iterations: 144
currently lose_sum: 497.40734481811523
time_elpased: 1.757
batch start
#iterations: 145
currently lose_sum: 500.0152290761471
time_elpased: 1.74
batch start
#iterations: 146
currently lose_sum: 500.02390682697296
time_elpased: 1.767
batch start
#iterations: 147
currently lose_sum: 498.89802649617195
time_elpased: 1.756
batch start
#iterations: 148
currently lose_sum: 496.51714208722115
time_elpased: 1.752
batch start
#iterations: 149
currently lose_sum: 498.2315065264702
time_elpased: 1.76
batch start
#iterations: 150
currently lose_sum: 496.8353906273842
time_elpased: 1.671
batch start
#iterations: 151
currently lose_sum: 495.3589653670788
time_elpased: 1.685
batch start
#iterations: 152
currently lose_sum: 496.9230714440346
time_elpased: 1.684
batch start
#iterations: 153
currently lose_sum: 495.212082862854
time_elpased: 1.683
batch start
#iterations: 154
currently lose_sum: 500.3956727683544
time_elpased: 1.721
batch start
#iterations: 155
currently lose_sum: 495.60841128230095
time_elpased: 1.755
batch start
#iterations: 156
currently lose_sum: 497.6272060871124
time_elpased: 1.685
batch start
#iterations: 157
currently lose_sum: 497.32460182905197
time_elpased: 1.681
batch start
#iterations: 158
currently lose_sum: 497.22586008906364
time_elpased: 1.665
batch start
#iterations: 159
currently lose_sum: 497.53757429122925
time_elpased: 1.744
start validation test
0.169793814433
1.0
0.169793814433
0.2902969948
0
validation finish
batch start
#iterations: 160
currently lose_sum: 498.47021985054016
time_elpased: 1.698
batch start
#iterations: 161
currently lose_sum: 496.3761047720909
time_elpased: 1.714
batch start
#iterations: 162
currently lose_sum: 496.5619712471962
time_elpased: 1.673
batch start
#iterations: 163
currently lose_sum: 497.0756890177727
time_elpased: 1.743
batch start
#iterations: 164
currently lose_sum: 497.88970842957497
time_elpased: 1.751
batch start
#iterations: 165
currently lose_sum: 499.28857773542404
time_elpased: 1.675
batch start
#iterations: 166
currently lose_sum: 498.19071531295776
time_elpased: 1.686
batch start
#iterations: 167
currently lose_sum: 496.486725628376
time_elpased: 1.682
batch start
#iterations: 168
currently lose_sum: 497.1779333949089
time_elpased: 1.686
batch start
#iterations: 169
currently lose_sum: 497.42949852347374
time_elpased: 1.731
batch start
#iterations: 170
currently lose_sum: 496.1308381855488
time_elpased: 1.68
batch start
#iterations: 171
currently lose_sum: 496.93152859807014
time_elpased: 1.707
batch start
#iterations: 172
currently lose_sum: 498.29875722527504
time_elpased: 1.743
batch start
#iterations: 173
currently lose_sum: 498.3037234246731
time_elpased: 1.726
batch start
#iterations: 174
currently lose_sum: 496.95554226636887
time_elpased: 1.697
batch start
#iterations: 175
currently lose_sum: 496.5915386080742
time_elpased: 1.676
batch start
#iterations: 176
currently lose_sum: 496.1487898826599
time_elpased: 1.681
batch start
#iterations: 177
currently lose_sum: 497.23500698804855
time_elpased: 1.681
batch start
#iterations: 178
currently lose_sum: 498.2660186290741
time_elpased: 1.756
batch start
#iterations: 179
currently lose_sum: 498.5377785861492
time_elpased: 1.769
start validation test
0.174948453608
1.0
0.174948453608
0.297797666052
0
validation finish
batch start
#iterations: 180
currently lose_sum: 498.3346247971058
time_elpased: 1.75
batch start
#iterations: 181
currently lose_sum: 498.9598158299923
time_elpased: 1.717
batch start
#iterations: 182
currently lose_sum: 497.36368200182915
time_elpased: 1.733
batch start
#iterations: 183
currently lose_sum: 498.17970272898674
time_elpased: 1.746
batch start
#iterations: 184
currently lose_sum: 496.11095654964447
time_elpased: 1.676
batch start
#iterations: 185
currently lose_sum: 497.63313776254654
time_elpased: 1.752
batch start
#iterations: 186
currently lose_sum: 497.7586291730404
time_elpased: 1.723
batch start
#iterations: 187
currently lose_sum: 498.13733461499214
time_elpased: 1.675
batch start
#iterations: 188
currently lose_sum: 497.628814637661
time_elpased: 1.76
batch start
#iterations: 189
currently lose_sum: 497.46283000707626
time_elpased: 1.749
batch start
#iterations: 190
currently lose_sum: 495.7923461794853
time_elpased: 1.753
batch start
#iterations: 191
currently lose_sum: 496.28366580605507
time_elpased: 1.707
batch start
#iterations: 192
currently lose_sum: 494.5696656703949
time_elpased: 1.678
batch start
#iterations: 193
currently lose_sum: 496.555482506752
time_elpased: 1.773
batch start
#iterations: 194
currently lose_sum: 497.23249953985214
time_elpased: 1.752
batch start
#iterations: 195
currently lose_sum: 499.85600143671036
time_elpased: 1.705
batch start
#iterations: 196
currently lose_sum: 495.9457124173641
time_elpased: 1.715
batch start
#iterations: 197
currently lose_sum: 498.657135874033
time_elpased: 1.698
batch start
#iterations: 198
currently lose_sum: 496.36538860201836
time_elpased: 1.679
batch start
#iterations: 199
currently lose_sum: 496.8124117553234
time_elpased: 1.738
start validation test
0.181237113402
1.0
0.181237113402
0.306859835922
0
validation finish
batch start
#iterations: 200
currently lose_sum: 496.26318368315697
time_elpased: 1.743
batch start
#iterations: 201
currently lose_sum: 498.88642022013664
time_elpased: 1.682
batch start
#iterations: 202
currently lose_sum: 495.83525291085243
time_elpased: 1.747
batch start
#iterations: 203
currently lose_sum: 496.00561025738716
time_elpased: 1.663
batch start
#iterations: 204
currently lose_sum: 496.3642571568489
time_elpased: 1.688
batch start
#iterations: 205
currently lose_sum: 496.8388464450836
time_elpased: 1.678
batch start
#iterations: 206
currently lose_sum: 498.98203667998314
time_elpased: 1.71
batch start
#iterations: 207
currently lose_sum: 495.1725054681301
time_elpased: 1.68
batch start
#iterations: 208
currently lose_sum: 495.4034665822983
time_elpased: 1.671
batch start
#iterations: 209
currently lose_sum: 496.62154573202133
time_elpased: 1.762
batch start
#iterations: 210
currently lose_sum: 495.4676425755024
time_elpased: 1.704
batch start
#iterations: 211
currently lose_sum: 496.95517843961716
time_elpased: 1.709
batch start
#iterations: 212
currently lose_sum: 495.5752934217453
time_elpased: 1.685
batch start
#iterations: 213
currently lose_sum: 496.30269396305084
time_elpased: 1.726
batch start
#iterations: 214
currently lose_sum: 496.92978459596634
time_elpased: 1.757
batch start
#iterations: 215
currently lose_sum: 495.98266834020615
time_elpased: 1.671
batch start
#iterations: 216
currently lose_sum: 496.6074197292328
time_elpased: 1.674
batch start
#iterations: 217
currently lose_sum: 495.48782151937485
time_elpased: 1.672
batch start
#iterations: 218
currently lose_sum: 495.6558962762356
time_elpased: 1.698
batch start
#iterations: 219
currently lose_sum: 498.13354510068893
time_elpased: 1.684
start validation test
0.0722680412371
1.0
0.0722680412371
0.134794731276
0
validation finish
batch start
#iterations: 220
currently lose_sum: 495.6529811024666
time_elpased: 1.763
batch start
#iterations: 221
currently lose_sum: 497.8409068584442
time_elpased: 1.692
batch start
#iterations: 222
currently lose_sum: 494.2755987048149
time_elpased: 1.743
batch start
#iterations: 223
currently lose_sum: 495.52706348896027
time_elpased: 1.692
batch start
#iterations: 224
currently lose_sum: 496.11136239767075
time_elpased: 1.82
batch start
#iterations: 225
currently lose_sum: 498.50542399287224
time_elpased: 1.763
batch start
#iterations: 226
currently lose_sum: 495.88876616954803
time_elpased: 1.676
batch start
#iterations: 227
currently lose_sum: 496.68800896406174
time_elpased: 1.676
batch start
#iterations: 228
currently lose_sum: 494.3961006104946
time_elpased: 1.673
batch start
#iterations: 229
currently lose_sum: 497.1081813275814
time_elpased: 1.681
batch start
#iterations: 230
currently lose_sum: 497.01630026102066
time_elpased: 1.696
batch start
#iterations: 231
currently lose_sum: 498.00210267305374
time_elpased: 1.692
batch start
#iterations: 232
currently lose_sum: 496.05888429284096
time_elpased: 1.755
batch start
#iterations: 233
currently lose_sum: 494.41742965579033
time_elpased: 1.729
batch start
#iterations: 234
currently lose_sum: 495.1262390613556
time_elpased: 1.749
batch start
#iterations: 235
currently lose_sum: 494.77655178308487
time_elpased: 1.728
batch start
#iterations: 236
currently lose_sum: 493.66716146469116
time_elpased: 1.734
batch start
#iterations: 237
currently lose_sum: 496.97456988692284
time_elpased: 1.775
batch start
#iterations: 238
currently lose_sum: 493.5131192803383
time_elpased: 1.743
batch start
#iterations: 239
currently lose_sum: 495.48948577046394
time_elpased: 1.688
start validation test
0.0478350515464
1.0
0.0478350515464
0.0913026367572
0
validation finish
batch start
#iterations: 240
currently lose_sum: 494.6582202613354
time_elpased: 1.677
batch start
#iterations: 241
currently lose_sum: 495.29482248425484
time_elpased: 1.672
batch start
#iterations: 242
currently lose_sum: 494.821027725935
time_elpased: 1.67
batch start
#iterations: 243
currently lose_sum: 494.67905086278915
time_elpased: 1.729
batch start
#iterations: 244
currently lose_sum: 497.0760362446308
time_elpased: 1.679
batch start
#iterations: 245
currently lose_sum: 494.11098849773407
time_elpased: 1.681
batch start
#iterations: 246
currently lose_sum: 495.0788245201111
time_elpased: 1.679
batch start
#iterations: 247
currently lose_sum: 495.851780295372
time_elpased: 1.696
batch start
#iterations: 248
currently lose_sum: 494.81149357557297
time_elpased: 1.678
batch start
#iterations: 249
currently lose_sum: 495.96072205901146
time_elpased: 1.689
batch start
#iterations: 250
currently lose_sum: 495.5241850614548
time_elpased: 1.73
batch start
#iterations: 251
currently lose_sum: 496.593220949173
time_elpased: 1.749
batch start
#iterations: 252
currently lose_sum: 497.41613867878914
time_elpased: 1.755
batch start
#iterations: 253
currently lose_sum: 494.76750659942627
time_elpased: 1.691
batch start
#iterations: 254
currently lose_sum: 498.53450003266335
time_elpased: 1.668
batch start
#iterations: 255
currently lose_sum: 495.09018686413765
time_elpased: 1.678
batch start
#iterations: 256
currently lose_sum: 497.44840025901794
time_elpased: 1.665
batch start
#iterations: 257
currently lose_sum: 495.12088918685913
time_elpased: 1.751
batch start
#iterations: 258
currently lose_sum: 495.1006949841976
time_elpased: 1.675
batch start
#iterations: 259
currently lose_sum: 496.44016563892365
time_elpased: 1.668
start validation test
0.226391752577
1.0
0.226391752577
0.369199731002
0
validation finish
batch start
#iterations: 260
currently lose_sum: 495.37919133901596
time_elpased: 1.765
batch start
#iterations: 261
currently lose_sum: 496.4483103454113
time_elpased: 1.717
batch start
#iterations: 262
currently lose_sum: 494.9888899922371
time_elpased: 1.725
batch start
#iterations: 263
currently lose_sum: 495.08129799366
time_elpased: 1.679
batch start
#iterations: 264
currently lose_sum: 493.91751530766487
time_elpased: 1.679
batch start
#iterations: 265
currently lose_sum: 495.7086938023567
time_elpased: 1.693
batch start
#iterations: 266
currently lose_sum: 495.17939427495
time_elpased: 1.67
batch start
#iterations: 267
currently lose_sum: 494.6178042590618
time_elpased: 1.699
batch start
#iterations: 268
currently lose_sum: 494.5144602358341
time_elpased: 1.685
batch start
#iterations: 269
currently lose_sum: 492.72697004675865
time_elpased: 1.733
batch start
#iterations: 270
currently lose_sum: 496.5868166387081
time_elpased: 1.71
batch start
#iterations: 271
currently lose_sum: 493.4611723124981
time_elpased: 1.735
batch start
#iterations: 272
currently lose_sum: 495.4162696301937
time_elpased: 1.681
batch start
#iterations: 273
currently lose_sum: 495.79328310489655
time_elpased: 1.686
batch start
#iterations: 274
currently lose_sum: 497.291759878397
time_elpased: 1.682
batch start
#iterations: 275
currently lose_sum: 494.3720692396164
time_elpased: 1.7
batch start
#iterations: 276
currently lose_sum: 495.2770344913006
time_elpased: 1.689
batch start
#iterations: 277
currently lose_sum: 495.2244032025337
time_elpased: 1.683
batch start
#iterations: 278
currently lose_sum: 493.5539046525955
time_elpased: 1.687
batch start
#iterations: 279
currently lose_sum: 497.32962051033974
time_elpased: 1.737
start validation test
0.229381443299
1.0
0.229381443299
0.373165618449
0
validation finish
batch start
#iterations: 280
currently lose_sum: 493.49334847927094
time_elpased: 1.68
batch start
#iterations: 281
currently lose_sum: 493.9692192077637
time_elpased: 1.71
batch start
#iterations: 282
currently lose_sum: 494.3757490813732
time_elpased: 1.68
batch start
#iterations: 283
currently lose_sum: 496.12939631938934
time_elpased: 1.702
batch start
#iterations: 284
currently lose_sum: 495.47791373729706
time_elpased: 1.714
batch start
#iterations: 285
currently lose_sum: 496.1778826415539
time_elpased: 1.763
batch start
#iterations: 286
currently lose_sum: 495.03042218089104
time_elpased: 1.685
batch start
#iterations: 287
currently lose_sum: 496.83903992176056
time_elpased: 1.703
batch start
#iterations: 288
currently lose_sum: 493.888447701931
time_elpased: 1.752
batch start
#iterations: 289
currently lose_sum: 493.9171392917633
time_elpased: 1.692
batch start
#iterations: 290
currently lose_sum: 492.88455379009247
time_elpased: 1.682
batch start
#iterations: 291
currently lose_sum: 495.20639246702194
time_elpased: 1.68
batch start
#iterations: 292
currently lose_sum: 495.7084890604019
time_elpased: 1.668
batch start
#iterations: 293
currently lose_sum: 496.43727242946625
time_elpased: 1.735
batch start
#iterations: 294
currently lose_sum: 496.1369321346283
time_elpased: 1.677
batch start
#iterations: 295
currently lose_sum: 496.2530669271946
time_elpased: 1.683
batch start
#iterations: 296
currently lose_sum: 494.1010812520981
time_elpased: 1.688
batch start
#iterations: 297
currently lose_sum: 495.24427303671837
time_elpased: 1.695
batch start
#iterations: 298
currently lose_sum: 493.7275238931179
time_elpased: 1.684
batch start
#iterations: 299
currently lose_sum: 495.90185728669167
time_elpased: 1.731
start validation test
0.300412371134
1.0
0.300412371134
0.462026319962
0
validation finish
batch start
#iterations: 300
currently lose_sum: 494.18826174736023
time_elpased: 1.762
batch start
#iterations: 301
currently lose_sum: 494.3244868218899
time_elpased: 1.67
batch start
#iterations: 302
currently lose_sum: 495.8132917881012
time_elpased: 1.734
batch start
#iterations: 303
currently lose_sum: 494.8041771054268
time_elpased: 1.678
batch start
#iterations: 304
currently lose_sum: 494.2541918158531
time_elpased: 1.676
batch start
#iterations: 305
currently lose_sum: 495.20602867007256
time_elpased: 1.675
batch start
#iterations: 306
currently lose_sum: 495.99385648965836
time_elpased: 1.677
batch start
#iterations: 307
currently lose_sum: 496.85015511512756
time_elpased: 1.681
batch start
#iterations: 308
currently lose_sum: 496.24166813492775
time_elpased: 1.75
batch start
#iterations: 309
currently lose_sum: 494.7699474990368
time_elpased: 1.685
batch start
#iterations: 310
currently lose_sum: 492.5605229437351
time_elpased: 1.736
batch start
#iterations: 311
currently lose_sum: 494.1369053721428
time_elpased: 1.683
batch start
#iterations: 312
currently lose_sum: 495.1797961592674
time_elpased: 1.689
batch start
#iterations: 313
currently lose_sum: 495.8442077934742
time_elpased: 1.668
batch start
#iterations: 314
currently lose_sum: 492.3590693771839
time_elpased: 1.672
batch start
#iterations: 315
currently lose_sum: 495.591449290514
time_elpased: 1.669
batch start
#iterations: 316
currently lose_sum: 495.41708290576935
time_elpased: 1.731
batch start
#iterations: 317
currently lose_sum: 493.3566406965256
time_elpased: 1.761
batch start
#iterations: 318
currently lose_sum: 498.20715883374214
time_elpased: 1.689
batch start
#iterations: 319
currently lose_sum: 493.05577954649925
time_elpased: 1.675
start validation test
0.0589690721649
1.0
0.0589690721649
0.111370716511
0
validation finish
batch start
#iterations: 320
currently lose_sum: 495.8311260044575
time_elpased: 1.75
batch start
#iterations: 321
currently lose_sum: 495.4937119483948
time_elpased: 1.736
batch start
#iterations: 322
currently lose_sum: 493.41592997312546
time_elpased: 1.741
batch start
#iterations: 323
currently lose_sum: 493.662670314312
time_elpased: 1.678
batch start
#iterations: 324
currently lose_sum: 497.65706822276115
time_elpased: 1.69
batch start
#iterations: 325
currently lose_sum: 495.11638724803925
time_elpased: 1.768
batch start
#iterations: 326
currently lose_sum: 495.59727108478546
time_elpased: 1.716
batch start
#iterations: 327
currently lose_sum: 495.03209614753723
time_elpased: 1.672
batch start
#iterations: 328
currently lose_sum: 494.15215942263603
time_elpased: 1.696
batch start
#iterations: 329
currently lose_sum: 494.3071917295456
time_elpased: 1.784
batch start
#iterations: 330
currently lose_sum: 496.4377318620682
time_elpased: 1.688
batch start
#iterations: 331
currently lose_sum: 494.72346591949463
time_elpased: 1.692
batch start
#iterations: 332
currently lose_sum: 496.41450133919716
time_elpased: 1.683
batch start
#iterations: 333
currently lose_sum: 494.72126814723015
time_elpased: 1.746
batch start
#iterations: 334
currently lose_sum: 493.7230388522148
time_elpased: 1.713
batch start
#iterations: 335
currently lose_sum: 493.6380629837513
time_elpased: 1.681
batch start
#iterations: 336
currently lose_sum: 495.0681928694248
time_elpased: 1.673
batch start
#iterations: 337
currently lose_sum: 493.6900995373726
time_elpased: 1.724
batch start
#iterations: 338
currently lose_sum: 492.1378838419914
time_elpased: 1.672
batch start
#iterations: 339
currently lose_sum: 495.0668032467365
time_elpased: 1.69
start validation test
0.213298969072
1.0
0.213298969072
0.351601665392
0
validation finish
batch start
#iterations: 340
currently lose_sum: 493.005382835865
time_elpased: 1.74
batch start
#iterations: 341
currently lose_sum: 494.514361590147
time_elpased: 1.704
batch start
#iterations: 342
currently lose_sum: 496.6460667848587
time_elpased: 1.731
batch start
#iterations: 343
currently lose_sum: 496.4049634337425
time_elpased: 1.69
batch start
#iterations: 344
currently lose_sum: 497.4470903277397
time_elpased: 1.774
batch start
#iterations: 345
currently lose_sum: 496.23542308807373
time_elpased: 1.678
batch start
#iterations: 346
currently lose_sum: 494.6091211140156
time_elpased: 1.753
batch start
#iterations: 347
currently lose_sum: 494.7352010309696
time_elpased: 1.782
batch start
#iterations: 348
currently lose_sum: 496.74226811528206
time_elpased: 1.678
batch start
#iterations: 349
currently lose_sum: 495.120394051075
time_elpased: 1.678
batch start
#iterations: 350
currently lose_sum: 495.1895401477814
time_elpased: 1.676
batch start
#iterations: 351
currently lose_sum: 495.1166225671768
time_elpased: 1.722
batch start
#iterations: 352
currently lose_sum: 493.73781552910805
time_elpased: 1.678
batch start
#iterations: 353
currently lose_sum: 496.8869419693947
time_elpased: 1.695
batch start
#iterations: 354
currently lose_sum: 494.7367785871029
time_elpased: 1.746
batch start
#iterations: 355
currently lose_sum: 495.3713876605034
time_elpased: 1.745
batch start
#iterations: 356
currently lose_sum: 491.3968309164047
time_elpased: 1.679
batch start
#iterations: 357
currently lose_sum: 493.0889721810818
time_elpased: 1.761
batch start
#iterations: 358
currently lose_sum: 495.74549371004105
time_elpased: 1.748
batch start
#iterations: 359
currently lose_sum: 492.78383645415306
time_elpased: 1.688
start validation test
0.216288659794
1.0
0.216288659794
0.355653500593
0
validation finish
batch start
#iterations: 360
currently lose_sum: 495.1433599293232
time_elpased: 1.767
batch start
#iterations: 361
currently lose_sum: 493.2467485964298
time_elpased: 1.701
batch start
#iterations: 362
currently lose_sum: 492.9664377570152
time_elpased: 1.671
batch start
#iterations: 363
currently lose_sum: 493.8523714542389
time_elpased: 1.75
batch start
#iterations: 364
currently lose_sum: 495.03693863749504
time_elpased: 1.672
batch start
#iterations: 365
currently lose_sum: 494.85096794366837
time_elpased: 1.732
batch start
#iterations: 366
currently lose_sum: 494.92477706074715
time_elpased: 1.694
batch start
#iterations: 367
currently lose_sum: 494.6899402141571
time_elpased: 1.768
batch start
#iterations: 368
currently lose_sum: 493.03411117196083
time_elpased: 1.683
batch start
#iterations: 369
currently lose_sum: 492.7205668389797
time_elpased: 1.691
batch start
#iterations: 370
currently lose_sum: 493.08003652095795
time_elpased: 1.686
batch start
#iterations: 371
currently lose_sum: 493.85352298617363
time_elpased: 1.755
batch start
#iterations: 372
currently lose_sum: 493.60594177246094
time_elpased: 1.675
batch start
#iterations: 373
currently lose_sum: 492.62445282936096
time_elpased: 1.707
batch start
#iterations: 374
currently lose_sum: 496.28739979863167
time_elpased: 1.675
batch start
#iterations: 375
currently lose_sum: 495.1837959587574
time_elpased: 1.73
batch start
#iterations: 376
currently lose_sum: 495.3678837120533
time_elpased: 1.698
batch start
#iterations: 377
currently lose_sum: 492.40195697546005
time_elpased: 1.723
batch start
#iterations: 378
currently lose_sum: 496.286023914814
time_elpased: 1.673
batch start
#iterations: 379
currently lose_sum: 494.62515956163406
time_elpased: 1.757
start validation test
0.281030927835
1.0
0.281030927835
0.438757444069
0
validation finish
batch start
#iterations: 380
currently lose_sum: 493.2681155204773
time_elpased: 1.718
batch start
#iterations: 381
currently lose_sum: 493.0541664659977
time_elpased: 1.711
batch start
#iterations: 382
currently lose_sum: 493.8007234632969
time_elpased: 1.683
batch start
#iterations: 383
currently lose_sum: 492.3783023059368
time_elpased: 1.702
batch start
#iterations: 384
currently lose_sum: 492.0267376899719
time_elpased: 1.694
batch start
#iterations: 385
currently lose_sum: 492.2055316269398
time_elpased: 1.709
batch start
#iterations: 386
currently lose_sum: 492.7541084289551
time_elpased: 1.771
batch start
#iterations: 387
currently lose_sum: 493.0407050848007
time_elpased: 1.725
batch start
#iterations: 388
currently lose_sum: 493.58286982774734
time_elpased: 1.751
batch start
#iterations: 389
currently lose_sum: 495.1535848379135
time_elpased: 1.758
batch start
#iterations: 390
currently lose_sum: 492.8997586965561
time_elpased: 1.685
batch start
#iterations: 391
currently lose_sum: 496.0629394352436
time_elpased: 1.71
batch start
#iterations: 392
currently lose_sum: 493.66710847616196
time_elpased: 1.766
batch start
#iterations: 393
currently lose_sum: 492.8102760016918
time_elpased: 1.676
batch start
#iterations: 394
currently lose_sum: 493.0125985443592
time_elpased: 1.684
batch start
#iterations: 395
currently lose_sum: 492.55134347081184
time_elpased: 1.686
batch start
#iterations: 396
currently lose_sum: 494.5445616543293
time_elpased: 1.741
batch start
#iterations: 397
currently lose_sum: 496.0875749886036
time_elpased: 1.687
batch start
#iterations: 398
currently lose_sum: 493.1935779750347
time_elpased: 1.784
batch start
#iterations: 399
currently lose_sum: 492.67799097299576
time_elpased: 1.743
start validation test
0.193195876289
1.0
0.193195876289
0.323829272507
0
validation finish
acc: 0.386
pre: 1.000
rec: 0.386
F1: 0.557
auc: 0.000
