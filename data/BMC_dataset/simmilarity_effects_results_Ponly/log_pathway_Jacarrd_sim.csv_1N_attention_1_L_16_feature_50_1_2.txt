start to construct graph
graph construct over
58300
epochs start
batch start
#iterations: 0
currently lose_sum: 401.94839721918106
time_elpased: 1.109
batch start
#iterations: 1
currently lose_sum: 396.05307376384735
time_elpased: 1.087
batch start
#iterations: 2
currently lose_sum: 392.3933096528053
time_elpased: 1.11
batch start
#iterations: 3
currently lose_sum: 388.38654857873917
time_elpased: 1.083
batch start
#iterations: 4
currently lose_sum: 386.89480298757553
time_elpased: 1.086
batch start
#iterations: 5
currently lose_sum: 385.65318751335144
time_elpased: 1.083
batch start
#iterations: 6
currently lose_sum: 383.94171530008316
time_elpased: 1.082
batch start
#iterations: 7
currently lose_sum: 383.33483922481537
time_elpased: 1.09
batch start
#iterations: 8
currently lose_sum: 381.963843524456
time_elpased: 1.082
batch start
#iterations: 9
currently lose_sum: 381.8189033269882
time_elpased: 1.123
batch start
#iterations: 10
currently lose_sum: 379.90719455480576
time_elpased: 1.089
batch start
#iterations: 11
currently lose_sum: 380.18439561128616
time_elpased: 1.15
batch start
#iterations: 12
currently lose_sum: 380.8518332839012
time_elpased: 1.091
batch start
#iterations: 13
currently lose_sum: 380.18839061260223
time_elpased: 1.075
batch start
#iterations: 14
currently lose_sum: 376.81068325042725
time_elpased: 1.088
batch start
#iterations: 15
currently lose_sum: 378.3148614168167
time_elpased: 1.087
batch start
#iterations: 16
currently lose_sum: 377.1173942089081
time_elpased: 1.075
batch start
#iterations: 17
currently lose_sum: 378.28002083301544
time_elpased: 1.108
batch start
#iterations: 18
currently lose_sum: 376.23259484767914
time_elpased: 1.097
batch start
#iterations: 19
currently lose_sum: 377.0677430033684
time_elpased: 1.11
start validation test
0.500721649485
1.0
0.500721649485
0.667307824414
0
validation finish
batch start
#iterations: 20
currently lose_sum: 376.52871465682983
time_elpased: 1.104
batch start
#iterations: 21
currently lose_sum: 376.27630484104156
time_elpased: 1.152
batch start
#iterations: 22
currently lose_sum: 376.8127163052559
time_elpased: 1.078
batch start
#iterations: 23
currently lose_sum: 377.4879521727562
time_elpased: 1.141
batch start
#iterations: 24
currently lose_sum: 376.8397639989853
time_elpased: 1.135
batch start
#iterations: 25
currently lose_sum: 375.59331941604614
time_elpased: 1.093
batch start
#iterations: 26
currently lose_sum: 375.1095403432846
time_elpased: 1.151
batch start
#iterations: 27
currently lose_sum: 375.1248128414154
time_elpased: 1.143
batch start
#iterations: 28
currently lose_sum: 374.13229155540466
time_elpased: 1.148
batch start
#iterations: 29
currently lose_sum: 375.7673845887184
time_elpased: 1.093
batch start
#iterations: 30
currently lose_sum: 376.1939227581024
time_elpased: 1.086
batch start
#iterations: 31
currently lose_sum: 374.9156029820442
time_elpased: 1.068
batch start
#iterations: 32
currently lose_sum: 375.3053572177887
time_elpased: 1.085
batch start
#iterations: 33
currently lose_sum: 375.4569637775421
time_elpased: 1.088
batch start
#iterations: 34
currently lose_sum: 374.82004284858704
time_elpased: 1.132
batch start
#iterations: 35
currently lose_sum: 375.4436659216881
time_elpased: 1.087
batch start
#iterations: 36
currently lose_sum: 374.4195458292961
time_elpased: 1.09
batch start
#iterations: 37
currently lose_sum: 373.7124059200287
time_elpased: 1.081
batch start
#iterations: 38
currently lose_sum: 374.7081256508827
time_elpased: 1.093
batch start
#iterations: 39
currently lose_sum: 374.7536451816559
time_elpased: 1.136
start validation test
0.550927835052
1.0
0.550927835052
0.710449348578
0
validation finish
batch start
#iterations: 40
currently lose_sum: 373.9667501449585
time_elpased: 1.108
batch start
#iterations: 41
currently lose_sum: 374.2111769914627
time_elpased: 1.128
batch start
#iterations: 42
currently lose_sum: 372.80805337429047
time_elpased: 1.083
batch start
#iterations: 43
currently lose_sum: 373.9250391125679
time_elpased: 1.089
batch start
#iterations: 44
currently lose_sum: 374.5496316552162
time_elpased: 1.083
batch start
#iterations: 45
currently lose_sum: 373.0602958202362
time_elpased: 1.106
batch start
#iterations: 46
currently lose_sum: 373.0493493080139
time_elpased: 1.136
batch start
#iterations: 47
currently lose_sum: 374.19865494966507
time_elpased: 1.155
batch start
#iterations: 48
currently lose_sum: 372.8624576330185
time_elpased: 1.075
batch start
#iterations: 49
currently lose_sum: 373.72836750745773
time_elpased: 1.078
batch start
#iterations: 50
currently lose_sum: 372.6369815468788
time_elpased: 1.121
batch start
#iterations: 51
currently lose_sum: 373.79341864585876
time_elpased: 1.079
batch start
#iterations: 52
currently lose_sum: 373.5634749531746
time_elpased: 1.119
batch start
#iterations: 53
currently lose_sum: 373.5615512728691
time_elpased: 1.124
batch start
#iterations: 54
currently lose_sum: 373.5488104224205
time_elpased: 1.08
batch start
#iterations: 55
currently lose_sum: 373.5475044250488
time_elpased: 1.092
batch start
#iterations: 56
currently lose_sum: 373.09787410497665
time_elpased: 1.084
batch start
#iterations: 57
currently lose_sum: 372.9321020245552
time_elpased: 1.092
batch start
#iterations: 58
currently lose_sum: 372.4454159140587
time_elpased: 1.084
batch start
#iterations: 59
currently lose_sum: 373.45981872081757
time_elpased: 1.076
start validation test
0.702989690722
1.0
0.702989690722
0.825594769659
0
validation finish
batch start
#iterations: 60
currently lose_sum: 374.1749702692032
time_elpased: 1.101
batch start
#iterations: 61
currently lose_sum: 372.8384365439415
time_elpased: 1.095
batch start
#iterations: 62
currently lose_sum: 372.0413848757744
time_elpased: 1.096
batch start
#iterations: 63
currently lose_sum: 372.8468222618103
time_elpased: 1.08
batch start
#iterations: 64
currently lose_sum: 372.8266528248787
time_elpased: 1.105
batch start
#iterations: 65
currently lose_sum: 373.04444175958633
time_elpased: 1.138
batch start
#iterations: 66
currently lose_sum: 373.02176707983017
time_elpased: 1.091
batch start
#iterations: 67
currently lose_sum: 372.103755235672
time_elpased: 1.089
batch start
#iterations: 68
currently lose_sum: 372.42805248498917
time_elpased: 1.105
batch start
#iterations: 69
currently lose_sum: 371.8452810049057
time_elpased: 1.089
batch start
#iterations: 70
currently lose_sum: 371.4544662833214
time_elpased: 1.083
batch start
#iterations: 71
currently lose_sum: 371.85724860429764
time_elpased: 1.094
batch start
#iterations: 72
currently lose_sum: 372.58763682842255
time_elpased: 1.077
batch start
#iterations: 73
currently lose_sum: 372.317046046257
time_elpased: 1.093
batch start
#iterations: 74
currently lose_sum: 371.6746523976326
time_elpased: 1.081
batch start
#iterations: 75
currently lose_sum: 371.0169919729233
time_elpased: 1.084
batch start
#iterations: 76
currently lose_sum: 371.5934584736824
time_elpased: 1.148
batch start
#iterations: 77
currently lose_sum: 371.92254465818405
time_elpased: 1.154
batch start
#iterations: 78
currently lose_sum: 372.25283122062683
time_elpased: 1.09
batch start
#iterations: 79
currently lose_sum: 372.8401992917061
time_elpased: 1.087
start validation test
0.706597938144
1.0
0.706597938144
0.828077805968
0
validation finish
batch start
#iterations: 80
currently lose_sum: 370.3123427629471
time_elpased: 1.086
batch start
#iterations: 81
currently lose_sum: 371.7173738479614
time_elpased: 1.087
batch start
#iterations: 82
currently lose_sum: 371.9780232310295
time_elpased: 1.149
batch start
#iterations: 83
currently lose_sum: 371.4408143758774
time_elpased: 1.09
batch start
#iterations: 84
currently lose_sum: 372.7454003095627
time_elpased: 1.088
batch start
#iterations: 85
currently lose_sum: 371.8540389537811
time_elpased: 1.074
batch start
#iterations: 86
currently lose_sum: 371.37908470630646
time_elpased: 1.125
batch start
#iterations: 87
currently lose_sum: 372.43708968162537
time_elpased: 1.085
batch start
#iterations: 88
currently lose_sum: 371.27393358945847
time_elpased: 1.09
batch start
#iterations: 89
currently lose_sum: 371.8647220134735
time_elpased: 1.087
batch start
#iterations: 90
currently lose_sum: 371.745268702507
time_elpased: 1.086
batch start
#iterations: 91
currently lose_sum: 371.7996777296066
time_elpased: 1.107
batch start
#iterations: 92
currently lose_sum: 370.8062726855278
time_elpased: 1.137
batch start
#iterations: 93
currently lose_sum: 371.6836462020874
time_elpased: 1.095
batch start
#iterations: 94
currently lose_sum: 372.03709346055984
time_elpased: 1.084
batch start
#iterations: 95
currently lose_sum: 370.6256550550461
time_elpased: 1.09
batch start
#iterations: 96
currently lose_sum: 371.2732729315758
time_elpased: 1.082
batch start
#iterations: 97
currently lose_sum: 372.4809611439705
time_elpased: 1.131
batch start
#iterations: 98
currently lose_sum: 371.5811847448349
time_elpased: 1.11
batch start
#iterations: 99
currently lose_sum: 370.756711602211
time_elpased: 1.141
start validation test
0.690412371134
1.0
0.690412371134
0.816856742087
0
validation finish
batch start
#iterations: 100
currently lose_sum: 370.0928527712822
time_elpased: 1.094
batch start
#iterations: 101
currently lose_sum: 370.92323738336563
time_elpased: 1.09
batch start
#iterations: 102
currently lose_sum: 370.27164870500565
time_elpased: 1.077
batch start
#iterations: 103
currently lose_sum: 370.9639272093773
time_elpased: 1.092
batch start
#iterations: 104
currently lose_sum: 371.7833940386772
time_elpased: 1.123
batch start
#iterations: 105
currently lose_sum: 371.44420593976974
time_elpased: 1.1
batch start
#iterations: 106
currently lose_sum: 371.3304972052574
time_elpased: 1.093
batch start
#iterations: 107
currently lose_sum: 370.59692829847336
time_elpased: 1.098
batch start
#iterations: 108
currently lose_sum: 371.2512214779854
time_elpased: 1.078
batch start
#iterations: 109
currently lose_sum: 370.6199943423271
time_elpased: 1.077
batch start
#iterations: 110
currently lose_sum: 371.36823785305023
time_elpased: 1.088
batch start
#iterations: 111
currently lose_sum: 369.44338434934616
time_elpased: 1.081
batch start
#iterations: 112
currently lose_sum: 369.96913570165634
time_elpased: 1.102
batch start
#iterations: 113
currently lose_sum: 370.72004252672195
time_elpased: 1.075
batch start
#iterations: 114
currently lose_sum: 370.1142017841339
time_elpased: 1.088
batch start
#iterations: 115
currently lose_sum: 370.7878905534744
time_elpased: 1.092
batch start
#iterations: 116
currently lose_sum: 371.13123893737793
time_elpased: 1.128
batch start
#iterations: 117
currently lose_sum: 370.64606761932373
time_elpased: 1.083
batch start
#iterations: 118
currently lose_sum: 369.14859360456467
time_elpased: 1.155
batch start
#iterations: 119
currently lose_sum: 370.8324722647667
time_elpased: 1.087
start validation test
0.70381443299
1.0
0.70381443299
0.826163248018
0
validation finish
batch start
#iterations: 120
currently lose_sum: 371.0833734869957
time_elpased: 1.09
batch start
#iterations: 121
currently lose_sum: 369.3618628978729
time_elpased: 1.081
batch start
#iterations: 122
currently lose_sum: 368.49376779794693
time_elpased: 1.1
batch start
#iterations: 123
currently lose_sum: 370.0659713745117
time_elpased: 1.1
batch start
#iterations: 124
currently lose_sum: 368.9879961013794
time_elpased: 1.08
batch start
#iterations: 125
currently lose_sum: 371.25144302845
time_elpased: 1.131
batch start
#iterations: 126
currently lose_sum: 371.50936365127563
time_elpased: 1.148
batch start
#iterations: 127
currently lose_sum: 371.05846977233887
time_elpased: 1.093
batch start
#iterations: 128
currently lose_sum: 370.4958875179291
time_elpased: 1.093
batch start
#iterations: 129
currently lose_sum: 370.26709324121475
time_elpased: 1.088
batch start
#iterations: 130
currently lose_sum: 369.8973705172539
time_elpased: 1.149
batch start
#iterations: 131
currently lose_sum: 369.41484892368317
time_elpased: 1.097
batch start
#iterations: 132
currently lose_sum: 370.5525108575821
time_elpased: 1.081
batch start
#iterations: 133
currently lose_sum: 371.1547757387161
time_elpased: 1.1
batch start
#iterations: 134
currently lose_sum: 371.05510926246643
time_elpased: 1.125
batch start
#iterations: 135
currently lose_sum: 370.1172350049019
time_elpased: 1.095
batch start
#iterations: 136
currently lose_sum: 371.2304273247719
time_elpased: 1.087
batch start
#iterations: 137
currently lose_sum: 370.4000981450081
time_elpased: 1.089
batch start
#iterations: 138
currently lose_sum: 370.6417244076729
time_elpased: 1.105
batch start
#iterations: 139
currently lose_sum: 368.6602224111557
time_elpased: 1.142
start validation test
0.715257731959
1.0
0.715257731959
0.833994470489
0
validation finish
batch start
#iterations: 140
currently lose_sum: 370.1622256040573
time_elpased: 1.095
batch start
#iterations: 141
currently lose_sum: 371.1116409897804
time_elpased: 1.111
batch start
#iterations: 142
currently lose_sum: 370.75779926776886
time_elpased: 1.082
batch start
#iterations: 143
currently lose_sum: 369.83850771188736
time_elpased: 1.096
batch start
#iterations: 144
currently lose_sum: 370.79197120666504
time_elpased: 1.147
batch start
#iterations: 145
currently lose_sum: 369.9828956127167
time_elpased: 1.084
batch start
#iterations: 146
currently lose_sum: 369.0465712547302
time_elpased: 1.092
batch start
#iterations: 147
currently lose_sum: 370.4476480484009
time_elpased: 1.095
batch start
#iterations: 148
currently lose_sum: 369.9889848828316
time_elpased: 1.135
batch start
#iterations: 149
currently lose_sum: 369.82471215724945
time_elpased: 1.093
batch start
#iterations: 150
currently lose_sum: 370.19401544332504
time_elpased: 1.074
batch start
#iterations: 151
currently lose_sum: 370.012443959713
time_elpased: 1.148
batch start
#iterations: 152
currently lose_sum: 369.9999802708626
time_elpased: 1.088
batch start
#iterations: 153
currently lose_sum: 371.13462764024734
time_elpased: 1.082
batch start
#iterations: 154
currently lose_sum: 369.9725625514984
time_elpased: 1.079
batch start
#iterations: 155
currently lose_sum: 371.1217465400696
time_elpased: 1.087
batch start
#iterations: 156
currently lose_sum: 370.31899279356
time_elpased: 1.127
batch start
#iterations: 157
currently lose_sum: 369.7116953134537
time_elpased: 1.112
batch start
#iterations: 158
currently lose_sum: 370.80837219953537
time_elpased: 1.1
batch start
#iterations: 159
currently lose_sum: 371.0190851688385
time_elpased: 1.09
start validation test
0.727422680412
1.0
0.727422680412
0.842205777035
0
validation finish
batch start
#iterations: 160
currently lose_sum: 370.71876233816147
time_elpased: 1.086
batch start
#iterations: 161
currently lose_sum: 370.1427302956581
time_elpased: 1.089
batch start
#iterations: 162
currently lose_sum: 369.98938405513763
time_elpased: 1.098
batch start
#iterations: 163
currently lose_sum: 369.59362787008286
time_elpased: 1.091
batch start
#iterations: 164
currently lose_sum: 369.63351184129715
time_elpased: 1.102
batch start
#iterations: 165
currently lose_sum: 369.32442063093185
time_elpased: 1.101
batch start
#iterations: 166
currently lose_sum: 369.62624257802963
time_elpased: 1.106
batch start
#iterations: 167
currently lose_sum: 369.77861309051514
time_elpased: 1.095
batch start
#iterations: 168
currently lose_sum: 369.5952507853508
time_elpased: 1.093
batch start
#iterations: 169
currently lose_sum: 368.88171672821045
time_elpased: 1.104
batch start
#iterations: 170
currently lose_sum: 369.8067481517792
time_elpased: 1.129
batch start
#iterations: 171
currently lose_sum: 370.28799700737
time_elpased: 1.112
batch start
#iterations: 172
currently lose_sum: 369.5186458826065
time_elpased: 1.077
batch start
#iterations: 173
currently lose_sum: 371.8486679196358
time_elpased: 1.094
batch start
#iterations: 174
currently lose_sum: 369.3770499229431
time_elpased: 1.077
batch start
#iterations: 175
currently lose_sum: 370.309259057045
time_elpased: 1.084
batch start
#iterations: 176
currently lose_sum: 368.8261513710022
time_elpased: 1.119
batch start
#iterations: 177
currently lose_sum: 369.082558631897
time_elpased: 1.098
batch start
#iterations: 178
currently lose_sum: 371.3495389223099
time_elpased: 1.091
batch start
#iterations: 179
currently lose_sum: 369.8408327102661
time_elpased: 1.087
start validation test
0.692371134021
1.0
0.692371134021
0.818226120858
0
validation finish
batch start
#iterations: 180
currently lose_sum: 369.25174152851105
time_elpased: 1.133
batch start
#iterations: 181
currently lose_sum: 369.9597113728523
time_elpased: 1.089
batch start
#iterations: 182
currently lose_sum: 369.9889518022537
time_elpased: 1.084
batch start
#iterations: 183
currently lose_sum: 369.359333217144
time_elpased: 1.076
batch start
#iterations: 184
currently lose_sum: 369.28038173913956
time_elpased: 1.114
batch start
#iterations: 185
currently lose_sum: 369.8528226017952
time_elpased: 1.102
batch start
#iterations: 186
currently lose_sum: 368.84244829416275
time_elpased: 1.073
batch start
#iterations: 187
currently lose_sum: 369.18191784620285
time_elpased: 1.09
batch start
#iterations: 188
currently lose_sum: 369.7536654472351
time_elpased: 1.092
batch start
#iterations: 189
currently lose_sum: 369.2131009697914
time_elpased: 1.089
batch start
#iterations: 190
currently lose_sum: 368.29189187288284
time_elpased: 1.082
batch start
#iterations: 191
currently lose_sum: 368.7127289175987
time_elpased: 1.089
batch start
#iterations: 192
currently lose_sum: 370.60214149951935
time_elpased: 1.089
batch start
#iterations: 193
currently lose_sum: 370.4882317185402
time_elpased: 1.086
batch start
#iterations: 194
currently lose_sum: 369.60399091243744
time_elpased: 1.135
batch start
#iterations: 195
currently lose_sum: 369.8518337011337
time_elpased: 1.116
batch start
#iterations: 196
currently lose_sum: 370.59148639440536
time_elpased: 1.108
batch start
#iterations: 197
currently lose_sum: 369.59977316856384
time_elpased: 1.151
batch start
#iterations: 198
currently lose_sum: 370.8395949602127
time_elpased: 1.089
batch start
#iterations: 199
currently lose_sum: 369.5563713312149
time_elpased: 1.146
start validation test
0.68587628866
1.0
0.68587628866
0.81367333211
0
validation finish
batch start
#iterations: 200
currently lose_sum: 370.3546844124794
time_elpased: 1.111
batch start
#iterations: 201
currently lose_sum: 367.97524040937424
time_elpased: 1.171
batch start
#iterations: 202
currently lose_sum: 368.1543056368828
time_elpased: 1.143
batch start
#iterations: 203
currently lose_sum: 368.04033356904984
time_elpased: 1.089
batch start
#iterations: 204
currently lose_sum: 369.92684811353683
time_elpased: 1.097
batch start
#iterations: 205
currently lose_sum: 367.5902879834175
time_elpased: 1.086
batch start
#iterations: 206
currently lose_sum: 369.91314429044724
time_elpased: 1.084
batch start
#iterations: 207
currently lose_sum: 368.9636620879173
time_elpased: 1.143
batch start
#iterations: 208
currently lose_sum: 369.48454082012177
time_elpased: 1.143
batch start
#iterations: 209
currently lose_sum: 368.58993059396744
time_elpased: 1.102
batch start
#iterations: 210
currently lose_sum: 368.2892844080925
time_elpased: 1.1
batch start
#iterations: 211
currently lose_sum: 369.36742627620697
time_elpased: 1.104
batch start
#iterations: 212
currently lose_sum: 369.9422838687897
time_elpased: 1.086
batch start
#iterations: 213
currently lose_sum: 369.94514483213425
time_elpased: 1.094
batch start
#iterations: 214
currently lose_sum: 370.17743372917175
time_elpased: 1.11
batch start
#iterations: 215
currently lose_sum: 369.0275772213936
time_elpased: 1.091
batch start
#iterations: 216
currently lose_sum: 369.4057511687279
time_elpased: 1.104
batch start
#iterations: 217
currently lose_sum: 368.21667301654816
time_elpased: 1.112
batch start
#iterations: 218
currently lose_sum: 369.3781068325043
time_elpased: 1.091
batch start
#iterations: 219
currently lose_sum: 370.00726622343063
time_elpased: 1.117
start validation test
0.681443298969
1.0
0.681443298969
0.810545677498
0
validation finish
batch start
#iterations: 220
currently lose_sum: 369.33216494321823
time_elpased: 1.113
batch start
#iterations: 221
currently lose_sum: 369.68717086315155
time_elpased: 1.116
batch start
#iterations: 222
currently lose_sum: 369.42282742261887
time_elpased: 1.108
batch start
#iterations: 223
currently lose_sum: 369.4068880081177
time_elpased: 1.088
batch start
#iterations: 224
currently lose_sum: 369.2176979780197
time_elpased: 1.104
batch start
#iterations: 225
currently lose_sum: 369.2227432131767
time_elpased: 1.081
batch start
#iterations: 226
currently lose_sum: 368.9857284426689
time_elpased: 1.091
batch start
#iterations: 227
currently lose_sum: 369.22956997156143
time_elpased: 1.1
batch start
#iterations: 228
currently lose_sum: 369.3966787457466
time_elpased: 1.089
batch start
#iterations: 229
currently lose_sum: 368.12522864341736
time_elpased: 1.132
batch start
#iterations: 230
currently lose_sum: 370.02591973543167
time_elpased: 1.1
batch start
#iterations: 231
currently lose_sum: 369.48877996206284
time_elpased: 1.088
batch start
#iterations: 232
currently lose_sum: 368.6972324848175
time_elpased: 1.086
batch start
#iterations: 233
currently lose_sum: 369.72074180841446
time_elpased: 1.131
batch start
#iterations: 234
currently lose_sum: 368.8269539475441
time_elpased: 1.103
batch start
#iterations: 235
currently lose_sum: 368.6285820007324
time_elpased: 1.089
batch start
#iterations: 236
currently lose_sum: 369.03617042303085
time_elpased: 1.103
batch start
#iterations: 237
currently lose_sum: 369.0416597723961
time_elpased: 1.12
batch start
#iterations: 238
currently lose_sum: 368.8413850069046
time_elpased: 1.144
batch start
#iterations: 239
currently lose_sum: 369.2842973470688
time_elpased: 1.094
start validation test
0.692268041237
1.0
0.692268041237
0.818154127323
0
validation finish
batch start
#iterations: 240
currently lose_sum: 368.178811609745
time_elpased: 1.088
batch start
#iterations: 241
currently lose_sum: 368.92804288864136
time_elpased: 1.127
batch start
#iterations: 242
currently lose_sum: 367.66314738988876
time_elpased: 1.095
batch start
#iterations: 243
currently lose_sum: 369.28980177640915
time_elpased: 1.135
batch start
#iterations: 244
currently lose_sum: 369.07037460803986
time_elpased: 1.088
batch start
#iterations: 245
currently lose_sum: 368.72864520549774
time_elpased: 1.137
batch start
#iterations: 246
currently lose_sum: 369.4623718261719
time_elpased: 1.172
batch start
#iterations: 247
currently lose_sum: 368.8433619737625
time_elpased: 1.104
batch start
#iterations: 248
currently lose_sum: 369.69971323013306
time_elpased: 1.087
batch start
#iterations: 249
currently lose_sum: 368.82333278656006
time_elpased: 1.12
batch start
#iterations: 250
currently lose_sum: 369.3415738940239
time_elpased: 1.146
batch start
#iterations: 251
currently lose_sum: 370.32851630449295
time_elpased: 1.097
batch start
#iterations: 252
currently lose_sum: 370.02035546302795
time_elpased: 1.135
batch start
#iterations: 253
currently lose_sum: 368.31353533267975
time_elpased: 1.089
batch start
#iterations: 254
currently lose_sum: 369.97410076856613
time_elpased: 1.11
batch start
#iterations: 255
currently lose_sum: 369.34258538484573
time_elpased: 1.127
batch start
#iterations: 256
currently lose_sum: 369.30404317379
time_elpased: 1.126
batch start
#iterations: 257
currently lose_sum: 369.9343290925026
time_elpased: 1.091
batch start
#iterations: 258
currently lose_sum: 370.8948407173157
time_elpased: 1.088
batch start
#iterations: 259
currently lose_sum: 369.15255361795425
time_elpased: 1.088
start validation test
0.719690721649
1.0
0.719690721649
0.837000179845
0
validation finish
batch start
#iterations: 260
currently lose_sum: 367.6003775000572
time_elpased: 1.118
batch start
#iterations: 261
currently lose_sum: 368.4283994436264
time_elpased: 1.083
batch start
#iterations: 262
currently lose_sum: 368.9963404536247
time_elpased: 1.088
batch start
#iterations: 263
currently lose_sum: 370.1236470937729
time_elpased: 1.109
batch start
#iterations: 264
currently lose_sum: 369.20403069257736
time_elpased: 1.088
batch start
#iterations: 265
currently lose_sum: 369.1053686738014
time_elpased: 1.088
batch start
#iterations: 266
currently lose_sum: 369.1996552348137
time_elpased: 1.085
batch start
#iterations: 267
currently lose_sum: 368.40258252620697
time_elpased: 1.112
batch start
#iterations: 268
currently lose_sum: 368.33004397153854
time_elpased: 1.081
batch start
#iterations: 269
currently lose_sum: 368.8018192052841
time_elpased: 1.087
batch start
#iterations: 270
currently lose_sum: 368.24231404066086
time_elpased: 1.102
batch start
#iterations: 271
currently lose_sum: 368.76468700170517
time_elpased: 1.093
batch start
#iterations: 272
currently lose_sum: 369.7187120914459
time_elpased: 1.095
batch start
#iterations: 273
currently lose_sum: 369.5238387584686
time_elpased: 1.082
batch start
#iterations: 274
currently lose_sum: 369.3555097579956
time_elpased: 1.113
batch start
#iterations: 275
currently lose_sum: 369.9245431423187
time_elpased: 1.097
batch start
#iterations: 276
currently lose_sum: 367.54492300748825
time_elpased: 1.146
batch start
#iterations: 277
currently lose_sum: 368.3435236811638
time_elpased: 1.109
batch start
#iterations: 278
currently lose_sum: 368.8059514760971
time_elpased: 1.092
batch start
#iterations: 279
currently lose_sum: 368.2394562959671
time_elpased: 1.08
start validation test
0.683608247423
1.0
0.683608247423
0.812075194416
0
validation finish
batch start
#iterations: 280
currently lose_sum: 368.5328812599182
time_elpased: 1.084
batch start
#iterations: 281
currently lose_sum: 369.31345212459564
time_elpased: 1.112
batch start
#iterations: 282
currently lose_sum: 369.2457675933838
time_elpased: 1.14
batch start
#iterations: 283
currently lose_sum: 368.4261076450348
time_elpased: 1.138
batch start
#iterations: 284
currently lose_sum: 368.7492488026619
time_elpased: 1.088
batch start
#iterations: 285
currently lose_sum: 367.60458052158356
time_elpased: 1.105
batch start
#iterations: 286
currently lose_sum: 368.70053935050964
time_elpased: 1.086
batch start
#iterations: 287
currently lose_sum: 367.62555539608
time_elpased: 1.119
batch start
#iterations: 288
currently lose_sum: 369.84066385030746
time_elpased: 1.094
batch start
#iterations: 289
currently lose_sum: 367.84673005342484
time_elpased: 1.093
batch start
#iterations: 290
currently lose_sum: 367.79920268058777
time_elpased: 1.085
batch start
#iterations: 291
currently lose_sum: 369.85985934734344
time_elpased: 1.094
batch start
#iterations: 292
currently lose_sum: 368.8876756429672
time_elpased: 1.1
batch start
#iterations: 293
currently lose_sum: 368.7092781662941
time_elpased: 1.09
batch start
#iterations: 294
currently lose_sum: 368.47793132066727
time_elpased: 1.097
batch start
#iterations: 295
currently lose_sum: 367.54307448863983
time_elpased: 1.088
batch start
#iterations: 296
currently lose_sum: 367.54299491643906
time_elpased: 1.097
batch start
#iterations: 297
currently lose_sum: 368.79788345098495
time_elpased: 1.089
batch start
#iterations: 298
currently lose_sum: 367.16957807540894
time_elpased: 1.095
batch start
#iterations: 299
currently lose_sum: 367.64541751146317
time_elpased: 1.094
start validation test
0.715463917526
1.0
0.715463917526
0.834134615385
0
validation finish
batch start
#iterations: 300
currently lose_sum: 367.40933471918106
time_elpased: 1.094
batch start
#iterations: 301
currently lose_sum: 368.93810975551605
time_elpased: 1.118
batch start
#iterations: 302
currently lose_sum: 369.88250106573105
time_elpased: 1.114
batch start
#iterations: 303
currently lose_sum: 370.4814963340759
time_elpased: 1.1
batch start
#iterations: 304
currently lose_sum: 369.0519946217537
time_elpased: 1.121
batch start
#iterations: 305
currently lose_sum: 369.2059061527252
time_elpased: 1.143
batch start
#iterations: 306
currently lose_sum: 367.69954484701157
time_elpased: 1.156
batch start
#iterations: 307
currently lose_sum: 367.85986483097076
time_elpased: 1.1
batch start
#iterations: 308
currently lose_sum: 368.91852498054504
time_elpased: 1.094
batch start
#iterations: 309
currently lose_sum: 369.1346241235733
time_elpased: 1.083
batch start
#iterations: 310
currently lose_sum: 367.2204409837723
time_elpased: 1.124
batch start
#iterations: 311
currently lose_sum: 369.5791364312172
time_elpased: 1.088
batch start
#iterations: 312
currently lose_sum: 367.4019270539284
time_elpased: 1.101
batch start
#iterations: 313
currently lose_sum: 367.65472918748856
time_elpased: 1.118
batch start
#iterations: 314
currently lose_sum: 369.42215740680695
time_elpased: 1.097
batch start
#iterations: 315
currently lose_sum: 369.628568649292
time_elpased: 1.094
batch start
#iterations: 316
currently lose_sum: 366.98299384117126
time_elpased: 1.086
batch start
#iterations: 317
currently lose_sum: 369.2925386428833
time_elpased: 1.139
batch start
#iterations: 318
currently lose_sum: 369.54354548454285
time_elpased: 1.153
batch start
#iterations: 319
currently lose_sum: 368.68218886852264
time_elpased: 1.099
start validation test
0.707628865979
1.0
0.707628865979
0.828785317556
0
validation finish
batch start
#iterations: 320
currently lose_sum: 367.7657586336136
time_elpased: 1.095
batch start
#iterations: 321
currently lose_sum: 369.04670852422714
time_elpased: 1.088
batch start
#iterations: 322
currently lose_sum: 368.6501406431198
time_elpased: 1.101
batch start
#iterations: 323
currently lose_sum: 369.5098791718483
time_elpased: 1.096
batch start
#iterations: 324
currently lose_sum: 368.100224673748
time_elpased: 1.142
batch start
#iterations: 325
currently lose_sum: 370.0134097337723
time_elpased: 1.105
batch start
#iterations: 326
currently lose_sum: 368.0935581922531
time_elpased: 1.083
batch start
#iterations: 327
currently lose_sum: 367.1978048682213
time_elpased: 1.09
batch start
#iterations: 328
currently lose_sum: 368.4342786669731
time_elpased: 1.093
batch start
#iterations: 329
currently lose_sum: 367.8056473135948
time_elpased: 1.141
batch start
#iterations: 330
currently lose_sum: 369.4847196340561
time_elpased: 1.098
batch start
#iterations: 331
currently lose_sum: 367.9301493167877
time_elpased: 1.113
batch start
#iterations: 332
currently lose_sum: 368.6208544373512
time_elpased: 1.13
batch start
#iterations: 333
currently lose_sum: 368.7197769880295
time_elpased: 1.101
batch start
#iterations: 334
currently lose_sum: 368.4120749235153
time_elpased: 1.088
batch start
#iterations: 335
currently lose_sum: 368.63190960884094
time_elpased: 1.089
batch start
#iterations: 336
currently lose_sum: 368.75875633955
time_elpased: 1.101
batch start
#iterations: 337
currently lose_sum: 367.71464598178864
time_elpased: 1.091
batch start
#iterations: 338
currently lose_sum: 369.5167031288147
time_elpased: 1.093
batch start
#iterations: 339
currently lose_sum: 368.42715096473694
time_elpased: 1.11
start validation test
0.689278350515
1.0
0.689278350515
0.816062492372
0
validation finish
batch start
#iterations: 340
currently lose_sum: 368.444339632988
time_elpased: 1.086
batch start
#iterations: 341
currently lose_sum: 368.3448375463486
time_elpased: 1.135
batch start
#iterations: 342
currently lose_sum: 368.0929042696953
time_elpased: 1.091
batch start
#iterations: 343
currently lose_sum: 368.55163836479187
time_elpased: 1.103
batch start
#iterations: 344
currently lose_sum: 367.9653790593147
time_elpased: 1.129
batch start
#iterations: 345
currently lose_sum: 367.8384032845497
time_elpased: 1.117
batch start
#iterations: 346
currently lose_sum: 368.26035475730896
time_elpased: 1.093
batch start
#iterations: 347
currently lose_sum: 368.3813890814781
time_elpased: 1.1
batch start
#iterations: 348
currently lose_sum: 367.13739228248596
time_elpased: 1.093
batch start
#iterations: 349
currently lose_sum: 367.8615049123764
time_elpased: 1.097
batch start
#iterations: 350
currently lose_sum: 367.85281121730804
time_elpased: 1.147
batch start
#iterations: 351
currently lose_sum: 368.77639377117157
time_elpased: 1.095
batch start
#iterations: 352
currently lose_sum: 369.96359050273895
time_elpased: 1.088
batch start
#iterations: 353
currently lose_sum: 367.78466176986694
time_elpased: 1.099
batch start
#iterations: 354
currently lose_sum: 367.98589211702347
time_elpased: 1.15
batch start
#iterations: 355
currently lose_sum: 368.35751444101334
time_elpased: 1.117
batch start
#iterations: 356
currently lose_sum: 367.9084126353264
time_elpased: 1.142
batch start
#iterations: 357
currently lose_sum: 368.71100920438766
time_elpased: 1.098
batch start
#iterations: 358
currently lose_sum: 368.60093075037
time_elpased: 1.095
batch start
#iterations: 359
currently lose_sum: 367.1295091509819
time_elpased: 1.089
start validation test
0.690515463918
1.0
0.690515463918
0.816928893768
0
validation finish
batch start
#iterations: 360
currently lose_sum: 368.68130773305893
time_elpased: 1.105
batch start
#iterations: 361
currently lose_sum: 368.31008249521255
time_elpased: 1.098
batch start
#iterations: 362
currently lose_sum: 367.42479968070984
time_elpased: 1.093
batch start
#iterations: 363
currently lose_sum: 368.8998252749443
time_elpased: 1.137
batch start
#iterations: 364
currently lose_sum: 367.24279123544693
time_elpased: 1.106
batch start
#iterations: 365
currently lose_sum: 368.6029939651489
time_elpased: 1.158
batch start
#iterations: 366
currently lose_sum: 369.4494354724884
time_elpased: 1.088
batch start
#iterations: 367
currently lose_sum: 369.0078536272049
time_elpased: 1.106
batch start
#iterations: 368
currently lose_sum: 368.00204652547836
time_elpased: 1.102
batch start
#iterations: 369
currently lose_sum: 368.0157318711281
time_elpased: 1.102
batch start
#iterations: 370
currently lose_sum: 367.98686373233795
time_elpased: 1.091
batch start
#iterations: 371
currently lose_sum: 368.1124030947685
time_elpased: 1.149
batch start
#iterations: 372
currently lose_sum: 368.1661992073059
time_elpased: 1.094
batch start
#iterations: 373
currently lose_sum: 369.17250496149063
time_elpased: 1.096
batch start
#iterations: 374
currently lose_sum: 367.55186355113983
time_elpased: 1.106
batch start
#iterations: 375
currently lose_sum: 367.02197456359863
time_elpased: 1.09
batch start
#iterations: 376
currently lose_sum: 368.38136488199234
time_elpased: 1.094
batch start
#iterations: 377
currently lose_sum: 368.2331649661064
time_elpased: 1.097
batch start
#iterations: 378
currently lose_sum: 367.99163722991943
time_elpased: 1.089
batch start
#iterations: 379
currently lose_sum: 367.70416736602783
time_elpased: 1.149
start validation test
0.709381443299
1.0
0.709381443299
0.829986128702
0
validation finish
batch start
#iterations: 380
currently lose_sum: 368.1522595882416
time_elpased: 1.097
batch start
#iterations: 381
currently lose_sum: 369.4643823504448
time_elpased: 1.083
batch start
#iterations: 382
currently lose_sum: 367.7435692548752
time_elpased: 1.091
batch start
#iterations: 383
currently lose_sum: 367.7585071325302
time_elpased: 1.135
batch start
#iterations: 384
currently lose_sum: 367.91822427511215
time_elpased: 1.085
batch start
#iterations: 385
currently lose_sum: 367.2307739853859
time_elpased: 1.084
batch start
#iterations: 386
currently lose_sum: 368.3880050778389
time_elpased: 1.095
batch start
#iterations: 387
currently lose_sum: 368.4968957901001
time_elpased: 1.121
batch start
#iterations: 388
currently lose_sum: 368.40544986724854
time_elpased: 1.094
batch start
#iterations: 389
currently lose_sum: 366.74583131074905
time_elpased: 1.15
batch start
#iterations: 390
currently lose_sum: 367.6599179506302
time_elpased: 1.092
batch start
#iterations: 391
currently lose_sum: 367.6250706911087
time_elpased: 1.083
batch start
#iterations: 392
currently lose_sum: 367.79841178655624
time_elpased: 1.078
batch start
#iterations: 393
currently lose_sum: 369.03848028182983
time_elpased: 1.097
batch start
#iterations: 394
currently lose_sum: 368.4293588399887
time_elpased: 1.138
batch start
#iterations: 395
currently lose_sum: 368.38248509168625
time_elpased: 1.107
batch start
#iterations: 396
currently lose_sum: 368.62884825468063
time_elpased: 1.096
batch start
#iterations: 397
currently lose_sum: 367.7961300611496
time_elpased: 1.104
batch start
#iterations: 398
currently lose_sum: 368.6797490119934
time_elpased: 1.11
batch start
#iterations: 399
currently lose_sum: 366.42874401807785
time_elpased: 1.088
start validation test
0.701546391753
1.0
0.701546391753
0.824598606483
0
validation finish
acc: 0.724
pre: 1.000
rec: 0.724
F1: 0.840
auc: 0.000
