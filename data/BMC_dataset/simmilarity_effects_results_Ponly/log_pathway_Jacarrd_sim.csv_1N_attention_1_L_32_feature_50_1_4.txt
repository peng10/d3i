start to construct graph
graph construct over
58302
epochs start
batch start
#iterations: 0
currently lose_sum: 401.4613023996353
time_elpased: 1.168
batch start
#iterations: 1
currently lose_sum: 395.43528008461
time_elpased: 1.138
batch start
#iterations: 2
currently lose_sum: 390.2661395072937
time_elpased: 1.117
batch start
#iterations: 3
currently lose_sum: 388.43661242723465
time_elpased: 1.122
batch start
#iterations: 4
currently lose_sum: 386.0996473431587
time_elpased: 1.112
batch start
#iterations: 5
currently lose_sum: 384.3160921931267
time_elpased: 1.119
batch start
#iterations: 6
currently lose_sum: 382.07090026140213
time_elpased: 1.096
batch start
#iterations: 7
currently lose_sum: 379.33557575941086
time_elpased: 1.161
batch start
#iterations: 8
currently lose_sum: 380.5860968232155
time_elpased: 1.115
batch start
#iterations: 9
currently lose_sum: 380.28806042671204
time_elpased: 1.102
batch start
#iterations: 10
currently lose_sum: 378.53629302978516
time_elpased: 1.132
batch start
#iterations: 11
currently lose_sum: 378.6606523990631
time_elpased: 1.115
batch start
#iterations: 12
currently lose_sum: 378.6169202923775
time_elpased: 1.108
batch start
#iterations: 13
currently lose_sum: 378.2958901524544
time_elpased: 1.104
batch start
#iterations: 14
currently lose_sum: 377.812725007534
time_elpased: 1.114
batch start
#iterations: 15
currently lose_sum: 376.517995595932
time_elpased: 1.124
batch start
#iterations: 16
currently lose_sum: 376.4416736960411
time_elpased: 1.106
batch start
#iterations: 17
currently lose_sum: 375.613417327404
time_elpased: 1.11
batch start
#iterations: 18
currently lose_sum: 375.25978487730026
time_elpased: 1.113
batch start
#iterations: 19
currently lose_sum: 376.39426028728485
time_elpased: 1.121
start validation test
0.746288659794
1.0
0.746288659794
0.85471397367
0
validation finish
batch start
#iterations: 20
currently lose_sum: 375.3623480796814
time_elpased: 1.102
batch start
#iterations: 21
currently lose_sum: 375.2675236463547
time_elpased: 1.117
batch start
#iterations: 22
currently lose_sum: 373.5392589569092
time_elpased: 1.114
batch start
#iterations: 23
currently lose_sum: 374.7406653165817
time_elpased: 1.136
batch start
#iterations: 24
currently lose_sum: 374.1211823821068
time_elpased: 1.116
batch start
#iterations: 25
currently lose_sum: 373.71690660715103
time_elpased: 1.119
batch start
#iterations: 26
currently lose_sum: 374.0442452430725
time_elpased: 1.13
batch start
#iterations: 27
currently lose_sum: 374.24405002593994
time_elpased: 1.112
batch start
#iterations: 28
currently lose_sum: 372.3324359059334
time_elpased: 1.163
batch start
#iterations: 29
currently lose_sum: 373.3788058757782
time_elpased: 1.11
batch start
#iterations: 30
currently lose_sum: 372.4631981253624
time_elpased: 1.129
batch start
#iterations: 31
currently lose_sum: 374.20550870895386
time_elpased: 1.12
batch start
#iterations: 32
currently lose_sum: 373.4404020309448
time_elpased: 1.123
batch start
#iterations: 33
currently lose_sum: 372.50271117687225
time_elpased: 1.126
batch start
#iterations: 34
currently lose_sum: 371.88730931282043
time_elpased: 1.102
batch start
#iterations: 35
currently lose_sum: 371.29902333021164
time_elpased: 1.112
batch start
#iterations: 36
currently lose_sum: 372.6121575832367
time_elpased: 1.162
batch start
#iterations: 37
currently lose_sum: 372.47343999147415
time_elpased: 1.131
batch start
#iterations: 38
currently lose_sum: 372.674124956131
time_elpased: 1.122
batch start
#iterations: 39
currently lose_sum: 372.20746529102325
time_elpased: 1.107
start validation test
0.778453608247
1.0
0.778453608247
0.875427511449
0
validation finish
batch start
#iterations: 40
currently lose_sum: 371.94560647010803
time_elpased: 1.111
batch start
#iterations: 41
currently lose_sum: 372.5715802907944
time_elpased: 1.121
batch start
#iterations: 42
currently lose_sum: 372.50137293338776
time_elpased: 1.165
batch start
#iterations: 43
currently lose_sum: 371.4717761874199
time_elpased: 1.123
batch start
#iterations: 44
currently lose_sum: 370.5442823767662
time_elpased: 1.111
batch start
#iterations: 45
currently lose_sum: 372.53681242465973
time_elpased: 1.109
batch start
#iterations: 46
currently lose_sum: 372.11400055885315
time_elpased: 1.113
batch start
#iterations: 47
currently lose_sum: 372.5244593024254
time_elpased: 1.115
batch start
#iterations: 48
currently lose_sum: 371.99213391542435
time_elpased: 1.129
batch start
#iterations: 49
currently lose_sum: 372.24130564928055
time_elpased: 1.153
batch start
#iterations: 50
currently lose_sum: 371.45737713575363
time_elpased: 1.123
batch start
#iterations: 51
currently lose_sum: 371.5025880932808
time_elpased: 1.103
batch start
#iterations: 52
currently lose_sum: 371.919127702713
time_elpased: 1.12
batch start
#iterations: 53
currently lose_sum: 371.2360568642616
time_elpased: 1.147
batch start
#iterations: 54
currently lose_sum: 371.71741968393326
time_elpased: 1.122
batch start
#iterations: 55
currently lose_sum: 371.13376688957214
time_elpased: 1.139
batch start
#iterations: 56
currently lose_sum: 370.5741235613823
time_elpased: 1.11
batch start
#iterations: 57
currently lose_sum: 370.1890648007393
time_elpased: 1.111
batch start
#iterations: 58
currently lose_sum: 371.54868322610855
time_elpased: 1.111
batch start
#iterations: 59
currently lose_sum: 371.68899911642075
time_elpased: 1.153
start validation test
0.75587628866
1.0
0.75587628866
0.860967590418
0
validation finish
batch start
#iterations: 60
currently lose_sum: 371.0989805459976
time_elpased: 1.181
batch start
#iterations: 61
currently lose_sum: 371.9518587589264
time_elpased: 1.136
batch start
#iterations: 62
currently lose_sum: 371.0509131550789
time_elpased: 1.119
batch start
#iterations: 63
currently lose_sum: 370.99182373285294
time_elpased: 1.148
batch start
#iterations: 64
currently lose_sum: 370.5840588212013
time_elpased: 1.125
batch start
#iterations: 65
currently lose_sum: 369.6429650783539
time_elpased: 1.168
batch start
#iterations: 66
currently lose_sum: 371.2566282749176
time_elpased: 1.125
batch start
#iterations: 67
currently lose_sum: 370.8587513566017
time_elpased: 1.112
batch start
#iterations: 68
currently lose_sum: 370.20682644844055
time_elpased: 1.124
batch start
#iterations: 69
currently lose_sum: 370.62804651260376
time_elpased: 1.149
batch start
#iterations: 70
currently lose_sum: 371.1365545988083
time_elpased: 1.121
batch start
#iterations: 71
currently lose_sum: 371.3639923930168
time_elpased: 1.175
batch start
#iterations: 72
currently lose_sum: 368.5775052309036
time_elpased: 1.107
batch start
#iterations: 73
currently lose_sum: 370.9448166489601
time_elpased: 1.119
batch start
#iterations: 74
currently lose_sum: 370.09317618608475
time_elpased: 1.168
batch start
#iterations: 75
currently lose_sum: 369.8346100449562
time_elpased: 1.162
batch start
#iterations: 76
currently lose_sum: 371.22200137376785
time_elpased: 1.115
batch start
#iterations: 77
currently lose_sum: 370.3543298840523
time_elpased: 1.104
batch start
#iterations: 78
currently lose_sum: 370.28665179014206
time_elpased: 1.132
batch start
#iterations: 79
currently lose_sum: 369.16117799282074
time_elpased: 1.152
start validation test
0.74793814433
1.0
0.74793814433
0.855794750811
0
validation finish
batch start
#iterations: 80
currently lose_sum: 371.28712099790573
time_elpased: 1.111
batch start
#iterations: 81
currently lose_sum: 371.34878504276276
time_elpased: 1.112
batch start
#iterations: 82
currently lose_sum: 369.87594813108444
time_elpased: 1.171
batch start
#iterations: 83
currently lose_sum: 369.50227868556976
time_elpased: 1.144
batch start
#iterations: 84
currently lose_sum: 370.8423354625702
time_elpased: 1.121
batch start
#iterations: 85
currently lose_sum: 370.69465655088425
time_elpased: 1.119
batch start
#iterations: 86
currently lose_sum: 369.6126806139946
time_elpased: 1.129
batch start
#iterations: 87
currently lose_sum: 370.36098021268845
time_elpased: 1.102
batch start
#iterations: 88
currently lose_sum: 370.2632495164871
time_elpased: 1.112
batch start
#iterations: 89
currently lose_sum: 369.55360984802246
time_elpased: 1.159
batch start
#iterations: 90
currently lose_sum: 370.2653120160103
time_elpased: 1.116
batch start
#iterations: 91
currently lose_sum: 369.6515363454819
time_elpased: 1.115
batch start
#iterations: 92
currently lose_sum: 371.0312840938568
time_elpased: 1.143
batch start
#iterations: 93
currently lose_sum: 369.4000464081764
time_elpased: 1.12
batch start
#iterations: 94
currently lose_sum: 370.43713241815567
time_elpased: 1.11
batch start
#iterations: 95
currently lose_sum: 369.23603743314743
time_elpased: 1.12
batch start
#iterations: 96
currently lose_sum: 371.2103068828583
time_elpased: 1.116
batch start
#iterations: 97
currently lose_sum: 369.54719972610474
time_elpased: 1.154
batch start
#iterations: 98
currently lose_sum: 370.1443418264389
time_elpased: 1.142
batch start
#iterations: 99
currently lose_sum: 369.1998263001442
time_elpased: 1.11
start validation test
0.733195876289
1.0
0.733195876289
0.846062336426
0
validation finish
batch start
#iterations: 100
currently lose_sum: 369.7813346982002
time_elpased: 1.117
batch start
#iterations: 101
currently lose_sum: 371.1167889237404
time_elpased: 1.114
batch start
#iterations: 102
currently lose_sum: 369.7911614179611
time_elpased: 1.164
batch start
#iterations: 103
currently lose_sum: 370.12809520959854
time_elpased: 1.166
batch start
#iterations: 104
currently lose_sum: 368.83321738243103
time_elpased: 1.105
batch start
#iterations: 105
currently lose_sum: 370.40288293361664
time_elpased: 1.103
batch start
#iterations: 106
currently lose_sum: 369.1809123158455
time_elpased: 1.157
batch start
#iterations: 107
currently lose_sum: 370.0297989845276
time_elpased: 1.111
batch start
#iterations: 108
currently lose_sum: 370.20219469070435
time_elpased: 1.111
batch start
#iterations: 109
currently lose_sum: 369.07698476314545
time_elpased: 1.155
batch start
#iterations: 110
currently lose_sum: 370.0812777876854
time_elpased: 1.166
batch start
#iterations: 111
currently lose_sum: 368.29400020837784
time_elpased: 1.115
batch start
#iterations: 112
currently lose_sum: 369.56613516807556
time_elpased: 1.155
batch start
#iterations: 113
currently lose_sum: 369.75373911857605
time_elpased: 1.119
batch start
#iterations: 114
currently lose_sum: 369.29448890686035
time_elpased: 1.118
batch start
#iterations: 115
currently lose_sum: 367.49654322862625
time_elpased: 1.107
batch start
#iterations: 116
currently lose_sum: 369.18461936712265
time_elpased: 1.12
batch start
#iterations: 117
currently lose_sum: 369.4903886318207
time_elpased: 1.114
batch start
#iterations: 118
currently lose_sum: 368.9126527309418
time_elpased: 1.112
batch start
#iterations: 119
currently lose_sum: 370.2373397946358
time_elpased: 1.118
start validation test
0.759278350515
1.0
0.759278350515
0.863170231468
0
validation finish
batch start
#iterations: 120
currently lose_sum: 368.90624529123306
time_elpased: 1.122
batch start
#iterations: 121
currently lose_sum: 368.8432391881943
time_elpased: 1.117
batch start
#iterations: 122
currently lose_sum: 369.5922483801842
time_elpased: 1.147
batch start
#iterations: 123
currently lose_sum: 368.22965079545975
time_elpased: 1.143
batch start
#iterations: 124
currently lose_sum: 369.27828258275986
time_elpased: 1.116
batch start
#iterations: 125
currently lose_sum: 368.1596145629883
time_elpased: 1.112
batch start
#iterations: 126
currently lose_sum: 368.78180754184723
time_elpased: 1.112
batch start
#iterations: 127
currently lose_sum: 369.2992824316025
time_elpased: 1.154
batch start
#iterations: 128
currently lose_sum: 369.0491217970848
time_elpased: 1.114
batch start
#iterations: 129
currently lose_sum: 369.09883964061737
time_elpased: 1.155
batch start
#iterations: 130
currently lose_sum: 367.57780408859253
time_elpased: 1.114
batch start
#iterations: 131
currently lose_sum: 369.08513897657394
time_elpased: 1.118
batch start
#iterations: 132
currently lose_sum: 368.63850605487823
time_elpased: 1.115
batch start
#iterations: 133
currently lose_sum: 368.3037405014038
time_elpased: 1.119
batch start
#iterations: 134
currently lose_sum: 368.5146707892418
time_elpased: 1.159
batch start
#iterations: 135
currently lose_sum: 368.93013697862625
time_elpased: 1.162
batch start
#iterations: 136
currently lose_sum: 367.8881577849388
time_elpased: 1.113
batch start
#iterations: 137
currently lose_sum: 368.5937268733978
time_elpased: 1.155
batch start
#iterations: 138
currently lose_sum: 368.67021083831787
time_elpased: 1.185
batch start
#iterations: 139
currently lose_sum: 369.34295868873596
time_elpased: 1.114
start validation test
0.759587628866
1.0
0.759587628866
0.863370049215
0
validation finish
batch start
#iterations: 140
currently lose_sum: 368.09257608652115
time_elpased: 1.151
batch start
#iterations: 141
currently lose_sum: 368.7732952237129
time_elpased: 1.108
batch start
#iterations: 142
currently lose_sum: 367.8505007624626
time_elpased: 1.165
batch start
#iterations: 143
currently lose_sum: 368.3050025701523
time_elpased: 1.115
batch start
#iterations: 144
currently lose_sum: 368.97540640830994
time_elpased: 1.111
batch start
#iterations: 145
currently lose_sum: 368.76045870780945
time_elpased: 1.124
batch start
#iterations: 146
currently lose_sum: 368.7158332467079
time_elpased: 1.114
batch start
#iterations: 147
currently lose_sum: 368.41239270567894
time_elpased: 1.162
batch start
#iterations: 148
currently lose_sum: 369.55637085437775
time_elpased: 1.121
batch start
#iterations: 149
currently lose_sum: 367.60086196660995
time_elpased: 1.116
batch start
#iterations: 150
currently lose_sum: 369.3723748922348
time_elpased: 1.159
batch start
#iterations: 151
currently lose_sum: 367.8991257548332
time_elpased: 1.138
batch start
#iterations: 152
currently lose_sum: 368.0484842658043
time_elpased: 1.156
batch start
#iterations: 153
currently lose_sum: 367.3428592681885
time_elpased: 1.186
batch start
#iterations: 154
currently lose_sum: 366.79616129398346
time_elpased: 1.195
batch start
#iterations: 155
currently lose_sum: 368.24455177783966
time_elpased: 1.123
batch start
#iterations: 156
currently lose_sum: 367.6539394259453
time_elpased: 1.127
batch start
#iterations: 157
currently lose_sum: 367.5451669096947
time_elpased: 1.125
batch start
#iterations: 158
currently lose_sum: 366.03572314977646
time_elpased: 1.161
batch start
#iterations: 159
currently lose_sum: 367.9839777946472
time_elpased: 1.121
start validation test
0.794845360825
1.0
0.794845360825
0.885697874785
0
validation finish
batch start
#iterations: 160
currently lose_sum: 369.02009904384613
time_elpased: 1.105
batch start
#iterations: 161
currently lose_sum: 367.7155826687813
time_elpased: 1.131
batch start
#iterations: 162
currently lose_sum: 367.9195578098297
time_elpased: 1.125
batch start
#iterations: 163
currently lose_sum: 368.71517211198807
time_elpased: 1.135
batch start
#iterations: 164
currently lose_sum: 368.20026808977127
time_elpased: 1.121
batch start
#iterations: 165
currently lose_sum: 368.4132688641548
time_elpased: 1.111
batch start
#iterations: 166
currently lose_sum: 367.85430002212524
time_elpased: 1.152
batch start
#iterations: 167
currently lose_sum: 367.5097787976265
time_elpased: 1.17
batch start
#iterations: 168
currently lose_sum: 367.55830705165863
time_elpased: 1.154
batch start
#iterations: 169
currently lose_sum: 367.294852912426
time_elpased: 1.125
batch start
#iterations: 170
currently lose_sum: 367.74528127908707
time_elpased: 1.124
batch start
#iterations: 171
currently lose_sum: 368.0764944553375
time_elpased: 1.115
batch start
#iterations: 172
currently lose_sum: 366.48196256160736
time_elpased: 1.135
batch start
#iterations: 173
currently lose_sum: 368.40013802051544
time_elpased: 1.177
batch start
#iterations: 174
currently lose_sum: 367.7333000898361
time_elpased: 1.153
batch start
#iterations: 175
currently lose_sum: 368.2508100271225
time_elpased: 1.133
batch start
#iterations: 176
currently lose_sum: 367.64719927310944
time_elpased: 1.116
batch start
#iterations: 177
currently lose_sum: 367.8031842112541
time_elpased: 1.16
batch start
#iterations: 178
currently lose_sum: 368.4926221370697
time_elpased: 1.126
batch start
#iterations: 179
currently lose_sum: 368.6211134791374
time_elpased: 1.179
start validation test
0.777628865979
1.0
0.777628865979
0.874905758859
0
validation finish
batch start
#iterations: 180
currently lose_sum: 366.64959877729416
time_elpased: 1.127
batch start
#iterations: 181
currently lose_sum: 366.64071691036224
time_elpased: 1.133
batch start
#iterations: 182
currently lose_sum: 367.9728299379349
time_elpased: 1.16
batch start
#iterations: 183
currently lose_sum: 367.09755885601044
time_elpased: 1.137
batch start
#iterations: 184
currently lose_sum: 367.645541369915
time_elpased: 1.124
batch start
#iterations: 185
currently lose_sum: 366.77824342250824
time_elpased: 1.171
batch start
#iterations: 186
currently lose_sum: 366.88940793275833
time_elpased: 1.112
batch start
#iterations: 187
currently lose_sum: 367.2970016002655
time_elpased: 1.117
batch start
#iterations: 188
currently lose_sum: 367.13530015945435
time_elpased: 1.168
batch start
#iterations: 189
currently lose_sum: 368.3002278804779
time_elpased: 1.148
batch start
#iterations: 190
currently lose_sum: 367.30261570215225
time_elpased: 1.133
batch start
#iterations: 191
currently lose_sum: 368.01923620700836
time_elpased: 1.111
batch start
#iterations: 192
currently lose_sum: 367.39924252033234
time_elpased: 1.124
batch start
#iterations: 193
currently lose_sum: 366.4885039329529
time_elpased: 1.117
batch start
#iterations: 194
currently lose_sum: 366.6478979587555
time_elpased: 1.154
batch start
#iterations: 195
currently lose_sum: 366.47587168216705
time_elpased: 1.153
batch start
#iterations: 196
currently lose_sum: 369.04618138074875
time_elpased: 1.133
batch start
#iterations: 197
currently lose_sum: 368.73138719797134
time_elpased: 1.187
batch start
#iterations: 198
currently lose_sum: 367.1827509999275
time_elpased: 1.169
batch start
#iterations: 199
currently lose_sum: 368.3089131116867
time_elpased: 1.175
start validation test
0.771030927835
1.0
0.771030927835
0.870714244135
0
validation finish
batch start
#iterations: 200
currently lose_sum: 367.9643166065216
time_elpased: 1.136
batch start
#iterations: 201
currently lose_sum: 367.0319355726242
time_elpased: 1.127
batch start
#iterations: 202
currently lose_sum: 366.81195056438446
time_elpased: 1.118
batch start
#iterations: 203
currently lose_sum: 367.29917788505554
time_elpased: 1.113
batch start
#iterations: 204
currently lose_sum: 368.3792772889137
time_elpased: 1.158
batch start
#iterations: 205
currently lose_sum: 366.9891387820244
time_elpased: 1.135
batch start
#iterations: 206
currently lose_sum: 367.37651830911636
time_elpased: 1.118
batch start
#iterations: 207
currently lose_sum: 367.1911962032318
time_elpased: 1.19
batch start
#iterations: 208
currently lose_sum: 368.7803872227669
time_elpased: 1.118
batch start
#iterations: 209
currently lose_sum: 366.61799401044846
time_elpased: 1.126
batch start
#iterations: 210
currently lose_sum: 368.0363051891327
time_elpased: 1.108
batch start
#iterations: 211
currently lose_sum: 367.68793708086014
time_elpased: 1.177
batch start
#iterations: 212
currently lose_sum: 366.91330057382584
time_elpased: 1.15
batch start
#iterations: 213
currently lose_sum: 366.6609593629837
time_elpased: 1.162
batch start
#iterations: 214
currently lose_sum: 367.7241401076317
time_elpased: 1.139
batch start
#iterations: 215
currently lose_sum: 367.8049075603485
time_elpased: 1.167
batch start
#iterations: 216
currently lose_sum: 366.6039634346962
time_elpased: 1.124
batch start
#iterations: 217
currently lose_sum: 368.1615099310875
time_elpased: 1.168
batch start
#iterations: 218
currently lose_sum: 366.6356317996979
time_elpased: 1.167
batch start
#iterations: 219
currently lose_sum: 367.4350504875183
time_elpased: 1.131
start validation test
0.766597938144
1.0
0.766597938144
0.867880485528
0
validation finish
batch start
#iterations: 220
currently lose_sum: 368.2956681251526
time_elpased: 1.117
batch start
#iterations: 221
currently lose_sum: 367.2658643126488
time_elpased: 1.129
batch start
#iterations: 222
currently lose_sum: 367.07260543107986
time_elpased: 1.127
batch start
#iterations: 223
currently lose_sum: 364.92607992887497
time_elpased: 1.134
batch start
#iterations: 224
currently lose_sum: 366.95686942338943
time_elpased: 1.136
batch start
#iterations: 225
currently lose_sum: 367.81258434057236
time_elpased: 1.154
batch start
#iterations: 226
currently lose_sum: 367.0682523846626
time_elpased: 1.145
batch start
#iterations: 227
currently lose_sum: 366.7534661889076
time_elpased: 1.115
batch start
#iterations: 228
currently lose_sum: 367.69263702630997
time_elpased: 1.148
batch start
#iterations: 229
currently lose_sum: 367.26203948259354
time_elpased: 1.12
batch start
#iterations: 230
currently lose_sum: 368.3179857134819
time_elpased: 1.113
batch start
#iterations: 231
currently lose_sum: 367.71788889169693
time_elpased: 1.124
batch start
#iterations: 232
currently lose_sum: 368.0723162293434
time_elpased: 1.124
batch start
#iterations: 233
currently lose_sum: 367.21376144886017
time_elpased: 1.112
batch start
#iterations: 234
currently lose_sum: 366.0833342075348
time_elpased: 1.11
batch start
#iterations: 235
currently lose_sum: 367.53882813453674
time_elpased: 1.118
batch start
#iterations: 236
currently lose_sum: 367.24551635980606
time_elpased: 1.163
batch start
#iterations: 237
currently lose_sum: 367.1884388923645
time_elpased: 1.149
batch start
#iterations: 238
currently lose_sum: 367.76043850183487
time_elpased: 1.104
batch start
#iterations: 239
currently lose_sum: 368.69609439373016
time_elpased: 1.117
start validation test
0.78412371134
1.0
0.78412371134
0.879001502369
0
validation finish
batch start
#iterations: 240
currently lose_sum: 368.7707580924034
time_elpased: 1.137
batch start
#iterations: 241
currently lose_sum: 366.8756334781647
time_elpased: 1.142
batch start
#iterations: 242
currently lose_sum: 365.89817202091217
time_elpased: 1.159
batch start
#iterations: 243
currently lose_sum: 368.4449517726898
time_elpased: 1.162
batch start
#iterations: 244
currently lose_sum: 367.1150195002556
time_elpased: 1.122
batch start
#iterations: 245
currently lose_sum: 367.3064418435097
time_elpased: 1.115
batch start
#iterations: 246
currently lose_sum: 367.4426528811455
time_elpased: 1.116
batch start
#iterations: 247
currently lose_sum: 367.52320927381516
time_elpased: 1.11
batch start
#iterations: 248
currently lose_sum: 366.88625848293304
time_elpased: 1.114
batch start
#iterations: 249
currently lose_sum: 366.5737289786339
time_elpased: 1.18
batch start
#iterations: 250
currently lose_sum: 366.4674643278122
time_elpased: 1.13
batch start
#iterations: 251
currently lose_sum: 368.3729290962219
time_elpased: 1.122
batch start
#iterations: 252
currently lose_sum: 367.831957757473
time_elpased: 1.135
batch start
#iterations: 253
currently lose_sum: 366.72350293397903
time_elpased: 1.174
batch start
#iterations: 254
currently lose_sum: 366.89086532592773
time_elpased: 1.12
batch start
#iterations: 255
currently lose_sum: 367.473945915699
time_elpased: 1.15
batch start
#iterations: 256
currently lose_sum: 368.0135149359703
time_elpased: 1.131
batch start
#iterations: 257
currently lose_sum: 367.81152641773224
time_elpased: 1.126
batch start
#iterations: 258
currently lose_sum: 367.1043669581413
time_elpased: 1.123
batch start
#iterations: 259
currently lose_sum: 365.83541399240494
time_elpased: 1.169
start validation test
0.763402061856
1.0
0.763402061856
0.865828705057
0
validation finish
batch start
#iterations: 260
currently lose_sum: 367.15314614772797
time_elpased: 1.129
batch start
#iterations: 261
currently lose_sum: 367.60984522104263
time_elpased: 1.109
batch start
#iterations: 262
currently lose_sum: 367.4891737103462
time_elpased: 1.154
batch start
#iterations: 263
currently lose_sum: 366.5675987005234
time_elpased: 1.13
batch start
#iterations: 264
currently lose_sum: 365.82386589050293
time_elpased: 1.121
batch start
#iterations: 265
currently lose_sum: 367.2786893248558
time_elpased: 1.162
batch start
#iterations: 266
currently lose_sum: 366.6809051036835
time_elpased: 1.135
batch start
#iterations: 267
currently lose_sum: 368.02577406167984
time_elpased: 1.148
batch start
#iterations: 268
currently lose_sum: 365.4660459756851
time_elpased: 1.114
batch start
#iterations: 269
currently lose_sum: 366.4105226993561
time_elpased: 1.155
batch start
#iterations: 270
currently lose_sum: 365.8201035261154
time_elpased: 1.142
batch start
#iterations: 271
currently lose_sum: 366.191315472126
time_elpased: 1.161
batch start
#iterations: 272
currently lose_sum: 367.4558721780777
time_elpased: 1.144
batch start
#iterations: 273
currently lose_sum: 366.291530251503
time_elpased: 1.117
batch start
#iterations: 274
currently lose_sum: 367.629339158535
time_elpased: 1.183
batch start
#iterations: 275
currently lose_sum: 366.79411178827286
time_elpased: 1.133
batch start
#iterations: 276
currently lose_sum: 367.5593572258949
time_elpased: 1.154
batch start
#iterations: 277
currently lose_sum: 366.63858503103256
time_elpased: 1.115
batch start
#iterations: 278
currently lose_sum: 367.69310545921326
time_elpased: 1.126
batch start
#iterations: 279
currently lose_sum: 366.3390014767647
time_elpased: 1.121
start validation test
0.779587628866
1.0
0.779587628866
0.876144131619
0
validation finish
batch start
#iterations: 280
currently lose_sum: 365.6814026236534
time_elpased: 1.153
batch start
#iterations: 281
currently lose_sum: 366.19033390283585
time_elpased: 1.186
batch start
#iterations: 282
currently lose_sum: 367.1953563094139
time_elpased: 1.122
batch start
#iterations: 283
currently lose_sum: 368.10975724458694
time_elpased: 1.116
batch start
#iterations: 284
currently lose_sum: 366.9967001080513
time_elpased: 1.18
batch start
#iterations: 285
currently lose_sum: 367.3417931199074
time_elpased: 1.125
batch start
#iterations: 286
currently lose_sum: 366.2086343765259
time_elpased: 1.159
batch start
#iterations: 287
currently lose_sum: 366.5960805416107
time_elpased: 1.123
batch start
#iterations: 288
currently lose_sum: 368.0227753520012
time_elpased: 1.156
batch start
#iterations: 289
currently lose_sum: 365.37252247333527
time_elpased: 1.125
batch start
#iterations: 290
currently lose_sum: 366.2075102329254
time_elpased: 1.118
batch start
#iterations: 291
currently lose_sum: 365.68216252326965
time_elpased: 1.167
batch start
#iterations: 292
currently lose_sum: 365.59451055526733
time_elpased: 1.133
batch start
#iterations: 293
currently lose_sum: 366.37090361118317
time_elpased: 1.121
batch start
#iterations: 294
currently lose_sum: 365.4126212000847
time_elpased: 1.116
batch start
#iterations: 295
currently lose_sum: 366.1231378316879
time_elpased: 1.17
batch start
#iterations: 296
currently lose_sum: 366.41350668668747
time_elpased: 1.122
batch start
#iterations: 297
currently lose_sum: 366.2349759340286
time_elpased: 1.117
batch start
#iterations: 298
currently lose_sum: 367.3166978955269
time_elpased: 1.118
batch start
#iterations: 299
currently lose_sum: 366.8805879354477
time_elpased: 1.153
start validation test
0.785979381443
1.0
0.785979381443
0.880166243362
0
validation finish
batch start
#iterations: 300
currently lose_sum: 367.66201174259186
time_elpased: 1.149
batch start
#iterations: 301
currently lose_sum: 364.71234941482544
time_elpased: 1.112
batch start
#iterations: 302
currently lose_sum: 367.10323852300644
time_elpased: 1.12
batch start
#iterations: 303
currently lose_sum: 366.50845420360565
time_elpased: 1.124
batch start
#iterations: 304
currently lose_sum: 366.19323098659515
time_elpased: 1.127
batch start
#iterations: 305
currently lose_sum: 367.0086678266525
time_elpased: 1.119
batch start
#iterations: 306
currently lose_sum: 365.4337345957756
time_elpased: 1.132
batch start
#iterations: 307
currently lose_sum: 367.75365394353867
time_elpased: 1.121
batch start
#iterations: 308
currently lose_sum: 368.373421728611
time_elpased: 1.149
batch start
#iterations: 309
currently lose_sum: 366.8538307547569
time_elpased: 1.117
batch start
#iterations: 310
currently lose_sum: 367.38578802347183
time_elpased: 1.113
batch start
#iterations: 311
currently lose_sum: 365.0104390382767
time_elpased: 1.164
batch start
#iterations: 312
currently lose_sum: 367.19240802526474
time_elpased: 1.189
batch start
#iterations: 313
currently lose_sum: 367.7342329621315
time_elpased: 1.144
batch start
#iterations: 314
currently lose_sum: 365.8881977200508
time_elpased: 1.115
batch start
#iterations: 315
currently lose_sum: 365.7051658630371
time_elpased: 1.113
batch start
#iterations: 316
currently lose_sum: 368.1284758448601
time_elpased: 1.104
batch start
#iterations: 317
currently lose_sum: 365.49020153284073
time_elpased: 1.152
batch start
#iterations: 318
currently lose_sum: 364.71121633052826
time_elpased: 1.119
batch start
#iterations: 319
currently lose_sum: 367.4071224331856
time_elpased: 1.123
start validation test
0.786082474227
1.0
0.786082474227
0.880230880231
0
validation finish
batch start
#iterations: 320
currently lose_sum: 365.81194645166397
time_elpased: 1.118
batch start
#iterations: 321
currently lose_sum: 366.6913028359413
time_elpased: 1.112
batch start
#iterations: 322
currently lose_sum: 367.0975546836853
time_elpased: 1.131
batch start
#iterations: 323
currently lose_sum: 365.5038330554962
time_elpased: 1.117
batch start
#iterations: 324
currently lose_sum: 365.4615647792816
time_elpased: 1.149
batch start
#iterations: 325
currently lose_sum: 365.66119742393494
time_elpased: 1.154
batch start
#iterations: 326
currently lose_sum: 366.2867736816406
time_elpased: 1.13
batch start
#iterations: 327
currently lose_sum: 366.0332592725754
time_elpased: 1.116
batch start
#iterations: 328
currently lose_sum: 366.00813418626785
time_elpased: 1.132
batch start
#iterations: 329
currently lose_sum: 366.502636551857
time_elpased: 1.142
batch start
#iterations: 330
currently lose_sum: 365.5771143436432
time_elpased: 1.152
batch start
#iterations: 331
currently lose_sum: 366.4454219341278
time_elpased: 1.159
batch start
#iterations: 332
currently lose_sum: 366.3598101735115
time_elpased: 1.112
batch start
#iterations: 333
currently lose_sum: 366.77306854724884
time_elpased: 1.109
batch start
#iterations: 334
currently lose_sum: 365.8726997971535
time_elpased: 1.118
batch start
#iterations: 335
currently lose_sum: 365.17562288045883
time_elpased: 1.149
batch start
#iterations: 336
currently lose_sum: 364.52670937776566
time_elpased: 1.13
batch start
#iterations: 337
currently lose_sum: 366.4198280572891
time_elpased: 1.122
batch start
#iterations: 338
currently lose_sum: 366.52790755033493
time_elpased: 1.127
batch start
#iterations: 339
currently lose_sum: 366.9744769334793
time_elpased: 1.124
start validation test
0.777835051546
1.0
0.777835051546
0.875036242389
0
validation finish
batch start
#iterations: 340
currently lose_sum: 367.3025386929512
time_elpased: 1.115
batch start
#iterations: 341
currently lose_sum: 364.63705068826675
time_elpased: 1.114
batch start
#iterations: 342
currently lose_sum: 365.13544899225235
time_elpased: 1.131
batch start
#iterations: 343
currently lose_sum: 365.57164484262466
time_elpased: 1.107
batch start
#iterations: 344
currently lose_sum: 366.90825057029724
time_elpased: 1.125
batch start
#iterations: 345
currently lose_sum: 365.35122960805893
time_elpased: 1.141
batch start
#iterations: 346
currently lose_sum: 364.82430720329285
time_elpased: 1.159
batch start
#iterations: 347
currently lose_sum: 365.79364091157913
time_elpased: 1.104
batch start
#iterations: 348
currently lose_sum: 367.2321939468384
time_elpased: 1.122
batch start
#iterations: 349
currently lose_sum: 365.9471312761307
time_elpased: 1.13
batch start
#iterations: 350
currently lose_sum: 365.97798973321915
time_elpased: 1.144
batch start
#iterations: 351
currently lose_sum: 366.22212213277817
time_elpased: 1.115
batch start
#iterations: 352
currently lose_sum: 366.2739698290825
time_elpased: 1.124
batch start
#iterations: 353
currently lose_sum: 365.2216342687607
time_elpased: 1.117
batch start
#iterations: 354
currently lose_sum: 365.1281753182411
time_elpased: 1.123
batch start
#iterations: 355
currently lose_sum: 366.04521185159683
time_elpased: 1.13
batch start
#iterations: 356
currently lose_sum: 366.87515568733215
time_elpased: 1.123
batch start
#iterations: 357
currently lose_sum: 365.7617207169533
time_elpased: 1.136
batch start
#iterations: 358
currently lose_sum: 366.9049359560013
time_elpased: 1.116
batch start
#iterations: 359
currently lose_sum: 365.7889779806137
time_elpased: 1.158
start validation test
0.781546391753
1.0
0.781546391753
0.877379781263
0
validation finish
batch start
#iterations: 360
currently lose_sum: 364.8469754457474
time_elpased: 1.115
batch start
#iterations: 361
currently lose_sum: 367.058576464653
time_elpased: 1.154
batch start
#iterations: 362
currently lose_sum: 365.6254555583
time_elpased: 1.154
batch start
#iterations: 363
currently lose_sum: 365.9899867773056
time_elpased: 1.114
batch start
#iterations: 364
currently lose_sum: 364.88496178388596
time_elpased: 1.135
batch start
#iterations: 365
currently lose_sum: 366.3209295272827
time_elpased: 1.116
batch start
#iterations: 366
currently lose_sum: 366.35795617103577
time_elpased: 1.179
batch start
#iterations: 367
currently lose_sum: 365.45935663580894
time_elpased: 1.118
batch start
#iterations: 368
currently lose_sum: 366.84309953451157
time_elpased: 1.115
batch start
#iterations: 369
currently lose_sum: 367.2630130648613
time_elpased: 1.136
batch start
#iterations: 370
currently lose_sum: 366.52015978097916
time_elpased: 1.21
batch start
#iterations: 371
currently lose_sum: 365.47567969560623
time_elpased: 1.174
batch start
#iterations: 372
currently lose_sum: 365.734654545784
time_elpased: 1.13
batch start
#iterations: 373
currently lose_sum: 366.5164992213249
time_elpased: 1.177
batch start
#iterations: 374
currently lose_sum: 365.74728816747665
time_elpased: 1.151
batch start
#iterations: 375
currently lose_sum: 365.8677651286125
time_elpased: 1.116
batch start
#iterations: 376
currently lose_sum: 366.6430323123932
time_elpased: 1.115
batch start
#iterations: 377
currently lose_sum: 366.2062695622444
time_elpased: 1.141
batch start
#iterations: 378
currently lose_sum: 365.9173136353493
time_elpased: 1.112
batch start
#iterations: 379
currently lose_sum: 366.3356200456619
time_elpased: 1.143
start validation test
0.782783505155
1.0
0.782783505155
0.878158792575
0
validation finish
batch start
#iterations: 380
currently lose_sum: 365.84385389089584
time_elpased: 1.126
batch start
#iterations: 381
currently lose_sum: 366.58178490400314
time_elpased: 1.162
batch start
#iterations: 382
currently lose_sum: 366.1063392162323
time_elpased: 1.131
batch start
#iterations: 383
currently lose_sum: 366.0787898302078
time_elpased: 1.119
batch start
#iterations: 384
currently lose_sum: 365.92881721258163
time_elpased: 1.154
batch start
#iterations: 385
currently lose_sum: 365.0438950061798
time_elpased: 1.116
batch start
#iterations: 386
currently lose_sum: 365.7923749089241
time_elpased: 1.19
batch start
#iterations: 387
currently lose_sum: 367.2618678212166
time_elpased: 1.142
batch start
#iterations: 388
currently lose_sum: 365.71325528621674
time_elpased: 1.14
batch start
#iterations: 389
currently lose_sum: 366.20815765857697
time_elpased: 1.161
batch start
#iterations: 390
currently lose_sum: 366.8074538707733
time_elpased: 1.172
batch start
#iterations: 391
currently lose_sum: 365.7972534894943
time_elpased: 1.121
batch start
#iterations: 392
currently lose_sum: 366.02095079421997
time_elpased: 1.173
batch start
#iterations: 393
currently lose_sum: 365.67142671346664
time_elpased: 1.15
batch start
#iterations: 394
currently lose_sum: 365.64660823345184
time_elpased: 1.124
batch start
#iterations: 395
currently lose_sum: 365.7572458386421
time_elpased: 1.13
batch start
#iterations: 396
currently lose_sum: 365.6511954665184
time_elpased: 1.12
batch start
#iterations: 397
currently lose_sum: 367.02917087078094
time_elpased: 1.121
batch start
#iterations: 398
currently lose_sum: 366.03062587976456
time_elpased: 1.182
batch start
#iterations: 399
currently lose_sum: 366.0637574791908
time_elpased: 1.108
start validation test
0.788041237113
1.0
0.788041237113
0.881457564576
0
validation finish
acc: 0.790
pre: 1.000
rec: 0.790
F1: 0.883
auc: 0.000
