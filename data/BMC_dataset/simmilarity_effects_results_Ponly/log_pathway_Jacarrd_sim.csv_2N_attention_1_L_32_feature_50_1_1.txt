start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 555.1637851595879
time_elpased: 1.69
batch start
#iterations: 1
currently lose_sum: 543.5087872743607
time_elpased: 1.666
batch start
#iterations: 2
currently lose_sum: 538.358302116394
time_elpased: 1.67
batch start
#iterations: 3
currently lose_sum: 532.5185711979866
time_elpased: 1.738
batch start
#iterations: 4
currently lose_sum: 529.4418013393879
time_elpased: 1.675
batch start
#iterations: 5
currently lose_sum: 529.358190625906
time_elpased: 1.67
batch start
#iterations: 6
currently lose_sum: 523.6435146331787
time_elpased: 1.656
batch start
#iterations: 7
currently lose_sum: 525.2639682590961
time_elpased: 1.691
batch start
#iterations: 8
currently lose_sum: 522.7422118782997
time_elpased: 1.67
batch start
#iterations: 9
currently lose_sum: 523.2903080880642
time_elpased: 1.76
batch start
#iterations: 10
currently lose_sum: 522.2222202420235
time_elpased: 1.681
batch start
#iterations: 11
currently lose_sum: 525.0586407482624
time_elpased: 1.674
batch start
#iterations: 12
currently lose_sum: 520.5797129869461
time_elpased: 1.7
batch start
#iterations: 13
currently lose_sum: 521.0107060968876
time_elpased: 1.719
batch start
#iterations: 14
currently lose_sum: 518.8508596420288
time_elpased: 1.746
batch start
#iterations: 15
currently lose_sum: 519.4045183956623
time_elpased: 1.677
batch start
#iterations: 16
currently lose_sum: 520.17588570714
time_elpased: 1.669
batch start
#iterations: 17
currently lose_sum: 517.5161572396755
time_elpased: 1.673
batch start
#iterations: 18
currently lose_sum: 518.3761238157749
time_elpased: 1.743
batch start
#iterations: 19
currently lose_sum: 515.6680037677288
time_elpased: 1.716
start validation test
0.184536082474
1.0
0.184536082474
0.311575282855
0
validation finish
batch start
#iterations: 20
currently lose_sum: 517.6368616223335
time_elpased: 1.689
batch start
#iterations: 21
currently lose_sum: 519.1575937271118
time_elpased: 1.683
batch start
#iterations: 22
currently lose_sum: 517.8804287314415
time_elpased: 1.676
batch start
#iterations: 23
currently lose_sum: 515.8010400533676
time_elpased: 1.703
batch start
#iterations: 24
currently lose_sum: 516.6369498074055
time_elpased: 1.671
batch start
#iterations: 25
currently lose_sum: 516.1800868213177
time_elpased: 1.664
batch start
#iterations: 26
currently lose_sum: 515.2119608223438
time_elpased: 1.701
batch start
#iterations: 27
currently lose_sum: 516.4070695340633
time_elpased: 1.698
batch start
#iterations: 28
currently lose_sum: 515.7538216412067
time_elpased: 1.683
batch start
#iterations: 29
currently lose_sum: 516.0002883970737
time_elpased: 1.717
batch start
#iterations: 30
currently lose_sum: 513.833080470562
time_elpased: 1.688
batch start
#iterations: 31
currently lose_sum: 514.8496437966824
time_elpased: 1.686
batch start
#iterations: 32
currently lose_sum: 516.0400631427765
time_elpased: 1.708
batch start
#iterations: 33
currently lose_sum: 514.5749774873257
time_elpased: 1.684
batch start
#iterations: 34
currently lose_sum: 514.7094142436981
time_elpased: 1.66
batch start
#iterations: 35
currently lose_sum: 513.202286273241
time_elpased: 1.669
batch start
#iterations: 36
currently lose_sum: 514.5342551767826
time_elpased: 1.682
batch start
#iterations: 37
currently lose_sum: 514.0558309853077
time_elpased: 1.679
batch start
#iterations: 38
currently lose_sum: 515.4023737609386
time_elpased: 1.707
batch start
#iterations: 39
currently lose_sum: 514.869197845459
time_elpased: 1.723
start validation test
0.135360824742
1.0
0.135360824742
0.238445473531
0
validation finish
batch start
#iterations: 40
currently lose_sum: 513.7882196307182
time_elpased: 1.71
batch start
#iterations: 41
currently lose_sum: 514.2418215870857
time_elpased: 1.679
batch start
#iterations: 42
currently lose_sum: 513.2177203595638
time_elpased: 1.746
batch start
#iterations: 43
currently lose_sum: 513.8339206874371
time_elpased: 1.732
batch start
#iterations: 44
currently lose_sum: 511.45629170536995
time_elpased: 1.668
batch start
#iterations: 45
currently lose_sum: 513.2067105174065
time_elpased: 1.705
batch start
#iterations: 46
currently lose_sum: 513.2616608440876
time_elpased: 1.738
batch start
#iterations: 47
currently lose_sum: 513.5827861726284
time_elpased: 1.724
batch start
#iterations: 48
currently lose_sum: 510.82185262441635
time_elpased: 1.685
batch start
#iterations: 49
currently lose_sum: 512.3325611948967
time_elpased: 1.68
batch start
#iterations: 50
currently lose_sum: 515.3246450126171
time_elpased: 1.708
batch start
#iterations: 51
currently lose_sum: 513.676340430975
time_elpased: 1.745
batch start
#iterations: 52
currently lose_sum: 513.3862237334251
time_elpased: 1.738
batch start
#iterations: 53
currently lose_sum: 513.8299070596695
time_elpased: 1.723
batch start
#iterations: 54
currently lose_sum: 512.9964164495468
time_elpased: 1.682
batch start
#iterations: 55
currently lose_sum: 512.4535713195801
time_elpased: 1.699
batch start
#iterations: 56
currently lose_sum: 513.6160992383957
time_elpased: 1.68
batch start
#iterations: 57
currently lose_sum: 511.26696440577507
time_elpased: 1.678
batch start
#iterations: 58
currently lose_sum: 512.4328449368477
time_elpased: 1.762
batch start
#iterations: 59
currently lose_sum: 510.4927409887314
time_elpased: 1.711
start validation test
0.125154639175
1.0
0.125154639175
0.222466556716
0
validation finish
batch start
#iterations: 60
currently lose_sum: 511.85286271572113
time_elpased: 1.678
batch start
#iterations: 61
currently lose_sum: 513.0575477182865
time_elpased: 1.71
batch start
#iterations: 62
currently lose_sum: 514.1611466407776
time_elpased: 1.686
batch start
#iterations: 63
currently lose_sum: 512.2161967456341
time_elpased: 1.771
batch start
#iterations: 64
currently lose_sum: 510.86467784643173
time_elpased: 1.675
batch start
#iterations: 65
currently lose_sum: 513.1683173775673
time_elpased: 1.755
batch start
#iterations: 66
currently lose_sum: 512.0083263218403
time_elpased: 1.679
batch start
#iterations: 67
currently lose_sum: 511.3867367506027
time_elpased: 1.682
batch start
#iterations: 68
currently lose_sum: 514.5086672306061
time_elpased: 1.675
batch start
#iterations: 69
currently lose_sum: 511.8505018353462
time_elpased: 1.745
batch start
#iterations: 70
currently lose_sum: 511.26906830072403
time_elpased: 1.702
batch start
#iterations: 71
currently lose_sum: 510.99683234095573
time_elpased: 1.751
batch start
#iterations: 72
currently lose_sum: 512.9587680399418
time_elpased: 1.757
batch start
#iterations: 73
currently lose_sum: 511.90851321816444
time_elpased: 1.741
batch start
#iterations: 74
currently lose_sum: 511.3064995408058
time_elpased: 1.722
batch start
#iterations: 75
currently lose_sum: 512.2587449848652
time_elpased: 1.677
batch start
#iterations: 76
currently lose_sum: 513.406209141016
time_elpased: 1.733
batch start
#iterations: 77
currently lose_sum: 511.92041915655136
time_elpased: 1.679
batch start
#iterations: 78
currently lose_sum: 510.10610541701317
time_elpased: 1.731
batch start
#iterations: 79
currently lose_sum: 511.1810688972473
time_elpased: 1.668
start validation test
0.264742268041
1.0
0.264742268041
0.418650146723
0
validation finish
batch start
#iterations: 80
currently lose_sum: 510.3575778603554
time_elpased: 1.761
batch start
#iterations: 81
currently lose_sum: 511.2572270333767
time_elpased: 1.669
batch start
#iterations: 82
currently lose_sum: 511.5509636104107
time_elpased: 1.699
batch start
#iterations: 83
currently lose_sum: 511.1466013491154
time_elpased: 1.714
batch start
#iterations: 84
currently lose_sum: 510.7091625928879
time_elpased: 1.768
batch start
#iterations: 85
currently lose_sum: 511.3928344845772
time_elpased: 1.718
batch start
#iterations: 86
currently lose_sum: 511.8328243792057
time_elpased: 1.671
batch start
#iterations: 87
currently lose_sum: 510.83219814300537
time_elpased: 1.709
batch start
#iterations: 88
currently lose_sum: 509.65219432115555
time_elpased: 1.672
batch start
#iterations: 89
currently lose_sum: 511.64891877770424
time_elpased: 1.675
batch start
#iterations: 90
currently lose_sum: 510.27011996507645
time_elpased: 1.771
batch start
#iterations: 91
currently lose_sum: 508.5794439315796
time_elpased: 1.679
batch start
#iterations: 92
currently lose_sum: 511.49163284897804
time_elpased: 1.702
batch start
#iterations: 93
currently lose_sum: 510.7507222890854
time_elpased: 1.683
batch start
#iterations: 94
currently lose_sum: 510.08952060341835
time_elpased: 1.685
batch start
#iterations: 95
currently lose_sum: 509.74910125136375
time_elpased: 1.778
batch start
#iterations: 96
currently lose_sum: 512.6660656034946
time_elpased: 1.685
batch start
#iterations: 97
currently lose_sum: 509.7009907364845
time_elpased: 1.672
batch start
#iterations: 98
currently lose_sum: 510.8913740515709
time_elpased: 1.686
batch start
#iterations: 99
currently lose_sum: 510.29391515254974
time_elpased: 1.684
start validation test
0.0855670103093
1.0
0.0855670103093
0.157644824311
0
validation finish
batch start
#iterations: 100
currently lose_sum: 511.9460477232933
time_elpased: 1.736
batch start
#iterations: 101
currently lose_sum: 511.16262274980545
time_elpased: 1.681
batch start
#iterations: 102
currently lose_sum: 509.71603840589523
time_elpased: 1.685
batch start
#iterations: 103
currently lose_sum: 509.4692535698414
time_elpased: 1.761
batch start
#iterations: 104
currently lose_sum: 511.7922251224518
time_elpased: 1.731
batch start
#iterations: 105
currently lose_sum: 510.9682866036892
time_elpased: 1.752
batch start
#iterations: 106
currently lose_sum: 511.5828709602356
time_elpased: 1.684
batch start
#iterations: 107
currently lose_sum: 509.80482321977615
time_elpased: 1.689
batch start
#iterations: 108
currently lose_sum: 509.97734186053276
time_elpased: 1.681
batch start
#iterations: 109
currently lose_sum: 511.0768732726574
time_elpased: 1.691
batch start
#iterations: 110
currently lose_sum: 510.34522008895874
time_elpased: 1.718
batch start
#iterations: 111
currently lose_sum: 513.5194889307022
time_elpased: 1.679
batch start
#iterations: 112
currently lose_sum: 509.40088483691216
time_elpased: 1.673
batch start
#iterations: 113
currently lose_sum: 509.57907778024673
time_elpased: 1.675
batch start
#iterations: 114
currently lose_sum: 511.83256500959396
time_elpased: 1.685
batch start
#iterations: 115
currently lose_sum: 510.16113448143005
time_elpased: 1.764
batch start
#iterations: 116
currently lose_sum: 509.0342610478401
time_elpased: 1.731
batch start
#iterations: 117
currently lose_sum: 509.6694322526455
time_elpased: 1.694
batch start
#iterations: 118
currently lose_sum: 511.203399926424
time_elpased: 1.676
batch start
#iterations: 119
currently lose_sum: 509.4860478937626
time_elpased: 1.694
start validation test
0.156494845361
1.0
0.156494845361
0.27063647709
0
validation finish
batch start
#iterations: 120
currently lose_sum: 509.7170171737671
time_elpased: 1.745
batch start
#iterations: 121
currently lose_sum: 507.99414440989494
time_elpased: 1.773
batch start
#iterations: 122
currently lose_sum: 509.25800704956055
time_elpased: 1.707
batch start
#iterations: 123
currently lose_sum: 511.2901881933212
time_elpased: 1.687
batch start
#iterations: 124
currently lose_sum: 510.10436606407166
time_elpased: 1.722
batch start
#iterations: 125
currently lose_sum: 510.95038107037544
time_elpased: 1.737
batch start
#iterations: 126
currently lose_sum: 508.5360875725746
time_elpased: 1.734
batch start
#iterations: 127
currently lose_sum: 512.0243053734303
time_elpased: 1.677
batch start
#iterations: 128
currently lose_sum: 510.4050591289997
time_elpased: 1.721
batch start
#iterations: 129
currently lose_sum: 509.9569864869118
time_elpased: 1.687
batch start
#iterations: 130
currently lose_sum: 508.76486587524414
time_elpased: 1.725
batch start
#iterations: 131
currently lose_sum: 509.95377165079117
time_elpased: 1.724
batch start
#iterations: 132
currently lose_sum: 509.60018864274025
time_elpased: 1.682
batch start
#iterations: 133
currently lose_sum: 512.1147689819336
time_elpased: 1.69
batch start
#iterations: 134
currently lose_sum: 509.88077226281166
time_elpased: 1.682
batch start
#iterations: 135
currently lose_sum: 507.2923256754875
time_elpased: 1.706
batch start
#iterations: 136
currently lose_sum: 508.73577880859375
time_elpased: 1.755
batch start
#iterations: 137
currently lose_sum: 508.51298582553864
time_elpased: 1.674
batch start
#iterations: 138
currently lose_sum: 509.6461949944496
time_elpased: 1.723
batch start
#iterations: 139
currently lose_sum: 509.4816881120205
time_elpased: 1.708
start validation test
0.12618556701
1.0
0.12618556701
0.224093738557
0
validation finish
batch start
#iterations: 140
currently lose_sum: 506.58544078469276
time_elpased: 1.679
batch start
#iterations: 141
currently lose_sum: 509.2370034456253
time_elpased: 1.719
batch start
#iterations: 142
currently lose_sum: 509.7703373134136
time_elpased: 1.688
batch start
#iterations: 143
currently lose_sum: 510.13701540231705
time_elpased: 1.769
batch start
#iterations: 144
currently lose_sum: 509.91990330815315
time_elpased: 1.683
batch start
#iterations: 145
currently lose_sum: 508.6423498094082
time_elpased: 1.773
batch start
#iterations: 146
currently lose_sum: 511.09775322675705
time_elpased: 1.753
batch start
#iterations: 147
currently lose_sum: 510.4095242321491
time_elpased: 1.712
batch start
#iterations: 148
currently lose_sum: 508.09746330976486
time_elpased: 1.675
batch start
#iterations: 149
currently lose_sum: 510.7817717194557
time_elpased: 1.713
batch start
#iterations: 150
currently lose_sum: 508.1257521510124
time_elpased: 1.753
batch start
#iterations: 151
currently lose_sum: 507.73506730794907
time_elpased: 1.759
batch start
#iterations: 152
currently lose_sum: 509.9455534815788
time_elpased: 1.676
batch start
#iterations: 153
currently lose_sum: 507.03988033533096
time_elpased: 1.748
batch start
#iterations: 154
currently lose_sum: 512.3426152169704
time_elpased: 1.687
batch start
#iterations: 155
currently lose_sum: 508.4653446674347
time_elpased: 1.683
batch start
#iterations: 156
currently lose_sum: 510.1126692891121
time_elpased: 1.748
batch start
#iterations: 157
currently lose_sum: 508.18045395612717
time_elpased: 1.719
batch start
#iterations: 158
currently lose_sum: 509.06376084685326
time_elpased: 1.712
batch start
#iterations: 159
currently lose_sum: 510.5568545758724
time_elpased: 1.692
start validation test
0.139896907216
1.0
0.139896907216
0.24545536764
0
validation finish
batch start
#iterations: 160
currently lose_sum: 510.27229166030884
time_elpased: 1.698
batch start
#iterations: 161
currently lose_sum: 509.5236796140671
time_elpased: 1.728
batch start
#iterations: 162
currently lose_sum: 506.941022336483
time_elpased: 1.696
batch start
#iterations: 163
currently lose_sum: 506.2030863761902
time_elpased: 1.686
batch start
#iterations: 164
currently lose_sum: 510.7278781533241
time_elpased: 1.685
batch start
#iterations: 165
currently lose_sum: 509.6264652311802
time_elpased: 1.718
batch start
#iterations: 166
currently lose_sum: 509.8845591247082
time_elpased: 1.678
batch start
#iterations: 167
currently lose_sum: 509.41006964445114
time_elpased: 1.76
batch start
#iterations: 168
currently lose_sum: 510.0175356864929
time_elpased: 1.747
batch start
#iterations: 169
currently lose_sum: 510.14539963006973
time_elpased: 1.672
batch start
#iterations: 170
currently lose_sum: 508.7794052362442
time_elpased: 1.681
batch start
#iterations: 171
currently lose_sum: 506.6082448363304
time_elpased: 1.692
batch start
#iterations: 172
currently lose_sum: 512.242347240448
time_elpased: 1.759
batch start
#iterations: 173
currently lose_sum: 510.0349064171314
time_elpased: 1.733
batch start
#iterations: 174
currently lose_sum: 510.20253521203995
time_elpased: 1.669
batch start
#iterations: 175
currently lose_sum: 507.92392787337303
time_elpased: 1.679
batch start
#iterations: 176
currently lose_sum: 508.59174421429634
time_elpased: 1.731
batch start
#iterations: 177
currently lose_sum: 507.7293865978718
time_elpased: 1.777
batch start
#iterations: 178
currently lose_sum: 509.9053587913513
time_elpased: 1.705
batch start
#iterations: 179
currently lose_sum: 510.393835991621
time_elpased: 1.689
start validation test
0.145154639175
1.0
0.145154639175
0.253510983075
0
validation finish
batch start
#iterations: 180
currently lose_sum: 507.7621031701565
time_elpased: 1.725
batch start
#iterations: 181
currently lose_sum: 510.784291356802
time_elpased: 1.753
batch start
#iterations: 182
currently lose_sum: 509.0686994791031
time_elpased: 1.689
batch start
#iterations: 183
currently lose_sum: 509.50460067391396
time_elpased: 1.728
batch start
#iterations: 184
currently lose_sum: 508.69096171855927
time_elpased: 1.695
batch start
#iterations: 185
currently lose_sum: 509.2415342926979
time_elpased: 1.695
batch start
#iterations: 186
currently lose_sum: 509.2759590446949
time_elpased: 1.706
batch start
#iterations: 187
currently lose_sum: 506.56379529833794
time_elpased: 1.774
batch start
#iterations: 188
currently lose_sum: 510.4190211594105
time_elpased: 1.751
batch start
#iterations: 189
currently lose_sum: 510.03644475340843
time_elpased: 1.699
batch start
#iterations: 190
currently lose_sum: 506.81424298882484
time_elpased: 1.725
batch start
#iterations: 191
currently lose_sum: 510.1204173564911
time_elpased: 1.688
batch start
#iterations: 192
currently lose_sum: 506.3250557780266
time_elpased: 1.694
batch start
#iterations: 193
currently lose_sum: 508.9514594376087
time_elpased: 1.698
batch start
#iterations: 194
currently lose_sum: 510.6288403868675
time_elpased: 1.755
batch start
#iterations: 195
currently lose_sum: 508.5405976176262
time_elpased: 1.683
batch start
#iterations: 196
currently lose_sum: 507.36555990576744
time_elpased: 1.779
batch start
#iterations: 197
currently lose_sum: 510.2011308372021
time_elpased: 1.677
batch start
#iterations: 198
currently lose_sum: 508.6310991346836
time_elpased: 1.684
batch start
#iterations: 199
currently lose_sum: 507.61257007718086
time_elpased: 1.693
start validation test
0.105463917526
1.0
0.105463917526
0.190804812086
0
validation finish
batch start
#iterations: 200
currently lose_sum: 508.8620453476906
time_elpased: 1.691
batch start
#iterations: 201
currently lose_sum: 508.83666357398033
time_elpased: 1.702
batch start
#iterations: 202
currently lose_sum: 509.2172783613205
time_elpased: 1.717
batch start
#iterations: 203
currently lose_sum: 507.68796250224113
time_elpased: 1.777
batch start
#iterations: 204
currently lose_sum: 507.3093864619732
time_elpased: 1.753
batch start
#iterations: 205
currently lose_sum: 509.07885417342186
time_elpased: 1.687
batch start
#iterations: 206
currently lose_sum: 510.75126284360886
time_elpased: 1.679
batch start
#iterations: 207
currently lose_sum: 509.4679598212242
time_elpased: 1.731
batch start
#iterations: 208
currently lose_sum: 507.40076583623886
time_elpased: 1.689
batch start
#iterations: 209
currently lose_sum: 507.7819735109806
time_elpased: 1.685
batch start
#iterations: 210
currently lose_sum: 508.32153737545013
time_elpased: 1.703
batch start
#iterations: 211
currently lose_sum: 509.46096685528755
time_elpased: 1.669
batch start
#iterations: 212
currently lose_sum: 508.6170055270195
time_elpased: 1.741
batch start
#iterations: 213
currently lose_sum: 507.9618853032589
time_elpased: 1.678
batch start
#iterations: 214
currently lose_sum: 508.2142269909382
time_elpased: 1.72
batch start
#iterations: 215
currently lose_sum: 510.2200016975403
time_elpased: 1.775
batch start
#iterations: 216
currently lose_sum: 508.23093271255493
time_elpased: 1.726
batch start
#iterations: 217
currently lose_sum: 508.5768571794033
time_elpased: 1.755
batch start
#iterations: 218
currently lose_sum: 508.3294448554516
time_elpased: 1.719
batch start
#iterations: 219
currently lose_sum: 509.9923631846905
time_elpased: 1.684
start validation test
0.141443298969
1.0
0.141443298969
0.247832369942
0
validation finish
batch start
#iterations: 220
currently lose_sum: 507.7533504664898
time_elpased: 1.691
batch start
#iterations: 221
currently lose_sum: 509.4643967151642
time_elpased: 1.741
batch start
#iterations: 222
currently lose_sum: 507.02158892154694
time_elpased: 1.682
batch start
#iterations: 223
currently lose_sum: 506.62877041101456
time_elpased: 1.679
batch start
#iterations: 224
currently lose_sum: 509.22843196988106
time_elpased: 1.692
batch start
#iterations: 225
currently lose_sum: 509.4734385609627
time_elpased: 1.767
batch start
#iterations: 226
currently lose_sum: 508.8276849091053
time_elpased: 1.788
batch start
#iterations: 227
currently lose_sum: 508.59056401252747
time_elpased: 1.765
batch start
#iterations: 228
currently lose_sum: 505.53249701857567
time_elpased: 1.76
batch start
#iterations: 229
currently lose_sum: 507.7894603610039
time_elpased: 1.778
batch start
#iterations: 230
currently lose_sum: 509.4521272778511
time_elpased: 1.675
batch start
#iterations: 231
currently lose_sum: 510.7292459309101
time_elpased: 1.683
batch start
#iterations: 232
currently lose_sum: 505.8954394161701
time_elpased: 1.699
batch start
#iterations: 233
currently lose_sum: 506.8760516643524
time_elpased: 1.754
batch start
#iterations: 234
currently lose_sum: 508.4009843170643
time_elpased: 1.722
batch start
#iterations: 235
currently lose_sum: 508.23206210136414
time_elpased: 1.702
batch start
#iterations: 236
currently lose_sum: 507.89322781562805
time_elpased: 1.694
batch start
#iterations: 237
currently lose_sum: 508.61808225512505
time_elpased: 1.67
batch start
#iterations: 238
currently lose_sum: 507.14648482203484
time_elpased: 1.735
batch start
#iterations: 239
currently lose_sum: 505.74638360738754
time_elpased: 1.769
start validation test
0.0871134020619
1.0
0.0871134020619
0.160265528687
0
validation finish
batch start
#iterations: 240
currently lose_sum: 506.5441623032093
time_elpased: 1.678
batch start
#iterations: 241
currently lose_sum: 506.35903999209404
time_elpased: 1.783
batch start
#iterations: 242
currently lose_sum: 505.58777210116386
time_elpased: 1.682
batch start
#iterations: 243
currently lose_sum: 508.3576549589634
time_elpased: 1.69
batch start
#iterations: 244
currently lose_sum: 507.95979648828506
time_elpased: 1.671
batch start
#iterations: 245
currently lose_sum: 507.7002344429493
time_elpased: 1.675
batch start
#iterations: 246
currently lose_sum: 508.40478935837746
time_elpased: 1.673
batch start
#iterations: 247
currently lose_sum: 507.0542975664139
time_elpased: 1.737
batch start
#iterations: 248
currently lose_sum: 507.344824552536
time_elpased: 1.74
batch start
#iterations: 249
currently lose_sum: 508.7246510386467
time_elpased: 1.68
batch start
#iterations: 250
currently lose_sum: 507.3849577307701
time_elpased: 1.686
batch start
#iterations: 251
currently lose_sum: 507.10448583960533
time_elpased: 1.711
batch start
#iterations: 252
currently lose_sum: 509.10102739930153
time_elpased: 1.685
batch start
#iterations: 253
currently lose_sum: 505.84827867150307
time_elpased: 1.716
batch start
#iterations: 254
currently lose_sum: 509.29891243577003
time_elpased: 1.731
batch start
#iterations: 255
currently lose_sum: 507.6844258606434
time_elpased: 1.711
batch start
#iterations: 256
currently lose_sum: 507.7560096979141
time_elpased: 1.697
batch start
#iterations: 257
currently lose_sum: 506.44044837355614
time_elpased: 1.735
batch start
#iterations: 258
currently lose_sum: 508.4860750436783
time_elpased: 1.683
batch start
#iterations: 259
currently lose_sum: 508.7986529171467
time_elpased: 1.678
start validation test
0.126907216495
1.0
0.126907216495
0.22523099442
0
validation finish
batch start
#iterations: 260
currently lose_sum: 508.9111120402813
time_elpased: 1.69
batch start
#iterations: 261
currently lose_sum: 510.64508217573166
time_elpased: 1.749
batch start
#iterations: 262
currently lose_sum: 508.5706818699837
time_elpased: 1.689
batch start
#iterations: 263
currently lose_sum: 507.5553605258465
time_elpased: 1.71
batch start
#iterations: 264
currently lose_sum: 507.6218517422676
time_elpased: 1.747
batch start
#iterations: 265
currently lose_sum: 507.51338186860085
time_elpased: 1.676
batch start
#iterations: 266
currently lose_sum: 508.4843721985817
time_elpased: 1.74
batch start
#iterations: 267
currently lose_sum: 506.86371180415154
time_elpased: 1.764
batch start
#iterations: 268
currently lose_sum: 509.48059472441673
time_elpased: 1.683
batch start
#iterations: 269
currently lose_sum: 509.57060703635216
time_elpased: 1.667
batch start
#iterations: 270
currently lose_sum: 508.1557723581791
time_elpased: 1.689
batch start
#iterations: 271
currently lose_sum: 504.8111700415611
time_elpased: 1.677
batch start
#iterations: 272
currently lose_sum: 508.6214550733566
time_elpased: 1.695
batch start
#iterations: 273
currently lose_sum: 507.52743914723396
time_elpased: 1.685
batch start
#iterations: 274
currently lose_sum: 508.53690591454506
time_elpased: 1.746
batch start
#iterations: 275
currently lose_sum: 507.8439565896988
time_elpased: 1.676
batch start
#iterations: 276
currently lose_sum: 506.0736150741577
time_elpased: 1.719
batch start
#iterations: 277
currently lose_sum: 509.73251566290855
time_elpased: 1.768
batch start
#iterations: 278
currently lose_sum: 508.41525876522064
time_elpased: 1.689
batch start
#iterations: 279
currently lose_sum: 508.26665049791336
time_elpased: 1.705
start validation test
0.0673195876289
1.0
0.0673195876289
0.126147010528
0
validation finish
batch start
#iterations: 280
currently lose_sum: 506.9465042054653
time_elpased: 1.691
batch start
#iterations: 281
currently lose_sum: 507.91672921180725
time_elpased: 1.689
batch start
#iterations: 282
currently lose_sum: 508.1725240647793
time_elpased: 1.685
batch start
#iterations: 283
currently lose_sum: 508.33074128627777
time_elpased: 1.752
batch start
#iterations: 284
currently lose_sum: 508.094164699316
time_elpased: 1.678
batch start
#iterations: 285
currently lose_sum: 507.50492295622826
time_elpased: 1.675
batch start
#iterations: 286
currently lose_sum: 508.09926119446754
time_elpased: 1.752
batch start
#iterations: 287
currently lose_sum: 507.9271793663502
time_elpased: 1.695
batch start
#iterations: 288
currently lose_sum: 508.42651480436325
time_elpased: 1.685
batch start
#iterations: 289
currently lose_sum: 507.1425533592701
time_elpased: 1.698
batch start
#iterations: 290
currently lose_sum: 506.8829108476639
time_elpased: 1.733
batch start
#iterations: 291
currently lose_sum: 508.7383887767792
time_elpased: 1.751
batch start
#iterations: 292
currently lose_sum: 506.8152699768543
time_elpased: 1.722
batch start
#iterations: 293
currently lose_sum: 508.0278167128563
time_elpased: 1.767
batch start
#iterations: 294
currently lose_sum: 506.9268949627876
time_elpased: 1.697
batch start
#iterations: 295
currently lose_sum: 507.30241337418556
time_elpased: 1.71
batch start
#iterations: 296
currently lose_sum: 505.2152853310108
time_elpased: 1.727
batch start
#iterations: 297
currently lose_sum: 508.7796601951122
time_elpased: 1.741
batch start
#iterations: 298
currently lose_sum: 507.8373769521713
time_elpased: 1.681
batch start
#iterations: 299
currently lose_sum: 507.36289831995964
time_elpased: 1.677
start validation test
0.191855670103
1.0
0.191855670103
0.321944468472
0
validation finish
batch start
#iterations: 300
currently lose_sum: 505.2549577951431
time_elpased: 1.682
batch start
#iterations: 301
currently lose_sum: 508.1870041489601
time_elpased: 1.731
batch start
#iterations: 302
currently lose_sum: 509.45132556557655
time_elpased: 1.772
batch start
#iterations: 303
currently lose_sum: 507.7417452633381
time_elpased: 1.694
batch start
#iterations: 304
currently lose_sum: 506.84347665309906
time_elpased: 1.691
batch start
#iterations: 305
currently lose_sum: 508.97435876727104
time_elpased: 1.69
batch start
#iterations: 306
currently lose_sum: 507.2366862297058
time_elpased: 1.741
batch start
#iterations: 307
currently lose_sum: 509.6747998595238
time_elpased: 1.692
batch start
#iterations: 308
currently lose_sum: 506.5159814953804
time_elpased: 1.764
batch start
#iterations: 309
currently lose_sum: 509.72694650292397
time_elpased: 1.717
batch start
#iterations: 310
currently lose_sum: 506.27930918335915
time_elpased: 1.693
batch start
#iterations: 311
currently lose_sum: 506.2710962295532
time_elpased: 1.731
batch start
#iterations: 312
currently lose_sum: 508.2405807375908
time_elpased: 1.718
batch start
#iterations: 313
currently lose_sum: 508.4365838766098
time_elpased: 1.723
batch start
#iterations: 314
currently lose_sum: 506.5268358886242
time_elpased: 1.729
batch start
#iterations: 315
currently lose_sum: 506.2083760499954
time_elpased: 1.736
batch start
#iterations: 316
currently lose_sum: 507.2047390937805
time_elpased: 1.712
batch start
#iterations: 317
currently lose_sum: 506.7884094119072
time_elpased: 1.696
batch start
#iterations: 318
currently lose_sum: 509.4179599583149
time_elpased: 1.672
batch start
#iterations: 319
currently lose_sum: 507.42483443021774
time_elpased: 1.685
start validation test
0.109484536082
1.0
0.109484536082
0.1973610853
0
validation finish
batch start
#iterations: 320
currently lose_sum: 507.91079860925674
time_elpased: 1.754
batch start
#iterations: 321
currently lose_sum: 506.59498462080956
time_elpased: 1.684
batch start
#iterations: 322
currently lose_sum: 506.64134562015533
time_elpased: 1.673
batch start
#iterations: 323
currently lose_sum: 508.54530200362206
time_elpased: 1.723
batch start
#iterations: 324
currently lose_sum: 508.90733274817467
time_elpased: 1.688
batch start
#iterations: 325
currently lose_sum: 508.9750811457634
time_elpased: 1.678
batch start
#iterations: 326
currently lose_sum: 508.80521324276924
time_elpased: 1.706
batch start
#iterations: 327
currently lose_sum: 508.2944675683975
time_elpased: 1.721
batch start
#iterations: 328
currently lose_sum: 506.9698475897312
time_elpased: 1.677
batch start
#iterations: 329
currently lose_sum: 507.55915954709053
time_elpased: 1.731
batch start
#iterations: 330
currently lose_sum: 507.5945850610733
time_elpased: 1.742
batch start
#iterations: 331
currently lose_sum: 510.26771399378777
time_elpased: 1.738
batch start
#iterations: 332
currently lose_sum: 508.27657476067543
time_elpased: 1.68
batch start
#iterations: 333
currently lose_sum: 507.8564285337925
time_elpased: 1.69
batch start
#iterations: 334
currently lose_sum: 508.9922626912594
time_elpased: 1.729
batch start
#iterations: 335
currently lose_sum: 505.45082768797874
time_elpased: 1.706
batch start
#iterations: 336
currently lose_sum: 507.4551035165787
time_elpased: 1.75
batch start
#iterations: 337
currently lose_sum: 506.9441225528717
time_elpased: 1.711
batch start
#iterations: 338
currently lose_sum: 508.0927647948265
time_elpased: 1.676
batch start
#iterations: 339
currently lose_sum: 506.9478292167187
time_elpased: 1.724
start validation test
0.168762886598
1.0
0.168762886598
0.288788921231
0
validation finish
batch start
#iterations: 340
currently lose_sum: 507.00786685943604
time_elpased: 1.745
batch start
#iterations: 341
currently lose_sum: 507.85104140639305
time_elpased: 1.728
batch start
#iterations: 342
currently lose_sum: 508.1150372326374
time_elpased: 1.675
batch start
#iterations: 343
currently lose_sum: 507.17920649051666
time_elpased: 1.767
batch start
#iterations: 344
currently lose_sum: 508.87672397494316
time_elpased: 1.678
batch start
#iterations: 345
currently lose_sum: 509.43230947852135
time_elpased: 1.681
batch start
#iterations: 346
currently lose_sum: 506.0739306509495
time_elpased: 1.671
batch start
#iterations: 347
currently lose_sum: 508.57844457030296
time_elpased: 1.735
batch start
#iterations: 348
currently lose_sum: 508.2828167080879
time_elpased: 1.735
batch start
#iterations: 349
currently lose_sum: 508.54481449723244
time_elpased: 1.729
batch start
#iterations: 350
currently lose_sum: 507.92747297883034
time_elpased: 1.739
batch start
#iterations: 351
currently lose_sum: 506.2403524518013
time_elpased: 1.749
batch start
#iterations: 352
currently lose_sum: 506.7858825325966
time_elpased: 1.702
batch start
#iterations: 353
currently lose_sum: 508.7801723778248
time_elpased: 1.688
batch start
#iterations: 354
currently lose_sum: 506.70190101861954
time_elpased: 1.793
batch start
#iterations: 355
currently lose_sum: 508.0962719321251
time_elpased: 1.692
batch start
#iterations: 356
currently lose_sum: 508.08322262763977
time_elpased: 1.687
batch start
#iterations: 357
currently lose_sum: 508.0753936767578
time_elpased: 1.712
batch start
#iterations: 358
currently lose_sum: 508.25301653146744
time_elpased: 1.772
batch start
#iterations: 359
currently lose_sum: 506.84372675418854
time_elpased: 1.687
start validation test
0.102371134021
1.0
0.102371134021
0.185728981577
0
validation finish
batch start
#iterations: 360
currently lose_sum: 509.110876172781
time_elpased: 1.758
batch start
#iterations: 361
currently lose_sum: 505.8773071169853
time_elpased: 1.747
batch start
#iterations: 362
currently lose_sum: 508.48481050133705
time_elpased: 1.685
batch start
#iterations: 363
currently lose_sum: 506.6242729127407
time_elpased: 1.7
batch start
#iterations: 364
currently lose_sum: 507.2673427760601
time_elpased: 1.683
batch start
#iterations: 365
currently lose_sum: 507.6728173792362
time_elpased: 1.728
batch start
#iterations: 366
currently lose_sum: 508.9546974003315
time_elpased: 1.763
batch start
#iterations: 367
currently lose_sum: 507.93398225307465
time_elpased: 1.739
batch start
#iterations: 368
currently lose_sum: 506.8621390759945
time_elpased: 1.687
batch start
#iterations: 369
currently lose_sum: 506.9225220680237
time_elpased: 1.688
batch start
#iterations: 370
currently lose_sum: 506.7655166089535
time_elpased: 1.775
batch start
#iterations: 371
currently lose_sum: 507.85877600312233
time_elpased: 1.712
batch start
#iterations: 372
currently lose_sum: 506.7796891629696
time_elpased: 1.685
batch start
#iterations: 373
currently lose_sum: 507.08784717321396
time_elpased: 1.689
batch start
#iterations: 374
currently lose_sum: 507.84925389289856
time_elpased: 1.73
batch start
#iterations: 375
currently lose_sum: 508.0199516415596
time_elpased: 1.699
batch start
#iterations: 376
currently lose_sum: 507.4312744438648
time_elpased: 1.739
batch start
#iterations: 377
currently lose_sum: 508.08745139837265
time_elpased: 1.696
batch start
#iterations: 378
currently lose_sum: 507.59601855278015
time_elpased: 1.762
batch start
#iterations: 379
currently lose_sum: 506.81858509778976
time_elpased: 1.699
start validation test
0.0942268041237
1.0
0.0942268041237
0.172225362728
0
validation finish
batch start
#iterations: 380
currently lose_sum: 507.2416358590126
time_elpased: 1.682
batch start
#iterations: 381
currently lose_sum: 506.96903613209724
time_elpased: 1.689
batch start
#iterations: 382
currently lose_sum: 508.6994724869728
time_elpased: 1.765
batch start
#iterations: 383
currently lose_sum: 506.0278119444847
time_elpased: 1.718
batch start
#iterations: 384
currently lose_sum: 505.5770235955715
time_elpased: 1.728
batch start
#iterations: 385
currently lose_sum: 505.8199722468853
time_elpased: 1.7
batch start
#iterations: 386
currently lose_sum: 507.32994267344475
time_elpased: 1.756
batch start
#iterations: 387
currently lose_sum: 507.2459424138069
time_elpased: 1.774
batch start
#iterations: 388
currently lose_sum: 507.76403626799583
time_elpased: 1.738
batch start
#iterations: 389
currently lose_sum: 506.09159687161446
time_elpased: 1.684
batch start
#iterations: 390
currently lose_sum: 504.8011691272259
time_elpased: 1.688
batch start
#iterations: 391
currently lose_sum: 507.0980726778507
time_elpased: 1.777
batch start
#iterations: 392
currently lose_sum: 507.30750170350075
time_elpased: 1.693
batch start
#iterations: 393
currently lose_sum: 507.62468978762627
time_elpased: 1.709
batch start
#iterations: 394
currently lose_sum: 507.4362871646881
time_elpased: 1.692
batch start
#iterations: 395
currently lose_sum: 506.7687343657017
time_elpased: 1.735
batch start
#iterations: 396
currently lose_sum: 507.4867486655712
time_elpased: 1.696
batch start
#iterations: 397
currently lose_sum: 507.26924723386765
time_elpased: 1.675
batch start
#iterations: 398
currently lose_sum: 507.1561414003372
time_elpased: 1.741
batch start
#iterations: 399
currently lose_sum: 507.17887231707573
time_elpased: 1.684
start validation test
0.0880412371134
1.0
0.0880412371134
0.161834375592
0
validation finish
acc: 0.271
pre: 1.000
rec: 0.271
F1: 0.427
auc: 0.000
