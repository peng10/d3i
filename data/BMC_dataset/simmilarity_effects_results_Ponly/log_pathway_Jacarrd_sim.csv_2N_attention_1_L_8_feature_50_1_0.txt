start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 554.7745745778084
time_elpased: 1.679
batch start
#iterations: 1
currently lose_sum: 543.0608476400375
time_elpased: 1.62
batch start
#iterations: 2
currently lose_sum: 537.0618552267551
time_elpased: 1.619
batch start
#iterations: 3
currently lose_sum: 531.5477703809738
time_elpased: 1.605
batch start
#iterations: 4
currently lose_sum: 527.7882596850395
time_elpased: 1.611
batch start
#iterations: 5
currently lose_sum: 525.7510012388229
time_elpased: 1.623
batch start
#iterations: 6
currently lose_sum: 524.5276141166687
time_elpased: 1.616
batch start
#iterations: 7
currently lose_sum: 522.4178946316242
time_elpased: 1.612
batch start
#iterations: 8
currently lose_sum: 522.4829961359501
time_elpased: 1.616
batch start
#iterations: 9
currently lose_sum: 524.1812447011471
time_elpased: 1.614
batch start
#iterations: 10
currently lose_sum: 522.3751250207424
time_elpased: 1.614
batch start
#iterations: 11
currently lose_sum: 523.2834427058697
time_elpased: 1.626
batch start
#iterations: 12
currently lose_sum: 522.6630277335644
time_elpased: 1.616
batch start
#iterations: 13
currently lose_sum: 520.6183462142944
time_elpased: 1.625
batch start
#iterations: 14
currently lose_sum: 520.8233714699745
time_elpased: 1.626
batch start
#iterations: 15
currently lose_sum: 518.1504913568497
time_elpased: 1.618
batch start
#iterations: 16
currently lose_sum: 519.4946493506432
time_elpased: 1.619
batch start
#iterations: 17
currently lose_sum: 518.2083363831043
time_elpased: 1.615
batch start
#iterations: 18
currently lose_sum: 517.9863226413727
time_elpased: 1.614
batch start
#iterations: 19
currently lose_sum: 519.2355385124683
time_elpased: 1.617
start validation test
0.160618556701
1.0
0.160618556701
0.276780955765
0
validation finish
batch start
#iterations: 20
currently lose_sum: 517.6950154900551
time_elpased: 1.607
batch start
#iterations: 21
currently lose_sum: 519.5453211963177
time_elpased: 1.609
batch start
#iterations: 22
currently lose_sum: 517.3082833588123
time_elpased: 1.619
batch start
#iterations: 23
currently lose_sum: 517.9737103283405
time_elpased: 1.6
batch start
#iterations: 24
currently lose_sum: 517.5915894508362
time_elpased: 1.629
batch start
#iterations: 25
currently lose_sum: 517.4371775388718
time_elpased: 1.615
batch start
#iterations: 26
currently lose_sum: 517.484979569912
time_elpased: 1.614
batch start
#iterations: 27
currently lose_sum: 516.9116132855415
time_elpased: 1.605
batch start
#iterations: 28
currently lose_sum: 515.7173245847225
time_elpased: 1.608
batch start
#iterations: 29
currently lose_sum: 516.6563993990421
time_elpased: 1.597
batch start
#iterations: 30
currently lose_sum: 514.3171715736389
time_elpased: 1.623
batch start
#iterations: 31
currently lose_sum: 516.1957124769688
time_elpased: 1.62
batch start
#iterations: 32
currently lose_sum: 515.42022767663
time_elpased: 1.618
batch start
#iterations: 33
currently lose_sum: 515.3388889729977
time_elpased: 1.596
batch start
#iterations: 34
currently lose_sum: 514.6358073055744
time_elpased: 1.592
batch start
#iterations: 35
currently lose_sum: 514.5133610665798
time_elpased: 1.609
batch start
#iterations: 36
currently lose_sum: 515.2160152196884
time_elpased: 1.611
batch start
#iterations: 37
currently lose_sum: 513.601107776165
time_elpased: 1.606
batch start
#iterations: 38
currently lose_sum: 514.6010906398296
time_elpased: 1.602
batch start
#iterations: 39
currently lose_sum: 515.6591813266277
time_elpased: 1.599
start validation test
0.0988659793814
1.0
0.0988659793814
0.179941833193
0
validation finish
batch start
#iterations: 40
currently lose_sum: 515.7398829460144
time_elpased: 1.614
batch start
#iterations: 41
currently lose_sum: 515.2162035703659
time_elpased: 1.618
batch start
#iterations: 42
currently lose_sum: 513.7769925892353
time_elpased: 1.622
batch start
#iterations: 43
currently lose_sum: 513.4043934345245
time_elpased: 1.61
batch start
#iterations: 44
currently lose_sum: 515.4036355316639
time_elpased: 1.624
batch start
#iterations: 45
currently lose_sum: 515.6825390458107
time_elpased: 1.594
batch start
#iterations: 46
currently lose_sum: 512.6706196069717
time_elpased: 1.649
batch start
#iterations: 47
currently lose_sum: 513.2536582052708
time_elpased: 1.6
batch start
#iterations: 48
currently lose_sum: 514.3906985521317
time_elpased: 1.608
batch start
#iterations: 49
currently lose_sum: 512.3788855671883
time_elpased: 1.613
batch start
#iterations: 50
currently lose_sum: 514.5018864274025
time_elpased: 1.613
batch start
#iterations: 51
currently lose_sum: 513.1116223335266
time_elpased: 1.611
batch start
#iterations: 52
currently lose_sum: 514.399022936821
time_elpased: 1.596
batch start
#iterations: 53
currently lose_sum: 513.9131393432617
time_elpased: 1.601
batch start
#iterations: 54
currently lose_sum: 512.4378438293934
time_elpased: 1.6
batch start
#iterations: 55
currently lose_sum: 513.764119207859
time_elpased: 1.618
batch start
#iterations: 56
currently lose_sum: 513.7887137234211
time_elpased: 1.626
batch start
#iterations: 57
currently lose_sum: 512.1111513078213
time_elpased: 1.605
batch start
#iterations: 58
currently lose_sum: 514.7757233381271
time_elpased: 1.605
batch start
#iterations: 59
currently lose_sum: 512.6469423770905
time_elpased: 1.62
start validation test
0.145979381443
1.0
0.145979381443
0.254767902123
0
validation finish
batch start
#iterations: 60
currently lose_sum: 512.801282286644
time_elpased: 1.627
batch start
#iterations: 61
currently lose_sum: 512.1692213714123
time_elpased: 1.606
batch start
#iterations: 62
currently lose_sum: 514.7528386712074
time_elpased: 1.605
batch start
#iterations: 63
currently lose_sum: 513.7024316489697
time_elpased: 1.629
batch start
#iterations: 64
currently lose_sum: 513.9562198221684
time_elpased: 1.609
batch start
#iterations: 65
currently lose_sum: 513.573483467102
time_elpased: 1.611
batch start
#iterations: 66
currently lose_sum: 512.3975552916527
time_elpased: 1.615
batch start
#iterations: 67
currently lose_sum: 511.1070195734501
time_elpased: 1.618
batch start
#iterations: 68
currently lose_sum: 512.817255795002
time_elpased: 1.62
batch start
#iterations: 69
currently lose_sum: 514.8493663668633
time_elpased: 1.611
batch start
#iterations: 70
currently lose_sum: 511.2114062011242
time_elpased: 1.611
batch start
#iterations: 71
currently lose_sum: 512.6942220628262
time_elpased: 1.621
batch start
#iterations: 72
currently lose_sum: 514.6530958712101
time_elpased: 1.605
batch start
#iterations: 73
currently lose_sum: 512.0855821669102
time_elpased: 1.612
batch start
#iterations: 74
currently lose_sum: 513.7252863943577
time_elpased: 1.608
batch start
#iterations: 75
currently lose_sum: 511.8787690103054
time_elpased: 1.609
batch start
#iterations: 76
currently lose_sum: 513.452064961195
time_elpased: 1.614
batch start
#iterations: 77
currently lose_sum: 513.224599301815
time_elpased: 1.605
batch start
#iterations: 78
currently lose_sum: 512.9847428798676
time_elpased: 1.613
batch start
#iterations: 79
currently lose_sum: 514.2624357640743
time_elpased: 1.619
start validation test
0.242268041237
1.0
0.242268041237
0.390041493776
0
validation finish
batch start
#iterations: 80
currently lose_sum: 511.4322177171707
time_elpased: 1.654
batch start
#iterations: 81
currently lose_sum: 511.19874411821365
time_elpased: 1.618
batch start
#iterations: 82
currently lose_sum: 511.47167894244194
time_elpased: 1.626
batch start
#iterations: 83
currently lose_sum: 513.2769267261028
time_elpased: 1.626
batch start
#iterations: 84
currently lose_sum: 511.91613960266113
time_elpased: 1.618
batch start
#iterations: 85
currently lose_sum: 510.94905775785446
time_elpased: 1.607
batch start
#iterations: 86
currently lose_sum: 511.9110109806061
time_elpased: 1.621
batch start
#iterations: 87
currently lose_sum: 510.7420245707035
time_elpased: 1.608
batch start
#iterations: 88
currently lose_sum: 511.5353526175022
time_elpased: 1.608
batch start
#iterations: 89
currently lose_sum: 511.903815895319
time_elpased: 1.598
batch start
#iterations: 90
currently lose_sum: 511.6106103658676
time_elpased: 1.618
batch start
#iterations: 91
currently lose_sum: 510.210470020771
time_elpased: 1.613
batch start
#iterations: 92
currently lose_sum: 512.3072320520878
time_elpased: 1.609
batch start
#iterations: 93
currently lose_sum: 511.354827105999
time_elpased: 1.616
batch start
#iterations: 94
currently lose_sum: 510.86118841171265
time_elpased: 1.606
batch start
#iterations: 95
currently lose_sum: 510.6084961295128
time_elpased: 1.619
batch start
#iterations: 96
currently lose_sum: 512.6492784917355
time_elpased: 1.64
batch start
#iterations: 97
currently lose_sum: 509.00618010759354
time_elpased: 1.608
batch start
#iterations: 98
currently lose_sum: 511.82976216077805
time_elpased: 1.603
batch start
#iterations: 99
currently lose_sum: 512.558457493782
time_elpased: 1.611
start validation test
0.099381443299
1.0
0.099381443299
0.1807951988
0
validation finish
batch start
#iterations: 100
currently lose_sum: 512.2095795273781
time_elpased: 1.636
batch start
#iterations: 101
currently lose_sum: 511.93383115530014
time_elpased: 1.628
batch start
#iterations: 102
currently lose_sum: 510.14303866028786
time_elpased: 1.628
batch start
#iterations: 103
currently lose_sum: 512.1471004188061
time_elpased: 1.616
batch start
#iterations: 104
currently lose_sum: 511.5052455365658
time_elpased: 1.614
batch start
#iterations: 105
currently lose_sum: 510.50089916586876
time_elpased: 1.631
batch start
#iterations: 106
currently lose_sum: 512.6944832503796
time_elpased: 1.618
batch start
#iterations: 107
currently lose_sum: 509.6499340236187
time_elpased: 1.628
batch start
#iterations: 108
currently lose_sum: 509.27501341700554
time_elpased: 1.641
batch start
#iterations: 109
currently lose_sum: 511.7737802863121
time_elpased: 1.62
batch start
#iterations: 110
currently lose_sum: 512.5174661576748
time_elpased: 1.616
batch start
#iterations: 111
currently lose_sum: 514.3106354773045
time_elpased: 1.609
batch start
#iterations: 112
currently lose_sum: 509.2691380083561
time_elpased: 1.623
batch start
#iterations: 113
currently lose_sum: 511.28879886865616
time_elpased: 1.617
batch start
#iterations: 114
currently lose_sum: 510.81840881705284
time_elpased: 1.61
batch start
#iterations: 115
currently lose_sum: 510.61136585474014
time_elpased: 1.603
batch start
#iterations: 116
currently lose_sum: 510.1703457534313
time_elpased: 1.621
batch start
#iterations: 117
currently lose_sum: 511.7035058736801
time_elpased: 1.608
batch start
#iterations: 118
currently lose_sum: 511.12511283159256
time_elpased: 1.618
batch start
#iterations: 119
currently lose_sum: 511.28462341427803
time_elpased: 1.606
start validation test
0.199896907216
1.0
0.199896907216
0.33319013661
0
validation finish
batch start
#iterations: 120
currently lose_sum: 511.65028724074364
time_elpased: 1.647
batch start
#iterations: 121
currently lose_sum: 510.21928682923317
time_elpased: 1.614
batch start
#iterations: 122
currently lose_sum: 509.50133165717125
time_elpased: 1.635
batch start
#iterations: 123
currently lose_sum: 509.1384171843529
time_elpased: 1.61
batch start
#iterations: 124
currently lose_sum: 508.9213145971298
time_elpased: 1.601
batch start
#iterations: 125
currently lose_sum: 511.59743416309357
time_elpased: 1.63
batch start
#iterations: 126
currently lose_sum: 512.0651941001415
time_elpased: 1.612
batch start
#iterations: 127
currently lose_sum: 511.4587344825268
time_elpased: 1.631
batch start
#iterations: 128
currently lose_sum: 510.5406717956066
time_elpased: 1.64
batch start
#iterations: 129
currently lose_sum: 510.76386818289757
time_elpased: 1.633
batch start
#iterations: 130
currently lose_sum: 510.2981811761856
time_elpased: 1.622
batch start
#iterations: 131
currently lose_sum: 511.03941690921783
time_elpased: 1.616
batch start
#iterations: 132
currently lose_sum: 510.5540162026882
time_elpased: 1.626
batch start
#iterations: 133
currently lose_sum: 511.63575661182404
time_elpased: 1.642
batch start
#iterations: 134
currently lose_sum: 509.6476108133793
time_elpased: 1.641
batch start
#iterations: 135
currently lose_sum: 509.88165307044983
time_elpased: 1.617
batch start
#iterations: 136
currently lose_sum: 510.34695306420326
time_elpased: 1.613
batch start
#iterations: 137
currently lose_sum: 508.6324495971203
time_elpased: 1.628
batch start
#iterations: 138
currently lose_sum: 508.4074532389641
time_elpased: 1.62
batch start
#iterations: 139
currently lose_sum: 510.18985372781754
time_elpased: 1.604
start validation test
0.130927835052
1.0
0.130927835052
0.231540565178
0
validation finish
batch start
#iterations: 140
currently lose_sum: 508.3126593232155
time_elpased: 1.609
batch start
#iterations: 141
currently lose_sum: 509.8299718797207
time_elpased: 1.613
batch start
#iterations: 142
currently lose_sum: 508.86524525284767
time_elpased: 1.62
batch start
#iterations: 143
currently lose_sum: 510.0710914134979
time_elpased: 1.62
batch start
#iterations: 144
currently lose_sum: 508.5028816461563
time_elpased: 1.632
batch start
#iterations: 145
currently lose_sum: 510.46282601356506
time_elpased: 1.628
batch start
#iterations: 146
currently lose_sum: 511.2206770181656
time_elpased: 1.621
batch start
#iterations: 147
currently lose_sum: 511.09379065036774
time_elpased: 1.618
batch start
#iterations: 148
currently lose_sum: 508.77947357296944
time_elpased: 1.612
batch start
#iterations: 149
currently lose_sum: 510.60437670350075
time_elpased: 1.62
batch start
#iterations: 150
currently lose_sum: 510.04804956912994
time_elpased: 1.605
batch start
#iterations: 151
currently lose_sum: 508.7134593427181
time_elpased: 1.614
batch start
#iterations: 152
currently lose_sum: 509.6881156563759
time_elpased: 1.615
batch start
#iterations: 153
currently lose_sum: 507.9008931815624
time_elpased: 1.617
batch start
#iterations: 154
currently lose_sum: 512.9138953387737
time_elpased: 1.621
batch start
#iterations: 155
currently lose_sum: 507.76855251193047
time_elpased: 1.618
batch start
#iterations: 156
currently lose_sum: 508.83556962013245
time_elpased: 1.612
batch start
#iterations: 157
currently lose_sum: 508.89775452017784
time_elpased: 1.623
batch start
#iterations: 158
currently lose_sum: 509.7018813788891
time_elpased: 1.615
batch start
#iterations: 159
currently lose_sum: 509.54041612148285
time_elpased: 1.625
start validation test
0.129381443299
1.0
0.129381443299
0.229119123688
0
validation finish
batch start
#iterations: 160
currently lose_sum: 510.77491495013237
time_elpased: 1.616
batch start
#iterations: 161
currently lose_sum: 508.78990921378136
time_elpased: 1.612
batch start
#iterations: 162
currently lose_sum: 509.0030480623245
time_elpased: 1.625
batch start
#iterations: 163
currently lose_sum: 508.8237869143486
time_elpased: 1.624
batch start
#iterations: 164
currently lose_sum: 510.7010498344898
time_elpased: 1.622
batch start
#iterations: 165
currently lose_sum: 511.81730660796165
time_elpased: 1.62
batch start
#iterations: 166
currently lose_sum: 509.91567838191986
time_elpased: 1.598
batch start
#iterations: 167
currently lose_sum: 508.8545433282852
time_elpased: 1.608
batch start
#iterations: 168
currently lose_sum: 508.7726768553257
time_elpased: 1.616
batch start
#iterations: 169
currently lose_sum: 509.9995102882385
time_elpased: 1.616
batch start
#iterations: 170
currently lose_sum: 508.7509586215019
time_elpased: 1.604
batch start
#iterations: 171
currently lose_sum: 508.7493759095669
time_elpased: 1.625
batch start
#iterations: 172
currently lose_sum: 510.8835570514202
time_elpased: 1.624
batch start
#iterations: 173
currently lose_sum: 510.09393402934074
time_elpased: 1.612
batch start
#iterations: 174
currently lose_sum: 509.9886117577553
time_elpased: 1.612
batch start
#iterations: 175
currently lose_sum: 510.70215970277786
time_elpased: 1.605
batch start
#iterations: 176
currently lose_sum: 507.34543961286545
time_elpased: 1.615
batch start
#iterations: 177
currently lose_sum: 509.86978045105934
time_elpased: 1.614
batch start
#iterations: 178
currently lose_sum: 510.1325857639313
time_elpased: 1.625
batch start
#iterations: 179
currently lose_sum: 510.0861733853817
time_elpased: 1.615
start validation test
0.138762886598
1.0
0.138762886598
0.24370812964
0
validation finish
batch start
#iterations: 180
currently lose_sum: 509.0008855462074
time_elpased: 1.609
batch start
#iterations: 181
currently lose_sum: 510.66686803102493
time_elpased: 1.621
batch start
#iterations: 182
currently lose_sum: 510.32382065057755
time_elpased: 1.636
batch start
#iterations: 183
currently lose_sum: 511.4366905391216
time_elpased: 1.615
batch start
#iterations: 184
currently lose_sum: 508.7010294497013
time_elpased: 1.623
batch start
#iterations: 185
currently lose_sum: 510.30122712254524
time_elpased: 1.614
batch start
#iterations: 186
currently lose_sum: 510.0207509994507
time_elpased: 1.616
batch start
#iterations: 187
currently lose_sum: 510.7825195491314
time_elpased: 1.604
batch start
#iterations: 188
currently lose_sum: 509.4278078377247
time_elpased: 1.601
batch start
#iterations: 189
currently lose_sum: 510.4447731077671
time_elpased: 1.612
batch start
#iterations: 190
currently lose_sum: 509.1647989153862
time_elpased: 1.615
batch start
#iterations: 191
currently lose_sum: 510.4468658864498
time_elpased: 1.619
batch start
#iterations: 192
currently lose_sum: 507.74684661626816
time_elpased: 1.615
batch start
#iterations: 193
currently lose_sum: 509.1655002236366
time_elpased: 1.595
batch start
#iterations: 194
currently lose_sum: 510.5045087337494
time_elpased: 1.642
batch start
#iterations: 195
currently lose_sum: 511.73770040273666
time_elpased: 1.602
batch start
#iterations: 196
currently lose_sum: 507.6367720067501
time_elpased: 1.61
batch start
#iterations: 197
currently lose_sum: 511.4397945404053
time_elpased: 1.62
batch start
#iterations: 198
currently lose_sum: 508.9599262177944
time_elpased: 1.609
batch start
#iterations: 199
currently lose_sum: 509.5168615579605
time_elpased: 1.612
start validation test
0.116082474227
1.0
0.116082474227
0.208017735082
0
validation finish
batch start
#iterations: 200
currently lose_sum: 509.60236245393753
time_elpased: 1.625
batch start
#iterations: 201
currently lose_sum: 511.2949985563755
time_elpased: 1.623
batch start
#iterations: 202
currently lose_sum: 507.57724863290787
time_elpased: 1.6
batch start
#iterations: 203
currently lose_sum: 508.475684016943
time_elpased: 1.625
batch start
#iterations: 204
currently lose_sum: 508.47173154354095
time_elpased: 1.616
batch start
#iterations: 205
currently lose_sum: 509.2436745464802
time_elpased: 1.608
batch start
#iterations: 206
currently lose_sum: 510.86136519908905
time_elpased: 1.612
batch start
#iterations: 207
currently lose_sum: 508.8384721875191
time_elpased: 1.619
batch start
#iterations: 208
currently lose_sum: 507.30376479029655
time_elpased: 1.622
batch start
#iterations: 209
currently lose_sum: 510.49255934357643
time_elpased: 1.613
batch start
#iterations: 210
currently lose_sum: 508.25790974497795
time_elpased: 1.626
batch start
#iterations: 211
currently lose_sum: 508.12433072924614
time_elpased: 1.609
batch start
#iterations: 212
currently lose_sum: 508.3013687133789
time_elpased: 1.616
batch start
#iterations: 213
currently lose_sum: 508.4068857431412
time_elpased: 1.62
batch start
#iterations: 214
currently lose_sum: 508.3090769648552
time_elpased: 1.6
batch start
#iterations: 215
currently lose_sum: 509.81914350390434
time_elpased: 1.619
batch start
#iterations: 216
currently lose_sum: 509.68979731202126
time_elpased: 1.618
batch start
#iterations: 217
currently lose_sum: 509.5141541361809
time_elpased: 1.604
batch start
#iterations: 218
currently lose_sum: 509.17274901270866
time_elpased: 1.611
batch start
#iterations: 219
currently lose_sum: 511.0779490470886
time_elpased: 1.621
start validation test
0.12412371134
1.0
0.12412371134
0.220836390315
0
validation finish
batch start
#iterations: 220
currently lose_sum: 509.553170979023
time_elpased: 1.626
batch start
#iterations: 221
currently lose_sum: 510.6810643672943
time_elpased: 1.611
batch start
#iterations: 222
currently lose_sum: 507.8108831346035
time_elpased: 1.614
batch start
#iterations: 223
currently lose_sum: 508.7904707491398
time_elpased: 1.628
batch start
#iterations: 224
currently lose_sum: 509.3576467037201
time_elpased: 1.608
batch start
#iterations: 225
currently lose_sum: 510.6299475431442
time_elpased: 1.612
batch start
#iterations: 226
currently lose_sum: 508.7290807068348
time_elpased: 1.614
batch start
#iterations: 227
currently lose_sum: 508.4172561764717
time_elpased: 1.622
batch start
#iterations: 228
currently lose_sum: 507.5972307026386
time_elpased: 1.608
batch start
#iterations: 229
currently lose_sum: 509.8653685748577
time_elpased: 1.617
batch start
#iterations: 230
currently lose_sum: 510.455009996891
time_elpased: 1.615
batch start
#iterations: 231
currently lose_sum: 511.0811020433903
time_elpased: 1.618
batch start
#iterations: 232
currently lose_sum: 508.69566801190376
time_elpased: 1.615
batch start
#iterations: 233
currently lose_sum: 507.6611638665199
time_elpased: 1.617
batch start
#iterations: 234
currently lose_sum: 507.2118863463402
time_elpased: 1.624
batch start
#iterations: 235
currently lose_sum: 506.7013254761696
time_elpased: 1.615
batch start
#iterations: 236
currently lose_sum: 508.0985278785229
time_elpased: 1.605
batch start
#iterations: 237
currently lose_sum: 510.2333558499813
time_elpased: 1.605
batch start
#iterations: 238
currently lose_sum: 507.84893921017647
time_elpased: 1.641
batch start
#iterations: 239
currently lose_sum: 507.6650316119194
time_elpased: 1.613
start validation test
0.102577319588
1.0
0.102577319588
0.186068256194
0
validation finish
batch start
#iterations: 240
currently lose_sum: 508.2544902563095
time_elpased: 1.62
batch start
#iterations: 241
currently lose_sum: 508.0488647520542
time_elpased: 1.641
batch start
#iterations: 242
currently lose_sum: 508.09471240639687
time_elpased: 1.619
batch start
#iterations: 243
currently lose_sum: 507.9109804928303
time_elpased: 1.623
batch start
#iterations: 244
currently lose_sum: 508.5118283331394
time_elpased: 1.617
batch start
#iterations: 245
currently lose_sum: 506.68594321608543
time_elpased: 1.625
batch start
#iterations: 246
currently lose_sum: 508.1333256959915
time_elpased: 1.629
batch start
#iterations: 247
currently lose_sum: 508.68343034386635
time_elpased: 1.608
batch start
#iterations: 248
currently lose_sum: 507.4349903166294
time_elpased: 1.645
batch start
#iterations: 249
currently lose_sum: 510.11588060855865
time_elpased: 1.625
batch start
#iterations: 250
currently lose_sum: 508.2204954624176
time_elpased: 1.613
batch start
#iterations: 251
currently lose_sum: 509.8963857591152
time_elpased: 1.625
batch start
#iterations: 252
currently lose_sum: 511.36046770215034
time_elpased: 1.623
batch start
#iterations: 253
currently lose_sum: 508.7689186632633
time_elpased: 1.624
batch start
#iterations: 254
currently lose_sum: 510.40178766846657
time_elpased: 1.612
batch start
#iterations: 255
currently lose_sum: 506.90780076384544
time_elpased: 1.609
batch start
#iterations: 256
currently lose_sum: 510.30486100912094
time_elpased: 1.629
batch start
#iterations: 257
currently lose_sum: 507.6077438592911
time_elpased: 1.622
batch start
#iterations: 258
currently lose_sum: 507.73039039969444
time_elpased: 1.616
batch start
#iterations: 259
currently lose_sum: 509.7633210718632
time_elpased: 1.629
start validation test
0.132783505155
1.0
0.132783505155
0.234437568256
0
validation finish
batch start
#iterations: 260
currently lose_sum: 509.507326990366
time_elpased: 1.615
batch start
#iterations: 261
currently lose_sum: 509.5414557158947
time_elpased: 1.619
batch start
#iterations: 262
currently lose_sum: 508.5907328426838
time_elpased: 1.602
batch start
#iterations: 263
currently lose_sum: 508.1717948317528
time_elpased: 1.622
batch start
#iterations: 264
currently lose_sum: 507.3598223924637
time_elpased: 1.61
batch start
#iterations: 265
currently lose_sum: 509.16987267136574
time_elpased: 1.633
batch start
#iterations: 266
currently lose_sum: 507.55570900440216
time_elpased: 1.613
batch start
#iterations: 267
currently lose_sum: 507.31588250398636
time_elpased: 1.615
batch start
#iterations: 268
currently lose_sum: 508.46645763516426
time_elpased: 1.635
batch start
#iterations: 269
currently lose_sum: 506.50406432151794
time_elpased: 1.628
batch start
#iterations: 270
currently lose_sum: 509.36293798685074
time_elpased: 1.612
batch start
#iterations: 271
currently lose_sum: 505.7162271440029
time_elpased: 1.612
batch start
#iterations: 272
currently lose_sum: 508.6305429637432
time_elpased: 1.613
batch start
#iterations: 273
currently lose_sum: 507.83635061979294
time_elpased: 1.61
batch start
#iterations: 274
currently lose_sum: 507.97481620311737
time_elpased: 1.622
batch start
#iterations: 275
currently lose_sum: 507.00230035185814
time_elpased: 1.629
batch start
#iterations: 276
currently lose_sum: 508.11606565117836
time_elpased: 1.635
batch start
#iterations: 277
currently lose_sum: 509.60566660761833
time_elpased: 1.63
batch start
#iterations: 278
currently lose_sum: 508.295902043581
time_elpased: 1.611
batch start
#iterations: 279
currently lose_sum: 509.8303398489952
time_elpased: 1.615
start validation test
0.154536082474
1.0
0.154536082474
0.267702473435
0
validation finish
batch start
#iterations: 280
currently lose_sum: 507.76798674464226
time_elpased: 1.63
batch start
#iterations: 281
currently lose_sum: 506.24541464447975
time_elpased: 1.622
batch start
#iterations: 282
currently lose_sum: 507.8936221897602
time_elpased: 1.63
batch start
#iterations: 283
currently lose_sum: 509.03563517332077
time_elpased: 1.612
batch start
#iterations: 284
currently lose_sum: 508.54623249173164
time_elpased: 1.612
batch start
#iterations: 285
currently lose_sum: 509.1142663061619
time_elpased: 1.614
batch start
#iterations: 286
currently lose_sum: 509.38341292738914
time_elpased: 1.612
batch start
#iterations: 287
currently lose_sum: 509.91418769955635
time_elpased: 1.626
batch start
#iterations: 288
currently lose_sum: 507.508855253458
time_elpased: 1.612
batch start
#iterations: 289
currently lose_sum: 507.8129169046879
time_elpased: 1.619
batch start
#iterations: 290
currently lose_sum: 507.3069698512554
time_elpased: 1.628
batch start
#iterations: 291
currently lose_sum: 509.15135967731476
time_elpased: 1.643
batch start
#iterations: 292
currently lose_sum: 508.47041657567024
time_elpased: 1.616
batch start
#iterations: 293
currently lose_sum: 509.59491086006165
time_elpased: 1.604
batch start
#iterations: 294
currently lose_sum: 509.6346814930439
time_elpased: 1.614
batch start
#iterations: 295
currently lose_sum: 508.2568746507168
time_elpased: 1.629
batch start
#iterations: 296
currently lose_sum: 507.60098192095757
time_elpased: 1.607
batch start
#iterations: 297
currently lose_sum: 508.3890843987465
time_elpased: 1.633
batch start
#iterations: 298
currently lose_sum: 509.38638785481453
time_elpased: 1.616
batch start
#iterations: 299
currently lose_sum: 509.79433608055115
time_elpased: 1.616
start validation test
0.174226804124
1.0
0.174226804124
0.296751536435
0
validation finish
batch start
#iterations: 300
currently lose_sum: 508.936875551939
time_elpased: 1.613
batch start
#iterations: 301
currently lose_sum: 507.37290132045746
time_elpased: 1.612
batch start
#iterations: 302
currently lose_sum: 510.38626596331596
time_elpased: 1.629
batch start
#iterations: 303
currently lose_sum: 509.48061379790306
time_elpased: 1.626
batch start
#iterations: 304
currently lose_sum: 506.8648907840252
time_elpased: 1.621
batch start
#iterations: 305
currently lose_sum: 510.9493436515331
time_elpased: 1.624
batch start
#iterations: 306
currently lose_sum: 508.9682078063488
time_elpased: 1.607
batch start
#iterations: 307
currently lose_sum: 511.5333395898342
time_elpased: 1.608
batch start
#iterations: 308
currently lose_sum: 508.42931815981865
time_elpased: 1.626
batch start
#iterations: 309
currently lose_sum: 509.02058151364326
time_elpased: 1.625
batch start
#iterations: 310
currently lose_sum: 506.45596945285797
time_elpased: 1.622
batch start
#iterations: 311
currently lose_sum: 507.5597846508026
time_elpased: 1.615
batch start
#iterations: 312
currently lose_sum: 509.87144178152084
time_elpased: 1.604
batch start
#iterations: 313
currently lose_sum: 509.38446938991547
time_elpased: 1.628
batch start
#iterations: 314
currently lose_sum: 507.2852171957493
time_elpased: 1.621
batch start
#iterations: 315
currently lose_sum: 509.67152374982834
time_elpased: 1.628
batch start
#iterations: 316
currently lose_sum: 508.8369024991989
time_elpased: 1.607
batch start
#iterations: 317
currently lose_sum: 507.54150861501694
time_elpased: 1.619
batch start
#iterations: 318
currently lose_sum: 509.99175560474396
time_elpased: 1.608
batch start
#iterations: 319
currently lose_sum: 507.8872867822647
time_elpased: 1.613
start validation test
0.147422680412
1.0
0.147422680412
0.256963162624
0
validation finish
batch start
#iterations: 320
currently lose_sum: 509.03553584218025
time_elpased: 1.637
batch start
#iterations: 321
currently lose_sum: 509.3803496956825
time_elpased: 1.618
batch start
#iterations: 322
currently lose_sum: 506.28029522299767
time_elpased: 1.675
batch start
#iterations: 323
currently lose_sum: 508.419332832098
time_elpased: 1.611
batch start
#iterations: 324
currently lose_sum: 509.9508816599846
time_elpased: 1.627
batch start
#iterations: 325
currently lose_sum: 508.4485315978527
time_elpased: 1.613
batch start
#iterations: 326
currently lose_sum: 508.8816515803337
time_elpased: 1.611
batch start
#iterations: 327
currently lose_sum: 509.66280058026314
time_elpased: 1.613
batch start
#iterations: 328
currently lose_sum: 508.96684843301773
time_elpased: 1.612
batch start
#iterations: 329
currently lose_sum: 507.68145897984505
time_elpased: 1.633
batch start
#iterations: 330
currently lose_sum: 510.12240010499954
time_elpased: 1.618
batch start
#iterations: 331
currently lose_sum: 510.10483035445213
time_elpased: 1.628
batch start
#iterations: 332
currently lose_sum: 510.30111134052277
time_elpased: 1.63
batch start
#iterations: 333
currently lose_sum: 508.8316324055195
time_elpased: 1.611
batch start
#iterations: 334
currently lose_sum: 508.7334997057915
time_elpased: 1.627
batch start
#iterations: 335
currently lose_sum: 509.16824582219124
time_elpased: 1.615
batch start
#iterations: 336
currently lose_sum: 509.5996181368828
time_elpased: 1.615
batch start
#iterations: 337
currently lose_sum: 508.68716511130333
time_elpased: 1.623
batch start
#iterations: 338
currently lose_sum: 507.05436649918556
time_elpased: 1.622
batch start
#iterations: 339
currently lose_sum: 509.1062477827072
time_elpased: 1.602
start validation test
0.176907216495
1.0
0.176907216495
0.300630693763
0
validation finish
batch start
#iterations: 340
currently lose_sum: 506.636420160532
time_elpased: 1.621
batch start
#iterations: 341
currently lose_sum: 508.79363372921944
time_elpased: 1.624
batch start
#iterations: 342
currently lose_sum: 510.46052926778793
time_elpased: 1.621
batch start
#iterations: 343
currently lose_sum: 508.6501533985138
time_elpased: 1.623
batch start
#iterations: 344
currently lose_sum: 509.961960375309
time_elpased: 1.613
batch start
#iterations: 345
currently lose_sum: 509.3435769379139
time_elpased: 1.622
batch start
#iterations: 346
currently lose_sum: 508.17184168100357
time_elpased: 1.621
batch start
#iterations: 347
currently lose_sum: 507.48094698786736
time_elpased: 1.606
batch start
#iterations: 348
currently lose_sum: 508.8523539304733
time_elpased: 1.629
batch start
#iterations: 349
currently lose_sum: 509.71806487441063
time_elpased: 1.628
batch start
#iterations: 350
currently lose_sum: 508.3598009943962
time_elpased: 1.621
batch start
#iterations: 351
currently lose_sum: 508.15799844264984
time_elpased: 1.652
batch start
#iterations: 352
currently lose_sum: 507.92379093170166
time_elpased: 1.629
batch start
#iterations: 353
currently lose_sum: 509.6224211156368
time_elpased: 1.619
batch start
#iterations: 354
currently lose_sum: 507.6192317903042
time_elpased: 1.627
batch start
#iterations: 355
currently lose_sum: 508.15261921286583
time_elpased: 1.615
batch start
#iterations: 356
currently lose_sum: 506.8412072658539
time_elpased: 1.632
batch start
#iterations: 357
currently lose_sum: 508.9711578786373
time_elpased: 1.634
batch start
#iterations: 358
currently lose_sum: 509.30618634819984
time_elpased: 1.618
batch start
#iterations: 359
currently lose_sum: 507.56535616517067
time_elpased: 1.623
start validation test
0.131958762887
1.0
0.131958762887
0.233151183971
0
validation finish
batch start
#iterations: 360
currently lose_sum: 508.07199615240097
time_elpased: 1.642
batch start
#iterations: 361
currently lose_sum: 507.2891038954258
time_elpased: 1.627
batch start
#iterations: 362
currently lose_sum: 507.4870271086693
time_elpased: 1.623
batch start
#iterations: 363
currently lose_sum: 508.32649570703506
time_elpased: 1.615
batch start
#iterations: 364
currently lose_sum: 509.323312073946
time_elpased: 1.635
batch start
#iterations: 365
currently lose_sum: 507.8302446305752
time_elpased: 1.627
batch start
#iterations: 366
currently lose_sum: 507.92292869091034
time_elpased: 1.622
batch start
#iterations: 367
currently lose_sum: 508.498716622591
time_elpased: 1.627
batch start
#iterations: 368
currently lose_sum: 507.2079782783985
time_elpased: 1.624
batch start
#iterations: 369
currently lose_sum: 507.4163380563259
time_elpased: 1.614
batch start
#iterations: 370
currently lose_sum: 506.67899510264397
time_elpased: 1.616
batch start
#iterations: 371
currently lose_sum: 507.36464843153954
time_elpased: 1.637
batch start
#iterations: 372
currently lose_sum: 507.8000872731209
time_elpased: 1.626
batch start
#iterations: 373
currently lose_sum: 507.7151289284229
time_elpased: 1.628
batch start
#iterations: 374
currently lose_sum: 509.21552753448486
time_elpased: 1.63
batch start
#iterations: 375
currently lose_sum: 508.97886142134666
time_elpased: 1.624
batch start
#iterations: 376
currently lose_sum: 508.5463619530201
time_elpased: 1.618
batch start
#iterations: 377
currently lose_sum: 507.4315609037876
time_elpased: 1.62
batch start
#iterations: 378
currently lose_sum: 509.05992636084557
time_elpased: 1.639
batch start
#iterations: 379
currently lose_sum: 509.6102822422981
time_elpased: 1.632
start validation test
0.150927835052
1.0
0.150927835052
0.262271587245
0
validation finish
batch start
#iterations: 380
currently lose_sum: 508.47853350639343
time_elpased: 1.617
batch start
#iterations: 381
currently lose_sum: 507.90606340765953
time_elpased: 1.624
batch start
#iterations: 382
currently lose_sum: 508.49216985702515
time_elpased: 1.627
batch start
#iterations: 383
currently lose_sum: 506.1058245897293
time_elpased: 1.63
batch start
#iterations: 384
currently lose_sum: 507.40661320090294
time_elpased: 1.616
batch start
#iterations: 385
currently lose_sum: 506.8581119477749
time_elpased: 1.621
batch start
#iterations: 386
currently lose_sum: 508.471337467432
time_elpased: 1.629
batch start
#iterations: 387
currently lose_sum: 507.3440400958061
time_elpased: 1.616
batch start
#iterations: 388
currently lose_sum: 507.5483210384846
time_elpased: 1.632
batch start
#iterations: 389
currently lose_sum: 507.6078127026558
time_elpased: 1.62
batch start
#iterations: 390
currently lose_sum: 507.4847029745579
time_elpased: 1.619
batch start
#iterations: 391
currently lose_sum: 508.5913852751255
time_elpased: 1.603
batch start
#iterations: 392
currently lose_sum: 507.42921993136406
time_elpased: 1.616
batch start
#iterations: 393
currently lose_sum: 507.35307815670967
time_elpased: 1.622
batch start
#iterations: 394
currently lose_sum: 506.41832143068314
time_elpased: 1.627
batch start
#iterations: 395
currently lose_sum: 507.77775752544403
time_elpased: 1.622
batch start
#iterations: 396
currently lose_sum: 508.19608557224274
time_elpased: 1.622
batch start
#iterations: 397
currently lose_sum: 508.96812534332275
time_elpased: 1.605
batch start
#iterations: 398
currently lose_sum: 506.5524537563324
time_elpased: 1.603
batch start
#iterations: 399
currently lose_sum: 507.045832157135
time_elpased: 1.603
start validation test
0.132577319588
1.0
0.132577319588
0.234116147825
0
validation finish
acc: 0.230
pre: 1.000
rec: 0.230
F1: 0.374
auc: 0.000
