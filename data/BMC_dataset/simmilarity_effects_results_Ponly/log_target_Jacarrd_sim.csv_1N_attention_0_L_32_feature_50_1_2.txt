start to construct graph
graph construct over
58300
epochs start
batch start
#iterations: 0
currently lose_sum: 399.233835875988
time_elpased: 0.972
batch start
#iterations: 1
currently lose_sum: 392.07439798116684
time_elpased: 0.897
batch start
#iterations: 2
currently lose_sum: 387.79199373722076
time_elpased: 0.9
batch start
#iterations: 3
currently lose_sum: 385.23563170433044
time_elpased: 0.906
batch start
#iterations: 4
currently lose_sum: 384.62922847270966
time_elpased: 0.901
batch start
#iterations: 5
currently lose_sum: 383.3203836083412
time_elpased: 0.916
batch start
#iterations: 6
currently lose_sum: 382.7090521454811
time_elpased: 0.911
batch start
#iterations: 7
currently lose_sum: 381.7056163549423
time_elpased: 0.911
batch start
#iterations: 8
currently lose_sum: 381.2245651483536
time_elpased: 0.905
batch start
#iterations: 9
currently lose_sum: 382.3841230273247
time_elpased: 0.913
batch start
#iterations: 10
currently lose_sum: 380.6604175567627
time_elpased: 0.909
batch start
#iterations: 11
currently lose_sum: 380.7267721891403
time_elpased: 0.91
batch start
#iterations: 12
currently lose_sum: 380.64096385240555
time_elpased: 0.911
batch start
#iterations: 13
currently lose_sum: 381.64979606866837
time_elpased: 0.906
batch start
#iterations: 14
currently lose_sum: 378.29611468315125
time_elpased: 0.895
batch start
#iterations: 15
currently lose_sum: 379.47149634361267
time_elpased: 0.909
batch start
#iterations: 16
currently lose_sum: 379.28395050764084
time_elpased: 0.921
batch start
#iterations: 17
currently lose_sum: 379.83898532390594
time_elpased: 0.915
batch start
#iterations: 18
currently lose_sum: 378.05931413173676
time_elpased: 0.909
batch start
#iterations: 19
currently lose_sum: 380.3239691257477
time_elpased: 0.908
start validation test
0.570927835052
1.0
0.570927835052
0.726867042919
0
validation finish
batch start
#iterations: 20
currently lose_sum: 379.29441809654236
time_elpased: 0.904
batch start
#iterations: 21
currently lose_sum: 379.1736230254173
time_elpased: 0.902
batch start
#iterations: 22
currently lose_sum: 379.1146522164345
time_elpased: 0.912
batch start
#iterations: 23
currently lose_sum: 380.2892825603485
time_elpased: 0.903
batch start
#iterations: 24
currently lose_sum: 379.14192032814026
time_elpased: 0.905
batch start
#iterations: 25
currently lose_sum: 379.53181010484695
time_elpased: 0.91
batch start
#iterations: 26
currently lose_sum: 378.71568870544434
time_elpased: 0.911
batch start
#iterations: 27
currently lose_sum: 378.5634630918503
time_elpased: 0.905
batch start
#iterations: 28
currently lose_sum: 377.787506878376
time_elpased: 0.911
batch start
#iterations: 29
currently lose_sum: 379.59917306900024
time_elpased: 0.909
batch start
#iterations: 30
currently lose_sum: 378.7936260700226
time_elpased: 0.903
batch start
#iterations: 31
currently lose_sum: 378.38645523786545
time_elpased: 0.898
batch start
#iterations: 32
currently lose_sum: 379.02782106399536
time_elpased: 0.923
batch start
#iterations: 33
currently lose_sum: 379.33419996500015
time_elpased: 0.896
batch start
#iterations: 34
currently lose_sum: 378.3551174402237
time_elpased: 0.914
batch start
#iterations: 35
currently lose_sum: 379.58937269449234
time_elpased: 0.905
batch start
#iterations: 36
currently lose_sum: 378.7675294280052
time_elpased: 0.912
batch start
#iterations: 37
currently lose_sum: 377.46636831760406
time_elpased: 0.904
batch start
#iterations: 38
currently lose_sum: 378.8218743801117
time_elpased: 0.905
batch start
#iterations: 39
currently lose_sum: 378.69097793102264
time_elpased: 0.913
start validation test
0.567113402062
1.0
0.567113402062
0.723768173147
0
validation finish
batch start
#iterations: 40
currently lose_sum: 377.4729228615761
time_elpased: 0.908
batch start
#iterations: 41
currently lose_sum: 378.0428690314293
time_elpased: 0.913
batch start
#iterations: 42
currently lose_sum: 376.50154823064804
time_elpased: 0.914
batch start
#iterations: 43
currently lose_sum: 377.9256166815758
time_elpased: 0.901
batch start
#iterations: 44
currently lose_sum: 379.57015174627304
time_elpased: 0.9
batch start
#iterations: 45
currently lose_sum: 376.976442694664
time_elpased: 0.913
batch start
#iterations: 46
currently lose_sum: 377.33874517679214
time_elpased: 0.911
batch start
#iterations: 47
currently lose_sum: 378.24759578704834
time_elpased: 0.915
batch start
#iterations: 48
currently lose_sum: 376.6789316534996
time_elpased: 0.901
batch start
#iterations: 49
currently lose_sum: 378.1309039592743
time_elpased: 0.91
batch start
#iterations: 50
currently lose_sum: 376.1592978835106
time_elpased: 0.907
batch start
#iterations: 51
currently lose_sum: 377.7468763589859
time_elpased: 0.907
batch start
#iterations: 52
currently lose_sum: 378.4637240767479
time_elpased: 0.903
batch start
#iterations: 53
currently lose_sum: 377.4337582588196
time_elpased: 0.903
batch start
#iterations: 54
currently lose_sum: 378.1273486018181
time_elpased: 0.912
batch start
#iterations: 55
currently lose_sum: 376.58732837438583
time_elpased: 0.91
batch start
#iterations: 56
currently lose_sum: 377.8042283654213
time_elpased: 0.914
batch start
#iterations: 57
currently lose_sum: 376.83025830984116
time_elpased: 0.902
batch start
#iterations: 58
currently lose_sum: 376.94900941848755
time_elpased: 0.921
batch start
#iterations: 59
currently lose_sum: 377.38935244083405
time_elpased: 0.915
start validation test
0.651030927835
1.0
0.651030927835
0.788635654074
0
validation finish
batch start
#iterations: 60
currently lose_sum: 377.23596692085266
time_elpased: 0.909
batch start
#iterations: 61
currently lose_sum: 377.21159195899963
time_elpased: 0.911
batch start
#iterations: 62
currently lose_sum: 377.22318375110626
time_elpased: 0.912
batch start
#iterations: 63
currently lose_sum: 376.7084998488426
time_elpased: 0.905
batch start
#iterations: 64
currently lose_sum: 376.9038084745407
time_elpased: 0.908
batch start
#iterations: 65
currently lose_sum: 377.72373563051224
time_elpased: 0.907
batch start
#iterations: 66
currently lose_sum: 377.0893548130989
time_elpased: 0.918
batch start
#iterations: 67
currently lose_sum: 377.0281884074211
time_elpased: 0.909
batch start
#iterations: 68
currently lose_sum: 377.6408450603485
time_elpased: 0.911
batch start
#iterations: 69
currently lose_sum: 377.07896614074707
time_elpased: 0.907
batch start
#iterations: 70
currently lose_sum: 377.01533567905426
time_elpased: 0.918
batch start
#iterations: 71
currently lose_sum: 376.9467907547951
time_elpased: 0.906
batch start
#iterations: 72
currently lose_sum: 376.92915803194046
time_elpased: 0.906
batch start
#iterations: 73
currently lose_sum: 377.156777381897
time_elpased: 0.91
batch start
#iterations: 74
currently lose_sum: 377.29887253046036
time_elpased: 0.912
batch start
#iterations: 75
currently lose_sum: 376.0185075998306
time_elpased: 0.91
batch start
#iterations: 76
currently lose_sum: 376.11539459228516
time_elpased: 0.906
batch start
#iterations: 77
currently lose_sum: 376.4365185499191
time_elpased: 0.919
batch start
#iterations: 78
currently lose_sum: 377.21953213214874
time_elpased: 0.905
batch start
#iterations: 79
currently lose_sum: 376.5426566004753
time_elpased: 0.908
start validation test
0.618453608247
1.0
0.618453608247
0.764252500159
0
validation finish
batch start
#iterations: 80
currently lose_sum: 375.8949962258339
time_elpased: 0.922
batch start
#iterations: 81
currently lose_sum: 377.3776852488518
time_elpased: 0.91
batch start
#iterations: 82
currently lose_sum: 376.8894339799881
time_elpased: 0.901
batch start
#iterations: 83
currently lose_sum: 375.41093122959137
time_elpased: 0.915
batch start
#iterations: 84
currently lose_sum: 377.7092626094818
time_elpased: 0.916
batch start
#iterations: 85
currently lose_sum: 376.69589799642563
time_elpased: 0.913
batch start
#iterations: 86
currently lose_sum: 377.03483802080154
time_elpased: 0.906
batch start
#iterations: 87
currently lose_sum: 376.4210181236267
time_elpased: 0.903
batch start
#iterations: 88
currently lose_sum: 376.02796775102615
time_elpased: 0.912
batch start
#iterations: 89
currently lose_sum: 377.2479373216629
time_elpased: 0.927
batch start
#iterations: 90
currently lose_sum: 377.85193943977356
time_elpased: 0.903
batch start
#iterations: 91
currently lose_sum: 376.5852249264717
time_elpased: 0.911
batch start
#iterations: 92
currently lose_sum: 376.09754037857056
time_elpased: 0.915
batch start
#iterations: 93
currently lose_sum: 376.9719997048378
time_elpased: 0.923
batch start
#iterations: 94
currently lose_sum: 376.4375849366188
time_elpased: 0.926
batch start
#iterations: 95
currently lose_sum: 376.1421517729759
time_elpased: 0.916
batch start
#iterations: 96
currently lose_sum: 376.70795917510986
time_elpased: 0.921
batch start
#iterations: 97
currently lose_sum: 375.7877564430237
time_elpased: 0.914
batch start
#iterations: 98
currently lose_sum: 375.52677154541016
time_elpased: 0.905
batch start
#iterations: 99
currently lose_sum: 376.34523969888687
time_elpased: 0.914
start validation test
0.621340206186
1.0
0.621340206186
0.766452597444
0
validation finish
batch start
#iterations: 100
currently lose_sum: 376.3534995317459
time_elpased: 0.909
batch start
#iterations: 101
currently lose_sum: 375.81445252895355
time_elpased: 0.92
batch start
#iterations: 102
currently lose_sum: 375.800989151001
time_elpased: 0.9
batch start
#iterations: 103
currently lose_sum: 376.186383664608
time_elpased: 0.912
batch start
#iterations: 104
currently lose_sum: 376.53172343969345
time_elpased: 0.909
batch start
#iterations: 105
currently lose_sum: 376.0738172531128
time_elpased: 0.924
batch start
#iterations: 106
currently lose_sum: 377.0966354608536
time_elpased: 0.903
batch start
#iterations: 107
currently lose_sum: 376.38211447000504
time_elpased: 0.909
batch start
#iterations: 108
currently lose_sum: 376.99259650707245
time_elpased: 0.923
batch start
#iterations: 109
currently lose_sum: 375.97969311475754
time_elpased: 0.91
batch start
#iterations: 110
currently lose_sum: 376.6272916197777
time_elpased: 0.914
batch start
#iterations: 111
currently lose_sum: 375.6048831343651
time_elpased: 0.917
batch start
#iterations: 112
currently lose_sum: 376.08751744031906
time_elpased: 0.918
batch start
#iterations: 113
currently lose_sum: 376.6679282784462
time_elpased: 0.911
batch start
#iterations: 114
currently lose_sum: 375.9712587594986
time_elpased: 0.908
batch start
#iterations: 115
currently lose_sum: 376.0525505542755
time_elpased: 0.914
batch start
#iterations: 116
currently lose_sum: 376.47290217876434
time_elpased: 0.915
batch start
#iterations: 117
currently lose_sum: 375.3532085418701
time_elpased: 0.913
batch start
#iterations: 118
currently lose_sum: 374.80295330286026
time_elpased: 0.91
batch start
#iterations: 119
currently lose_sum: 376.56015771627426
time_elpased: 0.924
start validation test
0.642783505155
1.0
0.642783505155
0.782554126137
0
validation finish
batch start
#iterations: 120
currently lose_sum: 377.0417814254761
time_elpased: 0.914
batch start
#iterations: 121
currently lose_sum: 375.0953420996666
time_elpased: 0.907
batch start
#iterations: 122
currently lose_sum: 374.7695152759552
time_elpased: 0.909
batch start
#iterations: 123
currently lose_sum: 375.7451515197754
time_elpased: 0.913
batch start
#iterations: 124
currently lose_sum: 374.9260549545288
time_elpased: 0.911
batch start
#iterations: 125
currently lose_sum: 376.0353282094002
time_elpased: 0.919
batch start
#iterations: 126
currently lose_sum: 377.2973835468292
time_elpased: 0.924
batch start
#iterations: 127
currently lose_sum: 375.8533853292465
time_elpased: 0.925
batch start
#iterations: 128
currently lose_sum: 375.1344419121742
time_elpased: 0.916
batch start
#iterations: 129
currently lose_sum: 375.95192497968674
time_elpased: 0.915
batch start
#iterations: 130
currently lose_sum: 375.3436266183853
time_elpased: 0.911
batch start
#iterations: 131
currently lose_sum: 376.5049151778221
time_elpased: 0.908
batch start
#iterations: 132
currently lose_sum: 375.91031938791275
time_elpased: 0.921
batch start
#iterations: 133
currently lose_sum: 375.24174696207047
time_elpased: 0.93
batch start
#iterations: 134
currently lose_sum: 376.6110488176346
time_elpased: 0.925
batch start
#iterations: 135
currently lose_sum: 375.774322450161
time_elpased: 0.906
batch start
#iterations: 136
currently lose_sum: 376.8631782531738
time_elpased: 0.907
batch start
#iterations: 137
currently lose_sum: 375.3678900003433
time_elpased: 0.915
batch start
#iterations: 138
currently lose_sum: 376.80705511569977
time_elpased: 0.92
batch start
#iterations: 139
currently lose_sum: 375.4296707510948
time_elpased: 0.915
start validation test
0.63
1.0
0.63
0.773006134969
0
validation finish
batch start
#iterations: 140
currently lose_sum: 375.7540373802185
time_elpased: 0.926
batch start
#iterations: 141
currently lose_sum: 376.31211000680923
time_elpased: 0.914
batch start
#iterations: 142
currently lose_sum: 376.3926472067833
time_elpased: 0.908
batch start
#iterations: 143
currently lose_sum: 376.8272500038147
time_elpased: 0.92
batch start
#iterations: 144
currently lose_sum: 375.66647577285767
time_elpased: 0.911
batch start
#iterations: 145
currently lose_sum: 375.67863005399704
time_elpased: 0.915
batch start
#iterations: 146
currently lose_sum: 375.2868599295616
time_elpased: 0.908
batch start
#iterations: 147
currently lose_sum: 375.55004328489304
time_elpased: 0.909
batch start
#iterations: 148
currently lose_sum: 375.9229834675789
time_elpased: 0.912
batch start
#iterations: 149
currently lose_sum: 376.3095172047615
time_elpased: 0.921
batch start
#iterations: 150
currently lose_sum: 375.08963245153427
time_elpased: 0.918
batch start
#iterations: 151
currently lose_sum: 375.95718306303024
time_elpased: 0.927
batch start
#iterations: 152
currently lose_sum: 375.0426335930824
time_elpased: 0.91
batch start
#iterations: 153
currently lose_sum: 377.3889957666397
time_elpased: 0.916
batch start
#iterations: 154
currently lose_sum: 375.5043767094612
time_elpased: 0.913
batch start
#iterations: 155
currently lose_sum: 376.2678831219673
time_elpased: 0.914
batch start
#iterations: 156
currently lose_sum: 376.49486154317856
time_elpased: 0.913
batch start
#iterations: 157
currently lose_sum: 375.4161346554756
time_elpased: 0.91
batch start
#iterations: 158
currently lose_sum: 376.18312138319016
time_elpased: 0.914
batch start
#iterations: 159
currently lose_sum: 377.0165410041809
time_elpased: 0.917
start validation test
0.670515463918
1.0
0.670515463918
0.802764749445
0
validation finish
batch start
#iterations: 160
currently lose_sum: 375.8677762746811
time_elpased: 0.909
batch start
#iterations: 161
currently lose_sum: 375.7124408483505
time_elpased: 0.913
batch start
#iterations: 162
currently lose_sum: 375.9947773218155
time_elpased: 0.926
batch start
#iterations: 163
currently lose_sum: 375.18854331970215
time_elpased: 0.91
batch start
#iterations: 164
currently lose_sum: 375.44474548101425
time_elpased: 0.908
batch start
#iterations: 165
currently lose_sum: 374.96856743097305
time_elpased: 0.915
batch start
#iterations: 166
currently lose_sum: 375.082925260067
time_elpased: 0.919
batch start
#iterations: 167
currently lose_sum: 375.73704320192337
time_elpased: 0.911
batch start
#iterations: 168
currently lose_sum: 375.0896274447441
time_elpased: 0.93
batch start
#iterations: 169
currently lose_sum: 374.4571588039398
time_elpased: 0.914
batch start
#iterations: 170
currently lose_sum: 375.4776286482811
time_elpased: 0.917
batch start
#iterations: 171
currently lose_sum: 376.31747967004776
time_elpased: 0.92
batch start
#iterations: 172
currently lose_sum: 375.9336606860161
time_elpased: 0.915
batch start
#iterations: 173
currently lose_sum: 376.00411969423294
time_elpased: 0.917
batch start
#iterations: 174
currently lose_sum: 374.79044473171234
time_elpased: 0.909
batch start
#iterations: 175
currently lose_sum: 376.6235171556473
time_elpased: 0.909
batch start
#iterations: 176
currently lose_sum: 375.11741358041763
time_elpased: 0.916
batch start
#iterations: 177
currently lose_sum: 374.75496888160706
time_elpased: 0.918
batch start
#iterations: 178
currently lose_sum: 376.8133360147476
time_elpased: 0.922
batch start
#iterations: 179
currently lose_sum: 375.78483134508133
time_elpased: 0.921
start validation test
0.641443298969
1.0
0.641443298969
0.781560105514
0
validation finish
batch start
#iterations: 180
currently lose_sum: 375.84770303964615
time_elpased: 0.924
batch start
#iterations: 181
currently lose_sum: 375.30976313352585
time_elpased: 0.917
batch start
#iterations: 182
currently lose_sum: 376.0576322078705
time_elpased: 0.922
batch start
#iterations: 183
currently lose_sum: 376.107399225235
time_elpased: 0.91
batch start
#iterations: 184
currently lose_sum: 375.6434192061424
time_elpased: 0.915
batch start
#iterations: 185
currently lose_sum: 375.47334855794907
time_elpased: 0.908
batch start
#iterations: 186
currently lose_sum: 375.0573180913925
time_elpased: 0.907
batch start
#iterations: 187
currently lose_sum: 374.36970168352127
time_elpased: 0.903
batch start
#iterations: 188
currently lose_sum: 375.9659378528595
time_elpased: 0.913
batch start
#iterations: 189
currently lose_sum: 376.2478572130203
time_elpased: 0.907
batch start
#iterations: 190
currently lose_sum: 374.0238475203514
time_elpased: 0.909
batch start
#iterations: 191
currently lose_sum: 376.04705864191055
time_elpased: 0.916
batch start
#iterations: 192
currently lose_sum: 376.43427473306656
time_elpased: 0.921
batch start
#iterations: 193
currently lose_sum: 376.4387477040291
time_elpased: 0.924
batch start
#iterations: 194
currently lose_sum: 375.0368495583534
time_elpased: 0.915
batch start
#iterations: 195
currently lose_sum: 375.5477648973465
time_elpased: 0.917
batch start
#iterations: 196
currently lose_sum: 377.2537229657173
time_elpased: 0.917
batch start
#iterations: 197
currently lose_sum: 374.8949053287506
time_elpased: 0.921
batch start
#iterations: 198
currently lose_sum: 376.31472301483154
time_elpased: 0.931
batch start
#iterations: 199
currently lose_sum: 374.7249069213867
time_elpased: 0.912
start validation test
0.619587628866
1.0
0.619587628866
0.765117759389
0
validation finish
batch start
#iterations: 200
currently lose_sum: 376.21576952934265
time_elpased: 0.904
batch start
#iterations: 201
currently lose_sum: 374.9591799378395
time_elpased: 0.919
batch start
#iterations: 202
currently lose_sum: 374.6087135672569
time_elpased: 0.91
batch start
#iterations: 203
currently lose_sum: 373.62270098924637
time_elpased: 0.919
batch start
#iterations: 204
currently lose_sum: 375.8378516435623
time_elpased: 0.913
batch start
#iterations: 205
currently lose_sum: 374.786357820034
time_elpased: 0.92
batch start
#iterations: 206
currently lose_sum: 374.7418202161789
time_elpased: 0.913
batch start
#iterations: 207
currently lose_sum: 376.20095175504684
time_elpased: 0.921
batch start
#iterations: 208
currently lose_sum: 375.6617525815964
time_elpased: 0.918
batch start
#iterations: 209
currently lose_sum: 375.4682143330574
time_elpased: 0.928
batch start
#iterations: 210
currently lose_sum: 374.7375280857086
time_elpased: 0.905
batch start
#iterations: 211
currently lose_sum: 374.6433514356613
time_elpased: 0.923
batch start
#iterations: 212
currently lose_sum: 376.22521221637726
time_elpased: 0.913
batch start
#iterations: 213
currently lose_sum: 375.93366914987564
time_elpased: 0.91
batch start
#iterations: 214
currently lose_sum: 376.4371125102043
time_elpased: 0.913
batch start
#iterations: 215
currently lose_sum: 375.82700234651566
time_elpased: 0.912
batch start
#iterations: 216
currently lose_sum: 375.7000986337662
time_elpased: 0.918
batch start
#iterations: 217
currently lose_sum: 375.0268197655678
time_elpased: 0.913
batch start
#iterations: 218
currently lose_sum: 375.19862800836563
time_elpased: 0.917
batch start
#iterations: 219
currently lose_sum: 376.8275707960129
time_elpased: 0.905
start validation test
0.629484536082
1.0
0.629484536082
0.772617993167
0
validation finish
batch start
#iterations: 220
currently lose_sum: 374.90841686725616
time_elpased: 0.924
batch start
#iterations: 221
currently lose_sum: 375.85868042707443
time_elpased: 0.91
batch start
#iterations: 222
currently lose_sum: 375.51227593421936
time_elpased: 0.909
batch start
#iterations: 223
currently lose_sum: 374.76395523548126
time_elpased: 0.921
batch start
#iterations: 224
currently lose_sum: 375.6874649524689
time_elpased: 0.908
batch start
#iterations: 225
currently lose_sum: 375.000505566597
time_elpased: 0.919
batch start
#iterations: 226
currently lose_sum: 376.20410364866257
time_elpased: 0.913
batch start
#iterations: 227
currently lose_sum: 375.31989538669586
time_elpased: 0.91
batch start
#iterations: 228
currently lose_sum: 375.5385675430298
time_elpased: 0.925
batch start
#iterations: 229
currently lose_sum: 373.79664570093155
time_elpased: 0.901
batch start
#iterations: 230
currently lose_sum: 375.92218470573425
time_elpased: 0.911
batch start
#iterations: 231
currently lose_sum: 375.66549080610275
time_elpased: 0.9
batch start
#iterations: 232
currently lose_sum: 375.489080786705
time_elpased: 0.909
batch start
#iterations: 233
currently lose_sum: 376.06714284420013
time_elpased: 0.909
batch start
#iterations: 234
currently lose_sum: 374.81874269247055
time_elpased: 0.911
batch start
#iterations: 235
currently lose_sum: 375.0372978448868
time_elpased: 0.914
batch start
#iterations: 236
currently lose_sum: 375.2614646553993
time_elpased: 0.914
batch start
#iterations: 237
currently lose_sum: 374.4334929585457
time_elpased: 0.911
batch start
#iterations: 238
currently lose_sum: 375.95471400022507
time_elpased: 0.909
batch start
#iterations: 239
currently lose_sum: 375.1750622987747
time_elpased: 0.91
start validation test
0.61793814433
1.0
0.61793814433
0.763858799541
0
validation finish
batch start
#iterations: 240
currently lose_sum: 374.290080845356
time_elpased: 0.933
batch start
#iterations: 241
currently lose_sum: 375.2719187736511
time_elpased: 0.918
batch start
#iterations: 242
currently lose_sum: 374.0087890625
time_elpased: 0.915
batch start
#iterations: 243
currently lose_sum: 376.0282784104347
time_elpased: 0.919
batch start
#iterations: 244
currently lose_sum: 376.0267829298973
time_elpased: 0.919
batch start
#iterations: 245
currently lose_sum: 375.0501691699028
time_elpased: 0.916
batch start
#iterations: 246
currently lose_sum: 375.2920213341713
time_elpased: 0.908
batch start
#iterations: 247
currently lose_sum: 376.17606794834137
time_elpased: 0.911
batch start
#iterations: 248
currently lose_sum: 376.56781101226807
time_elpased: 0.912
batch start
#iterations: 249
currently lose_sum: 375.47859197854996
time_elpased: 0.917
batch start
#iterations: 250
currently lose_sum: 375.91914987564087
time_elpased: 0.918
batch start
#iterations: 251
currently lose_sum: 376.6128258705139
time_elpased: 0.918
batch start
#iterations: 252
currently lose_sum: 376.6823088526726
time_elpased: 0.91
batch start
#iterations: 253
currently lose_sum: 374.83730524778366
time_elpased: 0.907
batch start
#iterations: 254
currently lose_sum: 375.986031293869
time_elpased: 0.913
batch start
#iterations: 255
currently lose_sum: 376.037859082222
time_elpased: 0.91
batch start
#iterations: 256
currently lose_sum: 375.5996428132057
time_elpased: 0.908
batch start
#iterations: 257
currently lose_sum: 376.37859350442886
time_elpased: 0.902
batch start
#iterations: 258
currently lose_sum: 376.5517518520355
time_elpased: 0.907
batch start
#iterations: 259
currently lose_sum: 375.1300057768822
time_elpased: 0.908
start validation test
0.644536082474
1.0
0.644536082474
0.783851554664
0
validation finish
batch start
#iterations: 260
currently lose_sum: 375.34170883893967
time_elpased: 0.918
batch start
#iterations: 261
currently lose_sum: 374.6573751568794
time_elpased: 0.911
batch start
#iterations: 262
currently lose_sum: 376.09886491298676
time_elpased: 0.913
batch start
#iterations: 263
currently lose_sum: 377.44804656505585
time_elpased: 0.918
batch start
#iterations: 264
currently lose_sum: 375.4637898206711
time_elpased: 0.908
batch start
#iterations: 265
currently lose_sum: 374.97093480825424
time_elpased: 0.91
batch start
#iterations: 266
currently lose_sum: 375.49542421102524
time_elpased: 0.914
batch start
#iterations: 267
currently lose_sum: 375.3478409051895
time_elpased: 0.914
batch start
#iterations: 268
currently lose_sum: 375.3202881217003
time_elpased: 0.913
batch start
#iterations: 269
currently lose_sum: 375.6906397342682
time_elpased: 0.919
batch start
#iterations: 270
currently lose_sum: 374.68011301755905
time_elpased: 0.924
batch start
#iterations: 271
currently lose_sum: 375.6742423772812
time_elpased: 0.907
batch start
#iterations: 272
currently lose_sum: 375.9477751851082
time_elpased: 0.911
batch start
#iterations: 273
currently lose_sum: 376.07725697755814
time_elpased: 0.912
batch start
#iterations: 274
currently lose_sum: 375.96171247959137
time_elpased: 0.922
batch start
#iterations: 275
currently lose_sum: 376.40454918146133
time_elpased: 0.929
batch start
#iterations: 276
currently lose_sum: 374.3539505600929
time_elpased: 0.91
batch start
#iterations: 277
currently lose_sum: 375.36952006816864
time_elpased: 0.911
batch start
#iterations: 278
currently lose_sum: 375.04423999786377
time_elpased: 0.913
batch start
#iterations: 279
currently lose_sum: 374.9995564222336
time_elpased: 0.92
start validation test
0.614845360825
1.0
0.614845360825
0.761491317671
0
validation finish
batch start
#iterations: 280
currently lose_sum: 374.8141771554947
time_elpased: 0.922
batch start
#iterations: 281
currently lose_sum: 376.38377422094345
time_elpased: 0.91
batch start
#iterations: 282
currently lose_sum: 376.23229283094406
time_elpased: 0.91
batch start
#iterations: 283
currently lose_sum: 375.06667602062225
time_elpased: 0.908
batch start
#iterations: 284
currently lose_sum: 375.7992432117462
time_elpased: 0.909
batch start
#iterations: 285
currently lose_sum: 374.8804526925087
time_elpased: 0.911
batch start
#iterations: 286
currently lose_sum: 375.57898139953613
time_elpased: 0.913
batch start
#iterations: 287
currently lose_sum: 374.5675600171089
time_elpased: 0.903
batch start
#iterations: 288
currently lose_sum: 376.0712704062462
time_elpased: 0.906
batch start
#iterations: 289
currently lose_sum: 375.2733556032181
time_elpased: 0.912
batch start
#iterations: 290
currently lose_sum: 375.0042941570282
time_elpased: 0.92
batch start
#iterations: 291
currently lose_sum: 374.8879341483116
time_elpased: 0.92
batch start
#iterations: 292
currently lose_sum: 375.1551511287689
time_elpased: 0.908
batch start
#iterations: 293
currently lose_sum: 376.1910054087639
time_elpased: 0.916
batch start
#iterations: 294
currently lose_sum: 375.3676763176918
time_elpased: 0.905
batch start
#iterations: 295
currently lose_sum: 375.04390436410904
time_elpased: 0.921
batch start
#iterations: 296
currently lose_sum: 375.00097489356995
time_elpased: 0.908
batch start
#iterations: 297
currently lose_sum: 375.1563032269478
time_elpased: 0.924
batch start
#iterations: 298
currently lose_sum: 375.36432403326035
time_elpased: 0.914
batch start
#iterations: 299
currently lose_sum: 374.05149590969086
time_elpased: 0.917
start validation test
0.65381443299
1.0
0.65381443299
0.790674479491
0
validation finish
batch start
#iterations: 300
currently lose_sum: 374.10552340745926
time_elpased: 0.906
batch start
#iterations: 301
currently lose_sum: 375.5396794080734
time_elpased: 0.919
batch start
#iterations: 302
currently lose_sum: 376.11787962913513
time_elpased: 0.911
batch start
#iterations: 303
currently lose_sum: 376.89763444662094
time_elpased: 0.921
batch start
#iterations: 304
currently lose_sum: 375.44924080371857
time_elpased: 0.923
batch start
#iterations: 305
currently lose_sum: 376.5568992495537
time_elpased: 0.907
batch start
#iterations: 306
currently lose_sum: 374.1376042366028
time_elpased: 0.909
batch start
#iterations: 307
currently lose_sum: 374.3242796063423
time_elpased: 0.915
batch start
#iterations: 308
currently lose_sum: 375.10241198539734
time_elpased: 0.916
batch start
#iterations: 309
currently lose_sum: 375.65836346149445
time_elpased: 0.918
batch start
#iterations: 310
currently lose_sum: 373.6911141872406
time_elpased: 0.911
batch start
#iterations: 311
currently lose_sum: 376.3298112154007
time_elpased: 0.918
batch start
#iterations: 312
currently lose_sum: 374.50264072418213
time_elpased: 0.925
batch start
#iterations: 313
currently lose_sum: 373.8720073103905
time_elpased: 0.92
batch start
#iterations: 314
currently lose_sum: 375.3093418478966
time_elpased: 0.921
batch start
#iterations: 315
currently lose_sum: 376.08910489082336
time_elpased: 0.917
batch start
#iterations: 316
currently lose_sum: 375.1279184818268
time_elpased: 0.917
batch start
#iterations: 317
currently lose_sum: 375.153556227684
time_elpased: 0.905
batch start
#iterations: 318
currently lose_sum: 376.18552350997925
time_elpased: 0.916
batch start
#iterations: 319
currently lose_sum: 374.9291260242462
time_elpased: 0.919
start validation test
0.660309278351
1.0
0.660309278351
0.795405153679
0
validation finish
batch start
#iterations: 320
currently lose_sum: 375.13594394922256
time_elpased: 0.928
batch start
#iterations: 321
currently lose_sum: 375.3926490545273
time_elpased: 0.91
batch start
#iterations: 322
currently lose_sum: 375.77351981401443
time_elpased: 0.913
batch start
#iterations: 323
currently lose_sum: 375.9926835298538
time_elpased: 0.921
batch start
#iterations: 324
currently lose_sum: 374.50624656677246
time_elpased: 0.921
batch start
#iterations: 325
currently lose_sum: 376.0452482700348
time_elpased: 0.92
batch start
#iterations: 326
currently lose_sum: 375.85160034894943
time_elpased: 0.924
batch start
#iterations: 327
currently lose_sum: 374.8845225572586
time_elpased: 0.917
batch start
#iterations: 328
currently lose_sum: 374.88536381721497
time_elpased: 0.909
batch start
#iterations: 329
currently lose_sum: 374.9044859409332
time_elpased: 0.922
batch start
#iterations: 330
currently lose_sum: 375.51154738664627
time_elpased: 0.898
batch start
#iterations: 331
currently lose_sum: 375.0707692503929
time_elpased: 0.914
batch start
#iterations: 332
currently lose_sum: 375.7616494297981
time_elpased: 0.918
batch start
#iterations: 333
currently lose_sum: 375.92883574962616
time_elpased: 0.91
batch start
#iterations: 334
currently lose_sum: 375.6038786768913
time_elpased: 0.921
batch start
#iterations: 335
currently lose_sum: 374.9209094643593
time_elpased: 0.916
batch start
#iterations: 336
currently lose_sum: 376.49172377586365
time_elpased: 0.909
batch start
#iterations: 337
currently lose_sum: 374.38584566116333
time_elpased: 0.914
batch start
#iterations: 338
currently lose_sum: 375.6502084136009
time_elpased: 0.907
batch start
#iterations: 339
currently lose_sum: 375.3460525870323
time_elpased: 0.908
start validation test
0.643608247423
1.0
0.643608247423
0.783165025403
0
validation finish
batch start
#iterations: 340
currently lose_sum: 375.7850642800331
time_elpased: 0.916
batch start
#iterations: 341
currently lose_sum: 375.79112392663956
time_elpased: 0.916
batch start
#iterations: 342
currently lose_sum: 375.4143451452255
time_elpased: 0.918
batch start
#iterations: 343
currently lose_sum: 374.75746113061905
time_elpased: 0.914
batch start
#iterations: 344
currently lose_sum: 374.92380398511887
time_elpased: 0.918
batch start
#iterations: 345
currently lose_sum: 375.7050225138664
time_elpased: 0.92
batch start
#iterations: 346
currently lose_sum: 375.62560135126114
time_elpased: 0.919
batch start
#iterations: 347
currently lose_sum: 376.17504209280014
time_elpased: 0.907
batch start
#iterations: 348
currently lose_sum: 373.6329234838486
time_elpased: 0.924
batch start
#iterations: 349
currently lose_sum: 375.265497982502
time_elpased: 0.917
batch start
#iterations: 350
currently lose_sum: 375.612784743309
time_elpased: 0.921
batch start
#iterations: 351
currently lose_sum: 375.6230330467224
time_elpased: 0.913
batch start
#iterations: 352
currently lose_sum: 376.1203950047493
time_elpased: 0.917
batch start
#iterations: 353
currently lose_sum: 375.6539406776428
time_elpased: 0.919
batch start
#iterations: 354
currently lose_sum: 374.97586339712143
time_elpased: 0.914
batch start
#iterations: 355
currently lose_sum: 374.6352964043617
time_elpased: 0.916
batch start
#iterations: 356
currently lose_sum: 374.78879910707474
time_elpased: 0.913
batch start
#iterations: 357
currently lose_sum: 375.41630882024765
time_elpased: 0.918
batch start
#iterations: 358
currently lose_sum: 374.6261076927185
time_elpased: 0.916
batch start
#iterations: 359
currently lose_sum: 374.01703387498856
time_elpased: 0.918
start validation test
0.619484536082
1.0
0.619484536082
0.765039149532
0
validation finish
batch start
#iterations: 360
currently lose_sum: 375.30940079689026
time_elpased: 0.909
batch start
#iterations: 361
currently lose_sum: 375.65227407217026
time_elpased: 0.915
batch start
#iterations: 362
currently lose_sum: 375.1273983716965
time_elpased: 0.911
batch start
#iterations: 363
currently lose_sum: 376.7670031785965
time_elpased: 0.915
batch start
#iterations: 364
currently lose_sum: 374.2992028594017
time_elpased: 0.923
batch start
#iterations: 365
currently lose_sum: 375.47633051872253
time_elpased: 0.912
batch start
#iterations: 366
currently lose_sum: 376.6095016002655
time_elpased: 0.915
batch start
#iterations: 367
currently lose_sum: 376.22120970487595
time_elpased: 0.919
batch start
#iterations: 368
currently lose_sum: 374.37454974651337
time_elpased: 0.913
batch start
#iterations: 369
currently lose_sum: 373.78251653909683
time_elpased: 0.924
batch start
#iterations: 370
currently lose_sum: 374.6823008656502
time_elpased: 0.914
batch start
#iterations: 371
currently lose_sum: 375.3845845460892
time_elpased: 0.907
batch start
#iterations: 372
currently lose_sum: 374.89202123880386
time_elpased: 0.911
batch start
#iterations: 373
currently lose_sum: 375.5092180967331
time_elpased: 0.916
batch start
#iterations: 374
currently lose_sum: 375.6654539704323
time_elpased: 0.91
batch start
#iterations: 375
currently lose_sum: 373.50767385959625
time_elpased: 0.919
batch start
#iterations: 376
currently lose_sum: 374.98673486709595
time_elpased: 0.937
batch start
#iterations: 377
currently lose_sum: 375.70975732803345
time_elpased: 0.907
batch start
#iterations: 378
currently lose_sum: 374.93296921253204
time_elpased: 0.914
batch start
#iterations: 379
currently lose_sum: 375.5686126947403
time_elpased: 0.908
start validation test
0.633711340206
1.0
0.633711340206
0.775793525588
0
validation finish
batch start
#iterations: 380
currently lose_sum: 376.913092315197
time_elpased: 0.923
batch start
#iterations: 381
currently lose_sum: 376.2087526321411
time_elpased: 0.916
batch start
#iterations: 382
currently lose_sum: 375.3893807530403
time_elpased: 0.927
batch start
#iterations: 383
currently lose_sum: 375.03311771154404
time_elpased: 0.918
batch start
#iterations: 384
currently lose_sum: 374.98611092567444
time_elpased: 0.914
batch start
#iterations: 385
currently lose_sum: 374.2685700058937
time_elpased: 0.926
batch start
#iterations: 386
currently lose_sum: 375.7622116804123
time_elpased: 0.917
batch start
#iterations: 387
currently lose_sum: 374.7923393249512
time_elpased: 0.903
batch start
#iterations: 388
currently lose_sum: 374.5311704277992
time_elpased: 0.908
batch start
#iterations: 389
currently lose_sum: 375.218991458416
time_elpased: 0.914
batch start
#iterations: 390
currently lose_sum: 374.0176419019699
time_elpased: 0.913
batch start
#iterations: 391
currently lose_sum: 374.8661279082298
time_elpased: 0.918
batch start
#iterations: 392
currently lose_sum: 373.8525148034096
time_elpased: 0.914
batch start
#iterations: 393
currently lose_sum: 375.46666556596756
time_elpased: 0.927
batch start
#iterations: 394
currently lose_sum: 375.1414318084717
time_elpased: 0.916
batch start
#iterations: 395
currently lose_sum: 375.3893299102783
time_elpased: 0.913
batch start
#iterations: 396
currently lose_sum: 375.17760837078094
time_elpased: 0.912
batch start
#iterations: 397
currently lose_sum: 374.46751564741135
time_elpased: 0.926
batch start
#iterations: 398
currently lose_sum: 375.2555009126663
time_elpased: 0.926
batch start
#iterations: 399
currently lose_sum: 374.07897049188614
time_elpased: 0.914
start validation test
0.640721649485
1.0
0.640721649485
0.781024191015
0
validation finish
acc: 0.666
pre: 1.000
rec: 0.666
F1: 0.800
auc: 0.000
