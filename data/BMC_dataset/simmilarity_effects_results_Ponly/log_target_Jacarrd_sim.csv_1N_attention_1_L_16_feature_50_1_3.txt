start to construct graph
graph construct over
58302
epochs start
batch start
#iterations: 0
currently lose_sum: 398.6968916654587
time_elpased: 1.149
batch start
#iterations: 1
currently lose_sum: 388.84963822364807
time_elpased: 1.099
batch start
#iterations: 2
currently lose_sum: 383.6195315718651
time_elpased: 1.087
batch start
#iterations: 3
currently lose_sum: 379.7490009665489
time_elpased: 1.089
batch start
#iterations: 4
currently lose_sum: 377.9633930325508
time_elpased: 1.084
batch start
#iterations: 5
currently lose_sum: 377.56141847372055
time_elpased: 1.079
batch start
#iterations: 6
currently lose_sum: 375.4343151450157
time_elpased: 1.143
batch start
#iterations: 7
currently lose_sum: 375.1680985093117
time_elpased: 1.079
batch start
#iterations: 8
currently lose_sum: 373.3791375756264
time_elpased: 1.081
batch start
#iterations: 9
currently lose_sum: 373.5338907837868
time_elpased: 1.135
batch start
#iterations: 10
currently lose_sum: 373.00511091947556
time_elpased: 1.087
batch start
#iterations: 11
currently lose_sum: 371.5232337117195
time_elpased: 1.089
batch start
#iterations: 12
currently lose_sum: 370.9960294365883
time_elpased: 1.121
batch start
#iterations: 13
currently lose_sum: 370.6026201248169
time_elpased: 1.092
batch start
#iterations: 14
currently lose_sum: 370.1399365067482
time_elpased: 1.083
batch start
#iterations: 15
currently lose_sum: 369.9162954688072
time_elpased: 1.079
batch start
#iterations: 16
currently lose_sum: 371.4780868291855
time_elpased: 1.084
batch start
#iterations: 17
currently lose_sum: 369.6036985516548
time_elpased: 1.113
batch start
#iterations: 18
currently lose_sum: 368.9291321635246
time_elpased: 1.097
batch start
#iterations: 19
currently lose_sum: 367.88319289684296
time_elpased: 1.088
start validation test
0.715463917526
1.0
0.715463917526
0.834134615385
0
validation finish
batch start
#iterations: 20
currently lose_sum: 367.38658863306046
time_elpased: 1.149
batch start
#iterations: 21
currently lose_sum: 369.1183532476425
time_elpased: 1.09
batch start
#iterations: 22
currently lose_sum: 369.058802485466
time_elpased: 1.088
batch start
#iterations: 23
currently lose_sum: 367.26450550556183
time_elpased: 1.08
batch start
#iterations: 24
currently lose_sum: 368.49174439907074
time_elpased: 1.132
batch start
#iterations: 25
currently lose_sum: 369.1001794934273
time_elpased: 1.095
batch start
#iterations: 26
currently lose_sum: 367.3997282385826
time_elpased: 1.079
batch start
#iterations: 27
currently lose_sum: 369.05151546001434
time_elpased: 1.113
batch start
#iterations: 28
currently lose_sum: 367.5931656360626
time_elpased: 1.089
batch start
#iterations: 29
currently lose_sum: 368.57353818416595
time_elpased: 1.093
batch start
#iterations: 30
currently lose_sum: 368.1201742887497
time_elpased: 1.087
batch start
#iterations: 31
currently lose_sum: 367.4411904811859
time_elpased: 1.102
batch start
#iterations: 32
currently lose_sum: 366.1525048017502
time_elpased: 1.088
batch start
#iterations: 33
currently lose_sum: 367.49214774370193
time_elpased: 1.077
batch start
#iterations: 34
currently lose_sum: 367.4878378510475
time_elpased: 1.082
batch start
#iterations: 35
currently lose_sum: 366.6429636478424
time_elpased: 1.098
batch start
#iterations: 36
currently lose_sum: 367.3366134762764
time_elpased: 1.132
batch start
#iterations: 37
currently lose_sum: 365.81211507320404
time_elpased: 1.092
batch start
#iterations: 38
currently lose_sum: 366.53603744506836
time_elpased: 1.126
batch start
#iterations: 39
currently lose_sum: 366.23873549699783
time_elpased: 1.141
start validation test
0.699793814433
1.0
0.699793814433
0.823386705483
0
validation finish
batch start
#iterations: 40
currently lose_sum: 365.78896337747574
time_elpased: 1.09
batch start
#iterations: 41
currently lose_sum: 365.2205911874771
time_elpased: 1.094
batch start
#iterations: 42
currently lose_sum: 366.5409012436867
time_elpased: 1.097
batch start
#iterations: 43
currently lose_sum: 365.42050218582153
time_elpased: 1.089
batch start
#iterations: 44
currently lose_sum: 365.3834694623947
time_elpased: 1.084
batch start
#iterations: 45
currently lose_sum: 364.9993146061897
time_elpased: 1.15
batch start
#iterations: 46
currently lose_sum: 367.0800590515137
time_elpased: 1.153
batch start
#iterations: 47
currently lose_sum: 365.4531002640724
time_elpased: 1.097
batch start
#iterations: 48
currently lose_sum: 365.6786768436432
time_elpased: 1.098
batch start
#iterations: 49
currently lose_sum: 365.1679838299751
time_elpased: 1.133
batch start
#iterations: 50
currently lose_sum: 367.6716665029526
time_elpased: 1.137
batch start
#iterations: 51
currently lose_sum: 365.6956680417061
time_elpased: 1.093
batch start
#iterations: 52
currently lose_sum: 365.53974056243896
time_elpased: 1.088
batch start
#iterations: 53
currently lose_sum: 366.16303634643555
time_elpased: 1.143
batch start
#iterations: 54
currently lose_sum: 364.7117959856987
time_elpased: 1.093
batch start
#iterations: 55
currently lose_sum: 364.72564232349396
time_elpased: 1.104
batch start
#iterations: 56
currently lose_sum: 366.26246094703674
time_elpased: 1.094
batch start
#iterations: 57
currently lose_sum: 365.6197270154953
time_elpased: 1.088
batch start
#iterations: 58
currently lose_sum: 363.90188950300217
time_elpased: 1.086
batch start
#iterations: 59
currently lose_sum: 365.82892525196075
time_elpased: 1.138
start validation test
0.731237113402
1.0
0.731237113402
0.844756743881
0
validation finish
batch start
#iterations: 60
currently lose_sum: 364.3725246191025
time_elpased: 1.092
batch start
#iterations: 61
currently lose_sum: 365.0074788928032
time_elpased: 1.09
batch start
#iterations: 62
currently lose_sum: 363.9746888279915
time_elpased: 1.148
batch start
#iterations: 63
currently lose_sum: 365.3920946717262
time_elpased: 1.113
batch start
#iterations: 64
currently lose_sum: 365.7646762728691
time_elpased: 1.094
batch start
#iterations: 65
currently lose_sum: 364.53870677948
time_elpased: 1.116
batch start
#iterations: 66
currently lose_sum: 365.5417625904083
time_elpased: 1.099
batch start
#iterations: 67
currently lose_sum: 365.98847448825836
time_elpased: 1.088
batch start
#iterations: 68
currently lose_sum: 364.347287774086
time_elpased: 1.091
batch start
#iterations: 69
currently lose_sum: 365.00225907564163
time_elpased: 1.08
batch start
#iterations: 70
currently lose_sum: 364.2177113890648
time_elpased: 1.111
batch start
#iterations: 71
currently lose_sum: 364.2076832652092
time_elpased: 1.144
batch start
#iterations: 72
currently lose_sum: 363.59153866767883
time_elpased: 1.157
batch start
#iterations: 73
currently lose_sum: 365.292465031147
time_elpased: 1.095
batch start
#iterations: 74
currently lose_sum: 365.43502724170685
time_elpased: 1.113
batch start
#iterations: 75
currently lose_sum: 363.54925590753555
time_elpased: 1.079
batch start
#iterations: 76
currently lose_sum: 364.30371302366257
time_elpased: 1.102
batch start
#iterations: 77
currently lose_sum: 364.1314368247986
time_elpased: 1.095
batch start
#iterations: 78
currently lose_sum: 363.26571249961853
time_elpased: 1.099
batch start
#iterations: 79
currently lose_sum: 363.3001658320427
time_elpased: 1.089
start validation test
0.742680412371
1.0
0.742680412371
0.852342640795
0
validation finish
batch start
#iterations: 80
currently lose_sum: 362.9755675792694
time_elpased: 1.145
batch start
#iterations: 81
currently lose_sum: 364.96447545289993
time_elpased: 1.156
batch start
#iterations: 82
currently lose_sum: 363.980639398098
time_elpased: 1.091
batch start
#iterations: 83
currently lose_sum: 364.3941615819931
time_elpased: 1.085
batch start
#iterations: 84
currently lose_sum: 364.0218877196312
time_elpased: 1.088
batch start
#iterations: 85
currently lose_sum: 364.0046285390854
time_elpased: 1.121
batch start
#iterations: 86
currently lose_sum: 365.02972185611725
time_elpased: 1.104
batch start
#iterations: 87
currently lose_sum: 364.3720237016678
time_elpased: 1.115
batch start
#iterations: 88
currently lose_sum: 362.8949021100998
time_elpased: 1.14
batch start
#iterations: 89
currently lose_sum: 363.39664006233215
time_elpased: 1.135
batch start
#iterations: 90
currently lose_sum: 362.97917062044144
time_elpased: 1.096
batch start
#iterations: 91
currently lose_sum: 363.52939838171005
time_elpased: 1.086
batch start
#iterations: 92
currently lose_sum: 363.40000653266907
time_elpased: 1.152
batch start
#iterations: 93
currently lose_sum: 366.49427872896194
time_elpased: 1.091
batch start
#iterations: 94
currently lose_sum: 364.92021787166595
time_elpased: 1.139
batch start
#iterations: 95
currently lose_sum: 361.4325624704361
time_elpased: 1.127
batch start
#iterations: 96
currently lose_sum: 363.333992600441
time_elpased: 1.095
batch start
#iterations: 97
currently lose_sum: 363.6079369187355
time_elpased: 1.103
batch start
#iterations: 98
currently lose_sum: 362.7111277580261
time_elpased: 1.114
batch start
#iterations: 99
currently lose_sum: 365.1854137182236
time_elpased: 1.107
start validation test
0.731855670103
1.0
0.731855670103
0.845169355319
0
validation finish
batch start
#iterations: 100
currently lose_sum: 364.49303448200226
time_elpased: 1.097
batch start
#iterations: 101
currently lose_sum: 365.82908618450165
time_elpased: 1.096
batch start
#iterations: 102
currently lose_sum: 362.0584263205528
time_elpased: 1.099
batch start
#iterations: 103
currently lose_sum: 364.2710772752762
time_elpased: 1.09
batch start
#iterations: 104
currently lose_sum: 364.4986727833748
time_elpased: 1.105
batch start
#iterations: 105
currently lose_sum: 364.06938540935516
time_elpased: 1.085
batch start
#iterations: 106
currently lose_sum: 363.8731082677841
time_elpased: 1.107
batch start
#iterations: 107
currently lose_sum: 362.4677836894989
time_elpased: 1.135
batch start
#iterations: 108
currently lose_sum: 363.42286133766174
time_elpased: 1.093
batch start
#iterations: 109
currently lose_sum: 363.12941604852676
time_elpased: 1.122
batch start
#iterations: 110
currently lose_sum: 362.1683555841446
time_elpased: 1.116
batch start
#iterations: 111
currently lose_sum: 362.9453759789467
time_elpased: 1.096
batch start
#iterations: 112
currently lose_sum: 362.83604568243027
time_elpased: 1.096
batch start
#iterations: 113
currently lose_sum: 363.59703612327576
time_elpased: 1.1
batch start
#iterations: 114
currently lose_sum: 364.27328139543533
time_elpased: 1.098
batch start
#iterations: 115
currently lose_sum: 362.360957801342
time_elpased: 1.128
batch start
#iterations: 116
currently lose_sum: 363.27072954177856
time_elpased: 1.093
batch start
#iterations: 117
currently lose_sum: 363.8266814351082
time_elpased: 1.154
batch start
#iterations: 118
currently lose_sum: 361.8517745733261
time_elpased: 1.126
batch start
#iterations: 119
currently lose_sum: 363.0493947863579
time_elpased: 1.145
start validation test
0.726701030928
1.0
0.726701030928
0.841721893844
0
validation finish
batch start
#iterations: 120
currently lose_sum: 362.7796722650528
time_elpased: 1.092
batch start
#iterations: 121
currently lose_sum: 362.755950987339
time_elpased: 1.117
batch start
#iterations: 122
currently lose_sum: 364.1415610909462
time_elpased: 1.108
batch start
#iterations: 123
currently lose_sum: 361.5818501710892
time_elpased: 1.112
batch start
#iterations: 124
currently lose_sum: 361.3168380856514
time_elpased: 1.094
batch start
#iterations: 125
currently lose_sum: 362.8722312450409
time_elpased: 1.097
batch start
#iterations: 126
currently lose_sum: 363.05203157663345
time_elpased: 1.093
batch start
#iterations: 127
currently lose_sum: 363.58967566490173
time_elpased: 1.093
batch start
#iterations: 128
currently lose_sum: 364.28228229284286
time_elpased: 1.135
batch start
#iterations: 129
currently lose_sum: 361.64435559511185
time_elpased: 1.117
batch start
#iterations: 130
currently lose_sum: 362.393074631691
time_elpased: 1.122
batch start
#iterations: 131
currently lose_sum: 363.4635748267174
time_elpased: 1.11
batch start
#iterations: 132
currently lose_sum: 362.635198533535
time_elpased: 1.142
batch start
#iterations: 133
currently lose_sum: 364.6789285540581
time_elpased: 1.151
batch start
#iterations: 134
currently lose_sum: 361.8863151669502
time_elpased: 1.108
batch start
#iterations: 135
currently lose_sum: 363.962997674942
time_elpased: 1.145
batch start
#iterations: 136
currently lose_sum: 362.79512643814087
time_elpased: 1.087
batch start
#iterations: 137
currently lose_sum: 362.31517946720123
time_elpased: 1.123
batch start
#iterations: 138
currently lose_sum: 363.6753777861595
time_elpased: 1.156
batch start
#iterations: 139
currently lose_sum: 361.76931470632553
time_elpased: 1.091
start validation test
0.741958762887
1.0
0.741958762887
0.85186719536
0
validation finish
batch start
#iterations: 140
currently lose_sum: 361.06093180179596
time_elpased: 1.093
batch start
#iterations: 141
currently lose_sum: 362.7097115814686
time_elpased: 1.146
batch start
#iterations: 142
currently lose_sum: 360.58772507309914
time_elpased: 1.097
batch start
#iterations: 143
currently lose_sum: 361.01223677396774
time_elpased: 1.084
batch start
#iterations: 144
currently lose_sum: 361.42060375213623
time_elpased: 1.1
batch start
#iterations: 145
currently lose_sum: 363.32128697633743
time_elpased: 1.108
batch start
#iterations: 146
currently lose_sum: 360.87773072719574
time_elpased: 1.113
batch start
#iterations: 147
currently lose_sum: 363.52613842487335
time_elpased: 1.093
batch start
#iterations: 148
currently lose_sum: 362.6284636259079
time_elpased: 1.093
batch start
#iterations: 149
currently lose_sum: 361.46369379758835
time_elpased: 1.113
batch start
#iterations: 150
currently lose_sum: 362.686547100544
time_elpased: 1.084
batch start
#iterations: 151
currently lose_sum: 362.3487478494644
time_elpased: 1.082
batch start
#iterations: 152
currently lose_sum: 361.42163184285164
time_elpased: 1.099
batch start
#iterations: 153
currently lose_sum: 363.26411378383636
time_elpased: 1.128
batch start
#iterations: 154
currently lose_sum: 362.2725664973259
time_elpased: 1.096
batch start
#iterations: 155
currently lose_sum: 363.10194623470306
time_elpased: 1.146
batch start
#iterations: 156
currently lose_sum: 363.48085528612137
time_elpased: 1.143
batch start
#iterations: 157
currently lose_sum: 363.82857221364975
time_elpased: 1.102
batch start
#iterations: 158
currently lose_sum: 362.39449828863144
time_elpased: 1.1
batch start
#iterations: 159
currently lose_sum: 361.38137769699097
time_elpased: 1.103
start validation test
0.742577319588
1.0
0.742577319588
0.852274744128
0
validation finish
batch start
#iterations: 160
currently lose_sum: 362.5718415379524
time_elpased: 1.117
batch start
#iterations: 161
currently lose_sum: 363.4270685315132
time_elpased: 1.15
batch start
#iterations: 162
currently lose_sum: 362.8639632463455
time_elpased: 1.146
batch start
#iterations: 163
currently lose_sum: 361.9865653514862
time_elpased: 1.086
batch start
#iterations: 164
currently lose_sum: 363.12100291252136
time_elpased: 1.095
batch start
#iterations: 165
currently lose_sum: 361.6058047413826
time_elpased: 1.098
batch start
#iterations: 166
currently lose_sum: 362.1118354797363
time_elpased: 1.094
batch start
#iterations: 167
currently lose_sum: 362.8528405427933
time_elpased: 1.126
batch start
#iterations: 168
currently lose_sum: 362.6887189745903
time_elpased: 1.138
batch start
#iterations: 169
currently lose_sum: 361.8797499537468
time_elpased: 1.094
batch start
#iterations: 170
currently lose_sum: 362.5791697502136
time_elpased: 1.11
batch start
#iterations: 171
currently lose_sum: 360.9975235462189
time_elpased: 1.103
batch start
#iterations: 172
currently lose_sum: 362.4637276530266
time_elpased: 1.11
batch start
#iterations: 173
currently lose_sum: 362.8422373533249
time_elpased: 1.087
batch start
#iterations: 174
currently lose_sum: 362.1438217163086
time_elpased: 1.099
batch start
#iterations: 175
currently lose_sum: 362.4680125117302
time_elpased: 1.111
batch start
#iterations: 176
currently lose_sum: 362.556792140007
time_elpased: 1.098
batch start
#iterations: 177
currently lose_sum: 362.9695811867714
time_elpased: 1.112
batch start
#iterations: 178
currently lose_sum: 361.37516832351685
time_elpased: 1.097
batch start
#iterations: 179
currently lose_sum: 361.4732539653778
time_elpased: 1.095
start validation test
0.713092783505
1.0
0.713092783505
0.832520912319
0
validation finish
batch start
#iterations: 180
currently lose_sum: 361.7751244902611
time_elpased: 1.097
batch start
#iterations: 181
currently lose_sum: 362.0144289135933
time_elpased: 1.148
batch start
#iterations: 182
currently lose_sum: 363.94605380296707
time_elpased: 1.097
batch start
#iterations: 183
currently lose_sum: 362.60968655347824
time_elpased: 1.087
batch start
#iterations: 184
currently lose_sum: 362.972519159317
time_elpased: 1.09
batch start
#iterations: 185
currently lose_sum: 360.6063526272774
time_elpased: 1.142
batch start
#iterations: 186
currently lose_sum: 363.20137614011765
time_elpased: 1.147
batch start
#iterations: 187
currently lose_sum: 362.1051853299141
time_elpased: 1.088
batch start
#iterations: 188
currently lose_sum: 361.9606013894081
time_elpased: 1.102
batch start
#iterations: 189
currently lose_sum: 362.7862249612808
time_elpased: 1.156
batch start
#iterations: 190
currently lose_sum: 362.4740682244301
time_elpased: 1.107
batch start
#iterations: 191
currently lose_sum: 363.09174424409866
time_elpased: 1.114
batch start
#iterations: 192
currently lose_sum: 361.9664977192879
time_elpased: 1.1
batch start
#iterations: 193
currently lose_sum: 362.0754576921463
time_elpased: 1.127
batch start
#iterations: 194
currently lose_sum: 360.97696256637573
time_elpased: 1.096
batch start
#iterations: 195
currently lose_sum: 361.1885488629341
time_elpased: 1.101
batch start
#iterations: 196
currently lose_sum: 362.42861688137054
time_elpased: 1.15
batch start
#iterations: 197
currently lose_sum: 363.94737643003464
time_elpased: 1.151
batch start
#iterations: 198
currently lose_sum: 362.70720559358597
time_elpased: 1.154
batch start
#iterations: 199
currently lose_sum: 362.5898684859276
time_elpased: 1.103
start validation test
0.746804123711
1.0
0.746804123711
0.855051935788
0
validation finish
batch start
#iterations: 200
currently lose_sum: 361.162716627121
time_elpased: 1.086
batch start
#iterations: 201
currently lose_sum: 362.5615057349205
time_elpased: 1.138
batch start
#iterations: 202
currently lose_sum: 362.67744052410126
time_elpased: 1.098
batch start
#iterations: 203
currently lose_sum: 360.4659961462021
time_elpased: 1.09
batch start
#iterations: 204
currently lose_sum: 362.85411554574966
time_elpased: 1.143
batch start
#iterations: 205
currently lose_sum: 362.25182911753654
time_elpased: 1.095
batch start
#iterations: 206
currently lose_sum: 362.5001697540283
time_elpased: 1.09
batch start
#iterations: 207
currently lose_sum: 361.23673313856125
time_elpased: 1.1
batch start
#iterations: 208
currently lose_sum: 362.9653907418251
time_elpased: 1.149
batch start
#iterations: 209
currently lose_sum: 359.7670227885246
time_elpased: 1.093
batch start
#iterations: 210
currently lose_sum: 362.3642386198044
time_elpased: 1.104
batch start
#iterations: 211
currently lose_sum: 360.8593974709511
time_elpased: 1.094
batch start
#iterations: 212
currently lose_sum: 360.9004240632057
time_elpased: 1.134
batch start
#iterations: 213
currently lose_sum: 361.3352240920067
time_elpased: 1.101
batch start
#iterations: 214
currently lose_sum: 362.491394251585
time_elpased: 1.09
batch start
#iterations: 215
currently lose_sum: 361.4408243894577
time_elpased: 1.106
batch start
#iterations: 216
currently lose_sum: 362.0497004389763
time_elpased: 1.135
batch start
#iterations: 217
currently lose_sum: 361.5652240514755
time_elpased: 1.145
batch start
#iterations: 218
currently lose_sum: 360.7721594572067
time_elpased: 1.096
batch start
#iterations: 219
currently lose_sum: 362.98546797037125
time_elpased: 1.097
start validation test
0.747835051546
1.0
0.747835051546
0.855727262003
0
validation finish
batch start
#iterations: 220
currently lose_sum: 362.3112670779228
time_elpased: 1.115
batch start
#iterations: 221
currently lose_sum: 361.7093376517296
time_elpased: 1.102
batch start
#iterations: 222
currently lose_sum: 361.0491525530815
time_elpased: 1.144
batch start
#iterations: 223
currently lose_sum: 361.9569079875946
time_elpased: 1.083
batch start
#iterations: 224
currently lose_sum: 359.6381123661995
time_elpased: 1.092
batch start
#iterations: 225
currently lose_sum: 361.5824890732765
time_elpased: 1.104
batch start
#iterations: 226
currently lose_sum: 362.5904461145401
time_elpased: 1.091
batch start
#iterations: 227
currently lose_sum: 361.16718089580536
time_elpased: 1.108
batch start
#iterations: 228
currently lose_sum: 360.7714408636093
time_elpased: 1.116
batch start
#iterations: 229
currently lose_sum: 361.1817265152931
time_elpased: 1.1
batch start
#iterations: 230
currently lose_sum: 362.40161049366
time_elpased: 1.098
batch start
#iterations: 231
currently lose_sum: 362.3284760117531
time_elpased: 1.09
batch start
#iterations: 232
currently lose_sum: 362.64443176984787
time_elpased: 1.119
batch start
#iterations: 233
currently lose_sum: 359.77202504873276
time_elpased: 1.11
batch start
#iterations: 234
currently lose_sum: 360.7063327431679
time_elpased: 1.099
batch start
#iterations: 235
currently lose_sum: 361.7847326993942
time_elpased: 1.093
batch start
#iterations: 236
currently lose_sum: 361.2210853099823
time_elpased: 1.094
batch start
#iterations: 237
currently lose_sum: 362.0614290833473
time_elpased: 1.153
batch start
#iterations: 238
currently lose_sum: 361.6308906674385
time_elpased: 1.101
batch start
#iterations: 239
currently lose_sum: 360.7834593653679
time_elpased: 1.093
start validation test
0.738144329897
1.0
0.738144329897
0.849347568209
0
validation finish
batch start
#iterations: 240
currently lose_sum: 362.55769515037537
time_elpased: 1.155
batch start
#iterations: 241
currently lose_sum: 360.78619265556335
time_elpased: 1.089
batch start
#iterations: 242
currently lose_sum: 359.9648152589798
time_elpased: 1.102
batch start
#iterations: 243
currently lose_sum: 363.47769939899445
time_elpased: 1.089
batch start
#iterations: 244
currently lose_sum: 362.65713292360306
time_elpased: 1.091
batch start
#iterations: 245
currently lose_sum: 362.5218079686165
time_elpased: 1.1
batch start
#iterations: 246
currently lose_sum: 361.10122162103653
time_elpased: 1.148
batch start
#iterations: 247
currently lose_sum: 361.1849031448364
time_elpased: 1.14
batch start
#iterations: 248
currently lose_sum: 361.5983964204788
time_elpased: 1.091
batch start
#iterations: 249
currently lose_sum: 362.93095433712006
time_elpased: 1.091
batch start
#iterations: 250
currently lose_sum: 361.51403242349625
time_elpased: 1.106
batch start
#iterations: 251
currently lose_sum: 361.79369699954987
time_elpased: 1.098
batch start
#iterations: 252
currently lose_sum: 361.24514704942703
time_elpased: 1.092
batch start
#iterations: 253
currently lose_sum: 360.7547724246979
time_elpased: 1.109
batch start
#iterations: 254
currently lose_sum: 361.47460466623306
time_elpased: 1.093
batch start
#iterations: 255
currently lose_sum: 362.4867097735405
time_elpased: 1.085
batch start
#iterations: 256
currently lose_sum: 362.3276738524437
time_elpased: 1.118
batch start
#iterations: 257
currently lose_sum: 361.1574056148529
time_elpased: 1.157
batch start
#iterations: 258
currently lose_sum: 362.64865523576736
time_elpased: 1.095
batch start
#iterations: 259
currently lose_sum: 363.10831874608994
time_elpased: 1.099
start validation test
0.737113402062
1.0
0.737113402062
0.848664688427
0
validation finish
batch start
#iterations: 260
currently lose_sum: 360.98492616415024
time_elpased: 1.097
batch start
#iterations: 261
currently lose_sum: 362.3575758934021
time_elpased: 1.134
batch start
#iterations: 262
currently lose_sum: 361.3210620880127
time_elpased: 1.104
batch start
#iterations: 263
currently lose_sum: 361.53924411535263
time_elpased: 1.099
batch start
#iterations: 264
currently lose_sum: 361.3175570368767
time_elpased: 1.102
batch start
#iterations: 265
currently lose_sum: 362.13081562519073
time_elpased: 1.127
batch start
#iterations: 266
currently lose_sum: 360.26597505807877
time_elpased: 1.082
batch start
#iterations: 267
currently lose_sum: 361.08907574415207
time_elpased: 1.09
batch start
#iterations: 268
currently lose_sum: 359.60647064447403
time_elpased: 1.091
batch start
#iterations: 269
currently lose_sum: 360.7304591536522
time_elpased: 1.139
batch start
#iterations: 270
currently lose_sum: 360.39806962013245
time_elpased: 1.091
batch start
#iterations: 271
currently lose_sum: 362.77743458747864
time_elpased: 1.121
batch start
#iterations: 272
currently lose_sum: 360.88049682974815
time_elpased: 1.093
batch start
#iterations: 273
currently lose_sum: 360.4615487456322
time_elpased: 1.125
batch start
#iterations: 274
currently lose_sum: 360.8696576952934
time_elpased: 1.101
batch start
#iterations: 275
currently lose_sum: 361.70777291059494
time_elpased: 1.081
batch start
#iterations: 276
currently lose_sum: 360.6868478655815
time_elpased: 1.123
batch start
#iterations: 277
currently lose_sum: 361.9581136703491
time_elpased: 1.076
batch start
#iterations: 278
currently lose_sum: 361.42495650053024
time_elpased: 1.144
batch start
#iterations: 279
currently lose_sum: 361.0224517583847
time_elpased: 1.138
start validation test
0.740103092784
1.0
0.740103092784
0.850642810593
0
validation finish
batch start
#iterations: 280
currently lose_sum: 359.94695979356766
time_elpased: 1.081
batch start
#iterations: 281
currently lose_sum: 360.0388227701187
time_elpased: 1.091
batch start
#iterations: 282
currently lose_sum: 360.88209533691406
time_elpased: 1.093
batch start
#iterations: 283
currently lose_sum: 359.74384421110153
time_elpased: 1.082
batch start
#iterations: 284
currently lose_sum: 361.940756380558
time_elpased: 1.089
batch start
#iterations: 285
currently lose_sum: 361.68641567230225
time_elpased: 1.095
batch start
#iterations: 286
currently lose_sum: 361.20493906736374
time_elpased: 1.125
batch start
#iterations: 287
currently lose_sum: 363.0364460647106
time_elpased: 1.092
batch start
#iterations: 288
currently lose_sum: 361.28883451223373
time_elpased: 1.138
batch start
#iterations: 289
currently lose_sum: 359.2219613790512
time_elpased: 1.092
batch start
#iterations: 290
currently lose_sum: 360.44269049167633
time_elpased: 1.091
batch start
#iterations: 291
currently lose_sum: 360.86952793598175
time_elpased: 1.083
batch start
#iterations: 292
currently lose_sum: 361.13653057813644
time_elpased: 1.099
batch start
#iterations: 293
currently lose_sum: 360.93488270044327
time_elpased: 1.101
batch start
#iterations: 294
currently lose_sum: 360.74538028240204
time_elpased: 1.092
batch start
#iterations: 295
currently lose_sum: 360.2892926335335
time_elpased: 1.089
batch start
#iterations: 296
currently lose_sum: 360.3019517660141
time_elpased: 1.097
batch start
#iterations: 297
currently lose_sum: 361.4333436489105
time_elpased: 1.149
batch start
#iterations: 298
currently lose_sum: 362.65915709733963
time_elpased: 1.1
batch start
#iterations: 299
currently lose_sum: 361.3993510603905
time_elpased: 1.111
start validation test
0.743711340206
1.0
0.743711340206
0.853021165898
0
validation finish
batch start
#iterations: 300
currently lose_sum: 360.70771074295044
time_elpased: 1.126
batch start
#iterations: 301
currently lose_sum: 361.6127163171768
time_elpased: 1.138
batch start
#iterations: 302
currently lose_sum: 360.52168279886246
time_elpased: 1.161
batch start
#iterations: 303
currently lose_sum: 359.9599298834801
time_elpased: 1.124
batch start
#iterations: 304
currently lose_sum: 361.4497029185295
time_elpased: 1.087
batch start
#iterations: 305
currently lose_sum: 362.5624025464058
time_elpased: 1.107
batch start
#iterations: 306
currently lose_sum: 361.85726523399353
time_elpased: 1.15
batch start
#iterations: 307
currently lose_sum: 361.8520811200142
time_elpased: 1.134
batch start
#iterations: 308
currently lose_sum: 360.5837483406067
time_elpased: 1.102
batch start
#iterations: 309
currently lose_sum: 360.32823246717453
time_elpased: 1.113
batch start
#iterations: 310
currently lose_sum: 361.083449780941
time_elpased: 1.108
batch start
#iterations: 311
currently lose_sum: 362.8392724990845
time_elpased: 1.102
batch start
#iterations: 312
currently lose_sum: 360.8516129255295
time_elpased: 1.09
batch start
#iterations: 313
currently lose_sum: 361.13701766729355
time_elpased: 1.119
batch start
#iterations: 314
currently lose_sum: 360.62841796875
time_elpased: 1.094
batch start
#iterations: 315
currently lose_sum: 361.3452152609825
time_elpased: 1.085
batch start
#iterations: 316
currently lose_sum: 360.224204659462
time_elpased: 1.091
batch start
#iterations: 317
currently lose_sum: 361.2611242234707
time_elpased: 1.104
batch start
#iterations: 318
currently lose_sum: 361.92641538381577
time_elpased: 1.098
batch start
#iterations: 319
currently lose_sum: 360.018356859684
time_elpased: 1.117
start validation test
0.738865979381
1.0
0.738865979381
0.849825102271
0
validation finish
batch start
#iterations: 320
currently lose_sum: 360.38086622953415
time_elpased: 1.084
batch start
#iterations: 321
currently lose_sum: 360.596470952034
time_elpased: 1.153
batch start
#iterations: 322
currently lose_sum: 360.52161210775375
time_elpased: 1.148
batch start
#iterations: 323
currently lose_sum: 361.53438127040863
time_elpased: 1.094
batch start
#iterations: 324
currently lose_sum: 359.2128145098686
time_elpased: 1.102
batch start
#iterations: 325
currently lose_sum: 359.989732503891
time_elpased: 1.147
batch start
#iterations: 326
currently lose_sum: 361.308220744133
time_elpased: 1.125
batch start
#iterations: 327
currently lose_sum: 361.5937940478325
time_elpased: 1.139
batch start
#iterations: 328
currently lose_sum: 363.4521524310112
time_elpased: 1.147
batch start
#iterations: 329
currently lose_sum: 362.8979341387749
time_elpased: 1.107
batch start
#iterations: 330
currently lose_sum: 360.63097447156906
time_elpased: 1.147
batch start
#iterations: 331
currently lose_sum: 361.87026166915894
time_elpased: 1.105
batch start
#iterations: 332
currently lose_sum: 360.2938216328621
time_elpased: 1.097
batch start
#iterations: 333
currently lose_sum: 362.58415323495865
time_elpased: 1.156
batch start
#iterations: 334
currently lose_sum: 360.5538654923439
time_elpased: 1.092
batch start
#iterations: 335
currently lose_sum: 360.26951986551285
time_elpased: 1.111
batch start
#iterations: 336
currently lose_sum: 360.6237116754055
time_elpased: 1.093
batch start
#iterations: 337
currently lose_sum: 361.56030118465424
time_elpased: 1.138
batch start
#iterations: 338
currently lose_sum: 360.27028089761734
time_elpased: 1.116
batch start
#iterations: 339
currently lose_sum: 360.4308782219887
time_elpased: 1.149
start validation test
0.744845360825
1.0
0.744845360825
0.85376661743
0
validation finish
batch start
#iterations: 340
currently lose_sum: 360.15894478559494
time_elpased: 1.123
batch start
#iterations: 341
currently lose_sum: 360.26530915498734
time_elpased: 1.116
batch start
#iterations: 342
currently lose_sum: 361.7144583463669
time_elpased: 1.13
batch start
#iterations: 343
currently lose_sum: 360.5911988019943
time_elpased: 1.152
batch start
#iterations: 344
currently lose_sum: 360.5404308438301
time_elpased: 1.141
batch start
#iterations: 345
currently lose_sum: 361.4601330757141
time_elpased: 1.117
batch start
#iterations: 346
currently lose_sum: 360.48419257998466
time_elpased: 1.089
batch start
#iterations: 347
currently lose_sum: 360.27903002500534
time_elpased: 1.139
batch start
#iterations: 348
currently lose_sum: 360.8029730916023
time_elpased: 1.088
batch start
#iterations: 349
currently lose_sum: 361.0631077885628
time_elpased: 1.137
batch start
#iterations: 350
currently lose_sum: 360.47910553216934
time_elpased: 1.118
batch start
#iterations: 351
currently lose_sum: 361.0185420513153
time_elpased: 1.107
batch start
#iterations: 352
currently lose_sum: 360.20948046445847
time_elpased: 1.095
batch start
#iterations: 353
currently lose_sum: 361.0902536511421
time_elpased: 1.099
batch start
#iterations: 354
currently lose_sum: 361.0238689184189
time_elpased: 1.135
batch start
#iterations: 355
currently lose_sum: 359.1832203269005
time_elpased: 1.131
batch start
#iterations: 356
currently lose_sum: 361.52046996355057
time_elpased: 1.087
batch start
#iterations: 357
currently lose_sum: 360.647567152977
time_elpased: 1.119
batch start
#iterations: 358
currently lose_sum: 360.42340379953384
time_elpased: 1.133
batch start
#iterations: 359
currently lose_sum: 360.64246737957
time_elpased: 1.102
start validation test
0.742577319588
1.0
0.742577319588
0.852274744128
0
validation finish
batch start
#iterations: 360
currently lose_sum: 361.23382717370987
time_elpased: 1.097
batch start
#iterations: 361
currently lose_sum: 361.0403594970703
time_elpased: 1.115
batch start
#iterations: 362
currently lose_sum: 360.5914676785469
time_elpased: 1.11
batch start
#iterations: 363
currently lose_sum: 361.4230109155178
time_elpased: 1.126
batch start
#iterations: 364
currently lose_sum: 361.9758979678154
time_elpased: 1.099
batch start
#iterations: 365
currently lose_sum: 360.93565797805786
time_elpased: 1.1
batch start
#iterations: 366
currently lose_sum: 360.2847620844841
time_elpased: 1.088
batch start
#iterations: 367
currently lose_sum: 361.6387005150318
time_elpased: 1.085
batch start
#iterations: 368
currently lose_sum: 360.986541390419
time_elpased: 1.136
batch start
#iterations: 369
currently lose_sum: 362.4904650449753
time_elpased: 1.137
batch start
#iterations: 370
currently lose_sum: 361.09562253952026
time_elpased: 1.136
batch start
#iterations: 371
currently lose_sum: 361.1848999261856
time_elpased: 1.093
batch start
#iterations: 372
currently lose_sum: 361.36602222919464
time_elpased: 1.118
batch start
#iterations: 373
currently lose_sum: 360.5597293674946
time_elpased: 1.084
batch start
#iterations: 374
currently lose_sum: 361.4932685494423
time_elpased: 1.089
batch start
#iterations: 375
currently lose_sum: 362.5032997727394
time_elpased: 1.092
batch start
#iterations: 376
currently lose_sum: 361.11379194259644
time_elpased: 1.148
batch start
#iterations: 377
currently lose_sum: 360.21693855524063
time_elpased: 1.083
batch start
#iterations: 378
currently lose_sum: 361.61020720005035
time_elpased: 1.087
batch start
#iterations: 379
currently lose_sum: 360.44113823771477
time_elpased: 1.093
start validation test
0.746288659794
1.0
0.746288659794
0.85471397367
0
validation finish
batch start
#iterations: 380
currently lose_sum: 361.17349326610565
time_elpased: 1.126
batch start
#iterations: 381
currently lose_sum: 361.41015619039536
time_elpased: 1.129
batch start
#iterations: 382
currently lose_sum: 360.9814842939377
time_elpased: 1.09
batch start
#iterations: 383
currently lose_sum: 359.08477967977524
time_elpased: 1.093
batch start
#iterations: 384
currently lose_sum: 361.0498037338257
time_elpased: 1.103
batch start
#iterations: 385
currently lose_sum: 359.9555653333664
time_elpased: 1.095
batch start
#iterations: 386
currently lose_sum: 361.3010146021843
time_elpased: 1.104
batch start
#iterations: 387
currently lose_sum: 360.22328865528107
time_elpased: 1.092
batch start
#iterations: 388
currently lose_sum: 360.94943702220917
time_elpased: 1.125
batch start
#iterations: 389
currently lose_sum: 360.2124025821686
time_elpased: 1.085
batch start
#iterations: 390
currently lose_sum: 359.79115706682205
time_elpased: 1.126
batch start
#iterations: 391
currently lose_sum: 359.3312259912491
time_elpased: 1.088
batch start
#iterations: 392
currently lose_sum: 361.1686542034149
time_elpased: 1.101
batch start
#iterations: 393
currently lose_sum: 362.05317586660385
time_elpased: 1.091
batch start
#iterations: 394
currently lose_sum: 359.84405636787415
time_elpased: 1.088
batch start
#iterations: 395
currently lose_sum: 360.72454339265823
time_elpased: 1.097
batch start
#iterations: 396
currently lose_sum: 360.6366562843323
time_elpased: 1.109
batch start
#iterations: 397
currently lose_sum: 360.6592282652855
time_elpased: 1.101
batch start
#iterations: 398
currently lose_sum: 361.4417155981064
time_elpased: 1.133
batch start
#iterations: 399
currently lose_sum: 361.64537984132767
time_elpased: 1.09
start validation test
0.749587628866
1.0
0.749587628866
0.856873490071
0
validation finish
acc: 0.760
pre: 1.000
rec: 0.760
F1: 0.864
auc: 0.000
