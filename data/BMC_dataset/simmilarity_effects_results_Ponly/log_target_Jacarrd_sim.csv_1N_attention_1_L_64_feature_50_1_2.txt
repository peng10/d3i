start to construct graph
graph construct over
58300
epochs start
batch start
#iterations: 0
currently lose_sum: 398.76928931474686
time_elpased: 1.225
batch start
#iterations: 1
currently lose_sum: 390.43042755126953
time_elpased: 1.185
batch start
#iterations: 2
currently lose_sum: 384.4533145427704
time_elpased: 1.179
batch start
#iterations: 3
currently lose_sum: 381.9466604590416
time_elpased: 1.182
batch start
#iterations: 4
currently lose_sum: 380.0565750002861
time_elpased: 1.177
batch start
#iterations: 5
currently lose_sum: 378.18435341119766
time_elpased: 1.179
batch start
#iterations: 6
currently lose_sum: 376.7750158905983
time_elpased: 1.19
batch start
#iterations: 7
currently lose_sum: 375.93215072155
time_elpased: 1.194
batch start
#iterations: 8
currently lose_sum: 374.60659170150757
time_elpased: 1.188
batch start
#iterations: 9
currently lose_sum: 375.8597228527069
time_elpased: 1.184
batch start
#iterations: 10
currently lose_sum: 372.28169018030167
time_elpased: 1.185
batch start
#iterations: 11
currently lose_sum: 373.0305069088936
time_elpased: 1.183
batch start
#iterations: 12
currently lose_sum: 373.0269293785095
time_elpased: 1.191
batch start
#iterations: 13
currently lose_sum: 374.75833255052567
time_elpased: 1.19
batch start
#iterations: 14
currently lose_sum: 369.4943070411682
time_elpased: 1.192
batch start
#iterations: 15
currently lose_sum: 370.9784957766533
time_elpased: 1.18
batch start
#iterations: 16
currently lose_sum: 370.8505408167839
time_elpased: 1.188
batch start
#iterations: 17
currently lose_sum: 371.4664170742035
time_elpased: 1.184
batch start
#iterations: 18
currently lose_sum: 368.99398493766785
time_elpased: 1.194
batch start
#iterations: 19
currently lose_sum: 371.10096502304077
time_elpased: 1.182
start validation test
0.668969072165
1.0
0.668969072165
0.801655445055
0
validation finish
batch start
#iterations: 20
currently lose_sum: 369.9866269826889
time_elpased: 1.218
batch start
#iterations: 21
currently lose_sum: 370.2762054204941
time_elpased: 1.184
batch start
#iterations: 22
currently lose_sum: 370.5395831465721
time_elpased: 1.178
batch start
#iterations: 23
currently lose_sum: 371.0120248198509
time_elpased: 1.182
batch start
#iterations: 24
currently lose_sum: 369.7835011482239
time_elpased: 1.2
batch start
#iterations: 25
currently lose_sum: 370.13263511657715
time_elpased: 1.174
batch start
#iterations: 26
currently lose_sum: 369.02741086483
time_elpased: 1.204
batch start
#iterations: 27
currently lose_sum: 369.35882806777954
time_elpased: 1.194
batch start
#iterations: 28
currently lose_sum: 367.41556626558304
time_elpased: 1.182
batch start
#iterations: 29
currently lose_sum: 369.4971249103546
time_elpased: 1.195
batch start
#iterations: 30
currently lose_sum: 370.2035798430443
time_elpased: 1.213
batch start
#iterations: 31
currently lose_sum: 368.07231599092484
time_elpased: 1.185
batch start
#iterations: 32
currently lose_sum: 368.7292211651802
time_elpased: 1.186
batch start
#iterations: 33
currently lose_sum: 369.08282113075256
time_elpased: 1.182
batch start
#iterations: 34
currently lose_sum: 368.6865387558937
time_elpased: 1.193
batch start
#iterations: 35
currently lose_sum: 368.985839009285
time_elpased: 1.186
batch start
#iterations: 36
currently lose_sum: 368.18316185474396
time_elpased: 1.22
batch start
#iterations: 37
currently lose_sum: 367.59843879938126
time_elpased: 1.241
batch start
#iterations: 38
currently lose_sum: 368.6803296804428
time_elpased: 1.206
batch start
#iterations: 39
currently lose_sum: 368.2918481826782
time_elpased: 1.202
start validation test
0.687628865979
1.0
0.687628865979
0.8149053146
0
validation finish
batch start
#iterations: 40
currently lose_sum: 367.24950951337814
time_elpased: 1.205
batch start
#iterations: 41
currently lose_sum: 367.4516754746437
time_elpased: 1.242
batch start
#iterations: 42
currently lose_sum: 365.96494752168655
time_elpased: 1.193
batch start
#iterations: 43
currently lose_sum: 367.4986003637314
time_elpased: 1.194
batch start
#iterations: 44
currently lose_sum: 368.1541277170181
time_elpased: 1.183
batch start
#iterations: 45
currently lose_sum: 366.6371042728424
time_elpased: 1.19
batch start
#iterations: 46
currently lose_sum: 366.8097086548805
time_elpased: 1.191
batch start
#iterations: 47
currently lose_sum: 367.51131212711334
time_elpased: 1.177
batch start
#iterations: 48
currently lose_sum: 366.6840350031853
time_elpased: 1.186
batch start
#iterations: 49
currently lose_sum: 367.1838019490242
time_elpased: 1.196
batch start
#iterations: 50
currently lose_sum: 365.66176921129227
time_elpased: 1.18
batch start
#iterations: 51
currently lose_sum: 367.1179420351982
time_elpased: 1.185
batch start
#iterations: 52
currently lose_sum: 367.0374373793602
time_elpased: 1.19
batch start
#iterations: 53
currently lose_sum: 367.3845454454422
time_elpased: 1.196
batch start
#iterations: 54
currently lose_sum: 367.3147704601288
time_elpased: 1.245
batch start
#iterations: 55
currently lose_sum: 365.67878770828247
time_elpased: 1.246
batch start
#iterations: 56
currently lose_sum: 367.04819309711456
time_elpased: 1.187
batch start
#iterations: 57
currently lose_sum: 365.9554648399353
time_elpased: 1.196
batch start
#iterations: 58
currently lose_sum: 365.9391168951988
time_elpased: 1.183
batch start
#iterations: 59
currently lose_sum: 366.45552480220795
time_elpased: 1.173
start validation test
0.742268041237
1.0
0.742268041237
0.852071005917
0
validation finish
batch start
#iterations: 60
currently lose_sum: 366.07248091697693
time_elpased: 1.195
batch start
#iterations: 61
currently lose_sum: 365.72091180086136
time_elpased: 1.184
batch start
#iterations: 62
currently lose_sum: 365.5491781234741
time_elpased: 1.178
batch start
#iterations: 63
currently lose_sum: 365.76482915878296
time_elpased: 1.202
batch start
#iterations: 64
currently lose_sum: 366.3080008625984
time_elpased: 1.211
batch start
#iterations: 65
currently lose_sum: 366.3636213541031
time_elpased: 1.184
batch start
#iterations: 66
currently lose_sum: 366.13534450531006
time_elpased: 1.184
batch start
#iterations: 67
currently lose_sum: 366.44892662763596
time_elpased: 1.19
batch start
#iterations: 68
currently lose_sum: 365.91593939065933
time_elpased: 1.189
batch start
#iterations: 69
currently lose_sum: 365.95611333847046
time_elpased: 1.177
batch start
#iterations: 70
currently lose_sum: 365.78004348278046
time_elpased: 1.239
batch start
#iterations: 71
currently lose_sum: 365.5051817893982
time_elpased: 1.178
batch start
#iterations: 72
currently lose_sum: 365.6098357439041
time_elpased: 1.178
batch start
#iterations: 73
currently lose_sum: 364.8436357975006
time_elpased: 1.186
batch start
#iterations: 74
currently lose_sum: 365.4616976380348
time_elpased: 1.259
batch start
#iterations: 75
currently lose_sum: 364.7178406715393
time_elpased: 1.178
batch start
#iterations: 76
currently lose_sum: 364.97023010253906
time_elpased: 1.191
batch start
#iterations: 77
currently lose_sum: 365.39264166355133
time_elpased: 1.181
batch start
#iterations: 78
currently lose_sum: 366.26174342632294
time_elpased: 1.221
batch start
#iterations: 79
currently lose_sum: 365.2406924366951
time_elpased: 1.242
start validation test
0.728144329897
1.0
0.728144329897
0.8426892561
0
validation finish
batch start
#iterations: 80
currently lose_sum: 364.5670667886734
time_elpased: 1.184
batch start
#iterations: 81
currently lose_sum: 365.62255096435547
time_elpased: 1.188
batch start
#iterations: 82
currently lose_sum: 365.4344722032547
time_elpased: 1.227
batch start
#iterations: 83
currently lose_sum: 364.7752470970154
time_elpased: 1.21
batch start
#iterations: 84
currently lose_sum: 366.29425573349
time_elpased: 1.204
batch start
#iterations: 85
currently lose_sum: 366.0663677453995
time_elpased: 1.234
batch start
#iterations: 86
currently lose_sum: 365.06920170783997
time_elpased: 1.184
batch start
#iterations: 87
currently lose_sum: 364.4478005170822
time_elpased: 1.201
batch start
#iterations: 88
currently lose_sum: 364.74227011203766
time_elpased: 1.236
batch start
#iterations: 89
currently lose_sum: 364.6855046749115
time_elpased: 1.216
batch start
#iterations: 90
currently lose_sum: 366.2020436525345
time_elpased: 1.211
batch start
#iterations: 91
currently lose_sum: 364.6132528781891
time_elpased: 1.178
batch start
#iterations: 92
currently lose_sum: 363.9011896252632
time_elpased: 1.184
batch start
#iterations: 93
currently lose_sum: 366.29640704393387
time_elpased: 1.172
batch start
#iterations: 94
currently lose_sum: 364.3535546660423
time_elpased: 1.192
batch start
#iterations: 95
currently lose_sum: 364.7202542424202
time_elpased: 1.17
batch start
#iterations: 96
currently lose_sum: 364.77425560355186
time_elpased: 1.23
batch start
#iterations: 97
currently lose_sum: 364.21665477752686
time_elpased: 1.207
batch start
#iterations: 98
currently lose_sum: 363.71392676234245
time_elpased: 1.183
batch start
#iterations: 99
currently lose_sum: 364.49161195755005
time_elpased: 1.198
start validation test
0.742371134021
1.0
0.742371134021
0.852138926691
0
validation finish
batch start
#iterations: 100
currently lose_sum: 364.6130741238594
time_elpased: 1.194
batch start
#iterations: 101
currently lose_sum: 364.05765610933304
time_elpased: 1.188
batch start
#iterations: 102
currently lose_sum: 363.92703956365585
time_elpased: 1.175
batch start
#iterations: 103
currently lose_sum: 363.5435695052147
time_elpased: 1.198
batch start
#iterations: 104
currently lose_sum: 364.72184628248215
time_elpased: 1.23
batch start
#iterations: 105
currently lose_sum: 363.85904628038406
time_elpased: 1.184
batch start
#iterations: 106
currently lose_sum: 364.8587651848793
time_elpased: 1.192
batch start
#iterations: 107
currently lose_sum: 364.6303310394287
time_elpased: 1.192
batch start
#iterations: 108
currently lose_sum: 364.93994653224945
time_elpased: 1.189
batch start
#iterations: 109
currently lose_sum: 364.50187599658966
time_elpased: 1.232
batch start
#iterations: 110
currently lose_sum: 364.78813922405243
time_elpased: 1.175
batch start
#iterations: 111
currently lose_sum: 363.99650579690933
time_elpased: 1.199
batch start
#iterations: 112
currently lose_sum: 364.2505431175232
time_elpased: 1.198
batch start
#iterations: 113
currently lose_sum: 364.3637371659279
time_elpased: 1.233
batch start
#iterations: 114
currently lose_sum: 363.30728536844254
time_elpased: 1.195
batch start
#iterations: 115
currently lose_sum: 363.7129690051079
time_elpased: 1.185
batch start
#iterations: 116
currently lose_sum: 364.8987372517586
time_elpased: 1.183
batch start
#iterations: 117
currently lose_sum: 363.4056847691536
time_elpased: 1.189
batch start
#iterations: 118
currently lose_sum: 362.41150438785553
time_elpased: 1.17
batch start
#iterations: 119
currently lose_sum: 363.7045360803604
time_elpased: 1.186
start validation test
0.746804123711
1.0
0.746804123711
0.855051935788
0
validation finish
batch start
#iterations: 120
currently lose_sum: 364.0562852025032
time_elpased: 1.177
batch start
#iterations: 121
currently lose_sum: 363.0313683748245
time_elpased: 1.2
batch start
#iterations: 122
currently lose_sum: 362.01173001527786
time_elpased: 1.23
batch start
#iterations: 123
currently lose_sum: 363.7418919801712
time_elpased: 1.187
batch start
#iterations: 124
currently lose_sum: 362.72494810819626
time_elpased: 1.231
batch start
#iterations: 125
currently lose_sum: 363.8757259249687
time_elpased: 1.252
batch start
#iterations: 126
currently lose_sum: 364.98192512989044
time_elpased: 1.181
batch start
#iterations: 127
currently lose_sum: 363.5832168459892
time_elpased: 1.208
batch start
#iterations: 128
currently lose_sum: 362.8211042881012
time_elpased: 1.184
batch start
#iterations: 129
currently lose_sum: 364.3185892701149
time_elpased: 1.225
batch start
#iterations: 130
currently lose_sum: 363.19039648771286
time_elpased: 1.207
batch start
#iterations: 131
currently lose_sum: 363.08694756031036
time_elpased: 1.245
batch start
#iterations: 132
currently lose_sum: 363.50384175777435
time_elpased: 1.187
batch start
#iterations: 133
currently lose_sum: 363.4406187534332
time_elpased: 1.185
batch start
#iterations: 134
currently lose_sum: 364.2765961289406
time_elpased: 1.238
batch start
#iterations: 135
currently lose_sum: 363.0315473675728
time_elpased: 1.181
batch start
#iterations: 136
currently lose_sum: 365.28096717596054
time_elpased: 1.189
batch start
#iterations: 137
currently lose_sum: 363.3049216866493
time_elpased: 1.2
batch start
#iterations: 138
currently lose_sum: 364.0214838683605
time_elpased: 1.187
batch start
#iterations: 139
currently lose_sum: 362.38861536979675
time_elpased: 1.2
start validation test
0.75
1.0
0.75
0.857142857143
0
validation finish
batch start
#iterations: 140
currently lose_sum: 363.27164047956467
time_elpased: 1.208
batch start
#iterations: 141
currently lose_sum: 364.2028714418411
time_elpased: 1.187
batch start
#iterations: 142
currently lose_sum: 364.236744761467
time_elpased: 1.245
batch start
#iterations: 143
currently lose_sum: 364.0979722738266
time_elpased: 1.234
batch start
#iterations: 144
currently lose_sum: 363.220715880394
time_elpased: 1.183
batch start
#iterations: 145
currently lose_sum: 363.76167917251587
time_elpased: 1.183
batch start
#iterations: 146
currently lose_sum: 362.963643014431
time_elpased: 1.193
batch start
#iterations: 147
currently lose_sum: 363.55248802900314
time_elpased: 1.196
batch start
#iterations: 148
currently lose_sum: 363.4507793188095
time_elpased: 1.178
batch start
#iterations: 149
currently lose_sum: 363.695947766304
time_elpased: 1.194
batch start
#iterations: 150
currently lose_sum: 362.92664551734924
time_elpased: 1.208
batch start
#iterations: 151
currently lose_sum: 363.2967920303345
time_elpased: 1.226
batch start
#iterations: 152
currently lose_sum: 362.89144307374954
time_elpased: 1.187
batch start
#iterations: 153
currently lose_sum: 364.5671244263649
time_elpased: 1.188
batch start
#iterations: 154
currently lose_sum: 363.22778284549713
time_elpased: 1.197
batch start
#iterations: 155
currently lose_sum: 364.42033898830414
time_elpased: 1.179
batch start
#iterations: 156
currently lose_sum: 363.71373450756073
time_elpased: 1.198
batch start
#iterations: 157
currently lose_sum: 362.9097546339035
time_elpased: 1.225
batch start
#iterations: 158
currently lose_sum: 363.27203154563904
time_elpased: 1.187
batch start
#iterations: 159
currently lose_sum: 364.62870317697525
time_elpased: 1.196
start validation test
0.749587628866
1.0
0.749587628866
0.856873490071
0
validation finish
batch start
#iterations: 160
currently lose_sum: 363.722828745842
time_elpased: 1.184
batch start
#iterations: 161
currently lose_sum: 362.35059440135956
time_elpased: 1.208
batch start
#iterations: 162
currently lose_sum: 363.0642935037613
time_elpased: 1.207
batch start
#iterations: 163
currently lose_sum: 363.1064732670784
time_elpased: 1.176
batch start
#iterations: 164
currently lose_sum: 362.33729112148285
time_elpased: 1.214
batch start
#iterations: 165
currently lose_sum: 361.7190326452255
time_elpased: 1.237
batch start
#iterations: 166
currently lose_sum: 362.35728096961975
time_elpased: 1.197
batch start
#iterations: 167
currently lose_sum: 363.4396364092827
time_elpased: 1.192
batch start
#iterations: 168
currently lose_sum: 362.7067939043045
time_elpased: 1.185
batch start
#iterations: 169
currently lose_sum: 362.3143926858902
time_elpased: 1.196
batch start
#iterations: 170
currently lose_sum: 363.17226696014404
time_elpased: 1.25
batch start
#iterations: 171
currently lose_sum: 362.94341123104095
time_elpased: 1.213
batch start
#iterations: 172
currently lose_sum: 363.0326362848282
time_elpased: 1.209
batch start
#iterations: 173
currently lose_sum: 363.73744881153107
time_elpased: 1.187
batch start
#iterations: 174
currently lose_sum: 361.45563519001007
time_elpased: 1.208
batch start
#iterations: 175
currently lose_sum: 364.04481291770935
time_elpased: 1.191
batch start
#iterations: 176
currently lose_sum: 363.34284061193466
time_elpased: 1.184
batch start
#iterations: 177
currently lose_sum: 362.4316709637642
time_elpased: 1.189
batch start
#iterations: 178
currently lose_sum: 364.05030357837677
time_elpased: 1.186
batch start
#iterations: 179
currently lose_sum: 362.66679483652115
time_elpased: 1.236
start validation test
0.745051546392
1.0
0.745051546392
0.853902049979
0
validation finish
batch start
#iterations: 180
currently lose_sum: 363.55438327789307
time_elpased: 1.191
batch start
#iterations: 181
currently lose_sum: 362.53236067295074
time_elpased: 1.19
batch start
#iterations: 182
currently lose_sum: 363.40669029951096
time_elpased: 1.251
batch start
#iterations: 183
currently lose_sum: 362.8356726169586
time_elpased: 1.185
batch start
#iterations: 184
currently lose_sum: 362.5116955637932
time_elpased: 1.214
batch start
#iterations: 185
currently lose_sum: 362.91182792186737
time_elpased: 1.209
batch start
#iterations: 186
currently lose_sum: 361.9324144721031
time_elpased: 1.191
batch start
#iterations: 187
currently lose_sum: 361.99016523361206
time_elpased: 1.187
batch start
#iterations: 188
currently lose_sum: 363.1216632127762
time_elpased: 1.264
batch start
#iterations: 189
currently lose_sum: 362.8440636396408
time_elpased: 1.293
batch start
#iterations: 190
currently lose_sum: 361.5224790275097
time_elpased: 1.188
batch start
#iterations: 191
currently lose_sum: 362.185067653656
time_elpased: 1.195
batch start
#iterations: 192
currently lose_sum: 363.82704842090607
time_elpased: 1.184
batch start
#iterations: 193
currently lose_sum: 363.062077999115
time_elpased: 1.188
batch start
#iterations: 194
currently lose_sum: 363.05897825956345
time_elpased: 1.188
batch start
#iterations: 195
currently lose_sum: 363.10827481746674
time_elpased: 1.23
batch start
#iterations: 196
currently lose_sum: 363.79957914352417
time_elpased: 1.193
batch start
#iterations: 197
currently lose_sum: 362.8406399488449
time_elpased: 1.194
batch start
#iterations: 198
currently lose_sum: 363.48953890800476
time_elpased: 1.17
batch start
#iterations: 199
currently lose_sum: 362.09169524908066
time_elpased: 1.23
start validation test
0.745670103093
1.0
0.745670103093
0.854308155672
0
validation finish
batch start
#iterations: 200
currently lose_sum: 363.46625792980194
time_elpased: 1.199
batch start
#iterations: 201
currently lose_sum: 361.73493790626526
time_elpased: 1.181
batch start
#iterations: 202
currently lose_sum: 360.88107019662857
time_elpased: 1.215
batch start
#iterations: 203
currently lose_sum: 360.86791360378265
time_elpased: 1.197
batch start
#iterations: 204
currently lose_sum: 362.7905855178833
time_elpased: 1.209
batch start
#iterations: 205
currently lose_sum: 361.38432163000107
time_elpased: 1.202
batch start
#iterations: 206
currently lose_sum: 363.1746743917465
time_elpased: 1.181
batch start
#iterations: 207
currently lose_sum: 364.49585741758347
time_elpased: 1.203
batch start
#iterations: 208
currently lose_sum: 362.89602440595627
time_elpased: 1.248
batch start
#iterations: 209
currently lose_sum: 361.2316298484802
time_elpased: 1.195
batch start
#iterations: 210
currently lose_sum: 362.1477425098419
time_elpased: 1.182
batch start
#iterations: 211
currently lose_sum: 362.6592212319374
time_elpased: 1.231
batch start
#iterations: 212
currently lose_sum: 362.2124448418617
time_elpased: 1.185
batch start
#iterations: 213
currently lose_sum: 363.52482891082764
time_elpased: 1.24
batch start
#iterations: 214
currently lose_sum: 364.11607897281647
time_elpased: 1.229
batch start
#iterations: 215
currently lose_sum: 362.4863682985306
time_elpased: 1.243
batch start
#iterations: 216
currently lose_sum: 362.9802197217941
time_elpased: 1.197
batch start
#iterations: 217
currently lose_sum: 362.6419785618782
time_elpased: 1.185
batch start
#iterations: 218
currently lose_sum: 362.28323298692703
time_elpased: 1.187
batch start
#iterations: 219
currently lose_sum: 363.69310253858566
time_elpased: 1.204
start validation test
0.744432989691
1.0
0.744432989691
0.853495656285
0
validation finish
batch start
#iterations: 220
currently lose_sum: 361.35254442691803
time_elpased: 1.188
batch start
#iterations: 221
currently lose_sum: 363.13150215148926
time_elpased: 1.191
batch start
#iterations: 222
currently lose_sum: 363.04293888807297
time_elpased: 1.193
batch start
#iterations: 223
currently lose_sum: 362.2825311422348
time_elpased: 1.198
batch start
#iterations: 224
currently lose_sum: 362.55747097730637
time_elpased: 1.187
batch start
#iterations: 225
currently lose_sum: 362.7108461856842
time_elpased: 1.227
batch start
#iterations: 226
currently lose_sum: 363.040894985199
time_elpased: 1.182
batch start
#iterations: 227
currently lose_sum: 362.7815663218498
time_elpased: 1.212
batch start
#iterations: 228
currently lose_sum: 362.1927065253258
time_elpased: 1.193
batch start
#iterations: 229
currently lose_sum: 361.49321419000626
time_elpased: 1.19
batch start
#iterations: 230
currently lose_sum: 363.2243373990059
time_elpased: 1.229
batch start
#iterations: 231
currently lose_sum: 362.57357305288315
time_elpased: 1.188
batch start
#iterations: 232
currently lose_sum: 362.82167410850525
time_elpased: 1.257
batch start
#iterations: 233
currently lose_sum: 362.3643527030945
time_elpased: 1.2
batch start
#iterations: 234
currently lose_sum: 362.3614048361778
time_elpased: 1.184
batch start
#iterations: 235
currently lose_sum: 361.56522846221924
time_elpased: 1.181
batch start
#iterations: 236
currently lose_sum: 362.14442962408066
time_elpased: 1.218
batch start
#iterations: 237
currently lose_sum: 361.6706608533859
time_elpased: 1.216
batch start
#iterations: 238
currently lose_sum: 362.90757870674133
time_elpased: 1.223
batch start
#iterations: 239
currently lose_sum: 360.9148054122925
time_elpased: 1.184
start validation test
0.742577319588
1.0
0.742577319588
0.852274744128
0
validation finish
batch start
#iterations: 240
currently lose_sum: 361.32793712615967
time_elpased: 1.189
batch start
#iterations: 241
currently lose_sum: 362.2713705301285
time_elpased: 1.172
batch start
#iterations: 242
currently lose_sum: 360.98979675769806
time_elpased: 1.199
batch start
#iterations: 243
currently lose_sum: 362.38678377866745
time_elpased: 1.213
batch start
#iterations: 244
currently lose_sum: 363.41824531555176
time_elpased: 1.187
batch start
#iterations: 245
currently lose_sum: 361.5628066062927
time_elpased: 1.186
batch start
#iterations: 246
currently lose_sum: 362.2180582880974
time_elpased: 1.193
batch start
#iterations: 247
currently lose_sum: 362.90129309892654
time_elpased: 1.179
batch start
#iterations: 248
currently lose_sum: 363.5438008904457
time_elpased: 1.185
batch start
#iterations: 249
currently lose_sum: 361.6800297498703
time_elpased: 1.193
batch start
#iterations: 250
currently lose_sum: 363.48256742954254
time_elpased: 1.184
batch start
#iterations: 251
currently lose_sum: 363.97461557388306
time_elpased: 1.237
batch start
#iterations: 252
currently lose_sum: 362.6042903661728
time_elpased: 1.199
batch start
#iterations: 253
currently lose_sum: 361.8299499154091
time_elpased: 1.205
batch start
#iterations: 254
currently lose_sum: 362.2934345006943
time_elpased: 1.183
batch start
#iterations: 255
currently lose_sum: 362.61859196424484
time_elpased: 1.184
batch start
#iterations: 256
currently lose_sum: 361.9105404615402
time_elpased: 1.186
batch start
#iterations: 257
currently lose_sum: 363.6185746192932
time_elpased: 1.179
batch start
#iterations: 258
currently lose_sum: 364.3997517824173
time_elpased: 1.187
batch start
#iterations: 259
currently lose_sum: 361.94855535030365
time_elpased: 1.247
start validation test
0.760721649485
1.0
0.760721649485
0.864102113707
0
validation finish
batch start
#iterations: 260
currently lose_sum: 361.5869468450546
time_elpased: 1.209
batch start
#iterations: 261
currently lose_sum: 361.637442111969
time_elpased: 1.18
batch start
#iterations: 262
currently lose_sum: 363.37803024053574
time_elpased: 1.19
batch start
#iterations: 263
currently lose_sum: 363.6780200600624
time_elpased: 1.195
batch start
#iterations: 264
currently lose_sum: 361.98041838407516
time_elpased: 1.195
batch start
#iterations: 265
currently lose_sum: 361.7225679755211
time_elpased: 1.183
batch start
#iterations: 266
currently lose_sum: 362.8899407982826
time_elpased: 1.224
batch start
#iterations: 267
currently lose_sum: 362.42934453487396
time_elpased: 1.179
batch start
#iterations: 268
currently lose_sum: 362.02092891931534
time_elpased: 1.183
batch start
#iterations: 269
currently lose_sum: 362.2440618276596
time_elpased: 1.25
batch start
#iterations: 270
currently lose_sum: 360.9443210363388
time_elpased: 1.198
batch start
#iterations: 271
currently lose_sum: 361.9545529484749
time_elpased: 1.186
batch start
#iterations: 272
currently lose_sum: 362.67750403285027
time_elpased: 1.18
batch start
#iterations: 273
currently lose_sum: 361.530535697937
time_elpased: 1.234
batch start
#iterations: 274
currently lose_sum: 363.2128297686577
time_elpased: 1.181
batch start
#iterations: 275
currently lose_sum: 363.1362843513489
time_elpased: 1.192
batch start
#iterations: 276
currently lose_sum: 361.6985988020897
time_elpased: 1.19
batch start
#iterations: 277
currently lose_sum: 362.367919921875
time_elpased: 1.19
batch start
#iterations: 278
currently lose_sum: 362.16867285966873
time_elpased: 1.188
batch start
#iterations: 279
currently lose_sum: 361.55389231443405
time_elpased: 1.206
start validation test
0.744432989691
1.0
0.744432989691
0.853495656285
0
validation finish
batch start
#iterations: 280
currently lose_sum: 361.9384807944298
time_elpased: 1.215
batch start
#iterations: 281
currently lose_sum: 363.6350308060646
time_elpased: 1.229
batch start
#iterations: 282
currently lose_sum: 361.9524676799774
time_elpased: 1.184
batch start
#iterations: 283
currently lose_sum: 361.22711157798767
time_elpased: 1.206
batch start
#iterations: 284
currently lose_sum: 362.1039211153984
time_elpased: 1.179
batch start
#iterations: 285
currently lose_sum: 360.64831006526947
time_elpased: 1.208
batch start
#iterations: 286
currently lose_sum: 362.0410031080246
time_elpased: 1.187
batch start
#iterations: 287
currently lose_sum: 361.2971478700638
time_elpased: 1.224
batch start
#iterations: 288
currently lose_sum: 362.31334018707275
time_elpased: 1.221
batch start
#iterations: 289
currently lose_sum: 361.1412299275398
time_elpased: 1.233
batch start
#iterations: 290
currently lose_sum: 361.41964983940125
time_elpased: 1.183
batch start
#iterations: 291
currently lose_sum: 362.31540578603745
time_elpased: 1.197
batch start
#iterations: 292
currently lose_sum: 362.11492413282394
time_elpased: 1.193
batch start
#iterations: 293
currently lose_sum: 362.19759607315063
time_elpased: 1.198
batch start
#iterations: 294
currently lose_sum: 361.58861339092255
time_elpased: 1.18
batch start
#iterations: 295
currently lose_sum: 361.9446676969528
time_elpased: 1.192
batch start
#iterations: 296
currently lose_sum: 361.25916957855225
time_elpased: 1.252
batch start
#iterations: 297
currently lose_sum: 361.79004591703415
time_elpased: 1.199
batch start
#iterations: 298
currently lose_sum: 361.1649606227875
time_elpased: 1.19
batch start
#iterations: 299
currently lose_sum: 360.2189131975174
time_elpased: 1.202
start validation test
0.758041237113
1.0
0.758041237113
0.862370257433
0
validation finish
batch start
#iterations: 300
currently lose_sum: 360.71244019269943
time_elpased: 1.228
batch start
#iterations: 301
currently lose_sum: 362.3143142461777
time_elpased: 1.204
batch start
#iterations: 302
currently lose_sum: 363.02310913801193
time_elpased: 1.214
batch start
#iterations: 303
currently lose_sum: 363.33837473392487
time_elpased: 1.184
batch start
#iterations: 304
currently lose_sum: 363.27504831552505
time_elpased: 1.18
batch start
#iterations: 305
currently lose_sum: 362.2771996855736
time_elpased: 1.185
batch start
#iterations: 306
currently lose_sum: 360.65764474868774
time_elpased: 1.185
batch start
#iterations: 307
currently lose_sum: 361.66975462436676
time_elpased: 1.178
batch start
#iterations: 308
currently lose_sum: 361.8740258216858
time_elpased: 1.192
batch start
#iterations: 309
currently lose_sum: 361.7768171429634
time_elpased: 1.185
batch start
#iterations: 310
currently lose_sum: 361.2493976354599
time_elpased: 1.221
batch start
#iterations: 311
currently lose_sum: 362.87256371974945
time_elpased: 1.244
batch start
#iterations: 312
currently lose_sum: 359.9056318998337
time_elpased: 1.216
batch start
#iterations: 313
currently lose_sum: 360.6671420931816
time_elpased: 1.178
batch start
#iterations: 314
currently lose_sum: 362.1928131580353
time_elpased: 1.22
batch start
#iterations: 315
currently lose_sum: 362.5132735967636
time_elpased: 1.186
batch start
#iterations: 316
currently lose_sum: 360.5729202032089
time_elpased: 1.204
batch start
#iterations: 317
currently lose_sum: 362.4487875699997
time_elpased: 1.202
batch start
#iterations: 318
currently lose_sum: 362.8216044306755
time_elpased: 1.211
batch start
#iterations: 319
currently lose_sum: 362.105339884758
time_elpased: 1.193
start validation test
0.759072164948
1.0
0.759072164948
0.863036980601
0
validation finish
batch start
#iterations: 320
currently lose_sum: 361.93231201171875
time_elpased: 1.173
batch start
#iterations: 321
currently lose_sum: 362.1811930537224
time_elpased: 1.196
batch start
#iterations: 322
currently lose_sum: 361.66135627031326
time_elpased: 1.208
batch start
#iterations: 323
currently lose_sum: 361.9883061647415
time_elpased: 1.18
batch start
#iterations: 324
currently lose_sum: 361.14675211906433
time_elpased: 1.186
batch start
#iterations: 325
currently lose_sum: 362.71953642368317
time_elpased: 1.194
batch start
#iterations: 326
currently lose_sum: 361.8410846590996
time_elpased: 1.196
batch start
#iterations: 327
currently lose_sum: 361.3702020049095
time_elpased: 1.203
batch start
#iterations: 328
currently lose_sum: 361.2070756852627
time_elpased: 1.197
batch start
#iterations: 329
currently lose_sum: 361.25115793943405
time_elpased: 1.184
batch start
#iterations: 330
currently lose_sum: 362.0526471734047
time_elpased: 1.186
batch start
#iterations: 331
currently lose_sum: 360.97822284698486
time_elpased: 1.199
batch start
#iterations: 332
currently lose_sum: 361.3532689809799
time_elpased: 1.179
batch start
#iterations: 333
currently lose_sum: 361.6444531083107
time_elpased: 1.194
batch start
#iterations: 334
currently lose_sum: 361.39373528957367
time_elpased: 1.233
batch start
#iterations: 335
currently lose_sum: 362.52869790792465
time_elpased: 1.196
batch start
#iterations: 336
currently lose_sum: 362.5403473973274
time_elpased: 1.187
batch start
#iterations: 337
currently lose_sum: 360.89947682619095
time_elpased: 1.185
batch start
#iterations: 338
currently lose_sum: 361.54650819301605
time_elpased: 1.188
batch start
#iterations: 339
currently lose_sum: 361.12893038988113
time_elpased: 1.213
start validation test
0.75587628866
1.0
0.75587628866
0.860967590418
0
validation finish
batch start
#iterations: 340
currently lose_sum: 361.63366264104843
time_elpased: 1.199
batch start
#iterations: 341
currently lose_sum: 362.0929648280144
time_elpased: 1.188
batch start
#iterations: 342
currently lose_sum: 361.8051196336746
time_elpased: 1.186
batch start
#iterations: 343
currently lose_sum: 360.7951267361641
time_elpased: 1.182
batch start
#iterations: 344
currently lose_sum: 361.5491814017296
time_elpased: 1.201
batch start
#iterations: 345
currently lose_sum: 361.9659501314163
time_elpased: 1.205
batch start
#iterations: 346
currently lose_sum: 362.2056125998497
time_elpased: 1.204
batch start
#iterations: 347
currently lose_sum: 362.7505156993866
time_elpased: 1.187
batch start
#iterations: 348
currently lose_sum: 360.21098136901855
time_elpased: 1.186
batch start
#iterations: 349
currently lose_sum: 360.62063306570053
time_elpased: 1.186
batch start
#iterations: 350
currently lose_sum: 361.9098491072655
time_elpased: 1.201
batch start
#iterations: 351
currently lose_sum: 361.088541328907
time_elpased: 1.212
batch start
#iterations: 352
currently lose_sum: 363.3759709596634
time_elpased: 1.219
batch start
#iterations: 353
currently lose_sum: 361.76448115706444
time_elpased: 1.238
batch start
#iterations: 354
currently lose_sum: 361.0211152434349
time_elpased: 1.182
batch start
#iterations: 355
currently lose_sum: 361.2185792326927
time_elpased: 1.247
batch start
#iterations: 356
currently lose_sum: 362.37318855524063
time_elpased: 1.243
batch start
#iterations: 357
currently lose_sum: 361.2046102285385
time_elpased: 1.238
batch start
#iterations: 358
currently lose_sum: 360.73711371421814
time_elpased: 1.174
batch start
#iterations: 359
currently lose_sum: 360.36340218782425
time_elpased: 1.182
start validation test
0.747422680412
1.0
0.747422680412
0.855457227139
0
validation finish
batch start
#iterations: 360
currently lose_sum: 362.34070444107056
time_elpased: 1.209
batch start
#iterations: 361
currently lose_sum: 361.7432951927185
time_elpased: 1.194
batch start
#iterations: 362
currently lose_sum: 361.27052640914917
time_elpased: 1.183
batch start
#iterations: 363
currently lose_sum: 363.90562295913696
time_elpased: 1.193
batch start
#iterations: 364
currently lose_sum: 360.2267280817032
time_elpased: 1.182
batch start
#iterations: 365
currently lose_sum: 361.8714040517807
time_elpased: 1.198
batch start
#iterations: 366
currently lose_sum: 362.97982382774353
time_elpased: 1.21
batch start
#iterations: 367
currently lose_sum: 362.6750720143318
time_elpased: 1.181
batch start
#iterations: 368
currently lose_sum: 360.12653732299805
time_elpased: 1.236
batch start
#iterations: 369
currently lose_sum: 359.87282371520996
time_elpased: 1.217
batch start
#iterations: 370
currently lose_sum: 361.0936939716339
time_elpased: 1.18
batch start
#iterations: 371
currently lose_sum: 361.87170976400375
time_elpased: 1.183
batch start
#iterations: 372
currently lose_sum: 361.8111270070076
time_elpased: 1.228
batch start
#iterations: 373
currently lose_sum: 361.7575847506523
time_elpased: 1.198
batch start
#iterations: 374
currently lose_sum: 362.4319089651108
time_elpased: 1.218
batch start
#iterations: 375
currently lose_sum: 360.0622922182083
time_elpased: 1.203
batch start
#iterations: 376
currently lose_sum: 361.47428780794144
time_elpased: 1.208
batch start
#iterations: 377
currently lose_sum: 362.53787726163864
time_elpased: 1.189
batch start
#iterations: 378
currently lose_sum: 360.91091936826706
time_elpased: 1.193
batch start
#iterations: 379
currently lose_sum: 360.952473282814
time_elpased: 1.2
start validation test
0.755979381443
1.0
0.755979381443
0.861034462514
0
validation finish
batch start
#iterations: 380
currently lose_sum: 363.1295939683914
time_elpased: 1.189
batch start
#iterations: 381
currently lose_sum: 363.13176822662354
time_elpased: 1.21
batch start
#iterations: 382
currently lose_sum: 360.8531178832054
time_elpased: 1.205
batch start
#iterations: 383
currently lose_sum: 361.39384484291077
time_elpased: 1.216
batch start
#iterations: 384
currently lose_sum: 360.75070226192474
time_elpased: 1.224
batch start
#iterations: 385
currently lose_sum: 360.02046447992325
time_elpased: 1.253
batch start
#iterations: 386
currently lose_sum: 363.1021457314491
time_elpased: 1.21
batch start
#iterations: 387
currently lose_sum: 360.6064270734787
time_elpased: 1.188
batch start
#iterations: 388
currently lose_sum: 361.22304713726044
time_elpased: 1.194
batch start
#iterations: 389
currently lose_sum: 360.8011848330498
time_elpased: 1.187
batch start
#iterations: 390
currently lose_sum: 360.24915277957916
time_elpased: 1.217
batch start
#iterations: 391
currently lose_sum: 360.6630262732506
time_elpased: 1.187
batch start
#iterations: 392
currently lose_sum: 361.4451570510864
time_elpased: 1.185
batch start
#iterations: 393
currently lose_sum: 361.8475649356842
time_elpased: 1.208
batch start
#iterations: 394
currently lose_sum: 361.6537075638771
time_elpased: 1.204
batch start
#iterations: 395
currently lose_sum: 361.1298637986183
time_elpased: 1.2
batch start
#iterations: 396
currently lose_sum: 361.9824349284172
time_elpased: 1.171
batch start
#iterations: 397
currently lose_sum: 360.3167533278465
time_elpased: 1.211
batch start
#iterations: 398
currently lose_sum: 361.61964255571365
time_elpased: 1.212
batch start
#iterations: 399
currently lose_sum: 360.28449189662933
time_elpased: 1.293
start validation test
0.758041237113
1.0
0.758041237113
0.862370257433
0
validation finish
acc: 0.751
pre: 1.000
rec: 0.751
F1: 0.858
auc: 0.000
