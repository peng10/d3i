start to construct graph
graph construct over
58300
epochs start
batch start
#iterations: 0
currently lose_sum: 398.23448634147644
time_elpased: 1.1
batch start
#iterations: 1
currently lose_sum: 389.45688050985336
time_elpased: 1.063
batch start
#iterations: 2
currently lose_sum: 384.28739804029465
time_elpased: 1.061
batch start
#iterations: 3
currently lose_sum: 381.18829786777496
time_elpased: 1.067
batch start
#iterations: 4
currently lose_sum: 380.04795986413956
time_elpased: 1.064
batch start
#iterations: 5
currently lose_sum: 377.2601258158684
time_elpased: 1.07
batch start
#iterations: 6
currently lose_sum: 376.0815633535385
time_elpased: 1.071
batch start
#iterations: 7
currently lose_sum: 373.9221000671387
time_elpased: 1.088
batch start
#iterations: 8
currently lose_sum: 374.44158524274826
time_elpased: 1.068
batch start
#iterations: 9
currently lose_sum: 374.1493511199951
time_elpased: 1.067
batch start
#iterations: 10
currently lose_sum: 372.7143483757973
time_elpased: 1.071
batch start
#iterations: 11
currently lose_sum: 371.73134899139404
time_elpased: 1.074
batch start
#iterations: 12
currently lose_sum: 370.9976485967636
time_elpased: 1.064
batch start
#iterations: 13
currently lose_sum: 371.3061618208885
time_elpased: 1.07
batch start
#iterations: 14
currently lose_sum: 370.9471068382263
time_elpased: 1.072
batch start
#iterations: 15
currently lose_sum: 369.5908758044243
time_elpased: 1.072
batch start
#iterations: 16
currently lose_sum: 370.2952376008034
time_elpased: 1.071
batch start
#iterations: 17
currently lose_sum: 370.26598566770554
time_elpased: 1.07
batch start
#iterations: 18
currently lose_sum: 369.19944900274277
time_elpased: 1.074
batch start
#iterations: 19
currently lose_sum: 368.35675871372223
time_elpased: 1.064
start validation test
0.643505154639
1.0
0.643505154639
0.783088696525
0
validation finish
batch start
#iterations: 20
currently lose_sum: 368.6594315767288
time_elpased: 1.077
batch start
#iterations: 21
currently lose_sum: 368.952957034111
time_elpased: 1.075
batch start
#iterations: 22
currently lose_sum: 368.20237761735916
time_elpased: 1.088
batch start
#iterations: 23
currently lose_sum: 369.56127083301544
time_elpased: 1.07
batch start
#iterations: 24
currently lose_sum: 368.4880648255348
time_elpased: 1.075
batch start
#iterations: 25
currently lose_sum: 369.0242804288864
time_elpased: 1.067
batch start
#iterations: 26
currently lose_sum: 367.00708454847336
time_elpased: 1.067
batch start
#iterations: 27
currently lose_sum: 366.84320574998856
time_elpased: 1.08
batch start
#iterations: 28
currently lose_sum: 367.61518812179565
time_elpased: 1.08
batch start
#iterations: 29
currently lose_sum: 366.61459743976593
time_elpased: 1.082
batch start
#iterations: 30
currently lose_sum: 369.07106351852417
time_elpased: 1.065
batch start
#iterations: 31
currently lose_sum: 367.316818356514
time_elpased: 1.064
batch start
#iterations: 32
currently lose_sum: 366.07327085733414
time_elpased: 1.07
batch start
#iterations: 33
currently lose_sum: 367.2761214375496
time_elpased: 1.066
batch start
#iterations: 34
currently lose_sum: 366.3293301463127
time_elpased: 1.064
batch start
#iterations: 35
currently lose_sum: 366.74136155843735
time_elpased: 1.06
batch start
#iterations: 36
currently lose_sum: 368.07520616054535
time_elpased: 1.061
batch start
#iterations: 37
currently lose_sum: 366.27854758501053
time_elpased: 1.073
batch start
#iterations: 38
currently lose_sum: 367.3886511325836
time_elpased: 1.064
batch start
#iterations: 39
currently lose_sum: 366.44784927368164
time_elpased: 1.069
start validation test
0.694742268041
1.0
0.694742268041
0.819879554717
0
validation finish
batch start
#iterations: 40
currently lose_sum: 366.1947208046913
time_elpased: 1.084
batch start
#iterations: 41
currently lose_sum: 365.84863620996475
time_elpased: 1.078
batch start
#iterations: 42
currently lose_sum: 364.66020089387894
time_elpased: 1.07
batch start
#iterations: 43
currently lose_sum: 366.18815076351166
time_elpased: 1.069
batch start
#iterations: 44
currently lose_sum: 367.27573466300964
time_elpased: 1.056
batch start
#iterations: 45
currently lose_sum: 366.3199241757393
time_elpased: 1.054
batch start
#iterations: 46
currently lose_sum: 363.49909323453903
time_elpased: 1.07
batch start
#iterations: 47
currently lose_sum: 365.8772897720337
time_elpased: 1.074
batch start
#iterations: 48
currently lose_sum: 365.51733285188675
time_elpased: 1.073
batch start
#iterations: 49
currently lose_sum: 366.6594384908676
time_elpased: 1.068
batch start
#iterations: 50
currently lose_sum: 366.1929460167885
time_elpased: 1.072
batch start
#iterations: 51
currently lose_sum: 364.3584154844284
time_elpased: 1.073
batch start
#iterations: 52
currently lose_sum: 364.60857778787613
time_elpased: 1.068
batch start
#iterations: 53
currently lose_sum: 365.296334028244
time_elpased: 1.063
batch start
#iterations: 54
currently lose_sum: 364.63897240161896
time_elpased: 1.077
batch start
#iterations: 55
currently lose_sum: 365.45133394002914
time_elpased: 1.064
batch start
#iterations: 56
currently lose_sum: 364.8047640323639
time_elpased: 1.072
batch start
#iterations: 57
currently lose_sum: 366.2768926024437
time_elpased: 1.068
batch start
#iterations: 58
currently lose_sum: 367.17781031131744
time_elpased: 1.082
batch start
#iterations: 59
currently lose_sum: 365.59641230106354
time_elpased: 1.088
start validation test
0.71824742268
1.0
0.71824742268
0.836023279534
0
validation finish
batch start
#iterations: 60
currently lose_sum: 365.677498459816
time_elpased: 1.079
batch start
#iterations: 61
currently lose_sum: 364.75196504592896
time_elpased: 1.074
batch start
#iterations: 62
currently lose_sum: 364.94979870319366
time_elpased: 1.071
batch start
#iterations: 63
currently lose_sum: 365.83291786909103
time_elpased: 1.072
batch start
#iterations: 64
currently lose_sum: 365.7985523343086
time_elpased: 1.066
batch start
#iterations: 65
currently lose_sum: 363.9943116903305
time_elpased: 1.074
batch start
#iterations: 66
currently lose_sum: 365.2035169005394
time_elpased: 1.06
batch start
#iterations: 67
currently lose_sum: 365.27251249551773
time_elpased: 1.085
batch start
#iterations: 68
currently lose_sum: 364.5153141617775
time_elpased: 1.076
batch start
#iterations: 69
currently lose_sum: 364.6233206987381
time_elpased: 1.079
batch start
#iterations: 70
currently lose_sum: 363.82835978269577
time_elpased: 1.076
batch start
#iterations: 71
currently lose_sum: 363.1547266840935
time_elpased: 1.084
batch start
#iterations: 72
currently lose_sum: 364.9707912802696
time_elpased: 1.08
batch start
#iterations: 73
currently lose_sum: 365.3402987718582
time_elpased: 1.067
batch start
#iterations: 74
currently lose_sum: 364.8516531586647
time_elpased: 1.071
batch start
#iterations: 75
currently lose_sum: 363.7768644094467
time_elpased: 1.072
batch start
#iterations: 76
currently lose_sum: 363.9596346616745
time_elpased: 1.068
batch start
#iterations: 77
currently lose_sum: 364.29909694194794
time_elpased: 1.07
batch start
#iterations: 78
currently lose_sum: 363.60174036026
time_elpased: 1.079
batch start
#iterations: 79
currently lose_sum: 365.1186903119087
time_elpased: 1.068
start validation test
0.72206185567
1.0
0.72206185567
0.838601532567
0
validation finish
batch start
#iterations: 80
currently lose_sum: 363.7201020717621
time_elpased: 1.085
batch start
#iterations: 81
currently lose_sum: 365.0551411509514
time_elpased: 1.081
batch start
#iterations: 82
currently lose_sum: 364.76398718357086
time_elpased: 1.09
batch start
#iterations: 83
currently lose_sum: 363.47640895843506
time_elpased: 1.067
batch start
#iterations: 84
currently lose_sum: 363.10506641864777
time_elpased: 1.076
batch start
#iterations: 85
currently lose_sum: 363.7444820404053
time_elpased: 1.066
batch start
#iterations: 86
currently lose_sum: 363.1129593849182
time_elpased: 1.071
batch start
#iterations: 87
currently lose_sum: 363.7377369403839
time_elpased: 1.064
batch start
#iterations: 88
currently lose_sum: 363.25128549337387
time_elpased: 1.063
batch start
#iterations: 89
currently lose_sum: 362.51079857349396
time_elpased: 1.075
batch start
#iterations: 90
currently lose_sum: 362.99073255062103
time_elpased: 1.074
batch start
#iterations: 91
currently lose_sum: 363.4789218902588
time_elpased: 1.063
batch start
#iterations: 92
currently lose_sum: 363.8602754473686
time_elpased: 1.074
batch start
#iterations: 93
currently lose_sum: 365.84097760915756
time_elpased: 1.078
batch start
#iterations: 94
currently lose_sum: 363.4550677537918
time_elpased: 1.064
batch start
#iterations: 95
currently lose_sum: 362.69768649339676
time_elpased: 1.074
batch start
#iterations: 96
currently lose_sum: 364.2071378827095
time_elpased: 1.077
batch start
#iterations: 97
currently lose_sum: 362.9415621161461
time_elpased: 1.081
batch start
#iterations: 98
currently lose_sum: 363.32238644361496
time_elpased: 1.071
batch start
#iterations: 99
currently lose_sum: 364.30436462163925
time_elpased: 1.071
start validation test
0.723711340206
1.0
0.723711340206
0.83971291866
0
validation finish
batch start
#iterations: 100
currently lose_sum: 363.15295374393463
time_elpased: 1.062
batch start
#iterations: 101
currently lose_sum: 363.3442384004593
time_elpased: 1.072
batch start
#iterations: 102
currently lose_sum: 363.1018476486206
time_elpased: 1.082
batch start
#iterations: 103
currently lose_sum: 362.9788786172867
time_elpased: 1.059
batch start
#iterations: 104
currently lose_sum: 362.7905751466751
time_elpased: 1.077
batch start
#iterations: 105
currently lose_sum: 362.5470445752144
time_elpased: 1.057
batch start
#iterations: 106
currently lose_sum: 364.9834897518158
time_elpased: 1.065
batch start
#iterations: 107
currently lose_sum: 363.14620488882065
time_elpased: 1.077
batch start
#iterations: 108
currently lose_sum: 363.9245018362999
time_elpased: 1.062
batch start
#iterations: 109
currently lose_sum: 363.99644446372986
time_elpased: 1.062
batch start
#iterations: 110
currently lose_sum: 362.9507353901863
time_elpased: 1.083
batch start
#iterations: 111
currently lose_sum: 361.83824276924133
time_elpased: 1.083
batch start
#iterations: 112
currently lose_sum: 363.2277309894562
time_elpased: 1.07
batch start
#iterations: 113
currently lose_sum: 363.9574102163315
time_elpased: 1.088
batch start
#iterations: 114
currently lose_sum: 365.3320707678795
time_elpased: 1.058
batch start
#iterations: 115
currently lose_sum: 364.1509844660759
time_elpased: 1.068
batch start
#iterations: 116
currently lose_sum: 362.4492049217224
time_elpased: 1.074
batch start
#iterations: 117
currently lose_sum: 362.11258709430695
time_elpased: 1.064
batch start
#iterations: 118
currently lose_sum: 364.22835528850555
time_elpased: 1.077
batch start
#iterations: 119
currently lose_sum: 363.997188359499
time_elpased: 1.08
start validation test
0.72793814433
1.0
0.72793814433
0.842551160432
0
validation finish
batch start
#iterations: 120
currently lose_sum: 362.78153371810913
time_elpased: 1.069
batch start
#iterations: 121
currently lose_sum: 364.2600454092026
time_elpased: 1.07
batch start
#iterations: 122
currently lose_sum: 363.07465916872025
time_elpased: 1.071
batch start
#iterations: 123
currently lose_sum: 362.6321318745613
time_elpased: 1.068
batch start
#iterations: 124
currently lose_sum: 361.2055695056915
time_elpased: 1.074
batch start
#iterations: 125
currently lose_sum: 362.77656400203705
time_elpased: 1.064
batch start
#iterations: 126
currently lose_sum: 362.92740947008133
time_elpased: 1.078
batch start
#iterations: 127
currently lose_sum: 363.2569155097008
time_elpased: 1.064
batch start
#iterations: 128
currently lose_sum: 363.5706725716591
time_elpased: 1.073
batch start
#iterations: 129
currently lose_sum: 362.10968774557114
time_elpased: 1.079
batch start
#iterations: 130
currently lose_sum: 363.56284672021866
time_elpased: 1.078
batch start
#iterations: 131
currently lose_sum: 362.00412970781326
time_elpased: 1.07
batch start
#iterations: 132
currently lose_sum: 364.2809941768646
time_elpased: 1.065
batch start
#iterations: 133
currently lose_sum: 362.32619774341583
time_elpased: 1.068
batch start
#iterations: 134
currently lose_sum: 362.6404137313366
time_elpased: 1.065
batch start
#iterations: 135
currently lose_sum: 363.45393884181976
time_elpased: 1.06
batch start
#iterations: 136
currently lose_sum: 363.5027219057083
time_elpased: 1.065
batch start
#iterations: 137
currently lose_sum: 362.0796640217304
time_elpased: 1.078
batch start
#iterations: 138
currently lose_sum: 363.0061622262001
time_elpased: 1.073
batch start
#iterations: 139
currently lose_sum: 361.7269086241722
time_elpased: 1.066
start validation test
0.71824742268
1.0
0.71824742268
0.836023279534
0
validation finish
batch start
#iterations: 140
currently lose_sum: 363.05986228585243
time_elpased: 1.079
batch start
#iterations: 141
currently lose_sum: 361.82106387615204
time_elpased: 1.069
batch start
#iterations: 142
currently lose_sum: 362.8751303553581
time_elpased: 1.074
batch start
#iterations: 143
currently lose_sum: 362.58990359306335
time_elpased: 1.059
batch start
#iterations: 144
currently lose_sum: 361.90524101257324
time_elpased: 1.071
batch start
#iterations: 145
currently lose_sum: 362.52712881565094
time_elpased: 1.071
batch start
#iterations: 146
currently lose_sum: 361.26881897449493
time_elpased: 1.071
batch start
#iterations: 147
currently lose_sum: 362.7495033144951
time_elpased: 1.074
batch start
#iterations: 148
currently lose_sum: 361.4305534362793
time_elpased: 1.071
batch start
#iterations: 149
currently lose_sum: 361.2233902812004
time_elpased: 1.067
batch start
#iterations: 150
currently lose_sum: 362.5656068325043
time_elpased: 1.069
batch start
#iterations: 151
currently lose_sum: 363.19903790950775
time_elpased: 1.067
batch start
#iterations: 152
currently lose_sum: 363.1214528679848
time_elpased: 1.08
batch start
#iterations: 153
currently lose_sum: 363.18772608041763
time_elpased: 1.073
batch start
#iterations: 154
currently lose_sum: 361.30923545360565
time_elpased: 1.062
batch start
#iterations: 155
currently lose_sum: 363.0881749391556
time_elpased: 1.078
batch start
#iterations: 156
currently lose_sum: 361.1720846891403
time_elpased: 1.069
batch start
#iterations: 157
currently lose_sum: 362.10317105054855
time_elpased: 1.073
batch start
#iterations: 158
currently lose_sum: 361.89054268598557
time_elpased: 1.08
batch start
#iterations: 159
currently lose_sum: 360.80324882268906
time_elpased: 1.07
start validation test
0.745773195876
1.0
0.745773195876
0.854375811976
0
validation finish
batch start
#iterations: 160
currently lose_sum: 362.7021336555481
time_elpased: 1.074
batch start
#iterations: 161
currently lose_sum: 363.88008975982666
time_elpased: 1.083
batch start
#iterations: 162
currently lose_sum: 361.5994525551796
time_elpased: 1.081
batch start
#iterations: 163
currently lose_sum: 361.79946732521057
time_elpased: 1.071
batch start
#iterations: 164
currently lose_sum: 361.5297078490257
time_elpased: 1.078
batch start
#iterations: 165
currently lose_sum: 363.62800627946854
time_elpased: 1.061
batch start
#iterations: 166
currently lose_sum: 362.51875245571136
time_elpased: 1.075
batch start
#iterations: 167
currently lose_sum: 361.7574737071991
time_elpased: 1.071
batch start
#iterations: 168
currently lose_sum: 361.26869559288025
time_elpased: 1.069
batch start
#iterations: 169
currently lose_sum: 362.20906549692154
time_elpased: 1.076
batch start
#iterations: 170
currently lose_sum: 362.7866249680519
time_elpased: 1.07
batch start
#iterations: 171
currently lose_sum: 363.041483104229
time_elpased: 1.07
batch start
#iterations: 172
currently lose_sum: 363.2883046269417
time_elpased: 1.061
batch start
#iterations: 173
currently lose_sum: 360.8533504605293
time_elpased: 1.072
batch start
#iterations: 174
currently lose_sum: 362.8676793575287
time_elpased: 1.062
batch start
#iterations: 175
currently lose_sum: 361.6064457297325
time_elpased: 1.069
batch start
#iterations: 176
currently lose_sum: 361.51587384939194
time_elpased: 1.065
batch start
#iterations: 177
currently lose_sum: 362.4113292694092
time_elpased: 1.069
batch start
#iterations: 178
currently lose_sum: 364.1459181904793
time_elpased: 1.084
batch start
#iterations: 179
currently lose_sum: 363.27772265672684
time_elpased: 1.069
start validation test
0.731030927835
1.0
0.731030927835
0.844619141207
0
validation finish
batch start
#iterations: 180
currently lose_sum: 362.19490045309067
time_elpased: 1.075
batch start
#iterations: 181
currently lose_sum: 360.5901995897293
time_elpased: 1.078
batch start
#iterations: 182
currently lose_sum: 362.5974588394165
time_elpased: 1.074
batch start
#iterations: 183
currently lose_sum: 362.55858117341995
time_elpased: 1.062
batch start
#iterations: 184
currently lose_sum: 361.5016442537308
time_elpased: 1.073
batch start
#iterations: 185
currently lose_sum: 362.2401793003082
time_elpased: 1.076
batch start
#iterations: 186
currently lose_sum: 362.67742013931274
time_elpased: 1.069
batch start
#iterations: 187
currently lose_sum: 361.6709608435631
time_elpased: 1.067
batch start
#iterations: 188
currently lose_sum: 361.5564401745796
time_elpased: 1.077
batch start
#iterations: 189
currently lose_sum: 362.37473714351654
time_elpased: 1.067
batch start
#iterations: 190
currently lose_sum: 363.2575942873955
time_elpased: 1.088
batch start
#iterations: 191
currently lose_sum: 362.1729600429535
time_elpased: 1.063
batch start
#iterations: 192
currently lose_sum: 363.48999071121216
time_elpased: 1.06
batch start
#iterations: 193
currently lose_sum: 362.7641050517559
time_elpased: 1.073
batch start
#iterations: 194
currently lose_sum: 363.16277611255646
time_elpased: 1.067
batch start
#iterations: 195
currently lose_sum: 362.03368294239044
time_elpased: 1.066
batch start
#iterations: 196
currently lose_sum: 364.58415365219116
time_elpased: 1.074
batch start
#iterations: 197
currently lose_sum: 361.34095591306686
time_elpased: 1.073
batch start
#iterations: 198
currently lose_sum: 361.06747007369995
time_elpased: 1.063
batch start
#iterations: 199
currently lose_sum: 362.08737230300903
time_elpased: 1.07
start validation test
0.725051546392
1.0
0.725051546392
0.840614354868
0
validation finish
batch start
#iterations: 200
currently lose_sum: 361.11085653305054
time_elpased: 1.075
batch start
#iterations: 201
currently lose_sum: 361.7962032854557
time_elpased: 1.067
batch start
#iterations: 202
currently lose_sum: 361.98032546043396
time_elpased: 1.064
batch start
#iterations: 203
currently lose_sum: 361.3778406381607
time_elpased: 1.075
batch start
#iterations: 204
currently lose_sum: 362.2813259959221
time_elpased: 1.066
batch start
#iterations: 205
currently lose_sum: 362.5550153851509
time_elpased: 1.066
batch start
#iterations: 206
currently lose_sum: 361.12456595897675
time_elpased: 1.065
batch start
#iterations: 207
currently lose_sum: 362.0110505223274
time_elpased: 1.067
batch start
#iterations: 208
currently lose_sum: 361.5802388191223
time_elpased: 1.07
batch start
#iterations: 209
currently lose_sum: 362.3884584903717
time_elpased: 1.077
batch start
#iterations: 210
currently lose_sum: 362.0158087015152
time_elpased: 1.07
batch start
#iterations: 211
currently lose_sum: 362.74128699302673
time_elpased: 1.078
batch start
#iterations: 212
currently lose_sum: 362.7385722398758
time_elpased: 1.069
batch start
#iterations: 213
currently lose_sum: 361.01399356126785
time_elpased: 1.062
batch start
#iterations: 214
currently lose_sum: 362.00080847740173
time_elpased: 1.078
batch start
#iterations: 215
currently lose_sum: 362.33242750167847
time_elpased: 1.101
batch start
#iterations: 216
currently lose_sum: 363.22182017564774
time_elpased: 1.063
batch start
#iterations: 217
currently lose_sum: 360.70283019542694
time_elpased: 1.061
batch start
#iterations: 218
currently lose_sum: 361.49814480543137
time_elpased: 1.089
batch start
#iterations: 219
currently lose_sum: 363.45629209280014
time_elpased: 1.088
start validation test
0.728865979381
1.0
0.728865979381
0.843172331544
0
validation finish
batch start
#iterations: 220
currently lose_sum: 362.45021772384644
time_elpased: 1.069
batch start
#iterations: 221
currently lose_sum: 360.25490576028824
time_elpased: 1.07
batch start
#iterations: 222
currently lose_sum: 361.8635351061821
time_elpased: 1.072
batch start
#iterations: 223
currently lose_sum: 362.0358783006668
time_elpased: 1.07
batch start
#iterations: 224
currently lose_sum: 362.50480711460114
time_elpased: 1.067
batch start
#iterations: 225
currently lose_sum: 360.4580520093441
time_elpased: 1.071
batch start
#iterations: 226
currently lose_sum: 360.5993461608887
time_elpased: 1.069
batch start
#iterations: 227
currently lose_sum: 361.90147733688354
time_elpased: 1.083
batch start
#iterations: 228
currently lose_sum: 362.821745634079
time_elpased: 1.072
batch start
#iterations: 229
currently lose_sum: 359.78265219926834
time_elpased: 1.076
batch start
#iterations: 230
currently lose_sum: 361.5490216612816
time_elpased: 1.068
batch start
#iterations: 231
currently lose_sum: 362.3627382516861
time_elpased: 1.079
batch start
#iterations: 232
currently lose_sum: 362.2129809856415
time_elpased: 1.063
batch start
#iterations: 233
currently lose_sum: 361.4977795481682
time_elpased: 1.089
batch start
#iterations: 234
currently lose_sum: 362.8746046423912
time_elpased: 1.067
batch start
#iterations: 235
currently lose_sum: 361.48795187473297
time_elpased: 1.069
batch start
#iterations: 236
currently lose_sum: 362.26672065258026
time_elpased: 1.07
batch start
#iterations: 237
currently lose_sum: 362.107709646225
time_elpased: 1.076
batch start
#iterations: 238
currently lose_sum: 361.6024411916733
time_elpased: 1.071
batch start
#iterations: 239
currently lose_sum: 360.895554959774
time_elpased: 1.061
start validation test
0.721649484536
1.0
0.721649484536
0.838323353293
0
validation finish
batch start
#iterations: 240
currently lose_sum: 362.6958307027817
time_elpased: 1.075
batch start
#iterations: 241
currently lose_sum: 362.75043028593063
time_elpased: 1.066
batch start
#iterations: 242
currently lose_sum: 363.16971349716187
time_elpased: 1.071
batch start
#iterations: 243
currently lose_sum: 362.42799282073975
time_elpased: 1.081
batch start
#iterations: 244
currently lose_sum: 361.7364816069603
time_elpased: 1.081
batch start
#iterations: 245
currently lose_sum: 362.35738068819046
time_elpased: 1.078
batch start
#iterations: 246
currently lose_sum: 361.9844873547554
time_elpased: 1.077
batch start
#iterations: 247
currently lose_sum: 360.2570136785507
time_elpased: 1.065
batch start
#iterations: 248
currently lose_sum: 361.51683551073074
time_elpased: 1.071
batch start
#iterations: 249
currently lose_sum: 360.7325007915497
time_elpased: 1.066
batch start
#iterations: 250
currently lose_sum: 362.2490524947643
time_elpased: 1.078
batch start
#iterations: 251
currently lose_sum: 361.60726737976074
time_elpased: 1.079
batch start
#iterations: 252
currently lose_sum: 361.0966143012047
time_elpased: 1.073
batch start
#iterations: 253
currently lose_sum: 361.60144579410553
time_elpased: 1.067
batch start
#iterations: 254
currently lose_sum: 362.20184391736984
time_elpased: 1.071
batch start
#iterations: 255
currently lose_sum: 361.75996828079224
time_elpased: 1.066
batch start
#iterations: 256
currently lose_sum: 361.1150150299072
time_elpased: 1.066
batch start
#iterations: 257
currently lose_sum: 361.6131392121315
time_elpased: 1.059
batch start
#iterations: 258
currently lose_sum: 360.70052576065063
time_elpased: 1.061
batch start
#iterations: 259
currently lose_sum: 361.7126144170761
time_elpased: 1.068
start validation test
0.73206185567
1.0
0.73206185567
0.845306826975
0
validation finish
batch start
#iterations: 260
currently lose_sum: 361.88312911987305
time_elpased: 1.07
batch start
#iterations: 261
currently lose_sum: 361.6643993258476
time_elpased: 1.072
batch start
#iterations: 262
currently lose_sum: 360.4393656849861
time_elpased: 1.079
batch start
#iterations: 263
currently lose_sum: 361.8822905421257
time_elpased: 1.066
batch start
#iterations: 264
currently lose_sum: 362.3736025094986
time_elpased: 1.068
batch start
#iterations: 265
currently lose_sum: 362.1069603264332
time_elpased: 1.07
batch start
#iterations: 266
currently lose_sum: 361.1671221256256
time_elpased: 1.066
batch start
#iterations: 267
currently lose_sum: 361.46154910326004
time_elpased: 1.074
batch start
#iterations: 268
currently lose_sum: 361.8572377562523
time_elpased: 1.071
batch start
#iterations: 269
currently lose_sum: 362.15411403775215
time_elpased: 1.062
batch start
#iterations: 270
currently lose_sum: 362.2858290672302
time_elpased: 1.07
batch start
#iterations: 271
currently lose_sum: 361.1126126050949
time_elpased: 1.083
batch start
#iterations: 272
currently lose_sum: 361.5963149666786
time_elpased: 1.075
batch start
#iterations: 273
currently lose_sum: 361.4175955057144
time_elpased: 1.074
batch start
#iterations: 274
currently lose_sum: 361.83689963817596
time_elpased: 1.079
batch start
#iterations: 275
currently lose_sum: 362.0588537454605
time_elpased: 1.076
batch start
#iterations: 276
currently lose_sum: 362.4359467625618
time_elpased: 1.062
batch start
#iterations: 277
currently lose_sum: 361.35124891996384
time_elpased: 1.077
batch start
#iterations: 278
currently lose_sum: 363.3255085349083
time_elpased: 1.082
batch start
#iterations: 279
currently lose_sum: 360.6654731631279
time_elpased: 1.08
start validation test
0.725463917526
1.0
0.725463917526
0.840891438131
0
validation finish
batch start
#iterations: 280
currently lose_sum: 361.63267743587494
time_elpased: 1.076
batch start
#iterations: 281
currently lose_sum: 361.6330227255821
time_elpased: 1.078
batch start
#iterations: 282
currently lose_sum: 361.86913764476776
time_elpased: 1.073
batch start
#iterations: 283
currently lose_sum: 361.7557896375656
time_elpased: 1.072
batch start
#iterations: 284
currently lose_sum: 362.3208095431328
time_elpased: 1.077
batch start
#iterations: 285
currently lose_sum: 361.22919076681137
time_elpased: 1.077
batch start
#iterations: 286
currently lose_sum: 361.08868622779846
time_elpased: 1.06
batch start
#iterations: 287
currently lose_sum: 362.412723839283
time_elpased: 1.069
batch start
#iterations: 288
currently lose_sum: 360.8320302963257
time_elpased: 1.062
batch start
#iterations: 289
currently lose_sum: 359.7308398485184
time_elpased: 1.073
batch start
#iterations: 290
currently lose_sum: 360.5812289118767
time_elpased: 1.083
batch start
#iterations: 291
currently lose_sum: 362.0518335700035
time_elpased: 1.062
batch start
#iterations: 292
currently lose_sum: 362.33164501190186
time_elpased: 1.064
batch start
#iterations: 293
currently lose_sum: 361.98488879203796
time_elpased: 1.073
batch start
#iterations: 294
currently lose_sum: 360.7233428955078
time_elpased: 1.075
batch start
#iterations: 295
currently lose_sum: 362.9035573005676
time_elpased: 1.072
batch start
#iterations: 296
currently lose_sum: 360.9330840110779
time_elpased: 1.067
batch start
#iterations: 297
currently lose_sum: 362.53468269109726
time_elpased: 1.069
batch start
#iterations: 298
currently lose_sum: 361.1746621131897
time_elpased: 1.069
batch start
#iterations: 299
currently lose_sum: 360.1067705154419
time_elpased: 1.07
start validation test
0.731649484536
1.0
0.731649484536
0.845031850926
0
validation finish
batch start
#iterations: 300
currently lose_sum: 361.1064196228981
time_elpased: 1.082
batch start
#iterations: 301
currently lose_sum: 360.516926407814
time_elpased: 1.071
batch start
#iterations: 302
currently lose_sum: 360.81884229183197
time_elpased: 1.065
batch start
#iterations: 303
currently lose_sum: 362.46226793527603
time_elpased: 1.072
batch start
#iterations: 304
currently lose_sum: 362.217747092247
time_elpased: 1.07
batch start
#iterations: 305
currently lose_sum: 361.1007142663002
time_elpased: 1.064
batch start
#iterations: 306
currently lose_sum: 360.97390484809875
time_elpased: 1.07
batch start
#iterations: 307
currently lose_sum: 361.10658091306686
time_elpased: 1.078
batch start
#iterations: 308
currently lose_sum: 360.9313979148865
time_elpased: 1.073
batch start
#iterations: 309
currently lose_sum: 360.64556753635406
time_elpased: 1.08
batch start
#iterations: 310
currently lose_sum: 362.4080556035042
time_elpased: 1.069
batch start
#iterations: 311
currently lose_sum: 361.3927025794983
time_elpased: 1.071
batch start
#iterations: 312
currently lose_sum: 360.4888935685158
time_elpased: 1.09
batch start
#iterations: 313
currently lose_sum: 361.43172013759613
time_elpased: 1.083
batch start
#iterations: 314
currently lose_sum: 360.814925968647
time_elpased: 1.075
batch start
#iterations: 315
currently lose_sum: 360.50514751672745
time_elpased: 1.071
batch start
#iterations: 316
currently lose_sum: 359.2808301448822
time_elpased: 1.078
batch start
#iterations: 317
currently lose_sum: 362.41566187143326
time_elpased: 1.074
batch start
#iterations: 318
currently lose_sum: 361.3722755908966
time_elpased: 1.072
batch start
#iterations: 319
currently lose_sum: 361.3581328392029
time_elpased: 1.07
start validation test
0.739381443299
1.0
0.739381443299
0.850165955429
0
validation finish
batch start
#iterations: 320
currently lose_sum: 360.0581239461899
time_elpased: 1.075
batch start
#iterations: 321
currently lose_sum: 362.11485546827316
time_elpased: 1.065
batch start
#iterations: 322
currently lose_sum: 360.6020283102989
time_elpased: 1.086
batch start
#iterations: 323
currently lose_sum: 361.32022631168365
time_elpased: 1.07
batch start
#iterations: 324
currently lose_sum: 361.0154641866684
time_elpased: 1.066
batch start
#iterations: 325
currently lose_sum: 361.9696099162102
time_elpased: 1.073
batch start
#iterations: 326
currently lose_sum: 361.917082965374
time_elpased: 1.078
batch start
#iterations: 327
currently lose_sum: 360.80021345615387
time_elpased: 1.075
batch start
#iterations: 328
currently lose_sum: 362.10610270500183
time_elpased: 1.073
batch start
#iterations: 329
currently lose_sum: 361.52519541978836
time_elpased: 1.069
batch start
#iterations: 330
currently lose_sum: 360.95706939697266
time_elpased: 1.079
batch start
#iterations: 331
currently lose_sum: 361.64248245954514
time_elpased: 1.088
batch start
#iterations: 332
currently lose_sum: 361.34087485074997
time_elpased: 1.072
batch start
#iterations: 333
currently lose_sum: 360.90287709236145
time_elpased: 1.076
batch start
#iterations: 334
currently lose_sum: 361.23528212308884
time_elpased: 1.08
batch start
#iterations: 335
currently lose_sum: 361.2276729941368
time_elpased: 1.076
batch start
#iterations: 336
currently lose_sum: 359.24939155578613
time_elpased: 1.085
batch start
#iterations: 337
currently lose_sum: 360.7638756632805
time_elpased: 1.074
batch start
#iterations: 338
currently lose_sum: 361.0449886918068
time_elpased: 1.084
batch start
#iterations: 339
currently lose_sum: 361.1365382671356
time_elpased: 1.059
start validation test
0.734432989691
1.0
0.734432989691
0.846885401807
0
validation finish
batch start
#iterations: 340
currently lose_sum: 362.8600521683693
time_elpased: 1.066
batch start
#iterations: 341
currently lose_sum: 361.1806088089943
time_elpased: 1.079
batch start
#iterations: 342
currently lose_sum: 361.88425213098526
time_elpased: 1.067
batch start
#iterations: 343
currently lose_sum: 360.85495311021805
time_elpased: 1.065
batch start
#iterations: 344
currently lose_sum: 361.0017883181572
time_elpased: 1.067
batch start
#iterations: 345
currently lose_sum: 361.9772450327873
time_elpased: 1.078
batch start
#iterations: 346
currently lose_sum: 360.3742880821228
time_elpased: 1.065
batch start
#iterations: 347
currently lose_sum: 360.714956343174
time_elpased: 1.079
batch start
#iterations: 348
currently lose_sum: 359.4338221549988
time_elpased: 1.066
batch start
#iterations: 349
currently lose_sum: 359.7397253513336
time_elpased: 1.079
batch start
#iterations: 350
currently lose_sum: 361.7172975540161
time_elpased: 1.068
batch start
#iterations: 351
currently lose_sum: 360.62067902088165
time_elpased: 1.075
batch start
#iterations: 352
currently lose_sum: 361.0139666199684
time_elpased: 1.076
batch start
#iterations: 353
currently lose_sum: 361.4415118098259
time_elpased: 1.087
batch start
#iterations: 354
currently lose_sum: 361.0813378691673
time_elpased: 1.064
batch start
#iterations: 355
currently lose_sum: 362.2733110189438
time_elpased: 1.075
batch start
#iterations: 356
currently lose_sum: 361.92185831069946
time_elpased: 1.076
batch start
#iterations: 357
currently lose_sum: 360.88838452100754
time_elpased: 1.068
batch start
#iterations: 358
currently lose_sum: 361.05848240852356
time_elpased: 1.074
batch start
#iterations: 359
currently lose_sum: 361.8208078742027
time_elpased: 1.068
start validation test
0.722164948454
1.0
0.722164948454
0.83867105657
0
validation finish
batch start
#iterations: 360
currently lose_sum: 362.0071847438812
time_elpased: 1.076
batch start
#iterations: 361
currently lose_sum: 361.96791395545006
time_elpased: 1.071
batch start
#iterations: 362
currently lose_sum: 361.34012228250504
time_elpased: 1.076
batch start
#iterations: 363
currently lose_sum: 361.23593628406525
time_elpased: 1.067
batch start
#iterations: 364
currently lose_sum: 361.4238520860672
time_elpased: 1.077
batch start
#iterations: 365
currently lose_sum: 361.0596762895584
time_elpased: 1.073
batch start
#iterations: 366
currently lose_sum: 361.523134291172
time_elpased: 1.071
batch start
#iterations: 367
currently lose_sum: 360.96065270900726
time_elpased: 1.08
batch start
#iterations: 368
currently lose_sum: 360.22081130743027
time_elpased: 1.071
batch start
#iterations: 369
currently lose_sum: 359.28527092933655
time_elpased: 1.062
batch start
#iterations: 370
currently lose_sum: 360.1271733045578
time_elpased: 1.065
batch start
#iterations: 371
currently lose_sum: 360.4325145483017
time_elpased: 1.078
batch start
#iterations: 372
currently lose_sum: 360.00096356868744
time_elpased: 1.066
batch start
#iterations: 373
currently lose_sum: 360.65581637620926
time_elpased: 1.072
batch start
#iterations: 374
currently lose_sum: 361.94670552015305
time_elpased: 1.088
batch start
#iterations: 375
currently lose_sum: 360.59684550762177
time_elpased: 1.073
batch start
#iterations: 376
currently lose_sum: 360.5879111289978
time_elpased: 1.066
batch start
#iterations: 377
currently lose_sum: 361.3575397133827
time_elpased: 1.077
batch start
#iterations: 378
currently lose_sum: 359.63852939009666
time_elpased: 1.083
batch start
#iterations: 379
currently lose_sum: 362.0394335389137
time_elpased: 1.081
start validation test
0.734536082474
1.0
0.734536082474
0.846953937593
0
validation finish
batch start
#iterations: 380
currently lose_sum: 361.47497618198395
time_elpased: 1.085
batch start
#iterations: 381
currently lose_sum: 359.8309141397476
time_elpased: 1.07
batch start
#iterations: 382
currently lose_sum: 360.5118595957756
time_elpased: 1.086
batch start
#iterations: 383
currently lose_sum: 360.1605888605118
time_elpased: 1.069
batch start
#iterations: 384
currently lose_sum: 359.24477380514145
time_elpased: 1.069
batch start
#iterations: 385
currently lose_sum: 361.3292972445488
time_elpased: 1.07
batch start
#iterations: 386
currently lose_sum: 359.70723098516464
time_elpased: 1.072
batch start
#iterations: 387
currently lose_sum: 360.1273406147957
time_elpased: 1.079
batch start
#iterations: 388
currently lose_sum: 361.19209426641464
time_elpased: 1.077
batch start
#iterations: 389
currently lose_sum: 362.4014578461647
time_elpased: 1.087
batch start
#iterations: 390
currently lose_sum: 360.02084827423096
time_elpased: 1.093
batch start
#iterations: 391
currently lose_sum: 360.62042570114136
time_elpased: 1.078
batch start
#iterations: 392
currently lose_sum: 361.7350099682808
time_elpased: 1.072
batch start
#iterations: 393
currently lose_sum: 359.5036122202873
time_elpased: 1.073
batch start
#iterations: 394
currently lose_sum: 361.2313912510872
time_elpased: 1.067
batch start
#iterations: 395
currently lose_sum: 360.27289551496506
time_elpased: 1.073
batch start
#iterations: 396
currently lose_sum: 360.4286643266678
time_elpased: 1.077
batch start
#iterations: 397
currently lose_sum: 361.6655184030533
time_elpased: 1.069
batch start
#iterations: 398
currently lose_sum: 361.0034713745117
time_elpased: 1.07
batch start
#iterations: 399
currently lose_sum: 360.489688038826
time_elpased: 1.069
start validation test
0.734432989691
1.0
0.734432989691
0.846885401807
0
validation finish
acc: 0.752
pre: 1.000
rec: 0.752
F1: 0.858
auc: 0.000
