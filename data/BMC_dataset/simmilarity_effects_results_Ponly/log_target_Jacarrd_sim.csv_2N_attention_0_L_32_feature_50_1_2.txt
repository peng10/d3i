start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 558.4135290980339
time_elpased: 1.397
batch start
#iterations: 1
currently lose_sum: 541.7110722661018
time_elpased: 1.359
batch start
#iterations: 2
currently lose_sum: 536.2462945580482
time_elpased: 1.358
batch start
#iterations: 3
currently lose_sum: 533.0095946192741
time_elpased: 1.372
batch start
#iterations: 4
currently lose_sum: 529.8650241792202
time_elpased: 1.361
batch start
#iterations: 5
currently lose_sum: 531.2700187861919
time_elpased: 1.386
batch start
#iterations: 6
currently lose_sum: 527.1993745863438
time_elpased: 1.351
batch start
#iterations: 7
currently lose_sum: 527.9216123819351
time_elpased: 1.356
batch start
#iterations: 8
currently lose_sum: 526.663102209568
time_elpased: 1.359
batch start
#iterations: 9
currently lose_sum: 527.0895892679691
time_elpased: 1.357
batch start
#iterations: 10
currently lose_sum: 525.5231918990612
time_elpased: 1.368
batch start
#iterations: 11
currently lose_sum: 526.5522247552872
time_elpased: 1.373
batch start
#iterations: 12
currently lose_sum: 525.3379494845867
time_elpased: 1.357
batch start
#iterations: 13
currently lose_sum: 523.695086479187
time_elpased: 1.357
batch start
#iterations: 14
currently lose_sum: 524.7652223706245
time_elpased: 1.358
batch start
#iterations: 15
currently lose_sum: 524.6232313215733
time_elpased: 1.361
batch start
#iterations: 16
currently lose_sum: 524.3563939929008
time_elpased: 1.366
batch start
#iterations: 17
currently lose_sum: 523.9860860109329
time_elpased: 1.365
batch start
#iterations: 18
currently lose_sum: 524.1458345353603
time_elpased: 1.358
batch start
#iterations: 19
currently lose_sum: 524.4740991294384
time_elpased: 1.359
start validation test
0.151649484536
1.0
0.151649484536
0.263360486975
0
validation finish
batch start
#iterations: 20
currently lose_sum: 524.3140540421009
time_elpased: 1.37
batch start
#iterations: 21
currently lose_sum: 525.7038779258728
time_elpased: 1.364
batch start
#iterations: 22
currently lose_sum: 523.3630032539368
time_elpased: 1.364
batch start
#iterations: 23
currently lose_sum: 523.9646965563297
time_elpased: 1.371
batch start
#iterations: 24
currently lose_sum: 521.7715612947941
time_elpased: 1.361
batch start
#iterations: 25
currently lose_sum: 524.2621856331825
time_elpased: 1.365
batch start
#iterations: 26
currently lose_sum: 523.3887445628643
time_elpased: 1.342
batch start
#iterations: 27
currently lose_sum: 523.1234079897404
time_elpased: 1.376
batch start
#iterations: 28
currently lose_sum: 522.2244418859482
time_elpased: 1.357
batch start
#iterations: 29
currently lose_sum: 522.2352212667465
time_elpased: 1.374
batch start
#iterations: 30
currently lose_sum: 521.8270272910595
time_elpased: 1.364
batch start
#iterations: 31
currently lose_sum: 521.0373204946518
time_elpased: 1.364
batch start
#iterations: 32
currently lose_sum: 523.7673629522324
time_elpased: 1.365
batch start
#iterations: 33
currently lose_sum: 521.2689583599567
time_elpased: 1.363
batch start
#iterations: 34
currently lose_sum: 523.0474084615707
time_elpased: 1.361
batch start
#iterations: 35
currently lose_sum: 520.1401199698448
time_elpased: 1.355
batch start
#iterations: 36
currently lose_sum: 523.0195018351078
time_elpased: 1.359
batch start
#iterations: 37
currently lose_sum: 521.7716813981533
time_elpased: 1.362
batch start
#iterations: 38
currently lose_sum: 522.0646095275879
time_elpased: 1.353
batch start
#iterations: 39
currently lose_sum: 523.6132897138596
time_elpased: 1.345
start validation test
0.141958762887
1.0
0.141958762887
0.248623273449
0
validation finish
batch start
#iterations: 40
currently lose_sum: 523.373601347208
time_elpased: 1.362
batch start
#iterations: 41
currently lose_sum: 523.7644385099411
time_elpased: 1.371
batch start
#iterations: 42
currently lose_sum: 522.2121557593346
time_elpased: 1.363
batch start
#iterations: 43
currently lose_sum: 522.1588203012943
time_elpased: 1.357
batch start
#iterations: 44
currently lose_sum: 521.3814553618431
time_elpased: 1.369
batch start
#iterations: 45
currently lose_sum: 521.7381528317928
time_elpased: 1.364
batch start
#iterations: 46
currently lose_sum: 521.0381625294685
time_elpased: 1.378
batch start
#iterations: 47
currently lose_sum: 521.3506681621075
time_elpased: 1.358
batch start
#iterations: 48
currently lose_sum: 520.6904930472374
time_elpased: 1.369
batch start
#iterations: 49
currently lose_sum: 520.5886771082878
time_elpased: 1.353
batch start
#iterations: 50
currently lose_sum: 522.569728165865
time_elpased: 1.369
batch start
#iterations: 51
currently lose_sum: 521.3468842208385
time_elpased: 1.356
batch start
#iterations: 52
currently lose_sum: 521.2451633512974
time_elpased: 1.366
batch start
#iterations: 53
currently lose_sum: 522.8447172641754
time_elpased: 1.364
batch start
#iterations: 54
currently lose_sum: 520.6959913372993
time_elpased: 1.375
batch start
#iterations: 55
currently lose_sum: 522.6727905869484
time_elpased: 1.365
batch start
#iterations: 56
currently lose_sum: 521.7584989368916
time_elpased: 1.373
batch start
#iterations: 57
currently lose_sum: 519.6893958747387
time_elpased: 1.355
batch start
#iterations: 58
currently lose_sum: 522.6460361778736
time_elpased: 1.367
batch start
#iterations: 59
currently lose_sum: 519.9446247816086
time_elpased: 1.358
start validation test
0.163608247423
1.0
0.163608247423
0.281208469921
0
validation finish
batch start
#iterations: 60
currently lose_sum: 522.2273411154747
time_elpased: 1.361
batch start
#iterations: 61
currently lose_sum: 522.05852663517
time_elpased: 1.382
batch start
#iterations: 62
currently lose_sum: 523.0636391937733
time_elpased: 1.366
batch start
#iterations: 63
currently lose_sum: 522.7047030925751
time_elpased: 1.383
batch start
#iterations: 64
currently lose_sum: 522.0393889844418
time_elpased: 1.377
batch start
#iterations: 65
currently lose_sum: 522.9108780920506
time_elpased: 1.358
batch start
#iterations: 66
currently lose_sum: 521.1548417806625
time_elpased: 1.365
batch start
#iterations: 67
currently lose_sum: 519.317911863327
time_elpased: 1.364
batch start
#iterations: 68
currently lose_sum: 523.6296494305134
time_elpased: 1.38
batch start
#iterations: 69
currently lose_sum: 522.0910792052746
time_elpased: 1.367
batch start
#iterations: 70
currently lose_sum: 521.6802940964699
time_elpased: 1.359
batch start
#iterations: 71
currently lose_sum: 521.2720176875591
time_elpased: 1.372
batch start
#iterations: 72
currently lose_sum: 521.3416317105293
time_elpased: 1.378
batch start
#iterations: 73
currently lose_sum: 519.8746642768383
time_elpased: 1.368
batch start
#iterations: 74
currently lose_sum: 520.8530305624008
time_elpased: 1.372
batch start
#iterations: 75
currently lose_sum: 521.6052447855473
time_elpased: 1.368
batch start
#iterations: 76
currently lose_sum: 522.7620221674442
time_elpased: 1.368
batch start
#iterations: 77
currently lose_sum: 521.3172877132893
time_elpased: 1.37
batch start
#iterations: 78
currently lose_sum: 521.6307371258736
time_elpased: 1.376
batch start
#iterations: 79
currently lose_sum: 522.681984692812
time_elpased: 1.359
start validation test
0.182680412371
1.0
0.182680412371
0.308926080893
0
validation finish
batch start
#iterations: 80
currently lose_sum: 520.0556950569153
time_elpased: 1.361
batch start
#iterations: 81
currently lose_sum: 521.9484295547009
time_elpased: 1.371
batch start
#iterations: 82
currently lose_sum: 520.9769824743271
time_elpased: 1.352
batch start
#iterations: 83
currently lose_sum: 522.0092825293541
time_elpased: 1.36
batch start
#iterations: 84
currently lose_sum: 522.2329058051109
time_elpased: 1.352
batch start
#iterations: 85
currently lose_sum: 519.4188551008701
time_elpased: 1.368
batch start
#iterations: 86
currently lose_sum: 520.9265342354774
time_elpased: 1.378
batch start
#iterations: 87
currently lose_sum: 519.7123519480228
time_elpased: 1.368
batch start
#iterations: 88
currently lose_sum: 521.6120537519455
time_elpased: 1.36
batch start
#iterations: 89
currently lose_sum: 520.171027213335
time_elpased: 1.371
batch start
#iterations: 90
currently lose_sum: 521.9679965078831
time_elpased: 1.373
batch start
#iterations: 91
currently lose_sum: 519.5358173847198
time_elpased: 1.382
batch start
#iterations: 92
currently lose_sum: 521.7396342158318
time_elpased: 1.365
batch start
#iterations: 93
currently lose_sum: 521.3321633040905
time_elpased: 1.37
batch start
#iterations: 94
currently lose_sum: 519.9692888259888
time_elpased: 1.356
batch start
#iterations: 95
currently lose_sum: 519.6180562973022
time_elpased: 1.378
batch start
#iterations: 96
currently lose_sum: 521.8927892148495
time_elpased: 1.379
batch start
#iterations: 97
currently lose_sum: 519.7285851240158
time_elpased: 1.366
batch start
#iterations: 98
currently lose_sum: 521.296147942543
time_elpased: 1.378
batch start
#iterations: 99
currently lose_sum: 521.0861384272575
time_elpased: 1.374
start validation test
0.148453608247
1.0
0.148453608247
0.258527827648
0
validation finish
batch start
#iterations: 100
currently lose_sum: 521.6339254975319
time_elpased: 1.367
batch start
#iterations: 101
currently lose_sum: 520.4580010473728
time_elpased: 1.358
batch start
#iterations: 102
currently lose_sum: 519.1896596550941
time_elpased: 1.37
batch start
#iterations: 103
currently lose_sum: 520.6135036349297
time_elpased: 1.361
batch start
#iterations: 104
currently lose_sum: 520.3740755021572
time_elpased: 1.36
batch start
#iterations: 105
currently lose_sum: 520.5396216213703
time_elpased: 1.356
batch start
#iterations: 106
currently lose_sum: 521.7759051322937
time_elpased: 1.375
batch start
#iterations: 107
currently lose_sum: 520.7578327953815
time_elpased: 1.375
batch start
#iterations: 108
currently lose_sum: 518.5500347614288
time_elpased: 1.36
batch start
#iterations: 109
currently lose_sum: 521.2417549192905
time_elpased: 1.373
batch start
#iterations: 110
currently lose_sum: 520.7916570007801
time_elpased: 1.364
batch start
#iterations: 111
currently lose_sum: 522.9861989915371
time_elpased: 1.367
batch start
#iterations: 112
currently lose_sum: 519.1224921643734
time_elpased: 1.371
batch start
#iterations: 113
currently lose_sum: 520.9146422445774
time_elpased: 1.356
batch start
#iterations: 114
currently lose_sum: 519.90957903862
time_elpased: 1.376
batch start
#iterations: 115
currently lose_sum: 520.7138596773148
time_elpased: 1.37
batch start
#iterations: 116
currently lose_sum: 518.9539378583431
time_elpased: 1.356
batch start
#iterations: 117
currently lose_sum: 520.4931853413582
time_elpased: 1.375
batch start
#iterations: 118
currently lose_sum: 520.0244267880917
time_elpased: 1.369
batch start
#iterations: 119
currently lose_sum: 520.0142458975315
time_elpased: 1.358
start validation test
0.189793814433
1.0
0.189793814433
0.319036478641
0
validation finish
batch start
#iterations: 120
currently lose_sum: 521.2471244335175
time_elpased: 1.379
batch start
#iterations: 121
currently lose_sum: 519.2378278970718
time_elpased: 1.372
batch start
#iterations: 122
currently lose_sum: 518.640072286129
time_elpased: 1.372
batch start
#iterations: 123
currently lose_sum: 518.768904030323
time_elpased: 1.365
batch start
#iterations: 124
currently lose_sum: 519.3973625004292
time_elpased: 1.37
batch start
#iterations: 125
currently lose_sum: 520.5819548368454
time_elpased: 1.362
batch start
#iterations: 126
currently lose_sum: 519.7536818683147
time_elpased: 1.376
batch start
#iterations: 127
currently lose_sum: 521.1539344191551
time_elpased: 1.377
batch start
#iterations: 128
currently lose_sum: 519.4376237094402
time_elpased: 1.369
batch start
#iterations: 129
currently lose_sum: 520.1945219039917
time_elpased: 1.378
batch start
#iterations: 130
currently lose_sum: 520.1967247128487
time_elpased: 1.365
batch start
#iterations: 131
currently lose_sum: 520.2452383339405
time_elpased: 1.351
batch start
#iterations: 132
currently lose_sum: 522.0171318948269
time_elpased: 1.379
batch start
#iterations: 133
currently lose_sum: 521.3656381070614
time_elpased: 1.377
batch start
#iterations: 134
currently lose_sum: 520.7124740183353
time_elpased: 1.36
batch start
#iterations: 135
currently lose_sum: 518.1454105377197
time_elpased: 1.359
batch start
#iterations: 136
currently lose_sum: 519.8478603065014
time_elpased: 1.365
batch start
#iterations: 137
currently lose_sum: 518.8373610675335
time_elpased: 1.356
batch start
#iterations: 138
currently lose_sum: 520.9652805924416
time_elpased: 1.36
batch start
#iterations: 139
currently lose_sum: 521.7041622400284
time_elpased: 1.363
start validation test
0.151134020619
1.0
0.151134020619
0.262582840767
0
validation finish
batch start
#iterations: 140
currently lose_sum: 517.0472506582737
time_elpased: 1.365
batch start
#iterations: 141
currently lose_sum: 521.2448913753033
time_elpased: 1.364
batch start
#iterations: 142
currently lose_sum: 520.3526788055897
time_elpased: 1.37
batch start
#iterations: 143
currently lose_sum: 520.9081355929375
time_elpased: 1.368
batch start
#iterations: 144
currently lose_sum: 521.0858365893364
time_elpased: 1.372
batch start
#iterations: 145
currently lose_sum: 521.2885807752609
time_elpased: 1.366
batch start
#iterations: 146
currently lose_sum: 522.6197154223919
time_elpased: 1.373
batch start
#iterations: 147
currently lose_sum: 520.3562366366386
time_elpased: 1.378
batch start
#iterations: 148
currently lose_sum: 518.8696267306805
time_elpased: 1.366
batch start
#iterations: 149
currently lose_sum: 521.7689385414124
time_elpased: 1.391
batch start
#iterations: 150
currently lose_sum: 519.874916523695
time_elpased: 1.373
batch start
#iterations: 151
currently lose_sum: 519.6034769713879
time_elpased: 1.373
batch start
#iterations: 152
currently lose_sum: 520.4990323781967
time_elpased: 1.379
batch start
#iterations: 153
currently lose_sum: 516.999597966671
time_elpased: 1.364
batch start
#iterations: 154
currently lose_sum: 522.7225592434406
time_elpased: 1.371
batch start
#iterations: 155
currently lose_sum: 519.0662369132042
time_elpased: 1.358
batch start
#iterations: 156
currently lose_sum: 520.645680218935
time_elpased: 1.381
batch start
#iterations: 157
currently lose_sum: 519.745759755373
time_elpased: 1.372
batch start
#iterations: 158
currently lose_sum: 518.9752531349659
time_elpased: 1.361
batch start
#iterations: 159
currently lose_sum: 520.5148001015186
time_elpased: 1.381
start validation test
0.177525773196
1.0
0.177525773196
0.301523375941
0
validation finish
batch start
#iterations: 160
currently lose_sum: 520.3082731664181
time_elpased: 1.371
batch start
#iterations: 161
currently lose_sum: 520.4425031244755
time_elpased: 1.367
batch start
#iterations: 162
currently lose_sum: 519.5766624808311
time_elpased: 1.366
batch start
#iterations: 163
currently lose_sum: 517.1810468137264
time_elpased: 1.366
batch start
#iterations: 164
currently lose_sum: 522.0627909004688
time_elpased: 1.382
batch start
#iterations: 165
currently lose_sum: 518.7648508250713
time_elpased: 1.365
batch start
#iterations: 166
currently lose_sum: 519.7063809037209
time_elpased: 1.384
batch start
#iterations: 167
currently lose_sum: 521.1459038555622
time_elpased: 1.373
batch start
#iterations: 168
currently lose_sum: 520.4134875237942
time_elpased: 1.372
batch start
#iterations: 169
currently lose_sum: 522.3523199558258
time_elpased: 1.374
batch start
#iterations: 170
currently lose_sum: 519.4553510844707
time_elpased: 1.372
batch start
#iterations: 171
currently lose_sum: 519.447210252285
time_elpased: 1.357
batch start
#iterations: 172
currently lose_sum: 522.1719124615192
time_elpased: 1.372
batch start
#iterations: 173
currently lose_sum: 520.4259974658489
time_elpased: 1.387
batch start
#iterations: 174
currently lose_sum: 521.2852310538292
time_elpased: 1.365
batch start
#iterations: 175
currently lose_sum: 520.3096012175083
time_elpased: 1.373
batch start
#iterations: 176
currently lose_sum: 519.8398550450802
time_elpased: 1.367
batch start
#iterations: 177
currently lose_sum: 520.4444182515144
time_elpased: 1.37
batch start
#iterations: 178
currently lose_sum: 519.0571373105049
time_elpased: 1.371
batch start
#iterations: 179
currently lose_sum: 520.127282500267
time_elpased: 1.363
start validation test
0.172268041237
1.0
0.172268041237
0.293905549204
0
validation finish
batch start
#iterations: 180
currently lose_sum: 519.7272450327873
time_elpased: 1.371
batch start
#iterations: 181
currently lose_sum: 521.6683076024055
time_elpased: 1.371
batch start
#iterations: 182
currently lose_sum: 518.724123686552
time_elpased: 1.36
batch start
#iterations: 183
currently lose_sum: 520.3750723004341
time_elpased: 1.362
batch start
#iterations: 184
currently lose_sum: 520.098893404007
time_elpased: 1.376
batch start
#iterations: 185
currently lose_sum: 518.5686901509762
time_elpased: 1.36
batch start
#iterations: 186
currently lose_sum: 521.6276560723782
time_elpased: 1.39
batch start
#iterations: 187
currently lose_sum: 519.0411207079887
time_elpased: 1.366
batch start
#iterations: 188
currently lose_sum: 518.9020929038525
time_elpased: 1.38
batch start
#iterations: 189
currently lose_sum: 519.2077826559544
time_elpased: 1.367
batch start
#iterations: 190
currently lose_sum: 517.6016780734062
time_elpased: 1.362
batch start
#iterations: 191
currently lose_sum: 521.1918129622936
time_elpased: 1.368
batch start
#iterations: 192
currently lose_sum: 519.943703353405
time_elpased: 1.376
batch start
#iterations: 193
currently lose_sum: 520.039639621973
time_elpased: 1.372
batch start
#iterations: 194
currently lose_sum: 519.7261427044868
time_elpased: 1.367
batch start
#iterations: 195
currently lose_sum: 520.4704422056675
time_elpased: 1.363
batch start
#iterations: 196
currently lose_sum: 518.4881239831448
time_elpased: 1.374
batch start
#iterations: 197
currently lose_sum: 521.4910718202591
time_elpased: 1.364
batch start
#iterations: 198
currently lose_sum: 520.2836645245552
time_elpased: 1.366
batch start
#iterations: 199
currently lose_sum: 519.5747397542
time_elpased: 1.364
start validation test
0.173505154639
1.0
0.173505154639
0.295704120179
0
validation finish
batch start
#iterations: 200
currently lose_sum: 521.9601005911827
time_elpased: 1.383
batch start
#iterations: 201
currently lose_sum: 519.494095236063
time_elpased: 1.382
batch start
#iterations: 202
currently lose_sum: 520.2168045938015
time_elpased: 1.375
batch start
#iterations: 203
currently lose_sum: 518.6806034743786
time_elpased: 1.367
batch start
#iterations: 204
currently lose_sum: 520.1169620156288
time_elpased: 1.376
batch start
#iterations: 205
currently lose_sum: 521.0719832479954
time_elpased: 1.371
batch start
#iterations: 206
currently lose_sum: 520.3284766972065
time_elpased: 1.361
batch start
#iterations: 207
currently lose_sum: 518.8212169110775
time_elpased: 1.376
batch start
#iterations: 208
currently lose_sum: 519.2646817862988
time_elpased: 1.364
batch start
#iterations: 209
currently lose_sum: 519.2433214485645
time_elpased: 1.371
batch start
#iterations: 210
currently lose_sum: 521.1367303431034
time_elpased: 1.367
batch start
#iterations: 211
currently lose_sum: 520.8296509683132
time_elpased: 1.384
batch start
#iterations: 212
currently lose_sum: 520.1826035380363
time_elpased: 1.368
batch start
#iterations: 213
currently lose_sum: 518.505149513483
time_elpased: 1.366
batch start
#iterations: 214
currently lose_sum: 520.8258872926235
time_elpased: 1.371
batch start
#iterations: 215
currently lose_sum: 520.7046604454517
time_elpased: 1.371
batch start
#iterations: 216
currently lose_sum: 520.5468904674053
time_elpased: 1.362
batch start
#iterations: 217
currently lose_sum: 519.4648041427135
time_elpased: 1.377
batch start
#iterations: 218
currently lose_sum: 521.8445448875427
time_elpased: 1.362
batch start
#iterations: 219
currently lose_sum: 521.0285631120205
time_elpased: 1.378
start validation test
0.172474226804
1.0
0.172474226804
0.294205574607
0
validation finish
batch start
#iterations: 220
currently lose_sum: 518.6009284555912
time_elpased: 1.373
batch start
#iterations: 221
currently lose_sum: 521.036872625351
time_elpased: 1.356
batch start
#iterations: 222
currently lose_sum: 518.9400548934937
time_elpased: 1.373
batch start
#iterations: 223
currently lose_sum: 520.6814347803593
time_elpased: 1.367
batch start
#iterations: 224
currently lose_sum: 519.230654001236
time_elpased: 1.377
batch start
#iterations: 225
currently lose_sum: 521.7272642254829
time_elpased: 1.373
batch start
#iterations: 226
currently lose_sum: 520.7755407392979
time_elpased: 1.375
batch start
#iterations: 227
currently lose_sum: 518.283672362566
time_elpased: 1.36
batch start
#iterations: 228
currently lose_sum: 517.7387780249119
time_elpased: 1.369
batch start
#iterations: 229
currently lose_sum: 519.2700281143188
time_elpased: 1.373
batch start
#iterations: 230
currently lose_sum: 521.3620358705521
time_elpased: 1.365
batch start
#iterations: 231
currently lose_sum: 522.7994804382324
time_elpased: 1.369
batch start
#iterations: 232
currently lose_sum: 519.2768213152885
time_elpased: 1.36
batch start
#iterations: 233
currently lose_sum: 517.4794581830502
time_elpased: 1.358
batch start
#iterations: 234
currently lose_sum: 518.3051976561546
time_elpased: 1.378
batch start
#iterations: 235
currently lose_sum: 519.0094202458858
time_elpased: 1.368
batch start
#iterations: 236
currently lose_sum: 518.2309039533138
time_elpased: 1.364
batch start
#iterations: 237
currently lose_sum: 519.6389119029045
time_elpased: 1.377
batch start
#iterations: 238
currently lose_sum: 518.7567088603973
time_elpased: 1.372
batch start
#iterations: 239
currently lose_sum: 519.0022490620613
time_elpased: 1.385
start validation test
0.156907216495
1.0
0.156907216495
0.271252896097
0
validation finish
batch start
#iterations: 240
currently lose_sum: 518.3068826198578
time_elpased: 1.368
batch start
#iterations: 241
currently lose_sum: 521.120188742876
time_elpased: 1.361
batch start
#iterations: 242
currently lose_sum: 518.5281431674957
time_elpased: 1.371
batch start
#iterations: 243
currently lose_sum: 518.409970164299
time_elpased: 1.363
batch start
#iterations: 244
currently lose_sum: 519.2168558835983
time_elpased: 1.364
batch start
#iterations: 245
currently lose_sum: 519.9375926852226
time_elpased: 1.367
batch start
#iterations: 246
currently lose_sum: 519.658863991499
time_elpased: 1.377
batch start
#iterations: 247
currently lose_sum: 519.7352610528469
time_elpased: 1.365
batch start
#iterations: 248
currently lose_sum: 518.9033455252647
time_elpased: 1.367
batch start
#iterations: 249
currently lose_sum: 520.3636658191681
time_elpased: 1.382
batch start
#iterations: 250
currently lose_sum: 519.3150193095207
time_elpased: 1.375
batch start
#iterations: 251
currently lose_sum: 520.8348600566387
time_elpased: 1.377
batch start
#iterations: 252
currently lose_sum: 519.9187385737896
time_elpased: 1.362
batch start
#iterations: 253
currently lose_sum: 519.2928144335747
time_elpased: 1.37
batch start
#iterations: 254
currently lose_sum: 520.5354146659374
time_elpased: 1.359
batch start
#iterations: 255
currently lose_sum: 518.4564250409603
time_elpased: 1.358
batch start
#iterations: 256
currently lose_sum: 518.7416621148586
time_elpased: 1.361
batch start
#iterations: 257
currently lose_sum: 517.7971709370613
time_elpased: 1.38
batch start
#iterations: 258
currently lose_sum: 520.2941901981831
time_elpased: 1.363
batch start
#iterations: 259
currently lose_sum: 520.6342580914497
time_elpased: 1.361
start validation test
0.181237113402
1.0
0.181237113402
0.306859835922
0
validation finish
batch start
#iterations: 260
currently lose_sum: 522.5000749528408
time_elpased: 1.367
batch start
#iterations: 261
currently lose_sum: 521.2389007508755
time_elpased: 1.375
batch start
#iterations: 262
currently lose_sum: 519.3087766468525
time_elpased: 1.359
batch start
#iterations: 263
currently lose_sum: 519.9425312876701
time_elpased: 1.37
batch start
#iterations: 264
currently lose_sum: 520.1380107402802
time_elpased: 1.368
batch start
#iterations: 265
currently lose_sum: 520.5666893422604
time_elpased: 1.369
batch start
#iterations: 266
currently lose_sum: 518.8351655900478
time_elpased: 1.361
batch start
#iterations: 267
currently lose_sum: 517.8092780411243
time_elpased: 1.356
batch start
#iterations: 268
currently lose_sum: 519.2558604180813
time_elpased: 1.358
batch start
#iterations: 269
currently lose_sum: 519.6983713805676
time_elpased: 1.364
batch start
#iterations: 270
currently lose_sum: 518.8466421067715
time_elpased: 1.37
batch start
#iterations: 271
currently lose_sum: 517.8045896887779
time_elpased: 1.363
batch start
#iterations: 272
currently lose_sum: 520.2103084921837
time_elpased: 1.375
batch start
#iterations: 273
currently lose_sum: 517.4068904817104
time_elpased: 1.374
batch start
#iterations: 274
currently lose_sum: 518.5730677545071
time_elpased: 1.378
batch start
#iterations: 275
currently lose_sum: 519.0794697105885
time_elpased: 1.373
batch start
#iterations: 276
currently lose_sum: 519.4045872092247
time_elpased: 1.355
batch start
#iterations: 277
currently lose_sum: 519.9252413511276
time_elpased: 1.388
batch start
#iterations: 278
currently lose_sum: 519.4245676994324
time_elpased: 1.357
batch start
#iterations: 279
currently lose_sum: 521.5789636969566
time_elpased: 1.364
start validation test
0.171855670103
1.0
0.171855670103
0.293305181666
0
validation finish
batch start
#iterations: 280
currently lose_sum: 517.2548317611217
time_elpased: 1.382
batch start
#iterations: 281
currently lose_sum: 518.4787001609802
time_elpased: 1.369
batch start
#iterations: 282
currently lose_sum: 520.0825270116329
time_elpased: 1.368
batch start
#iterations: 283
currently lose_sum: 519.0530907809734
time_elpased: 1.364
batch start
#iterations: 284
currently lose_sum: 520.1610869467258
time_elpased: 1.373
batch start
#iterations: 285
currently lose_sum: 518.8982587754726
time_elpased: 1.373
batch start
#iterations: 286
currently lose_sum: 521.2237374186516
time_elpased: 1.363
batch start
#iterations: 287
currently lose_sum: 520.2942331135273
time_elpased: 1.368
batch start
#iterations: 288
currently lose_sum: 520.2100892066956
time_elpased: 1.358
batch start
#iterations: 289
currently lose_sum: 520.1464640498161
time_elpased: 1.371
batch start
#iterations: 290
currently lose_sum: 518.8407487869263
time_elpased: 1.376
batch start
#iterations: 291
currently lose_sum: 519.4251581132412
time_elpased: 1.367
batch start
#iterations: 292
currently lose_sum: 520.0144249200821
time_elpased: 1.372
batch start
#iterations: 293
currently lose_sum: 520.3874611854553
time_elpased: 1.371
batch start
#iterations: 294
currently lose_sum: 520.7951096594334
time_elpased: 1.379
batch start
#iterations: 295
currently lose_sum: 519.5147080421448
time_elpased: 1.364
batch start
#iterations: 296
currently lose_sum: 517.976939290762
time_elpased: 1.381
batch start
#iterations: 297
currently lose_sum: 520.5080380141735
time_elpased: 1.371
batch start
#iterations: 298
currently lose_sum: 519.0138811767101
time_elpased: 1.372
batch start
#iterations: 299
currently lose_sum: 519.7424404323101
time_elpased: 1.36
start validation test
0.172577319588
1.0
0.172577319588
0.29435554774
0
validation finish
batch start
#iterations: 300
currently lose_sum: 519.1316868066788
time_elpased: 1.365
batch start
#iterations: 301
currently lose_sum: 519.1275615692139
time_elpased: 1.377
batch start
#iterations: 302
currently lose_sum: 521.382505863905
time_elpased: 1.36
batch start
#iterations: 303
currently lose_sum: 522.2200569808483
time_elpased: 1.367
batch start
#iterations: 304
currently lose_sum: 515.7142441272736
time_elpased: 1.373
batch start
#iterations: 305
currently lose_sum: 520.0383589267731
time_elpased: 1.354
batch start
#iterations: 306
currently lose_sum: 519.2047385275364
time_elpased: 1.362
batch start
#iterations: 307
currently lose_sum: 519.2839986383915
time_elpased: 1.366
batch start
#iterations: 308
currently lose_sum: 519.4015769660473
time_elpased: 1.367
batch start
#iterations: 309
currently lose_sum: 520.4894640743732
time_elpased: 1.362
batch start
#iterations: 310
currently lose_sum: 518.7533622086048
time_elpased: 1.381
batch start
#iterations: 311
currently lose_sum: 517.465964704752
time_elpased: 1.362
batch start
#iterations: 312
currently lose_sum: 520.4197881519794
time_elpased: 1.376
batch start
#iterations: 313
currently lose_sum: 521.0350998342037
time_elpased: 1.368
batch start
#iterations: 314
currently lose_sum: 518.9702702462673
time_elpased: 1.372
batch start
#iterations: 315
currently lose_sum: 519.5779289007187
time_elpased: 1.379
batch start
#iterations: 316
currently lose_sum: 519.3129023909569
time_elpased: 1.356
batch start
#iterations: 317
currently lose_sum: 519.3777159154415
time_elpased: 1.369
batch start
#iterations: 318
currently lose_sum: 522.0137463212013
time_elpased: 1.374
batch start
#iterations: 319
currently lose_sum: 518.6767372488976
time_elpased: 1.383
start validation test
0.175051546392
1.0
0.175051546392
0.297947008247
0
validation finish
batch start
#iterations: 320
currently lose_sum: 519.677755266428
time_elpased: 1.368
batch start
#iterations: 321
currently lose_sum: 521.3493973016739
time_elpased: 1.364
batch start
#iterations: 322
currently lose_sum: 518.8674812018871
time_elpased: 1.366
batch start
#iterations: 323
currently lose_sum: 517.4415437579155
time_elpased: 1.367
batch start
#iterations: 324
currently lose_sum: 522.0315300226212
time_elpased: 1.366
batch start
#iterations: 325
currently lose_sum: 518.3014631271362
time_elpased: 1.38
batch start
#iterations: 326
currently lose_sum: 518.5973435938358
time_elpased: 1.365
batch start
#iterations: 327
currently lose_sum: 519.4285822510719
time_elpased: 1.384
batch start
#iterations: 328
currently lose_sum: 519.8135368824005
time_elpased: 1.368
batch start
#iterations: 329
currently lose_sum: 519.342066526413
time_elpased: 1.371
batch start
#iterations: 330
currently lose_sum: 521.7866488099098
time_elpased: 1.382
batch start
#iterations: 331
currently lose_sum: 522.0293115377426
time_elpased: 1.376
batch start
#iterations: 332
currently lose_sum: 521.2721627950668
time_elpased: 1.377
batch start
#iterations: 333
currently lose_sum: 518.8451831638813
time_elpased: 1.376
batch start
#iterations: 334
currently lose_sum: 518.8859386146069
time_elpased: 1.382
batch start
#iterations: 335
currently lose_sum: 519.2357662022114
time_elpased: 1.384
batch start
#iterations: 336
currently lose_sum: 520.8771177828312
time_elpased: 1.374
batch start
#iterations: 337
currently lose_sum: 520.4774540066719
time_elpased: 1.365
batch start
#iterations: 338
currently lose_sum: 520.0177929401398
time_elpased: 1.367
batch start
#iterations: 339
currently lose_sum: 519.8480494618416
time_elpased: 1.362
start validation test
0.18
1.0
0.18
0.305084745763
0
validation finish
batch start
#iterations: 340
currently lose_sum: 519.9248351156712
time_elpased: 1.385
batch start
#iterations: 341
currently lose_sum: 520.1925028562546
time_elpased: 1.376
batch start
#iterations: 342
currently lose_sum: 521.699116319418
time_elpased: 1.381
batch start
#iterations: 343
currently lose_sum: 520.6902812123299
time_elpased: 1.368
batch start
#iterations: 344
currently lose_sum: 520.4823133945465
time_elpased: 1.373
batch start
#iterations: 345
currently lose_sum: 520.5660932660103
time_elpased: 1.373
batch start
#iterations: 346
currently lose_sum: 517.82445576787
time_elpased: 1.358
batch start
#iterations: 347
currently lose_sum: 518.8534198403358
time_elpased: 1.368
batch start
#iterations: 348
currently lose_sum: 518.6340339183807
time_elpased: 1.368
batch start
#iterations: 349
currently lose_sum: 519.9627899229527
time_elpased: 1.368
batch start
#iterations: 350
currently lose_sum: 520.2202701568604
time_elpased: 1.356
batch start
#iterations: 351
currently lose_sum: 518.8759503662586
time_elpased: 1.371
batch start
#iterations: 352
currently lose_sum: 519.3804925084114
time_elpased: 1.37
batch start
#iterations: 353
currently lose_sum: 520.6632829010487
time_elpased: 1.371
batch start
#iterations: 354
currently lose_sum: 518.936132311821
time_elpased: 1.372
batch start
#iterations: 355
currently lose_sum: 519.2513480782509
time_elpased: 1.37
batch start
#iterations: 356
currently lose_sum: 519.8712465167046
time_elpased: 1.357
batch start
#iterations: 357
currently lose_sum: 519.2551552057266
time_elpased: 1.379
batch start
#iterations: 358
currently lose_sum: 519.3973659873009
time_elpased: 1.376
batch start
#iterations: 359
currently lose_sum: 518.9096243083477
time_elpased: 1.366
start validation test
0.167835051546
1.0
0.167835051546
0.287429378531
0
validation finish
batch start
#iterations: 360
currently lose_sum: 520.2943056225777
time_elpased: 1.376
batch start
#iterations: 361
currently lose_sum: 517.4480408728123
time_elpased: 1.376
batch start
#iterations: 362
currently lose_sum: 518.2506942451
time_elpased: 1.375
batch start
#iterations: 363
currently lose_sum: 519.4376128613949
time_elpased: 1.385
batch start
#iterations: 364
currently lose_sum: 519.8896971046925
time_elpased: 1.376
batch start
#iterations: 365
currently lose_sum: 519.1006801724434
time_elpased: 1.367
batch start
#iterations: 366
currently lose_sum: 518.7204847931862
time_elpased: 1.386
batch start
#iterations: 367
currently lose_sum: 520.7158042490482
time_elpased: 1.371
batch start
#iterations: 368
currently lose_sum: 517.0948234200478
time_elpased: 1.356
batch start
#iterations: 369
currently lose_sum: 518.9824457168579
time_elpased: 1.372
batch start
#iterations: 370
currently lose_sum: 517.668478935957
time_elpased: 1.384
batch start
#iterations: 371
currently lose_sum: 520.9742090404034
time_elpased: 1.359
batch start
#iterations: 372
currently lose_sum: 519.0759456157684
time_elpased: 1.382
batch start
#iterations: 373
currently lose_sum: 520.0464678704739
time_elpased: 1.37
batch start
#iterations: 374
currently lose_sum: 521.199737071991
time_elpased: 1.377
batch start
#iterations: 375
currently lose_sum: 521.6919141411781
time_elpased: 1.373
batch start
#iterations: 376
currently lose_sum: 519.8028852045536
time_elpased: 1.364
batch start
#iterations: 377
currently lose_sum: 519.0709286630154
time_elpased: 1.368
batch start
#iterations: 378
currently lose_sum: 519.535617440939
time_elpased: 1.374
batch start
#iterations: 379
currently lose_sum: 517.4493819177151
time_elpased: 1.372
start validation test
0.16793814433
1.0
0.16793814433
0.287580545503
0
validation finish
batch start
#iterations: 380
currently lose_sum: 520.9696877896786
time_elpased: 1.37
batch start
#iterations: 381
currently lose_sum: 519.047422349453
time_elpased: 1.358
batch start
#iterations: 382
currently lose_sum: 520.2015977203846
time_elpased: 1.371
batch start
#iterations: 383
currently lose_sum: 518.1614218950272
time_elpased: 1.37
batch start
#iterations: 384
currently lose_sum: 517.701422303915
time_elpased: 1.369
batch start
#iterations: 385
currently lose_sum: 517.9445370435715
time_elpased: 1.372
batch start
#iterations: 386
currently lose_sum: 519.4365491867065
time_elpased: 1.365
batch start
#iterations: 387
currently lose_sum: 519.664854735136
time_elpased: 1.347
batch start
#iterations: 388
currently lose_sum: 519.3433338403702
time_elpased: 1.357
batch start
#iterations: 389
currently lose_sum: 519.7161385118961
time_elpased: 1.369
batch start
#iterations: 390
currently lose_sum: 519.6189568638802
time_elpased: 1.375
batch start
#iterations: 391
currently lose_sum: 519.8792110383511
time_elpased: 1.362
batch start
#iterations: 392
currently lose_sum: 519.042377859354
time_elpased: 1.373
batch start
#iterations: 393
currently lose_sum: 518.9901909530163
time_elpased: 1.371
batch start
#iterations: 394
currently lose_sum: 519.3649245500565
time_elpased: 1.367
batch start
#iterations: 395
currently lose_sum: 518.5470519959927
time_elpased: 1.37
batch start
#iterations: 396
currently lose_sum: 519.8305506706238
time_elpased: 1.367
batch start
#iterations: 397
currently lose_sum: 521.0943359732628
time_elpased: 1.375
batch start
#iterations: 398
currently lose_sum: 519.1289665699005
time_elpased: 1.378
batch start
#iterations: 399
currently lose_sum: 518.6991614699364
time_elpased: 1.379
start validation test
0.163608247423
1.0
0.163608247423
0.281208469921
0
validation finish
acc: 0.191
pre: 1.000
rec: 0.191
F1: 0.321
auc: 0.000
