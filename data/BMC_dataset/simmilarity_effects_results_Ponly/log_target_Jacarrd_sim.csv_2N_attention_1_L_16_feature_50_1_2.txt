start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 556.3888772130013
time_elpased: 1.662
batch start
#iterations: 1
currently lose_sum: 538.0969589948654
time_elpased: 1.625
batch start
#iterations: 2
currently lose_sum: 531.3879495859146
time_elpased: 1.631
batch start
#iterations: 3
currently lose_sum: 527.4038830697536
time_elpased: 1.702
batch start
#iterations: 4
currently lose_sum: 523.9090825915337
time_elpased: 1.623
batch start
#iterations: 5
currently lose_sum: 524.3133633136749
time_elpased: 1.619
batch start
#iterations: 6
currently lose_sum: 520.7246841490269
time_elpased: 1.71
batch start
#iterations: 7
currently lose_sum: 520.3391851782799
time_elpased: 1.626
batch start
#iterations: 8
currently lose_sum: 517.5026393532753
time_elpased: 1.623
batch start
#iterations: 9
currently lose_sum: 517.6564074754715
time_elpased: 1.656
batch start
#iterations: 10
currently lose_sum: 516.5756876170635
time_elpased: 1.688
batch start
#iterations: 11
currently lose_sum: 516.8049129247665
time_elpased: 1.63
batch start
#iterations: 12
currently lose_sum: 515.8981180787086
time_elpased: 1.642
batch start
#iterations: 13
currently lose_sum: 513.8269982337952
time_elpased: 1.629
batch start
#iterations: 14
currently lose_sum: 515.4832138121128
time_elpased: 1.628
batch start
#iterations: 15
currently lose_sum: 515.133320838213
time_elpased: 1.635
batch start
#iterations: 16
currently lose_sum: 513.5793642401695
time_elpased: 1.627
batch start
#iterations: 17
currently lose_sum: 513.4026763737202
time_elpased: 1.654
batch start
#iterations: 18
currently lose_sum: 512.6593981981277
time_elpased: 1.641
batch start
#iterations: 19
currently lose_sum: 512.8453809320927
time_elpased: 1.726
start validation test
0.19206185567
1.0
0.19206185567
0.322234714175
0
validation finish
batch start
#iterations: 20
currently lose_sum: 512.9307822883129
time_elpased: 1.641
batch start
#iterations: 21
currently lose_sum: 513.6932950019836
time_elpased: 1.613
batch start
#iterations: 22
currently lose_sum: 511.0820195376873
time_elpased: 1.674
batch start
#iterations: 23
currently lose_sum: 512.1207346618176
time_elpased: 1.659
batch start
#iterations: 24
currently lose_sum: 509.9616605937481
time_elpased: 1.652
batch start
#iterations: 25
currently lose_sum: 512.0829496085644
time_elpased: 1.64
batch start
#iterations: 26
currently lose_sum: 510.5425599217415
time_elpased: 1.719
batch start
#iterations: 27
currently lose_sum: 510.5097019672394
time_elpased: 1.63
batch start
#iterations: 28
currently lose_sum: 508.6151715517044
time_elpased: 1.642
batch start
#iterations: 29
currently lose_sum: 508.0287089943886
time_elpased: 1.633
batch start
#iterations: 30
currently lose_sum: 508.49422538280487
time_elpased: 1.625
batch start
#iterations: 31
currently lose_sum: 508.18049412965775
time_elpased: 1.658
batch start
#iterations: 32
currently lose_sum: 509.7962740957737
time_elpased: 1.643
batch start
#iterations: 33
currently lose_sum: 507.15908858180046
time_elpased: 1.671
batch start
#iterations: 34
currently lose_sum: 509.7463558912277
time_elpased: 1.631
batch start
#iterations: 35
currently lose_sum: 505.9314442873001
time_elpased: 1.713
batch start
#iterations: 36
currently lose_sum: 508.28700414299965
time_elpased: 1.674
batch start
#iterations: 37
currently lose_sum: 507.1380828022957
time_elpased: 1.625
batch start
#iterations: 38
currently lose_sum: 507.62950587272644
time_elpased: 1.697
batch start
#iterations: 39
currently lose_sum: 508.9696586430073
time_elpased: 1.623
start validation test
0.146494845361
1.0
0.146494845361
0.255552558223
0
validation finish
batch start
#iterations: 40
currently lose_sum: 509.53911367058754
time_elpased: 1.641
batch start
#iterations: 41
currently lose_sum: 508.86502370238304
time_elpased: 1.628
batch start
#iterations: 42
currently lose_sum: 507.8113892376423
time_elpased: 1.675
batch start
#iterations: 43
currently lose_sum: 506.55056300759315
time_elpased: 1.629
batch start
#iterations: 44
currently lose_sum: 506.9951866567135
time_elpased: 1.614
batch start
#iterations: 45
currently lose_sum: 505.8577769994736
time_elpased: 1.636
batch start
#iterations: 46
currently lose_sum: 505.48550644516945
time_elpased: 1.638
batch start
#iterations: 47
currently lose_sum: 505.7309487760067
time_elpased: 1.703
batch start
#iterations: 48
currently lose_sum: 506.3573760688305
time_elpased: 1.683
batch start
#iterations: 49
currently lose_sum: 505.6927179694176
time_elpased: 1.629
batch start
#iterations: 50
currently lose_sum: 507.10643887519836
time_elpased: 1.624
batch start
#iterations: 51
currently lose_sum: 505.45142301917076
time_elpased: 1.634
batch start
#iterations: 52
currently lose_sum: 505.87796998023987
time_elpased: 1.655
batch start
#iterations: 53
currently lose_sum: 507.69953736662865
time_elpased: 1.628
batch start
#iterations: 54
currently lose_sum: 504.69144865870476
time_elpased: 1.702
batch start
#iterations: 55
currently lose_sum: 508.7366119027138
time_elpased: 1.703
batch start
#iterations: 56
currently lose_sum: 506.3732073903084
time_elpased: 1.648
batch start
#iterations: 57
currently lose_sum: 505.0854412615299
time_elpased: 1.63
batch start
#iterations: 58
currently lose_sum: 507.52098602056503
time_elpased: 1.681
batch start
#iterations: 59
currently lose_sum: 503.95688104629517
time_elpased: 1.642
start validation test
0.238865979381
1.0
0.238865979381
0.385620371141
0
validation finish
batch start
#iterations: 60
currently lose_sum: 508.3663191795349
time_elpased: 1.667
batch start
#iterations: 61
currently lose_sum: 506.90553292632103
time_elpased: 1.65
batch start
#iterations: 62
currently lose_sum: 507.5903790593147
time_elpased: 1.717
batch start
#iterations: 63
currently lose_sum: 508.1545125246048
time_elpased: 1.651
batch start
#iterations: 64
currently lose_sum: 506.59633696079254
time_elpased: 1.632
batch start
#iterations: 65
currently lose_sum: 506.045381963253
time_elpased: 1.62
batch start
#iterations: 66
currently lose_sum: 505.5927501618862
time_elpased: 1.665
batch start
#iterations: 67
currently lose_sum: 504.04522120952606
time_elpased: 1.636
batch start
#iterations: 68
currently lose_sum: 508.2496098577976
time_elpased: 1.634
batch start
#iterations: 69
currently lose_sum: 505.3111749589443
time_elpased: 1.671
batch start
#iterations: 70
currently lose_sum: 506.3488608598709
time_elpased: 1.63
batch start
#iterations: 71
currently lose_sum: 506.7225153744221
time_elpased: 1.721
batch start
#iterations: 72
currently lose_sum: 505.96973273158073
time_elpased: 1.623
batch start
#iterations: 73
currently lose_sum: 503.9766873717308
time_elpased: 1.633
batch start
#iterations: 74
currently lose_sum: 505.5445531606674
time_elpased: 1.624
batch start
#iterations: 75
currently lose_sum: 504.77792194485664
time_elpased: 1.65
batch start
#iterations: 76
currently lose_sum: 506.866846293211
time_elpased: 1.716
batch start
#iterations: 77
currently lose_sum: 505.4493481516838
time_elpased: 1.71
batch start
#iterations: 78
currently lose_sum: 506.03330636024475
time_elpased: 1.714
batch start
#iterations: 79
currently lose_sum: 507.2266238927841
time_elpased: 1.624
start validation test
0.282268041237
1.0
0.282268041237
0.440263707992
0
validation finish
batch start
#iterations: 80
currently lose_sum: 503.4808640778065
time_elpased: 1.716
batch start
#iterations: 81
currently lose_sum: 505.1682458817959
time_elpased: 1.652
batch start
#iterations: 82
currently lose_sum: 504.4825772047043
time_elpased: 1.697
batch start
#iterations: 83
currently lose_sum: 505.23565605282784
time_elpased: 1.632
batch start
#iterations: 84
currently lose_sum: 505.8862376511097
time_elpased: 1.646
batch start
#iterations: 85
currently lose_sum: 503.26748353242874
time_elpased: 1.69
batch start
#iterations: 86
currently lose_sum: 503.6445318162441
time_elpased: 1.687
batch start
#iterations: 87
currently lose_sum: 503.8035615980625
time_elpased: 1.722
batch start
#iterations: 88
currently lose_sum: 504.1284595131874
time_elpased: 1.629
batch start
#iterations: 89
currently lose_sum: 504.2924084663391
time_elpased: 1.675
batch start
#iterations: 90
currently lose_sum: 506.2469410300255
time_elpased: 1.703
batch start
#iterations: 91
currently lose_sum: 502.53697365522385
time_elpased: 1.728
batch start
#iterations: 92
currently lose_sum: 505.11218586564064
time_elpased: 1.67
batch start
#iterations: 93
currently lose_sum: 504.61361491680145
time_elpased: 1.629
batch start
#iterations: 94
currently lose_sum: 503.9689171910286
time_elpased: 1.702
batch start
#iterations: 95
currently lose_sum: 502.67662087082863
time_elpased: 1.664
batch start
#iterations: 96
currently lose_sum: 504.9898558855057
time_elpased: 1.64
batch start
#iterations: 97
currently lose_sum: 504.01955872774124
time_elpased: 1.708
batch start
#iterations: 98
currently lose_sum: 504.69904255867004
time_elpased: 1.635
batch start
#iterations: 99
currently lose_sum: 504.6209870874882
time_elpased: 1.643
start validation test
0.18618556701
1.0
0.18618556701
0.31392317052
0
validation finish
batch start
#iterations: 100
currently lose_sum: 504.65629667043686
time_elpased: 1.656
batch start
#iterations: 101
currently lose_sum: 503.8374615609646
time_elpased: 1.641
batch start
#iterations: 102
currently lose_sum: 502.7290170788765
time_elpased: 1.628
batch start
#iterations: 103
currently lose_sum: 503.07080084085464
time_elpased: 1.672
batch start
#iterations: 104
currently lose_sum: 505.029503852129
time_elpased: 1.634
batch start
#iterations: 105
currently lose_sum: 503.45311215519905
time_elpased: 1.677
batch start
#iterations: 106
currently lose_sum: 505.31338784098625
time_elpased: 1.659
batch start
#iterations: 107
currently lose_sum: 504.9463151693344
time_elpased: 1.634
batch start
#iterations: 108
currently lose_sum: 501.9498963057995
time_elpased: 1.612
batch start
#iterations: 109
currently lose_sum: 504.8560547530651
time_elpased: 1.654
batch start
#iterations: 110
currently lose_sum: 503.94463923573494
time_elpased: 1.705
batch start
#iterations: 111
currently lose_sum: 505.6548907160759
time_elpased: 1.629
batch start
#iterations: 112
currently lose_sum: 502.79067346453667
time_elpased: 1.653
batch start
#iterations: 113
currently lose_sum: 503.3722529709339
time_elpased: 1.681
batch start
#iterations: 114
currently lose_sum: 503.33567130565643
time_elpased: 1.636
batch start
#iterations: 115
currently lose_sum: 503.245264172554
time_elpased: 1.682
batch start
#iterations: 116
currently lose_sum: 502.2789051234722
time_elpased: 1.618
batch start
#iterations: 117
currently lose_sum: 503.9673021733761
time_elpased: 1.679
batch start
#iterations: 118
currently lose_sum: 503.03730058670044
time_elpased: 1.622
batch start
#iterations: 119
currently lose_sum: 503.1515728831291
time_elpased: 1.657
start validation test
0.284845360825
1.0
0.284845360825
0.443392441627
0
validation finish
batch start
#iterations: 120
currently lose_sum: 504.7379586696625
time_elpased: 1.637
batch start
#iterations: 121
currently lose_sum: 501.3716087639332
time_elpased: 1.63
batch start
#iterations: 122
currently lose_sum: 501.9639759361744
time_elpased: 1.64
batch start
#iterations: 123
currently lose_sum: 501.3726215660572
time_elpased: 1.636
batch start
#iterations: 124
currently lose_sum: 502.1728843450546
time_elpased: 1.643
batch start
#iterations: 125
currently lose_sum: 502.75517722964287
time_elpased: 1.679
batch start
#iterations: 126
currently lose_sum: 502.6901919543743
time_elpased: 1.641
batch start
#iterations: 127
currently lose_sum: 504.6276116669178
time_elpased: 1.639
batch start
#iterations: 128
currently lose_sum: 502.7106560766697
time_elpased: 1.639
batch start
#iterations: 129
currently lose_sum: 502.7735939025879
time_elpased: 1.732
batch start
#iterations: 130
currently lose_sum: 501.77369126677513
time_elpased: 1.618
batch start
#iterations: 131
currently lose_sum: 502.7393384575844
time_elpased: 1.699
batch start
#iterations: 132
currently lose_sum: 505.23029881715775
time_elpased: 1.646
batch start
#iterations: 133
currently lose_sum: 503.103332221508
time_elpased: 1.636
batch start
#iterations: 134
currently lose_sum: 503.1099852025509
time_elpased: 1.669
batch start
#iterations: 135
currently lose_sum: 501.3210714459419
time_elpased: 1.632
batch start
#iterations: 136
currently lose_sum: 502.06716853380203
time_elpased: 1.651
batch start
#iterations: 137
currently lose_sum: 501.3143789768219
time_elpased: 1.637
batch start
#iterations: 138
currently lose_sum: 502.4441316127777
time_elpased: 1.725
batch start
#iterations: 139
currently lose_sum: 504.87296056747437
time_elpased: 1.646
start validation test
0.154226804124
1.0
0.154226804124
0.267238299393
0
validation finish
batch start
#iterations: 140
currently lose_sum: 500.02462962269783
time_elpased: 1.663
batch start
#iterations: 141
currently lose_sum: 504.43635186553
time_elpased: 1.678
batch start
#iterations: 142
currently lose_sum: 502.90593606233597
time_elpased: 1.7
batch start
#iterations: 143
currently lose_sum: 504.0416637659073
time_elpased: 1.665
batch start
#iterations: 144
currently lose_sum: 503.0313666462898
time_elpased: 1.703
batch start
#iterations: 145
currently lose_sum: 504.5366180241108
time_elpased: 1.706
batch start
#iterations: 146
currently lose_sum: 505.7938566505909
time_elpased: 1.63
batch start
#iterations: 147
currently lose_sum: 503.3597943484783
time_elpased: 1.638
batch start
#iterations: 148
currently lose_sum: 501.2619464099407
time_elpased: 1.648
batch start
#iterations: 149
currently lose_sum: 504.2783741950989
time_elpased: 1.731
batch start
#iterations: 150
currently lose_sum: 503.16988322138786
time_elpased: 1.636
batch start
#iterations: 151
currently lose_sum: 502.3719439506531
time_elpased: 1.643
batch start
#iterations: 152
currently lose_sum: 503.4049341082573
time_elpased: 1.644
batch start
#iterations: 153
currently lose_sum: 498.4976242482662
time_elpased: 1.714
batch start
#iterations: 154
currently lose_sum: 504.7003401219845
time_elpased: 1.629
batch start
#iterations: 155
currently lose_sum: 501.9469079375267
time_elpased: 1.642
batch start
#iterations: 156
currently lose_sum: 502.52131006121635
time_elpased: 1.627
batch start
#iterations: 157
currently lose_sum: 502.41315802931786
time_elpased: 1.644
batch start
#iterations: 158
currently lose_sum: 502.7345139682293
time_elpased: 1.711
batch start
#iterations: 159
currently lose_sum: 503.508404314518
time_elpased: 1.64
start validation test
0.236494845361
1.0
0.236494845361
0.382524595631
0
validation finish
batch start
#iterations: 160
currently lose_sum: 502.9443059563637
time_elpased: 1.63
batch start
#iterations: 161
currently lose_sum: 502.55676862597466
time_elpased: 1.719
batch start
#iterations: 162
currently lose_sum: 502.1799332499504
time_elpased: 1.639
batch start
#iterations: 163
currently lose_sum: 500.33428901433945
time_elpased: 1.704
batch start
#iterations: 164
currently lose_sum: 504.2097391784191
time_elpased: 1.642
batch start
#iterations: 165
currently lose_sum: 501.9722501039505
time_elpased: 1.73
batch start
#iterations: 166
currently lose_sum: 503.16685619950294
time_elpased: 1.697
batch start
#iterations: 167
currently lose_sum: 504.13218799233437
time_elpased: 1.634
batch start
#iterations: 168
currently lose_sum: 503.17634972929955
time_elpased: 1.674
batch start
#iterations: 169
currently lose_sum: 505.65793523192406
time_elpased: 1.641
batch start
#iterations: 170
currently lose_sum: 502.80569928884506
time_elpased: 1.636
batch start
#iterations: 171
currently lose_sum: 501.4168771505356
time_elpased: 1.649
batch start
#iterations: 172
currently lose_sum: 503.5974631905556
time_elpased: 1.668
batch start
#iterations: 173
currently lose_sum: 503.4370265007019
time_elpased: 1.642
batch start
#iterations: 174
currently lose_sum: 503.55531457066536
time_elpased: 1.641
batch start
#iterations: 175
currently lose_sum: 503.36498084664345
time_elpased: 1.626
batch start
#iterations: 176
currently lose_sum: 502.37547865509987
time_elpased: 1.686
batch start
#iterations: 177
currently lose_sum: 503.93657809495926
time_elpased: 1.706
batch start
#iterations: 178
currently lose_sum: 500.9888749420643
time_elpased: 1.63
batch start
#iterations: 179
currently lose_sum: 501.0523038804531
time_elpased: 1.648
start validation test
0.228144329897
1.0
0.228144329897
0.371526903383
0
validation finish
batch start
#iterations: 180
currently lose_sum: 502.484100908041
time_elpased: 1.632
batch start
#iterations: 181
currently lose_sum: 505.15544232726097
time_elpased: 1.677
batch start
#iterations: 182
currently lose_sum: 500.7355412244797
time_elpased: 1.696
batch start
#iterations: 183
currently lose_sum: 502.43282571434975
time_elpased: 1.64
batch start
#iterations: 184
currently lose_sum: 502.8434918820858
time_elpased: 1.727
batch start
#iterations: 185
currently lose_sum: 500.34483486413956
time_elpased: 1.705
batch start
#iterations: 186
currently lose_sum: 504.24678364396095
time_elpased: 1.678
batch start
#iterations: 187
currently lose_sum: 501.9215057492256
time_elpased: 1.644
batch start
#iterations: 188
currently lose_sum: 500.525642067194
time_elpased: 1.641
batch start
#iterations: 189
currently lose_sum: 500.9069267809391
time_elpased: 1.732
batch start
#iterations: 190
currently lose_sum: 500.7995871603489
time_elpased: 1.638
batch start
#iterations: 191
currently lose_sum: 503.0706576406956
time_elpased: 1.64
batch start
#iterations: 192
currently lose_sum: 501.78295573592186
time_elpased: 1.627
batch start
#iterations: 193
currently lose_sum: 502.65612372756004
time_elpased: 1.707
batch start
#iterations: 194
currently lose_sum: 503.5251985192299
time_elpased: 1.632
batch start
#iterations: 195
currently lose_sum: 501.9889630675316
time_elpased: 1.629
batch start
#iterations: 196
currently lose_sum: 501.29190692305565
time_elpased: 1.627
batch start
#iterations: 197
currently lose_sum: 504.0322584807873
time_elpased: 1.694
batch start
#iterations: 198
currently lose_sum: 502.73471888899803
time_elpased: 1.684
batch start
#iterations: 199
currently lose_sum: 502.43359994888306
time_elpased: 1.647
start validation test
0.223195876289
1.0
0.223195876289
0.364938895912
0
validation finish
batch start
#iterations: 200
currently lose_sum: 504.50154212117195
time_elpased: 1.67
batch start
#iterations: 201
currently lose_sum: 501.87722197175026
time_elpased: 1.717
batch start
#iterations: 202
currently lose_sum: 502.3336027264595
time_elpased: 1.629
batch start
#iterations: 203
currently lose_sum: 500.9499277472496
time_elpased: 1.719
batch start
#iterations: 204
currently lose_sum: 503.16822078824043
time_elpased: 1.694
batch start
#iterations: 205
currently lose_sum: 503.21556183695793
time_elpased: 1.624
batch start
#iterations: 206
currently lose_sum: 502.28222012519836
time_elpased: 1.687
batch start
#iterations: 207
currently lose_sum: 500.9255179464817
time_elpased: 1.696
batch start
#iterations: 208
currently lose_sum: 501.0471923649311
time_elpased: 1.685
batch start
#iterations: 209
currently lose_sum: 501.20769718289375
time_elpased: 1.704
batch start
#iterations: 210
currently lose_sum: 502.25198647379875
time_elpased: 1.656
batch start
#iterations: 211
currently lose_sum: 502.9103651046753
time_elpased: 1.724
batch start
#iterations: 212
currently lose_sum: 502.0935344696045
time_elpased: 1.636
batch start
#iterations: 213
currently lose_sum: 501.4985474050045
time_elpased: 1.689
batch start
#iterations: 214
currently lose_sum: 503.3859971165657
time_elpased: 1.641
batch start
#iterations: 215
currently lose_sum: 502.2157033383846
time_elpased: 1.634
batch start
#iterations: 216
currently lose_sum: 502.6529729962349
time_elpased: 1.671
batch start
#iterations: 217
currently lose_sum: 501.70534563064575
time_elpased: 1.726
batch start
#iterations: 218
currently lose_sum: 503.2644478082657
time_elpased: 1.662
batch start
#iterations: 219
currently lose_sum: 502.91015261411667
time_elpased: 1.727
start validation test
0.221649484536
1.0
0.221649484536
0.362869198312
0
validation finish
batch start
#iterations: 220
currently lose_sum: 500.2490222454071
time_elpased: 1.681
batch start
#iterations: 221
currently lose_sum: 503.5330744087696
time_elpased: 1.719
batch start
#iterations: 222
currently lose_sum: 500.68790301680565
time_elpased: 1.703
batch start
#iterations: 223
currently lose_sum: 503.6076600551605
time_elpased: 1.648
batch start
#iterations: 224
currently lose_sum: 501.5696790218353
time_elpased: 1.636
batch start
#iterations: 225
currently lose_sum: 504.14452281594276
time_elpased: 1.714
batch start
#iterations: 226
currently lose_sum: 503.42036324739456
time_elpased: 1.634
batch start
#iterations: 227
currently lose_sum: 499.4814816713333
time_elpased: 1.723
batch start
#iterations: 228
currently lose_sum: 500.1103955209255
time_elpased: 1.645
batch start
#iterations: 229
currently lose_sum: 500.61179652810097
time_elpased: 1.636
batch start
#iterations: 230
currently lose_sum: 504.3386632204056
time_elpased: 1.722
batch start
#iterations: 231
currently lose_sum: 505.51286593079567
time_elpased: 1.64
batch start
#iterations: 232
currently lose_sum: 501.15695253014565
time_elpased: 1.638
batch start
#iterations: 233
currently lose_sum: 500.1359128654003
time_elpased: 1.645
batch start
#iterations: 234
currently lose_sum: 501.786267131567
time_elpased: 1.635
batch start
#iterations: 235
currently lose_sum: 501.0581949353218
time_elpased: 1.682
batch start
#iterations: 236
currently lose_sum: 500.1955797672272
time_elpased: 1.637
batch start
#iterations: 237
currently lose_sum: 501.99089059233665
time_elpased: 1.625
batch start
#iterations: 238
currently lose_sum: 500.82261550426483
time_elpased: 1.631
batch start
#iterations: 239
currently lose_sum: 501.1291997730732
time_elpased: 1.645
start validation test
0.141546391753
1.0
0.141546391753
0.247990607785
0
validation finish
batch start
#iterations: 240
currently lose_sum: 500.9297677576542
time_elpased: 1.708
batch start
#iterations: 241
currently lose_sum: 502.4574150145054
time_elpased: 1.696
batch start
#iterations: 242
currently lose_sum: 501.1128779053688
time_elpased: 1.722
batch start
#iterations: 243
currently lose_sum: 500.65146550536156
time_elpased: 1.641
batch start
#iterations: 244
currently lose_sum: 501.7037436068058
time_elpased: 1.712
batch start
#iterations: 245
currently lose_sum: 500.60112711787224
time_elpased: 1.642
batch start
#iterations: 246
currently lose_sum: 500.87644773721695
time_elpased: 1.706
batch start
#iterations: 247
currently lose_sum: 501.303753644228
time_elpased: 1.634
batch start
#iterations: 248
currently lose_sum: 500.1560369133949
time_elpased: 1.673
batch start
#iterations: 249
currently lose_sum: 501.8968168795109
time_elpased: 1.718
batch start
#iterations: 250
currently lose_sum: 501.6358069181442
time_elpased: 1.644
batch start
#iterations: 251
currently lose_sum: 503.16627302765846
time_elpased: 1.633
batch start
#iterations: 252
currently lose_sum: 501.6734459400177
time_elpased: 1.635
batch start
#iterations: 253
currently lose_sum: 501.2796086668968
time_elpased: 1.668
batch start
#iterations: 254
currently lose_sum: 502.6416226029396
time_elpased: 1.695
batch start
#iterations: 255
currently lose_sum: 501.1153296530247
time_elpased: 1.695
batch start
#iterations: 256
currently lose_sum: 500.4592439830303
time_elpased: 1.634
batch start
#iterations: 257
currently lose_sum: 500.04715529084206
time_elpased: 1.65
batch start
#iterations: 258
currently lose_sum: 503.0119618475437
time_elpased: 1.653
batch start
#iterations: 259
currently lose_sum: 502.7917971909046
time_elpased: 1.632
start validation test
0.24206185567
1.0
0.24206185567
0.389774236388
0
validation finish
batch start
#iterations: 260
currently lose_sum: 503.4583628177643
time_elpased: 1.701
batch start
#iterations: 261
currently lose_sum: 502.5878109931946
time_elpased: 1.626
batch start
#iterations: 262
currently lose_sum: 500.4506365060806
time_elpased: 1.681
batch start
#iterations: 263
currently lose_sum: 502.3779302537441
time_elpased: 1.664
batch start
#iterations: 264
currently lose_sum: 501.73453029990196
time_elpased: 1.679
batch start
#iterations: 265
currently lose_sum: 502.94697618484497
time_elpased: 1.655
batch start
#iterations: 266
currently lose_sum: 501.5348535478115
time_elpased: 1.712
batch start
#iterations: 267
currently lose_sum: 500.2184319496155
time_elpased: 1.632
batch start
#iterations: 268
currently lose_sum: 500.6612804532051
time_elpased: 1.712
batch start
#iterations: 269
currently lose_sum: 501.06398448348045
time_elpased: 1.693
batch start
#iterations: 270
currently lose_sum: 501.28154957294464
time_elpased: 1.632
batch start
#iterations: 271
currently lose_sum: 499.03528955578804
time_elpased: 1.635
batch start
#iterations: 272
currently lose_sum: 501.9945501089096
time_elpased: 1.659
batch start
#iterations: 273
currently lose_sum: 498.629881054163
time_elpased: 1.731
batch start
#iterations: 274
currently lose_sum: 501.0836162865162
time_elpased: 1.64
batch start
#iterations: 275
currently lose_sum: 501.3927702009678
time_elpased: 1.64
batch start
#iterations: 276
currently lose_sum: 501.0812736451626
time_elpased: 1.636
batch start
#iterations: 277
currently lose_sum: 502.91592559218407
time_elpased: 1.642
batch start
#iterations: 278
currently lose_sum: 501.57629522681236
time_elpased: 1.623
batch start
#iterations: 279
currently lose_sum: 503.34037110209465
time_elpased: 1.66
start validation test
0.201546391753
1.0
0.201546391753
0.335478335478
0
validation finish
batch start
#iterations: 280
currently lose_sum: 500.6967736184597
time_elpased: 1.701
batch start
#iterations: 281
currently lose_sum: 500.0165384709835
time_elpased: 1.64
batch start
#iterations: 282
currently lose_sum: 501.49664437770844
time_elpased: 1.735
batch start
#iterations: 283
currently lose_sum: 501.0918202996254
time_elpased: 1.7
batch start
#iterations: 284
currently lose_sum: 502.3624725341797
time_elpased: 1.688
batch start
#iterations: 285
currently lose_sum: 501.9720351397991
time_elpased: 1.639
batch start
#iterations: 286
currently lose_sum: 504.5146940648556
time_elpased: 1.687
batch start
#iterations: 287
currently lose_sum: 502.5662393271923
time_elpased: 1.687
batch start
#iterations: 288
currently lose_sum: 503.5993674695492
time_elpased: 1.639
batch start
#iterations: 289
currently lose_sum: 501.71852192282677
time_elpased: 1.716
batch start
#iterations: 290
currently lose_sum: 501.00368055701256
time_elpased: 1.637
batch start
#iterations: 291
currently lose_sum: 501.7276172339916
time_elpased: 1.679
batch start
#iterations: 292
currently lose_sum: 500.41471150517464
time_elpased: 1.629
batch start
#iterations: 293
currently lose_sum: 502.00408032536507
time_elpased: 1.692
batch start
#iterations: 294
currently lose_sum: 502.2379880845547
time_elpased: 1.687
batch start
#iterations: 295
currently lose_sum: 501.75594025850296
time_elpased: 1.705
batch start
#iterations: 296
currently lose_sum: 500.86255779862404
time_elpased: 1.719
batch start
#iterations: 297
currently lose_sum: 502.9603892862797
time_elpased: 1.681
batch start
#iterations: 298
currently lose_sum: 500.48791867494583
time_elpased: 1.636
batch start
#iterations: 299
currently lose_sum: 501.84853741526604
time_elpased: 1.673
start validation test
0.207835051546
1.0
0.207835051546
0.344144759304
0
validation finish
batch start
#iterations: 300
currently lose_sum: 500.8342792093754
time_elpased: 1.654
batch start
#iterations: 301
currently lose_sum: 500.64815405011177
time_elpased: 1.69
batch start
#iterations: 302
currently lose_sum: 503.6741424500942
time_elpased: 1.68
batch start
#iterations: 303
currently lose_sum: 503.20833295583725
time_elpased: 1.639
batch start
#iterations: 304
currently lose_sum: 497.98257714509964
time_elpased: 1.635
batch start
#iterations: 305
currently lose_sum: 501.6983947455883
time_elpased: 1.656
batch start
#iterations: 306
currently lose_sum: 500.8614931702614
time_elpased: 1.723
batch start
#iterations: 307
currently lose_sum: 501.65255868434906
time_elpased: 1.692
batch start
#iterations: 308
currently lose_sum: 499.96654307842255
time_elpased: 1.655
batch start
#iterations: 309
currently lose_sum: 501.5222533047199
time_elpased: 1.698
batch start
#iterations: 310
currently lose_sum: 500.58498352766037
time_elpased: 1.71
batch start
#iterations: 311
currently lose_sum: 499.3441229760647
time_elpased: 1.629
batch start
#iterations: 312
currently lose_sum: 502.60297533869743
time_elpased: 1.646
batch start
#iterations: 313
currently lose_sum: 503.1950277686119
time_elpased: 1.731
batch start
#iterations: 314
currently lose_sum: 500.2011886835098
time_elpased: 1.674
batch start
#iterations: 315
currently lose_sum: 501.52077183127403
time_elpased: 1.711
batch start
#iterations: 316
currently lose_sum: 501.02598547935486
time_elpased: 1.649
batch start
#iterations: 317
currently lose_sum: 502.1858111321926
time_elpased: 1.685
batch start
#iterations: 318
currently lose_sum: 504.05731070041656
time_elpased: 1.626
batch start
#iterations: 319
currently lose_sum: 500.71466821432114
time_elpased: 1.69
start validation test
0.192577319588
1.0
0.192577319588
0.32295988935
0
validation finish
batch start
#iterations: 320
currently lose_sum: 501.73766100406647
time_elpased: 1.648
batch start
#iterations: 321
currently lose_sum: 503.738823145628
time_elpased: 1.633
batch start
#iterations: 322
currently lose_sum: 499.6508224904537
time_elpased: 1.703
batch start
#iterations: 323
currently lose_sum: 498.56685465574265
time_elpased: 1.691
batch start
#iterations: 324
currently lose_sum: 501.862511575222
time_elpased: 1.642
batch start
#iterations: 325
currently lose_sum: 499.7336165010929
time_elpased: 1.661
batch start
#iterations: 326
currently lose_sum: 499.69268998503685
time_elpased: 1.637
batch start
#iterations: 327
currently lose_sum: 501.3655815720558
time_elpased: 1.684
batch start
#iterations: 328
currently lose_sum: 501.60914942622185
time_elpased: 1.697
batch start
#iterations: 329
currently lose_sum: 501.3046446144581
time_elpased: 1.701
batch start
#iterations: 330
currently lose_sum: 503.4535143375397
time_elpased: 1.703
batch start
#iterations: 331
currently lose_sum: 503.01791992783546
time_elpased: 1.684
batch start
#iterations: 332
currently lose_sum: 502.5903077721596
time_elpased: 1.639
batch start
#iterations: 333
currently lose_sum: 500.26729360222816
time_elpased: 1.619
batch start
#iterations: 334
currently lose_sum: 499.95743295550346
time_elpased: 1.726
batch start
#iterations: 335
currently lose_sum: 499.8359365463257
time_elpased: 1.681
batch start
#iterations: 336
currently lose_sum: 502.37722408771515
time_elpased: 1.707
batch start
#iterations: 337
currently lose_sum: 500.98691070079803
time_elpased: 1.682
batch start
#iterations: 338
currently lose_sum: 501.83290445804596
time_elpased: 1.721
batch start
#iterations: 339
currently lose_sum: 501.38972705602646
time_elpased: 1.627
start validation test
0.223608247423
1.0
0.223608247423
0.365489931755
0
validation finish
batch start
#iterations: 340
currently lose_sum: 501.9537088871002
time_elpased: 1.635
batch start
#iterations: 341
currently lose_sum: 500.95641961693764
time_elpased: 1.637
batch start
#iterations: 342
currently lose_sum: 503.1019722223282
time_elpased: 1.623
batch start
#iterations: 343
currently lose_sum: 502.5577621757984
time_elpased: 1.638
batch start
#iterations: 344
currently lose_sum: 500.87012934684753
time_elpased: 1.659
batch start
#iterations: 345
currently lose_sum: 500.6946141719818
time_elpased: 1.631
batch start
#iterations: 346
currently lose_sum: 499.7455649971962
time_elpased: 1.696
batch start
#iterations: 347
currently lose_sum: 499.9212906062603
time_elpased: 1.635
batch start
#iterations: 348
currently lose_sum: 501.34278190135956
time_elpased: 1.647
batch start
#iterations: 349
currently lose_sum: 502.65358105301857
time_elpased: 1.66
batch start
#iterations: 350
currently lose_sum: 501.60236713290215
time_elpased: 1.65
batch start
#iterations: 351
currently lose_sum: 499.9344016313553
time_elpased: 1.718
batch start
#iterations: 352
currently lose_sum: 501.7609557211399
time_elpased: 1.684
batch start
#iterations: 353
currently lose_sum: 501.6010274589062
time_elpased: 1.642
batch start
#iterations: 354
currently lose_sum: 500.80851900577545
time_elpased: 1.677
batch start
#iterations: 355
currently lose_sum: 501.8183521926403
time_elpased: 1.625
batch start
#iterations: 356
currently lose_sum: 501.79141730070114
time_elpased: 1.636
batch start
#iterations: 357
currently lose_sum: 501.3916247189045
time_elpased: 1.677
batch start
#iterations: 358
currently lose_sum: 502.3373776972294
time_elpased: 1.631
batch start
#iterations: 359
currently lose_sum: 500.4000471532345
time_elpased: 1.643
start validation test
0.20175257732
1.0
0.20175257732
0.335763918675
0
validation finish
batch start
#iterations: 360
currently lose_sum: 502.31956458091736
time_elpased: 1.697
batch start
#iterations: 361
currently lose_sum: 498.0421586036682
time_elpased: 1.717
batch start
#iterations: 362
currently lose_sum: 500.2283811867237
time_elpased: 1.713
batch start
#iterations: 363
currently lose_sum: 500.9870794415474
time_elpased: 1.678
batch start
#iterations: 364
currently lose_sum: 502.1773375570774
time_elpased: 1.691
batch start
#iterations: 365
currently lose_sum: 501.46473610401154
time_elpased: 1.649
batch start
#iterations: 366
currently lose_sum: 501.0063427090645
time_elpased: 1.635
batch start
#iterations: 367
currently lose_sum: 503.34301272034645
time_elpased: 1.718
batch start
#iterations: 368
currently lose_sum: 497.71482130885124
time_elpased: 1.72
batch start
#iterations: 369
currently lose_sum: 501.04421865940094
time_elpased: 1.641
batch start
#iterations: 370
currently lose_sum: 500.33379808068275
time_elpased: 1.672
batch start
#iterations: 371
currently lose_sum: 501.8488285243511
time_elpased: 1.666
batch start
#iterations: 372
currently lose_sum: 501.69673371315
time_elpased: 1.634
batch start
#iterations: 373
currently lose_sum: 502.2249296605587
time_elpased: 1.677
batch start
#iterations: 374
currently lose_sum: 502.79628598690033
time_elpased: 1.64
batch start
#iterations: 375
currently lose_sum: 503.50337049365044
time_elpased: 1.641
batch start
#iterations: 376
currently lose_sum: 502.57579135894775
time_elpased: 1.651
batch start
#iterations: 377
currently lose_sum: 500.56600111722946
time_elpased: 1.64
batch start
#iterations: 378
currently lose_sum: 501.9710620045662
time_elpased: 1.646
batch start
#iterations: 379
currently lose_sum: 498.8032941520214
time_elpased: 1.634
start validation test
0.194845360825
1.0
0.194845360825
0.32614322692
0
validation finish
batch start
#iterations: 380
currently lose_sum: 502.54996675252914
time_elpased: 1.651
batch start
#iterations: 381
currently lose_sum: 500.444286942482
time_elpased: 1.627
batch start
#iterations: 382
currently lose_sum: 501.80696681141853
time_elpased: 1.635
batch start
#iterations: 383
currently lose_sum: 499.38440042734146
time_elpased: 1.64
batch start
#iterations: 384
currently lose_sum: 499.63608103990555
time_elpased: 1.621
batch start
#iterations: 385
currently lose_sum: 499.90442898869514
time_elpased: 1.723
batch start
#iterations: 386
currently lose_sum: 501.42988511919975
time_elpased: 1.711
batch start
#iterations: 387
currently lose_sum: 500.71144872903824
time_elpased: 1.641
batch start
#iterations: 388
currently lose_sum: 501.63281378149986
time_elpased: 1.634
batch start
#iterations: 389
currently lose_sum: 501.1517373621464
time_elpased: 1.654
batch start
#iterations: 390
currently lose_sum: 501.43375620245934
time_elpased: 1.64
batch start
#iterations: 391
currently lose_sum: 501.53455024957657
time_elpased: 1.643
batch start
#iterations: 392
currently lose_sum: 501.1816314160824
time_elpased: 1.727
batch start
#iterations: 393
currently lose_sum: 500.2431769669056
time_elpased: 1.669
batch start
#iterations: 394
currently lose_sum: 501.69639521837234
time_elpased: 1.641
batch start
#iterations: 395
currently lose_sum: 500.55845403671265
time_elpased: 1.706
batch start
#iterations: 396
currently lose_sum: 501.902135938406
time_elpased: 1.63
batch start
#iterations: 397
currently lose_sum: 502.98620799183846
time_elpased: 1.65
batch start
#iterations: 398
currently lose_sum: 500.19623270630836
time_elpased: 1.638
batch start
#iterations: 399
currently lose_sum: 499.88279151916504
time_elpased: 1.635
start validation test
0.16793814433
1.0
0.16793814433
0.287580545503
0
validation finish
acc: 0.276
pre: 1.000
rec: 0.276
F1: 0.432
auc: 0.000
