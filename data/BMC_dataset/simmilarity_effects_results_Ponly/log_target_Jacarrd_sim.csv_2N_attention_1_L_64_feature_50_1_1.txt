start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 556.7613548636436
time_elpased: 1.803
batch start
#iterations: 1
currently lose_sum: 538.7703026533127
time_elpased: 1.773
batch start
#iterations: 2
currently lose_sum: 532.9391018152237
time_elpased: 1.769
batch start
#iterations: 3
currently lose_sum: 527.3176190853119
time_elpased: 1.768
batch start
#iterations: 4
currently lose_sum: 524.2096282243729
time_elpased: 1.759
batch start
#iterations: 5
currently lose_sum: 524.3417107462883
time_elpased: 1.795
batch start
#iterations: 6
currently lose_sum: 519.9899915456772
time_elpased: 1.83
batch start
#iterations: 7
currently lose_sum: 519.9868896007538
time_elpased: 1.799
batch start
#iterations: 8
currently lose_sum: 517.893846988678
time_elpased: 1.765
batch start
#iterations: 9
currently lose_sum: 518.1591321527958
time_elpased: 1.806
batch start
#iterations: 10
currently lose_sum: 516.8589835762978
time_elpased: 1.777
batch start
#iterations: 11
currently lose_sum: 518.9686815142632
time_elpased: 1.775
batch start
#iterations: 12
currently lose_sum: 515.7458527684212
time_elpased: 1.837
batch start
#iterations: 13
currently lose_sum: 515.0000202357769
time_elpased: 1.808
batch start
#iterations: 14
currently lose_sum: 512.7953943312168
time_elpased: 1.762
batch start
#iterations: 15
currently lose_sum: 513.3616645932198
time_elpased: 1.773
batch start
#iterations: 16
currently lose_sum: 514.428877979517
time_elpased: 1.79
batch start
#iterations: 17
currently lose_sum: 511.54533967375755
time_elpased: 1.782
batch start
#iterations: 18
currently lose_sum: 511.97698351740837
time_elpased: 1.779
batch start
#iterations: 19
currently lose_sum: 509.7132420837879
time_elpased: 1.771
start validation test
0.145567010309
1.0
0.145567010309
0.254139668826
0
validation finish
batch start
#iterations: 20
currently lose_sum: 511.0389722287655
time_elpased: 1.778
batch start
#iterations: 21
currently lose_sum: 512.1070980131626
time_elpased: 1.783
batch start
#iterations: 22
currently lose_sum: 511.4071779549122
time_elpased: 1.758
batch start
#iterations: 23
currently lose_sum: 509.5132974386215
time_elpased: 1.827
batch start
#iterations: 24
currently lose_sum: 508.59492623806
time_elpased: 1.764
batch start
#iterations: 25
currently lose_sum: 509.9610760509968
time_elpased: 1.772
batch start
#iterations: 26
currently lose_sum: 510.09544548392296
time_elpased: 1.765
batch start
#iterations: 27
currently lose_sum: 508.4948090016842
time_elpased: 1.779
batch start
#iterations: 28
currently lose_sum: 507.6642626821995
time_elpased: 1.761
batch start
#iterations: 29
currently lose_sum: 508.19965121150017
time_elpased: 1.773
batch start
#iterations: 30
currently lose_sum: 505.86736437678337
time_elpased: 1.785
batch start
#iterations: 31
currently lose_sum: 507.50434014201164
time_elpased: 1.781
batch start
#iterations: 32
currently lose_sum: 508.0836499631405
time_elpased: 1.823
batch start
#iterations: 33
currently lose_sum: 505.1897950768471
time_elpased: 1.773
batch start
#iterations: 34
currently lose_sum: 507.7352715432644
time_elpased: 1.817
batch start
#iterations: 35
currently lose_sum: 505.11138504743576
time_elpased: 1.77
batch start
#iterations: 36
currently lose_sum: 506.5924756228924
time_elpased: 1.774
batch start
#iterations: 37
currently lose_sum: 506.3041822910309
time_elpased: 1.808
batch start
#iterations: 38
currently lose_sum: 506.7168490290642
time_elpased: 1.806
batch start
#iterations: 39
currently lose_sum: 508.5842287540436
time_elpased: 1.776
start validation test
0.212680412371
1.0
0.212680412371
0.350760860325
0
validation finish
batch start
#iterations: 40
currently lose_sum: 506.42150965332985
time_elpased: 1.849
batch start
#iterations: 41
currently lose_sum: 506.75706550478935
time_elpased: 1.822
batch start
#iterations: 42
currently lose_sum: 505.2735912501812
time_elpased: 1.766
batch start
#iterations: 43
currently lose_sum: 504.94471949338913
time_elpased: 1.822
batch start
#iterations: 44
currently lose_sum: 504.18764075636864
time_elpased: 1.824
batch start
#iterations: 45
currently lose_sum: 505.5958632528782
time_elpased: 1.812
batch start
#iterations: 46
currently lose_sum: 505.25615814328194
time_elpased: 1.819
batch start
#iterations: 47
currently lose_sum: 507.3469351232052
time_elpased: 1.779
batch start
#iterations: 48
currently lose_sum: 503.2289581000805
time_elpased: 1.841
batch start
#iterations: 49
currently lose_sum: 503.43997836112976
time_elpased: 1.889
batch start
#iterations: 50
currently lose_sum: 506.56102934479713
time_elpased: 1.789
batch start
#iterations: 51
currently lose_sum: 504.2929185628891
time_elpased: 1.784
batch start
#iterations: 52
currently lose_sum: 504.3893366754055
time_elpased: 1.765
batch start
#iterations: 53
currently lose_sum: 505.36537981033325
time_elpased: 1.777
batch start
#iterations: 54
currently lose_sum: 503.7167420387268
time_elpased: 1.766
batch start
#iterations: 55
currently lose_sum: 504.09134325385094
time_elpased: 1.866
batch start
#iterations: 56
currently lose_sum: 505.22207075357437
time_elpased: 1.781
batch start
#iterations: 57
currently lose_sum: 502.58356788754463
time_elpased: 1.77
batch start
#iterations: 58
currently lose_sum: 502.7333245873451
time_elpased: 1.874
batch start
#iterations: 59
currently lose_sum: 502.7999515533447
time_elpased: 1.827
start validation test
0.242474226804
1.0
0.242474226804
0.390308662463
0
validation finish
batch start
#iterations: 60
currently lose_sum: 504.0838679075241
time_elpased: 1.8
batch start
#iterations: 61
currently lose_sum: 503.96071577072144
time_elpased: 1.778
batch start
#iterations: 62
currently lose_sum: 505.4759123325348
time_elpased: 1.866
batch start
#iterations: 63
currently lose_sum: 503.0441468656063
time_elpased: 1.809
batch start
#iterations: 64
currently lose_sum: 502.42604625225067
time_elpased: 1.768
batch start
#iterations: 65
currently lose_sum: 503.8452672958374
time_elpased: 1.829
batch start
#iterations: 66
currently lose_sum: 503.31700563430786
time_elpased: 1.782
batch start
#iterations: 67
currently lose_sum: 502.22498801350594
time_elpased: 1.781
batch start
#iterations: 68
currently lose_sum: 505.2475577890873
time_elpased: 1.773
batch start
#iterations: 69
currently lose_sum: 503.65036004781723
time_elpased: 1.838
batch start
#iterations: 70
currently lose_sum: 503.7089749276638
time_elpased: 1.839
batch start
#iterations: 71
currently lose_sum: 503.707494109869
time_elpased: 1.784
batch start
#iterations: 72
currently lose_sum: 504.5156361460686
time_elpased: 1.766
batch start
#iterations: 73
currently lose_sum: 503.54342409968376
time_elpased: 1.785
batch start
#iterations: 74
currently lose_sum: 502.77067559957504
time_elpased: 1.785
batch start
#iterations: 75
currently lose_sum: 502.733121663332
time_elpased: 1.766
batch start
#iterations: 76
currently lose_sum: 504.10667592287064
time_elpased: 1.799
batch start
#iterations: 77
currently lose_sum: 502.9263682961464
time_elpased: 1.801
batch start
#iterations: 78
currently lose_sum: 501.748926281929
time_elpased: 1.787
batch start
#iterations: 79
currently lose_sum: 504.5850097835064
time_elpased: 1.887
start validation test
0.252989690722
1.0
0.252989690722
0.403817673194
0
validation finish
batch start
#iterations: 80
currently lose_sum: 502.40526404976845
time_elpased: 1.786
batch start
#iterations: 81
currently lose_sum: 502.81748020648956
time_elpased: 1.831
batch start
#iterations: 82
currently lose_sum: 502.47875741124153
time_elpased: 1.78
batch start
#iterations: 83
currently lose_sum: 503.2548889219761
time_elpased: 1.78
batch start
#iterations: 84
currently lose_sum: 502.5065922141075
time_elpased: 1.823
batch start
#iterations: 85
currently lose_sum: 502.9150467514992
time_elpased: 1.78
batch start
#iterations: 86
currently lose_sum: 503.80164486169815
time_elpased: 1.783
batch start
#iterations: 87
currently lose_sum: 501.08103227615356
time_elpased: 1.77
batch start
#iterations: 88
currently lose_sum: 501.65218472480774
time_elpased: 1.777
batch start
#iterations: 89
currently lose_sum: 504.45670768618584
time_elpased: 1.871
batch start
#iterations: 90
currently lose_sum: 503.1616659462452
time_elpased: 1.774
batch start
#iterations: 91
currently lose_sum: 500.7358793616295
time_elpased: 1.855
batch start
#iterations: 92
currently lose_sum: 502.5153651833534
time_elpased: 1.773
batch start
#iterations: 93
currently lose_sum: 502.69645619392395
time_elpased: 1.761
batch start
#iterations: 94
currently lose_sum: 501.7081326544285
time_elpased: 1.884
batch start
#iterations: 95
currently lose_sum: 502.4895344376564
time_elpased: 1.779
batch start
#iterations: 96
currently lose_sum: 503.38425102829933
time_elpased: 1.871
batch start
#iterations: 97
currently lose_sum: 501.76683589816093
time_elpased: 1.773
batch start
#iterations: 98
currently lose_sum: 503.07861202955246
time_elpased: 1.774
batch start
#iterations: 99
currently lose_sum: 501.9688478410244
time_elpased: 1.815
start validation test
0.157113402062
1.0
0.157113402062
0.271560940841
0
validation finish
batch start
#iterations: 100
currently lose_sum: 502.35284182429314
time_elpased: 1.778
batch start
#iterations: 101
currently lose_sum: 502.2763417363167
time_elpased: 1.81
batch start
#iterations: 102
currently lose_sum: 499.93004497885704
time_elpased: 1.822
batch start
#iterations: 103
currently lose_sum: 500.6427885890007
time_elpased: 1.767
batch start
#iterations: 104
currently lose_sum: 503.6480058133602
time_elpased: 1.788
batch start
#iterations: 105
currently lose_sum: 501.64462503790855
time_elpased: 1.789
batch start
#iterations: 106
currently lose_sum: 504.7331585884094
time_elpased: 1.818
batch start
#iterations: 107
currently lose_sum: 502.90532797574997
time_elpased: 1.81
batch start
#iterations: 108
currently lose_sum: 501.0926330089569
time_elpased: 1.75
batch start
#iterations: 109
currently lose_sum: 501.8231809437275
time_elpased: 1.82
batch start
#iterations: 110
currently lose_sum: 502.1049194037914
time_elpased: 1.825
batch start
#iterations: 111
currently lose_sum: 505.2262974381447
time_elpased: 1.788
batch start
#iterations: 112
currently lose_sum: 501.70195081830025
time_elpased: 1.785
batch start
#iterations: 113
currently lose_sum: 501.4631861150265
time_elpased: 1.823
batch start
#iterations: 114
currently lose_sum: 502.3294737637043
time_elpased: 1.78
batch start
#iterations: 115
currently lose_sum: 503.1896304190159
time_elpased: 1.779
batch start
#iterations: 116
currently lose_sum: 501.7353675663471
time_elpased: 1.826
batch start
#iterations: 117
currently lose_sum: 501.122340798378
time_elpased: 1.806
batch start
#iterations: 118
currently lose_sum: 502.6434378325939
time_elpased: 1.791
batch start
#iterations: 119
currently lose_sum: 502.2231397032738
time_elpased: 1.758
start validation test
0.279175257732
1.0
0.279175257732
0.436492585429
0
validation finish
batch start
#iterations: 120
currently lose_sum: 500.74970948696136
time_elpased: 1.813
batch start
#iterations: 121
currently lose_sum: 500.6882541179657
time_elpased: 1.802
batch start
#iterations: 122
currently lose_sum: 502.3396220803261
time_elpased: 1.845
batch start
#iterations: 123
currently lose_sum: 502.059599339962
time_elpased: 1.862
batch start
#iterations: 124
currently lose_sum: 501.3348562717438
time_elpased: 1.825
batch start
#iterations: 125
currently lose_sum: 504.1762133538723
time_elpased: 1.763
batch start
#iterations: 126
currently lose_sum: 500.813064455986
time_elpased: 1.769
batch start
#iterations: 127
currently lose_sum: 503.92024570703506
time_elpased: 1.788
batch start
#iterations: 128
currently lose_sum: 502.299320936203
time_elpased: 1.8
batch start
#iterations: 129
currently lose_sum: 501.7330877184868
time_elpased: 1.784
batch start
#iterations: 130
currently lose_sum: 501.6092310845852
time_elpased: 1.857
batch start
#iterations: 131
currently lose_sum: 501.83438846468925
time_elpased: 1.775
batch start
#iterations: 132
currently lose_sum: 500.824501901865
time_elpased: 1.776
batch start
#iterations: 133
currently lose_sum: 504.2453581094742
time_elpased: 1.825
batch start
#iterations: 134
currently lose_sum: 502.0694645643234
time_elpased: 1.808
batch start
#iterations: 135
currently lose_sum: 499.69553193449974
time_elpased: 1.782
batch start
#iterations: 136
currently lose_sum: 499.6860830783844
time_elpased: 1.774
batch start
#iterations: 137
currently lose_sum: 500.5213157236576
time_elpased: 1.782
batch start
#iterations: 138
currently lose_sum: 501.3447695672512
time_elpased: 1.834
batch start
#iterations: 139
currently lose_sum: 501.9111131429672
time_elpased: 1.781
start validation test
0.152164948454
1.0
0.152164948454
0.264137437366
0
validation finish
batch start
#iterations: 140
currently lose_sum: 499.5734590291977
time_elpased: 1.798
batch start
#iterations: 141
currently lose_sum: 500.036901563406
time_elpased: 1.789
batch start
#iterations: 142
currently lose_sum: 501.75173956155777
time_elpased: 1.838
batch start
#iterations: 143
currently lose_sum: 501.0136801600456
time_elpased: 1.775
batch start
#iterations: 144
currently lose_sum: 502.9776384830475
time_elpased: 1.782
batch start
#iterations: 145
currently lose_sum: 499.5670281648636
time_elpased: 1.842
batch start
#iterations: 146
currently lose_sum: 502.99609184265137
time_elpased: 1.776
batch start
#iterations: 147
currently lose_sum: 502.35372772812843
time_elpased: 1.761
batch start
#iterations: 148
currently lose_sum: 499.2152124643326
time_elpased: 1.855
batch start
#iterations: 149
currently lose_sum: 503.3416966199875
time_elpased: 1.787
batch start
#iterations: 150
currently lose_sum: 500.09183490276337
time_elpased: 1.861
batch start
#iterations: 151
currently lose_sum: 497.9312453866005
time_elpased: 1.773
batch start
#iterations: 152
currently lose_sum: 501.69725131988525
time_elpased: 1.8
batch start
#iterations: 153
currently lose_sum: 499.2617191672325
time_elpased: 1.763
batch start
#iterations: 154
currently lose_sum: 503.7949478626251
time_elpased: 1.808
batch start
#iterations: 155
currently lose_sum: 499.69118762016296
time_elpased: 1.769
batch start
#iterations: 156
currently lose_sum: 501.7844768166542
time_elpased: 1.849
batch start
#iterations: 157
currently lose_sum: 500.23395425081253
time_elpased: 1.763
batch start
#iterations: 158
currently lose_sum: 499.4652456343174
time_elpased: 1.855
batch start
#iterations: 159
currently lose_sum: 502.31390446424484
time_elpased: 1.777
start validation test
0.240206185567
1.0
0.240206185567
0.387364921031
0
validation finish
batch start
#iterations: 160
currently lose_sum: 502.16189739108086
time_elpased: 1.772
batch start
#iterations: 161
currently lose_sum: 502.64558893442154
time_elpased: 1.81
batch start
#iterations: 162
currently lose_sum: 500.16684821248055
time_elpased: 1.822
batch start
#iterations: 163
currently lose_sum: 498.58336585760117
time_elpased: 1.851
batch start
#iterations: 164
currently lose_sum: 501.9383346438408
time_elpased: 1.777
batch start
#iterations: 165
currently lose_sum: 502.50939899683
time_elpased: 1.778
batch start
#iterations: 166
currently lose_sum: 502.8560347855091
time_elpased: 1.768
batch start
#iterations: 167
currently lose_sum: 500.8518676459789
time_elpased: 1.839
batch start
#iterations: 168
currently lose_sum: 501.91029581427574
time_elpased: 1.782
batch start
#iterations: 169
currently lose_sum: 500.32623744010925
time_elpased: 1.893
batch start
#iterations: 170
currently lose_sum: 500.7148250937462
time_elpased: 1.842
batch start
#iterations: 171
currently lose_sum: 499.3703556060791
time_elpased: 1.889
batch start
#iterations: 172
currently lose_sum: 504.1276161968708
time_elpased: 1.769
batch start
#iterations: 173
currently lose_sum: 500.9228774011135
time_elpased: 1.771
batch start
#iterations: 174
currently lose_sum: 501.5427649319172
time_elpased: 1.83
batch start
#iterations: 175
currently lose_sum: 500.8468864560127
time_elpased: 1.775
batch start
#iterations: 176
currently lose_sum: 500.32956686615944
time_elpased: 1.815
batch start
#iterations: 177
currently lose_sum: 500.02579736709595
time_elpased: 1.777
batch start
#iterations: 178
currently lose_sum: 501.0856372117996
time_elpased: 1.775
batch start
#iterations: 179
currently lose_sum: 501.2417382597923
time_elpased: 1.773
start validation test
0.230412371134
1.0
0.230412371134
0.374528697109
0
validation finish
batch start
#iterations: 180
currently lose_sum: 500.1528905928135
time_elpased: 1.795
batch start
#iterations: 181
currently lose_sum: 502.120912194252
time_elpased: 1.771
batch start
#iterations: 182
currently lose_sum: 500.981287330389
time_elpased: 1.772
batch start
#iterations: 183
currently lose_sum: 501.43985003232956
time_elpased: 1.764
batch start
#iterations: 184
currently lose_sum: 500.2738316357136
time_elpased: 1.811
batch start
#iterations: 185
currently lose_sum: 501.13848027586937
time_elpased: 1.79
batch start
#iterations: 186
currently lose_sum: 502.11090695858
time_elpased: 1.845
batch start
#iterations: 187
currently lose_sum: 498.99267143011093
time_elpased: 1.84
batch start
#iterations: 188
currently lose_sum: 500.52327436208725
time_elpased: 1.774
batch start
#iterations: 189
currently lose_sum: 502.16593298316
time_elpased: 1.794
batch start
#iterations: 190
currently lose_sum: 500.80686634778976
time_elpased: 1.859
batch start
#iterations: 191
currently lose_sum: 501.2892087697983
time_elpased: 1.789
batch start
#iterations: 192
currently lose_sum: 500.0477266907692
time_elpased: 1.835
batch start
#iterations: 193
currently lose_sum: 500.4578041434288
time_elpased: 1.782
batch start
#iterations: 194
currently lose_sum: 502.8056454360485
time_elpased: 1.841
batch start
#iterations: 195
currently lose_sum: 500.09382647275925
time_elpased: 1.814
batch start
#iterations: 196
currently lose_sum: 498.6083402335644
time_elpased: 1.808
batch start
#iterations: 197
currently lose_sum: 502.4517341554165
time_elpased: 1.771
batch start
#iterations: 198
currently lose_sum: 500.39670968055725
time_elpased: 1.781
batch start
#iterations: 199
currently lose_sum: 499.0009487271309
time_elpased: 1.839
start validation test
0.244845360825
1.0
0.244845360825
0.393374741201
0
validation finish
batch start
#iterations: 200
currently lose_sum: 500.60288763046265
time_elpased: 1.772
batch start
#iterations: 201
currently lose_sum: 499.35571986436844
time_elpased: 1.867
batch start
#iterations: 202
currently lose_sum: 500.75688049197197
time_elpased: 1.787
batch start
#iterations: 203
currently lose_sum: 500.1517573595047
time_elpased: 1.828
batch start
#iterations: 204
currently lose_sum: 499.701993227005
time_elpased: 1.778
batch start
#iterations: 205
currently lose_sum: 500.86823773384094
time_elpased: 1.85
batch start
#iterations: 206
currently lose_sum: 502.6127293407917
time_elpased: 1.78
batch start
#iterations: 207
currently lose_sum: 501.35002771019936
time_elpased: 1.814
batch start
#iterations: 208
currently lose_sum: 499.95339179039
time_elpased: 1.765
batch start
#iterations: 209
currently lose_sum: 499.69687658548355
time_elpased: 1.769
batch start
#iterations: 210
currently lose_sum: 499.64698854088783
time_elpased: 1.776
batch start
#iterations: 211
currently lose_sum: 501.7426235675812
time_elpased: 1.797
batch start
#iterations: 212
currently lose_sum: 500.89930775761604
time_elpased: 1.791
batch start
#iterations: 213
currently lose_sum: 500.4036069512367
time_elpased: 1.769
batch start
#iterations: 214
currently lose_sum: 501.3816749751568
time_elpased: 1.829
batch start
#iterations: 215
currently lose_sum: 501.19556161761284
time_elpased: 1.77
batch start
#iterations: 216
currently lose_sum: 499.9573165476322
time_elpased: 1.785
batch start
#iterations: 217
currently lose_sum: 500.22077667713165
time_elpased: 1.773
batch start
#iterations: 218
currently lose_sum: 500.3889503479004
time_elpased: 1.785
batch start
#iterations: 219
currently lose_sum: 500.99901700019836
time_elpased: 1.8
start validation test
0.21618556701
1.0
0.21618556701
0.355514113758
0
validation finish
batch start
#iterations: 220
currently lose_sum: 499.2353399693966
time_elpased: 1.78
batch start
#iterations: 221
currently lose_sum: 502.63108292222023
time_elpased: 1.792
batch start
#iterations: 222
currently lose_sum: 500.26374143362045
time_elpased: 1.861
batch start
#iterations: 223
currently lose_sum: 499.5765805840492
time_elpased: 1.876
batch start
#iterations: 224
currently lose_sum: 502.5233803987503
time_elpased: 1.813
batch start
#iterations: 225
currently lose_sum: 500.9254811704159
time_elpased: 1.789
batch start
#iterations: 226
currently lose_sum: 501.6079849600792
time_elpased: 1.766
batch start
#iterations: 227
currently lose_sum: 500.84436240792274
time_elpased: 1.779
batch start
#iterations: 228
currently lose_sum: 498.1718564927578
time_elpased: 1.792
batch start
#iterations: 229
currently lose_sum: 500.79539838433266
time_elpased: 1.872
batch start
#iterations: 230
currently lose_sum: 501.50096118450165
time_elpased: 1.775
batch start
#iterations: 231
currently lose_sum: 502.0706504881382
time_elpased: 1.774
batch start
#iterations: 232
currently lose_sum: 496.28549975156784
time_elpased: 1.784
batch start
#iterations: 233
currently lose_sum: 498.73351898789406
time_elpased: 1.798
batch start
#iterations: 234
currently lose_sum: 500.62942257523537
time_elpased: 1.751
batch start
#iterations: 235
currently lose_sum: 500.78969100117683
time_elpased: 1.791
batch start
#iterations: 236
currently lose_sum: 500.726103335619
time_elpased: 1.785
batch start
#iterations: 237
currently lose_sum: 500.7674257159233
time_elpased: 1.763
batch start
#iterations: 238
currently lose_sum: 498.7368711233139
time_elpased: 1.79
batch start
#iterations: 239
currently lose_sum: 497.9745565056801
time_elpased: 1.793
start validation test
0.204536082474
1.0
0.204536082474
0.339609722698
0
validation finish
batch start
#iterations: 240
currently lose_sum: 497.69063383340836
time_elpased: 1.796
batch start
#iterations: 241
currently lose_sum: 498.88033697009087
time_elpased: 1.809
batch start
#iterations: 242
currently lose_sum: 497.94157499074936
time_elpased: 1.803
batch start
#iterations: 243
currently lose_sum: 500.0332931280136
time_elpased: 1.774
batch start
#iterations: 244
currently lose_sum: 499.96894642710686
time_elpased: 1.78
batch start
#iterations: 245
currently lose_sum: 499.9691231250763
time_elpased: 1.766
batch start
#iterations: 246
currently lose_sum: 500.20426017045975
time_elpased: 1.818
batch start
#iterations: 247
currently lose_sum: 499.9732454717159
time_elpased: 1.808
batch start
#iterations: 248
currently lose_sum: 499.7659908235073
time_elpased: 1.776
batch start
#iterations: 249
currently lose_sum: 501.0017591714859
time_elpased: 1.779
batch start
#iterations: 250
currently lose_sum: 499.13445004820824
time_elpased: 1.835
batch start
#iterations: 251
currently lose_sum: 499.8369152843952
time_elpased: 1.777
batch start
#iterations: 252
currently lose_sum: 501.2038543820381
time_elpased: 1.778
batch start
#iterations: 253
currently lose_sum: 498.12671944499016
time_elpased: 1.829
batch start
#iterations: 254
currently lose_sum: 501.7159558534622
time_elpased: 1.766
batch start
#iterations: 255
currently lose_sum: 500.64718121290207
time_elpased: 1.775
batch start
#iterations: 256
currently lose_sum: 499.19607239961624
time_elpased: 1.815
batch start
#iterations: 257
currently lose_sum: 499.0999017059803
time_elpased: 1.794
batch start
#iterations: 258
currently lose_sum: 499.50430166721344
time_elpased: 1.776
batch start
#iterations: 259
currently lose_sum: 501.09940192103386
time_elpased: 1.776
start validation test
0.259381443299
1.0
0.259381443299
0.411918795023
0
validation finish
batch start
#iterations: 260
currently lose_sum: 501.8024328649044
time_elpased: 1.793
batch start
#iterations: 261
currently lose_sum: 501.2651553452015
time_elpased: 1.78
batch start
#iterations: 262
currently lose_sum: 500.5454679131508
time_elpased: 1.769
batch start
#iterations: 263
currently lose_sum: 500.19398349523544
time_elpased: 1.776
batch start
#iterations: 264
currently lose_sum: 499.2271193265915
time_elpased: 1.88
batch start
#iterations: 265
currently lose_sum: 499.25978952646255
time_elpased: 1.778
batch start
#iterations: 266
currently lose_sum: 500.5537522137165
time_elpased: 1.776
batch start
#iterations: 267
currently lose_sum: 499.39492455124855
time_elpased: 1.796
batch start
#iterations: 268
currently lose_sum: 500.5175857245922
time_elpased: 1.777
batch start
#iterations: 269
currently lose_sum: 501.0928239226341
time_elpased: 1.777
batch start
#iterations: 270
currently lose_sum: 499.7463579773903
time_elpased: 1.792
batch start
#iterations: 271
currently lose_sum: 496.448871165514
time_elpased: 1.862
batch start
#iterations: 272
currently lose_sum: 500.75947961211205
time_elpased: 1.796
batch start
#iterations: 273
currently lose_sum: 497.9848052561283
time_elpased: 1.875
batch start
#iterations: 274
currently lose_sum: 500.3430155515671
time_elpased: 1.784
batch start
#iterations: 275
currently lose_sum: 499.92596009373665
time_elpased: 1.782
batch start
#iterations: 276
currently lose_sum: 498.4586134850979
time_elpased: 1.78
batch start
#iterations: 277
currently lose_sum: 501.8764178156853
time_elpased: 1.854
batch start
#iterations: 278
currently lose_sum: 501.0883811712265
time_elpased: 1.835
batch start
#iterations: 279
currently lose_sum: 499.2834752500057
time_elpased: 1.774
start validation test
0.24
1.0
0.24
0.387096774194
0
validation finish
batch start
#iterations: 280
currently lose_sum: 498.8621417284012
time_elpased: 1.773
batch start
#iterations: 281
currently lose_sum: 500.5575920343399
time_elpased: 1.779
batch start
#iterations: 282
currently lose_sum: 500.1519324183464
time_elpased: 1.886
batch start
#iterations: 283
currently lose_sum: 499.33906686306
time_elpased: 1.778
batch start
#iterations: 284
currently lose_sum: 500.2570119202137
time_elpased: 1.884
batch start
#iterations: 285
currently lose_sum: 499.9490946829319
time_elpased: 1.769
batch start
#iterations: 286
currently lose_sum: 500.2948280274868
time_elpased: 1.832
batch start
#iterations: 287
currently lose_sum: 500.448343873024
time_elpased: 1.858
batch start
#iterations: 288
currently lose_sum: 499.31050449609756
time_elpased: 1.806
batch start
#iterations: 289
currently lose_sum: 499.7344301342964
time_elpased: 1.78
batch start
#iterations: 290
currently lose_sum: 498.78905937075615
time_elpased: 1.776
batch start
#iterations: 291
currently lose_sum: 500.37935784459114
time_elpased: 1.796
batch start
#iterations: 292
currently lose_sum: 498.63556203246117
time_elpased: 1.805
batch start
#iterations: 293
currently lose_sum: 499.7412083745003
time_elpased: 1.785
batch start
#iterations: 294
currently lose_sum: 500.12191608548164
time_elpased: 1.835
batch start
#iterations: 295
currently lose_sum: 499.5303670167923
time_elpased: 1.852
batch start
#iterations: 296
currently lose_sum: 497.29823195934296
time_elpased: 1.832
batch start
#iterations: 297
currently lose_sum: 499.27424070239067
time_elpased: 1.851
batch start
#iterations: 298
currently lose_sum: 498.99613907933235
time_elpased: 1.79
batch start
#iterations: 299
currently lose_sum: 499.4532741904259
time_elpased: 1.774
start validation test
0.234020618557
1.0
0.234020618557
0.379281537176
0
validation finish
batch start
#iterations: 300
currently lose_sum: 497.72618383169174
time_elpased: 1.796
batch start
#iterations: 301
currently lose_sum: 499.95847541093826
time_elpased: 1.803
batch start
#iterations: 302
currently lose_sum: 501.0239078104496
time_elpased: 1.837
batch start
#iterations: 303
currently lose_sum: 498.92860984802246
time_elpased: 1.778
batch start
#iterations: 304
currently lose_sum: 500.3575311899185
time_elpased: 1.775
batch start
#iterations: 305
currently lose_sum: 500.81986650824547
time_elpased: 1.776
batch start
#iterations: 306
currently lose_sum: 499.54022097587585
time_elpased: 1.78
batch start
#iterations: 307
currently lose_sum: 502.38814228773117
time_elpased: 1.814
batch start
#iterations: 308
currently lose_sum: 499.673985093832
time_elpased: 1.776
batch start
#iterations: 309
currently lose_sum: 501.59424206614494
time_elpased: 1.801
batch start
#iterations: 310
currently lose_sum: 497.5024642050266
time_elpased: 1.771
batch start
#iterations: 311
currently lose_sum: 498.54047629237175
time_elpased: 1.828
batch start
#iterations: 312
currently lose_sum: 499.3543897867203
time_elpased: 1.767
batch start
#iterations: 313
currently lose_sum: 500.75738593935966
time_elpased: 1.868
batch start
#iterations: 314
currently lose_sum: 498.68557664752007
time_elpased: 1.793
batch start
#iterations: 315
currently lose_sum: 499.4124626815319
time_elpased: 1.795
batch start
#iterations: 316
currently lose_sum: 499.6291497051716
time_elpased: 1.77
batch start
#iterations: 317
currently lose_sum: 498.49691435694695
time_elpased: 1.78
batch start
#iterations: 318
currently lose_sum: 501.72341525554657
time_elpased: 1.884
batch start
#iterations: 319
currently lose_sum: 500.0018741488457
time_elpased: 1.801
start validation test
0.229793814433
1.0
0.229793814433
0.373711124151
0
validation finish
batch start
#iterations: 320
currently lose_sum: 500.30335560441017
time_elpased: 1.869
batch start
#iterations: 321
currently lose_sum: 498.01513797044754
time_elpased: 1.822
batch start
#iterations: 322
currently lose_sum: 498.61505779623985
time_elpased: 1.771
batch start
#iterations: 323
currently lose_sum: 499.39276054501534
time_elpased: 1.776
batch start
#iterations: 324
currently lose_sum: 501.60186383128166
time_elpased: 1.788
batch start
#iterations: 325
currently lose_sum: 500.85739400982857
time_elpased: 1.857
batch start
#iterations: 326
currently lose_sum: 501.0142365396023
time_elpased: 1.785
batch start
#iterations: 327
currently lose_sum: 499.8970058262348
time_elpased: 1.831
batch start
#iterations: 328
currently lose_sum: 498.5561239719391
time_elpased: 1.785
batch start
#iterations: 329
currently lose_sum: 499.99739292263985
time_elpased: 1.782
batch start
#iterations: 330
currently lose_sum: 500.96994799375534
time_elpased: 1.784
batch start
#iterations: 331
currently lose_sum: 501.97457841038704
time_elpased: 1.788
batch start
#iterations: 332
currently lose_sum: 500.0247113108635
time_elpased: 1.868
batch start
#iterations: 333
currently lose_sum: 501.5155097544193
time_elpased: 1.771
batch start
#iterations: 334
currently lose_sum: 500.42860546708107
time_elpased: 1.83
batch start
#iterations: 335
currently lose_sum: 497.8260481953621
time_elpased: 1.793
batch start
#iterations: 336
currently lose_sum: 499.7991981804371
time_elpased: 1.778
batch start
#iterations: 337
currently lose_sum: 499.1893775165081
time_elpased: 1.787
batch start
#iterations: 338
currently lose_sum: 500.30287355184555
time_elpased: 1.772
batch start
#iterations: 339
currently lose_sum: 498.66866463422775
time_elpased: 1.799
start validation test
0.282268041237
1.0
0.282268041237
0.440263707992
0
validation finish
batch start
#iterations: 340
currently lose_sum: 500.10649371147156
time_elpased: 1.814
batch start
#iterations: 341
currently lose_sum: 500.5562820136547
time_elpased: 1.839
batch start
#iterations: 342
currently lose_sum: 498.0135029554367
time_elpased: 1.783
batch start
#iterations: 343
currently lose_sum: 499.45853981375694
time_elpased: 1.767
batch start
#iterations: 344
currently lose_sum: 500.34856110811234
time_elpased: 1.772
batch start
#iterations: 345
currently lose_sum: 500.265348136425
time_elpased: 1.777
batch start
#iterations: 346
currently lose_sum: 498.4242108166218
time_elpased: 1.783
batch start
#iterations: 347
currently lose_sum: 499.1195611357689
time_elpased: 1.782
batch start
#iterations: 348
currently lose_sum: 501.26481434702873
time_elpased: 1.92
batch start
#iterations: 349
currently lose_sum: 501.1258362531662
time_elpased: 1.83
batch start
#iterations: 350
currently lose_sum: 500.6227413713932
time_elpased: 1.779
batch start
#iterations: 351
currently lose_sum: 499.06516551971436
time_elpased: 1.793
batch start
#iterations: 352
currently lose_sum: 499.26392874121666
time_elpased: 1.783
batch start
#iterations: 353
currently lose_sum: 500.721165984869
time_elpased: 1.785
batch start
#iterations: 354
currently lose_sum: 498.43024253845215
time_elpased: 1.772
batch start
#iterations: 355
currently lose_sum: 500.14971882104874
time_elpased: 1.796
batch start
#iterations: 356
currently lose_sum: 500.2848326563835
time_elpased: 1.808
batch start
#iterations: 357
currently lose_sum: 499.86406949162483
time_elpased: 1.821
batch start
#iterations: 358
currently lose_sum: 499.63556146621704
time_elpased: 1.797
batch start
#iterations: 359
currently lose_sum: 499.12100461125374
time_elpased: 1.865
start validation test
0.211030927835
1.0
0.211030927835
0.348514514344
0
validation finish
batch start
#iterations: 360
currently lose_sum: 500.21726259589195
time_elpased: 1.849
batch start
#iterations: 361
currently lose_sum: 498.2381282746792
time_elpased: 1.788
batch start
#iterations: 362
currently lose_sum: 499.8761070072651
time_elpased: 1.795
batch start
#iterations: 363
currently lose_sum: 498.83710846304893
time_elpased: 1.788
batch start
#iterations: 364
currently lose_sum: 499.21327248215675
time_elpased: 1.854
batch start
#iterations: 365
currently lose_sum: 499.5026140511036
time_elpased: 1.775
batch start
#iterations: 366
currently lose_sum: 500.99272111058235
time_elpased: 1.855
batch start
#iterations: 367
currently lose_sum: 501.0389356613159
time_elpased: 1.807
batch start
#iterations: 368
currently lose_sum: 497.39967703819275
time_elpased: 1.803
batch start
#iterations: 369
currently lose_sum: 497.88228034973145
time_elpased: 1.783
batch start
#iterations: 370
currently lose_sum: 497.96265798807144
time_elpased: 1.841
batch start
#iterations: 371
currently lose_sum: 500.01889112591743
time_elpased: 1.793
batch start
#iterations: 372
currently lose_sum: 499.50829339027405
time_elpased: 1.833
batch start
#iterations: 373
currently lose_sum: 499.29387763142586
time_elpased: 1.791
batch start
#iterations: 374
currently lose_sum: 500.667425096035
time_elpased: 1.78
batch start
#iterations: 375
currently lose_sum: 499.4610389471054
time_elpased: 1.791
batch start
#iterations: 376
currently lose_sum: 499.41415479779243
time_elpased: 1.769
batch start
#iterations: 377
currently lose_sum: 500.39325788617134
time_elpased: 1.792
batch start
#iterations: 378
currently lose_sum: 500.16967698931694
time_elpased: 1.768
batch start
#iterations: 379
currently lose_sum: 499.0421142876148
time_elpased: 1.803
start validation test
0.206804123711
1.0
0.206804123711
0.342730223817
0
validation finish
batch start
#iterations: 380
currently lose_sum: 500.1163476407528
time_elpased: 1.808
batch start
#iterations: 381
currently lose_sum: 498.6844790279865
time_elpased: 1.819
batch start
#iterations: 382
currently lose_sum: 499.8404080569744
time_elpased: 1.776
batch start
#iterations: 383
currently lose_sum: 497.4146126806736
time_elpased: 1.812
batch start
#iterations: 384
currently lose_sum: 498.45438212156296
time_elpased: 1.784
batch start
#iterations: 385
currently lose_sum: 497.8006649017334
time_elpased: 1.773
batch start
#iterations: 386
currently lose_sum: 500.17495012283325
time_elpased: 1.772
batch start
#iterations: 387
currently lose_sum: 500.340273052454
time_elpased: 1.778
batch start
#iterations: 388
currently lose_sum: 500.0670757293701
time_elpased: 1.783
batch start
#iterations: 389
currently lose_sum: 497.78340581059456
time_elpased: 1.809
batch start
#iterations: 390
currently lose_sum: 497.99312949180603
time_elpased: 1.821
batch start
#iterations: 391
currently lose_sum: 500.6484929025173
time_elpased: 1.829
batch start
#iterations: 392
currently lose_sum: 499.17437785863876
time_elpased: 1.806
batch start
#iterations: 393
currently lose_sum: 499.24159449338913
time_elpased: 1.813
batch start
#iterations: 394
currently lose_sum: 498.7882045507431
time_elpased: 1.789
batch start
#iterations: 395
currently lose_sum: 498.5723279416561
time_elpased: 1.892
batch start
#iterations: 396
currently lose_sum: 499.3979899287224
time_elpased: 1.802
batch start
#iterations: 397
currently lose_sum: 499.6710898280144
time_elpased: 1.791
batch start
#iterations: 398
currently lose_sum: 499.613990008831
time_elpased: 1.823
batch start
#iterations: 399
currently lose_sum: 500.0497645139694
time_elpased: 1.787
start validation test
0.195979381443
1.0
0.195979381443
0.327730368072
0
validation finish
acc: 0.275
pre: 1.000
rec: 0.275
F1: 0.432
auc: 0.000
