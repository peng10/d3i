start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 555.64711779356
time_elpased: 1.645
batch start
#iterations: 1
currently lose_sum: 537.2741804122925
time_elpased: 1.601
batch start
#iterations: 2
currently lose_sum: 529.8220899701118
time_elpased: 1.593
batch start
#iterations: 3
currently lose_sum: 525.3137511312962
time_elpased: 1.611
batch start
#iterations: 4
currently lose_sum: 521.2472307682037
time_elpased: 1.592
batch start
#iterations: 5
currently lose_sum: 519.8489156961441
time_elpased: 1.603
batch start
#iterations: 6
currently lose_sum: 518.4705016314983
time_elpased: 1.583
batch start
#iterations: 7
currently lose_sum: 516.5512718558311
time_elpased: 1.589
batch start
#iterations: 8
currently lose_sum: 516.4124698638916
time_elpased: 1.59
batch start
#iterations: 9
currently lose_sum: 517.5933700203896
time_elpased: 1.599
batch start
#iterations: 10
currently lose_sum: 516.5165255665779
time_elpased: 1.599
batch start
#iterations: 11
currently lose_sum: 517.9604876637459
time_elpased: 1.591
batch start
#iterations: 12
currently lose_sum: 516.5618029236794
time_elpased: 1.598
batch start
#iterations: 13
currently lose_sum: 513.9741407036781
time_elpased: 1.602
batch start
#iterations: 14
currently lose_sum: 514.7782990038395
time_elpased: 1.586
batch start
#iterations: 15
currently lose_sum: 512.3290164768696
time_elpased: 1.606
batch start
#iterations: 16
currently lose_sum: 513.034943729639
time_elpased: 1.579
batch start
#iterations: 17
currently lose_sum: 513.2775898873806
time_elpased: 1.602
batch start
#iterations: 18
currently lose_sum: 512.0165441632271
time_elpased: 1.61
batch start
#iterations: 19
currently lose_sum: 511.82422986626625
time_elpased: 1.592
start validation test
0.198144329897
1.0
0.198144329897
0.330752022027
0
validation finish
batch start
#iterations: 20
currently lose_sum: 510.887936681509
time_elpased: 1.603
batch start
#iterations: 21
currently lose_sum: 512.4905207455158
time_elpased: 1.591
batch start
#iterations: 22
currently lose_sum: 511.3158275783062
time_elpased: 1.595
batch start
#iterations: 23
currently lose_sum: 511.35417288541794
time_elpased: 1.6
batch start
#iterations: 24
currently lose_sum: 509.5097629725933
time_elpased: 1.596
batch start
#iterations: 25
currently lose_sum: 510.34157568216324
time_elpased: 1.607
batch start
#iterations: 26
currently lose_sum: 509.47251456975937
time_elpased: 1.593
batch start
#iterations: 27
currently lose_sum: 509.463175624609
time_elpased: 1.6
batch start
#iterations: 28
currently lose_sum: 508.08229342103004
time_elpased: 1.597
batch start
#iterations: 29
currently lose_sum: 508.82902586460114
time_elpased: 1.605
batch start
#iterations: 30
currently lose_sum: 506.63487884402275
time_elpased: 1.598
batch start
#iterations: 31
currently lose_sum: 508.65486735105515
time_elpased: 1.645
batch start
#iterations: 32
currently lose_sum: 508.43417716026306
time_elpased: 1.596
batch start
#iterations: 33
currently lose_sum: 507.91939479112625
time_elpased: 1.602
batch start
#iterations: 34
currently lose_sum: 508.44671005010605
time_elpased: 1.6
batch start
#iterations: 35
currently lose_sum: 506.9266223311424
time_elpased: 1.6
batch start
#iterations: 36
currently lose_sum: 506.9455215036869
time_elpased: 1.609
batch start
#iterations: 37
currently lose_sum: 505.62866872549057
time_elpased: 1.602
batch start
#iterations: 38
currently lose_sum: 506.533310264349
time_elpased: 1.612
batch start
#iterations: 39
currently lose_sum: 508.629850178957
time_elpased: 1.585
start validation test
0.18381443299
1.0
0.18381443299
0.310546024558
0
validation finish
batch start
#iterations: 40
currently lose_sum: 506.97016617655754
time_elpased: 1.591
batch start
#iterations: 41
currently lose_sum: 508.67511561512947
time_elpased: 1.595
batch start
#iterations: 42
currently lose_sum: 505.5370816588402
time_elpased: 1.605
batch start
#iterations: 43
currently lose_sum: 506.29989528656006
time_elpased: 1.594
batch start
#iterations: 44
currently lose_sum: 508.36041510105133
time_elpased: 1.598
batch start
#iterations: 45
currently lose_sum: 508.809623003006
time_elpased: 1.591
batch start
#iterations: 46
currently lose_sum: 504.4319869875908
time_elpased: 1.613
batch start
#iterations: 47
currently lose_sum: 506.353491961956
time_elpased: 1.599
batch start
#iterations: 48
currently lose_sum: 507.05561020970345
time_elpased: 1.618
batch start
#iterations: 49
currently lose_sum: 504.0393747985363
time_elpased: 1.611
batch start
#iterations: 50
currently lose_sum: 507.3009523153305
time_elpased: 1.593
batch start
#iterations: 51
currently lose_sum: 505.559122890234
time_elpased: 1.59
batch start
#iterations: 52
currently lose_sum: 505.42720210552216
time_elpased: 1.606
batch start
#iterations: 53
currently lose_sum: 505.6993373632431
time_elpased: 1.605
batch start
#iterations: 54
currently lose_sum: 505.2704858779907
time_elpased: 1.597
batch start
#iterations: 55
currently lose_sum: 505.7337682545185
time_elpased: 1.61
batch start
#iterations: 56
currently lose_sum: 505.8511272370815
time_elpased: 1.592
batch start
#iterations: 57
currently lose_sum: 505.3111773133278
time_elpased: 1.599
batch start
#iterations: 58
currently lose_sum: 506.5261653661728
time_elpased: 1.579
batch start
#iterations: 59
currently lose_sum: 504.31910866498947
time_elpased: 1.61
start validation test
0.197113402062
1.0
0.197113402062
0.329314502239
0
validation finish
batch start
#iterations: 60
currently lose_sum: 503.9636246263981
time_elpased: 1.599
batch start
#iterations: 61
currently lose_sum: 505.2168092429638
time_elpased: 1.606
batch start
#iterations: 62
currently lose_sum: 507.4632088840008
time_elpased: 1.632
batch start
#iterations: 63
currently lose_sum: 506.65271016955376
time_elpased: 1.595
batch start
#iterations: 64
currently lose_sum: 505.2621938288212
time_elpased: 1.598
batch start
#iterations: 65
currently lose_sum: 506.04073294997215
time_elpased: 1.604
batch start
#iterations: 66
currently lose_sum: 504.4481391310692
time_elpased: 1.602
batch start
#iterations: 67
currently lose_sum: 503.4449734091759
time_elpased: 1.605
batch start
#iterations: 68
currently lose_sum: 505.2556136250496
time_elpased: 1.598
batch start
#iterations: 69
currently lose_sum: 506.8576366305351
time_elpased: 1.612
batch start
#iterations: 70
currently lose_sum: 502.31102615594864
time_elpased: 1.599
batch start
#iterations: 71
currently lose_sum: 505.0957353711128
time_elpased: 1.598
batch start
#iterations: 72
currently lose_sum: 507.05973196029663
time_elpased: 1.605
batch start
#iterations: 73
currently lose_sum: 502.9711152613163
time_elpased: 1.6
batch start
#iterations: 74
currently lose_sum: 505.88468810915947
time_elpased: 1.616
batch start
#iterations: 75
currently lose_sum: 504.64412409067154
time_elpased: 1.603
batch start
#iterations: 76
currently lose_sum: 504.2175452411175
time_elpased: 1.643
batch start
#iterations: 77
currently lose_sum: 504.98308849334717
time_elpased: 1.605
batch start
#iterations: 78
currently lose_sum: 505.3720451295376
time_elpased: 1.602
batch start
#iterations: 79
currently lose_sum: 506.7907265126705
time_elpased: 1.612
start validation test
0.260927835052
1.0
0.260927835052
0.413866405036
0
validation finish
batch start
#iterations: 80
currently lose_sum: 504.49085932970047
time_elpased: 1.599
batch start
#iterations: 81
currently lose_sum: 502.9453343451023
time_elpased: 1.609
batch start
#iterations: 82
currently lose_sum: 504.2695811390877
time_elpased: 1.598
batch start
#iterations: 83
currently lose_sum: 503.50642439723015
time_elpased: 1.599
batch start
#iterations: 84
currently lose_sum: 503.5897034704685
time_elpased: 1.623
batch start
#iterations: 85
currently lose_sum: 502.5555298924446
time_elpased: 1.605
batch start
#iterations: 86
currently lose_sum: 503.51359793543816
time_elpased: 1.59
batch start
#iterations: 87
currently lose_sum: 502.51244485378265
time_elpased: 1.604
batch start
#iterations: 88
currently lose_sum: 504.5893433690071
time_elpased: 1.595
batch start
#iterations: 89
currently lose_sum: 504.52334555983543
time_elpased: 1.585
batch start
#iterations: 90
currently lose_sum: 503.0468811392784
time_elpased: 1.601
batch start
#iterations: 91
currently lose_sum: 503.05225774645805
time_elpased: 1.595
batch start
#iterations: 92
currently lose_sum: 503.2783107161522
time_elpased: 1.598
batch start
#iterations: 93
currently lose_sum: 503.68193462491035
time_elpased: 1.599
batch start
#iterations: 94
currently lose_sum: 503.5454000234604
time_elpased: 1.598
batch start
#iterations: 95
currently lose_sum: 502.6539205610752
time_elpased: 1.611
batch start
#iterations: 96
currently lose_sum: 505.3466140627861
time_elpased: 1.595
batch start
#iterations: 97
currently lose_sum: 501.2116204202175
time_elpased: 1.617
batch start
#iterations: 98
currently lose_sum: 504.1445755958557
time_elpased: 1.583
batch start
#iterations: 99
currently lose_sum: 504.6341824233532
time_elpased: 1.61
start validation test
0.228762886598
1.0
0.228762886598
0.372346673379
0
validation finish
batch start
#iterations: 100
currently lose_sum: 503.65510869026184
time_elpased: 1.613
batch start
#iterations: 101
currently lose_sum: 504.41831400990486
time_elpased: 1.606
batch start
#iterations: 102
currently lose_sum: 501.8609307706356
time_elpased: 1.598
batch start
#iterations: 103
currently lose_sum: 504.84537160396576
time_elpased: 1.614
batch start
#iterations: 104
currently lose_sum: 502.97780495882034
time_elpased: 1.611
batch start
#iterations: 105
currently lose_sum: 502.9743826985359
time_elpased: 1.601
batch start
#iterations: 106
currently lose_sum: 505.38937696814537
time_elpased: 1.628
batch start
#iterations: 107
currently lose_sum: 502.4589821100235
time_elpased: 1.623
batch start
#iterations: 108
currently lose_sum: 501.30732560157776
time_elpased: 1.613
batch start
#iterations: 109
currently lose_sum: 503.32000744342804
time_elpased: 1.602
batch start
#iterations: 110
currently lose_sum: 505.10531201958656
time_elpased: 1.602
batch start
#iterations: 111
currently lose_sum: 505.51255494356155
time_elpased: 1.596
batch start
#iterations: 112
currently lose_sum: 501.9114560484886
time_elpased: 1.613
batch start
#iterations: 113
currently lose_sum: 503.794848382473
time_elpased: 1.614
batch start
#iterations: 114
currently lose_sum: 504.3570222556591
time_elpased: 1.596
batch start
#iterations: 115
currently lose_sum: 503.7483120560646
time_elpased: 1.612
batch start
#iterations: 116
currently lose_sum: 501.855326294899
time_elpased: 1.602
batch start
#iterations: 117
currently lose_sum: 504.5270431637764
time_elpased: 1.611
batch start
#iterations: 118
currently lose_sum: 502.641341149807
time_elpased: 1.614
batch start
#iterations: 119
currently lose_sum: 503.2649495303631
time_elpased: 1.601
start validation test
0.230412371134
1.0
0.230412371134
0.374528697109
0
validation finish
batch start
#iterations: 120
currently lose_sum: 503.489983022213
time_elpased: 1.624
batch start
#iterations: 121
currently lose_sum: 501.25797083973885
time_elpased: 1.613
batch start
#iterations: 122
currently lose_sum: 502.2277580201626
time_elpased: 1.608
batch start
#iterations: 123
currently lose_sum: 501.59578865766525
time_elpased: 1.608
batch start
#iterations: 124
currently lose_sum: 502.09972563385963
time_elpased: 1.611
batch start
#iterations: 125
currently lose_sum: 503.44777965545654
time_elpased: 1.618
batch start
#iterations: 126
currently lose_sum: 503.38389214873314
time_elpased: 1.607
batch start
#iterations: 127
currently lose_sum: 503.63408625125885
time_elpased: 1.598
batch start
#iterations: 128
currently lose_sum: 503.13513669371605
time_elpased: 1.598
batch start
#iterations: 129
currently lose_sum: 502.9413057565689
time_elpased: 1.618
batch start
#iterations: 130
currently lose_sum: 503.120148986578
time_elpased: 1.629
batch start
#iterations: 131
currently lose_sum: 503.5242493748665
time_elpased: 1.59
batch start
#iterations: 132
currently lose_sum: 502.6643747985363
time_elpased: 1.6
batch start
#iterations: 133
currently lose_sum: 504.28935101628304
time_elpased: 1.6
batch start
#iterations: 134
currently lose_sum: 502.37286111712456
time_elpased: 1.62
batch start
#iterations: 135
currently lose_sum: 501.3766559958458
time_elpased: 1.609
batch start
#iterations: 136
currently lose_sum: 502.5385521352291
time_elpased: 1.61
batch start
#iterations: 137
currently lose_sum: 501.548131108284
time_elpased: 1.611
batch start
#iterations: 138
currently lose_sum: 499.55278584361076
time_elpased: 1.597
batch start
#iterations: 139
currently lose_sum: 501.99788627028465
time_elpased: 1.602
start validation test
0.190721649485
1.0
0.190721649485
0.320346320346
0
validation finish
batch start
#iterations: 140
currently lose_sum: 500.7109743952751
time_elpased: 1.606
batch start
#iterations: 141
currently lose_sum: 502.12344071269035
time_elpased: 1.613
batch start
#iterations: 142
currently lose_sum: 499.29053550958633
time_elpased: 1.597
batch start
#iterations: 143
currently lose_sum: 502.2229368686676
time_elpased: 1.611
batch start
#iterations: 144
currently lose_sum: 501.4411934018135
time_elpased: 1.621
batch start
#iterations: 145
currently lose_sum: 503.63330975174904
time_elpased: 1.597
batch start
#iterations: 146
currently lose_sum: 503.74606105685234
time_elpased: 1.61
batch start
#iterations: 147
currently lose_sum: 503.2159883379936
time_elpased: 1.618
batch start
#iterations: 148
currently lose_sum: 500.75212931632996
time_elpased: 1.593
batch start
#iterations: 149
currently lose_sum: 503.4161017537117
time_elpased: 1.623
batch start
#iterations: 150
currently lose_sum: 502.16110199689865
time_elpased: 1.611
batch start
#iterations: 151
currently lose_sum: 500.1038558781147
time_elpased: 1.605
batch start
#iterations: 152
currently lose_sum: 500.937603443861
time_elpased: 1.62
batch start
#iterations: 153
currently lose_sum: 499.6885401904583
time_elpased: 1.614
batch start
#iterations: 154
currently lose_sum: 505.4184683263302
time_elpased: 1.613
batch start
#iterations: 155
currently lose_sum: 499.80925422906876
time_elpased: 1.598
batch start
#iterations: 156
currently lose_sum: 502.00533774495125
time_elpased: 1.621
batch start
#iterations: 157
currently lose_sum: 500.81243321299553
time_elpased: 1.619
batch start
#iterations: 158
currently lose_sum: 500.7829154729843
time_elpased: 1.604
batch start
#iterations: 159
currently lose_sum: 501.1593467891216
time_elpased: 1.605
start validation test
0.240515463918
1.0
0.240515463918
0.387766974154
0
validation finish
batch start
#iterations: 160
currently lose_sum: 503.0721185207367
time_elpased: 1.603
batch start
#iterations: 161
currently lose_sum: 500.3373444378376
time_elpased: 1.6
batch start
#iterations: 162
currently lose_sum: 501.78326192498207
time_elpased: 1.601
batch start
#iterations: 163
currently lose_sum: 501.32275488972664
time_elpased: 1.594
batch start
#iterations: 164
currently lose_sum: 502.1631173789501
time_elpased: 1.596
batch start
#iterations: 165
currently lose_sum: 503.68945932388306
time_elpased: 1.605
batch start
#iterations: 166
currently lose_sum: 502.8932281732559
time_elpased: 1.606
batch start
#iterations: 167
currently lose_sum: 501.8176198899746
time_elpased: 1.612
batch start
#iterations: 168
currently lose_sum: 501.6429159939289
time_elpased: 1.597
batch start
#iterations: 169
currently lose_sum: 502.9942527115345
time_elpased: 1.593
batch start
#iterations: 170
currently lose_sum: 500.23273116350174
time_elpased: 1.621
batch start
#iterations: 171
currently lose_sum: 500.9186035692692
time_elpased: 1.601
batch start
#iterations: 172
currently lose_sum: 503.6530704796314
time_elpased: 1.61
batch start
#iterations: 173
currently lose_sum: 502.80389472842216
time_elpased: 1.601
batch start
#iterations: 174
currently lose_sum: 502.30320701003075
time_elpased: 1.6
batch start
#iterations: 175
currently lose_sum: 503.01557344198227
time_elpased: 1.6
batch start
#iterations: 176
currently lose_sum: 500.15739223361015
time_elpased: 1.598
batch start
#iterations: 177
currently lose_sum: 502.06345373392105
time_elpased: 1.615
batch start
#iterations: 178
currently lose_sum: 502.8445674479008
time_elpased: 1.598
batch start
#iterations: 179
currently lose_sum: 503.83709889650345
time_elpased: 1.608
start validation test
0.211237113402
1.0
0.211237113402
0.348795642182
0
validation finish
batch start
#iterations: 180
currently lose_sum: 503.3451906144619
time_elpased: 1.601
batch start
#iterations: 181
currently lose_sum: 505.09865033626556
time_elpased: 1.6
batch start
#iterations: 182
currently lose_sum: 502.23733645677567
time_elpased: 1.608
batch start
#iterations: 183
currently lose_sum: 502.8395106494427
time_elpased: 1.612
batch start
#iterations: 184
currently lose_sum: 501.2873729765415
time_elpased: 1.611
batch start
#iterations: 185
currently lose_sum: 502.09289887547493
time_elpased: 1.595
batch start
#iterations: 186
currently lose_sum: 502.8479507267475
time_elpased: 1.605
batch start
#iterations: 187
currently lose_sum: 502.41992339491844
time_elpased: 1.61
batch start
#iterations: 188
currently lose_sum: 501.51513519883156
time_elpased: 1.596
batch start
#iterations: 189
currently lose_sum: 501.26242208480835
time_elpased: 1.618
batch start
#iterations: 190
currently lose_sum: 501.4873580336571
time_elpased: 1.597
batch start
#iterations: 191
currently lose_sum: 501.63979867100716
time_elpased: 1.599
batch start
#iterations: 192
currently lose_sum: 500.03391820192337
time_elpased: 1.606
batch start
#iterations: 193
currently lose_sum: 501.4762801527977
time_elpased: 1.609
batch start
#iterations: 194
currently lose_sum: 502.0136786401272
time_elpased: 1.596
batch start
#iterations: 195
currently lose_sum: 503.996508449316
time_elpased: 1.6
batch start
#iterations: 196
currently lose_sum: 499.8024807572365
time_elpased: 1.61
batch start
#iterations: 197
currently lose_sum: 503.7046854496002
time_elpased: 1.604
batch start
#iterations: 198
currently lose_sum: 501.41015246510506
time_elpased: 1.602
batch start
#iterations: 199
currently lose_sum: 502.6156920194626
time_elpased: 1.599
start validation test
0.230927835052
1.0
0.230927835052
0.375209380235
0
validation finish
batch start
#iterations: 200
currently lose_sum: 501.5377479493618
time_elpased: 1.604
batch start
#iterations: 201
currently lose_sum: 503.51146110892296
time_elpased: 1.613
batch start
#iterations: 202
currently lose_sum: 500.0355794131756
time_elpased: 1.614
batch start
#iterations: 203
currently lose_sum: 500.92514595389366
time_elpased: 1.617
batch start
#iterations: 204
currently lose_sum: 501.0244786143303
time_elpased: 1.592
batch start
#iterations: 205
currently lose_sum: 502.01166144013405
time_elpased: 1.605
batch start
#iterations: 206
currently lose_sum: 504.4343143105507
time_elpased: 1.61
batch start
#iterations: 207
currently lose_sum: 499.9951977431774
time_elpased: 1.61
batch start
#iterations: 208
currently lose_sum: 500.81593829393387
time_elpased: 1.6
batch start
#iterations: 209
currently lose_sum: 501.37405037879944
time_elpased: 1.605
batch start
#iterations: 210
currently lose_sum: 500.759490609169
time_elpased: 1.587
batch start
#iterations: 211
currently lose_sum: 501.71693298220634
time_elpased: 1.6
batch start
#iterations: 212
currently lose_sum: 501.5299932062626
time_elpased: 1.601
batch start
#iterations: 213
currently lose_sum: 500.7499071061611
time_elpased: 1.613
batch start
#iterations: 214
currently lose_sum: 501.31539729237556
time_elpased: 1.604
batch start
#iterations: 215
currently lose_sum: 502.084197640419
time_elpased: 1.588
batch start
#iterations: 216
currently lose_sum: 501.8075378835201
time_elpased: 1.616
batch start
#iterations: 217
currently lose_sum: 501.0703409612179
time_elpased: 1.596
batch start
#iterations: 218
currently lose_sum: 501.5341248214245
time_elpased: 1.617
batch start
#iterations: 219
currently lose_sum: 503.317950963974
time_elpased: 1.608
start validation test
0.225360824742
1.0
0.225360824742
0.36782769645
0
validation finish
batch start
#iterations: 220
currently lose_sum: 501.9584307074547
time_elpased: 1.595
batch start
#iterations: 221
currently lose_sum: 503.2228233218193
time_elpased: 1.615
batch start
#iterations: 222
currently lose_sum: 498.9845966398716
time_elpased: 1.584
batch start
#iterations: 223
currently lose_sum: 502.6337902545929
time_elpased: 1.609
batch start
#iterations: 224
currently lose_sum: 501.9019359052181
time_elpased: 1.619
batch start
#iterations: 225
currently lose_sum: 503.87810826301575
time_elpased: 1.599
batch start
#iterations: 226
currently lose_sum: 501.8125376403332
time_elpased: 1.596
batch start
#iterations: 227
currently lose_sum: 501.99827122688293
time_elpased: 1.603
batch start
#iterations: 228
currently lose_sum: 499.5236615240574
time_elpased: 1.598
batch start
#iterations: 229
currently lose_sum: 502.47548609972
time_elpased: 1.592
batch start
#iterations: 230
currently lose_sum: 502.6196632683277
time_elpased: 1.6
batch start
#iterations: 231
currently lose_sum: 503.43292957544327
time_elpased: 1.604
batch start
#iterations: 232
currently lose_sum: 501.2297582924366
time_elpased: 1.597
batch start
#iterations: 233
currently lose_sum: 499.5823850929737
time_elpased: 1.616
batch start
#iterations: 234
currently lose_sum: 500.35936495661736
time_elpased: 1.598
batch start
#iterations: 235
currently lose_sum: 500.52346417307854
time_elpased: 1.608
batch start
#iterations: 236
currently lose_sum: 500.54860496520996
time_elpased: 1.606
batch start
#iterations: 237
currently lose_sum: 502.518905878067
time_elpased: 1.601
batch start
#iterations: 238
currently lose_sum: 500.2343166768551
time_elpased: 1.589
batch start
#iterations: 239
currently lose_sum: 500.88615545630455
time_elpased: 1.616
start validation test
0.17793814433
1.0
0.17793814433
0.302117976545
0
validation finish
batch start
#iterations: 240
currently lose_sum: 501.0229644179344
time_elpased: 1.604
batch start
#iterations: 241
currently lose_sum: 500.8658936023712
time_elpased: 1.598
batch start
#iterations: 242
currently lose_sum: 500.30590546131134
time_elpased: 1.616
batch start
#iterations: 243
currently lose_sum: 500.43283846974373
time_elpased: 1.615
batch start
#iterations: 244
currently lose_sum: 501.45602759718895
time_elpased: 1.613
batch start
#iterations: 245
currently lose_sum: 499.3871083557606
time_elpased: 1.603
batch start
#iterations: 246
currently lose_sum: 499.6519905626774
time_elpased: 1.611
batch start
#iterations: 247
currently lose_sum: 501.6610199511051
time_elpased: 1.605
batch start
#iterations: 248
currently lose_sum: 500.4880637228489
time_elpased: 1.609
batch start
#iterations: 249
currently lose_sum: 501.24697321653366
time_elpased: 1.617
batch start
#iterations: 250
currently lose_sum: 501.24207496643066
time_elpased: 1.6
batch start
#iterations: 251
currently lose_sum: 502.5805161893368
time_elpased: 1.618
batch start
#iterations: 252
currently lose_sum: 503.06609374284744
time_elpased: 1.599
batch start
#iterations: 253
currently lose_sum: 500.66438299417496
time_elpased: 1.6
batch start
#iterations: 254
currently lose_sum: 504.3282884657383
time_elpased: 1.598
batch start
#iterations: 255
currently lose_sum: 500.70589435100555
time_elpased: 1.585
batch start
#iterations: 256
currently lose_sum: 503.5382476449013
time_elpased: 1.615
batch start
#iterations: 257
currently lose_sum: 500.22897386550903
time_elpased: 1.596
batch start
#iterations: 258
currently lose_sum: 500.7280113697052
time_elpased: 1.607
batch start
#iterations: 259
currently lose_sum: 503.1358126103878
time_elpased: 1.601
start validation test
0.202164948454
1.0
0.202164948454
0.336334791184
0
validation finish
batch start
#iterations: 260
currently lose_sum: 502.65495669841766
time_elpased: 1.624
batch start
#iterations: 261
currently lose_sum: 502.0781103372574
time_elpased: 1.618
batch start
#iterations: 262
currently lose_sum: 500.3427589535713
time_elpased: 1.615
batch start
#iterations: 263
currently lose_sum: 500.9594876766205
time_elpased: 1.612
batch start
#iterations: 264
currently lose_sum: 500.83493316173553
time_elpased: 1.611
batch start
#iterations: 265
currently lose_sum: 502.4790294468403
time_elpased: 1.608
batch start
#iterations: 266
currently lose_sum: 500.9373587369919
time_elpased: 1.604
batch start
#iterations: 267
currently lose_sum: 499.95598271489143
time_elpased: 1.611
batch start
#iterations: 268
currently lose_sum: 500.54525592923164
time_elpased: 1.611
batch start
#iterations: 269
currently lose_sum: 498.66438150405884
time_elpased: 1.604
batch start
#iterations: 270
currently lose_sum: 501.86706575751305
time_elpased: 1.612
batch start
#iterations: 271
currently lose_sum: 499.04914516210556
time_elpased: 1.598
batch start
#iterations: 272
currently lose_sum: 500.47000351548195
time_elpased: 1.603
batch start
#iterations: 273
currently lose_sum: 501.4083049595356
time_elpased: 1.597
batch start
#iterations: 274
currently lose_sum: 501.9939641058445
time_elpased: 1.61
batch start
#iterations: 275
currently lose_sum: 500.2013265788555
time_elpased: 1.609
batch start
#iterations: 276
currently lose_sum: 501.22512063384056
time_elpased: 1.601
batch start
#iterations: 277
currently lose_sum: 502.11912018060684
time_elpased: 1.602
batch start
#iterations: 278
currently lose_sum: 500.7216162979603
time_elpased: 1.602
batch start
#iterations: 279
currently lose_sum: 503.00022238492966
time_elpased: 1.61
start validation test
0.239896907216
1.0
0.239896907216
0.386962667332
0
validation finish
batch start
#iterations: 280
currently lose_sum: 500.4465584754944
time_elpased: 1.602
batch start
#iterations: 281
currently lose_sum: 498.96046155691147
time_elpased: 1.6
batch start
#iterations: 282
currently lose_sum: 501.5141421556473
time_elpased: 1.593
batch start
#iterations: 283
currently lose_sum: 501.7911188900471
time_elpased: 1.609
batch start
#iterations: 284
currently lose_sum: 501.03500613570213
time_elpased: 1.602
batch start
#iterations: 285
currently lose_sum: 503.1727667748928
time_elpased: 1.604
batch start
#iterations: 286
currently lose_sum: 501.2168819308281
time_elpased: 1.607
batch start
#iterations: 287
currently lose_sum: 503.0190227329731
time_elpased: 1.626
batch start
#iterations: 288
currently lose_sum: 499.5276700556278
time_elpased: 1.6
batch start
#iterations: 289
currently lose_sum: 499.9111169576645
time_elpased: 1.609
batch start
#iterations: 290
currently lose_sum: 499.40594694018364
time_elpased: 1.613
batch start
#iterations: 291
currently lose_sum: 501.34915778040886
time_elpased: 1.612
batch start
#iterations: 292
currently lose_sum: 502.2137827575207
time_elpased: 1.605
batch start
#iterations: 293
currently lose_sum: 502.2230865061283
time_elpased: 1.613
batch start
#iterations: 294
currently lose_sum: 503.5612878501415
time_elpased: 1.6
batch start
#iterations: 295
currently lose_sum: 501.90113478899
time_elpased: 1.596
batch start
#iterations: 296
currently lose_sum: 500.51438984274864
time_elpased: 1.603
batch start
#iterations: 297
currently lose_sum: 500.88955837488174
time_elpased: 1.602
batch start
#iterations: 298
currently lose_sum: 501.04070246219635
time_elpased: 1.605
batch start
#iterations: 299
currently lose_sum: 501.8808586001396
time_elpased: 1.588
start validation test
0.210309278351
1.0
0.210309278351
0.347529812606
0
validation finish
batch start
#iterations: 300
currently lose_sum: 501.97416892647743
time_elpased: 1.612
batch start
#iterations: 301
currently lose_sum: 499.79591459035873
time_elpased: 1.604
batch start
#iterations: 302
currently lose_sum: 501.8475160598755
time_elpased: 1.598
batch start
#iterations: 303
currently lose_sum: 502.041866928339
time_elpased: 1.624
batch start
#iterations: 304
currently lose_sum: 499.9043462276459
time_elpased: 1.595
batch start
#iterations: 305
currently lose_sum: 502.34031531214714
time_elpased: 1.605
batch start
#iterations: 306
currently lose_sum: 502.713708370924
time_elpased: 1.598
batch start
#iterations: 307
currently lose_sum: 503.43067905306816
time_elpased: 1.608
batch start
#iterations: 308
currently lose_sum: 502.1221949458122
time_elpased: 1.616
batch start
#iterations: 309
currently lose_sum: 501.8372581899166
time_elpased: 1.604
batch start
#iterations: 310
currently lose_sum: 499.5925181508064
time_elpased: 1.626
batch start
#iterations: 311
currently lose_sum: 500.06920650601387
time_elpased: 1.601
batch start
#iterations: 312
currently lose_sum: 502.36053985357285
time_elpased: 1.597
batch start
#iterations: 313
currently lose_sum: 503.2036192715168
time_elpased: 1.611
batch start
#iterations: 314
currently lose_sum: 500.71906292438507
time_elpased: 1.601
batch start
#iterations: 315
currently lose_sum: 501.589956343174
time_elpased: 1.604
batch start
#iterations: 316
currently lose_sum: 502.12524658441544
time_elpased: 1.599
batch start
#iterations: 317
currently lose_sum: 499.6191138923168
time_elpased: 1.599
batch start
#iterations: 318
currently lose_sum: 503.7123296558857
time_elpased: 1.607
batch start
#iterations: 319
currently lose_sum: 500.4882526099682
time_elpased: 1.613
start validation test
0.228659793814
1.0
0.228659793814
0.372210102366
0
validation finish
batch start
#iterations: 320
currently lose_sum: 502.0399584174156
time_elpased: 1.625
batch start
#iterations: 321
currently lose_sum: 501.0613504052162
time_elpased: 1.612
batch start
#iterations: 322
currently lose_sum: 499.73830538988113
time_elpased: 1.588
batch start
#iterations: 323
currently lose_sum: 500.848360478878
time_elpased: 1.623
batch start
#iterations: 324
currently lose_sum: 503.5017340183258
time_elpased: 1.619
batch start
#iterations: 325
currently lose_sum: 500.10715785622597
time_elpased: 1.592
batch start
#iterations: 326
currently lose_sum: 502.16049724817276
time_elpased: 1.6
batch start
#iterations: 327
currently lose_sum: 501.35003438591957
time_elpased: 1.604
batch start
#iterations: 328
currently lose_sum: 501.30937787890434
time_elpased: 1.611
batch start
#iterations: 329
currently lose_sum: 500.6373727917671
time_elpased: 1.613
batch start
#iterations: 330
currently lose_sum: 502.3201124072075
time_elpased: 1.616
batch start
#iterations: 331
currently lose_sum: 502.36701813340187
time_elpased: 1.613
batch start
#iterations: 332
currently lose_sum: 502.0305153131485
time_elpased: 1.608
batch start
#iterations: 333
currently lose_sum: 500.9441680908203
time_elpased: 1.626
batch start
#iterations: 334
currently lose_sum: 500.61433720588684
time_elpased: 1.621
batch start
#iterations: 335
currently lose_sum: 500.8691084384918
time_elpased: 1.605
batch start
#iterations: 336
currently lose_sum: 501.593950510025
time_elpased: 1.617
batch start
#iterations: 337
currently lose_sum: 500.5150584280491
time_elpased: 1.624
batch start
#iterations: 338
currently lose_sum: 499.86513444781303
time_elpased: 1.616
batch start
#iterations: 339
currently lose_sum: 501.65874764323235
time_elpased: 1.616
start validation test
0.210927835052
1.0
0.210927835052
0.348373914524
0
validation finish
batch start
#iterations: 340
currently lose_sum: 500.07628297805786
time_elpased: 1.61
batch start
#iterations: 341
currently lose_sum: 501.8211708366871
time_elpased: 1.604
batch start
#iterations: 342
currently lose_sum: 503.13694509863853
time_elpased: 1.608
batch start
#iterations: 343
currently lose_sum: 502.11365249753
time_elpased: 1.6
batch start
#iterations: 344
currently lose_sum: 503.46862491965294
time_elpased: 1.597
batch start
#iterations: 345
currently lose_sum: 502.9904572069645
time_elpased: 1.601
batch start
#iterations: 346
currently lose_sum: 501.56724840402603
time_elpased: 1.59
batch start
#iterations: 347
currently lose_sum: 501.4444553256035
time_elpased: 1.605
batch start
#iterations: 348
currently lose_sum: 503.2797166109085
time_elpased: 1.606
batch start
#iterations: 349
currently lose_sum: 501.93015491962433
time_elpased: 1.597
batch start
#iterations: 350
currently lose_sum: 501.56523138284683
time_elpased: 1.616
batch start
#iterations: 351
currently lose_sum: 501.62753280997276
time_elpased: 1.596
batch start
#iterations: 352
currently lose_sum: 500.8761321604252
time_elpased: 1.603
batch start
#iterations: 353
currently lose_sum: 502.8063661456108
time_elpased: 1.592
batch start
#iterations: 354
currently lose_sum: 501.26424264907837
time_elpased: 1.604
batch start
#iterations: 355
currently lose_sum: 501.88999405503273
time_elpased: 1.608
batch start
#iterations: 356
currently lose_sum: 499.0109488964081
time_elpased: 1.601
batch start
#iterations: 357
currently lose_sum: 499.4968156218529
time_elpased: 1.607
batch start
#iterations: 358
currently lose_sum: 502.48010128736496
time_elpased: 1.603
batch start
#iterations: 359
currently lose_sum: 499.1751389503479
time_elpased: 1.617
start validation test
0.241030927835
1.0
0.241030927835
0.388436617378
0
validation finish
batch start
#iterations: 360
currently lose_sum: 501.36761286854744
time_elpased: 1.621
batch start
#iterations: 361
currently lose_sum: 500.33718517422676
time_elpased: 1.621
batch start
#iterations: 362
currently lose_sum: 500.3697706460953
time_elpased: 1.592
batch start
#iterations: 363
currently lose_sum: 501.41072255373
time_elpased: 1.6
batch start
#iterations: 364
currently lose_sum: 501.73581051826477
time_elpased: 1.599
batch start
#iterations: 365
currently lose_sum: 501.9607826471329
time_elpased: 1.625
batch start
#iterations: 366
currently lose_sum: 502.39081957936287
time_elpased: 1.606
batch start
#iterations: 367
currently lose_sum: 502.2061048448086
time_elpased: 1.596
batch start
#iterations: 368
currently lose_sum: 500.535489410162
time_elpased: 1.608
batch start
#iterations: 369
currently lose_sum: 499.7513864040375
time_elpased: 1.592
batch start
#iterations: 370
currently lose_sum: 498.9600739181042
time_elpased: 1.602
batch start
#iterations: 371
currently lose_sum: 500.5834088921547
time_elpased: 1.606
batch start
#iterations: 372
currently lose_sum: 499.77713933587074
time_elpased: 1.603
batch start
#iterations: 373
currently lose_sum: 501.14683187007904
time_elpased: 1.606
batch start
#iterations: 374
currently lose_sum: 502.9853422641754
time_elpased: 1.608
batch start
#iterations: 375
currently lose_sum: 502.4263023138046
time_elpased: 1.61
batch start
#iterations: 376
currently lose_sum: 502.024192661047
time_elpased: 1.581
batch start
#iterations: 377
currently lose_sum: 499.9007288515568
time_elpased: 1.6
batch start
#iterations: 378
currently lose_sum: 503.0332292318344
time_elpased: 1.606
batch start
#iterations: 379
currently lose_sum: 501.473381459713
time_elpased: 1.602
start validation test
0.233711340206
1.0
0.233711340206
0.378875240244
0
validation finish
batch start
#iterations: 380
currently lose_sum: 499.85813108086586
time_elpased: 1.612
batch start
#iterations: 381
currently lose_sum: 499.94671311974525
time_elpased: 1.606
batch start
#iterations: 382
currently lose_sum: 500.8183825612068
time_elpased: 1.614
batch start
#iterations: 383
currently lose_sum: 498.6029124855995
time_elpased: 1.606
batch start
#iterations: 384
currently lose_sum: 499.3894465267658
time_elpased: 1.591
batch start
#iterations: 385
currently lose_sum: 500.43449515104294
time_elpased: 1.612
batch start
#iterations: 386
currently lose_sum: 499.92175737023354
time_elpased: 1.614
batch start
#iterations: 387
currently lose_sum: 499.8804422914982
time_elpased: 1.608
batch start
#iterations: 388
currently lose_sum: 500.8992197215557
time_elpased: 1.606
batch start
#iterations: 389
currently lose_sum: 502.11604049801826
time_elpased: 1.616
batch start
#iterations: 390
currently lose_sum: 500.47569504380226
time_elpased: 1.607
batch start
#iterations: 391
currently lose_sum: 502.3229073882103
time_elpased: 1.604
batch start
#iterations: 392
currently lose_sum: 499.97678622603416
time_elpased: 1.624
batch start
#iterations: 393
currently lose_sum: 499.4072657227516
time_elpased: 1.612
batch start
#iterations: 394
currently lose_sum: 499.23441711068153
time_elpased: 1.596
batch start
#iterations: 395
currently lose_sum: 500.62302455306053
time_elpased: 1.606
batch start
#iterations: 396
currently lose_sum: 501.1355567276478
time_elpased: 1.595
batch start
#iterations: 397
currently lose_sum: 501.9832009971142
time_elpased: 1.612
batch start
#iterations: 398
currently lose_sum: 500.45395335555077
time_elpased: 1.624
batch start
#iterations: 399
currently lose_sum: 499.732823073864
time_elpased: 1.598
start validation test
0.209690721649
1.0
0.209690721649
0.346684847452
0
validation finish
acc: 0.249
pre: 1.000
rec: 0.249
F1: 0.399
auc: 0.000
