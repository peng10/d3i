start to construct graph
graph construct over
58300
epochs start
batch start
#iterations: 0
currently lose_sum: 403.8249559998512
time_elpased: 0.949
batch start
#iterations: 1
currently lose_sum: 400.2912979722023
time_elpased: 0.911
batch start
#iterations: 2
currently lose_sum: 397.6889961361885
time_elpased: 0.917
batch start
#iterations: 3
currently lose_sum: 396.6109978556633
time_elpased: 0.904
batch start
#iterations: 4
currently lose_sum: 396.09574872255325
time_elpased: 0.92
batch start
#iterations: 5
currently lose_sum: 395.01433777809143
time_elpased: 0.918
batch start
#iterations: 6
currently lose_sum: 394.9038309454918
time_elpased: 0.927
batch start
#iterations: 7
currently lose_sum: 394.4390363097191
time_elpased: 0.911
batch start
#iterations: 8
currently lose_sum: 393.74246340990067
time_elpased: 0.925
batch start
#iterations: 9
currently lose_sum: 393.48550271987915
time_elpased: 0.911
batch start
#iterations: 10
currently lose_sum: 393.2255370616913
time_elpased: 0.914
batch start
#iterations: 11
currently lose_sum: 392.90008944272995
time_elpased: 0.912
batch start
#iterations: 12
currently lose_sum: 392.3113176226616
time_elpased: 0.926
batch start
#iterations: 13
currently lose_sum: 393.01665234565735
time_elpased: 0.916
batch start
#iterations: 14
currently lose_sum: 391.48850244283676
time_elpased: 0.927
batch start
#iterations: 15
currently lose_sum: 392.5014656186104
time_elpased: 0.935
batch start
#iterations: 16
currently lose_sum: 391.8851845264435
time_elpased: 0.917
batch start
#iterations: 17
currently lose_sum: 392.6397603750229
time_elpased: 0.923
batch start
#iterations: 18
currently lose_sum: 391.55083137750626
time_elpased: 0.921
batch start
#iterations: 19
currently lose_sum: 392.02774727344513
time_elpased: 0.921
start validation test
0.422164948454
1.0
0.422164948454
0.593693367162
0
validation finish
batch start
#iterations: 20
currently lose_sum: 391.4018090367317
time_elpased: 0.92
batch start
#iterations: 21
currently lose_sum: 392.2683370709419
time_elpased: 0.923
batch start
#iterations: 22
currently lose_sum: 391.4468891620636
time_elpased: 0.912
batch start
#iterations: 23
currently lose_sum: 391.97629570961
time_elpased: 0.918
batch start
#iterations: 24
currently lose_sum: 392.51159316301346
time_elpased: 0.914
batch start
#iterations: 25
currently lose_sum: 392.2687349319458
time_elpased: 0.921
batch start
#iterations: 26
currently lose_sum: 391.0214975476265
time_elpased: 0.918
batch start
#iterations: 27
currently lose_sum: 391.45119720697403
time_elpased: 0.909
batch start
#iterations: 28
currently lose_sum: 391.22564792633057
time_elpased: 0.907
batch start
#iterations: 29
currently lose_sum: 391.9049617052078
time_elpased: 0.914
batch start
#iterations: 30
currently lose_sum: 391.12294417619705
time_elpased: 0.912
batch start
#iterations: 31
currently lose_sum: 391.62021017074585
time_elpased: 0.915
batch start
#iterations: 32
currently lose_sum: 391.80521738529205
time_elpased: 0.912
batch start
#iterations: 33
currently lose_sum: 391.6100670695305
time_elpased: 0.914
batch start
#iterations: 34
currently lose_sum: 391.72676026821136
time_elpased: 0.926
batch start
#iterations: 35
currently lose_sum: 393.517967402935
time_elpased: 0.913
batch start
#iterations: 36
currently lose_sum: 390.97883838415146
time_elpased: 0.92
batch start
#iterations: 37
currently lose_sum: 390.59947299957275
time_elpased: 0.917
batch start
#iterations: 38
currently lose_sum: 391.51325684785843
time_elpased: 0.917
batch start
#iterations: 39
currently lose_sum: 391.66624319553375
time_elpased: 0.908
start validation test
0.413092783505
1.0
0.413092783505
0.584664769826
0
validation finish
batch start
#iterations: 40
currently lose_sum: 390.95447301864624
time_elpased: 0.913
batch start
#iterations: 41
currently lose_sum: 391.46341502666473
time_elpased: 0.914
batch start
#iterations: 42
currently lose_sum: 390.2212066054344
time_elpased: 0.905
batch start
#iterations: 43
currently lose_sum: 390.0629439353943
time_elpased: 0.917
batch start
#iterations: 44
currently lose_sum: 390.8285015821457
time_elpased: 0.923
batch start
#iterations: 45
currently lose_sum: 390.6760895252228
time_elpased: 0.919
batch start
#iterations: 46
currently lose_sum: 391.07959109544754
time_elpased: 0.918
batch start
#iterations: 47
currently lose_sum: 390.7973892092705
time_elpased: 0.911
batch start
#iterations: 48
currently lose_sum: 390.7475655078888
time_elpased: 0.915
batch start
#iterations: 49
currently lose_sum: 391.18557554483414
time_elpased: 0.92
batch start
#iterations: 50
currently lose_sum: 390.78169018030167
time_elpased: 0.917
batch start
#iterations: 51
currently lose_sum: 390.8453850746155
time_elpased: 0.918
batch start
#iterations: 52
currently lose_sum: 391.1319441795349
time_elpased: 0.904
batch start
#iterations: 53
currently lose_sum: 390.74358159303665
time_elpased: 0.912
batch start
#iterations: 54
currently lose_sum: 390.8432827591896
time_elpased: 0.916
batch start
#iterations: 55
currently lose_sum: 391.19851636886597
time_elpased: 0.909
batch start
#iterations: 56
currently lose_sum: 391.2361515760422
time_elpased: 0.915
batch start
#iterations: 57
currently lose_sum: 390.51207506656647
time_elpased: 0.922
batch start
#iterations: 58
currently lose_sum: 389.7021828889847
time_elpased: 0.912
batch start
#iterations: 59
currently lose_sum: 391.4567446708679
time_elpased: 0.908
start validation test
0.49175257732
1.0
0.49175257732
0.659295093296
0
validation finish
batch start
#iterations: 60
currently lose_sum: 391.56503814458847
time_elpased: 0.913
batch start
#iterations: 61
currently lose_sum: 390.63375103473663
time_elpased: 0.921
batch start
#iterations: 62
currently lose_sum: 390.59565645456314
time_elpased: 0.916
batch start
#iterations: 63
currently lose_sum: 390.60692435503006
time_elpased: 0.915
batch start
#iterations: 64
currently lose_sum: 391.4179168343544
time_elpased: 0.917
batch start
#iterations: 65
currently lose_sum: 390.9992782473564
time_elpased: 0.914
batch start
#iterations: 66
currently lose_sum: 390.9242413043976
time_elpased: 0.912
batch start
#iterations: 67
currently lose_sum: 390.4378350973129
time_elpased: 0.917
batch start
#iterations: 68
currently lose_sum: 390.88838332891464
time_elpased: 0.918
batch start
#iterations: 69
currently lose_sum: 390.50812697410583
time_elpased: 0.918
batch start
#iterations: 70
currently lose_sum: 390.61122703552246
time_elpased: 0.914
batch start
#iterations: 71
currently lose_sum: 391.0209211111069
time_elpased: 0.907
batch start
#iterations: 72
currently lose_sum: 389.9713298678398
time_elpased: 0.923
batch start
#iterations: 73
currently lose_sum: 390.6576653122902
time_elpased: 0.931
batch start
#iterations: 74
currently lose_sum: 390.708636701107
time_elpased: 0.925
batch start
#iterations: 75
currently lose_sum: 389.9899835586548
time_elpased: 0.915
batch start
#iterations: 76
currently lose_sum: 389.71410512924194
time_elpased: 0.912
batch start
#iterations: 77
currently lose_sum: 390.6695924401283
time_elpased: 0.911
batch start
#iterations: 78
currently lose_sum: 391.0081295967102
time_elpased: 0.932
batch start
#iterations: 79
currently lose_sum: 390.71102464199066
time_elpased: 0.913
start validation test
0.538762886598
1.0
0.538762886598
0.700254589307
0
validation finish
batch start
#iterations: 80
currently lose_sum: 389.9412960410118
time_elpased: 0.917
batch start
#iterations: 81
currently lose_sum: 390.4702959060669
time_elpased: 0.928
batch start
#iterations: 82
currently lose_sum: 390.4014711380005
time_elpased: 0.911
batch start
#iterations: 83
currently lose_sum: 390.2637606859207
time_elpased: 0.915
batch start
#iterations: 84
currently lose_sum: 391.28125137090683
time_elpased: 0.911
batch start
#iterations: 85
currently lose_sum: 391.12319177389145
time_elpased: 0.917
batch start
#iterations: 86
currently lose_sum: 390.72930097579956
time_elpased: 0.93
batch start
#iterations: 87
currently lose_sum: 390.4074094891548
time_elpased: 0.913
batch start
#iterations: 88
currently lose_sum: 389.6317814588547
time_elpased: 0.913
batch start
#iterations: 89
currently lose_sum: 390.854411482811
time_elpased: 0.925
batch start
#iterations: 90
currently lose_sum: 391.12868428230286
time_elpased: 0.914
batch start
#iterations: 91
currently lose_sum: 390.26079016923904
time_elpased: 0.91
batch start
#iterations: 92
currently lose_sum: 391.0895677804947
time_elpased: 0.904
batch start
#iterations: 93
currently lose_sum: 391.1407145857811
time_elpased: 0.928
batch start
#iterations: 94
currently lose_sum: 390.5855358839035
time_elpased: 0.919
batch start
#iterations: 95
currently lose_sum: 390.330531001091
time_elpased: 0.906
batch start
#iterations: 96
currently lose_sum: 390.139426112175
time_elpased: 0.926
batch start
#iterations: 97
currently lose_sum: 389.53187388181686
time_elpased: 0.912
batch start
#iterations: 98
currently lose_sum: 390.1183571219444
time_elpased: 0.917
batch start
#iterations: 99
currently lose_sum: 390.7313351035118
time_elpased: 0.925
start validation test
0.498144329897
1.0
0.498144329897
0.665015139004
0
validation finish
batch start
#iterations: 100
currently lose_sum: 391.63003289699554
time_elpased: 0.928
batch start
#iterations: 101
currently lose_sum: 390.2397689819336
time_elpased: 0.919
batch start
#iterations: 102
currently lose_sum: 389.80925983190536
time_elpased: 0.911
batch start
#iterations: 103
currently lose_sum: 390.389088511467
time_elpased: 0.919
batch start
#iterations: 104
currently lose_sum: 390.930470764637
time_elpased: 0.907
batch start
#iterations: 105
currently lose_sum: 389.9097942709923
time_elpased: 0.919
batch start
#iterations: 106
currently lose_sum: 390.97724854946136
time_elpased: 0.912
batch start
#iterations: 107
currently lose_sum: 390.6799759864807
time_elpased: 0.915
batch start
#iterations: 108
currently lose_sum: 390.52868980169296
time_elpased: 0.914
batch start
#iterations: 109
currently lose_sum: 390.170276761055
time_elpased: 0.928
batch start
#iterations: 110
currently lose_sum: 390.3870494365692
time_elpased: 0.922
batch start
#iterations: 111
currently lose_sum: 389.6912211179733
time_elpased: 0.906
batch start
#iterations: 112
currently lose_sum: 390.44780963659286
time_elpased: 0.925
batch start
#iterations: 113
currently lose_sum: 390.3860966563225
time_elpased: 0.921
batch start
#iterations: 114
currently lose_sum: 390.3873677253723
time_elpased: 0.91
batch start
#iterations: 115
currently lose_sum: 390.17748737335205
time_elpased: 0.926
batch start
#iterations: 116
currently lose_sum: 389.95360642671585
time_elpased: 0.917
batch start
#iterations: 117
currently lose_sum: 389.8093907237053
time_elpased: 0.917
batch start
#iterations: 118
currently lose_sum: 389.7291696667671
time_elpased: 0.929
batch start
#iterations: 119
currently lose_sum: 391.0350921154022
time_elpased: 0.92
start validation test
0.468041237113
1.0
0.468041237113
0.637640449438
0
validation finish
batch start
#iterations: 120
currently lose_sum: 390.25666266679764
time_elpased: 0.918
batch start
#iterations: 121
currently lose_sum: 389.4529194831848
time_elpased: 0.925
batch start
#iterations: 122
currently lose_sum: 390.33885967731476
time_elpased: 0.923
batch start
#iterations: 123
currently lose_sum: 390.05357682704926
time_elpased: 0.919
batch start
#iterations: 124
currently lose_sum: 389.5461714863777
time_elpased: 0.908
batch start
#iterations: 125
currently lose_sum: 390.3123027086258
time_elpased: 0.923
batch start
#iterations: 126
currently lose_sum: 390.5869374871254
time_elpased: 0.923
batch start
#iterations: 127
currently lose_sum: 390.1151424050331
time_elpased: 0.925
batch start
#iterations: 128
currently lose_sum: 390.2967514395714
time_elpased: 0.923
batch start
#iterations: 129
currently lose_sum: 390.6178666949272
time_elpased: 0.916
batch start
#iterations: 130
currently lose_sum: 390.22530567646027
time_elpased: 0.917
batch start
#iterations: 131
currently lose_sum: 389.63999330997467
time_elpased: 0.918
batch start
#iterations: 132
currently lose_sum: 390.2965399622917
time_elpased: 0.91
batch start
#iterations: 133
currently lose_sum: 391.0249277949333
time_elpased: 0.921
batch start
#iterations: 134
currently lose_sum: 390.46856039762497
time_elpased: 0.924
batch start
#iterations: 135
currently lose_sum: 390.1294054389
time_elpased: 0.919
batch start
#iterations: 136
currently lose_sum: 390.47435957193375
time_elpased: 0.918
batch start
#iterations: 137
currently lose_sum: 389.5327485203743
time_elpased: 0.918
batch start
#iterations: 138
currently lose_sum: 390.52896696329117
time_elpased: 0.923
batch start
#iterations: 139
currently lose_sum: 390.58978444337845
time_elpased: 0.917
start validation test
0.540309278351
1.0
0.540309278351
0.701559467238
0
validation finish
batch start
#iterations: 140
currently lose_sum: 389.6679782271385
time_elpased: 0.928
batch start
#iterations: 141
currently lose_sum: 390.41431999206543
time_elpased: 0.932
batch start
#iterations: 142
currently lose_sum: 390.57578551769257
time_elpased: 0.916
batch start
#iterations: 143
currently lose_sum: 391.1650924086571
time_elpased: 0.918
batch start
#iterations: 144
currently lose_sum: 390.4335502386093
time_elpased: 0.919
batch start
#iterations: 145
currently lose_sum: 390.13585364818573
time_elpased: 0.914
batch start
#iterations: 146
currently lose_sum: 389.6455731987953
time_elpased: 0.909
batch start
#iterations: 147
currently lose_sum: 390.0566262602806
time_elpased: 0.914
batch start
#iterations: 148
currently lose_sum: 390.64018630981445
time_elpased: 0.923
batch start
#iterations: 149
currently lose_sum: 390.46138948202133
time_elpased: 0.92
batch start
#iterations: 150
currently lose_sum: 390.4811758995056
time_elpased: 0.914
batch start
#iterations: 151
currently lose_sum: 390.7629694342613
time_elpased: 0.926
batch start
#iterations: 152
currently lose_sum: 389.5841608643532
time_elpased: 0.921
batch start
#iterations: 153
currently lose_sum: 389.99418514966965
time_elpased: 0.925
batch start
#iterations: 154
currently lose_sum: 390.1177273988724
time_elpased: 0.919
batch start
#iterations: 155
currently lose_sum: 390.6566437482834
time_elpased: 0.917
batch start
#iterations: 156
currently lose_sum: 389.6233093738556
time_elpased: 0.916
batch start
#iterations: 157
currently lose_sum: 389.6215984225273
time_elpased: 0.921
batch start
#iterations: 158
currently lose_sum: 390.5990141630173
time_elpased: 0.915
batch start
#iterations: 159
currently lose_sum: 391.79484075307846
time_elpased: 0.926
start validation test
0.564329896907
1.0
0.564329896907
0.72149729801
0
validation finish
batch start
#iterations: 160
currently lose_sum: 390.43063735961914
time_elpased: 0.928
batch start
#iterations: 161
currently lose_sum: 390.48803716897964
time_elpased: 0.927
batch start
#iterations: 162
currently lose_sum: 389.94672590494156
time_elpased: 0.919
batch start
#iterations: 163
currently lose_sum: 389.87756019830704
time_elpased: 0.918
batch start
#iterations: 164
currently lose_sum: 390.22506362199783
time_elpased: 0.918
batch start
#iterations: 165
currently lose_sum: 389.6042981147766
time_elpased: 0.918
batch start
#iterations: 166
currently lose_sum: 389.33729124069214
time_elpased: 0.915
batch start
#iterations: 167
currently lose_sum: 389.8825738430023
time_elpased: 0.916
batch start
#iterations: 168
currently lose_sum: 390.28860288858414
time_elpased: 0.943
batch start
#iterations: 169
currently lose_sum: 389.06510639190674
time_elpased: 0.925
batch start
#iterations: 170
currently lose_sum: 389.83301001787186
time_elpased: 0.919
batch start
#iterations: 171
currently lose_sum: 391.148734331131
time_elpased: 0.917
batch start
#iterations: 172
currently lose_sum: 390.84582567214966
time_elpased: 0.912
batch start
#iterations: 173
currently lose_sum: 390.3618908524513
time_elpased: 0.919
batch start
#iterations: 174
currently lose_sum: 389.7152789235115
time_elpased: 0.914
batch start
#iterations: 175
currently lose_sum: 390.68621879816055
time_elpased: 0.911
batch start
#iterations: 176
currently lose_sum: 390.107925593853
time_elpased: 0.922
batch start
#iterations: 177
currently lose_sum: 389.2785909771919
time_elpased: 0.92
batch start
#iterations: 178
currently lose_sum: 389.2754721045494
time_elpased: 0.928
batch start
#iterations: 179
currently lose_sum: 389.9918649792671
time_elpased: 0.926
start validation test
0.535154639175
1.0
0.535154639175
0.697199650796
0
validation finish
batch start
#iterations: 180
currently lose_sum: 389.9080157279968
time_elpased: 0.921
batch start
#iterations: 181
currently lose_sum: 389.801913022995
time_elpased: 0.921
batch start
#iterations: 182
currently lose_sum: 390.45622301101685
time_elpased: 0.927
batch start
#iterations: 183
currently lose_sum: 390.1274375319481
time_elpased: 0.913
batch start
#iterations: 184
currently lose_sum: 390.2538585662842
time_elpased: 0.928
batch start
#iterations: 185
currently lose_sum: 390.31113958358765
time_elpased: 0.916
batch start
#iterations: 186
currently lose_sum: 389.31093311309814
time_elpased: 0.925
batch start
#iterations: 187
currently lose_sum: 389.24133265018463
time_elpased: 0.925
batch start
#iterations: 188
currently lose_sum: 390.51951879262924
time_elpased: 0.92
batch start
#iterations: 189
currently lose_sum: 390.53461903333664
time_elpased: 0.92
batch start
#iterations: 190
currently lose_sum: 389.5260480046272
time_elpased: 0.912
batch start
#iterations: 191
currently lose_sum: 390.34195578098297
time_elpased: 0.941
batch start
#iterations: 192
currently lose_sum: 390.5288034081459
time_elpased: 0.93
batch start
#iterations: 193
currently lose_sum: 391.16964650154114
time_elpased: 0.925
batch start
#iterations: 194
currently lose_sum: 389.2962467074394
time_elpased: 0.925
batch start
#iterations: 195
currently lose_sum: 389.8301771879196
time_elpased: 0.926
batch start
#iterations: 196
currently lose_sum: 391.131806910038
time_elpased: 0.92
batch start
#iterations: 197
currently lose_sum: 390.3317343592644
time_elpased: 0.919
batch start
#iterations: 198
currently lose_sum: 390.7207223176956
time_elpased: 0.918
batch start
#iterations: 199
currently lose_sum: 390.1849917769432
time_elpased: 0.927
start validation test
0.478453608247
1.0
0.478453608247
0.647235199777
0
validation finish
batch start
#iterations: 200
currently lose_sum: 390.6540054678917
time_elpased: 0.917
batch start
#iterations: 201
currently lose_sum: 389.0551148056984
time_elpased: 0.924
batch start
#iterations: 202
currently lose_sum: 389.73113828897476
time_elpased: 0.919
batch start
#iterations: 203
currently lose_sum: 388.8601036667824
time_elpased: 0.92
batch start
#iterations: 204
currently lose_sum: 390.2076056599617
time_elpased: 0.926
batch start
#iterations: 205
currently lose_sum: 389.6994289755821
time_elpased: 0.918
batch start
#iterations: 206
currently lose_sum: 389.8359030485153
time_elpased: 0.914
batch start
#iterations: 207
currently lose_sum: 390.8388437628746
time_elpased: 0.928
batch start
#iterations: 208
currently lose_sum: 389.97845977544785
time_elpased: 0.915
batch start
#iterations: 209
currently lose_sum: 389.4140340089798
time_elpased: 0.934
batch start
#iterations: 210
currently lose_sum: 389.5190413594246
time_elpased: 0.926
batch start
#iterations: 211
currently lose_sum: 390.00740545988083
time_elpased: 0.928
batch start
#iterations: 212
currently lose_sum: 390.7201046347618
time_elpased: 0.921
batch start
#iterations: 213
currently lose_sum: 390.23860079050064
time_elpased: 0.932
batch start
#iterations: 214
currently lose_sum: 390.5068742632866
time_elpased: 0.919
batch start
#iterations: 215
currently lose_sum: 389.95383989810944
time_elpased: 0.928
batch start
#iterations: 216
currently lose_sum: 390.29913234710693
time_elpased: 0.929
batch start
#iterations: 217
currently lose_sum: 389.7428157925606
time_elpased: 0.919
batch start
#iterations: 218
currently lose_sum: 390.7287090420723
time_elpased: 0.925
batch start
#iterations: 219
currently lose_sum: 390.469095826149
time_elpased: 0.918
start validation test
0.50618556701
1.0
0.50618556701
0.672142368241
0
validation finish
batch start
#iterations: 220
currently lose_sum: 389.70923191308975
time_elpased: 0.92
batch start
#iterations: 221
currently lose_sum: 390.39077216386795
time_elpased: 0.92
batch start
#iterations: 222
currently lose_sum: 389.8521890640259
time_elpased: 0.928
batch start
#iterations: 223
currently lose_sum: 389.90743923187256
time_elpased: 0.923
batch start
#iterations: 224
currently lose_sum: 389.92171370983124
time_elpased: 0.937
batch start
#iterations: 225
currently lose_sum: 389.17325979471207
time_elpased: 0.927
batch start
#iterations: 226
currently lose_sum: 389.6708651781082
time_elpased: 0.915
batch start
#iterations: 227
currently lose_sum: 389.31949239969254
time_elpased: 0.912
batch start
#iterations: 228
currently lose_sum: 389.4615330696106
time_elpased: 0.918
batch start
#iterations: 229
currently lose_sum: 389.4130642414093
time_elpased: 0.921
batch start
#iterations: 230
currently lose_sum: 390.2422382235527
time_elpased: 0.928
batch start
#iterations: 231
currently lose_sum: 390.35306537151337
time_elpased: 0.918
batch start
#iterations: 232
currently lose_sum: 390.6043594479561
time_elpased: 0.92
batch start
#iterations: 233
currently lose_sum: 390.45604264736176
time_elpased: 0.922
batch start
#iterations: 234
currently lose_sum: 390.31515741348267
time_elpased: 0.938
batch start
#iterations: 235
currently lose_sum: 389.8522980809212
time_elpased: 0.919
batch start
#iterations: 236
currently lose_sum: 390.26166504621506
time_elpased: 0.928
batch start
#iterations: 237
currently lose_sum: 390.2310049533844
time_elpased: 0.919
batch start
#iterations: 238
currently lose_sum: 390.3011417388916
time_elpased: 0.922
batch start
#iterations: 239
currently lose_sum: 390.1120488643646
time_elpased: 0.924
start validation test
0.518453608247
1.0
0.518453608247
0.682870527531
0
validation finish
batch start
#iterations: 240
currently lose_sum: 389.1990243792534
time_elpased: 0.919
batch start
#iterations: 241
currently lose_sum: 389.58178079128265
time_elpased: 0.931
batch start
#iterations: 242
currently lose_sum: 389.00609558820724
time_elpased: 0.923
batch start
#iterations: 243
currently lose_sum: 390.27108430862427
time_elpased: 0.917
batch start
#iterations: 244
currently lose_sum: 390.3914564847946
time_elpased: 0.919
batch start
#iterations: 245
currently lose_sum: 389.96325051784515
time_elpased: 0.92
batch start
#iterations: 246
currently lose_sum: 389.93846279382706
time_elpased: 0.932
batch start
#iterations: 247
currently lose_sum: 390.6623539328575
time_elpased: 0.919
batch start
#iterations: 248
currently lose_sum: 390.9921885728836
time_elpased: 0.925
batch start
#iterations: 249
currently lose_sum: 389.4871597290039
time_elpased: 0.924
batch start
#iterations: 250
currently lose_sum: 390.82479614019394
time_elpased: 0.935
batch start
#iterations: 251
currently lose_sum: 390.0080940723419
time_elpased: 0.914
batch start
#iterations: 252
currently lose_sum: 390.4209624528885
time_elpased: 0.925
batch start
#iterations: 253
currently lose_sum: 388.9220200777054
time_elpased: 0.953
batch start
#iterations: 254
currently lose_sum: 390.4717735648155
time_elpased: 0.923
batch start
#iterations: 255
currently lose_sum: 390.62505000829697
time_elpased: 0.93
batch start
#iterations: 256
currently lose_sum: 390.31561613082886
time_elpased: 0.945
batch start
#iterations: 257
currently lose_sum: 390.0545788407326
time_elpased: 0.917
batch start
#iterations: 258
currently lose_sum: 391.005263030529
time_elpased: 0.922
batch start
#iterations: 259
currently lose_sum: 389.4365564584732
time_elpased: 0.916
start validation test
0.542989690722
1.0
0.542989690722
0.703815059798
0
validation finish
batch start
#iterations: 260
currently lose_sum: 389.6101196408272
time_elpased: 0.925
batch start
#iterations: 261
currently lose_sum: 390.0409192442894
time_elpased: 0.919
batch start
#iterations: 262
currently lose_sum: 391.0432360768318
time_elpased: 0.936
batch start
#iterations: 263
currently lose_sum: 391.5653375983238
time_elpased: 0.92
batch start
#iterations: 264
currently lose_sum: 389.83834767341614
time_elpased: 0.916
batch start
#iterations: 265
currently lose_sum: 389.26965790987015
time_elpased: 0.933
batch start
#iterations: 266
currently lose_sum: 389.9242275953293
time_elpased: 0.922
batch start
#iterations: 267
currently lose_sum: 390.2587391138077
time_elpased: 0.934
batch start
#iterations: 268
currently lose_sum: 389.2220851778984
time_elpased: 0.918
batch start
#iterations: 269
currently lose_sum: 389.8822186589241
time_elpased: 0.934
batch start
#iterations: 270
currently lose_sum: 389.12760829925537
time_elpased: 0.917
batch start
#iterations: 271
currently lose_sum: 390.38425093889236
time_elpased: 0.931
batch start
#iterations: 272
currently lose_sum: 390.6366063952446
time_elpased: 0.928
batch start
#iterations: 273
currently lose_sum: 390.3050257563591
time_elpased: 0.924
batch start
#iterations: 274
currently lose_sum: 390.98575526475906
time_elpased: 0.926
batch start
#iterations: 275
currently lose_sum: 390.81805896759033
time_elpased: 0.921
batch start
#iterations: 276
currently lose_sum: 388.5835619568825
time_elpased: 0.93
batch start
#iterations: 277
currently lose_sum: 390.0576224923134
time_elpased: 0.923
batch start
#iterations: 278
currently lose_sum: 390.0440601706505
time_elpased: 0.943
batch start
#iterations: 279
currently lose_sum: 389.72515720129013
time_elpased: 0.924
start validation test
0.495360824742
1.0
0.495360824742
0.662530162013
0
validation finish
batch start
#iterations: 280
currently lose_sum: 390.4153698682785
time_elpased: 0.919
batch start
#iterations: 281
currently lose_sum: 390.74135851860046
time_elpased: 0.92
batch start
#iterations: 282
currently lose_sum: 389.7957189679146
time_elpased: 0.915
batch start
#iterations: 283
currently lose_sum: 390.30241030454636
time_elpased: 0.931
batch start
#iterations: 284
currently lose_sum: 390.1231565475464
time_elpased: 0.929
batch start
#iterations: 285
currently lose_sum: 389.4648646712303
time_elpased: 0.914
batch start
#iterations: 286
currently lose_sum: 388.9918263554573
time_elpased: 0.918
batch start
#iterations: 287
currently lose_sum: 389.1423488855362
time_elpased: 0.915
batch start
#iterations: 288
currently lose_sum: 389.8612611293793
time_elpased: 0.927
batch start
#iterations: 289
currently lose_sum: 389.9688844680786
time_elpased: 0.933
batch start
#iterations: 290
currently lose_sum: 390.14341926574707
time_elpased: 0.918
batch start
#iterations: 291
currently lose_sum: 389.86347484588623
time_elpased: 0.919
batch start
#iterations: 292
currently lose_sum: 390.38165122270584
time_elpased: 0.93
batch start
#iterations: 293
currently lose_sum: 390.43709194660187
time_elpased: 0.933
batch start
#iterations: 294
currently lose_sum: 389.91259014606476
time_elpased: 0.919
batch start
#iterations: 295
currently lose_sum: 389.56289172172546
time_elpased: 0.916
batch start
#iterations: 296
currently lose_sum: 388.966679751873
time_elpased: 0.917
batch start
#iterations: 297
currently lose_sum: 390.28795099258423
time_elpased: 0.919
batch start
#iterations: 298
currently lose_sum: 390.370118021965
time_elpased: 0.926
batch start
#iterations: 299
currently lose_sum: 389.39568346738815
time_elpased: 0.924
start validation test
0.528969072165
1.0
0.528969072165
0.691929067494
0
validation finish
batch start
#iterations: 300
currently lose_sum: 389.5020871758461
time_elpased: 0.913
batch start
#iterations: 301
currently lose_sum: 390.22701478004456
time_elpased: 0.915
batch start
#iterations: 302
currently lose_sum: 390.7064923644066
time_elpased: 0.926
batch start
#iterations: 303
currently lose_sum: 390.8205820918083
time_elpased: 0.92
batch start
#iterations: 304
currently lose_sum: 390.37985348701477
time_elpased: 0.917
batch start
#iterations: 305
currently lose_sum: 390.41282719373703
time_elpased: 0.924
batch start
#iterations: 306
currently lose_sum: 389.4419295191765
time_elpased: 0.928
batch start
#iterations: 307
currently lose_sum: 388.8850975036621
time_elpased: 0.916
batch start
#iterations: 308
currently lose_sum: 390.2705188989639
time_elpased: 0.931
batch start
#iterations: 309
currently lose_sum: 389.8139365911484
time_elpased: 0.931
batch start
#iterations: 310
currently lose_sum: 388.9825590848923
time_elpased: 0.935
batch start
#iterations: 311
currently lose_sum: 390.32105100154877
time_elpased: 0.925
batch start
#iterations: 312
currently lose_sum: 389.49027079343796
time_elpased: 0.925
batch start
#iterations: 313
currently lose_sum: 389.8177641630173
time_elpased: 0.913
batch start
#iterations: 314
currently lose_sum: 389.57777565717697
time_elpased: 0.926
batch start
#iterations: 315
currently lose_sum: 390.63245302438736
time_elpased: 0.925
batch start
#iterations: 316
currently lose_sum: 389.96510177850723
time_elpased: 0.928
batch start
#iterations: 317
currently lose_sum: 389.51685601472855
time_elpased: 0.919
batch start
#iterations: 318
currently lose_sum: 390.41592252254486
time_elpased: 0.924
batch start
#iterations: 319
currently lose_sum: 389.740928709507
time_elpased: 0.929
start validation test
0.524742268041
1.0
0.524742268041
0.68830290737
0
validation finish
batch start
#iterations: 320
currently lose_sum: 390.3390610218048
time_elpased: 0.92
batch start
#iterations: 321
currently lose_sum: 390.7081220149994
time_elpased: 0.916
batch start
#iterations: 322
currently lose_sum: 390.46275168657303
time_elpased: 0.915
batch start
#iterations: 323
currently lose_sum: 390.2700444459915
time_elpased: 0.921
batch start
#iterations: 324
currently lose_sum: 390.14819663763046
time_elpased: 0.924
batch start
#iterations: 325
currently lose_sum: 390.3553911447525
time_elpased: 0.921
batch start
#iterations: 326
currently lose_sum: 390.1525990366936
time_elpased: 0.923
batch start
#iterations: 327
currently lose_sum: 389.4269695878029
time_elpased: 0.918
batch start
#iterations: 328
currently lose_sum: 389.7331150174141
time_elpased: 0.925
batch start
#iterations: 329
currently lose_sum: 389.6000735759735
time_elpased: 0.919
batch start
#iterations: 330
currently lose_sum: 389.55056339502335
time_elpased: 0.924
batch start
#iterations: 331
currently lose_sum: 389.54818826913834
time_elpased: 0.924
batch start
#iterations: 332
currently lose_sum: 389.32376927137375
time_elpased: 0.921
batch start
#iterations: 333
currently lose_sum: 390.6967383623123
time_elpased: 0.935
batch start
#iterations: 334
currently lose_sum: 390.7691704630852
time_elpased: 0.921
batch start
#iterations: 335
currently lose_sum: 389.64014184474945
time_elpased: 0.923
batch start
#iterations: 336
currently lose_sum: 390.1457878947258
time_elpased: 0.918
batch start
#iterations: 337
currently lose_sum: 389.5973937511444
time_elpased: 0.918
batch start
#iterations: 338
currently lose_sum: 390.1405401825905
time_elpased: 0.924
batch start
#iterations: 339
currently lose_sum: 389.4319878220558
time_elpased: 0.933
start validation test
0.505979381443
1.0
0.505979381443
0.671960569551
0
validation finish
batch start
#iterations: 340
currently lose_sum: 389.79800230264664
time_elpased: 0.927
batch start
#iterations: 341
currently lose_sum: 390.6448554992676
time_elpased: 0.931
batch start
#iterations: 342
currently lose_sum: 390.3110653758049
time_elpased: 0.924
batch start
#iterations: 343
currently lose_sum: 389.550958275795
time_elpased: 0.944
batch start
#iterations: 344
currently lose_sum: 389.8769551515579
time_elpased: 0.93
batch start
#iterations: 345
currently lose_sum: 390.14891171455383
time_elpased: 0.922
batch start
#iterations: 346
currently lose_sum: 389.2151952981949
time_elpased: 0.92
batch start
#iterations: 347
currently lose_sum: 390.1996291875839
time_elpased: 0.928
batch start
#iterations: 348
currently lose_sum: 389.0416927933693
time_elpased: 0.923
batch start
#iterations: 349
currently lose_sum: 389.5402239561081
time_elpased: 0.93
batch start
#iterations: 350
currently lose_sum: 390.3851909637451
time_elpased: 0.929
batch start
#iterations: 351
currently lose_sum: 390.0761698484421
time_elpased: 0.923
batch start
#iterations: 352
currently lose_sum: 390.2024433016777
time_elpased: 0.92
batch start
#iterations: 353
currently lose_sum: 389.6213345527649
time_elpased: 0.93
batch start
#iterations: 354
currently lose_sum: 389.2246897816658
time_elpased: 0.921
batch start
#iterations: 355
currently lose_sum: 389.08844715356827
time_elpased: 0.929
batch start
#iterations: 356
currently lose_sum: 389.93244141340256
time_elpased: 0.921
batch start
#iterations: 357
currently lose_sum: 389.2860943078995
time_elpased: 0.921
batch start
#iterations: 358
currently lose_sum: 390.1338765025139
time_elpased: 0.916
batch start
#iterations: 359
currently lose_sum: 388.7896314263344
time_elpased: 0.929
start validation test
0.500721649485
1.0
0.500721649485
0.667307824414
0
validation finish
batch start
#iterations: 360
currently lose_sum: 390.1812962293625
time_elpased: 0.911
batch start
#iterations: 361
currently lose_sum: 390.2242374420166
time_elpased: 0.932
batch start
#iterations: 362
currently lose_sum: 389.59249126911163
time_elpased: 0.924
batch start
#iterations: 363
currently lose_sum: 391.1112892627716
time_elpased: 0.923
batch start
#iterations: 364
currently lose_sum: 389.3368143439293
time_elpased: 0.93
batch start
#iterations: 365
currently lose_sum: 389.8065551519394
time_elpased: 0.922
batch start
#iterations: 366
currently lose_sum: 390.43596655130386
time_elpased: 0.925
batch start
#iterations: 367
currently lose_sum: 390.7252821326256
time_elpased: 0.923
batch start
#iterations: 368
currently lose_sum: 388.74012327194214
time_elpased: 0.922
batch start
#iterations: 369
currently lose_sum: 389.03447514772415
time_elpased: 0.924
batch start
#iterations: 370
currently lose_sum: 388.9468413591385
time_elpased: 0.918
batch start
#iterations: 371
currently lose_sum: 390.24364590644836
time_elpased: 0.927
batch start
#iterations: 372
currently lose_sum: 389.7254899740219
time_elpased: 0.925
batch start
#iterations: 373
currently lose_sum: 389.9868707060814
time_elpased: 0.923
batch start
#iterations: 374
currently lose_sum: 390.2253447175026
time_elpased: 0.918
batch start
#iterations: 375
currently lose_sum: 389.4721040725708
time_elpased: 0.934
batch start
#iterations: 376
currently lose_sum: 389.5826027393341
time_elpased: 0.937
batch start
#iterations: 377
currently lose_sum: 390.368971824646
time_elpased: 0.919
batch start
#iterations: 378
currently lose_sum: 389.76171720027924
time_elpased: 0.935
batch start
#iterations: 379
currently lose_sum: 390.41912972927094
time_elpased: 0.921
start validation test
0.530206185567
1.0
0.530206185567
0.692986593007
0
validation finish
batch start
#iterations: 380
currently lose_sum: 389.54653692245483
time_elpased: 0.92
batch start
#iterations: 381
currently lose_sum: 390.0603178739548
time_elpased: 0.929
batch start
#iterations: 382
currently lose_sum: 389.593621969223
time_elpased: 0.913
batch start
#iterations: 383
currently lose_sum: 389.60634791851044
time_elpased: 0.918
batch start
#iterations: 384
currently lose_sum: 389.55954146385193
time_elpased: 0.921
batch start
#iterations: 385
currently lose_sum: 389.7050688266754
time_elpased: 0.921
batch start
#iterations: 386
currently lose_sum: 390.601478099823
time_elpased: 0.932
batch start
#iterations: 387
currently lose_sum: 389.62312811613083
time_elpased: 0.928
batch start
#iterations: 388
currently lose_sum: 389.3926605582237
time_elpased: 0.925
batch start
#iterations: 389
currently lose_sum: 389.92262107133865
time_elpased: 0.92
batch start
#iterations: 390
currently lose_sum: 389.51300102472305
time_elpased: 0.942
batch start
#iterations: 391
currently lose_sum: 389.62952119112015
time_elpased: 0.925
batch start
#iterations: 392
currently lose_sum: 389.22737741470337
time_elpased: 0.916
batch start
#iterations: 393
currently lose_sum: 390.0848575234413
time_elpased: 0.914
batch start
#iterations: 394
currently lose_sum: 389.5300495028496
time_elpased: 0.908
batch start
#iterations: 395
currently lose_sum: 389.98475980758667
time_elpased: 0.929
batch start
#iterations: 396
currently lose_sum: 390.37123388051987
time_elpased: 0.927
batch start
#iterations: 397
currently lose_sum: 388.7965694665909
time_elpased: 0.931
batch start
#iterations: 398
currently lose_sum: 389.9459041953087
time_elpased: 0.926
batch start
#iterations: 399
currently lose_sum: 389.25146466493607
time_elpased: 0.924
start validation test
0.530824742268
1.0
0.530824742268
0.693514714796
0
validation finish
acc: 0.558
pre: 1.000
rec: 0.558
F1: 0.716
auc: 0.000
