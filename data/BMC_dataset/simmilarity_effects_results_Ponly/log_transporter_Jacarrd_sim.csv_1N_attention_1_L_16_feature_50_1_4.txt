start to construct graph
graph construct over
58302
epochs start
batch start
#iterations: 0
currently lose_sum: 404.13836801052094
time_elpased: 1.117
batch start
#iterations: 1
currently lose_sum: 399.53145360946655
time_elpased: 1.133
batch start
#iterations: 2
currently lose_sum: 397.59667789936066
time_elpased: 1.085
batch start
#iterations: 3
currently lose_sum: 396.2476682662964
time_elpased: 1.09
batch start
#iterations: 4
currently lose_sum: 395.1045478582382
time_elpased: 1.09
batch start
#iterations: 5
currently lose_sum: 394.12351167201996
time_elpased: 1.081
batch start
#iterations: 6
currently lose_sum: 393.6025540828705
time_elpased: 1.081
batch start
#iterations: 7
currently lose_sum: 392.5100876688957
time_elpased: 1.079
batch start
#iterations: 8
currently lose_sum: 392.514480471611
time_elpased: 1.075
batch start
#iterations: 9
currently lose_sum: 393.23354482650757
time_elpased: 1.079
batch start
#iterations: 10
currently lose_sum: 392.19002664089203
time_elpased: 1.09
batch start
#iterations: 11
currently lose_sum: 391.9454057812691
time_elpased: 1.091
batch start
#iterations: 12
currently lose_sum: 391.5621484518051
time_elpased: 1.084
batch start
#iterations: 13
currently lose_sum: 392.28539460897446
time_elpased: 1.08
batch start
#iterations: 14
currently lose_sum: 391.1537798047066
time_elpased: 1.101
batch start
#iterations: 15
currently lose_sum: 391.17916548252106
time_elpased: 1.141
batch start
#iterations: 16
currently lose_sum: 390.58292466402054
time_elpased: 1.085
batch start
#iterations: 17
currently lose_sum: 390.03786170482635
time_elpased: 1.086
batch start
#iterations: 18
currently lose_sum: 390.349775493145
time_elpased: 1.092
batch start
#iterations: 19
currently lose_sum: 389.5866597890854
time_elpased: 1.082
start validation test
0.538453608247
1.0
0.538453608247
0.699993298935
0
validation finish
batch start
#iterations: 20
currently lose_sum: 390.1284370422363
time_elpased: 1.085
batch start
#iterations: 21
currently lose_sum: 390.20928478240967
time_elpased: 1.111
batch start
#iterations: 22
currently lose_sum: 389.02137464284897
time_elpased: 1.087
batch start
#iterations: 23
currently lose_sum: 389.34143203496933
time_elpased: 1.092
batch start
#iterations: 24
currently lose_sum: 388.4444851279259
time_elpased: 1.078
batch start
#iterations: 25
currently lose_sum: 389.06895738840103
time_elpased: 1.087
batch start
#iterations: 26
currently lose_sum: 388.8267183303833
time_elpased: 1.11
batch start
#iterations: 27
currently lose_sum: 388.66680812835693
time_elpased: 1.077
batch start
#iterations: 28
currently lose_sum: 388.2327317595482
time_elpased: 1.089
batch start
#iterations: 29
currently lose_sum: 388.6942912340164
time_elpased: 1.096
batch start
#iterations: 30
currently lose_sum: 387.8972941637039
time_elpased: 1.117
batch start
#iterations: 31
currently lose_sum: 389.3119440674782
time_elpased: 1.074
batch start
#iterations: 32
currently lose_sum: 387.60913586616516
time_elpased: 1.088
batch start
#iterations: 33
currently lose_sum: 388.8083041906357
time_elpased: 1.093
batch start
#iterations: 34
currently lose_sum: 387.9883586168289
time_elpased: 1.087
batch start
#iterations: 35
currently lose_sum: 387.6752616763115
time_elpased: 1.089
batch start
#iterations: 36
currently lose_sum: 388.0301055908203
time_elpased: 1.137
batch start
#iterations: 37
currently lose_sum: 387.6454563140869
time_elpased: 1.09
batch start
#iterations: 38
currently lose_sum: 388.7726117372513
time_elpased: 1.129
batch start
#iterations: 39
currently lose_sum: 388.4072952270508
time_elpased: 1.109
start validation test
0.547216494845
1.0
0.547216494845
0.707356076759
0
validation finish
batch start
#iterations: 40
currently lose_sum: 388.1950015425682
time_elpased: 1.126
batch start
#iterations: 41
currently lose_sum: 388.4642404913902
time_elpased: 1.093
batch start
#iterations: 42
currently lose_sum: 388.10998380184174
time_elpased: 1.081
batch start
#iterations: 43
currently lose_sum: 388.0692170262337
time_elpased: 1.085
batch start
#iterations: 44
currently lose_sum: 387.9732714891434
time_elpased: 1.082
batch start
#iterations: 45
currently lose_sum: 387.8421214222908
time_elpased: 1.089
batch start
#iterations: 46
currently lose_sum: 387.7658985853195
time_elpased: 1.09
batch start
#iterations: 47
currently lose_sum: 388.01112312078476
time_elpased: 1.097
batch start
#iterations: 48
currently lose_sum: 388.113432765007
time_elpased: 1.13
batch start
#iterations: 49
currently lose_sum: 388.1652981042862
time_elpased: 1.1
batch start
#iterations: 50
currently lose_sum: 388.10606849193573
time_elpased: 1.101
batch start
#iterations: 51
currently lose_sum: 387.4931356906891
time_elpased: 1.09
batch start
#iterations: 52
currently lose_sum: 387.9328619837761
time_elpased: 1.076
batch start
#iterations: 53
currently lose_sum: 388.19274163246155
time_elpased: 1.122
batch start
#iterations: 54
currently lose_sum: 387.8529691696167
time_elpased: 1.145
batch start
#iterations: 55
currently lose_sum: 386.93124610185623
time_elpased: 1.084
batch start
#iterations: 56
currently lose_sum: 387.0412404537201
time_elpased: 1.083
batch start
#iterations: 57
currently lose_sum: 387.1275248527527
time_elpased: 1.103
batch start
#iterations: 58
currently lose_sum: 387.48813539743423
time_elpased: 1.095
batch start
#iterations: 59
currently lose_sum: 387.4483339190483
time_elpased: 1.08
start validation test
0.481237113402
1.0
0.481237113402
0.649777282851
0
validation finish
batch start
#iterations: 60
currently lose_sum: 387.52979439496994
time_elpased: 1.093
batch start
#iterations: 61
currently lose_sum: 387.84093058109283
time_elpased: 1.128
batch start
#iterations: 62
currently lose_sum: 388.0746816396713
time_elpased: 1.138
batch start
#iterations: 63
currently lose_sum: 388.5327336192131
time_elpased: 1.112
batch start
#iterations: 64
currently lose_sum: 387.44562870264053
time_elpased: 1.085
batch start
#iterations: 65
currently lose_sum: 386.92373114824295
time_elpased: 1.119
batch start
#iterations: 66
currently lose_sum: 387.68218219280243
time_elpased: 1.142
batch start
#iterations: 67
currently lose_sum: 386.15730130672455
time_elpased: 1.093
batch start
#iterations: 68
currently lose_sum: 386.94962775707245
time_elpased: 1.09
batch start
#iterations: 69
currently lose_sum: 387.55707198381424
time_elpased: 1.094
batch start
#iterations: 70
currently lose_sum: 387.96129083633423
time_elpased: 1.1
batch start
#iterations: 71
currently lose_sum: 387.75410157442093
time_elpased: 1.087
batch start
#iterations: 72
currently lose_sum: 386.0087700486183
time_elpased: 1.111
batch start
#iterations: 73
currently lose_sum: 387.2638610005379
time_elpased: 1.085
batch start
#iterations: 74
currently lose_sum: 386.30242121219635
time_elpased: 1.1
batch start
#iterations: 75
currently lose_sum: 387.1380313038826
time_elpased: 1.155
batch start
#iterations: 76
currently lose_sum: 387.6558229327202
time_elpased: 1.141
batch start
#iterations: 77
currently lose_sum: 385.8354432582855
time_elpased: 1.121
batch start
#iterations: 78
currently lose_sum: 386.6873747110367
time_elpased: 1.075
batch start
#iterations: 79
currently lose_sum: 386.0083912014961
time_elpased: 1.117
start validation test
0.522371134021
1.0
0.522371134021
0.68625990384
0
validation finish
batch start
#iterations: 80
currently lose_sum: 387.42675989866257
time_elpased: 1.124
batch start
#iterations: 81
currently lose_sum: 387.6302179098129
time_elpased: 1.142
batch start
#iterations: 82
currently lose_sum: 386.7384188771248
time_elpased: 1.093
batch start
#iterations: 83
currently lose_sum: 386.69839465618134
time_elpased: 1.093
batch start
#iterations: 84
currently lose_sum: 386.8843890428543
time_elpased: 1.082
batch start
#iterations: 85
currently lose_sum: 387.01905113458633
time_elpased: 1.093
batch start
#iterations: 86
currently lose_sum: 386.3632024526596
time_elpased: 1.09
batch start
#iterations: 87
currently lose_sum: 387.4732948541641
time_elpased: 1.094
batch start
#iterations: 88
currently lose_sum: 387.00648498535156
time_elpased: 1.089
batch start
#iterations: 89
currently lose_sum: 387.4253391623497
time_elpased: 1.084
batch start
#iterations: 90
currently lose_sum: 386.91018187999725
time_elpased: 1.089
batch start
#iterations: 91
currently lose_sum: 386.1612065434456
time_elpased: 1.14
batch start
#iterations: 92
currently lose_sum: 386.5987613797188
time_elpased: 1.082
batch start
#iterations: 93
currently lose_sum: 386.7877005338669
time_elpased: 1.087
batch start
#iterations: 94
currently lose_sum: 388.82479828596115
time_elpased: 1.088
batch start
#iterations: 95
currently lose_sum: 386.4549629688263
time_elpased: 1.094
batch start
#iterations: 96
currently lose_sum: 387.48593205213547
time_elpased: 1.08
batch start
#iterations: 97
currently lose_sum: 387.1651419997215
time_elpased: 1.085
batch start
#iterations: 98
currently lose_sum: 387.10567861795425
time_elpased: 1.092
batch start
#iterations: 99
currently lose_sum: 386.2202698588371
time_elpased: 1.143
start validation test
0.51793814433
1.0
0.51793814433
0.68242325455
0
validation finish
batch start
#iterations: 100
currently lose_sum: 387.11829298734665
time_elpased: 1.088
batch start
#iterations: 101
currently lose_sum: 387.44732761383057
time_elpased: 1.135
batch start
#iterations: 102
currently lose_sum: 387.3565511107445
time_elpased: 1.108
batch start
#iterations: 103
currently lose_sum: 386.1150299310684
time_elpased: 1.096
batch start
#iterations: 104
currently lose_sum: 387.01931595802307
time_elpased: 1.102
batch start
#iterations: 105
currently lose_sum: 386.9061422944069
time_elpased: 1.128
batch start
#iterations: 106
currently lose_sum: 386.85036784410477
time_elpased: 1.093
batch start
#iterations: 107
currently lose_sum: 386.8635311126709
time_elpased: 1.149
batch start
#iterations: 108
currently lose_sum: 386.9223359823227
time_elpased: 1.09
batch start
#iterations: 109
currently lose_sum: 385.8398189544678
time_elpased: 1.085
batch start
#iterations: 110
currently lose_sum: 387.5819666981697
time_elpased: 1.102
batch start
#iterations: 111
currently lose_sum: 385.89320492744446
time_elpased: 1.149
batch start
#iterations: 112
currently lose_sum: 387.00427997112274
time_elpased: 1.083
batch start
#iterations: 113
currently lose_sum: 386.6527026295662
time_elpased: 1.148
batch start
#iterations: 114
currently lose_sum: 385.4681943655014
time_elpased: 1.144
batch start
#iterations: 115
currently lose_sum: 386.74577182531357
time_elpased: 1.131
batch start
#iterations: 116
currently lose_sum: 386.1326180100441
time_elpased: 1.092
batch start
#iterations: 117
currently lose_sum: 386.9678238630295
time_elpased: 1.136
batch start
#iterations: 118
currently lose_sum: 385.79809206724167
time_elpased: 1.156
batch start
#iterations: 119
currently lose_sum: 386.88649094104767
time_elpased: 1.093
start validation test
0.529690721649
1.0
0.529690721649
0.692546165251
0
validation finish
batch start
#iterations: 120
currently lose_sum: 386.7345771789551
time_elpased: 1.09
batch start
#iterations: 121
currently lose_sum: 386.31489312648773
time_elpased: 1.092
batch start
#iterations: 122
currently lose_sum: 386.4635396003723
time_elpased: 1.087
batch start
#iterations: 123
currently lose_sum: 386.6480602025986
time_elpased: 1.087
batch start
#iterations: 124
currently lose_sum: 386.60546058416367
time_elpased: 1.093
batch start
#iterations: 125
currently lose_sum: 386.53214633464813
time_elpased: 1.091
batch start
#iterations: 126
currently lose_sum: 385.7575379014015
time_elpased: 1.086
batch start
#iterations: 127
currently lose_sum: 385.7480701804161
time_elpased: 1.117
batch start
#iterations: 128
currently lose_sum: 386.22791624069214
time_elpased: 1.117
batch start
#iterations: 129
currently lose_sum: 386.53527492284775
time_elpased: 1.093
batch start
#iterations: 130
currently lose_sum: 385.5168950557709
time_elpased: 1.151
batch start
#iterations: 131
currently lose_sum: 387.01118236780167
time_elpased: 1.104
batch start
#iterations: 132
currently lose_sum: 386.06695234775543
time_elpased: 1.108
batch start
#iterations: 133
currently lose_sum: 386.2209892272949
time_elpased: 1.134
batch start
#iterations: 134
currently lose_sum: 386.3948930501938
time_elpased: 1.086
batch start
#iterations: 135
currently lose_sum: 385.7464917898178
time_elpased: 1.103
batch start
#iterations: 136
currently lose_sum: 385.947272837162
time_elpased: 1.102
batch start
#iterations: 137
currently lose_sum: 387.54237097501755
time_elpased: 1.097
batch start
#iterations: 138
currently lose_sum: 386.7324919104576
time_elpased: 1.094
batch start
#iterations: 139
currently lose_sum: 387.00178986787796
time_elpased: 1.088
start validation test
0.51381443299
1.0
0.51381443299
0.678834105148
0
validation finish
batch start
#iterations: 140
currently lose_sum: 385.93669348955154
time_elpased: 1.091
batch start
#iterations: 141
currently lose_sum: 386.91447108983994
time_elpased: 1.154
batch start
#iterations: 142
currently lose_sum: 386.1961282491684
time_elpased: 1.152
batch start
#iterations: 143
currently lose_sum: 386.6270800232887
time_elpased: 1.09
batch start
#iterations: 144
currently lose_sum: 386.23062723875046
time_elpased: 1.15
batch start
#iterations: 145
currently lose_sum: 385.91936177015305
time_elpased: 1.138
batch start
#iterations: 146
currently lose_sum: 387.42402017116547
time_elpased: 1.084
batch start
#iterations: 147
currently lose_sum: 386.4950124025345
time_elpased: 1.092
batch start
#iterations: 148
currently lose_sum: 386.7356128692627
time_elpased: 1.095
batch start
#iterations: 149
currently lose_sum: 385.8925483226776
time_elpased: 1.087
batch start
#iterations: 150
currently lose_sum: 386.5855897665024
time_elpased: 1.084
batch start
#iterations: 151
currently lose_sum: 385.18591463565826
time_elpased: 1.107
batch start
#iterations: 152
currently lose_sum: 385.8198063969612
time_elpased: 1.149
batch start
#iterations: 153
currently lose_sum: 386.3948078751564
time_elpased: 1.08
batch start
#iterations: 154
currently lose_sum: 385.54587841033936
time_elpased: 1.089
batch start
#iterations: 155
currently lose_sum: 386.6560710668564
time_elpased: 1.134
batch start
#iterations: 156
currently lose_sum: 386.0535500049591
time_elpased: 1.091
batch start
#iterations: 157
currently lose_sum: 385.99709206819534
time_elpased: 1.093
batch start
#iterations: 158
currently lose_sum: 385.8405154943466
time_elpased: 1.096
batch start
#iterations: 159
currently lose_sum: 385.7712388038635
time_elpased: 1.09
start validation test
0.526082474227
1.0
0.526082474227
0.689454840235
0
validation finish
batch start
#iterations: 160
currently lose_sum: 386.36593264341354
time_elpased: 1.112
batch start
#iterations: 161
currently lose_sum: 386.9299865961075
time_elpased: 1.082
batch start
#iterations: 162
currently lose_sum: 386.09234672784805
time_elpased: 1.09
batch start
#iterations: 163
currently lose_sum: 386.844411611557
time_elpased: 1.087
batch start
#iterations: 164
currently lose_sum: 386.22972321510315
time_elpased: 1.081
batch start
#iterations: 165
currently lose_sum: 386.9706943035126
time_elpased: 1.112
batch start
#iterations: 166
currently lose_sum: 385.7537370324135
time_elpased: 1.095
batch start
#iterations: 167
currently lose_sum: 386.17499047517776
time_elpased: 1.091
batch start
#iterations: 168
currently lose_sum: 386.3734048008919
time_elpased: 1.091
batch start
#iterations: 169
currently lose_sum: 385.8978109359741
time_elpased: 1.125
batch start
#iterations: 170
currently lose_sum: 386.01401668787
time_elpased: 1.16
batch start
#iterations: 171
currently lose_sum: 386.1751648783684
time_elpased: 1.094
batch start
#iterations: 172
currently lose_sum: 386.3374824523926
time_elpased: 1.09
batch start
#iterations: 173
currently lose_sum: 386.61029452085495
time_elpased: 1.104
batch start
#iterations: 174
currently lose_sum: 386.7329134941101
time_elpased: 1.101
batch start
#iterations: 175
currently lose_sum: 385.80775570869446
time_elpased: 1.08
batch start
#iterations: 176
currently lose_sum: 387.4617947936058
time_elpased: 1.163
batch start
#iterations: 177
currently lose_sum: 386.62403482198715
time_elpased: 1.087
batch start
#iterations: 178
currently lose_sum: 385.50573509931564
time_elpased: 1.092
batch start
#iterations: 179
currently lose_sum: 385.9113819003105
time_elpased: 1.104
start validation test
0.499278350515
1.0
0.499278350515
0.6660248917
0
validation finish
batch start
#iterations: 180
currently lose_sum: 386.1360926628113
time_elpased: 1.101
batch start
#iterations: 181
currently lose_sum: 384.9607542157173
time_elpased: 1.108
batch start
#iterations: 182
currently lose_sum: 385.9288759827614
time_elpased: 1.081
batch start
#iterations: 183
currently lose_sum: 386.4046545624733
time_elpased: 1.114
batch start
#iterations: 184
currently lose_sum: 385.52462619543076
time_elpased: 1.099
batch start
#iterations: 185
currently lose_sum: 384.9523048400879
time_elpased: 1.085
batch start
#iterations: 186
currently lose_sum: 386.1723533272743
time_elpased: 1.086
batch start
#iterations: 187
currently lose_sum: 385.66820722818375
time_elpased: 1.11
batch start
#iterations: 188
currently lose_sum: 385.8369601368904
time_elpased: 1.136
batch start
#iterations: 189
currently lose_sum: 385.72789669036865
time_elpased: 1.103
batch start
#iterations: 190
currently lose_sum: 386.4642766714096
time_elpased: 1.084
batch start
#iterations: 191
currently lose_sum: 385.98259204626083
time_elpased: 1.089
batch start
#iterations: 192
currently lose_sum: 386.3070764541626
time_elpased: 1.134
batch start
#iterations: 193
currently lose_sum: 385.525861620903
time_elpased: 1.096
batch start
#iterations: 194
currently lose_sum: 384.9523591995239
time_elpased: 1.091
batch start
#iterations: 195
currently lose_sum: 385.6146230697632
time_elpased: 1.085
batch start
#iterations: 196
currently lose_sum: 386.81012511253357
time_elpased: 1.085
batch start
#iterations: 197
currently lose_sum: 385.541958630085
time_elpased: 1.129
batch start
#iterations: 198
currently lose_sum: 386.0858259797096
time_elpased: 1.096
batch start
#iterations: 199
currently lose_sum: 385.480631172657
time_elpased: 1.086
start validation test
0.505979381443
1.0
0.505979381443
0.671960569551
0
validation finish
batch start
#iterations: 200
currently lose_sum: 386.15624767541885
time_elpased: 1.087
batch start
#iterations: 201
currently lose_sum: 385.40944623947144
time_elpased: 1.088
batch start
#iterations: 202
currently lose_sum: 385.3442177772522
time_elpased: 1.094
batch start
#iterations: 203
currently lose_sum: 385.99894881248474
time_elpased: 1.123
batch start
#iterations: 204
currently lose_sum: 386.1779353618622
time_elpased: 1.081
batch start
#iterations: 205
currently lose_sum: 385.289486348629
time_elpased: 1.085
batch start
#iterations: 206
currently lose_sum: 386.3991795182228
time_elpased: 1.094
batch start
#iterations: 207
currently lose_sum: 386.4080585241318
time_elpased: 1.084
batch start
#iterations: 208
currently lose_sum: 386.3839278817177
time_elpased: 1.082
batch start
#iterations: 209
currently lose_sum: 385.74748116731644
time_elpased: 1.094
batch start
#iterations: 210
currently lose_sum: 386.17001926898956
time_elpased: 1.093
batch start
#iterations: 211
currently lose_sum: 385.4937849640846
time_elpased: 1.118
batch start
#iterations: 212
currently lose_sum: 385.1368240714073
time_elpased: 1.149
batch start
#iterations: 213
currently lose_sum: 385.6330201625824
time_elpased: 1.09
batch start
#iterations: 214
currently lose_sum: 386.93540102243423
time_elpased: 1.102
batch start
#iterations: 215
currently lose_sum: 384.71961385011673
time_elpased: 1.107
batch start
#iterations: 216
currently lose_sum: 384.9993896484375
time_elpased: 1.086
batch start
#iterations: 217
currently lose_sum: 385.83749586343765
time_elpased: 1.085
batch start
#iterations: 218
currently lose_sum: 385.7698795199394
time_elpased: 1.096
batch start
#iterations: 219
currently lose_sum: 385.6804896593094
time_elpased: 1.091
start validation test
0.526907216495
1.0
0.526907216495
0.6901627169
0
validation finish
batch start
#iterations: 220
currently lose_sum: 385.29546654224396
time_elpased: 1.114
batch start
#iterations: 221
currently lose_sum: 385.8295617699623
time_elpased: 1.089
batch start
#iterations: 222
currently lose_sum: 385.25934690237045
time_elpased: 1.106
batch start
#iterations: 223
currently lose_sum: 384.70213508605957
time_elpased: 1.1
batch start
#iterations: 224
currently lose_sum: 386.36451840400696
time_elpased: 1.093
batch start
#iterations: 225
currently lose_sum: 386.29703348875046
time_elpased: 1.141
batch start
#iterations: 226
currently lose_sum: 386.20285099744797
time_elpased: 1.104
batch start
#iterations: 227
currently lose_sum: 385.9052747488022
time_elpased: 1.168
batch start
#iterations: 228
currently lose_sum: 386.0689222216606
time_elpased: 1.09
batch start
#iterations: 229
currently lose_sum: 386.55125999450684
time_elpased: 1.1
batch start
#iterations: 230
currently lose_sum: 386.3274469971657
time_elpased: 1.093
batch start
#iterations: 231
currently lose_sum: 385.74457907676697
time_elpased: 1.09
batch start
#iterations: 232
currently lose_sum: 386.82058453559875
time_elpased: 1.101
batch start
#iterations: 233
currently lose_sum: 385.5524079799652
time_elpased: 1.113
batch start
#iterations: 234
currently lose_sum: 385.3242853283882
time_elpased: 1.098
batch start
#iterations: 235
currently lose_sum: 385.5950384736061
time_elpased: 1.084
batch start
#iterations: 236
currently lose_sum: 386.37586057186127
time_elpased: 1.088
batch start
#iterations: 237
currently lose_sum: 385.59242540597916
time_elpased: 1.121
batch start
#iterations: 238
currently lose_sum: 386.70676869153976
time_elpased: 1.115
batch start
#iterations: 239
currently lose_sum: 386.7620086669922
time_elpased: 1.085
start validation test
0.500824742268
1.0
0.500824742268
0.667399368045
0
validation finish
batch start
#iterations: 240
currently lose_sum: 385.42226552963257
time_elpased: 1.126
batch start
#iterations: 241
currently lose_sum: 384.8136091828346
time_elpased: 1.099
batch start
#iterations: 242
currently lose_sum: 385.0564224720001
time_elpased: 1.09
batch start
#iterations: 243
currently lose_sum: 386.26073211431503
time_elpased: 1.092
batch start
#iterations: 244
currently lose_sum: 385.3257864713669
time_elpased: 1.141
batch start
#iterations: 245
currently lose_sum: 384.9545298218727
time_elpased: 1.144
batch start
#iterations: 246
currently lose_sum: 385.49322402477264
time_elpased: 1.095
batch start
#iterations: 247
currently lose_sum: 386.5352275967598
time_elpased: 1.119
batch start
#iterations: 248
currently lose_sum: 385.2719075679779
time_elpased: 1.085
batch start
#iterations: 249
currently lose_sum: 386.0339504480362
time_elpased: 1.147
batch start
#iterations: 250
currently lose_sum: 385.0267062187195
time_elpased: 1.086
batch start
#iterations: 251
currently lose_sum: 385.67661213874817
time_elpased: 1.097
batch start
#iterations: 252
currently lose_sum: 386.0965930223465
time_elpased: 1.093
batch start
#iterations: 253
currently lose_sum: 385.17845219373703
time_elpased: 1.088
batch start
#iterations: 254
currently lose_sum: 385.30220103263855
time_elpased: 1.097
batch start
#iterations: 255
currently lose_sum: 386.3104668855667
time_elpased: 1.092
batch start
#iterations: 256
currently lose_sum: 386.3389977812767
time_elpased: 1.129
batch start
#iterations: 257
currently lose_sum: 386.5298670530319
time_elpased: 1.081
batch start
#iterations: 258
currently lose_sum: 385.28633999824524
time_elpased: 1.111
batch start
#iterations: 259
currently lose_sum: 385.54586440324783
time_elpased: 1.153
start validation test
0.518556701031
1.0
0.518556701031
0.682959945689
0
validation finish
batch start
#iterations: 260
currently lose_sum: 385.8750544786453
time_elpased: 1.087
batch start
#iterations: 261
currently lose_sum: 384.83159375190735
time_elpased: 1.097
batch start
#iterations: 262
currently lose_sum: 384.99816620349884
time_elpased: 1.139
batch start
#iterations: 263
currently lose_sum: 386.36763191223145
time_elpased: 1.091
batch start
#iterations: 264
currently lose_sum: 385.500530064106
time_elpased: 1.106
batch start
#iterations: 265
currently lose_sum: 385.9476302266121
time_elpased: 1.083
batch start
#iterations: 266
currently lose_sum: 384.9659202694893
time_elpased: 1.09
batch start
#iterations: 267
currently lose_sum: 385.69494420289993
time_elpased: 1.107
batch start
#iterations: 268
currently lose_sum: 384.6024097800255
time_elpased: 1.093
batch start
#iterations: 269
currently lose_sum: 385.6028000116348
time_elpased: 1.086
batch start
#iterations: 270
currently lose_sum: 385.3017253279686
time_elpased: 1.111
batch start
#iterations: 271
currently lose_sum: 384.7388684153557
time_elpased: 1.122
batch start
#iterations: 272
currently lose_sum: 384.73459166288376
time_elpased: 1.096
batch start
#iterations: 273
currently lose_sum: 386.09209871292114
time_elpased: 1.171
batch start
#iterations: 274
currently lose_sum: 386.2868421077728
time_elpased: 1.095
batch start
#iterations: 275
currently lose_sum: 385.30737882852554
time_elpased: 1.088
batch start
#iterations: 276
currently lose_sum: 385.8826535344124
time_elpased: 1.083
batch start
#iterations: 277
currently lose_sum: 385.4874034523964
time_elpased: 1.232
batch start
#iterations: 278
currently lose_sum: 385.5937299132347
time_elpased: 1.077
batch start
#iterations: 279
currently lose_sum: 385.53357470035553
time_elpased: 1.096
start validation test
0.525257731959
1.0
0.525257731959
0.68874619804
0
validation finish
batch start
#iterations: 280
currently lose_sum: 385.72496914863586
time_elpased: 1.106
batch start
#iterations: 281
currently lose_sum: 384.621250808239
time_elpased: 1.092
batch start
#iterations: 282
currently lose_sum: 386.4149925708771
time_elpased: 1.086
batch start
#iterations: 283
currently lose_sum: 387.08365309238434
time_elpased: 1.088
batch start
#iterations: 284
currently lose_sum: 385.88584822416306
time_elpased: 1.092
batch start
#iterations: 285
currently lose_sum: 384.6051874756813
time_elpased: 1.117
batch start
#iterations: 286
currently lose_sum: 385.2611835002899
time_elpased: 1.092
batch start
#iterations: 287
currently lose_sum: 385.627846121788
time_elpased: 1.095
batch start
#iterations: 288
currently lose_sum: 385.76866441965103
time_elpased: 1.097
batch start
#iterations: 289
currently lose_sum: 385.82701712846756
time_elpased: 1.09
batch start
#iterations: 290
currently lose_sum: 385.7747241258621
time_elpased: 1.092
batch start
#iterations: 291
currently lose_sum: 384.89482337236404
time_elpased: 1.089
batch start
#iterations: 292
currently lose_sum: 385.1549226641655
time_elpased: 1.102
batch start
#iterations: 293
currently lose_sum: 385.77820402383804
time_elpased: 1.148
batch start
#iterations: 294
currently lose_sum: 384.7833003997803
time_elpased: 1.088
batch start
#iterations: 295
currently lose_sum: 385.0349212884903
time_elpased: 1.085
batch start
#iterations: 296
currently lose_sum: 384.37140595912933
time_elpased: 1.099
batch start
#iterations: 297
currently lose_sum: 385.21486461162567
time_elpased: 1.106
batch start
#iterations: 298
currently lose_sum: 385.2033213376999
time_elpased: 1.086
batch start
#iterations: 299
currently lose_sum: 385.2378385066986
time_elpased: 1.108
start validation test
0.525979381443
1.0
0.525979381443
0.689366301851
0
validation finish
batch start
#iterations: 300
currently lose_sum: 385.2805156111717
time_elpased: 1.101
batch start
#iterations: 301
currently lose_sum: 385.4930702447891
time_elpased: 1.131
batch start
#iterations: 302
currently lose_sum: 384.8661442399025
time_elpased: 1.119
batch start
#iterations: 303
currently lose_sum: 385.6265277862549
time_elpased: 1.088
batch start
#iterations: 304
currently lose_sum: 385.78472822904587
time_elpased: 1.083
batch start
#iterations: 305
currently lose_sum: 385.18754345178604
time_elpased: 1.127
batch start
#iterations: 306
currently lose_sum: 384.8457205295563
time_elpased: 1.093
batch start
#iterations: 307
currently lose_sum: 386.4771050810814
time_elpased: 1.087
batch start
#iterations: 308
currently lose_sum: 386.8320059776306
time_elpased: 1.096
batch start
#iterations: 309
currently lose_sum: 385.3302305340767
time_elpased: 1.101
batch start
#iterations: 310
currently lose_sum: 386.11710900068283
time_elpased: 1.1
batch start
#iterations: 311
currently lose_sum: 384.8727823495865
time_elpased: 1.094
batch start
#iterations: 312
currently lose_sum: 385.65674674510956
time_elpased: 1.091
batch start
#iterations: 313
currently lose_sum: 385.6312171816826
time_elpased: 1.122
batch start
#iterations: 314
currently lose_sum: 384.16956532001495
time_elpased: 1.105
batch start
#iterations: 315
currently lose_sum: 384.5885443687439
time_elpased: 1.103
batch start
#iterations: 316
currently lose_sum: 386.0099133849144
time_elpased: 1.086
batch start
#iterations: 317
currently lose_sum: 384.8388441801071
time_elpased: 1.09
batch start
#iterations: 318
currently lose_sum: 383.46197348833084
time_elpased: 1.138
batch start
#iterations: 319
currently lose_sum: 385.391032576561
time_elpased: 1.141
start validation test
0.509278350515
1.0
0.509278350515
0.674863387978
0
validation finish
batch start
#iterations: 320
currently lose_sum: 384.57450330257416
time_elpased: 1.082
batch start
#iterations: 321
currently lose_sum: 385.70719063282013
time_elpased: 1.093
batch start
#iterations: 322
currently lose_sum: 385.8125106692314
time_elpased: 1.075
batch start
#iterations: 323
currently lose_sum: 384.4017449617386
time_elpased: 1.127
batch start
#iterations: 324
currently lose_sum: 384.8082823753357
time_elpased: 1.094
batch start
#iterations: 325
currently lose_sum: 385.718567609787
time_elpased: 1.132
batch start
#iterations: 326
currently lose_sum: 385.67013585567474
time_elpased: 1.087
batch start
#iterations: 327
currently lose_sum: 385.3500498533249
time_elpased: 1.085
batch start
#iterations: 328
currently lose_sum: 385.5056872367859
time_elpased: 1.094
batch start
#iterations: 329
currently lose_sum: 385.483047246933
time_elpased: 1.088
batch start
#iterations: 330
currently lose_sum: 384.7106347680092
time_elpased: 1.106
batch start
#iterations: 331
currently lose_sum: 384.8096638917923
time_elpased: 1.125
batch start
#iterations: 332
currently lose_sum: 385.28477811813354
time_elpased: 1.13
batch start
#iterations: 333
currently lose_sum: 385.4443811774254
time_elpased: 1.097
batch start
#iterations: 334
currently lose_sum: 385.0295525789261
time_elpased: 1.12
batch start
#iterations: 335
currently lose_sum: 385.2277002334595
time_elpased: 1.132
batch start
#iterations: 336
currently lose_sum: 384.72858250141144
time_elpased: 1.11
batch start
#iterations: 337
currently lose_sum: 385.6015409231186
time_elpased: 1.086
batch start
#iterations: 338
currently lose_sum: 384.97213673591614
time_elpased: 1.087
batch start
#iterations: 339
currently lose_sum: 386.3082569241524
time_elpased: 1.093
start validation test
0.521649484536
1.0
0.521649484536
0.685636856369
0
validation finish
batch start
#iterations: 340
currently lose_sum: 385.3746226429939
time_elpased: 1.124
batch start
#iterations: 341
currently lose_sum: 384.3088958263397
time_elpased: 1.107
batch start
#iterations: 342
currently lose_sum: 384.2577219605446
time_elpased: 1.093
batch start
#iterations: 343
currently lose_sum: 386.1798976659775
time_elpased: 1.096
batch start
#iterations: 344
currently lose_sum: 385.97744250297546
time_elpased: 1.117
batch start
#iterations: 345
currently lose_sum: 384.5322881937027
time_elpased: 1.117
batch start
#iterations: 346
currently lose_sum: 384.5726106762886
time_elpased: 1.132
batch start
#iterations: 347
currently lose_sum: 384.9572678208351
time_elpased: 1.076
batch start
#iterations: 348
currently lose_sum: 385.86114996671677
time_elpased: 1.088
batch start
#iterations: 349
currently lose_sum: 385.2529199719429
time_elpased: 1.085
batch start
#iterations: 350
currently lose_sum: 384.6061223745346
time_elpased: 1.085
batch start
#iterations: 351
currently lose_sum: 385.13574343919754
time_elpased: 1.154
batch start
#iterations: 352
currently lose_sum: 385.3944433927536
time_elpased: 1.095
batch start
#iterations: 353
currently lose_sum: 385.5025749206543
time_elpased: 1.124
batch start
#iterations: 354
currently lose_sum: 384.2800903916359
time_elpased: 1.092
batch start
#iterations: 355
currently lose_sum: 385.5367210507393
time_elpased: 1.163
batch start
#iterations: 356
currently lose_sum: 386.00501465797424
time_elpased: 1.128
batch start
#iterations: 357
currently lose_sum: 384.9289382696152
time_elpased: 1.091
batch start
#iterations: 358
currently lose_sum: 384.9316799044609
time_elpased: 1.098
batch start
#iterations: 359
currently lose_sum: 386.1203702688217
time_elpased: 1.137
start validation test
0.510515463918
1.0
0.510515463918
0.675948675949
0
validation finish
batch start
#iterations: 360
currently lose_sum: 384.1543045639992
time_elpased: 1.099
batch start
#iterations: 361
currently lose_sum: 385.64557832479477
time_elpased: 1.108
batch start
#iterations: 362
currently lose_sum: 386.1877386569977
time_elpased: 1.098
batch start
#iterations: 363
currently lose_sum: 384.8460401892662
time_elpased: 1.1
batch start
#iterations: 364
currently lose_sum: 384.1443859934807
time_elpased: 1.09
batch start
#iterations: 365
currently lose_sum: 384.4048220515251
time_elpased: 1.096
batch start
#iterations: 366
currently lose_sum: 385.13725584745407
time_elpased: 1.133
batch start
#iterations: 367
currently lose_sum: 384.7100239992142
time_elpased: 1.112
batch start
#iterations: 368
currently lose_sum: 385.7729878425598
time_elpased: 1.096
batch start
#iterations: 369
currently lose_sum: 385.63503241539
time_elpased: 1.085
batch start
#iterations: 370
currently lose_sum: 385.642983853817
time_elpased: 1.082
batch start
#iterations: 371
currently lose_sum: 385.2423605918884
time_elpased: 1.084
batch start
#iterations: 372
currently lose_sum: 384.9903306365013
time_elpased: 1.1
batch start
#iterations: 373
currently lose_sum: 385.39451003074646
time_elpased: 1.099
batch start
#iterations: 374
currently lose_sum: 385.1851239800453
time_elpased: 1.149
batch start
#iterations: 375
currently lose_sum: 385.82747703790665
time_elpased: 1.12
batch start
#iterations: 376
currently lose_sum: 386.03310465812683
time_elpased: 1.14
batch start
#iterations: 377
currently lose_sum: 386.494263112545
time_elpased: 1.111
batch start
#iterations: 378
currently lose_sum: 384.41584837436676
time_elpased: 1.102
batch start
#iterations: 379
currently lose_sum: 385.55720484256744
time_elpased: 1.099
start validation test
0.506701030928
1.0
0.506701030928
0.67259664728
0
validation finish
batch start
#iterations: 380
currently lose_sum: 385.9608516097069
time_elpased: 1.112
batch start
#iterations: 381
currently lose_sum: 384.5222864151001
time_elpased: 1.087
batch start
#iterations: 382
currently lose_sum: 386.80426329374313
time_elpased: 1.112
batch start
#iterations: 383
currently lose_sum: 384.9280918240547
time_elpased: 1.127
batch start
#iterations: 384
currently lose_sum: 385.1243054866791
time_elpased: 1.089
batch start
#iterations: 385
currently lose_sum: 384.1028393507004
time_elpased: 1.092
batch start
#iterations: 386
currently lose_sum: 385.38112622499466
time_elpased: 1.106
batch start
#iterations: 387
currently lose_sum: 384.4611570239067
time_elpased: 1.128
batch start
#iterations: 388
currently lose_sum: 384.40403693914413
time_elpased: 1.153
batch start
#iterations: 389
currently lose_sum: 386.13193517923355
time_elpased: 1.099
batch start
#iterations: 390
currently lose_sum: 385.1478171348572
time_elpased: 1.133
batch start
#iterations: 391
currently lose_sum: 384.9352907538414
time_elpased: 1.082
batch start
#iterations: 392
currently lose_sum: 384.8701944947243
time_elpased: 1.08
batch start
#iterations: 393
currently lose_sum: 384.1233757138252
time_elpased: 1.155
batch start
#iterations: 394
currently lose_sum: 384.33617383241653
time_elpased: 1.094
batch start
#iterations: 395
currently lose_sum: 384.519584774971
time_elpased: 1.092
batch start
#iterations: 396
currently lose_sum: 384.23669624328613
time_elpased: 1.108
batch start
#iterations: 397
currently lose_sum: 385.0923800468445
time_elpased: 1.1
batch start
#iterations: 398
currently lose_sum: 386.1413894891739
time_elpased: 1.112
batch start
#iterations: 399
currently lose_sum: 385.0693660378456
time_elpased: 1.092
start validation test
0.517835051546
1.0
0.517835051546
0.682333763499
0
validation finish
acc: 0.538
pre: 1.000
rec: 0.538
F1: 0.699
auc: 0.000
