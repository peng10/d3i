start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 575.7803275585175
time_elpased: 1.388
batch start
#iterations: 1
currently lose_sum: 553.186828315258
time_elpased: 1.343
batch start
#iterations: 2
currently lose_sum: 548.4452806711197
time_elpased: 1.346
batch start
#iterations: 3
currently lose_sum: 545.1647811532021
time_elpased: 1.361
batch start
#iterations: 4
currently lose_sum: 543.320619225502
time_elpased: 1.354
batch start
#iterations: 5
currently lose_sum: 544.0863248705864
time_elpased: 1.357
batch start
#iterations: 6
currently lose_sum: 541.7194013595581
time_elpased: 1.348
batch start
#iterations: 7
currently lose_sum: 542.6047981977463
time_elpased: 1.352
batch start
#iterations: 8
currently lose_sum: 540.4886952638626
time_elpased: 1.365
batch start
#iterations: 9
currently lose_sum: 542.1274321675301
time_elpased: 1.362
batch start
#iterations: 10
currently lose_sum: 540.9595616459846
time_elpased: 1.363
batch start
#iterations: 11
currently lose_sum: 542.6304337382317
time_elpased: 1.351
batch start
#iterations: 12
currently lose_sum: 539.6919609904289
time_elpased: 1.35
batch start
#iterations: 13
currently lose_sum: 540.1403104662895
time_elpased: 1.364
batch start
#iterations: 14
currently lose_sum: 538.5044562220573
time_elpased: 1.35
batch start
#iterations: 15
currently lose_sum: 538.6647148132324
time_elpased: 1.357
batch start
#iterations: 16
currently lose_sum: 541.1182081103325
time_elpased: 1.374
batch start
#iterations: 17
currently lose_sum: 538.5934983193874
time_elpased: 1.367
batch start
#iterations: 18
currently lose_sum: 539.3992491364479
time_elpased: 1.356
batch start
#iterations: 19
currently lose_sum: 538.3787758350372
time_elpased: 1.361
start validation test
0.06
1.0
0.06
0.11320754717
0
validation finish
batch start
#iterations: 20
currently lose_sum: 538.7384212613106
time_elpased: 1.359
batch start
#iterations: 21
currently lose_sum: 540.2452372908592
time_elpased: 1.372
batch start
#iterations: 22
currently lose_sum: 540.1077647805214
time_elpased: 1.376
batch start
#iterations: 23
currently lose_sum: 539.2895349562168
time_elpased: 1.362
batch start
#iterations: 24
currently lose_sum: 537.7314160466194
time_elpased: 1.356
batch start
#iterations: 25
currently lose_sum: 538.7798520624638
time_elpased: 1.371
batch start
#iterations: 26
currently lose_sum: 538.6743496656418
time_elpased: 1.355
batch start
#iterations: 27
currently lose_sum: 539.1623756885529
time_elpased: 1.365
batch start
#iterations: 28
currently lose_sum: 538.0289083719254
time_elpased: 1.373
batch start
#iterations: 29
currently lose_sum: 538.8268920779228
time_elpased: 1.355
batch start
#iterations: 30
currently lose_sum: 537.5690258145332
time_elpased: 1.366
batch start
#iterations: 31
currently lose_sum: 537.8332318663597
time_elpased: 1.371
batch start
#iterations: 32
currently lose_sum: 540.0818945169449
time_elpased: 1.365
batch start
#iterations: 33
currently lose_sum: 537.0005241632462
time_elpased: 1.355
batch start
#iterations: 34
currently lose_sum: 538.8578487634659
time_elpased: 1.362
batch start
#iterations: 35
currently lose_sum: 537.4471834897995
time_elpased: 1.364
batch start
#iterations: 36
currently lose_sum: 538.7838625907898
time_elpased: 1.362
batch start
#iterations: 37
currently lose_sum: 537.8269434571266
time_elpased: 1.355
batch start
#iterations: 38
currently lose_sum: 538.266619861126
time_elpased: 1.35
batch start
#iterations: 39
currently lose_sum: 539.763744533062
time_elpased: 1.363
start validation test
0.0950515463918
1.0
0.0950515463918
0.1736019582
0
validation finish
batch start
#iterations: 40
currently lose_sum: 539.8234767913818
time_elpased: 1.358
batch start
#iterations: 41
currently lose_sum: 538.5672609806061
time_elpased: 1.341
batch start
#iterations: 42
currently lose_sum: 538.030991435051
time_elpased: 1.346
batch start
#iterations: 43
currently lose_sum: 537.6079280972481
time_elpased: 1.361
batch start
#iterations: 44
currently lose_sum: 536.7264883518219
time_elpased: 1.368
batch start
#iterations: 45
currently lose_sum: 538.7970428168774
time_elpased: 1.373
batch start
#iterations: 46
currently lose_sum: 537.5084981024265
time_elpased: 1.345
batch start
#iterations: 47
currently lose_sum: 538.5662197172642
time_elpased: 1.354
batch start
#iterations: 48
currently lose_sum: 536.9727989435196
time_elpased: 1.349
batch start
#iterations: 49
currently lose_sum: 536.9672482907772
time_elpased: 1.365
batch start
#iterations: 50
currently lose_sum: 539.2524769306183
time_elpased: 1.371
batch start
#iterations: 51
currently lose_sum: 537.8579813241959
time_elpased: 1.362
batch start
#iterations: 52
currently lose_sum: 538.5383810400963
time_elpased: 1.358
batch start
#iterations: 53
currently lose_sum: 538.1454440951347
time_elpased: 1.372
batch start
#iterations: 54
currently lose_sum: 536.9276248812675
time_elpased: 1.355
batch start
#iterations: 55
currently lose_sum: 538.2694621086121
time_elpased: 1.365
batch start
#iterations: 56
currently lose_sum: 538.1805661320686
time_elpased: 1.352
batch start
#iterations: 57
currently lose_sum: 536.9102812409401
time_elpased: 1.345
batch start
#iterations: 58
currently lose_sum: 537.3718134462833
time_elpased: 1.359
batch start
#iterations: 59
currently lose_sum: 536.6717844605446
time_elpased: 1.362
start validation test
0.0798969072165
1.0
0.0798969072165
0.147971360382
0
validation finish
batch start
#iterations: 60
currently lose_sum: 538.6257529258728
time_elpased: 1.37
batch start
#iterations: 61
currently lose_sum: 538.0700828135014
time_elpased: 1.353
batch start
#iterations: 62
currently lose_sum: 538.3426511585712
time_elpased: 1.389
batch start
#iterations: 63
currently lose_sum: 539.6274905204773
time_elpased: 1.348
batch start
#iterations: 64
currently lose_sum: 537.6276161074638
time_elpased: 1.359
batch start
#iterations: 65
currently lose_sum: 538.741810977459
time_elpased: 1.356
batch start
#iterations: 66
currently lose_sum: 536.7984262704849
time_elpased: 1.353
batch start
#iterations: 67
currently lose_sum: 535.9341346025467
time_elpased: 1.369
batch start
#iterations: 68
currently lose_sum: 538.8771130442619
time_elpased: 1.374
batch start
#iterations: 69
currently lose_sum: 538.9185609817505
time_elpased: 1.364
batch start
#iterations: 70
currently lose_sum: 536.6157360076904
time_elpased: 1.377
batch start
#iterations: 71
currently lose_sum: 538.4926439523697
time_elpased: 1.357
batch start
#iterations: 72
currently lose_sum: 539.3814076185226
time_elpased: 1.372
batch start
#iterations: 73
currently lose_sum: 537.2435619235039
time_elpased: 1.369
batch start
#iterations: 74
currently lose_sum: 538.533197760582
time_elpased: 1.372
batch start
#iterations: 75
currently lose_sum: 538.1968896090984
time_elpased: 1.356
batch start
#iterations: 76
currently lose_sum: 537.0195635259151
time_elpased: 1.364
batch start
#iterations: 77
currently lose_sum: 537.1828075647354
time_elpased: 1.364
batch start
#iterations: 78
currently lose_sum: 537.5930634737015
time_elpased: 1.365
batch start
#iterations: 79
currently lose_sum: 539.4534860253334
time_elpased: 1.357
start validation test
0.120515463918
1.0
0.120515463918
0.215107185574
0
validation finish
batch start
#iterations: 80
currently lose_sum: 537.292511343956
time_elpased: 1.367
batch start
#iterations: 81
currently lose_sum: 537.9468250870705
time_elpased: 1.358
batch start
#iterations: 82
currently lose_sum: 537.7205192744732
time_elpased: 1.369
batch start
#iterations: 83
currently lose_sum: 538.534905821085
time_elpased: 1.375
batch start
#iterations: 84
currently lose_sum: 537.176880300045
time_elpased: 1.382
batch start
#iterations: 85
currently lose_sum: 537.8248413801193
time_elpased: 1.388
batch start
#iterations: 86
currently lose_sum: 538.6660670042038
time_elpased: 1.359
batch start
#iterations: 87
currently lose_sum: 536.2034341990948
time_elpased: 1.371
batch start
#iterations: 88
currently lose_sum: 537.6632531881332
time_elpased: 1.358
batch start
#iterations: 89
currently lose_sum: 537.9286778569221
time_elpased: 1.359
batch start
#iterations: 90
currently lose_sum: 537.4965953826904
time_elpased: 1.351
batch start
#iterations: 91
currently lose_sum: 536.4744017124176
time_elpased: 1.36
batch start
#iterations: 92
currently lose_sum: 537.7319582700729
time_elpased: 1.38
batch start
#iterations: 93
currently lose_sum: 537.3461068272591
time_elpased: 1.372
batch start
#iterations: 94
currently lose_sum: 535.843814432621
time_elpased: 1.355
batch start
#iterations: 95
currently lose_sum: 538.65251070261
time_elpased: 1.358
batch start
#iterations: 96
currently lose_sum: 538.0680793523788
time_elpased: 1.371
batch start
#iterations: 97
currently lose_sum: 536.29386729002
time_elpased: 1.356
batch start
#iterations: 98
currently lose_sum: 537.7295426726341
time_elpased: 1.363
batch start
#iterations: 99
currently lose_sum: 538.6938089728355
time_elpased: 1.356
start validation test
0.0487628865979
1.0
0.0487628865979
0.0929912513516
0
validation finish
batch start
#iterations: 100
currently lose_sum: 538.2286595702171
time_elpased: 1.368
batch start
#iterations: 101
currently lose_sum: 537.6798200905323
time_elpased: 1.362
batch start
#iterations: 102
currently lose_sum: 535.9870790839195
time_elpased: 1.36
batch start
#iterations: 103
currently lose_sum: 537.7187579274178
time_elpased: 1.363
batch start
#iterations: 104
currently lose_sum: 538.6035536527634
time_elpased: 1.371
batch start
#iterations: 105
currently lose_sum: 536.953273832798
time_elpased: 1.392
batch start
#iterations: 106
currently lose_sum: 538.5376871228218
time_elpased: 1.353
batch start
#iterations: 107
currently lose_sum: 538.6707963347435
time_elpased: 1.357
batch start
#iterations: 108
currently lose_sum: 535.8083215355873
time_elpased: 1.355
batch start
#iterations: 109
currently lose_sum: 538.2235538959503
time_elpased: 1.358
batch start
#iterations: 110
currently lose_sum: 537.4538127779961
time_elpased: 1.362
batch start
#iterations: 111
currently lose_sum: 539.3468143343925
time_elpased: 1.379
batch start
#iterations: 112
currently lose_sum: 536.3743112683296
time_elpased: 1.378
batch start
#iterations: 113
currently lose_sum: 537.6598717570305
time_elpased: 1.375
batch start
#iterations: 114
currently lose_sum: 537.3285540342331
time_elpased: 1.352
batch start
#iterations: 115
currently lose_sum: 536.4511966109276
time_elpased: 1.359
batch start
#iterations: 116
currently lose_sum: 536.5247749090195
time_elpased: 1.396
batch start
#iterations: 117
currently lose_sum: 536.6830697953701
time_elpased: 1.361
batch start
#iterations: 118
currently lose_sum: 537.5391829609871
time_elpased: 1.359
batch start
#iterations: 119
currently lose_sum: 537.9478657245636
time_elpased: 1.379
start validation test
0.0960824742268
1.0
0.0960824742268
0.175319789315
0
validation finish
batch start
#iterations: 120
currently lose_sum: 537.6369141936302
time_elpased: 1.357
batch start
#iterations: 121
currently lose_sum: 536.5383005738258
time_elpased: 1.361
batch start
#iterations: 122
currently lose_sum: 536.3222719430923
time_elpased: 1.364
batch start
#iterations: 123
currently lose_sum: 536.6913523972034
time_elpased: 1.36
batch start
#iterations: 124
currently lose_sum: 536.767142623663
time_elpased: 1.359
batch start
#iterations: 125
currently lose_sum: 538.0969321131706
time_elpased: 1.366
batch start
#iterations: 126
currently lose_sum: 536.5805451273918
time_elpased: 1.359
batch start
#iterations: 127
currently lose_sum: 538.9267764985561
time_elpased: 1.364
batch start
#iterations: 128
currently lose_sum: 536.896998167038
time_elpased: 1.37
batch start
#iterations: 129
currently lose_sum: 538.5674918293953
time_elpased: 1.358
batch start
#iterations: 130
currently lose_sum: 537.0512456297874
time_elpased: 1.369
batch start
#iterations: 131
currently lose_sum: 536.2300013005733
time_elpased: 1.37
batch start
#iterations: 132
currently lose_sum: 537.0778013467789
time_elpased: 1.354
batch start
#iterations: 133
currently lose_sum: 538.7629652321339
time_elpased: 1.365
batch start
#iterations: 134
currently lose_sum: 537.149042069912
time_elpased: 1.357
batch start
#iterations: 135
currently lose_sum: 535.9338454008102
time_elpased: 1.361
batch start
#iterations: 136
currently lose_sum: 536.2127901017666
time_elpased: 1.349
batch start
#iterations: 137
currently lose_sum: 536.2589256167412
time_elpased: 1.369
batch start
#iterations: 138
currently lose_sum: 537.300324589014
time_elpased: 1.369
batch start
#iterations: 139
currently lose_sum: 537.6886062026024
time_elpased: 1.377
start validation test
0.0821649484536
1.0
0.0821649484536
0.151852910355
0
validation finish
batch start
#iterations: 140
currently lose_sum: 534.7374210953712
time_elpased: 1.356
batch start
#iterations: 141
currently lose_sum: 537.1080920100212
time_elpased: 1.378
batch start
#iterations: 142
currently lose_sum: 536.4288124740124
time_elpased: 1.361
batch start
#iterations: 143
currently lose_sum: 537.7912532687187
time_elpased: 1.354
batch start
#iterations: 144
currently lose_sum: 537.6138864457607
time_elpased: 1.385
batch start
#iterations: 145
currently lose_sum: 537.4905322790146
time_elpased: 1.365
batch start
#iterations: 146
currently lose_sum: 539.2020447850227
time_elpased: 1.367
batch start
#iterations: 147
currently lose_sum: 538.6983174681664
time_elpased: 1.38
batch start
#iterations: 148
currently lose_sum: 536.0706862509251
time_elpased: 1.354
batch start
#iterations: 149
currently lose_sum: 538.6880271434784
time_elpased: 1.362
batch start
#iterations: 150
currently lose_sum: 537.1308438777924
time_elpased: 1.366
batch start
#iterations: 151
currently lose_sum: 536.0050823092461
time_elpased: 1.374
batch start
#iterations: 152
currently lose_sum: 536.5767044126987
time_elpased: 1.359
batch start
#iterations: 153
currently lose_sum: 535.0259959697723
time_elpased: 1.361
batch start
#iterations: 154
currently lose_sum: 541.2790541648865
time_elpased: 1.378
batch start
#iterations: 155
currently lose_sum: 535.1817539930344
time_elpased: 1.381
batch start
#iterations: 156
currently lose_sum: 537.5090090036392
time_elpased: 1.36
batch start
#iterations: 157
currently lose_sum: 536.9759448766708
time_elpased: 1.356
batch start
#iterations: 158
currently lose_sum: 535.2554556727409
time_elpased: 1.364
batch start
#iterations: 159
currently lose_sum: 539.0139422416687
time_elpased: 1.375
start validation test
0.0737113402062
1.0
0.0737113402062
0.137301968315
0
validation finish
batch start
#iterations: 160
currently lose_sum: 537.5589033961296
time_elpased: 1.361
batch start
#iterations: 161
currently lose_sum: 537.829908490181
time_elpased: 1.36
batch start
#iterations: 162
currently lose_sum: 536.0660458803177
time_elpased: 1.372
batch start
#iterations: 163
currently lose_sum: 535.4409480690956
time_elpased: 1.37
batch start
#iterations: 164
currently lose_sum: 538.3414369821548
time_elpased: 1.364
batch start
#iterations: 165
currently lose_sum: 537.5063952207565
time_elpased: 1.362
batch start
#iterations: 166
currently lose_sum: 538.0683791935444
time_elpased: 1.353
batch start
#iterations: 167
currently lose_sum: 537.5410971045494
time_elpased: 1.365
batch start
#iterations: 168
currently lose_sum: 536.5901825428009
time_elpased: 1.358
batch start
#iterations: 169
currently lose_sum: 539.1587695479393
time_elpased: 1.359
batch start
#iterations: 170
currently lose_sum: 536.9848744273186
time_elpased: 1.366
batch start
#iterations: 171
currently lose_sum: 536.3462908864021
time_elpased: 1.366
batch start
#iterations: 172
currently lose_sum: 540.2869353294373
time_elpased: 1.364
batch start
#iterations: 173
currently lose_sum: 537.6509374976158
time_elpased: 1.376
batch start
#iterations: 174
currently lose_sum: 538.124757528305
time_elpased: 1.377
batch start
#iterations: 175
currently lose_sum: 536.3043883740902
time_elpased: 1.37
batch start
#iterations: 176
currently lose_sum: 537.5819475650787
time_elpased: 1.377
batch start
#iterations: 177
currently lose_sum: 537.0002888441086
time_elpased: 1.363
batch start
#iterations: 178
currently lose_sum: 536.8576716482639
time_elpased: 1.367
batch start
#iterations: 179
currently lose_sum: 538.2021752595901
time_elpased: 1.382
start validation test
0.0974226804124
1.0
0.0974226804124
0.177548144669
0
validation finish
batch start
#iterations: 180
currently lose_sum: 537.4774186611176
time_elpased: 1.375
batch start
#iterations: 181
currently lose_sum: 538.4567195177078
time_elpased: 1.359
batch start
#iterations: 182
currently lose_sum: 537.2466468214989
time_elpased: 1.37
batch start
#iterations: 183
currently lose_sum: 538.1755172610283
time_elpased: 1.381
batch start
#iterations: 184
currently lose_sum: 536.6751864850521
time_elpased: 1.37
batch start
#iterations: 185
currently lose_sum: 538.0061540007591
time_elpased: 1.366
batch start
#iterations: 186
currently lose_sum: 538.2923626899719
time_elpased: 1.378
batch start
#iterations: 187
currently lose_sum: 537.0605694651604
time_elpased: 1.364
batch start
#iterations: 188
currently lose_sum: 536.2873117923737
time_elpased: 1.37
batch start
#iterations: 189
currently lose_sum: 537.59350207448
time_elpased: 1.364
batch start
#iterations: 190
currently lose_sum: 535.7724665403366
time_elpased: 1.363
batch start
#iterations: 191
currently lose_sum: 538.2411269545555
time_elpased: 1.359
batch start
#iterations: 192
currently lose_sum: 535.6585882306099
time_elpased: 1.354
batch start
#iterations: 193
currently lose_sum: 536.0962823629379
time_elpased: 1.383
batch start
#iterations: 194
currently lose_sum: 538.3147755563259
time_elpased: 1.362
batch start
#iterations: 195
currently lose_sum: 537.2771379947662
time_elpased: 1.372
batch start
#iterations: 196
currently lose_sum: 535.4662743806839
time_elpased: 1.364
batch start
#iterations: 197
currently lose_sum: 538.4670305848122
time_elpased: 1.384
batch start
#iterations: 198
currently lose_sum: 536.8461509943008
time_elpased: 1.361
batch start
#iterations: 199
currently lose_sum: 535.7064279317856
time_elpased: 1.368
start validation test
0.0875257731959
1.0
0.0875257731959
0.160963124467
0
validation finish
batch start
#iterations: 200
currently lose_sum: 537.0697772204876
time_elpased: 1.371
batch start
#iterations: 201
currently lose_sum: 536.7423613667488
time_elpased: 1.353
batch start
#iterations: 202
currently lose_sum: 537.3077819645405
time_elpased: 1.358
batch start
#iterations: 203
currently lose_sum: 536.546350389719
time_elpased: 1.365
batch start
#iterations: 204
currently lose_sum: 536.5965684652328
time_elpased: 1.357
batch start
#iterations: 205
currently lose_sum: 538.290590941906
time_elpased: 1.344
batch start
#iterations: 206
currently lose_sum: 539.6060335934162
time_elpased: 1.378
batch start
#iterations: 207
currently lose_sum: 537.1996444165707
time_elpased: 1.36
batch start
#iterations: 208
currently lose_sum: 535.4614010751247
time_elpased: 1.36
batch start
#iterations: 209
currently lose_sum: 535.7651145458221
time_elpased: 1.368
batch start
#iterations: 210
currently lose_sum: 536.9247646927834
time_elpased: 1.372
batch start
#iterations: 211
currently lose_sum: 536.302924156189
time_elpased: 1.37
batch start
#iterations: 212
currently lose_sum: 537.4059468507767
time_elpased: 1.373
batch start
#iterations: 213
currently lose_sum: 536.1872408986092
time_elpased: 1.37
batch start
#iterations: 214
currently lose_sum: 537.7287026047707
time_elpased: 1.378
batch start
#iterations: 215
currently lose_sum: 536.7496797144413
time_elpased: 1.37
batch start
#iterations: 216
currently lose_sum: 538.0269967317581
time_elpased: 1.374
batch start
#iterations: 217
currently lose_sum: 537.8132827877998
time_elpased: 1.378
batch start
#iterations: 218
currently lose_sum: 537.442890048027
time_elpased: 1.37
batch start
#iterations: 219
currently lose_sum: 538.0879507660866
time_elpased: 1.362
start validation test
0.106597938144
1.0
0.106597938144
0.192658841066
0
validation finish
batch start
#iterations: 220
currently lose_sum: 536.1962867975235
time_elpased: 1.366
batch start
#iterations: 221
currently lose_sum: 538.2371448278427
time_elpased: 1.37
batch start
#iterations: 222
currently lose_sum: 536.6366805434227
time_elpased: 1.372
batch start
#iterations: 223
currently lose_sum: 536.5310754477978
time_elpased: 1.363
batch start
#iterations: 224
currently lose_sum: 538.6882789731026
time_elpased: 1.366
batch start
#iterations: 225
currently lose_sum: 538.8493404984474
time_elpased: 1.364
batch start
#iterations: 226
currently lose_sum: 537.6405606865883
time_elpased: 1.355
batch start
#iterations: 227
currently lose_sum: 538.5152069330215
time_elpased: 1.371
batch start
#iterations: 228
currently lose_sum: 535.199342250824
time_elpased: 1.365
batch start
#iterations: 229
currently lose_sum: 537.9598887562752
time_elpased: 1.367
batch start
#iterations: 230
currently lose_sum: 538.1168128848076
time_elpased: 1.363
batch start
#iterations: 231
currently lose_sum: 538.3976927697659
time_elpased: 1.379
batch start
#iterations: 232
currently lose_sum: 534.5275943279266
time_elpased: 1.362
batch start
#iterations: 233
currently lose_sum: 535.3905947804451
time_elpased: 1.359
batch start
#iterations: 234
currently lose_sum: 536.230150282383
time_elpased: 1.358
batch start
#iterations: 235
currently lose_sum: 536.8716296553612
time_elpased: 1.359
batch start
#iterations: 236
currently lose_sum: 537.2654116153717
time_elpased: 1.377
batch start
#iterations: 237
currently lose_sum: 537.0759699940681
time_elpased: 1.358
batch start
#iterations: 238
currently lose_sum: 536.2518701553345
time_elpased: 1.369
batch start
#iterations: 239
currently lose_sum: 534.7749963998795
time_elpased: 1.379
start validation test
0.0656701030928
1.0
0.0656701030928
0.12324658992
0
validation finish
batch start
#iterations: 240
currently lose_sum: 536.3185251951218
time_elpased: 1.357
batch start
#iterations: 241
currently lose_sum: 536.3352861404419
time_elpased: 1.375
batch start
#iterations: 242
currently lose_sum: 535.6282345056534
time_elpased: 1.36
batch start
#iterations: 243
currently lose_sum: 536.3489429354668
time_elpased: 1.365
batch start
#iterations: 244
currently lose_sum: 537.0468696951866
time_elpased: 1.351
batch start
#iterations: 245
currently lose_sum: 537.7899214923382
time_elpased: 1.357
batch start
#iterations: 246
currently lose_sum: 536.5943382978439
time_elpased: 1.358
batch start
#iterations: 247
currently lose_sum: 536.5574738383293
time_elpased: 1.363
batch start
#iterations: 248
currently lose_sum: 536.7025780081749
time_elpased: 1.37
batch start
#iterations: 249
currently lose_sum: 537.8581978082657
time_elpased: 1.357
batch start
#iterations: 250
currently lose_sum: 535.5925506353378
time_elpased: 1.377
batch start
#iterations: 251
currently lose_sum: 537.6748167276382
time_elpased: 1.366
batch start
#iterations: 252
currently lose_sum: 537.1451317369938
time_elpased: 1.369
batch start
#iterations: 253
currently lose_sum: 535.4812025427818
time_elpased: 1.364
batch start
#iterations: 254
currently lose_sum: 538.3294322490692
time_elpased: 1.366
batch start
#iterations: 255
currently lose_sum: 537.0070024132729
time_elpased: 1.366
batch start
#iterations: 256
currently lose_sum: 536.223152846098
time_elpased: 1.364
batch start
#iterations: 257
currently lose_sum: 535.5865317583084
time_elpased: 1.377
batch start
#iterations: 258
currently lose_sum: 536.9392293393612
time_elpased: 1.367
batch start
#iterations: 259
currently lose_sum: 537.3835000991821
time_elpased: 1.378
start validation test
0.112989690722
1.0
0.112989690722
0.203038162282
0
validation finish
batch start
#iterations: 260
currently lose_sum: 538.6660881638527
time_elpased: 1.367
batch start
#iterations: 261
currently lose_sum: 538.8708937466145
time_elpased: 1.361
batch start
#iterations: 262
currently lose_sum: 537.3977344036102
time_elpased: 1.369
batch start
#iterations: 263
currently lose_sum: 536.630584448576
time_elpased: 1.356
batch start
#iterations: 264
currently lose_sum: 536.5836161673069
time_elpased: 1.387
batch start
#iterations: 265
currently lose_sum: 536.7996478378773
time_elpased: 1.363
batch start
#iterations: 266
currently lose_sum: 537.1521806716919
time_elpased: 1.363
batch start
#iterations: 267
currently lose_sum: 536.0100280046463
time_elpased: 1.361
batch start
#iterations: 268
currently lose_sum: 537.1663956642151
time_elpased: 1.375
batch start
#iterations: 269
currently lose_sum: 537.0284953117371
time_elpased: 1.36
batch start
#iterations: 270
currently lose_sum: 536.5586574971676
time_elpased: 1.366
batch start
#iterations: 271
currently lose_sum: 533.9933696091175
time_elpased: 1.369
batch start
#iterations: 272
currently lose_sum: 537.9016174376011
time_elpased: 1.376
batch start
#iterations: 273
currently lose_sum: 535.3700073361397
time_elpased: 1.371
batch start
#iterations: 274
currently lose_sum: 537.3654761910439
time_elpased: 1.357
batch start
#iterations: 275
currently lose_sum: 536.332518517971
time_elpased: 1.38
batch start
#iterations: 276
currently lose_sum: 535.6602957844734
time_elpased: 1.381
batch start
#iterations: 277
currently lose_sum: 538.9892310500145
time_elpased: 1.356
batch start
#iterations: 278
currently lose_sum: 537.9164716601372
time_elpased: 1.362
batch start
#iterations: 279
currently lose_sum: 537.8915672600269
time_elpased: 1.368
start validation test
0.0944329896907
1.0
0.0944329896907
0.172569706104
0
validation finish
batch start
#iterations: 280
currently lose_sum: 536.6135583519936
time_elpased: 1.376
batch start
#iterations: 281
currently lose_sum: 535.7636556327343
time_elpased: 1.368
batch start
#iterations: 282
currently lose_sum: 536.909638851881
time_elpased: 1.354
batch start
#iterations: 283
currently lose_sum: 536.6351939439774
time_elpased: 1.363
batch start
#iterations: 284
currently lose_sum: 537.4371123313904
time_elpased: 1.371
batch start
#iterations: 285
currently lose_sum: 537.1062117815018
time_elpased: 1.37
batch start
#iterations: 286
currently lose_sum: 537.89863717556
time_elpased: 1.368
batch start
#iterations: 287
currently lose_sum: 537.2890060245991
time_elpased: 1.375
batch start
#iterations: 288
currently lose_sum: 536.9738847613335
time_elpased: 1.36
batch start
#iterations: 289
currently lose_sum: 537.0064015984535
time_elpased: 1.36
batch start
#iterations: 290
currently lose_sum: 535.9993408024311
time_elpased: 1.363
batch start
#iterations: 291
currently lose_sum: 538.3990920782089
time_elpased: 1.367
batch start
#iterations: 292
currently lose_sum: 537.4358382225037
time_elpased: 1.379
batch start
#iterations: 293
currently lose_sum: 536.6725580990314
time_elpased: 1.365
batch start
#iterations: 294
currently lose_sum: 535.6569024920464
time_elpased: 1.383
batch start
#iterations: 295
currently lose_sum: 537.3881491422653
time_elpased: 1.358
batch start
#iterations: 296
currently lose_sum: 534.4904741942883
time_elpased: 1.348
batch start
#iterations: 297
currently lose_sum: 536.9983921647072
time_elpased: 1.386
batch start
#iterations: 298
currently lose_sum: 536.1396653652191
time_elpased: 1.365
batch start
#iterations: 299
currently lose_sum: 536.2537152767181
time_elpased: 1.37
start validation test
0.112164948454
1.0
0.112164948454
0.201705598813
0
validation finish
batch start
#iterations: 300
currently lose_sum: 536.157881885767
time_elpased: 1.373
batch start
#iterations: 301
currently lose_sum: 536.32332649827
time_elpased: 1.374
batch start
#iterations: 302
currently lose_sum: 537.0118107795715
time_elpased: 1.369
batch start
#iterations: 303
currently lose_sum: 537.8044798970222
time_elpased: 1.366
batch start
#iterations: 304
currently lose_sum: 536.8105144500732
time_elpased: 1.367
batch start
#iterations: 305
currently lose_sum: 537.9094264507294
time_elpased: 1.363
batch start
#iterations: 306
currently lose_sum: 537.9998311400414
time_elpased: 1.368
batch start
#iterations: 307
currently lose_sum: 538.8306810855865
time_elpased: 1.368
batch start
#iterations: 308
currently lose_sum: 536.9975342154503
time_elpased: 1.367
batch start
#iterations: 309
currently lose_sum: 537.9383099079132
time_elpased: 1.368
batch start
#iterations: 310
currently lose_sum: 535.5861862897873
time_elpased: 1.365
batch start
#iterations: 311
currently lose_sum: 535.5994532704353
time_elpased: 1.371
batch start
#iterations: 312
currently lose_sum: 535.6212506592274
time_elpased: 1.372
batch start
#iterations: 313
currently lose_sum: 537.5993038117886
time_elpased: 1.363
batch start
#iterations: 314
currently lose_sum: 535.605623871088
time_elpased: 1.367
batch start
#iterations: 315
currently lose_sum: 536.9423671960831
time_elpased: 1.37
batch start
#iterations: 316
currently lose_sum: 537.787016838789
time_elpased: 1.377
batch start
#iterations: 317
currently lose_sum: 535.7948531806469
time_elpased: 1.367
batch start
#iterations: 318
currently lose_sum: 539.2915458679199
time_elpased: 1.373
batch start
#iterations: 319
currently lose_sum: 536.6164716482162
time_elpased: 1.37
start validation test
0.09
1.0
0.09
0.165137614679
0
validation finish
batch start
#iterations: 320
currently lose_sum: 537.3973130583763
time_elpased: 1.359
batch start
#iterations: 321
currently lose_sum: 535.6902929544449
time_elpased: 1.366
batch start
#iterations: 322
currently lose_sum: 535.1982616782188
time_elpased: 1.36
batch start
#iterations: 323
currently lose_sum: 536.0721881687641
time_elpased: 1.362
batch start
#iterations: 324
currently lose_sum: 539.6909210085869
time_elpased: 1.362
batch start
#iterations: 325
currently lose_sum: 537.8579071164131
time_elpased: 1.361
batch start
#iterations: 326
currently lose_sum: 537.8041552901268
time_elpased: 1.379
batch start
#iterations: 327
currently lose_sum: 538.0558650493622
time_elpased: 1.358
batch start
#iterations: 328
currently lose_sum: 536.4687802195549
time_elpased: 1.374
batch start
#iterations: 329
currently lose_sum: 538.0532930493355
time_elpased: 1.379
batch start
#iterations: 330
currently lose_sum: 538.105762809515
time_elpased: 1.36
batch start
#iterations: 331
currently lose_sum: 539.3410444557667
time_elpased: 1.373
batch start
#iterations: 332
currently lose_sum: 537.3917870521545
time_elpased: 1.377
batch start
#iterations: 333
currently lose_sum: 537.3974537849426
time_elpased: 1.373
batch start
#iterations: 334
currently lose_sum: 537.6185258626938
time_elpased: 1.362
batch start
#iterations: 335
currently lose_sum: 535.6575024724007
time_elpased: 1.368
batch start
#iterations: 336
currently lose_sum: 537.6019271016121
time_elpased: 1.365
batch start
#iterations: 337
currently lose_sum: 537.203776717186
time_elpased: 1.372
batch start
#iterations: 338
currently lose_sum: 538.1352899074554
time_elpased: 1.367
batch start
#iterations: 339
currently lose_sum: 537.3986282944679
time_elpased: 1.366
start validation test
0.0920618556701
1.0
0.0920618556701
0.16860190692
0
validation finish
batch start
#iterations: 340
currently lose_sum: 537.0943249464035
time_elpased: 1.362
batch start
#iterations: 341
currently lose_sum: 537.3387297391891
time_elpased: 1.367
batch start
#iterations: 342
currently lose_sum: 537.369443833828
time_elpased: 1.365
batch start
#iterations: 343
currently lose_sum: 537.2939941287041
time_elpased: 1.384
batch start
#iterations: 344
currently lose_sum: 537.5987143218517
time_elpased: 1.365
batch start
#iterations: 345
currently lose_sum: 537.7636872828007
time_elpased: 1.387
batch start
#iterations: 346
currently lose_sum: 535.6142225265503
time_elpased: 1.375
batch start
#iterations: 347
currently lose_sum: 536.2055115103722
time_elpased: 1.373
batch start
#iterations: 348
currently lose_sum: 537.279902279377
time_elpased: 1.367
batch start
#iterations: 349
currently lose_sum: 537.4742422103882
time_elpased: 1.387
batch start
#iterations: 350
currently lose_sum: 537.2357330322266
time_elpased: 1.361
batch start
#iterations: 351
currently lose_sum: 536.5517302453518
time_elpased: 1.364
batch start
#iterations: 352
currently lose_sum: 537.1076036393642
time_elpased: 1.372
batch start
#iterations: 353
currently lose_sum: 538.6405480504036
time_elpased: 1.372
batch start
#iterations: 354
currently lose_sum: 535.6519272327423
time_elpased: 1.365
batch start
#iterations: 355
currently lose_sum: 536.8248019814491
time_elpased: 1.362
batch start
#iterations: 356
currently lose_sum: 537.0756692290306
time_elpased: 1.366
batch start
#iterations: 357
currently lose_sum: 537.0339761078358
time_elpased: 1.368
batch start
#iterations: 358
currently lose_sum: 537.5739454030991
time_elpased: 1.373
batch start
#iterations: 359
currently lose_sum: 536.9085290133953
time_elpased: 1.375
start validation test
0.0905154639175
1.0
0.0905154639175
0.166004915863
0
validation finish
batch start
#iterations: 360
currently lose_sum: 538.2260164618492
time_elpased: 1.377
batch start
#iterations: 361
currently lose_sum: 534.2440041601658
time_elpased: 1.373
batch start
#iterations: 362
currently lose_sum: 537.1794139742851
time_elpased: 1.367
batch start
#iterations: 363
currently lose_sum: 536.8625272512436
time_elpased: 1.358
batch start
#iterations: 364
currently lose_sum: 537.1923699378967
time_elpased: 1.374
batch start
#iterations: 365
currently lose_sum: 536.1105711460114
time_elpased: 1.363
batch start
#iterations: 366
currently lose_sum: 538.6738929152489
time_elpased: 1.371
batch start
#iterations: 367
currently lose_sum: 537.978907853365
time_elpased: 1.367
batch start
#iterations: 368
currently lose_sum: 534.7915358245373
time_elpased: 1.371
batch start
#iterations: 369
currently lose_sum: 536.8461805582047
time_elpased: 1.383
batch start
#iterations: 370
currently lose_sum: 535.657478094101
time_elpased: 1.38
batch start
#iterations: 371
currently lose_sum: 537.3803893327713
time_elpased: 1.364
batch start
#iterations: 372
currently lose_sum: 536.823714017868
time_elpased: 1.364
batch start
#iterations: 373
currently lose_sum: 537.9167229533195
time_elpased: 1.375
batch start
#iterations: 374
currently lose_sum: 538.5182883739471
time_elpased: 1.368
batch start
#iterations: 375
currently lose_sum: 538.1930167078972
time_elpased: 1.368
batch start
#iterations: 376
currently lose_sum: 537.0323534607887
time_elpased: 1.369
batch start
#iterations: 377
currently lose_sum: 537.1508110165596
time_elpased: 1.365
batch start
#iterations: 378
currently lose_sum: 537.6904082894325
time_elpased: 1.376
batch start
#iterations: 379
currently lose_sum: 535.3733677864075
time_elpased: 1.374
start validation test
0.102474226804
1.0
0.102474226804
0.185898634748
0
validation finish
batch start
#iterations: 380
currently lose_sum: 537.5742951631546
time_elpased: 1.372
batch start
#iterations: 381
currently lose_sum: 536.3406391143799
time_elpased: 1.37
batch start
#iterations: 382
currently lose_sum: 536.6268570423126
time_elpased: 1.364
batch start
#iterations: 383
currently lose_sum: 534.6915974318981
time_elpased: 1.375
batch start
#iterations: 384
currently lose_sum: 535.3722421824932
time_elpased: 1.366
batch start
#iterations: 385
currently lose_sum: 535.8054838180542
time_elpased: 1.363
batch start
#iterations: 386
currently lose_sum: 537.127859711647
time_elpased: 1.37
batch start
#iterations: 387
currently lose_sum: 537.0777322649956
time_elpased: 1.379
batch start
#iterations: 388
currently lose_sum: 537.1900798678398
time_elpased: 1.371
batch start
#iterations: 389
currently lose_sum: 536.1018392443657
time_elpased: 1.378
batch start
#iterations: 390
currently lose_sum: 535.7621650099754
time_elpased: 1.364
batch start
#iterations: 391
currently lose_sum: 536.405816078186
time_elpased: 1.366
batch start
#iterations: 392
currently lose_sum: 536.2976595759392
time_elpased: 1.356
batch start
#iterations: 393
currently lose_sum: 536.7528523802757
time_elpased: 1.373
batch start
#iterations: 394
currently lose_sum: 537.1526775360107
time_elpased: 1.373
batch start
#iterations: 395
currently lose_sum: 536.246014803648
time_elpased: 1.359
batch start
#iterations: 396
currently lose_sum: 536.4764059782028
time_elpased: 1.38
batch start
#iterations: 397
currently lose_sum: 537.9636351466179
time_elpased: 1.364
batch start
#iterations: 398
currently lose_sum: 537.5414540469646
time_elpased: 1.36
batch start
#iterations: 399
currently lose_sum: 537.5362234115601
time_elpased: 1.363
start validation test
0.0805154639175
1.0
0.0805154639175
0.149031580956
0
validation finish
acc: 0.114
pre: 1.000
rec: 0.114
F1: 0.204
auc: 0.000
