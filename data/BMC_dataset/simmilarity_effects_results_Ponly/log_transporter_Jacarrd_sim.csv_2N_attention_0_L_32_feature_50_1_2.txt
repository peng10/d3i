start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 576.4545535445213
time_elpased: 1.398
batch start
#iterations: 1
currently lose_sum: 553.3829162716866
time_elpased: 1.378
batch start
#iterations: 2
currently lose_sum: 547.9168670773506
time_elpased: 1.381
batch start
#iterations: 3
currently lose_sum: 545.9352278709412
time_elpased: 1.394
batch start
#iterations: 4
currently lose_sum: 543.5445648431778
time_elpased: 1.361
batch start
#iterations: 5
currently lose_sum: 544.6549480557442
time_elpased: 1.379
batch start
#iterations: 6
currently lose_sum: 541.710278570652
time_elpased: 1.382
batch start
#iterations: 7
currently lose_sum: 542.5985624194145
time_elpased: 1.37
batch start
#iterations: 8
currently lose_sum: 540.4998943805695
time_elpased: 1.362
batch start
#iterations: 9
currently lose_sum: 541.6909465789795
time_elpased: 1.37
batch start
#iterations: 10
currently lose_sum: 541.5136127471924
time_elpased: 1.372
batch start
#iterations: 11
currently lose_sum: 542.4158355593681
time_elpased: 1.37
batch start
#iterations: 12
currently lose_sum: 541.4743832945824
time_elpased: 1.368
batch start
#iterations: 13
currently lose_sum: 538.7186695635319
time_elpased: 1.359
batch start
#iterations: 14
currently lose_sum: 539.605563044548
time_elpased: 1.386
batch start
#iterations: 15
currently lose_sum: 539.9725838899612
time_elpased: 1.373
batch start
#iterations: 16
currently lose_sum: 540.5952535271645
time_elpased: 1.376
batch start
#iterations: 17
currently lose_sum: 539.5416274964809
time_elpased: 1.375
batch start
#iterations: 18
currently lose_sum: 540.0735480189323
time_elpased: 1.373
batch start
#iterations: 19
currently lose_sum: 540.5731984376907
time_elpased: 1.381
start validation test
0.0581443298969
1.0
0.0581443298969
0.109898674981
0
validation finish
batch start
#iterations: 20
currently lose_sum: 540.0222934186459
time_elpased: 1.38
batch start
#iterations: 21
currently lose_sum: 542.1833646893501
time_elpased: 1.368
batch start
#iterations: 22
currently lose_sum: 539.5100554227829
time_elpased: 1.385
batch start
#iterations: 23
currently lose_sum: 540.08753657341
time_elpased: 1.383
batch start
#iterations: 24
currently lose_sum: 537.5982313454151
time_elpased: 1.389
batch start
#iterations: 25
currently lose_sum: 540.345408976078
time_elpased: 1.371
batch start
#iterations: 26
currently lose_sum: 539.6594977378845
time_elpased: 1.381
batch start
#iterations: 27
currently lose_sum: 539.3455169200897
time_elpased: 1.393
batch start
#iterations: 28
currently lose_sum: 539.4362307190895
time_elpased: 1.374
batch start
#iterations: 29
currently lose_sum: 539.573597073555
time_elpased: 1.377
batch start
#iterations: 30
currently lose_sum: 537.5909158587456
time_elpased: 1.383
batch start
#iterations: 31
currently lose_sum: 538.4260391592979
time_elpased: 1.375
batch start
#iterations: 32
currently lose_sum: 540.0722591876984
time_elpased: 1.386
batch start
#iterations: 33
currently lose_sum: 538.6598890423775
time_elpased: 1.387
batch start
#iterations: 34
currently lose_sum: 539.1226395964622
time_elpased: 1.372
batch start
#iterations: 35
currently lose_sum: 536.6221967637539
time_elpased: 1.384
batch start
#iterations: 36
currently lose_sum: 538.9685872793198
time_elpased: 1.393
batch start
#iterations: 37
currently lose_sum: 536.9254227280617
time_elpased: 1.389
batch start
#iterations: 38
currently lose_sum: 538.98687261343
time_elpased: 1.382
batch start
#iterations: 39
currently lose_sum: 539.6800374388695
time_elpased: 1.375
start validation test
0.0815463917526
1.0
0.0815463917526
0.150795920313
0
validation finish
batch start
#iterations: 40
currently lose_sum: 539.3908202648163
time_elpased: 1.375
batch start
#iterations: 41
currently lose_sum: 539.6577188372612
time_elpased: 1.375
batch start
#iterations: 42
currently lose_sum: 538.0602692961693
time_elpased: 1.382
batch start
#iterations: 43
currently lose_sum: 538.5431489646435
time_elpased: 1.372
batch start
#iterations: 44
currently lose_sum: 538.2576477527618
time_elpased: 1.367
batch start
#iterations: 45
currently lose_sum: 538.5627469718456
time_elpased: 1.391
batch start
#iterations: 46
currently lose_sum: 537.8734737634659
time_elpased: 1.381
batch start
#iterations: 47
currently lose_sum: 538.0226276218891
time_elpased: 1.367
batch start
#iterations: 48
currently lose_sum: 537.4950796961784
time_elpased: 1.369
batch start
#iterations: 49
currently lose_sum: 537.7599353194237
time_elpased: 1.376
batch start
#iterations: 50
currently lose_sum: 539.6213606595993
time_elpased: 1.382
batch start
#iterations: 51
currently lose_sum: 538.4138011932373
time_elpased: 1.367
batch start
#iterations: 52
currently lose_sum: 538.2299135923386
time_elpased: 1.39
batch start
#iterations: 53
currently lose_sum: 539.355647534132
time_elpased: 1.379
batch start
#iterations: 54
currently lose_sum: 537.2843558192253
time_elpased: 1.37
batch start
#iterations: 55
currently lose_sum: 538.6896104514599
time_elpased: 1.388
batch start
#iterations: 56
currently lose_sum: 537.838563144207
time_elpased: 1.381
batch start
#iterations: 57
currently lose_sum: 537.2782737016678
time_elpased: 1.388
batch start
#iterations: 58
currently lose_sum: 538.8632142543793
time_elpased: 1.373
batch start
#iterations: 59
currently lose_sum: 537.5847019553185
time_elpased: 1.373
start validation test
0.0864948453608
1.0
0.0864948453608
0.159218142139
0
validation finish
batch start
#iterations: 60
currently lose_sum: 539.2582119107246
time_elpased: 1.382
batch start
#iterations: 61
currently lose_sum: 539.2214285731316
time_elpased: 1.384
batch start
#iterations: 62
currently lose_sum: 540.3371090888977
time_elpased: 1.383
batch start
#iterations: 63
currently lose_sum: 539.9041919112206
time_elpased: 1.38
batch start
#iterations: 64
currently lose_sum: 538.974300891161
time_elpased: 1.393
batch start
#iterations: 65
currently lose_sum: 540.5851579904556
time_elpased: 1.377
batch start
#iterations: 66
currently lose_sum: 537.9288050234318
time_elpased: 1.384
batch start
#iterations: 67
currently lose_sum: 536.6518403291702
time_elpased: 1.371
batch start
#iterations: 68
currently lose_sum: 540.2502755522728
time_elpased: 1.376
batch start
#iterations: 69
currently lose_sum: 539.9232901334763
time_elpased: 1.377
batch start
#iterations: 70
currently lose_sum: 539.1146557927132
time_elpased: 1.378
batch start
#iterations: 71
currently lose_sum: 538.9043978452682
time_elpased: 1.385
batch start
#iterations: 72
currently lose_sum: 538.8767048716545
time_elpased: 1.378
batch start
#iterations: 73
currently lose_sum: 537.8931548595428
time_elpased: 1.362
batch start
#iterations: 74
currently lose_sum: 538.6031352877617
time_elpased: 1.378
batch start
#iterations: 75
currently lose_sum: 538.229799091816
time_elpased: 1.4
batch start
#iterations: 76
currently lose_sum: 539.7815008759499
time_elpased: 1.381
batch start
#iterations: 77
currently lose_sum: 538.2545994520187
time_elpased: 1.384
batch start
#iterations: 78
currently lose_sum: 538.5769503712654
time_elpased: 1.39
batch start
#iterations: 79
currently lose_sum: 539.4830170869827
time_elpased: 1.378
start validation test
0.090824742268
1.0
0.090824742268
0.166524903128
0
validation finish
batch start
#iterations: 80
currently lose_sum: 537.499299377203
time_elpased: 1.367
batch start
#iterations: 81
currently lose_sum: 538.7250627875328
time_elpased: 1.377
batch start
#iterations: 82
currently lose_sum: 538.792515873909
time_elpased: 1.371
batch start
#iterations: 83
currently lose_sum: 539.613557100296
time_elpased: 1.386
batch start
#iterations: 84
currently lose_sum: 538.9122982621193
time_elpased: 1.364
batch start
#iterations: 85
currently lose_sum: 537.0190969407558
time_elpased: 1.384
batch start
#iterations: 86
currently lose_sum: 538.4053066372871
time_elpased: 1.387
batch start
#iterations: 87
currently lose_sum: 536.943984568119
time_elpased: 1.372
batch start
#iterations: 88
currently lose_sum: 538.3001771271229
time_elpased: 1.367
batch start
#iterations: 89
currently lose_sum: 537.5365695953369
time_elpased: 1.392
batch start
#iterations: 90
currently lose_sum: 538.7844337821007
time_elpased: 1.388
batch start
#iterations: 91
currently lose_sum: 537.2217922210693
time_elpased: 1.368
batch start
#iterations: 92
currently lose_sum: 539.2371842861176
time_elpased: 1.391
batch start
#iterations: 93
currently lose_sum: 538.0957493782043
time_elpased: 1.377
batch start
#iterations: 94
currently lose_sum: 536.7906243801117
time_elpased: 1.376
batch start
#iterations: 95
currently lose_sum: 538.5604131221771
time_elpased: 1.387
batch start
#iterations: 96
currently lose_sum: 539.6994494199753
time_elpased: 1.378
batch start
#iterations: 97
currently lose_sum: 537.6362226009369
time_elpased: 1.38
batch start
#iterations: 98
currently lose_sum: 539.1481870412827
time_elpased: 1.377
batch start
#iterations: 99
currently lose_sum: 538.9050239920616
time_elpased: 1.375
start validation test
0.0553608247423
1.0
0.0553608247423
0.104913548891
0
validation finish
batch start
#iterations: 100
currently lose_sum: 538.1449596881866
time_elpased: 1.377
batch start
#iterations: 101
currently lose_sum: 538.3714598417282
time_elpased: 1.373
batch start
#iterations: 102
currently lose_sum: 536.8771724700928
time_elpased: 1.371
batch start
#iterations: 103
currently lose_sum: 538.3455318212509
time_elpased: 1.376
batch start
#iterations: 104
currently lose_sum: 538.3868072926998
time_elpased: 1.38
batch start
#iterations: 105
currently lose_sum: 538.1816722750664
time_elpased: 1.388
batch start
#iterations: 106
currently lose_sum: 538.9335852861404
time_elpased: 1.367
batch start
#iterations: 107
currently lose_sum: 538.5796176195145
time_elpased: 1.38
batch start
#iterations: 108
currently lose_sum: 536.3935729265213
time_elpased: 1.38
batch start
#iterations: 109
currently lose_sum: 538.7831885814667
time_elpased: 1.371
batch start
#iterations: 110
currently lose_sum: 538.6376896500587
time_elpased: 1.392
batch start
#iterations: 111
currently lose_sum: 539.7700448036194
time_elpased: 1.366
batch start
#iterations: 112
currently lose_sum: 537.6110136806965
time_elpased: 1.377
batch start
#iterations: 113
currently lose_sum: 538.0155040025711
time_elpased: 1.373
batch start
#iterations: 114
currently lose_sum: 537.7570764422417
time_elpased: 1.362
batch start
#iterations: 115
currently lose_sum: 538.4404690861702
time_elpased: 1.376
batch start
#iterations: 116
currently lose_sum: 536.6445322036743
time_elpased: 1.371
batch start
#iterations: 117
currently lose_sum: 538.0993902683258
time_elpased: 1.372
batch start
#iterations: 118
currently lose_sum: 537.9037810266018
time_elpased: 1.374
batch start
#iterations: 119
currently lose_sum: 538.3879015445709
time_elpased: 1.373
start validation test
0.0979381443299
1.0
0.0979381443299
0.178403755869
0
validation finish
batch start
#iterations: 120
currently lose_sum: 538.2441090941429
time_elpased: 1.374
batch start
#iterations: 121
currently lose_sum: 537.3035118579865
time_elpased: 1.379
batch start
#iterations: 122
currently lose_sum: 536.1359114646912
time_elpased: 1.386
batch start
#iterations: 123
currently lose_sum: 537.3408520519733
time_elpased: 1.392
batch start
#iterations: 124
currently lose_sum: 537.8302811384201
time_elpased: 1.386
batch start
#iterations: 125
currently lose_sum: 538.3353353142738
time_elpased: 1.37
batch start
#iterations: 126
currently lose_sum: 537.8911125659943
time_elpased: 1.378
batch start
#iterations: 127
currently lose_sum: 539.6089229881763
time_elpased: 1.367
batch start
#iterations: 128
currently lose_sum: 537.1264458596706
time_elpased: 1.375
batch start
#iterations: 129
currently lose_sum: 538.1185851991177
time_elpased: 1.374
batch start
#iterations: 130
currently lose_sum: 537.219785630703
time_elpased: 1.384
batch start
#iterations: 131
currently lose_sum: 537.3903531134129
time_elpased: 1.387
batch start
#iterations: 132
currently lose_sum: 538.7870180010796
time_elpased: 1.383
batch start
#iterations: 133
currently lose_sum: 539.2212558984756
time_elpased: 1.388
batch start
#iterations: 134
currently lose_sum: 538.8343231678009
time_elpased: 1.394
batch start
#iterations: 135
currently lose_sum: 536.3493719100952
time_elpased: 1.389
batch start
#iterations: 136
currently lose_sum: 538.3451580405235
time_elpased: 1.382
batch start
#iterations: 137
currently lose_sum: 537.3194313645363
time_elpased: 1.38
batch start
#iterations: 138
currently lose_sum: 537.8886241018772
time_elpased: 1.396
batch start
#iterations: 139
currently lose_sum: 538.0944442152977
time_elpased: 1.395
start validation test
0.0581443298969
1.0
0.0581443298969
0.109898674981
0
validation finish
batch start
#iterations: 140
currently lose_sum: 535.0534926950932
time_elpased: 1.393
batch start
#iterations: 141
currently lose_sum: 538.3051969408989
time_elpased: 1.388
batch start
#iterations: 142
currently lose_sum: 537.3589192926884
time_elpased: 1.393
batch start
#iterations: 143
currently lose_sum: 538.285543680191
time_elpased: 1.385
batch start
#iterations: 144
currently lose_sum: 537.5248481035233
time_elpased: 1.386
batch start
#iterations: 145
currently lose_sum: 538.914826542139
time_elpased: 1.377
batch start
#iterations: 146
currently lose_sum: 540.2313044071198
time_elpased: 1.385
batch start
#iterations: 147
currently lose_sum: 538.3508431017399
time_elpased: 1.371
batch start
#iterations: 148
currently lose_sum: 536.6198942661285
time_elpased: 1.389
batch start
#iterations: 149
currently lose_sum: 539.1904512345791
time_elpased: 1.37
batch start
#iterations: 150
currently lose_sum: 537.3457368612289
time_elpased: 1.39
batch start
#iterations: 151
currently lose_sum: 536.8296530246735
time_elpased: 1.388
batch start
#iterations: 152
currently lose_sum: 537.722305804491
time_elpased: 1.383
batch start
#iterations: 153
currently lose_sum: 536.1085113883018
time_elpased: 1.368
batch start
#iterations: 154
currently lose_sum: 540.4274893403053
time_elpased: 1.388
batch start
#iterations: 155
currently lose_sum: 536.1054235398769
time_elpased: 1.384
batch start
#iterations: 156
currently lose_sum: 538.3919885754585
time_elpased: 1.368
batch start
#iterations: 157
currently lose_sum: 537.6372942328453
time_elpased: 1.393
batch start
#iterations: 158
currently lose_sum: 536.599775582552
time_elpased: 1.388
batch start
#iterations: 159
currently lose_sum: 538.000237941742
time_elpased: 1.371
start validation test
0.0643298969072
1.0
0.0643298969072
0.120883378535
0
validation finish
batch start
#iterations: 160
currently lose_sum: 537.3859629631042
time_elpased: 1.396
batch start
#iterations: 161
currently lose_sum: 538.682829618454
time_elpased: 1.376
batch start
#iterations: 162
currently lose_sum: 537.6613035202026
time_elpased: 1.389
batch start
#iterations: 163
currently lose_sum: 535.8474008440971
time_elpased: 1.383
batch start
#iterations: 164
currently lose_sum: 539.4289198219776
time_elpased: 1.365
batch start
#iterations: 165
currently lose_sum: 537.2258049845695
time_elpased: 1.373
batch start
#iterations: 166
currently lose_sum: 537.978624522686
time_elpased: 1.381
batch start
#iterations: 167
currently lose_sum: 538.8735821247101
time_elpased: 1.384
batch start
#iterations: 168
currently lose_sum: 537.6734812259674
time_elpased: 1.373
batch start
#iterations: 169
currently lose_sum: 539.3169475793839
time_elpased: 1.387
batch start
#iterations: 170
currently lose_sum: 536.7730072140694
time_elpased: 1.38
batch start
#iterations: 171
currently lose_sum: 536.7125578522682
time_elpased: 1.377
batch start
#iterations: 172
currently lose_sum: 540.2289962768555
time_elpased: 1.371
batch start
#iterations: 173
currently lose_sum: 538.2680025994778
time_elpased: 1.386
batch start
#iterations: 174
currently lose_sum: 538.7390787005424
time_elpased: 1.365
batch start
#iterations: 175
currently lose_sum: 538.402376294136
time_elpased: 1.384
batch start
#iterations: 176
currently lose_sum: 537.8151251673698
time_elpased: 1.386
batch start
#iterations: 177
currently lose_sum: 538.7904737591743
time_elpased: 1.383
batch start
#iterations: 178
currently lose_sum: 537.1008412539959
time_elpased: 1.384
batch start
#iterations: 179
currently lose_sum: 538.8962818980217
time_elpased: 1.381
start validation test
0.0858762886598
1.0
0.0858762886598
0.158169562328
0
validation finish
batch start
#iterations: 180
currently lose_sum: 537.5671825408936
time_elpased: 1.377
batch start
#iterations: 181
currently lose_sum: 539.4179252684116
time_elpased: 1.371
batch start
#iterations: 182
currently lose_sum: 537.295806825161
time_elpased: 1.387
batch start
#iterations: 183
currently lose_sum: 538.3809059560299
time_elpased: 1.379
batch start
#iterations: 184
currently lose_sum: 537.3791641891003
time_elpased: 1.382
batch start
#iterations: 185
currently lose_sum: 536.8309146165848
time_elpased: 1.377
batch start
#iterations: 186
currently lose_sum: 539.3443365097046
time_elpased: 1.362
batch start
#iterations: 187
currently lose_sum: 537.039426356554
time_elpased: 1.396
batch start
#iterations: 188
currently lose_sum: 536.2208334505558
time_elpased: 1.371
batch start
#iterations: 189
currently lose_sum: 538.622432589531
time_elpased: 1.381
batch start
#iterations: 190
currently lose_sum: 536.5537925362587
time_elpased: 1.384
batch start
#iterations: 191
currently lose_sum: 538.8209020495415
time_elpased: 1.389
batch start
#iterations: 192
currently lose_sum: 537.3877195715904
time_elpased: 1.394
batch start
#iterations: 193
currently lose_sum: 537.8192209005356
time_elpased: 1.389
batch start
#iterations: 194
currently lose_sum: 537.7510741949081
time_elpased: 1.375
batch start
#iterations: 195
currently lose_sum: 538.5942458510399
time_elpased: 1.374
batch start
#iterations: 196
currently lose_sum: 535.5846549868584
time_elpased: 1.402
batch start
#iterations: 197
currently lose_sum: 538.9811929166317
time_elpased: 1.413
batch start
#iterations: 198
currently lose_sum: 537.8503650128841
time_elpased: 1.382
batch start
#iterations: 199
currently lose_sum: 537.7432823777199
time_elpased: 1.39
start validation test
0.089793814433
1.0
0.089793814433
0.164790464478
0
validation finish
batch start
#iterations: 200
currently lose_sum: 539.2642139196396
time_elpased: 1.389
batch start
#iterations: 201
currently lose_sum: 537.6576715409756
time_elpased: 1.382
batch start
#iterations: 202
currently lose_sum: 538.141294658184
time_elpased: 1.376
batch start
#iterations: 203
currently lose_sum: 537.137132614851
time_elpased: 1.374
batch start
#iterations: 204
currently lose_sum: 537.8701834082603
time_elpased: 1.373
batch start
#iterations: 205
currently lose_sum: 539.2007200717926
time_elpased: 1.366
batch start
#iterations: 206
currently lose_sum: 538.9847790002823
time_elpased: 1.376
batch start
#iterations: 207
currently lose_sum: 537.1312205791473
time_elpased: 1.376
batch start
#iterations: 208
currently lose_sum: 536.4774572849274
time_elpased: 1.382
batch start
#iterations: 209
currently lose_sum: 537.237283885479
time_elpased: 1.379
batch start
#iterations: 210
currently lose_sum: 539.3194513916969
time_elpased: 1.385
batch start
#iterations: 211
currently lose_sum: 539.0184190869331
time_elpased: 1.382
batch start
#iterations: 212
currently lose_sum: 537.9043202996254
time_elpased: 1.385
batch start
#iterations: 213
currently lose_sum: 537.0894498825073
time_elpased: 1.393
batch start
#iterations: 214
currently lose_sum: 538.8775142431259
time_elpased: 1.374
batch start
#iterations: 215
currently lose_sum: 536.9578676521778
time_elpased: 1.384
batch start
#iterations: 216
currently lose_sum: 539.3436350822449
time_elpased: 1.383
batch start
#iterations: 217
currently lose_sum: 538.1805348992348
time_elpased: 1.385
batch start
#iterations: 218
currently lose_sum: 538.4731150269508
time_elpased: 1.374
batch start
#iterations: 219
currently lose_sum: 538.7797074317932
time_elpased: 1.376
start validation test
0.0621649484536
1.0
0.0621649484536
0.117053285451
0
validation finish
batch start
#iterations: 220
currently lose_sum: 536.0038608312607
time_elpased: 1.387
batch start
#iterations: 221
currently lose_sum: 539.6815693974495
time_elpased: 1.369
batch start
#iterations: 222
currently lose_sum: 537.4670321345329
time_elpased: 1.393
batch start
#iterations: 223
currently lose_sum: 538.0247423052788
time_elpased: 1.404
batch start
#iterations: 224
currently lose_sum: 538.7834268808365
time_elpased: 1.372
batch start
#iterations: 225
currently lose_sum: 539.356487929821
time_elpased: 1.384
batch start
#iterations: 226
currently lose_sum: 538.563873231411
time_elpased: 1.393
batch start
#iterations: 227
currently lose_sum: 538.0982484817505
time_elpased: 1.379
batch start
#iterations: 228
currently lose_sum: 535.7909852266312
time_elpased: 1.381
batch start
#iterations: 229
currently lose_sum: 537.3473983407021
time_elpased: 1.38
batch start
#iterations: 230
currently lose_sum: 538.2120383381844
time_elpased: 1.374
batch start
#iterations: 231
currently lose_sum: 540.0473065376282
time_elpased: 1.382
batch start
#iterations: 232
currently lose_sum: 536.9895432591438
time_elpased: 1.37
batch start
#iterations: 233
currently lose_sum: 535.9381168484688
time_elpased: 1.382
batch start
#iterations: 234
currently lose_sum: 536.728598356247
time_elpased: 1.378
batch start
#iterations: 235
currently lose_sum: 537.2329992055893
time_elpased: 1.374
batch start
#iterations: 236
currently lose_sum: 536.8489055633545
time_elpased: 1.383
batch start
#iterations: 237
currently lose_sum: 537.3750850558281
time_elpased: 1.374
batch start
#iterations: 238
currently lose_sum: 537.0826954841614
time_elpased: 1.373
batch start
#iterations: 239
currently lose_sum: 535.8945696353912
time_elpased: 1.394
start validation test
0.0547422680412
1.0
0.0547422680412
0.103802169876
0
validation finish
batch start
#iterations: 240
currently lose_sum: 536.4256587028503
time_elpased: 1.382
batch start
#iterations: 241
currently lose_sum: 538.2762705683708
time_elpased: 1.388
batch start
#iterations: 242
currently lose_sum: 536.2715759277344
time_elpased: 1.388
batch start
#iterations: 243
currently lose_sum: 536.3672003149986
time_elpased: 1.37
batch start
#iterations: 244
currently lose_sum: 537.4857515096664
time_elpased: 1.378
batch start
#iterations: 245
currently lose_sum: 537.5759860277176
time_elpased: 1.369
batch start
#iterations: 246
currently lose_sum: 536.9128414094448
time_elpased: 1.384
batch start
#iterations: 247
currently lose_sum: 537.3761913180351
time_elpased: 1.38
batch start
#iterations: 248
currently lose_sum: 537.4680411219597
time_elpased: 1.389
batch start
#iterations: 249
currently lose_sum: 537.1697133779526
time_elpased: 1.38
batch start
#iterations: 250
currently lose_sum: 536.4730095565319
time_elpased: 1.398
batch start
#iterations: 251
currently lose_sum: 538.574104487896
time_elpased: 1.385
batch start
#iterations: 252
currently lose_sum: 538.6095381975174
time_elpased: 1.377
batch start
#iterations: 253
currently lose_sum: 536.7319954037666
time_elpased: 1.385
batch start
#iterations: 254
currently lose_sum: 539.2109379768372
time_elpased: 1.373
batch start
#iterations: 255
currently lose_sum: 536.301524579525
time_elpased: 1.383
batch start
#iterations: 256
currently lose_sum: 537.4482719302177
time_elpased: 1.406
batch start
#iterations: 257
currently lose_sum: 536.2096013426781
time_elpased: 1.398
batch start
#iterations: 258
currently lose_sum: 538.4760002493858
time_elpased: 1.384
batch start
#iterations: 259
currently lose_sum: 537.7750734090805
time_elpased: 1.374
start validation test
0.0919587628866
1.0
0.0919587628866
0.168429003021
0
validation finish
batch start
#iterations: 260
currently lose_sum: 539.7985663414001
time_elpased: 1.381
batch start
#iterations: 261
currently lose_sum: 539.4978519082069
time_elpased: 1.381
batch start
#iterations: 262
currently lose_sum: 537.845900952816
time_elpased: 1.379
batch start
#iterations: 263
currently lose_sum: 537.3973558545113
time_elpased: 1.381
batch start
#iterations: 264
currently lose_sum: 537.6729756593704
time_elpased: 1.383
batch start
#iterations: 265
currently lose_sum: 538.4563629031181
time_elpased: 1.401
batch start
#iterations: 266
currently lose_sum: 537.2928932309151
time_elpased: 1.388
batch start
#iterations: 267
currently lose_sum: 536.1031140089035
time_elpased: 1.397
batch start
#iterations: 268
currently lose_sum: 536.5427528619766
time_elpased: 1.376
batch start
#iterations: 269
currently lose_sum: 537.7335753440857
time_elpased: 1.381
batch start
#iterations: 270
currently lose_sum: 536.6428675055504
time_elpased: 1.394
batch start
#iterations: 271
currently lose_sum: 535.5350196957588
time_elpased: 1.383
batch start
#iterations: 272
currently lose_sum: 537.9520645141602
time_elpased: 1.381
batch start
#iterations: 273
currently lose_sum: 535.0016043782234
time_elpased: 1.39
batch start
#iterations: 274
currently lose_sum: 537.3022176921368
time_elpased: 1.378
batch start
#iterations: 275
currently lose_sum: 537.5533223748207
time_elpased: 1.365
batch start
#iterations: 276
currently lose_sum: 537.1671243906021
time_elpased: 1.368
batch start
#iterations: 277
currently lose_sum: 539.0457365512848
time_elpased: 1.386
batch start
#iterations: 278
currently lose_sum: 538.1002365350723
time_elpased: 1.389
batch start
#iterations: 279
currently lose_sum: 539.4573223590851
time_elpased: 1.381
start validation test
0.0677319587629
1.0
0.0677319587629
0.126870715458
0
validation finish
batch start
#iterations: 280
currently lose_sum: 535.390068769455
time_elpased: 1.375
batch start
#iterations: 281
currently lose_sum: 536.4477420449257
time_elpased: 1.38
batch start
#iterations: 282
currently lose_sum: 538.6604613363743
time_elpased: 1.375
batch start
#iterations: 283
currently lose_sum: 537.2587649226189
time_elpased: 1.379
batch start
#iterations: 284
currently lose_sum: 538.7528437376022
time_elpased: 1.376
batch start
#iterations: 285
currently lose_sum: 537.5571966171265
time_elpased: 1.383
batch start
#iterations: 286
currently lose_sum: 538.4472574591637
time_elpased: 1.392
batch start
#iterations: 287
currently lose_sum: 538.7058194279671
time_elpased: 1.375
batch start
#iterations: 288
currently lose_sum: 537.9612993001938
time_elpased: 1.384
batch start
#iterations: 289
currently lose_sum: 537.4990348219872
time_elpased: 1.375
batch start
#iterations: 290
currently lose_sum: 536.4061789512634
time_elpased: 1.371
batch start
#iterations: 291
currently lose_sum: 538.5528227686882
time_elpased: 1.376
batch start
#iterations: 292
currently lose_sum: 537.9485108852386
time_elpased: 1.386
batch start
#iterations: 293
currently lose_sum: 539.1503351926804
time_elpased: 1.391
batch start
#iterations: 294
currently lose_sum: 538.3604846298695
time_elpased: 1.385
batch start
#iterations: 295
currently lose_sum: 537.7512257695198
time_elpased: 1.382
batch start
#iterations: 296
currently lose_sum: 536.1455926299095
time_elpased: 1.373
batch start
#iterations: 297
currently lose_sum: 537.5248789191246
time_elpased: 1.385
batch start
#iterations: 298
currently lose_sum: 537.4284827709198
time_elpased: 1.385
batch start
#iterations: 299
currently lose_sum: 537.737771242857
time_elpased: 1.379
start validation test
0.0968041237113
1.0
0.0968041237113
0.176520349657
0
validation finish
batch start
#iterations: 300
currently lose_sum: 536.4655566811562
time_elpased: 1.378
batch start
#iterations: 301
currently lose_sum: 537.3688006997108
time_elpased: 1.384
batch start
#iterations: 302
currently lose_sum: 538.825158983469
time_elpased: 1.38
batch start
#iterations: 303
currently lose_sum: 541.2447152733803
time_elpased: 1.399
batch start
#iterations: 304
currently lose_sum: 534.8693707883358
time_elpased: 1.384
batch start
#iterations: 305
currently lose_sum: 538.0608906745911
time_elpased: 1.372
batch start
#iterations: 306
currently lose_sum: 538.2609699964523
time_elpased: 1.381
batch start
#iterations: 307
currently lose_sum: 538.9095542430878
time_elpased: 1.387
batch start
#iterations: 308
currently lose_sum: 538.1017082333565
time_elpased: 1.388
batch start
#iterations: 309
currently lose_sum: 538.7619785070419
time_elpased: 1.388
batch start
#iterations: 310
currently lose_sum: 536.6226022839546
time_elpased: 1.377
batch start
#iterations: 311
currently lose_sum: 536.7991521954536
time_elpased: 1.375
batch start
#iterations: 312
currently lose_sum: 537.435108423233
time_elpased: 1.376
batch start
#iterations: 313
currently lose_sum: 538.9774745106697
time_elpased: 1.384
batch start
#iterations: 314
currently lose_sum: 536.4273947477341
time_elpased: 1.39
batch start
#iterations: 315
currently lose_sum: 537.5651564002037
time_elpased: 1.385
batch start
#iterations: 316
currently lose_sum: 537.5332306623459
time_elpased: 1.384
batch start
#iterations: 317
currently lose_sum: 537.7513015270233
time_elpased: 1.379
batch start
#iterations: 318
currently lose_sum: 539.2980426549911
time_elpased: 1.367
batch start
#iterations: 319
currently lose_sum: 537.7843173742294
time_elpased: 1.381
start validation test
0.0737113402062
1.0
0.0737113402062
0.137301968315
0
validation finish
batch start
#iterations: 320
currently lose_sum: 538.3345744013786
time_elpased: 1.383
batch start
#iterations: 321
currently lose_sum: 538.7316602468491
time_elpased: 1.39
batch start
#iterations: 322
currently lose_sum: 537.2089039981365
time_elpased: 1.385
batch start
#iterations: 323
currently lose_sum: 535.9480192065239
time_elpased: 1.39
batch start
#iterations: 324
currently lose_sum: 540.588723897934
time_elpased: 1.388
batch start
#iterations: 325
currently lose_sum: 535.7640357613564
time_elpased: 1.374
batch start
#iterations: 326
currently lose_sum: 537.2047451138496
time_elpased: 1.391
batch start
#iterations: 327
currently lose_sum: 538.3855429291725
time_elpased: 1.386
batch start
#iterations: 328
currently lose_sum: 538.1528441905975
time_elpased: 1.386
batch start
#iterations: 329
currently lose_sum: 537.5804663896561
time_elpased: 1.378
batch start
#iterations: 330
currently lose_sum: 539.7630606293678
time_elpased: 1.378
batch start
#iterations: 331
currently lose_sum: 540.3978267908096
time_elpased: 1.357
batch start
#iterations: 332
currently lose_sum: 538.7788318991661
time_elpased: 1.372
batch start
#iterations: 333
currently lose_sum: 536.429517686367
time_elpased: 1.391
batch start
#iterations: 334
currently lose_sum: 537.3491441607475
time_elpased: 1.377
batch start
#iterations: 335
currently lose_sum: 536.288302063942
time_elpased: 1.381
batch start
#iterations: 336
currently lose_sum: 539.205793440342
time_elpased: 1.399
batch start
#iterations: 337
currently lose_sum: 538.0569719672203
time_elpased: 1.386
batch start
#iterations: 338
currently lose_sum: 537.0631194114685
time_elpased: 1.393
batch start
#iterations: 339
currently lose_sum: 538.6434872448444
time_elpased: 1.384
start validation test
0.0894845360825
1.0
0.0894845360825
0.164269492808
0
validation finish
batch start
#iterations: 340
currently lose_sum: 538.0794669985771
time_elpased: 1.386
batch start
#iterations: 341
currently lose_sum: 538.1116706728935
time_elpased: 1.386
batch start
#iterations: 342
currently lose_sum: 540.0569631457329
time_elpased: 1.393
batch start
#iterations: 343
currently lose_sum: 538.2804480493069
time_elpased: 1.396
batch start
#iterations: 344
currently lose_sum: 538.169031381607
time_elpased: 1.402
batch start
#iterations: 345
currently lose_sum: 538.3721410632133
time_elpased: 1.402
batch start
#iterations: 346
currently lose_sum: 536.0131480097771
time_elpased: 1.368
batch start
#iterations: 347
currently lose_sum: 537.24903896451
time_elpased: 1.373
batch start
#iterations: 348
currently lose_sum: 537.6143265962601
time_elpased: 1.409
batch start
#iterations: 349
currently lose_sum: 537.6631373763084
time_elpased: 1.376
batch start
#iterations: 350
currently lose_sum: 538.1580556035042
time_elpased: 1.373
batch start
#iterations: 351
currently lose_sum: 536.7346131205559
time_elpased: 1.426
batch start
#iterations: 352
currently lose_sum: 537.095407307148
time_elpased: 1.376
batch start
#iterations: 353
currently lose_sum: 537.9993188381195
time_elpased: 1.378
batch start
#iterations: 354
currently lose_sum: 537.0872056484222
time_elpased: 1.382
batch start
#iterations: 355
currently lose_sum: 537.4567689299583
time_elpased: 1.385
batch start
#iterations: 356
currently lose_sum: 538.4462344050407
time_elpased: 1.385
batch start
#iterations: 357
currently lose_sum: 537.9590475559235
time_elpased: 1.379
batch start
#iterations: 358
currently lose_sum: 537.8464348912239
time_elpased: 1.382
batch start
#iterations: 359
currently lose_sum: 537.153981089592
time_elpased: 1.378
start validation test
0.0768041237113
1.0
0.0768041237113
0.142651986596
0
validation finish
batch start
#iterations: 360
currently lose_sum: 537.8500368595123
time_elpased: 1.375
batch start
#iterations: 361
currently lose_sum: 536.1310492157936
time_elpased: 1.375
batch start
#iterations: 362
currently lose_sum: 537.2703801989555
time_elpased: 1.387
batch start
#iterations: 363
currently lose_sum: 537.6455079317093
time_elpased: 1.377
batch start
#iterations: 364
currently lose_sum: 537.9554806947708
time_elpased: 1.391
batch start
#iterations: 365
currently lose_sum: 536.6905980706215
time_elpased: 1.389
batch start
#iterations: 366
currently lose_sum: 538.27087444067
time_elpased: 1.379
batch start
#iterations: 367
currently lose_sum: 539.0181776881218
time_elpased: 1.385
batch start
#iterations: 368
currently lose_sum: 536.2282361388206
time_elpased: 1.377
batch start
#iterations: 369
currently lose_sum: 537.0877951979637
time_elpased: 1.396
batch start
#iterations: 370
currently lose_sum: 536.0841941833496
time_elpased: 1.394
batch start
#iterations: 371
currently lose_sum: 538.0933567881584
time_elpased: 1.386
batch start
#iterations: 372
currently lose_sum: 537.4183332920074
time_elpased: 1.382
batch start
#iterations: 373
currently lose_sum: 537.802850484848
time_elpased: 1.37
batch start
#iterations: 374
currently lose_sum: 539.6396160125732
time_elpased: 1.372
batch start
#iterations: 375
currently lose_sum: 539.1205652356148
time_elpased: 1.374
batch start
#iterations: 376
currently lose_sum: 538.351765871048
time_elpased: 1.387
batch start
#iterations: 377
currently lose_sum: 537.4827992916107
time_elpased: 1.378
batch start
#iterations: 378
currently lose_sum: 538.3899248838425
time_elpased: 1.378
batch start
#iterations: 379
currently lose_sum: 536.0122962594032
time_elpased: 1.399
start validation test
0.0686597938144
1.0
0.0686597938144
0.128497009454
0
validation finish
batch start
#iterations: 380
currently lose_sum: 538.3294924497604
time_elpased: 1.37
batch start
#iterations: 381
currently lose_sum: 536.969702899456
time_elpased: 1.375
batch start
#iterations: 382
currently lose_sum: 538.5747088193893
time_elpased: 1.368
batch start
#iterations: 383
currently lose_sum: 536.0272017717361
time_elpased: 1.379
batch start
#iterations: 384
currently lose_sum: 535.6880877614021
time_elpased: 1.371
batch start
#iterations: 385
currently lose_sum: 535.8948359489441
time_elpased: 1.366
batch start
#iterations: 386
currently lose_sum: 537.6588905453682
time_elpased: 1.372
batch start
#iterations: 387
currently lose_sum: 537.4829993247986
time_elpased: 1.38
batch start
#iterations: 388
currently lose_sum: 537.7301090955734
time_elpased: 1.371
batch start
#iterations: 389
currently lose_sum: 537.3059420585632
time_elpased: 1.384
batch start
#iterations: 390
currently lose_sum: 537.024126380682
time_elpased: 1.387
batch start
#iterations: 391
currently lose_sum: 538.545117855072
time_elpased: 1.391
batch start
#iterations: 392
currently lose_sum: 537.2035374045372
time_elpased: 1.389
batch start
#iterations: 393
currently lose_sum: 537.2975859045982
time_elpased: 1.378
batch start
#iterations: 394
currently lose_sum: 537.6417781114578
time_elpased: 1.379
batch start
#iterations: 395
currently lose_sum: 536.5216812491417
time_elpased: 1.386
batch start
#iterations: 396
currently lose_sum: 537.3845872879028
time_elpased: 1.38
batch start
#iterations: 397
currently lose_sum: 538.4483015835285
time_elpased: 1.395
batch start
#iterations: 398
currently lose_sum: 537.7553531527519
time_elpased: 1.375
batch start
#iterations: 399
currently lose_sum: 537.0538354516029
time_elpased: 1.378
start validation test
0.0726804123711
1.0
0.0726804123711
0.135511773186
0
validation finish
acc: 0.102
pre: 1.000
rec: 0.102
F1: 0.186
auc: 0.000
