start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 572.0689298510551
time_elpased: 1.647
batch start
#iterations: 1
currently lose_sum: 552.3181089758873
time_elpased: 1.617
batch start
#iterations: 2
currently lose_sum: 547.3077432513237
time_elpased: 1.645
batch start
#iterations: 3
currently lose_sum: 543.997730255127
time_elpased: 1.625
batch start
#iterations: 4
currently lose_sum: 541.9677053689957
time_elpased: 1.657
batch start
#iterations: 5
currently lose_sum: 542.8708010911942
time_elpased: 1.622
batch start
#iterations: 6
currently lose_sum: 539.9782061576843
time_elpased: 1.641
batch start
#iterations: 7
currently lose_sum: 541.1593900918961
time_elpased: 1.631
batch start
#iterations: 8
currently lose_sum: 539.0128067731857
time_elpased: 1.655
batch start
#iterations: 9
currently lose_sum: 540.587161719799
time_elpased: 1.643
batch start
#iterations: 10
currently lose_sum: 539.4887583255768
time_elpased: 1.689
batch start
#iterations: 11
currently lose_sum: 540.958131313324
time_elpased: 1.622
batch start
#iterations: 12
currently lose_sum: 537.7766490280628
time_elpased: 1.635
batch start
#iterations: 13
currently lose_sum: 538.2465087175369
time_elpased: 1.642
batch start
#iterations: 14
currently lose_sum: 536.7120690643787
time_elpased: 1.711
batch start
#iterations: 15
currently lose_sum: 537.1349833607674
time_elpased: 1.623
batch start
#iterations: 16
currently lose_sum: 538.677410453558
time_elpased: 1.684
batch start
#iterations: 17
currently lose_sum: 537.230253636837
time_elpased: 1.66
batch start
#iterations: 18
currently lose_sum: 536.8862837553024
time_elpased: 1.627
batch start
#iterations: 19
currently lose_sum: 536.3875765800476
time_elpased: 1.613
start validation test
0.07
1.0
0.07
0.130841121495
0
validation finish
batch start
#iterations: 20
currently lose_sum: 536.8473624587059
time_elpased: 1.644
batch start
#iterations: 21
currently lose_sum: 538.384409070015
time_elpased: 1.699
batch start
#iterations: 22
currently lose_sum: 538.1368355751038
time_elpased: 1.701
batch start
#iterations: 23
currently lose_sum: 536.9643908143044
time_elpased: 1.679
batch start
#iterations: 24
currently lose_sum: 535.1971045732498
time_elpased: 1.609
batch start
#iterations: 25
currently lose_sum: 536.7373651862144
time_elpased: 1.647
batch start
#iterations: 26
currently lose_sum: 536.904895812273
time_elpased: 1.671
batch start
#iterations: 27
currently lose_sum: 536.9752181768417
time_elpased: 1.679
batch start
#iterations: 28
currently lose_sum: 535.8401127457619
time_elpased: 1.663
batch start
#iterations: 29
currently lose_sum: 535.9861749410629
time_elpased: 1.624
batch start
#iterations: 30
currently lose_sum: 535.0082524716854
time_elpased: 1.614
batch start
#iterations: 31
currently lose_sum: 534.857065320015
time_elpased: 1.702
batch start
#iterations: 32
currently lose_sum: 537.3720939159393
time_elpased: 1.706
batch start
#iterations: 33
currently lose_sum: 534.5009426772594
time_elpased: 1.629
batch start
#iterations: 34
currently lose_sum: 535.943547129631
time_elpased: 1.665
batch start
#iterations: 35
currently lose_sum: 534.3491505384445
time_elpased: 1.698
batch start
#iterations: 36
currently lose_sum: 535.395792722702
time_elpased: 1.615
batch start
#iterations: 37
currently lose_sum: 534.556313931942
time_elpased: 1.617
batch start
#iterations: 38
currently lose_sum: 534.8399723172188
time_elpased: 1.701
batch start
#iterations: 39
currently lose_sum: 536.2346309423447
time_elpased: 1.69
start validation test
0.0852577319588
1.0
0.0852577319588
0.157119787214
0
validation finish
batch start
#iterations: 40
currently lose_sum: 536.2285219430923
time_elpased: 1.711
batch start
#iterations: 41
currently lose_sum: 534.742711186409
time_elpased: 1.616
batch start
#iterations: 42
currently lose_sum: 534.4444782733917
time_elpased: 1.651
batch start
#iterations: 43
currently lose_sum: 533.7790006101131
time_elpased: 1.638
batch start
#iterations: 44
currently lose_sum: 532.7434548139572
time_elpased: 1.666
batch start
#iterations: 45
currently lose_sum: 535.3660597801208
time_elpased: 1.633
batch start
#iterations: 46
currently lose_sum: 533.4270830750465
time_elpased: 1.62
batch start
#iterations: 47
currently lose_sum: 534.3835396468639
time_elpased: 1.702
batch start
#iterations: 48
currently lose_sum: 533.1975566148758
time_elpased: 1.616
batch start
#iterations: 49
currently lose_sum: 532.5555528104305
time_elpased: 1.656
batch start
#iterations: 50
currently lose_sum: 535.1176378130913
time_elpased: 1.636
batch start
#iterations: 51
currently lose_sum: 533.3913233578205
time_elpased: 1.69
batch start
#iterations: 52
currently lose_sum: 534.1093875467777
time_elpased: 1.617
batch start
#iterations: 53
currently lose_sum: 534.0553271174431
time_elpased: 1.678
batch start
#iterations: 54
currently lose_sum: 532.8604178726673
time_elpased: 1.71
batch start
#iterations: 55
currently lose_sum: 533.9191071093082
time_elpased: 1.697
batch start
#iterations: 56
currently lose_sum: 533.775746524334
time_elpased: 1.618
batch start
#iterations: 57
currently lose_sum: 532.2488138079643
time_elpased: 1.696
batch start
#iterations: 58
currently lose_sum: 533.219121158123
time_elpased: 1.62
batch start
#iterations: 59
currently lose_sum: 532.0044594109058
time_elpased: 1.648
start validation test
0.138453608247
1.0
0.138453608247
0.243231006067
0
validation finish
batch start
#iterations: 60
currently lose_sum: 534.7174242138863
time_elpased: 1.672
batch start
#iterations: 61
currently lose_sum: 534.1586689651012
time_elpased: 1.622
batch start
#iterations: 62
currently lose_sum: 534.1903177201748
time_elpased: 1.628
batch start
#iterations: 63
currently lose_sum: 534.6670715510845
time_elpased: 1.689
batch start
#iterations: 64
currently lose_sum: 533.1011011600494
time_elpased: 1.703
batch start
#iterations: 65
currently lose_sum: 534.1295809149742
time_elpased: 1.659
batch start
#iterations: 66
currently lose_sum: 531.8186993002892
time_elpased: 1.7
batch start
#iterations: 67
currently lose_sum: 531.4776764810085
time_elpased: 1.621
batch start
#iterations: 68
currently lose_sum: 534.4874873161316
time_elpased: 1.705
batch start
#iterations: 69
currently lose_sum: 534.2542046308517
time_elpased: 1.64
batch start
#iterations: 70
currently lose_sum: 532.1635345518589
time_elpased: 1.686
batch start
#iterations: 71
currently lose_sum: 533.895489692688
time_elpased: 1.649
batch start
#iterations: 72
currently lose_sum: 535.2576788663864
time_elpased: 1.66
batch start
#iterations: 73
currently lose_sum: 532.8359667360783
time_elpased: 1.637
batch start
#iterations: 74
currently lose_sum: 533.7418602705002
time_elpased: 1.649
batch start
#iterations: 75
currently lose_sum: 533.7511060535908
time_elpased: 1.691
batch start
#iterations: 76
currently lose_sum: 532.7654109299183
time_elpased: 1.662
batch start
#iterations: 77
currently lose_sum: 532.3116625547409
time_elpased: 1.635
batch start
#iterations: 78
currently lose_sum: 532.4874308109283
time_elpased: 1.637
batch start
#iterations: 79
currently lose_sum: 534.6772063970566
time_elpased: 1.663
start validation test
0.159278350515
1.0
0.159278350515
0.27478879502
0
validation finish
batch start
#iterations: 80
currently lose_sum: 532.6347677409649
time_elpased: 1.639
batch start
#iterations: 81
currently lose_sum: 533.2357548475266
time_elpased: 1.697
batch start
#iterations: 82
currently lose_sum: 533.1754302382469
time_elpased: 1.641
batch start
#iterations: 83
currently lose_sum: 534.024739086628
time_elpased: 1.614
batch start
#iterations: 84
currently lose_sum: 531.5839912891388
time_elpased: 1.651
batch start
#iterations: 85
currently lose_sum: 533.3877603113651
time_elpased: 1.617
batch start
#iterations: 86
currently lose_sum: 534.1596981883049
time_elpased: 1.667
batch start
#iterations: 87
currently lose_sum: 531.6951950788498
time_elpased: 1.645
batch start
#iterations: 88
currently lose_sum: 533.1860319077969
time_elpased: 1.695
batch start
#iterations: 89
currently lose_sum: 533.3189761042595
time_elpased: 1.652
batch start
#iterations: 90
currently lose_sum: 532.4670943319798
time_elpased: 1.642
batch start
#iterations: 91
currently lose_sum: 531.9729557037354
time_elpased: 1.709
batch start
#iterations: 92
currently lose_sum: 533.1789411306381
time_elpased: 1.723
batch start
#iterations: 93
currently lose_sum: 532.6202560067177
time_elpased: 1.697
batch start
#iterations: 94
currently lose_sum: 530.8015758991241
time_elpased: 1.624
batch start
#iterations: 95
currently lose_sum: 533.544431835413
time_elpased: 1.651
batch start
#iterations: 96
currently lose_sum: 532.9321981668472
time_elpased: 1.618
batch start
#iterations: 97
currently lose_sum: 531.327526062727
time_elpased: 1.672
batch start
#iterations: 98
currently lose_sum: 532.7805816233158
time_elpased: 1.632
batch start
#iterations: 99
currently lose_sum: 533.738301217556
time_elpased: 1.629
start validation test
0.0646391752577
1.0
0.0646391752577
0.121429263097
0
validation finish
batch start
#iterations: 100
currently lose_sum: 532.804728269577
time_elpased: 1.692
batch start
#iterations: 101
currently lose_sum: 533.1936354935169
time_elpased: 1.616
batch start
#iterations: 102
currently lose_sum: 530.3346700072289
time_elpased: 1.641
batch start
#iterations: 103
currently lose_sum: 532.4670480191708
time_elpased: 1.715
batch start
#iterations: 104
currently lose_sum: 533.9831340312958
time_elpased: 1.683
batch start
#iterations: 105
currently lose_sum: 531.9465957283974
time_elpased: 1.628
batch start
#iterations: 106
currently lose_sum: 533.7015933096409
time_elpased: 1.612
batch start
#iterations: 107
currently lose_sum: 533.632960408926
time_elpased: 1.653
batch start
#iterations: 108
currently lose_sum: 530.8562824726105
time_elpased: 1.702
batch start
#iterations: 109
currently lose_sum: 533.620578110218
time_elpased: 1.675
batch start
#iterations: 110
currently lose_sum: 532.0954267382622
time_elpased: 1.687
batch start
#iterations: 111
currently lose_sum: 534.5161298215389
time_elpased: 1.683
batch start
#iterations: 112
currently lose_sum: 531.7087089121342
time_elpased: 1.642
batch start
#iterations: 113
currently lose_sum: 532.8416295647621
time_elpased: 1.619
batch start
#iterations: 114
currently lose_sum: 532.3433865308762
time_elpased: 1.703
batch start
#iterations: 115
currently lose_sum: 531.5011851489544
time_elpased: 1.66
batch start
#iterations: 116
currently lose_sum: 531.6720807254314
time_elpased: 1.687
batch start
#iterations: 117
currently lose_sum: 530.9536408782005
time_elpased: 1.681
batch start
#iterations: 118
currently lose_sum: 532.678412348032
time_elpased: 1.698
batch start
#iterations: 119
currently lose_sum: 532.8968026340008
time_elpased: 1.663
start validation test
0.11824742268
1.0
0.11824742268
0.21148704711
0
validation finish
batch start
#iterations: 120
currently lose_sum: 532.7305538654327
time_elpased: 1.627
batch start
#iterations: 121
currently lose_sum: 530.9223431348801
time_elpased: 1.694
batch start
#iterations: 122
currently lose_sum: 531.3450141847134
time_elpased: 1.696
batch start
#iterations: 123
currently lose_sum: 531.4288507699966
time_elpased: 1.635
batch start
#iterations: 124
currently lose_sum: 531.4284979403019
time_elpased: 1.631
batch start
#iterations: 125
currently lose_sum: 532.8743159174919
time_elpased: 1.694
batch start
#iterations: 126
currently lose_sum: 531.2491844594479
time_elpased: 1.633
batch start
#iterations: 127
currently lose_sum: 534.0375029444695
time_elpased: 1.694
batch start
#iterations: 128
currently lose_sum: 531.8508861064911
time_elpased: 1.659
batch start
#iterations: 129
currently lose_sum: 533.0332439243793
time_elpased: 1.625
batch start
#iterations: 130
currently lose_sum: 531.4575335085392
time_elpased: 1.638
batch start
#iterations: 131
currently lose_sum: 530.6594838500023
time_elpased: 1.713
batch start
#iterations: 132
currently lose_sum: 532.05955940485
time_elpased: 1.627
batch start
#iterations: 133
currently lose_sum: 533.8868771195412
time_elpased: 1.622
batch start
#iterations: 134
currently lose_sum: 531.3844661712646
time_elpased: 1.685
batch start
#iterations: 135
currently lose_sum: 530.6242741346359
time_elpased: 1.685
batch start
#iterations: 136
currently lose_sum: 530.9743153452873
time_elpased: 1.716
batch start
#iterations: 137
currently lose_sum: 530.812537997961
time_elpased: 1.619
batch start
#iterations: 138
currently lose_sum: 532.4535675644875
time_elpased: 1.724
batch start
#iterations: 139
currently lose_sum: 532.0162988901138
time_elpased: 1.673
start validation test
0.135154639175
1.0
0.135154639175
0.238125510853
0
validation finish
batch start
#iterations: 140
currently lose_sum: 529.8681120276451
time_elpased: 1.629
batch start
#iterations: 141
currently lose_sum: 531.3529406487942
time_elpased: 1.669
batch start
#iterations: 142
currently lose_sum: 531.3303327262402
time_elpased: 1.69
batch start
#iterations: 143
currently lose_sum: 532.5904737114906
time_elpased: 1.709
batch start
#iterations: 144
currently lose_sum: 532.2773434817791
time_elpased: 1.617
batch start
#iterations: 145
currently lose_sum: 532.2443405985832
time_elpased: 1.671
batch start
#iterations: 146
currently lose_sum: 533.3193175494671
time_elpased: 1.693
batch start
#iterations: 147
currently lose_sum: 533.0175032019615
time_elpased: 1.637
batch start
#iterations: 148
currently lose_sum: 530.6285430192947
time_elpased: 1.648
batch start
#iterations: 149
currently lose_sum: 533.2406867444515
time_elpased: 1.704
batch start
#iterations: 150
currently lose_sum: 531.7646601498127
time_elpased: 1.696
batch start
#iterations: 151
currently lose_sum: 530.167167365551
time_elpased: 1.628
batch start
#iterations: 152
currently lose_sum: 531.8044803440571
time_elpased: 1.693
batch start
#iterations: 153
currently lose_sum: 529.0895425081253
time_elpased: 1.636
batch start
#iterations: 154
currently lose_sum: 535.7270365953445
time_elpased: 1.699
batch start
#iterations: 155
currently lose_sum: 529.831573843956
time_elpased: 1.719
batch start
#iterations: 156
currently lose_sum: 532.0261917114258
time_elpased: 1.692
batch start
#iterations: 157
currently lose_sum: 531.6173902750015
time_elpased: 1.639
batch start
#iterations: 158
currently lose_sum: 529.5412882566452
time_elpased: 1.674
batch start
#iterations: 159
currently lose_sum: 533.2676282525063
time_elpased: 1.643
start validation test
0.101958762887
1.0
0.101958762887
0.185050051455
0
validation finish
batch start
#iterations: 160
currently lose_sum: 531.8636774420738
time_elpased: 1.712
batch start
#iterations: 161
currently lose_sum: 532.2711970806122
time_elpased: 1.62
batch start
#iterations: 162
currently lose_sum: 530.6674775481224
time_elpased: 1.617
batch start
#iterations: 163
currently lose_sum: 529.6522970199585
time_elpased: 1.681
batch start
#iterations: 164
currently lose_sum: 532.866229891777
time_elpased: 1.637
batch start
#iterations: 165
currently lose_sum: 532.3633858263493
time_elpased: 1.675
batch start
#iterations: 166
currently lose_sum: 532.1240093708038
time_elpased: 1.702
batch start
#iterations: 167
currently lose_sum: 531.3088508546352
time_elpased: 1.635
batch start
#iterations: 168
currently lose_sum: 531.5627609491348
time_elpased: 1.665
batch start
#iterations: 169
currently lose_sum: 533.6801446676254
time_elpased: 1.691
batch start
#iterations: 170
currently lose_sum: 531.2420097887516
time_elpased: 1.667
batch start
#iterations: 171
currently lose_sum: 530.9654106199741
time_elpased: 1.631
batch start
#iterations: 172
currently lose_sum: 534.8638740777969
time_elpased: 1.658
batch start
#iterations: 173
currently lose_sum: 531.9242630600929
time_elpased: 1.636
batch start
#iterations: 174
currently lose_sum: 532.8244445621967
time_elpased: 1.713
batch start
#iterations: 175
currently lose_sum: 530.8164594769478
time_elpased: 1.64
batch start
#iterations: 176
currently lose_sum: 531.4134728610516
time_elpased: 1.705
batch start
#iterations: 177
currently lose_sum: 531.3572443127632
time_elpased: 1.708
batch start
#iterations: 178
currently lose_sum: 531.7311040461063
time_elpased: 1.631
batch start
#iterations: 179
currently lose_sum: 533.0602290332317
time_elpased: 1.645
start validation test
0.11175257732
1.0
0.11175257732
0.201038575668
0
validation finish
batch start
#iterations: 180
currently lose_sum: 531.1228559613228
time_elpased: 1.619
batch start
#iterations: 181
currently lose_sum: 532.5050803422928
time_elpased: 1.718
batch start
#iterations: 182
currently lose_sum: 531.421330332756
time_elpased: 1.678
batch start
#iterations: 183
currently lose_sum: 532.0308038592339
time_elpased: 1.641
batch start
#iterations: 184
currently lose_sum: 530.7955364584923
time_elpased: 1.624
batch start
#iterations: 185
currently lose_sum: 531.8577798604965
time_elpased: 1.672
batch start
#iterations: 186
currently lose_sum: 532.5185417830944
time_elpased: 1.699
batch start
#iterations: 187
currently lose_sum: 531.6032851040363
time_elpased: 1.695
batch start
#iterations: 188
currently lose_sum: 530.5743860304356
time_elpased: 1.717
batch start
#iterations: 189
currently lose_sum: 532.1261821389198
time_elpased: 1.622
batch start
#iterations: 190
currently lose_sum: 529.9293929040432
time_elpased: 1.716
batch start
#iterations: 191
currently lose_sum: 532.4760748445988
time_elpased: 1.708
batch start
#iterations: 192
currently lose_sum: 529.9895521402359
time_elpased: 1.627
batch start
#iterations: 193
currently lose_sum: 530.4819457530975
time_elpased: 1.717
batch start
#iterations: 194
currently lose_sum: 533.4052346944809
time_elpased: 1.627
batch start
#iterations: 195
currently lose_sum: 531.6750933229923
time_elpased: 1.627
batch start
#iterations: 196
currently lose_sum: 529.2447388470173
time_elpased: 1.623
batch start
#iterations: 197
currently lose_sum: 532.799293667078
time_elpased: 1.662
batch start
#iterations: 198
currently lose_sum: 530.9507451057434
time_elpased: 1.617
batch start
#iterations: 199
currently lose_sum: 529.4926060438156
time_elpased: 1.681
start validation test
0.140927835052
1.0
0.140927835052
0.247040751785
0
validation finish
batch start
#iterations: 200
currently lose_sum: 531.0406925976276
time_elpased: 1.657
batch start
#iterations: 201
currently lose_sum: 530.720770418644
time_elpased: 1.625
batch start
#iterations: 202
currently lose_sum: 531.5069203972816
time_elpased: 1.632
batch start
#iterations: 203
currently lose_sum: 530.007442086935
time_elpased: 1.711
batch start
#iterations: 204
currently lose_sum: 531.1797861456871
time_elpased: 1.663
batch start
#iterations: 205
currently lose_sum: 532.6190979778767
time_elpased: 1.628
batch start
#iterations: 206
currently lose_sum: 533.6999146342278
time_elpased: 1.703
batch start
#iterations: 207
currently lose_sum: 531.2519402503967
time_elpased: 1.626
batch start
#iterations: 208
currently lose_sum: 530.0481828451157
time_elpased: 1.646
batch start
#iterations: 209
currently lose_sum: 529.5533054172993
time_elpased: 1.662
batch start
#iterations: 210
currently lose_sum: 530.5548267066479
time_elpased: 1.611
batch start
#iterations: 211
currently lose_sum: 530.524210870266
time_elpased: 1.622
batch start
#iterations: 212
currently lose_sum: 531.3057335317135
time_elpased: 1.688
batch start
#iterations: 213
currently lose_sum: 530.106327354908
time_elpased: 1.634
batch start
#iterations: 214
currently lose_sum: 531.6925827264786
time_elpased: 1.626
batch start
#iterations: 215
currently lose_sum: 530.9140757918358
time_elpased: 1.648
batch start
#iterations: 216
currently lose_sum: 532.2832210659981
time_elpased: 1.607
batch start
#iterations: 217
currently lose_sum: 532.1236277520657
time_elpased: 1.628
batch start
#iterations: 218
currently lose_sum: 531.733728647232
time_elpased: 1.622
batch start
#iterations: 219
currently lose_sum: 532.9244269430637
time_elpased: 1.614
start validation test
0.114742268041
1.0
0.114742268041
0.205863312679
0
validation finish
batch start
#iterations: 220
currently lose_sum: 529.8537413477898
time_elpased: 1.623
batch start
#iterations: 221
currently lose_sum: 532.8667768537998
time_elpased: 1.623
batch start
#iterations: 222
currently lose_sum: 530.9428508877754
time_elpased: 1.63
batch start
#iterations: 223
currently lose_sum: 530.3945136070251
time_elpased: 1.615
batch start
#iterations: 224
currently lose_sum: 532.022812217474
time_elpased: 1.734
batch start
#iterations: 225
currently lose_sum: 532.7222922444344
time_elpased: 1.703
batch start
#iterations: 226
currently lose_sum: 531.7863259911537
time_elpased: 1.668
batch start
#iterations: 227
currently lose_sum: 532.7462462186813
time_elpased: 1.62
batch start
#iterations: 228
currently lose_sum: 529.5240148603916
time_elpased: 1.618
batch start
#iterations: 229
currently lose_sum: 532.0863544344902
time_elpased: 1.719
batch start
#iterations: 230
currently lose_sum: 532.4284111857414
time_elpased: 1.637
batch start
#iterations: 231
currently lose_sum: 532.0237731337547
time_elpased: 1.704
batch start
#iterations: 232
currently lose_sum: 528.292644649744
time_elpased: 1.701
batch start
#iterations: 233
currently lose_sum: 529.0881650149822
time_elpased: 1.683
batch start
#iterations: 234
currently lose_sum: 530.9632291793823
time_elpased: 1.659
batch start
#iterations: 235
currently lose_sum: 530.5821535289288
time_elpased: 1.695
batch start
#iterations: 236
currently lose_sum: 531.2671514451504
time_elpased: 1.69
batch start
#iterations: 237
currently lose_sum: 530.763479322195
time_elpased: 1.704
batch start
#iterations: 238
currently lose_sum: 530.7216616272926
time_elpased: 1.645
batch start
#iterations: 239
currently lose_sum: 528.5369431972504
time_elpased: 1.684
start validation test
0.0762886597938
1.0
0.0762886597938
0.141762452107
0
validation finish
batch start
#iterations: 240
currently lose_sum: 530.1154899597168
time_elpased: 1.61
batch start
#iterations: 241
currently lose_sum: 530.0088082849979
time_elpased: 1.622
batch start
#iterations: 242
currently lose_sum: 529.3298627734184
time_elpased: 1.69
batch start
#iterations: 243
currently lose_sum: 530.4877317845821
time_elpased: 1.624
batch start
#iterations: 244
currently lose_sum: 531.1156491339207
time_elpased: 1.712
batch start
#iterations: 245
currently lose_sum: 532.0230225026608
time_elpased: 1.632
batch start
#iterations: 246
currently lose_sum: 530.8099561929703
time_elpased: 1.624
batch start
#iterations: 247
currently lose_sum: 530.4874741733074
time_elpased: 1.711
batch start
#iterations: 248
currently lose_sum: 530.7516157031059
time_elpased: 1.68
batch start
#iterations: 249
currently lose_sum: 531.9899109005928
time_elpased: 1.684
batch start
#iterations: 250
currently lose_sum: 528.9765529930592
time_elpased: 1.676
batch start
#iterations: 251
currently lose_sum: 531.7334756851196
time_elpased: 1.635
batch start
#iterations: 252
currently lose_sum: 531.5534395873547
time_elpased: 1.629
batch start
#iterations: 253
currently lose_sum: 529.1537417769432
time_elpased: 1.62
batch start
#iterations: 254
currently lose_sum: 532.3021692037582
time_elpased: 1.685
batch start
#iterations: 255
currently lose_sum: 530.9048609733582
time_elpased: 1.694
batch start
#iterations: 256
currently lose_sum: 530.5631008446217
time_elpased: 1.694
batch start
#iterations: 257
currently lose_sum: 530.0463531911373
time_elpased: 1.623
batch start
#iterations: 258
currently lose_sum: 531.2897138595581
time_elpased: 1.632
batch start
#iterations: 259
currently lose_sum: 530.9733726382256
time_elpased: 1.641
start validation test
0.128350515464
1.0
0.128350515464
0.227501142074
0
validation finish
batch start
#iterations: 260
currently lose_sum: 532.0545220971107
time_elpased: 1.72
batch start
#iterations: 261
currently lose_sum: 532.8890700638294
time_elpased: 1.623
batch start
#iterations: 262
currently lose_sum: 531.096090555191
time_elpased: 1.673
batch start
#iterations: 263
currently lose_sum: 530.3593676686287
time_elpased: 1.66
batch start
#iterations: 264
currently lose_sum: 531.1118414700031
time_elpased: 1.662
batch start
#iterations: 265
currently lose_sum: 531.2030707597733
time_elpased: 1.629
batch start
#iterations: 266
currently lose_sum: 531.3403361439705
time_elpased: 1.638
batch start
#iterations: 267
currently lose_sum: 529.8617652952671
time_elpased: 1.633
batch start
#iterations: 268
currently lose_sum: 531.008971631527
time_elpased: 1.63
batch start
#iterations: 269
currently lose_sum: 530.8740773797035
time_elpased: 1.621
batch start
#iterations: 270
currently lose_sum: 531.2479173541069
time_elpased: 1.652
batch start
#iterations: 271
currently lose_sum: 527.8512277007103
time_elpased: 1.703
batch start
#iterations: 272
currently lose_sum: 532.1073914170265
time_elpased: 1.659
batch start
#iterations: 273
currently lose_sum: 529.5550735890865
time_elpased: 1.628
batch start
#iterations: 274
currently lose_sum: 531.3167034685612
time_elpased: 1.715
batch start
#iterations: 275
currently lose_sum: 530.3580423593521
time_elpased: 1.647
batch start
#iterations: 276
currently lose_sum: 529.4758564829826
time_elpased: 1.704
batch start
#iterations: 277
currently lose_sum: 532.9657473564148
time_elpased: 1.639
batch start
#iterations: 278
currently lose_sum: 532.082434296608
time_elpased: 1.676
batch start
#iterations: 279
currently lose_sum: 532.3235031068325
time_elpased: 1.69
start validation test
0.10206185567
1.0
0.10206185567
0.185219831618
0
validation finish
batch start
#iterations: 280
currently lose_sum: 530.2484079003334
time_elpased: 1.612
batch start
#iterations: 281
currently lose_sum: 529.5573668777943
time_elpased: 1.622
batch start
#iterations: 282
currently lose_sum: 530.7437980175018
time_elpased: 1.675
batch start
#iterations: 283
currently lose_sum: 530.7911668717861
time_elpased: 1.703
batch start
#iterations: 284
currently lose_sum: 531.7593908309937
time_elpased: 1.683
batch start
#iterations: 285
currently lose_sum: 530.786107301712
time_elpased: 1.629
batch start
#iterations: 286
currently lose_sum: 531.648068189621
time_elpased: 1.625
batch start
#iterations: 287
currently lose_sum: 530.4759923815727
time_elpased: 1.672
batch start
#iterations: 288
currently lose_sum: 530.7400122284889
time_elpased: 1.634
batch start
#iterations: 289
currently lose_sum: 530.5334882736206
time_elpased: 1.618
batch start
#iterations: 290
currently lose_sum: 530.2937997579575
time_elpased: 1.643
batch start
#iterations: 291
currently lose_sum: 531.9475319683552
time_elpased: 1.723
batch start
#iterations: 292
currently lose_sum: 531.3474968671799
time_elpased: 1.669
batch start
#iterations: 293
currently lose_sum: 530.5018611848354
time_elpased: 1.707
batch start
#iterations: 294
currently lose_sum: 529.2646064758301
time_elpased: 1.69
batch start
#iterations: 295
currently lose_sum: 530.8768028914928
time_elpased: 1.677
batch start
#iterations: 296
currently lose_sum: 528.8935763537884
time_elpased: 1.633
batch start
#iterations: 297
currently lose_sum: 531.141654253006
time_elpased: 1.692
batch start
#iterations: 298
currently lose_sum: 529.625177770853
time_elpased: 1.625
batch start
#iterations: 299
currently lose_sum: 530.2925618290901
time_elpased: 1.627
start validation test
0.15381443299
1.0
0.15381443299
0.266619013581
0
validation finish
batch start
#iterations: 300
currently lose_sum: 529.7086381018162
time_elpased: 1.651
batch start
#iterations: 301
currently lose_sum: 530.1496449112892
time_elpased: 1.651
batch start
#iterations: 302
currently lose_sum: 531.0712039768696
time_elpased: 1.639
batch start
#iterations: 303
currently lose_sum: 532.0834127664566
time_elpased: 1.694
batch start
#iterations: 304
currently lose_sum: 531.0758083164692
time_elpased: 1.635
batch start
#iterations: 305
currently lose_sum: 531.3676861524582
time_elpased: 1.656
batch start
#iterations: 306
currently lose_sum: 531.7935990095139
time_elpased: 1.674
batch start
#iterations: 307
currently lose_sum: 533.1280070841312
time_elpased: 1.685
batch start
#iterations: 308
currently lose_sum: 530.8370586633682
time_elpased: 1.621
batch start
#iterations: 309
currently lose_sum: 531.5162931680679
time_elpased: 1.668
batch start
#iterations: 310
currently lose_sum: 529.7862469255924
time_elpased: 1.634
batch start
#iterations: 311
currently lose_sum: 529.9788067042828
time_elpased: 1.641
batch start
#iterations: 312
currently lose_sum: 529.6553761959076
time_elpased: 1.641
batch start
#iterations: 313
currently lose_sum: 531.1902661919594
time_elpased: 1.626
batch start
#iterations: 314
currently lose_sum: 529.4526963829994
time_elpased: 1.635
batch start
#iterations: 315
currently lose_sum: 530.8134287893772
time_elpased: 1.678
batch start
#iterations: 316
currently lose_sum: 531.9095973670483
time_elpased: 1.721
batch start
#iterations: 317
currently lose_sum: 528.9701054096222
time_elpased: 1.654
batch start
#iterations: 318
currently lose_sum: 532.648517370224
time_elpased: 1.699
batch start
#iterations: 319
currently lose_sum: 530.9989766776562
time_elpased: 1.62
start validation test
0.105567010309
1.0
0.105567010309
0.190973517344
0
validation finish
batch start
#iterations: 320
currently lose_sum: 531.1342319846153
time_elpased: 1.7
batch start
#iterations: 321
currently lose_sum: 529.9314542412758
time_elpased: 1.629
batch start
#iterations: 322
currently lose_sum: 528.7761157155037
time_elpased: 1.704
batch start
#iterations: 323
currently lose_sum: 530.6408403515816
time_elpased: 1.72
batch start
#iterations: 324
currently lose_sum: 533.3988103866577
time_elpased: 1.692
batch start
#iterations: 325
currently lose_sum: 531.5871934890747
time_elpased: 1.622
batch start
#iterations: 326
currently lose_sum: 532.094605743885
time_elpased: 1.668
batch start
#iterations: 327
currently lose_sum: 532.3375840187073
time_elpased: 1.636
batch start
#iterations: 328
currently lose_sum: 529.7300983071327
time_elpased: 1.611
batch start
#iterations: 329
currently lose_sum: 532.0517129898071
time_elpased: 1.697
batch start
#iterations: 330
currently lose_sum: 531.9514079988003
time_elpased: 1.622
batch start
#iterations: 331
currently lose_sum: 533.0907898843288
time_elpased: 1.677
batch start
#iterations: 332
currently lose_sum: 531.6712371110916
time_elpased: 1.709
batch start
#iterations: 333
currently lose_sum: 531.1187855303288
time_elpased: 1.686
batch start
#iterations: 334
currently lose_sum: 532.0865676403046
time_elpased: 1.624
batch start
#iterations: 335
currently lose_sum: 528.9189613759518
time_elpased: 1.675
batch start
#iterations: 336
currently lose_sum: 531.6973367929459
time_elpased: 1.651
batch start
#iterations: 337
currently lose_sum: 531.2251008450985
time_elpased: 1.665
batch start
#iterations: 338
currently lose_sum: 531.5354634523392
time_elpased: 1.651
batch start
#iterations: 339
currently lose_sum: 530.9979321062565
time_elpased: 1.706
start validation test
0.120309278351
1.0
0.120309278351
0.21477868777
0
validation finish
batch start
#iterations: 340
currently lose_sum: 531.1306045055389
time_elpased: 1.695
batch start
#iterations: 341
currently lose_sum: 530.6239512860775
time_elpased: 1.632
batch start
#iterations: 342
currently lose_sum: 531.1518942713737
time_elpased: 1.68
batch start
#iterations: 343
currently lose_sum: 531.3970545530319
time_elpased: 1.702
batch start
#iterations: 344
currently lose_sum: 531.0141078531742
time_elpased: 1.636
batch start
#iterations: 345
currently lose_sum: 531.5735292434692
time_elpased: 1.628
batch start
#iterations: 346
currently lose_sum: 529.727728843689
time_elpased: 1.667
batch start
#iterations: 347
currently lose_sum: 530.1868304014206
time_elpased: 1.667
batch start
#iterations: 348
currently lose_sum: 531.3679023683071
time_elpased: 1.712
batch start
#iterations: 349
currently lose_sum: 531.0917901992798
time_elpased: 1.728
batch start
#iterations: 350
currently lose_sum: 530.9983432888985
time_elpased: 1.65
batch start
#iterations: 351
currently lose_sum: 530.4392592906952
time_elpased: 1.659
batch start
#iterations: 352
currently lose_sum: 530.548916965723
time_elpased: 1.712
batch start
#iterations: 353
currently lose_sum: 531.9543385505676
time_elpased: 1.619
batch start
#iterations: 354
currently lose_sum: 528.9866221249104
time_elpased: 1.642
batch start
#iterations: 355
currently lose_sum: 530.6890160143375
time_elpased: 1.683
batch start
#iterations: 356
currently lose_sum: 530.8889699876308
time_elpased: 1.627
batch start
#iterations: 357
currently lose_sum: 530.9100708067417
time_elpased: 1.627
batch start
#iterations: 358
currently lose_sum: 531.435975342989
time_elpased: 1.647
batch start
#iterations: 359
currently lose_sum: 530.5249710977077
time_elpased: 1.661
start validation test
0.113505154639
1.0
0.113505154639
0.203870012036
0
validation finish
batch start
#iterations: 360
currently lose_sum: 532.3674119412899
time_elpased: 1.659
batch start
#iterations: 361
currently lose_sum: 528.2936323285103
time_elpased: 1.649
batch start
#iterations: 362
currently lose_sum: 530.7755971252918
time_elpased: 1.636
batch start
#iterations: 363
currently lose_sum: 530.4278473854065
time_elpased: 1.634
batch start
#iterations: 364
currently lose_sum: 530.0746648609638
time_elpased: 1.676
batch start
#iterations: 365
currently lose_sum: 529.8513683080673
time_elpased: 1.704
batch start
#iterations: 366
currently lose_sum: 532.9733136296272
time_elpased: 1.706
batch start
#iterations: 367
currently lose_sum: 531.7468037009239
time_elpased: 1.692
batch start
#iterations: 368
currently lose_sum: 528.193039804697
time_elpased: 1.718
batch start
#iterations: 369
currently lose_sum: 531.1554231643677
time_elpased: 1.632
batch start
#iterations: 370
currently lose_sum: 529.2717856764793
time_elpased: 1.637
batch start
#iterations: 371
currently lose_sum: 531.5144792795181
time_elpased: 1.71
batch start
#iterations: 372
currently lose_sum: 530.9440295696259
time_elpased: 1.62
batch start
#iterations: 373
currently lose_sum: 530.9384754300117
time_elpased: 1.694
batch start
#iterations: 374
currently lose_sum: 532.0269570052624
time_elpased: 1.636
batch start
#iterations: 375
currently lose_sum: 532.0541533827782
time_elpased: 1.688
batch start
#iterations: 376
currently lose_sum: 530.8052155375481
time_elpased: 1.667
batch start
#iterations: 377
currently lose_sum: 530.7761825323105
time_elpased: 1.627
batch start
#iterations: 378
currently lose_sum: 531.4928598105907
time_elpased: 1.666
batch start
#iterations: 379
currently lose_sum: 529.5126594007015
time_elpased: 1.698
start validation test
0.120927835052
1.0
0.120927835052
0.215763818633
0
validation finish
batch start
#iterations: 380
currently lose_sum: 531.5434314608574
time_elpased: 1.689
batch start
#iterations: 381
currently lose_sum: 529.2866415679455
time_elpased: 1.646
batch start
#iterations: 382
currently lose_sum: 530.1673727333546
time_elpased: 1.701
batch start
#iterations: 383
currently lose_sum: 528.4246753156185
time_elpased: 1.618
batch start
#iterations: 384
currently lose_sum: 529.2290121614933
time_elpased: 1.625
batch start
#iterations: 385
currently lose_sum: 529.8933329582214
time_elpased: 1.637
batch start
#iterations: 386
currently lose_sum: 531.0013411641121
time_elpased: 1.702
batch start
#iterations: 387
currently lose_sum: 531.7960733473301
time_elpased: 1.693
batch start
#iterations: 388
currently lose_sum: 531.5176207721233
time_elpased: 1.68
batch start
#iterations: 389
currently lose_sum: 529.4117601215839
time_elpased: 1.666
batch start
#iterations: 390
currently lose_sum: 529.4379931986332
time_elpased: 1.628
batch start
#iterations: 391
currently lose_sum: 530.1953691840172
time_elpased: 1.686
batch start
#iterations: 392
currently lose_sum: 530.2553761899471
time_elpased: 1.704
batch start
#iterations: 393
currently lose_sum: 530.4051713347435
time_elpased: 1.637
batch start
#iterations: 394
currently lose_sum: 531.2147569656372
time_elpased: 1.683
batch start
#iterations: 395
currently lose_sum: 530.2119534611702
time_elpased: 1.707
batch start
#iterations: 396
currently lose_sum: 529.8244224190712
time_elpased: 1.621
batch start
#iterations: 397
currently lose_sum: 531.3280180990696
time_elpased: 1.643
batch start
#iterations: 398
currently lose_sum: 531.3902097046375
time_elpased: 1.683
batch start
#iterations: 399
currently lose_sum: 530.8858683109283
time_elpased: 1.634
start validation test
0.115051546392
1.0
0.115051546392
0.206360946746
0
validation finish
acc: 0.154
pre: 1.000
rec: 0.154
F1: 0.267
auc: 0.000
