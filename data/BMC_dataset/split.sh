#!/bin/tcsh
foreach a (0 1 2 3 4)
	if ($a == 4) then
		set b=0
	else 
		set b=`expr $a + 1`
	endif
	echo $b 
	paste y P_set.txt | awk -v a="$a" '{if($1==a) {print $2"\t"$3"\t"$4} }' > PN_test_$a.txt
	paste y P_set.txt | awk -v b="$b" '{if($1==b) {print $2"\t"$3"\t"$4} }' > PN_validation_$a.txt
	paste y P_set.txt | awk -v a="$a" -v b="$b" '{if($1!=a && $1!=b) {print $2"\t"$3"\t"$4} }' > PN_train_$a.txt
end
