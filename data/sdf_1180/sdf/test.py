import os
import numpy as np
f = open('CID_57166.sdf', 'r')
path_save = '../adj_matrix/'
row_num = 0
marker = 0
for line in f:
	if(row_num == 3):
		num_vertex = int(line[0:3])
                num_edge = int(line[3:6])
		print num_vertex, num_edge
                adj_matrix = np.zeros([num_vertex,num_vertex])
		print 'np'
                marker = 1
        if( marker == 1 and row_num >= (3 + num_vertex + 1) and row_num <= (3 + num_vertex + num_edge) ):
                row_index = int(line[0:3]) - 1
                column_index = int(line[3:6]) - 1
                adj_matrix[row_index][column_index] = 1
        if( marker == 1 and row_num > (3 + num_vertex + num_edge) ):
                break

        row_num += 1

np.savetxt(path_save + f.name.split('.')[0] + '_matrix', adj_matrix, fmt = '%d', delimiter=',')

