#!/bin/tcsh
foreach object (target enzyme transporter)
	foreach type (gene protein)
		less gene_$object.fasta | awk -v object="drugbank_$object" '{if($1~object) {print $0}}' | perl -pe 's/; /,/g' | awk -F'\|| ' '{print $2"\t"$NF}' | perl -pe 's/\(|\)//g' | perl -pe 's/,/ /g' | less | perl -pe 's/\t/;/g' > $object\_dbid_$type.txt
		less $object\_dbid_$type.txt | awk -F';' '{print $1}' | sort | uniq -c | awk '{print (NR-1)" "$2}' > id_$object\_$type.txt
		less $object\_dbid_$type.txt | awk -F';| ' '{for(i=2;i<=NF;i++) {print $i}}' | sort | uniq -c | awk '{print (NR-1)" "$2}' > id_$object\_$type\_dbid.txt
		set num_object = `less id_$object\_$type.txt | wc | awk '{print $1}'`
		set num_drugs = `less id_$object\_$type\_dbid.txt | wc | awk '{print $1}'`
		cat id_$object\_$type.txt id_$object\_$type\_dbid.txt $object\_dbid_gene.txt | awk -F';| ' -v num_object="$num_object" -v num_drugs="$num_drugs" '{if(NR<=num_object) {a[$2] = $1} else if(NR<=num_object+num_drugs) {b[$2]=$1} else{for(i=2;i<=NF;i++) {print a[$1]" "b[$i]" "1} }}' > $object\_$type\_CSR
		python make_matrix.py $object\_$type\_CSR idMatrix
		less idMatrix | perl -pe 's/\[|\]//g' | perl -pe 's/, / /g' > test
		cat id_$object\_$type.txt id_$object\_$type\_dbid.txt test | awk -F' ' -v num_object="$num_object" -v num_drugs="$num_drugs" 'BEGIN{ORS=""}{if(NR<=num_object) {a[$1] = $2} else if(NR<=num_object+num_drugs) {b[$1]=$2} else{print b[$1]";";for(i=2;i<=NF;i++) {if(i!=NF) {print a[$i]","} else {print a[$i]"\n"}} }}' > dbid_$object\_$type.txt
	end
end
rm idMatrix test
