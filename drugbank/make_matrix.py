import numpy as np
import pandas as pd
import sys
from scipy.sparse import csr_matrix

CSR_file = sys.argv[1]
df = pd.read_csv(CSR_file, delimiter = " ", names = ["row", "col", "value"])

row = df["row"]
col = df["col"]
value = df["value"]

sparse_matrix = csr_matrix((value, (row, col)), [max(row)+1, max(col)+1])
matrix = sparse_matrix.T.todense()

f = open(sys.argv[2], 'w')

for i in xrange(matrix.shape[0]):
	index = [i] + list(np.where(matrix[i]==1)[1])
	f.writelines("%s\n" % index)

f.close()
