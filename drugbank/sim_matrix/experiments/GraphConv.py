import tensorflow as tf
import numpy as np
import pdb
import sklearn.metrics

#operate Conv for each node
def node_level_Conv(node_adj_matrix, node_features, weight_self_list, weight_neig_list, b_matrix, degree_matrix, degree_max, degree_min, act):

	temp = tf.expand_dims(node_adj_matrix, 2)
	temp2 = tf.expand_dims(node_features, 0)
	neig = tf.multiply(temp2, temp)
	dm = tf.expand_dims(degree_matrix, 2)
	neig_sum = tf.reduce_sum(neig, 1)
	deg_list = tf.cast(tf.reduce_sum(node_adj_matrix, 1), tf.int32)
	#embedding indx start from zero
	wei_self = tf.nn.embedding_lookup(weight_self_list, deg_list)
	wei_neig = tf.nn.embedding_lookup(weight_neig_list, deg_list)
        b = tf.nn.embedding_lookup(b_matrix, deg_list)
        self_embedding = tf.matmul(tf.expand_dims(node_features, 1), wei_self)
	neig_embedding = tf.matmul(tf.expand_dims(neig_sum, 1), wei_neig)
        atoms_features = tf.squeeze(self_embedding + neig_embedding)
	atoms_features = tf.add(atoms_features, b)
	#pdb.set_trace()
	'''
	sum_list_n = []

	for i in xrange(degree_min, degree_max + 1):
		x = tf.multiply( neig, tf.expand_dims( dm[i], 0 ) )
		x = tf.reduce_sum(x, 1)
		temp3 = tf.reduce_sum(x, 1)
		x_w = tf.matmul(x, weight_neig_list[i])
		temp4 = tf.cast(tf.not_equal(temp3, 0), tf.float32)
		b_temp = tf.matmul( tf.expand_dims(temp4,1), tf.expand_dims(b_matrix[i],0) )
		x_total = tf.add(x_w, b_temp)
		sum_list_n.append(x_total)

	sum_list_s = []

	for i in xrange(degree_min, degree_max + 1):
		y = tf.multiply( node_features, tf.expand_dims( degree_matrix[i], 1 ) )
		y = tf.matmul(y, weight_self_list[i])
		sum_list_s.append(y)
        
	atoms_features = tf.add_n(sum_list_s) + tf.add_n(sum_list_n)
	'''
	return act(atoms_features)



#make gamma,beta to tf.variable to updata
def node_level_normalization(atoms_features_matrix, gamma, beta, eps):
	
	r_dim = tf.shape(atoms_features_matrix)[0]
	#pdb.set_trace()
	mu, var = tf.nn.moments(atoms_features_matrix, axes = 1)
	mu = tf.reshape(mu, [r_dim, 1])
	var = tf.reshape(var, [r_dim, 1])
	return tf.nn.batch_normalization(atoms_features_matrix, mu, var, beta, gamma, eps)

	
def Graph_Pool(node_adj_matrix, normalized_node_feature_matrix, feature_size):

	r_dim = tf.shape(node_adj_matrix)[0]
	fill_one_node_adj_matrix = node_adj_matrix + tf.diag(tf.ones([r_dim]))

	node_and_neig = tf.multiply( tf.expand_dims(normalized_node_feature_matrix, 0), tf.expand_dims(fill_one_node_adj_matrix, 2) )
	
	poolized_matrix = tf.reduce_max(node_and_neig, 1)

	return  poolized_matrix


def Graph_Gathering(poolized_matrix, supernode_feature, weight_self_Gathering, weight_neig_Gathering, b_Gathering):

	sum_matrix = tf.reduce_sum(poolized_matrix, 0)
	sum_matrix = tf.expand_dims(sum_matrix, 0)
	Graph_feature = tf.matmul(supernode_feature, weight_self_Gathering) + tf.matmul(sum_matrix, weight_neig_Gathering) + b_Gathering

	return Graph_feature


def Combine_Multi_Graph(Graph_feature_1, Graph_feature_2, activity_1, activity_2, activity):
	if(activity == 1):
		#activity_1 = node_level_normalization(activity_1, 0.99, 0.01, 0.001)
		#activity_2 = node_level_normalization(activity_2, 0.99, 0.01, 0.001)
		#Graph_feature_1 = node_level_normalization(Graph_feature_1, 0.99, 0.01, 0.001)
		#Graph_feature_2 = node_level_normalization(Graph_feature_2, 0.99, 0.01, 0.001)
		Graph_activity_1 = tf.add(Graph_feature_1, activity_1)
		Graph_activity_2 = tf.add(Graph_feature_2, activity_2)
		combined_feature_1 = tf.concat([Graph_activity_1, Graph_activity_2], 1)
		combined_feature_2 = tf.concat([Graph_activity_2, Graph_activity_1], 1)
		return tf.nn.tanh(combined_feature_1), tf.nn.tanh(combined_feature_2), tf.nn.tanh(Graph_activity_1), tf.nn.tanh(Graph_activity_2)
	else:
		combined_feature_1 = tf.concat([Graph_feature_1, Graph_feature_2], 1)
		combined_feature_2 = tf.concat([Graph_feature_2, Graph_feature_1], 1)
		return tf.nn.tanh(combined_feature_1), tf.nn.tanh(combined_feature_2), tf.nn.tanh(Graph_feature_1), tf.nn.tanh(Graph_feature_2)

def NN_relationship_encoder(combined_graph_feature_1, combined_graph_feature_2, NN, W_layers, b_layers, W_layers_hidden, b_layers_hidden, keep_prob, n_layers, Graph_feature_1, Graph_feature_2, attention, att_V, att_W, batch_size, graph_feature_size, drug_order = 2):
	if(NN == 1):
		layer_1 = combined_graph_feature_1
		layer_2 = combined_graph_feature_2

                layer_1 = tf.nn.tanh( tf.matmul(layer_1, W_layers) + b_layers )
                layer_1 = tf.nn.dropout(layer_1, keep_prob = keep_prob)
                layer_2 = tf.nn.tanh( tf.matmul(layer_2, W_layers) + b_layers )
                layer_2 = tf.nn.dropout(layer_2, keep_prob = keep_prob)

		for i in range(n_layers - 1):
			layer_1 = tf.nn.tanh( tf.matmul(layer_1, W_layers_hidden[i]) + b_layers_hidden[i] )
        		layer_1 = tf.nn.dropout(layer_1, keep_prob = keep_prob)

        		layer_2 = tf.nn.tanh( tf.matmul(layer_2, W_layers_hidden[i]) + b_layers_hidden[i] )
        		layer_2 = tf.nn.dropout(layer_2, keep_prob = keep_prob)

		layer = tf.add(layer_1, layer_2)/2

		return layer, 0
	elif(attention == 1):
		mat_Graph = tf.reshape(combined_graph_feature_1, [drug_order * batch_size, graph_feature_size])
		Att_trans = tf.nn.tanh(tf.matmul(att_V, tf.transpose(mat_Graph)))
		Att_temp = tf.matmul(att_W, Att_trans)
		Att = tf.nn.softmax(tf.reshape(Att_temp, [batch_size, drug_order]))
		mat_Graph3D = tf.reshape(combined_graph_feature_1, [batch_size, drug_order, graph_feature_size])
		layer = tf.matmul(tf.expand_dims(Att, 1), mat_Graph3D)
		layer = tf.squeeze(layer)
                for i in range(n_layers - 1):
                        layer = tf.nn.tanh( tf.matmul(layer, W_layers_hidden[i])+b_layers_hidden[i] )
                        layer = tf.nn.dropout(layer, keep_prob = keep_prob)
		
		return layer, Att
	else:
		if(drug_order == 3):
			layer = tf.reduce_mean(tf.reshape(combined_graph_feature_1, [batch_size, drug_order, -1]), 1)
			for i in range(n_layers - 1):
				layer = tf.nn.tanh( tf.matmul(layer, W_layers_hidden[i])+b_layers_hidden[i] )
				layer = tf.nn.dropout(layer, keep_prob = keep_prob)
		else:
			layer = tf.add(Graph_feature_1, Graph_feature_2)/2
			for i in range(n_layers - 1):
				layer = tf.nn.tanh( tf.matmul(layer, W_layers_hidden[i])+b_layers_hidden[i] )
				layer = tf.nn.dropout(layer, keep_prob = keep_prob)

		return layer, 0

def Classifier(layer, feature1, feature2, class_weight, class_b, option, R_matrix, order2):
	if(option == 1):
		combination = layer + graph_feature_matrix_1 + graph_feature_matrix_2
	else:
		combination = layer
	if(order2 == 1):
		temp = tf.matmul(feature1, R_matrix)
		temp_2 = tf.matmul(tf.expand_dims(temp, 1), tf.expand_dims(feature2, 2))
		hypothesis_order2 = tf.reshape(temp_2, [100, 1])
		hypothesis = tf.sigmoid( tf.matmul(combination, class_weight) + class_b + hypothesis_order2)
	else:
		hypothesis = tf.sigmoid( tf.matmul(combination, class_weight) + class_b )
	return hypothesis

def mixfeature(chem_sim1, chem_sim2, W_transsim):
	chem_sim1_transed = tf.matmul(chem_sim1, W_transsim)
	chem_sim2_transed = tf.matmul(chem_sim2, W_transsim)
	#chem_sim1_transed = chem_sim1
	#chem_sim2_transed = chem_sim2
	return chem_sim1_transed, chem_sim2_transed

def mixfeature_order3(chem_sim1, chem_sim2, chem_sim3, W_transsim):
	return tf.matmul(chem_sim1, W_transsim), tf.matmul(chem_sim2, W_transsim), tf.matmul(chem_sim3, W_transsim)

def mixfeature_all(sim_list1, sim_list2, W_transsim, sim_attention, simatt_V,simatt_W, batch_size):
	sim_list1_transed = tf.map_fn(lambda x: tf.matmul(x, W_transsim), sim_list1)
	sim_list2_transed = tf.map_fn(lambda x: tf.matmul(x, W_transsim), sim_list2)
	#pdb.set_trace()
	if(sim_attention == 0):
		return tf.reduce_mean(sim_list1_transed, 0), tf.reduce_mean(sim_list2_transed, 0)
	else:
		# 8 is the number of types of similarities
		width = tf.shape(W_transsim)[1]
		#sim_list1 mix
		temp = tf.concat(tf.split(sim_list1_transed, 8), 2)
		sim_list1_matrix = tf.reshape(temp, [8 * batch_size, tf.shape(W_transsim)[1]])
		Att_trans = tf.nn.tanh(tf.matmul(simatt_V, sim_list1_matrix, transpose_b = True))
		Att_temp = tf.matmul(simatt_W, Att_trans)
                Att = tf.nn.softmax(tf.reshape(Att_temp, [batch_size, 8]))
                mat_Graph3D = tf.reshape(temp, [batch_size, 8, width])
                mixedsim1 = tf.matmul(tf.expand_dims(Att, 1), mat_Graph3D)
		#sim_list2 mix
		temp2 = tf.concat(tf.split(sim_list2_transed, 8), 2)
		sim_list2_matrix = tf.reshape(temp2, [8 * batch_size, tf.shape(W_transsim)[1]])
                Att_trans2 = tf.nn.tanh(tf.matmul(simatt_V, sim_list2_matrix, transpose_b = True))
                Att_temp2 = tf.matmul(simatt_W, Att_trans2)
                Att2 = tf.nn.softmax(tf.reshape(Att_temp2, [batch_size, 8]))
                mat_Graph3D2 = tf.reshape(temp2, [batch_size, 8, width])
                mixedsim2 = tf.matmul(tf.expand_dims(Att2, 1), mat_Graph3D2)
		return tf.squeeze(mixedsim1), tf.squeeze(mixedsim2)

def evaluation(label_list, predicted_list):
	accuracy = sklearn.metrics.accuracy_score(label_list, predicted_list)
	precision = sklearn.metrics.precision_score(label_list, predicted_list)
	recall = sklearn.metrics.recall_score(label_list, predicted_list)
	F1 = 2 * recall * precision / (recall + precision)
	AUC =  sklearn.metrics.roc_auc_score(label_list, predicted_list)
	#AUC = 0
	return accuracy, precision, recall, F1, AUC

def negative_sample(num_sample, preb, ID_drugId, result):
	candidate_list = []
	for i in xrange(num_sample):
		while True:
			te1, te2 = np.random.choice(len(preb), 2, p = preb)
			drug_1, drug_2 = ID_drugId[te1], ID_drugId[te2]
			if(result[drug_1, drug_2] == 0 and result[drug_2, drug_1] == 0):
				candidate_list.append(str(drug_1)+"\t"+str(drug_2)+"\t0")
				break
	return candidate_list

def negative_sample_order3(num_sample, preb, ID_drugId, result):
	candidate_list = []
	for i in xrange(num_sample):
                while True:
                        te1, te2, te3 = np.random.choice(len(preb), 3, p = preb)
                        drug_1, drug_2, drug_3 = ID_drugId[te1], ID_drugId[te2], ID_drugId[te3]
                        if(result[drug_1, drug_2, drug_3] == 0):
                                candidate_list.append(str(drug_1)+"\t"+str(drug_2)+"\t"+str(drug_3)+"\t0")
                                break
        return candidate_list

		

def update_feed_dict(graph_adj, graph_feature, graph_degree, activity_matrix, id_matrixID_dict, ind_list_1, ind_list_2, X_1_adj, X_1_feature, X_1_degree_matrix, X_2_adj, X_2_feature, X_2_degree_matrix, label, activity_1, activity_2, label_list, feed_dict, keep_probe_tf, keep_probe, Graph, chem_sim, activity, chem_sim1, chem_sim2, chemsim_matrix, enzymesim_matrix, indicationsim_matrix, offsideeffectsim_matrix, pathwaysim_matrix, sideeffectsim_matrix, targetsim_matrix, transportersim_matrix, sim_list1, sim_list2, all_sim, drug_order, ind_list_3, chem_sim3):
	if(all_sim == 1):
		matrix_indice1 = [int(e) for e in ind_list_1]
		matrix_indice2 = [int(e) for e in ind_list_2]
		sim_list_value1 = [chemsim_matrix[matrix_indice1], enzymesim_matrix[matrix_indice1], indicationsim_matrix[matrix_indice1], offsideeffectsim_matrix[matrix_indice1], pathwaysim_matrix[matrix_indice1], sideeffectsim_matrix[matrix_indice1], targetsim_matrix[matrix_indice1], transportersim_matrix[matrix_indice1]]
		sim_list_value2 = [chemsim_matrix[matrix_indice2], enzymesim_matrix[matrix_indice2], indicationsim_matrix[matrix_indice2], offsideeffectsim_matrix[matrix_indice2], pathwaysim_matrix[matrix_indice2], sideeffectsim_matrix[matrix_indice2], targetsim_matrix[matrix_indice2], transportersim_matrix[matrix_indice2]]

		feed_dict.update({sim_list1: sim_list_value1})
		feed_dict.update({sim_list2: sim_list_value2})
		
	elif(Graph == 1):
		j = 0
		for ind_1 in ind_list_1:
			feed_dict.update({X_1_adj[j]:graph_adj[ind_1]})
			feed_dict.update({X_1_feature[j]:graph_feature[ind_1]})
			feed_dict.update({X_1_degree_matrix[j]:graph_degree[ind_1]})
			j+=1
		j = 0
		for ind_2 in ind_list_2:
			feed_dict.update({X_2_adj[j]:graph_adj[ind_2]})
			feed_dict.update({X_2_feature[j]:graph_feature[ind_2]})
			feed_dict.update({X_2_degree_matrix[j]:graph_degree[ind_2]})
			j+=1
	elif(chem_sim == 1 and drug_order == 2):
		matrix_indice1 = [int(e) for e in ind_list_1]
		matrix_indice2 = [int(e) for e in ind_list_2]
		feed_dict.update({chem_sim1: chemsim_matrix[matrix_indice1]})
		feed_dict.update({chem_sim2: chemsim_matrix[matrix_indice2]})
	elif(chem_sim == 1 and drug_order == 3):
		#pdb.set_trace()
		matrix_indice1 = [int(e) for e in ind_list_1]
		matrix_indice2 = [int(e) for e in ind_list_2]
		matrix_indice3 = [int(e) for e in ind_list_3]
		feed_dict.update({chem_sim1: chemsim_matrix[matrix_indice1]})
		feed_dict.update({chem_sim2: chemsim_matrix[matrix_indice2]})
		feed_dict.update({chem_sim3: chemsim_matrix[matrix_indice3]})
	elif(activity == 1):
        	matrix_indice1 = [id_matrixID_dict[e]-1 for e in ind_list_1]
		matrix_indice2 = [id_matrixID_dict[e]-1 for e in ind_list_2]
		feed_dict.update({activity_1: activity_matrix[matrix_indice1]})
		feed_dict.update({activity_2: activity_matrix[matrix_indice2]})

        feed_dict.update({label:label_list})
        feed_dict.update({keep_probe_tf:keep_probe})
	return feed_dict	
