#!/bin/tcsh

set sum_dim = $4
set num_drug = $4

foreach folds ($2)
	set num_test = `less PNN_entire_test$folds\_$3.txt | wc | awk '{print $1}'`
	set num_val = `less PNN_entire_validation$folds\_$3.txt | wc | awk '{print $1}'`
	set num_train = `less PN_train_$folds\_$3.txt | wc | awk '{print $1}'`

	foreach sim_file ($3_sim_mat.txt)
		foreach size (32 64 128 256 512)
			if($1 == 1) then
				foreach L (16 32 64 128)
					python frame_work_arbitraryOrder.py --attention $1 --train PN_train_$folds\_$3.txt --test PNN_entire_test$folds\_$3.txt --validation PNN_entire_validation$folds\_$3.txt --num_train_sample $num_train --num_validation_sample $num_val --num_test_sample $num_test --sim_dim $sum_dim --num_drug $num_drug --sim_file $sim_file --L_dim $L --freq_file freq_PNtrain$folds\_$3.txt --input_size $size --out_path ./batch_sample_results/$3_prediction_value/  > ./batch_sample_results/$3_log/log_$sim_file\_1N_attention_$1_L_$L\_feature_$size\_0_$folds.txt
					#end
				end
			else
				foreach L (32)
					#foreach layer (0 1 3 5)
					foreach layer (0)
						python frame_work_arbitraryOrder.py --attention $1 --train PN_train_$folds\_$3.txt --test PNN_entire_test$folds\_$3.txt --validation PNN_entire_validation$folds\_$3.txt --num_train_sample $num_train --num_validation_sample $num_val --num_test_sample $num_test --sim_dim $sum_dim --num_drug $num_drug --sim_file $sim_file --L_dim $L --freq_file freq_PNtrain$folds\_$3.txt --input_size $size --num_DAN $layer --max_pool 0 --out_path ./batch_sample_results/$3_mean_prediction_value/  > ./batch_sample_results/$3_mean/log_$sim_file\_1N_attention_$1_L_$L\_feature_$size\_$layer\_$folds.txt
					end
				end
			endif
		end
	end
end
