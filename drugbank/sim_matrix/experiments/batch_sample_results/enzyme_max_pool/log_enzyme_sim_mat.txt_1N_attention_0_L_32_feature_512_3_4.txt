start to construct computing graph
graph construct over
2360
epochs start
#iterations: 0
currently lose_sum: 0.7192428708076477
time_elpased: 5.226
#iterations: 1
currently lose_sum: 0.6765919923782349
time_elpased: 5.146
#iterations: 2
currently lose_sum: 0.7093559503555298
time_elpased: 5.185
#iterations: 3
currently lose_sum: 0.698616623878479
time_elpased: 5.209
#iterations: 4
currently lose_sum: 0.7037347555160522
time_elpased: 5.175
#iterations: 5
currently lose_sum: 0.7189618349075317
time_elpased: 5.206
#iterations: 6
currently lose_sum: 0.6867579221725464
time_elpased: 5.228
#iterations: 7
currently lose_sum: 0.7330031394958496
time_elpased: 5.19
#iterations: 8
currently lose_sum: 0.6962268948554993
time_elpased: 5.225
#iterations: 9
currently lose_sum: 0.7279065847396851
time_elpased: 5.207
#iterations: 10
currently lose_sum: 0.6644402742385864
time_elpased: 5.159
#iterations: 11
currently lose_sum: 0.7412815093994141
time_elpased: 5.188
#iterations: 12
currently lose_sum: 0.645394504070282
time_elpased: 5.24
#iterations: 13
currently lose_sum: 0.6998600363731384
time_elpased: 5.181
#iterations: 14
currently lose_sum: 0.650408148765564
time_elpased: 5.223
#iterations: 15
currently lose_sum: 0.7056752443313599
time_elpased: 5.256
#iterations: 16
currently lose_sum: 0.6604875326156616
time_elpased: 5.17
#iterations: 17
currently lose_sum: 0.6565227508544922
time_elpased: 5.191
#iterations: 18
currently lose_sum: 0.7537506222724915
time_elpased: 5.21
#iterations: 19
currently lose_sum: 0.6684309840202332
time_elpased: 5.268
start validation test
0.55641025641
0.579831932773
0.438373570521
0.499276410999
0.55747915266
26.653
#iterations: 20
currently lose_sum: 0.7704302072525024
time_elpased: 4.916
#iterations: 21
currently lose_sum: 0.6570907831192017
time_elpased: 4.893
#iterations: 22
currently lose_sum: 0.6595635414123535
time_elpased: 4.873
#iterations: 23
currently lose_sum: 0.7048498392105103
time_elpased: 4.938
#iterations: 24
currently lose_sum: 0.6872954964637756
time_elpased: 4.932
#iterations: 25
currently lose_sum: 0.6206041574478149
time_elpased: 4.965
#iterations: 26
currently lose_sum: 0.5964001417160034
time_elpased: 4.891
#iterations: 27
currently lose_sum: 0.660892128944397
time_elpased: 4.96
#iterations: 28
currently lose_sum: 0.7120753526687622
time_elpased: 4.946
#iterations: 29
currently lose_sum: 0.6436936855316162
time_elpased: 4.928
#iterations: 30
currently lose_sum: 0.5946342945098877
time_elpased: 4.957
#iterations: 31
currently lose_sum: 0.7019568681716919
time_elpased: 4.962
#iterations: 32
currently lose_sum: 0.5670918226242065
time_elpased: 4.983
#iterations: 33
currently lose_sum: 0.6035798788070679
time_elpased: 4.99
#iterations: 34
currently lose_sum: 0.7980843782424927
time_elpased: 5.012
#iterations: 35
currently lose_sum: 0.6126087307929993
time_elpased: 5.025
#iterations: 36
currently lose_sum: 0.6631231307983398
time_elpased: 4.96
#iterations: 37
currently lose_sum: 0.6208178400993347
time_elpased: 4.968
#iterations: 38
currently lose_sum: 0.6742989420890808
time_elpased: 4.981
#iterations: 39
currently lose_sum: 0.5944259762763977
time_elpased: 5.119
start validation test
0.612820512821
0.595213319459
0.726810673443
0.654462242563
0.611788260396
25.752
#iterations: 40
currently lose_sum: 0.6574175953865051
time_elpased: 4.957
#iterations: 41
currently lose_sum: 0.6482359766960144
time_elpased: 4.886
#iterations: 42
currently lose_sum: 0.586818516254425
time_elpased: 4.951
#iterations: 43
currently lose_sum: 0.5568097829818726
time_elpased: 4.913
#iterations: 44
currently lose_sum: 0.5555413961410522
time_elpased: 4.905
#iterations: 45
currently lose_sum: 0.5976214408874512
time_elpased: 4.966
#iterations: 46
currently lose_sum: 0.5602368116378784
time_elpased: 4.913
#iterations: 47
currently lose_sum: 0.6839210391044617
time_elpased: 4.947
#iterations: 48
currently lose_sum: 0.6150036454200745
time_elpased: 4.915
#iterations: 49
currently lose_sum: 0.6796902418136597
time_elpased: 4.932
#iterations: 50
currently lose_sum: 0.5652154684066772
time_elpased: 4.942
#iterations: 51
currently lose_sum: 0.5688709020614624
time_elpased: 4.898
#iterations: 52
currently lose_sum: 0.6071752905845642
time_elpased: 4.991
#iterations: 53
currently lose_sum: 0.6803160905838013
time_elpased: 4.927
#iterations: 54
currently lose_sum: 0.5432230830192566
time_elpased: 4.958
#iterations: 55
currently lose_sum: 0.5539622902870178
time_elpased: 4.906
#iterations: 56
currently lose_sum: 0.578274130821228
time_elpased: 4.924
#iterations: 57
currently lose_sum: 0.5483736991882324
time_elpased: 5.056
#iterations: 58
currently lose_sum: 0.3885388970375061
time_elpased: 4.952
#iterations: 59
currently lose_sum: 0.633240818977356
time_elpased: 4.918
start validation test
0.632692307692
0.615053763441
0.726810673443
0.666278392545
0.631840006838
26.013
#iterations: 60
currently lose_sum: 0.6035181283950806
time_elpased: 4.914
#iterations: 61
currently lose_sum: 0.6185779571533203
time_elpased: 4.954
#iterations: 62
currently lose_sum: 0.5086789131164551
time_elpased: 4.872
#iterations: 63
currently lose_sum: 0.544451892375946
time_elpased: 4.918
#iterations: 64
currently lose_sum: 0.647077202796936
time_elpased: 4.928
#iterations: 65
currently lose_sum: 0.5014686584472656
time_elpased: 4.908
#iterations: 66
currently lose_sum: 0.5626550912857056
time_elpased: 4.927
#iterations: 67
currently lose_sum: 0.6977568864822388
time_elpased: 5.005
#iterations: 68
currently lose_sum: 0.5614873766899109
time_elpased: 4.954
#iterations: 69
currently lose_sum: 0.5884264707565308
time_elpased: 4.953
#iterations: 70
currently lose_sum: 0.5870124697685242
time_elpased: 4.916
#iterations: 71
currently lose_sum: 0.6156269311904907
time_elpased: 4.932
#iterations: 72
currently lose_sum: 0.5939701795578003
time_elpased: 4.947
#iterations: 73
currently lose_sum: 0.47536277770996094
time_elpased: 4.965
#iterations: 74
currently lose_sum: 0.5800897479057312
time_elpased: 5.001
#iterations: 75
currently lose_sum: 0.6245355010032654
time_elpased: 4.898
#iterations: 76
currently lose_sum: 0.6000486612319946
time_elpased: 4.961
#iterations: 77
currently lose_sum: 0.5225226283073425
time_elpased: 4.962
#iterations: 78
currently lose_sum: 0.547300398349762
time_elpased: 4.936
#iterations: 79
currently lose_sum: 0.617496132850647
time_elpased: 4.932
start validation test
0.641666666667
0.771428571429
0.411689961881
0.536868268434
0.643749250022
25.761
#iterations: 80
currently lose_sum: 0.5315219163894653
time_elpased: 4.895
#iterations: 81
currently lose_sum: 0.4613151550292969
time_elpased: 4.929
#iterations: 82
currently lose_sum: 0.6891751289367676
time_elpased: 4.921
#iterations: 83
currently lose_sum: 0.6395096778869629
time_elpased: 4.959
#iterations: 84
currently lose_sum: 0.5345957279205322
time_elpased: 4.904
#iterations: 85
currently lose_sum: 0.48678112030029297
time_elpased: 5.027
#iterations: 86
currently lose_sum: 0.6337164044380188
time_elpased: 4.975
#iterations: 87
currently lose_sum: 0.43701615929603577
time_elpased: 4.939
#iterations: 88
currently lose_sum: 0.5872920155525208
time_elpased: 4.973
#iterations: 89
currently lose_sum: 0.7319492101669312
time_elpased: 4.892
#iterations: 90
currently lose_sum: 0.5950486063957214
time_elpased: 4.892
#iterations: 91
currently lose_sum: 0.5882018208503723
time_elpased: 4.924
#iterations: 92
currently lose_sum: 0.46056032180786133
time_elpased: 4.907
#iterations: 93
currently lose_sum: 0.453207403421402
time_elpased: 4.984
#iterations: 94
currently lose_sum: 0.571804404258728
time_elpased: 4.988
#iterations: 95
currently lose_sum: 0.43681231141090393
time_elpased: 4.939
#iterations: 96
currently lose_sum: 0.5290620923042297
time_elpased: 4.938
#iterations: 97
currently lose_sum: 0.564261794090271
time_elpased: 4.889
#iterations: 98
currently lose_sum: 0.47799748182296753
time_elpased: 4.9
#iterations: 99
currently lose_sum: 0.47948065400123596
time_elpased: 4.927
start validation test
0.630769230769
0.595475113122
0.836086404066
0.6955602537
0.628909954944
29.421
#iterations: 100
currently lose_sum: 0.46936148405075073
time_elpased: 4.924
#iterations: 101
currently lose_sum: 0.47494879364967346
time_elpased: 4.989
#iterations: 102
currently lose_sum: 0.5723435878753662
time_elpased: 4.933
#iterations: 103
currently lose_sum: 0.515802800655365
time_elpased: 4.943
#iterations: 104
currently lose_sum: 0.5599802732467651
time_elpased: 4.96
#iterations: 105
currently lose_sum: 0.7091894745826721
time_elpased: 4.95
#iterations: 106
currently lose_sum: 0.4661674499511719
time_elpased: 4.92
#iterations: 107
currently lose_sum: 0.4298895001411438
time_elpased: 4.906
#iterations: 108
currently lose_sum: 0.3776285648345947
time_elpased: 4.97
#iterations: 109
currently lose_sum: 0.7612382769584656
time_elpased: 4.904
#iterations: 110
currently lose_sum: 0.5443955659866333
time_elpased: 4.907
#iterations: 111
currently lose_sum: 0.4109063148498535
time_elpased: 5.187
#iterations: 112
currently lose_sum: 0.5046577453613281
time_elpased: 5.088
#iterations: 113
currently lose_sum: 0.40806078910827637
time_elpased: 5.188
#iterations: 114
currently lose_sum: 0.5312808752059937
time_elpased: 5.085
#iterations: 115
currently lose_sum: 0.5936992168426514
time_elpased: 5.051
#iterations: 116
currently lose_sum: 0.45346373319625854
time_elpased: 4.934
#iterations: 117
currently lose_sum: 0.5957731008529663
time_elpased: 4.974
#iterations: 118
currently lose_sum: 0.6058504581451416
time_elpased: 5.041
#iterations: 119
currently lose_sum: 0.5115822553634644
time_elpased: 5.017
start validation test
0.662820512821
0.706161137441
0.567979669632
0.629577464789
0.663679356161
25.318
#iterations: 120
currently lose_sum: 0.4936143755912781
time_elpased: 4.925
#iterations: 121
currently lose_sum: 0.5974076986312866
time_elpased: 4.939
#iterations: 122
currently lose_sum: 0.4134008288383484
time_elpased: 4.98
#iterations: 123
currently lose_sum: 0.4733756184577942
time_elpased: 4.925
#iterations: 124
currently lose_sum: 0.49187350273132324
time_elpased: 4.952
#iterations: 125
currently lose_sum: 0.48289695382118225
time_elpased: 4.924
#iterations: 126
currently lose_sum: 0.4623146057128906
time_elpased: 4.928
#iterations: 127
currently lose_sum: 0.5422555804252625
time_elpased: 4.913
#iterations: 128
currently lose_sum: 0.47530192136764526
time_elpased: 4.918
#iterations: 129
currently lose_sum: 0.4131973683834076
time_elpased: 4.908
#iterations: 130
currently lose_sum: 0.656864583492279
time_elpased: 4.912
#iterations: 131
currently lose_sum: 0.6053764820098877
time_elpased: 4.962
#iterations: 132
currently lose_sum: 0.6101654767990112
time_elpased: 4.935
#iterations: 133
currently lose_sum: 0.5164139866828918
time_elpased: 4.941
#iterations: 134
currently lose_sum: 0.5332104563713074
time_elpased: 4.905
#iterations: 135
currently lose_sum: 0.4174392819404602
time_elpased: 4.949
#iterations: 136
currently lose_sum: 0.3083501160144806
time_elpased: 4.954
#iterations: 137
currently lose_sum: 0.4843021035194397
time_elpased: 4.902
#iterations: 138
currently lose_sum: 0.46193018555641174
time_elpased: 4.906
#iterations: 139
currently lose_sum: 0.4112470746040344
time_elpased: 4.926
start validation test
0.675641025641
0.740994854202
0.548919949174
0.630656934307
0.676788564496
25.660
#iterations: 140
currently lose_sum: 0.5492538809776306
time_elpased: 4.896
#iterations: 141
currently lose_sum: 0.5703120231628418
time_elpased: 4.891
#iterations: 142
currently lose_sum: 0.4168910086154938
time_elpased: 4.917
#iterations: 143
currently lose_sum: 0.2625427842140198
time_elpased: 4.959
#iterations: 144
currently lose_sum: 0.5447372794151306
time_elpased: 4.921
#iterations: 145
currently lose_sum: 0.3772178590297699
time_elpased: 4.944
#iterations: 146
currently lose_sum: 0.4965950846672058
time_elpased: 4.926
#iterations: 147
currently lose_sum: 0.47572773694992065
time_elpased: 4.941
#iterations: 148
currently lose_sum: 0.4759069085121155
time_elpased: 4.95
#iterations: 149
currently lose_sum: 0.28659242391586304
time_elpased: 4.942
#iterations: 150
currently lose_sum: 0.5970157384872437
time_elpased: 4.926
#iterations: 151
currently lose_sum: 0.3802325129508972
time_elpased: 4.936
#iterations: 152
currently lose_sum: 0.4261151850223541
time_elpased: 4.92
#iterations: 153
currently lose_sum: 0.41852059960365295
time_elpased: 4.861
#iterations: 154
currently lose_sum: 0.5614669919013977
time_elpased: 4.939
#iterations: 155
currently lose_sum: 0.35396939516067505
time_elpased: 4.915
#iterations: 156
currently lose_sum: 0.36561134457588196
time_elpased: 4.934
#iterations: 157
currently lose_sum: 0.44468408823013306
time_elpased: 4.926
#iterations: 158
currently lose_sum: 0.470803439617157
time_elpased: 4.926
#iterations: 159
currently lose_sum: 0.6137377023696899
time_elpased: 4.995
start validation test
0.65641025641
0.62951496388
0.775095298602
0.694760820046
0.655335488887
24.985
#iterations: 160
currently lose_sum: 0.4873664975166321
time_elpased: 4.909
#iterations: 161
currently lose_sum: 0.4723488390445709
time_elpased: 4.939
#iterations: 162
currently lose_sum: 0.328923761844635
time_elpased: 4.898
#iterations: 163
currently lose_sum: 0.3641622066497803
time_elpased: 4.954
#iterations: 164
currently lose_sum: 0.3404439389705658
time_elpased: 4.882
#iterations: 165
currently lose_sum: 0.4204215407371521
time_elpased: 4.919
#iterations: 166
currently lose_sum: 0.5471493005752563
time_elpased: 4.955
#iterations: 167
currently lose_sum: 0.5020624399185181
time_elpased: 4.9
#iterations: 168
currently lose_sum: 0.4320030212402344
time_elpased: 4.931
#iterations: 169
currently lose_sum: 0.3593164384365082
time_elpased: 4.899
#iterations: 170
currently lose_sum: 0.5213547945022583
time_elpased: 4.97
#iterations: 171
currently lose_sum: 0.5652830600738525
time_elpased: 4.887
#iterations: 172
currently lose_sum: 0.4074353277683258
time_elpased: 4.965
#iterations: 173
currently lose_sum: 0.45155850052833557
time_elpased: 4.905
#iterations: 174
currently lose_sum: 0.33033934235572815
time_elpased: 4.912
#iterations: 175
currently lose_sum: 0.4883609712123871
time_elpased: 4.907
#iterations: 176
currently lose_sum: 0.3448987603187561
time_elpased: 4.932
#iterations: 177
currently lose_sum: 0.28707319498062134
time_elpased: 4.965
#iterations: 178
currently lose_sum: 0.3872172236442566
time_elpased: 4.915
#iterations: 179
currently lose_sum: 0.5723026394844055
time_elpased: 5.192
start validation test
0.654487179487
0.806930693069
0.414231257942
0.547439126784
0.656662847599
31.891
#iterations: 180
currently lose_sum: 0.4420333504676819
time_elpased: 5.0
#iterations: 181
currently lose_sum: 0.46998119354248047
time_elpased: 5.079
#iterations: 182
currently lose_sum: 0.5159304738044739
time_elpased: 4.989
#iterations: 183
currently lose_sum: 0.43620187044143677
time_elpased: 4.916
#iterations: 184
currently lose_sum: 0.4004676938056946
time_elpased: 5.003
#iterations: 185
currently lose_sum: 0.4939168393611908
time_elpased: 4.945
#iterations: 186
currently lose_sum: 0.2876526713371277
time_elpased: 4.949
#iterations: 187
currently lose_sum: 0.46801844239234924
time_elpased: 4.927
#iterations: 188
currently lose_sum: 0.43009814620018005
time_elpased: 4.983
#iterations: 189
currently lose_sum: 0.43504300713539124
time_elpased: 4.93
#iterations: 190
currently lose_sum: 0.5264881253242493
time_elpased: 4.903
#iterations: 191
currently lose_sum: 0.4599152207374573
time_elpased: 4.952
#iterations: 192
currently lose_sum: 0.39460012316703796
time_elpased: 4.953
#iterations: 193
currently lose_sum: 0.5608795881271362
time_elpased: 4.893
#iterations: 194
currently lose_sum: 0.46015721559524536
time_elpased: 5.011
#iterations: 195
currently lose_sum: 0.44141632318496704
time_elpased: 5.007
#iterations: 196
currently lose_sum: 0.45061931014060974
time_elpased: 5.011
#iterations: 197
currently lose_sum: 0.5681418776512146
time_elpased: 4.954
#iterations: 198
currently lose_sum: 0.6085362434387207
time_elpased: 4.923
#iterations: 199
currently lose_sum: 0.4680498242378235
time_elpased: 4.907
start validation test
0.621153846154
0.865671641791
0.294790343075
0.43981042654
0.624109272443
27.998
#iterations: 200
currently lose_sum: 0.5453193783760071
time_elpased: 4.944
#iterations: 201
currently lose_sum: 0.3468654751777649
time_elpased: 4.915
#iterations: 202
currently lose_sum: 0.4209650456905365
time_elpased: 5.071
#iterations: 203
currently lose_sum: 0.5462774038314819
time_elpased: 5.073
#iterations: 204
currently lose_sum: 0.5297912359237671
time_elpased: 4.983
#iterations: 205
currently lose_sum: 0.3546982407569885
time_elpased: 4.896
#iterations: 206
currently lose_sum: 0.66978919506073
time_elpased: 4.898
#iterations: 207
currently lose_sum: 0.38540786504745483
time_elpased: 4.923
#iterations: 208
currently lose_sum: 0.44345760345458984
time_elpased: 4.89
#iterations: 209
currently lose_sum: 0.42738789319992065
time_elpased: 4.927
#iterations: 210
currently lose_sum: 0.3279181122779846
time_elpased: 4.946
#iterations: 211
currently lose_sum: 0.4033200144767761
time_elpased: 4.904
#iterations: 212
currently lose_sum: 0.5398191213607788
time_elpased: 4.903
#iterations: 213
currently lose_sum: 0.4123806357383728
time_elpased: 4.934
#iterations: 214
currently lose_sum: 0.23402008414268494
time_elpased: 4.977
#iterations: 215
currently lose_sum: 0.29682597517967224
time_elpased: 4.979
#iterations: 216
currently lose_sum: 0.5421061515808105
time_elpased: 5.017
#iterations: 217
currently lose_sum: 0.48979297280311584
time_elpased: 4.952
#iterations: 218
currently lose_sum: 0.4583856463432312
time_elpased: 4.893
#iterations: 219
currently lose_sum: 0.3470895290374756
time_elpased: 4.972
start validation test
0.667307692308
0.679624664879
0.644218551461
0.6614481409
0.667516778965
27.145
#iterations: 220
currently lose_sum: 0.5836480855941772
time_elpased: 4.961
#iterations: 221
currently lose_sum: 0.45135098695755005
time_elpased: 4.892
#iterations: 222
currently lose_sum: 0.29404160380363464
time_elpased: 4.935
#iterations: 223
currently lose_sum: 0.3092983663082123
time_elpased: 4.935
#iterations: 224
currently lose_sum: 0.4115414619445801
time_elpased: 4.879
#iterations: 225
currently lose_sum: 0.44017845392227173
time_elpased: 4.978
#iterations: 226
currently lose_sum: 0.4343600869178772
time_elpased: 4.954
#iterations: 227
currently lose_sum: 0.4050380289554596
time_elpased: 4.938
#iterations: 228
currently lose_sum: 0.4567902088165283
time_elpased: 4.985
#iterations: 229
currently lose_sum: 0.3266526758670807
time_elpased: 4.918
#iterations: 230
currently lose_sum: 0.40861400961875916
time_elpased: 4.932
#iterations: 231
currently lose_sum: 0.4843927323818207
time_elpased: 4.956
#iterations: 232
currently lose_sum: 0.4667800962924957
time_elpased: 4.966
#iterations: 233
currently lose_sum: 0.4596485197544098
time_elpased: 4.933
#iterations: 234
currently lose_sum: 0.5303953289985657
time_elpased: 4.925
#iterations: 235
currently lose_sum: 0.34375327825546265
time_elpased: 4.93
#iterations: 236
currently lose_sum: 0.34514370560646057
time_elpased: 4.94
#iterations: 237
currently lose_sum: 0.3407526910305023
time_elpased: 4.955
#iterations: 238
currently lose_sum: 0.5028183460235596
time_elpased: 5.005
#iterations: 239
currently lose_sum: 0.6107637286186218
time_elpased: 4.92
start validation test
0.646153846154
0.850746268657
0.362134688691
0.508021390374
0.648725817826
31.320
#iterations: 240
currently lose_sum: 0.27444082498550415
time_elpased: 4.903
#iterations: 241
currently lose_sum: 0.33307433128356934
time_elpased: 4.943
#iterations: 242
currently lose_sum: 0.4103282392024994
time_elpased: 4.91
#iterations: 243
currently lose_sum: 0.29124507308006287
time_elpased: 4.939
#iterations: 244
currently lose_sum: 0.40791255235671997
time_elpased: 4.926
#iterations: 245
currently lose_sum: 0.39182204008102417
time_elpased: 4.959
#iterations: 246
currently lose_sum: 0.48067235946655273
time_elpased: 4.939
#iterations: 247
currently lose_sum: 0.4171779751777649
time_elpased: 4.948
#iterations: 248
currently lose_sum: 0.41888031363487244
time_elpased: 4.914
#iterations: 249
currently lose_sum: 0.37817099690437317
time_elpased: 5.08
#iterations: 250
currently lose_sum: 0.3737393319606781
time_elpased: 5.078
#iterations: 251
currently lose_sum: 0.4718666076660156
time_elpased: 4.985
#iterations: 252
currently lose_sum: 0.5320236086845398
time_elpased: 4.945
#iterations: 253
currently lose_sum: 0.45916619896888733
time_elpased: 4.945
#iterations: 254
currently lose_sum: 0.43728309869766235
time_elpased: 4.946
#iterations: 255
currently lose_sum: 0.31857770681381226
time_elpased: 4.913
#iterations: 256
currently lose_sum: 0.2509337067604065
time_elpased: 4.95
#iterations: 257
currently lose_sum: 0.29360431432724
time_elpased: 5.141
#iterations: 258
currently lose_sum: 0.5204700231552124
time_elpased: 5.092
#iterations: 259
currently lose_sum: 0.3908139765262604
time_elpased: 4.949
start validation test
0.675
0.708333333333
0.604828462516
0.652501713502
0.675635447299
27.737
#iterations: 260
currently lose_sum: 0.3750450015068054
time_elpased: 4.926
#iterations: 261
currently lose_sum: 0.3617950677871704
time_elpased: 4.951
#iterations: 262
currently lose_sum: 0.40269097685813904
time_elpased: 4.946
#iterations: 263
currently lose_sum: 0.41744154691696167
time_elpased: 4.972
#iterations: 264
currently lose_sum: 0.34423479437828064
time_elpased: 4.912
#iterations: 265
currently lose_sum: 0.38230711221694946
time_elpased: 4.954
#iterations: 266
currently lose_sum: 0.38762497901916504
time_elpased: 5.042
#iterations: 267
currently lose_sum: 0.40072211623191833
time_elpased: 4.955
#iterations: 268
currently lose_sum: 0.27731969952583313
time_elpased: 4.903
#iterations: 269
currently lose_sum: 0.4254274368286133
time_elpased: 4.893
#iterations: 270
currently lose_sum: 0.39225250482559204
time_elpased: 4.927
#iterations: 271
currently lose_sum: 0.20444336533546448
time_elpased: 4.937
#iterations: 272
currently lose_sum: 0.3469504117965698
time_elpased: 4.927
#iterations: 273
currently lose_sum: 0.30271825194358826
time_elpased: 5.033
#iterations: 274
currently lose_sum: 0.5495627522468567
time_elpased: 4.956
#iterations: 275
currently lose_sum: 0.3963393568992615
time_elpased: 4.977
#iterations: 276
currently lose_sum: 0.3797268569469452
time_elpased: 4.913
#iterations: 277
currently lose_sum: 0.2860221564769745
time_elpased: 4.975
#iterations: 278
currently lose_sum: 0.30114609003067017
time_elpased: 4.955
#iterations: 279
currently lose_sum: 0.30785995721817017
time_elpased: 4.943
start validation test
0.686538461538
0.713467048711
0.632782719187
0.670707070707
0.687025253513
30.220
#iterations: 280
currently lose_sum: 0.3720419406890869
time_elpased: 4.951
#iterations: 281
currently lose_sum: 0.31579476594924927
time_elpased: 4.928
#iterations: 282
currently lose_sum: 0.39563265442848206
time_elpased: 5.0
#iterations: 283
currently lose_sum: 0.21465525031089783
time_elpased: 4.918
#iterations: 284
currently lose_sum: 0.40170544385910034
time_elpased: 4.944
#iterations: 285
currently lose_sum: 0.23957225680351257
time_elpased: 4.918
#iterations: 286
currently lose_sum: 0.2734666168689728
time_elpased: 4.948
#iterations: 287
currently lose_sum: 0.6150516271591187
time_elpased: 4.908
#iterations: 288
currently lose_sum: 0.3814505636692047
time_elpased: 4.93
#iterations: 289
currently lose_sum: 0.28654804825782776
time_elpased: 4.95
#iterations: 290
currently lose_sum: 0.4260752201080322
time_elpased: 4.968
#iterations: 291
currently lose_sum: 0.5616072416305542
time_elpased: 4.892
#iterations: 292
currently lose_sum: 0.35343730449676514
time_elpased: 4.917
#iterations: 293
currently lose_sum: 0.2659134268760681
time_elpased: 5.021
#iterations: 294
currently lose_sum: 0.4721105098724365
time_elpased: 4.895
#iterations: 295
currently lose_sum: 0.35724499821662903
time_elpased: 4.922
#iterations: 296
currently lose_sum: 0.37648218870162964
time_elpased: 4.901
#iterations: 297
currently lose_sum: 0.3916313648223877
time_elpased: 4.964
#iterations: 298
currently lose_sum: 0.3697834610939026
time_elpased: 4.901
#iterations: 299
currently lose_sum: 0.26673874258995056
time_elpased: 4.976
start validation test
0.673076923077
0.696453900709
0.623888182973
0.6581769437
0.673522357981
27.811
#iterations: 300
currently lose_sum: 0.30328255891799927
time_elpased: 4.927
#iterations: 301
currently lose_sum: 0.35752028226852417
time_elpased: 4.935
#iterations: 302
currently lose_sum: 0.3855920433998108
time_elpased: 4.942
#iterations: 303
currently lose_sum: 0.35975727438926697
time_elpased: 4.942
#iterations: 304
currently lose_sum: 0.32403048872947693
time_elpased: 4.901
#iterations: 305
currently lose_sum: 0.3669375479221344
time_elpased: 4.947
#iterations: 306
currently lose_sum: 0.42699670791625977
time_elpased: 4.943
#iterations: 307
currently lose_sum: 0.4815998077392578
time_elpased: 4.956
#iterations: 308
currently lose_sum: 0.45905375480651855
time_elpased: 4.953
#iterations: 309
currently lose_sum: 0.5505062341690063
time_elpased: 4.915
#iterations: 310
currently lose_sum: 0.44447803497314453
time_elpased: 4.93
#iterations: 311
currently lose_sum: 0.4998093247413635
time_elpased: 4.942
#iterations: 312
currently lose_sum: 0.21907714009284973
time_elpased: 4.997
#iterations: 313
currently lose_sum: 0.3471231460571289
time_elpased: 4.985
#iterations: 314
currently lose_sum: 0.40173354744911194
time_elpased: 4.954
#iterations: 315
currently lose_sum: 0.335720956325531
time_elpased: 4.956
#iterations: 316
currently lose_sum: 0.453115850687027
time_elpased: 4.9
#iterations: 317
currently lose_sum: 0.40333279967308044
time_elpased: 4.953
#iterations: 318
currently lose_sum: 0.49018439650535583
time_elpased: 5.025
#iterations: 319
currently lose_sum: 0.3375822901725769
time_elpased: 4.96
start validation test
0.672435897436
0.752747252747
0.522236340534
0.616654163541
0.673796048663
29.308
#iterations: 320
currently lose_sum: 0.3818555772304535
time_elpased: 4.958
#iterations: 321
currently lose_sum: 0.3791700303554535
time_elpased: 4.996
#iterations: 322
currently lose_sum: 0.5575194358825684
time_elpased: 4.921
#iterations: 323
currently lose_sum: 0.4209051728248596
time_elpased: 4.953
#iterations: 324
currently lose_sum: 0.4227622151374817
time_elpased: 4.898
#iterations: 325
currently lose_sum: 0.3480856120586395
time_elpased: 4.907
#iterations: 326
currently lose_sum: 0.3289719820022583
time_elpased: 4.925
#iterations: 327
currently lose_sum: 0.32033565640449524
time_elpased: 4.972
#iterations: 328
currently lose_sum: 0.3813213109970093
time_elpased: 4.924
#iterations: 329
currently lose_sum: 0.5853196978569031
time_elpased: 4.94
#iterations: 330
currently lose_sum: 0.20978078246116638
time_elpased: 4.932
#iterations: 331
currently lose_sum: 0.31821078062057495
time_elpased: 4.925
#iterations: 332
currently lose_sum: 0.4688340723514557
time_elpased: 4.944
#iterations: 333
currently lose_sum: 0.4447181224822998
time_elpased: 4.953
#iterations: 334
currently lose_sum: 0.2816358208656311
time_elpased: 4.969
#iterations: 335
currently lose_sum: 0.35583123564720154
time_elpased: 4.908
#iterations: 336
currently lose_sum: 0.3504179120063782
time_elpased: 4.9
#iterations: 337
currently lose_sum: 0.32210487127304077
time_elpased: 4.951
#iterations: 338
currently lose_sum: 0.5441872477531433
time_elpased: 4.912
#iterations: 339
currently lose_sum: 0.5582213401794434
time_elpased: 4.925
start validation test
0.675
0.801724137931
0.472681067344
0.594724220624
0.676832124875
33.615
#iterations: 340
currently lose_sum: 0.4727403223514557
time_elpased: 4.891
#iterations: 341
currently lose_sum: 0.4179611802101135
time_elpased: 4.95
#iterations: 342
currently lose_sum: 0.49919962882995605
time_elpased: 4.885
#iterations: 343
currently lose_sum: 0.35675033926963806
time_elpased: 4.99
#iterations: 344
currently lose_sum: 0.3853565454483032
time_elpased: 4.936
#iterations: 345
currently lose_sum: 0.3957304060459137
time_elpased: 4.923
#iterations: 346
currently lose_sum: 0.3112495541572571
time_elpased: 4.936
#iterations: 347
currently lose_sum: 0.3011722266674042
time_elpased: 4.871
#iterations: 348
currently lose_sum: 0.4248484671115875
time_elpased: 4.969
#iterations: 349
currently lose_sum: 0.2829510569572449
time_elpased: 4.962
#iterations: 350
currently lose_sum: 0.43952980637550354
time_elpased: 5.016
#iterations: 351
currently lose_sum: 0.3684404790401459
time_elpased: 4.937
#iterations: 352
currently lose_sum: 0.348258376121521
time_elpased: 4.937
#iterations: 353
currently lose_sum: 0.4620954394340515
time_elpased: 5.054
#iterations: 354
currently lose_sum: 0.22090983390808105
time_elpased: 4.931
#iterations: 355
currently lose_sum: 0.2518337368965149
time_elpased: 5.01
#iterations: 356
currently lose_sum: 0.2580023407936096
time_elpased: 4.959
#iterations: 357
currently lose_sum: 0.2962118685245514
time_elpased: 4.876
#iterations: 358
currently lose_sum: 0.35001182556152344
time_elpased: 4.972
#iterations: 359
currently lose_sum: 0.25923794507980347
time_elpased: 4.952
start validation test
0.675641025641
0.701001430615
0.622617534943
0.659488559892
0.676121186618
29.798
#iterations: 360
currently lose_sum: 0.37754446268081665
time_elpased: 4.998
#iterations: 361
currently lose_sum: 0.363031804561615
time_elpased: 4.956
#iterations: 362
currently lose_sum: 0.48820382356643677
time_elpased: 4.979
#iterations: 363
currently lose_sum: nan
time_elpased: 4.997
train finish final lose is: nan
acc: 0.686
pre: 0.656
rec: 0.791
F1: 0.717
auc: 0.685
