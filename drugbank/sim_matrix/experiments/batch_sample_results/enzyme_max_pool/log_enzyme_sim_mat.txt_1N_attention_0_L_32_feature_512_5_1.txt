start to construct computing graph
graph construct over
2359
epochs start
#iterations: 0
currently lose_sum: 0.6850658059120178
time_elpased: 5.388
#iterations: 1
currently lose_sum: 0.6993807554244995
time_elpased: 5.365
#iterations: 2
currently lose_sum: 0.6636525392532349
time_elpased: 5.356
#iterations: 3
currently lose_sum: 0.7141062021255493
time_elpased: 5.366
#iterations: 4
currently lose_sum: 0.7122315168380737
time_elpased: 5.381
#iterations: 5
currently lose_sum: 0.7053081393241882
time_elpased: 5.412
#iterations: 6
currently lose_sum: 0.6988812685012817
time_elpased: 5.363
#iterations: 7
currently lose_sum: 0.7103004455566406
time_elpased: 5.377
#iterations: 8
currently lose_sum: 0.6611404418945312
time_elpased: 5.343
#iterations: 9
currently lose_sum: 0.6676082611083984
time_elpased: 5.367
#iterations: 10
currently lose_sum: 0.6896559000015259
time_elpased: 5.364
#iterations: 11
currently lose_sum: 0.6832539439201355
time_elpased: 5.4
#iterations: 12
currently lose_sum: 0.6907170414924622
time_elpased: 5.338
#iterations: 13
currently lose_sum: 0.6749679446220398
time_elpased: 5.408
#iterations: 14
currently lose_sum: 0.720678985118866
time_elpased: 5.408
#iterations: 15
currently lose_sum: 0.6998387575149536
time_elpased: 5.411
#iterations: 16
currently lose_sum: 0.6921947598457336
time_elpased: 5.433
#iterations: 17
currently lose_sum: 0.665564775466919
time_elpased: 5.41
#iterations: 18
currently lose_sum: 0.6663450002670288
time_elpased: 5.363
#iterations: 19
currently lose_sum: 0.7018020749092102
time_elpased: 5.404
start validation test
0.552564102564
0.534738485558
0.870393900889
0.66247582205
0.549685954326
26.925
#iterations: 20
currently lose_sum: 0.6907666921615601
time_elpased: 5.126
#iterations: 21
currently lose_sum: 0.6938685178756714
time_elpased: 5.131
#iterations: 22
currently lose_sum: 0.6898528337478638
time_elpased: 5.074
#iterations: 23
currently lose_sum: 0.6240214109420776
time_elpased: 5.083
#iterations: 24
currently lose_sum: 0.7262082099914551
time_elpased: 5.074
#iterations: 25
currently lose_sum: 0.7320952415466309
time_elpased: 5.165
#iterations: 26
currently lose_sum: 0.6878871321678162
time_elpased: 5.09
#iterations: 27
currently lose_sum: 0.6323844194412231
time_elpased: 5.129
#iterations: 28
currently lose_sum: 0.5896800756454468
time_elpased: 5.075
#iterations: 29
currently lose_sum: 0.6862655878067017
time_elpased: 5.099
#iterations: 30
currently lose_sum: 0.6929623484611511
time_elpased: 5.066
#iterations: 31
currently lose_sum: 0.6733449101448059
time_elpased: 5.178
#iterations: 32
currently lose_sum: 0.6344874501228333
time_elpased: 5.111
#iterations: 33
currently lose_sum: 0.5969035625457764
time_elpased: 5.125
#iterations: 34
currently lose_sum: 0.6647557020187378
time_elpased: 5.152
#iterations: 35
currently lose_sum: 0.6283936500549316
time_elpased: 5.1
#iterations: 36
currently lose_sum: 0.669891357421875
time_elpased: 5.145
#iterations: 37
currently lose_sum: 0.7290455102920532
time_elpased: 5.15
#iterations: 38
currently lose_sum: 0.6433553695678711
time_elpased: 5.118
#iterations: 39
currently lose_sum: 0.6803992390632629
time_elpased: 5.112
start validation test
0.59358974359
0.637837837838
0.449809402795
0.527570789866
0.594891764787
26.147
#iterations: 40
currently lose_sum: 0.7250632047653198
time_elpased: 5.13
#iterations: 41
currently lose_sum: 0.6543687582015991
time_elpased: 5.135
#iterations: 42
currently lose_sum: 0.6775704622268677
time_elpased: 5.167
#iterations: 43
currently lose_sum: 0.6060425639152527
time_elpased: 5.086
#iterations: 44
currently lose_sum: 0.5418379902839661
time_elpased: 5.143
#iterations: 45
currently lose_sum: 0.6796014308929443
time_elpased: 5.198
#iterations: 46
currently lose_sum: 0.6225767135620117
time_elpased: 5.126
#iterations: 47
currently lose_sum: 0.6760188341140747
time_elpased: 5.079
#iterations: 48
currently lose_sum: 0.5066651105880737
time_elpased: 5.086
#iterations: 49
currently lose_sum: 0.7212639451026917
time_elpased: 5.124
#iterations: 50
currently lose_sum: 0.6999181509017944
time_elpased: 5.092
#iterations: 51
currently lose_sum: 0.7054380178451538
time_elpased: 5.13
#iterations: 52
currently lose_sum: 0.6588598489761353
time_elpased: 5.117
#iterations: 53
currently lose_sum: 0.6150525212287903
time_elpased: 5.15
#iterations: 54
currently lose_sum: 0.6715816259384155
time_elpased: 5.099
#iterations: 55
currently lose_sum: 0.6065621376037598
time_elpased: 5.117
#iterations: 56
currently lose_sum: 0.5202755331993103
time_elpased: 5.114
#iterations: 57
currently lose_sum: 0.488599956035614
time_elpased: 5.078
#iterations: 58
currently lose_sum: 0.6048148274421692
time_elpased: 5.146
#iterations: 59
currently lose_sum: 0.517026424407959
time_elpased: 5.167
start validation test
0.637820512821
0.654166666667
0.598475222363
0.625082946251
0.638176809112
24.946
#iterations: 60
currently lose_sum: 0.590420126914978
time_elpased: 5.093
#iterations: 61
currently lose_sum: 0.5546547770500183
time_elpased: 5.121
#iterations: 62
currently lose_sum: 0.6649934649467468
time_elpased: 5.129
#iterations: 63
currently lose_sum: 0.5600355863571167
time_elpased: 5.135
#iterations: 64
currently lose_sum: 0.5849572420120239
time_elpased: 5.119
#iterations: 65
currently lose_sum: 0.6395166516304016
time_elpased: 5.081
#iterations: 66
currently lose_sum: 0.557349681854248
time_elpased: 5.079
#iterations: 67
currently lose_sum: 0.5962485074996948
time_elpased: 5.131
#iterations: 68
currently lose_sum: 0.48802676796913147
time_elpased: 5.095
#iterations: 69
currently lose_sum: 0.6311239004135132
time_elpased: 5.11
#iterations: 70
currently lose_sum: 0.7397032380104065
time_elpased: 5.156
#iterations: 71
currently lose_sum: 0.5824671983718872
time_elpased: 5.143
#iterations: 72
currently lose_sum: 0.6611943244934082
time_elpased: 5.133
#iterations: 73
currently lose_sum: 0.5985344052314758
time_elpased: 5.093
#iterations: 74
currently lose_sum: 0.5650008320808411
time_elpased: 5.098
#iterations: 75
currently lose_sum: 0.6305912137031555
time_elpased: 5.129
#iterations: 76
currently lose_sum: 0.6416939496994019
time_elpased: 5.097
#iterations: 77
currently lose_sum: 0.44494134187698364
time_elpased: 5.097
#iterations: 78
currently lose_sum: 0.6302737593650818
time_elpased: 5.097
#iterations: 79
currently lose_sum: 0.5486904382705688
time_elpased: 5.145
start validation test
0.639102564103
0.663742690058
0.576874205845
0.617267165194
0.63966608093
24.750
#iterations: 80
currently lose_sum: 0.5564733743667603
time_elpased: 5.146
#iterations: 81
currently lose_sum: 0.6055063009262085
time_elpased: 5.141
#iterations: 82
currently lose_sum: 0.5665062069892883
time_elpased: 5.144
#iterations: 83
currently lose_sum: 0.46681493520736694
time_elpased: 5.112
#iterations: 84
currently lose_sum: 0.5285162925720215
time_elpased: 5.111
#iterations: 85
currently lose_sum: 0.625627338886261
time_elpased: 5.133
#iterations: 86
currently lose_sum: 0.5370696783065796
time_elpased: 5.122
#iterations: 87
currently lose_sum: 0.5696777701377869
time_elpased: 5.089
#iterations: 88
currently lose_sum: 0.6816779375076294
time_elpased: 5.143
#iterations: 89
currently lose_sum: 0.5562165379524231
time_elpased: 5.124
#iterations: 90
currently lose_sum: 0.526569664478302
time_elpased: 5.161
#iterations: 91
currently lose_sum: 0.807929515838623
time_elpased: 5.11
#iterations: 92
currently lose_sum: 0.6341482400894165
time_elpased: 5.155
#iterations: 93
currently lose_sum: 0.5794200897216797
time_elpased: 5.128
#iterations: 94
currently lose_sum: 0.6861036419868469
time_elpased: 5.085
#iterations: 95
currently lose_sum: 0.6275347471237183
time_elpased: 5.133
#iterations: 96
currently lose_sum: 0.6422797441482544
time_elpased: 5.061
#iterations: 97
currently lose_sum: 0.4955310821533203
time_elpased: 5.133
#iterations: 98
currently lose_sum: 0.5051533579826355
time_elpased: 5.079
#iterations: 99
currently lose_sum: 0.5198105573654175
time_elpased: 5.154
start validation test
0.658974358974
0.626112759644
0.804320203304
0.704115684093
0.65765816116
25.642
#iterations: 100
currently lose_sum: 0.5162453651428223
time_elpased: 5.137
#iterations: 101
currently lose_sum: 0.5112071633338928
time_elpased: 5.113
#iterations: 102
currently lose_sum: 0.5601247549057007
time_elpased: 5.112
#iterations: 103
currently lose_sum: 0.5621508955955505
time_elpased: 5.128
#iterations: 104
currently lose_sum: 0.5838482975959778
time_elpased: 5.133
#iterations: 105
currently lose_sum: 0.7171251177787781
time_elpased: 5.12
#iterations: 106
currently lose_sum: 0.5864384174346924
time_elpased: 5.071
#iterations: 107
currently lose_sum: 0.7488117218017578
time_elpased: 5.122
#iterations: 108
currently lose_sum: 0.5290775299072266
time_elpased: 5.169
#iterations: 109
currently lose_sum: 0.6755579710006714
time_elpased: 5.135
#iterations: 110
currently lose_sum: 0.49264007806777954
time_elpased: 5.105
#iterations: 111
currently lose_sum: 0.6686345934867859
time_elpased: 5.113
#iterations: 112
currently lose_sum: 0.435016930103302
time_elpased: 5.13
#iterations: 113
currently lose_sum: 0.5473223924636841
time_elpased: 5.1
#iterations: 114
currently lose_sum: 0.5386937260627747
time_elpased: 5.087
#iterations: 115
currently lose_sum: 0.6120250821113586
time_elpased: 5.151
#iterations: 116
currently lose_sum: 0.3861515522003174
time_elpased: 5.116
#iterations: 117
currently lose_sum: 0.7165371775627136
time_elpased: 5.082
#iterations: 118
currently lose_sum: 0.47523242235183716
time_elpased: 5.188
#iterations: 119
currently lose_sum: 0.6733154058456421
time_elpased: 5.104
start validation test
0.632692307692
0.681355932203
0.510800508259
0.583877995643
0.633796114414
26.431
#iterations: 120
currently lose_sum: 0.6099898219108582
time_elpased: 5.111
#iterations: 121
currently lose_sum: 0.5191024541854858
time_elpased: 5.161
#iterations: 122
currently lose_sum: 0.44406843185424805
time_elpased: 5.105
#iterations: 123
currently lose_sum: 0.5134203433990479
time_elpased: 5.132
#iterations: 124
currently lose_sum: 0.5908015370368958
time_elpased: 5.148
#iterations: 125
currently lose_sum: 0.5202638506889343
time_elpased: 5.147
#iterations: 126
currently lose_sum: 0.6282522678375244
time_elpased: 5.109
#iterations: 127
currently lose_sum: 0.5533589720726013
time_elpased: 5.132
#iterations: 128
currently lose_sum: 0.44894951581954956
time_elpased: 5.113
#iterations: 129
currently lose_sum: 0.5072395205497742
time_elpased: 5.081
#iterations: 130
currently lose_sum: 0.6147933006286621
time_elpased: 5.135
#iterations: 131
currently lose_sum: 0.6025876402854919
time_elpased: 5.139
#iterations: 132
currently lose_sum: 0.40198153257369995
time_elpased: 5.211
#iterations: 133
currently lose_sum: 0.6237412095069885
time_elpased: 5.167
#iterations: 134
currently lose_sum: 0.5312985181808472
time_elpased: 5.163
#iterations: 135
currently lose_sum: 0.6158444881439209
time_elpased: 5.133
#iterations: 136
currently lose_sum: 0.6563025712966919
time_elpased: 5.181
#iterations: 137
currently lose_sum: 0.35204175114631653
time_elpased: 5.101
#iterations: 138
currently lose_sum: 0.5773115158081055
time_elpased: 5.139
#iterations: 139
currently lose_sum: 0.5100639462471008
time_elpased: 5.129
start validation test
0.662179487179
0.650462962963
0.714104193139
0.680799515445
0.661709276388
24.094
#iterations: 140
currently lose_sum: 0.43733301758766174
time_elpased: 5.114
#iterations: 141
currently lose_sum: 0.5888541340827942
time_elpased: 5.148
#iterations: 142
currently lose_sum: 0.6124200820922852
time_elpased: 5.118
#iterations: 143
currently lose_sum: 0.4761603772640228
time_elpased: 5.182
#iterations: 144
currently lose_sum: 0.4860580861568451
time_elpased: 5.208
#iterations: 145
currently lose_sum: 0.5989252328872681
time_elpased: 5.114
#iterations: 146
currently lose_sum: 0.2755112051963806
time_elpased: 5.095
#iterations: 147
currently lose_sum: 0.4790921211242676
time_elpased: 5.109
#iterations: 148
currently lose_sum: 0.5547860264778137
time_elpased: 5.132
#iterations: 149
currently lose_sum: 0.620791494846344
time_elpased: 5.102
#iterations: 150
currently lose_sum: 0.5763286352157593
time_elpased: 5.111
#iterations: 151
currently lose_sum: 0.39071452617645264
time_elpased: 5.149
#iterations: 152
currently lose_sum: 0.49511805176734924
time_elpased: 5.126
#iterations: 153
currently lose_sum: 0.49061593413352966
time_elpased: 5.143
#iterations: 154
currently lose_sum: 0.6797336339950562
time_elpased: 5.066
#iterations: 155
currently lose_sum: 0.35573717951774597
time_elpased: 5.18
#iterations: 156
currently lose_sum: 0.5146318674087524
time_elpased: 5.122
#iterations: 157
currently lose_sum: 0.4962221086025238
time_elpased: 5.121
#iterations: 158
currently lose_sum: 0.5267331600189209
time_elpased: 5.143
#iterations: 159
currently lose_sum: 0.5684634447097778
time_elpased: 5.121
start validation test
0.640384615385
0.606805293006
0.815756035578
0.69593495935
0.638796517142
27.118
#iterations: 160
currently lose_sum: 0.3537963926792145
time_elpased: 5.097
#iterations: 161
currently lose_sum: 0.33934634923934937
time_elpased: 5.094
#iterations: 162
currently lose_sum: 0.44739213585853577
time_elpased: 5.112
#iterations: 163
currently lose_sum: 0.3800114095211029
time_elpased: 5.16
#iterations: 164
currently lose_sum: 0.5062378644943237
time_elpased: 5.11
#iterations: 165
currently lose_sum: 0.4702836573123932
time_elpased: 5.174
#iterations: 166
currently lose_sum: 0.44589439034461975
time_elpased: 5.12
#iterations: 167
currently lose_sum: 0.3800255060195923
time_elpased: 5.144
#iterations: 168
currently lose_sum: 0.5476009249687195
time_elpased: 5.125
#iterations: 169
currently lose_sum: 0.45246657729148865
time_elpased: 5.083
#iterations: 170
currently lose_sum: 0.43340587615966797
time_elpased: 5.177
#iterations: 171
currently lose_sum: 0.3729347586631775
time_elpased: 5.125
#iterations: 172
currently lose_sum: 0.5109725594520569
time_elpased: 5.187
#iterations: 173
currently lose_sum: 0.5044817924499512
time_elpased: 5.174
#iterations: 174
currently lose_sum: 0.4351062774658203
time_elpased: 5.149
#iterations: 175
currently lose_sum: 0.6269775629043579
time_elpased: 5.122
#iterations: 176
currently lose_sum: 0.4734157621860504
time_elpased: 5.177
#iterations: 177
currently lose_sum: 0.39159393310546875
time_elpased: 5.111
#iterations: 178
currently lose_sum: 0.4872571527957916
time_elpased: 5.149
#iterations: 179
currently lose_sum: 0.5486844182014465
time_elpased: 5.132
start validation test
0.637820512821
0.729338842975
0.448538754765
0.555468135327
0.6395345779
25.300
#iterations: 180
currently lose_sum: 0.6243851780891418
time_elpased: 5.08
#iterations: 181
currently lose_sum: 0.46392154693603516
time_elpased: 5.133
#iterations: 182
currently lose_sum: 0.5900192260742188
time_elpased: 5.129
#iterations: 183
currently lose_sum: 0.5490723848342896
time_elpased: 5.111
#iterations: 184
currently lose_sum: 0.45888662338256836
time_elpased: 5.075
#iterations: 185
currently lose_sum: 0.44429558515548706
time_elpased: 5.07
#iterations: 186
currently lose_sum: 0.29931631684303284
time_elpased: 5.133
#iterations: 187
currently lose_sum: 0.5093604326248169
time_elpased: 5.119
#iterations: 188
currently lose_sum: 0.4240795075893402
time_elpased: 5.105
#iterations: 189
currently lose_sum: 0.5046074390411377
time_elpased: 5.13
#iterations: 190
currently lose_sum: 0.5658096075057983
time_elpased: 5.121
#iterations: 191
currently lose_sum: 0.483967125415802
time_elpased: 5.172
#iterations: 192
currently lose_sum: 0.4673725962638855
time_elpased: 5.086
#iterations: 193
currently lose_sum: 0.43689146637916565
time_elpased: 5.104
#iterations: 194
currently lose_sum: 0.706297755241394
time_elpased: 5.133
#iterations: 195
currently lose_sum: 0.3683655261993408
time_elpased: 5.154
#iterations: 196
currently lose_sum: 0.5392218232154846
time_elpased: 5.076
#iterations: 197
currently lose_sum: 0.3459581434726715
time_elpased: 5.095
#iterations: 198
currently lose_sum: 0.40164700150489807
time_elpased: 5.133
#iterations: 199
currently lose_sum: 0.35003361105918884
time_elpased: 5.147
start validation test
0.646794871795
0.712230215827
0.503176620076
0.589724497394
0.648095425174
27.427
#iterations: 200
currently lose_sum: 0.7053031325340271
time_elpased: 5.118
#iterations: 201
currently lose_sum: 0.5178620219230652
time_elpased: 5.119
#iterations: 202
currently lose_sum: 0.38441285490989685
time_elpased: 5.139
#iterations: 203
currently lose_sum: 0.45927000045776367
time_elpased: 5.17
#iterations: 204
currently lose_sum: 0.3621141016483307
time_elpased: 5.144
#iterations: 205
currently lose_sum: 0.37716037034988403
time_elpased: 5.104
#iterations: 206
currently lose_sum: 0.4134233593940735
time_elpased: 5.133
#iterations: 207
currently lose_sum: 0.23755903542041779
time_elpased: 5.132
#iterations: 208
currently lose_sum: 0.43468841910362244
time_elpased: 5.098
#iterations: 209
currently lose_sum: 0.3781266212463379
time_elpased: 5.126
#iterations: 210
currently lose_sum: 0.39715376496315
time_elpased: 5.11
#iterations: 211
currently lose_sum: 0.6371711492538452
time_elpased: 5.145
#iterations: 212
currently lose_sum: 0.38119345903396606
time_elpased: 5.081
#iterations: 213
currently lose_sum: 0.4254611134529114
time_elpased: 5.188
#iterations: 214
currently lose_sum: 0.4870108664035797
time_elpased: 5.146
#iterations: 215
currently lose_sum: 0.4144011437892914
time_elpased: 5.171
#iterations: 216
currently lose_sum: 0.48058128356933594
time_elpased: 5.178
#iterations: 217
currently lose_sum: 0.4904863238334656
time_elpased: 5.138
#iterations: 218
currently lose_sum: 0.40627360343933105
time_elpased: 5.092
#iterations: 219
currently lose_sum: 0.4536851942539215
time_elpased: 5.121
start validation test
0.627564102564
0.765463917526
0.377382465057
0.505531914894
0.629829654262
30.456
#iterations: 220
currently lose_sum: 0.432807058095932
time_elpased: 5.137
#iterations: 221
currently lose_sum: 0.4247865080833435
time_elpased: 5.136
#iterations: 222
currently lose_sum: 0.4498752951622009
time_elpased: 5.104
#iterations: 223
currently lose_sum: 0.49354642629623413
time_elpased: 5.138
#iterations: 224
currently lose_sum: 0.4164668023586273
time_elpased: 5.124
#iterations: 225
currently lose_sum: 0.3209681510925293
time_elpased: 5.076
#iterations: 226
currently lose_sum: 0.339650958776474
time_elpased: 5.181
#iterations: 227
currently lose_sum: 0.37299782037734985
time_elpased: 5.151
#iterations: 228
currently lose_sum: 0.5911033749580383
time_elpased: 5.184
#iterations: 229
currently lose_sum: 0.41024526953697205
time_elpased: 5.12
#iterations: 230
currently lose_sum: 0.34250450134277344
time_elpased: 5.118
#iterations: 231
currently lose_sum: 0.19263756275177002
time_elpased: 5.107
#iterations: 232
currently lose_sum: 0.4973030090332031
time_elpased: 5.112
#iterations: 233
currently lose_sum: 0.25475555658340454
time_elpased: 5.119
#iterations: 234
currently lose_sum: 0.49378499388694763
time_elpased: 5.161
#iterations: 235
currently lose_sum: 0.4021456241607666
time_elpased: 5.137
#iterations: 236
currently lose_sum: 0.36617693305015564
time_elpased: 5.108
#iterations: 237
currently lose_sum: 0.3848591446876526
time_elpased: 5.142
#iterations: 238
currently lose_sum: 0.48953381180763245
time_elpased: 5.153
#iterations: 239
currently lose_sum: 0.47105178236961365
time_elpased: 5.151
start validation test
0.65641025641
0.668909825034
0.631512071156
0.649673202614
0.656635725099
24.844
#iterations: 240
currently lose_sum: 0.5169849991798401
time_elpased: 5.13
#iterations: 241
currently lose_sum: 0.5766934156417847
time_elpased: 5.098
#iterations: 242
currently lose_sum: 0.2708875238895416
time_elpased: 5.143
#iterations: 243
currently lose_sum: 0.3514280319213867
time_elpased: 5.093
#iterations: 244
currently lose_sum: 0.48844796419143677
time_elpased: 5.149
#iterations: 245
currently lose_sum: 0.421245813369751
time_elpased: 5.086
#iterations: 246
currently lose_sum: 0.3249562680721283
time_elpased: 5.163
#iterations: 247
currently lose_sum: 0.46319109201431274
time_elpased: 5.172
#iterations: 248
currently lose_sum: 0.37657099962234497
time_elpased: 5.094
#iterations: 249
currently lose_sum: 0.41384822130203247
time_elpased: 5.164
#iterations: 250
currently lose_sum: 0.3607789874076843
time_elpased: 5.053
#iterations: 251
currently lose_sum: 0.3481413424015045
time_elpased: 5.137
#iterations: 252
currently lose_sum: 0.4477411210536957
time_elpased: 5.156
#iterations: 253
currently lose_sum: 0.5460171699523926
time_elpased: 5.085
#iterations: 254
currently lose_sum: 0.44019007682800293
time_elpased: 5.132
#iterations: 255
currently lose_sum: 0.461953729391098
time_elpased: 5.126
#iterations: 256
currently lose_sum: 0.33055421710014343
time_elpased: 5.116
#iterations: 257
currently lose_sum: 0.4142306447029114
time_elpased: 5.177
#iterations: 258
currently lose_sum: 0.6112263798713684
time_elpased: 5.131
#iterations: 259
currently lose_sum: 0.45655184984207153
time_elpased: 5.141
start validation test
0.637179487179
0.762470308789
0.407878017789
0.531456953642
0.639255955854
35.223
#iterations: 260
currently lose_sum: 0.5285623669624329
time_elpased: 5.1
#iterations: 261
currently lose_sum: 0.3717584013938904
time_elpased: 5.155
#iterations: 262
currently lose_sum: 0.3670976758003235
time_elpased: 5.139
#iterations: 263
currently lose_sum: 0.3918914794921875
time_elpased: 5.075
#iterations: 264
currently lose_sum: 0.3301202952861786
time_elpased: 5.097
#iterations: 265
currently lose_sum: 0.6478806734085083
time_elpased: 5.085
#iterations: 266
currently lose_sum: 0.3884258270263672
time_elpased: 5.212
#iterations: 267
currently lose_sum: 0.3587561249732971
time_elpased: 5.069
#iterations: 268
currently lose_sum: 0.43619459867477417
time_elpased: 5.057
#iterations: 269
currently lose_sum: 0.37228813767433167
time_elpased: 5.107
#iterations: 270
currently lose_sum: 0.4501533508300781
time_elpased: 5.164
#iterations: 271
currently lose_sum: 0.28238430619239807
time_elpased: 5.174
#iterations: 272
currently lose_sum: 0.3389177918434143
time_elpased: 5.113
#iterations: 273
currently lose_sum: 0.3958239257335663
time_elpased: 5.156
#iterations: 274
currently lose_sum: 0.4415203928947449
time_elpased: 5.172
#iterations: 275
currently lose_sum: 0.3609391152858734
time_elpased: 5.131
#iterations: 276
currently lose_sum: 0.3035581111907959
time_elpased: 5.126
#iterations: 277
currently lose_sum: 0.4788459837436676
time_elpased: 5.117
#iterations: 278
currently lose_sum: 0.40982943773269653
time_elpased: 5.11
#iterations: 279
currently lose_sum: 0.2786393165588379
time_elpased: 5.136
start validation test
0.641025641026
0.708256880734
0.490470139771
0.57957957958
0.642389015552
29.989
#iterations: 280
currently lose_sum: 0.2992171347141266
time_elpased: 5.117
#iterations: 281
currently lose_sum: 0.45689478516578674
time_elpased: 5.155
#iterations: 282
currently lose_sum: 0.40637779235839844
time_elpased: 5.173
#iterations: 283
currently lose_sum: 0.34076207876205444
time_elpased: 5.089
#iterations: 284
currently lose_sum: 0.3715095520019531
time_elpased: 5.144
#iterations: 285
currently lose_sum: 0.35392147302627563
time_elpased: 5.087
#iterations: 286
currently lose_sum: 0.3403244912624359
time_elpased: 5.131
#iterations: 287
currently lose_sum: 0.3569195866584778
time_elpased: 5.11
#iterations: 288
currently lose_sum: 0.43721804022789
time_elpased: 5.126
#iterations: 289
currently lose_sum: 0.5094338655471802
time_elpased: 5.128
#iterations: 290
currently lose_sum: 0.4194518029689789
time_elpased: 5.132
#iterations: 291
currently lose_sum: 0.41295742988586426
time_elpased: 5.194
#iterations: 292
currently lose_sum: 0.33360251784324646
time_elpased: 5.182
#iterations: 293
currently lose_sum: 0.4204232096672058
time_elpased: 5.158
#iterations: 294
currently lose_sum: 0.3955085873603821
time_elpased: 5.088
#iterations: 295
currently lose_sum: 0.5245187878608704
time_elpased: 5.089
#iterations: 296
currently lose_sum: 0.3288029730319977
time_elpased: 5.151
#iterations: 297
currently lose_sum: 0.44459468126296997
time_elpased: 5.166
#iterations: 298
currently lose_sum: 0.27313151955604553
time_elpased: 5.149
#iterations: 299
currently lose_sum: 0.35299283266067505
time_elpased: 5.148
start validation test
0.641666666667
0.748908296943
0.43583227446
0.551004016064
0.643530626234
32.695
#iterations: 300
currently lose_sum: 0.434975802898407
time_elpased: 5.163
#iterations: 301
currently lose_sum: 0.3848312497138977
time_elpased: 5.167
#iterations: 302
currently lose_sum: 0.4123435914516449
time_elpased: 5.141
#iterations: 303
currently lose_sum: 0.3921242356300354
time_elpased: 5.076
#iterations: 304
currently lose_sum: 0.6593731045722961
time_elpased: 5.137
#iterations: 305
currently lose_sum: 0.2946093678474426
time_elpased: 5.089
#iterations: 306
currently lose_sum: 0.37246376276016235
time_elpased: 5.124
#iterations: 307
currently lose_sum: 0.35191085934638977
time_elpased: 5.225
#iterations: 308
currently lose_sum: 0.46526795625686646
time_elpased: 5.11
#iterations: 309
currently lose_sum: 0.34692057967185974
time_elpased: 5.128
#iterations: 310
currently lose_sum: 0.36721357703208923
time_elpased: 5.157
#iterations: 311
currently lose_sum: 0.3941584527492523
time_elpased: 5.074
#iterations: 312
currently lose_sum: 0.3291279673576355
time_elpased: 5.123
#iterations: 313
currently lose_sum: 0.44651055335998535
time_elpased: 5.132
#iterations: 314
currently lose_sum: 0.40456175804138184
time_elpased: 5.148
#iterations: 315
currently lose_sum: 0.38068193197250366
time_elpased: 5.084
#iterations: 316
currently lose_sum: 0.28174275159835815
time_elpased: 5.163
#iterations: 317
currently lose_sum: 0.4370585083961487
time_elpased: 5.165
#iterations: 318
currently lose_sum: 0.36734461784362793
time_elpased: 5.143
#iterations: 319
currently lose_sum: 0.2493598908185959
time_elpased: 5.124
start validation test
0.665384615385
0.673202614379
0.654383735705
0.663659793814
0.665484235252
27.634
#iterations: 320
currently lose_sum: 0.3777698874473572
time_elpased: 5.145
#iterations: 321
currently lose_sum: 0.40164074301719666
time_elpased: 5.131
#iterations: 322
currently lose_sum: 0.5144140124320984
time_elpased: 5.127
#iterations: 323
currently lose_sum: 0.271803617477417
time_elpased: 5.093
#iterations: 324
currently lose_sum: 0.31106117367744446
time_elpased: 5.162
#iterations: 325
currently lose_sum: 0.2980749309062958
time_elpased: 5.121
#iterations: 326
currently lose_sum: 0.28868427872657776
time_elpased: 5.154
#iterations: 327
currently lose_sum: 0.40088120102882385
time_elpased: 5.126
#iterations: 328
currently lose_sum: 0.3936257064342499
time_elpased: 5.106
#iterations: 329
currently lose_sum: 0.43319034576416016
time_elpased: 5.129
#iterations: 330
currently lose_sum: 0.2888298034667969
time_elpased: 5.096
#iterations: 331
currently lose_sum: 0.24920225143432617
time_elpased: 5.069
#iterations: 332
currently lose_sum: 0.2954159379005432
time_elpased: 5.121
#iterations: 333
currently lose_sum: 0.3763929605484009
time_elpased: 5.16
#iterations: 334
currently lose_sum: 0.4034256041049957
time_elpased: 5.183
#iterations: 335
currently lose_sum: 0.5142109990119934
time_elpased: 5.14
#iterations: 336
currently lose_sum: 0.2218746691942215
time_elpased: 5.081
#iterations: 337
currently lose_sum: 0.3772657513618469
time_elpased: 5.125
#iterations: 338
currently lose_sum: 0.32868823409080505
time_elpased: 5.179
#iterations: 339
currently lose_sum: 0.3174349367618561
time_elpased: 5.144
start validation test
0.650641025641
0.744939271255
0.467598475222
0.574551131928
0.652298590781
31.104
#iterations: 340
currently lose_sum: 0.18422669172286987
time_elpased: 5.059
#iterations: 341
currently lose_sum: 0.2944842278957367
time_elpased: 5.141
#iterations: 342
currently lose_sum: 0.3490851819515228
time_elpased: 5.182
#iterations: 343
currently lose_sum: 0.4212493896484375
time_elpased: 5.079
#iterations: 344
currently lose_sum: 0.5160785913467407
time_elpased: 5.096
#iterations: 345
currently lose_sum: 0.37214940786361694
time_elpased: 5.138
#iterations: 346
currently lose_sum: 0.3472524583339691
time_elpased: 5.16
#iterations: 347
currently lose_sum: 0.45671314001083374
time_elpased: 5.157
#iterations: 348
currently lose_sum: 0.3489803671836853
time_elpased: 5.112
#iterations: 349
currently lose_sum: 0.3093847334384918
time_elpased: 5.097
#iterations: 350
currently lose_sum: 0.46683555841445923
time_elpased: 5.101
#iterations: 351
currently lose_sum: 0.23406009376049042
time_elpased: 5.156
#iterations: 352
currently lose_sum: 0.479836642742157
time_elpased: 5.137
#iterations: 353
currently lose_sum: 0.2296457290649414
time_elpased: 5.126
#iterations: 354
currently lose_sum: 0.3871898949146271
time_elpased: 5.174
#iterations: 355
currently lose_sum: 0.5557443499565125
time_elpased: 5.092
#iterations: 356
currently lose_sum: 0.48470741510391235
time_elpased: 5.194
#iterations: 357
currently lose_sum: 0.3668988347053528
time_elpased: 5.198
#iterations: 358
currently lose_sum: nan
time_elpased: 5.481
train finish final lose is: nan
acc: 0.680
pre: 0.677
rec: 0.700
F1: 0.688
auc: 0.680
