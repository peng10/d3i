start to construct computing graph
graph construct over
3372
epochs start
#iterations: 0
currently lose_sum: 0.6747018098831177
time_elpased: 2.167
#iterations: 1
currently lose_sum: 0.6541587114334106
time_elpased: 2.15
#iterations: 2
currently lose_sum: 0.6526607275009155
time_elpased: 2.169
#iterations: 3
currently lose_sum: 0.5735630989074707
time_elpased: 2.184
#iterations: 4
currently lose_sum: 0.5127345323562622
time_elpased: 2.144
#iterations: 5
currently lose_sum: 0.4962683618068695
time_elpased: 2.124
#iterations: 6
currently lose_sum: 0.5348812341690063
time_elpased: 2.213
#iterations: 7
currently lose_sum: 0.5235027074813843
time_elpased: 2.165
#iterations: 8
currently lose_sum: 0.42472100257873535
time_elpased: 2.119
#iterations: 9
currently lose_sum: 0.4664630889892578
time_elpased: 2.152
#iterations: 10
currently lose_sum: 0.3354196846485138
time_elpased: 2.16
#iterations: 11
currently lose_sum: 0.4577654004096985
time_elpased: 2.137
#iterations: 12
currently lose_sum: 0.3335084915161133
time_elpased: 2.106
#iterations: 13
currently lose_sum: 0.5592778325080872
time_elpased: 2.132
#iterations: 14
currently lose_sum: 0.3880372941493988
time_elpased: 2.13
#iterations: 15
currently lose_sum: 0.3678281605243683
time_elpased: 2.128
#iterations: 16
currently lose_sum: 0.39475154876708984
time_elpased: 2.121
#iterations: 17
currently lose_sum: 0.3108300566673279
time_elpased: 2.124
#iterations: 18
currently lose_sum: 0.41663122177124023
time_elpased: 2.185
#iterations: 19
currently lose_sum: 0.5585892200469971
time_elpased: 2.133
start validation test
0.752678571429
0.823329558324
0.646222222222
0.724103585657
0.75315595416
28.961
#iterations: 20
currently lose_sum: 0.49420562386512756
time_elpased: 1.633
#iterations: 21
currently lose_sum: 0.4031097888946533
time_elpased: 1.63
#iterations: 22
currently lose_sum: 0.5421763062477112
time_elpased: 1.677
#iterations: 23
currently lose_sum: 0.46272411942481995
time_elpased: 1.639
#iterations: 24
currently lose_sum: 0.3974274694919586
time_elpased: 1.639
#iterations: 25
currently lose_sum: 0.34098249673843384
time_elpased: 1.68
#iterations: 26
currently lose_sum: 0.4609341621398926
time_elpased: 1.61
#iterations: 27
currently lose_sum: 0.2951359152793884
time_elpased: 1.665
#iterations: 28
currently lose_sum: 0.2623863220214844
time_elpased: 1.631
#iterations: 29
currently lose_sum: 0.2796247899532318
time_elpased: 1.683
#iterations: 30
currently lose_sum: 0.3486727774143219
time_elpased: 1.634
#iterations: 31
currently lose_sum: 0.4196940064430237
time_elpased: 1.668
#iterations: 32
currently lose_sum: 0.31259971857070923
time_elpased: 1.635
#iterations: 33
currently lose_sum: 0.30576109886169434
time_elpased: 1.61
#iterations: 34
currently lose_sum: 0.45527753233909607
time_elpased: 1.644
#iterations: 35
currently lose_sum: 0.24923892319202423
time_elpased: 1.644
#iterations: 36
currently lose_sum: 0.259761244058609
time_elpased: 1.624
#iterations: 37
currently lose_sum: 0.39231669902801514
time_elpased: 1.634
#iterations: 38
currently lose_sum: 0.4965677261352539
time_elpased: 1.645
#iterations: 39
currently lose_sum: 0.3550521731376648
time_elpased: 1.673
start validation test
0.74375
0.828367103695
0.617777777778
0.707739307536
0.744314897857
32.511
#iterations: 40
currently lose_sum: 0.25881609320640564
time_elpased: 1.658
#iterations: 41
currently lose_sum: 0.3866937756538391
time_elpased: 1.657
#iterations: 42
currently lose_sum: 0.30459171533584595
time_elpased: 1.658
#iterations: 43
currently lose_sum: 0.42675599455833435
time_elpased: 1.655
#iterations: 44
currently lose_sum: 0.29305973649024963
time_elpased: 1.623
#iterations: 45
currently lose_sum: 0.40089982748031616
time_elpased: 1.677
#iterations: 46
currently lose_sum: 0.3156212270259857
time_elpased: 1.655
#iterations: 47
currently lose_sum: 0.2985289692878723
time_elpased: 1.66
#iterations: 48
currently lose_sum: 0.4868840277194977
time_elpased: 1.654
#iterations: 49
currently lose_sum: 0.4265015721321106
time_elpased: 1.665
#iterations: 50
currently lose_sum: 0.3420698344707489
time_elpased: 1.611
#iterations: 51
currently lose_sum: 0.2715272903442383
time_elpased: 1.605
#iterations: 52
currently lose_sum: 0.37502723932266235
time_elpased: 1.613
#iterations: 53
currently lose_sum: 0.3690982758998871
time_elpased: 1.631
#iterations: 54
currently lose_sum: 0.26979485154151917
time_elpased: 1.635
#iterations: 55
currently lose_sum: 0.37367385625839233
time_elpased: 1.618
#iterations: 56
currently lose_sum: 0.3045130670070648
time_elpased: 1.631
#iterations: 57
currently lose_sum: 0.31961140036582947
time_elpased: 1.592
#iterations: 58
currently lose_sum: 0.26624077558517456
time_elpased: 1.665
#iterations: 59
currently lose_sum: 0.31427550315856934
time_elpased: 1.617
start validation test
0.758035714286
0.848267622461
0.631111111111
0.72375127421
0.75860488291
33.888
#iterations: 60
currently lose_sum: 0.3822544515132904
time_elpased: 1.657
#iterations: 61
currently lose_sum: 0.445453941822052
time_elpased: 1.635
#iterations: 62
currently lose_sum: 0.4625498652458191
time_elpased: 1.611
#iterations: 63
currently lose_sum: 0.3668793737888336
time_elpased: 1.658
#iterations: 64
currently lose_sum: 0.3038506805896759
time_elpased: 1.636
#iterations: 65
currently lose_sum: 0.24094268679618835
time_elpased: 1.642
#iterations: 66
currently lose_sum: 0.39642488956451416
time_elpased: 1.622
#iterations: 67
currently lose_sum: 0.3359920084476471
time_elpased: 1.648
#iterations: 68
currently lose_sum: 0.2869650423526764
time_elpased: 1.676
#iterations: 69
currently lose_sum: 0.1509721726179123
time_elpased: 1.618
#iterations: 70
currently lose_sum: 0.28478798270225525
time_elpased: 1.634
#iterations: 71
currently lose_sum: 0.39799198508262634
time_elpased: 1.629
#iterations: 72
currently lose_sum: 0.3004908263683319
time_elpased: 1.647
#iterations: 73
currently lose_sum: 0.24434328079223633
time_elpased: 1.613
#iterations: 74
currently lose_sum: 0.32112443447113037
time_elpased: 1.638
#iterations: 75
currently lose_sum: 0.2816011309623718
time_elpased: 1.659
#iterations: 76
currently lose_sum: 0.3614555299282074
time_elpased: 1.636
#iterations: 77
currently lose_sum: 0.3097418248653412
time_elpased: 1.652
#iterations: 78
currently lose_sum: 0.3335474133491516
time_elpased: 1.649
#iterations: 79
currently lose_sum: 0.3007955849170685
time_elpased: 1.619
start validation test
0.761607142857
0.833145434047
0.656888888889
0.734592445328
0.76207673144
34.457
#iterations: 80
currently lose_sum: 0.19741186499595642
time_elpased: 1.618
#iterations: 81
currently lose_sum: 0.2600182890892029
time_elpased: 1.629
#iterations: 82
currently lose_sum: 0.2890856862068176
time_elpased: 1.643
#iterations: 83
currently lose_sum: 0.2566296458244324
time_elpased: 1.643
#iterations: 84
currently lose_sum: 0.15578940510749817
time_elpased: 1.655
#iterations: 85
currently lose_sum: 0.28372806310653687
time_elpased: 1.654
#iterations: 86
currently lose_sum: 0.28487491607666016
time_elpased: 1.675
#iterations: 87
currently lose_sum: 0.16451668739318848
time_elpased: 1.705
#iterations: 88
currently lose_sum: 0.39596349000930786
time_elpased: 1.663
#iterations: 89
currently lose_sum: 0.44953054189682007
time_elpased: 1.649
#iterations: 90
currently lose_sum: 0.19600021839141846
time_elpased: 1.632
#iterations: 91
currently lose_sum: 0.2115367352962494
time_elpased: 1.633
#iterations: 92
currently lose_sum: 0.3359712064266205
time_elpased: 1.602
#iterations: 93
currently lose_sum: 0.3208363652229309
time_elpased: 1.628
#iterations: 94
currently lose_sum: 0.20318880677223206
time_elpased: 1.711
#iterations: 95
currently lose_sum: 0.26576265692710876
time_elpased: 1.618
#iterations: 96
currently lose_sum: 0.366119921207428
time_elpased: 1.614
#iterations: 97
currently lose_sum: 0.441957950592041
time_elpased: 1.691
#iterations: 98
currently lose_sum: 0.21286439895629883
time_elpased: 1.634
#iterations: 99
currently lose_sum: 0.16021540760993958
time_elpased: 1.624
start validation test
0.748214285714
0.846724351051
0.608888888889
0.708376421923
0.748839063279
38.309
#iterations: 100
currently lose_sum: 0.17475156486034393
time_elpased: 1.661
#iterations: 101
currently lose_sum: 0.25074875354766846
time_elpased: 1.665
#iterations: 102
currently lose_sum: 0.20802459120750427
time_elpased: 1.627
#iterations: 103
currently lose_sum: 0.329853892326355
time_elpased: 1.653
#iterations: 104
currently lose_sum: 0.2906937003135681
time_elpased: 1.63
#iterations: 105
currently lose_sum: 0.29134589433670044
time_elpased: 1.602
#iterations: 106
currently lose_sum: 0.23901326954364777
time_elpased: 1.632
#iterations: 107
currently lose_sum: 0.24516257643699646
time_elpased: 1.65
#iterations: 108
currently lose_sum: 0.3302927315235138
time_elpased: 1.613
#iterations: 109
currently lose_sum: 0.15202191472053528
time_elpased: 1.639
#iterations: 110
currently lose_sum: 0.40139150619506836
time_elpased: 1.675
#iterations: 111
currently lose_sum: 0.15854409337043762
time_elpased: 1.652
#iterations: 112
currently lose_sum: 0.3006410300731659
time_elpased: 1.653
#iterations: 113
currently lose_sum: 0.22901959717273712
time_elpased: 1.619
#iterations: 114
currently lose_sum: 0.276111364364624
time_elpased: 1.648
#iterations: 115
currently lose_sum: 0.12415964901447296
time_elpased: 1.596
#iterations: 116
currently lose_sum: 0.10173781961202621
time_elpased: 1.632
#iterations: 117
currently lose_sum: 0.3499176502227783
time_elpased: 1.604
#iterations: 118
currently lose_sum: 0.33733510971069336
time_elpased: 1.64
#iterations: 119
currently lose_sum: 0.329778790473938
time_elpased: 1.641
start validation test
0.745982142857
0.855498721228
0.594666666667
0.701625589932
0.746660687593
39.948
#iterations: 120
currently lose_sum: 0.2559271454811096
time_elpased: 1.633
#iterations: 121
currently lose_sum: 0.1689276397228241
time_elpased: 1.654
#iterations: 122
currently lose_sum: 0.29259854555130005
time_elpased: 1.644
#iterations: 123
currently lose_sum: 0.2726749777793884
time_elpased: 1.709
#iterations: 124
currently lose_sum: 0.3343169093132019
time_elpased: 1.637
#iterations: 125
currently lose_sum: 0.5777010321617126
time_elpased: 1.634
#iterations: 126
currently lose_sum: 0.19370529055595398
time_elpased: 1.658
#iterations: 127
currently lose_sum: 0.1560196727514267
time_elpased: 1.621
#iterations: 128
currently lose_sum: 0.11378075182437897
time_elpased: 1.684
#iterations: 129
currently lose_sum: 0.2959635555744171
time_elpased: 1.664
#iterations: 130
currently lose_sum: 0.37794744968414307
time_elpased: 1.631
#iterations: 131
currently lose_sum: 0.12518410384655
time_elpased: 1.654
#iterations: 132
currently lose_sum: 0.17224766314029694
time_elpased: 1.646
#iterations: 133
currently lose_sum: 0.20601458847522736
time_elpased: 1.633
#iterations: 134
currently lose_sum: 0.14725510776042938
time_elpased: 1.649
#iterations: 135
currently lose_sum: 0.2124411165714264
time_elpased: 1.623
#iterations: 136
currently lose_sum: 0.18916788697242737
time_elpased: 1.661
#iterations: 137
currently lose_sum: 0.19755156338214874
time_elpased: 1.644
#iterations: 138
currently lose_sum: 0.30664271116256714
time_elpased: 1.639
#iterations: 139
currently lose_sum: 0.1712886244058609
time_elpased: 1.636
start validation test
0.755357142857
0.843861740167
0.629333333333
0.720977596741
0.755922272048
39.132
#iterations: 140
currently lose_sum: 0.2028498351573944
time_elpased: 1.695
#iterations: 141
currently lose_sum: 0.2681162357330322
time_elpased: 1.68
#iterations: 142
currently lose_sum: 0.22252421081066132
time_elpased: 1.667
#iterations: 143
currently lose_sum: 0.4321659505367279
time_elpased: 1.65
#iterations: 144
currently lose_sum: 0.15628552436828613
time_elpased: 1.654
#iterations: 145
currently lose_sum: 0.09797745943069458
time_elpased: 1.614
#iterations: 146
currently lose_sum: 0.21101918816566467
time_elpased: 1.601
#iterations: 147
currently lose_sum: 0.21843580901622772
time_elpased: 1.643
#iterations: 148
currently lose_sum: 0.37321749329566956
time_elpased: 1.679
#iterations: 149
currently lose_sum: 0.1512363851070404
time_elpased: 1.624
#iterations: 150
currently lose_sum: 0.1666623204946518
time_elpased: 1.62
#iterations: 151
currently lose_sum: 0.16721615195274353
time_elpased: 1.647
#iterations: 152
currently lose_sum: 0.14842592179775238
time_elpased: 1.641
#iterations: 153
currently lose_sum: 0.37005454301834106
time_elpased: 1.609
#iterations: 154
currently lose_sum: 0.22970712184906006
time_elpased: 1.635
#iterations: 155
currently lose_sum: 0.142188161611557
time_elpased: 1.64
#iterations: 156
currently lose_sum: 0.5095440149307251
time_elpased: 1.669
#iterations: 157
currently lose_sum: 0.17104145884513855
time_elpased: 1.647
#iterations: 158
currently lose_sum: 0.31902721524238586
time_elpased: 1.652
#iterations: 159
currently lose_sum: 0.26185595989227295
time_elpased: 1.629
start validation test
0.754464285714
0.849331713244
0.621333333333
0.717659137577
0.755061285501
42.572
#iterations: 160
currently lose_sum: 0.1964249312877655
time_elpased: 1.669
#iterations: 161
currently lose_sum: 0.107601597905159
time_elpased: 1.656
#iterations: 162
currently lose_sum: 0.32614555954933167
time_elpased: 1.664
#iterations: 163
currently lose_sum: 0.21330198645591736
time_elpased: 1.644
#iterations: 164
currently lose_sum: 0.2771875262260437
time_elpased: 1.667
#iterations: 165
currently lose_sum: 0.11187569797039032
time_elpased: 1.641
#iterations: 166
currently lose_sum: 0.08323589712381363
time_elpased: 1.619
#iterations: 167
currently lose_sum: 0.17668764293193817
time_elpased: 1.677
#iterations: 168
currently lose_sum: 0.15887513756752014
time_elpased: 1.635
#iterations: 169
currently lose_sum: 0.20637425780296326
time_elpased: 1.624
#iterations: 170
currently lose_sum: 0.20692376792430878
time_elpased: 1.662
#iterations: 171
currently lose_sum: 0.24727609753608704
time_elpased: 1.653
#iterations: 172
currently lose_sum: 0.11504755914211273
time_elpased: 1.618
#iterations: 173
currently lose_sum: 0.499762624502182
time_elpased: 1.635
#iterations: 174
currently lose_sum: 0.1415644884109497
time_elpased: 1.693
#iterations: 175
currently lose_sum: 0.15682396292686462
time_elpased: 1.651
#iterations: 176
currently lose_sum: 0.14095811545848846
time_elpased: 1.618
#iterations: 177
currently lose_sum: 0.13738314807415009
time_elpased: 1.643
#iterations: 178
currently lose_sum: 0.2441767156124115
time_elpased: 1.661
#iterations: 179
currently lose_sum: 0.14912119507789612
time_elpased: 1.621
start validation test
0.74375
0.859191655802
0.585777777778
0.696617336152
0.744458395615
45.291
#iterations: 180
currently lose_sum: 0.22510859370231628
time_elpased: 1.646
#iterations: 181
currently lose_sum: 0.18963466584682465
time_elpased: 1.661
#iterations: 182
currently lose_sum: 0.18776002526283264
time_elpased: 1.612
#iterations: 183
currently lose_sum: 0.13597173988819122
time_elpased: 1.658
#iterations: 184
currently lose_sum: 0.4190407693386078
time_elpased: 1.631
#iterations: 185
currently lose_sum: 0.3144819438457489
time_elpased: 1.678
#iterations: 186
currently lose_sum: 0.20684179663658142
time_elpased: 1.66
#iterations: 187
currently lose_sum: 0.1777745485305786
time_elpased: 1.667
#iterations: 188
currently lose_sum: 0.33509552478790283
time_elpased: 1.637
#iterations: 189
currently lose_sum: 0.25340646505355835
time_elpased: 1.643
#iterations: 190
currently lose_sum: 0.12199368327856064
time_elpased: 1.624
#iterations: 191
currently lose_sum: 0.18771199882030487
time_elpased: 1.646
#iterations: 192
currently lose_sum: 0.20340919494628906
time_elpased: 1.667
#iterations: 193
currently lose_sum: 0.1508980691432953
time_elpased: 1.634
#iterations: 194
currently lose_sum: 0.233510822057724
time_elpased: 1.652
#iterations: 195
currently lose_sum: 0.27340957522392273
time_elpased: 1.623
#iterations: 196
currently lose_sum: 0.14826491475105286
time_elpased: 1.673
#iterations: 197
currently lose_sum: 0.10070301592350006
time_elpased: 1.679
#iterations: 198
currently lose_sum: 0.226057767868042
time_elpased: 1.698
#iterations: 199
currently lose_sum: 0.14169062674045563
time_elpased: 1.664
start validation test
0.763839285714
0.854761904762
0.638222222222
0.730788804071
0.764402590932
43.471
#iterations: 200
currently lose_sum: 0.10753387212753296
time_elpased: 1.639
#iterations: 201
currently lose_sum: 0.24539831280708313
time_elpased: 1.671
#iterations: 202
currently lose_sum: 0.10061542689800262
time_elpased: 1.632
#iterations: 203
currently lose_sum: 0.24835416674613953
time_elpased: 1.669
#iterations: 204
currently lose_sum: 0.22942039370536804
time_elpased: 1.662
#iterations: 205
currently lose_sum: 0.27693191170692444
time_elpased: 1.681
#iterations: 206
currently lose_sum: 0.26864081621170044
time_elpased: 1.64
#iterations: 207
currently lose_sum: 0.22077421844005585
time_elpased: 1.634
#iterations: 208
currently lose_sum: 0.16102096438407898
time_elpased: 1.661
#iterations: 209
currently lose_sum: 0.1406438797712326
time_elpased: 1.644
#iterations: 210
currently lose_sum: 0.27173304557800293
time_elpased: 1.653
#iterations: 211
currently lose_sum: 0.18991591036319733
time_elpased: 1.65
#iterations: 212
currently lose_sum: 0.2743043303489685
time_elpased: 1.636
#iterations: 213
currently lose_sum: 0.3458084464073181
time_elpased: 1.644
#iterations: 214
currently lose_sum: 0.2326190024614334
time_elpased: 1.675
#iterations: 215
currently lose_sum: 0.15438634157180786
time_elpased: 1.708
#iterations: 216
currently lose_sum: 0.26689982414245605
time_elpased: 1.611
#iterations: 217
currently lose_sum: 0.22926506400108337
time_elpased: 1.623
#iterations: 218
currently lose_sum: 0.21906034648418427
time_elpased: 1.611
#iterations: 219
currently lose_sum: 0.3253644108772278
time_elpased: 1.647
start validation test
0.729464285714
0.856946354883
0.553777777778
0.672786177106
0.730252117588
50.813
#iterations: 220
currently lose_sum: 0.10518260300159454
time_elpased: 1.657
#iterations: 221
currently lose_sum: 0.3020452558994293
time_elpased: 1.651
#iterations: 222
currently lose_sum: 0.17042967677116394
time_elpased: 1.605
#iterations: 223
currently lose_sum: 0.27381956577301025
time_elpased: 1.611
#iterations: 224
currently lose_sum: 0.2176465541124344
time_elpased: 1.63
#iterations: 225
currently lose_sum: 0.1684093326330185
time_elpased: 1.635
#iterations: 226
currently lose_sum: 0.286986768245697
time_elpased: 1.652
#iterations: 227
currently lose_sum: 0.160093754529953
time_elpased: 1.667
#iterations: 228
currently lose_sum: 0.2308669090270996
time_elpased: 1.62
#iterations: 229
currently lose_sum: 0.16719123721122742
time_elpased: 1.633
#iterations: 230
currently lose_sum: 0.2841522991657257
time_elpased: 1.617
#iterations: 231
currently lose_sum: 0.12351886928081512
time_elpased: 1.594
#iterations: 232
currently lose_sum: 0.14799384772777557
time_elpased: 1.657
#iterations: 233
currently lose_sum: 0.1386457234621048
time_elpased: 1.653
#iterations: 234
currently lose_sum: 0.16599330306053162
time_elpased: 1.634
#iterations: 235
currently lose_sum: 0.3388616740703583
time_elpased: 1.634
#iterations: 236
currently lose_sum: 0.4047105312347412
time_elpased: 1.635
#iterations: 237
currently lose_sum: 0.19286811351776123
time_elpased: 1.65
#iterations: 238
currently lose_sum: 0.19476696848869324
time_elpased: 1.617
#iterations: 239
currently lose_sum: 0.20769238471984863
time_elpased: 1.668
start validation test
0.746875
0.856777493606
0.595555555556
0.70267435763
0.747553562531
48.394
#iterations: 240
currently lose_sum: 0.18277119100093842
time_elpased: 1.63
#iterations: 241
currently lose_sum: 0.3627094626426697
time_elpased: 1.635
#iterations: 242
currently lose_sum: 0.24308200180530548
time_elpased: 1.636
#iterations: 243
currently lose_sum: 0.12461526691913605
time_elpased: 1.621
#iterations: 244
currently lose_sum: 0.44635477662086487
time_elpased: 1.647
#iterations: 245
currently lose_sum: 0.08865711092948914
time_elpased: 1.668
#iterations: 246
currently lose_sum: 0.08989670127630234
time_elpased: 1.656
#iterations: 247
currently lose_sum: 0.17539456486701965
time_elpased: 1.637
#iterations: 248
currently lose_sum: 0.2056078016757965
time_elpased: 1.647
#iterations: 249
currently lose_sum: 0.16904403269290924
time_elpased: 1.648
#iterations: 250
currently lose_sum: 0.3118878901004791
time_elpased: 1.648
#iterations: 251
currently lose_sum: 0.21150359511375427
time_elpased: 1.652
#iterations: 252
currently lose_sum: 0.31759244203567505
time_elpased: 1.628
#iterations: 253
currently lose_sum: 0.17932097613811493
time_elpased: 1.651
#iterations: 254
currently lose_sum: 0.2066231220960617
time_elpased: 1.619
#iterations: 255
currently lose_sum: 0.2604915201663971
time_elpased: 1.62
#iterations: 256
currently lose_sum: 0.08770318329334259
time_elpased: 1.624
#iterations: 257
currently lose_sum: 0.34832364320755005
time_elpased: 1.642
#iterations: 258
currently lose_sum: 0.07888931781053543
time_elpased: 1.631
#iterations: 259
currently lose_sum: 0.13558779656887054
time_elpased: 1.634
start validation test
0.737053571429
0.871191135734
0.559111111111
0.681104493774
0.737851519681
53.735
#iterations: 260
currently lose_sum: 0.1344565749168396
time_elpased: 1.647
#iterations: 261
currently lose_sum: 0.15418854355812073
time_elpased: 1.619
#iterations: 262
currently lose_sum: 0.24714091420173645
time_elpased: 1.699
#iterations: 263
currently lose_sum: 0.21589632332324982
time_elpased: 1.65
#iterations: 264
currently lose_sum: 0.18868207931518555
time_elpased: 1.635
#iterations: 265
currently lose_sum: 0.15667679905891418
time_elpased: 1.66
#iterations: 266
currently lose_sum: 0.24628958106040955
time_elpased: 1.668
#iterations: 267
currently lose_sum: 0.16582421958446503
time_elpased: 1.631
#iterations: 268
currently lose_sum: 0.3401663899421692
time_elpased: 1.646
#iterations: 269
currently lose_sum: 0.20638680458068848
time_elpased: 1.647
#iterations: 270
currently lose_sum: 0.12221942096948624
time_elpased: 1.635
#iterations: 271
currently lose_sum: 0.15887536108493805
time_elpased: 1.644
#iterations: 272
currently lose_sum: 0.14964759349822998
time_elpased: 1.652
#iterations: 273
currently lose_sum: 0.3378578722476959
time_elpased: 1.647
#iterations: 274
currently lose_sum: 0.21978580951690674
time_elpased: 1.663
#iterations: 275
currently lose_sum: 0.11737015098333359
time_elpased: 1.619
#iterations: 276
currently lose_sum: 0.29568538069725037
time_elpased: 1.64
#iterations: 277
currently lose_sum: 0.29231613874435425
time_elpased: 1.644
#iterations: 278
currently lose_sum: 0.16422750055789948
time_elpased: 1.663
#iterations: 279
currently lose_sum: 0.08186829090118408
time_elpased: 1.639
start validation test
0.741964285714
0.875171467764
0.567111111111
0.688241639698
0.742748380668
52.923
#iterations: 280
currently lose_sum: 0.15600767731666565
time_elpased: 1.635
#iterations: 281
currently lose_sum: 0.1306956708431244
time_elpased: 1.669
#iterations: 282
currently lose_sum: 0.14047905802726746
time_elpased: 1.65
#iterations: 283
currently lose_sum: 0.2203684151172638
time_elpased: 1.665
#iterations: 284
currently lose_sum: 0.21744957566261292
time_elpased: 1.645
#iterations: 285
currently lose_sum: 0.20169267058372498
time_elpased: 1.633
#iterations: 286
currently lose_sum: 0.1809011995792389
time_elpased: 1.635
#iterations: 287
currently lose_sum: 0.13777853548526764
time_elpased: 1.666
#iterations: 288
currently lose_sum: 0.3764222264289856
time_elpased: 1.642
#iterations: 289
currently lose_sum: 0.14791862666606903
time_elpased: 1.651
#iterations: 290
currently lose_sum: 0.22592711448669434
time_elpased: 1.609
#iterations: 291
currently lose_sum: 0.23009093105793
time_elpased: 1.638
#iterations: 292
currently lose_sum: 0.07467466592788696
time_elpased: 1.645
#iterations: 293
currently lose_sum: 0.25069114565849304
time_elpased: 1.647
#iterations: 294
currently lose_sum: 0.2022409439086914
time_elpased: 1.693
#iterations: 295
currently lose_sum: 0.2875484824180603
time_elpased: 1.631
#iterations: 296
currently lose_sum: 0.1832500398159027
time_elpased: 1.684
#iterations: 297
currently lose_sum: 0.14609840512275696
time_elpased: 1.64
#iterations: 298
currently lose_sum: 0.16057929396629333
time_elpased: 1.675
#iterations: 299
currently lose_sum: 0.3441251814365387
time_elpased: 1.663
start validation test
0.733928571429
0.863823933975
0.558222222222
0.67818574514
0.734716492277
57.070
#iterations: 300
currently lose_sum: 0.2818582057952881
time_elpased: 1.656
#iterations: 301
currently lose_sum: 0.23226657509803772
time_elpased: 1.601
#iterations: 302
currently lose_sum: 0.20276100933551788
time_elpased: 1.66
#iterations: 303
currently lose_sum: 0.10958719253540039
time_elpased: 1.663
#iterations: 304
currently lose_sum: 0.19701418280601501
time_elpased: 1.609
#iterations: 305
currently lose_sum: 0.14693908393383026
time_elpased: 1.637
#iterations: 306
currently lose_sum: 0.20365023612976074
time_elpased: 1.641
#iterations: 307
currently lose_sum: 0.22779273986816406
time_elpased: 1.645
#iterations: 308
currently lose_sum: 0.20129886269569397
time_elpased: 1.632
#iterations: 309
currently lose_sum: 0.08347951620817184
time_elpased: 1.633
#iterations: 310
currently lose_sum: 0.11790625751018524
time_elpased: 1.65
#iterations: 311
currently lose_sum: 0.22495754063129425
time_elpased: 1.641
#iterations: 312
currently lose_sum: 0.09007550776004791
time_elpased: 1.62
#iterations: 313
currently lose_sum: 0.2726440131664276
time_elpased: 1.641
#iterations: 314
currently lose_sum: 0.15254397690296173
time_elpased: 1.608
#iterations: 315
currently lose_sum: 0.159230574965477
time_elpased: 1.637
#iterations: 316
currently lose_sum: 0.2706083655357361
time_elpased: 1.669
#iterations: 317
currently lose_sum: 0.08432752639055252
time_elpased: 1.642
#iterations: 318
currently lose_sum: 0.3921261429786682
time_elpased: 1.637
#iterations: 319
currently lose_sum: 0.14038319885730743
time_elpased: 1.618
start validation test
0.735714285714
0.868603042877
0.558222222222
0.679653679654
0.73651021425
56.870
#iterations: 320
currently lose_sum: 0.15638700127601624
time_elpased: 1.657
#iterations: 321
currently lose_sum: 0.2795354723930359
time_elpased: 1.661
#iterations: 322
currently lose_sum: 0.07429705560207367
time_elpased: 1.669
#iterations: 323
currently lose_sum: 0.2278168499469757
time_elpased: 1.657
#iterations: 324
currently lose_sum: 0.0921955555677414
time_elpased: 1.612
#iterations: 325
currently lose_sum: 0.2579634189605713
time_elpased: 1.617
#iterations: 326
currently lose_sum: 0.11123766750097275
time_elpased: 1.669
#iterations: 327
currently lose_sum: 0.3798375725746155
time_elpased: 1.618
#iterations: 328
currently lose_sum: 0.27638697624206543
time_elpased: 1.622
#iterations: 329
currently lose_sum: 0.1537608653306961
time_elpased: 1.61
#iterations: 330
currently lose_sum: 0.08209897577762604
time_elpased: 1.635
#iterations: 331
currently lose_sum: 0.06282730400562286
time_elpased: 1.651
#iterations: 332
currently lose_sum: 0.24194243550300598
time_elpased: 1.662
#iterations: 333
currently lose_sum: 0.3142329156398773
time_elpased: 1.638
#iterations: 334
currently lose_sum: 0.10665898025035858
time_elpased: 1.678
#iterations: 335
currently lose_sum: 0.11720515787601471
time_elpased: 1.656
#iterations: 336
currently lose_sum: 0.18480317294597626
time_elpased: 1.653
#iterations: 337
currently lose_sum: 0.06232232600450516
time_elpased: 1.615
#iterations: 338
currently lose_sum: 0.12112077325582504
time_elpased: 1.659
#iterations: 339
currently lose_sum: 0.33026158809661865
time_elpased: 1.631
start validation test
0.734821428571
0.86926286509
0.555555555556
0.677874186551
0.73562531141
58.589
#iterations: 340
currently lose_sum: 0.1098434180021286
time_elpased: 1.632
#iterations: 341
currently lose_sum: 0.17208653688430786
time_elpased: 1.693
#iterations: 342
currently lose_sum: 0.16554954648017883
time_elpased: 1.668
#iterations: 343
currently lose_sum: 0.09366932511329651
time_elpased: 1.643
#iterations: 344
currently lose_sum: 0.2519119381904602
time_elpased: 1.666
#iterations: 345
currently lose_sum: 0.1069631353020668
time_elpased: 1.663
#iterations: 346
currently lose_sum: 0.13944879174232483
time_elpased: 1.611
#iterations: 347
currently lose_sum: 0.14179809391498566
time_elpased: 1.671
#iterations: 348
currently lose_sum: 0.09626326709985733
time_elpased: 1.654
#iterations: 349
currently lose_sum: 0.19090677797794342
time_elpased: 1.638
#iterations: 350
currently lose_sum: 0.15756353735923767
time_elpased: 1.642
#iterations: 351
currently lose_sum: 0.19053570926189423
time_elpased: 1.642
#iterations: 352
currently lose_sum: 0.1675979346036911
time_elpased: 1.633
#iterations: 353
currently lose_sum: 0.16628198325634003
time_elpased: 1.646
#iterations: 354
currently lose_sum: 0.17817072570323944
time_elpased: 1.666
#iterations: 355
currently lose_sum: 0.3051595091819763
time_elpased: 1.671
#iterations: 356
currently lose_sum: 0.14854371547698975
time_elpased: 1.669
#iterations: 357
currently lose_sum: 0.14653737843036652
time_elpased: 1.651
#iterations: 358
currently lose_sum: 0.12746044993400574
time_elpased: 1.631
#iterations: 359
currently lose_sum: 0.28892937302589417
time_elpased: 1.657
start validation test
0.736160714286
0.875
0.553777777778
0.678279804028
0.736978574988
60.613
#iterations: 360
currently lose_sum: 0.0923212394118309
time_elpased: 1.653
#iterations: 361
currently lose_sum: 0.2313200682401657
time_elpased: 1.647
#iterations: 362
currently lose_sum: 0.2189372330904007
time_elpased: 1.684
#iterations: 363
currently lose_sum: 0.15018662810325623
time_elpased: 1.639
#iterations: 364
currently lose_sum: 0.223903089761734
time_elpased: 1.64
#iterations: 365
currently lose_sum: 0.1615477353334427
time_elpased: 1.605
#iterations: 366
currently lose_sum: 0.1748153418302536
time_elpased: 1.661
#iterations: 367
currently lose_sum: 0.21248884499073029
time_elpased: 1.685
#iterations: 368
currently lose_sum: 0.13925273716449738
time_elpased: 1.666
#iterations: 369
currently lose_sum: 0.18704862892627716
time_elpased: 1.652
#iterations: 370
currently lose_sum: 0.2713656425476074
time_elpased: 1.643
#iterations: 371
currently lose_sum: 0.20722365379333496
time_elpased: 1.634
#iterations: 372
currently lose_sum: 0.1631711721420288
time_elpased: 1.607
#iterations: 373
currently lose_sum: 0.2077186554670334
time_elpased: 1.654
#iterations: 374
currently lose_sum: 0.21272078156471252
time_elpased: 1.711
#iterations: 375
currently lose_sum: 0.07740338146686554
time_elpased: 1.644
#iterations: 376
currently lose_sum: 0.4087693691253662
time_elpased: 1.64
#iterations: 377
currently lose_sum: 0.13620854914188385
time_elpased: 1.649
#iterations: 378
currently lose_sum: 0.20365998148918152
time_elpased: 1.647
#iterations: 379
currently lose_sum: 0.16955089569091797
time_elpased: 1.653
start validation test
0.739285714286
0.871056241427
0.564444444444
0.685005393743
0.740069755855
61.228
#iterations: 380
currently lose_sum: 0.2052605152130127
time_elpased: 1.655
#iterations: 381
currently lose_sum: 0.28873711824417114
time_elpased: 1.605
#iterations: 382
currently lose_sum: 0.16991885006427765
time_elpased: 1.636
#iterations: 383
currently lose_sum: 0.2169070541858673
time_elpased: 1.65
#iterations: 384
currently lose_sum: 0.19665104150772095
time_elpased: 1.652
#iterations: 385
currently lose_sum: 0.15392987430095673
time_elpased: 1.66
#iterations: 386
currently lose_sum: 0.19801241159439087
time_elpased: 1.657
#iterations: 387
currently lose_sum: 0.22670674324035645
time_elpased: 1.644
#iterations: 388
currently lose_sum: 0.15989361703395844
time_elpased: 1.668
#iterations: 389
currently lose_sum: 0.21157196164131165
time_elpased: 1.613
#iterations: 390
currently lose_sum: 0.1544077843427658
time_elpased: 1.625
#iterations: 391
currently lose_sum: 0.0798405185341835
time_elpased: 1.66
#iterations: 392
currently lose_sum: 0.20823845267295837
time_elpased: 1.683
#iterations: 393
currently lose_sum: 0.20273585617542267
time_elpased: 1.627
#iterations: 394
currently lose_sum: 0.11414261162281036
time_elpased: 1.672
#iterations: 395
currently lose_sum: 0.13288481533527374
time_elpased: 1.647
#iterations: 396
currently lose_sum: 0.09573988616466522
time_elpased: 1.625
#iterations: 397
currently lose_sum: 0.148905947804451
time_elpased: 1.625
#iterations: 398
currently lose_sum: 0.11336784064769745
time_elpased: 1.658
#iterations: 399
currently lose_sum: 0.1367635428905487
time_elpased: 1.665
start validation test
0.736607142857
0.865937072503
0.562666666667
0.682112068966
0.737387144993
60.710
#iterations: 400
currently lose_sum: 0.14680162072181702
time_elpased: 1.661
#iterations: 401
currently lose_sum: 0.26316526532173157
time_elpased: 1.625
#iterations: 402
currently lose_sum: 0.1828470379114151
time_elpased: 1.651
#iterations: 403
currently lose_sum: 0.24882838129997253
time_elpased: 1.608
#iterations: 404
currently lose_sum: 0.11820266395807266
time_elpased: 1.605
#iterations: 405
currently lose_sum: 0.2584245800971985
time_elpased: 1.634
#iterations: 406
currently lose_sum: 0.1664070338010788
time_elpased: 1.614
#iterations: 407
currently lose_sum: 0.03161447495222092
time_elpased: 1.635
#iterations: 408
currently lose_sum: 0.11026990413665771
time_elpased: 1.672
#iterations: 409
currently lose_sum: 0.1546643078327179
time_elpased: 1.653
#iterations: 410
currently lose_sum: 0.14783985912799835
time_elpased: 1.632
#iterations: 411
currently lose_sum: 0.19073067605495453
time_elpased: 1.641
#iterations: 412
currently lose_sum: 0.09580892324447632
time_elpased: 1.634
#iterations: 413
currently lose_sum: 0.20003601908683777
time_elpased: 1.68
#iterations: 414
currently lose_sum: 0.22340433299541473
time_elpased: 1.644
#iterations: 415
currently lose_sum: 0.10705326497554779
time_elpased: 1.646
#iterations: 416
currently lose_sum: 0.08763737976551056
time_elpased: 1.633
#iterations: 417
currently lose_sum: 0.2444358617067337
time_elpased: 1.608
#iterations: 418
currently lose_sum: 0.18194620311260223
time_elpased: 1.653
#iterations: 419
currently lose_sum: 0.08791756629943848
time_elpased: 1.621
start validation test
0.732142857143
0.883211678832
0.537777777778
0.668508287293
0.733014449427
64.449
#iterations: 420
currently lose_sum: 0.20072698593139648
time_elpased: 1.643
#iterations: 421
currently lose_sum: 0.14771516621112823
time_elpased: 1.631
#iterations: 422
currently lose_sum: 0.17093732953071594
time_elpased: 1.639
#iterations: 423
currently lose_sum: 0.12064089626073837
time_elpased: 1.641
#iterations: 424
currently lose_sum: 0.16688646376132965
time_elpased: 1.669
#iterations: 425
currently lose_sum: 0.216671422123909
time_elpased: 1.624
#iterations: 426
currently lose_sum: 0.058862388134002686
time_elpased: 1.644
#iterations: 427
currently lose_sum: 0.14558474719524384
time_elpased: 1.611
#iterations: 428
currently lose_sum: 0.1761254221200943
time_elpased: 1.629
#iterations: 429
currently lose_sum: 0.1656198799610138
time_elpased: 1.591
#iterations: 430
currently lose_sum: 0.19846193492412567
time_elpased: 1.663
#iterations: 431
currently lose_sum: 0.11851546913385391
time_elpased: 1.614
#iterations: 432
currently lose_sum: 0.16458983719348907
time_elpased: 1.62
#iterations: 433
currently lose_sum: 0.16959556937217712
time_elpased: 1.671
#iterations: 434
currently lose_sum: 0.18082815408706665
time_elpased: 1.622
#iterations: 435
currently lose_sum: 0.12168025970458984
time_elpased: 1.691
#iterations: 436
currently lose_sum: 0.08939816802740097
time_elpased: 1.652
#iterations: 437
currently lose_sum: 0.12488844245672226
time_elpased: 1.639
#iterations: 438
currently lose_sum: 0.2159692347049713
time_elpased: 1.628
#iterations: 439
currently lose_sum: 0.11825772374868393
time_elpased: 1.635
start validation test
0.740178571429
0.866396761134
0.570666666667
0.688102893891
0.740938714499
60.509
#iterations: 440
currently lose_sum: 0.188228040933609
time_elpased: 1.639
#iterations: 441
currently lose_sum: 0.08474773168563843
time_elpased: 1.66
#iterations: 442
currently lose_sum: 0.19519031047821045
time_elpased: 1.64
#iterations: 443
currently lose_sum: 0.10915199667215347
time_elpased: 1.679
#iterations: 444
currently lose_sum: 0.15507888793945312
time_elpased: 1.668
#iterations: 445
currently lose_sum: 0.20891623198986053
time_elpased: 1.671
#iterations: 446
currently lose_sum: 0.11686553806066513
time_elpased: 1.663
#iterations: 447
currently lose_sum: 0.1411224603652954
time_elpased: 1.672
#iterations: 448
currently lose_sum: 0.12237496674060822
time_elpased: 1.62
#iterations: 449
currently lose_sum: 0.11912529170513153
time_elpased: 1.698
#iterations: 450
currently lose_sum: 0.19913390278816223
time_elpased: 1.674
#iterations: 451
currently lose_sum: 0.12121729552745819
time_elpased: 1.664
#iterations: 452
currently lose_sum: 0.06439588963985443
time_elpased: 1.64
#iterations: 453
currently lose_sum: 0.14750193059444427
time_elpased: 1.66
#iterations: 454
currently lose_sum: 0.2687140107154846
time_elpased: 1.698
#iterations: 455
currently lose_sum: 0.20226137340068817
time_elpased: 1.677
#iterations: 456
currently lose_sum: 0.08680842816829681
time_elpased: 1.675
#iterations: 457
currently lose_sum: 0.13297899067401886
time_elpased: 1.649
#iterations: 458
currently lose_sum: 0.07537654787302017
time_elpased: 1.665
#iterations: 459
currently lose_sum: 0.16050872206687927
time_elpased: 1.649
start validation test
0.732589285714
0.886764705882
0.536
0.668144044321
0.733470852018
65.861
#iterations: 460
currently lose_sum: nan
time_elpased: 1.741
train finish final lose is: nan
acc: 0.750
pre: 0.811
rec: 0.654
F1: 0.724
auc: 0.750
