start to construct computing graph
graph construct over
3372
epochs start
#iterations: 0
currently lose_sum: 0.6364824771881104
time_elpased: 9.16
#iterations: 1
currently lose_sum: 0.6883341670036316
time_elpased: 9.161
#iterations: 2
currently lose_sum: 0.5727415084838867
time_elpased: 9.176
#iterations: 3
currently lose_sum: 0.6554129719734192
time_elpased: 9.151
#iterations: 4
currently lose_sum: 0.643022358417511
time_elpased: 9.177
#iterations: 5
currently lose_sum: 0.5055899024009705
time_elpased: 9.161
#iterations: 6
currently lose_sum: 0.4679710268974304
time_elpased: 9.132
#iterations: 7
currently lose_sum: 0.5332366824150085
time_elpased: 9.147
#iterations: 8
currently lose_sum: 0.562163233757019
time_elpased: 9.193
#iterations: 9
currently lose_sum: 0.40434175729751587
time_elpased: 9.16
#iterations: 10
currently lose_sum: 0.36664241552352905
time_elpased: 9.214
#iterations: 11
currently lose_sum: 0.486100971698761
time_elpased: 9.168
#iterations: 12
currently lose_sum: 0.5729051232337952
time_elpased: 9.137
#iterations: 13
currently lose_sum: 0.41979479789733887
time_elpased: 9.151
#iterations: 14
currently lose_sum: 0.5174509882926941
time_elpased: 9.126
#iterations: 15
currently lose_sum: 0.36268335580825806
time_elpased: 9.182
#iterations: 16
currently lose_sum: 0.355706125497818
time_elpased: 9.15
#iterations: 17
currently lose_sum: 0.34681421518325806
time_elpased: 9.151
#iterations: 18
currently lose_sum: 0.45980602502822876
time_elpased: 9.147
#iterations: 19
currently lose_sum: 0.3413756787776947
time_elpased: 9.181
start validation test
0.752232142857
0.818589025756
0.650355871886
0.724838869608
0.752597290782
32.400
#iterations: 20
currently lose_sum: 0.4290136694908142
time_elpased: 8.659
#iterations: 21
currently lose_sum: 0.3569698929786682
time_elpased: 8.651
#iterations: 22
currently lose_sum: 0.4994860291481018
time_elpased: 8.683
#iterations: 23
currently lose_sum: 0.31344637274742126
time_elpased: 8.652
#iterations: 24
currently lose_sum: 0.46496886014938354
time_elpased: 8.663
#iterations: 25
currently lose_sum: 0.2725452482700348
time_elpased: 8.689
#iterations: 26
currently lose_sum: 0.236554354429245
time_elpased: 8.725
#iterations: 27
currently lose_sum: 0.1595812290906906
time_elpased: 8.626
#iterations: 28
currently lose_sum: 0.23494203388690948
time_elpased: 8.689
#iterations: 29
currently lose_sum: 0.6036152243614197
time_elpased: 8.657
#iterations: 30
currently lose_sum: 0.3132656514644623
time_elpased: 8.636
#iterations: 31
currently lose_sum: 0.2540532350540161
time_elpased: 8.711
#iterations: 32
currently lose_sum: 0.4613044261932373
time_elpased: 8.631
#iterations: 33
currently lose_sum: 0.2823484241962433
time_elpased: 8.673
#iterations: 34
currently lose_sum: 0.29921573400497437
time_elpased: 8.699
#iterations: 35
currently lose_sum: 0.3326362669467926
time_elpased: 8.674
#iterations: 36
currently lose_sum: 0.2562393546104431
time_elpased: 8.654
#iterations: 37
currently lose_sum: 0.2579421103000641
time_elpased: 8.701
#iterations: 38
currently lose_sum: 0.24860171973705292
time_elpased: 8.681
#iterations: 39
currently lose_sum: 0.2382344752550125
time_elpased: 8.652
start validation test
0.751785714286
0.845498783455
0.618327402135
0.714285714286
0.752264059491
37.169
#iterations: 40
currently lose_sum: 0.39712923765182495
time_elpased: 8.673
#iterations: 41
currently lose_sum: 0.3163643479347229
time_elpased: 8.697
#iterations: 42
currently lose_sum: 0.3026765286922455
time_elpased: 8.628
#iterations: 43
currently lose_sum: 0.23487749695777893
time_elpased: 8.668
#iterations: 44
currently lose_sum: 0.16664333641529083
time_elpased: 8.685
#iterations: 45
currently lose_sum: 0.22258415818214417
time_elpased: 8.71
#iterations: 46
currently lose_sum: 0.20778298377990723
time_elpased: 8.634
#iterations: 47
currently lose_sum: 0.18606030941009521
time_elpased: 8.654
#iterations: 48
currently lose_sum: 0.19820483028888702
time_elpased: 8.682
#iterations: 49
currently lose_sum: 0.20320765674114227
time_elpased: 8.685
#iterations: 50
currently lose_sum: 0.1717034876346588
time_elpased: 8.657
#iterations: 51
currently lose_sum: 0.21343305706977844
time_elpased: 8.731
#iterations: 52
currently lose_sum: 0.2577672004699707
time_elpased: 8.646
#iterations: 53
currently lose_sum: 0.2403678148984909
time_elpased: 8.679
#iterations: 54
currently lose_sum: 0.442390501499176
time_elpased: 8.7
#iterations: 55
currently lose_sum: 0.31774187088012695
time_elpased: 8.692
#iterations: 56
currently lose_sum: 0.24205851554870605
time_elpased: 8.639
#iterations: 57
currently lose_sum: 0.09241701662540436
time_elpased: 8.656
#iterations: 58
currently lose_sum: 0.18147386610507965
time_elpased: 8.694
#iterations: 59
currently lose_sum: 0.17251083254814148
time_elpased: 8.684
start validation test
0.757142857143
0.850241545894
0.626334519573
0.72131147541
0.757611704231
40.433
#iterations: 60
currently lose_sum: 0.39558321237564087
time_elpased: 8.666
#iterations: 61
currently lose_sum: 0.2140580713748932
time_elpased: 8.734
#iterations: 62
currently lose_sum: 0.09044051915407181
time_elpased: 8.67
#iterations: 63
currently lose_sum: 0.11786894500255585
time_elpased: 8.704
#iterations: 64
currently lose_sum: 0.23146042227745056
time_elpased: 8.674
#iterations: 65
currently lose_sum: 0.14608387649059296
time_elpased: 8.702
#iterations: 66
currently lose_sum: 0.2694127559661865
time_elpased: 8.665
#iterations: 67
currently lose_sum: 0.3419856131076813
time_elpased: 8.662
#iterations: 68
currently lose_sum: 0.2688834071159363
time_elpased: 8.709
#iterations: 69
currently lose_sum: 0.22048404812812805
time_elpased: 8.677
#iterations: 70
currently lose_sum: 0.16863590478897095
time_elpased: 8.702
#iterations: 71
currently lose_sum: 0.07052645832300186
time_elpased: 8.7
#iterations: 72
currently lose_sum: 0.10265149921178818
time_elpased: 8.647
#iterations: 73
currently lose_sum: 0.10271413624286652
time_elpased: 8.682
#iterations: 74
currently lose_sum: 0.22203879058361053
time_elpased: 8.647
#iterations: 75
currently lose_sum: 0.32439717650413513
time_elpased: 8.684
#iterations: 76
currently lose_sum: 0.19762995839118958
time_elpased: 8.688
#iterations: 77
currently lose_sum: 0.146264910697937
time_elpased: 8.633
#iterations: 78
currently lose_sum: 0.12623949348926544
time_elpased: 8.647
#iterations: 79
currently lose_sum: 0.06244794279336929
time_elpased: 8.657
start validation test
0.750892857143
0.875331564987
0.5871886121
0.702875399361
0.751479610709
50.638
#iterations: 80
currently lose_sum: 0.33771324157714844
time_elpased: 8.695
#iterations: 81
currently lose_sum: 0.291864275932312
time_elpased: 8.695
#iterations: 82
currently lose_sum: 0.11907048523426056
time_elpased: 8.694
#iterations: 83
currently lose_sum: 0.11350755393505096
time_elpased: 8.684
#iterations: 84
currently lose_sum: 0.1518353819847107
time_elpased: 8.683
#iterations: 85
currently lose_sum: 0.23026108741760254
time_elpased: 8.649
#iterations: 86
currently lose_sum: 0.2912828028202057
time_elpased: 8.643
#iterations: 87
currently lose_sum: 0.04539089277386665
time_elpased: 8.655
#iterations: 88
currently lose_sum: 0.22124116122722626
time_elpased: 8.635
#iterations: 89
currently lose_sum: 0.4039115905761719
time_elpased: 8.699
#iterations: 90
currently lose_sum: 0.11644025146961212
time_elpased: 8.647
#iterations: 91
currently lose_sum: 0.11585907638072968
time_elpased: 8.693
#iterations: 92
currently lose_sum: 0.19470739364624023
time_elpased: 8.698
#iterations: 93
currently lose_sum: 0.25482481718063354
time_elpased: 8.664
#iterations: 94
currently lose_sum: 0.24576199054718018
time_elpased: 8.661
#iterations: 95
currently lose_sum: 0.11279591172933578
time_elpased: 8.694
#iterations: 96
currently lose_sum: 0.09489625692367554
time_elpased: 8.648
#iterations: 97
currently lose_sum: 0.25755807757377625
time_elpased: 8.661
#iterations: 98
currently lose_sum: 0.09625907242298126
time_elpased: 8.692
#iterations: 99
currently lose_sum: 0.22531428933143616
time_elpased: 8.691
start validation test
0.737946428571
0.863328822733
0.567615658363
0.684916800859
0.738556933124
59.861
#iterations: 100
currently lose_sum: 0.12922120094299316
time_elpased: 8.675
#iterations: 101
currently lose_sum: 0.27484971284866333
time_elpased: 8.628
#iterations: 102
currently lose_sum: 0.07713782787322998
time_elpased: 8.676
#iterations: 103
currently lose_sum: 0.25753650069236755
time_elpased: 8.629
#iterations: 104
currently lose_sum: 0.15618346631526947
time_elpased: 8.676
#iterations: 105
currently lose_sum: 0.12049315124750137
time_elpased: 8.696
#iterations: 106
currently lose_sum: 0.13319554924964905
time_elpased: 8.636
#iterations: 107
currently lose_sum: 0.11965496838092804
time_elpased: 8.702
#iterations: 108
currently lose_sum: 0.15366366505622864
time_elpased: 8.664
#iterations: 109
currently lose_sum: 0.19095218181610107
time_elpased: 8.668
#iterations: 110
currently lose_sum: 0.2718229293823242
time_elpased: 8.746
#iterations: 111
currently lose_sum: 0.13487304747104645
time_elpased: 8.733
#iterations: 112
currently lose_sum: 0.1318727433681488
time_elpased: 8.657
#iterations: 113
currently lose_sum: 0.2417694330215454
time_elpased: 8.672
#iterations: 114
currently lose_sum: 0.22702160477638245
time_elpased: 8.676
#iterations: 115
currently lose_sum: 0.23486241698265076
time_elpased: 8.642
#iterations: 116
currently lose_sum: 0.1392642855644226
time_elpased: 8.663
#iterations: 117
currently lose_sum: 0.1424238234758377
time_elpased: 8.763
#iterations: 118
currently lose_sum: 0.2096244841814041
time_elpased: 8.666
#iterations: 119
currently lose_sum: 0.20933827757835388
time_elpased: 8.68
start validation test
0.732142857143
0.863888888889
0.553380782918
0.674620390456
0.732783581423
65.524
#iterations: 120
currently lose_sum: 0.09139610826969147
time_elpased: 8.653
#iterations: 121
currently lose_sum: 0.22300739586353302
time_elpased: 8.66
#iterations: 122
currently lose_sum: 0.2076689749956131
time_elpased: 8.675
#iterations: 123
currently lose_sum: 0.09682969748973846
time_elpased: 8.639
#iterations: 124
currently lose_sum: 0.10111822187900543
time_elpased: 8.658
#iterations: 125
currently lose_sum: 0.15366628766059875
time_elpased: 8.68
#iterations: 126
currently lose_sum: 0.31843894720077515
time_elpased: 8.643
#iterations: 127
currently lose_sum: 0.1256171017885208
time_elpased: 8.727
#iterations: 128
currently lose_sum: 0.24809491634368896
time_elpased: 8.713
#iterations: 129
currently lose_sum: 0.13454052805900574
time_elpased: 8.717
#iterations: 130
currently lose_sum: 0.14947448670864105
time_elpased: 8.687
#iterations: 131
currently lose_sum: 0.10069490969181061
time_elpased: 8.681
#iterations: 132
currently lose_sum: 0.18661420047283173
time_elpased: 8.641
#iterations: 133
currently lose_sum: 0.05095720291137695
time_elpased: 8.731
#iterations: 134
currently lose_sum: 0.19539734721183777
time_elpased: 8.704
#iterations: 135
currently lose_sum: 0.15735818445682526
time_elpased: 8.686
#iterations: 136
currently lose_sum: 0.1319011002779007
time_elpased: 8.69
#iterations: 137
currently lose_sum: 0.09903044998645782
time_elpased: 8.679
#iterations: 138
currently lose_sum: 0.024802878499031067
time_elpased: 8.653
#iterations: 139
currently lose_sum: 0.06254060566425323
time_elpased: 8.695
start validation test
0.714732142857
0.893030794165
0.490213523132
0.632969557725
0.715536869093
76.669
#iterations: 140
currently lose_sum: 0.06279756873846054
time_elpased: 8.635
#iterations: 141
currently lose_sum: 0.052452586591243744
time_elpased: 8.644
#iterations: 142
currently lose_sum: 0.1416928917169571
time_elpased: 8.68
#iterations: 143
currently lose_sum: 0.21930575370788574
time_elpased: 8.672
#iterations: 144
currently lose_sum: 0.056480489671230316
time_elpased: 8.651
#iterations: 145
currently lose_sum: 0.3672952651977539
time_elpased: 8.705
#iterations: 146
currently lose_sum: 0.1521947830915451
time_elpased: 8.648
#iterations: 147
currently lose_sum: 0.2303122580051422
time_elpased: 8.716
#iterations: 148
currently lose_sum: 0.09319232404232025
time_elpased: 8.714
#iterations: 149
currently lose_sum: 0.09271398931741714
time_elpased: 8.678
#iterations: 150
currently lose_sum: 0.0296340174973011
time_elpased: 8.727
#iterations: 151
currently lose_sum: 0.0488835871219635
time_elpased: 8.634
#iterations: 152
currently lose_sum: 0.12860754132270813
time_elpased: 8.634
#iterations: 153
currently lose_sum: 0.27105647325515747
time_elpased: 8.674
#iterations: 154
currently lose_sum: 0.08408590406179428
time_elpased: 8.721
#iterations: 155
currently lose_sum: 0.17714039981365204
time_elpased: 8.635
#iterations: 156
currently lose_sum: 0.07064993679523468
time_elpased: 8.696
#iterations: 157
currently lose_sum: 0.1425384283065796
time_elpased: 8.679
#iterations: 158
currently lose_sum: 0.13682717084884644
time_elpased: 8.716
#iterations: 159
currently lose_sum: 0.07472632825374603
time_elpased: 8.618
start validation test
0.710267857143
0.891268533773
0.481316725979
0.625072212594
0.711088470516
84.171
#iterations: 160
currently lose_sum: 0.0880042165517807
time_elpased: 8.682
#iterations: 161
currently lose_sum: 0.05745966359972954
time_elpased: 8.723
#iterations: 162
currently lose_sum: 0.1775372326374054
time_elpased: 8.672
#iterations: 163
currently lose_sum: 0.028134172782301903
time_elpased: 8.681
#iterations: 164
currently lose_sum: 0.0863330140709877
time_elpased: 8.679
#iterations: 165
currently lose_sum: 0.07871529459953308
time_elpased: 8.657
#iterations: 166
currently lose_sum: 0.09520824253559113
time_elpased: 8.643
#iterations: 167
currently lose_sum: 0.13545933365821838
time_elpased: 8.645
#iterations: 168
currently lose_sum: 0.05046703293919563
time_elpased: 8.715
#iterations: 169
currently lose_sum: 0.1699000895023346
time_elpased: 8.675
#iterations: 170
currently lose_sum: 0.090840645134449
time_elpased: 8.666
#iterations: 171
currently lose_sum: 0.05786456540226936
time_elpased: 8.77
#iterations: 172
currently lose_sum: 0.12493640184402466
time_elpased: 8.705
#iterations: 173
currently lose_sum: 0.13422530889511108
time_elpased: 8.653
#iterations: 174
currently lose_sum: 0.20425768196582794
time_elpased: 8.707
#iterations: 175
currently lose_sum: 0.1363741159439087
time_elpased: 8.723
#iterations: 176
currently lose_sum: 0.08574734628200531
time_elpased: 8.645
#iterations: 177
currently lose_sum: 0.1574425995349884
time_elpased: 8.655
#iterations: 178
currently lose_sum: 0.04464089125394821
time_elpased: 8.648
#iterations: 179
currently lose_sum: 0.14074289798736572
time_elpased: 8.66
start validation test
0.716517857143
0.89756097561
0.491103202847
0.634847613571
0.717325794972
88.154
#iterations: 180
currently lose_sum: 0.07409614324569702
time_elpased: 8.681
#iterations: 181
currently lose_sum: 0.11439362913370132
time_elpased: 8.651
#iterations: 182
currently lose_sum: 0.16127212345600128
time_elpased: 8.685
#iterations: 183
currently lose_sum: 0.02227177284657955
time_elpased: 8.687
#iterations: 184
currently lose_sum: 0.1534481793642044
time_elpased: 8.714
#iterations: 185
currently lose_sum: nan
time_elpased: 8.653
train finish final lose is: nan
acc: 0.756
pre: 0.830
rec: 0.647
F1: 0.727
auc: 0.757
