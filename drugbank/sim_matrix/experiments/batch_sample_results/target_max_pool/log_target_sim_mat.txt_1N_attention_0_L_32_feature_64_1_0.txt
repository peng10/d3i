start to construct computing graph
graph construct over
3372
epochs start
#iterations: 0
currently lose_sum: 0.6658331155776978
time_elpased: 2.641
#iterations: 1
currently lose_sum: 0.6667729616165161
time_elpased: 2.623
#iterations: 2
currently lose_sum: 0.5624985694885254
time_elpased: 2.657
#iterations: 3
currently lose_sum: 0.6392261981964111
time_elpased: 2.595
#iterations: 4
currently lose_sum: 0.6284804344177246
time_elpased: 2.585
#iterations: 5
currently lose_sum: 0.4934873580932617
time_elpased: 2.601
#iterations: 6
currently lose_sum: 0.43454498052597046
time_elpased: 2.624
#iterations: 7
currently lose_sum: 0.5189222097396851
time_elpased: 2.588
#iterations: 8
currently lose_sum: 0.6043018102645874
time_elpased: 2.627
#iterations: 9
currently lose_sum: 0.4057123064994812
time_elpased: 2.616
#iterations: 10
currently lose_sum: 0.39796966314315796
time_elpased: 2.661
#iterations: 11
currently lose_sum: 0.4771438539028168
time_elpased: 2.584
#iterations: 12
currently lose_sum: 0.5119118094444275
time_elpased: 2.625
#iterations: 13
currently lose_sum: 0.40370282530784607
time_elpased: 2.648
#iterations: 14
currently lose_sum: 0.542567789554596
time_elpased: 2.593
#iterations: 15
currently lose_sum: 0.31817516684532166
time_elpased: 2.626
#iterations: 16
currently lose_sum: 0.4006772041320801
time_elpased: 2.607
#iterations: 17
currently lose_sum: 0.40112799406051636
time_elpased: 2.632
#iterations: 18
currently lose_sum: 0.4088311791419983
time_elpased: 2.618
#iterations: 19
currently lose_sum: 0.33794721961021423
time_elpased: 2.584
start validation test
0.752232142857
0.81161007667
0.659252669039
0.727540500736
0.75256540262
31.200
#iterations: 20
currently lose_sum: 0.39651718735694885
time_elpased: 2.111
#iterations: 21
currently lose_sum: 0.3184666633605957
time_elpased: 2.12
#iterations: 22
currently lose_sum: 0.4787753224372864
time_elpased: 2.089
#iterations: 23
currently lose_sum: 0.3116433024406433
time_elpased: 2.131
#iterations: 24
currently lose_sum: 0.4816276431083679
time_elpased: 2.168
#iterations: 25
currently lose_sum: 0.3281438946723938
time_elpased: 2.123
#iterations: 26
currently lose_sum: 0.3062629997730255
time_elpased: 2.078
#iterations: 27
currently lose_sum: 0.27681463956832886
time_elpased: 2.124
#iterations: 28
currently lose_sum: 0.2593044340610504
time_elpased: 2.141
#iterations: 29
currently lose_sum: 0.65362548828125
time_elpased: 2.141
#iterations: 30
currently lose_sum: 0.404629647731781
time_elpased: 2.094
#iterations: 31
currently lose_sum: 0.3587212562561035
time_elpased: 2.104
#iterations: 32
currently lose_sum: 0.4989321827888489
time_elpased: 2.122
#iterations: 33
currently lose_sum: 0.35859984159469604
time_elpased: 2.249
#iterations: 34
currently lose_sum: 0.2412225753068924
time_elpased: 2.11
#iterations: 35
currently lose_sum: 0.3643440902233124
time_elpased: 2.1
#iterations: 36
currently lose_sum: 0.27060121297836304
time_elpased: 2.163
#iterations: 37
currently lose_sum: 0.25745153427124023
time_elpased: 2.07
#iterations: 38
currently lose_sum: 0.3391273319721222
time_elpased: 2.147
#iterations: 39
currently lose_sum: 0.2698407769203186
time_elpased: 2.134
start validation test
0.748214285714
0.82634032634
0.630782918149
0.715438950555
0.748635186673
32.698
#iterations: 40
currently lose_sum: 0.32908302545547485
time_elpased: 2.089
#iterations: 41
currently lose_sum: 0.39427894353866577
time_elpased: 2.128
#iterations: 42
currently lose_sum: 0.3409353792667389
time_elpased: 2.159
#iterations: 43
currently lose_sum: 0.25938740372657776
time_elpased: 2.088
#iterations: 44
currently lose_sum: 0.21784010529518127
time_elpased: 2.104
#iterations: 45
currently lose_sum: 0.26423823833465576
time_elpased: 2.127
#iterations: 46
currently lose_sum: 0.25419217348098755
time_elpased: 2.079
#iterations: 47
currently lose_sum: 0.1772008240222931
time_elpased: 2.11
#iterations: 48
currently lose_sum: 0.2594525218009949
time_elpased: 2.147
#iterations: 49
currently lose_sum: 0.27697592973709106
time_elpased: 2.217
#iterations: 50
currently lose_sum: 0.23754587769508362
time_elpased: 2.115
#iterations: 51
currently lose_sum: 0.24398677051067352
time_elpased: 2.126
#iterations: 52
currently lose_sum: 0.3085455596446991
time_elpased: 2.131
#iterations: 53
currently lose_sum: 0.26746612787246704
time_elpased: 2.16
#iterations: 54
currently lose_sum: 0.5075352787971497
time_elpased: 2.109
#iterations: 55
currently lose_sum: 0.33821994066238403
time_elpased: 2.06
#iterations: 56
currently lose_sum: 0.23524188995361328
time_elpased: 2.149
#iterations: 57
currently lose_sum: 0.1343483179807663
time_elpased: 2.137
#iterations: 58
currently lose_sum: 0.20139512419700623
time_elpased: 2.124
#iterations: 59
currently lose_sum: 0.17155015468597412
time_elpased: 2.111
start validation test
0.751339285714
0.836298932384
0.627224199288
0.716827656329
0.751784142655
36.196
#iterations: 60
currently lose_sum: 0.3531685769557953
time_elpased: 2.152
#iterations: 61
currently lose_sum: 0.26655107736587524
time_elpased: 2.106
#iterations: 62
currently lose_sum: 0.21352751553058624
time_elpased: 2.171
#iterations: 63
currently lose_sum: 0.20346322655677795
time_elpased: 2.164
#iterations: 64
currently lose_sum: 0.27423015236854553
time_elpased: 2.11
#iterations: 65
currently lose_sum: 0.16216860711574554
time_elpased: 2.189
#iterations: 66
currently lose_sum: 0.2937862277030945
time_elpased: 2.13
#iterations: 67
currently lose_sum: 0.24640336632728577
time_elpased: 2.156
#iterations: 68
currently lose_sum: 0.16469743847846985
time_elpased: 2.133
#iterations: 69
currently lose_sum: 0.2973684072494507
time_elpased: 2.139
#iterations: 70
currently lose_sum: 0.18678174912929535
time_elpased: 2.083
#iterations: 71
currently lose_sum: 0.08250325173139572
time_elpased: 2.121
#iterations: 72
currently lose_sum: 0.10442666709423065
time_elpased: 2.104
#iterations: 73
currently lose_sum: 0.10351566970348358
time_elpased: 2.095
#iterations: 74
currently lose_sum: 0.20909348130226135
time_elpased: 2.066
#iterations: 75
currently lose_sum: 0.3428496718406677
time_elpased: 2.124
#iterations: 76
currently lose_sum: 0.216711163520813
time_elpased: 2.144
#iterations: 77
currently lose_sum: 0.14692740142345428
time_elpased: 2.134
#iterations: 78
currently lose_sum: 0.1617947816848755
time_elpased: 2.086
#iterations: 79
currently lose_sum: 0.09488718211650848
time_elpased: 2.13
start validation test
0.750892857143
0.872368421053
0.589857651246
0.703821656051
0.751470044261
41.766
#iterations: 80
currently lose_sum: 0.38261470198631287
time_elpased: 2.103
#iterations: 81
currently lose_sum: 0.28566330671310425
time_elpased: 2.135
#iterations: 82
currently lose_sum: 0.20900984108448029
time_elpased: 2.118
#iterations: 83
currently lose_sum: 0.14908058941364288
time_elpased: 2.145
#iterations: 84
currently lose_sum: 0.23353275656700134
time_elpased: 2.109
#iterations: 85
currently lose_sum: 0.34269607067108154
time_elpased: 2.128
#iterations: 86
currently lose_sum: 0.25625038146972656
time_elpased: 2.086
#iterations: 87
currently lose_sum: 0.11031641066074371
time_elpased: 2.066
#iterations: 88
currently lose_sum: 0.19404268264770508
time_elpased: 2.116
#iterations: 89
currently lose_sum: 0.2489742785692215
time_elpased: 2.117
#iterations: 90
currently lose_sum: 0.12303604930639267
time_elpased: 2.131
#iterations: 91
currently lose_sum: 0.15117600560188293
time_elpased: 2.146
#iterations: 92
currently lose_sum: 0.21875353157520294
time_elpased: 2.142
#iterations: 93
currently lose_sum: 0.3278682827949524
time_elpased: 2.121
#iterations: 94
currently lose_sum: 0.3021630644798279
time_elpased: 2.146
#iterations: 95
currently lose_sum: 0.16675351560115814
time_elpased: 2.117
#iterations: 96
currently lose_sum: 0.17363640666007996
time_elpased: 2.145
#iterations: 97
currently lose_sum: 0.19845688343048096
time_elpased: 2.131
#iterations: 98
currently lose_sum: 0.15728120505809784
time_elpased: 2.128
#iterations: 99
currently lose_sum: 0.16332605481147766
time_elpased: 2.098
start validation test
0.752232142857
0.835891381346
0.629893238434
0.718417047184
0.752670633554
43.607
#iterations: 100
currently lose_sum: 0.1396782100200653
time_elpased: 2.089
#iterations: 101
currently lose_sum: 0.24509279429912567
time_elpased: 2.14
#iterations: 102
currently lose_sum: 0.12443853914737701
time_elpased: 2.155
#iterations: 103
currently lose_sum: 0.3563484251499176
time_elpased: 2.13
#iterations: 104
currently lose_sum: 0.1595574915409088
time_elpased: 2.115
#iterations: 105
currently lose_sum: 0.1472952514886856
time_elpased: 2.097
#iterations: 106
currently lose_sum: 0.22193224728107452
time_elpased: 2.1
#iterations: 107
currently lose_sum: 0.22022709250450134
time_elpased: 2.123
#iterations: 108
currently lose_sum: 0.15435044467449188
time_elpased: 2.089
#iterations: 109
currently lose_sum: 0.2527310252189636
time_elpased: 2.115
#iterations: 110
currently lose_sum: 0.3436696529388428
time_elpased: 2.105
#iterations: 111
currently lose_sum: 0.14692270755767822
time_elpased: 2.127
#iterations: 112
currently lose_sum: 0.21532857418060303
time_elpased: 2.143
#iterations: 113
currently lose_sum: 0.22299687564373016
time_elpased: 2.141
#iterations: 114
currently lose_sum: 0.35349041223526
time_elpased: 2.123
#iterations: 115
currently lose_sum: 0.3451994061470032
time_elpased: 2.106
#iterations: 116
currently lose_sum: 0.11418905109167099
time_elpased: 2.165
#iterations: 117
currently lose_sum: 0.22842004895210266
time_elpased: 2.156
#iterations: 118
currently lose_sum: 0.23373079299926758
time_elpased: 2.087
#iterations: 119
currently lose_sum: 0.14990587532520294
time_elpased: 2.135
start validation test
0.740178571429
0.842171717172
0.593416370107
0.69624217119
0.740704600824
48.138
#iterations: 120
currently lose_sum: 0.14814643561840057
time_elpased: 2.105
#iterations: 121
currently lose_sum: 0.24265745282173157
time_elpased: 2.081
#iterations: 122
currently lose_sum: 0.3108690679073334
time_elpased: 2.123
#iterations: 123
currently lose_sum: 0.16376714408397675
time_elpased: 2.092
#iterations: 124
currently lose_sum: 0.11835179477930069
time_elpased: 2.157
#iterations: 125
currently lose_sum: 0.11626018583774567
time_elpased: 2.135
#iterations: 126
currently lose_sum: 0.3006545901298523
time_elpased: 2.093
#iterations: 127
currently lose_sum: 0.11346454918384552
time_elpased: 2.104
#iterations: 128
currently lose_sum: 0.18974032998085022
time_elpased: 2.127
#iterations: 129
currently lose_sum: 0.11284388601779938
time_elpased: 2.155
#iterations: 130
currently lose_sum: 0.21885362267494202
time_elpased: 2.117
#iterations: 131
currently lose_sum: 0.12197200953960419
time_elpased: 2.146
#iterations: 132
currently lose_sum: 0.2141539305448532
time_elpased: 2.094
#iterations: 133
currently lose_sum: 0.08629095554351807
time_elpased: 2.116
#iterations: 134
currently lose_sum: 0.25425973534584045
time_elpased: 2.098
#iterations: 135
currently lose_sum: 0.15716837346553802
time_elpased: 2.15
#iterations: 136
currently lose_sum: 0.19130150973796844
time_elpased: 2.22
#iterations: 137
currently lose_sum: 0.18602807819843292
time_elpased: 2.267
#iterations: 138
currently lose_sum: 0.11673770099878311
time_elpased: 2.199
#iterations: 139
currently lose_sum: 0.0905940905213356
time_elpased: 2.179
start validation test
0.7375
0.890670553936
0.54359430605
0.675138121547
0.738195002487
56.981
#iterations: 140
currently lose_sum: 0.05915277078747749
time_elpased: 2.222
#iterations: 141
currently lose_sum: 0.07750613987445831
time_elpased: 2.174
#iterations: 142
currently lose_sum: 0.1697295904159546
time_elpased: 2.201
#iterations: 143
currently lose_sum: 0.283966988325119
time_elpased: 2.151
#iterations: 144
currently lose_sum: 0.10030300915241241
time_elpased: 2.142
#iterations: 145
currently lose_sum: 0.3639688789844513
time_elpased: 2.126
#iterations: 146
currently lose_sum: 0.12149995565414429
time_elpased: 2.175
#iterations: 147
currently lose_sum: 0.2509404718875885
time_elpased: 2.123
#iterations: 148
currently lose_sum: 0.15517625212669373
time_elpased: 2.172
#iterations: 149
currently lose_sum: 0.10141898691654205
time_elpased: 2.097
#iterations: 150
currently lose_sum: 0.057796381413936615
time_elpased: 2.133
#iterations: 151
currently lose_sum: 0.07506997883319855
time_elpased: 2.138
#iterations: 152
currently lose_sum: 0.19032728672027588
time_elpased: 2.116
#iterations: 153
currently lose_sum: 0.25677958130836487
time_elpased: 2.088
#iterations: 154
currently lose_sum: 0.2312636375427246
time_elpased: 2.104
#iterations: 155
currently lose_sum: 0.13965055346488953
time_elpased: 2.088
#iterations: 156
currently lose_sum: 0.12430636584758759
time_elpased: 2.158
#iterations: 157
currently lose_sum: 0.1418101042509079
time_elpased: 2.124
#iterations: 158
currently lose_sum: 0.18674615025520325
time_elpased: 2.135
#iterations: 159
currently lose_sum: 0.09490621089935303
time_elpased: 2.121
start validation test
0.731696428571
0.888558692422
0.532028469751
0.665553700612
0.732412084338
60.821
#iterations: 160
currently lose_sum: 0.1531541347503662
time_elpased: 2.149
#iterations: 161
currently lose_sum: 0.09347069263458252
time_elpased: 2.104
#iterations: 162
currently lose_sum: 0.21935462951660156
time_elpased: 2.084
#iterations: 163
currently lose_sum: 0.09922699630260468
time_elpased: 2.089
#iterations: 164
currently lose_sum: 0.10873210430145264
time_elpased: 2.088
#iterations: 165
currently lose_sum: 0.11236982047557831
time_elpased: 2.093
#iterations: 166
currently lose_sum: 0.11223526298999786
time_elpased: 2.136
#iterations: 167
currently lose_sum: 0.1787194013595581
time_elpased: 2.119
#iterations: 168
currently lose_sum: 0.09378735721111298
time_elpased: 2.121
#iterations: 169
currently lose_sum: 0.23461489379405975
time_elpased: 2.12
#iterations: 170
currently lose_sum: 0.1254132241010666
time_elpased: 2.125
#iterations: 171
currently lose_sum: 0.11753545701503754
time_elpased: 2.172
#iterations: 172
currently lose_sum: 0.20453038811683655
time_elpased: 2.114
#iterations: 173
currently lose_sum: 0.2775372266769409
time_elpased: 2.142
#iterations: 174
currently lose_sum: 0.2191813886165619
time_elpased: 2.148
#iterations: 175
currently lose_sum: 0.13340574502944946
time_elpased: 2.11
#iterations: 176
currently lose_sum: 0.09571702778339386
time_elpased: 2.122
#iterations: 177
currently lose_sum: 0.1488824337720871
time_elpased: 2.148
#iterations: 178
currently lose_sum: 0.08335361629724503
time_elpased: 2.133
#iterations: 179
currently lose_sum: 0.20250678062438965
time_elpased: 2.11
start validation test
0.732142857143
0.866946778711
0.550711743772
0.673558215452
0.732793147872
61.676
#iterations: 180
currently lose_sum: 0.08845625072717667
time_elpased: 2.141
#iterations: 181
currently lose_sum: 0.17699415981769562
time_elpased: 2.113
#iterations: 182
currently lose_sum: 0.23818400502204895
time_elpased: 2.198
#iterations: 183
currently lose_sum: 0.08708465844392776
time_elpased: 2.13
#iterations: 184
currently lose_sum: 0.20416446030139923
time_elpased: 2.153
#iterations: 185
currently lose_sum: 0.10747723281383514
time_elpased: 2.109
#iterations: 186
currently lose_sum: 0.1266520619392395
time_elpased: 2.107
#iterations: 187
currently lose_sum: 0.0932973176240921
time_elpased: 2.076
#iterations: 188
currently lose_sum: 0.09698539972305298
time_elpased: 2.089
#iterations: 189
currently lose_sum: 0.1391965001821518
time_elpased: 2.094
#iterations: 190
currently lose_sum: 0.060460545122623444
time_elpased: 2.116
#iterations: 191
currently lose_sum: 0.09062723070383072
time_elpased: 2.101
#iterations: 192
currently lose_sum: 0.2514067590236664
time_elpased: 2.141
#iterations: 193
currently lose_sum: 0.16721174120903015
time_elpased: 2.114
#iterations: 194
currently lose_sum: 0.2268509417772293
time_elpased: 2.158
#iterations: 195
currently lose_sum: 0.10276494175195694
time_elpased: 2.151
#iterations: 196
currently lose_sum: 0.23606912791728973
time_elpased: 2.099
#iterations: 197
currently lose_sum: 0.26166608929634094
time_elpased: 2.113
#iterations: 198
currently lose_sum: 0.2717389166355133
time_elpased: 2.131
#iterations: 199
currently lose_sum: 0.14272502064704895
time_elpased: 2.174
start validation test
0.733035714286
0.884502923977
0.538256227758
0.669247787611
0.733733848646
67.113
#iterations: 200
currently lose_sum: 0.11045880615711212
time_elpased: 2.102
#iterations: 201
currently lose_sum: 0.17129230499267578
time_elpased: 2.154
#iterations: 202
currently lose_sum: 0.04775872081518173
time_elpased: 2.11
#iterations: 203
currently lose_sum: 0.05813101679086685
time_elpased: 2.122
#iterations: 204
currently lose_sum: 0.1315745711326599
time_elpased: 2.127
#iterations: 205
currently lose_sum: 0.3404937982559204
time_elpased: 2.075
#iterations: 206
currently lose_sum: 0.23547831177711487
time_elpased: 2.118
#iterations: 207
currently lose_sum: 0.2336432933807373
time_elpased: 2.133
#iterations: 208
currently lose_sum: 0.07102110236883163
time_elpased: 2.113
#iterations: 209
currently lose_sum: 0.31829071044921875
time_elpased: 2.1
#iterations: 210
currently lose_sum: 0.21941447257995605
time_elpased: 2.148
#iterations: 211
currently lose_sum: 0.06940432637929916
time_elpased: 2.103
#iterations: 212
currently lose_sum: 0.22225907444953918
time_elpased: 2.119
#iterations: 213
currently lose_sum: 0.13170497119426727
time_elpased: 2.18
#iterations: 214
currently lose_sum: 0.127857968211174
time_elpased: 2.142
#iterations: 215
currently lose_sum: 0.1246662512421608
time_elpased: 2.113
#iterations: 216
currently lose_sum: 0.08889814466238022
time_elpased: 2.087
#iterations: 217
currently lose_sum: 0.256401389837265
time_elpased: 2.141
#iterations: 218
currently lose_sum: 0.057417310774326324
time_elpased: 2.143
#iterations: 219
currently lose_sum: 0.06024075672030449
time_elpased: 2.146
start validation test
0.729910714286
0.879941434846
0.534697508897
0.665190924184
0.730610403194
70.052
#iterations: 220
currently lose_sum: 0.28352969884872437
time_elpased: 2.113
#iterations: 221
currently lose_sum: 0.15425965189933777
time_elpased: 2.139
#iterations: 222
currently lose_sum: 0.18528246879577637
time_elpased: 2.177
#iterations: 223
currently lose_sum: 0.07214663177728653
time_elpased: 2.09
#iterations: 224
currently lose_sum: 0.09314937889575958
time_elpased: 2.081
#iterations: 225
currently lose_sum: 0.169490247964859
time_elpased: 2.156
#iterations: 226
currently lose_sum: 0.049654338508844376
time_elpased: 2.102
#iterations: 227
currently lose_sum: 0.3152291178703308
time_elpased: 2.104
#iterations: 228
currently lose_sum: 0.02783782407641411
time_elpased: 2.184
#iterations: 229
currently lose_sum: 0.10320453345775604
time_elpased: 2.13
#iterations: 230
currently lose_sum: 0.045832905918359756
time_elpased: 2.11
#iterations: 231
currently lose_sum: 0.2875434160232544
time_elpased: 2.176
#iterations: 232
currently lose_sum: 0.120064876973629
time_elpased: 2.097
#iterations: 233
currently lose_sum: 0.3521449565887451
time_elpased: 2.089
#iterations: 234
currently lose_sum: 0.3826668858528137
time_elpased: 2.169
#iterations: 235
currently lose_sum: 0.1169237345457077
time_elpased: 2.141
#iterations: 236
currently lose_sum: 0.14337573945522308
time_elpased: 2.116
#iterations: 237
currently lose_sum: 0.24755768477916718
time_elpased: 2.132
#iterations: 238
currently lose_sum: 0.13473278284072876
time_elpased: 2.122
#iterations: 239
currently lose_sum: 0.1910407841205597
time_elpased: 2.155
start validation test
0.731696428571
0.874105865522
0.54359430605
0.670323642348
0.732370629727
71.332
#iterations: 240
currently lose_sum: 0.12963168323040009
time_elpased: 2.201
#iterations: 241
currently lose_sum: 0.1303965300321579
time_elpased: 2.074
#iterations: 242
currently lose_sum: 0.05801398307085037
time_elpased: 2.1
#iterations: 243
currently lose_sum: 0.1096249371767044
time_elpased: 2.103
#iterations: 244
currently lose_sum: 0.09690292179584503
time_elpased: 2.122
#iterations: 245
currently lose_sum: 0.13270403444766998
time_elpased: 2.127
#iterations: 246
currently lose_sum: 0.31346994638442993
time_elpased: 2.075
#iterations: 247
currently lose_sum: 0.10692097246646881
time_elpased: 2.137
#iterations: 248
currently lose_sum: 0.07465864717960358
time_elpased: 2.121
#iterations: 249
currently lose_sum: 0.0598430261015892
time_elpased: 2.139
#iterations: 250
currently lose_sum: 0.04070088267326355
time_elpased: 2.144
#iterations: 251
currently lose_sum: 0.15491363406181335
time_elpased: 2.088
#iterations: 252
currently lose_sum: 0.3041050434112549
time_elpased: 2.079
#iterations: 253
currently lose_sum: 0.0899532288312912
time_elpased: 2.143
#iterations: 254
currently lose_sum: 0.32881253957748413
time_elpased: 2.134
#iterations: 255
currently lose_sum: 0.18187770247459412
time_elpased: 2.101
#iterations: 256
currently lose_sum: 0.11048493534326553
time_elpased: 2.117
#iterations: 257
currently lose_sum: 0.0971255749464035
time_elpased: 2.137
#iterations: 258
currently lose_sum: 0.15775595605373383
time_elpased: 2.09
#iterations: 259
currently lose_sum: 0.14970001578330994
time_elpased: 2.11
start validation test
0.727678571429
0.888217522659
0.523131672598
0.658454647256
0.728411714435
78.191
#iterations: 260
currently lose_sum: 0.16777583956718445
time_elpased: 2.133
#iterations: 261
currently lose_sum: 0.1190374344587326
time_elpased: 2.14
#iterations: 262
currently lose_sum: 0.059945784509181976
time_elpased: 2.138
#iterations: 263
currently lose_sum: 0.09110984206199646
time_elpased: 2.115
#iterations: 264
currently lose_sum: 0.17027601599693298
time_elpased: 2.122
#iterations: 265
currently lose_sum: 0.0677480474114418
time_elpased: 2.135
#iterations: 266
currently lose_sum: 0.047765687108039856
time_elpased: 2.151
#iterations: 267
currently lose_sum: 0.06345613300800323
time_elpased: 2.134
#iterations: 268
currently lose_sum: 0.12978672981262207
time_elpased: 2.095
#iterations: 269
currently lose_sum: 0.09366454929113388
time_elpased: 2.127
#iterations: 270
currently lose_sum: 0.19609785079956055
time_elpased: 2.097
#iterations: 271
currently lose_sum: 0.03599483519792557
time_elpased: 2.12
#iterations: 272
currently lose_sum: 0.11165772378444672
time_elpased: 2.095
#iterations: 273
currently lose_sum: 0.13753336668014526
time_elpased: 2.133
#iterations: 274
currently lose_sum: 0.16870558261871338
time_elpased: 2.105
#iterations: 275
currently lose_sum: 0.06596577912569046
time_elpased: 2.139
#iterations: 276
currently lose_sum: 0.1293250024318695
time_elpased: 2.094
#iterations: 277
currently lose_sum: 0.1527535319328308
time_elpased: 2.124
#iterations: 278
currently lose_sum: 0.10535581409931183
time_elpased: 2.126
#iterations: 279
currently lose_sum: 0.1904211938381195
time_elpased: 2.09
start validation test
0.727678571429
0.880177514793
0.529359430605
0.661111111111
0.728389392722
77.321
#iterations: 280
currently lose_sum: 0.052234940230846405
time_elpased: 2.096
#iterations: 281
currently lose_sum: 0.05449432134628296
time_elpased: 2.13
#iterations: 282
currently lose_sum: 0.2739470601081848
time_elpased: 2.105
#iterations: 283
currently lose_sum: 0.09803657233715057
time_elpased: 2.146
#iterations: 284
currently lose_sum: 0.08416058868169785
time_elpased: 2.157
#iterations: 285
currently lose_sum: 0.23999866843223572
time_elpased: 2.126
#iterations: 286
currently lose_sum: 0.22377291321754456
time_elpased: 2.089
#iterations: 287
currently lose_sum: 0.14846500754356384
time_elpased: 2.11
#iterations: 288
currently lose_sum: 0.1355290412902832
time_elpased: 2.129
#iterations: 289
currently lose_sum: 0.1880323588848114
time_elpased: 2.087
#iterations: 290
currently lose_sum: 0.10562505573034286
time_elpased: 2.141
#iterations: 291
currently lose_sum: 0.11499850451946259
time_elpased: 2.187
#iterations: 292
currently lose_sum: 0.32526829838752747
time_elpased: 2.173
#iterations: 293
currently lose_sum: 0.19123998284339905
time_elpased: 2.183
#iterations: 294
currently lose_sum: 0.061095573008060455
time_elpased: 2.141
#iterations: 295
currently lose_sum: 0.0546291247010231
time_elpased: 2.166
#iterations: 296
currently lose_sum: 0.1991455852985382
time_elpased: 2.165
#iterations: 297
currently lose_sum: 0.13193945586681366
time_elpased: 2.119
#iterations: 298
currently lose_sum: 0.12624017894268036
time_elpased: 2.175
#iterations: 299
currently lose_sum: 0.09040578454732895
time_elpased: 2.139
start validation test
0.724107142857
0.883333333333
0.518683274021
0.653587443946
0.724843429125
81.714
#iterations: 300
currently lose_sum: 0.07085885852575302
time_elpased: 2.161
#iterations: 301
currently lose_sum: 0.07877518236637115
time_elpased: 2.151
#iterations: 302
currently lose_sum: 0.1790793091058731
time_elpased: 2.189
#iterations: 303
currently lose_sum: 0.06170235946774483
time_elpased: 2.163
#iterations: 304
currently lose_sum: 0.2996924817562103
time_elpased: 2.134
#iterations: 305
currently lose_sum: 0.09110095351934433
time_elpased: 2.133
#iterations: 306
currently lose_sum: 0.11385571956634521
time_elpased: 2.094
#iterations: 307
currently lose_sum: 0.1147347241640091
time_elpased: 2.121
#iterations: 308
currently lose_sum: 0.21231546998023987
time_elpased: 2.149
#iterations: 309
currently lose_sum: 0.3552091121673584
time_elpased: 2.136
#iterations: 310
currently lose_sum: 0.23001904785633087
time_elpased: 2.121
#iterations: 311
currently lose_sum: 0.12652871012687683
time_elpased: 2.119
#iterations: 312
currently lose_sum: 0.2564946711063385
time_elpased: 2.211
#iterations: 313
currently lose_sum: 0.07896634936332703
time_elpased: 2.157
#iterations: 314
currently lose_sum: 0.05586143583059311
time_elpased: 2.148
#iterations: 315
currently lose_sum: 0.14764294028282166
time_elpased: 2.095
#iterations: 316
currently lose_sum: 0.0558086521923542
time_elpased: 2.114
#iterations: 317
currently lose_sum: 0.14867305755615234
time_elpased: 2.134
#iterations: 318
currently lose_sum: 0.1885109543800354
time_elpased: 2.087
#iterations: 319
currently lose_sum: 0.04348619654774666
time_elpased: 2.103
start validation test
0.717857142857
0.885579937304
0.502669039146
0.641316685585
0.718628426383
85.432
#iterations: 320
currently lose_sum: 0.0620107538998127
time_elpased: 2.192
#iterations: 321
currently lose_sum: 0.12192630767822266
time_elpased: 2.136
#iterations: 322
currently lose_sum: 0.1501278430223465
time_elpased: 2.127
#iterations: 323
currently lose_sum: 0.04733553156256676
time_elpased: 2.139
#iterations: 324
currently lose_sum: 0.16332808136940002
time_elpased: 2.187
#iterations: 325
currently lose_sum: 0.07671745121479034
time_elpased: 2.123
#iterations: 326
currently lose_sum: 0.06615617871284485
time_elpased: 2.122
#iterations: 327
currently lose_sum: 0.1161908507347107
time_elpased: 2.133
#iterations: 328
currently lose_sum: 0.07471761852502823
time_elpased: 2.122
#iterations: 329
currently lose_sum: 0.05592280626296997
time_elpased: 2.171
#iterations: 330
currently lose_sum: 0.33841127157211304
time_elpased: 2.129
#iterations: 331
currently lose_sum: 0.06108386442065239
time_elpased: 2.16
#iterations: 332
currently lose_sum: 0.08588160574436188
time_elpased: 2.116
#iterations: 333
currently lose_sum: 0.1456998586654663
time_elpased: 2.07
#iterations: 334
currently lose_sum: 0.09908954799175262
time_elpased: 2.097
#iterations: 335
currently lose_sum: 0.06701280176639557
time_elpased: 2.087
#iterations: 336
currently lose_sum: 0.15678252279758453
time_elpased: 2.137
#iterations: 337
currently lose_sum: 0.18075263500213623
time_elpased: 2.149
#iterations: 338
currently lose_sum: 0.09657611697912216
time_elpased: 2.12
#iterations: 339
currently lose_sum: 0.07193481177091599
time_elpased: 2.079
start validation test
0.719196428571
0.886115444618
0.505338078292
0.643626062323
0.719962945956
89.131
#iterations: 340
currently lose_sum: 0.19218721985816956
time_elpased: 2.111
#iterations: 341
currently lose_sum: 0.037360481917858124
time_elpased: 2.137
#iterations: 342
currently lose_sum: nan
time_elpased: 2.255
train finish final lose is: nan
acc: 0.742
pre: 0.810
rec: 0.636
F1: 0.712
auc: 0.742
