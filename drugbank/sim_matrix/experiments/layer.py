import tensorflow as tf
import numpy as np

def omit_zero(x, max_pool = 0):
	# x is a rank 2 tensor
	intermediate_tensor = tf.reduce_sum(tf.abs(x), 1)
	zero_vector = tf.zeros(shape=(1,1), dtype=tf.float32)
	bool_mask = tf.squeeze(tf.not_equal(intermediate_tensor, zero_vector))
	if(max_pool == 0):
		return tf.reduce_mean(tf.boolean_mask(x, bool_mask), 0)
	else:
		return tf.reduce_max(tf.boolean_mask(x, bool_mask), 0)
def Att(att_V, att_W, trans_sim, max_order, zero_vector = tf.zeros(shape=(1,1), dtype=tf.float32), re_type = 0):
	#trans_sim is a tensor with multi rows are zero
	#get rid of these zero rows
	intermediate_tensor = tf.reduce_sum(tf.abs(trans_sim), 1)
	bool_mask = tf.squeeze(tf.not_equal(intermediate_tensor, zero_vector))
	sim_trans = tf.boolean_mask(trans_sim, bool_mask)

	#do attention
	Att_trans = tf.nn.tanh(tf.matmul(att_V, sim_trans, transpose_b = True))
	Att = tf.nn.softmax(tf.matmul(att_W, Att_trans))
	padding = [[0,0], [0, max_order - tf.shape(Att)[1]]]

	if(re_type == 0):
		return tf.matmul(Att, sim_trans)
	elif(re_type == 1):
		return tf.pad(Att, padding, mode = 'CONSTANT')

def classifier(sim_trans, class_weight, class_b):
	return tf.sigmoid( tf.matmul(sim_trans, class_weight) + class_b )	

def negative_sample(len_drug_comb, preb, ID_drugId, positive_triplets):

	while True:
		te = np.random.choice(len(preb), len_drug_comb, p = preb)
		te_tuple = tuple(sorted([ID_drugId[e] for e in te]))
		if(len(set(te_tuple)) == len(te_tuple) and te_tuple not in positive_triplets):
			break
	te_list = list(te_tuple)
	te_list.append(0)
	return te_list
