import numpy as np
import pandas as pd
import sys
from scipy.sparse import csr_matrix

CSR_file = sys.argv[1]
output = sys.argv[2]

df = pd.read_csv(CSR_file, delimiter = "\t", names = ["row", "col", "value"])

row = df["row"]
col = df["col"]
value = df["value"]

sparse_matrix = csr_matrix((value, (row, col)), [max(row)+1, max(col)+1])
matrix = sparse_matrix.todense()

np.savetxt(output, matrix, fmt='%f', delimiter = ',')
