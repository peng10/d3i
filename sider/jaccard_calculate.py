import sys
import numpy as np
import scipy.io as sio
import pdb

mat_content = sio.loadmat(sys.argv[1])
mat = mat_content['H'].todense()
#pdb.set_trace()
jaccard_simMatrix = np.zeros([mat.shape[0], mat.shape[0]])
for i in xrange(mat.shape[0]):
	for j in xrange(mat.shape[0]):
		jaccard_simMatrix[i, j] = np.sum(np.logical_and(mat[i], mat[j])) / float(np.sum(np.logical_or(mat[i], mat[j])))

np.savetxt(sys.argv[2], jaccard_simMatrix, fmt='%f', delimiter = ',')
