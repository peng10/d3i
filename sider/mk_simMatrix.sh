#!/bin/tcsh

set num_drug = `less $1_CID_list.txt | wc | awk '{print $1}'`
cat $1_CID_list.txt CID_$1_list.txt | awk -v nd="$num_drug" -F ';|,' 'BEGIN{ORS=""}{if(NR<=nd) {a[$1] = NR} else {if($1 in a) {print a[$1]" "; for(i=2;i<=NF;i++) {if(i!=NF) {print $i" "} else {print $i"\n"}} } }}' > $1_temp

#replace $1 with index
set num = `less $1_uniq.txt | wc | awk '{print $1}'`
cat $1_uniq.txt $1_temp | awk -v n="$num" 'BEGIN{ORS=""}{if(NR<=n) {a[$1] = NR} else {print $1" ";for(i=2;i<=NF;i++) {if(i!=NF) {print a[$i]" "} else {print a[$i]"\n"}} } }' > $1_final
rm $1_temp

less $1_final | awk '{for(i=2;i<=NF;i++) {print $1"\t"$i"\t"1}}' > $1_prof_CSR.txt
rm $1_final
