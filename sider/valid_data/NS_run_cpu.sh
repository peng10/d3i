#!/bin/tcsh

set sum_dim = $4
set num_drug = $4
#set temp = $2

foreach folds ($2)
	set num_test = `less PNN_test$folds\_$3.txt | wc | awk '{print $1}'`
	set num_val = `less PN_validation_$folds\_$3.txt | wc | awk '{print $1}'`
	set num_train = `less PN_train_$folds\_$3.txt | wc | awk '{print $1}'`

	foreach sim_file (chem_sim_mat.txt)
		#foreach size (64 128 256 512)
		foreach size (32)
			if($1 == 1) then
				foreach L (16 32 64 128)
					python frame_work_arbitraryOrder_cpu.py --attention $1 --train PN_train_$folds\_$3.txt --test PNN_test$folds\_$3.txt --validation PN_validation_$folds\_$3.txt --num_train_sample $num_train --num_validation_sample $num_val --num_test_sample $num_test --sim_dim $sum_dim --num_drug $num_drug --sim_file $sim_file --L_dim $L --freq_file freq_Ptrain$folds\_$3.txt --input_size $size --out_path ./batch_sample_results/chem_prediction_value_negsam/  > ./batch_sample_results/$3_log_negsam/log_$sim_file\_1N_attention_$1_L_$L\_feature_$size\_0_$folds.txt
					#end
				end
			else
				foreach L (32)
					foreach layer (0 1 3 5)
						python frame_work_arbitraryOrder_cpu.py --attention $1 --train PN_train_$folds\_$3.txt --test PNN_test$folds\_$3.txt --validation PN_validation_$folds\_$3.txt --num_train_sample $num_train --num_validation_sample $num_val --num_test_sample $num_test --sim_dim $sum_dim --num_drug $num_drug --sim_file $sim_file --L_dim $L --freq_file freq_Ptrain$folds\_$3.txt --input_size $size --num_DAN $layer --max_pool 1 --out_path ./batch_sample_results/$3_max_pool_prediction_value_negsam/  > ./batch_sample_results/$3_max_pool_negsam/log_$sim_file\_1N_attention_$1_L_$L\_feature_$size\_$layer\_$folds.txt
						python frame_work_arbitraryOrder_cpu.py --attention $1 --train PN_train_$folds\_$3.txt --test PNN_test$folds\_$3.txt --validation PN_validation_$folds\_$3.txt --num_train_sample $num_train --num_validation_sample $num_val --num_test_sample $num_test --sim_dim $sum_dim --num_drug $num_drug --sim_file $sim_file --L_dim $L --freq_file freq_Ptrain$folds\_$3.txt --input_size $size --num_DAN $layer --max_pool 0 --out_path  ./batch_sample_results/$3_mean_prediction_value_negsam/  > ./batch_sample_results/$3_mean_negsam/log_$sim_file\_1N_attention_$1_L_$L\_feature_$size\_$layer\_$folds.txt
					end
				end
			endif
		end
	end
end
