#!/bin/tcsh
foreach folds (0 1 2 3 4)
        foreach step (test)
                set num_sample = `less PN_$step\_$folds\_$1.txt | wc | awk '{print $1}'`
                if($step == validation || $step == test) then
                        cat PN_test_$folds\_$1.txt PN_train_$folds\_$1.txt  PN_validation_$folds\_$1.txt > temp
                        less temp | awk '{for(i=1;i<=NF-1;i++) {print $i} }' | sort | uniq -c | sort -k2n > freq_PN$step$folds\_$1.txt
                        less PN_$step\_$folds\_$1.txt | awk '{if($NF == 1) {print $0} }' > P_set.txt
                        less temp | awk '{if($NF == 1) {print $0} }' > tem
                        python negsamp.py --num_drugs $2 --avoid_file tem --train_file P_set.txt --freq_file freq_PN$step$folds\_$1.txt --outfile PN_$step\_N_set$folds\_$1.txt
                        cat P_set.txt PN_$step\_N_set$folds\_$1.txt > PNN_$step$folds\_$1.txt
                        rm temp P_set.txt
                else
                        less PN_$step\_$folds\_$1.txt | awk '{for(i=1;i<=NF-1;i++) {print $i} }' | sort | uniq -c | sort -k2n > freq_PN$step$folds\_$1.txt
                        python negsamp.py --avoid_file PN_$step\_$folds\_$1.txt --train_file PN_$step\_$folds\_$1.txt --freq_file freq_PN$step$folds\_$1.txt --outfile PN_$step\_N_set$folds\_$1.txt
                endif
                cat PN_$step\_$folds\_$1.txt PN_$step\_N_set$folds\_$1.txt > PNN_entire_$step$folds\_$1.txt
		rm PN_$step\_N_set$folds\_$1.txt
        end
end
