#!/bin/tcsh

cat $1_sim_mat.txt_max_0 $1_sim_mat.txt_max_1 $1_sim_mat.txt_max_3 $1_sim_mat.txt_max_5 > $1_max.txt
cat $1_sim_mat.txt_max_negsam0 $1_sim_mat.txt_max_negsam1 $1_sim_mat.txt_max_negsam3 $1_sim_mat.txt_max_negsam5 > $1_max_negsam.txt

cat $1_sim_mat.txt_mean_0 $1_sim_mat.txt_mean_1 $1_sim_mat.txt_mean_3 $1_sim_mat.txt_mean_5 > $1_mean.txt
cat $1_sim_mat.txt_mean_negsam0 $1_sim_mat.txt_mean_negsam1 $1_sim_mat.txt_mean_negsam3 $1_sim_mat.txt_mean_negsam5 > $1_mean_negsam.txt

cat $1_max.txt $1_mean.txt $1_sim_mat.txt_att > $1_results.txt
cat $1_max_negsam.txt $1_mean_negsam.txt $1_sim_mat.txt_negsam_att > $1_negsam_results.txt
