#!/bin/tcsh
foreach folds (0 1 2 3 4)
	set file = ../$2_max_pool_prediction_value/prediction_value_0_$2_sim_mat.txt_PN_test_$folds\_$2.txt_32_128_1_1
	set num = `less $file | wc | awk '{print $1}'` 
	paste PN_test_$folds\_$2.txt $file | awk -v "order=$1" -v "num=$num" '{if(NR<=num && NF-2 >= order)print $(NF-1)}' > ground_truth$folds
	set num_truth = `less ground_truth$folds | wc | awk '{print $1}'`
	paste PN_test_$folds\_$2.txt $file | awk -v "order=$1" -v "num=$num" '{if(NR<=num && NF-2 >= order)print $NF}' > prediction_value$folds
	set num_pred = `less prediction_value$folds | wc | awk '{print $1}'`
	python evaluation.py --ground_truth ground_truth$folds --prediction_value prediction_value$folds --out_path temp$folds
end
paste temp0 temp1 temp2 temp3 temp4 | awk -v "nt=$num_truth" '{mean = ($1+$2+$3+$4+$5)/5.0;sum=0;for(i=1;i<=5;i=i+1) {sum=sum+($i-mean)^2};std=sqrt(sum/4.0); printf ("%.4f\tstd:\t%.4f\n", mean ,std)}' > evaluation$1.txt
