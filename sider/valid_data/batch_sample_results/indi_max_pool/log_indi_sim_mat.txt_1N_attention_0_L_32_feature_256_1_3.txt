start to construct computing graph
graph construct over
1854
epochs start
#iterations: 0
currently lose_sum: 0.649877667427063
time_elpased: 2.522
#iterations: 1
currently lose_sum: 0.5900124311447144
time_elpased: 3.053
#iterations: 2
currently lose_sum: 0.6035962104797363
time_elpased: 3.679
#iterations: 3
currently lose_sum: 0.5467432737350464
time_elpased: 3.494
#iterations: 4
currently lose_sum: 0.5205186009407043
time_elpased: 3.563
#iterations: 5
currently lose_sum: 0.4710548520088196
time_elpased: 3.496
#iterations: 6
currently lose_sum: 0.5130202174186707
time_elpased: 3.485
#iterations: 7
currently lose_sum: 0.37048783898353577
time_elpased: 3.452
#iterations: 8
currently lose_sum: 0.3419143557548523
time_elpased: 3.557
#iterations: 9
currently lose_sum: 0.4318322241306305
time_elpased: 3.516
#iterations: 10
currently lose_sum: 0.28395289182662964
time_elpased: 3.663
#iterations: 11
currently lose_sum: 0.3179781138896942
time_elpased: 3.51
#iterations: 12
currently lose_sum: 0.32844191789627075
time_elpased: 3.444
#iterations: 13
currently lose_sum: 0.33635005354881287
time_elpased: 3.627
#iterations: 14
currently lose_sum: 0.38094082474708557
time_elpased: 3.439
#iterations: 15
currently lose_sum: 0.3469325006008148
time_elpased: 3.49
#iterations: 16
currently lose_sum: 0.38641566038131714
time_elpased: 3.564
#iterations: 17
currently lose_sum: 0.27859216928482056
time_elpased: 3.607
#iterations: 18
currently lose_sum: 0.43538898229599
time_elpased: 3.475
#iterations: 19
currently lose_sum: 0.21398910880088806
time_elpased: 3.702
start validation test
0.7675
0.828793774319
0.690437601297
0.753315649867
0.769747102535
15.248
#iterations: 20
currently lose_sum: 0.31602761149406433
time_elpased: 3.383
#iterations: 21
currently lose_sum: 0.4991282522678375
time_elpased: 3.325
#iterations: 22
currently lose_sum: 0.4562777578830719
time_elpased: 3.489
#iterations: 23
currently lose_sum: 0.21083660423755646
time_elpased: 3.439
#iterations: 24
currently lose_sum: 0.16396605968475342
time_elpased: 3.458
#iterations: 25
currently lose_sum: 0.21859702467918396
time_elpased: 3.486
#iterations: 26
currently lose_sum: 0.22581744194030762
time_elpased: 3.433
#iterations: 27
currently lose_sum: 0.43448877334594727
time_elpased: 3.459
#iterations: 28
currently lose_sum: 0.5330540537834167
time_elpased: 3.468
#iterations: 29
currently lose_sum: 0.11171970516443253
time_elpased: 3.347
#iterations: 30
currently lose_sum: 0.19584348797798157
time_elpased: 3.451
#iterations: 31
currently lose_sum: 0.4157668650150299
time_elpased: 3.443
#iterations: 32
currently lose_sum: 0.18236683309078217
time_elpased: 3.371
#iterations: 33
currently lose_sum: 0.27515408396720886
time_elpased: 3.424
#iterations: 34
currently lose_sum: 0.1836092174053192
time_elpased: 3.384
#iterations: 35
currently lose_sum: 0.13643254339694977
time_elpased: 3.394
#iterations: 36
currently lose_sum: 0.2719171643257141
time_elpased: 3.49
#iterations: 37
currently lose_sum: 0.3121020793914795
time_elpased: 3.438
#iterations: 38
currently lose_sum: 0.4165373742580414
time_elpased: 3.386
#iterations: 39
currently lose_sum: 0.25618776679039
time_elpased: 3.569
start validation test
0.738333333333
0.85150812065
0.594813614263
0.700381679389
0.742518299413
20.933
#iterations: 40
currently lose_sum: 0.23484644293785095
time_elpased: 3.569
#iterations: 41
currently lose_sum: 0.2572612166404724
time_elpased: 3.353
#iterations: 42
currently lose_sum: 0.35802939534187317
time_elpased: 3.52
#iterations: 43
currently lose_sum: 0.1568996012210846
time_elpased: 3.525
#iterations: 44
currently lose_sum: 0.2665986120700836
time_elpased: 3.513
#iterations: 45
currently lose_sum: 0.10130540281534195
time_elpased: 3.344
#iterations: 46
currently lose_sum: 0.1684582233428955
time_elpased: 3.525
#iterations: 47
currently lose_sum: 0.09215308725833893
time_elpased: 3.379
#iterations: 48
currently lose_sum: 0.2907565236091614
time_elpased: 3.43
#iterations: 49
currently lose_sum: 0.21045580506324768
time_elpased: 3.451
#iterations: 50
currently lose_sum: 0.23891901969909668
time_elpased: 3.492
#iterations: 51
currently lose_sum: 0.24814005196094513
time_elpased: 3.461
#iterations: 52
currently lose_sum: 0.23017799854278564
time_elpased: 3.408
#iterations: 53
currently lose_sum: 0.3561672270298004
time_elpased: 3.459
#iterations: 54
currently lose_sum: 0.17934802174568176
time_elpased: 3.518
#iterations: 55
currently lose_sum: 0.12519323825836182
time_elpased: 3.51
#iterations: 56
currently lose_sum: 0.1773577630519867
time_elpased: 3.399
#iterations: 57
currently lose_sum: 0.06417381018400192
time_elpased: 3.533
#iterations: 58
currently lose_sum: 0.21205277740955353
time_elpased: 3.41
#iterations: 59
currently lose_sum: 0.18525941669940948
time_elpased: 3.48
start validation test
0.690833333333
0.915540540541
0.439222042139
0.593647316539
0.698170197742
33.052
#iterations: 60
currently lose_sum: 0.18127699196338654
time_elpased: 3.414
#iterations: 61
currently lose_sum: 0.18801777064800262
time_elpased: 3.438
#iterations: 62
currently lose_sum: 0.22827260196208954
time_elpased: 3.403
#iterations: 63
currently lose_sum: 0.15837909281253815
time_elpased: 3.477
#iterations: 64
currently lose_sum: 0.2531884014606476
time_elpased: 3.564
#iterations: 65
currently lose_sum: 0.24667644500732422
time_elpased: 3.424
#iterations: 66
currently lose_sum: 0.1447395384311676
time_elpased: 3.476
#iterations: 67
currently lose_sum: 0.214455246925354
time_elpased: 3.427
#iterations: 68
currently lose_sum: 0.1468590945005417
time_elpased: 3.338
#iterations: 69
currently lose_sum: 0.14597144722938538
time_elpased: 3.358
#iterations: 70
currently lose_sum: nan
time_elpased: 3.407
train finish final lose is: nan
acc: 0.772
pre: 0.833
rec: 0.695
F1: 0.758
auc: 0.774
