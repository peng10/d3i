start to construct computing graph
graph construct over
1852
epochs start
#iterations: 0
currently lose_sum: 0.6526390314102173
time_elpased: 6.533
#iterations: 1
currently lose_sum: 0.6077533960342407
time_elpased: 6.693
#iterations: 2
currently lose_sum: 0.6491712927818298
time_elpased: 6.729
#iterations: 3
currently lose_sum: 0.5153979659080505
time_elpased: 6.534
#iterations: 4
currently lose_sum: 0.7391130328178406
time_elpased: 6.692
#iterations: 5
currently lose_sum: 0.49447450041770935
time_elpased: 6.785
#iterations: 6
currently lose_sum: 0.4106331467628479
time_elpased: 6.717
#iterations: 7
currently lose_sum: 0.2715352177619934
time_elpased: 6.92
#iterations: 8
currently lose_sum: 0.48434358835220337
time_elpased: 7.034
#iterations: 9
currently lose_sum: 0.5083016157150269
time_elpased: 6.607
#iterations: 10
currently lose_sum: 0.3402298092842102
time_elpased: 6.623
#iterations: 11
currently lose_sum: 0.2501044273376465
time_elpased: 6.541
#iterations: 12
currently lose_sum: 0.4060456156730652
time_elpased: 6.792
#iterations: 13
currently lose_sum: 0.5120981931686401
time_elpased: 6.7
#iterations: 14
currently lose_sum: 0.3123844265937805
time_elpased: 6.598
#iterations: 15
currently lose_sum: 0.36199837923049927
time_elpased: 6.622
#iterations: 16
currently lose_sum: 0.29889756441116333
time_elpased: 6.598
#iterations: 17
currently lose_sum: 0.2897595167160034
time_elpased: 6.605
#iterations: 18
currently lose_sum: 0.32751327753067017
time_elpased: 6.56
#iterations: 19
currently lose_sum: 0.30939286947250366
time_elpased: 6.572
start validation test
0.745833333333
0.850111856823
0.614886731392
0.713615023474
0.749883228239
18.863
#iterations: 20
currently lose_sum: 0.29109078645706177
time_elpased: 6.347
#iterations: 21
currently lose_sum: 0.2527165412902832
time_elpased: 6.484
#iterations: 22
currently lose_sum: 0.3166283667087555
time_elpased: 6.749
#iterations: 23
currently lose_sum: 0.28363195061683655
time_elpased: 6.461
#iterations: 24
currently lose_sum: 0.23540250957012177
time_elpased: 6.627
#iterations: 25
currently lose_sum: 0.2897087633609772
time_elpased: 6.802
#iterations: 26
currently lose_sum: 0.3375835418701172
time_elpased: 6.731
#iterations: 27
currently lose_sum: 0.3043452203273773
time_elpased: 6.52
#iterations: 28
currently lose_sum: 0.3134244382381439
time_elpased: 6.488
#iterations: 29
currently lose_sum: 0.32326242327690125
time_elpased: 6.346
#iterations: 30
currently lose_sum: 0.20774471759796143
time_elpased: 6.373
#iterations: 31
currently lose_sum: 0.14096492528915405
time_elpased: 6.369
#iterations: 32
currently lose_sum: 0.2662930190563202
time_elpased: 6.483
#iterations: 33
currently lose_sum: 0.2630489468574524
time_elpased: 6.464
#iterations: 34
currently lose_sum: 0.295593798160553
time_elpased: 6.287
#iterations: 35
currently lose_sum: 0.6388381123542786
time_elpased: 6.425
#iterations: 36
currently lose_sum: 0.14911988377571106
time_elpased: 6.529
#iterations: 37
currently lose_sum: 0.35183486342430115
time_elpased: 6.479
#iterations: 38
currently lose_sum: 0.18487435579299927
time_elpased: 6.351
#iterations: 39
currently lose_sum: 0.1493125557899475
time_elpased: 6.477
start validation test
0.766666666667
0.870614035088
0.642394822006
0.739292364991
0.77051012578
19.289
#iterations: 40
currently lose_sum: 0.3278094232082367
time_elpased: 6.609
#iterations: 41
currently lose_sum: 0.15198533236980438
time_elpased: 6.859
#iterations: 42
currently lose_sum: 0.12970972061157227
time_elpased: 6.469
#iterations: 43
currently lose_sum: 0.17987266182899475
time_elpased: 6.64
#iterations: 44
currently lose_sum: 0.2733105421066284
time_elpased: 6.564
#iterations: 45
currently lose_sum: 0.3794953525066376
time_elpased: 6.396
#iterations: 46
currently lose_sum: 0.28947004675865173
time_elpased: 6.424
#iterations: 47
currently lose_sum: 0.3769848346710205
time_elpased: 5.781
#iterations: 48
currently lose_sum: 0.2565849721431732
time_elpased: 3.813
#iterations: 49
currently lose_sum: 0.2583630084991455
time_elpased: 4.392
#iterations: 50
currently lose_sum: 0.22935035824775696
time_elpased: 6.22
#iterations: 51
currently lose_sum: 0.18628177046775818
time_elpased: 6.283
#iterations: 52
currently lose_sum: 0.17738938331604004
time_elpased: 6.243
#iterations: 53
currently lose_sum: 0.1607757806777954
time_elpased: 6.36
#iterations: 54
currently lose_sum: 0.1349058598279953
time_elpased: 6.328
#iterations: 55
currently lose_sum: 0.09825193881988525
time_elpased: 6.158
#iterations: 56
currently lose_sum: 0.2974623739719391
time_elpased: 6.201
#iterations: 57
currently lose_sum: 0.3005695343017578
time_elpased: 6.312
#iterations: 58
currently lose_sum: 0.20229403674602509
time_elpased: 6.305
#iterations: 59
currently lose_sum: 0.11694647371768951
time_elpased: 6.292
start validation test
0.730833333333
0.889182058047
0.545307443366
0.676028084253
0.736571247456
25.881
#iterations: 60
currently lose_sum: 0.14310160279273987
time_elpased: 6.245
#iterations: 61
currently lose_sum: 0.1754070222377777
time_elpased: 6.259
#iterations: 62
currently lose_sum: 0.08259912580251694
time_elpased: 6.232
#iterations: 63
currently lose_sum: 0.24413490295410156
time_elpased: 6.189
#iterations: 64
currently lose_sum: 0.27330732345581055
time_elpased: 6.265
#iterations: 65
currently lose_sum: 0.28724583983421326
time_elpased: 6.393
#iterations: 66
currently lose_sum: 0.3472292423248291
time_elpased: 6.406
#iterations: 67
currently lose_sum: 0.191209077835083
time_elpased: 6.409
#iterations: 68
currently lose_sum: 0.08146347105503082
time_elpased: 6.482
#iterations: 69
currently lose_sum: nan
time_elpased: 6.847
train finish final lose is: nan
acc: 0.743
pre: 0.860
rec: 0.597
F1: 0.705
auc: 0.747
