#!/bin/tcsh

foreach col (1 4 7 10 13)
	foreach num (1 2 3 4)
		set temp=`expr $num - 1`
		set start=`expr $num \* 3 + $temp \* 5 + 1`
		less ../batch_reports/$1_results.txt | awk -v a="$start" -v b="$col" '{if(NR>=a && NR<(a+5)) {print $b} }' > temp_$num.txt
	end
	paste -d "& " temp_1.txt /dev/null temp_2.txt /dev/null temp_3.txt /dev/null temp_4.txt > col$col.txt
	rm temp_1.txt temp_2.txt temp_3.txt temp_4.txt
end

paste -d "&&" col1.txt /dev/null col4.txt /dev/null col7.txt /dev/null col10.txt /dev/null col13.txt > ../batch_reports/$1_table_max.txt
rm col*

set base=`expr 33`
foreach col (1 4 7 10 13)
        foreach num (1 2 3 4)
                set temp=`expr $num - 1`
                set start=`expr $num \* 3 + $temp \* 5 + 1 + $base`
                less ../batch_reports/$1_results.txt | awk -v a="$start" -v b="$col" '{if(NR>=a && NR<(a+5)) {print $b} }' > temp_$num.txt
        end
        paste -d "& " temp_1.txt /dev/null temp_2.txt /dev/null temp_3.txt /dev/null temp_4.txt > col$col.txt
        rm temp_1.txt temp_2.txt temp_3.txt temp_4.txt
end

paste -d "&&" col1.txt /dev/null col4.txt /dev/null col7.txt /dev/null col10.txt /dev/null col13.txt > ../batch_reports/$1_table_att.txt
rm col*
