start to construct computing graph
graph construct over
673
epochs start
#iterations: 0
currently lose_sum: 0.6973140239715576
time_elpased: 0.871
#iterations: 1
currently lose_sum: 0.6877856850624084
time_elpased: 0.862
#iterations: 2
currently lose_sum: 0.6774644255638123
time_elpased: 0.843
#iterations: 3
currently lose_sum: 0.7088239789009094
time_elpased: 0.863
#iterations: 4
currently lose_sum: 0.6896204352378845
time_elpased: 0.853
#iterations: 5
currently lose_sum: 0.6668811440467834
time_elpased: 0.824
#iterations: 6
currently lose_sum: 0.6716286540031433
time_elpased: 0.908
#iterations: 7
currently lose_sum: 0.6566799879074097
time_elpased: 0.896
#iterations: 8
currently lose_sum: 0.6941913962364197
time_elpased: 0.875
#iterations: 9
currently lose_sum: 0.6771043539047241
time_elpased: 0.832
#iterations: 10
currently lose_sum: 0.681258499622345
time_elpased: 0.824
#iterations: 11
currently lose_sum: 0.8206793665885925
time_elpased: 0.804
#iterations: 12
currently lose_sum: 0.6650115251541138
time_elpased: 0.812
#iterations: 13
currently lose_sum: 0.5938433408737183
time_elpased: 0.86
#iterations: 14
currently lose_sum: 0.6742700338363647
time_elpased: 0.814
#iterations: 15
currently lose_sum: 0.6108922362327576
time_elpased: 0.82
#iterations: 16
currently lose_sum: 0.6350796818733215
time_elpased: 0.806
#iterations: 17
currently lose_sum: 0.6744629740715027
time_elpased: 0.826
#iterations: 18
currently lose_sum: 2.688779830932617
time_elpased: 0.823
#iterations: 19
currently lose_sum: 0.6519700288772583
time_elpased: 0.806
start validation test
0.609090909091
0.608333333333
0.651785714286
0.629310344828
0.60830026455
7.320
#iterations: 20
currently lose_sum: 0.7032941579818726
time_elpased: 0.809
#iterations: 21
currently lose_sum: 0.6440756320953369
time_elpased: 0.84
#iterations: 22
currently lose_sum: 0.5948582291603088
time_elpased: 0.818
#iterations: 23
currently lose_sum: 0.6310299634933472
time_elpased: 0.848
#iterations: 24
currently lose_sum: 0.6387017965316772
time_elpased: 0.836
#iterations: 25
currently lose_sum: 0.5135593414306641
time_elpased: 0.837
#iterations: 26
currently lose_sum: 0.6268044710159302
time_elpased: 0.844
#iterations: 27
currently lose_sum: 0.5825024843215942
time_elpased: 0.862
#iterations: 28
currently lose_sum: 0.5338142514228821
time_elpased: 0.827
#iterations: 29
currently lose_sum: 0.6732396483421326
time_elpased: 0.815
#iterations: 30
currently lose_sum: 0.6425231099128723
time_elpased: 0.856
#iterations: 31
currently lose_sum: 0.5208356380462646
time_elpased: 0.836
#iterations: 32
currently lose_sum: 0.4814368188381195
time_elpased: 0.815
#iterations: 33
currently lose_sum: 0.6637367010116577
time_elpased: 0.836
#iterations: 34
currently lose_sum: 0.5532947182655334
time_elpased: 0.812
#iterations: 35
currently lose_sum: 0.5001510381698608
time_elpased: 0.813
#iterations: 36
currently lose_sum: 0.6270758509635925
time_elpased: 0.833
#iterations: 37
currently lose_sum: 0.5620087385177612
time_elpased: 0.854
#iterations: 38
currently lose_sum: 0.5960701704025269
time_elpased: 0.876
#iterations: 39
currently lose_sum: 0.5668020248413086
time_elpased: 0.827
start validation test
0.668181818182
0.627450980392
0.857142857143
0.724528301887
0.664682539683
6.797
#iterations: 40
currently lose_sum: 0.49441295862197876
time_elpased: 0.851
#iterations: 41
currently lose_sum: 0.6746336817741394
time_elpased: 0.823
#iterations: 42
currently lose_sum: 0.5631924271583557
time_elpased: 0.838
#iterations: 43
currently lose_sum: 0.5387185215950012
time_elpased: 0.875
#iterations: 44
currently lose_sum: 0.4356592297554016
time_elpased: 0.806
#iterations: 45
currently lose_sum: 0.5280235409736633
time_elpased: 0.839
#iterations: 46
currently lose_sum: 0.46841734647750854
time_elpased: 0.849
#iterations: 47
currently lose_sum: 0.38733386993408203
time_elpased: 0.848
#iterations: 48
currently lose_sum: 0.6920911073684692
time_elpased: 0.838
#iterations: 49
currently lose_sum: 0.5507699251174927
time_elpased: 0.818
#iterations: 50
currently lose_sum: 0.6864179372787476
time_elpased: 0.826
#iterations: 51
currently lose_sum: 0.5651990175247192
time_elpased: 0.832
#iterations: 52
currently lose_sum: 0.6910697221755981
time_elpased: 0.84
#iterations: 53
currently lose_sum: 0.602953314781189
time_elpased: 0.831
#iterations: 54
currently lose_sum: 0.6920965313911438
time_elpased: 0.858
#iterations: 55
currently lose_sum: 0.6905081272125244
time_elpased: 0.923
#iterations: 56
currently lose_sum: 0.6095101237297058
time_elpased: 0.907
#iterations: 57
currently lose_sum: 0.6426025629043579
time_elpased: 0.898
#iterations: 58
currently lose_sum: 0.68748539686203
time_elpased: 0.912
#iterations: 59
currently lose_sum: 0.4569084048271179
time_elpased: 0.829
start validation test
0.670454545455
0.63122923588
0.848214285714
0.72380952381
0.667162698413
6.592
#iterations: 60
currently lose_sum: 0.5825685262680054
time_elpased: 0.819
#iterations: 61
currently lose_sum: 0.6686608195304871
time_elpased: 0.848
#iterations: 62
currently lose_sum: 0.5260170698165894
time_elpased: 0.85
#iterations: 63
currently lose_sum: 0.5495370626449585
time_elpased: 0.827
#iterations: 64
currently lose_sum: 0.5510063171386719
time_elpased: 0.832
#iterations: 65
currently lose_sum: 0.5520198941230774
time_elpased: 0.814
#iterations: 66
currently lose_sum: 0.553212583065033
time_elpased: 0.82
#iterations: 67
currently lose_sum: 0.4696931838989258
time_elpased: 0.838
#iterations: 68
currently lose_sum: 0.47509974241256714
time_elpased: 0.824
#iterations: 69
currently lose_sum: 0.4861164093017578
time_elpased: 0.878
#iterations: 70
currently lose_sum: 0.5380138158798218
time_elpased: 0.808
#iterations: 71
currently lose_sum: 0.4251052737236023
time_elpased: 0.832
#iterations: 72
currently lose_sum: 0.5608901381492615
time_elpased: 0.844
#iterations: 73
currently lose_sum: 0.49740153551101685
time_elpased: 0.855
#iterations: 74
currently lose_sum: 0.4615468978881836
time_elpased: 0.864
#iterations: 75
currently lose_sum: 0.48738908767700195
time_elpased: 0.831
#iterations: 76
currently lose_sum: 0.5012577772140503
time_elpased: 0.843
#iterations: 77
currently lose_sum: 0.41080188751220703
time_elpased: 0.815
#iterations: 78
currently lose_sum: 0.6912261247634888
time_elpased: 0.803
#iterations: 79
currently lose_sum: 0.6639456748962402
time_elpased: 0.837
start validation test
0.611363636364
0.569190600522
0.973214285714
0.718286655684
0.604662698413
7.305
#iterations: 80
currently lose_sum: 0.38341403007507324
time_elpased: 0.82
#iterations: 81
currently lose_sum: 0.5893245935440063
time_elpased: 0.806
#iterations: 82
currently lose_sum: 0.43660178780555725
time_elpased: 0.809
#iterations: 83
currently lose_sum: 0.6323229074478149
time_elpased: 0.814
#iterations: 84
currently lose_sum: 0.4745749831199646
time_elpased: 0.832
#iterations: 85
currently lose_sum: 0.6394108533859253
time_elpased: 0.826
#iterations: 86
currently lose_sum: 0.46383267641067505
time_elpased: 0.839
#iterations: 87
currently lose_sum: 0.6073083281517029
time_elpased: 0.852
#iterations: 88
currently lose_sum: 0.6817716956138611
time_elpased: 0.825
#iterations: 89
currently lose_sum: 0.560888409614563
time_elpased: 0.824
#iterations: 90
currently lose_sum: 0.45487433671951294
time_elpased: 0.83
#iterations: 91
currently lose_sum: 0.30962300300598145
time_elpased: 0.817
#iterations: 92
currently lose_sum: 0.6084734201431274
time_elpased: 0.82
#iterations: 93
currently lose_sum: 0.3958124816417694
time_elpased: 0.842
#iterations: 94
currently lose_sum: 0.4731922149658203
time_elpased: 0.834
#iterations: 95
currently lose_sum: 0.3553641438484192
time_elpased: 0.807
#iterations: 96
currently lose_sum: 0.32351481914520264
time_elpased: 0.82
#iterations: 97
currently lose_sum: 0.7148418426513672
time_elpased: 0.813
#iterations: 98
currently lose_sum: 0.5533417463302612
time_elpased: 0.803
#iterations: 99
currently lose_sum: 0.3720359206199646
time_elpased: 0.83
start validation test
0.665909090909
0.74213836478
0.526785714286
0.616187989556
0.668485449735
7.818
#iterations: 100
currently lose_sum: 0.6100902557373047
time_elpased: 0.804
#iterations: 101
currently lose_sum: 0.37147897481918335
time_elpased: 0.826
#iterations: 102
currently lose_sum: 0.6055948138237
time_elpased: 0.841
#iterations: 103
currently lose_sum: 0.5423288345336914
time_elpased: 0.823
#iterations: 104
currently lose_sum: 0.4192277491092682
time_elpased: 0.821
#iterations: 105
currently lose_sum: 0.5016767382621765
time_elpased: 0.878
#iterations: 106
currently lose_sum: 0.42568618059158325
time_elpased: 0.891
#iterations: 107
currently lose_sum: 0.49236613512039185
time_elpased: 0.863
#iterations: 108
currently lose_sum: 0.3434557318687439
time_elpased: 0.874
#iterations: 109
currently lose_sum: 0.6194882392883301
time_elpased: 0.842
#iterations: 110
currently lose_sum: 0.35641369223594666
time_elpased: 0.815
#iterations: 111
currently lose_sum: 0.4293091893196106
time_elpased: 0.818
#iterations: 112
currently lose_sum: 0.4937022626399994
time_elpased: 0.824
#iterations: 113
currently lose_sum: 0.3905399441719055
time_elpased: 0.803
#iterations: 114
currently lose_sum: 0.7761543393135071
time_elpased: 0.819
#iterations: 115
currently lose_sum: 0.4781981408596039
time_elpased: 0.826
#iterations: 116
currently lose_sum: 0.5397331714630127
time_elpased: 0.823
#iterations: 117
currently lose_sum: 0.45988044142723083
time_elpased: 0.838
#iterations: 118
currently lose_sum: 0.5675028562545776
time_elpased: 0.843
#iterations: 119
currently lose_sum: 0.3792593479156494
time_elpased: 0.869
start validation test
0.686363636364
0.752941176471
0.571428571429
0.649746192893
0.688492063492
7.106
#iterations: 120
currently lose_sum: 0.6999596357345581
time_elpased: 0.846
#iterations: 121
currently lose_sum: 0.505102813243866
time_elpased: 0.863
#iterations: 122
currently lose_sum: 0.5275616645812988
time_elpased: 0.891
#iterations: 123
currently lose_sum: 0.38851702213287354
time_elpased: 0.871
#iterations: 124
currently lose_sum: 0.44560378789901733
time_elpased: 0.847
#iterations: 125
currently lose_sum: 0.46450528502464294
time_elpased: 0.826
#iterations: 126
currently lose_sum: 0.6374595761299133
time_elpased: 0.857
#iterations: 127
currently lose_sum: 0.5781322717666626
time_elpased: 0.84
#iterations: 128
currently lose_sum: 0.5884388089179993
time_elpased: 0.817
#iterations: 129
currently lose_sum: 0.6238046884536743
time_elpased: 0.831
#iterations: 130
currently lose_sum: 0.6191592216491699
time_elpased: 0.804
#iterations: 131
currently lose_sum: 0.5335628390312195
time_elpased: 0.808
#iterations: 132
currently lose_sum: 0.4064829349517822
time_elpased: 0.819
#iterations: 133
currently lose_sum: 0.5053182244300842
time_elpased: 0.804
#iterations: 134
currently lose_sum: 0.4938852787017822
time_elpased: 0.831
#iterations: 135
currently lose_sum: 0.5276898145675659
time_elpased: 0.838
#iterations: 136
currently lose_sum: 0.23129978775978088
time_elpased: 0.841
#iterations: 137
currently lose_sum: 0.7736804485321045
time_elpased: 0.799
#iterations: 138
currently lose_sum: 0.5129573941230774
time_elpased: 0.855
#iterations: 139
currently lose_sum: 0.6554053425788879
time_elpased: 0.825
start validation test
0.706818181818
0.661016949153
0.870535714286
0.751445086705
0.703786375661
7.213
#iterations: 140
currently lose_sum: 0.4299543499946594
time_elpased: 0.841
#iterations: 141
currently lose_sum: 0.326747864484787
time_elpased: 0.852
#iterations: 142
currently lose_sum: 0.38846784830093384
time_elpased: 0.845
#iterations: 143
currently lose_sum: 0.6041070222854614
time_elpased: 0.816
#iterations: 144
currently lose_sum: 0.45920997858047485
time_elpased: 0.838
#iterations: 145
currently lose_sum: 0.5044593811035156
time_elpased: 0.859
#iterations: 146
currently lose_sum: 0.35557815432548523
time_elpased: 0.798
#iterations: 147
currently lose_sum: 0.3434426486492157
time_elpased: 0.806
#iterations: 148
currently lose_sum: 0.3581816554069519
time_elpased: 0.847
#iterations: 149
currently lose_sum: 0.4870762825012207
time_elpased: 0.859
#iterations: 150
currently lose_sum: 0.4766899049282074
time_elpased: 0.832
#iterations: 151
currently lose_sum: 0.45175737142562866
time_elpased: 0.82
#iterations: 152
currently lose_sum: 0.48278552293777466
time_elpased: 0.811
#iterations: 153
currently lose_sum: 0.43961256742477417
time_elpased: 0.811
#iterations: 154
currently lose_sum: 0.4062132239341736
time_elpased: 0.835
#iterations: 155
currently lose_sum: 0.4577203691005707
time_elpased: 0.816
#iterations: 156
currently lose_sum: 0.3774004578590393
time_elpased: 0.848
#iterations: 157
currently lose_sum: 0.43665599822998047
time_elpased: 0.84
#iterations: 158
currently lose_sum: 0.30121931433677673
time_elpased: 0.843
#iterations: 159
currently lose_sum: 0.32452279329299927
time_elpased: 0.84
start validation test
0.645454545455
0.774193548387
0.428571428571
0.551724137931
0.649470899471
7.892
#iterations: 160
currently lose_sum: 0.539230227470398
time_elpased: 0.826
#iterations: 161
currently lose_sum: 0.5126506090164185
time_elpased: 0.823
#iterations: 162
currently lose_sum: 0.3279208540916443
time_elpased: 0.882
#iterations: 163
currently lose_sum: 0.2772476077079773
time_elpased: 0.841
#iterations: 164
currently lose_sum: 0.4045720100402832
time_elpased: 0.796
#iterations: 165
currently lose_sum: 0.295897513628006
time_elpased: 0.816
#iterations: 166
currently lose_sum: 0.6504196524620056
time_elpased: 0.87
#iterations: 167
currently lose_sum: 0.9797306060791016
time_elpased: 0.835
#iterations: 168
currently lose_sum: 0.43180742859840393
time_elpased: 0.816
#iterations: 169
currently lose_sum: nan
time_elpased: 0.904
train finish final lose is: nan
acc: 0.700
pre: 0.648
rec: 0.897
F1: 0.753
auc: 0.696
