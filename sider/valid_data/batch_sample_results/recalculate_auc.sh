#!/bin/tcsh
foreach folds (0 1 2 3 4)
	set num_pred = `less ./$1\_$2\_prediction_value/hypothesis0_$1\_sim_mat.txt_PN_test_$folds\_$1.txt_32_32_1_0 | wc | awk '{print $1}'`
	echo $num_pred
	set num_pred_negsam = `less ./$1\_$2\_prediction_value_negsam/hypothesis0_$1\_sim_mat.txt_PN_test_$folds\_$1.txt_32_32_1_0 | wc | awk '{print $1}'`
	echo $num_pred_negsam
	less ../PN_test_$folds\_$1.txt | awk -v "num=$num_pred" '{if(NR<=num) {print $NF} }' > ground$folds
	less ../PNN_test$folds\_$1.txt | awk -v "num=$num_pred_negsam" '{if(NR<=num) {print $NF} }' > ground_negsam$folds
	foreach layer (0 1 3 5)
		foreach dim (32 64 128 256 512)
			python evaluation.py --ground_truth ground$folds --prediction_value ./$1\_$2\_prediction_value/hypothesis0_$1\_sim_mat.txt_PN_test_$folds\_$1.txt_32_$dim\_1_$layer --out_path true.txt
			python evaluation.py --ground_truth ground_negsam$folds --prediction_value ./$1\_$2\_prediction_value_negsam/hypothesis0_$1\_sim_mat.txt_PN_test_$folds\_$1.txt_32_$dim\_1_$layer --out_path negsam.txt
			set true_auc = `less true.txt | awk '{printf ("%.3f\n", $1)}'`
			set negsam_auc = `less negsam.txt | awk '{printf ("%.3f\n", $1)}'`
			less $1\_$2/log_$1\_sim_mat.txt_1N_attention_0_L_32_feature_$dim\_$layer\_$folds.txt | awk -v "true_auc=$true_auc" -v "negsam_auc=$negsam_auc" '{if(NR == 2019) {$2=true_auc} else if(NR == 2024) {$2=negsam_auc}; print $0}' > $1\_$2/test_$dim\_$layer\_$folds.txt
			mv $1\_$2/test_$dim\_$layer\_$folds.txt $1\_$2/log_$1\_sim_mat.txt_1N_attention_0_L_32_feature_$dim\_$layer\_$folds.txt
		end
	end
end
