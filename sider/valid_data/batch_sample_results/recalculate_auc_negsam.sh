#!/bin/tcsh
foreach folds (0 1 2 3 4)
	set num_pred = `less ./chem_prediction_value/hypothesis1_$1\_sim_mat.txt_PN_test_$folds\_$1.txt_32_32_1_1 | wc | awk '{print $1}'`
	echo $num_pred
	less ../PN_test_$folds\_$1.txt | awk -v "num=$num_pred" '{if(NR<=num) {print $NF} }' > ground$folds
	foreach layer (16 32 64 128)
		foreach dim (32 64 128 256 512)
			python evaluation.py --ground_truth ground$folds --prediction_value ./chem_prediction_value/hypothesis1_$1\_sim_mat.txt_PN_test_$folds\_$1.txt_$layer\_$dim\_1_1 --out_path true.txt
			set true_auc = `less true.txt | awk '{printf ("%.3f\n", $1)}'`
			less $1\_log/log_$1\_sim_mat.txt_1N_attention_1_L_$layer\_feature_$dim\_0\_$folds.txt | awk -v "true_auc=$true_auc" '{if(NR == 2019) {$2=true_auc}; print $0}' > $1\_log/test_$dim\_$layer\_$folds.txt
			mv $1\_log/test_$dim\_$layer\_$folds.txt $1_log/log_$1\_sim_mat.txt_1N_attention_1_L_$layer\_feature_$dim\_0\_$folds.txt
		end
	end
end
