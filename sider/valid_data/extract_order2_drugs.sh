#!/bin/tcsh
foreach sim (target)
	foreach folds(0 1 2 3 4)
		less P_train_$folds\_$sim.txt | awk '{if(NF-1 == 2) print $0}' > ./order_2_drug_combination/P_train_$folds\_$sim.txt
		less PNN_validation$folds\_$sim.txt | awk '{if(NF-1 == 2) print $0}' > ./order_2_drug_combination/PNN_validation$folds\_$sim.txt
		less PNN_test$folds\_$sim.txt | awk '{if(NF-1 == 2) print $0}' > ./order_2_drug_combination/PNN_test$folds\_$sim.txt
	end
end
