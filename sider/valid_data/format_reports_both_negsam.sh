#!/bin/tcsh
foreach sim_file ($1_sim_mat.txt)
	touch ./batch_reports/$sim_file\_max_$2
	touch ./batch_reports/$sim_file\_max_negsam$2
	#touch ./batch_reports/$sim_file\_negsam_att
	#touch ./batch_reports/$sim_file\_att
	foreach att (0)
		echo att: $att >> ./batch_reports/$sim_file\_negsam_att
		echo att: $att >> ./batch_reports/$sim_file\_att
		if($att == 1) then
			foreach L (16 32 64 128)
				echo L: $L >> ./batch_reports/$sim_file\_negsam_att
				echo L: $L >> ./batch_reports/$sim_file\_att
				foreach size (32 64 128 256 512)
					foreach folds (0 1 2 3 4)
						less ./batch_sample_results/$1_log/log_$sim_file\_1N_attention_$att\_L_$L\_feature_$size\_0_$folds.txt | tail -5 > temp_negsam$folds
						less ./batch_sample_results/$1_log/log_$sim_file\_1N_attention_$att\_L_$L\_feature_$size\_0_$folds.txt | tail -10 | head -5 > temp_$folds
					end
					paste temp_0 temp_1 temp_2 temp_3 temp_4 | awk '{mean = ($2+$4+$6+$8+$10)/5.0;sum=0;for(i=2;i<=10;i=i+2) {sum=sum+($i-mean)^2};std=sqrt(sum/4.0); printf ("%.4f std: %.4f\n", mean ,std) }' > temp$size.txt
					paste temp_negsam0 temp_negsam1 temp_negsam2 temp_negsam3 temp_negsam4 | awk '{mean = ($2+$4+$6+$8+$10)/5.0;sum=0;for(i=2;i<=10;i=i+2) {sum=sum+($i-mean)^2};std=sqrt(sum/4.0); printf ("%.4f std: %.4f\n", mean ,std) }' > temp_negsam$size.txt
				end
				echo "32\t\t\t64\t\t\t128\t\t\t256\t\t\t512" >> ./batch_reports/$sim_file\_att
				paste temp32.txt temp64.txt temp128.txt temp256.txt temp512.txt >> ./batch_reports/$sim_file\_att
				echo "" >> ./batch_reports/$sim_file\_att

				echo "32\t\t\t64\t\t\t128\t\t\t256\t\t\t512" >> ./batch_reports/$sim_file\_negsam_att
                                paste temp_negsam32.txt temp_negsam64.txt temp_negsam128.txt temp_negsam256.txt temp_negsam512.txt >> ./batch_reports/$sim_file\_negsam_att
                                echo "" >> ./batch_reports/$sim_file\_negsam_att
			end
		else
			foreach size (32 64 128 256 512)
				foreach folds (0 1 2 3 4)
					less ./batch_sample_results/$1_max_pool/log_$sim_file\_1N_attention_$att\_L_32_feature_$size\_$2\_$folds.txt | tail -5 > temp_negsam$folds
					less ./batch_sample_results/$1_max_pool/log_$sim_file\_1N_attention_$att\_L_32_feature_$size\_$2\_$folds.txt | tail -10 | head -5 > temp_$folds
				end
				paste temp_0 temp_1 temp_2 temp_3 temp_4 | awk '{mean = ($2+$4+$6+$8+$10)/5.0;sum=0;for(i=2;i<=10;i=i+2) {sum=sum+($i-mean)^2};std=sqrt(sum/4.0);printf ("%.4f std: %.4f\n", mean ,std) }' > temp$size.txt
				paste temp_negsam0 temp_negsam1 temp_negsam2 temp_negsam3 temp_negsam4 | awk '{mean = ($2+$4+$6+$8+$10)/5.0;sum=0;for(i=2;i<=10;i=i+2) {sum=sum+($i-mean)^2};std=sqrt(sum/4.0);printf ("%.4f std: %.4f\n", mean ,std) }' > temp_negsam$size.txt
			end
			echo "32\t\t\t64\t\t\t128\t\t\t256\t\t\t512" >> ./batch_reports/$sim_file\_max_$2
			paste temp32.txt temp64.txt temp128.txt temp256.txt temp512.txt >> ./batch_reports/$sim_file\_max_$2
			echo "" >> ./batch_reports/$sim_file\_max_$2
			
			echo "32\t\t\t64\t\t\t128\t\t\t256\t\t\t512" >> ./batch_reports/$sim_file\_max_negsam$2
                        paste temp_negsam32.txt temp_negsam64.txt temp_negsam128.txt temp_negsam256.txt temp_negsam512.txt >> ./batch_reports/$sim_file\_max_negsam$2
                        echo "" >> ./batch_reports/$sim_file\_max_negsam$2
		endif
	end
end
rm temp_0 temp_1 temp_2 temp_3 temp_4
rm temp32.txt temp64.txt temp128.txt temp256.txt temp512.txt
rm temp_negsam0 temp_negsam1 temp_negsam2 temp_negsam3 temp_negsam4
rm temp_negsam32.txt temp_negsam64.txt temp_negsam128.txt temp_negsam256.txt temp_negsam512.txt
