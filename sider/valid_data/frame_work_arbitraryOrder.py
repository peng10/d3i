import tensorflow as tf
import numpy as np
import GraphConv
import pdb, math, random, re, os, sys, time, argparse
import layer
import sklearn.metrics
import os
os.environ["CUDA_VISIBLE_DEVICES"] = "1"

np.random.seed(234)
tf.set_random_seed(234)
random.seed(234)

path_data = '../'
path_sim = './'

#hyperparameter define
parser = argparse.ArgumentParser()
parser.add_argument('--train', default = 'PN_train_0.txt', type = str)
parser.add_argument('--validation', default = 'PN_validation_0.txt', type = str)
parser.add_argument('--test', default = 'PN_test_0.txt', type = str)
parser.add_argument('--num_train_sample', default = 3802, type = int)
parser.add_argument('--num_validation_sample', default = 1268, type = int)
parser.add_argument('--num_test_sample', default = 1268, type = int)

#the size of input similarity matrix
parser.add_argument('--sim_dim', default = 1180, type = int)
#the size of the transformed vector which will be feed into the next step
parser.add_argument('--input_size', default = 512, type = int)
#the size of attention matrix
parser.add_argument('--L_dim', default = 16, type = int)

parser.add_argument('--batch_size', default = 50, type = int)
parser.add_argument('--num_drugs', default = 1180, type = int)
parser.add_argument('--max_order', default = 2, type = int)
parser.add_argument('--sim_file', default = 'chem_sim_1180.txt',type = str)
parser.add_argument('--freq_file', default = 'freq_PNtrain0.txt',type = str)
parser.add_argument('--out_path', default = './batch_sample_results/prediction_value/',type = str)

parser.add_argument('--attention', default = 0,type = int)
parser.add_argument('--num_trans', default = 1,type = int)
parser.add_argument('--num_DAN', default = 1,type = int)
parser.add_argument('--max_pool', default =0, type = int)

args = parser.parse_args()

file_train = './' + args.train
file_validation = './' + args.validation
file_test = './' + args.test

sim_dim = args.sim_dim
input_size = args.input_size
L_dim = args.L_dim
batch_size = args.batch_size
max_order = args.max_order
num_drugs = args.num_drugs

#load perb list, drug map
with open(args.freq_file, 'r') as f:
	pre = f.read().splitlines()

pre_t = [int(e.lstrip(' ').split(' ')[0]) for e in pre]
temp = sum(pre_t)
preb = [(float(e)/temp) for e in pre_t]
drugId = [int(e.lstrip(' ').split(' ')[1]) for e in pre]
ID_drugId = {}
for i in range(len(drugId)):
	ID_drugId[i] = drugId[i]

positive_triplets = []
with open(args.train, 'r') as f:
	for line in f:
		temp = list(map(int, line.rstrip('\n').split('\t')[:-1]))
		positive_triplets.append(tuple(sorted(temp)))

#load the similarity matrix
sim_matrix = np.loadtxt(args.sim_file, delimiter = ',')

#placeholder
with tf.variable_scope('input'):
	label = tf.placeholder(tf.float32)
	sim_holder = tf.placeholder(tf.float32, [None, max_order, sim_dim])

#learnable parameter initialization
with tf.variable_scope('embedding'):
	W_transsim = tf.get_variable( 'W_transsim', shape = [sim_dim, input_size], initializer = tf.random_uniform_initializer( minval = -np.sqrt(6)/np.sqrt(sim_dim + input_size), maxval = np.sqrt(6)/np.sqrt(sim_dim + input_size) ) )

	att_V = tf.get_variable( 'att_V', shape = [L_dim, input_size], initializer = tf.random_uniform_initializer( minval = -np.sqrt(6)/np.sqrt(L_dim + input_size), maxval = np.sqrt(6)/np.sqrt(L_dim + input_size) ) )

	att_W = tf.get_variable( 'att_W', shape = [1, L_dim], initializer = tf.random_uniform_initializer( minval = -np.sqrt(6)/np.sqrt(L_dim), maxval = np.sqrt(6)/np.sqrt(L_dim) ) )

	class_weight = tf.get_variable('class_weight', shape = [input_size, 1], initializer = tf.random_uniform_initializer( minval = -np.sqrt(6)/np.sqrt(input_size + 1), maxval = np.sqrt(6)/np.sqrt(input_size + 1) ), regularizer = tf.contrib.layers.l2_regularizer(0.2) )

	class_b = tf.Variable(tf.random_uniform([1], minval = -np.sqrt(6)/np.sqrt(2*input_size), maxval = np.sqrt(6)/np.sqrt(2*input_size) ))

	W_transsim_layer = tf.get_variable('W_transsim_layer', shape = [args.num_trans-1, input_size, input_size], initializer = tf.random_uniform_initializer( minval = -np.sqrt(6)/np.sqrt(input_size + input_size), maxval = np.sqrt(6)/np.sqrt(input_size + input_size) ) )
	
	W_DAN_layer = tf.get_variable('W_DAN_layer', shape = [args.num_DAN, input_size, input_size], initializer = tf.random_uniform_initializer( minval = -np.sqrt(6)/np.sqrt(input_size + input_size), maxval = np.sqrt(6)/np.sqrt(input_size + input_size) ) )
	b_DAN_layer = tf.get_variable('b_DAN_layer', shape = [args.num_DAN, input_size], initializer = tf.random_uniform_initializer( minval = -np.sqrt(6)/np.sqrt(input_size), maxval = np.sqrt(6)/np.sqrt(input_size) ))


print ('start to construct computing graph')
#computing graph

#convert [batch, max_order, sim_dim] to [batch_size * max_order, sim_dim]
#holder_sim = tf.reshape(sim_holder, [batch_size * max_order, sim_dim])
holder_sim = tf.reshape(sim_holder, [-1, sim_dim])

#do transformation
trans_sim = tf.matmul(holder_sim, W_transsim)
for i in range(args.num_trans-1):
	trans_sim = tf.matmul(trans_sim, W_transsim_layer[i])
trans_sim = tf.reshape(trans_sim, [-1, max_order, input_size])

if(args.attention == 0):
	#average each drug combination vector
	sim_trans = tf.map_fn(lambda x: layer.omit_zero(x, args.max_pool), trans_sim)
	for i in range(args.num_DAN):
		sim_trans = tf.nn.tanh(tf.matmul(sim_trans, W_DAN_layer[i]) + b_DAN_layer[i])
else:
	sim_trans = tf.map_fn(lambda x: layer.Att(att_V, att_W, x, max_order), trans_sim)
	Att = tf.map_fn(lambda x: layer.Att(att_V, att_W, x, max_order, re_type = 1), trans_sim)

hypothesis = tf.reshape(layer.classifier(tf.squeeze(sim_trans), class_weight, class_b), [-1])
prediction_value = tf.cast(hypothesis > 0.5 , dtype = tf.int32)

cost = -tf.reduce_mean(label * tf.log(hypothesis + 1e-12) + (1 - label) * tf.log(1 - hypothesis + 1e-12))

#training setup
global_step = tf.Variable(0, trainable=False)
starter_learning_rate = 1e-4
learning_rate = tf.train.exponential_decay(starter_learning_rate, global_step, 1000, 0.96, staircase=True)
train_op = tf.train.AdamOptimizer(learning_rate, epsilon = 1e-4).minimize(cost, global_step = global_step)

#for embedding test
sm = tf.cast(sim_matrix, tf.float32)
drug_embedding = tf.matmul(sm, W_transsim)


session_conf = tf.ConfigProto(
      intra_op_parallelism_threads=25,
      inter_op_parallelism_threads=25)

sess = tf.Session(config=session_conf)
sess.run(tf.global_variables_initializer())

with open(file_train, 'r') as f:
        total_lines = f.read().splitlines()

print ('graph construct over')
print (len(total_lines))
print ('epochs start')

minimum = 1000
last_sum_lose = 0
maximum_F1 = 0
minimum_lose = 1e6
break_marker = 0

for epochs in range(600):
	start_time = time.time()
	random.shuffle(total_lines)
	if(epochs+1 % 80 == 0):
		learning_rate *= 0.8
	
	for i in range(args.num_train_sample//batch_size):
		
		sum_lose = 0
		tensor_list = []
		label_list = []
		sample_list = []
		feed_dict = {}
		
		ind_list = random.sample(total_lines, batch_size//2)
		for comb in ind_list:
			#get rid of label
			pos = list(map(int, comb.rstrip('\n').split('\t')))
			drug_num = len(comb.rstrip('\n').split('\t')) - 1
			neg = layer.negative_sample(drug_num, preb, ID_drugId, positive_triplets)
			sample_list.append(pos)
			sample_list.append(neg)

		random.shuffle(sample_list)
		
		for j in range(len(sample_list)):
			ind = sample_list[j][:-1]
			label_ = sample_list[j][-1]
			pad_matrix = np.pad(sim_matrix[ind], ((0, max_order - len(sim_matrix[ind])), (0,0)), 'constant')
			tensor_list.append(pad_matrix)
			label_list.append(label_)

		feed_dict = {sim_holder: tensor_list, label:label_list}
		cos, _ = sess.run([cost, train_op], feed_dict)
		sum_lose += cos

	end_time = time.time()
	
	print ('#iterations: ' + str(epochs))
	print ('currently lose_sum: ' + str(sum_lose))
	print ('time_elpased: ' + str(round(end_time - start_time, 3)))

	if((epochs + 1)%20 == 0):

		print ('start validation test')
		with open(file_validation, 'r') as f:
			validation_lines = f.read().splitlines()
		
		prediction_list = []
		label_list = []
		validation_lose = 0

		for i_v in range(args.num_validation_sample//batch_size):
			feed_dict_validation = {}
			tensor_list = []
			label_list_batch = []
			for j_v in range(batch_size):
				temp = list(map(int, validation_lines[i_v * batch_size + j_v].rstrip('\n').split('\t')))
				ind, label_ = temp[:-1], temp[-1]

				pad_matrix = np.pad(sim_matrix[ind], 
					((0, max_order - len(sim_matrix[ind])), (0,0)), 'constant')
				
				tensor_list.append(pad_matrix)
				label_list.append(label_)
				label_list_batch.append(label_)

			feed_dict_validation = {sim_holder: tensor_list, label: label_list_batch}
			prediction = sess.run(prediction_value, feed_dict_validation).tolist()
			cos = sess.run(cost, feed_dict_validation)
			prediction_list += prediction
			validation_lose += cos
		
		acc, pre, rec, F1, auc = GraphConv.evaluation(label_list, prediction_list)
		print (acc)
		print (pre)
		print (rec)
		print (F1)
		print (auc)
		print ('%.3f' % validation_lose)
		'''
		if(maximum_F1 < F1):
			Variable_list = sess.run(tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES))
			maximum_F1 = F1
		'''
		if(minimum_lose > validation_lose):
			Variable_list = sess.run(tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES))
			minimum_lose = validation_lose
		elif(math.isnan(F1)):
			break_marker = 1
			break

	if(math.isnan(sum_lose) or break_marker == 1):
		print ('train finish final lose is: ' + str(sum_lose))
		break
	else:
		last_sum_lose = sum_lose

#retract variables
sess.run(W_transsim.assign(Variable_list[0]))
sess.run(att_V.assign(Variable_list[1]))
sess.run(att_W.assign(Variable_list[2]))
sess.run(class_weight.assign(Variable_list[3]))
sess.run(class_b.assign(Variable_list[4]))
sess.run(W_transsim_layer.assign(Variable_list[5]))
sess.run(W_DAN_layer.assign(Variable_list[6]))
sess.run(b_DAN_layer.assign(Variable_list[7]))

prediction_list = []
hypo_list = []
label_list = []

with open(file_test, 'r') as f:
        test_lines = f.read().splitlines()

attention_weight_list = []
for i in range(args.num_test_sample // 2):

	feed_dict_test = {}
	tensor_list = []

	for j in range(2):
		temp = list(map(int, test_lines[i * 2 + j].rstrip('\n').split('\t')))
		ind, label_ = temp[:-1], temp[-1]

		pad_matrix = np.pad(sim_matrix[ind],
			((0, max_order - len(sim_matrix[ind])), (0,0)), 'constant')

		tensor_list.append(pad_matrix)
		label_list.append(label_)

	feed_dict_test = {sim_holder: tensor_list}

	prediction, hypo = sess.run([prediction_value, hypothesis], feed_dict_test)
	prediction = prediction.tolist()
	hypo = hypo.tolist()
	prediction_list += prediction
	hypo_list += hypo

	if(args.attention == 1):
		AttW = sess.run(tf.squeeze(Att), feed_dict_test)
		attention_weight_list.append(AttW)

acc, pre, rec, F1, auc = GraphConv.evaluation(label_list, prediction_list)
print ('acc: %.3f' % acc)
print ('pre: %.3f' % pre)
print ('rec: %.3f' % rec)
print ('F1: %.3f' % F1)
print ('auc: %.3f' % auc)

if(args.attention == 1):
	attention_weight_matrix = np.concatenate(attention_weight_list, axis = 0)
	np.savetxt('./att_weight/' + args.sim_file + '_' + str(args.L_dim) + '_' + str(args.input_size) + '_' + str(args.num_trans) + '_' + str(args.num_DAN) + '_' + str(args.test), attention_weight_matrix, fmt = '%.4f', delimiter = ',')

with open(args.out_path + 'prediction_value_' + str(args.attention) + '_' + args.sim_file + '_' + args.test + '_' + str(args.L_dim) +'_'+str(args.input_size) + '_' + str(args.num_trans) + '_' + str(args.num_DAN), 'w') as f:
	for e in prediction_list:
		f.write("%d\n" % e)

with open(args.out_path + 'hypothesis' + str(args.attention) + '_' + args.sim_file + '_' + args.test + '_' + str(args.L_dim) +'_'+str(args.input_size) + '_' + str(args.num_trans) + '_' + str(args.num_DAN), 'w') as f:
	for e in hypo_list:
		f.write("%.3f\n" % e)

drug_embedding_out, att_W_out = sess.run([drug_embedding, att_W])
np.savetxt('./embedding_test/drug_embedding_attention_' + str(args.attention) + '_max_pool_' + str(args.max_pool) + '_' + args.test, drug_embedding_out, fmt = '%.4f', delimiter = ',')

if(args.attention == 1):
	np.savetxt('./embedding_test/att_W_' + args.test , att_W_out, fmt = '%.4f', delimiter = ',')

	

