import scipy.io as sio
import numpy as np
import random
import argparse
import layer
import pdb

path = './'
random.seed(123)
np.random.seed(123)

parser = argparse.ArgumentParser()
parser.add_argument('--avoid_file', type = str)
parser.add_argument('--num_drugs', type = int)
parser.add_argument('--train_file', type = str)
parser.add_argument('--freq_file',  type = str)
parser.add_argument('--outfile',  type = str)

args = parser.parse_args()

with open(args.freq_file, 'r') as f:
        pre = f.read().splitlines()

pre_t = [int(e.lstrip(' ').split(' ')[0]) for e in pre]
temp = sum(pre_t)
preb = [(float(e)/temp) for e in pre_t]
print (len(pre_t))

drugId = [int(e.lstrip(' ').split(' ')[1]) for e in pre]
ID_drugId = {}
for i in range(len(drugId)):
	ID_drugId[i] = drugId[i]
#pdb.set_trace()

positive_triplets = []
with open(path + args.avoid_file, 'r') as f:
	for line in f:
		temp = list(map(int, line.rstrip('\n').split('\t')[:-1]))
		#print temp
		positive_triplets.append(tuple(sorted(temp)))

pos_list = []
with open(path + args.train_file, 'r') as f:
	for line in f:
		temp = list(map(int, line.rstrip('\n').split('\t')[:-1]))
		pos_list.append(temp)


neg_list = []
for e in pos_list:
	#pdb.set_trace()
	neg = layer.negative_sample(len(e), preb, ID_drugId, positive_triplets)
	neg_list.append(neg)

f = open(args.outfile,'w')
for neg_sample in neg_list:
	for ele_ind in range(len(neg_sample)):
		if(ele_ind != len(neg_sample) - 1):
			f.write(str(neg_sample[ele_ind]) + "\t")
		else:
			f.write(str(neg_sample[ele_ind]) + "\n")
f.close()
