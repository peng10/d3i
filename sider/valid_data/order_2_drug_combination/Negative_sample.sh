#!/bin/tcsh
foreach folds (0 1 2 3 4)
        foreach step (train)
                set num_sample = `less PN_$step\_$folds\_$1.txt | wc | awk '{print $1}'`
                if($step == validation || $step == test) then
                        cat PN_$step\_$folds\_$1.txt PN_train_$folds\_$1.txt > temp
                        less temp | awk '{for(i=1;i<=NF-1;i++) {print $i} }' | sort | uniq -c | sort -k2n > freq_PN$step$folds\_$1.txt
                        python negsamp.py --num_drugs $2 --avoid_file temp --train_file PN_$step\_$folds\_$1.txt --freq_file freq_PN$step$folds\_$1.txt --outfile PN_$step\_N_set$folds\_$1.txt
                        rm temp
                else
                        less PN_$step\_$folds\_$1.txt | awk '{for(i=1;i<=NF-1;i++) {print $i} }' | sort | uniq -c | sort -k2n > freq_PN$step$folds\_$1.txt
                endif
        end
end
