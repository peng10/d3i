#!/bin/tcsh

cat $1_sim_mat.txt_max_0 $1_sim_mat.txt_max_1 $1_sim_mat.txt_max_3 $1_sim_mat.txt_max_5 > $1_max.txt

cat $1_sim_mat.txt_mean_0 $1_sim_mat.txt_mean_1 $1_sim_mat.txt_mean_3 $1_sim_mat.txt_mean_5 > $1_mean.txt

cat $1_max.txt $1_mean.txt $1_sim_mat.txt_att > $1_results.txt
