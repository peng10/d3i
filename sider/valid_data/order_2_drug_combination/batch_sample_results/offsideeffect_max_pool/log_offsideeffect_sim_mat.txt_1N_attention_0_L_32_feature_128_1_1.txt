start to construct computing graph
graph construct over
507
epochs start
#iterations: 0
currently lose_sum: 0.6899034976959229
time_elpased: 0.099
#iterations: 1
currently lose_sum: 0.7108259201049805
time_elpased: 0.079
#iterations: 2
currently lose_sum: 0.6964753866195679
time_elpased: 0.078
#iterations: 3
currently lose_sum: 0.6919340491294861
time_elpased: 0.077
#iterations: 4
currently lose_sum: 0.6669641733169556
time_elpased: 0.078
#iterations: 5
currently lose_sum: 0.6933897733688354
time_elpased: 0.087
#iterations: 6
currently lose_sum: 0.707522988319397
time_elpased: 0.08
#iterations: 7
currently lose_sum: 0.7231960892677307
time_elpased: 0.084
#iterations: 8
currently lose_sum: 0.6944272518157959
time_elpased: 0.08
#iterations: 9
currently lose_sum: 0.6445248126983643
time_elpased: 0.077
#iterations: 10
currently lose_sum: 0.6989994049072266
time_elpased: 0.079
#iterations: 11
currently lose_sum: 0.6288926005363464
time_elpased: 0.086
#iterations: 12
currently lose_sum: 0.6393291354179382
time_elpased: 0.08
#iterations: 13
currently lose_sum: 0.5719885230064392
time_elpased: 0.076
#iterations: 14
currently lose_sum: 0.5818909406661987
time_elpased: 0.076
#iterations: 15
currently lose_sum: 0.5944386124610901
time_elpased: 0.076
#iterations: 16
currently lose_sum: 0.6028329730033875
time_elpased: 0.076
#iterations: 17
currently lose_sum: 0.6830775141716003
time_elpased: 0.077
#iterations: 18
currently lose_sum: 0.6185871362686157
time_elpased: 0.077
#iterations: 19
currently lose_sum: 0.6704694628715515
time_elpased: 0.082
start validation test
0.551515151515
0.69696969697
0.264367816092
0.383333333333
0.568081343943
7.532
#iterations: 20
currently lose_sum: 0.5721449255943298
time_elpased: 0.075
#iterations: 21
currently lose_sum: 0.6973660588264465
time_elpased: 0.082
#iterations: 22
currently lose_sum: 0.5340272188186646
time_elpased: 0.088
#iterations: 23
currently lose_sum: 0.6876823306083679
time_elpased: 0.076
#iterations: 24
currently lose_sum: 0.526536762714386
time_elpased: 0.078
#iterations: 25
currently lose_sum: 0.6378815174102783
time_elpased: 0.077
#iterations: 26
currently lose_sum: 0.5228541493415833
time_elpased: 0.08
#iterations: 27
currently lose_sum: 0.6503438949584961
time_elpased: 0.081
#iterations: 28
currently lose_sum: 0.5090828537940979
time_elpased: 0.088
#iterations: 29
currently lose_sum: 0.6896469593048096
time_elpased: 0.08
#iterations: 30
currently lose_sum: 0.49199235439300537
time_elpased: 0.079
#iterations: 31
currently lose_sum: 0.531083881855011
time_elpased: 0.076
#iterations: 32
currently lose_sum: 0.5053113102912903
time_elpased: 0.075
#iterations: 33
currently lose_sum: 0.4820645749568939
time_elpased: 0.077
#iterations: 34
currently lose_sum: 0.6197982430458069
time_elpased: 0.079
#iterations: 35
currently lose_sum: 0.5725770592689514
time_elpased: 0.077
#iterations: 36
currently lose_sum: 0.3788311779499054
time_elpased: 0.075
#iterations: 37
currently lose_sum: 0.5677429437637329
time_elpased: 0.077
#iterations: 38
currently lose_sum: 0.5261901021003723
time_elpased: 0.093
#iterations: 39
currently lose_sum: 0.5780020356178284
time_elpased: 0.079
start validation test
0.572727272727
0.770491803279
0.270114942529
0.4
0.590185676393
8.042
#iterations: 40
currently lose_sum: 0.5401226282119751
time_elpased: 0.093
#iterations: 41
currently lose_sum: 0.5892905592918396
time_elpased: 0.078
#iterations: 42
currently lose_sum: 0.5308212637901306
time_elpased: 0.076
#iterations: 43
currently lose_sum: 0.5782430171966553
time_elpased: 0.076
#iterations: 44
currently lose_sum: 0.625632643699646
time_elpased: 0.078
#iterations: 45
currently lose_sum: 0.5953179597854614
time_elpased: 0.075
#iterations: 46
currently lose_sum: 0.5855558514595032
time_elpased: 0.081
#iterations: 47
currently lose_sum: 0.6432008147239685
time_elpased: 0.082
#iterations: 48
currently lose_sum: 0.46015021204948425
time_elpased: 0.077
#iterations: 49
currently lose_sum: 0.5523331761360168
time_elpased: 0.08
#iterations: 50
currently lose_sum: 0.6566839218139648
time_elpased: 0.076
#iterations: 51
currently lose_sum: 0.4812504053115845
time_elpased: 0.077
#iterations: 52
currently lose_sum: 0.40008077025413513
time_elpased: 0.079
#iterations: 53
currently lose_sum: 0.683114230632782
time_elpased: 0.079
#iterations: 54
currently lose_sum: 0.44343358278274536
time_elpased: 0.075
#iterations: 55
currently lose_sum: 0.530987024307251
time_elpased: 0.082
#iterations: 56
currently lose_sum: 0.5447194576263428
time_elpased: 0.092
#iterations: 57
currently lose_sum: 0.6129133701324463
time_elpased: 0.08
#iterations: 58
currently lose_sum: 0.5753422379493713
time_elpased: 0.076
#iterations: 59
currently lose_sum: 0.6056967377662659
time_elpased: 0.077
start validation test
0.663636363636
0.658291457286
0.752873563218
0.702412868633
0.65848806366
6.838
#iterations: 60
currently lose_sum: 0.4663637578487396
time_elpased: 0.078
#iterations: 61
currently lose_sum: 0.6135676503181458
time_elpased: 0.077
#iterations: 62
currently lose_sum: 0.4672998785972595
time_elpased: 0.077
#iterations: 63
currently lose_sum: 0.48091036081314087
time_elpased: 0.076
#iterations: 64
currently lose_sum: 0.4420466721057892
time_elpased: 0.075
#iterations: 65
currently lose_sum: 0.4880048632621765
time_elpased: 0.076
#iterations: 66
currently lose_sum: 0.5130046606063843
time_elpased: 0.111
#iterations: 67
currently lose_sum: 0.46185287833213806
time_elpased: 0.075
#iterations: 68
currently lose_sum: 0.45505672693252563
time_elpased: 0.078
#iterations: 69
currently lose_sum: 0.5054070353507996
time_elpased: 0.078
#iterations: 70
currently lose_sum: 0.5419951677322388
time_elpased: 0.079
#iterations: 71
currently lose_sum: 0.41848599910736084
time_elpased: 0.076
#iterations: 72
currently lose_sum: 0.6687411069869995
time_elpased: 0.075
#iterations: 73
currently lose_sum: 0.625550389289856
time_elpased: 0.078
#iterations: 74
currently lose_sum: 0.4178371727466583
time_elpased: 0.079
#iterations: 75
currently lose_sum: 0.40729087591171265
time_elpased: 0.079
#iterations: 76
currently lose_sum: 0.5043678879737854
time_elpased: 0.079
#iterations: 77
currently lose_sum: 0.5796251893043518
time_elpased: 0.078
#iterations: 78
currently lose_sum: 0.5083665251731873
time_elpased: 0.099
#iterations: 79
currently lose_sum: 0.5816243290901184
time_elpased: 0.078
start validation test
0.60303030303
0.811594202899
0.32183908046
0.460905349794
0.619252873563
8.381
#iterations: 80
currently lose_sum: 0.39746329188346863
time_elpased: 0.084
#iterations: 81
currently lose_sum: 0.4812706708908081
time_elpased: 0.08
#iterations: 82
currently lose_sum: 0.35144662857055664
time_elpased: 0.081
#iterations: 83
currently lose_sum: 0.5787537097930908
time_elpased: 0.081
#iterations: 84
currently lose_sum: 0.44450366497039795
time_elpased: 0.077
#iterations: 85
currently lose_sum: 0.3550206124782562
time_elpased: 0.078
#iterations: 86
currently lose_sum: 0.5096502304077148
time_elpased: 0.079
#iterations: 87
currently lose_sum: 0.4851948916912079
time_elpased: 0.077
#iterations: 88
currently lose_sum: 0.4639238715171814
time_elpased: 0.076
#iterations: 89
currently lose_sum: 0.33674225211143494
time_elpased: 0.079
#iterations: 90
currently lose_sum: 0.49702560901641846
time_elpased: 0.076
#iterations: 91
currently lose_sum: 0.44751212000846863
time_elpased: 0.079
#iterations: 92
currently lose_sum: 0.48882734775543213
time_elpased: 0.08
#iterations: 93
currently lose_sum: 0.6140794157981873
time_elpased: 0.084
#iterations: 94
currently lose_sum: 0.4990693926811218
time_elpased: 0.076
#iterations: 95
currently lose_sum: 0.5678845643997192
time_elpased: 0.079
#iterations: 96
currently lose_sum: 0.6829491853713989
time_elpased: 0.079
#iterations: 97
currently lose_sum: 0.5510454177856445
time_elpased: 0.077
#iterations: 98
currently lose_sum: 0.5733045935630798
time_elpased: 0.076
#iterations: 99
currently lose_sum: 0.9244212508201599
time_elpased: 0.088
start validation test
0.651515151515
0.715328467153
0.563218390805
0.630225080386
0.656609195402
7.374
#iterations: 100
currently lose_sum: 0.3988546133041382
time_elpased: 0.091
#iterations: 101
currently lose_sum: 0.5205695629119873
time_elpased: 0.078
#iterations: 102
currently lose_sum: 0.48593589663505554
time_elpased: 0.083
#iterations: 103
currently lose_sum: 0.38569849729537964
time_elpased: 0.076
#iterations: 104
currently lose_sum: 0.4781099259853363
time_elpased: 0.08
#iterations: 105
currently lose_sum: 0.428462952375412
time_elpased: 0.079
#iterations: 106
currently lose_sum: 0.4647192656993866
time_elpased: 0.075
#iterations: 107
currently lose_sum: 0.4180271327495575
time_elpased: 0.077
#iterations: 108
currently lose_sum: 0.4352094531059265
time_elpased: 0.085
#iterations: 109
currently lose_sum: 0.2761644124984741
time_elpased: 0.077
#iterations: 110
currently lose_sum: 0.41063228249549866
time_elpased: 0.079
#iterations: 111
currently lose_sum: 0.3035220205783844
time_elpased: 0.076
#iterations: 112
currently lose_sum: 0.3027840852737427
time_elpased: 0.079
#iterations: 113
currently lose_sum: 0.29514893889427185
time_elpased: 0.077
#iterations: 114
currently lose_sum: 0.4501470923423767
time_elpased: 0.083
#iterations: 115
currently lose_sum: 0.33227553963661194
time_elpased: 0.078
#iterations: 116
currently lose_sum: 0.3089967370033264
time_elpased: 0.079
#iterations: 117
currently lose_sum: 0.4699322283267975
time_elpased: 0.08
#iterations: 118
currently lose_sum: 0.6539918184280396
time_elpased: 0.081
#iterations: 119
currently lose_sum: 0.35024774074554443
time_elpased: 0.078
start validation test
0.657575757576
0.678362573099
0.666666666667
0.672463768116
0.657051282051
7.313
#iterations: 120
currently lose_sum: 0.33643239736557007
time_elpased: 0.077
#iterations: 121
currently lose_sum: 0.6034260392189026
time_elpased: 0.077
#iterations: 122
currently lose_sum: 0.36741364002227783
time_elpased: 0.078
#iterations: 123
currently lose_sum: 0.3951468765735626
time_elpased: 0.076
#iterations: 124
currently lose_sum: 0.3565080463886261
time_elpased: 0.085
#iterations: 125
currently lose_sum: 0.46745166182518005
time_elpased: 0.079
#iterations: 126
currently lose_sum: 0.8284076452255249
time_elpased: 0.076
#iterations: 127
currently lose_sum: 0.3441111743450165
time_elpased: 0.094
#iterations: 128
currently lose_sum: 0.5559364557266235
time_elpased: 0.099
#iterations: 129
currently lose_sum: 0.4780893623828888
time_elpased: 0.078
#iterations: 130
currently lose_sum: 0.36152908205986023
time_elpased: 0.095
#iterations: 131
currently lose_sum: 0.2794540524482727
time_elpased: 0.077
#iterations: 132
currently lose_sum: 0.37101513147354126
time_elpased: 0.08
#iterations: 133
currently lose_sum: 0.727451503276825
time_elpased: 0.081
#iterations: 134
currently lose_sum: 0.3917297124862671
time_elpased: 0.085
#iterations: 135
currently lose_sum: 0.27367526292800903
time_elpased: 0.081
#iterations: 136
currently lose_sum: 0.726894199848175
time_elpased: 0.076
#iterations: 137
currently lose_sum: 0.39227235317230225
time_elpased: 0.081
#iterations: 138
currently lose_sum: 0.26795437932014465
time_elpased: 0.078
#iterations: 139
currently lose_sum: 0.29890817403793335
time_elpased: 0.09
start validation test
0.645454545455
0.747826086957
0.494252873563
0.595155709343
0.654177718833
8.198
#iterations: 140
currently lose_sum: 0.38916051387786865
time_elpased: 0.076
#iterations: 141
currently lose_sum: 0.32995983958244324
time_elpased: 0.078
#iterations: 142
currently lose_sum: 0.5398333668708801
time_elpased: 0.077
#iterations: 143
currently lose_sum: 0.24232593178749084
time_elpased: 0.078
#iterations: 144
currently lose_sum: 0.3565303087234497
time_elpased: 0.077
#iterations: 145
currently lose_sum: 0.3865800201892853
time_elpased: 0.081
#iterations: 146
currently lose_sum: 0.4240385890007019
time_elpased: 0.08
#iterations: 147
currently lose_sum: 0.157334104180336
time_elpased: 0.081
#iterations: 148
currently lose_sum: 0.3444794714450836
time_elpased: 0.082
#iterations: 149
currently lose_sum: 0.5081411600112915
time_elpased: 0.079
#iterations: 150
currently lose_sum: 0.8309434652328491
time_elpased: 0.075
#iterations: 151
currently lose_sum: 0.3765052556991577
time_elpased: 0.084
#iterations: 152
currently lose_sum: 0.24832193553447723
time_elpased: 0.077
#iterations: 153
currently lose_sum: 0.394354909658432
time_elpased: 0.088
#iterations: 154
currently lose_sum: 0.8947051167488098
time_elpased: 0.077
#iterations: 155
currently lose_sum: 0.645111083984375
time_elpased: 0.08
#iterations: 156
currently lose_sum: 0.32981306314468384
time_elpased: 0.077
#iterations: 157
currently lose_sum: 0.4573379456996918
time_elpased: 0.083
#iterations: 158
currently lose_sum: 0.36093869805336
time_elpased: 0.081
#iterations: 159
currently lose_sum: 0.3949591815471649
time_elpased: 0.076
start validation test
0.612121212121
0.755555555556
0.390804597701
0.515151515152
0.624889478338
9.368
#iterations: 160
currently lose_sum: 0.3854712247848511
time_elpased: 0.078
#iterations: 161
currently lose_sum: 0.23663733899593353
time_elpased: 0.083
#iterations: 162
currently lose_sum: 0.37223824858665466
time_elpased: 0.079
#iterations: 163
currently lose_sum: 0.23970171809196472
time_elpased: 0.084
#iterations: 164
currently lose_sum: 0.20933188498020172
time_elpased: 0.075
#iterations: 165
currently lose_sum: 0.25594887137413025
time_elpased: 0.079
#iterations: 166
currently lose_sum: 0.5284362435340881
time_elpased: 0.077
#iterations: 167
currently lose_sum: 0.4048619866371155
time_elpased: 0.076
#iterations: 168
currently lose_sum: 0.4047620892524719
time_elpased: 0.08
#iterations: 169
currently lose_sum: 0.3901667296886444
time_elpased: 0.079
#iterations: 170
currently lose_sum: 0.375516802072525
time_elpased: 0.076
#iterations: 171
currently lose_sum: 0.24803254008293152
time_elpased: 0.077
#iterations: 172
currently lose_sum: 0.33932965993881226
time_elpased: 0.078
#iterations: 173
currently lose_sum: 0.19870151579380035
time_elpased: 0.078
#iterations: 174
currently lose_sum: 0.4280353784561157
time_elpased: 0.076
#iterations: 175
currently lose_sum: 0.4402783513069153
time_elpased: 0.079
#iterations: 176
currently lose_sum: 0.2231309711933136
time_elpased: 0.08
#iterations: 177
currently lose_sum: 0.44702860713005066
time_elpased: 0.077
#iterations: 178
currently lose_sum: 0.25510114431381226
time_elpased: 0.078
#iterations: 179
currently lose_sum: 0.4701840579509735
time_elpased: 0.078
start validation test
0.654545454545
0.702702702703
0.597701149425
0.645962732919
0.657824933687
8.319
#iterations: 180
currently lose_sum: 0.38245639204978943
time_elpased: 0.076
#iterations: 181
currently lose_sum: 0.3396492302417755
time_elpased: 0.083
#iterations: 182
currently lose_sum: 0.3049001395702362
time_elpased: 0.076
#iterations: 183
currently lose_sum: 0.5206756591796875
time_elpased: 0.076
#iterations: 184
currently lose_sum: 0.38069161772727966
time_elpased: 0.077
#iterations: 185
currently lose_sum: 0.2673162519931793
time_elpased: 0.079
#iterations: 186
currently lose_sum: 0.3722805380821228
time_elpased: 0.077
#iterations: 187
currently lose_sum: 0.24388761818408966
time_elpased: 0.078
#iterations: 188
currently lose_sum: 0.23163287341594696
time_elpased: 0.077
#iterations: 189
currently lose_sum: 0.22428016364574432
time_elpased: 0.076
#iterations: 190
currently lose_sum: 0.24296358227729797
time_elpased: 0.083
#iterations: 191
currently lose_sum: 0.3642234802246094
time_elpased: 0.078
#iterations: 192
currently lose_sum: 0.30629202723503113
time_elpased: 0.101
#iterations: 193
currently lose_sum: 0.2708616852760315
time_elpased: 0.105
#iterations: 194
currently lose_sum: 0.15803834795951843
time_elpased: 0.077
#iterations: 195
currently lose_sum: 0.37325403094291687
time_elpased: 0.077
#iterations: 196
currently lose_sum: 0.28791844844818115
time_elpased: 0.079
#iterations: 197
currently lose_sum: 0.361966609954834
time_elpased: 0.08
#iterations: 198
currently lose_sum: 0.4398138225078583
time_elpased: 0.079
#iterations: 199
currently lose_sum: 0.2692528963088989
time_elpased: 0.08
start validation test
0.678787878788
0.720779220779
0.637931034483
0.676829268293
0.681145004421
8.383
#iterations: 200
currently lose_sum: 0.26809972524642944
time_elpased: 0.081
#iterations: 201
currently lose_sum: 0.18374770879745483
time_elpased: 0.076
#iterations: 202
currently lose_sum: 0.1763550490140915
time_elpased: 0.078
#iterations: 203
currently lose_sum: 0.2294115275144577
time_elpased: 0.076
#iterations: 204
currently lose_sum: 0.2926144003868103
time_elpased: 0.084
#iterations: 205
currently lose_sum: 0.2566739320755005
time_elpased: 0.084
#iterations: 206
currently lose_sum: 0.46564894914627075
time_elpased: 0.075
#iterations: 207
currently lose_sum: 0.4223669469356537
time_elpased: 0.086
#iterations: 208
currently lose_sum: 0.26862895488739014
time_elpased: 0.082
#iterations: 209
currently lose_sum: 0.20343275368213654
time_elpased: 0.077
#iterations: 210
currently lose_sum: 0.15581461787223816
time_elpased: 0.08
#iterations: 211
currently lose_sum: 0.5695860385894775
time_elpased: 0.078
#iterations: 212
currently lose_sum: 0.9506329894065857
time_elpased: 0.078
#iterations: 213
currently lose_sum: 0.3510207235813141
time_elpased: 0.08
#iterations: 214
currently lose_sum: 0.3760353624820709
time_elpased: 0.083
#iterations: 215
currently lose_sum: 0.17503617703914642
time_elpased: 0.078
#iterations: 216
currently lose_sum: 0.38212957978248596
time_elpased: 0.076
#iterations: 217
currently lose_sum: 0.3052613139152527
time_elpased: 0.078
#iterations: 218
currently lose_sum: 0.22218063473701477
time_elpased: 0.082
#iterations: 219
currently lose_sum: 0.4110943675041199
time_elpased: 0.077
start validation test
0.472727272727
0.0
0.0
nan
0.5
24.247
acc: 0.727
pre: 0.691
rec: 0.839
F1: 0.758
auc: 0.725
