start to construct computing graph
graph construct over
508
epochs start
#iterations: 0
currently lose_sum: 0.7190412282943726
time_elpased: 0.113
#iterations: 1
currently lose_sum: 0.6820263266563416
time_elpased: 0.09
#iterations: 2
currently lose_sum: 0.7223145961761475
time_elpased: 0.091
#iterations: 3
currently lose_sum: 0.6704655289649963
time_elpased: 0.088
#iterations: 4
currently lose_sum: 0.683445930480957
time_elpased: 0.088
#iterations: 5
currently lose_sum: 0.6797822713851929
time_elpased: 0.089
#iterations: 6
currently lose_sum: 0.7058131694793701
time_elpased: 0.09
#iterations: 7
currently lose_sum: 0.69150310754776
time_elpased: 0.09
#iterations: 8
currently lose_sum: 0.6810924410820007
time_elpased: 0.088
#iterations: 9
currently lose_sum: 0.5127276182174683
time_elpased: 0.089
#iterations: 10
currently lose_sum: 0.673559308052063
time_elpased: 0.09
#iterations: 11
currently lose_sum: 0.7530809640884399
time_elpased: 0.093
#iterations: 12
currently lose_sum: 0.7039509415626526
time_elpased: 0.091
#iterations: 13
currently lose_sum: 0.6867100596427917
time_elpased: 0.088
#iterations: 14
currently lose_sum: 0.5535158514976501
time_elpased: 0.09
#iterations: 15
currently lose_sum: 0.5723090767860413
time_elpased: 0.09
#iterations: 16
currently lose_sum: 0.6237749457359314
time_elpased: 0.09
#iterations: 17
currently lose_sum: 0.6601274609565735
time_elpased: 0.089
#iterations: 18
currently lose_sum: 0.44244372844696045
time_elpased: 0.092
#iterations: 19
currently lose_sum: 0.6150884032249451
time_elpased: 0.09
start validation test
0.684848484848
0.645161290323
0.838323353293
0.729166666667
0.682965357628
6.634
#iterations: 20
currently lose_sum: 0.6424332857131958
time_elpased: 0.096
#iterations: 21
currently lose_sum: 0.5934642553329468
time_elpased: 0.088
#iterations: 22
currently lose_sum: 0.6834203004837036
time_elpased: 0.091
#iterations: 23
currently lose_sum: 0.5866259336471558
time_elpased: 0.088
#iterations: 24
currently lose_sum: 0.6677449941635132
time_elpased: 0.09
#iterations: 25
currently lose_sum: 0.6514015793800354
time_elpased: 0.088
#iterations: 26
currently lose_sum: 0.4161888659000397
time_elpased: 0.089
#iterations: 27
currently lose_sum: 0.5143579840660095
time_elpased: 0.089
#iterations: 28
currently lose_sum: 0.6012565493583679
time_elpased: 0.089
#iterations: 29
currently lose_sum: 0.4519385099411011
time_elpased: 0.096
#iterations: 30
currently lose_sum: 0.579666256904602
time_elpased: 0.093
#iterations: 31
currently lose_sum: 0.5605962872505188
time_elpased: 0.1
#iterations: 32
currently lose_sum: 0.624032735824585
time_elpased: 0.107
#iterations: 33
currently lose_sum: 0.475922554731369
time_elpased: 0.089
#iterations: 34
currently lose_sum: 0.5793623328208923
time_elpased: 0.092
#iterations: 35
currently lose_sum: 0.4913859963417053
time_elpased: 0.088
#iterations: 36
currently lose_sum: 0.39209848642349243
time_elpased: 0.092
#iterations: 37
currently lose_sum: 0.6385253667831421
time_elpased: 0.094
#iterations: 38
currently lose_sum: 0.5750289559364319
time_elpased: 0.089
#iterations: 39
currently lose_sum: 0.5261185169219971
time_elpased: 0.093
start validation test
0.654545454545
0.59925093633
0.958083832335
0.73732718894
0.650821057272
7.172
#iterations: 40
currently lose_sum: 0.608080267906189
time_elpased: 0.091
#iterations: 41
currently lose_sum: 0.6081057190895081
time_elpased: 0.089
#iterations: 42
currently lose_sum: 0.5749677419662476
time_elpased: 0.089
#iterations: 43
currently lose_sum: 0.5469194650650024
time_elpased: 0.115
#iterations: 44
currently lose_sum: 0.5718712210655212
time_elpased: 0.095
#iterations: 45
currently lose_sum: 0.6246950626373291
time_elpased: 0.088
#iterations: 46
currently lose_sum: 0.49733811616897583
time_elpased: 0.088
#iterations: 47
currently lose_sum: 0.6366572976112366
time_elpased: 0.094
#iterations: 48
currently lose_sum: 0.5151609182357788
time_elpased: 0.098
#iterations: 49
currently lose_sum: 0.6007729768753052
time_elpased: 0.09
#iterations: 50
currently lose_sum: 0.5140172243118286
time_elpased: 0.088
#iterations: 51
currently lose_sum: 0.5830710530281067
time_elpased: 0.09
#iterations: 52
currently lose_sum: 0.4677616059780121
time_elpased: 0.09
#iterations: 53
currently lose_sum: 0.44452565908432007
time_elpased: 0.097
#iterations: 54
currently lose_sum: 0.5972203612327576
time_elpased: 0.088
#iterations: 55
currently lose_sum: 0.4046168923377991
time_elpased: 0.089
#iterations: 56
currently lose_sum: 0.6475028991699219
time_elpased: 0.09
#iterations: 57
currently lose_sum: 0.5925357341766357
time_elpased: 0.091
#iterations: 58
currently lose_sum: 0.5959720015525818
time_elpased: 0.095
#iterations: 59
currently lose_sum: 0.548359215259552
time_elpased: 0.089
start validation test
0.7
0.691011235955
0.736526946108
0.713043478261
0.699551816612
6.605
#iterations: 60
currently lose_sum: 0.7164922952651978
time_elpased: 0.097
#iterations: 61
currently lose_sum: 0.5514681935310364
time_elpased: 0.088
#iterations: 62
currently lose_sum: 0.3823663294315338
time_elpased: 0.09
#iterations: 63
currently lose_sum: 0.5258504748344421
time_elpased: 0.088
#iterations: 64
currently lose_sum: 0.5230581164360046
time_elpased: 0.093
#iterations: 65
currently lose_sum: 0.5240128636360168
time_elpased: 0.091
#iterations: 66
currently lose_sum: 1.3399200439453125
time_elpased: 0.097
#iterations: 67
currently lose_sum: 0.5496514439582825
time_elpased: 0.089
#iterations: 68
currently lose_sum: 0.40654489398002625
time_elpased: 0.089
#iterations: 69
currently lose_sum: 0.5657510757446289
time_elpased: 0.091
#iterations: 70
currently lose_sum: 0.4915115535259247
time_elpased: 0.088
#iterations: 71
currently lose_sum: 0.5426663756370544
time_elpased: 0.089
#iterations: 72
currently lose_sum: 0.4647241532802582
time_elpased: 0.094
#iterations: 73
currently lose_sum: 0.4097394347190857
time_elpased: 0.111
#iterations: 74
currently lose_sum: 0.6095489263534546
time_elpased: 0.096
#iterations: 75
currently lose_sum: 0.5652802586555481
time_elpased: 0.09
#iterations: 76
currently lose_sum: 0.5831658244132996
time_elpased: 0.088
#iterations: 77
currently lose_sum: 0.4995253384113312
time_elpased: 0.095
#iterations: 78
currently lose_sum: 0.4784538447856903
time_elpased: 0.088
#iterations: 79
currently lose_sum: 0.4254721999168396
time_elpased: 0.088
start validation test
0.624242424242
0.741573033708
0.395209580838
0.515625
0.62705264318
6.555
#iterations: 80
currently lose_sum: 0.4893026351928711
time_elpased: 0.091
#iterations: 81
currently lose_sum: 0.4606434404850006
time_elpased: 0.096
#iterations: 82
currently lose_sum: 0.49207738041877747
time_elpased: 0.09
#iterations: 83
currently lose_sum: 0.4806262254714966
time_elpased: 0.091
#iterations: 84
currently lose_sum: 0.4821958541870117
time_elpased: 0.104
#iterations: 85
currently lose_sum: 0.48496776819229126
time_elpased: 0.095
#iterations: 86
currently lose_sum: 0.523391842842102
time_elpased: 0.087
#iterations: 87
currently lose_sum: 0.38203415274620056
time_elpased: 0.09
#iterations: 88
currently lose_sum: 0.46921274065971375
time_elpased: 0.09
#iterations: 89
currently lose_sum: 0.6411106586456299
time_elpased: 0.092
#iterations: 90
currently lose_sum: 0.46397000551223755
time_elpased: 0.108
#iterations: 91
currently lose_sum: 0.543455958366394
time_elpased: 0.094
#iterations: 92
currently lose_sum: 0.40459027886390686
time_elpased: 0.089
#iterations: 93
currently lose_sum: 0.5301730632781982
time_elpased: 0.093
#iterations: 94
currently lose_sum: 0.500413715839386
time_elpased: 0.096
#iterations: 95
currently lose_sum: 0.3891681432723999
time_elpased: 0.092
#iterations: 96
currently lose_sum: 0.31884846091270447
time_elpased: 0.09
#iterations: 97
currently lose_sum: 0.5322517156600952
time_elpased: 0.088
#iterations: 98
currently lose_sum: 0.48229923844337463
time_elpased: 0.089
#iterations: 99
currently lose_sum: 0.33654502034187317
time_elpased: 0.091
start validation test
0.687878787879
0.716216216216
0.634730538922
0.673015873016
0.688530913633
6.322
#iterations: 100
currently lose_sum: 0.5840278267860413
time_elpased: 0.088
#iterations: 101
currently lose_sum: 0.6808756589889526
time_elpased: 0.088
#iterations: 102
currently lose_sum: 0.4133133888244629
time_elpased: 0.09
#iterations: 103
currently lose_sum: 0.5910698175430298
time_elpased: 0.09
#iterations: 104
currently lose_sum: 0.601055920124054
time_elpased: 0.091
#iterations: 105
currently lose_sum: 0.5349061489105225
time_elpased: 0.089
#iterations: 106
currently lose_sum: 0.4870196580886841
time_elpased: 0.09
#iterations: 107
currently lose_sum: 0.5686413049697876
time_elpased: 0.088
#iterations: 108
currently lose_sum: 0.36137625575065613
time_elpased: 0.091
#iterations: 109
currently lose_sum: 0.46810370683670044
time_elpased: 0.088
#iterations: 110
currently lose_sum: 0.8027788996696472
time_elpased: 0.088
#iterations: 111
currently lose_sum: 0.4981399476528168
time_elpased: 0.088
#iterations: 112
currently lose_sum: 0.4317260682582855
time_elpased: 0.089
#iterations: 113
currently lose_sum: 0.5070048570632935
time_elpased: 0.091
#iterations: 114
currently lose_sum: 0.3382398188114166
time_elpased: 0.093
#iterations: 115
currently lose_sum: 0.6909664273262024
time_elpased: 0.094
#iterations: 116
currently lose_sum: 0.49557992815971375
time_elpased: 0.094
#iterations: 117
currently lose_sum: 0.3370818793773651
time_elpased: 0.094
#iterations: 118
currently lose_sum: 0.42601367831230164
time_elpased: 0.092
#iterations: 119
currently lose_sum: 0.5487040877342224
time_elpased: 0.097
start validation test
0.530303030303
0.8
0.0958083832335
0.171122994652
0.535634252966
8.963
#iterations: 120
currently lose_sum: 0.46600499749183655
time_elpased: 0.09
#iterations: 121
currently lose_sum: 0.4128169119358063
time_elpased: 0.091
#iterations: 122
currently lose_sum: 0.5169439911842346
time_elpased: 0.092
#iterations: 123
currently lose_sum: 0.3607085645198822
time_elpased: 0.098
#iterations: 124
currently lose_sum: 0.2687954604625702
time_elpased: 0.088
#iterations: 125
currently lose_sum: 0.2432532012462616
time_elpased: 0.114
#iterations: 126
currently lose_sum: 0.5011560916900635
time_elpased: 0.096
#iterations: 127
currently lose_sum: 0.24930815398693085
time_elpased: 0.093
#iterations: 128
currently lose_sum: 0.3690849244594574
time_elpased: 0.093
#iterations: 129
currently lose_sum: 0.16960692405700684
time_elpased: 0.089
#iterations: 130
currently lose_sum: 0.3036021590232849
time_elpased: 0.088
#iterations: 131
currently lose_sum: 0.39713409543037415
time_elpased: 0.089
#iterations: 132
currently lose_sum: 0.5954126119613647
time_elpased: 0.091
#iterations: 133
currently lose_sum: 0.3487403094768524
time_elpased: 0.091
#iterations: 134
currently lose_sum: 0.4616665542125702
time_elpased: 0.09
#iterations: 135
currently lose_sum: 0.22631944715976715
time_elpased: 0.088
#iterations: 136
currently lose_sum: 0.3913952708244324
time_elpased: 0.096
#iterations: 137
currently lose_sum: 0.4438280761241913
time_elpased: 0.099
#iterations: 138
currently lose_sum: 0.3542313575744629
time_elpased: 0.09
#iterations: 139
currently lose_sum: 0.44387856125831604
time_elpased: 0.089
start validation test
0.727272727273
0.71270718232
0.77245508982
0.741379310345
0.726718342456
6.162
#iterations: 140
currently lose_sum: 0.49141553044319153
time_elpased: 0.091
#iterations: 141
currently lose_sum: 0.4323044717311859
time_elpased: 0.094
#iterations: 142
currently lose_sum: 0.2173452079296112
time_elpased: 0.096
#iterations: 143
currently lose_sum: 0.2540455758571625
time_elpased: 0.096
#iterations: 144
currently lose_sum: 0.5673345327377319
time_elpased: 0.089
#iterations: 145
currently lose_sum: 0.5012285709381104
time_elpased: 0.088
#iterations: 146
currently lose_sum: 0.675333559513092
time_elpased: 0.09
#iterations: 147
currently lose_sum: 0.35413238406181335
time_elpased: 0.096
#iterations: 148
currently lose_sum: 0.35668691992759705
time_elpased: 0.088
#iterations: 149
currently lose_sum: 0.3580639660358429
time_elpased: 0.089
#iterations: 150
currently lose_sum: 0.34159284830093384
time_elpased: 0.088
#iterations: 151
currently lose_sum: 0.2921549081802368
time_elpased: 0.088
#iterations: 152
currently lose_sum: 0.1453288048505783
time_elpased: 0.089
#iterations: 153
currently lose_sum: 1.373103380203247
time_elpased: 0.094
#iterations: 154
currently lose_sum: 0.5586652159690857
time_elpased: 0.088
#iterations: 155
currently lose_sum: 0.3582038879394531
time_elpased: 0.095
#iterations: 156
currently lose_sum: 0.6747375726699829
time_elpased: 0.11
#iterations: 157
currently lose_sum: 0.3969287872314453
time_elpased: 0.1
#iterations: 158
currently lose_sum: 0.5126155018806458
time_elpased: 0.099
#iterations: 159
currently lose_sum: 0.4759567975997925
time_elpased: 0.103
start validation test
0.575757575758
0.787234042553
0.221556886228
0.345794392523
0.580103596488
7.071
#iterations: 160
currently lose_sum: 0.332929402589798
time_elpased: 0.098
#iterations: 161
currently lose_sum: 0.6211318969726562
time_elpased: 0.127
#iterations: 162
currently lose_sum: 0.5636757612228394
time_elpased: 0.091
#iterations: 163
currently lose_sum: 0.3796056807041168
time_elpased: 0.096
#iterations: 164
currently lose_sum: 0.42495644092559814
time_elpased: 0.092
#iterations: 165
currently lose_sum: 0.5043561458587646
time_elpased: 0.109
#iterations: 166
currently lose_sum: 0.44581031799316406
time_elpased: 0.091
#iterations: 167
currently lose_sum: 0.5924444198608398
time_elpased: 0.093
#iterations: 168
currently lose_sum: 0.38005200028419495
time_elpased: 0.088
#iterations: 169
currently lose_sum: 0.410759836435318
time_elpased: 0.091
#iterations: 170
currently lose_sum: 0.30139192938804626
time_elpased: 0.089
#iterations: 171
currently lose_sum: 0.27149778604507446
time_elpased: 0.089
#iterations: 172
currently lose_sum: 0.6294144988059998
time_elpased: 0.094
#iterations: 173
currently lose_sum: 0.33437153697013855
time_elpased: 0.093
#iterations: 174
currently lose_sum: 0.3685694634914398
time_elpased: 0.089
#iterations: 175
currently lose_sum: 0.3317236602306366
time_elpased: 0.089
#iterations: 176
currently lose_sum: 0.4669342637062073
time_elpased: 0.098
#iterations: 177
currently lose_sum: 0.3835112154483795
time_elpased: 0.089
#iterations: 178
currently lose_sum: 0.4026472270488739
time_elpased: 0.095
#iterations: 179
currently lose_sum: 0.1776515692472458
time_elpased: 0.115
start validation test
0.715151515152
0.659388646288
0.904191616766
0.762626262626
0.712832004702
8.480
#iterations: 180
currently lose_sum: 0.4142809808254242
time_elpased: 0.089
#iterations: 181
currently lose_sum: 0.44693389534950256
time_elpased: 0.096
#iterations: 182
currently lose_sum: 0.400815486907959
time_elpased: 0.089
#iterations: 183
currently lose_sum: 0.349455863237381
time_elpased: 0.088
#iterations: 184
currently lose_sum: 0.4051462411880493
time_elpased: 0.088
#iterations: 185
currently lose_sum: 0.6295515894889832
time_elpased: 0.088
#iterations: 186
currently lose_sum: 0.4576662480831146
time_elpased: 0.091
#iterations: 187
currently lose_sum: 0.2791600525379181
time_elpased: 0.106
#iterations: 188
currently lose_sum: 0.2764332890510559
time_elpased: 0.087
#iterations: 189
currently lose_sum: 0.42143699526786804
time_elpased: 0.091
#iterations: 190
currently lose_sum: 0.3138333857059479
time_elpased: 0.09
#iterations: 191
currently lose_sum: 0.3476019501686096
time_elpased: 0.09
#iterations: 192
currently lose_sum: 0.4919280707836151
time_elpased: 0.087
#iterations: 193
currently lose_sum: 0.4447929561138153
time_elpased: 0.089
#iterations: 194
currently lose_sum: 0.2824823558330536
time_elpased: 0.089
#iterations: 195
currently lose_sum: 0.238731250166893
time_elpased: 0.092
#iterations: 196
currently lose_sum: 0.42020219564437866
time_elpased: 0.089
#iterations: 197
currently lose_sum: 0.5901696085929871
time_elpased: 0.087
#iterations: 198
currently lose_sum: 0.2609501779079437
time_elpased: 0.091
#iterations: 199
currently lose_sum: 0.45750150084495544
time_elpased: 0.089
start validation test
0.551515151515
0.711111111111
0.191616766467
0.301886792453
0.55593108262
10.917
#iterations: 200
currently lose_sum: 0.2656157910823822
time_elpased: 0.088
#iterations: 201
currently lose_sum: 0.3749034106731415
time_elpased: 0.097
#iterations: 202
currently lose_sum: 0.219237819314003
time_elpased: 0.09
#iterations: 203
currently lose_sum: 0.2884647250175476
time_elpased: 0.088
#iterations: 204
currently lose_sum: 0.2635665535926819
time_elpased: 0.097
#iterations: 205
currently lose_sum: 0.333511620759964
time_elpased: 0.089
#iterations: 206
currently lose_sum: 0.37758874893188477
time_elpased: 0.101
#iterations: 207
currently lose_sum: 0.17065736651420593
time_elpased: 0.095
#iterations: 208
currently lose_sum: 0.4160207211971283
time_elpased: 0.091
#iterations: 209
currently lose_sum: 0.2945775091648102
time_elpased: 0.106
#iterations: 210
currently lose_sum: 0.32072874903678894
time_elpased: 0.09
#iterations: 211
currently lose_sum: 0.6880829334259033
time_elpased: 0.089
#iterations: 212
currently lose_sum: 0.38794171810150146
time_elpased: 0.094
#iterations: 213
currently lose_sum: 0.3010755479335785
time_elpased: 0.088
#iterations: 214
currently lose_sum: 0.26619741320610046
time_elpased: 0.09
#iterations: 215
currently lose_sum: 0.37079212069511414
time_elpased: 0.087
#iterations: 216
currently lose_sum: 0.39499321579933167
time_elpased: 0.09
#iterations: 217
currently lose_sum: 0.2260230928659439
time_elpased: 0.089
#iterations: 218
currently lose_sum: 0.34818729758262634
time_elpased: 0.09
#iterations: 219
currently lose_sum: 0.4943763017654419
time_elpased: 0.089
start validation test
0.527272727273
0.823529411765
0.0838323353293
0.152173913043
0.532713713677
19.787
#iterations: 220
currently lose_sum: 0.32464638352394104
time_elpased: 0.089
#iterations: 221
currently lose_sum: 0.18710245192050934
time_elpased: 0.098
#iterations: 222
currently lose_sum: 0.3207074999809265
time_elpased: 0.098
#iterations: 223
currently lose_sum: 0.11794193089008331
time_elpased: 0.089
#iterations: 224
currently lose_sum: 0.34686774015426636
time_elpased: 0.1
#iterations: 225
currently lose_sum: 0.5136858820915222
time_elpased: 0.096
#iterations: 226
currently lose_sum: 0.25934189558029175
time_elpased: 0.089
#iterations: 227
currently lose_sum: 0.14089244604110718
time_elpased: 0.088
#iterations: 228
currently lose_sum: 0.2540034055709839
time_elpased: 0.092
#iterations: 229
currently lose_sum: 0.26849666237831116
time_elpased: 0.09
#iterations: 230
currently lose_sum: 0.5556641817092896
time_elpased: 0.091
#iterations: 231
currently lose_sum: 0.25801733136177063
time_elpased: 0.097
#iterations: 232
currently lose_sum: 0.5738155841827393
time_elpased: 0.09
#iterations: 233
currently lose_sum: 0.23148582875728607
time_elpased: 0.09
#iterations: 234
currently lose_sum: 0.25891610980033875
time_elpased: 0.092
#iterations: 235
currently lose_sum: 0.2988396883010864
time_elpased: 0.091
#iterations: 236
currently lose_sum: 0.35041096806526184
time_elpased: 0.088
#iterations: 237
currently lose_sum: 0.2704978287220001
time_elpased: 0.09
#iterations: 238
currently lose_sum: 0.5947476029396057
time_elpased: 0.093
#iterations: 239
currently lose_sum: 0.29822832345962524
time_elpased: 0.09
start validation test
0.606060606061
0.768115942029
0.317365269461
0.449152542373
0.609602880129
8.391
#iterations: 240
currently lose_sum: 0.13192756474018097
time_elpased: 0.088
#iterations: 241
currently lose_sum: 0.2797928750514984
time_elpased: 0.091
#iterations: 242
currently lose_sum: 0.3478042483329773
time_elpased: 0.094
#iterations: 243
currently lose_sum: 0.3031270205974579
time_elpased: 0.09
#iterations: 244
currently lose_sum: 0.2245568484067917
time_elpased: 0.092
#iterations: 245
currently lose_sum: 0.6184706091880798
time_elpased: 0.091
#iterations: 246
currently lose_sum: 0.23777587711811066
time_elpased: 0.089
#iterations: 247
currently lose_sum: 0.23875752091407776
time_elpased: 0.088
#iterations: 248
currently lose_sum: 0.22682759165763855
time_elpased: 0.09
#iterations: 249
currently lose_sum: 0.47610828280448914
time_elpased: 0.095
#iterations: 250
currently lose_sum: 0.17282460629940033
time_elpased: 0.089
#iterations: 251
currently lose_sum: 0.34199777245521545
time_elpased: 0.092
#iterations: 252
currently lose_sum: 0.09900687634944916
time_elpased: 0.109
#iterations: 253
currently lose_sum: 0.2080000787973404
time_elpased: 0.096
#iterations: 254
currently lose_sum: 0.4093548655509949
time_elpased: 0.095
#iterations: 255
currently lose_sum: 0.7938750982284546
time_elpased: 0.093
#iterations: 256
currently lose_sum: 0.46486395597457886
time_elpased: 0.095
#iterations: 257
currently lose_sum: 0.2894795835018158
time_elpased: 0.093
#iterations: 258
currently lose_sum: 0.1452879011631012
time_elpased: 0.101
#iterations: 259
currently lose_sum: 0.27763107419013977
time_elpased: 0.097
start validation test
0.545454545455
0.814814814815
0.131736526946
0.226804123711
0.55053084016
15.912
#iterations: 260
currently lose_sum: 0.09724240005016327
time_elpased: 0.096
#iterations: 261
currently lose_sum: 0.31718260049819946
time_elpased: 0.098
#iterations: 262
currently lose_sum: 0.6470610499382019
time_elpased: 0.091
#iterations: 263
currently lose_sum: 0.24605236947536469
time_elpased: 0.096
#iterations: 264
currently lose_sum: 0.4416741728782654
time_elpased: 0.094
#iterations: 265
currently lose_sum: 0.1953759342432022
time_elpased: 0.098
#iterations: 266
currently lose_sum: 0.1293194442987442
time_elpased: 0.096
#iterations: 267
currently lose_sum: 0.27230116724967957
time_elpased: 0.091
#iterations: 268
currently lose_sum: 0.4590838551521301
time_elpased: 0.109
#iterations: 269
currently lose_sum: 0.6802141666412354
time_elpased: 0.09
#iterations: 270
currently lose_sum: 0.3726974129676819
time_elpased: 0.091
#iterations: 271
currently lose_sum: 0.314361035823822
time_elpased: 0.091
#iterations: 272
currently lose_sum: 0.24114713072776794
time_elpased: 0.089
#iterations: 273
currently lose_sum: 0.11601376533508301
time_elpased: 0.089
#iterations: 274
currently lose_sum: 0.5001906156539917
time_elpased: 0.095
#iterations: 275
currently lose_sum: 0.34114545583724976
time_elpased: 0.091
#iterations: 276
currently lose_sum: 0.1574804186820984
time_elpased: 0.089
#iterations: 277
currently lose_sum: 0.8232238292694092
time_elpased: 0.09
#iterations: 278
currently lose_sum: 0.3122444450855255
time_elpased: 0.088
#iterations: 279
currently lose_sum: 0.310858815908432
time_elpased: 0.089
start validation test
0.618181818182
0.715789473684
0.407185628743
0.519083969466
0.620770728482
8.518
#iterations: 280
currently lose_sum: 0.6601962447166443
time_elpased: 0.092
#iterations: 281
currently lose_sum: 0.4981321394443512
time_elpased: 0.096
#iterations: 282
currently lose_sum: 0.4564516246318817
time_elpased: 0.099
#iterations: 283
currently lose_sum: 0.2638762295246124
time_elpased: 0.09
#iterations: 284
currently lose_sum: 0.1882232129573822
time_elpased: 0.088
#iterations: 285
currently lose_sum: 0.28535348176956177
time_elpased: 0.105
#iterations: 286
currently lose_sum: 0.43338197469711304
time_elpased: 0.093
#iterations: 287
currently lose_sum: 0.2467702329158783
time_elpased: 0.089
#iterations: 288
currently lose_sum: 0.2402738779783249
time_elpased: 0.096
#iterations: 289
currently lose_sum: 0.39412572979927063
time_elpased: 0.091
#iterations: 290
currently lose_sum: 0.21994081139564514
time_elpased: 0.098
#iterations: 291
currently lose_sum: 0.23594780266284943
time_elpased: 0.09
#iterations: 292
currently lose_sum: 0.3810541033744812
time_elpased: 0.088
#iterations: 293
currently lose_sum: 0.35676103830337524
time_elpased: 0.1
#iterations: 294
currently lose_sum: 0.22269421815872192
time_elpased: 0.088
#iterations: 295
currently lose_sum: 0.21526631712913513
time_elpased: 0.089
#iterations: 296
currently lose_sum: 0.26873916387557983
time_elpased: 0.087
#iterations: 297
currently lose_sum: 0.1889476776123047
time_elpased: 0.098
#iterations: 298
currently lose_sum: 0.2650783658027649
time_elpased: 0.092
#iterations: 299
currently lose_sum: 0.222439706325531
time_elpased: 0.094
start validation test
0.590909090909
0.742424242424
0.293413173653
0.420600858369
0.594559347563
9.133
#iterations: 300
currently lose_sum: 0.17663195729255676
time_elpased: 0.09
#iterations: 301
currently lose_sum: 0.3115560710430145
time_elpased: 0.088
#iterations: 302
currently lose_sum: 0.24943441152572632
time_elpased: 0.1
#iterations: 303
currently lose_sum: 0.3649672865867615
time_elpased: 0.089
#iterations: 304
currently lose_sum: 0.2496417611837387
time_elpased: 0.094
#iterations: 305
currently lose_sum: 0.1946675330400467
time_elpased: 0.096
#iterations: 306
currently lose_sum: 0.1376127302646637
time_elpased: 0.097
#iterations: 307
currently lose_sum: 0.3762699067592621
time_elpased: 0.09
#iterations: 308
currently lose_sum: 0.305866539478302
time_elpased: 0.09
#iterations: 309
currently lose_sum: 0.2341349720954895
time_elpased: 0.088
#iterations: 310
currently lose_sum: 0.4306977093219757
time_elpased: 0.099
#iterations: 311
currently lose_sum: 0.623300313949585
time_elpased: 0.093
#iterations: 312
currently lose_sum: 0.2947004437446594
time_elpased: 0.092
#iterations: 313
currently lose_sum: 0.26508980989456177
time_elpased: 0.088
#iterations: 314
currently lose_sum: 0.1292889267206192
time_elpased: 0.107
#iterations: 315
currently lose_sum: 0.08720411360263824
time_elpased: 0.09
#iterations: 316
currently lose_sum: 0.16919350624084473
time_elpased: 0.09
#iterations: 317
currently lose_sum: 0.21476192772388458
time_elpased: 0.089
#iterations: 318
currently lose_sum: 0.21493588387966156
time_elpased: 0.091
#iterations: 319
currently lose_sum: 0.1847022920846939
time_elpased: 0.089
start validation test
0.581818181818
0.78431372549
0.239520958084
0.366972477064
0.586018147754
11.632
#iterations: 320
currently lose_sum: 1.0452767610549927
time_elpased: 0.089
#iterations: 321
currently lose_sum: 0.4290611147880554
time_elpased: 0.089
#iterations: 322
currently lose_sum: 0.26917675137519836
time_elpased: 0.088
#iterations: 323
currently lose_sum: 0.26178374886512756
time_elpased: 0.092
#iterations: 324
currently lose_sum: 0.2910308837890625
time_elpased: 0.089
#iterations: 325
currently lose_sum: 0.23000702261924744
time_elpased: 0.091
#iterations: 326
currently lose_sum: 0.487527459859848
time_elpased: 0.096
#iterations: 327
currently lose_sum: 0.19782012701034546
time_elpased: 0.098
#iterations: 328
currently lose_sum: 0.44425973296165466
time_elpased: 0.089
#iterations: 329
currently lose_sum: 0.13800054788589478
time_elpased: 0.097
#iterations: 330
currently lose_sum: 0.22732774913311005
time_elpased: 0.091
#iterations: 331
currently lose_sum: 0.2915482223033905
time_elpased: 0.092
#iterations: 332
currently lose_sum: 0.3691777288913727
time_elpased: 0.089
#iterations: 333
currently lose_sum: 0.5407245755195618
time_elpased: 0.088
#iterations: 334
currently lose_sum: 0.3261328935623169
time_elpased: 0.087
#iterations: 335
currently lose_sum: 0.2691818177700043
time_elpased: 0.092
#iterations: 336
currently lose_sum: 0.21616362035274506
time_elpased: 0.088
#iterations: 337
currently lose_sum: 0.12910328805446625
time_elpased: 0.09
#iterations: 338
currently lose_sum: 0.20444601774215698
time_elpased: 0.092
#iterations: 339
currently lose_sum: 0.6391822099685669
time_elpased: 0.092
start validation test
0.684848484848
0.65671641791
0.790419161677
0.717391304348
0.683553139121
8.240
#iterations: 340
currently lose_sum: 0.2503702938556671
time_elpased: 0.094
#iterations: 341
currently lose_sum: 0.18834397196769714
time_elpased: 0.089
#iterations: 342
currently lose_sum: 0.15206366777420044
time_elpased: 0.126
#iterations: 343
currently lose_sum: 0.16067613661289215
time_elpased: 0.093
#iterations: 344
currently lose_sum: 0.34080636501312256
time_elpased: 0.09
#iterations: 345
currently lose_sum: 0.07129307836294174
time_elpased: 0.088
#iterations: 346
currently lose_sum: 0.7763614058494568
time_elpased: 0.09
#iterations: 347
currently lose_sum: 0.6504673361778259
time_elpased: 0.091
#iterations: 348
currently lose_sum: 0.16559092700481415
time_elpased: 0.09
#iterations: 349
currently lose_sum: 0.2155000865459442
time_elpased: 0.089
#iterations: 350
currently lose_sum: 0.09095647186040878
time_elpased: 0.087
#iterations: 351
currently lose_sum: 0.45758122205734253
time_elpased: 0.089
#iterations: 352
currently lose_sum: 0.16489951312541962
time_elpased: 0.091
#iterations: 353
currently lose_sum: 0.7252832055091858
time_elpased: 0.101
#iterations: 354
currently lose_sum: nan
time_elpased: 0.096
train finish final lose is: nan
acc: 0.709
pre: 0.735
rec: 0.701
F1: 0.718
auc: 0.710
