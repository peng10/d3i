start to construct computing graph
graph construct over
508
epochs start
#iterations: 0
currently lose_sum: 0.7024416327476501
time_elpased: 0.095
#iterations: 1
currently lose_sum: 0.6917912364006042
time_elpased: 0.073
#iterations: 2
currently lose_sum: 0.6999655961990356
time_elpased: 0.073
#iterations: 3
currently lose_sum: 0.6537272334098816
time_elpased: 0.073
#iterations: 4
currently lose_sum: 0.6784412860870361
time_elpased: 0.072
#iterations: 5
currently lose_sum: 0.6945894956588745
time_elpased: 0.073
#iterations: 6
currently lose_sum: 0.6739943027496338
time_elpased: 0.077
#iterations: 7
currently lose_sum: 0.6615607738494873
time_elpased: 0.073
#iterations: 8
currently lose_sum: 0.6504308581352234
time_elpased: 0.072
#iterations: 9
currently lose_sum: 0.5288099050521851
time_elpased: 0.08
#iterations: 10
currently lose_sum: 0.646611750125885
time_elpased: 0.078
#iterations: 11
currently lose_sum: 0.7629197239875793
time_elpased: 0.073
#iterations: 12
currently lose_sum: 0.6705866456031799
time_elpased: 0.079
#iterations: 13
currently lose_sum: 0.6615273356437683
time_elpased: 0.072
#iterations: 14
currently lose_sum: 0.6052131652832031
time_elpased: 0.074
#iterations: 15
currently lose_sum: 0.5544015169143677
time_elpased: 0.074
#iterations: 16
currently lose_sum: 0.6266698241233826
time_elpased: 0.076
#iterations: 17
currently lose_sum: 0.641249418258667
time_elpased: 0.072
#iterations: 18
currently lose_sum: 0.52239990234375
time_elpased: 0.073
#iterations: 19
currently lose_sum: 0.5767443776130676
time_elpased: 0.073
start validation test
0.669696969697
0.737704918033
0.538922155689
0.622837370242
0.671301568642
6.694
#iterations: 20
currently lose_sum: 0.6262636184692383
time_elpased: 0.075
#iterations: 21
currently lose_sum: 0.6008957624435425
time_elpased: 0.076
#iterations: 22
currently lose_sum: 0.6485520601272583
time_elpased: 0.073
#iterations: 23
currently lose_sum: 0.5777946710586548
time_elpased: 0.073
#iterations: 24
currently lose_sum: 0.5330366492271423
time_elpased: 0.078
#iterations: 25
currently lose_sum: 0.674994170665741
time_elpased: 0.088
#iterations: 26
currently lose_sum: 0.4397645592689514
time_elpased: 0.073
#iterations: 27
currently lose_sum: 0.5422888398170471
time_elpased: 0.072
#iterations: 28
currently lose_sum: 0.5500416159629822
time_elpased: 0.073
#iterations: 29
currently lose_sum: 0.5271524786949158
time_elpased: 0.072
#iterations: 30
currently lose_sum: 0.604213297367096
time_elpased: 0.073
#iterations: 31
currently lose_sum: 0.5630189180374146
time_elpased: 0.073
#iterations: 32
currently lose_sum: 0.6482320427894592
time_elpased: 0.077
#iterations: 33
currently lose_sum: 0.6127105355262756
time_elpased: 0.072
#iterations: 34
currently lose_sum: 0.5709087252616882
time_elpased: 0.074
#iterations: 35
currently lose_sum: 0.44859302043914795
time_elpased: 0.073
#iterations: 36
currently lose_sum: 0.47585007548332214
time_elpased: 0.074
#iterations: 37
currently lose_sum: 0.6708694696426392
time_elpased: 0.075
#iterations: 38
currently lose_sum: 0.5259788036346436
time_elpased: 0.073
#iterations: 39
currently lose_sum: 0.5063825249671936
time_elpased: 0.072
start validation test
0.721212121212
0.724550898204
0.724550898204
0.724550898204
0.721171154623
6.265
#iterations: 40
currently lose_sum: 0.5740157961845398
time_elpased: 0.075
#iterations: 41
currently lose_sum: 0.4953468441963196
time_elpased: 0.074
#iterations: 42
currently lose_sum: 0.513595461845398
time_elpased: 0.073
#iterations: 43
currently lose_sum: 0.6018266081809998
time_elpased: 0.077
#iterations: 44
currently lose_sum: 0.6465383768081665
time_elpased: 0.074
#iterations: 45
currently lose_sum: 0.5383403301239014
time_elpased: 0.073
#iterations: 46
currently lose_sum: 0.46616628766059875
time_elpased: 0.08
#iterations: 47
currently lose_sum: 0.41862624883651733
time_elpased: 0.078
#iterations: 48
currently lose_sum: 0.5795518755912781
time_elpased: 0.073
#iterations: 49
currently lose_sum: 0.5513558983802795
time_elpased: 0.072
#iterations: 50
currently lose_sum: 0.5394762754440308
time_elpased: 0.072
#iterations: 51
currently lose_sum: 0.5935827493667603
time_elpased: 0.08
#iterations: 52
currently lose_sum: 0.46578842401504517
time_elpased: 0.074
#iterations: 53
currently lose_sum: 0.5193518400192261
time_elpased: 0.079
#iterations: 54
currently lose_sum: 0.5616271495819092
time_elpased: 0.075
#iterations: 55
currently lose_sum: 0.4483652710914612
time_elpased: 0.104
#iterations: 56
currently lose_sum: 0.5323165059089661
time_elpased: 0.073
#iterations: 57
currently lose_sum: 0.431928813457489
time_elpased: 0.072
#iterations: 58
currently lose_sum: 0.5238009691238403
time_elpased: 0.073
#iterations: 59
currently lose_sum: 0.5338590741157532
time_elpased: 0.081
start validation test
0.615151515152
0.743902439024
0.365269461078
0.489959839357
0.618217552625
7.294
#iterations: 60
currently lose_sum: 0.48591047525405884
time_elpased: 0.076
#iterations: 61
currently lose_sum: 0.4572555720806122
time_elpased: 0.08
#iterations: 62
currently lose_sum: 0.43426698446273804
time_elpased: 0.072
#iterations: 63
currently lose_sum: 0.475929319858551
time_elpased: 0.072
#iterations: 64
currently lose_sum: 0.43917906284332275
time_elpased: 0.072
#iterations: 65
currently lose_sum: 0.6077735424041748
time_elpased: 0.078
#iterations: 66
currently lose_sum: 0.81235671043396
time_elpased: 0.08
#iterations: 67
currently lose_sum: 0.33046576380729675
time_elpased: 0.072
#iterations: 68
currently lose_sum: 0.41602426767349243
time_elpased: 0.073
#iterations: 69
currently lose_sum: 0.4379458427429199
time_elpased: 0.072
#iterations: 70
currently lose_sum: 0.38155582547187805
time_elpased: 0.078
#iterations: 71
currently lose_sum: 0.4761669337749481
time_elpased: 0.073
#iterations: 72
currently lose_sum: 0.6151197552680969
time_elpased: 0.072
#iterations: 73
currently lose_sum: 0.37722453474998474
time_elpased: 0.073
#iterations: 74
currently lose_sum: 0.5232251882553101
time_elpased: 0.074
#iterations: 75
currently lose_sum: 0.2912661135196686
time_elpased: 0.073
#iterations: 76
currently lose_sum: 0.4396619498729706
time_elpased: 0.074
#iterations: 77
currently lose_sum: 0.514104962348938
time_elpased: 0.075
#iterations: 78
currently lose_sum: 0.4979993999004364
time_elpased: 0.073
#iterations: 79
currently lose_sum: 0.3979518711566925
time_elpased: 0.073
start validation test
0.724242424242
0.753333333333
0.676646706587
0.712933753943
0.724826420778
6.182
#iterations: 80
currently lose_sum: 0.5503529906272888
time_elpased: 0.081
#iterations: 81
currently lose_sum: 0.36373910307884216
time_elpased: 0.072
#iterations: 82
currently lose_sum: 0.5691691040992737
time_elpased: 0.072
#iterations: 83
currently lose_sum: 0.5942786931991577
time_elpased: 0.072
#iterations: 84
currently lose_sum: 0.484176903963089
time_elpased: 0.075
#iterations: 85
currently lose_sum: 0.5169885158538818
time_elpased: 0.074
#iterations: 86
currently lose_sum: 0.4721948504447937
time_elpased: 0.077
#iterations: 87
currently lose_sum: 0.4496021568775177
time_elpased: 0.077
#iterations: 88
currently lose_sum: 0.3685623109340668
time_elpased: 0.076
#iterations: 89
currently lose_sum: 0.47283124923706055
time_elpased: 0.075
#iterations: 90
currently lose_sum: 0.47471001744270325
time_elpased: 0.078
#iterations: 91
currently lose_sum: 0.5485228300094604
time_elpased: 0.073
#iterations: 92
currently lose_sum: 0.35836419463157654
time_elpased: 0.079
#iterations: 93
currently lose_sum: 0.42525556683540344
time_elpased: 0.073
#iterations: 94
currently lose_sum: 0.47905051708221436
time_elpased: 0.075
#iterations: 95
currently lose_sum: 0.32618799805641174
time_elpased: 0.089
#iterations: 96
currently lose_sum: 0.3940407931804657
time_elpased: 0.073
#iterations: 97
currently lose_sum: 0.655608057975769
time_elpased: 0.073
#iterations: 98
currently lose_sum: 0.6633248925209045
time_elpased: 0.073
#iterations: 99
currently lose_sum: 0.3898623287677765
time_elpased: 0.073
start validation test
0.733333333333
0.768707482993
0.676646706587
0.71974522293
0.734028874766
6.348
#iterations: 100
currently lose_sum: 0.5380071997642517
time_elpased: 0.079
#iterations: 101
currently lose_sum: 0.5169263482093811
time_elpased: 0.072
#iterations: 102
currently lose_sum: 0.3513408601284027
time_elpased: 0.072
#iterations: 103
currently lose_sum: 0.4464917480945587
time_elpased: 0.08
#iterations: 104
currently lose_sum: 0.4600769579410553
time_elpased: 0.073
#iterations: 105
currently lose_sum: 0.4816003143787384
time_elpased: 0.073
#iterations: 106
currently lose_sum: 0.4210786521434784
time_elpased: 0.074
#iterations: 107
currently lose_sum: 0.37408745288848877
time_elpased: 0.079
#iterations: 108
currently lose_sum: 0.3112889528274536
time_elpased: 0.072
#iterations: 109
currently lose_sum: 0.47155168652534485
time_elpased: 0.074
#iterations: 110
currently lose_sum: 0.502389132976532
time_elpased: 0.075
#iterations: 111
currently lose_sum: 0.39427655935287476
time_elpased: 0.076
#iterations: 112
currently lose_sum: 0.6711764931678772
time_elpased: 0.072
#iterations: 113
currently lose_sum: 0.5646049976348877
time_elpased: 0.073
#iterations: 114
currently lose_sum: 0.3351905643939972
time_elpased: 0.072
#iterations: 115
currently lose_sum: 0.5300979018211365
time_elpased: 0.073
#iterations: 116
currently lose_sum: 0.46027326583862305
time_elpased: 0.074
#iterations: 117
currently lose_sum: 0.26936474442481995
time_elpased: 0.072
#iterations: 118
currently lose_sum: 0.3387942612171173
time_elpased: 0.073
#iterations: 119
currently lose_sum: 0.3371654748916626
time_elpased: 0.075
start validation test
0.675757575758
0.788461538462
0.491017964072
0.605166051661
0.678024319459
7.430
#iterations: 120
currently lose_sum: 0.45388907194137573
time_elpased: 0.075
#iterations: 121
currently lose_sum: 0.5097836852073669
time_elpased: 0.073
#iterations: 122
currently lose_sum: 0.4465124309062958
time_elpased: 0.073
#iterations: 123
currently lose_sum: 0.2926281690597534
time_elpased: 0.073
#iterations: 124
currently lose_sum: 0.31168821454048157
time_elpased: 0.072
#iterations: 125
currently lose_sum: 0.26569995284080505
time_elpased: 0.075
#iterations: 126
currently lose_sum: 0.7117935419082642
time_elpased: 0.073
#iterations: 127
currently lose_sum: 0.32173794507980347
time_elpased: 0.072
#iterations: 128
currently lose_sum: 0.36489713191986084
time_elpased: 0.077
#iterations: 129
currently lose_sum: 0.21295343339443207
time_elpased: 0.073
#iterations: 130
currently lose_sum: 0.40808695554733276
time_elpased: 0.072
#iterations: 131
currently lose_sum: 0.46318843960762024
time_elpased: 0.072
#iterations: 132
currently lose_sum: 0.5506933927536011
time_elpased: 0.073
#iterations: 133
currently lose_sum: 0.4307228922843933
time_elpased: 0.073
#iterations: 134
currently lose_sum: 0.4439326822757721
time_elpased: 0.072
#iterations: 135
currently lose_sum: 0.41539573669433594
time_elpased: 0.077
#iterations: 136
currently lose_sum: 0.32754695415496826
time_elpased: 0.079
#iterations: 137
currently lose_sum: 0.6088590025901794
time_elpased: 0.073
#iterations: 138
currently lose_sum: 0.2631009817123413
time_elpased: 0.072
#iterations: 139
currently lose_sum: 0.3410312831401825
time_elpased: 0.073
start validation test
0.712121212121
0.768656716418
0.616766467066
0.68438538206
0.713291208993
6.584
#iterations: 140
currently lose_sum: 0.34887799620628357
time_elpased: 0.075
#iterations: 141
currently lose_sum: 0.6655498743057251
time_elpased: 0.075
#iterations: 142
currently lose_sum: 0.2400609850883484
time_elpased: 0.072
#iterations: 143
currently lose_sum: 0.3230964243412018
time_elpased: 0.072
#iterations: 144
currently lose_sum: 0.3787548243999481
time_elpased: 0.072
#iterations: 145
currently lose_sum: 0.5568256974220276
time_elpased: 0.079
#iterations: 146
currently lose_sum: 0.4250398874282837
time_elpased: 0.073
#iterations: 147
currently lose_sum: 0.3427213132381439
time_elpased: 0.072
#iterations: 148
currently lose_sum: 0.26265424489974976
time_elpased: 0.072
#iterations: 149
currently lose_sum: 0.6249881386756897
time_elpased: 0.079
#iterations: 150
currently lose_sum: 0.33658087253570557
time_elpased: 0.073
#iterations: 151
currently lose_sum: 0.38192957639694214
time_elpased: 0.072
#iterations: 152
currently lose_sum: 0.23329660296440125
time_elpased: 0.072
#iterations: 153
currently lose_sum: 0.42629656195640564
time_elpased: 0.072
#iterations: 154
currently lose_sum: 0.5193763375282288
time_elpased: 0.072
#iterations: 155
currently lose_sum: 0.3821638524532318
time_elpased: 0.073
#iterations: 156
currently lose_sum: 0.4209200441837311
time_elpased: 0.072
#iterations: 157
currently lose_sum: 0.3317371606826782
time_elpased: 0.072
#iterations: 158
currently lose_sum: 0.4863850772380829
time_elpased: 0.087
#iterations: 159
currently lose_sum: 0.33272621035575867
time_elpased: 0.082
start validation test
0.660606060606
0.772277227723
0.467065868263
0.582089552239
0.662980786892
7.721
#iterations: 160
currently lose_sum: 0.3278008699417114
time_elpased: 0.076
#iterations: 161
currently lose_sum: 1.0423201322555542
time_elpased: 0.072
#iterations: 162
currently lose_sum: 0.3820458948612213
time_elpased: 0.086
#iterations: 163
currently lose_sum: 0.6568079590797424
time_elpased: 0.072
#iterations: 164
currently lose_sum: 0.41433727741241455
time_elpased: 0.072
#iterations: 165
currently lose_sum: 0.36030861735343933
time_elpased: 0.071
#iterations: 166
currently lose_sum: 0.42351433634757996
time_elpased: 0.072
#iterations: 167
currently lose_sum: 0.5149596333503723
time_elpased: 0.073
#iterations: 168
currently lose_sum: 0.24649973213672638
time_elpased: 0.073
#iterations: 169
currently lose_sum: 0.3403133153915405
time_elpased: 0.076
#iterations: 170
currently lose_sum: 0.32271644473075867
time_elpased: 0.073
#iterations: 171
currently lose_sum: 0.564075767993927
time_elpased: 0.072
#iterations: 172
currently lose_sum: 0.3388769328594208
time_elpased: 0.078
#iterations: 173
currently lose_sum: 0.3539535403251648
time_elpased: 0.072
#iterations: 174
currently lose_sum: 0.2891138792037964
time_elpased: 0.073
#iterations: 175
currently lose_sum: 0.3963947594165802
time_elpased: 0.106
#iterations: 176
currently lose_sum: 0.43569332361221313
time_elpased: 0.071
#iterations: 177
currently lose_sum: 0.4204100966453552
time_elpased: 0.074
#iterations: 178
currently lose_sum: 0.6830328702926636
time_elpased: 0.073
#iterations: 179
currently lose_sum: 0.28000250458717346
time_elpased: 0.072
start validation test
0.687878787879
0.677777777778
0.730538922156
0.703170028818
0.687355350648
6.329
#iterations: 180
currently lose_sum: 0.3963063359260559
time_elpased: 0.073
#iterations: 181
currently lose_sum: 0.3673323690891266
time_elpased: 0.074
#iterations: 182
currently lose_sum: 0.3990274667739868
time_elpased: 0.072
#iterations: 183
currently lose_sum: 0.4558543264865875
time_elpased: 0.076
#iterations: 184
currently lose_sum: 0.34260883927345276
time_elpased: 0.075
#iterations: 185
currently lose_sum: 0.15085649490356445
time_elpased: 0.075
#iterations: 186
currently lose_sum: 0.22273173928260803
time_elpased: 0.074
#iterations: 187
currently lose_sum: 0.29480743408203125
time_elpased: 0.073
#iterations: 188
currently lose_sum: 0.5004897117614746
time_elpased: 0.072
#iterations: 189
currently lose_sum: 0.23046916723251343
time_elpased: 0.073
#iterations: 190
currently lose_sum: 0.29961153864860535
time_elpased: 0.073
#iterations: 191
currently lose_sum: 0.5277877449989319
time_elpased: 0.072
#iterations: 192
currently lose_sum: 0.31770971417427063
time_elpased: 0.072
#iterations: 193
currently lose_sum: 0.30486783385276794
time_elpased: 0.072
#iterations: 194
currently lose_sum: 0.3893824815750122
time_elpased: 0.089
#iterations: 195
currently lose_sum: 0.530123770236969
time_elpased: 0.073
#iterations: 196
currently lose_sum: 0.5844009518623352
time_elpased: 0.085
#iterations: 197
currently lose_sum: 0.32885631918907166
time_elpased: 0.078
#iterations: 198
currently lose_sum: 0.28890129923820496
time_elpased: 0.073
#iterations: 199
currently lose_sum: 0.3759564161300659
time_elpased: 0.08
start validation test
0.6
0.753623188406
0.311377245509
0.440677966102
0.603541383491
9.077
#iterations: 200
currently lose_sum: 0.32438284158706665
time_elpased: 0.074
#iterations: 201
currently lose_sum: 0.4397064745426178
time_elpased: 0.074
#iterations: 202
currently lose_sum: 0.30428075790405273
time_elpased: 0.072
#iterations: 203
currently lose_sum: 0.31977447867393494
time_elpased: 0.081
#iterations: 204
currently lose_sum: 0.22149761021137238
time_elpased: 0.073
#iterations: 205
currently lose_sum: 0.4948573112487793
time_elpased: 0.08
#iterations: 206
currently lose_sum: 0.47994592785835266
time_elpased: 0.072
#iterations: 207
currently lose_sum: 0.23737488687038422
time_elpased: 0.076
#iterations: 208
currently lose_sum: 0.49779605865478516
time_elpased: 0.073
#iterations: 209
currently lose_sum: 0.21947871148586273
time_elpased: 0.083
#iterations: 210
currently lose_sum: 0.24853642284870148
time_elpased: 0.073
#iterations: 211
currently lose_sum: 0.46262332797050476
time_elpased: 0.073
#iterations: 212
currently lose_sum: 0.258147656917572
time_elpased: 0.106
#iterations: 213
currently lose_sum: 0.20959921181201935
time_elpased: 0.072
#iterations: 214
currently lose_sum: 0.2100672423839569
time_elpased: 0.076
#iterations: 215
currently lose_sum: 0.3894003927707672
time_elpased: 0.073
#iterations: 216
currently lose_sum: 0.14296381175518036
time_elpased: 0.073
#iterations: 217
currently lose_sum: 0.19053640961647034
time_elpased: 0.079
#iterations: 218
currently lose_sum: 0.291018009185791
time_elpased: 0.072
#iterations: 219
currently lose_sum: 0.34878355264663696
time_elpased: 0.072
start validation test
0.530303030303
0.833333333333
0.0898203592814
0.162162162162
0.535707725653
15.363
#iterations: 220
currently lose_sum: 0.2436911016702652
time_elpased: 0.072
#iterations: 221
currently lose_sum: 0.20423798263072968
time_elpased: 0.083
#iterations: 222
currently lose_sum: 0.4587661623954773
time_elpased: 0.075
#iterations: 223
currently lose_sum: 0.14413557946681976
time_elpased: 0.072
#iterations: 224
currently lose_sum: 0.38103094696998596
time_elpased: 0.072
#iterations: 225
currently lose_sum: 0.17596827447414398
time_elpased: 0.089
#iterations: 226
currently lose_sum: 0.4154682159423828
time_elpased: 0.072
#iterations: 227
currently lose_sum: 0.3918885588645935
time_elpased: 0.073
#iterations: 228
currently lose_sum: 0.2767120599746704
time_elpased: 0.093
#iterations: 229
currently lose_sum: 0.2005711942911148
time_elpased: 0.072
#iterations: 230
currently lose_sum: 0.6122581362724304
time_elpased: 0.088
#iterations: 231
currently lose_sum: 0.2565184235572815
time_elpased: 0.072
#iterations: 232
currently lose_sum: 0.23734593391418457
time_elpased: 0.072
#iterations: 233
currently lose_sum: 0.2186066061258316
time_elpased: 0.072
#iterations: 234
currently lose_sum: 0.4686312973499298
time_elpased: 0.079
#iterations: 235
currently lose_sum: 0.41207873821258545
time_elpased: 0.072
#iterations: 236
currently lose_sum: 0.32590338587760925
time_elpased: 0.074
#iterations: 237
currently lose_sum: 0.43888458609580994
time_elpased: 0.073
#iterations: 238
currently lose_sum: 0.36431884765625
time_elpased: 0.074
#iterations: 239
currently lose_sum: 0.3349466919898987
time_elpased: 0.073
start validation test
0.693939393939
0.775
0.556886227545
0.648083623693
0.695621027883
7.178
#iterations: 240
currently lose_sum: 0.21625080704689026
time_elpased: 0.074
#iterations: 241
currently lose_sum: 0.2982986867427826
time_elpased: 0.072
#iterations: 242
currently lose_sum: 0.14308707416057587
time_elpased: 0.073
#iterations: 243
currently lose_sum: 1.0497257709503174
time_elpased: 0.073
#iterations: 244
currently lose_sum: 0.4213135540485382
time_elpased: 0.073
#iterations: 245
currently lose_sum: 0.33712098002433777
time_elpased: 0.072
#iterations: 246
currently lose_sum: 0.24124674499034882
time_elpased: 0.083
#iterations: 247
currently lose_sum: 0.26054835319519043
time_elpased: 0.073
#iterations: 248
currently lose_sum: 0.21865995228290558
time_elpased: 0.074
#iterations: 249
currently lose_sum: 0.41760972142219543
time_elpased: 0.074
#iterations: 250
currently lose_sum: 0.17961470782756805
time_elpased: 0.073
#iterations: 251
currently lose_sum: 0.2049003392457962
time_elpased: 0.072
#iterations: 252
currently lose_sum: 0.8234330415725708
time_elpased: 0.079
#iterations: 253
currently lose_sum: 0.35026952624320984
time_elpased: 0.073
#iterations: 254
currently lose_sum: 0.4435324966907501
time_elpased: 0.072
#iterations: 255
currently lose_sum: 0.34764590859413147
time_elpased: 0.072
#iterations: 256
currently lose_sum: 0.3950163722038269
time_elpased: 0.074
#iterations: 257
currently lose_sum: 0.33839893341064453
time_elpased: 0.073
#iterations: 258
currently lose_sum: 0.24073772132396698
time_elpased: 0.072
#iterations: 259
currently lose_sum: 0.5815982818603516
time_elpased: 0.074
start validation test
0.709090909091
0.729032258065
0.676646706587
0.701863354037
0.709488997465
7.115
#iterations: 260
currently lose_sum: 0.24308297038078308
time_elpased: 0.077
#iterations: 261
currently lose_sum: 0.4390052258968353
time_elpased: 0.072
#iterations: 262
currently lose_sum: 0.6603360772132874
time_elpased: 0.076
#iterations: 263
currently lose_sum: 0.20571912825107574
time_elpased: 0.072
#iterations: 264
currently lose_sum: 0.35285431146621704
time_elpased: 0.073
#iterations: 265
currently lose_sum: 0.25809091329574585
time_elpased: 0.073
#iterations: 266
currently lose_sum: 0.16185127198696136
time_elpased: 0.074
#iterations: 267
currently lose_sum: 0.3652840852737427
time_elpased: 0.076
#iterations: 268
currently lose_sum: 0.5285443067550659
time_elpased: 0.073
#iterations: 269
currently lose_sum: 0.18547211587429047
time_elpased: 0.074
#iterations: 270
currently lose_sum: 0.5096099376678467
time_elpased: 0.072
#iterations: 271
currently lose_sum: 0.3854580521583557
time_elpased: 0.072
#iterations: 272
currently lose_sum: 0.9184203743934631
time_elpased: 0.072
#iterations: 273
currently lose_sum: 0.43039533495903015
time_elpased: 0.079
#iterations: 274
currently lose_sum: 0.41645509004592896
time_elpased: 0.073
#iterations: 275
currently lose_sum: 0.2220052033662796
time_elpased: 0.076
#iterations: 276
currently lose_sum: 0.21762166917324066
time_elpased: 0.072
#iterations: 277
currently lose_sum: 0.34565991163253784
time_elpased: 0.074
#iterations: 278
currently lose_sum: 0.5197766423225403
time_elpased: 0.073
#iterations: 279
currently lose_sum: 0.21196506917476654
time_elpased: 0.073
start validation test
0.575757575758
0.775510204082
0.22754491018
0.351851851852
0.580030123801
12.331
#iterations: 280
currently lose_sum: 0.38915446400642395
time_elpased: 0.075
#iterations: 281
currently lose_sum: 0.11701355874538422
time_elpased: 0.073
#iterations: 282
currently lose_sum: 0.3732096552848816
time_elpased: 0.08
#iterations: 283
currently lose_sum: 0.1261596828699112
time_elpased: 0.072
#iterations: 284
currently lose_sum: 0.22056251764297485
time_elpased: 0.108
#iterations: 285
currently lose_sum: 0.17612110078334808
time_elpased: 0.081
#iterations: 286
currently lose_sum: 0.14412742853164673
time_elpased: 0.075
#iterations: 287
currently lose_sum: 0.2384067177772522
time_elpased: 0.072
#iterations: 288
currently lose_sum: 0.4400470554828644
time_elpased: 0.089
#iterations: 289
currently lose_sum: 0.2398345023393631
time_elpased: 0.074
#iterations: 290
currently lose_sum: 0.14222398400306702
time_elpased: 0.073
#iterations: 291
currently lose_sum: 0.23565785586833954
time_elpased: 0.073
#iterations: 292
currently lose_sum: 0.28353404998779297
time_elpased: 0.082
#iterations: 293
currently lose_sum: 0.30378803610801697
time_elpased: 0.076
#iterations: 294
currently lose_sum: 0.18101602792739868
time_elpased: 0.072
#iterations: 295
currently lose_sum: 0.3064490854740143
time_elpased: 0.073
#iterations: 296
currently lose_sum: 0.18476471304893494
time_elpased: 0.072
#iterations: 297
currently lose_sum: 0.30882537364959717
time_elpased: 0.072
#iterations: 298
currently lose_sum: 0.2071875035762787
time_elpased: 0.072
#iterations: 299
currently lose_sum: 0.11444061249494553
time_elpased: 0.072
start validation test
0.715151515152
0.792
0.592814371257
0.678082191781
0.716652584402
8.904
#iterations: 300
currently lose_sum: 0.13790078461170197
time_elpased: 0.073
#iterations: 301
currently lose_sum: 0.6492702960968018
time_elpased: 0.071
#iterations: 302
currently lose_sum: 0.13722997903823853
time_elpased: 0.073
#iterations: 303
currently lose_sum: 0.18990673124790192
time_elpased: 0.072
#iterations: 304
currently lose_sum: 0.1715312898159027
time_elpased: 0.076
#iterations: 305
currently lose_sum: 0.3517656922340393
time_elpased: 0.081
#iterations: 306
currently lose_sum: 0.1570296287536621
time_elpased: 0.073
#iterations: 307
currently lose_sum: 0.23083308339118958
time_elpased: 0.074
#iterations: 308
currently lose_sum: 0.2738531231880188
time_elpased: 0.073
#iterations: 309
currently lose_sum: 0.19988703727722168
time_elpased: 0.073
#iterations: 310
currently lose_sum: 0.15544775128364563
time_elpased: 0.077
#iterations: 311
currently lose_sum: 0.4979795217514038
time_elpased: 0.073
#iterations: 312
currently lose_sum: 0.2778276205062866
time_elpased: 0.074
#iterations: 313
currently lose_sum: 0.21963563561439514
time_elpased: 0.073
#iterations: 314
currently lose_sum: 0.1982090175151825
time_elpased: 0.073
#iterations: 315
currently lose_sum: 0.13866497576236725
time_elpased: 0.072
#iterations: 316
currently lose_sum: 0.2854706645011902
time_elpased: 0.073
#iterations: 317
currently lose_sum: 0.4055171608924866
time_elpased: 0.076
#iterations: 318
currently lose_sum: 0.3735663592815399
time_elpased: 0.072
#iterations: 319
currently lose_sum: 0.4348592758178711
time_elpased: 0.072
start validation test
0.675757575758
0.638888888889
0.826347305389
0.720626631854
0.673909849014
7.506
#iterations: 320
currently lose_sum: 0.2389550656080246
time_elpased: 0.073
#iterations: 321
currently lose_sum: 0.23543661832809448
time_elpased: 0.072
#iterations: 322
currently lose_sum: 0.739877462387085
time_elpased: 0.088
#iterations: 323
currently lose_sum: 0.34775876998901367
time_elpased: 0.072
#iterations: 324
currently lose_sum: 0.2325713187456131
time_elpased: 0.072
#iterations: 325
currently lose_sum: 0.18497058749198914
time_elpased: 0.074
#iterations: 326
currently lose_sum: 0.41496744751930237
time_elpased: 0.072
#iterations: 327
currently lose_sum: 0.21355517208576202
time_elpased: 0.073
#iterations: 328
currently lose_sum: 0.16987527906894684
time_elpased: 0.073
#iterations: 329
currently lose_sum: 0.13549843430519104
time_elpased: 0.08
#iterations: 330
currently lose_sum: 0.1849987953901291
time_elpased: 0.072
#iterations: 331
currently lose_sum: 0.7434427738189697
time_elpased: 0.074
#iterations: 332
currently lose_sum: 0.38317087292671204
time_elpased: 0.075
#iterations: 333
currently lose_sum: 0.32645952701568604
time_elpased: 0.072
#iterations: 334
currently lose_sum: 0.5628636479377747
time_elpased: 0.072
#iterations: 335
currently lose_sum: 0.13146398961544037
time_elpased: 0.087
#iterations: 336
currently lose_sum: 0.08477966487407684
time_elpased: 0.079
#iterations: 337
currently lose_sum: 0.15164309740066528
time_elpased: 0.073
#iterations: 338
currently lose_sum: 0.2928336560726166
time_elpased: 0.072
#iterations: 339
currently lose_sum: 0.10830426961183548
time_elpased: 0.072
start validation test
0.551515151515
0.88
0.131736526946
0.229166666667
0.556665809485
15.632
#iterations: 340
currently lose_sum: 0.16076086461544037
time_elpased: 0.072
#iterations: 341
currently lose_sum: 0.22916480898857117
time_elpased: 0.072
#iterations: 342
currently lose_sum: 0.20427556335926056
time_elpased: 0.073
#iterations: 343
currently lose_sum: 0.196165069937706
time_elpased: 0.072
#iterations: 344
currently lose_sum: 0.18673083186149597
time_elpased: 0.081
#iterations: 345
currently lose_sum: 0.13903994858264923
time_elpased: 0.073
#iterations: 346
currently lose_sum: 0.11621221899986267
time_elpased: 0.072
#iterations: 347
currently lose_sum: 0.3568153977394104
time_elpased: 0.081
#iterations: 348
currently lose_sum: 0.16288384795188904
time_elpased: 0.083
#iterations: 349
currently lose_sum: 0.35741227865219116
time_elpased: 0.076
#iterations: 350
currently lose_sum: 0.2299754023551941
time_elpased: 0.087
#iterations: 351
currently lose_sum: 0.24215632677078247
time_elpased: 0.091
#iterations: 352
currently lose_sum: 0.25529879331588745
time_elpased: 0.077
#iterations: 353
currently lose_sum: 0.179837167263031
time_elpased: 0.077
#iterations: 354
currently lose_sum: 0.22919198870658875
time_elpased: 0.076
#iterations: 355
currently lose_sum: 0.19464601576328278
time_elpased: 0.087
#iterations: 356
currently lose_sum: 0.2542700469493866
time_elpased: 0.075
#iterations: 357
currently lose_sum: 0.1344294399023056
time_elpased: 0.078
#iterations: 358
currently lose_sum: 0.13568677008152008
time_elpased: 0.073
#iterations: 359
currently lose_sum: 0.3809589743614197
time_elpased: 0.074
start validation test
0.551515151515
0.827586206897
0.14371257485
0.244897959184
0.556518864112
16.123
#iterations: 360
currently lose_sum: 0.2957382798194885
time_elpased: 0.085
#iterations: 361
currently lose_sum: 0.35181620717048645
time_elpased: 0.073
#iterations: 362
currently lose_sum: 0.140390083193779
time_elpased: 0.077
#iterations: 363
currently lose_sum: 0.21302813291549683
time_elpased: 0.077
#iterations: 364
currently lose_sum: 0.27293142676353455
time_elpased: 0.073
#iterations: 365
currently lose_sum: 0.19325311481952667
time_elpased: 0.072
#iterations: 366
currently lose_sum: 0.3240795433521271
time_elpased: 0.073
#iterations: 367
currently lose_sum: 0.4205896258354187
time_elpased: 0.073
#iterations: 368
currently lose_sum: 0.20934216678142548
time_elpased: 0.072
#iterations: 369
currently lose_sum: 0.0988626480102539
time_elpased: 0.073
#iterations: 370
currently lose_sum: 0.24118898808956146
time_elpased: 0.073
#iterations: 371
currently lose_sum: 0.2850554287433624
time_elpased: 0.073
#iterations: 372
currently lose_sum: 0.2773131728172302
time_elpased: 0.073
#iterations: 373
currently lose_sum: 0.13236567378044128
time_elpased: 0.071
#iterations: 374
currently lose_sum: 0.07925114780664444
time_elpased: 0.08
#iterations: 375
currently lose_sum: 0.21815253794193268
time_elpased: 0.08
#iterations: 376
currently lose_sum: 0.08982148766517639
time_elpased: 0.077
#iterations: 377
currently lose_sum: 0.3538622558116913
time_elpased: 0.073
#iterations: 378
currently lose_sum: 0.3710721433162689
time_elpased: 0.073
#iterations: 379
currently lose_sum: 0.46553078293800354
time_elpased: 0.072
start validation test
0.690909090909
0.715231788079
0.646706586826
0.679245283019
0.691451452922
8.130
#iterations: 380
currently lose_sum: 0.33455270528793335
time_elpased: 0.073
#iterations: 381
currently lose_sum: 0.14105772972106934
time_elpased: 0.072
#iterations: 382
currently lose_sum: 0.1683400720357895
time_elpased: 0.074
#iterations: 383
currently lose_sum: 0.15795429050922394
time_elpased: 0.072
#iterations: 384
currently lose_sum: 0.13376392424106598
time_elpased: 0.073
#iterations: 385
currently lose_sum: 0.5060983896255493
time_elpased: 0.072
#iterations: 386
currently lose_sum: 0.2250966876745224
time_elpased: 0.078
#iterations: 387
currently lose_sum: 0.3276691436767578
time_elpased: 0.072
#iterations: 388
currently lose_sum: 0.41655242443084717
time_elpased: 0.072
#iterations: 389
currently lose_sum: 0.25862613320350647
time_elpased: 0.077
#iterations: 390
currently lose_sum: 0.2897668182849884
time_elpased: 0.073
#iterations: 391
currently lose_sum: 0.245499387383461
time_elpased: 0.088
#iterations: 392
currently lose_sum: 0.15143615007400513
time_elpased: 0.072
#iterations: 393
currently lose_sum: 0.22895002365112305
time_elpased: 0.072
#iterations: 394
currently lose_sum: 0.1372712403535843
time_elpased: 0.073
#iterations: 395
currently lose_sum: 0.10877121239900589
time_elpased: 0.072
#iterations: 396
currently lose_sum: 0.3520597517490387
time_elpased: 0.072
#iterations: 397
currently lose_sum: 0.3240271210670471
time_elpased: 0.072
#iterations: 398
currently lose_sum: 0.2751045227050781
time_elpased: 0.074
#iterations: 399
currently lose_sum: 0.20420178771018982
time_elpased: 0.072
start validation test
0.50303030303
1.0
0.0179640718563
0.0352941176471
0.508982035928
27.242
#iterations: 400
currently lose_sum: 0.35391122102737427
time_elpased: 0.073
#iterations: 401
currently lose_sum: 0.5042408108711243
time_elpased: 0.072
#iterations: 402
currently lose_sum: 0.22763749957084656
time_elpased: 0.072
#iterations: 403
currently lose_sum: 0.1292857974767685
time_elpased: 0.072
#iterations: 404
currently lose_sum: 0.19692476093769073
time_elpased: 0.073
#iterations: 405
currently lose_sum: 0.07461263239383698
time_elpased: 0.072
#iterations: 406
currently lose_sum: 0.10550471395254135
time_elpased: 0.072
#iterations: 407
currently lose_sum: 0.24891036748886108
time_elpased: 0.072
#iterations: 408
currently lose_sum: 0.2885505259037018
time_elpased: 0.086
#iterations: 409
currently lose_sum: 0.18602164089679718
time_elpased: 0.073
#iterations: 410
currently lose_sum: 0.20418347418308258
time_elpased: 0.073
#iterations: 411
currently lose_sum: 0.15505020320415497
time_elpased: 0.073
#iterations: 412
currently lose_sum: 0.18806569278240204
time_elpased: 0.074
#iterations: 413
currently lose_sum: 0.09725973755121231
time_elpased: 0.073
#iterations: 414
currently lose_sum: 0.14476312696933746
time_elpased: 0.073
#iterations: 415
currently lose_sum: 0.7624296545982361
time_elpased: 0.077
#iterations: 416
currently lose_sum: 0.41588276624679565
time_elpased: 0.073
#iterations: 417
currently lose_sum: 0.24098534882068634
time_elpased: 0.073
#iterations: 418
currently lose_sum: 0.17495602369308472
time_elpased: 0.074
#iterations: 419
currently lose_sum: 0.15443873405456543
time_elpased: 0.073
start validation test
0.709090909091
0.678391959799
0.808383233533
0.737704918033
0.707872598362
8.051
#iterations: 420
currently lose_sum: 0.48058250546455383
time_elpased: 0.073
#iterations: 421
currently lose_sum: 0.20432227849960327
time_elpased: 0.073
#iterations: 422
currently lose_sum: 0.13275672495365143
time_elpased: 0.071
#iterations: 423
currently lose_sum: 0.28146684169769287
time_elpased: 0.074
#iterations: 424
currently lose_sum: 0.14103494584560394
time_elpased: 0.072
#iterations: 425
currently lose_sum: 0.5170771479606628
time_elpased: 0.082
#iterations: 426
currently lose_sum: 0.14541323482990265
time_elpased: 0.077
#iterations: 427
currently lose_sum: 0.16150729358196259
time_elpased: 0.072
#iterations: 428
currently lose_sum: 0.22512659430503845
time_elpased: 0.074
#iterations: 429
currently lose_sum: 0.05901128426194191
time_elpased: 0.072
#iterations: 430
currently lose_sum: 0.19966672360897064
time_elpased: 0.116
#iterations: 431
currently lose_sum: 0.12005674839019775
time_elpased: 0.073
#iterations: 432
currently lose_sum: 1.7398663759231567
time_elpased: 0.075
#iterations: 433
currently lose_sum: 0.32784122228622437
time_elpased: 0.08
#iterations: 434
currently lose_sum: 0.2752780020236969
time_elpased: 0.074
#iterations: 435
currently lose_sum: 0.155226469039917
time_elpased: 0.08
#iterations: 436
currently lose_sum: 0.06263662874698639
time_elpased: 0.073
#iterations: 437
currently lose_sum: 0.1802860051393509
time_elpased: 0.072
#iterations: 438
currently lose_sum: 0.1301664412021637
time_elpased: 0.076
#iterations: 439
currently lose_sum: 0.2599119246006012
time_elpased: 0.078
start validation test
0.521212121212
1.0
0.0538922155689
0.102272727273
0.526946107784
20.674
#iterations: 440
currently lose_sum: 0.06669925153255463
time_elpased: 0.073
#iterations: 441
currently lose_sum: 0.133473739027977
time_elpased: 0.073
#iterations: 442
currently lose_sum: 0.17969118058681488
time_elpased: 0.073
#iterations: 443
currently lose_sum: 0.18771527707576752
time_elpased: 0.074
#iterations: 444
currently lose_sum: 0.18442867696285248
time_elpased: 0.073
#iterations: 445
currently lose_sum: 0.3169234097003937
time_elpased: 0.074
#iterations: 446
currently lose_sum: 0.5289883613586426
time_elpased: 0.073
#iterations: 447
currently lose_sum: 0.22409409284591675
time_elpased: 0.073
#iterations: 448
currently lose_sum: 0.41777652502059937
time_elpased: 0.073
#iterations: 449
currently lose_sum: 0.25560086965560913
time_elpased: 0.073
#iterations: 450
currently lose_sum: 0.1759534478187561
time_elpased: 0.073
#iterations: 451
currently lose_sum: 0.053906720131635666
time_elpased: 0.072
#iterations: 452
currently lose_sum: 0.46155908703804016
time_elpased: 0.079
#iterations: 453
currently lose_sum: 0.19024546444416046
time_elpased: 0.073
#iterations: 454
currently lose_sum: 0.3023681342601776
time_elpased: 0.074
#iterations: 455
currently lose_sum: 0.2994515299797058
time_elpased: 0.072
#iterations: 456
currently lose_sum: 0.09698545187711716
time_elpased: 0.074
#iterations: 457
currently lose_sum: 0.0894489586353302
time_elpased: 0.086
#iterations: 458
currently lose_sum: 0.11179254204034805
time_elpased: 0.072
#iterations: 459
currently lose_sum: 0.3475073575973511
time_elpased: 0.078
start validation test
0.551515151515
0.756756756757
0.167664670659
0.274509803922
0.556224973366
16.674
#iterations: 460
currently lose_sum: 0.47906920313835144
time_elpased: 0.098
#iterations: 461
currently lose_sum: 0.21597279608249664
time_elpased: 0.072
#iterations: 462
currently lose_sum: 0.1857146918773651
time_elpased: 0.076
#iterations: 463
currently lose_sum: 0.6273216605186462
time_elpased: 0.073
#iterations: 464
currently lose_sum: 0.17874015867710114
time_elpased: 0.08
#iterations: 465
currently lose_sum: 0.2046303153038025
time_elpased: 0.09
#iterations: 466
currently lose_sum: 0.2933386266231537
time_elpased: 0.073
#iterations: 467
currently lose_sum: 0.09447961300611496
time_elpased: 0.073
#iterations: 468
currently lose_sum: 0.15598367154598236
time_elpased: 0.075
#iterations: 469
currently lose_sum: 0.1051102727651596
time_elpased: 0.074
#iterations: 470
currently lose_sum: 0.17889562249183655
time_elpased: 0.072
#iterations: 471
currently lose_sum: 0.15048736333847046
time_elpased: 0.072
#iterations: 472
currently lose_sum: 0.23910212516784668
time_elpased: 0.072
#iterations: 473
currently lose_sum: nan
time_elpased: 0.08
train finish final lose is: nan
acc: 0.682
pre: 0.752
rec: 0.592
F1: 0.662
auc: 0.687
