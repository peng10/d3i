#!/bin/tcsh
foreach folds (0 1 2 3 4)
	set num_pred = `less ./$1\_$2\_prediction_value/hypothesis0_$1\_sim_mat.txt_PNN_test$folds\_$1.txt_32_32_1_0 | wc | awk '{print $1}'`
	echo $num_pred
	less ../PNN_test$folds\_$1.txt | awk -v "num=$num_pred" '{if(NR<=num) {print $NF} }' > ground$folds
	foreach layer (0 1 3 5)
		foreach dim (32 64 128 256 512)
			python evaluation.py --ground_truth ground$folds --prediction_value ./$1\_$2\_prediction_value/hypothesis0_$1\_sim_mat.txt_PNN_test$folds\_$1.txt_32_$dim\_1_$layer --out_path true.txt
			set true_auc = `less true.txt | awk '{printf ("%.3f\n", $1)}'`
			less $1\_$2/log_$1\_sim_mat.txt_1N_attention_0_L_32_feature_$dim\_$layer\_$folds.txt | awk -v "true_auc=$true_auc" '{if(NR == 2019) {$2=true_auc}; print $0}' > $1\_$2/test_$dim\_$layer\_$folds.txt
			mv $1\_$2/test_$dim\_$layer\_$folds.txt $1\_$2/log_$1\_sim_mat.txt_1N_attention_0_L_32_feature_$dim\_$layer\_$folds.txt
		end
	end
end
