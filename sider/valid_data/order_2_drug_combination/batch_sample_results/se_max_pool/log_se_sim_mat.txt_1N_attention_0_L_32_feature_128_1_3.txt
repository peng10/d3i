start to construct computing graph
graph construct over
1132
epochs start
#iterations: 0
currently lose_sum: 0.6739839315414429
time_elpased: 0.797
#iterations: 1
currently lose_sum: 0.7093843817710876
time_elpased: 0.696
#iterations: 2
currently lose_sum: 0.6337307691574097
time_elpased: 0.72
#iterations: 3
currently lose_sum: 0.6449256539344788
time_elpased: 0.704
#iterations: 4
currently lose_sum: 0.611801028251648
time_elpased: 0.685
#iterations: 5
currently lose_sum: 0.5717241168022156
time_elpased: 0.71
#iterations: 6
currently lose_sum: 0.6667179465293884
time_elpased: 0.655
#iterations: 7
currently lose_sum: 0.6490750908851624
time_elpased: 0.69
#iterations: 8
currently lose_sum: 0.7020228505134583
time_elpased: 0.681
#iterations: 9
currently lose_sum: 0.6120885014533997
time_elpased: 0.705
#iterations: 10
currently lose_sum: 0.5324221253395081
time_elpased: 0.542
#iterations: 11
currently lose_sum: 0.6247438788414001
time_elpased: 0.581
#iterations: 12
currently lose_sum: 0.6535614132881165
time_elpased: 0.627
#iterations: 13
currently lose_sum: 0.6554698348045349
time_elpased: 0.707
#iterations: 14
currently lose_sum: 0.7822251915931702
time_elpased: 0.674
#iterations: 15
currently lose_sum: 0.5770137906074524
time_elpased: 0.656
#iterations: 16
currently lose_sum: 0.5334200263023376
time_elpased: 0.716
#iterations: 17
currently lose_sum: 0.5840475559234619
time_elpased: 0.625
#iterations: 18
currently lose_sum: 0.4925784468650818
time_elpased: 0.661
#iterations: 19
currently lose_sum: 0.575309157371521
time_elpased: 0.513
start validation test
0.666666666667
0.701257861635
0.60597826087
0.650145772595
0.668045948617
14.127
#iterations: 20
currently lose_sum: 0.5796676874160767
time_elpased: 0.656
#iterations: 21
currently lose_sum: 0.6727622151374817
time_elpased: 0.676
#iterations: 22
currently lose_sum: 0.5547491908073425
time_elpased: 0.687
#iterations: 23
currently lose_sum: 0.690146803855896
time_elpased: 0.669
#iterations: 24
currently lose_sum: 0.5882883667945862
time_elpased: 0.668
#iterations: 25
currently lose_sum: 0.47300657629966736
time_elpased: 0.676
#iterations: 26
currently lose_sum: 0.49232637882232666
time_elpased: 0.544
#iterations: 27
currently lose_sum: 0.6087948083877563
time_elpased: 0.68
#iterations: 28
currently lose_sum: 0.5326479077339172
time_elpased: 0.667
#iterations: 29
currently lose_sum: 0.6060908436775208
time_elpased: 0.669
#iterations: 30
currently lose_sum: 0.5494327545166016
time_elpased: 0.657
#iterations: 31
currently lose_sum: 0.5399943590164185
time_elpased: 0.719
#iterations: 32
currently lose_sum: 0.5706580877304077
time_elpased: 0.704
#iterations: 33
currently lose_sum: 0.5324325561523438
time_elpased: 0.645
#iterations: 34
currently lose_sum: 0.41604799032211304
time_elpased: 0.678
#iterations: 35
currently lose_sum: 0.6624689698219299
time_elpased: 0.705
#iterations: 36
currently lose_sum: 0.5057356357574463
time_elpased: 0.715
#iterations: 37
currently lose_sum: 0.6365236043930054
time_elpased: 0.765
#iterations: 38
currently lose_sum: 0.4644606411457062
time_elpased: 0.677
#iterations: 39
currently lose_sum: 0.5309852957725525
time_elpased: 0.674
start validation test
0.708333333333
0.723163841808
0.695652173913
0.709141274238
0.708621541502
13.430
#iterations: 40
currently lose_sum: 0.5914768576622009
time_elpased: 0.712
#iterations: 41
currently lose_sum: 0.629253089427948
time_elpased: 0.708
#iterations: 42
currently lose_sum: 0.5343368649482727
time_elpased: 0.699
#iterations: 43
currently lose_sum: 0.49262216687202454
time_elpased: 0.664
#iterations: 44
currently lose_sum: 0.5306629538536072
time_elpased: 0.723
#iterations: 45
currently lose_sum: 0.5399197936058044
time_elpased: 0.683
#iterations: 46
currently lose_sum: 0.5640386939048767
time_elpased: 0.59
#iterations: 47
currently lose_sum: 0.6582626104354858
time_elpased: 0.696
#iterations: 48
currently lose_sum: 0.446293443441391
time_elpased: 0.715
#iterations: 49
currently lose_sum: 0.36093246936798096
time_elpased: 0.671
#iterations: 50
currently lose_sum: 0.7414312958717346
time_elpased: 0.664
#iterations: 51
currently lose_sum: 0.5774301290512085
time_elpased: 0.66
#iterations: 52
currently lose_sum: 0.6106414794921875
time_elpased: 0.703
#iterations: 53
currently lose_sum: 0.6071945428848267
time_elpased: 0.674
#iterations: 54
currently lose_sum: 0.45782214403152466
time_elpased: 0.69
#iterations: 55
currently lose_sum: 0.48713430762290955
time_elpased: 0.695
#iterations: 56
currently lose_sum: 0.46871843934059143
time_elpased: 0.712
#iterations: 57
currently lose_sum: 0.4222525656223297
time_elpased: 0.681
#iterations: 58
currently lose_sum: 0.5904437303543091
time_elpased: 0.633
#iterations: 59
currently lose_sum: 0.3496312201023102
time_elpased: 0.592
start validation test
0.736111111111
0.711904761905
0.8125
0.758883248731
0.734375
12.883
#iterations: 60
currently lose_sum: 0.5035296678543091
time_elpased: 0.708
#iterations: 61
currently lose_sum: 0.4426518976688385
time_elpased: 0.59
#iterations: 62
currently lose_sum: 0.40737536549568176
time_elpased: 0.538
#iterations: 63
currently lose_sum: 0.47199746966362
time_elpased: 0.694
#iterations: 64
currently lose_sum: 0.548776388168335
time_elpased: 0.698
#iterations: 65
currently lose_sum: 0.47066599130630493
time_elpased: 0.687
#iterations: 66
currently lose_sum: 0.31748542189598083
time_elpased: 0.687
#iterations: 67
currently lose_sum: 0.5736714005470276
time_elpased: 0.607
#iterations: 68
currently lose_sum: 0.752634584903717
time_elpased: 0.579
#iterations: 69
currently lose_sum: 0.4740123152732849
time_elpased: 0.603
#iterations: 70
currently lose_sum: 0.3773060142993927
time_elpased: 0.593
#iterations: 71
currently lose_sum: 0.538794219493866
time_elpased: 0.665
#iterations: 72
currently lose_sum: 0.4012760818004608
time_elpased: 0.705
#iterations: 73
currently lose_sum: 0.4698687493801117
time_elpased: 0.717
#iterations: 74
currently lose_sum: 0.47302931547164917
time_elpased: 0.672
#iterations: 75
currently lose_sum: 0.4944351017475128
time_elpased: 0.662
#iterations: 76
currently lose_sum: 0.43415367603302
time_elpased: 0.712
#iterations: 77
currently lose_sum: 0.3587090075016022
time_elpased: 0.698
#iterations: 78
currently lose_sum: 0.6090680360794067
time_elpased: 0.66
#iterations: 79
currently lose_sum: 0.36081185936927795
time_elpased: 0.711
start validation test
0.736111111111
0.725888324873
0.777173913043
0.750656167979
0.735177865613
13.049
#iterations: 80
currently lose_sum: 0.5343323349952698
time_elpased: 0.668
#iterations: 81
currently lose_sum: 0.3545520007610321
time_elpased: 0.672
#iterations: 82
currently lose_sum: 0.558472752571106
time_elpased: 0.655
#iterations: 83
currently lose_sum: 0.48315146565437317
time_elpased: 0.638
#iterations: 84
currently lose_sum: 0.8115069270133972
time_elpased: 0.644
#iterations: 85
currently lose_sum: 0.48366209864616394
time_elpased: 0.63
#iterations: 86
currently lose_sum: 0.39017289876937866
time_elpased: 0.681
#iterations: 87
currently lose_sum: 0.446008563041687
time_elpased: 0.68
#iterations: 88
currently lose_sum: 0.3829701244831085
time_elpased: 0.658
#iterations: 89
currently lose_sum: 0.69798743724823
time_elpased: 0.581
#iterations: 90
currently lose_sum: 0.5842698812484741
time_elpased: 0.601
#iterations: 91
currently lose_sum: 0.42272746562957764
time_elpased: 0.696
#iterations: 92
currently lose_sum: 0.5019875168800354
time_elpased: 0.637
#iterations: 93
currently lose_sum: 0.4526974856853485
time_elpased: 0.713
#iterations: 94
currently lose_sum: 0.5274946093559265
time_elpased: 0.694
#iterations: 95
currently lose_sum: 0.5852086544036865
time_elpased: 0.706
#iterations: 96
currently lose_sum: 0.4344741404056549
time_elpased: 0.685
#iterations: 97
currently lose_sum: 0.5685823559761047
time_elpased: 0.711
#iterations: 98
currently lose_sum: 0.7114941477775574
time_elpased: 0.674
#iterations: 99
currently lose_sum: 0.601565420627594
time_elpased: 0.651
start validation test
0.652777777778
0.831460674157
0.402173913043
0.542124542125
0.658473320158
15.222
#iterations: 100
currently lose_sum: 0.49668392539024353
time_elpased: 0.716
#iterations: 101
currently lose_sum: 0.5456197261810303
time_elpased: 0.687
#iterations: 102
currently lose_sum: 0.5082764029502869
time_elpased: 0.663
#iterations: 103
currently lose_sum: 0.5888203382492065
time_elpased: 0.705
#iterations: 104
currently lose_sum: 0.3230680227279663
time_elpased: 0.651
#iterations: 105
currently lose_sum: 0.48230451345443726
time_elpased: 0.671
#iterations: 106
currently lose_sum: 0.4238208532333374
time_elpased: 0.723
#iterations: 107
currently lose_sum: 0.5536664724349976
time_elpased: 0.663
#iterations: 108
currently lose_sum: 0.4128533601760864
time_elpased: 0.726
#iterations: 109
currently lose_sum: 0.6258409023284912
time_elpased: 0.66
#iterations: 110
currently lose_sum: 0.48750972747802734
time_elpased: 0.685
#iterations: 111
currently lose_sum: 0.37840768694877625
time_elpased: 0.683
#iterations: 112
currently lose_sum: 0.48935168981552124
time_elpased: 0.704
#iterations: 113
currently lose_sum: 0.47030773758888245
time_elpased: 0.658
#iterations: 114
currently lose_sum: 0.5187275409698486
time_elpased: 0.647
#iterations: 115
currently lose_sum: 0.5736324787139893
time_elpased: 0.728
#iterations: 116
currently lose_sum: 0.37773433327674866
time_elpased: 0.617
#iterations: 117
currently lose_sum: 0.6492721438407898
time_elpased: 0.708
#iterations: 118
currently lose_sum: 0.3560031056404114
time_elpased: 0.721
#iterations: 119
currently lose_sum: 0.5353195667266846
time_elpased: 0.716
start validation test
0.684722222222
0.816143497758
0.494565217391
0.615905245347
0.689043972332
14.880
#iterations: 120
currently lose_sum: 0.33641937375068665
time_elpased: 0.686
#iterations: 121
currently lose_sum: 0.6022428870201111
time_elpased: 0.656
#iterations: 122
currently lose_sum: 0.391055166721344
time_elpased: 0.747
#iterations: 123
currently lose_sum: 0.48837921023368835
time_elpased: 0.651
#iterations: 124
currently lose_sum: 0.5629384517669678
time_elpased: 0.69
#iterations: 125
currently lose_sum: 0.4394296109676361
time_elpased: 0.67
#iterations: 126
currently lose_sum: 0.36555248498916626
time_elpased: 0.685
#iterations: 127
currently lose_sum: 0.39589938521385193
time_elpased: 0.64
#iterations: 128
currently lose_sum: 0.4923851191997528
time_elpased: 0.689
#iterations: 129
currently lose_sum: 0.47076255083084106
time_elpased: 0.651
#iterations: 130
currently lose_sum: 0.3680635690689087
time_elpased: 0.635
#iterations: 131
currently lose_sum: 0.4378677010536194
time_elpased: 0.594
#iterations: 132
currently lose_sum: 0.8770039677619934
time_elpased: 0.528
#iterations: 133
currently lose_sum: 0.457379013299942
time_elpased: 0.506
#iterations: 134
currently lose_sum: 0.4524003863334656
time_elpased: 0.774
#iterations: 135
currently lose_sum: 0.33426588773727417
time_elpased: 0.676
#iterations: 136
currently lose_sum: 0.3844362497329712
time_elpased: 0.656
#iterations: 137
currently lose_sum: 0.3563752770423889
time_elpased: 0.737
#iterations: 138
currently lose_sum: 0.5820459127426147
time_elpased: 0.68
#iterations: 139
currently lose_sum: 0.6651210188865662
time_elpased: 0.703
start validation test
0.7
0.801587301587
0.548913043478
0.651612903226
0.703433794466
14.654
#iterations: 140
currently lose_sum: 0.38029131293296814
time_elpased: 0.652
#iterations: 141
currently lose_sum: 0.2985988259315491
time_elpased: 0.648
#iterations: 142
currently lose_sum: 0.483128160238266
time_elpased: 0.603
#iterations: 143
currently lose_sum: 0.49119099974632263
time_elpased: 0.501
#iterations: 144
currently lose_sum: 0.5388759970664978
time_elpased: 0.611
#iterations: 145
currently lose_sum: 0.3665688633918762
time_elpased: 0.648
#iterations: 146
currently lose_sum: 0.5144751667976379
time_elpased: 0.673
#iterations: 147
currently lose_sum: 0.3612877130508423
time_elpased: 0.619
#iterations: 148
currently lose_sum: 0.5094892382621765
time_elpased: 0.696
#iterations: 149
currently lose_sum: 0.2782556712627411
time_elpased: 0.721
#iterations: 150
currently lose_sum: 0.3483510911464691
time_elpased: 0.571
#iterations: 151
currently lose_sum: 0.3515044152736664
time_elpased: 0.682
#iterations: 152
currently lose_sum: 0.321805477142334
time_elpased: 0.68
#iterations: 153
currently lose_sum: 0.30166053771972656
time_elpased: 0.649
#iterations: 154
currently lose_sum: 0.3807230293750763
time_elpased: 0.639
#iterations: 155
currently lose_sum: 0.3777608871459961
time_elpased: 0.53
#iterations: 156
currently lose_sum: 0.3255111277103424
time_elpased: 0.502
#iterations: 157
currently lose_sum: 0.2780143618583679
time_elpased: 0.587
#iterations: 158
currently lose_sum: 0.4966447949409485
time_elpased: 0.619
#iterations: 159
currently lose_sum: 0.310543030500412
time_elpased: 0.677
start validation test
0.663888888889
0.824742268041
0.434782608696
0.569395017794
0.669095849802
16.673
#iterations: 160
currently lose_sum: 0.3368469476699829
time_elpased: 0.676
#iterations: 161
currently lose_sum: 0.49168267846107483
time_elpased: 0.682
#iterations: 162
currently lose_sum: 0.29617220163345337
time_elpased: 0.677
#iterations: 163
currently lose_sum: 0.4526562988758087
time_elpased: 0.675
#iterations: 164
currently lose_sum: 0.29194143414497375
time_elpased: 0.661
#iterations: 165
currently lose_sum: 0.373146653175354
time_elpased: 0.667
#iterations: 166
currently lose_sum: 0.3691466748714447
time_elpased: 0.668
#iterations: 167
currently lose_sum: 0.32427725195884705
time_elpased: 0.651
#iterations: 168
currently lose_sum: 0.3551144301891327
time_elpased: 0.608
#iterations: 169
currently lose_sum: 0.39490458369255066
time_elpased: 0.693
#iterations: 170
currently lose_sum: 0.2442227303981781
time_elpased: 0.715
#iterations: 171
currently lose_sum: 0.3391282260417938
time_elpased: 0.705
#iterations: 172
currently lose_sum: 0.3128022253513336
time_elpased: 0.626
#iterations: 173
currently lose_sum: 0.5031139850616455
time_elpased: 0.603
#iterations: 174
currently lose_sum: 0.41149812936782837
time_elpased: 0.677
#iterations: 175
currently lose_sum: 0.3633340001106262
time_elpased: 0.628
#iterations: 176
currently lose_sum: 0.2209486961364746
time_elpased: 0.632
#iterations: 177
currently lose_sum: 0.39423561096191406
time_elpased: 0.659
#iterations: 178
currently lose_sum: 0.9142906069755554
time_elpased: 0.673
#iterations: 179
currently lose_sum: 0.4429343640804291
time_elpased: 0.621
start validation test
0.686111111111
0.793388429752
0.521739130435
0.629508196721
0.689846837945
15.330
#iterations: 180
currently lose_sum: 0.6351972818374634
time_elpased: 0.692
#iterations: 181
currently lose_sum: 0.34271058440208435
time_elpased: 0.626
#iterations: 182
currently lose_sum: 0.3394733965396881
time_elpased: 0.625
#iterations: 183
currently lose_sum: 0.30933377146720886
time_elpased: 0.637
#iterations: 184
currently lose_sum: 0.5736442804336548
time_elpased: 0.599
#iterations: 185
currently lose_sum: 0.4155277609825134
time_elpased: 0.597
#iterations: 186
currently lose_sum: 0.3723903000354767
time_elpased: 0.698
#iterations: 187
currently lose_sum: 0.2948189079761505
time_elpased: 0.641
#iterations: 188
currently lose_sum: 0.3054252564907074
time_elpased: 0.637
#iterations: 189
currently lose_sum: 0.2835739254951477
time_elpased: 0.622
#iterations: 190
currently lose_sum: 0.3207346796989441
time_elpased: 0.704
#iterations: 191
currently lose_sum: 0.2978339195251465
time_elpased: 0.63
#iterations: 192
currently lose_sum: 0.5193113684654236
time_elpased: 0.64
#iterations: 193
currently lose_sum: 0.2977693974971771
time_elpased: 0.599
#iterations: 194
currently lose_sum: 0.3293883204460144
time_elpased: 0.643
#iterations: 195
currently lose_sum: 0.4816328287124634
time_elpased: 0.61
#iterations: 196
currently lose_sum: 0.4129258096218109
time_elpased: 0.578
#iterations: 197
currently lose_sum: 0.48340994119644165
time_elpased: 0.604
#iterations: 198
currently lose_sum: 0.3215235471725464
time_elpased: 0.686
#iterations: 199
currently lose_sum: 0.38048866391181946
time_elpased: 0.654
start validation test
0.586111111111
0.864583333333
0.225543478261
0.35775862069
0.59430583004
20.437
#iterations: 200
currently lose_sum: 0.39723867177963257
time_elpased: 0.664
#iterations: 201
currently lose_sum: 0.3235771059989929
time_elpased: 0.683
#iterations: 202
currently lose_sum: 0.3573401868343353
time_elpased: 0.646
#iterations: 203
currently lose_sum: 0.46568864583969116
time_elpased: 0.685
#iterations: 204
currently lose_sum: 0.5535780191421509
time_elpased: 0.671
#iterations: 205
currently lose_sum: 0.2968733012676239
time_elpased: 0.677
#iterations: 206
currently lose_sum: 0.18471641838550568
time_elpased: 0.608
#iterations: 207
currently lose_sum: 0.5002615451812744
time_elpased: 0.653
#iterations: 208
currently lose_sum: 0.2138894945383072
time_elpased: 0.713
#iterations: 209
currently lose_sum: 0.17610915005207062
time_elpased: 0.685
#iterations: 210
currently lose_sum: 0.31473615765571594
time_elpased: 0.651
#iterations: 211
currently lose_sum: 0.19797112047672272
time_elpased: 0.681
#iterations: 212
currently lose_sum: 0.3084109425544739
time_elpased: 0.601
#iterations: 213
currently lose_sum: 0.6102458238601685
time_elpased: 0.709
#iterations: 214
currently lose_sum: 0.2985745072364807
time_elpased: 0.664
#iterations: 215
currently lose_sum: 0.18686828017234802
time_elpased: 0.676
#iterations: 216
currently lose_sum: 0.6307313442230225
time_elpased: 0.614
#iterations: 217
currently lose_sum: 0.5994539856910706
time_elpased: 0.615
#iterations: 218
currently lose_sum: 0.3782733082771301
time_elpased: 0.711
#iterations: 219
currently lose_sum: 0.5599426627159119
time_elpased: 0.572
start validation test
0.7375
0.785942492013
0.66847826087
0.722466960352
0.739068675889
13.778
#iterations: 220
currently lose_sum: 0.43792688846588135
time_elpased: 0.638
#iterations: 221
currently lose_sum: 0.18498946726322174
time_elpased: 0.655
#iterations: 222
currently lose_sum: 0.4295152425765991
time_elpased: 0.656
#iterations: 223
currently lose_sum: 0.3911702334880829
time_elpased: 0.614
#iterations: 224
currently lose_sum: 0.4063321352005005
time_elpased: 0.66
#iterations: 225
currently lose_sum: 0.45108547806739807
time_elpased: 0.633
#iterations: 226
currently lose_sum: 0.43006688356399536
time_elpased: 0.716
#iterations: 227
currently lose_sum: 0.2768709361553192
time_elpased: 0.697
#iterations: 228
currently lose_sum: 0.18367642164230347
time_elpased: 0.656
#iterations: 229
currently lose_sum: 0.42603886127471924
time_elpased: 0.652
#iterations: 230
currently lose_sum: 0.18960852921009064
time_elpased: 0.639
#iterations: 231
currently lose_sum: 0.27309736609458923
time_elpased: 0.681
#iterations: 232
currently lose_sum: 0.34303680062294006
time_elpased: 0.659
#iterations: 233
currently lose_sum: 0.5542730689048767
time_elpased: 0.706
#iterations: 234
currently lose_sum: 0.3828854262828827
time_elpased: 0.641
#iterations: 235
currently lose_sum: 0.16816332936286926
time_elpased: 0.655
#iterations: 236
currently lose_sum: 0.4202633500099182
time_elpased: 0.591
#iterations: 237
currently lose_sum: 0.48572584986686707
time_elpased: 0.653
#iterations: 238
currently lose_sum: 0.34668511152267456
time_elpased: 0.626
#iterations: 239
currently lose_sum: 0.7292134165763855
time_elpased: 0.687
start validation test
0.734722222222
0.775700934579
0.676630434783
0.722786647315
0.736042490119
14.148
#iterations: 240
currently lose_sum: 0.2370372712612152
time_elpased: 0.613
#iterations: 241
currently lose_sum: 0.17801381647586823
time_elpased: 0.621
#iterations: 242
currently lose_sum: 0.3177873492240906
time_elpased: 0.675
#iterations: 243
currently lose_sum: 0.32272523641586304
time_elpased: 0.715
#iterations: 244
currently lose_sum: 0.29316091537475586
time_elpased: 0.63
#iterations: 245
currently lose_sum: 0.5354271531105042
time_elpased: 0.644
#iterations: 246
currently lose_sum: 0.23352952301502228
time_elpased: 0.648
#iterations: 247
currently lose_sum: 0.4319266676902771
time_elpased: 0.647
#iterations: 248
currently lose_sum: 0.30060499906539917
time_elpased: 0.631
#iterations: 249
currently lose_sum: 0.28431087732315063
time_elpased: 0.656
#iterations: 250
currently lose_sum: 0.3304329216480255
time_elpased: 0.642
#iterations: 251
currently lose_sum: 0.5099156498908997
time_elpased: 0.678
#iterations: 252
currently lose_sum: 0.43500223755836487
time_elpased: 0.631
#iterations: 253
currently lose_sum: 0.2583995461463928
time_elpased: 0.69
#iterations: 254
currently lose_sum: 0.3628625273704529
time_elpased: 0.711
#iterations: 255
currently lose_sum: 0.26947930455207825
time_elpased: 0.686
#iterations: 256
currently lose_sum: 0.40062734484672546
time_elpased: 0.67
#iterations: 257
currently lose_sum: 0.21960300207138062
time_elpased: 0.616
#iterations: 258
currently lose_sum: 0.39553168416023254
time_elpased: 0.692
#iterations: 259
currently lose_sum: 0.27606335282325745
time_elpased: 0.619
start validation test
0.734722222222
0.72987012987
0.763586956522
0.746347941567
0.734066205534
13.740
#iterations: 260
currently lose_sum: 0.23665830492973328
time_elpased: 0.66
#iterations: 261
currently lose_sum: 0.2697935700416565
time_elpased: 0.67
#iterations: 262
currently lose_sum: 0.33707159757614136
time_elpased: 0.682
#iterations: 263
currently lose_sum: 0.6943809986114502
time_elpased: 0.638
#iterations: 264
currently lose_sum: 0.3717115521430969
time_elpased: 0.645
#iterations: 265
currently lose_sum: 0.4382997453212738
time_elpased: 0.611
#iterations: 266
currently lose_sum: 0.3100578188896179
time_elpased: 0.679
#iterations: 267
currently lose_sum: 0.6378632187843323
time_elpased: 0.67
#iterations: 268
currently lose_sum: 0.2905469238758087
time_elpased: 0.643
#iterations: 269
currently lose_sum: 0.21674898266792297
time_elpased: 0.668
#iterations: 270
currently lose_sum: 0.2704499661922455
time_elpased: 0.632
#iterations: 271
currently lose_sum: 0.34921836853027344
time_elpased: 0.638
#iterations: 272
currently lose_sum: 0.3044300079345703
time_elpased: 0.609
#iterations: 273
currently lose_sum: 0.1832830309867859
time_elpased: 0.696
#iterations: 274
currently lose_sum: 0.26812049746513367
time_elpased: 0.603
#iterations: 275
currently lose_sum: 0.17078541219234467
time_elpased: 0.62
#iterations: 276
currently lose_sum: 0.27829834818840027
time_elpased: 0.619
#iterations: 277
currently lose_sum: 0.2653564512729645
time_elpased: 0.618
#iterations: 278
currently lose_sum: 0.32242926955223083
time_elpased: 0.721
#iterations: 279
currently lose_sum: 0.5348240733146667
time_elpased: 0.727
start validation test
0.748611111111
0.746701846966
0.76902173913
0.757697456493
0.748147233202
14.368
#iterations: 280
currently lose_sum: 0.2713857591152191
time_elpased: 0.597
#iterations: 281
currently lose_sum: 0.27908653020858765
time_elpased: 0.687
#iterations: 282
currently lose_sum: 0.3092542588710785
time_elpased: 0.636
#iterations: 283
currently lose_sum: 0.44537416100502014
time_elpased: 0.627
#iterations: 284
currently lose_sum: 0.2436661273241043
time_elpased: 0.661
#iterations: 285
currently lose_sum: 0.15906216204166412
time_elpased: 0.694
#iterations: 286
currently lose_sum: 0.16625072062015533
time_elpased: 0.658
#iterations: 287
currently lose_sum: 0.469165176153183
time_elpased: 0.569
#iterations: 288
currently lose_sum: 0.18783044815063477
time_elpased: 0.63
#iterations: 289
currently lose_sum: 0.5388125777244568
time_elpased: 0.653
#iterations: 290
currently lose_sum: 0.29891833662986755
time_elpased: 0.632
#iterations: 291
currently lose_sum: 0.4273953437805176
time_elpased: 0.674
#iterations: 292
currently lose_sum: 0.2099784016609192
time_elpased: 0.589
#iterations: 293
currently lose_sum: 0.2221115082502365
time_elpased: 0.625
#iterations: 294
currently lose_sum: 0.3384982645511627
time_elpased: 0.668
#iterations: 295
currently lose_sum: 0.17310258746147156
time_elpased: 0.676
#iterations: 296
currently lose_sum: 0.31593140959739685
time_elpased: 0.693
#iterations: 297
currently lose_sum: 0.3925963044166565
time_elpased: 0.668
#iterations: 298
currently lose_sum: 0.2446647435426712
time_elpased: 0.674
#iterations: 299
currently lose_sum: 0.3112674653530121
time_elpased: 0.663
start validation test
0.651388888889
0.826815642458
0.402173913043
0.54113345521
0.657052865613
21.019
#iterations: 300
currently lose_sum: 0.2511904835700989
time_elpased: 0.603
#iterations: 301
currently lose_sum: 0.15585194528102875
time_elpased: 0.616
#iterations: 302
currently lose_sum: 0.3819428086280823
time_elpased: 0.621
#iterations: 303
currently lose_sum: 0.2223096787929535
time_elpased: 0.638
#iterations: 304
currently lose_sum: 0.39075106382369995
time_elpased: 0.665
#iterations: 305
currently lose_sum: 0.33095118403434753
time_elpased: 0.643
#iterations: 306
currently lose_sum: 0.42464450001716614
time_elpased: 0.601
#iterations: 307
currently lose_sum: 0.3064455986022949
time_elpased: 0.625
#iterations: 308
currently lose_sum: 0.19440661370754242
time_elpased: 0.605
#iterations: 309
currently lose_sum: 0.4053547978401184
time_elpased: 0.639
#iterations: 310
currently lose_sum: 0.22348450124263763
time_elpased: 0.716
#iterations: 311
currently lose_sum: 0.2273995578289032
time_elpased: 0.65
#iterations: 312
currently lose_sum: 0.3307512700557709
time_elpased: 0.697
#iterations: 313
currently lose_sum: 0.3970726728439331
time_elpased: 0.676
#iterations: 314
currently lose_sum: 0.2475663721561432
time_elpased: 0.642
#iterations: 315
currently lose_sum: 0.21977946162223816
time_elpased: 0.654
#iterations: 316
currently lose_sum: 0.16743920743465424
time_elpased: 0.732
#iterations: 317
currently lose_sum: 0.466143935918808
time_elpased: 0.706
#iterations: 318
currently lose_sum: 0.1518152803182602
time_elpased: 0.584
#iterations: 319
currently lose_sum: 0.38342827558517456
time_elpased: 0.637
start validation test
0.643055555556
0.872483221477
0.353260869565
0.502901353965
0.649641798419
22.100
#iterations: 320
currently lose_sum: 0.3139572739601135
time_elpased: 0.641
#iterations: 321
currently lose_sum: 0.12646302580833435
time_elpased: 0.689
#iterations: 322
currently lose_sum: 0.4782136380672455
time_elpased: 0.619
#iterations: 323
currently lose_sum: 0.20598088204860687
time_elpased: 0.613
#iterations: 324
currently lose_sum: 0.13712568581104279
time_elpased: 0.62
#iterations: 325
currently lose_sum: 0.11978884786367416
time_elpased: 0.614
#iterations: 326
currently lose_sum: 0.24643200635910034
time_elpased: 0.609
#iterations: 327
currently lose_sum: 0.24908354878425598
time_elpased: 0.657
#iterations: 328
currently lose_sum: 0.4283476769924164
time_elpased: 0.616
#iterations: 329
currently lose_sum: 0.3523276448249817
time_elpased: 0.639
#iterations: 330
currently lose_sum: 0.2681538164615631
time_elpased: 0.599
#iterations: 331
currently lose_sum: 0.3190697133541107
time_elpased: 0.628
#iterations: 332
currently lose_sum: 0.34014707803726196
time_elpased: 0.606
#iterations: 333
currently lose_sum: 0.4829360246658325
time_elpased: 0.668
#iterations: 334
currently lose_sum: 0.4061651825904846
time_elpased: 0.625
#iterations: 335
currently lose_sum: 0.22586491703987122
time_elpased: 0.696
#iterations: 336
currently lose_sum: 0.16053207218647003
time_elpased: 0.663
#iterations: 337
currently lose_sum: 0.15836739540100098
time_elpased: 0.574
#iterations: 338
currently lose_sum: 0.1755499243736267
time_elpased: 0.541
#iterations: 339
currently lose_sum: 0.2675251364707947
time_elpased: 0.647
start validation test
0.725
0.779605263158
0.64402173913
0.705357142857
0.72684041502
15.825
#iterations: 340
currently lose_sum: 0.17210879921913147
time_elpased: 0.644
#iterations: 341
currently lose_sum: 0.37859708070755005
time_elpased: 0.642
#iterations: 342
currently lose_sum: 0.22844578325748444
time_elpased: 0.622
#iterations: 343
currently lose_sum: 0.22368304431438446
time_elpased: 0.678
#iterations: 344
currently lose_sum: 0.3464807868003845
time_elpased: 0.64
#iterations: 345
currently lose_sum: 0.1709691435098648
time_elpased: 0.629
#iterations: 346
currently lose_sum: 0.14676152169704437
time_elpased: 0.627
#iterations: 347
currently lose_sum: 0.07655828446149826
time_elpased: 0.64
#iterations: 348
currently lose_sum: 0.2989192306995392
time_elpased: 0.702
#iterations: 349
currently lose_sum: 0.3212229907512665
time_elpased: 0.55
#iterations: 350
currently lose_sum: 0.24643206596374512
time_elpased: 0.622
#iterations: 351
currently lose_sum: 0.3292439877986908
time_elpased: 0.646
#iterations: 352
currently lose_sum: 0.23396320641040802
time_elpased: 0.573
#iterations: 353
currently lose_sum: 0.3942670524120331
time_elpased: 0.662
#iterations: 354
currently lose_sum: 0.19412094354629517
time_elpased: 0.655
#iterations: 355
currently lose_sum: 0.6267871856689453
time_elpased: 0.689
#iterations: 356
currently lose_sum: 0.5911767482757568
time_elpased: 0.583
#iterations: 357
currently lose_sum: 0.16414520144462585
time_elpased: 0.628
#iterations: 358
currently lose_sum: 0.22865048050880432
time_elpased: 0.634
#iterations: 359
currently lose_sum: 0.561730682849884
time_elpased: 0.645
start validation test
0.573611111111
0.886075949367
0.190217391304
0.313199105145
0.582324604743
30.462
#iterations: 360
currently lose_sum: 0.17475849390029907
time_elpased: 0.719
#iterations: 361
currently lose_sum: 0.1752864569425583
time_elpased: 0.654
#iterations: 362
currently lose_sum: 0.38391703367233276
time_elpased: 0.637
#iterations: 363
currently lose_sum: 0.2363244742155075
time_elpased: 0.685
#iterations: 364
currently lose_sum: 0.18063688278198242
time_elpased: 0.651
#iterations: 365
currently lose_sum: 0.20915073156356812
time_elpased: 0.609
#iterations: 366
currently lose_sum: 0.2972983717918396
time_elpased: 0.651
#iterations: 367
currently lose_sum: 0.2135298252105713
time_elpased: 0.707
#iterations: 368
currently lose_sum: 0.2251126915216446
time_elpased: 0.651
#iterations: 369
currently lose_sum: 0.28721392154693604
time_elpased: 0.652
#iterations: 370
currently lose_sum: 0.2588014006614685
time_elpased: 0.61
#iterations: 371
currently lose_sum: 0.15499524772167206
time_elpased: 0.661
#iterations: 372
currently lose_sum: 0.31373313069343567
time_elpased: 0.699
#iterations: 373
currently lose_sum: 0.20300787687301636
time_elpased: 0.644
#iterations: 374
currently lose_sum: 0.2978959083557129
time_elpased: 0.671
#iterations: 375
currently lose_sum: 0.1681215763092041
time_elpased: 0.68
#iterations: 376
currently lose_sum: 0.11827976256608963
time_elpased: 0.724
#iterations: 377
currently lose_sum: 0.27488458156585693
time_elpased: 0.694
#iterations: 378
currently lose_sum: 0.14725586771965027
time_elpased: 0.715
#iterations: 379
currently lose_sum: 0.22959059476852417
time_elpased: 0.632
start validation test
0.731944444444
0.800687285223
0.633152173913
0.707132018209
0.73418972332
16.576
#iterations: 380
currently lose_sum: 0.286526620388031
time_elpased: 0.674
#iterations: 381
currently lose_sum: 0.3032515347003937
time_elpased: 0.663
#iterations: 382
currently lose_sum: 0.23371990025043488
time_elpased: 0.61
#iterations: 383
currently lose_sum: 0.2936166226863861
time_elpased: 0.591
#iterations: 384
currently lose_sum: 0.15901032090187073
time_elpased: 0.656
#iterations: 385
currently lose_sum: 0.3737887442111969
time_elpased: 0.636
#iterations: 386
currently lose_sum: 0.29616469144821167
time_elpased: 0.653
#iterations: 387
currently lose_sum: 0.38378408551216125
time_elpased: 0.654
#iterations: 388
currently lose_sum: 0.2268557846546173
time_elpased: 0.663
#iterations: 389
currently lose_sum: 0.1861366331577301
time_elpased: 0.585
#iterations: 390
currently lose_sum: 0.16266407072544098
time_elpased: 0.62
#iterations: 391
currently lose_sum: 0.20917317271232605
time_elpased: 0.55
#iterations: 392
currently lose_sum: 0.32925599813461304
time_elpased: 0.593
#iterations: 393
currently lose_sum: 0.1484004706144333
time_elpased: 0.535
#iterations: 394
currently lose_sum: 0.17996817827224731
time_elpased: 0.6
#iterations: 395
currently lose_sum: 0.20619472861289978
time_elpased: 0.637
#iterations: 396
currently lose_sum: 0.187266007065773
time_elpased: 0.668
#iterations: 397
currently lose_sum: 0.3446758985519409
time_elpased: 0.592
#iterations: 398
currently lose_sum: 0.16798771917819977
time_elpased: 0.671
#iterations: 399
currently lose_sum: 0.4261934459209442
time_elpased: 0.506
start validation test
0.705555555556
0.788888888889
0.578804347826
0.667711598746
0.708436264822
18.665
#iterations: 400
currently lose_sum: 0.33229562640190125
time_elpased: 0.563
#iterations: 401
currently lose_sum: 0.16283877193927765
time_elpased: 0.599
#iterations: 402
currently lose_sum: 0.30331966280937195
time_elpased: 0.61
#iterations: 403
currently lose_sum: 0.29831838607788086
time_elpased: 0.58
#iterations: 404
currently lose_sum: 0.24687668681144714
time_elpased: 0.558
#iterations: 405
currently lose_sum: 0.4008411169052124
time_elpased: 0.521
#iterations: 406
currently lose_sum: 0.251645028591156
time_elpased: 0.602
#iterations: 407
currently lose_sum: 0.4857547879219055
time_elpased: 0.644
#iterations: 408
currently lose_sum: 0.1493111252784729
time_elpased: 0.578
#iterations: 409
currently lose_sum: 0.11246024072170258
time_elpased: 0.568
#iterations: 410
currently lose_sum: 0.1522105634212494
time_elpased: 0.602
#iterations: 411
currently lose_sum: 0.21018847823143005
time_elpased: 0.605
#iterations: 412
currently lose_sum: 0.09919656068086624
time_elpased: 0.656
#iterations: 413
currently lose_sum: 0.24466535449028015
time_elpased: 0.688
#iterations: 414
currently lose_sum: 0.11352866142988205
time_elpased: 0.718
#iterations: 415
currently lose_sum: 0.2948596179485321
time_elpased: 0.708
#iterations: 416
currently lose_sum: 0.2490347921848297
time_elpased: 0.639
#iterations: 417
currently lose_sum: 0.25174129009246826
time_elpased: 0.693
#iterations: 418
currently lose_sum: 0.2305024266242981
time_elpased: 0.703
#iterations: 419
currently lose_sum: 0.18937340378761292
time_elpased: 0.69
start validation test
0.629166666667
0.843537414966
0.336956521739
0.481553398058
0.635807806324
26.116
#iterations: 420
currently lose_sum: 0.2652870714664459
time_elpased: 0.677
#iterations: 421
currently lose_sum: 0.15083152055740356
time_elpased: 0.662
#iterations: 422
currently lose_sum: 0.1360558569431305
time_elpased: 0.641
#iterations: 423
currently lose_sum: 0.17640918493270874
time_elpased: 0.658
#iterations: 424
currently lose_sum: 0.2727029025554657
time_elpased: 0.709
#iterations: 425
currently lose_sum: 0.3391460180282593
time_elpased: 0.666
#iterations: 426
currently lose_sum: 0.1435803323984146
time_elpased: 0.651
#iterations: 427
currently lose_sum: 0.1563933789730072
time_elpased: 0.653
#iterations: 428
currently lose_sum: 0.1422910839319229
time_elpased: 0.669
#iterations: 429
currently lose_sum: 0.15838328003883362
time_elpased: 0.704
#iterations: 430
currently lose_sum: 0.1786039173603058
time_elpased: 0.686
#iterations: 431
currently lose_sum: 0.10234102606773376
time_elpased: 0.685
#iterations: 432
currently lose_sum: 0.2725146412849426
time_elpased: 0.695
#iterations: 433
currently lose_sum: 0.18496055901050568
time_elpased: 0.693
#iterations: 434
currently lose_sum: 0.17466291785240173
time_elpased: 0.688
#iterations: 435
currently lose_sum: 0.18597744405269623
time_elpased: 0.681
#iterations: 436
currently lose_sum: 0.1496867537498474
time_elpased: 0.7
#iterations: 437
currently lose_sum: 0.19400499761104584
time_elpased: 0.676
#iterations: 438
currently lose_sum: 0.31963762640953064
time_elpased: 0.659
#iterations: 439
currently lose_sum: 0.3211241662502289
time_elpased: 0.664
start validation test
0.641666666667
0.831325301205
0.375
0.516853932584
0.647727272727
25.933
#iterations: 440
currently lose_sum: 0.2638801336288452
time_elpased: 0.708
#iterations: 441
currently lose_sum: 0.1450713723897934
time_elpased: 0.676
#iterations: 442
currently lose_sum: 0.24000708758831024
time_elpased: 0.673
#iterations: 443
currently lose_sum: 0.07692725211381912
time_elpased: 0.731
#iterations: 444
currently lose_sum: nan
time_elpased: 0.646
train finish final lose is: nan
acc: 0.719
pre: 0.705
rec: 0.768
F1: 0.735
auc: 0.718
