start to construct computing graph
graph construct over
1131
epochs start
#iterations: 0
currently lose_sum: 0.6874885559082031
time_elpased: 0.587
#iterations: 1
currently lose_sum: 0.7043793201446533
time_elpased: 0.516
#iterations: 2
currently lose_sum: 0.7637189626693726
time_elpased: 0.544
#iterations: 3
currently lose_sum: 0.6431153416633606
time_elpased: 0.55
#iterations: 4
currently lose_sum: 0.7198652625083923
time_elpased: 0.491
#iterations: 5
currently lose_sum: 0.6554259061813354
time_elpased: 0.508
#iterations: 6
currently lose_sum: 0.6904613375663757
time_elpased: 0.452
#iterations: 7
currently lose_sum: 0.6227482557296753
time_elpased: 0.444
#iterations: 8
currently lose_sum: 0.5622122287750244
time_elpased: 0.482
#iterations: 9
currently lose_sum: 0.6404651403427124
time_elpased: 0.511
#iterations: 10
currently lose_sum: 0.5901005864143372
time_elpased: 0.499
#iterations: 11
currently lose_sum: 0.568679928779602
time_elpased: 0.49
#iterations: 12
currently lose_sum: 0.5955064296722412
time_elpased: 0.515
#iterations: 13
currently lose_sum: 0.6169888377189636
time_elpased: 0.545
#iterations: 14
currently lose_sum: 0.6751797795295715
time_elpased: 0.504
#iterations: 15
currently lose_sum: 0.5510379672050476
time_elpased: 0.527
#iterations: 16
currently lose_sum: 0.7233659029006958
time_elpased: 0.497
#iterations: 17
currently lose_sum: 0.5226078629493713
time_elpased: 0.536
#iterations: 18
currently lose_sum: 0.601490318775177
time_elpased: 0.512
#iterations: 19
currently lose_sum: 0.5924766659736633
time_elpased: 0.529
start validation test
0.541025641026
0.74358974359
0.146464646465
0.244725738397
0.547190656566
17.778
#iterations: 20
currently lose_sum: 0.5660536289215088
time_elpased: 0.509
#iterations: 21
currently lose_sum: 0.5511020421981812
time_elpased: 0.516
#iterations: 22
currently lose_sum: 0.6525679230690002
time_elpased: 0.529
#iterations: 23
currently lose_sum: 0.6628233194351196
time_elpased: 0.542
#iterations: 24
currently lose_sum: 0.6657378673553467
time_elpased: 0.542
#iterations: 25
currently lose_sum: 0.6422269940376282
time_elpased: 0.54
#iterations: 26
currently lose_sum: 0.6219856142997742
time_elpased: 0.489
#iterations: 27
currently lose_sum: 0.5211135149002075
time_elpased: 0.541
#iterations: 28
currently lose_sum: 0.5841543674468994
time_elpased: 0.544
#iterations: 29
currently lose_sum: 0.7998408675193787
time_elpased: 0.539
#iterations: 30
currently lose_sum: 0.5140191912651062
time_elpased: 0.542
#iterations: 31
currently lose_sum: 0.5792701244354248
time_elpased: 0.544
#iterations: 32
currently lose_sum: 0.6029067635536194
time_elpased: 0.536
#iterations: 33
currently lose_sum: 0.6825305223464966
time_elpased: 0.539
#iterations: 34
currently lose_sum: 0.5906250476837158
time_elpased: 0.542
#iterations: 35
currently lose_sum: 0.45771345496177673
time_elpased: 0.534
#iterations: 36
currently lose_sum: 0.6039251685142517
time_elpased: 0.548
#iterations: 37
currently lose_sum: 0.5043335556983948
time_elpased: 0.527
#iterations: 38
currently lose_sum: 0.5651742219924927
time_elpased: 0.522
#iterations: 39
currently lose_sum: 0.6041585206985474
time_elpased: 0.516
start validation test
0.696153846154
0.688836104513
0.732323232323
0.709914320685
0.695588699495
15.551
#iterations: 40
currently lose_sum: 0.5422356128692627
time_elpased: 0.542
#iterations: 41
currently lose_sum: 0.6083916425704956
time_elpased: 0.554
#iterations: 42
currently lose_sum: 0.6601953506469727
time_elpased: 0.56
#iterations: 43
currently lose_sum: 0.5576449036598206
time_elpased: 0.565
#iterations: 44
currently lose_sum: 0.6578933000564575
time_elpased: 0.511
#iterations: 45
currently lose_sum: 0.5398886799812317
time_elpased: 0.545
#iterations: 46
currently lose_sum: 0.5644349455833435
time_elpased: 0.554
#iterations: 47
currently lose_sum: 0.6132916808128357
time_elpased: 0.537
#iterations: 48
currently lose_sum: 0.6083861589431763
time_elpased: 0.543
#iterations: 49
currently lose_sum: 0.6030523180961609
time_elpased: 0.555
#iterations: 50
currently lose_sum: 0.6026877164840698
time_elpased: 0.555
#iterations: 51
currently lose_sum: 0.6891419291496277
time_elpased: 0.521
#iterations: 52
currently lose_sum: 0.5263316035270691
time_elpased: 0.533
#iterations: 53
currently lose_sum: 0.5743456482887268
time_elpased: 0.544
#iterations: 54
currently lose_sum: 0.5505955815315247
time_elpased: 0.518
#iterations: 55
currently lose_sum: 0.5802030563354492
time_elpased: 0.525
#iterations: 56
currently lose_sum: 0.47893109917640686
time_elpased: 0.456
#iterations: 57
currently lose_sum: 0.38022688031196594
time_elpased: 0.516
#iterations: 58
currently lose_sum: 0.49832388758659363
time_elpased: 0.54
#iterations: 59
currently lose_sum: 0.5408907532691956
time_elpased: 0.506
start validation test
0.692307692308
0.655378486056
0.830808080808
0.732739420935
0.690143623737
15.630
#iterations: 60
currently lose_sum: 0.6001508831977844
time_elpased: 0.501
#iterations: 61
currently lose_sum: 0.5303636789321899
time_elpased: 0.534
#iterations: 62
currently lose_sum: 0.5892839431762695
time_elpased: 0.531
#iterations: 63
currently lose_sum: 0.608376681804657
time_elpased: 0.534
#iterations: 64
currently lose_sum: 0.6063726544380188
time_elpased: 0.516
#iterations: 65
currently lose_sum: 0.4553282558917999
time_elpased: 0.521
#iterations: 66
currently lose_sum: 0.39068374037742615
time_elpased: 0.543
#iterations: 67
currently lose_sum: 0.5846965909004211
time_elpased: 0.47
#iterations: 68
currently lose_sum: 0.474014550447464
time_elpased: 0.514
#iterations: 69
currently lose_sum: 0.6241178512573242
time_elpased: 0.514
#iterations: 70
currently lose_sum: 0.5558518171310425
time_elpased: 0.504
#iterations: 71
currently lose_sum: 0.485479474067688
time_elpased: 0.532
#iterations: 72
currently lose_sum: 0.3983204662799835
time_elpased: 0.514
#iterations: 73
currently lose_sum: 0.46894973516464233
time_elpased: 0.515
#iterations: 74
currently lose_sum: 0.5687848329544067
time_elpased: 0.515
#iterations: 75
currently lose_sum: 0.44141367077827454
time_elpased: 0.527
#iterations: 76
currently lose_sum: 0.6695082783699036
time_elpased: 0.523
#iterations: 77
currently lose_sum: 0.43547922372817993
time_elpased: 0.516
#iterations: 78
currently lose_sum: 0.4642302095890045
time_elpased: 0.51
#iterations: 79
currently lose_sum: 0.4795821011066437
time_elpased: 0.51
start validation test
0.7
0.663306451613
0.830808080808
0.737668161435
0.697956123737
15.154
#iterations: 80
currently lose_sum: 0.43832728266716003
time_elpased: 0.477
#iterations: 81
currently lose_sum: 0.505429208278656
time_elpased: 0.506
#iterations: 82
currently lose_sum: 0.4542342722415924
time_elpased: 0.51
#iterations: 83
currently lose_sum: 0.4165133237838745
time_elpased: 0.495
#iterations: 84
currently lose_sum: 0.5563386678695679
time_elpased: 0.525
#iterations: 85
currently lose_sum: 0.4616822302341461
time_elpased: 0.511
#iterations: 86
currently lose_sum: 0.6348146796226501
time_elpased: 0.465
#iterations: 87
currently lose_sum: 0.4819526970386505
time_elpased: 0.537
#iterations: 88
currently lose_sum: 0.5671745538711548
time_elpased: 0.502
#iterations: 89
currently lose_sum: 0.6372308731079102
time_elpased: 0.484
#iterations: 90
currently lose_sum: 0.5078744292259216
time_elpased: 0.479
#iterations: 91
currently lose_sum: 0.41099533438682556
time_elpased: 0.48
#iterations: 92
currently lose_sum: 0.4825873076915741
time_elpased: 0.511
#iterations: 93
currently lose_sum: 0.4992576539516449
time_elpased: 0.541
#iterations: 94
currently lose_sum: 0.6816424131393433
time_elpased: 0.509
#iterations: 95
currently lose_sum: 0.4630424380302429
time_elpased: 0.511
#iterations: 96
currently lose_sum: 0.5242040753364563
time_elpased: 0.473
#iterations: 97
currently lose_sum: 0.5431072115898132
time_elpased: 0.498
#iterations: 98
currently lose_sum: 0.4480854570865631
time_elpased: 0.504
#iterations: 99
currently lose_sum: 0.6548029184341431
time_elpased: 0.478
start validation test
0.696153846154
0.760655737705
0.585858585859
0.661911554922
0.697877209596
19.185
#iterations: 100
currently lose_sum: 0.39348936080932617
time_elpased: 0.503
#iterations: 101
currently lose_sum: 0.4007757306098938
time_elpased: 0.472
#iterations: 102
currently lose_sum: 0.4764353334903717
time_elpased: 0.511
#iterations: 103
currently lose_sum: 0.4387769103050232
time_elpased: 0.488
#iterations: 104
currently lose_sum: 0.5211800932884216
time_elpased: 0.546
#iterations: 105
currently lose_sum: 0.5354396104812622
time_elpased: 0.496
#iterations: 106
currently lose_sum: 0.3822706937789917
time_elpased: 0.486
#iterations: 107
currently lose_sum: 0.4634276330471039
time_elpased: 0.517
#iterations: 108
currently lose_sum: 0.6758058071136475
time_elpased: 0.524
#iterations: 109
currently lose_sum: 0.4030466377735138
time_elpased: 0.532
#iterations: 110
currently lose_sum: 0.4212472140789032
time_elpased: 0.49
#iterations: 111
currently lose_sum: 0.3591732978820801
time_elpased: 0.48
#iterations: 112
currently lose_sum: 0.520575225353241
time_elpased: 0.486
#iterations: 113
currently lose_sum: 0.5788335204124451
time_elpased: 0.493
#iterations: 114
currently lose_sum: 0.4375039339065552
time_elpased: 0.5
#iterations: 115
currently lose_sum: 0.39449888467788696
time_elpased: 0.46
#iterations: 116
currently lose_sum: 0.6254765391349792
time_elpased: 0.506
#iterations: 117
currently lose_sum: 0.503269612789154
time_elpased: 0.494
#iterations: 118
currently lose_sum: 0.4450809359550476
time_elpased: 0.481
#iterations: 119
currently lose_sum: 0.5266790986061096
time_elpased: 0.46
start validation test
0.714102564103
0.717884130982
0.719696969697
0.718789407314
0.714015151515
15.906
#iterations: 120
currently lose_sum: 0.4207926392555237
time_elpased: 0.492
#iterations: 121
currently lose_sum: 0.5784007906913757
time_elpased: 0.496
#iterations: 122
currently lose_sum: 0.57676762342453
time_elpased: 0.507
#iterations: 123
currently lose_sum: 0.5075104236602783
time_elpased: 0.496
#iterations: 124
currently lose_sum: 0.409883588552475
time_elpased: 0.501
#iterations: 125
currently lose_sum: 0.5428361892700195
time_elpased: 0.495
#iterations: 126
currently lose_sum: 0.725482165813446
time_elpased: 0.457
#iterations: 127
currently lose_sum: 0.6660237908363342
time_elpased: 0.524
#iterations: 128
currently lose_sum: 0.548897385597229
time_elpased: 0.499
#iterations: 129
currently lose_sum: 0.5114724040031433
time_elpased: 0.527
#iterations: 130
currently lose_sum: 0.5514066219329834
time_elpased: 0.507
#iterations: 131
currently lose_sum: 0.5956777334213257
time_elpased: 0.506
#iterations: 132
currently lose_sum: 0.5450229644775391
time_elpased: 0.533
#iterations: 133
currently lose_sum: 0.5429994463920593
time_elpased: 0.499
#iterations: 134
currently lose_sum: 0.4814293682575226
time_elpased: 0.531
#iterations: 135
currently lose_sum: 0.40339893102645874
time_elpased: 0.543
#iterations: 136
currently lose_sum: 0.33568328619003296
time_elpased: 0.531
#iterations: 137
currently lose_sum: 0.5100687742233276
time_elpased: 0.483
#iterations: 138
currently lose_sum: 0.5569162368774414
time_elpased: 0.504
#iterations: 139
currently lose_sum: 0.4765925109386444
time_elpased: 0.509
start validation test
0.634615384615
0.832335329341
0.35101010101
0.49378330373
0.639046717172
24.322
#iterations: 140
currently lose_sum: 0.3504868149757385
time_elpased: 0.447
#iterations: 141
currently lose_sum: 0.5209723711013794
time_elpased: 0.513
#iterations: 142
currently lose_sum: 0.7890103459358215
time_elpased: 0.481
#iterations: 143
currently lose_sum: 0.46628502011299133
time_elpased: 0.487
#iterations: 144
currently lose_sum: 0.44019120931625366
time_elpased: 0.493
#iterations: 145
currently lose_sum: 0.2568318545818329
time_elpased: 0.492
#iterations: 146
currently lose_sum: 0.6539381742477417
time_elpased: 0.496
#iterations: 147
currently lose_sum: 0.44368740916252136
time_elpased: 0.494
#iterations: 148
currently lose_sum: 0.42971131205558777
time_elpased: 0.516
#iterations: 149
currently lose_sum: 0.6161877512931824
time_elpased: 0.495
#iterations: 150
currently lose_sum: 0.3753899931907654
time_elpased: 0.472
#iterations: 151
currently lose_sum: 0.4036196768283844
time_elpased: 0.483
#iterations: 152
currently lose_sum: 0.5451247096061707
time_elpased: 0.511
#iterations: 153
currently lose_sum: 0.41556206345558167
time_elpased: 0.51
#iterations: 154
currently lose_sum: 0.5016249418258667
time_elpased: 0.492
#iterations: 155
currently lose_sum: 0.3854789137840271
time_elpased: 0.527
#iterations: 156
currently lose_sum: 0.6959521174430847
time_elpased: 0.515
#iterations: 157
currently lose_sum: 0.42043301463127136
time_elpased: 0.49
#iterations: 158
currently lose_sum: 0.32128646969795227
time_elpased: 0.491
#iterations: 159
currently lose_sum: 0.41336002945899963
time_elpased: 0.479
start validation test
0.703846153846
0.757009345794
0.613636363636
0.677824267782
0.705255681818
16.217
#iterations: 160
currently lose_sum: 0.3499900996685028
time_elpased: 0.491
#iterations: 161
currently lose_sum: 0.6052886247634888
time_elpased: 0.509
#iterations: 162
currently lose_sum: 0.336791455745697
time_elpased: 0.491
#iterations: 163
currently lose_sum: 0.4890076220035553
time_elpased: 0.517
#iterations: 164
currently lose_sum: 0.3468678891658783
time_elpased: 0.49
#iterations: 165
currently lose_sum: 0.2575382888317108
time_elpased: 0.499
#iterations: 166
currently lose_sum: 0.37623390555381775
time_elpased: 0.496
#iterations: 167
currently lose_sum: 0.47058966755867004
time_elpased: 0.495
#iterations: 168
currently lose_sum: 0.5500919222831726
time_elpased: 0.495
#iterations: 169
currently lose_sum: 0.44051393866539
time_elpased: 0.407
#iterations: 170
currently lose_sum: 0.5344511866569519
time_elpased: 0.495
#iterations: 171
currently lose_sum: 0.34643468260765076
time_elpased: 0.483
#iterations: 172
currently lose_sum: 0.3349755108356476
time_elpased: 0.524
#iterations: 173
currently lose_sum: 0.4353576600551605
time_elpased: 0.48
#iterations: 174
currently lose_sum: 0.3319324254989624
time_elpased: 0.503
#iterations: 175
currently lose_sum: 0.28952130675315857
time_elpased: 0.495
#iterations: 176
currently lose_sum: 0.5452749729156494
time_elpased: 0.473
#iterations: 177
currently lose_sum: 0.44212737679481506
time_elpased: 0.502
#iterations: 178
currently lose_sum: 0.5117549300193787
time_elpased: 0.485
#iterations: 179
currently lose_sum: 0.5973585844039917
time_elpased: 0.504
start validation test
0.646153846154
0.837078651685
0.376262626263
0.519163763066
0.650370896465
20.982
#iterations: 180
currently lose_sum: 0.6399462223052979
time_elpased: 0.503
#iterations: 181
currently lose_sum: 0.4077325463294983
time_elpased: 0.461
#iterations: 182
currently lose_sum: 0.7079867720603943
time_elpased: 0.488
#iterations: 183
currently lose_sum: 0.5462474822998047
time_elpased: 0.505
#iterations: 184
currently lose_sum: 0.5525467991828918
time_elpased: 0.487
#iterations: 185
currently lose_sum: 0.4517787992954254
time_elpased: 0.526
#iterations: 186
currently lose_sum: 0.5045983195304871
time_elpased: 0.477
#iterations: 187
currently lose_sum: 0.4281662106513977
time_elpased: 0.492
#iterations: 188
currently lose_sum: 0.3251745402812958
time_elpased: 0.51
#iterations: 189
currently lose_sum: 0.2640088200569153
time_elpased: 0.513
#iterations: 190
currently lose_sum: 0.24940776824951172
time_elpased: 0.498
#iterations: 191
currently lose_sum: 0.4833775758743286
time_elpased: 0.461
#iterations: 192
currently lose_sum: 0.7073548436164856
time_elpased: 0.495
#iterations: 193
currently lose_sum: 0.35777512192726135
time_elpased: 0.49
#iterations: 194
currently lose_sum: 0.302192360162735
time_elpased: 0.506
#iterations: 195
currently lose_sum: 0.3706367611885071
time_elpased: 0.524
#iterations: 196
currently lose_sum: 0.42972901463508606
time_elpased: 0.521
#iterations: 197
currently lose_sum: 0.4615398347377777
time_elpased: 0.511
#iterations: 198
currently lose_sum: 0.4526534676551819
time_elpased: 0.477
#iterations: 199
currently lose_sum: 0.43255355954170227
time_elpased: 0.505
start validation test
0.714102564103
0.797250859107
0.585858585859
0.675400291121
0.716106376263
19.459
#iterations: 200
currently lose_sum: 0.3796965181827545
time_elpased: 0.494
#iterations: 201
currently lose_sum: 0.35946741700172424
time_elpased: 0.509
#iterations: 202
currently lose_sum: 0.44123461842536926
time_elpased: 0.528
#iterations: 203
currently lose_sum: 0.3460267186164856
time_elpased: 0.565
#iterations: 204
currently lose_sum: 0.5007627010345459
time_elpased: 0.562
#iterations: 205
currently lose_sum: 0.46971601247787476
time_elpased: 0.516
#iterations: 206
currently lose_sum: 0.45704519748687744
time_elpased: 0.477
#iterations: 207
currently lose_sum: 0.39413362741470337
time_elpased: 0.498
#iterations: 208
currently lose_sum: 0.5910504460334778
time_elpased: 0.489
#iterations: 209
currently lose_sum: 0.4297627806663513
time_elpased: 0.482
#iterations: 210
currently lose_sum: 0.49177077412605286
time_elpased: 0.468
#iterations: 211
currently lose_sum: 0.211796835064888
time_elpased: 0.501
#iterations: 212
currently lose_sum: 0.33306974172592163
time_elpased: 0.5
#iterations: 213
currently lose_sum: 0.4330579340457916
time_elpased: 0.463
#iterations: 214
currently lose_sum: 0.525579571723938
time_elpased: 0.481
#iterations: 215
currently lose_sum: 0.3839893639087677
time_elpased: 0.501
#iterations: 216
currently lose_sum: 0.39623865485191345
time_elpased: 0.496
#iterations: 217
currently lose_sum: 0.34923750162124634
time_elpased: 0.476
#iterations: 218
currently lose_sum: 0.4030971825122833
time_elpased: 0.541
#iterations: 219
currently lose_sum: 0.41854050755500793
time_elpased: 0.482
start validation test
0.697435897436
0.79197080292
0.54797979798
0.64776119403
0.69977114899
17.779
#iterations: 220
currently lose_sum: 0.5970479846000671
time_elpased: 0.445
#iterations: 221
currently lose_sum: 0.40677520632743835
time_elpased: 0.541
#iterations: 222
currently lose_sum: 0.4341287314891815
time_elpased: 0.502
#iterations: 223
currently lose_sum: 0.32020658254623413
time_elpased: 0.501
#iterations: 224
currently lose_sum: 0.37734535336494446
time_elpased: 0.52
#iterations: 225
currently lose_sum: 0.3650854825973511
time_elpased: 0.47
#iterations: 226
currently lose_sum: 0.48072153329849243
time_elpased: 0.507
#iterations: 227
currently lose_sum: 0.46898698806762695
time_elpased: 0.509
#iterations: 228
currently lose_sum: 0.5064880847930908
time_elpased: 0.495
#iterations: 229
currently lose_sum: 0.554747462272644
time_elpased: 0.498
#iterations: 230
currently lose_sum: 0.285042405128479
time_elpased: 0.47
#iterations: 231
currently lose_sum: 0.35795193910598755
time_elpased: 0.439
#iterations: 232
currently lose_sum: 0.24937224388122559
time_elpased: 0.469
#iterations: 233
currently lose_sum: 0.2945343852043152
time_elpased: 0.487
#iterations: 234
currently lose_sum: 0.3938797116279602
time_elpased: 0.48
#iterations: 235
currently lose_sum: 0.28130149841308594
time_elpased: 0.504
#iterations: 236
currently lose_sum: 0.3624749779701233
time_elpased: 0.451
#iterations: 237
currently lose_sum: 0.3760318160057068
time_elpased: 0.486
#iterations: 238
currently lose_sum: 0.39670875668525696
time_elpased: 0.473
#iterations: 239
currently lose_sum: 0.5269498825073242
time_elpased: 0.512
start validation test
0.726923076923
0.698481561822
0.813131313131
0.751458576429
0.725576073232
15.339
#iterations: 240
currently lose_sum: 0.4434802234172821
time_elpased: 0.506
#iterations: 241
currently lose_sum: 0.496237576007843
time_elpased: 0.506
#iterations: 242
currently lose_sum: 0.25249871611595154
time_elpased: 0.5
#iterations: 243
currently lose_sum: 0.24364063143730164
time_elpased: 0.494
#iterations: 244
currently lose_sum: 0.31849390268325806
time_elpased: 0.487
#iterations: 245
currently lose_sum: 0.3035186231136322
time_elpased: 0.438
#iterations: 246
currently lose_sum: 0.40620124340057373
time_elpased: 0.46
#iterations: 247
currently lose_sum: 0.3486007750034332
time_elpased: 0.516
#iterations: 248
currently lose_sum: 0.5445592999458313
time_elpased: 0.495
#iterations: 249
currently lose_sum: 0.6512824892997742
time_elpased: 0.488
#iterations: 250
currently lose_sum: 0.3436109125614166
time_elpased: 0.503
#iterations: 251
currently lose_sum: 0.39362722635269165
time_elpased: 0.504
#iterations: 252
currently lose_sum: 0.3263517916202545
time_elpased: 0.511
#iterations: 253
currently lose_sum: 0.3357762098312378
time_elpased: 0.508
#iterations: 254
currently lose_sum: 0.4005882143974304
time_elpased: 0.497
#iterations: 255
currently lose_sum: 0.36126595735549927
time_elpased: 0.498
#iterations: 256
currently lose_sum: 0.5526201128959656
time_elpased: 0.503
#iterations: 257
currently lose_sum: 0.721616804599762
time_elpased: 0.476
#iterations: 258
currently lose_sum: 0.2717066705226898
time_elpased: 0.473
#iterations: 259
currently lose_sum: 0.45579078793525696
time_elpased: 0.512
start validation test
0.721794871795
0.739946380697
0.69696969697
0.717815344603
0.722182765152
18.993
#iterations: 260
currently lose_sum: 0.34907266497612
time_elpased: 0.484
#iterations: 261
currently lose_sum: 0.3387240469455719
time_elpased: 0.503
#iterations: 262
currently lose_sum: 0.43035778403282166
time_elpased: 0.488
#iterations: 263
currently lose_sum: 0.3084680438041687
time_elpased: 0.511
#iterations: 264
currently lose_sum: 0.5280722975730896
time_elpased: 0.485
#iterations: 265
currently lose_sum: 0.2386430948972702
time_elpased: 0.505
#iterations: 266
currently lose_sum: 0.5358404517173767
time_elpased: 0.483
#iterations: 267
currently lose_sum: 0.304959774017334
time_elpased: 0.481
#iterations: 268
currently lose_sum: 0.37586498260498047
time_elpased: 0.535
#iterations: 269
currently lose_sum: 0.42086926102638245
time_elpased: 0.483
#iterations: 270
currently lose_sum: 0.5756723284721375
time_elpased: 0.485
#iterations: 271
currently lose_sum: 0.4631557762622833
time_elpased: 0.476
#iterations: 272
currently lose_sum: 0.6381402611732483
time_elpased: 0.504
#iterations: 273
currently lose_sum: 0.7590184807777405
time_elpased: 0.485
#iterations: 274
currently lose_sum: 0.43333691358566284
time_elpased: 0.503
#iterations: 275
currently lose_sum: 0.6987294554710388
time_elpased: 0.478
#iterations: 276
currently lose_sum: 0.4734892249107361
time_elpased: 0.483
#iterations: 277
currently lose_sum: 0.4045499265193939
time_elpased: 0.497
#iterations: 278
currently lose_sum: 0.5148178339004517
time_elpased: 0.512
#iterations: 279
currently lose_sum: 0.3815304636955261
time_elpased: 0.535
start validation test
0.721794871795
0.726582278481
0.724747474747
0.725663716814
0.721748737374
17.550
#iterations: 280
currently lose_sum: 0.3538453280925751
time_elpased: 0.503
#iterations: 281
currently lose_sum: 0.23485665023326874
time_elpased: 0.48
#iterations: 282
currently lose_sum: 0.4208088517189026
time_elpased: 0.466
#iterations: 283
currently lose_sum: 0.49443337321281433
time_elpased: 0.509
#iterations: 284
currently lose_sum: 0.4374540448188782
time_elpased: 0.496
#iterations: 285
currently lose_sum: 0.29070618748664856
time_elpased: 0.467
#iterations: 286
currently lose_sum: 0.3610887825489044
time_elpased: 0.502
#iterations: 287
currently lose_sum: 0.24108359217643738
time_elpased: 0.527
#iterations: 288
currently lose_sum: 0.4068487882614136
time_elpased: 0.491
#iterations: 289
currently lose_sum: 0.2755487859249115
time_elpased: 0.522
#iterations: 290
currently lose_sum: 0.5711963772773743
time_elpased: 0.488
#iterations: 291
currently lose_sum: 0.3207423985004425
time_elpased: 0.493
#iterations: 292
currently lose_sum: 0.5127871632575989
time_elpased: 0.505
#iterations: 293
currently lose_sum: 0.3399762511253357
time_elpased: 0.522
#iterations: 294
currently lose_sum: 0.32460692524909973
time_elpased: 0.461
#iterations: 295
currently lose_sum: 0.3071090579032898
time_elpased: 0.486
#iterations: 296
currently lose_sum: 0.47452160716056824
time_elpased: 0.502
#iterations: 297
currently lose_sum: 0.40594223141670227
time_elpased: 0.491
#iterations: 298
currently lose_sum: 0.3573227822780609
time_elpased: 0.495
#iterations: 299
currently lose_sum: 0.3705621361732483
time_elpased: 0.487
start validation test
0.697435897436
0.814960629921
0.522727272727
0.636923076923
0.700165719697
27.522
#iterations: 300
currently lose_sum: 0.3811227083206177
time_elpased: 0.489
#iterations: 301
currently lose_sum: 0.2547207176685333
time_elpased: 0.507
#iterations: 302
currently lose_sum: 0.17849212884902954
time_elpased: 0.507
#iterations: 303
currently lose_sum: 0.1791241616010666
time_elpased: 0.509
#iterations: 304
currently lose_sum: 0.34050142765045166
time_elpased: 0.511
#iterations: 305
currently lose_sum: 0.24464799463748932
time_elpased: 0.495
#iterations: 306
currently lose_sum: 0.5194642543792725
time_elpased: 0.495
#iterations: 307
currently lose_sum: 0.3105056583881378
time_elpased: 0.515
#iterations: 308
currently lose_sum: 0.24748452007770538
time_elpased: 0.483
#iterations: 309
currently lose_sum: 0.40509599447250366
time_elpased: 0.509
#iterations: 310
currently lose_sum: 0.34728938341140747
time_elpased: 0.514
#iterations: 311
currently lose_sum: 0.17489686608314514
time_elpased: 0.498
#iterations: 312
currently lose_sum: 0.4443100392818451
time_elpased: 0.446
#iterations: 313
currently lose_sum: 0.3662257790565491
time_elpased: 0.497
#iterations: 314
currently lose_sum: 0.3047090470790863
time_elpased: 0.47
#iterations: 315
currently lose_sum: 0.2980593144893646
time_elpased: 0.486
#iterations: 316
currently lose_sum: 0.531951904296875
time_elpased: 0.48
#iterations: 317
currently lose_sum: 0.2704312205314636
time_elpased: 0.496
#iterations: 318
currently lose_sum: 0.5310935378074646
time_elpased: 0.484
#iterations: 319
currently lose_sum: 0.2499101310968399
time_elpased: 0.503
start validation test
0.716666666667
0.738419618529
0.684343434343
0.710353866317
0.717171717172
20.146
#iterations: 320
currently lose_sum: 0.41533347964286804
time_elpased: 0.496
#iterations: 321
currently lose_sum: 0.2073955237865448
time_elpased: 0.484
#iterations: 322
currently lose_sum: 0.6385913491249084
time_elpased: 0.525
#iterations: 323
currently lose_sum: 0.6960151791572571
time_elpased: 0.554
#iterations: 324
currently lose_sum: 0.6931421160697937
time_elpased: 0.484
#iterations: 325
currently lose_sum: 0.6930736303329468
time_elpased: 0.476
#iterations: 326
currently lose_sum: 0.6930921673774719
time_elpased: 0.496
#iterations: 327
currently lose_sum: 0.6930062174797058
time_elpased: 0.481
#iterations: 328
currently lose_sum: 0.6927900910377502
time_elpased: 0.439
#iterations: 329
currently lose_sum: 0.6927751898765564
time_elpased: 0.404
#iterations: 330
currently lose_sum: 0.6921511888504028
time_elpased: 0.467
#iterations: 331
currently lose_sum: 0.6907684206962585
time_elpased: 0.48
#iterations: 332
currently lose_sum: 0.6720768809318542
time_elpased: 0.501
#iterations: 333
currently lose_sum: 0.5421192646026611
time_elpased: 0.494
#iterations: 334
currently lose_sum: 0.5476636290550232
time_elpased: 0.497
#iterations: 335
currently lose_sum: 0.5524339079856873
time_elpased: 0.494
#iterations: 336
currently lose_sum: 0.44574785232543945
time_elpased: 0.491
#iterations: 337
currently lose_sum: 0.5992835164070129
time_elpased: 0.468
#iterations: 338
currently lose_sum: 0.45911893248558044
time_elpased: 0.501
#iterations: 339
currently lose_sum: 0.5264260768890381
time_elpased: 0.471
start validation test
0.739743589744
0.781341107872
0.676767676768
0.725304465494
0.740727588384
15.124
#iterations: 340
currently lose_sum: 0.45853328704833984
time_elpased: 0.494
#iterations: 341
currently lose_sum: 0.5862318873405457
time_elpased: 0.485
#iterations: 342
currently lose_sum: 0.41981688141822815
time_elpased: 0.512
#iterations: 343
currently lose_sum: 0.26898571848869324
time_elpased: 0.546
#iterations: 344
currently lose_sum: 0.20336686074733734
time_elpased: 0.484
#iterations: 345
currently lose_sum: 0.38503652811050415
time_elpased: 0.499
#iterations: 346
currently lose_sum: 0.37877628207206726
time_elpased: 0.506
#iterations: 347
currently lose_sum: 0.5242800116539001
time_elpased: 0.498
#iterations: 348
currently lose_sum: 0.23572741448879242
time_elpased: 0.523
#iterations: 349
currently lose_sum: 0.1609908491373062
time_elpased: 0.463
#iterations: 350
currently lose_sum: 0.33587533235549927
time_elpased: 0.476
#iterations: 351
currently lose_sum: 0.3961014151573181
time_elpased: 0.488
#iterations: 352
currently lose_sum: 0.3717723488807678
time_elpased: 0.513
#iterations: 353
currently lose_sum: 0.47730109095573425
time_elpased: 0.5
#iterations: 354
currently lose_sum: 0.5706010460853577
time_elpased: 0.498
#iterations: 355
currently lose_sum: 0.4614343047142029
time_elpased: 0.491
#iterations: 356
currently lose_sum: 0.3800583779811859
time_elpased: 0.536
#iterations: 357
currently lose_sum: 1.0638960599899292
time_elpased: 0.528
#iterations: 358
currently lose_sum: 0.6932165622711182
time_elpased: 0.507
#iterations: 359
currently lose_sum: 0.693204939365387
time_elpased: 0.521
start validation test
0.492307692308
0.0
0.0
nan
0.5
18.024
acc: 0.760
pre: 0.783
rec: 0.734
F1: 0.758
auc: 0.760
