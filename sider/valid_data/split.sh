#!/bin/tcsh
foreach a (0 1 2 3 4)
	if ($a == 4) then
		set b=0
	else 
		set b=`expr $a + 1`
	endif
	echo $b 
	paste $1 $1_drug_combination_id.txt | awk -v a="$a" 'BEGIN{ORS=""}{if($1==a) {for(i=2;i<=NF;i++) {print $i-1"\t"}; print 1"\n"} }' > PN_test_$a\_$1.txt
	paste $1 $1_drug_combination_id.txt | awk -v b="$b" 'BEGIN{ORS=""}{if($1==b) {for(i=2;i<=NF;i++) {print $i-1"\t"}; print 1"\n"} }' > PN_validation_$a\_$1.txt
	paste $1 $1_drug_combination_id.txt | awk -v a="$a" -v b="$b" 'BEGIN{ORS=""}{if($1!=a && $1!=b) {for(i=2;i<=NF;i++) {print $i-1"\t"}; print 1"\n"} }' > PN_train_$a\_$1.txt
end
