import numpy as np
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
plt.switch_backend('agg')
from ggplot import *
import pandas as pd
import sys
import pdb

class Visualiza:
	def __init__(self):

		self.original_embedding = None
		self.entity_categary = None
		self.TSNE_embedding =None

	def load_embedding(self, data_path):
		
		self.original_embedding = np.loadtxt(data_path, delimiter = ',')

	def load_category(self, categary_path):

		with open(categary_path) as categary_f:
			categaries = categary_f.readlines()

		self.entity_categary = [single_categary.strip('\n') for single_categary in categaries]	
	
	def get_tSNE_embedding(self):
		
		embedding_shape = self.original_embedding.shape
		print('original embedding shape')
		print(embedding_shape)

		tsne_model = TSNE(random_state = 0, init='pca')
		self.tSNE_embedding = tsne_model.fit_transform(self.original_embedding)

	def save_tSNE_embedding(self, tSNE_path):
		
		np.savetxt(tSNE_path, self.tSNE_embedding)

	def draw_2D_fig(self, embedding_path, fig_path, fig_name, model_name, sub_categ_list):
		
		# create dataframe
		tSNE_embedding = np.loadtxt(embedding_path)
		df_data = pd.DataFrame(tSNE_embedding, columns = ['X', 'Y'])
		df_label = pd.DataFrame(self.entity_categary, columns = ['Category'])
		df = pd.concat([df_data, df_label], axis = 1)
                #pdb.set_trace()

		# select subset of df
		#sub_df = df.loc[ df['Category'].isin(sub_categ_list) ]
		
		#draw
		chart = ggplot(df, aes(x = 'X', y = 'Y', color = 'Category')) \
                + geom_point(size=15, alpha = 1) \
                + xlab('X') + ylab('Y') + ggtitle(model_name)

		chart

		chart.save(fig_path + fig_name)

def main():

	import_file = sys.argv[1]
	output_file = sys.argv[2]
	model_name = sys.argv[2]

	data_path = './'
	tSNE_path = './tsne_embedding/' + import_file
	fig_path = './fig/'
	fig_name = sys.argv[2] + '.png'
        sub_categ_list = ['no_myopathy', 'myopathy', 'no_appear']

	vis = Visualiza()
	vis.load_embedding(import_file)
	vis.load_category('category_se.txt')
	vis.get_tSNE_embedding()
	vis.save_tSNE_embedding(tSNE_path)
	vis.draw_2D_fig(tSNE_path, fig_path, fig_name, model_name, sub_categ_list)

main()
