#!/bin/tcsh

foreach folds (0 1 2 3 4)
	python Visualization.py drug_embedding_attention_0_max_pool_1_PNN_entire_test$folds.txt max$folds
end

foreach folds (0 1 2 3 4)
        python Visualization.py drug_embedding_attention_1_max_pool_0_PNN_entire_test$folds.txt att$folds
end
