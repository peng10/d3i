import numpy as np
import sys
import pdb
import math

embedding = np.loadtxt('drug_embedding_negasam.txt', delimiter = ' ')

xlow = int(sys.argv[1])
xhigh = int(sys.argv[2])
ylow = int(sys.argv[3])
yhigh = int(sys.argv[4])

l = []
for rows in embedding:
	if(rows[0] > xlow and rows[0] < xhigh and rows[1] > ylow and rows[1] < yhigh):
		l.append(rows)


matrix = np.asarray(l)
center = np.mean(matrix, axis = 0)
distance = []
distance_x = []
distance_y = []
for rows in matrix:
	distance_rows = math.sqrt((rows[0] - center[0])**2 + (rows[1] - center[1])**2)
	distancex = rows[0] - center[0]
	distancey = rows[1] - center[1]
	distance.append(distance_rows)
	distance_x.append(distancex)
	distance_y.append(distancey)

max_distance = max(distance)
print ("%.3f %.3f %.3f %.3f %.3f" % (center[0], center[1], max_distance, max(distance_x), max(distance_y)))
