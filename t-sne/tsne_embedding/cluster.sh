#!/bin/tcsh

set xlow = $1
set xhigh = $2
set ylow = $3
set yhigh = $4

less drug_embedding_negasam.txt |awk -v "xlow=$xlow" -v "xhigh=$xhigh" -v "ylow=$ylow" -v "yhigh=$yhigh" '{if($1>=xlow && $1<=xhigh && $2>=ylow && $2<= yhigh) {print NR";"$0} }' > temp

set num_drug = `less temp | wc | awk '{print $1}'`
cat temp id_dbid_drugName_target.txt | awk -v "num_drug=$num_drug" -F';' '{if(NR<=num_drug) {a[$1]=""} else {if($1 in a) {print $0} } }' > cluster_$1_$2_$3_$4_TPRN.txt
